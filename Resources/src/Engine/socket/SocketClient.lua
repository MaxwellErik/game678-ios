SOCKET_EVENT_TABLE = 
{
	ON_MESSAGE_HANDLER=0,
	ON_CONNECTED_HANDLER=1,
	ON_CONNECTTIMEOUT_HANDLER=2,
	ON_DISCONNECTED_HANDLER=3,
	ON_EXCEPTION_HANDLER=4,
}
--c++版本的socket封装
function SocketCc()
	local sc = {};
	
	--重置retry参数
	function sc.initRetryArg()
		sc.MAX_RETRY_TIME = 5;
		sc.NOW_RETRY_TIME = 0;
		sc.RETRY_PER_SECOND = 3;--失败之后每过多少秒再次尝试
		sc.LAST_RETRY_TIME = 0;--上一次尝试的时间
		sc.isRetrying = false;--是否正在尝试重新连接
	end
	
	function sc.clear()
		sc.name = "";
		sc.socket = nil;
		sc.ip = "";
		sc.port = -1;
		sc.isConnect = false;
		
		sc.retry = false;--断开连接之后是否重新连接
		
		sc.initRetryArg();
		
		if sc.SocketThreadKey then
			StopThread(sc.SocketThreadKey);
		end
		
	end

	--销毁这个socket
	function sc.destroy()
		if sc.socket then
			sc.socket:release();--create的时候自动的retain了一次并且autoRelease()，如果需要释放内存，只需要release一次就足够了。
		end
		sc.clear();
	end
	
		
	--展开连接
	function sc.connect(_ip,_port,_name)
		if not sc.socket then
			local socket = cc.SocketClient:create();
			sc.socket = socket;
			sc.registerHandler();
		end
		sc.ip = _ip;
		sc.port = _port;
		sc.name = _name;
		
		local socket = sc.socket;
		socket:startConnect(_ip,_port);
		socket:setName(_name);
	end
	
	--socket连接成功的回调
	function sc.OnConnected()
		Log("Socket : "..sc.name..",ip = "..sc.ip..",port = "..sc.port..",Connect success!");
		sc.isConnect = true;
		sc.initRetryArg();
		sc.SocketThreadKey = "SocketThreadKey";
		StartThread(sc.update,sc.SocketThreadKey);--开始协程
		
		
		local proto = require "app.protoco"
		local sproto = require "Engine.sproto.sproto"
		local sp = sproto.new(proto.s2c);
		local host = sp:host("package");
		
		local request = host:attach(sproto.new(proto.c2s));
		
		local session = 1;
		local str = request("handshake",nil,session);
		local packaged = string.pack(">s2",str);
		local socket = sc.socket;
		local n = #str;
		local m = #packaged;
		Log("n ======== "..n..",m =========== "..m..",str = "..str);
		socket:sendData(packaged);
		
	end
	
	--sockect断开连接的回调
	function sc.OnDisconnected()
		Log("Socket : "..sc.name..",ip = "..sc.ip..",port = "..sc.port..",DisConnect!");
	end
	
	--socket连接超时的回调
	function sc.OnConnectTimeOut()
		Log("Socket : "..sc.name..",ip = "..sc.ip..",port = "..sc.port..",Connect timeout!");
	end

	--socket连接错误的回调
	function sc.OnConnectError(code)
		Log("Socket : "..sc.name..",ip = "..sc.ip..",port = "..sc.port..",Connect error : "..code.."!");
	end
	
	--回调消息处理
	function sc.OnMessage()
		
	end
	
	--协程，每帧回调
	function sc.update(dt)
		while true do
			
			WaitFrame();
		end
	end
	
	--注册回调函数到c++
	function sc.registerHandler()
		local socket = sc.socket;
		local funcs = {sc.OnMessage,sc.OnConnected,sc.OnConnectTimeOut,sc.OnDisconnected,sc.OnConnectError};
		for key,val in pairs(SOCKET_EVENT_TABLE) do
			local f = funcs[val + 1];
			socket:registerScriptHandler(f,val);
		end
	end
	
	return sc;
end