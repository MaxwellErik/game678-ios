Global = {};
sockt = require("socket.core");--用来获取时间
require("Engine.TimeUtil");--加载一些时间的处理函数
require("Engine.Common");--加载通用方法
require("Engine.ActionCommon");--加载动作通用方法
require("Engine.shader");--加载shader
require("Engine.Thread.Thread");--加载协程
require("Engine.Object");--Object对象和一些继承的方法
require("Engine.UIObject");--UI的对象声明文件
require("Engine.UIManager");--加载UI管理器
require("Engine.CocosAnimation.init");--加载Cocostudio动画封装
--require("Engine.sproto.sproto")
--require("Engine.socket.Socket");--网络连接
--require("Engine.socket.SocketClient");--C++网络连接

--退出当前app
function ExitApp()
	RemoveAllUI();--首先清理所有的UI
	
	
	ClearUIManager();--最后释放UI管理器
end

--主线程
function MainThread(dt)
	UpdateThread(dt);--刷新所有的协程
end

--初始化所有的数据
function InitApp()
	InitThread();
	InitUIManager();
	Global["MainThreadID"] = AddTimerTask(MainThread,0);
end