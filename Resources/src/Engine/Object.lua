--从很多table中找出某个字段
function search(classes,key)
    for i = 1,#classes do
        local value = classes[i][key];
        if value ~= nil then
            return value;
        end
    end
end

--继承
function CreateClass(...)
    local parent = {...};
    local child = {};

    --设置类的元表
    setmetatable(child,{
        __index = function(table,key)
            return search(parent,key);
        end
    });

    --给新类增加一个new函数，用于创建对象
    function child:new()
        local o = {};
        setmetatable(o,self);
        self.__index = self;
        return o;
    end

    -- 返回这个继承了多个类的子类
    return child;
end