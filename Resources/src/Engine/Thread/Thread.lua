--初始化协程
function InitThread()
	Global["SThread"] = {};
end

--新建一个协程
--callback:协程回调的函数
--key:协程的唯一标识key
--arg:协程需要传入的参数
function StartThread(callback,key,arg)
	local t = coroutine.create(callback);
	local ta = {};
	ta.func = t;
	ta.key = key;
	ta.arg = arg;
	ta.paused = false;
	Global["SThread"][key] = ta;
end

--获取一个协程
function GetThread(key)
	return Global["SThread"][key];
end

--暂停一个协程
function PauseThread(key)
	local ta = GetThread(key);
	if not ta then
		Log("暂停协程没有找到对象："..key);
		return;
	end
	
	ta.paused = true;
end

--将一个已经暂停的协程恢复
function ResumeThread(key)
	local ta = GetThread(key);
	if not ta then
		Log("恢复协程没有找到对象："..key);
		return;
	end
	
	ta.paused = false;
end

--停止并销毁一个协程
function StopThread(key)
	Global["SThread"][key] = nil;	
end

--暂停一帧（等待一帧之后再执行下面的代码,请在对应的协程中调用）
function WaitFrame()
	coroutine.yield();
end

--将一个协程暂停多少帧
function WaitFrameTime(time)
	for i = 1,time do
		WaitFrame();
	end
end


--更新协程
function UpdateThread(dt)
	local function TraceBackThread(key,thread,exception)
		print("=========================================")
		print("LUA ERROR:Thread ERROR->"..key);
		local bc = debug.traceback(thread,exception);
		print(bc);
		print("=========================================");
	end
	
	for key,ta in pairs(Global["SThread"]) do
		if ta then
			local thread = ta.func;
			local arg = ta.arg;
			local paused = ta.paused;
			if not paused then
				local state = coroutine.status(thread);
				if state == "suspended" then
					local succ = true;
					local exception = "";
					succ,exception = coroutine.resume(thread,arg);
					if not succ then
						TraceBackThread(key,thread,exception);
					end
				end
				if state == "dead" then
					StopThread(key);
				end
			end
		end
	end	
end

--停止掉，除了key以外的所有协程
function StopAllThreadExceptKey(key)
	for name,ta in pairs(Global["SThread"]) do
		if name ~= key then
			StopThread(name);
		end
	end
	
end