


--获取当前的导演
function GetDirector()
	return cc.Director:getInstance();
end

--打印输出
function Log(info)
	if DEBUG == 0 then
		return;
	end
	
	local function LogTable(lua_table,indent)
		indent = indent or 0
		for k, v in pairs(lua_table) do
			if type(k) == "string" then
				k = string.format("%q", k)
			end
			local szSuffix = ""
			if type(v) == "table" then
				szSuffix = "{"
			end
			local szPrefix = string.rep("    ", indent)
			formatting = szPrefix.."["..k.."]".." = "..szSuffix
			if type(v) == "table" then
				print(formatting)
				LogTable(v, indent + 1)
				print(szPrefix.."},")
			else
				local szValue = ""
				if type(v) == "string" then
					szValue = string.format("%q", v)
				else
					szValue = tostring(v)
				end
				print(formatting..szValue..",")
			end
		end
	end
	
	local ty = type(info);
	if ty == "table" then
		print("AppDebugTable:");
		LogTable(info);
		return;
	end
	print("["..socket.gettime().."]:"..info);
end

--增加一个计时器，并返回这个计时器的唯一ID，ID为移除定时器的必要参数
function AddTimerTask(callback,delay)
	return GetDirector():getScheduler():scheduleScriptFunc(callback,delay,false);
end

--移除一个定时器
--ID:生成定时器的时候返回的ID
function RemoveTimerTask(ID)
	GetDirector():getScheduler():unscheduleScriptEntry(ID);
end

--获取当前正在运行的场景
function GetRunningScene()
	return GetDirector():getRunningScene();
end

--获取控制权，屏蔽所有点击
function GetControl()
	if Global["controled"] then
		return;
	end
	local scene = GetRunningScene();
	local function layerTouchBegan(touch,event)
		return true;
	end
	Global["controled"] = true;
	local cl = cc.Layer:create();
	cl:setTouchEnabled(true);
	local listener = cc.EventListenerTouchOneByOne:create();
    listener:registerScriptHandler(layerTouchBegan,cc.Handler.EVENT_TOUCH_BEGAN);
    listener:setSwallowTouches(true);
    local dispatcher = cl:getEventDispatcher();
    dispatcher:addEventListenerWithSceneGraphPriority(listener,cl);
    scene:addChild(cl,32,10086);
end

--释放控制权，恢复所有点击
function PutControl()
	if not Global["controled"] then
		return;
	end
	local scene = GetRunningScene();
	scene:removeChildByTag(10086,true);
	Global["controled"] = false;
end

--获取某个资源的完整路径
function GetFullPath(path)
	return cc.FileUtils:getInstance():fullPathForFilename(path);
end

--加载一个UI界面
function LoadUIJson(path)
	local widget = ccs.GUIReader:getInstance():widgetFromJsonFile(path);
	return widget;
end

--加载一个csb的UI界面
function LoadUICSB(path)
	local widget = cc.CSLoader:createNode(path);
	return widget;
end

--通过名字，找到一个控件
function GetWidgetByName(root,name)
	root = StaticCast(root,"ccui.Widget");
	local widget = ccui.Helper:seekWidgetByName(root,name);
	return widget;
end

--将obj强制转换成ty类型
function StaticCast(obj,ty)
	obj = tolua.cast(obj,ty);
	return obj;
end

--加载一个plist的资源纹理文件
function LoadTexture(plist)
	local path = GetFullPath(plist);
	cc.SpriteFrameCache:getInstance():addSpriteFrames(path);
end

--清理一个plist文件
function RemoveTexture(plist)
	cc.SpriteFrameCache:getInstance():removeSpriteFramesFromFile(plist);
end

--通过key，移除一个纹理
function RemoveTextureByKey(key)
	local director = GetDirector();
	director:getTextureCache():removeTextureForKey(key);
end

--移除所有没有使用的纹理
function RemoveUnusedTextures()
	local director = GetDirector();
	director:getTextureCache():removeUnusedTextures();
end

--移除所有没有被使用的精灵帧
function RemoveUnusedSpriteFrame()
	cc.SpriteFrameCache:getInstance():removeUnusedSpriteFrames();
end

--设置播放速度
function SetSchedulerTimeScale(scale)
	local director = GetDirector();
	director:getScheduler():setTimeScale(scale);
end

--增加资源的搜索路径
function AddSearchPathForResource(path)
	local utils = cc.FileUtils:getInstance()
    utils:addSearchPath(path,true)
end

--增加代码的所有路径
function AddSearchPathForCode(path)	
	local engine = cc.ScriptEngineManager:getInstance();
	engine:addSearchPath(path);
end

--复制一张表格
function CopyTable(st)
	local tab = {}
    for k, v in pairs(st or {}) do
        if type(v) ~= "table" then
            tab[k] = v
        else
            tab[k] = CopyTable(v)
        end
    end
    return tab
end

--对一个数求整
function GgetIntPart(x)
    local rt = math.ceil(x)
    if rt>0 and rt~=x then
        rt = rt - 1
    end
    return rt
end

--获取一张csv的表格
function GetCSVTable(path,key)
	local goter = custom.CSVHelper:create();
	local ta = goter:getTable(path,key);
	return ta;
end

--获取xml数据表
function GetXmlTable(path,key)
	if not key then
		key = "";
	end
	local parser = custom.XMLParser:create();
	local ta = parser:parseXML(path,key);
	return ta;
end

--分割字符串函数
function Split(szFullString, szSeparator)
    local nFindStartIndex = 1
    local nSplitIndex = 1
    local nSplitArray = {}
--	Log("Split String:"..szFullString..","..szSeparator);
    while true do
        local nFindLastIndex = string.find(szFullString, szSeparator, nFindStartIndex)
        if not nFindLastIndex then
            nSplitArray[nSplitIndex] = string.sub(szFullString, nFindStartIndex, string.len(szFullString))
            break
        end
        local str = string.sub(szFullString, nFindStartIndex, nFindLastIndex - 1)
        str = string.gsub(str," ","")

        nSplitArray[nSplitIndex] = str
        nFindStartIndex = nFindLastIndex + string.len(szSeparator)
        nSplitIndex = nSplitIndex + 1
    end
    return nSplitArray
end