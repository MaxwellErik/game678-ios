function UIManager()
    local um = {};
    um.threadKey = "umThread";
    function um.clear()
        if um.objects then
            for i = 1,#um.objects do
                local uiObject = um.objects[i];
                uiObject:exit();
                uiObject = nil;
            end
        end
        um.objects = {};
		um.removeList = {};--需要被移除的列表
		StopThread(um.threadKey);
		if um.timerKey then
			RemoveTimerTask(um.timerKey);
		end
		um.timerKey = nil;
    end
    
    function um.init()
        um.clear();
		StartThread(um.update, um.threadKey);
		um.timerKey = AddTimerTask(um.timerTask,0);
    end
	
	--定时器触发。每帧执行一次
	function um.timerTask(dt)
		for key,object in pairs(um.objects) do
			if object then
				object:run(dt);
			end
		end		
	end
    
    --显示这个UI
    function um.AddUIObject(uiObject,node,zorder)
        uiObject:show(node,zorder);
		table.insert(um.objects,uiObject);
		--[[for i = 1,#um.objects do
			Log(um.objects[i].name);
		end--]]
    end
	
	function um.insertRemovedList(uiObject)
		local name = uiObject.name;
		for key,object in pairs(um.removeList) do
			if name == object.name then
				return;
			end
		end
		table.insert(um.removeList,uiObject);
		
	end
    
	--移除一个UI对象
    function um.RemoveUIObject(uiObject)
		if uiObject then
			um.insertRemovedList(uiObject);
		end
    end
	
	--移除所有的UI对象
	function um.RemoveAllUIObject()
		for key,object in pairs(um.objects) do
			if object then
				um.insertRemovedList(object);
			end
		end
		um.objects = {};
	end
	
	function um.RemoveUIObjectUntilName(name)
		for i = #um.objects,1,-1 do
			local ui = um.objects[i];
			if name == ui.name then
				break;
			end
			um.insertRemovedList(ui);
		end
	end
	
	function um.RemoveUIObjectByName(name)
		local uiObject = nil;
		for key,object in pairs(um.objects) do
			if object and object.name == name  then
				uiObject = object;
				break;
			end
		end
		if uiObject then
			um.insertRemovedList(uiObject);
		else
			Log("没有找到名字为："..name.."的UI界面")
		end
	end
	
	--线程
	function um.update(dt)
		while true do
			WaitFrame();
			
			if #um.removeList > 0 then
				local uiObject = um.removeList[1];
				local succ = false;
				local name = uiObject:getName();
				local function clearEndCallBack()
					succ = true;
					uiObject = nil;
				end
				uiObject:exit(clearEndCallBack);
				while not succ do
					WaitFrame();
				end
				
				for i = 1,#um.objects do
					local obj = um.objects[i];
					if obj:getName() == name then
						table.remove(um.objects,i);
						obj = nil;
						break;
					end
				end
				
				local n = #um.objects;
				if n > 0 then--将最后一个UI设置为可见
					local ui = um.objects[n];
					ui:setVisible(true);
				end

				WaitFrame();
				table.remove(um.removeList,1);
				RemoveUnusedTextures();
				WaitFrame();
			end
		end
	end
	
	--根据名字获取一个UIObject
	function um.GetUIObject(name)
		for key,uiObject in pairs(um.objects) do
			if uiObject:getName() == name then
				return uiObject;
			end
		end
		return nil;
	end
    
    return um;
end

--初始化UI管理器
function InitUIManager()
	local um = UIManager();
	um.init();
	Global["um"] = um;
end

--清理UI管理器
function ClearUIManager()
	local um = GetUIManager();
	um.clear();
	um = nil;
	Global["um"] = um;
end

--获取UI管理器
function GetUIManager()
	local um = Global["um"];
	return um;
end

--增加一个UI到界面最上层
function AddUI(object,node,zorder)
	local um = GetUIManager();
	um.AddUIObject(object,node,zorder);
end

--直接移除这个UI对象
function RemoveUI(object)
	local um = GetUIManager();
	um.RemoveUIObject(object);
end

--通过名字移除一个UI对象
function RemoveUIByName(name)
	local um = GetUIManager();
	um.RemoveUIObjectByName(name);
end

--通过名字增加一个UI
function AddUIToManagerByName(name,path,node,zorder)
	local Object = require(path);
	local ui = Object:new();
	ui:init(name);
	AddUI(ui,node,zorder);
end
--移除上面所有的UI，直到遇到name为止
function RemoveUIUntilName(name)
	local um = GetUIManager();
	um.RemoveUIObjectUntilName(name);
end

--当前UI池中是否有这个UI
function IsHasThisNameUI(name)
	local um = GetUIManager();
	for key,obj in pairs(um.objects) do
		if obj and obj.name == name then
			return true;
		end
	end
	return false;
end

--移除所有的UI
function RemoveAllUI()
	local um = GetUIManager();
	um.RemoveAllUIObject();
end

--获取最顶层的UI
function GotTopUI()
	local um = GetUIManager();
	local n = #um.objects;
	if n > 0 then
		return um.objects[n];
	end
	return nil;
end