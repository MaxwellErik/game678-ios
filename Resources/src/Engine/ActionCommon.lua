--将node对象在time时间内移动到x,y坐标
function MoveTo(time,x,y)
	-- body
	local p = cc.p(x,y)
	local moveTo = cc.MoveTo:create(time,p)
	return moveTo
end

function MoveBy( time, x, y )
	local p = cc.p(x,y)
	local moveby = cc.MoveBy:create(time,p)
	return moveby
end
--调用一个方法
function CallFunction(fun)
	-- body
	local call = cc.CallFunc:create(fun)
	return call
end
--讲CCArray中的所有action一个一个的轮流调用
function ActionOneByOne(array)
	-- body
	local sequence = cc.Sequence:create(array)
	return sequence
end

function MakeEaseAction(during,x,y,callBack)
	-- body
	local mt = __G__MoveTo(during,x,y);
	local ease = EaseInOut(mt);
	if callBack ~= nil then
		local call = __G__CallFunction(callBack);
		local arr = {};
		arr[1]=ease;
		arr[2]=call;
		local obo = __G__ActionOneByOne(arr);
		return obo;
	else
		return ease;
	end
end
--从小到放大的动画 scale >=1 放大，scale<1缩小
function EaseZoomAction(during,scale,callBack)
	
	local mt = ScaleTo(during,scale);
	local ease = nil ;--__G__EaseInOut(mt);
	if scale>= 1 then 
		 ease = cc.EaseBackOut:create(mt);
	else
		 ease = cc.EaseBackIn:create(mt);
	end 
	if callBack ~= nil then
		local call = __G__CallFunction(callBack);
		local arr = {};
		arr[1]=ease;
		arr[2]=call;
		local obo = __G__ActionOneByOne(arr);
		return obo;
	else
		return ease;
	end
end

--让一个array的动作同时执行
function ActionTogether(array)
	-- body
	local spaw = cc.Spawn:create(array)
	return spaw
end

--渐入
function FadeIn(time)
	--body
	local fadeIn = cc.FadeIn:create(time)
	return fadeIn
end
--渐出
function FadeOut(time)
	-- body
	local fadeOut = cc.FadeOut:create(time)
	return fadeOut
end
--回震缓冲动作
function EaseInOut(action)
	-- body
	local ac = cc.EaseBackInOut:create(action)
	return ac
end
--延时
function Delay(time)
	-- body
	local ac = cc.DelayTime:create(time)
	return ac
end
--缩放到
function ScaleBy(duration,scale)
	local ac = cc.ScaleBy:create(duration,scale);
	return ac;
end
--缩放到（不可逆）
function ScaleTo(duration,scale)
	local ac = cc.ScaleTo:create(duration,scale);
	return ac;
	
end

function RotateTo(duration,angle)
	
	return cc.RotateTo:create(duration,angle);
end

function RotateBy(duration,angle)
	return cc.RotateBy:create(duration,angle);
end

function RepeatForever(ac)
	return cc.RepeatForever:create(ac);
	
end

function BezierTo(t,pAr)
	local be = cc.BezierTo:create(t,pAr);
	return be;
end
