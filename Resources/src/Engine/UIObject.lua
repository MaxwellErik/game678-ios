--UI对象
UIObject = {
	name = "",
	layer = nil,
	widget = nil,
	visible = true,
};

--初始化一个UI对象
function UIObject:new()	
	local o = {};
    setmetatable(o,self);
    self.__index = self;
	self.layer = nil;
	self.widget = nil;
	self.name = "";
	self.visible = true;
    return o;
end

--请重写此方法，用于将这个UI设置为不可见，当有新的全屏UI加载进入的时候，引擎会主动调用此方法
function UIObject:backGround()
	
end

--请重写此方法，用于，当这个界面重新出现在最顶层的时候需要处理的事情，例如恢复显示，播放音乐，等
function UIObject:frontGround()
	
end

--请重写此方法，在这个方法中，请初始化自己的界面，动画，对象，数据等
function UIObject:initSelf(arg)
	
end

--重写此方法，用于退出该界面的时候清理自己定义的变量，内存等
function UIObject:clearSelf()
	
end

--重写此方法，在这个方法中定义自己需要注册的网络消息协议
function UIObject:initRegisterMessage()
	
end

--重写此方法，用于处理网络数据（引擎主动回调）
function UIObject:netWorkCallBack()
	
	
end

--重写此方法来获取一个每帧遍历的定时器的回调
function UIObject:run(dt)
	
end

--根据json文件来初始化一个UI对象
function UIObject:init(jsonName,arg)
	
	local layer = cc.Layer:create();
	local widget = LoadUIJson(jsonName..".json");
--	local widget = LoadUICSB(jsonName..".csb");
	layer:addChild(widget,2);
	layer:retain();
	widget:retain();
	
	self.layer = layer;
	self.widget = widget;
	
	self.name = jsonName;
	self:initSelf(arg);
	self:initRegisterMessage();
end

--或者这个UI的唯一Key
function UIObject:getName()
	return self.name;
end

--根据一个名字获取一个widget
function UIObject:getWidgetByName(name)
	local root = self.widget;
	return GetWidgetByName(root,name);
end

--设置当前UI的可见度
function UIObject:setVisible(v)
	if not self.layer then
		return;
	end
	if v then
		self:frontGround();
	else
		self:backGround();
	end
	self.visible = v;
end

--当前UI界面是否可见
function UIObject:isVisible()
	return self.visible;
end

--在node上加载这个界面
function UIObject:show(node,zOrder)

	local layer = self.layer;
	if zOrder then
		node:addChild(layer,zOrder);
	else
		node:addChild(layer);
	end
end

--清理自己
function UIObject:clear(callback)
	self:clearSelf();
	if self.widget then
		self.widget:removeFromParent(true);
		self.widget:release();
	end
	self.widget = nil;
	if self.layer then
		self.layer:removeFromParent(true);
		self.layer:release();
	end
	self.layer = nil;
	if callback then
		callback(self);
	end
end

--退出该界面，UIManager主动调用
--callback:退出执行完毕之后会主动
function UIObject:exit(callback,action)
	local layer = self.layer;
	
	local function runActionEnd()
		self:clear(callback);
	end
	
	if not action then
		runActionEnd();
	else
		local callFunc = CallFunction(runActionEnd);
		local at = ActionOneByOne(action,callFunc);
		layer:runAction(at);
	end
end


function UIObject:getLayer()
	return self.layer;
end

--增加一个node到layer上
function UIObject:addNodeToLayer(node,zOrder,tag)
	if zOrder then
		self.layer:addChild(node,zOrder);
		return;
	end
	
	if tag then
		self.layer:addChild(node,zOrder,tag);
		return;
	end
	
	self.layer:addChild(node);
end
