local function getVertDefaultSource()
	local vertDefaultSource = "\n"..
                           "attribute vec4 a_position; \n" ..
                           "attribute vec2 a_texCoord; \n" ..
                           "attribute vec4 a_color; \n"..                                                    
                           "#ifdef GL_ES  \n"..
                           "varying lowp vec4 v_fragmentColor;\n"..
                           "varying mediump vec2 v_texCoord;\n"..
                           "#else                      \n" ..
                           "varying vec4 v_fragmentColor; \n" ..
                           "varying vec2 v_texCoord;  \n"..
                           "#endif    \n"..
                           "void main() \n"..
                           "{\n" ..
                            "gl_Position = CC_PMatrix * a_position; \n"..
                           "v_fragmentColor = a_color;\n"..
                           "v_texCoord = a_texCoord;\n"..
                           "}"
	return vertDefaultSource;
end

--红色大鲶鱼
local function getRedFragSource()
	local pszFragSource = "#ifdef GL_ES \n" ..
                          "precision mediump float; \n" ..
                            "#endif \n" ..
                            "varying vec4 v_fragmentColor; \n" ..
                            "varying vec2 v_texCoord; \n" ..
                            "void main(void) \n" ..
                            "{ \n" ..
                             "vec4 c = v_fragmentColor*texture2D(CC_Texture0, v_texCoord); \n" ..
                            "c *= vec4(1.0,1.0,1.0,1.0); \n"..
                            "c.r = c.a * 1.2; \n"..
							"gl_FragColor = c; \n"..
                            "}"
	return pszFragSource;
end 

local function getColorfulFragSource()
	local pszFragSource = "#ifdef GL_ES \n" ..
        "precision mediump float; \n" ..
        "#endif \n" ..
        "varying vec4 v_fragmentColor; \n" ..
        "varying vec2 v_texCoord; \n" ..
        "void main(void) \n" ..
        "{ \n" ..
        "vec4 c = v_fragmentColor * texture2D(CC_Texture0, v_texCoord); \n" ..
        "c.r *= 1.0; \n"..
        "c.g *= 1.0; \n"..
        "c.b *= 1.0; \n"..
        "gl_FragColor = c; \n"..
        "}"
	return pszFragSource;
end

local function setGLProframToNode(node,vertDefaultSource,pszFragSource)
	if not node then
		return;
	end
	local pProgram = cc.GLProgram:createWithByteArrays(vertDefaultSource,pszFragSource);
    
    pProgram:bindAttribLocation(cc.ATTRIBUTE_NAME_POSITION,cc.VERTEX_ATTRIB_POSITION);
    pProgram:bindAttribLocation(cc.ATTRIBUTE_NAME_COLOR,cc.VERTEX_ATTRIB_COLOR);
    pProgram:bindAttribLocation(cc.ATTRIBUTE_NAME_TEX_COORD,cc.VERTEX_ATTRIB_FLAG_TEX_COORDS);
    pProgram:link();
    pProgram:updateUniforms();
    node:setGLProgram(pProgram);
end

local function setGLProgramToArmature(node,vertDefaultSource,pszFragSource)
	if not node then
		return;
	end
	local pProgram = cc.GLProgram:createWithByteArrays(vertDefaultSource,pszFragSource);
    
    pProgram:bindAttribLocation(cc.ATTRIBUTE_NAME_POSITION,cc.VERTEX_ATTRIB_POSITION);
    pProgram:bindAttribLocation(cc.ATTRIBUTE_NAME_COLOR,cc.VERTEX_ATTRIB_COLOR);
    pProgram:bindAttribLocation(cc.ATTRIBUTE_NAME_TEX_COORD,cc.VERTEX_ATTRIB_FLAG_TEX_COORDS);
    pProgram:link();
    pProgram:updateUniforms();
--    node:setGLProgram(pProgram);
	custom.XMLParser:updateArmatureGLProgram(node,pProgram);
end

--让该节点变灰
function DarkThisNode(node)
	local vertDefaultSource = getVertDefaultSource();
	local pszFragSource = "#ifdef GL_ES \n" ..
                          "precision mediump float; \n" ..
                            "#endif \n" ..
                            "varying vec4 v_fragmentColor; \n" ..
                            "varying vec2 v_texCoord; \n" ..
                            "void main(void) \n" ..
                            "{ \n" ..
                             "vec4 c = texture2D(CC_Texture0, v_texCoord); \n" ..
                            "gl_FragColor.xyz = vec3(0.4*c.r + 0.4*c.g +0.4*c.b); \n"..
                            "gl_FragColor.w = c.w; \n"..
                            "}"
							
	setGLProframToNode(node,vertDefaultSource,pszFragSource);
end

--将设置变暗的node变会原来的颜色
function ColorThisNode(node)
    local vertDefaultSource = getVertDefaultSource();
    local pszFragSource = getColorfulFragSource();

    setGLProframToNode(node,vertDefaultSource,pszFragSource);
end

--将这个ccs动画变成红色
function RedThisArmature(arm)
	local vertDefaultSource = getVertDefaultSource();
	local pszFragSource = getRedFragSource();
	setGLProgramToArmature(arm,vertDefaultSource,pszFragSource);
	
end

--将这个ccs动画变成正常颜色
function ColorThisArmature(arm)
	local vertDefaultSource = getVertDefaultSource();
	local pszFragSource = getColorfulFragSource();
	setGLProgramToArmature(arm,vertDefaultSource,pszFragSource);
end

--红色受击效果
function RedThisNode(node)
	local vertDefaultSource = getVertDefaultSource();
	local pszFragSource = "#ifdef GL_ES \n" ..
                          "precision mediump float; \n" ..
                            "#endif \n" ..
                            "varying vec4 v_fragmentColor; \n" ..
                            "varying vec2 v_texCoord; \n" ..
                            "void main(void) \n" ..
                            "{ \n" ..
                             "vec4 c = v_fragmentColor*texture2D(CC_Texture0, v_texCoord); \n" ..
                            "c *= vec4(1.0,1.0,1.0,1.0); \n"..
                            "c.r = c.a * 1.2; \n"..
							"gl_FragColor = c; \n"..
                            "}"
							
	setGLProframToNode(node,vertDefaultSource,pszFragSource);
	
end

--给这个node展现冰冻效果
function FreezeThisNode(node)
	local vertDefaultSource = getVertDefaultSource();
	local pszFragSource = "#ifdef GL_ES \n" ..
							"precision mediump float; \n" ..
                            "#endif \n" ..
                            "varying vec4 v_fragmentColor; \n" ..
                            "varying vec2 v_texCoord; \n" ..
                            "void main(void) \n" ..
                            "{ \n" ..
                             "vec4 c = v_fragmentColor * texture2D(CC_Texture0, v_texCoord); \n" ..
							"c *= vec4(0.8,0.8,0.8,1); \n"..
							"c.b += c.a * 0.3; \n"..
							"gl_FragColor = c; \n"..
                        "}"
						
	setGLProframToNode(node,vertDefaultSource,pszFragSource);				
end
--给这个节点展现中毒效果
function PoisonThisNode(node)
	local vertDefaultSource = getVertDefaultSource();
	local pszFragSource = "#ifdef GL_ES \n" ..
							"precision mediump float; \n" ..
                            "#endif \n" ..
                            "varying vec4 v_fragmentColor; \n" ..
                            "varying vec2 v_texCoord; \n" ..
                            "void main(void) \n" ..
                            "{ \n" ..
                            "vec4 c = v_fragmentColor * texture2D(CC_Texture0, v_texCoord); \n" ..
							"c.r *= 0.8; \n"..
							"c.r += 0.08 * c.a; \n"..
							"c.g *= 0.8; \n"..
							"c.b *= 0.8; \n"..
							"c.g += 0.3 * c.a; \n"..
							"gl_FragColor = c; \n"..
                        "}"
	setGLProframToNode(node,vertDefaultSource,pszFragSource);
end
--让该节点高亮
function HightLightThisNode(node)
	local vertDefaultSource = getVertDefaultSource();
	local pszFragSource = "#ifdef GL_ES \n" ..
                          "precision mediump float; \n" ..
                            "#endif \n" ..
                            "varying vec4 v_fragmentColor; \n" ..
                            "varying vec2 v_texCoord; \n" ..
                            "void main(void) \n" ..
                            "{ \n" ..
                            "vec4 c = v_fragmentColor * texture2D(CC_Texture0, v_texCoord); \n" ..
							"c.r *= 2.0; \n"..
							"c.g *= 2.0; \n"..
							"c.b *= 2.0; \n"..
							"gl_FragColor = c; \n"..
                            "}"
							
	setGLProframToNode(node,vertDefaultSource,pszFragSource);
	
end
--让该节点按下去
function LowLightThisNode(node)
	local vertDefaultSource = getVertDefaultSource();
	local pszFragSource = "#ifdef GL_ES \n" ..
                          "precision mediump float; \n" ..
                            "#endif \n" ..
                            "varying vec4 v_fragmentColor; \n" ..
                            "varying vec2 v_texCoord; \n" ..
                            "void main(void) \n" ..
                            "{ \n" ..
                            "vec4 c = v_fragmentColor * texture2D(CC_Texture0, v_texCoord); \n" ..
							"c.r *= 0.7; \n"..
							"c.g *= 0.7; \n"..
							"c.b *= 0.7; \n"..
							"gl_FragColor = c; \n"..
                            "}"
							
	setGLProframToNode(node,vertDefaultSource,pszFragSource);
	
end

function OutLineThisNode(node)
	local vertDefaultSource = getVertDefaultSource();
	local pszFragSource = "#ifdef GL_ES \n" ..
                          "precision mediump float; \n" ..
                            "#endif \n" ..
                            "varying vec4 v_fragmentColor; \n" ..
                            "varying vec2 v_texCoord; \n" ..
                            "void main(void) \n" ..
                            "{ \n" ..
                            "float radius = 0.01; \n"..
							"vec4 accum = vec4(0.0); \n"..
							"vec4 normal = vec4(0.0); \n"..
							"normal = texture2D(CC_Texture0,vec2(v_texCoord.x,v_texCoord.y)); \n"..
							"accum += texture2D(CC_Texture0, vec2(v_texCoord.x - radius, v_texCoord.y-radius)); \n"..
							"accum += texture2D(CC_Texture0, vec2(v_texCoord.x + radius, v_texCoord.y-radius)); \n"..
							"accum += texture2D(CC_Texture0, vec2(v_texCoord.x + radius, v_texCoord.y+radius)); \n"..
							"accum += texture2D(CC_Texture0, vec2(v_texCoord.x - radius, v_texCoord.y+radius)); \n"..
							"accum *= 1.75; \n"..
							"accum.r = 1.0; \n"..
							"accum.g = 0.2; \n"..
							"accum.b = 0.5; \n"..
							"normal = (accum *(1.0-normal.a)) + (normal*normal.a); \n"..
							"gl_FragColor = v_fragmentColor * normal; \n"..
                            "}"
							
	setGLProframToNode(node,vertDefaultSource,pszFragSource);
	
end