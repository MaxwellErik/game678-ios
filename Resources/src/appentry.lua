package.path = "src/"
require("framework.init")
require("Engine.init")
InitApp()

VERSION_DEBYG = true
GAME_SUBGAME = false  --是否执行拆包逻辑
GAME_DEBUG = true   --是否是本地调试
GAME_JIUJIU = true  --@true则是安卓版本， @false则是IOS版本

cc.Director:getInstance():getOpenGLView():setDesignResolutionSize(1280, 720, cc.ResolutionPolicy.EXACT_FIT)
-- require("app.GameBase")
-- require("sdk.GameBase")
_G.MyApp = require("app.MyApp").new()
_G.MyApp:run()
require("sdk.GameInit")