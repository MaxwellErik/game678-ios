
--数值定义
--头像大小
FACE_CX                     =   128                                 --头像宽度
FACE_CY                     =   128                                 --头像高度
--长度定义
LEN_LESS_ACCOUNTS           =   5                                   --最短帐号
LEN_LESS_NICKNAME           =   5                                   --最短昵称
LEN_LESS_PASSWORD           =   6                                   --最短密码
LEN_LESS_MAX                =   18                                  --最大账号
--人数定义
MAX_CHAIR                   =   300                                 --最大椅子
MAX_TABLE                   =   512                                 --最大桌子
MAX_COLUMN                  =   32                                  --最大列表
MAX_ANDROID                 =   256                                 --最大机器
MAX_PROPERTY                =   128                                 --最大道具
MAX_WHISPER_USER            =   16                                  --最大私聊
--列表定义
MAX_KIND                    =   128                                 --最大类型
MAX_SERVER                  =   1024                                --最大房间
--参数定义
INVALID_CHAIR               =   0xFFFF                              --无效椅子
INVALID_TABLE               =   0xFFFF                              --无效桌子
--税收定义
REVENUE_BENCHMARK           =   0                                  --税收起点
REVENUE_DENOMINATOR         =   1000                               --税收分母
--系统参数
--游戏状态
GAME_STATUS_FREE            =   0                                   --空闲状态
GAME_STATUS_PLAY            =   100                                 --游戏状态
GAME_STATUS_WAIT            =   200                                 --等待状态
--系统参数
LEN_USER_CHAT               =   128                                 --聊天长度
TIME_USER_CHAT              =   1                                  --聊天间隔
TRUMPET_MAX_CHAR            =   128                                 --喇叭长度
--索引质数
--列表质数
PRIME_TYPE                  =   11                                 --种类数目
PRIME_KIND                  =   53                                 --类型数目
PRIME_NODE                  =   101                                --节点数目
PRIME_PAGE                  =   53                               --自定数目
PRIME_SERVER                =   1009                              --房间数目
--人数质数
PRIME_SERVER_USER           =   503                                --房间人数
PRIME_ANDROID_USER          =   503                                --机器人数
PRIME_PLATFORM_USER         =   100003                            --平台人数
--数据长度
--资料数据
LEN_MD5                     =   33                                  --加密密码
LEN_USERNOTE                =   32                                  --备注长度
LEN_ACCOUNTS                =   32                                  --帐号长度
LEN_NICKNAME                =   32                                  --昵称长度
LEN_PASSWORD                =   33                                  --密码长度
LEN_GROUP_NAME              =   32                                  --社团名字
LEN_UNDER_WRITE             =   32                                  --个性签名
--数据长度
LEN_QQ                      =   16                                  --Q Q 号码
LEN_EMAIL                   =   33                                  --电子邮件
LEN_USER_NOTE               =   256                                 --用户备注
LEN_SEAT_PHONE              =   33                                  --固定电话
LEN_MOBILE_PHONE            =   12                                  --移动电话
LEN_PASS_PORT_ID            =   19                                  --证件号码
LEN_COMPELLATION            =   16                                  --真实名字
LEN_DWELLING_PLACE          =   128                                 --联系地址
--机器标识
LEN_NETWORK_ID              =   13                                  --网卡长度
LEN_MACHINE_ID              =   33                                  --序列长度
LEN_OSVERSION_ID            =   128                                 --系统版本长度
LEN_CPUID_ID                =   64                                  --CPUID长度
LEN_DISKSERIAL_ID           =   64                                  --硬盘序列号长度
LEN_INSTALLER_ID            =   128                                 --安装包名称长度
LEN_SCREEN                  =   12                                  --屏幕分辨率长度
LEN_CHANNEL_ID              =   128                                 --安装包渠道名称长度
--列表数据
LEN_TYPE                    =   32                                  --种类长度
LEN_KIND                    =   32                                  --类型长度
LEN_NODE                    =   32                                  --节点长度
LEN_PAGE                    =   32                                  --定制长度
LEN_SERVER                  =   32                                  --房间长度
LEN_PROCESS                 =   32                                  --进程长度
--用户关系
CP_NORMAL                   =   0                                   --未知关系
CP_FRIEND                   =   1                                   --好友关系
CP_DETEST                   =   2                                   --厌恶关系
CP_SHIELD                   =   3                                   --屏蔽聊天
--性别定义
GENDER_FEMALE               =   0                                   --女性性别
GENDER_MANKIND              =   1                                   --男性性别
--游戏模式
GAME_GENRE_GOLD             =   0x0001                              --金币类型
GAME_GENRE_SCORE            =   0x0002                              --点值类型
GAME_GENRE_MATCH            =   0x0004                              --比赛类型
GAME_GENRE_EDUCATE          =   0x0008                              --训练类型
--分数模式
SCORE_GENRE_NORMAL          =   0x0100                              --普通模式
SCORE_GENRE_POSITIVE        =   0x0200                              --非负模式
--用户状态
US_NULL                     =   0x00                                --没有状态
US_FREE                     =   0x01                                --站立状态
US_SIT                      =   0x02                                --坐下状态
US_READY                    =   0x03                                --同意状态
US_LOOKON                   =   0x04                                --旁观状态
US_PLAYING                  =   0x05                                --游戏状态
US_OFFLINE                  =   0x06                                --断线状态
--比赛状态
MS_NULL                     =   0x00                                --没有状态
MS_SIGNUP                   =   0x01                                --报名状态
MS_MATCHING                 =   0x02                                --比赛状态
MS_OUT                      =   0x03                                --淘汰状态
--房间规则
SRL_LOOKON                  =   0x00000001                          --旁观标志
SRL_OFFLINE                 =   0x00000002                          --断线标志
SRL_SAME_IP                 =   0x00000004                          --同网标志
--房间规则
SRL_ROOM_CHAT               =   0x00000100                          --聊天标志
SRL_GAME_CHAT               =   0x00000200                          --聊天标志
SRL_WISPER_CHAT             =   0x00000400                          --私聊标志
SRL_HIDE_USER_INFO          =   0x00000800                          --隐藏标志
--列表数据
--无效属性
UD_NULL                     =   0                                   --无效子项
UD_IMAGE                    =   100                                 --图形子项
UD_CUSTOM                   =   200                                 --自定子项
--基本属性
UD_GAME_ID                  =   1                                   --游戏标识
UD_USER_ID                  =   2                                   --用户标识
UD_NICKNAME                 =   3                                   --用户昵称
--扩展属性
UD_GENDER                   =   10                                  --用户性别
UD_GROUP_NAME               =   11                                  --社团名字
UD_UNDER_WRITE              =   12                                  --个性签名
--状态信息
UD_TABLE                    =   20                                  --游戏桌号
UD_CHAIR                    =   21                                  --椅子号码
--积分信息
UD_SCORE                    =   30                                  --用户分数
UD_GRADE                    =   31                                  --用户成绩
UD_USER_MEDAL               =   32                                  --用户经验
UD_EXPERIENCE               =   33                                  --用户经验
UD_LOVELINESS               =   34                                  --用户魅力
UD_WIN_COUNT                =   35                                  --胜局盘数
UD_LOST_COUNT               =   36                                  --输局盘数
UD_DRAW_COUNT               =   37                                  --和局盘数
UD_FLEE_COUNT               =   38                                  --逃局盘数
UD_PLAY_COUNT               =   39                                  --总局盘数
--积分比率
UD_WIN_RATE                 =   40                                  --用户胜率
UD_LOST_RATE                =   41                                  --用户输率
UD_DRAW_RATE                =   42                                  --用户和率
UD_FLEE_RATE                =   43                                  --用户逃率
UD_GAME_LEVEL               =   44                                  --游戏等级
--扩展信息
UD_NOTE_INFO                =   50                                  --用户备注
UD_LOOKON_USER              =   51                                  --旁观用户
--图像列表
UD_IMAGE_FLAG               =   (UD_IMAGE+1)                        --用户标志
UD_IMAGE_GENDER             =   (UD_IMAGE+2)                        --用户性别
UD_IMAGE_STATUS             =   (UD_IMAGE+3)                        --用户状态
--数据库定义
DB_ERROR                    =   -1                                  --处理失败
DB_SUCCESS                  =   0                                   --处理成功
DB_NEEDMB                   =   18                                  --处理失败
--道具标示
PT_USE_MARK_DOUBLE_SCORE    =   0x0001                              --双倍积分
PT_USE_MARK_FOURE_SCORE     =   0x0002                              --四倍积分
PT_USE_MARK_GUARDKICK_CARD  =   0x0010                              --防踢道具
PT_USE_MARK_POSSESS         =   0x0020                              --附身道具 
MAX_PT_MARK                 =   4                                   --标识数目
--有效范围
VALID_TIME_DOUBLE_SCORE     =   3600                                --有效时间
VALID_TIME_FOUR_SCORE       =   3600                                --有效时间
VALID_TIME_GUARDKICK_CARD   =   3600                                --防踢时间
VALID_TIME_POSSESS          =   3600                                --附身时间
VALID_TIME_KICK_BY_MANAGER  =   3600                                --游戏时间 
--设备类型
DEVICE_TYPE_PC              =   0x00                                --PC
DEVICE_TYPE_ANDROID         =   0x10                                --Android
DEVICE_TYPE_ITOUCH          =   0x20                                --iTouch
DEVICE_TYPE_IPHONE          =   0x40                                --iPhone
DEVICE_TYPE_IPAD            =   0x80                                --iPad

--手机定义
--视图模式
VIEW_MODE_ALL               =   0x0001                              --全部可视
VIEW_MODE_PART              =   0x0002                              --部分可视
--信息模式
VIEW_INFO_LEVEL_1           =   0x0010                              --部分信息
VIEW_INFO_LEVEL_2           =   0x0020                              --部分信息
VIEW_INFO_LEVEL_3           =   0x0040                              --部分信息
VIEW_INFO_LEVEL_4           =   0x0080                              --部分信息
--其他配置
RECVICE_GAME_CHAT           =   0x0100                              --接收聊天
RECVICE_ROOM_CHAT           =   0x0200                              --接收聊天
RECVICE_ROOM_WHISPER        =   0x0400                              --接收私聊
--行为标识
BEHAVIOR_LOGON_NORMAL       =   0x0000                              --普通登录
BEHAVIOR_LOGON_IMMEDIATELY  =   0x1000                              --立即登录
--处理结果
RESULT_ERROR                    =   -1                              --处理错误
RESULT_SUCCESS                  =   0                               --处理成功
RESULT_FAIL                     =   1                               --处理失败
--变化原因                                                          
SCORE_REASON_WRITE              =   0                               --写分变化
SCORE_REASON_INSURE             =   1                               --银行变化
SCORE_REASON_PROPERTY           =   2                               --道具变化
SCORE_REASON_MATCH_FEE          =   3                               --比赛报名
SCORE_REASON_MATCH_QUIT         =   4                               --比赛退赛
--登录房间失败原因
LOGON_FAIL_SERVER_INVALIDATION = 200                             --房间失效        

--其他信息
DTP_GR_TABLE_PASSWORD       =   1                                   --桌子密码

DTP_NULL                    =   0                                   --无效数据
--用户属性
DTP_GR_NICK_NAME            =   12                                  --用户昵称
-- DTP_GR_GROUP_NAME           =   11                                  --社团名字
-- DTP_GR_UNDER_WRITE          =   12                                  --个性签名

--附加信息
DTP_GR_USER_NOTE            =   20                                  --用户备注
DTP_GR_CUSTOM_FACE          =   21                                  --自定头像

--请求错误
REQUEST_FAILURE_NORMAL      =   0                                   --常规原因
REQUEST_FAILURE_NOGOLD      =   1                                   --金币不足
REQUEST_FAILURE_NOSCORE     =   2                                   --积分不足
REQUEST_FAILURE_PASSWORD    =   3                                   --密码错误             

INVALID_BYTE                =   0xFF                            --//无效数值
INVALID_WORD                =   0xFFFF                          --//无效数值
INVALID_DWORD               =   0xFFFFFFFF                      --//无效数值
INVALID_SCORE               =   0x8000000000000000              --//无效数值
INVALID_INT                 =   0x80000000                      --//无效数值

--系统消息
SMT_CHAT                    = 0x0001                              --//聊天消息
SMT_EJECT                   = 0x0002                              --//弹出消息
SMT_GLOBAL                  = 0x0004                              --//全局消息
SMT_PROMPT                  = 0x0008                              --//提示消息
SMT_TABLE_ROLL              = 0x0010                              --//滚动消息
SMT_GAME                    = 0x0020                              --//游戏消息


--//类型掩码
SMT_CHAT                    = 0x0001                              --//聊天消息
SMT_EJECT                   = 0x0002                              --//弹出消息
SMT_GLOBAL                  = 0x0004                              --//全局消息
SMT_PROMPT                  = 0x0008                              --//提示消息
SMT_TABLE_ROLL              = 0x0010                              --//滚动消息
SMT_GAME                    = 0x0020                              --//游戏消息

--//控制掩码
SMT_CLOSE_ROOM              = 0x0100                              --//关闭房间
SMT_CLOSE_GAME              = 0x0200                              --//关闭游戏
SMT_CLOSE_LINK              = 0x0400                              --//中断连接

--网络状态
SOCKET_CONNECTED            =   1                                 --网络连接好
SOCKET_CONNECTING           =   2                                 --网络连接中
SOCKET_CLOSED               =   3                                 --网络断开