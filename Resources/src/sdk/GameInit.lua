
--定义常量

--服务器ID，大厅or游戏
CLIENT_HALL =   10
CLIENT_GAME =   20

--全局变量，gallLogic, table, eventDispather
app.hallLogic = HallLogic.new()
app.table = Table.new()
app.eventDispather = GameEventDispather.new()
app.musicSound = GMusicSound.new()

app.buttonConfig = nil --按钮的开启配置
app.GETPAYCONFIG_UEL = nil --充值配置的域名
app.GETGAMESHUNXU = nil --游戏显示的顺序

--各种接口注册
local loginServer = LoginServer.new()
local gameServer = GameServer.new()
gameServer:registITable(app.table)
loginServer:registIHall(app.hallLogic)
app.hallLogic:registLoginServer(loginServer)
app.table:registGameServer(gameServer)

require("ServerData")
app.userInfo = getuserInfo()
app.FishRoomID = 0
--获取手机信息的参数
MOBILE_TYPE     =   "MobileType"    --手机型号
MOBILE_OS       =   "MobileOs"      --手机操作系统
MOBILE_NUM      =   "MobileNumber"  --手机号(能不能拿到要看运营商)
MOBILE_NET_STATE    =   "MobileNetState"  --手机网络状态


--程序版本
VERSION_FRAME           =    101122307              --框架版本
VERSION_PLAZA           =    101253890              --大厅版本

TCP_HEART_DELAY_TIME    =   30
TCP_HEART_GAME_SERVER_TIME  =   20