
--loginserver事件
eventHallNetConnected           =   "eventHallNetConnected"     -- true or false
eventHallNetClosed              =   "eventHallNetClosed"        -- null
eventLoginSrvLoginSuccessed     =   "eventLoginSuccessed"       --CMD_GP_LogonSuccess
eventLoginSrvLoginFailed        =   "eventLoginFailed"          --CMD_GP_LogonFailure
eventLoginSrvLoginFinish        =   "eventLoginSrvLoginFinish"  --CMD_GP_LogonFinish
eventLoginSrvListFinish         =   "eventLoginSrvListFinish"   --null
eventLoginSrvQuickRegist        =   "eventLoginSrvQuickRegist"  --CMD_MB_QuickRegisterResult
eventLoginSrvNickName           =   "eventLoginSrvNickName"     --修改nickname返回后的
eventLoginSrvNickName1          =   "eventLoginSrvNickName1"    --修改nickname失败返回后的
eventLoginGameChoice            =   "eventLoginGameChoice"      --修改玩家姓名
eventLoginArenaChoice           =   "eventLoginArenaChoice"     --修改玩家姓名
eventLoginNoLingQu              =   "eventLoginNoLingQu"        --没有签到gamechoice
eventLoginNoLingQu1             =   "eventLoginNoLingQu1"       --没有签到arenachoice
eventLoginRenShu                =   "eventLoginRenShu"          --总人数
eventLoginStoreCoin             =   "eventLoginStoreCoin"       --商城兑换成功后更新金币
eventLoginStoreErro             =   "eventLoginStoreErro"       --商城兑换失败
eventLoginBaoXiangFinish        =   "eventLoginBaoXiangFinish"  --宝箱领取成功
eventLoginBaoXiangError         =   "eventLoginBaoXiangError"   --宝箱领取失败
eventLoginGameGateWay           =   "eventLoginGameGateWay"     --网关信息返回
eventGetServerInfoFailed        =   "eventGetServerInfoFailed"  --获取服务器配置信息失败
eventBaoXiangLingQu             =   "eventBaoXiangLingQu"       --宝箱领取成功事件
eventLoginUserCoinUpdate        =   "eventLoginUserCoinUpdate"  --玩家金币更新
eventLoginSignOk                =   "eventLoginSignOk"          --签到完成
eventLoginGatewayNull           =   "eventLoginGatewayNull"     --没有网关信息
eventRemoveLoading              =   "eventRemoveLoading"        --removeLoading
eventJiuJiJinLingQu             =   "eventJiuJiJinLingQu"       --領取結果
eventFirstRec                   =   "eventFirstRec"				--显示首充回调
eventExchangeData               =   "eventExchangeData"			--兑换记录回调

 
eventLoginCoinRank              =   "eventLoginCoinRank"        --金币排行
eventLoginYueRank               =   "eventLoginYueRank"         --月排行
eventLoginZhouRank              =   "eventLoginZhouRank"        --周排行
eventLoginRiRank                =   "eventLoginRiRank"          --日排行
eventLoginFanKui                =   "eventLoginFanKui"          --反馈
eventBuyTrumpetRet              =   "eventBuyTrumpetRet"        --购买喇叭结果  CMD_GP_S_BuyTrumpetResult
eventTrumpetCount               =   "eventTrumpetCount"         --刷新玩家喇叭个数
eventRedEnvelopeInfo            =   "eventRedEnvelopeInfo"      --红包配置信息
eventUserRedEvevelopeInfo       =   "eventUserRedEvevelopeInfo" --个人红包信息 CMD_GP_S_UserRedEnvelope
eventBuyRedEnvelope             =   "eventBuyRedEnvelope"       --兑换红包  CMD_GP_S_ExchangeRedEnvelope
eventGiveRedEnvelope            =   "eventGiveRedEnvelope"      --赠送红包  CMD_GP_S_GiveRedEnvelope
eventUseRedEnvelope             =   "eventUseRedEnvelope"       --使用红包  CMD_GP_S_OpenRedEnvelope


--gameserver事件，游戏事件
eventGameNetConnected           =   "eventGameNetConnected"     -- true or false
eventGameNetClosed              =   "eventGameNetClosed"        -- null
eventGameNetReconnect           =   "eventGameNetReconnect"     --准备重新连接

eventGameReqFailed              =   "eventGameReqFailed"        --CMD_GR_RequestFailure
eventGameChat                   =   "eventGameChat"             --CMD_GR_S_UserChat
eventGameChatExpression         =   "eventGameChatExpression"   --CMD_GR_S_UserExpression
eventGameLoginSuccess           =   "eventGameLoginSuccess"     --CMD_GR_LogonSuccess
eventGameLoginFailed            =   "eventGameLoginFailed"      --CMD_GR_LogonFailure
eventGameLoginFinish            =   "eventGameLoginFinish"      --null
eventGameUodate                 =   "eventGameUodate"           --CMD_GR_UpdateNotify
eventGameNoPlay                 =   "eventGameNoPlay"           --多少秒没有发送协议后断网

--自定义事件
eventUiShowShop                 =   "eventUiShowShop"           --显示商城页面
eventUpdateVipConf				=	"eventUpdateVipConf"		--更新vip信息
eventBindSucceed				=	"eventBindSucceed"			--绑定账号成功
eventBindFailed					=	"eventBindFailed"			--绑定账号失败
eventOperateSucceed				=	"eventOperateSucceed"		--操作成功
eventOperateFailed				=	"eventOperateFailed"		--操作失败 	


eventGiveRedBoxData             =   "eventGiveRedBoxData"       --直接赠送
eventGiveListData				=   "eventGiveListData"       	--赠送用户列表
eventAdressMessageData			=   "eventAdressMessageData"    --用户地址信息
eventAdressMessageResult		=   "eventAdressMessageResult"  --提交用户信息结果
eventChangeThingResult			=   "eventChangeThingResult"    --兑换实物结果
eventShiWuMessage               =   "eventShiWuMessage"    		--实物信息

eventPaySuccessCallBack         =   "eventPaySuccessCallBack"   --支付成功的回调