
GameEventDispather = class("EventDispather")

function GameEventDispather:ctor()
    self._listeners = {}
    self._delListener = {}
end


--event监听
function GameEventDispather:addListenerEvent(eventId, target, callBack)
    local callBacks = self._listeners[eventId] or {}
    table.insert(callBacks, {["Target"] = target, ["CallBack"] = callBack})
    self._listeners[eventId] = callBacks
end

--删除event监听
function GameEventDispather:delListenerEvent(target)
    local delEvent = {["Target"] = target}
    table.insert(self._delListener, delEvent)
    
end

--事件分发
function GameEventDispather:dispatherEvent(eventId, eventInfo)
    self:doDelListener()

    local callBacks = self._listeners[eventId];
    if callBacks then
        for index, callback in pairs(callBacks) do
            callback.CallBack(eventInfo)
        end
    end    
    
    self:doDelListener()
end

function GameEventDispather:doDelListener()
    for delIndex, delEvent in pairs(self._delListener) do
        for index, calls in pairs( self._listeners ) do
            for callIndex, call in pairs(calls) do
                if delEvent["Target"] == call["Target"] then
                    table.remove(calls, callIndex)
                end
            end  

            if #calls == 0 then
                self._delListener[index] = {}
            end          
        end
    end

    self._delListener = {}
end


return GameEventDispather