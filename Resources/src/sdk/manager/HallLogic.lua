
import(".HallConf")
import("..IHall")
local DataManager = require("app.uiManager.DataManager")
HallLogic = class("HallLogic", IHall)

function HallLogic:ctor()
    self._serverIndex = 1
    self._loginServer = nil
    self._loginSuccessInfo = nil
    self._gameList = {} --游戏列表
    self._gameRoomServerList = {} --游戏房间列表
    self._leJuan = nil   --乐卷
    self._zongRenShu=0  --总人数

    self._shiwuNumTab = nil

    self.isplayGame = false --是否玩过游戏

    self._userInfo = {} --用户的一些信息
    self._currentScene = nil    --当前页面
    self._signinInfo = {}   --签到信息

    self._jiemian=0         --0.是gamechoice 1.是arenachoice

    self.IsLingQu = true    --是否领取过了
    self.SigninDay = 1      --签到的天数
    self.SigninCoin = 0     --奖励金币
    self._bQianDao = false  --是否正在签到
    self._bModifyNickname = false  --是否修改昵称

    self._modifyNicknameMsg = {}  --修改昵称协议
    self._qianDaoMsg = {}  --签到协议

    self._leJuan1DuiHuan=nil  --商店兑换数据
    self._jiFen1DuiHuan=nil
    self._yinHangFen1DuiHuan=nil

    self._baoxiangJuShu=0  --宝箱局数
    self._baoxiangkaiqi=0  --宝箱开启局数
    self._baoxiangGeShu=0  --宝箱个数
    self._baoxiangGe=0     --已开启多少个宝箱
    self._baoXianMsg=nil    --领取的宝箱
    self._backCmdInfos = nil
    self._gameGateWayInfo = nil
    self._gameLevelConf = nil
    self._sendMsgArr = {}
    self._sendHeartScheduler = nil
    self._bUserLogin = false
    self._bLoginIng = false
    self._bSendHeart = false
    self._caifuRankMsg=nil  --财富排行
    self._yueRankMsg=nil    --月排行
    self._zhouRankMsg=nil   --周排行
    self._tianRankMsg=nil   --天排行
    self._isKaiQi= true            --是否能开启宝箱
    self._nHeartTimeCount = 0
    self._bRequestLoginByDns = false
    self._trumpetNeedCoin = 0
    self._trumpetCount = 0
    self._nSocketState = SOCKET_CLOSED
    self._BtnsConfig = nil  --用于显示大厅页面哪些按钮不显示
    self.webGameConf = nil
    
    
    local eventDispatcher = cc.Director:getInstance():getEventDispatcher()
    local customListenerBg = cc.EventListenerCustom:create("APP_ENTER_BACKGROUND_EVENT",
        handler(self, self.onEnterBackground))
    eventDispatcher:addEventListenerWithFixedPriority(customListenerBg, 1)
    local customListenerFg = cc.EventListenerCustom:create("APP_ENTER_FOREGROUND_EVENT",
        handler(self, self.onEnterForeground))
    eventDispatcher:addEventListenerWithFixedPriority(customListenerFg, 1)
    
    custom.PlatUtil:setMessageParsePerFrame(5)
end

function HallLogic:onEnterBackground()
    if VERSION_DEBYG == true then
        -- print("HallLogic:onEnterBackground()........")
    end
end


function HallLogic:onEnterForeground()
    if VERSION_DEBYG == true then
        -- print("HallLogic:onEnterForeground()........")
    end
end

function HallLogic:getGameServer(wKindId, serverId)
    for index, info in ipairs(self._gameRoomServerList) do
        if info.wKindID == wKindId and info.wServerID == serverId then
            return info
        end
    end

    return nil
end

--连接游戏服务器
function HallLogic:connectGameServer(gameServerInfo)
    app.table:setServerInfo(gameServerInfo)
    app.table:reset()
    print(gameServerInfo.szServerAddr)
    print(gameServerInfo.wServerPort)
    custom.PlatUtil:connectServer(CLIENT_GAME,gameServerInfo.szServerAddr, gameServerInfo.wServerPort, TCP_HEART_GAME_SERVER_TIME)
end

function HallLogic:registLoginServer(loginServer)
    self._loginServer = loginServer
end

function HallLogic:registCurrentScene(currentScene)
    self._currentScene = currentScene
end

function HallLogic:getLoginServer()
    return self._loginServer
end

function HallLogic:resetHallServer()
    self._serverIndex = 1
end

function HallLogic:setLastSelGame(gameId)
    self._lastSelGameId = gameId
end

function HallLogic:getLastSelGame()
    return self._lastSelGameId
end

function HallLogic:connectServer()
    if self:isHallServerConnected() == true then
    	return
    end
    
    if hallServerConf then
        self:connectLoginServer()
    else
        self:requestServerInfo()
    end
end

function HallLogic:requestServerInfo()
    -- local requestUrl = SERVER_INFO_CONF_IP
    -- if self._bRequestLoginByDns == true then
    --     requestUrl = SERVERINFO_URL
    -- end

    -- if VERSION_DEBYG == true then
    --     print("获取登录服务器配置:"..requestUrl)
    -- end
    
    -- local request = network.createHTTPRequest(function(event)
    --     self:onLoginServerResponse(event)
    -- end, requestUrl, "GET")
    -- if request then
    --     request:setTimeout(6)
    --     request:start()
    -- end

    local waitTime = 5
    local login_success = false

    local t_channel = getChannelName()
    local t_version = getVersionName()
    local xhr = cc.XMLHttpRequest:new() -- 新建一个XMLHttpRequest对象  
    xhr.responseType = cc.XMLHTTPREQUEST_RESPONSE_JSON -- json数据类型
    xhr:open("POST", GETCONFIG_UEL)-- POST方式  

    local t_sign = crypto.md5("LoginConfig"..t_channel..t_version.."tjyjerge!@#sddf$^$h234^^DsSEG")
    local param = "action=LoginConfig&channel="..t_channel.."&version="..t_version..
                "&sign="..t_sign
    xhr:send(param)

    local function LoginCallBack()  
        local response = xhr.response -- 获得响应数据  
        local output = json.decode(response) -- 解析json数据
        if output then
            login_success = true
            hallServerConf = output
        end
        xhr = nil
    end
    -- 注册脚本方法回调 
    xhr:registerScriptHandler(LoginCallBack)

    local scheduler = cc.Director:getInstance():getScheduler()
    local schedulerEntry2 = nil
    local function Countdown(dt)
        waitTime = waitTime - 1
        print("requestServerInfo ", waitTime)
        if waitTime <= 0 then
            -- self._bRequestLoginByDns = false
            -- self:getLoginServerConfFailed()
            scheduler:unscheduleScriptEntry(schedulerEntry2)

            --从Web得不到配置
            local request = network.createHTTPRequest(
                function(event)
                    self:onLoginServerResponse(event)
                end, 
                requestUrl, "GET")
            if request then
                request:setTimeout(6)
                request:start()
            end
        else
            if login_success then
                scheduler:unscheduleScriptEntry(schedulerEntry2)
                self._bRequestLoginByDns = false
                self:connectLoginServer()
            end
        end
    end
    schedulerEntry2 = scheduler:scheduleScriptFunc(Countdown, 1, false)  --时间记录
end

function HallLogic:onLoginServerResponse(event, dumpResponse)
    local request = event.request
    if event.name == "completed" then
        if request:getResponseStatusCode() ~= 200 then
            if self._bRequestLoginByDns == false then
                self._bRequestLoginByDns = true
                self:requestServerInfo()
            else
                self._bRequestLoginByDns = false
                self:getLoginServerConfFailed()
            end
        else
            self._bRequestLoginByDns = false
            self.dataRecv = request:getResponseData()
            local  script=self.dataRecv
            local tb=assert(loadstring(script))()
            hallServerConf = tb.loginServerConfs
            self:connectLoginServer()
        end
    elseif event.name == "failed" then
        -- if self._bRequestLoginByDns == false then
        --     self._bRequestLoginByDns = true
        --     self:requestServerInfo()
        -- else
            self._bRequestLoginByDns = false
            self:getLoginServerConfFailed()
        -- end
    end
end

function HallLogic:getLoginServerConfFailed()
    app.eventDispather:dispatherEvent(eventGetServerInfoFailed)
end

function HallLogic:connectLoginServer()
    if self._nSocketState ~= SOCKET_CLOSED then
    	return
    end
    
    self._nSocketState = SOCKET_CONNECTING
    if self._serverIndex <= #hallServerConf then
        print( hallServerConf[self._serverIndex]["ip"] )
        self._loginServer:connect(CLIENT_HALL, hallServerConf[self._serverIndex]["ip"], hallServerConf[self._serverIndex]["port"], TCP_HEART_DELAY_TIME)
    end
end

--具体协议处理，这里只处理大厅的协议
function HallLogic:onGameMessage(mainId, subId, msg)
    if VERSION_DEBYG == true then
        print(">>>>>>>>>>>halllogic",mainId,subId,msg)
    end
    
    if mainId == MDM_MB_LOGON then    --登录信息
        self:onLoginMessage(subId, msg)
    elseif mainId == MDM_MB_SERVER_LIST then  --列表信息
        self:onListMessage(subId, msg)
    elseif mainId == MDM_GP_USER_SERVICE then  --用户服务
        self:onServiceMessage(subId, msg)
    elseif mainId == MDM_GP_LEQUAN_SERVICE then  --乐卷宝箱业务
        self:onLeJuanMessage(subId, msg)
    elseif mainId == MDM_GP_GAME_GATEWAY then  --网关
        self:onGameGateWay(subId, msg)
    elseif mainId == MDM_GP_LOGON then
        self:onGPLogin(subId, msg)
    elseif mainId == MDM_GP_RANKING then  --排行榜
        self:onRanking(subId, msg)
    elseif mainId == MDM_GP_FEEDBACK then  --反馈
        self:onFankui(subId, msg)
    elseif mainId == MDM_GP_TRUMPET then --小喇叭
        self:onTrumpet(subId, msg)
    elseif mainId == MDM_GP_REDENVELOPE then --宝箱，红包
        self:onRedEnvelope(subId, msg)
    elseif mainId == MDM_GP_EXCHANGE_SERVICE then--购买记录
        self:onBuyRecord(subId, msg)
    elseif mainId == MDM_GP_REALPROPS_SERVICE then --实物道具服务
        self:onshiwuMessage(subId, msg)
    end
end

--购买记录
function HallLogic:onBuyRecord(subId, msg)
    if subId == SUB_GP_S_USER_EXCHANGE_RECORDS then 
        DataManager:getInstance():getNewStoreLayer():setPayRecordData(msg)
    elseif subId == SUB_GP_S_HISTORY_TRANSACTION_USER then --历史交易用户
        app.eventDispather:dispatherEvent(eventGiveListData, msg)
    end
end

--实物道具服务
function HallLogic:onshiwuMessage(subId, msg)
    if subId == SUB_GP_S_USER_REALPROPS_INFO then  --实物道具信息
        --DataManager:getInstance():getNewStoreLayer():shiwuCountMessage(msg)
        self._shiwuNumTab = msg
        app.eventDispather:dispatherEvent(eventShiWuMessage, msg)
    elseif subId == SUB_GP_S_SUBMIT_DELIVERY_ADDRESS_RESULT then --提交收货地址结果  
        app.eventDispather:dispatherEvent(eventAdressMessageResult, msg)
    elseif subId == SUB_GP_S_DELIVERY_ADDRESS then --收货地址
        app.eventDispather:dispatherEvent(eventAdressMessageData, msg)
    elseif subId == SUB_GP_S_EXCHANGE_REALPROPS_RESULT then --兑换实物道具结果
        app.eventDispather:dispatherEvent(eventChangeThingResult, msg)
    end
end

--反馈
function HallLogic:onFankui(subId, msg)
    if subId == SUB_GP_S_FEEDBACK_RESULT then          --财富排行
        if VERSION_DEBYG == true then
            print("反馈回来了")
        end
        self.fankuiMsg=msg["szMessage"]
        app.eventDispather:dispatherEvent(eventLoginFanKui)
    end
end

function HallLogic:onTrumpet(subId, msg)
    if sublId == SUB_GP_S_TRUMPET_INFO then  --喇叭配置信息
        self._trumpetNeedCoin = msg.lPrice
    elseif subId == SUB_GP_S_USER_TRUMPET then  --用户喇叭信息
        self._trumpetCount = msg.dwTrumpetCount
        app.eventDispather:dispatherEvent(eventTrumpetCount, msg.dwTrumpetCount)
    elseif subId == SUB_GP_S_BUY_TRUMPET_RESULT then --用户喇叭购买结果
        if msg.cbResult == 0 then
            self._trumpetCount = msg.dwTrumpetCount
            app.eventDispather:dispatherEvent(eventTrumpetCount, msg.dwTrumpetCount)
            self:sendUpdateUserCoin()
        end

        app.eventDispather:dispatherEvent(eventBuyTrumpetRet, msg)
    end
end

function HallLogic:onRedEnvelope(subId, msg)
    if VERSION_DEBYG == true then
        print("红包信息")
    end
    --dump(msg)
    if subId == SUB_GP_S_REDENVELOPE_INFO then --红包配置信息
        self._redEnvelopeInfo = msg
        app.eventDispather:dispatherEvent(eventRedEnvelopeInfo, msg)
    elseif subId == SUB_GP_S_USER_REDENVELOPE then  --用户红包信息
        self._userRedEnvelopeInfo = msg
        app.eventDispather:dispatherEvent(eventUserRedEvevelopeInfo, msg)
    elseif subId == SUB_GP_S_EXCHANGE_REDENVELOPE then  --兑换红包
        app.eventDispather:dispatherEvent(eventBuyRedEnvelope, msg)
        if msg.cbResult == 0 then --兑换成功
            self:queryUserRedEnvelope()
            self:sendUpdateUserCoin()
        end
    elseif subId == SUB_GP_S_GIVE_REDENVELOPE then  --赠送红包
        app.eventDispather:dispatherEvent(eventGiveRedEnvelope, msg)
        if msg.cbResult == 0 then  --赠送成功
            self:queryUserRedEnvelope()
        end
    elseif subId == SUB_GP_S_OPEN_REDENVELOPE then  --打开红包
        app.eventDispather:dispatherEvent(eventUseRedEnvelope, msg)
        if msg.cbResult == 0 then   --使用红包
            self:queryUserRedEnvelope()
            self._loginSuccessInfo.lUserScore = msg.lUserScore
            self:sendUpdateUserCoin()
        end
    elseif subId == SUB_GP_S_GIVE_REDENVELOPE_RECORD then  --送出红包记录
        print("SUB_GP_S_GIVE_REDENVELOPE_RECORD")
        -- dump(msg)
        DataManager:getInstance():getBoxLayer():recordCallBack(msg)
    elseif subId == SUB_GP_S_RECV_REDENVELOPE_RECORD then  --得到红包记录
        print("SUB_GP_S_RECV_REDENVELOPE_RECORD")
        -- dump(msg)
        DataManager:getInstance():getBoxLayer():recordCallBack(msg)
    elseif subId == SUB_GP_S_GET_REDENVELOPE then  --别人赠送红包
        print("SUB_GP_S_GET_REDENVELOPE")
        self:queryUserRedEnvelope()
        local gamechoiceLayer = DataManager:getInstance():getChoiceLayer()
        if gamechoiceLayer then
           gamechoiceLayer:getRedPackageTip(msg)
        else
            local PublicUI = require("app.object.PublicUI")
            PublicUI:getRedPackageTip(msg)
        end
    elseif subId == SUB_GP_S_EXCHANGE_AND_GIVE_REDENVELOPE then  --赠送别人红包
        app.eventDispather:dispatherEvent(eventGiveRedBoxData, msg)
        if msg.cbResult == 0 then   --赠送别人红包成功
            self:queryUserRedEnvelope()
            self._loginSuccessInfo.lUserScore = msg.lUserScore
            self:sendUpdateUserCoin()
        end
    end
end

--反馈请求
function HallLogic:goFankui()
    local info = {}
    info.dwUserID = self._loginSuccessInfo["dwUserID"]
    info.szTitle = self.szTitle
    info.szContent = self.szContent
    info.szQQ = self.szQQ
    info.szEMail = self.szEMail
    info.szMobilePhone = self.szMobilePhone
    if VERSION_DEBYG == true then
        print("反馈发送了")
    end
    local backCmdIds = {{['mainId']=MDM_GP_FEEDBACK, ['subId']=SUB_GP_S_FEEDBACK_RESULT}}
    self:sendMessage( MDM_GP_FEEDBACK, SUB_GP_C_USER_FEEDBACK, info, false, backCmdIds)
end

--排行榜
function HallLogic:onRanking(subId, msg)
    --    dump(msg.UserInfo[16])
    if msg ~= nil then
        if subId == SUB_GP_S_TREASURE_RANKING then          --财富排行
            if VERSION_DEBYG == true then
                print('财富排行')
            end
            self._caifuRankMsg=msg
            app.eventDispather:dispatherEvent(eventLoginCoinRank)
        elseif subId == SUB_GP_S_MONTH_GAIN_RANKING then    --月盈利排行
            if VERSION_DEBYG == true then
                print('月盈利排行')
            end
            self._yueRankMsg=msg
            app.eventDispather:dispatherEvent(eventLoginYueRank)
        elseif subId == SUB_GP_S_WEEK_GAIN_RANKING then     --周盈利排行
            if VERSION_DEBYG == true then
                print('周盈利排行')
            end
            self._zhouRankMsg=msg
            app.eventDispather:dispatherEvent(eventLoginZhouRank)
        elseif subId == SUB_GP_S_DAY_GAIN_RANKING then      --天盈利排行
            if VERSION_DEBYG == true then
                print('天盈利排行')
            end
            self._tianRankMsg=msg
            app.eventDispather:dispatherEvent(eventLoginRiRank)
        end
    end
end

--财富排行榜
function HallLogic:sendCoinRank()
    if VERSION_DEBYG == true then
        print("查询财富排行榜")
    end
    self:sendMessage( MDM_GP_RANKING, SUB_GP_C_QUERY_TREASURE_RANKING, nil, false)
end

--天排行
function HallLogic:sendDayRank()
    if VERSION_DEBYG == true then
        print("查询天排行榜")
    end
    self:sendMessage( MDM_GP_RANKING, SUB_GP_C_QUERY_DAY_GAIN_RANKING, nil, false)
end

--周排行
function HallLogic:sendWeekRank()
    if VERSION_DEBYG == true then
        print("查询周排行榜")
    end
    self:sendMessage( MDM_GP_RANKING, SUB_GP_C_QUERY_WEEK_GAIN_RANKING, nil, false)
end

function HallLogic:sendMonthRank()
    if VERSION_DEBYG == true then
        print("查询月排行榜")
    end
    self:sendMessage( MDM_GP_RANKING, SUB_GP_C_QUERY_MONTH_GAIN_RANKING, nil, false)
end

--乐卷宝箱
function HallLogic:onLeJuanMessage(subId, msg)
    --dump(msg)
    if subId == SUB_GP_S_USER_LEQUAN_INFO then  --查询用户乐券信息
        if VERSION_DEBYG == true then
            print("用户乐卷"..msg["lLeQuan"])
        end
        self._leJuan=msg["lLeQuan"]
    elseif subId == SUB_GP_S_LEQUAN_EXCHANGE_INFO then  --乐券兑换信息
        if VERSION_DEBYG == true then
            print("乐券兑换信息")
        end
        self._leJuan1DuiHuan=msg
    elseif subId == SUB_GP_S_SCORE_EXCHANGE_INFO then  --积分兑换信息
        if VERSION_DEBYG == true then
            print("积分兑换信息")
        end
        self._jiFen1DuiHuan=msg
    elseif subId == SUB_GP_S_REFILLCARD_EXCHANGE_INFO then  --充值卡兑换信息
        if VERSION_DEBYG == true then
            print("充值卡兑换信息")
        end
        self._yinHangFen1DuiHuan=msg
    elseif subId == SUB_GP_S_EXCHANGE_LEQUAN_RESULT then  --兑换乐券结果
        if VERSION_DEBYG == true then
            print("乐卷返回结果")
        end
        if msg["cbError"]==0 then
            self._leJuan=msg["lLeQuan"]
            self._loginSuccessInfo["lUserScore"] = msg["lScore"]
            app.eventDispather:dispatherEvent(eventLoginGameChoice)
            --app.eventDispather:dispatherEvent(eventLoginArenaChoice)
            app.eventDispather:dispatherEvent(eventLoginStoreCoin)
    else
        app.eventDispather:dispatherEvent(eventLoginStoreErro)
    end
    elseif subId == SUB_GP_S_EXCHANGE_REFILLCARD_RESULT then  --兑换充值卡结果
        if VERSION_DEBYG == true then
            print("兑换充值卡结果")
        end
        if msg["cbError"]==0  then
            self._leJuan=msg["lLeQuan"]
            app.eventDispather:dispatherEvent(eventLoginGameChoice)
            --app.eventDispather:dispatherEvent(eventLoginArenaChoice)
            app.eventDispather:dispatherEvent(eventLoginStoreCoin)
    else
        app.eventDispather:dispatherEvent(eventLoginStoreErro)
    end

    elseif subId == SUB_GP_S_EXCHANGE_SCORE_RESULT then  --兑换积分结果
        if VERSION_DEBYG == true then
            print("兑换积分结果")
        end
        if msg["cbError"]==0  then
            self._leJuan=msg["lLeQuan"]
            self._loginSuccessInfo["lUserScore"] = msg["lScore"]
            app.eventDispather:dispatherEvent(eventLoginGameChoice)
            --app.eventDispather:dispatherEvent(eventLoginArenaChoice)
            app.eventDispather:dispatherEvent(eventLoginStoreCoin)
        else
            app.eventDispather:dispatherEvent(eventLoginStoreErro)
        end

    elseif subId == SUB_GP_S_TREASUREBOX_CONDITION then  --查询打开宝箱条件
        if VERSION_DEBYG == true then
            print("查询打开宝箱条件")
        end
        --dump(msg)
        self._baoxiangkaiqi=msg["dwLeastPlay"]  --宝箱开启局数
        app.eventDispather:dispatherEvent(eventLoginGameChoice)
        --app.eventDispather:dispatherEvent(eventLoginArenaChoice)
    elseif subId == SUB_GP_S_USER_GAME_INNINGS then  --查询玩家当天玩的游戏局数
        if VERSION_DEBYG == true then
            print("查询玩家当天玩的游戏局数"..self._baoxiangkaiqi.."+++"..msg["dwAllInnings"])
        end
        --dump(msg)
        self._baoxiangJuShu=msg["dwAllInnings"]  --宝箱局数
        app.eventDispather:dispatherEvent(eventLoginGameChoice)
        --app.eventDispather:dispatherEvent(eventLoginArenaChoice)
    elseif subId == SUB_GP_S_USER_OPEN_TREASUREBOX_INFO then  --查询用户当天开宝箱信息
        if VERSION_DEBYG == true then
            print("查询用户当天开宝箱信息")
        end
        self._baoxiangGe=msg["dwUserdOpenCount"]--已开启多少个宝箱
        self._baoxiangGeShu=msg["dwMaxOpenCount"]-msg["dwUserdOpenCount"]  --宝箱个数
        app.eventDispather:dispatherEvent(eventLoginGameChoice)
        --app.eventDispather:dispatherEvent(eventLoginArenaChoice)
    elseif subId == SUB_GP_S_OPEN_TREASUREBOX_RESULT then  --打开宝箱结果
        if VERSION_DEBYG == true then
            print("打开宝箱结果")
        end
        if msg["cbError"]==0 then
            self._isKaiQi=true
            self._leJuan=msg["lLeQuan"]
            self._loginSuccessInfo["lUserScore"] = msg["lScore"]
            self._baoXianMsg=msg
            app.eventDispather:dispatherEvent(eventLoginBaoXiangFinish)
            self:leJuanInquire()--跟新信息
        else
            self._isKaiQi=true
            self._baoXianMsg=msg
            app.eventDispather:dispatherEvent(eventLoginBaoXiangError)
        end
    elseif subId == SUB_GP_S_EXCHANGE_SCORE_RECORD then --乐卷换分
        print("--------乐卷换分信息--------")
        app.eventDispather:dispatherEvent(eventExchangeData, msg)
        -- DataManager:getInstance():getNewStoreLayer():setPayRecordData(msg)
    elseif subId == SUB_GP_S_EXCHANGE_REFILLCARD_RECORD then--充值卡
        print("--------充值卡兑换信息--------")
        app.eventDispather:dispatherEvent(eventExchangeData, msg)
        -- DataManager:getInstance():getNewStoreLayer():setPayRecordData(msg)
    end
end

function HallLogic:onGameGateWay(subId, msg)
    if VERSION_DEBYG == true then
        print('网关信息回来了')
    end
    if subId == SUB_GP_S_GAME_GATEWAY_ADDRESS then
        print( "onGameGateWay" )
        self._gameGateWayInfo = msg
        app.eventDispather:dispatherEvent(eventLoginGameGateWay, msg)
    end
end

--查询乐卷信息,查询乐券兑换信息,查询积分兑换信息,查询充值卡兑换信息,查询打开宝箱条件,查询玩家当天玩的游戏局数,查询用户当天开宝箱信息
function HallLogic:leJuanInquire()
    local info = {}
    info.dwUserID = self._loginSuccessInfo["dwUserID"]
    info.szPassword = custom.PlatUtil:getMD5String(self._userInfo["password"])

    local backCmdIds = {{['mainId']=MDM_GP_LEQUAN_SERVICE, ['subId']=SUB_GP_S_USER_OPEN_TREASUREBOX_INFO}}

    self:sendMessage( MDM_GP_LEQUAN_SERVICE, SUB_GP_C_QUERY_USER_LEQUAN_INFO, info, false)
    self:sendMessage( MDM_GP_LEQUAN_SERVICE, SUB_GP_C_QUERY_LEQUAN_EXCHANGE_INFO, info, false)
    self:sendMessage( MDM_GP_LEQUAN_SERVICE, SUB_GP_C_QUERY_SCORE_EXCHANGE_INFO, info, false)
    self:sendMessage( MDM_GP_LEQUAN_SERVICE, SUB_GP_C_QUERY_REFILLCARD_EXCHANGE_INFO, info, false)
    self:sendMessage( MDM_GP_LEQUAN_SERVICE, SUB_GP_C_QUERY_TREASUREBOX_CONDITION, info, false)
    self:sendMessage( MDM_GP_LEQUAN_SERVICE, SUB_GP_C_QUERY_USER_GAME_INNINGS, info, false)
    self:sendMessage( MDM_GP_LEQUAN_SERVICE, SUB_GP_C_QUERY_USER_OPEN_TREASUREBOX_INFO, info, false, backCmdIds)
end

--开宝箱
function HallLogic:OpenBaoxiangBox()
    local info = {}
    info.dwUserID = self._loginSuccessInfo["dwUserID"]
    info.szPassword = custom.PlatUtil:getMD5String(self._userInfo["password"])
    local backCmdIds = {{['mainId']=MDM_GP_LEQUAN_SERVICE, ['subId']=SUB_GP_S_OPEN_TREASUREBOX_RESULT}}

    self:sendMessage( MDM_GP_LEQUAN_SERVICE, SUB_GP_C_OPEN_TREASUREBOX, info, false, backCmdIds)--开宝箱
end

--兑换充值卡
function HallLogic:exchangeRefillCard(type, mobileNum)
    local info = {}
    info.dwUserID = self._loginSuccessInfo["dwUserID"]
    info.szPassword = custom.PlatUtil:getMD5String(self._userInfo["password"])
    info.dwAmount = 1
    info.dwType = type
    info.szMobilePhone=mobileNum

    local backCmdIds = {{['mainId']=MDM_GP_LEQUAN_SERVICE, ['subId']=SUB_GP_S_EXCHANGE_REFILLCARD_RESULT}}
    self:sendMessage(MDM_GP_LEQUAN_SERVICE, SUB_GP_C_EXCHANGE_REFILLCARD, info, false, backCmdIds)
end

--兑换乐券
function HallLogic:exchangeLeQuan(amount)
    local info = {}
    info.dwUserID = self._loginSuccessInfo["dwUserID"]
    info.szPassword = custom.PlatUtil:getMD5String(self._userInfo["password"])
    info.dwAmount = amount

    local backCmdIds = {{['mainId']=MDM_GP_LEQUAN_SERVICE, ['subId']=SUB_GP_S_EXCHANGE_LEQUAN_RESULT}}
    self:sendMessage( MDM_GP_LEQUAN_SERVICE, SUB_GP_C_EXCHANGE_LEQUAN, info, false, backCmdIds)
end

--兑换金币
function HallLogic:exchangeScore(score)
    local info = {}
    info.dwUserID = self._loginSuccessInfo["dwUserID"]
    info.szPassword = custom.PlatUtil:getMD5String(self._userInfo["password"])
    info.lScore = score
    local backCmdIds = {{['mainId']=MDM_GP_LEQUAN_SERVICE, ['subId']=SUB_GP_S_EXCHANGE_SCORE_RESULT}}
    self:sendMessage( MDM_GP_LEQUAN_SERVICE, SUB_GP_C_EXCHANGE_SCORE, info, false, backCmdIds)
end

--请求赠送红包记录
function HallLogic:sendGivingRecord(pagenum)
    local info = {}
    info.dwUserID = self._loginSuccessInfo["dwUserID"]
    info.dwPage = pagenum
    local backCmdIds = {{['mainId']=MDM_GP_REDENVELOPE, ['subId']=SUB_GP_S_GIVE_REDENVELOPE_RECORD}}
    self:sendMessage( MDM_GP_REDENVELOPE, SUB_GP_C_GIVE_REDENVELOPE_RECORD, info, false, backCmdIds)
end

--请求收到红包记录
function HallLogic:sendGetRecord(pagenum)
    local info = {}
    info.dwUserID = self._loginSuccessInfo["dwUserID"]
    info.dwPage = pagenum
    local backCmdIds = {{['mainId']=MDM_GP_REDENVELOPE, ['subId']=SUB_GP_S_RECV_REDENVELOPE_RECORD}}
    self:sendMessage( MDM_GP_REDENVELOPE, SUB_GP_C_RECV_REDENVELOPE_RECORD, info, false, backCmdIds)
end

function HallLogic:setSendHeart(bSend)
    self._bSendHeart = bSend
    if bSend == false then
        local scheduler = cc.Director:getInstance():getScheduler()
        if self._sendHeartScheduler then
            scheduler:unscheduleScriptEntry(self._sendHeartScheduler)
        end
        self._sendHeartScheduler = nil
    end
end

--登录信息
function HallLogic:onLoginMessage(subId, msg)
    print("onLoginMessage enter")
    if subId == SUB_MB_LOGON_SUCCESS then  --登录成功
        print("onLoginSuccess")
        if self._account and self._password and #self._account > 0 and #self._password>0 then
            cc.UserDefault:getInstance():setStringForKey("account", self._account)
            cc.UserDefault:getInstance():setStringForKey("password",self._password)
        end

        self._bSendHeart = true
        self._bLoginIng = false
        self._bUserLogin = true
        self._loginSuccessInfo = msg

        self._gameRoomServerList = {}
        self._gameList = {}

        self:querySigninReward()--签到
        self:queryUserSignin()
        
        self:queryTrumpet()--查询系统喇叭配置
        self:queryUserTrumpet()  --查询玩家喇叭信息
        self:leJuanInquire()--查询乐卷
        -- self:shiwuMessage()--查询实物
        self:queryRedEnvelope()
        self:queryUserRedEnvelope()
        
        self:sendQueryNickname()--查询玩家是否修改过昵称
        self:getBeneCount() --还剩的救济金次数
        self._vipConf = nil  --vip配置信息

        for index, item in ipairs(self._sendMsgArr) do
            if item.bNet == false then
                -- if item.mainId == MDM_GP_GAME_GATEWAY and item.subId == SUB_GP_C_QUERY_GAME_GATEWAY then
                --     item.msg.szDynamicPassword = self._loginSuccessInfo.szDynamicPassword
                -- elseif item.mainId == MDM_GP_USER_SERVICE and item.subId == SUB_GP_USER_HEARTBEAT then
                --     item.msg.szDynamicPassword = self._loginSuccessInfo.szDynamicPassword
                -- end
                self:sendMessage(item.mainId,item.subId,item.msg, false, item.backCmds)
            end
        end

        app.eventDispather:dispatherEvent(eventLoginSrvLoginSuccessed, msg)

        --定时器，20s心跳一次
        self._nHeartTimeCount = 0
        self:closeHeartScheduler()

        local onHeartSchedule = function()
            --发送心跳
            local info = {}
            info.dwUserID = self._loginSuccessInfo.dwUserID
            -- info.szDynamicPassword = self._loginSuccessInfo.szDynamicPassword
            local backCmdIds = {{['mainId']=MDM_GP_USER_SERVICE, ['subId']=SUB_GP_S_USER_HEARTBEAT}}

            self:sendMessage(MDM_GP_USER_SERVICE,SUB_GP_USER_HEARTBEAT,info,false,backCmdIds)
            
            self._nHeartTimeCount = self._nHeartTimeCount + 1
            if self._nHeartTimeCount >= 3 then   --心跳超时
                self:closeNet()
                self:onLoginNetClosed()        	
            end
        end

        local scheduler = cc.Director:getInstance():getScheduler()
        self._sendHeartScheduler = scheduler:scheduleScriptFunc(onHeartSchedule, 10.0, false)  --时间记录
    elseif subId == SUB_MB_LOGON_FAILURE then  --登录失败
        app.eventDispather:dispatherEvent(eventLoginSrvLoginFailed, msg)
        self:closeNet()
    elseif subId == SUB_MB_UPDATE_NOTIFY then
        if VERSION_DEBYG == true then
            print("SUB_MB_UPDATE_NOTIFY")
        end
    elseif subId == SUB_MB_QUICK_REGISTER_RESULT then
        self._username = msg.szAccount
        self._password = msg.szPassword
        cc.UserDefault:getInstance():setStringForKey("account", msg.szAccount)
        cc.UserDefault:getInstance():setStringForKey("password",msg.szPassword)

        app.eventDispather:dispatherEvent(eventLoginSrvQuickRegist, msg)
    elseif subId == SUB_GP_S_BIND_ACCOUNT_RESULT then
        self._bLoginIng = false
        self._bUserLogin = true
        if msg.cbResult == 0 then
            cc.UserDefault:getInstance():setStringForKey("account", self._account)
            cc.UserDefault:getInstance():setStringForKey("password",self._password)

            app.eventDispather:dispatherEvent(eventBindSucceed)
        else
            app.eventDispather:dispatherEvent(eventBindFailed)
        end
    end
    self._sendMsgArr = {}
end

function HallLogic:onGPLogin(subId, msg)
    if subId == SUB_GP_GROWLEVEL_CONFIG then
        self._gameLevelConf = msg
    end
end

function HallLogic:quickRegist()
    if self._bLoginIng == true then
        return
    end

    self._bLoginIng = true
    self._bUserLogin = false

    local info = {}
    info.wModuleID = INVALID_WORD
    info.dwPlazaVersion = VERSION_PLAZA
    info.cbDeviceType = 1
    info.szMachineID = getMobileImei()
    info.szOSVersion = getOsVersion()
    info.szMobileType = getMobileType()
    info.szMobilePhone = "000"
    info.szChannelName = getChannelName()

    local backCmdIds = {{['mainId']=MDM_MB_LOGON, ['subId']=SUB_MB_LOGON_SUCCESS},
        {['mainId']=MDM_MB_LOGON, ['subId']=SUB_MB_LOGON_FAILURE}}

    self:sendMessage(MDM_MB_LOGON, SUB_MB_QUICK_REGISTER, info, true, backCmdIds)
end

--是否修改过昵称
function HallLogic:sendQueryNickname()
    local msg = {}
    msg.dwGameID = self._loginSuccessInfo.dwGameID

    local backCmdIds = {{['mainId']=MDM_GP_USER_SERVICE, ['subId']=SUB_GP_S_QUERY_RENAMED}}

    self:sendMessage(MDM_GP_USER_SERVICE, SUB_GP_C_QUERY_RENAMED, msg, false, backCmdIds)
end

--修改昵称
function HallLogic:sendChangeNickname(nickName,password)
    local info = {}
    info.dwGameID = self._loginSuccessInfo.dwGameID
    info.szNickname=nickName
    info.szPassword=password

    local backCmdIds = {{['mainId']=MDM_GP_USER_SERVICE, ['subId']=SUB_GP_S_MODIFY_NICKNAME}}
    self:sendMessage(MDM_GP_USER_SERVICE, SUB_GP_C_MODIFY_NICKNAME, info, false, backCmdIds)
end

--bNet=true:网络连接成功就发送，false：登录成功再发送
function HallLogic:sendMessage(mainId, subId, msg, bNet, backCmdInfos)
    if backCmdInfos then
        self._backCmdInfos = backCmdInfos;
    end

    if (self._bUserLogin == false and bNet==false) or self:isHallServerConnected()== false then
        local msgInfo = {}
        msgInfo.mainId = mainId
        msgInfo.subId = subId
        msgInfo.msg = msg
        msgInfo.backCmds = backCmdInfos
        if bNet== nil then
            msgInfo.bNet = false
        else
            msgInfo.bNet = bNet
        end

        table.insert(self._sendMsgArr, msgInfo)

        if self._nSocketState == SOCKET_CLOSED then
            self:resetHallServer()
            self:connectServer()
            self._bUserLogin = false
        end
        return
    end
    self._loginServer:sendMessage(mainId, subId, msg)
end

--列表信息
function HallLogic:onListMessage(subId, msg)
    --dump(msg)
    if subId == SUB_MB_LIST_KIND then
        self:onListKind(msg)
        if VERSION_DEBYG == true then
            print("SUB_MB_LIST_KIND")
        end
    elseif subId == SUB_MB_LIST_SERVER then   --服务器列表
        if VERSION_DEBYG == true then
            print("服务器列表")--有附加字段
        end
        self:onListServer(msg)
    elseif subId == SUB_MB_LIST_FINISH then -- 发送完成
        if VERSION_DEBYG == true then
            print("SUB_GP_LIST_FINISH")
        end
        app.eventDispather:dispatherEvent(eventLoginSrvListFinish)
    end
end

--用户服务
function HallLogic:onServiceMessage(subId, msg)
    if subId == SUB_GP_S_QUERY_RENAMED then   --返回值：零，未更改过昵称；非零，更改过昵称。
        if VERSION_DEBYG == true then
            print("SUB_GP_S_QUERY_RENAMED")
        end
        self._isNick = msg.cbResult
    elseif subId == SUB_GP_S_MODIFY_NICKNAME then   --修返回值：零，修改失败；非零，修改成功。
        if VERSION_DEBYG == true then
            print("SUB_GP_S_MODIFY_NICKNAME")
        end
        if msg["cbResult"]~=0 then
            app.eventDispather:dispatherEvent(eventLoginSrvNickName)
        else
            app.eventDispather:dispatherEvent(eventLoginSrvNickName1)
        end
    elseif subId == SUB_GP_S_SIGNIN_REWARD_INFO then  --签到奖励信息
        --dump(msg)
        self._signinInfo = msg["RewardInfo"]
    elseif subId == SUB_GP_S_USER_SIGNIN_INFO then --用户签到信息
        --dump(msg)
        local day = msg["dwSigninDuration"]
        if msg["cbSigninToday"] == 0 then
            day = day + 1
        end

        if day > 7 then
            day = 7
        end

        self.SigninDay = self._signinInfo[day]["dwDuration"]
        self.SigninCoin = self._signinInfo[day]["llRewardScore"]
        if msg["cbSigninToday"] == 0 then
            self.IsLingQu = false --没签到
            if self._jiemian==0  then
                app.eventDispather:dispatherEvent(eventLoginNoLingQu)
            else
                app.eventDispather:dispatherEvent(eventLoginNoLingQu1)
            end

        else
            self.IsLingQu = true
        end

    elseif subId == SUB_GP_S_USER_SIGNIN_RESULT then --签到结果
        if VERSION_DEBYG == true then
            print("签到结果")
        end
        self._bQianDao = false
        if msg["cbResult"] ~= 0 then
            self.IsLingQu = true
            self._loginSuccessInfo["lUserScore"] = msg["llUserScore"]
            app.eventDispather:dispatherEvent(eventLoginGameChoice)
            app.eventDispather:dispatherEvent(eventLoginSignOk)
        end
    elseif subId == SUB_GP_S_USER_HEARTBEAT then  --心跳
        self._nHeartTimeCount = 0
    elseif subId == SUB_GP_S_USER_SCORE_INFO then  --金币信息更新
        self._loginSuccessInfo.lUserScore = msg.lScore
        self._loginSuccessInfo.lUserInsure = msg.lBankScore
        app.eventDispather:dispatherEvent(eventLoginUserCoinUpdate)
    elseif subId == SUB_GP_S_RECV_RELIEF_DONATE_RESULT then -- 救济金
        if msg.cbResult == 0 then
            self._loginSuccessInfo.lUserScore = msg.llScore
        end
        app.eventDispather:dispatherEvent(eventJiuJiJinLingQu, msg)
    elseif subId == SUB_GP_S_USER_RELIEF_INFO then --救济金次数
        local count = msg.dwAvailableCount
        cc.UserDefault:getInstance():setIntegerForKey("getCount", count)
        cc.UserDefault:getInstance():setIntegerForKey("isPay", msg.cbPay)
        cc.UserDefault:getInstance():setIntegerForKey("isRotrayTab", msg.cbLucky)
        app.eventDispather:dispatherEvent(eventFirstRec)
    elseif subId == SUB_GP_S_LUCKY_ROLLER_RESULT then --幸运转盘
        if msg.cbResult == 0 then
            local logic = DataManager:getInstance():getRotary()
            logic:rotaryLogic(msg.cbIndex)
        end
    elseif subId == SUB_GP_OPERATE_FAILURE then     --操作失败        
        app.eventDispather:dispatherEvent(eventOperateFailed, msg)
    elseif subId == SUB_GP_OPERATE_SUCCESS then     --操作成功
        cc.UserDefault:getInstance():setStringForKey("password",self._password)        
        app.eventDispather:dispatherEvent(eventOperateSucceed, msg)
    end
end

function HallLogic:closeHeartScheduler()
    local scheduler = cc.Director:getInstance():getScheduler()
    if self._sendHeartScheduler then
        scheduler:unscheduleScriptEntry(self._sendHeartScheduler)
        self._sendHeartScheduler = nil
    end
end

--兑换记录
function HallLogic:qureyExchange(num)
    local info = {}
    info.dwUserID = self._loginSuccessInfo["dwUserID"]
    if num == 2 then --乐卷兑换
        local backCmdIds = {{['mainId']=MDM_GP_LEQUAN_SERVICE, ['subId']=SUB_GP_S_EXCHANGE_SCORE_RECORD}}
        self:sendMessage( MDM_GP_LEQUAN_SERVICE, SUB_GP_C_EXCHANGE_SCORE_RECORD, info, false, backCmdIds)
    elseif num == 3 then --充值卡兑换
        local backCmdIds = {{['mainId']=MDM_GP_LEQUAN_SERVICE, ['subId']=SUB_GP_S_EXCHANGE_REFILLCARD_RECORD}}
        self:sendMessage( MDM_GP_LEQUAN_SERVICE, SUB_GP_C_EXCHANGE_REFILLCARD_RECORD, info, false, backCmdIds)
    end
end

--购买记录
function HallLogic:qureyBuyReward()
    local info = {}
    info.dwUserID = self._loginSuccessInfo["dwUserID"]
    local backCmdIds = {{['mainId']=MDM_GP_EXCHANGE_SERVICE, ['subId']=SUB_GP_S_USER_EXCHANGE_RECORDS}}
    self:sendMessage(MDM_GP_EXCHANGE_SERVICE, SUB_GP_C_QUERY_USER_EXCHANGE_RECORDS, info, false, backCmdIds)
end

--实物信息
function HallLogic:shiwuMessage()
    local info = {}
    info.dwUserID = self._loginSuccessInfo["dwUserID"]
    local backCmdIds = {{['mainId']=MDM_GP_REALPROPS_SERVICE, ['subId']=SUB_GP_S_USER_REALPROPS_INFO}}
    self:sendMessage(MDM_GP_REALPROPS_SERVICE, SUB_GP_C_QUERY_USER_REALPROPS_INFO, info, false, backCmdIds)
end

--提交收货地址
function HallLogic:sendRessMessage(ress, name, phonenum)
    local info = {}
    info.dwUserID = self._loginSuccessInfo["dwUserID"]
    info.szPassword = custom.PlatUtil:getMD5String(self._userInfo["password"])
    info.szAddress = ress
    info.szContact = name
    info.szMobilePhone = phonenum

    local backCmdIds = {{['mainId']=MDM_GP_REALPROPS_SERVICE, ['subId']=SUB_GP_S_SUBMIT_DELIVERY_ADDRESS_RESULT}}
    self:sendMessage(MDM_GP_REALPROPS_SERVICE, SUB_GP_C_SUBMIT_DELIVERY_ADDRESS, info, false, backCmdIds)
end

--查询收货地址
function HallLogic:queryRessMessage()
    local info = {}
    info.dwUserID = self._loginSuccessInfo["dwUserID"]
    local backCmdIds = {{['mainId']=MDM_GP_REALPROPS_SERVICE, ['subId']=SUB_GP_S_DELIVERY_ADDRESS}}
    self:sendMessage(MDM_GP_REALPROPS_SERVICE, SUB_GP_C_QUERY_DELIVERY_ADDRESS, info, false, backCmdIds)
end

--查询赠送列表
function HallLogic:queryGiveList()
    local info = {}
    info.dwUserID = self._loginSuccessInfo["dwUserID"]
    local backCmdIds = {{['mainId']=MDM_GP_EXCHANGE_SERVICE, ['subId']=SUB_GP_S_HISTORY_TRANSACTION_USER}}
    self:sendMessage(MDM_GP_EXCHANGE_SERVICE, SUB_GP_C_QUERY_HISTORY_TRANSACTION_USER, info, false, backCmdIds)
end

--兑换实物道具
function HallLogic:changeRealProps(wPropsID, dwAmount)
    local info = {}
    info.dwUserID = self._loginSuccessInfo["dwUserID"]
    info.szPassword = custom.PlatUtil:getMD5String(self._userInfo["password"])
    info.wPropsID = wPropsID
    info.dwAmount = dwAmount
    dump(info)
    local backCmdIds = {{['mainId']=MDM_GP_REALPROPS_SERVICE, ['subId']=SUB_GP_S_EXCHANGE_REALPROPS_RESULT}}
    self:sendMessage(MDM_GP_REALPROPS_SERVICE, SUB_GP_C_EXCHANGE_REALPROPS, info, false, backCmdIds)
end

--查询签到奖励
function HallLogic:querySigninReward()
    local backCmdIds = {{['mainId']=MDM_GP_USER_SERVICE, ['subId']=SUB_GP_S_SIGNIN_REWARD_INFO}}
    self:sendMessage( MDM_GP_USER_SERVICE, SUB_GP_C_QUERY_SIGNIN_REWARD, nil, false, backCmdIds)
end

--查询用户是否签到
function HallLogic:queryUserSignin()
    local info = {}
    info.dwGameID = self._loginSuccessInfo["dwGameID"]
    local backCmdIds = {{['mainId']=MDM_GP_USER_SERVICE, ['subId']=SUB_GP_S_USER_SIGNIN_INFO}}
    self:sendMessage( MDM_GP_USER_SERVICE, SUB_GP_C_QUERY_USER_SIGNIN, info, false, backCmdIds)
end

--用户签到
function HallLogic:userSignin()
    local info = {}
    info.dwGameID = self._loginSuccessInfo["dwGameID"]
    info.szPassword = custom.PlatUtil:getMD5String(cc.UserDefault:getInstance():getStringForKey("password"))
    local backCmdIds = {{['mainId']=MDM_GP_USER_SERVICE, ['subId']=SUB_GP_S_USER_SIGNIN_RESULT}}
    self:sendMessage( MDM_GP_USER_SERVICE, SUB_GP_C_USER_SIGNIN, info, false, backCmdIds)
end

function HallLogic:onListKind(msgs)
    for index, msg in ipairs(msgs) do
        table.insert(self._gameList, msg)
    end
end

-- 幸运转盘
function HallLogic:onRotaryTab()
    local info = {}
    info.dwGameID = self._loginSuccessInfo["dwGameID"]
    info.szPassword = custom.PlatUtil:getMD5String(cc.UserDefault:getInstance():getStringForKey("password"))
    local backCmdIds = {{['mainId']=MDM_GP_USER_SERVICE, ['subId']=SUB_GP_S_LUCKY_ROLLER_RESULT}}
    self:sendMessage( MDM_GP_USER_SERVICE, SUB_GP_C_LUCKY_ROLLER, info, false, backCmdIds)
end

--商城直接赠送红包
function HallLogic:storeGiveBox(account, redValue, count)
    local info = {}
    info.dwUserID = self._loginSuccessInfo["dwUserID"]
    info.szPassword = custom.PlatUtil:getMD5String(cc.UserDefault:getInstance():getStringForKey("password"))
    info.szAccount = account
    info.dwRMBValue = redValue
    info.dwCount = count
    local backCmdIds = {{['mainId']=MDM_GP_REDENVELOPE, ['subId']=SUB_GP_S_EXCHANGE_AND_GIVE_REDENVELOPE}}
    self:sendMessage(MDM_GP_REDENVELOPE, SUB_GP_C_EXCHANGE_AND_GIVE_REDENVELOPE, info, false, backCmdIds)
end

-- 领取救济金
function HallLogic:onBenefits()
    local info = {}
    info.dwGameID = self._loginSuccessInfo["dwGameID"]
    info.szPassword = custom.PlatUtil:getMD5String(cc.UserDefault:getInstance():getStringForKey("password"))
    local backCmdIds = {{['mainId']=MDM_GP_USER_SERVICE, ['subId']=SUB_GP_S_RECV_RELIEF_DONATE_RESULT}}
    self:sendMessage( MDM_GP_USER_SERVICE, SUB_GP_C_RECV_RELIEF_DONATE, info, false, backCmdIds)
end

-- 得到救济金次数
function HallLogic:getBeneCount()
    local info = {}
    info.dwGameID = self._loginSuccessInfo["dwGameID"]
    info.szPassword = custom.PlatUtil:getMD5String(cc.UserDefault:getInstance():getStringForKey("password"))
    local backCmdIds = {{['mainId']=MDM_GP_USER_SERVICE, ['subId']=SUB_GP_S_USER_RELIEF_INFO}}
    self:sendMessage( MDM_GP_USER_SERVICE, SUB_GP_C_QUERY_USER_RELIEF, info, false, backCmdIds)
end

function HallLogic:onListServer(msgs)
    for index, msg in ipairs(msgs) do
        table.insert(self._gameRoomServerList, msg)
        self._zongRenShu=self._zongRenShu+msg["dwOnLineCount"]
    end
    -- DataManager:getInstance():getChoiceLayer():setRenShu()
    app.eventDispather:dispatherEvent(eventLoginRenShu)
end

function HallLogic:getGames()
    local games = {}

    for index, gameServer in ipairs(self._gameRoomServerList) do
        local bExsit = false
        for gameIndex, gameDetail in ipairs(games) do
            if gameDetail.gameId == gameServer.wKindID then
                bExsit = true
                break
            end
        end
        if not bExsit then
            table.insert(games, {["gameId"]=gameServer.wKindID, ["gameName"] = gameServer.szServerName})
        end
    end
    return games
end

function HallLogic:getGameServersByKindId(wKindId)
    local gameServers = {}
    for index, gameServer in ipairs(self._gameRoomServerList) do
        if gameServer.wKindID == wKindId then
            table.insert(gameServers, gameServer)
        end
    end

    return gameServers
end

--网络连接结果
function HallLogic:onLoginNetConnected(bConnected)
    if bConnected ~= true then
        print("connect to server success")
        self._nSocketState = SOCKET_CLOSED
        self._serverIndex = self._serverIndex + 1
        if self._serverIndex <= #hallServerConf then
            self:connectServer()
        else
            self._serverIndex = 1
            app.eventDispather:dispatherEvent(eventHallNetConnected, false)
        end
    else
        print("connect to server error")
        self._nSocketState = SOCKET_CONNECTED
        local sendLogin = false
        for index, item in ipairs(self._sendMsgArr) do
            if item.bNet == true then
                sendLogin = true
                self:sendMessage(item.mainId,item.subId,item.msg, true, item.backCmds)
            end
        end

        if sendLogin == false then
            local username = cc.UserDefault:getInstance():getStringForKey("account")
            local password = cc.UserDefault:getInstance():getStringForKey("password")
            self:loginByUsername(username,password)
        end

        app.eventDispather:dispatherEvent(eventHallNetConnected, true)
    end
end

--网络断开，确定是否需要断线重连
function HallLogic:onLoginNetClosed()
    print("----------onLoginNetClosed")
    self._bUserLogin = false
    self._bLoginIng = false
    self._nSocketState = SOCKET_CLOSED
    
    self._sendMsgArr = {}
    self:closeHeartScheduler()
    app.eventDispather:dispatherEvent(eventHallNetClosed)
end

function HallLogic:closeNet()
    self._loginServer:closeNet()
    self:closeHeartScheduler()
    self._bUserLogin = false
    self._bLoginIng = false
    self._nSocketState = SOCKET_CLOSED
end

function HallLogic:loginByUsername(username, password)    
    if username==nil or #username==0 or password==nil or #password==0 then
        return
    end

    if self._bLoginIng == true then
        return
    end

    self._bLoginIng = true
    self._bUserLogin = false

    self._account = username
    self._password = password

    local loginInfo = {}
    loginInfo.wModuleID = INVALID_WORD
    loginInfo.dwPlazaVersion = VERSION_PLAZA
    loginInfo.cbDeviceType = 1
    loginInfo.szPassword = custom.PlatUtil:getMD5String(password)
    loginInfo.szAccounts = username
    loginInfo.szMachineID = getMobileImei()
    loginInfo.szMobilePhone = "108"
    loginInfo.szChannelName = getChannelName()

    self._userInfo["password"] = password
    local backCmdIds = {{['mainId']=MDM_MB_LOGON, ['subId']=SUB_MB_LOGON_SUCCESS},
        {['mainId']=MDM_MB_LOGON, ['subId']=SUB_MB_LOGON_FAILURE},
        {['mainId']=MDM_MB_LOGON, ['subId']=SUB_MB_UPDATE_NOTIFY},}
	--Log(loginInfo);
    self:sendMessage(MDM_MB_LOGON, SUB_MB_LOGON_ACCOUNTS, loginInfo, true, backCmdIds);
end

--注册
function HallLogic:regist(username, password, insurepass)
    if self._bLoginIng == true then
        return
    end

    self._bLoginIng = true
    self._bUserLogin = false

    self._account = username
    self._password = password

    local registInfo = {}
    registInfo.wModuleID = INVALID_WORD
    registInfo.dwPlazaVersion = VERSION_PLAZA
    registInfo.cbDeviceType = 1
    registInfo.szLogonPass = custom.PlatUtil:getMD5String(password)
    registInfo.szInsurePass = custom.PlatUtil:getMD5String(insurepass)
    registInfo.wFaceID = 1
    registInfo.cbGender = 1
    registInfo.szAccounts = username
    registInfo.szNickName = username
    registInfo.szMachineID = getMobileImei()
    registInfo.szMobilePhone = "108"
    registInfo.szChannelName = getChannelName()

    self._userInfo["password"] = password
    local backCmdIds = {{['mainId']=MDM_MB_LOGON, ['subId']=SUB_MB_LOGON_SUCCESS},
        {['mainId']=MDM_MB_LOGON, ['subId']=SUB_MB_LOGON_FAILURE},
        {['mainId']=MDM_MB_LOGON, ['subId']=SUB_MB_UPDATE_NOTIFY}}
    self:sendMessage(MDM_MB_LOGON, SUB_MB_REGISTER_ACCOUNTS, registInfo, true, backCmdIds);
end

--绑定账号
function HallLogic:bind(name, password)
    if self._bLoginIng == true then
        return
    end

    self._bLoginIng = true
    self._bUserLogin = false

    self._account = name
    self._password = password

    local t_info = {}
    t_info.dwGameID = self._loginSuccessInfo.dwGameID
    t_info.szPassword = custom.PlatUtil:getMD5String(cc.UserDefault:getInstance():getStringForKey("password"))    
    t_info.szNewAccounts = name
    t_info.szNewPassword = crypto.md5(password)

    self._loginServer:sendMessage(MDM_MB_LOGON, SUB_GP_C_BIND_ACCOUNT, t_info)
    -- self:sendMessage(MDM_MB_LOGON, SUB_GP_C_BIND_ACCOUNT, t_info, false, nil);
end

function HallLogic:sendQueryGameGameWay()
    local info = {}
    info.dwUserID = self._loginSuccessInfo.dwUserID
    -- info.szDynamicPassword = self._loginSuccessInfo.szDynamicPassword

    local backCmdIds = {{['mainId']=MDM_GP_GAME_GATEWAY, ['subId']=SUB_GP_S_GAME_GATEWAY_ADDRESS}}
    self:sendMessage(MDM_GP_GAME_GATEWAY,SUB_GP_C_QUERY_GAME_GATEWAY,info,false, backCmdIds)
end

function HallLogic:connectGameGateWay()
    app.table:closeNet()
    if self._gameGateWayInfo and self._gameGateWayInfo.cbActiveServer == 1 then
        custom.PlatUtil:connectServer(CLIENT_GAME,self._gameGateWayInfo.szAddress, self._gameGateWayInfo.wPort, TCP_HEART_GAME_SERVER_TIME)
    else
        app.eventDispather:dispatherEvent(eventLoginGatewayNull)
    end
end

function HallLogic:isHallServerConnected()
    return self._loginServer:isServerConnected()
end

--获得玩家等级
function HallLogic:getPlayerLevel()
    local t_level = 0
    local t_exp = self._loginSuccessInfo.dwExperience
    if VERSION_DEBYG == true then
        print( "#self._gameLevelConf  "..#self._gameLevelConf )
    end
    for i=1, #self._gameLevelConf.GrowLevelItem do
        local t_value = self._gameLevelConf.GrowLevelItem[i]
        if t_exp <= t_value.dwExperience then
            t_level = t_value.wLevelID
            break
        end
    end
    return t_level
end

--获得玩家经验值百分比
function HallLogic:getPlayerExpPer()
    local t_per = 0
    local t_exp = self._loginSuccessInfo.dwExperience
    for i=1, #self._gameLevelConf.GrowLevelItem do
        local t_value = self._gameLevelConf.GrowLevelItem[i]
        if t_exp <= t_value.dwExperience then
            if t_value.wLevelID == 1 then
                t_per = t_exp / t_value.dwExperience * 100
            else
                t_per = (t_exp - self._gameLevelConf.GrowLevelItem[i-1].dwExperience) / (t_value.dwExperience - self._gameLevelConf.GrowLevelItem[i-1].dwExperience) * 100
            end
            break
        end
    end
    return t_per
end

function HallLogic:queryTrumpet()--查询系统喇叭配置
    self:sendMessage(MDM_GP_TRUMPET, SUB_GP_C_QUERY_TRUMPET_INFO, nil, false, nil)
end

function HallLogic:queryUserTrumpet()  --查询玩家喇叭信息
    local info = {}
    info.dwUserID = self._loginSuccessInfo.dwUserID
    self:sendMessage(MDM_GP_TRUMPET,SUB_GP_C_QUERY_USER_TRUMPET, info, false, nil)
end

function HallLogic:buyTrumpet(num)
    local msg = {}
    msg.dwUserID = self._loginSuccessInfo.dwUserID
    msg.szPassword = custom.PlatUtil:getMD5String(cc.UserDefault:getInstance():getStringForKey("password"))
    msg.dwTrumpetCount = num

    self:sendMessage(MDM_GP_TRUMPET,SUB_GP_C_BUY_TRUMPET, msg, false, nil)
end

--查询红包信息
function HallLogic:queryRedEnvelope()
    self:sendMessage(MDM_GP_REDENVELOPE, SUB_GP_C_QUERY_REDENVELOPE_INFO, nil, false, nil)
end

--查询玩家红包信息
function HallLogic:queryUserRedEnvelope()
    local info = {}
    info.dwUserID = self._loginSuccessInfo.dwUserID
    self:sendMessage(MDM_GP_REDENVELOPE, SUB_GP_C_QUERY_USER_REDENVELOPE, info, false, nil)
end

--购买红包
function HallLogic:buyRedEnvelope(redValue, count)
    local msg = {}
    msg.dwUserID = self._loginSuccessInfo.dwUserID
    msg.szPassword = custom.PlatUtil:getMD5String(cc.UserDefault:getInstance():getStringForKey("password"))
    msg.dwRMBValue = redValue
    msg.dwCount = count
    self:sendMessage(MDM_GP_REDENVELOPE, SUB_GP_C_EXCHANGE_REDENVELOPE, msg, false, nil)
end

--赠送红包
function HallLogic:giveRedEnvelope(account, redValue, count)
    local msg = {}
    msg.dwUserID = self._loginSuccessInfo.dwUserID
    msg.szPassword = custom.PlatUtil:getMD5String(cc.UserDefault:getInstance():getStringForKey("password"))
    msg.szAccount = account
    msg.dwRMBValue = redValue
    msg.dwCount = count
    
    --dump(msg)
    self:sendMessage(MDM_GP_REDENVELOPE, SUB_GP_C_GIVE_REDENVELOPE, msg, false, nil)
end

--打开红包
function HallLogic:openRedEnvelope(redValue, count)
    local msg = {}
    msg.dwUserID = self._loginSuccessInfo.dwUserID
    msg.szPassword = custom.PlatUtil:getMD5String(cc.UserDefault:getInstance():getStringForKey("password"))
    msg.dwRMBValue = redValue
    msg.dwCount = count
    self:sendMessage(MDM_GP_REDENVELOPE, SUB_GP_C_OPEN_REDENVELOPE, msg, false, nil)
end

function HallLogic:sendUpdateUserCoin()
	local msg = {}
	msg.dwUserID = self._loginSuccessInfo.dwUserID
	
    self:sendMessage(MDM_GP_USER_SERVICE, SUB_GP_C_QUERY_USER_SCORE, msg, false, nil)
end

function HallLogic:revisePassword(oldPassword, newPassword)
    self._password = newPassword
    
    oldPassword = crypto.md5(oldPassword)
    newPassword = crypto.md5(newPassword)

    local t_info = {}
    t_info.dwUserID = self._loginSuccessInfo.dwUserID    
    t_info.szDesPassword = newPassword
    t_info.szScrPassword = oldPassword    

    self._loginServer:sendMessage(MDM_GP_USER_SERVICE, SUB_GP_MODIFY_LOGON_PASS, t_info)    
end

function HallLogic:isBinded()
    local res = string.split( self._loginSuccessInfo.szNickName, "-" )
    local t_bind = false
    if res[1] == "Guest" then
        t_bind = false
    else
        t_bind = true
    end
    return t_bind
end

return HallLogic