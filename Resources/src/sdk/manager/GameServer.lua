
GameServer = class("GameServer")

ALL_COUNT_DOWN = 180

GameServer.countDownTime = ALL_COUNT_DOWN
GameServer.begin = false

function GameServer:ctor()
    self._tableCallBack = nil
    
    self.messageDeal = MessageDeal.new()
    self.messageDeal:parseConf("gameMsg/gameserver.json")
    
    self:coutDown()
end

function GameServer:beginCountDown()
    self.begin = true
    self.countDownTime = ALL_COUNT_DOWN
end

function GameServer:endCountDown()
    self.begin = false
    self.countDownTime = ALL_COUNT_DOWN
end

function GameServer:pauseCountDown()
    self.begin = false
end

function GameServer:continueCountDown()
    self.begin = true
end

function GameServer:coutDown()
    local scheduler = cc.Director:getInstance():getScheduler()
    local scheLoop = nil

    local function loopTimer()
        if scheLoop then
            scheduler:unscheduleScriptEntry(scheLoop)
            scheLoop = nil
        end
        
        if self.begin then
            if self.countDownTime > 0 then
                self.countDownTime = self.countDownTime - 1
            else
                self.begin = false
                app.eventDispather:dispatherEvent(eventGameNoPlay)
            end
        end

        scheLoop = scheduler:scheduleScriptFunc(loopTimer, 1.0, false)  --参数2，设置时间
    end

    loopTimer()
end

function GameServer:onNetConnected(bConnect)
    if self._tableCallBack then
        self._tableCallBack:onGameServerConnected(bConnect)
	end
end

function GameServer:onNetClosed()
    if self._tableCallBack then
        self._tableCallBack:onGameServerClosed()
    end
end

function GameServer:sendMessage(mainId, subId, msg)
    --判断服务器是否连接成功
    if self._bNetConnected == false then
        return
    end    

    local msgStr = nil
    local clientId = CLIENT_GAME

    msgStr = self.messageDeal:encodeMessage(mainId, subId, msg)
 
    --调用c++协议发送接口
        
    local netMsg = custom.NetMessage:new()
    netMsg:setMainID(mainId)
    netMsg:setSubId(subId)
    if msgStr then
        netMsg:setMsgLen(msgStr:getLen())
    else
        netMsg:setMsgLen(0)
    end

    if VERSION_DEBYG==true then
        printf("GameServer:sendMessage mainId:%d   subId:%d   len:%d", mainId, subId, netMsg:getMsgLen())
    end
    
    if msgStr then
        netMsg:setMsgLen(msgStr:getLen())
        msgStr:setPos(1)
        for i=1, msgStr:getLen() do
            netMsg:setDataAtIndex(i-1, msgStr:readUByte())
        end
    end

    custom.PlatUtil:sendMessage(clientId,netMsg)
end

function GameServer:sendDetailMessage(mainId, subId, msgStr)
    if self._bNetConnected == false then
        return
    end
    
    if subId ~= SUB_GF_HEART then        
        self.countDownTime = ALL_COUNT_DOWN
    end
    
    --调用c++协议发送接口
    
    local netMsg = custom.NetMessage:new()
    netMsg:setMainID(mainId)
    netMsg:setSubId(subId)
    if msgStr then
        netMsg:setMsgLen(msgStr:getLen())
    else
        netMsg:setMsgLen(0)
    end
    
    if VERSION_DEBYG==true then
        printf("GameServer:sendDetailMessage mainId:%d   subId:%d   len:%d", mainId, subId, netMsg:getMsgLen())
    end
    
    if msgStr then
        netMsg:setMsgLen(msgStr:getLen())
        msgStr:setPos(1)
        for i=1, msgStr:getLen() do
            netMsg:setDataAtIndex(i-1, msgStr:readUByte())
        end
    end

    custom.PlatUtil:sendMessage(CLIENT_GAME, netMsg)
end

function GameServer:onGameMessage(msg)
    local mainId = msg:getMainID()
    local subId = msg:getSubId()
    local bytelen = msg:getMsgLen()
       
    if VERSION_DEBYG==true then
        printf("GameServer::onMessage maindid: %d,  subId: %d,  bytelen: %d", mainId, subId, bytelen)
    end
    if msg then
        local msgDetail = self.messageDeal:decodeMessage(mainId, subId, msg)
        if msgDetail then
            if self._tableCallBack then
                self._tableCallBack:onGameMessage(mainId, subId, msgDetail)                
                return
            end
        end        
    end
    
    if self._tableCallBack then
        self._tableCallBack:onGameDetailMessage(mainId, subId, msg)
    end
end

--连接游戏服
function GameServer:connect(clientId, ip, port, heartDelaytime)
    custom.PlatUtil:connectServer(clientId, ip, port, heartDelaytime)
end

--注册ITable接口,具体接口函数查看ITable.lua
function GameServer:registITable(callBack)
    self._tableCallBack = callBack
end

function GameServer:resetITable(parameters)
    self._tableCallBack = nil
end

return GameServer