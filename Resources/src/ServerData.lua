if type(ServerDatals) ~= 'table' then
	_G.ServerDatals = {}
end

if type(UserInfo) ~= 'table' then
	_G.UserInfo = {}
end

function clearServerData()
	ServerDatals = {}
end

function setServerData(wKindID, wSortID, wServerID, wServerPort, dwOnLineCount, szServerAddr, szServerName)
	local serverData = {}	
	serverData.wKindID = wKindID
	serverData.wSortID = wSortID
	serverData.wServerID = wServerID
	serverData.wServerPort = wServerPort
	serverData.dwOnLineCount = dwOnLineCount
	serverData.szServerAddr = szServerAddr
	serverData.szServerName = szServerName
	table.insert(ServerDatals, serverData)
	-- ServerDatals[#ServerDatals+1] = serverData
	-- print("---------szServerName = "..szServerName)
end

function setUserData(dwUserID, szPassword, szNickName, lWebToken, platform)
	UserInfo.dwUserID = dwUserID
	UserInfo.szPassword = szPassword
	UserInfo.szNickName = szNickName
	UserInfo.lWebToken = lWebToken
	UserInfo.dwplatform = platform
	
	--print("------------"..loginTime)
	return
end

function getServerData ()
	return ServerDatals
end

function getuserInfo()
	return UserInfo
end
