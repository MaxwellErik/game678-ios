GameUpdate = class("GameUpdate")

import(".GameDownLoad")

local function hex(s)
    s=string.gsub(s,"(.)",function (x) return string.format("%02X",string.byte(x)) end)
    return s
end

local function readFile(path)
    local file = io.open(path, "rb")
    if file then
        local content = file:read("*all")
        io.close(file)
        return content
    end
    return nil
end

local function removeFile(path)
    os.remove(path)
end

local function checkFile(fileName, cryptoCode)
    --print("checkFile:", fileName)
    --print("cryptoCode:", cryptoCode)

    if not io.exists(fileName) then
        --print("checkFile:"..fileName.." not exists")
        return false
    end

    local data=readFile(fileName)
    if data==nil then
        --print("checkFile:"..fileName.." filedata is nil")
        return false
    end

    if cryptoCode==nil then
        return true
    end

    local ms = crypto.md5(hex(data))
    --print("file cryptoCode:", ms)
    if ms==cryptoCode then
        return true
    end

    return false
end

local function checkDirOK( path )
    if path == nil then
        return false
    end

    local ret = lfs.chdir(path)
    if ret==nil then
        return false
    else
        return true
    end
end

local function findLastStr(srcStr, subStr)
    if srcStr ~= nil and srcStr ~= "" then
        local startIndex, endIndex = string.find(srcStr,subStr)
        if startIndex == nil then
            return 0
        else
            return endIndex + findLastStr(string.sub(srcStr, endIndex+1, -1), subStr)
        end
    else
        return 0
    end
end

local function createDir(path)
    path = string.sub(path, 1, -2)
    local lastIndex = findLastStr(path, "/")
    local subPath = string.sub(path, 1, lastIndex)
    local dirName = string.sub(path, lastIndex, string.len(path))

    local pathinfo = io.pathinfo(subPath)
    if checkDirOK(pathinfo.dirname)==false then
        createDir(subPath)
    end

    lfs.mkdir(path)
end

function os.rmdir(path)
    --print("os.rmdir:", path)
    --print(io.exists(path))
    if checkDirOK(path)==true then
        local function _rmdir(path)
            local iter, dir_obj = lfs.dir(path)
            while true do
                local dir = iter(dir_obj)
                if dir == nil then
                    break
                end

                if dir ~= "." and dir ~= ".." then
                    local curDir = path..dir
                    local mode = lfs.attributes(curDir, "mode") 
                    if mode == "directory" then
                        _rmdir(curDir.."/")
                    elseif mode == "file" then
                        --print('delete file:'..curDir)
                        os.remove(curDir)
                    end
                end
            end
            lfs.rmdir(path)
            return succ
        end
        _rmdir(path)
    end
    return true
end

function GameUpdate:ctor()
    self._path = device.writablePath.."upd/"
    self._gameKey = nil
    self._url = nil
    self._callUpdateBack = nil
    self._bDownloadOK = false
    self._gameConfName = "GUpdateConf.txt"
end

--[[
callBack需要实现5个函数
1、onConfFailed(gameKey) 获取更新文件失败
2、getConfOK(gameKey, downlist, removelist)
3、downProgress(gameKey, progress)
4、downError(gameKey) 
5、downFinish(gameKey)
]]
function GameUpdate:setUpdateData(gameKey, baseUrl, callBack)
	self._gameKey = gameKey
    self._path = self._path..gameKey.."/"
    self._url = baseUrl
    self._callBack = callBack
	self._bDownloadOK = true
	self._bEndProcess = false
	
	self._curListFile = self._path..self._gameConfName
	self._newListFile = self._curListFile..".upd"
    self._fileList = nil
    if io.exists(self._curListFile) then
        self._fileList = dofile(self._curListFile)
    end
    self._baseFileList = {
            ver = "1.0.0",
            detail = {
                code = {},
                res = {},
            },
        }
        
    if self._fileList==nil then
        self._fileList = self._baseFileList
    end
    
    local fileVers = string.split(self._fileList.ver, '.')
    local baseVers = string.split(self._baseFileList.ver, '.')
    
    local clearCache = false
    if #fileVers ~= #baseVers or #baseVers ~= 3 then
        clearCache = true
    else
        local fileVer1 = tonumber(fileVers[1])
        local fileVer2 = tonumber(fileVers[2])
        local fileVer3 = tonumber(fileVers[3])

        local baseVer1 = tonumber(baseVers[1])
        local baseVer2 = tonumber(baseVers[2])
        local baseVer3 = tonumber(baseVers[3])

        if  (fileVer1 < baseVer1) or (fileVer1 == baseVer1 and fileVer2 < baseVer2) or (fileVer1 == baseVer1 and fileVer2 == baseVer2 and fileVer3 <= baseVer3) then
            clearCache = true
        end
    end
    
    if clearCache == true then  --删除之前热更新的所有文件
        os.rmdir(self._path)
    end
    
    self._updateUrl = self._url..self._gameKey.."/"..self._gameConfName
    self:startGetConf()   
end

function GameUpdate:startGetConf()
    self:requestFromServer(self._updateUrl)
    self:downLoop()
end

function GameUpdate:startDownLoad()
    self._bStartDownLoad = true
    if self._downFileList and #self._downFileList > 0 then
        printf("开始下载，一共要下载的文件个数:%d", #self._downFileList)
        --开启下载
        local function downProgress(gameKey,progress)
            if self._callBack then
                self._callBack:downProgress(self._gameKey, progress)
            end
        end

        local function downError(gameKey)
            if self._callBack then
                self._callBack:downError(self._gameKey)
            end
        end

        local function downFinish(gameKey)
            for index, item in ipairs(self._downFileList) do
                if item.act == "unzip" then
                    local unZipFile = self._path..item.name
                    --printf("解压文件：%s", unZipFile)
                    custom.PlatUtil:uncompressZip(unZipFile, self._path)
                    os.remove(unZipFile)
                end            	
            end
            
            local con = readFile(self._newListFile)
            if con then
                io.writefileCheckDir(self._curListFile, con)
                if self._callBack then
                    self._callBack:downFinish(self._gameKey)
                end
            else
                if self._callBack then
                    self._callBack:downError(self._gameKey) 
                end
            end
            os.remove(self._newListFile)
        end

        local gameDownload = GameDownLoad.new()
        gameDownload:setDownData(self._gameKey, self._downFileList, self._url..self._gameKey.."/", downProgress, downError, downFinish)
    elseif self._downFileList then
        --print("没有下载文件")
        if self._callBack then
            self._callBack:downFinish(self._gameKey)
        end
    else
        self:startGetConf()  
        if self._callBack then
            self._callBack:downError(self._gameKey)
        end
    end
end

function GameUpdate:requestFromServer(url)
    --printf('获取配置信息:%s', url)
    local request = network.createHTTPRequest(function(event)
        self:onResponse(event)
    end, url, "GET")
    
    if request then
        request:setTimeout(100)
        request:start()
    else
        self._bDownloadOK = false
        self:endProcess()
    end
end

function GameUpdate:onResponse(event, dumpResponse)
    local request = event.request
    if event.name == "completed" then
        if request:getResponseStatusCode() ~= 200 then
            self._bDownloadOK = false
            self._bEndProcess = true            
        else
            self.dataRecv = request:getResponseData()
            --printf("接受数据完毕 len:%d", #self.dataRecv)
        end
    elseif event.name == "failed" then
        self._bDownloadOK = false
        self._bEndProcess = true
    end
end

function GameUpdate:downLoop()
    local scheduler = cc.Director:getInstance():getScheduler()
    self.schedulerEntry = scheduler:scheduleScriptFunc(function() self:onEnterFrame(0) end, 0, false)
end

function GameUpdate:onEnterFrame(dt)
    if self.dataRecv then
        --printf("接收到配置数据 gameKey:%s", self._gameKey)
        io.writefileCheckDir(self._newListFile, self.dataRecv)
        self.dataRecv = nil
        self._downFileList = {}
        self._removeFileList = {}
        
        self.fileListNew = dofile(self._newListFile)
        
        --self:checkUpdateFileExist()
        if self:checkUpdateByVersion(self._fileList, self.fileListNew.ver)== true then  
            self:parseUpdateConf()
        end             
            
        if self._callBack then
            self._callBack:getConfOK(self._gameKey, self._downFileList, self._removeFileList)
        end
        
        if self._bStartDownLoad == true then
        	self:startDownload()
        end
    end
    
    if self.bEndProcess == true then
        self:endProcess()
    end
end

function GameUpdate:endProcess()
    local scheduler = cc.Director:getInstance():getScheduler()
    scheduler:unscheduleScriptEntry(self.schedulerEntry) 
    
    if self._callBack then
    	self._callBack:onConfFailed(self._gameKey)
    end
end

--确定所有下载文件,删除文件
function GameUpdate:parseUpdateConf()
    for index, item in ipairs(self.fileListNew.detail.code) do
    	table.insert(self._downFileList, item)
    end
    
    for index, item in ipairs(self.fileListNew.detail.res) do
    	if self:checkUpdateByVersion(self._fileList, item.ver)== true then
            for fileIndex, fileItem in ipairs(item.stage) do
                table.insert(self._downFileList, fileItem)
    		end
    		
            for fileIndex, fileItem in ipairs(item.remove) do
                table.insert(self._removeFileList, fileItem)
    		end
    	end
    end
end

function GameUpdate:checkUpdateByVersion(fileList, confVersion)
    local curVers = string.split(fileList.ver, '.')
    local confVers = string.split(confVersion, '.')
        
    if #curVers ~= #confVers or #curVers ~= 3 then
    	return false
    end
    
    local curVer1 = tonumber(curVers[1])
    local curVer2 = tonumber(curVers[2])
    local curVer3 = tonumber(curVers[3])
    
    local confVer1 = tonumber(confVers[1])
    local confVer2 = tonumber(confVers[2])
    local confVer3 = tonumber(confVers[3])
    
    if  curVer1 < confVer1 then
    	return true
    end
    
    if curVer1 == confVer1 and curVer2 < confVer2 then
    	return true
    end
    
    if curVer1 == confVer1 and curVer2 == confVer2 and curVer3 < confVer3 then
        return true
    end
    return false
end

function GameUpdate:checkUpdateFileExist()
    local bUpdate = false
    if self:checkUpdateByVersion(self._baseFileList, self._fileList.ver)== true then
        for index, item in ipairs(self.fileListNew.detail.code) do
            local filePath = self._path..item.name
            if not io.exists(filePath) then
                table.insert(self._downFileList, item)
                bUpdate = true
            end
        end
        
        for index, item in ipairs(self._fileList.detail.res) do
            if self:checkUpdateByVersion(self._baseFileList, item.ver)== true then
                for fileIndex, fileItem in ipairs(item.stage) do
                    local filePath = self._path..fileItem.name
                    if not io.exists(filePath) then
                        table.insert(self._downFileList, fileItem)
                        bUpdate = true
                    end
                end
            end
        end
    end
    return bUpdate
end
  


function GameUpdate:checkUpdateByVersion(fileList, confVersion)
    local curVers = string.split(fileList.ver, '.')
    local confVers = string.split(confVersion, '.')

    if #curVers ~= #confVers or #curVers ~= 3 then
        return false
    end

    local curVer1 = tonumber(curVers[1])
    local curVer2 = tonumber(curVers[2])
    local curVer3 = tonumber(curVers[3])

    local confVer1 = tonumber(confVers[1])
    local confVer2 = tonumber(confVers[2])
    local confVer3 = tonumber(confVers[3])

    if  curVer1 < confVer1 then
        return true
    end

    if curVer1 == confVer1 and curVer2 < confVer2 then
        return true
    end

    if curVer1 == confVer1 and curVer2 == confVer2 and curVer3 < confVer3 then
        return true
    end
    return false
end

return GameUpdate