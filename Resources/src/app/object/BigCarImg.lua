
BigCarImg = class("BigCarImg", function()
    return ccui.Widget:new()
end)

BigCarImg._car=nil       --车子图片

function BigCarImg:ctor()
    --文字描述图片
    local bg=cc.Sprite:create("HallUI/gameLayer/bookingSheetBig.png")
    bg:setAnchorPoint(0,0)
    self:addChild(bg)

    self._car=cc.Sprite:create("HallUI/gameLayer/bsj.png")
    self._car:setAnchorPoint(0,0)
    self._car:setPosition(0,0)
    bg:addChild(self._car)

end

----参数1，车子图片 
function BigCarImg:setContent(describe)
    if describe==0 then
        self._car:setTexture("HallUI/gameLayer/bsj.png")
    elseif describe==1 then 
        self._car:setTexture("HallUI/gameLayer/bsj1.png")
    elseif describe==2 then 
        self._car:setTexture("HallUI/gameLayer/msld.png")
    elseif describe==3 then 
        self._car:setTexture("HallUI/gameLayer/msld1.png")
    elseif describe==4 then 
        self._car:setTexture("HallUI/gameLayer/bmw.png")
    elseif describe==5 then 
        self._car:setTexture("HallUI/gameLayer/bmw1.png")
    elseif describe==6 then 
        self._car:setTexture("HallUI/gameLayer/lbjn.png")
    elseif describe==7 then 
        self._car:setTexture("HallUI/gameLayer/lbjn1.png")
    end
    local bg=cc.Sprite:create("HallUI/gameLayer/bookingSheetBig.png")   --设置每个控件大小
    self:setContentSize(bg:getContentSize())
end


return BigCarImg

