
CarImg = class("CarImg", function()
    return ccui.Widget:new()
end)

CarImg._car=nil       --车子图片

function CarImg:ctor()
    --文字描述图片
    local bg=cc.Sprite:create("HallUI/gameLayer/bookingSheet.png")
    bg:setAnchorPoint(0,0)
    self:addChild(bg)
    
    self._car=cc.Sprite:create("HallUI/gameLayer/img_2.png")
    self._car:setPosition(35,45)
    bg:addChild(self._car)

end

----参数1，车子图片 
function CarImg:setContent(describe)
    if describe==0 then
        self._car:setTexture("HallUI/gameLayer/img_5.png")
    elseif describe==1 then 
        self._car:setTexture("HallUI/gameLayer/img_9.png")
    elseif describe==2 then 
        self._car:setTexture("HallUI/gameLayer/img_3.png")
    elseif describe==3 then 
        self._car:setTexture("HallUI/gameLayer/img_7.png")
    elseif describe==4 then 
        self._car:setTexture("HallUI/gameLayer/img_4.png")
    elseif describe==5 then 
        self._car:setTexture("HallUI/gameLayer/img_8.png")
    elseif describe==6 then 
        self._car:setTexture("HallUI/gameLayer/img_2.png")
    elseif describe==7 then 
        self._car:setTexture("HallUI/gameLayer/img_6.png")
    end
    local bg=cc.Sprite:create("HallUI/gameLayer/bookingSheet.png") --设置每个控件大小
    self:setContentSize(bg:getContentSize())
end


return CarImg

