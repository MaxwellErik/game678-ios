BackLayer = class("BackLayer", function ()
    return display.newLayer()
end)

function BackLayer:ctor()  
    local uiLayer = ccs.GUIReader:getInstance():widgetFromJsonFile("HallUI/BackLayer.json")
    self:addChild(uiLayer)
    
    uiLayer:addTouchEventListener(function(sender, type)
        if type == ccui.TouchEventType.ended then
            self:dismiss()
        end
    end)
    
    self.backBG = ccui.Helper:seekWidgetByName(uiLayer,"Image_Back")
    self.backBG:setVisible(false)
    self.bgPosX = self.backBG:getPositionX()
    self.bgPosY = self.backBG:getPositionY()
        
    local backRegionBtn = ccui.Helper:seekWidgetByName(uiLayer, "Button_Region")
    self.soundOpenBtn = ccui.Helper:seekWidgetByName(uiLayer, "Button_SoundOpen")
    self.musicOpenBtn = ccui.Helper:seekWidgetByName(uiLayer, "Button_MusicOpen")
    self.soundCloseBtn = ccui.Helper:seekWidgetByName(uiLayer, "Button_SoundClose")
    self.musicCloseBtn = ccui.Helper:seekWidgetByName(uiLayer, "Button_MusicClose")
    
    backRegionBtn:addTouchEventListener(function(sender, type)
        if type == ccui.TouchEventType.ended then
            self.callBack()
            self:dismiss()
        end
    end)
    
    self.soundOpenBtn:addTouchEventListener(function(sender, type)
        if type == ccui.TouchEventType.ended then
            cc.UserDefault:getInstance():setFloatForKey("soundEffect", 0)
            self.soundOpenBtn:setVisible(false)
            self.soundCloseBtn:setVisible(true)
        end
    end)
    
    self.soundCloseBtn:addTouchEventListener(function(sender, type)
        if type == ccui.TouchEventType.ended then
            cc.UserDefault:getInstance():setFloatForKey("soundEffect", 1)
            self.soundOpenBtn:setVisible(true)
            self.soundCloseBtn:setVisible(false)
        end
    end)
    
    self.musicOpenBtn:addTouchEventListener(function(sender, type)
        if type == ccui.TouchEventType.ended then
            cc.UserDefault:getInstance():setFloatForKey("sound", 0)
            self.musicOpenBtn:setVisible(false)
            self.musicCloseBtn:setVisible(true)
            app.musicSound:stopMusic()
        end
    end)
    
    self.musicCloseBtn:addTouchEventListener(function(sender, type)
        if type == ccui.TouchEventType.ended then
            cc.UserDefault:getInstance():setFloatForKey("sound", 1)
            self.musicOpenBtn:setVisible(true)
            self.musicCloseBtn:setVisible(false)
            if self.musicKey~= nil then
                app.musicSound:playMusic(self.musicKey, true)
            end
        end
    end)
    
    self.soundOpenBtn:setVisible(false)
    self.musicOpenBtn:setVisible(false)
    self.soundCloseBtn:setVisible(false)
    self.musicCloseBtn:setVisible(false)
    
    if cc.UserDefault:getInstance():getFloatForKey("soundEffect")~=0 then        
        self.soundOpenBtn:setVisible(true)
    else        
        self.soundCloseBtn:setVisible(true)
    end

    if cc.UserDefault:getInstance():getFloatForKey("sound")~=0 then
        self.musicOpenBtn:setVisible(true)
    else
        self.musicCloseBtn:setVisible(true)
    end  
end

function BackLayer:setMusicKey(musicKey)
	self.musicKey = musicKey
end

function BackLayer:setBackRegionCallBack(backFunc)
	self.callBack = backFunc
end

function BackLayer:setBGPosY(posY)
	if posY~=nil then
        self.bgPosY = posY
	end
end

function BackLayer:show()
    self.backBG:stopAllActions()
    self.backBG:setVisible(true)
    self.backBG:setPosition(self.bgPosX, 1200)
    print(self.bgPosY)
    self.backBG:runAction(cc.Sequence:create(cc.MoveTo:create(0.5, {['x']=self.bgPosX,['y']=self.bgPosY}), cc.MoveBy:create(0.03, {['x']=0,['y']=20}), cc.MoveBy:create(0.05,{['x']=0,['y']=-20})))
end

function BackLayer:dismiss()
    self.backBG:runAction(cc.Sequence:create(cc.MoveBy:create(0.5, {['x']=0,['y']=1200}), cc.CallFunc:create(function()
        self:removeFromParent()
    end)))
end

function BackLayer:setBg( a_bg )
    self.backBG:loadTexture(a_bg)
end


return BackLayer
