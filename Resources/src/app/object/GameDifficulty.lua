
GameDifficulty = class("GameDifficulty", function()
    return ccui.Widget:new()
end)

GameDifficulty._uiFrame=nil   --图片
GameDifficulty._wKindId=0           --id
GameDifficulty._gameServerInfo = nil

function GameDifficulty:ctor(gameServer)
    self._gameServerInfo = gameServer
    --返回按钮点击事件
    local function choiseCallBack(sender, eventType)


        if eventType == ccui.TouchEventType.ended then            
            --判断启动哪个游戏
            for index, gameConf in pairs(allGameConfs) do
                if gameConf.wKind == self._gameServerInfo.wKindID then                    
                    import(gameConf.entrance)
                    app.table:setClientVersion(gameConf.dwClientVersion)
                    app.eventDispather:dispatherEvent("GameLevelStartGame", self._gameServerInfo)
                    break
                end
            end
        end
    end

    --按钮
    local normalFile = string.format("GameRes/%d_Room_%d.png",self._gameServerInfo.wKindID, self._gameServerInfo.wServerID)
    local touchFile = string.format("GameRes/%d_Room_%d_touch.png",self._gameServerInfo.wKindID, self._gameServerInfo.wServerID)
    self._uiFrame = ccui.Button:create(normalFile, touchFile)
    self._uiFrame:setAnchorPoint(0,0)
    self:addChild(self._uiFrame)
    self._uiFrame:setSwallowTouches(false)
    self._uiFrame:addTouchEventListener(choiseCallBack)
end

--参数1，设置图片；参数2，设置位置x；参数3，设置位置y
function GameDifficulty:setContent(name0,name1,x,y)
    self._uiFrame:loadTextures(name0,name1,name1)
    self:setPosition(x,y)
end

return GameDifficulty

