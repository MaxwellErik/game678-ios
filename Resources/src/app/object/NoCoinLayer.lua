NoCoinLayer = class("NoCoinLayer", function ()
    return display.newLayer()
end)

function NoCoinLayer:ctor()  
    local uiLayer = ccs.GUIReader:getInstance():widgetFromJsonFile("HallUI/NoCoin.json")
    self:addChild(uiLayer)
    
    self.backBG = ccui.Helper:seekWidgetByName(uiLayer,"queQianTiShi")
    self.backBG:setTouchEnabled(true)
    self.bgPosX = self.backBG:getPositionX()
    self.bgPosY = self.backBG:getPositionY()
    
    self.backBG:setPositionX(2000)
        
    local okBtn = ccui.Helper:seekWidgetByName(uiLayer, "okBtn")
    local cancleBtn = ccui.Helper:seekWidgetByName(uiLayer, "quXiaoBtn")
    
    cancleBtn:addTouchEventListener(function(sender, type)
        if type == ccui.TouchEventType.ended then
            self:dismiss()
        end
    end)
    
    okBtn:addTouchEventListener(function(sender, type)
        if type == ccui.TouchEventType.ended then
            self:callBack()
            self:dismiss()
        end
    end)
end

function NoCoinLayer:setCallBack(backFunc)
	self.callBack = backFunc
end

function NoCoinLayer:show()
    self.backBG:stopAllActions()
    app.musicSound:playSound(SOUND_UI_OUTIN)
    self.backBG:runAction(cc.Sequence:create(cc.MoveTo:create(0.2, {['x']=self.bgPosX,['y']=self.bgPosY}), 
        cc.MoveBy:create(0.03, cc.p(20, 0)),
        cc.MoveBy:create(0.05,cc.p(-20, 0))))
end

function NoCoinLayer:dismiss()
    app.musicSound:playSound(SOUND_UI_OUTIN)
    self.backBG:runAction(cc.Sequence:create(cc.MoveBy:create(0.2, cc.p(-700, 0)), cc.CallFunc:create(function()
        self:removeFromParent()
    end)))
end


return NoCoinLayer
