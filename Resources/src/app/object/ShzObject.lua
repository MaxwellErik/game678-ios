
ShzObject = class("ShzObject", function()
    return ccui.Widget:new()
end)

ShzObject._bg=nil      --bg图片
ShzObject._animation = nil     --动画
ShzObject._num=nil

--参数一，第几个动画
function ShzObject:ctor(num)
    local sprite = display.newSprite("GameShz/border_00.png")
    self:addChild(sprite, 20)
    sprite:setPosition(cc.p(sprite:getContentSize().width/2, sprite:getContentSize().height/2))

    self._num = num
    if num <= 9 then
        ccs.ArmatureDataManager:getInstance():addArmatureFileInfo("GameShz/shztubiao/shztubiao.ExportJson")
        self._animation = ccs.Armature:create("shztubiao")
    else
        ccs.ArmatureDataManager:getInstance():addArmatureFileInfo("GameShz/shztubiao/shztubiao_2.ExportJson")
        self._animation = ccs.Armature:create("shztubiao_2")
        self._animation:getAnimation():setSpeedScale(0.8)
    end
    sprite:addChild(self._animation, -1)
    self._animation:getAnimation():play("shzdonghua"..num.."_1")
    self._animation:setPosition(cc.p(sprite:getContentSize().width/2, sprite:getContentSize().height/2))
end

function ShzObject:setNum(num)
    self._num = num
end

--播放动画
function ShzObject:playDonghua()
    self._animation:getAnimation():play("shzdonghua"..self._num.."_2")
    if cc.UserDefault:getInstance():getFloatForKey("shzShengli")==self._num then
        --print("播放音效："..self._num)
        if self._num==1 then    --图片飞龙是1 游戏中是8
            app.musicSound:playSound("laohuji_feilong")
        elseif self._num==2 then    --忠义堂
            app.musicSound:playSound("laohuji_zhongyitang")
        elseif self._num==3 then    --替天行道
            app.musicSound:playSound("laohuji_titianxingdao")
        elseif self._num==4 then    --松江
            app.musicSound:playSound("laohuji_songjiang")
        elseif self._num==5 then    --林冲
            app.musicSound:playSound("laohuji_linchong")
        elseif self._num==6 then    --鲁智深
            app.musicSound:playSound("laohuji_luzhishen")
        elseif self._num==7 then    --大刀
            app.musicSound:playSound("laohuji_jindao")
        elseif self._num==8 then    --银枪
            app.musicSound:playSound("laohuji_yinqiang")
        elseif self._num==9 then    --斧头
            app.musicSound:playSound("laohuji_tiefu")
        end
    end
end

--播放指定动画
function ShzObject:playDonghua2(num)
    self._num = num
    self._animation:getAnimation():play("shzdonghua"..self._num.."_2")
    if self._num==1 then    --图片飞龙是1 游戏中是8
        app.musicSound:playSound("laohuji_feilong")
    elseif self._num==2 then    --忠义堂
        app.musicSound:playSound("laohuji_zhongyitang")
    elseif self._num==3 then    --替天行道
        app.musicSound:playSound("laohuji_titianxingdao")
    elseif self._num==4 then    --松江
        app.musicSound:playSound("laohuji_songjiang")
    elseif self._num==5 then    --林冲
        app.musicSound:playSound("laohuji_linchong")
    elseif self._num==6 then    --鲁智深
        app.musicSound:playSound("laohuji_luzhishen")
    elseif self._num==7 then    --大刀
        app.musicSound:playSound("laohuji_jindao")
    elseif self._num==8 then    --银枪
        app.musicSound:playSound("laohuji_yinqiang")
    elseif self._num==9 then    --斧头
        app.musicSound:playSound("laohuji_tiefu")
    end
end

return ShzObject
