Clock = class("Clock", function()
    return ccui.Widget:new()
end)

Clock._uiFrame = nil      --图片


function Clock:ctor( owner, posX, posY )                             
    --闹钟动画          
    ccs.ArmatureDataManager:getInstance():addArmatureFileInfo("GameRes/naozhong/naozhong.ExportJson")            
    local t_coinAnimation = ccs.Armature:create( "naozhong" )                            
    self:addChild(t_coinAnimation)
--    t_coinAnimation:getAnimation():play("naozhong")
    t_coinAnimation:setPosition(posX, posY)
    t_coinAnimation:setTag(1000000000)    

    self:setVisible(false)
end

function Clock:playClock()
    app.musicSound:playSound("naoZhong")
    local t_coinAnimation = self:getChildByTag(1000000000)
    t_coinAnimation:getAnimation():play("naozhong")
    self:setVisible(true)
    
    local function moveEnd()
        self:setVisible(false)
    end
    local callfunc = cc.CallFunc:create(moveEnd)
    self:runAction(cc.Sequence:create(cc.MoveBy:create(5, cc.p(0, 0)),  callfunc))
end

function Clock:closeClock()
	self:stopAllActions()
	self:setVisible(false)
end

return Clock