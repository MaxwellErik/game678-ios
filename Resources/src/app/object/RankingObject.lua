RankingObject = class("RankingObject", function ()
    return ccui.Widget:create()
end)

RankingObject._layerInventory = nil
RankingObject._rankIcon = nil   --排名等级
RankingObject._coin = nil       --金币数量
RankingObject._headIcon = nil   --头像
RankingObject._name = nil       --昵称
RankingObject._list = nil       --玩过的游戏


--排名等级，金币数量，头像，玩过的游戏
function RankingObject:ctor(rankIcon, coin,headIcon,name,list )
    print("进来了")
    local layerInventory = ccs.GUIReader:getInstance():widgetFromJsonFile("HallUI/RankingObject.json")
    --    self._uiLayer:addChild(layerInventory)
    self:addChild(layerInventory)
    self._layerInventory = layerInventory
    self:setContentSize(layerInventory:getContentSize())

    self._rankIcon = layerInventory:getChildByName("rankIcon")
    self._coin = layerInventory:getChildByName("coin")

    local shuzichang=0
    if string.len(coin)>=16 then
        coin=string.sub(coin,0,string.len(coin)-8)
        shuzichang=1
    else
        if string.len(coin)>=12 then
            coin=string.sub(coin,0,string.len(coin)-4)
            shuzichang=2
        end
    end
    self._coin:setString(coin)

    if shuzichang==1 then
        self._coin:getChildByName("qian"):setVisible(true)
        self._coin:getChildByName("qian"):setPosition(cc.p(string.len(coin)*18,14))
    elseif shuzichang==2 then
        self._coin:getChildByName("wan"):setVisible(true)
        self._coin:getChildByName("wan"):setPosition(cc.p(string.len(coin)*18,14))
    end



    self._name = layerInventory:getChildByName("name")
    self._name:setString(name)
    self._headIcon = layerInventory:getChildByName("headIcon")
    self._list=layerInventory:getChildByName("list")
    local headIconNum=0
    
    --设置头像
    if name==app.hallLogic._loginSuccessInfo["szNickName"] then
        headIconNum=cc.UserDefault:getInstance():getIntegerForKey("headIcon")
    else
        headIconNum=math.random(1,30)
    end  
    if headIconNum<=15 then
        self._headIcon:loadTexture("headIcon/y"..headIconNum..".png")
    else
        self._headIcon:loadTexture("headIcon/yy"..(headIconNum-15)..".png")
    end
    
    

    if rankIcon==1 then
        self._rankIcon:loadTexture("HallUI/Ranking/jiangbei1.png")
    elseif rankIcon==2 then
        self._rankIcon:loadTexture("HallUI/Ranking/jiangbei2.png")
    elseif rankIcon==3 then
        self._rankIcon:loadTexture("HallUI/Ranking/jiangbei3.png")
    else
        self._rankIcon:setVisible(false)
    end

    for index, item in ipairs(list) do
        local gameicon = string.format("HallUI/Ranking/rankgame_%d.png", item)
        local icon = cc.Sprite:create(gameicon)
        if icon ~= nil then
            local wicon=ccui.Widget:new()
            icon:setAnchorPoint(0,0)
            wicon:addChild(icon)
            wicon:setContentSize(icon:getContentSize())
            self._list:insertCustomItem(wicon,0)
        end
    end
end

return RankingObject