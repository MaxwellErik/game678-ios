
GameXml = class("GameXml",function()
    return display:newScene(sceneName)
end)

GameXml._jinbi=nil                  --金币
GameXml.IMG={}
GameXml.IMG[0]     =       9      --斧头
GameXml.IMG[1]     =       8      --银枪
GameXml.IMG[2]     =       7      --大刀
GameXml.IMG[3]     =       6      --鲁智深
GameXml.IMG[4]     =       5      --林冲
GameXml.IMG[5]     =       4      --松江
GameXml.IMG[6]     =       3      --替天行道
GameXml.IMG[7]     =       2      --忠义堂
GameXml.IMG[8]     =       1      --水浒传
GameXml.IMG[9]     =       10      --松江
GameXml.IMG[10]     =      11      --替天行道
GameXml.IMG[11]     =      12      --忠义堂
GameXml.IMG[12]     =      13      --水浒传

GameXml.randImgNum={}       --四张图片Num       --这里的是测试数据
GameXml._pingguoEndNum=10   --苹果机结束数字
GameXml._times=0           --总共能玩的次数


GameXml.randImg=nil         --本次的四张图片
GameXml._pingguoNum=1       --苹果机当前数字
GameXml._pingguoStarNum=1   --苹果机开始数字
GameXml._pointEndNum=0      --苹果机结束数字转换成的图标数字0-7和以前一样8是exit
GameXml._pingguoQuan=0      --苹果机当前圈数
GameXml._loop={}            --苹果机所有的图片
GameXml._repeatLoop=nil     --苹果机运动动画
GameXml._isLoop=true        --苹果机开始运动
GameXml._shanShuoLoop=nil   --闪烁动画
GameXml._donghuaPanel=nil   --动画层
GameXml._donghuaNum={}      --存放动画
GameXml._donghua34Lian=nil  --存放动画
GameXml._donghua34Lian1=nil --存放动画
GameXml._donghua34Lian2=nil --存放动画
GameXml._zhongJiang =false  --中没中奖
GameXml._timesText=nil      --记录时间的文字   

GameXml.gameShz=nil         --游戏水浒传
GameXml._oldDenFen=0        --之前的得分


GameXml._beishuPanel=nil            --倍数闪光

function GameXml:ctor(shz)
    self._uiLayer = cc.Layer:create()
    self:addChild(self._uiLayer)

    local layerInventory = ccs.GUIReader:getInstance():widgetFromJsonFile("GameShz/ShzXiaoMaLi.json")
    self._uiLayer:addChild(layerInventory)
    self._layerInventory=layerInventory
    self._loopPanel=layerInventory:getChildByName("gundong")
    self:setLoopPanel()  --持续滚动
    self.randImg=layerInventory:getChildByName("randImg")
    
    self.gameShz=shz    --游戏1本体 
    self._times=self.gameShz._xiaoMaLiTimes --获取小玛丽次数
    

    self._beishuPanel=self._layerInventory:getChildByName("bg"):getChildByName("beishuPanel")            --倍数闪光

    --获取水果机的24个图片
    for i=1, 24 do
        self._loop[i]=layerInventory:getChildByName("loop"):getChildByName("img"..i)
    end
    --创建四个动画
    self._donghuaPanel=layerInventory:getChildByName("donghuaPanel")
    for i=1, 4 do
        local obj=ShzObject.new(1)
        self._donghuaPanel:addChild(obj)
        self._donghuaNum[i]=obj
        local objx=113
        local objy=82
        if i==1 then
            obj:setPosition(320-objx,242-objy)
        elseif i==2 then
            obj:setPosition(535-objx,242-objy)
        elseif i==3 then
            obj:setPosition(748-objx,242-objy)
        elseif i==4 then
            obj:setPosition(961-objx,242-objy)
        end
        obj:setVisible(false)
    end
    --3连4连图片
    self._center500=self._layerInventory:getChildByName("bg"):getChildByName("center500")
    self._left20=self._layerInventory:getChildByName("bg"):getChildByName("left20")
    self._right20=self._layerInventory:getChildByName("bg"):getChildByName("right20")
    self._center500:setVisible(false)   
    self._left20:setVisible(false)     
    self._right20:setVisible(false)   
     
    self._timesText=self._layerInventory:getChildByName("bg"):getChildByName("times")     --时间记录文字
    self._timesText:setString(self._times)  --设置可玩的数字
    
    --初始化桌面的4张选项图
    for i = 1, 4 do
        self.randImg:getChildByName("img"..i):loadTexture("GameShz/result_"..self.IMG[math.random(0,7)] ..".png")
    end
    
    
    --设置得分金币    
    self._coin=self._layerInventory:getChildByName("bg"):getChildByName("coin")                         --玩家金币
    self.gameShz._zongCoin=self.gameShz._zongCoin+self.gameShz._game1Defen-self.gameShz._userDenFen     --玩家的得分
    self._oldDenFen=self.gameShz._game1Defen
    self._coin:setString(self.gameShz._zongCoin)
    self._zongDeFen=self._layerInventory:getChildByName("bg"):getChildByName("zongDeFen")               --赢得的总分
    self._zongDeFen:setString(self.gameShz._game1Defen )
    self._defen=self._layerInventory:getChildByName("bg"):getChildByName("defen")                       --押注的得分
    self._defen:setString(self.gameShz._userDenFen)
    
  
    local function unReversal1()
        --开始游戏
        self.gameShz._shzLogic:sendGame3Start()  
    end
    local callfunc1 = cc.CallFunc:create(unReversal1)
    self._layerInventory:runAction(cc.Sequence:create(cc.MoveBy:create(2,cc.p(0,0)),    --等待两秒开始游戏
    callfunc1))


    --测试按钮
    local function testAccount(sender, eventType)
        if eventType == ccui.TouchEventType.ended then
            app.musicSound:playSound(SOUND_HALL_TOUCH)
            self:start()    --开始游戏
        end
    end
    local testBtn=layerInventory:getChildByName("testBtn")
    testBtn:addTouchEventListener(testAccount)
    testBtn:setVisible(false)

end


--小玛丽倍数闪光
function GameXml:beiShuShanGuang(num)
    self._beishuPanel:getChildByName("img"..num):setVisible(true)     
end

--刷新得分
function GameXml:initDefen()  
    if self._oldDenFen~=self.gameShz._game1Defen then --判断小玛丽有没有赢取得分
        self.gameShz._zongCoin=self.gameShz._zongCoin+self.gameShz._game1Defen-self._oldDenFen  
        self._oldDenFen=self.gameShz._game1Defen       
    end   
    self._coin:setString(self.gameShz._zongCoin)
    self._zongDeFen:setString(self.gameShz._game1Defen)
end

--设置四张图片Num
function GameXml:setImgs()
    self.randImg:getChildByName("img1"):loadTexture("GameShz/result_"..self.IMG[self.randImgNum[1]] ..".png")
    self.randImg:getChildByName("img2"):loadTexture("GameShz/result_"..self.IMG[self.randImgNum[2]] ..".png")
    self.randImg:getChildByName("img3"):loadTexture("GameShz/result_"..self.IMG[self.randImgNum[3]] ..".png")
    self.randImg:getChildByName("img4"):loadTexture("GameShz/result_"..self.IMG[self.randImgNum[4]] ..".png")
end

--开始游戏
function GameXml:start()
    --苹果机部分-----------------------------------------------------
    self:pingGuoJi()

    --老虎机部分-----------------------------------------------------
    self:laoHuJi()

end

--拿到结果
function GameXml:getJieGuo(endNum,imgNum)
    self._pingguoEndNum=endNum        --math.random(1,24)      --苹果机结束数字
    self:hunXiaoJieGuo()    --把结果混淆
    self.randImgNum[1]=imgNum[1]--math.random(0,7)
    self.randImgNum[2]=imgNum[2]--math.random(0,7)
    self.randImgNum[3]=imgNum[3]--math.random(0,7)
    self.randImgNum[4]=imgNum[4]--math.random(0,7)
    
    self:start()    --开始游戏
end

--苹果机初始化
function GameXml:gameInit()
    --初始化
    self._center500:setVisible(false)   
    self._left20:setVisible(false)    
    self._right20:setVisible(false)   
    for i=1, 4 do
        self._donghuaNum[i]:setVisible(false)
    end
    self._loop[self._pingguoNum]:stopAction(self._shanShuoLoop) --停止闪烁动画
    self:stopAction(self._repeatLoop)       --停止以前的动画
    self._isLoop=true   
    self._pingguoQuan=0         --苹果机当前圈数
    self._pingguoStarNum=self._pingguoNum   --设置当前的坐标位置
    self._zhongJiang=false --中没中奖

end

--苹果机部分
function GameXml:pingGuoJi()
    self:gameInit() --游戏初始化
    local dengdai=0.02          --跳到下一个的时间
    local moveTime=0            --时间记录
    ---------------------------------
    if self._pingguoEndNum<=5 then--当结束数字在起始数字5个以内时
        if (self._pingguoEndNum+24)-self._pingguoStarNum<=5 then        --当结束位置比起始位置大5的时候
            self._pingguoStarNum=self._pingguoEndNum-5+24
    elseif self._pingguoEndNum-self._pingguoStarNum<=5 and self._pingguoEndNum<=5 then  --结束位置和开始位置都小于5的时候
        self._pingguoStarNum=self._pingguoEndNum-5+24
    end
    else
        if self._pingguoEndNum-self._pingguoStarNum<=5 then        --当结束位置比起始位置大5的时候
            self._pingguoStarNum=self._pingguoEndNum-5
        end
    end
    print("_pingguoEndNum"..self._pingguoEndNum.."self._pingguoStarNum"..self._pingguoStarNum)

    local function unReversal1()
        moveTime=moveTime+0.01
        if moveTime<dengdai then    --是否进行下面操作
            return
        end
        moveTime=0
        if self._isLoop==false then
            return
        end
        self._loop[self._pingguoNum]:setVisible(false)  --隐藏前一个
        self._pingguoNum=self._pingguoNum+1     --苹果机当前数字
        if self._pingguoNum>24 then
            self._pingguoNum=1
        end
        self._loop[self._pingguoNum]:setVisible(true)   --显示后一个

        if self._pingguoNum==self._pingguoStarNum then  --当过了一圈过后
            self._pingguoQuan= self._pingguoQuan+1
        end
       -- print("self._pingguoQuan:"..self._pingguoQuan.."self.pingguoNum:"..self._pingguoNum)

        --减速
        local dengdaiT1=0.5        --停止的时间
        local dengdaiT2=0.3
        local dengdaiT3=0.2
        local dengdaiT4=0.1
        local dengdaiT5=0.05

        if self._pingguoQuan>=3 then   --到指定位置的时候
            for i=1,5 do        --最后5个的减速
                if self._pingguoEndNum-i<=0 then    --当结束坐标小于5的时候
                    if (self._pingguoEndNum+24)-i==self._pingguoNum then
                        if i==5 then
                            dengdai=dengdaiT5
                        elseif i==4 then
                            dengdai=dengdaiT4
                        elseif i==3 then
                            dengdai=dengdaiT3
                        elseif i==2 then
                            dengdai=dengdaiT2
                        elseif i==1 then
                            dengdai=dengdaiT1
                        end
                end
            else   --结束坐标大于5的时候
                if self._pingguoEndNum-i==self._pingguoNum then
                    if i==5 then
                        dengdai=dengdaiT5
                    elseif i==4 then
                        dengdai=dengdaiT4
                    elseif i==3 then
                        dengdai=dengdaiT3
                    elseif i==2 then
                        dengdai=dengdaiT2
                    elseif i==1 then
                        dengdai=dengdaiT1
                    end
            end
            end
        end


        --停止
        if self._pingguoNum == self._pingguoEndNum then
            print("停止了")
            self._isLoop=false      --停止运动
            self:endGame()  --结束判断
        end
        end

    end
    local callfunc1 = cc.CallFunc:create(unReversal1)

    self._repeatLoop=cc.RepeatForever:create(cc.Sequence:create(callfunc1,
        cc.MoveBy:create(0.01,cc.p(0,0))     --等待多久到下一个
    ))
    self:runAction(self._repeatLoop)
end

--3,4连闪光
function GameXml:lianXu34()
    --判断老虎机3连4连 self._donghua34Lian
    if self.randImgNum[1]==self.randImgNum[2] and self.randImgNum[2]==self.randImgNum[3] and self.randImgNum[3]==self.randImgNum[4] then
        self._center500:setVisible(true)
        for i=1,4 do
            if self._pointEndNum~=8 then   --8是退出
                self._donghuaNum[i]:setNum(self.IMG[self.randImgNum[i]]) --设置播放的动画
                self._donghuaNum[i]:setVisible(true)
                self._zhongJiang=true 
            end

        end     
    end

    if self.randImgNum[1]==self.randImgNum[2] and self.randImgNum[2]==self.randImgNum[3] and self.randImgNum[3]~=self.randImgNum[4] then
        self._left20:setVisible(true)
        for i=1,3 do
            if self._pointEndNum~=8 then   --8是退出
                self._donghuaNum[i]:setNum(self.IMG[self.randImgNum[i]]) --设置播放的动画
                self._donghuaNum[i]:setVisible(true)
                self._zhongJiang=true 
            end

        end  
    end

    if self.randImgNum[1]~=self.randImgNum[2] and self.randImgNum[2]==self.randImgNum[3] and self.randImgNum[3]==self.randImgNum[4] then
        self._right20:setVisible(true)
        for i=2,4 do
            if self._pointEndNum~=8 then   --8是退出
                self._donghuaNum[i]:setNum(self.IMG[self.randImgNum[i]]) --设置播放的动画
                self._donghuaNum[i]:setVisible(true)
                self._zhongJiang=true 
            end

        end  
    end
end

--结束判断
function GameXml:endGame()
    self._shanShuoLoop=cc.RepeatForever:create(cc.Blink:create(0.5,1))    --闪烁动画
    self._pointEndNum=self:getPointNum()    --拿到最后的点对应的图标   

    for i=1, 4 do   --判断中没中奖
        if self._pointEndNum==self.randImgNum[i] and self._pointEndNum~=8 then  --中奖且 不等exit
            self._zhongJiang=true 
            --print("动画进来了:i"..i.."self._pointEndNum:"..self._pointEndNum)
            self._donghuaNum[i]:setNum(self.IMG[self._pointEndNum]) --设置播放的动画
            self._donghuaNum[i]:setVisible(true)
            self:beiShuShanGuang(self._pointEndNum)     --倍数高亮显示
        end
    end  
    
    self:lianXu34() --34连判断
    
    local isTuichu=false --是否退出
    if self._pointEndNum==8 then    --等于退出的时候
        self._zhongJiang=false
        self._times=self._times-1
        if self._times==0 then
        	isTuichu=true
        end
    end
   
    local function unReversal()
        if isTuichu ==false then    --可以继续游戏
            self:initDefen()        --刷新得分
            self._timesText:setString(self._times)  --设置可玩的数字
            if self._pointEndNum~=8 then
                self._beishuPanel:getChildByName("img"..self._pointEndNum):setVisible(false)     --倍数高亮关闭
            end
            
            self.gameShz._shzLogic:sendGame3Start()      --开始游戏
        else        --退出游戏
            self.gameShz:qiziPlay()     --播放旗子动画
            self:removeFromParent()
            self.gameShz:tuichuXML()    --调用游戏1的退出           
        end
        
    end
    local callfunc = cc.CallFunc:create(unReversal)
    if self._zhongJiang==true then  --中奖过后
        for i=1, 4 do
            self._donghuaNum[i]:playDonghua()
        end
    self._loop[self._pingguoNum]:runAction(self._shanShuoLoop)


    self:runAction(cc.Sequence:create(cc.MoveBy:create(2.5,cc.p(0,0)),    --等待3秒后开始游戏
        callfunc))
    else    --没有中奖
        self:runAction(cc.Sequence:create(cc.MoveBy:create(1,cc.p(0,0)),    --等待1秒后开始游戏
            callfunc))
       
    end


end

--得到得分的num
function GameXml:getPointNum()
    local num=8
    --斧头
    if self._pingguoEndNum==5 or self._pingguoEndNum==11 or  self._pingguoEndNum==16 or  self._pingguoEndNum==22 then
        num=0
    end
    --枪
    if self._pingguoEndNum==3 or self._pingguoEndNum==10 or  self._pingguoEndNum==21 then
        num=1
    end
    --刀
    if self._pingguoEndNum==6 or self._pingguoEndNum==15 or  self._pingguoEndNum==23 then
        num=2
    end
    --鲁智深
    if self._pingguoEndNum==2 or self._pingguoEndNum==9 or  self._pingguoEndNum==17 then
        num=3
    end
    --林冲
    if self._pingguoEndNum==14 or self._pingguoEndNum==20 then
        num=4
    end
    --宋江
    if self._pingguoEndNum==8 or self._pingguoEndNum==18 then
        num=5
    end
    --替天行道
    if self._pingguoEndNum==12 or self._pingguoEndNum==24 then
        num=6
    end
    --忠义堂
    if self._pingguoEndNum==4 then
        num=7
    end
    --EXIT
    if self._pingguoEndNum==1 or self._pingguoEndNum==7 or self._pingguoEndNum==13 or self._pingguoEndNum==19 then
        num=8
    end

    return num
end


--把结果转化到轮盘上
function GameXml:hunXiaoJieGuo()
    print("游戏结果转化前"..self._pingguoEndNum)
    if self._pingguoEndNum == 0 then        --斧头
	   local num={}
	   num[1]=5
       num[2]=11
       num[3]=16
       num[4]=22
        self._pingguoEndNum= num[math.random(1,4)]
    elseif self._pingguoEndNum == 1 then --银枪
        local num={}
        num[1]=3
        num[2]=10
        num[3]=21
        self._pingguoEndNum= num[math.random(1,3)]    
    elseif self._pingguoEndNum == 2 then        --大刀
        local num={}
        num[1]=6
        num[2]=15
        num[3]=23
        self._pingguoEndNum= num[math.random(1,3)]
    elseif self._pingguoEndNum == 3 then    --鲁智深
        local num={}
        num[1]=2
        num[2]=9
        num[3]=17
        self._pingguoEndNum= num[math.random(1,3)]    
    elseif self._pingguoEndNum == 4 then    --林冲
        local num={}
        num[1]=14
        num[2]=20
        self._pingguoEndNum= num[math.random(1,2)]
    elseif self._pingguoEndNum == 5 then    --松江
        local num={}
        num[1]=8
        num[2]=18
        self._pingguoEndNum= num[math.random(1,2)]
    elseif self._pingguoEndNum == 6 then    --替天行道
        local num={}
        num[1]=12
        num[2]=24
        self._pingguoEndNum= num[math.random(1,2)]
    elseif self._pingguoEndNum == 7 then    --忠义堂
        local num={}
        num[1]=4
        self._pingguoEndNum= num[1]
    elseif self._pingguoEndNum == 8 then    --退出
        local num={}
        num[1]=1
        num[2]=7
        num[3]=13
        num[4]=19
        self._pingguoEndNum= num[math.random(1,4)]      
    end

    print("游戏结果转化值"..self._pingguoEndNum)
    
end

--老虎机部分
function GameXml:laoHuJi()
    local function unReversal1()--4张的图片依次停止
        self.randImg:setPosition(cc.p(0,0))
        for i=1, 4 do
            self.randImg:getChildByName("img"..i):runAction(cc.Sequence:create(cc.MoveBy:create(0,cc.p(0,200)),
                cc.MoveBy:create(0.13*i,cc.p(0,-200)),
                cc.MoveBy:create(0.07,cc.p(0,30)),
                cc.MoveBy:create(0.05,cc.p(0,-30)),   --回弹
                cc.MoveBy:create(0.07,cc.p(0,10)),
                cc.MoveBy:create(0.05,cc.p(0,-10))   --回弹2
            ))
        end


    end
    local function unReversal()
        self:setImgs()  --设置图片
    end

    local callfunc1 = cc.CallFunc:create(unReversal1)
    local callfunc = cc.CallFunc:create(unReversal)
    self.randImg:runAction(cc.Sequence:create(cc.MoveBy:create(0.1,cc.p(0,-200)),
        callfunc,   --设置图片
        cc.MoveBy:create(2,cc.p(0,0)),  --老虎机距离结束等待时间
        callfunc1   --播放结束动画
    ))
end

--老虎机背景一直旋转
function GameXml:setLoopPanel()
    local function unReversal1()
        self._loopPanel:setPosition(cc.p(0,0))
    end

    local callfunc1 = cc.CallFunc:create(unReversal1)
    self._loopPanel:runAction( cc.RepeatForever:create(cc.Sequence:create(cc.MoveBy:create(0.4,cc.p(0,-1402)),callfunc1)))
end


return GameXml
