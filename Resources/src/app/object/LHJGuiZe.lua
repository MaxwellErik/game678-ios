LHJGuiZe = class("LHJGuiZe", function ()
    return display.newLayer()
end)

LHJGuiZe._uiLayer=nil


--1，要添加的对象，2文字，3持续时间
function LHJGuiZe:ctor()
    self._uiLayer = cc.Layer:create()
    self:addChild(self._uiLayer)
    local layerInventory = ccs.GUIReader:getInstance():widgetFromJsonFile("GameLHJ/LHJGuiZe.json")
    self._uiLayer:addChild(layerInventory)

    --返回按钮事件
    local function returnBtnAccount(sender, eventType)
        if eventType == ccui.TouchEventType.ended then
            app.musicSound:playSound(SOUND_HALL_TOUCH)                   
            local function unReversal1()
                self:removeFromParent()
            end
            local callfunc1 = cc.CallFunc:create(unReversal1)
            self:runAction(cc.Sequence:create(cc.MoveBy:create(0.3,cc.p(0,720)),callfunc1))
            
        end
    end
    --返回按钮
    local returnBtn = layerInventory:getChildByName("closeBtn")
    returnBtn:addTouchEventListener(returnBtnAccount)
    
    --wanfaImg玩法   fanjiangImg返奖
    local wanfaImg = layerInventory:getChildByName("wanfaImg")
    local fanjiangImg = layerInventory:getChildByName("fanjiangImg")
    --玩法按钮事件
    local function wanfaBtnAccount(sender, eventType)
        if eventType == ccui.TouchEventType.ended then
            app.musicSound:playSound(SOUND_HALL_TOUCH)                   
            wanfaImg:setPositionY(311)
            fanjiangImg:setPositionY(1000)
        end
    end
    --玩法按钮
    local wanfaBtn = layerInventory:getChildByName("wanfaBtn")
    wanfaBtn:addTouchEventListener(wanfaBtnAccount)
    
    --返奖按钮事件
    local function fanjiangBtnAccount(sender, eventType)
        if eventType == ccui.TouchEventType.ended then
            app.musicSound:playSound(SOUND_HALL_TOUCH)                   
            wanfaImg:setPositionY(1000)
            fanjiangImg:setPositionY(291)
        end
    end
    --返奖按钮
    local fanjiangBtn = layerInventory:getChildByName("fanjiangBtn")
    fanjiangBtn:addTouchEventListener(fanjiangBtnAccount)


end

return LHJGuiZe