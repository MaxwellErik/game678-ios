HeGuan = class("HeGuan", function()
    return ccui.Widget:new()
end)

HeGuan._uiFrame = nil      --图片
HeGuan._animation = nil     --动画

function HeGuan:ctor( owner, posX, posY )             
    --荷官动画
    ccs.ArmatureDataManager:getInstance():addArmatureFileInfo( "HeGuan/heguandonghua/heguandonghua0.png", 
        "HeGuan/heguandonghua/heguandonghua0.plist" , 
        "HeGuan/heguandonghua/heguandonghua.ExportJson" )  
    self._animation = ccs.Armature:create( "heguandonghua" )   
    self:addChild(self._animation) 
    self._animation:setPosition(posX, posY)
    
    self:setVisible(false)
    self._owner = owner
end

function HeGuan:wait()
    self:setVisible(true)
    self._owner._mengBan:setVisible(true)
    self._animation:getAnimation():stop()
    self._animation:getAnimation():play("fapaidengdai") 
end

function HeGuan:faPai()
    self._animation:getAnimation():stop()
    self._animation:getAnimation():play("fapai")
end

function HeGuan:faPaiEnd()    
    self._animation:getAnimation():stop()
    self._animation:getAnimation():play("fapaijieshu")
    
    local function moveEnd()
        self:setVisible(false)
        self._owner._mengBan:setVisible(false)
    end
    local callfunc = cc.CallFunc:create(moveEnd)
    self:runAction(cc.Sequence:create(cc.MoveBy:create(1.2, cc.p(0, 0)),  callfunc))
end

return HeGuan