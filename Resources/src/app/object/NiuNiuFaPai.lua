NiuNiuFaPai = class("NiuNiuFaPai", function()
    return ccui.Widget:new()
end)

NiuNiuFaPai._uiFrame = nil      --图片
NiuNiuFaPai._animation = nil     --动画

function NiuNiuFaPai:ctor( owner )             
    app.musicSound:addSound("GAIZHANG", "HallSound/NIUNIU_GAIZHANG.mp3")
    app.musicSound:addSound("FANPAI", "HallSound/NIUNIU_FANPAI.mp3")
    
    ccs.ArmatureDataManager:getInstance():addArmatureFileInfo( "HeGuan/niuniufanpaidonghua/fanpai0.png", 
        "HeGuan/niuniufanpaidonghua/fanpai0.plist" , 
        "HeGuan/niuniufanpaidonghua/fanpai.ExportJson" )  
    self._animation = ccs.Armature:create( "fanpai" )   
    self:addChild(self._animation) 
    
    self:setVisible(false)
    
    self._owner = owner
end

--发牌器的动画
function NiuNiuFaPai:faPaiQiAnimation( posX, posY )
    self:setVisible(true)
    self._animation:setPosition(posX, posY)
    self._animation:getAnimation():stop()
    self._animation:getAnimation():play("fapaidonghua1") 
end

--发牌的前四张  参数1，2 坐标    参数3  点数； 参数4  花色
function NiuNiuFaPai:faPaiQianSi( posX, posY, dian, hua )
    self:setVisible(true)
    self._animation:setPosition(posX, posY)
    
    local t_str = ""        
    if dian>=14 then
        t_str = "poker/".."w"..dian..".png"
    else
        t_str = "poker/"..hua..dian..".png"
    end
    local skin = ccs.Skin:create(t_str)

    self._animation:getBone("niuniufanpaipuke1"):addDisplay(skin, 0)
    
    self._animation:getBone("niuniufanpaipuke1"):changeDisplayWithIndex(0, true)

    self._animation:getAnimation():stop()
    self._animation:getAnimation():play("fapaidonghua2")
end

--发牌第五章   参数5  暂停多少秒才开始播放 ;参数6 牛几；参数7，8   牛几图标的坐标;参数9 是否胜利 用来抖动屏幕
function NiuNiuFaPai:faPaiDiWuZhang( posX, posY, dian, hua, zantingTime, num, niuPosX, niuPosY, isWin )    
    self:setVisible(true)
    self._animation:setPosition(posX, posY)
    

    local t_str = ""
    local t_str2 = ""        
    if dian>=14 then
        t_str = "fanpaidonghuaxiaoshuzi/fanpaidonghuashuzi".."w"..dian..".png"
        t_str2 = "poker/".."w"..dian..".png"
    else
        t_str = "fanpaidonghuaxiaoshuzi/fanpaidonghuashuzi"..hua..dian..".png"
        t_str2 = "poker/"..hua..dian..".png"
    end
    local skin = ccs.Skin:create(t_str)
    local skin2 = ccs.Skin:create(t_str2)

    self._animation:getBone("niuniufanpaishuzi"):addDisplay(skin, 0)
    self._animation:getBone("niuniufanpaipuke0"):addDisplay(skin2, 0)

    self._animation:getBone("niuniufanpaishuzi"):changeDisplayWithIndex(0, true)
    self._animation:getBone("niuniufanpaipuke0"):changeDisplayWithIndex(0, true)
    
    self._animation:getAnimation():stop()
    self._animation:getAnimation():play("fapaidonghua3")
    self._animation:getAnimation():gotoAndPause(0)
    
    local function zanTingEnd()
        app.musicSound:playSound("FANPAI")
        self._animation:getAnimation():play("fapaidonghua3")
    end
    local callfunc = cc.CallFunc:create(zanTingEnd)
    
    ccs.ArmatureDataManager:getInstance():addArmatureFileInfo( "GameBrnn/niuniuyinzhang/niuniuyinzhang0.png",
        "GameBrnn/niuniuyinzhang/niuniuyinzhang0.plist",
        "GameBrnn/niuniuyinzhang/niuniuyinzhang.ExportJson" )  
        
    local t_niuAnimation = ccs.Armature:create( "niuniuyinzhang" ) 
    self:addChild(t_niuAnimation)
    t_niuAnimation:setPosition(niuPosX, niuPosY)
    t_niuAnimation:setVisible(false)
    t_niuAnimation:setScale(0.7)
    
    --替换图片
    local t_niuStr = ""  
    local defen = num

    if defen>=3 and defen<=11 then
        local shu=defen-2
        t_niuStr = "GameBrnn/".."niu"..shu..".png"        
    elseif defen == 1 then
        t_niuStr = "GameBrnn/".."niu0"..".png"
    elseif defen>=12 and defen<=14 then
        t_niuStr = "GameBrnn/".."niuniu"..".png"
    elseif defen==15 then
        t_niuStr = "GameBrnn/".."yinNiu"..".png"
    elseif defen==16 then
        t_niuStr = "GameBrnn/".."jinNiu"..".png"
    elseif defen==17 then
        t_niuStr = "GameBrnn/".."zhaDan"..".png"
    end   

    local skinNiu = ccs.Skin:create(t_niuStr)
    t_niuAnimation:getBone("niuniuyinzhang0"):addDisplay(skinNiu, 0)
    t_niuAnimation:getBone("niuniuyinzhang0"):changeDisplayWithIndex(0, true)
    
    local function fanPaiEnd()
        t_niuAnimation:setVisible(true)
        t_niuAnimation:getAnimation():play("niuniuyinzhangdonghua0")
        
        if isWin then
            self._owner._layerInventory:runAction(cc.Sequence:create(cc.MoveBy:create(0.05, cc.p(0, 10)),
                                                                     cc.MoveBy:create(0.05, cc.p(0, -20)),
                                                                     cc.MoveBy:create(0.05, cc.p(0, 20)),
                                                                     cc.MoveBy:create(0.05, cc.p(0, -20)),
                                                                     cc.MoveBy:create(0.05, cc.p(0, 20)),
                                                                     cc.MoveBy:create(0.05, cc.p(0, -20)),
                                                                     cc.MoveBy:create(0.05, cc.p(0, 20)),
                                                                     cc.MoveBy:create(0.05, cc.p(0, -20)),
                                                                     cc.MoveBy:create(0.05, cc.p(0, 20)),
                                                                     cc.MoveBy:create(0.05, cc.p(0, -20)),
                                                                     cc.MoveBy:create(0.05, cc.p(0, 20)),
                                                                     cc.MoveBy:create(0.05, cc.p(0, -10))))
        end
    end
    local callfunc2 = cc.CallFunc:create(fanPaiEnd)
    
    local function fanPaiEndSound()
        app.musicSound:playSound("GAIZHANG")
    end
    local callfunc3 = cc.CallFunc:create(fanPaiEndSound)
    self:runAction(cc.Sequence:create(cc.MoveBy:create(zantingTime, cc.p(0, 0)),  callfunc, 
                                      cc.MoveBy:create(1.5, cc.p(0, 0)), callfunc2, 
                                      cc.MoveBy:create(0.25, cc.p(0, 0)), callfunc3))            
        
end

return NiuNiuFaPai