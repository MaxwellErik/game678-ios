payRecordItem = class("payRecordItem", function ()
    return ccui.Widget:create()
end)

--payRecordItem._uiLayer = nil 
payRecordItem._time = nil   --購買時間
payRecordItem._RMB = nil    --消費金額
payRecordItem._coin = nil   --獲得的金幣
payRecordItem._layerInventory = nil

function payRecordItem:ctor( a_time, a_rmb, a_coin, num )
    if not num then
    --    self._uiLayer = cc.Layer:create()
    --    self:addChild(self._uiLayer)

        local layerInventory = ccs.GUIReader:getInstance():widgetFromJsonFile("HallUI/TimeItem.json")
    --    self._uiLayer:addChild(layerInventory)
        self:addChild(layerInventory)
        self._layerInventory = layerInventory
        
        self._time = layerInventory:getChildByName("time"):getChildByName("label")
        self._time:setString(a_time)
        
        self._RMB = layerInventory:getChildByName("RMB"):getChildByName("label")
        self._RMB:setString(a_rmb)
        
        self._coin = layerInventory:getChildByName("coin"):getChildByName("label")
        self._coin:setString(a_coin)
    elseif num == 1 then -- 赠送或获赠记录
        local layerInventory = ccs.GUIReader:getInstance():widgetFromJsonFile("HallUI/RecordItem.json")
        self:addChild(layerInventory)
        self._layerInventory = layerInventory
        
        self._time = layerInventory:getChildByName("time"):getChildByName("Label_2")
        self._time:setString(a_time)
        
        self._RMB = layerInventory:getChildByName("who"):getChildByName("Label_4")
        self._RMB:setString(a_rmb)
        
        self._coin = layerInventory:getChildByName("coin"):getChildByName("Label_6")
        self._coin:setString(a_coin)
    end
end


return payRecordItem