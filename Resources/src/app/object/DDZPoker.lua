
DDZPoker = class("DDZPoker", function()
    return display.newSprite()
end)

--DDZPoker._uiFrame=nil      --图片
--DDZPoker._isFront=nil      --正反(true正面，false反面)
--DDZPoker._color=nil        --花色("1"黑,""3"樱,2"红,"4"方,"0"王)
--DDZPoker._point=nil        --点数(1-13花色，14小王，15大王)
local XiaoWang=14
local DaWang=15
local myHuaSe={"GameDDZ/poker/spade.png","GameDDZ/poker/club.png","GameDDZ/poker/heart.png","GameDDZ/poker/diamond.png"} --todo


--参数一，正反，参数二，花色，参数三，点数
function DDZPoker:ctor(isFront,flowerColor,num,gameDDZ)
    self:setTexture("GameDDZ/poker/poker_sp.png")
    self._gameDDZ=gameDDZ   --游戏场景
    local layerInventory = ccs.GUIReader:getInstance():widgetFromJsonFile("GameDDZ/pokerObj.json")
    layerInventory:setPosition(8,12)
    self:addChild(layerInventory)
    self._layerInventory = layerInventory
    self._uiFrame=layerInventory:getChildByName("dianshu")
    self._bg=layerInventory:getChildByName("bg")
    self._huase=layerInventory:getChildByName("huase")
    self._yinying=layerInventory:getChildByName("yinying")
    self:setContent(isFront,flowerColor,num)
    self._isXuanZhong=false
    self._isTaiQi=false     --是否抬起
    self._isOther=true      --是不是庄家的3张牌
    self._myNum=0
    self._panduanNum=0      --3-13 A=14 2=15 w=16,17
    
    if num==1 then      --A
    	self._myNum=14
    	self._panduanNum=14
    elseif num==2 then  --2
        self._myNum=20
        self._panduanNum=20
    elseif num==14 then --小鬼
        self._myNum=30  
        self._panduanNum=30
    elseif num==15 then --大鬼
        self._myNum=40
        self._panduanNum=40
    else
        self._myNum=num
        self._panduanNum=num
    end

    local dianjiX=0
    local ismove=false
    
    if flowerColor==1 then
        self._myNum=self._myNum+0.4
    elseif flowerColor==2 then
        self._myNum=self._myNum+0.2
    elseif flowerColor==3 then
        self._myNum=self._myNum+0.3
    elseif flowerColor==4 then
        self._myNum=self._myNum+0.1
    end


    local function touchCallBack(sender, eventType)
        if eventType == ccui.TouchEventType.began then
            print("被点击了")
            if self._isOther==true then
                return
            end
            if self._isXuanZhong==false then
                self:xuanzhong()         --选中
            else
                self:fangxia()      --放下
            end
            
            if #self._gameDDZ._panduanPoker~=0 then  --当别人有出牌的时候
                local isChuPai=self._gameDDZ:comparePokers()       --判断能不能出牌
                self._gameDDZ._chupaiBtn:setTouchEnabled(isChuPai)
                self._gameDDZ._chupaiBtn:setBright(isChuPai)
            else                            --自己出牌的时候
                local isChuPai=self._gameDDZ:choiceXuanZe()       --判断能不能出牌
                self._gameDDZ._chupaiBtn:setTouchEnabled(isChuPai)
                self._gameDDZ._chupaiBtn:setBright(isChuPai)
            end

        end

    end
    layerInventory:addTouchEventListener(touchCallBack)




end

--删除自己
function DDZPoker:deleteSelf()
    --self._eventDispatcher:removeEventListener(self.listener);
    self:removeAllChildren()
end

--判断点击事件在不在精灵内
function DDZPoker:myContainsPoint(pp,ppp)
    local isNei=false
    local minX=ppp.x
    local minY=ppp.y
    local maxX=ppp.x+130
    local maxY=ppp.y+140
    if pp.x>minX and pp.x<maxX and pp.y>minY and pp.y<maxY then
        isNei=true
    end

    return isNei
end


--放下
function DDZPoker:fangxia()
    if self._isTaiQi==true then
        self:runAction(cc.MoveBy:create(0,(cc.p(0,-50))))
    end
    self._isTaiQi=false --是否抬起
    self._isXuanZhong=false
    self._gameDDZ._xuanzhongNum=self._gameDDZ._xuanzhongNum-1
    self:setYinYing(false)    --放下
end

--上抬
function DDZPoker:shangTai()
    for k,v in pairs(self._gameDDZ._myPoker) do
        if v._isXuanZhong==true then
            if v._isTaiQi==false then
                v._isTaiQi=true --是否抬起
                v:runAction(cc.MoveBy:create(0,(cc.p(0,50))))
            end
        end

    end
end

--选中
function DDZPoker:xuanzhong()
    if self._isXuanZhong==true then
        return
    end
    self._isXuanZhong=true
    self:setYinYing(true)
    self._gameDDZ._xuanzhongNum=self._gameDDZ._xuanzhongNum+1
end

--加阴影
function DDZPoker:setYinYing(isYinYing)
    self._yinying:setVisible(isYinYing)
end

--参数一，正反，参数二，花色，参数三，点数
function DDZPoker:setContent(isFront,flowerColor,num)
    self._isFront=isFront
    self._color=flowerColor
    self._point=num

    if self._point<=13 then
        if self._color<=2 then  --黑色
            self._uiFrame:loadTexture("GameDDZ/poker/black_num"..self._point..".png")
        elseif self._color>2 then   --红色
            self._uiFrame:loadTexture("GameDDZ/poker/red_num"..self._point..".png")
        end
        --设置花色
        self._uiFrame:setPosition(cc.p(27,120))
        self._huase:setVisible(true)
        self._huase:loadTexture(myHuaSe[self._color])
    elseif self._point==XiaoWang then
        self._uiFrame:loadTexture("GameDDZ/poker/joker_black.png")
        self._uiFrame:setPosition(cc.p(60,80))
        self._huase:setVisible(false)
    elseif self._point==DaWang then
        self._uiFrame:loadTexture("GameDDZ/poker/joker_red.png")
        self._uiFrame:setPosition(cc.p(60,80))
        self._huase:setVisible(false)
    end

    self:setZhengFan(isFront)   --设置正反

end

--设置正反
function DDZPoker:setZhengFan(isFront)
    self._isFront=isFront
    if isFront then --正反
        self._bg:loadTexture("GameDDZ/poker/poker_bk.png")
        self._uiFrame:setVisible(true)
        if self._point<=13 then
            self._huase:setVisible(true)
        end
    else
        self._bg:loadTexture("GameDDZ/poker/poker_lord.png")
        self._uiFrame:setVisible(false)
        self._huase:setVisible(false)
    end
end

--将扑克翻转 time翻转用的时间
function DDZPoker:reversal(time,flowerColor,num)
    if flowerColor~=nil then
        self._color=flowerColor
    end
    if num~=nil then
        self._point=num
    end

    local function unReversal()
        if self._isFront then
            self._uiFrame:loadTexture("GameDDZ/poker/poker_lord.png")
            self._isFront=false
        else
            if self._color<=2 then  --黑色
                self._uiFrame:loadTexture("GameDDZ/poker/black_num"..self._point..".png")
            elseif self._color>2 then   --红色
                self._uiFrame:loadTexture("GameDDZ/poker/red_num"..self._point..".png")
            end
            self._uiFrame:setFlippedX(true)
            self._isFront=true
        end
    end

    local callfunc = cc.CallFunc:create(unReversal)
    self._uiFrame:runAction(cc.Sequence:create(cc.RotateTo:create(time/2,0,90),callfunc,cc.RotateTo:create(time/2,0,180)))
end

return DDZPoker
