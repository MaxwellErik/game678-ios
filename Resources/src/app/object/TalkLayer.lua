TalkLayer = class("TalkLayer", function ()
    return display.newScene()
end)

TalkLayer._bg=nil       --移动的背景
TalkLayer._text=nil     --输入的文字
TalkLayer._list=nil     --列表
TalkLayer._listLenth=30 --最大记录条数

--1，要添加的对象，2文字，3持续时间
function TalkLayer:ctor()
    self._uiLayer = cc.Layer:create()
    self:addChild(self._uiLayer)
    self._bShow = false
    self._text = nil
    self.layerInventory = ccs.GUIReader:getInstance():widgetFromJsonFile("HallUI/Talk.json")
    self._uiLayer:addChild(self.layerInventory)
    
    self._bg=self.layerInventory:getChildByName("bg")
    self._list=self._bg:getChildByName("list")
    self._bg:setTouchEnabled(true)

    self._xiaoxinum = 0
    --表情
    self._biaoqingbg = self._bg:getChildByName("ScrollView_12")
    self._biaoqingbg:setVisible(false)
    self._biaoqingbg:setTouchEnabled(false)

    --聊天按钮
    self._bg:getChildByName("chatBtn"):addTouchEventListener(function(sender, type)
        if type == ccui.TouchEventType.ended then
            self._biaoqingbg:setVisible(false) 
            self._biaoqingbg:setTouchEnabled(false)
        end
    end)

    --表情按钮
    local faceBtn = self._bg:getChildByName("biaoqingbtn") 
    faceBtn:addTouchEventListener(function(sender, type)
        if type == ccui.TouchEventType.ended then
            self._biaoqingbg:setVisible(true) 
            self._biaoqingbg:setTouchEnabled(true)
        end
    end)

    faceBtn:setVisible(false)

    local strname = ""
    for i = 0, 23 do
        strname = tostring(i)
        if i < 10 then
            strname = tostring("0"..i)
        end
        self._biaoqingbg:getChildByName(strname):setTag(tonumber(strname))
        self._biaoqingbg:getChildByName(strname):setScale(1)
        self._biaoqingbg:getChildByName(strname):addTouchEventListener(function(sender, type)
            if type == ccui.TouchEventType.ended then
                print("strname == ",sender:getTag())
                strname = "$$"..sender:getTag().."$$"
                app.table:sendChatMessage(strname)
                self._biaoqingbg:setVisible(false) 
                self._biaoqingbg:setTouchEnabled(false)
            end
        end)
    end

    
    
    self._recordLayer = ccui.Helper:seekWidgetByName(self.layerInventory,"bg")
    
    self.layerInventory:setTouchEnabled(false)
    self.layerInventory:addTouchEventListener(function(sender, type)
        if type == ccui.TouchEventType.ended then
            if self._bShow == true then
                self:dismissRecords()
            end   
        end
    end)
    
    local openImg = ccui.Helper:seekWidgetByName(self.layerInventory,"ImgTalkOpen")
    openImg:addTouchEventListener(function(sender, type)
        if type == ccui.TouchEventType.ended then
            if self._bShow == false then
                self:showRecords()
                self._xiaoxinum = 0
                self._tishibg:setVisible(false)
                cc.UserDefault:getInstance():setBoolForKey("isFirstTalk", false)
                self.tishi:setVisible(false)
            end
        end
    end)
    
    self.light = cc.Sprite:create("HallUI/Talk/trumpetLight.png")
    self.light:setAnchorPoint(0, 0)
    openImg:addChild(self.light)
    self.light:setVisible(false)
    
    local closeBtn = ccui.Helper:seekWidgetByName(self.layerInventory, "ButtonTalkClose")
    closeBtn:addTouchEventListener(function(sender, type)
        if type == ccui.TouchEventType.ended then
            if self._bShow == true then
                self:dismissRecords()
            end            
        end
    end)
    
    --使用按钮事件
    local function shiyongBtnAccount(sender, eventType)
        if eventType == ccui.TouchEventType.ended then
            app.musicSound:playSound(SOUND_HALL_TOUCH)
            local sendTxt = nil
            if self._text then
                sendTxt = string.rtrim(string.ltrim(self._text:getText()))
            end
            
            
			if sendTxt==nil or string.len(sendTxt)==0 then
            	MyToast.new(self:getParent(), STR_TRUMPET_NULL)
            	return
            end
            
            if app.hallLogic._trumpetCount <= 0 then
            	MyToast.new(self:getParent(), STR_TRUMPET_NOCOUNT)
                return
            end
            
            self._text:setText("")
                       
            --app.table:sendChatMessage(sendTxt)

            app.table:sendUserTrumpet(sendTxt)
            app.hallLogic:queryUserTrumpet()
        end
    end
    --使用按钮
    local shiyongBtn = self._bg:getChildByName("fasongBtn")
    shiyongBtn:addTouchEventListener(shiyongBtnAccount)
    
    self.tishi = self.layerInventory:getChildByName("tishi")
    self.tishi:setVisible(false)

    --消息提示
    self._tishibg = self.layerInventory:getChildByName("tishibg")
    self._tishinum = self._tishibg:getChildByName("Labelnum")
    if self._xiaoxinum <= 0 then
        self._tishibg:setVisible(false)
    end
end

function TalkLayer:showRecords()
    self._bShow = true
    
    self.light:stopAllActions()
    
    local function showEditBox()
        self.layerInventory:setTouchEnabled(true)
        local editBoxSize = cc.size(330, 40)
        self._text = ccui.EditBox:create(editBoxSize, "HallUI/loginUI/textbg.png")
        self._text:setPosition(cc.p(230,190))
        self._text:setFontSize(10)
        self._text:setFontColor(cc.c3b(136,122,93))
        self._text:setPlaceHolder("点击输入信息...")
        self._text:setPlaceholderFontColor(cc.c3b(136,122,93))
        self._text:setMaxLength(200)
        self._text:setReturnType(cc.KEYBOARD_RETURNTYPE_DONE)
        self._uiLayer:addChild(self._text,100)
    end
        
    self._recordLayer:runAction(cc.Sequence:create(cc.MoveBy:create(0.5, {['x']=520, ['y']=0}), cc.CallFunc:create(showEditBox)))
end

function TalkLayer:dismissRecords()
    self._bShow = false
    self._recordLayer:runAction(cc.MoveBy:create(0.5, {['x']=-520, ['y']=0}))
    if self._text then
    	self._text:removeFromParent()
    	self._text = nil
        self.layerInventory:setTouchEnabled(false)
    end
end

--给列表设置文字
function TalkLayer:setLabel(nickname, content, vipNum)
    self.light:stopAllActions()
    self._xiaoxinum = self._xiaoxinum + 1
    
    if self._xiaoxinum > 0 then
        self._tishibg:setVisible(true)
        self._tishinum:setString(self._xiaoxinum)
        if self._xiaoxinum >= 10 then
            self._tishibg:loadTexture("HallUI/Talk/beijing1.png")
            if self._xiaoxinum > 99 then
                self._tishinum:setString("99+")
            end
        else
            self._tishibg:loadTexture("HallUI/Talk/beijing.png")
        end  
        local size = self._tishibg:getContentSize()
        self._tishinum:setPosition(cc.p(size.width/2, size.height/2))
    else
        self._tishibg:setVisible(false)
    end
    if self._bShow == false then
        self.light:runAction(cc.RepeatForever:create(cc.Sequence:create( cc.CallFunc:create(function()
            self.light:setVisible(true)
        end),cc.DelayTime:create(0.5),cc.CallFunc:create(function()
            self.light:setVisible(false)
        end), cc.DelayTime:create(0.5))))   

        if cc.UserDefault:getInstance():getBoolForKey("isFirstTalk", true) then
            -- self.tishi:setVisible(true)
            self.tishi:setVisible(false)
        end    
    else
        self.light:setVisible(false)
    end     

    local gameType = ccui.Widget:create()     
    local line = ccui.ImageView:create()
    line:loadTexture("HallUI/Talk/fengexian.png")
    line:setAnchorPoint(0,0)
    line:setPosition(0,0)
    gameType:addChild(line)

    local color = cc.c3b(15, 200, 205)
    local vipcolor = cc.c3b(222, 200, 205)

    local msgStr = nickname..":\n"..content
    if vipNum and tonumber(vipNum) > 0 then
        msgStr = "V"..vipNum.." "..nickname..":\n"..content
    else
        vipNum = 0
    end 
    -- local txt = ccui.Text:create(msgStr,"",22)
    -- txt:setAnchorPoint(0,0)
    -- txt:setPosition(0,line:getContentSize().height+10)
    -- txt:setColor(color)
    -- if tonumber(vipNum) > 0 then
    --     txt:setColor(vipcolor)
    --     -- local vippic = display.newSprite("Vip/vipClose.png")
    --     -- vippic:setPosition(0, line:getContentSize().height+10)
    --     --gameType:addChild(vippic)
    -- end
    -- gameType:addChild(txt)
    local ChatUI = require("sdk.manager.ChatUI")
    local txt = ChatUI:getChatText(content, vipNum, nickname, 330)
    local txt_size = txt:getContentSize()
    txt:setPosition(cc.p(0, 0))
    gameType:addChild(txt)

    line:setVisible(false)

    gameType:setContentSize(cc.size(380, txt_size.height+line:getContentSize().height+20))
    self._list:insertCustomItem(gameType, 0)           
    if self._list:getChildrenCount()>self._listLenth then
        self._list:removeItem(self._listLenth)
        self._list:removeItem(self._listLenth)
    end
end

function TalkLayer:onEnter()
    app.eventDispather:addListenerEvent(eventGameChat, self, function(data)
        self:setLabel(data.szSendNickName, data.szChatString, data.cbMemberOrder)
    end)
end

function TalkLayer:onExit()
    app.eventDispather:delListenerEvent(self)
end

return TalkLayer