Store = class("Store", import(".BaseScene"))

Store._uiLayer= nil

StoreSelect = {
    null = 0,
    coin = 1,
    lejuan = 2,
    chongzhika = 3,
    daoju = 4 
}

Store._select = StoreSelect.null
Store._layerInventory = nil
Store._payRecordData = nil
Store._coinPanel = nil  --兑换金币页面
Store._lejuanPanel = nil--兑换乐卷
Store._chongzhiPanel = nil--兑换充值卡
Store._boxBg = nil
Store._kehuanjinbi = nil
Store._kehuandaoju = nil
Store._shoujihaoBg = nil
Store._changeType = nil
Store._shoujiLable = nil
Store._duiHuanType = nil    --1乐卷换手机卡，2乐卷换金币
Store._daojuPanel = nil --道具
Store._num = 0

function Store:onLoginSrvLoginSuccess(data)
    Store.super.onLoginSrvLoginSuccess(data)
    self:delLoading()
    self:updateLoginInfo()
end

function Store:ctor()
    --当前是哪一个界面
    local layerInventory = ccs.GUIReader:getInstance():widgetFromJsonFile("HallUI/Store.json")
    self:addChild(layerInventory)
    self._layerInventory = layerInventory
    self._layerInventory:setPositionY(1500)
    -------------box事件

    self.TargetPlatform = cc.Application:getInstance():getTargetPlatform() --设备系统
    -------------
    self.coinTiShi = BombBox.new(false, STR_STOR_COIN_MSG)
    self:addChild(self.coinTiShi, 1000)
    self.coinTiShi:setVisible(false)
    -------------
    self.leQuanTiShi = BombBox.new(false, STR_STOR_LEQUAN_MSG)
    self:addChild(self.leQuanTiShi, 1000)
    self.leQuanTiShi:setVisible(false)
    ----------------------
    self._select = StoreSelect.null
    self._payRecordData = {}
    
    self._uiLayer = cc.Layer:create()
    self:addChild(self._uiLayer)
    
    --返回按钮点击事件
    local function returnAccount(sender, eventType)
        if eventType == ccui.TouchEventType.ended then
            app.musicSound:playSound(SOUND_HALL_TOUCH)
            self:dismiss()    
        end
    end
    
    --返回按钮
    local returnBtn = layerInventory:getChildByName("returnBtn")
    returnBtn:addTouchEventListener(returnAccount)
    
    ----------------------------------------------------------
    --    --选择金币按钮       
    self._coinPanel = layerInventory:getChildByName("storList"):getChildByName("coinPanel")
    self._lejuanPanel = layerInventory:getChildByName("storList"):getChildByName("lejuanPanel")
    self._chongzhiPanel = layerInventory:getChildByName("storList"):getChildByName("chongzhiPanel")
    self._boxBg = layerInventory:getChildByName("storList"):getChildByName("boxBg")
    self._kehuanjinbi = self._boxBg:getChildByName("kehuanjinbi")
    self._kehuandaoju = self._boxBg:getChildByName("kehuandaoju")
    self._shoujihaoBg = layerInventory:getChildByName("shoujihaoBg")
    self._shoujiLable=layerInventory:getChildByName("shoujihaoBg"):getChildByName("shoujiLable")
    self._daojuPanel=layerInventory:getChildByName("daojuPanel")

    local vipBtn = layerInventory:getChildByName("Button_Vip")
    vipBtn:addTouchEventListener(function (sender, type)
        if type == ccui.TouchEventType.ended then
            app.musicSound:playSound(SOUND_HALL_TOUCH)
            local vipLayer = Vip.new(self)
            vipLayer:setParam(app.hallLogic._vipConf)
            vipLayer:show()
            self._layerInventory:addChild(vipLayer, 100)
        end
    end)

    --购买金币
    for i=0, 7 do
        local buyBtn = self._coinPanel:getChildByName("buyBtn"..i):getChildByName("btn")
        buyBtn:addTouchEventListener(function(sender, eventType)
            if eventType == ccui.TouchEventType.ended then
                app.musicSound:playSound(SOUND_HALL_TOUCH)
                print("选择"..i)
                --self:sethintBox()
                pay(i+1)   --暂时屏蔽支付按钮
            end
        end)
    end   
    --选择金币按钮       
    for i=0, 7 do
        local buyBtn = self._coinPanel:getChildByName("buyBtn"..i)
        buyBtn:addTouchEventListener(function(sender, eventType)
            if eventType == ccui.TouchEventType.ended then
                app.musicSound:playSound(SOUND_HALL_TOUCH)
                print("选择"..i)
                --self:sethintBox()
                pay(i+1)  --暂时屏蔽支付按钮
            end
        end)
    end  
    
    --换乐卷或金币
    for i=1, 3 do
        local buyBtn = self._lejuanPanel:getChildByName("buyBtn"..(i)):getChildByName("btn")
        buyBtn:addTouchEventListener(function(sender, eventType)
            if eventType == ccui.TouchEventType.ended then
                app.musicSound:playSound(SOUND_HALL_TOUCH)
                print("选择乐卷"..i)
                if i==1 then
                	self:leJuanExchangeItem(1)
                    self._changeType = 2
                -- elseif i==2 then
                --     self:leJuanExchangeItem(1)
                --     self._changeType=2
                -- elseif i==3 then
                --     self:leJuanExchangeItem(2)
                --     self._changeType=2
                end
            end
        end)
    end    
    for i=1, 3 do
        local buyBtn = self._lejuanPanel:getChildByName("buyBtn"..(i))
        buyBtn:addTouchEventListener(function(sender, eventType)
            if eventType == ccui.TouchEventType.ended then
                app.musicSound:playSound(SOUND_HALL_TOUCH)
                print("选择乐卷"..i)
                if i==1 then
                    self:leJuanExchangeItem(1)
                    self._changeType = 2
                -- elseif i==2 then
                --     self:leJuanExchangeItem(1)
                        --self._changeType = 2
                -- elseif i==3 then
                --     self:leJuanExchangeItem(2)
                --self._changeType = 2
                end
            end
        end)
    end  
    
    --乐卷换充值卡
    for i=1, 4 do
        local buyBtn = self._chongzhiPanel:getChildByName("buyBtn"..(i)):getChildByName("btn")
        buyBtn:addTouchEventListener(function(sender, eventType)
            if eventType == ccui.TouchEventType.ended then
                app.musicSound:playSound(SOUND_HALL_TOUCH)
                print("选择充值"..i)
                local num=i+1
                self:leJuanExchangeItem(num)
                self._changeType=1
            end
        end)
    end     
    for i=1, 4 do
        local buyBtn = self._chongzhiPanel:getChildByName("buyBtn"..(i))
        buyBtn:addTouchEventListener(function(sender, eventType)
            if eventType == ccui.TouchEventType.ended then
                app.musicSound:playSound(SOUND_HALL_TOUCH)
                print("选择充值"..i)
                local num=i+1
                self:leJuanExchangeItem(num)
                self._changeType=1
            end
        end)
    end  
    
    
    -----确认取消事件

    --充值手机号确认事件
    local function shoujiQRCallback( sender, eventType )
        if eventType == ccui.TouchEventType.ended then
            app.musicSound:playSound(SOUND_HALL_TOUCH)        
            local mobileNum = self._shoujiLable:getString()
            ------------------------
            mobileNum = string.ltrim(mobileNum)--去掉前端空格
            mobileNum = string.rtrim(mobileNum)--去掉后面空格
            
            print("手机号。。"..mobileNum)
            local n = tonumber(mobileNum);--判断是不是手机号，
            if n then
            -- n就是得到数字
            else
            -- 转数字失败,不是数字, 这时n == nil
                print("手机号是英文")
                self:sethintBox(3)
                return 
            end
            
            if  string.len(mobileNum)~=11 then--判断长度是不是11位
                print("手机号不对无法充值")
                self:sethintBox(3)
            	return 
            end
            --------------------------  
            app.hallLogic:exchangeRefillCard(self._duihuanType, mobileNum)  
            self._shoujihaoBg:setPosition(cc.p(1500,0))
        end
    end    
    --充值手机号确认按鈕
    local shoujiQRBtn = self._shoujihaoBg:getChildByName("querenBtn")
    shoujiQRBtn:addTouchEventListener(shoujiQRCallback)
    
    --充值手机号取消事件
    local function shoujiQXCallback( sender, eventType )
        if eventType == ccui.TouchEventType.ended then
            app.musicSound:playSound(SOUND_HALL_TOUCH)
            print("取消充值了")
            self._shoujihaoBg:setPosition(cc.p(1500,0))
        end
    end    
    --充值手机号取消按鈕
    local shoujiQXBtn = self._shoujihaoBg:getChildByName("closeBtn")
    shoujiQXBtn:addTouchEventListener(shoujiQXCallback)
    
    
    
    --兑换金币确认事件
    local function coinconfirmCallback( sender, eventType )
        if eventType == ccui.TouchEventType.ended then
            app.musicSound:playSound(SOUND_HALL_TOUCH)
            print("兑换金币确认事件")
            self._boxBg:setPosition(cc.p(1500,0))
            app.hallLogic:exchangeLeQuan(app.hallLogic._leJuan1DuiHuan["leQuanInfo"][1]["lLeQuan"])
        end
    end    
    --兑换金币确认按鈕
    local coinconfirmBtn = self._kehuanjinbi:getChildByName("confirmBtn")
    coinconfirmBtn:addTouchEventListener(coinconfirmCallback)
    
    --兑换道具确认事件
    local function itmeconfirmCallback( sender, eventType )
        if eventType == ccui.TouchEventType.ended then
            app.musicSound:playSound(SOUND_HALL_TOUCH)        
            local lab=self._kehuandaoju:getChildByName("lab")
            
            if self._changeType==1 then    --1兑换手机卡，2兑换道具
                print("兑换手机卡确认事件")
                self._shoujihaoBg:setPosition(cc.p(0,0))                    
                for k,v in pairs(app.hallLogic._yinHangFen1DuiHuan["RefillCard"]) do                           
                    if lab:getString()==v["lLeQuan"].."" then
                        self._duihuanType = v["dwType"]
                    end
                end                   
            else            
                print("兑换道具确认事件")
                for k,v in pairs(app.hallLogic._jiFen1DuiHuan["scoreInfo"]) do     
                    if lab:getString()==v["lLeQuan"].."" then
                        print("进来了"..v["lScore"]) 
                        app.hallLogic:exchangeScore(v["lScore"])
                    end
                end                   
            end
            self._boxBg:setPosition(cc.p(1500,0))           
        end
    end
    --兑换道具确认按鈕
    local itemconfirmBtn = self._kehuandaoju:getChildByName("confirmBtn")
    itemconfirmBtn:addTouchEventListener(itmeconfirmCallback)
    
    
    
    --兑换金币道具取消事件
    local function coinItemcancelCallback( sender, eventType )
        if eventType == ccui.TouchEventType.ended then
            app.musicSound:playSound(SOUND_HALL_TOUCH)
            print("兑换取消事件")
            self._boxBg:setPosition(cc.p(1500,0))
        end
    end
    --兑换金币取消按鈕
    local coincancelBtn = self._kehuanjinbi:getChildByName("cancelBtn")
    coincancelBtn:addTouchEventListener(coinItemcancelCallback) 
    --兑换道具取消按鈕
    local itmecancelBtn = self._kehuandaoju:getChildByName("cancelBtn")
    itmecancelBtn:addTouchEventListener(coinItemcancelCallback)
     
  ----------------------------------------------------------------  
    
    
    --金币按钮点击事件
    local function coinBtnCallback( sender, eventType )
        if eventType == ccui.TouchEventType.ended then
            app.musicSound:playSound(SOUND_HALL_TOUCH)
            self:setWhitchSelected(StoreSelect.coin)
            self:changePage(1)
        end
    end
    
    --金幣按鈕
    local t_coinBtn = layerInventory:getChildByName("coinBtn")
    t_coinBtn:addTouchEventListener(coinBtnCallback)
    
    --乐劵按钮点击事件
    local function lejuanBtnCallback( sender, eventType )
        if eventType == ccui.TouchEventType.ended then
            app.musicSound:playSound(SOUND_HALL_TOUCH)
            self:setWhitchSelected(StoreSelect.lejuan)
            self:changePage(2)
        end
    end

    --乐劵按鈕
    local t_lejuanBtn = layerInventory:getChildByName("lejuanBtn")
    t_lejuanBtn:addTouchEventListener(lejuanBtnCallback)
    -- t_lejuanBtn:setVisible(false)
    if self.TargetPlatform == cc.PLATFORM_OS_IPHONE or self.TargetPlatform == cc.PLATFORM_OS_IPAD then
        t_lejuanBtn:setVisible(false)
        t_lejuanBtn:setTouchEnabled(false)
    end
    
    --充值卡按钮点击事件
    local function chongzhikaBtnCallback( sender, eventType )
        if eventType == ccui.TouchEventType.ended then
            app.musicSound:playSound(SOUND_HALL_TOUCH)
            self:setWhitchSelected(StoreSelect.chongzhika)
            self:changePage(3)
        end
    end

    --充值卡按鈕
    local t_chongzhikaBtn = layerInventory:getChildByName("chongzhikaBtn")
    t_chongzhikaBtn:addTouchEventListener(chongzhikaBtnCallback)   
    -- t_chongzhikaBtn:setVisible(false)   
    if self.TargetPlatform == cc.PLATFORM_OS_IPHONE or self.TargetPlatform == cc.PLATFORM_OS_IPAD then
        t_chongzhikaBtn:setVisible(false)
        t_chongzhikaBtn:setTouchEnabled(false)
    end
    
    self.trumpetCount = ccui.Helper:seekWidgetByName(layerInventory,"trumpetCount")    --小喇叭
    self.trumpetCount:setString(app.hallLogic._trumpetCount)
       
    --道具点击事件
    local function daojuBtnCallback( sender, eventType )
        if eventType == ccui.TouchEventType.ended then
            app.musicSound:playSound(SOUND_HALL_TOUCH)
            self:setWhitchSelected(StoreSelect.daoju)
            self:changePage(4)
        end
    end

    --道具按鈕
    local t_t_daojuBtn = layerInventory:getChildByName("daojuBtn")
    t_t_daojuBtn:addTouchEventListener(daojuBtnCallback)   
    
    --具体道具
    for i=1, 6 do
        local buyBtn =  self._daojuPanel:getChildByName("btnPanel"):getChildByName("baoxiang"..(i))
        buyBtn.index = i
        buyBtn:addTouchEventListener(function(sender, eventType)
            if eventType == ccui.TouchEventType.ended then
                app.musicSound:playSound(SOUND_HALL_TOUCH)
                
                if sender.index == 6 then  --小喇叭
                    if app.hallLogic._loginSuccessInfo.lUserScore >= app.hallLogic._trumpetNeedCoin then
                		app.hallLogic:buyTrumpet()
                    else
                        MyToast.new(self, STR_TRUMPET_NO_SCORE)
                	end
                else
                    if app.hallLogic._redEnvelopeInfo and sender.index>=1 and sender.index<= app.hallLogic._redEnvelopeInfo.dwCount then
                        if app.hallLogic._loginSuccessInfo.lUserScore >= app.hallLogic._redEnvelopeInfo.items[sender.index].ExchangeScore then
                            -- app.hallLogic:buyRedEnvelope(app.hallLogic._redEnvelopeInfo.items[sender.index].dwRMBValue, 1)
                            local t_coin = app.hallLogic._loginSuccessInfo.lUserScore
                            local t_score = app.hallLogic._redEnvelopeInfo.items[sender.index].ExchangeScore

                            local t_num = math.floor(t_coin / t_score)
                            if t_num > 10 then 
                                t_num = 10
                            end

                            PayBox.new(1, t_num, sender.index, app.hallLogic._redEnvelopeInfo.items[sender.index].dwRMBValue, self)
                        else
                            MyToast.new(self, STR_REDENVELOPE_NO_SCORE)
                        end
                    end
                end
            end
        end)
    end 
    
    --玩家金币显示   
    self.playerCoin = layerInventory:getChildByName("playerCoin")
    self.playerLeJuan = layerInventory:getChildByName("playerLeJuan")
    if self.TargetPlatform == cc.PLATFORM_OS_IPHONE or self.TargetPlatform == cc.PLATFORM_OS_IPAD then
        self.playerLeJuan:setVisible(false)
    end
    self:updateLoginInfo()
    
    --更新商城，兑换成功
    app.eventDispather:addListenerEvent(eventLoginStoreCoin, self, function(data)
        self:updateLoginInfo()
        self:sethintBox(1)
    end)
    
    --兑换失败
    app.eventDispather:addListenerEvent(eventLoginStoreErro, self, function(data)
        self:sethintBox(2)
    end)
    
    app.eventDispather:addListenerEvent(eventTrumpetCount, self, function(data)
        self.trumpetCount:setString(app.hallLogic._trumpetCount)
    end)
    
    app.eventDispather:addListenerEvent(eventLoginUserCoinUpdate, self, function(data)
        self:updateLoginInfo()
    end)
    
    self:setWhitchSelected(StoreSelect.coin)
    self:InitPayRecord()    
    
    self._bShow = false
    self._bShowIng = false
end

function Store:show()
    if self._bShow  == true or self._bShowIng == true then
        return
    end

    self._bShowIng = true
    self._layerInventory:stopAllActions()
    self._layerInventory:runAction(cc.Sequence:create(cc.MoveTo:create(0.6, cc.p(0, 0)),
        cc.MoveBy:create(0.05, {['x']=0, ['y']=20}), cc.MoveBy:create(0.04, {['x']=0, ['y']=-20}), cc.CallFunc:create(function() 
            self._bShow = true
            self._bShowIng = false
        end)))
end

function Store:dismiss()
    if self._bShow == false then
        return
    end

    self._layerInventory:stopAllActions()
    self._layerInventory:runAction(cc.Sequence:create(cc.MoveBy:create(0.6,{['x']=0, ['y']=2000}), cc.CallFunc:create(function()
        self._bShow = false
    end)))
end

--获取购买记录
function Store:getPayRecord()
    self.xhr = cc.XMLHttpRequest:new() -- 新建一个XMLHttpRequest对象  
    self.xhr.responseType = cc.XMLHTTPREQUEST_RESPONSE_JSON -- json数据类型
    self.xhr:open("POST", "http://gameapi.jiujiu.com:82/api.aspx")-- POST方式

    local t_userId = app.hallLogic._loginSuccessInfo["dwUserID"]
    local t_sign = crypto.md5("PayRecord"..t_userId.."tjyjerge!@#sddf$^$h234^^DsSEG")    
    local param = "action=PayRecord&".."userID="..t_userId.."&sign="..t_sign

    self.xhr:send(param)

    local function onReadyStateChange()  
        print(" http請求回調 ")
        local response   = self.xhr.response -- 获得响应数据  
        local output = json.decode(response,1) -- 解析json数据 
--        dump(output)
        self._payRecordData = output
        self:setPayRecordData()
        self.xhr = nil
    end

    -- 注册脚本方法回调 
    self.xhr:registerScriptHandler(onReadyStateChange)
end

function Store:onEnter()
    Store.super.onEnter(self)
    self:runAction(cc.Sequence:create(cc.DelayTime:create(1.0), cc.CallFunc:create(function()
        self:getPayRecord()
    end)))
end

function Store:onExit()
    Store.super.onExit(self)
    app.eventDispather:delListenerEvent(self)
    if self.xhr then
    	self.xhr:unregisterScriptHandler()
    	self.xhr = nil
    end    
end

--金币换道具
function Store:coinExchangeItem()
    if app.hallLogic._loginSuccessInfo["lUserScore"]<50000 then
        self.coinTiShi:showText()
	   return
    end
    self._boxBg:setPosition(cc.p(0,0))
    self._kehuanjinbi:setPosition(cc.p(650,365))
    self._kehuandaoju:setPosition(cc.p(650,2256))
end

--乐卷换道具
function Store:leJuanExchangeItem(num)
    local lab=self._kehuandaoju:getChildByName("lab")
    local flag=false
    if num==1 then
        if app.hallLogic._leJuan<1 then
        	flag=true
        end
        lab:setString("1")
    elseif num==2 then
        if app.hallLogic._leJuan<12 then
            flag=true
        end
        lab:setString("12")
    elseif num==3 then
        if app.hallLogic._leJuan<20 then
            flag=true
        end
        lab:setString("20")
    elseif num==4 then
        if app.hallLogic._leJuan<45 then
            flag=true
        end
        lab:setString("45")
    elseif num==5 then
        if app.hallLogic._leJuan<85 then
            flag=true
        end
        lab:setString("85")
    end
    
    if flag then
        self.leQuanTiShi:showText()
        return
    end
    
    self._boxBg:setPosition(cc.p(0,0))
    self._kehuanjinbi:setPosition(cc.p(650,2256))
    self._kehuandaoju:setPosition(cc.p(650,365))
end

--换页 1金币 2乐卷 3充值卡 4道具
function Store:changePage(num)
    self._num = num
    print("self._num---------",self._num)
    local t_payRecordBtn = self._layerInventory:getChildByName("payRecordBtn")
    local duihuanRecordBtn = self._layerInventory:getChildByName("duihuanBtn")

	if num==1 then
        t_payRecordBtn:setVisible(true)
        t_payRecordBtn:setTouchEnabled(true)
        duihuanRecordBtn:setVisible(false)
        duihuanRecordBtn:setTouchEnabled(false)

        self._coinPanel:setPosition(cc.p(124,5))
        self._lejuanPanel:setPosition(cc.p(124,1000))
        self._chongzhiPanel:setPosition(cc.p(124,1000))
        self._daojuPanel:setPosition(cc.p(-2000,-2000))
	elseif num==2 then
        t_payRecordBtn:setVisible(false)
        t_payRecordBtn:setTouchEnabled(false)
        duihuanRecordBtn:setVisible(true)
        duihuanRecordBtn:setTouchEnabled(true)
        duihuanRecordBtn:setTag(2)

        self._coinPanel:setPosition(cc.p(124,1000))
        self._lejuanPanel:setPosition(cc.p(125,11))
        self._chongzhiPanel:setPosition(cc.p(124,1000))
        self._daojuPanel:setPosition(cc.p(-2000,-2000))
	elseif num==3 then
        t_payRecordBtn:setVisible(false)
        t_payRecordBtn:setTouchEnabled(false)
        duihuanRecordBtn:setVisible(true)
        duihuanRecordBtn:setTouchEnabled(true)
        duihuanRecordBtn:setTag(3)

        self._coinPanel:setPosition(cc.p(124,1000))
        self._lejuanPanel:setPosition(cc.p(124,1000))
        self._chongzhiPanel:setPosition(cc.p(124,5))
        self._daojuPanel:setPosition(cc.p(-2000,-2000))
    elseif num==4 then
        t_payRecordBtn:setVisible(false)
        t_payRecordBtn:setTouchEnabled(false)
        duihuanRecordBtn:setVisible(false)
        duihuanRecordBtn:setTouchEnabled(false)

        self._coinPanel:setPosition(cc.p(124,1000))
        self._lejuanPanel:setPosition(cc.p(124,1000))
        self._chongzhiPanel:setPosition(cc.p(124,1000))
        self._daojuPanel:setPosition(cc.p(0,0))
	end
end

function Store:InitPayRecord()
    local t_payRecord = self._layerInventory:getChildByName("payRecord")
    
    --点击空白关闭事件
    local function closePayRecordCallback( sender, eventType )
        if eventType == ccui.TouchEventType.ended then
            t_payRecord:setVisible(false)
        end
    end
    --空白的背景 用来关闭
    local t_bg = t_payRecord:getChildByName("payRecordBg")
    t_bg:addTouchEventListener(closePayRecordCallback)
    t_payRecord:getChildByName("payRecordCloseBtn"):addTouchEventListener(closePayRecordCallback)

    local paytips = self._layerInventory:getChildByName("payRecord"):getChildByName("Image_8")
    local duihuantips = self._layerInventory:getChildByName("payRecord"):getChildByName("Image_6")

    --购买记录按钮点击事件
    local function payRecordBtnCallback( sender, eventType )
        if eventType == ccui.TouchEventType.ended then
            app.musicSound:playSound(SOUND_HALL_TOUCH)
            self:getPayRecord() --请求充值记录
            t_payRecord:getChildByName("recordList"):removeAllChildren()
            t_payRecord:setVisible(true)
            duihuantips:setVisible(false)
            paytips:setVisible(true)
        end
    end
    --购买记录按鈕
    local t_payRecordBtn = self._layerInventory:getChildByName("payRecordBtn")
    t_payRecordBtn:addTouchEventListener(payRecordBtnCallback)
    
    local duihuanRecordBtn = self._layerInventory:getChildByName("duihuanBtn")
    --兑换记录按钮点击事件
    local function duihuanRecordBtnCallback( sender, eventType )
        if eventType == ccui.TouchEventType.ended then
            app.musicSound:playSound(SOUND_HALL_TOUCH)
            Store:getExchangeRecord(duihuanRecordBtn:getTag()) --请求兑换记录
            t_payRecord:getChildByName("recordList"):removeAllChildren()
            t_payRecord:setVisible(true)
            paytips:setVisible(false)
            duihuantips:setVisible(true)
        end
    end
    --兑换记录按鈕
    duihuanRecordBtn:addTouchEventListener(duihuanRecordBtnCallback)
    duihuanRecordBtn:setVisible(false)
    duihuanRecordBtn:setTouchEnabled(false)
end

--请求兑换记录
function Store:getExchangeRecord(num)
    print("self._num == ", num)
    app.hallLogic:qureyExchange(num)
end

function Store:onEnter()
    Store.super.onEnter(self)
    app.eventDispather:addListenerEvent(eventExchangeData, self, function(data)
        self:setPayRecordData(data)
    end)
end

function Store:onExit()
    Store.super.onExit(self)
    app.eventDispather:delListenerEvent(self)
end

function Store:setPayRecordData(m_tab)
    print( "Store:setPayRecordData" )    
    local t_payRecord = self._layerInventory:getChildByName("payRecord")
    local t_list = t_payRecord:getChildByName("recordList")
    t_list:setInertiaScrollEnabled(true)
    t_list:setDirection(1)
    t_list:setGravity(2)
    t_list:removeAllChildren()
    
--    --測試代碼
--    for i=1, 31 do
--        local t_test1 = payRecordItem.new( "2015/10/15 10:05:22", ""..i.."元", ""..i..i..i.."萬" )
--        local t_width = t_test1:getContentSize().width
--        local t_height = t_test1:getContentSize().height
--        print( "t_width  "..t_width.." t_height "..t_height )
--        print( "t_test1 "..tostring(t_test1)..' t_list:getChildrenCount() '..t_list:getChildrenCount() )
--        local moduleWgt = ccui.Widget:create()
--        moduleWgt:setContentSize(cc.size(920, 20))
--        moduleWgt:addChild(t_test1)
--        t_test1:setPosition(460,10)
--        
--        t_list:insertCustomItem( moduleWgt, t_list:getChildrenCount() )
--    end    
    if self._payRecordData ~= nil and not m_tab then   
        local t_pay = self._payRecordData["TableInfo"]

        local function myComps(a, b)
            return a.CreateTime > b.CreateTime
        end

        table.sort(t_pay, myComps)     
        for k,v in pairs(t_pay) do     
            local t_coin = v["Gold"] / 10000  
            local t_test1 = payRecordItem.new(v["CreateTime"], v["PayAmount"].."元", t_coin.."万" )    
                
            local moduleWgt = ccui.Widget:create()
            moduleWgt:setContentSize(cc.size(920, 20))
            moduleWgt:addChild(t_test1)
            t_test1:setPosition(460,10)
            
            t_list:insertCustomItem( moduleWgt, t_list:getChildrenCount() )
        end
    
--        local t_test2 = payRecordItem.new( "", "", "" )
        local moduleWgt2 = ccui.Widget:create()
        moduleWgt2:setContentSize(cc.size(920, 20))
        t_list:insertCustomItem( moduleWgt2, t_list:getChildrenCount() )     
    end

    if m_tab and m_tab.dwAmount ~= 0 then
        local t_pay = m_tab.refillCardRecord
        if m_tab.ScoreRecord then
            t_pay = m_tab.ScoreRecord
        end
        -- local function myComps(a, b)
        --     return a.data > b.data
        -- end
        -- dump(t_pay)
        -- table.sort(t_pay, myComps)     
        for k,v in pairs(t_pay) do 
            local t_test1 = nil
            if v.szMobilePhone then
                local count = v["lBeforeLeQuan"]-v["lAfterLeQuan"]
                local string = ""
                if v.dwType == 1 then
                    string = v.szMobilePhone.." 10元充值卡"
                elseif v.dwType == 2 then
                    string = v.szMobilePhone.." 30元充值卡"
                elseif v.dwType == 3 then
                    string = v.szMobilePhone.." 50元充值卡"
                end
                local timestr = self:setTimeForTab(v.data)
                t_test1 = payRecordItem.new(timestr, count.."乐卷", string)
            else
                local count = math.abs(v["lBeforeLeQuan"]-v["lAfterLeQuan"])
                local score = v["lAfterScore"]-v["lBeforeScore"]
                local timestr = self:setTimeForTab(v.data)
                t_test1 = payRecordItem.new(timestr, count.."乐卷", score)
            end    
                
            local moduleWgt = ccui.Widget:create()
            moduleWgt:setContentSize(cc.size(920, 20))
            moduleWgt:addChild(t_test1)
            t_test1:setPosition(460,10)
            
            t_list:insertCustomItem( moduleWgt, t_list:getChildrenCount() )
        end

        local moduleWgt2 = ccui.Widget:create()
        moduleWgt2:setContentSize(cc.size(920, 20))
        t_list:insertCustomItem( moduleWgt2, t_list:getChildrenCount() )     
    else
        return
    end
end

function Store:setTimeForTab(tab)
    local wYear = tab.wYear
    local wMonth = tab.wMonth
    local wDay = tab.wDay
    local wHour = string.format("%02d", tab.wHour)
    local wMinute = string.format("%02d", tab.wMinute)
    local wSecond = string.format("%02d", tab.wSecond)
    local timestr = wYear.."/"..wMonth.."/"..wDay.." "..wHour..":"..wMinute..":"..wSecond
    return timestr
end

--金币不够无法兑换
function Store:setCoinBuZuBox(num)
    self.baoXiangTiShi:showText()
end

--支付提示框
function Store:sethintBox(num)
    if num==1 then
        MyToast.new(self, STR_STORE_HINT_MSG)
	elseif num==2 then
	    MyToast.new(self, STR_STORE_HINT_MSG1)
	elseif num==3 then
        MyToast.new(self, STR_STORE_HINT_MSG2)
    end
end

function Store:updateLoginInfo()
    local userInfo = app.hallLogic._loginSuccessInfo
    self.playerCoin:setString(userInfo.lUserScore)
    self.playerLeJuan:setString(app.hallLogic._leJuan)    
end

function Store:setWhitchSelected( a_select )
    if self._select ~= a_select then        
        self._select = a_select
        self:setPayRecordVisible( a_select == StoreSelect.coin, false )
        local t_btn = {}
        t_btn[1] = self._layerInventory:getChildByName("coinBtn")
        t_btn[2] = self._layerInventory:getChildByName("lejuanBtn")
        t_btn[3] = self._layerInventory:getChildByName("chongzhikaBtn")
        t_btn[4] = self._layerInventory:getChildByName("daojuBtn")
        
        for i=1, 4 do
            if i == a_select then
                t_btn[i]:setTouchEnabled(false)
                t_btn[i]:setBright(false)                                
            else
                t_btn[i]:setTouchEnabled(true)
                t_btn[i]:setBright(true)
            end
        end
    end
end

--第一個參數設置 购买记录按钮的显示；第二个参数设置购买记录的显示
function Store:setPayRecordVisible( a_btnVisible, a_bgVisible )
    local t_btn = self._layerInventory:getChildByName("payRecordBtn")
    local t_bg = self._layerInventory:getChildByName("payRecord")
    
    t_btn:setVisible(a_btnVisible)
    t_bg:setVisible(a_bgVisible)
end

return Store