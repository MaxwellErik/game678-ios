
local PhysicBaseScene = class("PhysicBaseScene", function(sceneName)
    return display.newPhysicsScene("PhysicBaseScene");
end)

function PhysicBaseScene:ctor()
    if VERSION_DEBYG == true then
        print("PhysicBaseScene")
    end
end

function PhysicBaseScene:onEnter()
    if VERSION_DEBYG == true then
        print("PhysicBaseScene:onEnter")
    end
    app.eventDispather:addListenerEvent(eventHallNetConnected, self, function(data)
        return self:onHallNetConnected(data)
    end)
    
    app.eventDispather:addListenerEvent(eventHallNetClosed, self, function(data)
        self:onHallNetClose(data)
    end)    
    
    app.eventDispather:addListenerEvent(eventLoginSrvLoginSuccessed, self, function(data)
        self:onLoginSrvLoginSuccess(data)
    end)
    
    app.eventDispather:addListenerEvent(eventLoginSrvLoginFailed, self, function(data)
        self:onLoginSrvLoginFailed(data)
    end)
    
    app.eventDispather:addListenerEvent(eventGameLoginFailed, self, function(data)
        self:onGameLoginFailed(data)
    end)
    
    app.eventDispather:addListenerEvent(eventGameReqFailed, self, function(data)
    	self:onGameReqFailed(data)
    end)
    
    app.eventDispather:addListenerEvent(eventRemoveLoading, self, function(data)
        self._loadingLayer = nil
        self:onLoadingCancle()
    end)   
end

function PhysicBaseScene:onEnterTransitionFinish()
    if VERSION_DEBYG == true then
        print( "PhysicBaseScene:onEnterTransitionFinish" )
    end
end

function PhysicBaseScene:onExit()
    app.eventDispather:delListenerEvent(self)
end

function PhysicBaseScene:onLoadingCancle()
end

--����loading
function PhysicBaseScene:addLoading()
    if VERSION_DEBYG == true then
        print("PhysicBaseScene:addLoading")
    end
    if self._loadingLayer then
        return
    end
    
    self._loadingLayer = Loading:new()
    self:addChild(self._loadingLayer, 1000)
end

function PhysicBaseScene:delLoading()
    if self._loadingLayer  then
        self:removeChild(self._loadingLayer)
        self._loadingLayer = nil
    end
end

function PhysicBaseScene:showTip(showBtn, tips, okCallBack, cancleCallBack)
	local tipBox = BombBox.new(showBtn, tips, okCallBack, cancleCallBack)
	self:addChild(tipBox, 1000)
end

function PhysicBaseScene:onHallNetConnected(bConnected)
end

function PhysicBaseScene:onHallNetClose(data)
end

function PhysicBaseScene:onLoginSrvLoginSuccess(data)
    checkPayFailed()
end

function PhysicBaseScene:onLoginSrvLoginFailed(data)
	self:delLoading()
    MyToast.new(self,data.szDescribeString)
    --self:showTip(false, data.szDescribeString)
end

function PhysicBaseScene:onGameLoginFailed(data)
    self:delLoading()
    MyToast.new(self,data.szDescribeString)
    --self:showTip(false, data.szDescribeString)
    custom.PlatUtil:closeServer(CLIENT_GAME)
end

function PhysicBaseScene:onGameReqFailed(data)
    self:delLoading()
    MyToast.new(self,data.szDescribeString)
    --self:showTip(false, data.szDescribeString)
end

return PhysicBaseScene