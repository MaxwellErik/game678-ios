
Notice = class("Notice", function()
    return display.newLayer()
end)

function Notice:ctor()
    self._uiLayer = cc.Layer:create()
    self:addChild(self._uiLayer)
    
    local layerInventory = ccs.GUIReader:getInstance():widgetFromJsonFile("GameSelect/Notice.json")
    self._uiLayer:addChild(layerInventory)
    
    self._noticeBG = ccui.Helper:seekWidgetByName(layerInventory,"GongGaoBg")
    self._bgPosX = self._noticeBG:getPositionX()
    self._bgPosY = self._noticeBG:getPositionY()
    
    local function closeTarget(sender, type)
    	if type == ccui.TouchEventType.ended then
    		self:dismiss()
    	end
    end
    
    local closeBtn = ccui.Helper:seekWidgetByName(layerInventory,"closeBtn")
    closeBtn:addTouchEventListener(closeTarget)
    
    layerInventory:addTouchEventListener(closeTarget)
    
    self._noticeBG:setPositionY(2000)
    self._bShow = false
end

function Notice:show()
    self._noticeBG:stopAllActions()
	self._noticeBG:runAction(cc.Sequence:create(cc.MoveTo:create(0.6, {['x']=self._bgPosX, ['y']=self._bgPosY}),
        cc.MoveBy:create(0.05, {['x']=0, ['y']=20}), cc.MoveBy:create(0.04, {['x']=0, ['y']=-20}), cc.CallFunc:create(function() 
            self._bShow = true
        end)))
end

function Notice:dismiss()
    if self._bShow == false then
		return
	end
	
    self._noticeBG:stopAllActions()
    self._noticeBG:runAction(cc.Sequence:create(cc.MoveBy:create(0.6,{['x']=0, ['y']=2000}), cc.CallFunc:create(function()
        self:removeFromParent()
    end)))
end

return Notice