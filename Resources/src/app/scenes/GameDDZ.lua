
local GameDDZ = class("GameDDZ", function()
    return display.newScene("GameDDZ")
end)

local my_dayin=true     --打印print

local xiaowangNum=30
local dawangNum=40

local ddd_meiyou=-1     --没有牌          （比牌大小）   v
local ddd_wangzhadan=0  --双王                （最大）            v
local ddd_zhadan=1      --炸弹                （比牌大小）  v
local ddd_sandaier=2    --三带二           （比牌大小）  v
local ddd_sandaiyi=3    --三带一           （比牌大小）  v
local ddd_shunzi=4      --顺子                （比牌大小）  v
local ddd_feiji=5       --飞机                （比牌大小）  v
local ddd_duizi=6       --对子                （比牌大小）  v
local ddd_danpai=7      --单牌                （比牌大小）  v
local ddd_sidaier=8     --四带二           （比牌大小）  v
local ddd_sidaidui=9    --四带对子      （比牌大小）  v
local ddd_sanbudai=10   --三不带           （比牌大小）  v
local ddd_liandui=11    --连对                （比牌大小）  v
local ddd_feiji1=12     --飞机带一                                      v
local ddd_feiji2=13     --飞机带对                                      v
local talkWenZi={"GameDDZ/buchu_paopao.png","GameDDZ/bujiao_paopao.png",
    "GameDDZ/buqiang_paopao.png","GameDDZ/jiaodizhu_paopao.png","GameDDZ/qiangdizhu_paopao.png"}

function GameDDZ:ctor()
    self._uiLayer= nil
    self._layerInventory=nil--当前场景

    self._uiLayer = cc.Layer:create()
    self:addChild(self._uiLayer)

    local layerInventory = ccs.GUIReader:getInstance():widgetFromJsonFile("GameDDZ/GameDDZ.json")
    self._uiLayer:addChild(layerInventory)
    self._layerInventory=layerInventory
    self._playerPoker=layerInventory:getChildByName("playerPoker")  --玩家扑克
    self._playerPokerNum=0
    self._table=layerInventory:getChildByName("table")  --桌子按钮

    self._xuanzhongNum=0
    self._paiDa=0       --获取牌的大小
    self._paiDaNum=0    --获取牌的数量

    self._dizhupai={}       --地主牌
    self._myPoker={}        --自己的排
    self._player1Poker={}   --下家打出的牌
    self._player2Poker={}   --上家打出的牌
    self._panduanPoker={}   --与玩家比的牌
    for i=1,3 do
        local poker0=DDZPoker.new(false,math.random(1,4),math.random(1,15),self)
        self._dizhupai[i]=poker0
        poker0:setPosition(567+(50*i)-45+70*0.4,632+86*0.4)
        self._layerInventory:addChild(poker0,2)
        poker0:setScale(0.4)
        poker0:setZhengFan(true)
    end

    --碰撞最后一个判断用
    local poker=DDZPoker.new(false,1,1,self)
    self._layerInventory:addChild(poker)
    poker:setPosition(-1000,-1000)
    poker:setLocalZOrder(100)
    poker._isOther=false

    --返回键和home键
    self._uiLayer:setKeypadEnabled(true)
    self._uiLayer:addNodeEventListener(cc.KEYPAD_EVENT, function (event)
        app.musicSound:playSound(SOUND_HALL_TOUCH)
        if event.key == "back" then  --返回键
            --if self._userStatus~=true then
            --app.table:sendStandup()
            self:gotoGameChoice()
            --custom.PlatUtil:closeServer(CLIENT_GAME)

            --else
            --self._tipBox:showText()
            --end

        end
    end)

    --出牌
    self._chupaiBtn = self._table:getChildByName("chupaiBtn")
    local function chupaiCallBack(sender, eventType)
        if eventType == ccui.TouchEventType.ended then
            self:setTestPoker()     --上家出牌poker测试用
            self:chuPai()

            local isCeShi=self:isSetPoker()   --测试用
            if isCeShi==true then
                self:myPrint("自己有牌比他大")
            else
                self:myPrint("自己的牌比他小")
            end
        end
    end
    self._chupaiBtn:addTouchEventListener(chupaiCallBack)
    self._chupaiBtn:setTouchEnabled(false)
    self._chupaiBtn:setBright(false)

    --创建点击层
    self._dianjiLayer= cc.Layer:create()
    self:addChild(self._dianjiLayer)
    self._dianjiLayer:setLocalZOrder(5)
    local _layer= self._dianjiLayer
    _layer:setTouchEnabled(true)
    self._dianjiLayer.listener = cc.EventListenerTouchOneByOne:create();
    local dianjiX=0
    local ismove=false
    --点击开始
    local function onTouchBegin(touch,event)
        return true;
    end
    --点击移动
    local function onTouchMove(touch,event)
        if self._isOther==true then
            return
        end

        local p = touch:getLocation();
        p = _layer:convertToNodeSpace(p);
        local pp=self:convertToWorldSpace(p)

        if p.y>=170 then
            return
        end

        for k,v in pairs(self._myPoker) do
            local ppp=v:convertToWorldSpace(cc.p(0,0))
            local s_juli=2
            if v:myContainsPoint(pp,ppp) then
                if v._isTaiQi==true then    --如果抬起，那不调用下面的
                    return
                end
                if ismove==false then   --用来获取第一个move点
                    ismove=true
                    dianjiX=pp.x
                end
                local szX=pp.x

                if dianjiX>szX then     --右划
                    if ppp.x<dianjiX then
                        if (ppp.x+130)-szX>s_juli then
                            v:xuanzhong()           --选中
                        else
                            v:fangxia()
                        end
                end

                end

                if dianjiX<szX then     --左划
                    if ppp.x>dianjiX then
                        if szX-(ppp.x)>s_juli then
                            v:xuanzhong()           --选中
                        else
                            v:fangxia()
                        end
                end

                end

            end


            if dianjiX>pp.x  then     --右划
                if ppp.x<dianjiX then
                    if (ppp.x+130)-pp.x >20 then
                    else
                        if v._isTaiQi==false then   --如果抬起不调用
                            v:fangxia()
                        end

                    end
            end
            end
            if dianjiX<pp.x  then     --左划
                if ppp.x>dianjiX then
                    if pp.x -(ppp.x)>20 then
                    else
                        if v._isTaiQi==false then   --如果抬起不调用
                            v:fangxia()
                        end
                    end
            end

            end

        end
    end
    --点击结束
    local function onTouchEnd(touch,event)
        if self._isOther==true then
            return
        end

        local p = touch:getLocation();
        p = _layer:convertToNodeSpace(p);
        local pp=self:convertToWorldSpace(p)
        local ppp=self:convertToWorldSpace(cc.p(0,0))

        self:shangTai() --上抬

        if #self._panduanPoker~=0 then  --当别人有出牌的时候
            local isChuPai=self:comparePokers()       --判断能不能出牌
            self._chupaiBtn:setTouchEnabled(isChuPai)
            self._chupaiBtn:setBright(isChuPai)
        else                            --自己出牌的时候
            local isChuPai=self:choiceXuanZe()       --判断能不能出牌
            self._chupaiBtn:setTouchEnabled(isChuPai)
            self._chupaiBtn:setBright(isChuPai)
        end


        ismove=false
    end

    self._dianjiLayer.listener:registerScriptHandler(onTouchBegin,cc.Handler.EVENT_TOUCH_BEGAN);
    self._dianjiLayer.listener:registerScriptHandler(onTouchMove,cc.Handler.EVENT_TOUCH_MOVED);
    self._dianjiLayer.listener:registerScriptHandler(onTouchEnd,cc.Handler.EVENT_TOUCH_ENDED);

    local eventDispatcher = _layer:getEventDispatcher()
    eventDispatcher:addEventListenerWithSceneGraphPriority(self._dianjiLayer.listener, _layer)

    --放下poker
    local fangxiaPanel = layerInventory:getChildByName("fangxiaPanel")
    local function touchCallBack(sender, eventType)
        if eventType == ccui.TouchEventType.ended then
            for k,v in pairs(self._myPoker) do
                v:fangxia()      --放下
            end
            self._chupaiBtn:setTouchEnabled(false)
            self._chupaiBtn:setBright(false)

            self._table:setVisible(true)    --暂时显示桌子
            self:shanchuChuPai()            --暂时删除出牌
            self:shanchuChuPai1()           --暂时删除出牌
            --self:shanchuChuPai2()           --暂时删除出牌

        end
    end
    fangxiaPanel:addTouchEventListener(touchCallBack)

    --不出
    local buchuBtn = self._table:getChildByName("buchuBtn")
    local function buchuCallBack(sender, eventType)
        if eventType == ccui.TouchEventType.ended then
            for k,v in pairs(self._myPoker) do
                v:fangxia()      --放下自己所有的牌
            end
            --self:fahua(math.random(1,3),math.random(1,5))
            self:setTestPoker()     --上家出牌poker测试用
            self:shanchuChuPai2()           --暂时删除出牌
            self:chuPaiPlayer2()

            local isCeShi=self:isSetPoker()   --测试用
            if isCeShi==true then
                self:myPrint("自己有牌比他大")
            else
                self:myPrint("自己的牌比他小")
            end

        end
    end
    buchuBtn:addTouchEventListener(buchuCallBack)

    --提示
    local tishiBtn = self._table:getChildByName("tishiBtn")
    local function tishiCallBack(sender, eventType)
        if eventType == ccui.TouchEventType.ended then
            for k,v in pairs(self._myPoker) do  --提示前，先把其他的牌放下
                v:fangxia()      --放下
            end
            self:tiShi()
        end
    end
    tishiBtn:addTouchEventListener(tishiCallBack)

    --提示测试用
    local tishiBtn_0 = self._table:getChildByName("tishiBtn_0")
    local function tishiBtn_0CallBack(sender, eventType)
        if eventType == ccui.TouchEventType.ended then
            self._panduanPoker={}   --测试用
        end
    end
    tishiBtn_0:addTouchEventListener(tishiBtn_0CallBack)


    --自己发牌区
    self._myFaPaiQu= cc.Layer:create()
    self._layerInventory:addChild(self._myFaPaiQu)
    self._myFaPaiQu:setScale(0.7)

    --玩家1发牌区    下家
    self._player1FaPaiQu= cc.Layer:create()
    self._layerInventory:addChild(self._player1FaPaiQu)
    self._player1FaPaiQu:setScale(0.7)

    --玩家2发牌区    上家
    self._player2FaPaiQu= cc.Layer:create()
    self._layerInventory:addChild(self._player2FaPaiQu)
    self._player2FaPaiQu:setScale(0.7)



    --创建扑克 测试用
    local fa=1
    local t_num=4
    for i=1,30 do   --自己的
        local t_poker={}
        t_poker["huase"]=math.random(1,4)
        if fa==3 then
            fa=1
            t_num=t_num+1
        else
            fa=fa+1
        end
        t_poker["dianshu"]=math.random(1,15)    --math.random(1,15)  t_num
        self:addPlayerPoker(t_poker)
    end
    self:pokerPaiXu(self._myPoker)   --排序
    self:pokerFangZhi() --扑克放置
    for i=1,10 do        --下家
        local t_poker={}
        t_poker["huase"]=math.random(1,4)
        t_poker["dianshu"]=math.random(1,15)
        table.insert(self._player1Poker,t_poker)
    end

    self:setTestPoker()        --上家
    self:chuPaiPlayer2()

    --玩家信息，对话
    self._wanjiaXinXi = self._layerInventory:getChildByName("wanjiaXinXi")
    self._playerDuiHua = self._wanjiaXinXi:getChildByName("playerDuiHua")           --发话
    self._player1DuiHua = self._wanjiaXinXi:getChildByName("player1DuiHua")
    self._player2DuiHua = self._wanjiaXinXi:getChildByName("player2DuiHua")
    self._playerXinXiPanel = self._wanjiaXinXi:getChildByName("playerXinXiPanel")   --玩家信息
    self._player1XinXiPanel = self._wanjiaXinXi:getChildByName("player1XinXiPanel")
    self._player2XinXiPanel = self._wanjiaXinXi:getChildByName("player2XinXiPanel")

    for i=1,3 do    --测试用玩家数据
        self:sheZhiXinxi(i,"name"..i,"vip"..i,i*1000)
    end

    --显示玩家信息
    local playersPanel = self._layerInventory:getChildByName("playersPanel")
    local playerIcon = playersPanel:getChildByName("playerIcon")
    local playerIcon1 = playersPanel:getChildByName("playerIcon1")
    local playerIcon2 = playersPanel:getChildByName("playerIcon2")
    local closeXinXiPanel = playersPanel:getChildByName("closeXinXiPanel")
    local btn1=playerIcon:getChildByName("btn")
    local btn2=playerIcon1:getChildByName("btn")
    local btn3=playerIcon2:getChildByName("btn")
    local function playerIconCallBack(sender, eventType)
        if eventType == ccui.TouchEventType.ended then
            if sender==btn1 then
                self._playerXinXiPanel:setVisible(true)
            elseif sender==btn2 then
                self._player1XinXiPanel:setVisible(true)
            elseif sender==btn3 then
                self._player2XinXiPanel:setVisible(true)
            end
            closeXinXiPanel:setVisible(true)
        end
    end
    btn1:addTouchEventListener(playerIconCallBack)
    btn2:addTouchEventListener(playerIconCallBack)
    btn3:addTouchEventListener(playerIconCallBack)
    local function closeXinXiCallBack(sender, eventType)    --关闭玩家信息
        closeXinXiPanel:setVisible(false)
        self._playerXinXiPanel:setVisible(false)
        self._player1XinXiPanel:setVisible(false)
        self._player2XinXiPanel:setVisible(false)
    end
    closeXinXiPanel:addTouchEventListener(closeXinXiCallBack)


end
--[[
local ddd_meiyou=-1     --没有牌          （比牌大小）   v
local ddd_wangzhadan=0  --双王                （最大）            v v1 c t
local ddd_zhadan=1      --炸弹                （比牌大小）  v v1 c t
local ddd_sandaier=2    --三带二           （比牌大小）  v v1 c t
local ddd_sandaiyi=3    --三带一           （比牌大小）  v v1 c t
local ddd_shunzi=4      --顺子                （比牌大小）  v v1 c t
local ddd_feiji=5       --飞机                （比牌大小）  v v1 c t
local ddd_duizi=6       --对子                （比牌大小）  v v1 c t
local ddd_danpai=7      --单牌                （比牌大小）  v v1 c t
local ddd_sidaier=8     --四带二           （比牌大小）  v v1 c t
local ddd_sidaidui=9    --四带对子      （比牌大小）  v v1 c t
local ddd_sanbudai=10   --三不带           （比牌大小）  v v1 c t
local ddd_liandui=11    --连对                （比牌大小）  v v1 c t
local ddd_feiji1=12     --飞机带一                                      v v1 c t
local ddd_feiji2=13     --飞机带对                                      v v1 c t
]]

--洗牌发牌功能
function GameDDZ:xiPaiFaPai()
	
end

--提示
function GameDDZ:tiShi()
    --别人的牌
    local ceshitable={}
    if #self._panduanPoker==0 then
        self:myPrint("其他玩家没有出牌")
        return true
    end

    for k,v in pairs(self._panduanPoker) do
        ceshitable[k]={}
        ceshitable[k]["dianshu"]=v._panduanNum
    end

    local b_paiZhong=self:paiZhongLei(ceshitable)
    local b_da=self._paiDa
    local b_num=#ceshitable

    self:myPrint("别人的牌")
    self:dayinPaiZhong(b_paiZhong)

    --如果别人是王炸直接返回false
    if b_paiZhong==ddd_wangzhadan then
        return false
    end

    local isNeedZhaDan=true     --是否需要炸弹
    local isNeedShuangWang=true --是否需要双王
    local function myComps(a, b)    --自己的牌从小到大排一次
        return a._myNum < b._myNum
    end
    table.sort(self._myPoker, myComps)

    if b_paiZhong==ddd_danpai then      --单牌判断
        for k,v in pairs(self._myPoker) do
            if b_da<v._panduanNum then
                v._isXuanZhong=true
                isNeedZhaDan=false
                break
            end
    end
    end

    if b_paiZhong==ddd_duizi then       --对子判断
        local duiNum=0
        for k,v in pairs(self._myPoker) do
            if duiNum==v._panduanNum then
                if b_da<v._panduanNum then
                    v._isXuanZhong=true
                    self._myPoker[k-1]._isXuanZhong=true
                    isNeedZhaDan=false
                    break
                end
            else
                duiNum=v._panduanNum
            end
        end
    end

    if b_paiZhong==ddd_sanbudai then    --三不带判断
        local duiNum=0
        local duiNum2=0
        for k,v in pairs(self._myPoker) do
            if duiNum==v._panduanNum then
                if duiNum2==v._panduanNum then
                    self._paiDa=v._panduanNum
                    if self._paiDa>b_da then
                        v._isXuanZhong=true
                        self._myPoker[k-1]._isXuanZhong=true
                        self._myPoker[k-2]._isXuanZhong=true
                        isNeedZhaDan=false
                        break
                    end
                else
                    duiNum2=v._panduanNum
                end

            else
                duiNum=v._panduanNum
            end
        end
    end

    if b_paiZhong==ddd_sandaiyi then    --三带一
        if #self._myPoker>=4 then
            local duiNum=0
            local duiNum2=0
            local isSange=true  --判断是不是3个
            for k,v in pairs(self._myPoker) do    --找到3个的
                if duiNum==v._panduanNum then
                    if duiNum2==v._panduanNum then
                        self._paiDa=v._panduanNum
                        if self._paiDa>b_da then
                            v._isXuanZhong=true
                            self._myPoker[k-1]._isXuanZhong=true
                            self._myPoker[k-2]._isXuanZhong=true
                            isSange=false
                            break
                        end
                    else
                        duiNum2=v._panduanNum
                    end

            else
                duiNum=v._panduanNum
            end
            end
            if isSange==false then     --有三个，进入下一步判断
                for k,v in pairs(self._myPoker) do  --找到带的一个
                    if v._panduanNum~=duiNum2 then
                        v._isXuanZhong=true
                        isNeedZhaDan=false
                        break
                end
            end
            end

    end

    end

    if b_paiZhong==ddd_sandaier then    --三带对子
        if #self._myPoker>=5 then
            local duiNum=0
            local duiNum2=0
            local isSange=true  --判断是不是3个
            for k,v in pairs(self._myPoker) do    --找到3个的
                if duiNum==v._panduanNum then
                    if duiNum2==v._panduanNum then
                        self._paiDa=v._panduanNum
                        if self._paiDa>b_da then
                            v._isXuanZhong=true
                            self._myPoker[k-1]._isXuanZhong=true
                            self._myPoker[k-2]._isXuanZhong=true
                            isSange=false
                            break
                        end
                    else
                        duiNum2=v._panduanNum
                    end
            else
                duiNum=v._panduanNum
            end
            end

            if isSange==false then     --有三个，进入下一步判断
                local duiNum=0
                for k,v in pairs(self._myPoker) do
                    if duiNum==v._panduanNum then
                        if duiNum2~=v._panduanNum then
                            v._isXuanZhong=true
                            self._myPoker[k-1]._isXuanZhong=true
                            isNeedZhaDan=false
                            break
                        end
                    else
                        duiNum=v._panduanNum
                    end
                end
            end
    end
    end

    if b_paiZhong==ddd_shunzi then     --顺子
        if b_da<14 then    --A是顺子里面最大的
            local b_zuiXiao=b_da-b_num+1  --最小值
            local zhi=0
            local num=1
            for i=1,#self._myPoker-1 do
                if self._myPoker[i]._panduanNum>b_zuiXiao then  --如果当前的值比别人顺子最小值大
                    zhi=self._myPoker[i]._panduanNum
                    self._myPoker[i]._isXuanZhong=true
                    num=1
                    for j=i+1,#self._myPoker do     --连子判断
                        if zhi+1==self._myPoker[j]._panduanNum then --如果出现比当前值小1的值，连子就+1
                            self._myPoker[j]._isXuanZhong=true
                            num=num+1
                            zhi=zhi+1
                            if num==b_num then
                                break
                            end
                    end
                    end

                    if num==b_num and zhi<=14 then      --当顺子的个数大于等于别人顺子个数的时候跳出
                        isNeedZhaDan=false
                        break
                    else
                        for k,v in pairs(self._myPoker) do
                            v._isXuanZhong=false
                        end
                    end
                end
            end
    end
    end

    if b_paiZhong==ddd_liandui then     --连对
        if b_da<14 then    --A是顺子里面最大的
            local zhi=0
            local num=1
            local isShunZi=false    --判断是不是顺子
            local isDiZi=false    --判断是不是顺子
            local duizi={}
            local b_zuiXiao=b_da-b_num/2+1  --最小值
            for i=1,#self._myPoker-1 do
                if isShunZi==true then
                    break
                else
                    duizi={}
                end

                if self._myPoker[i]._panduanNum>b_zuiXiao then  --如果当前最大的值比别人顺子最大值小，直接返回false
                    zhi=self._myPoker[i]._panduanNum
                    num=1
                end
                local isFirst=true
                local keyiDuizi=false   --判断用不用进行对子判断
                for j=i+1,#self._myPoker do     --连子判断
                    if zhi+1==self._myPoker[j]._panduanNum then --如果出现比当前值小1的值，连子就+1
                        num=num+1
                        if isFirst==true then   --第一次进入添加最早的值
                            table.insert(duizi,zhi)
                        end
                        table.insert(duizi,zhi+1)
                        zhi=zhi+1
                end
                if num==b_num/2 then   --实际数量除以二是顺子的数量
                    keyiDuizi=true
                    break
                end
                end

                --对子判断
                for k,v in pairs(duizi) do
                    if keyiDuizi==false then
                        break
                    end
                    local shu=0
                    for i=1,#self._myPoker do     --对子判断
                        if v==self._myPoker[i]._panduanNum then
                            shu=shu+1
                            self._myPoker[i]._isXuanZhong=true
                            if shu==2 then
                                break
                            end
                    end
                    end
                    if shu<2 then   --连子有不是3个相连的跳出
                        isShunZi=false
                        break
                    else
                        if k==#duizi then   --如果是对子的最后一个
                            isShunZi=true
                            break
                        end
                    end
                end

                if num==b_num/2 and zhi<=14 and isShunZi==true then      --当顺子的个数大于等于别人顺子个数的时候跳出
                    isNeedZhaDan=false
                    break
                else
                    for k,v in pairs(self._myPoker) do
                        v._isXuanZhong=false
                    end
                end
            end
            self:mydump(duizi)
    end
    end

    if b_paiZhong==ddd_feiji then     --飞机
        if b_da<14 and b_num<#self._myPoker then    --A是顺子里面最大的
            local zhi=0
            local num=1
            local isShunZi=false    --判断是不是顺子
            local isDiZi=false    --判断是不是顺子
            local duizi={}
            local b_zuiXiao=b_da-b_num/3+1  --最小值
            for i=1,#self._myPoker-1 do
                if isShunZi==true then
                    break
                else
                    duizi={}
                end

                if self._myPoker[i]._panduanNum>b_zuiXiao then  --如果当前最大的值比别人顺子最大值小，直接返回false
                    zhi=self._myPoker[i]._panduanNum
                    num=1
                end
                local isFirst=true
                local keyiDuizi=false   --判断用不用进行对子判断
                for j=i+1,#self._myPoker do     --连子判断
                    if zhi+1==self._myPoker[j]._panduanNum then --如果出现比当前值小1的值，连子就+1
                        num=num+1
                        if isFirst==true then   --第一次进入添加最早的值
                            table.insert(duizi,zhi)
                        end
                        table.insert(duizi,zhi+1)
                        zhi=zhi+1
                end
                if num==b_num/3 then   --实际数量除以二是顺子的数量
                    keyiDuizi=true
                    break
                end
                end

                --对子判断
                for k,v in pairs(duizi) do
                    if keyiDuizi==false then
                        break
                    end
                    local shu=0
                    for i=1,#self._myPoker do     --对子判断
                        if v==self._myPoker[i]._panduanNum then
                            shu=shu+1
                            self._myPoker[i]._isXuanZhong=true
                            if shu==3 then
                                break
                            end
                    end
                    end
                    if shu<3 then   --连子有不是3个相连的跳出
                        isShunZi=false
                        break
                    else
                        if k==#duizi then   --如果是对子的最后一个
                            isShunZi=true
                            break
                        end
                    end
                end

                if num==b_num/3 and zhi<=14 and isShunZi==true then      --当顺子的个数大于等于别人顺子个数的时候跳出
                    isNeedZhaDan=false
                    break
                else
                    for k,v in pairs(self._myPoker) do
                        v._isXuanZhong=false
                    end
                end
            end
            self:mydump(duizi)
        end
    end
    
    if b_paiZhong==ddd_feiji1 then     --飞机带一
        if b_da<14 and b_num<#self._myPoker then    --A是顺子里面最大的
            local zhi=0
            local num=1
            local isShunZi=false    --判断是不是顺子
            local isDiZi=false    --判断是不是顺子
            local duizi={}
            local b_zuiXiao=b_da-b_num/4+1  --最小值
            for i=1,#self._myPoker-1 do
                if isShunZi==true then
                    break
                else
                    duizi={}
                end

                if self._myPoker[i]._panduanNum>b_zuiXiao then  --如果当前最大的值比别人顺子最大值小，直接返回false
                    zhi=self._myPoker[i]._panduanNum
                    num=1
                end
                local isFirst=true
                local keyiDuizi=false   --判断用不用进行对子判断
                for j=i+1,#self._myPoker do     --连子判断
                    if zhi+1==self._myPoker[j]._panduanNum then --如果出现比当前值小1的值，连子就+1
                        num=num+1
                        if isFirst==true then   --第一次进入添加最早的值
                            table.insert(duizi,zhi)
                        end
                        table.insert(duizi,zhi+1)
                        zhi=zhi+1
                end
                if num==b_num/4 then   --实际数量除以二是顺子的数量
                    keyiDuizi=true
                    break
                end
                end

                --对子判断
                for k,v in pairs(duizi) do
                    if keyiDuizi==false then
                        break
                    end
                    local shu=0
                    for i=1,#self._myPoker do     --对子判断
                        if v==self._myPoker[i]._panduanNum then
                            shu=shu+1
                            self._myPoker[i]._isXuanZhong=true
                            if shu==3 then
                                break
                            end
                    end
                    end
                    if shu<3 then   --连子有不是3个相连的跳出
                        isShunZi=false
                        break
                    else
                        if k==#duizi then   --如果是对子的最后一个
                            isShunZi=true
                            break
                        end
                    end
                end

                --获取能带的值的数
                local nengdaiTab={}
                local nengdaiNum=0
                local isNengDaiZhi=false
                local isChuLai=false
                for i=1,#self._myPoker do     --对子判断
                    if isChuLai==true then
                    	break
                    end
                    for k,v in pairs(duizi) do
                        if v==self._myPoker[i]._panduanNum then --如果出现飞机的值直接跳出
                            break
                        else
                            if k==#duizi then   --所有的遍历过后发现没有
                                local nengjiazhi=true  --能不能增加值
                                for k1,v1 in pairs(nengdaiTab) do   --判断这张牌计没计算过
                                    if self._myPoker[i]._panduanNum==v1 then
                                        nengjiazhi=false
                                    end
                                end
                                if nengjiazhi==true then    --这张牌没被计算过，添加到计算里面
                                    self._myPoker[i]._isXuanZhong=true
                                    nengdaiNum=nengdaiNum+1
                                    table.insert(nengdaiTab,self._myPoker[i]._panduanNum)
                                    if nengdaiNum==b_num/4 then
                                        isChuLai=true
                                        break
                                    end
                                end
                            end
                        end

                end
                end

                if nengdaiNum==b_num/4 then
                    isNengDaiZhi=true
                end
                self:myPrint("能出的牌的数量:"..nengdaiNum)

                if num==b_num/4 and zhi<=14 and isShunZi==true and isNengDaiZhi==true then      --当顺子的个数大于等于别人顺子个数的时候跳出
                    isNeedZhaDan=false
                    break
                else
                    for k,v in pairs(self._myPoker) do
                        v._isXuanZhong=false
                    end
                end
            end
            self:mydump(duizi)
        end
    end

    if b_paiZhong==ddd_feiji2 then     --飞机带对子
        if b_da<14 and b_num<#self._myPoker then    --A是顺子里面最大的
            local zhi=0
            local num=1
            local isShunZi=false    --判断是不是顺子
            local isDiZi=false    --判断是不是顺子
            local duizi={}
            local b_zuiXiao=b_da-b_num/5+1  --最小值
            for i=1,#self._myPoker-1 do
                if isShunZi==true then
                    break
                else
                    duizi={}
                end

                if self._myPoker[i]._panduanNum>b_zuiXiao then  --如果当前最大的值比别人顺子最大值小，直接返回false
                    zhi=self._myPoker[i]._panduanNum
                    num=1
                end
                local isFirst=true
                local keyiDuizi=false   --判断用不用进行对子判断
                for j=i+1,#self._myPoker do     --连子判断
                    if zhi+1==self._myPoker[j]._panduanNum then --如果出现比当前值小1的值，连子就+1
                        num=num+1
                        if isFirst==true then   --第一次进入添加最早的值
                            table.insert(duizi,zhi)
                        end
                        table.insert(duizi,zhi+1)
                        zhi=zhi+1
                end
                if num==b_num/5 then   --实际数量除以二是顺子的数量
                    keyiDuizi=true
                    break
                end
                end

                --对子判断
                for k,v in pairs(duizi) do
                    if keyiDuizi==false then
                        break
                    end
                    local shu=0
                    for i=1,#self._myPoker do     --对子判断
                        if v==self._myPoker[i]._panduanNum then
                            shu=shu+1
                            self._myPoker[i]._isXuanZhong=true
                            if shu==3 then
                                break
                            end
                    end
                    end
                    if shu<3 then   --连子有不是3个相连的跳出
                        isShunZi=false
                        break
                    else
                        if k==#duizi then   --如果是对子的最后一个
                            isShunZi=true
                            break
                        end
                    end
                end

                --获取能带的值的数
                local nengdaiTab={}
                local nengdaiNum=0
                local isNengDaiZhi=false
                local isChuLai=false
                for i=1,#self._myPoker do     --对子判断
                    for k,v in pairs(duizi) do
                        if v==self._myPoker[i]._panduanNum then --如果出现飞机的值直接跳出
                            break
                        else
                            if k==#duizi then   --所有的遍历过后发现没有
                                local nengjiazhi=true  --能不能增加值
                                for k1,v1 in pairs(nengdaiTab) do   --判断这张牌计没计算过
                                    if self._myPoker[i]._panduanNum==v1 then
                                        nengjiazhi=false
                                    end
                                end
                                if nengjiazhi==true then    --这张牌没被计算过，添加到计算里面
                                    nengdaiNum=nengdaiNum+1
                                    table.insert(nengdaiTab,self._myPoker[i]._panduanNum)
                                end
                            end
                        end
                    end
                end

                if nengdaiNum>=b_num/5 then
                    isNengDaiZhi=true
                end
                self:myPrint("能出的牌的数量:"..nengdaiNum)
                --self:mydump(nengdaiTab)
                --获取能带的数是不是对子和对子的数量
                local isNengDaiZhiNum=false
                local t_zongShu=0   --获取的对子的数量
                local isTiaoChu=false
                for k,v in pairs(nengdaiTab) do
                    if isTiaoChu==true then
                    	break
                    end
                    local shu=0
                    for i=1,#self._myPoker do     --对子判断
                        if v==self._myPoker[i]._panduanNum then
                            shu=shu+1
                        end                      
                        if shu==2 then
                            t_zongShu=t_zongShu+1
                            self._myPoker[i]._isXuanZhong=true
                            self._myPoker[i-1]._isXuanZhong=true
                            if t_zongShu==b_num/5 then
                                isTiaoChu=true                               
                            end
                            --print("对子："..v)
                            break
                        end
                    end
                end
                
                if t_zongShu==b_num/5 then    --能带的对子
                    isNengDaiZhiNum=true
                end
                self:myPrint("能出的牌的对子数量:"..t_zongShu)

                if num==b_num/5 and zhi<=14 and isShunZi==true and isNengDaiZhi==true and isNengDaiZhiNum==true then      --当顺子的个数大于等于别人顺子个数的时候跳出
                    isNeedZhaDan=false
                    break
                else
                    for k,v in pairs(self._myPoker) do
                        v._isXuanZhong=false
                    end
                end
            end
            self:mydump(duizi)
        end
    end



    if isNeedZhaDan==true then      --是否需要使用炸弹。
        if b_paiZhong~=ddd_zhadan then  --当别人的牌不是炸弹的时候
            local zhaDanNum=0
            local jiluNum=0
            local xuanZhongV=nil
            for k,v in pairs(self._myPoker) do      --找到炸弹
                if jiluNum==v._panduanNum then
                    zhaDanNum=zhaDanNum+1
                    self._paiDa=v._panduanNum
                    if zhaDanNum==3 then
                        xuanZhongV=v._panduanNum
                        break
                    end
            else
                jiluNum=v._panduanNum
                zhaDanNum=0
            end
            end
            for k,v in pairs(self._myPoker) do      --抬起炸弹
                if xuanZhongV==v._panduanNum then
                    v._isXuanZhong=true
                    isNeedShuangWang=false
            end
            end
    elseif b_paiZhong==ddd_zhadan then  --当别人的牌是炸弹的时候
        local zhaDanNum=0
        local jiluNum=0
        local xuanZhongV=nil
        for k,v in pairs(self._myPoker) do      --找到炸弹
            if jiluNum==v._panduanNum and v._panduanNum>b_da then   --自己的牌要比别人的炸弹大
                zhaDanNum=zhaDanNum+1
                self._paiDa=v._panduanNum
                if zhaDanNum==3 then
                    xuanZhongV=v._panduanNum
                end
        else
            jiluNum=v._panduanNum
            zhaDanNum=0
        end
        end
        for k,v in pairs(self._myPoker) do      --抬起炸弹
            if xuanZhongV==v._panduanNum then
                v._isXuanZhong=true
                isNeedShuangWang=false
        end
        end

    end
    else    --如果不需要炸弹，那么也不需要双王炸弹
        isNeedShuangWang=false
    end

    self:pokerPaiXu(self._myPoker)  --改回从大到小
    if isNeedShuangWang==true then  --看需不需出双王
        if self._myPoker[1]._panduanNum==dawangNum and self._myPoker[2]._panduanNum==xiaowangNum then   --判断有没有双王
            self:myPrint("自己有王炸")
            self._myPoker[1]._isXuanZhong=true
            self._myPoker[2]._isXuanZhong=true
    end
    end

    self:shangTai()                 --选中的牌上抬
    if #self._panduanPoker~=0 then  --当别人有出牌的时候
        local isChuPai=self:comparePokers()       --判断能不能出牌
        self._chupaiBtn:setTouchEnabled(isChuPai)
        self._chupaiBtn:setBright(isChuPai)
    else                            --自己出牌的时候
        local isChuPai=self:choiceXuanZe()       --判断能不能出牌
        self._chupaiBtn:setTouchEnabled(isChuPai)
        self._chupaiBtn:setBright(isChuPai)
    end
end

--判断自己有没有牌比别人的牌大
function GameDDZ:isSetPoker()
    --别人的牌
    local ceshitable={}
    --dump(self._panduanPoker)
    if #self._panduanPoker==0 then
        self:myPrint("其他玩家没有出牌")
        return true
    end

    for k,v in pairs(self._panduanPoker) do
        ceshitable[k]={}
        ceshitable[k]["dianshu"]=v._panduanNum
    end

    local b_paiZhong=self:paiZhongLei(ceshitable)
    local b_da=self._paiDa
    local b_num=#ceshitable

    self:myPrint("别人的牌")
    self:dayinPaiZhong(b_paiZhong)

    --如果别人是王炸直接返回false
    if b_paiZhong==ddd_wangzhadan then
        return false
    end
    --自己有王炸
    if #self._myPoker>=2 then
        if self._myPoker[1]._panduanNum==dawangNum and self._myPoker[2]._panduanNum==xiaowangNum then
            self:myPrint("自己有王炸")
            return true
        end
    end

    if b_paiZhong~=ddd_zhadan then      --当别人不是炸弹的时候，自己有炸弹的时候
        if self:isMyZhaDan()==true then
            self:myPrint("自己有炸弹")
            return true
    end
    end

    if b_paiZhong==ddd_zhadan then      --当别人的牌是炸弹的时候
        if self:isMyZhaDan()==true then
            if self._paiDa>b_da then
                self:myPrint("自己的炸弹比别人炸弹打")
                return true
            end
    end
    self:myPrint("别人是炸弹")
    return false
    end

    if b_paiZhong==ddd_danpai then      --单牌判断
        for k,v in pairs(self._myPoker) do
            if b_da<v._panduanNum then
                return true
            end
    end
    return false
    end

    if b_paiZhong==ddd_duizi then       --对子判断
        local duiNum=0
        for k,v in pairs(self._myPoker) do
            if duiNum==v._panduanNum then
                if b_da<v._panduanNum then
                    return true
                end
            else
                duiNum=v._panduanNum
            end
        end
        return false
    end

    if b_paiZhong==ddd_sanbudai then    --三不带判断
        if self:isMySanGe()==true then
            if self._paiDa>b_da then
                return true
            end
    end
    return false
    end

    if b_paiZhong==ddd_sandaiyi then    --三带一
        if self:isMySanGe()==true then
            if self._paiDa>b_da then
                if #self._myPoker>=4 then   --总牌数大于4张
                    return true
                end
            end
    end
    return false
    end

    if b_paiZhong==ddd_sandaier then    --三带对子
        if self:isMySanGe()==true then
            if self._paiDa>b_da then
                if self:isMySanDaiYi(self._paiDa)==true then    --获取对子
                    return true
                end
            end
    end
    return false
    end

    if b_paiZhong==ddd_shunzi then     --顺子
        if b_da==14 then    --A是顺子里面最大的
            return false
    end
    if self:isMyShunZi(b_da,b_num)==true then   --获取顺子的比较
        return true
    end
    return false
    end

    if b_paiZhong==ddd_liandui then     --连对
        if b_da==14 then    --A是顺子里面最大的
            return false
    end
    if self:isMyLianDui(b_da,b_num)==true then   --获取连对的比较
        return true
    end
    return false
    end

    if b_paiZhong==ddd_feiji then     --飞机
        if b_da==14 then    --A是顺子里面最大的
            return false
    end
    if self:isMyFeiJi(b_da,b_num)==true then   --获取飞机的比较
        return true
    end
    return false
    end

    if b_paiZhong==ddd_feiji1 then     --飞机带一
        if b_da==14 then    --A是顺子里面最大的
            return false
    end
    if b_num>#self._myPoker then    --当飞机的出牌数，比玩家牌的总数都要大的时候直接返回false
        return false
    end
    if self:isMyFeiJiDaiYi(b_da,b_num) then   --获取飞机的比较
        return true
    end
    return false
    end

    if b_paiZhong==ddd_feiji2 then     --飞机带对子
        if b_da==14 then    --A是顺子里面最大的
            return false
    end
    if b_num>#self._myPoker then    --当飞机的出牌数，比玩家牌的总数都要大的时候直接返回false
        return false
    end
    if self:isMyFeiJiDaiDuiZi(b_da,b_num) then   --获取飞机的比较
        return true
    end
    return false
    end

    if b_paiZhong==ddd_sidaier then     --四带二
        if self:isMySiGe()==true then   --获取四个牌
            if self._paiDa>b_da then
                if self:isMySiDaiEr(self._paiDa)==true then --四带二的两个单数
                    return true
                end
        end
    end
    return false
    end

    if b_paiZhong==ddd_sidaidui then     --四带二对子
        if self:isMySiGe()==true then   --获取四个牌
            if self._paiDa>b_da then
                if self:isMySiDaiDuiZi(self._paiDa)==true then  --四带对子的两个对子
                    return true
                end
        end
    end
    return false
    end


    return false
end

--获取飞机带对子的比较
function GameDDZ:isMyFeiJiDaiDuiZi(b_da,b_num)
    local zhi=0
    local num=1
    local isShunZi=false        --判断是不是顺子
    local isDiZi=false          --判断是不是对子
    local duizi={}
    local tb_num=b_num-b_num/5*2
    local isNengDaiZhi=false    --判断能带的值够不够
    for i=1,#self._myPoker-1 do
        if isShunZi==true then
            break
        else
            duizi={}
        end
        if self._myPoker[i]._panduanNum<=b_da then  --如果当前最大的值比别人顺子最大值小，直接返回false
            return false
        else
            zhi=self._myPoker[i]._panduanNum
            num=1
        end

        local isFirst=true
        local keyiDuizi=false   --判断用不用进行对子判断
        for j=i+1,#self._myPoker do     --连子判断
            if zhi-1==self._myPoker[j]._panduanNum then --如果出现比当前值小1的值，连子就+1
                num=num+1
                if isFirst==true then   --第一次进入添加最早的值
                    table.insert(duizi,zhi)
                end
                table.insert(duizi,zhi-1)
                zhi=zhi-1
        end
        if num>=tb_num/3 then   --实际数量除以二是顺子的数量
            keyiDuizi=true
            break
        end
        if zhi>14 then  --当zhi比A大的时候
            break
        end
        end


        --对子判断
        for k,v in pairs(duizi) do
            if keyiDuizi==false then
                break
            end
            local shu=0
            for i=1,#self._myPoker do     --对子判断
                if v==self._myPoker[i]._panduanNum then
                    shu=shu+1
            end
            end
            if shu<3 then   --连子有不是3个相连的跳出
                isShunZi=false
                break
            else
                if k==#duizi then   --如果是对子的最后一个
                    isShunZi=true
                    break
                end
            end
        end

    end
    self:mydump(duizi)
    isDiZi=true --成功出来那么是成对的

    --获取能带的单数值的数
    local nengdaiTab={}
    local nengdaiNum=0

    for i=1,#self._myPoker do     --对子判断
        for k,v in pairs(duizi) do
            local nengjiazhi=true  --能不能增加值
            if v~=self._myPoker[i]._panduanNum then --牌不是飞机的数
                if k==#duizi then
                    for k1,v1 in pairs(nengdaiTab) do   --判断这张牌计没计算过
                        if self._myPoker[i]._panduanNum==v1 then
                            nengjiazhi=false
                    end
                    end
                    if nengjiazhi==true then    --这张牌没被计算过，添加到计算里面
                        nengdaiNum=nengdaiNum+1
                        table.insert(nengdaiTab,self._myPoker[i]._panduanNum)
                    end
            end
            else
                break
            end
    end
    end

    if nengdaiNum>=b_num/5 then --*****重点 这里获取的是能带的单数的个数
        isNengDaiZhi=true
    end

    self:myPrint("能出的牌的数量:"..nengdaiNum)
    --self:mydump(nengdaiTab)
    --获取能带的数是不是对子和对子的数量
    local isNengDaiZhiNum=false
    local t_zongShu=0   --获取的对子的数量
    for k,v in pairs(nengdaiTab) do
        local shu=0
        for i=1,#self._myPoker do     --对子判断
            if v==self._myPoker[i]._panduanNum then
                shu=shu+1
        end
        end
        if shu>=2 then
            t_zongShu=t_zongShu+1
        end
    end

    if t_zongShu>=b_num/5 then    --能带的对子
        isNengDaiZhiNum=true
    end
    self:myPrint("能出的牌的对子数量:"..t_zongShu)

    --总判断
    if isShunZi==true and isDiZi==true and isNengDaiZhi==true and isNengDaiZhiNum==true then
        return true
    end

    return false
end

--获取飞机带一的比较
function GameDDZ:isMyFeiJiDaiYi(b_da,b_num)
    local zhi=0
    local num=1
    local isShunZi=false        --判断是不是顺子
    local isDiZi=false          --判断是不是对子
    local duizi={}
    local tb_num=b_num-b_num/4
    local isNengDaiZhi=false    --判断能带的值够不够
    for i=1,#self._myPoker-1 do
        if isShunZi==true then
            break
        else
            duizi={}
        end
        if self._myPoker[i]._panduanNum<=b_da then  --如果当前最大的值比别人顺子最大值小，直接返回false
            return false
        else
            zhi=self._myPoker[i]._panduanNum
            num=1
        end

        local isFirst=true
        local keyiDuizi=false   --判断用不用进行对子判断
        for j=i+1,#self._myPoker do     --连子判断
            if zhi-1==self._myPoker[j]._panduanNum then --如果出现比当前值小1的值，连子就+1
                num=num+1
                if isFirst==true then   --第一次进入添加最早的值
                    table.insert(duizi,zhi)
                end
                table.insert(duizi,zhi-1)
                zhi=zhi-1
        end
        if num>=tb_num/3 then   --实际数量除以二是顺子的数量
            keyiDuizi=true
            break
        end
        if zhi>14 then  --当zhi比A大的时候
            break
        end
        end


        --对子判断
        for k,v in pairs(duizi) do
            if keyiDuizi==false then
                break
            end
            local shu=0
            for i=1,#self._myPoker do     --对子判断
                if v==self._myPoker[i]._panduanNum then
                    shu=shu+1
            end
            end
            if shu<3 then   --连子有不是3个相连的跳出
                isShunZi=false
                break
            else
                if k==#duizi then   --如果是对子的最后一个
                    isShunZi=true
                    break
                end
            end
        end


    end
    self:mydump(duizi)
    isDiZi=true --成功出来那么是成对的

    --获取能带的值的数
    local nengdaiTab={}
    local nengdaiNum=0

    for i=1,#self._myPoker do     --对子判断
        for k,v in pairs(duizi) do
            if v==self._myPoker[i]._panduanNum then --如果出现飞机的值直接跳出
                break
            else
                if k==#duizi then   --所有的遍历过后发现没有
                    local nengjiazhi=true  --能不能增加值
                    for k1,v1 in pairs(nengdaiTab) do   --判断这张牌计没计算过
                        if self._myPoker[i]._panduanNum==v1 then
                            nengjiazhi=false
                    end
                    end
                    if nengjiazhi==true then    --这张牌没被计算过，添加到计算里面
                        nengdaiNum=nengdaiNum+1
                        table.insert(nengdaiTab,self._myPoker[i]._panduanNum)
                    end
                end
            end

    end
    end

    if nengdaiNum>=b_num/4 then
        isNengDaiZhi=true
    end
    self:myPrint("能出的牌的数量:"..nengdaiNum)
    --self:mydump(nengdaiTab)
    --总判断
    if isShunZi==true and isDiZi==true and isNengDaiZhi==true then
        return true
    end

    return false
end

--获取飞机的比较
function GameDDZ:isMyFeiJi(b_da,b_num)
    local zhi=0
    local num=1
    local isShunZi=false    --判断是不是顺子
    local isDiZi=false      --判断是不是对子
    local duizi={}
    for i=1,#self._myPoker-1 do
        if isShunZi==true then
            break
        else
            duizi={}
        end
        if self._myPoker[i]._panduanNum<=b_da then  --如果当前最大的值比别人顺子最大值小，直接返回false
            return false
        else
            zhi=self._myPoker[i]._panduanNum
            num=1
        end

        local isFirst=true
        local keyiDuizi=false   --判断用不用进行对子判断
        for j=i+1,#self._myPoker do     --连子判断
            if zhi-1==self._myPoker[j]._panduanNum then --如果出现比当前值小1的值，连子就+1
                num=num+1
                if isFirst==true then   --第一次进入添加最早的值
                    table.insert(duizi,zhi)
                end
                table.insert(duizi,zhi-1)
                zhi=zhi-1
        end
        if num>=b_num/3 then   --实际数量除以二是顺子的数量
            keyiDuizi=true
            break
        end
        if zhi>14 then  --当zhi比A大的时候
            break
        end
        end

        --对子判断
        for k,v in pairs(duizi) do
            if keyiDuizi==false then
                break
            end
            local shu=0
            for i=1,#self._myPoker do     --对子判断
                if v==self._myPoker[i]._panduanNum then
                    shu=shu+1
            end
            end
            if shu<3 then   --连子有不是3个相连的跳出
                isShunZi=false
                break
            else
                if k==#duizi then   --如果是对子的最后一个
                    isShunZi=true
                    break
                end
            end
        end

    end
    self:mydump(duizi)
    isDiZi=true --成功出来那么是成对的

    --总判断
    if isShunZi==true and isDiZi==true then
        return true
    end

    return false
end

--获取连对的比较
function GameDDZ:isMyLianDui(b_da,b_num)
    local zhi=0
    local num=1
    local isShunZi=false    --判断是不是顺子
    local isDiZi=false    --判断是不是顺子
    local duizi={}
    for i=1,#self._myPoker-1 do
        if isShunZi==true then
            break
        else
            duizi={}
        end

        if self._myPoker[i]._panduanNum<=b_da then  --如果当前最大的值比别人顺子最大值小，直接返回false
            return false
        else
            zhi=self._myPoker[i]._panduanNum
            num=1
        end
        local isFirst=true
        local keyiDuizi=false   --判断用不用进行对子判断
        for j=i+1,#self._myPoker do     --连子判断
            if zhi-1==self._myPoker[j]._panduanNum then --如果出现比当前值小1的值，连子就+1
                num=num+1
                if isFirst==true then   --第一次进入添加最早的值
                    table.insert(duizi,zhi)
                end
                table.insert(duizi,zhi-1)
                zhi=zhi-1
        end
        if num>=b_num/2 then   --实际数量除以二是顺子的数量
            keyiDuizi=true
            break
        end
        if zhi>14 then  --当zhi比A大的时候
            break
        end
        end

        --对子判断
        for k,v in pairs(duizi) do
            if keyiDuizi==false then
                break
            end
            local shu=0
            for i=1,#self._myPoker do     --对子判断
                if v==self._myPoker[i]._panduanNum then
                    shu=shu+1
            end
            end
            if shu<2 then   --连子有不是3个相连的跳出
                isShunZi=false
                break
            else
                if k==#duizi then   --如果是对子的最后一个
                    isShunZi=true
                    break
                end
            end
        end

    end
    self:myPrint("连对判断")
    self:mydump(duizi)
    isDiZi=true --成功出来那么是成对的

    --总判断
    if isShunZi==true and isDiZi==true then
        return true
    end

    return false
end

--获取顺子的比较
function GameDDZ:isMyShunZi(b_da,b_num)
    local zhi=0
    local num=1
    for i=1,#self._myPoker-1 do
        if self._myPoker[i]._panduanNum<=b_da then  --如果当前最大的值比别人顺子最大值小，直接返回false
            return false
        else
            zhi=self._myPoker[i]._panduanNum
            num=1
        end

        for j=i+1,#self._myPoker do     --连子判断
            if zhi-1==self._myPoker[j]._panduanNum then --如果出现比当前值小1的值，连子就+1
                num=num+1
                zhi=zhi-1
        end
        if num>=b_num then
            return true
        end
        if zhi>14 then  --当zhi比A大的时候
            break
        end
        end
    end


    return false
end

--判断自己的牌里四带对子的两个对子
function GameDDZ:isMySiDaiDuiZi(num)
    local zhi1=0
    local zhi2=0

    local isDui1=true   --获取第一个对子
    for k,v in pairs(self._myPoker) do
        if v._panduanNum~=num then
            if isDui1 then      --是不是在获取第一个对子
                if zhi1~=v._panduanNum then
                    zhi1=v._panduanNum
            else
                isDui1=false
            end
            else
                if zhi1~=v._panduanNum then
                    if zhi2~=v._panduanNum then
                        zhi2=v._panduanNum
                    else
                        return true
                    end
                end

            end

        end
    end
    return false
end

--判断自己的牌里四带二的两个的值
function GameDDZ:isMySiDaiEr(num)
    local zhi1=0
    local zhi2=0
    for k,v in pairs(self._myPoker) do
        if v._panduanNum~=num then
            if zhi1==0 then
                zhi1=v._panduanNum
            else
                zhi2=v._panduanNum
                if zhi1~=zhi2 then
                    return true
                end
            end
        end
    end
    return false
end

--判断自己的牌里有没有四个
function GameDDZ:isMySiGe()
    local duiNum=0
    local duiNum2=0
    local duiNum3=0
    for k,v in pairs(self._myPoker) do
        if duiNum==v._panduanNum then
            if duiNum2==v._panduanNum then
                if duiNum3==v._panduanNum then
                    self._paiDa=v._panduanNum
                    return true
                else
                    duiNum3=v._panduanNum
                end

            else
                duiNum2=v._panduanNum
            end

        else
            duiNum=v._panduanNum
        end
    end

    return false
end

--判断自己牌里有没有三带对子,的对子判断
function GameDDZ:isMySanDaiYi(num)
    local duiNum=0
    for k,v in pairs(self._myPoker) do
        if duiNum==v._panduanNum then
            if num~=v._panduanNum then
                return true
            end
        else
            duiNum=v._panduanNum
        end
    end
    return false
end

--判断自己的牌里有没有三个
function GameDDZ:isMySanGe()
    local duiNum=0
    local duiNum2=0
    for k,v in pairs(self._myPoker) do
        if duiNum==v._panduanNum then
            if duiNum2==v._panduanNum then
                self._paiDa=v._panduanNum
                return true
            else
                duiNum2=v._panduanNum
            end

        else
            duiNum=v._panduanNum
        end
    end

    return false
end

--判断自己的牌里有没有炸弹
function GameDDZ:isMyZhaDan()
    local zhaDanNum=0
    local jiluNum=0
    for k,v in pairs(self._myPoker) do
        if jiluNum==v._panduanNum then
            zhaDanNum=zhaDanNum+1
            self._paiDa=v._panduanNum
            if zhaDanNum==3 then
                return true
            end
        else
            jiluNum=v._panduanNum
            zhaDanNum=0
        end

    end

    return false
end

--自己选择出牌的判断
function GameDDZ:choiceXuanZe()
    local ceshitable={}
    local chupaiTable={}
    for k,v in pairs(self._myPoker) do      --确认要发的牌
        if v._isTaiQi==true then
            table.insert(chupaiTable,v)
    end
    end

    for k,v in pairs(chupaiTable) do        --创建图片
        ceshitable[k]={}
        ceshitable[k]["dianshu"]=v._panduanNum
    end

    local a_paiZhong=self:paiZhongLei(ceshitable)
    local a_da=self._paiDa
    local a_num=#ceshitable
    self:myPrint("自己的牌")
    self:dayinPaiZhong(a_paiZhong)
    if a_paiZhong>=0 and a_paiZhong <=13 then
        return true
    end

    return false
end

--比较自己的牌与前面的牌的大小
function GameDDZ:comparePokers()
    --别人的牌
    local ceshitable={}
    --dump(self._panduanPoker)
    if #self._panduanPoker==0 then
        self:myPrint("其他玩家没有出牌")
        return true
    end

    for k,v in pairs(self._panduanPoker) do
        ceshitable[k]={}
        ceshitable[k]["dianshu"]=v._panduanNum
    end

    local b_paiZhong=self:paiZhongLei(ceshitable)
    local b_da=self._paiDa
    local b_num=#ceshitable
    self:myPrint("-------------------------")
    self:myPrint("别人的牌")
    self:dayinPaiZhong(b_paiZhong)

    --自己的牌
    ceshitable={}
    local chupaiTable={}
    for k,v in pairs(self._myPoker) do      --确认要发的牌
        if v._isTaiQi==true then
            table.insert(chupaiTable,v)
    end
    end

    for k,v in pairs(chupaiTable) do
        ceshitable[k]={}
        ceshitable[k]["dianshu"]=v._panduanNum
    end

    local a_paiZhong=self:paiZhongLei(ceshitable)
    local a_da=self._paiDa
    local a_num=#ceshitable
    self:myPrint("自己的牌")
    self:dayinPaiZhong(a_paiZhong)



    --牌大小比较
    if a_paiZhong==ddd_wangzhadan then  --如果自己是王炸，那么可以出牌
        return true
    end

    if b_paiZhong==ddd_wangzhadan then  --如果别人是王炸，那么不能出牌
        return false
    end

    if b_paiZhong==ddd_zhadan and a_paiZhong==ddd_zhadan and a_da>b_da then --同为炸弹的比较
        return true
    end

    if b_paiZhong~=ddd_wangzhadan and b_paiZhong~=ddd_zhadan  then   --当别人的牌不为炸弹并且不为双王的时候
        if a_paiZhong==ddd_zhadan then  --自己是炸弹的时候
            return true
    elseif b_paiZhong==a_paiZhong and a_da>b_da and a_num==b_num then
        return true
    else
        self:myPrint("牌种不正确a:"..a_paiZhong.."b:"..b_paiZhong.."或者数量不对a:"..a_num.."b:"..b_num.."或者点数大小不对a:"..a_da.."b:"..b_da)
    end
    end



    return false
end

--获取出牌种类
function GameDDZ:paiZhongLei(pokers)
    local shuNum=#pokers
    if shuNum==1 then   --1张牌
        self._paiDa=pokers[1]["dianshu"]
        return ddd_danpai
    end

    if shuNum==2 then   --2张牌
        if pokers[1]["dianshu"]==dawangNum and pokers[2]["dianshu"]==xiaowangNum  then  --双王
            self._paiDa=pokers[1]["dianshu"]
            return ddd_wangzhadan
    end
    if pokers[1]["dianshu"]==pokers[2]["dianshu"] then  --对子
        self._paiDa=pokers[1]["dianshu"]
        return ddd_duizi
    end
    end

    if shuNum==3 then   --3张牌
        if pokers[1]["dianshu"]==pokers[2]["dianshu"] and pokers[1]["dianshu"]==pokers[3]["dianshu"] then  --3不带
            self._paiDa=pokers[1]["dianshu"]
            return ddd_sanbudai
    end
    end

    if shuNum==4 then   --4张牌
        if pokers[1]["dianshu"]==pokers[2]["dianshu"] and pokers[1]["dianshu"]==pokers[3]["dianshu"]    --炸弹
            and pokers[1]["dianshu"]==pokers[4]["dianshu"] then
        self._paiDa=pokers[1]["dianshu"]
        return ddd_zhadan
    end
    if pokers[1]["dianshu"]==pokers[2]["dianshu"] and pokers[1]["dianshu"]==pokers[3]["dianshu"] then  --3带1
        self._paiDa=pokers[1]["dianshu"]
        return ddd_sandaiyi
    end

    if pokers[4]["dianshu"]==pokers[2]["dianshu"] and pokers[4]["dianshu"]==pokers[3]["dianshu"] then  --3带1
        self._paiDa=pokers[4]["dianshu"]
        return ddd_sandaiyi
    end
    end

    if shuNum==5 then   --5张牌
        if self:isLianZi(pokers)==true then     --顺子
            return ddd_shunzi
    end

    if pokers[1]["dianshu"]==pokers[2]["dianshu"] and pokers[1]["dianshu"]==pokers[3]["dianshu"]
        and pokers[4]["dianshu"]==pokers[5]["dianshu"] then  --3带2
        self._paiDa=pokers[1]["dianshu"]
        return ddd_sandaier
    end

    if pokers[5]["dianshu"]==pokers[3]["dianshu"] and pokers[5]["dianshu"]==pokers[4]["dianshu"]
        and pokers[1]["dianshu"]==pokers[2]["dianshu"] then  --3带2
        self._paiDa=pokers[5]["dianshu"]
        return ddd_sandaier
    end
    end

    if shuNum>5 then
        if self:isLianZi(pokers)==true then     --顺子
            return ddd_shunzi
        end

        if self:isLianDui(pokers)==true then    --连对
            return ddd_liandui
        end

        if self:isFeiJi(pokers)==true then      --飞机
            return ddd_feiji
        end

    end

    if shuNum>=8 then
        if self:isFeiJi1(pokers)==true then      --飞机带1
            return ddd_feiji1
        end
    end

    if shuNum>=10 then
        if self:isFeiJi2(pokers)==true then      --飞机带对子
            return ddd_feiji2
        end
    end


    if shuNum==6 then   --4带2
        if self:isSiDaiEr(pokers)==true then
            return ddd_sidaier
    end
    end

    if shuNum==8 then   --4带两对
        if self:isSiDaiDui(pokers)==true then
            return ddd_sidaidui
    end
    end

    self._paiDa=0
    return ddd_meiyou --没有牌
end

--是不是飞机
function GameDDZ:isFeiJi(pokers)
    local shuNum=#pokers
    local hang, lie = math.modf(shuNum/3)     --整数部分是hang ,lie小数部分是列
    if lie~=0 then
        return false
    end
    local sanLian=0
    local isSanGe=false     --是不是3个相同的值
    local isLianZi=false
    for i=1,shuNum do       --判断是不是每3个数相同
        local hang, lie = math.modf(i/3)
        if lie<0.4 and lie~=0 then
            sanLian=pokers[i]["dianshu"]
        end
        if lie>0.6 or lie==0 then
            if pokers[i]["dianshu"]~=sanLian then
                --self:myPrint("3连对出错")
                return false
            else
                if i==shuNum then
                    isSanGe=true
                end
            end
        end
    end

    local lianziNum=0
    for i=3,shuNum, 3 do        --每次+3看是不是顺子
        if pokers[i]["dianshu"]>14 then      --3-13 A=14 2=15 w=16,17
            self:myPrint("连对，顺子不能包含2和王")
            return false
    end
    if i==3 then
        lianziNum=pokers[i]["dianshu"]
    end
    if i>=6 then
        if lianziNum==pokers[i]["dianshu"]+1 then   --判断下一个是不是比上一个小一
            lianziNum=pokers[i]["dianshu"]
            if i==shuNum then
                isLianZi=true
            end
        else
            --self:myPrint("不是连对,顺子出错")
            return false
        end
    end
    end


    if isSanGe==true and isLianZi==true then
        self._paiDa=pokers[1]["dianshu"]
        return true
    end

    return false
end

--是不是飞机1
function GameDDZ:isFeiJi1(pokers)
    local daiShu={}     --存放带的数
    local daiShuNum=1   --代数的个数
    local zhiPokers={}  --存放飞机
    local isDaiShu=false
    local shuNum=#pokers
    local hang, lie = math.modf(shuNum/4)     --整数部分是hang ,lie小数部分是列
    if lie~=0 then
        return false
    end

    for i=1,shuNum do        --找出带的牌
        for j=1,shuNum do
            if pokers[i]["dianshu"]==pokers[j]["dianshu"] then
                if i~=j then
                    break
                end
                if i==shuNum and j==i then  --最后一张
                    daiShu[daiShuNum]=pokers[i]
                end
            else
                if j==shuNum then
                    daiShu[daiShuNum]=pokers[i]
                    daiShuNum=daiShuNum+1
                end
            end
    end
    end
    --self:myPrint("带的数")
    --dump(daiShu)
    daiShuNum=#daiShu
    if daiShuNum==shuNum/4 then
        isDaiShu=true
    end

    local zhiPokerNum=1
    for i=1,shuNum do        --找出带的牌
        for j=1,daiShuNum do        --找出带的牌
            if pokers[i]["dianshu"]==daiShu[j]["dianshu"] then
                break
        else
            if j==daiShuNum then
                zhiPokers[zhiPokerNum]=pokers[i]
                zhiPokerNum=zhiPokerNum+1
            end
        end
    end
    end

    --dump(zhiPokers)

    if isDaiShu==true and self:isFeiJi(zhiPokers)==true then    --带的数，和飞机相同
        return true
    else
    --self:myPrint("带的数不正确，或者飞机不正确")
    end

    return false
end

--是不是飞机2
function GameDDZ:isFeiJi2(pokers)

    local daiShu={}     --存放带的数
    local daiShuNum=1   --代数的个数
    local zhiPokers={}  --存放飞机
    local isDaiShu=false
    local shuNum=#pokers
    local hang, lie = math.modf(shuNum/5)     --整数部分是hang ,lie小数部分是列
    if lie~=0 then
        return false
    end

    for i=1,shuNum do        --找出带的牌
        local jishuNum=0
        for j=1,shuNum do
            if pokers[i]["dianshu"]==pokers[j]["dianshu"] then
                jishuNum=jishuNum+1
            end
            if j==shuNum and jishuNum==2 then
                daiShu[daiShuNum]=pokers[i]
                daiShuNum=daiShuNum+1
            end
        end
    end

    --self:myPrint("带的数")
    --dump(daiShu)
    daiShuNum=#daiShu
    if daiShuNum==shuNum/5*2 then
        isDaiShu=true
    end

    local zhiPokerNum=1
    for i=1,shuNum do        --找出带的牌
        for j=1,daiShuNum do        --找出带的牌
            if pokers[i]["dianshu"]==daiShu[j]["dianshu"] then
                break
        else
            if j==daiShuNum then
                zhiPokers[zhiPokerNum]=pokers[i]
                zhiPokerNum=zhiPokerNum+1
            end
        end
    end
    end

    --dump(zhiPokers)

    if isDaiShu==true and self:isFeiJi(zhiPokers)==true then    --带的数，和飞机相同
        return true
    else
        self:myPrint("带的数2不正确，或者飞机2不正确")
    end

    return false
end

--是不是四带对
function GameDDZ:isSiDaiDui(pokers)
    local poker1=0
    local poker2=0
    local poker3=0
    local shu1=0
    local shu2=0
    local shu3=0
    local isDui1=false
    local isDui2=false
    local isDui3=false
    for i=1,8 do
        if poker1==pokers[i]["dianshu"] then
            shu1=shu1+1
        end

        if poker2==pokers[i]["dianshu"] then
            shu2=shu2+1
        end

        if poker3==pokers[i]["dianshu"] then
            shu3=shu3+1
        end

        if poker3==0 and poker2~=0 then
            if pokers[i]["dianshu"]~=poker1 and pokers[i]["dianshu"]~=poker2 then
                poker3=pokers[i]["dianshu"]
                shu3=shu3+1
            end
        end

        if poker2==0 and poker1~=0 then
            if pokers[i]["dianshu"]~=poker1 then
                poker2=pokers[i]["dianshu"]
                shu2=shu2+1
            end
        end

        if poker1==0 then
            poker1=pokers[1]["dianshu"]
            shu1=shu1+1
        end
    end

    if shu1==2 or shu1==4 then
        isDui1=true
    end

    if shu2==2 or shu2==4 then
        isDui2=true
    end

    if shu3==2 or shu3==4 then
        isDui3=true
    end

    if isDui1==true and isDui2==true and isDui3==true then
        if shu1+shu2+shu3==8 then
            if shu1==4 then
                self._paiDa=poker1
            end
            if shu2==4 then
                self._paiDa=poker2
            end
            if shu3==4 then
                self._paiDa=poker3
            end

            return true
        end
    end


    return false
end

--是不是四带二
function GameDDZ:isSiDaiEr(pokers)
    local numFour=0

    --获取4带数字
    local fourZhi=0 --4带的数字
    local fourZhi1=pokers[1]["dianshu"]
    local fourZhi2=pokers[2]["dianshu"]
    local fourZhi3=pokers[3]["dianshu"]
    if fourZhi1==fourZhi2 then
        fourZhi=fourZhi1
    end
    if fourZhi1==fourZhi3 then
        fourZhi=fourZhi1
    end
    if fourZhi2==fourZhi3 then
        fourZhi=fourZhi2
    end
    if fourZhi==0 then
        for i=1,3 do
            if pokers[4]["dianshu"]==pokers[i]["dianshu"] then
                fourZhi=pokers[4]["dianshu"]
            end
        end
    end

    --获取带的牌
    local no1=0 --带的牌1
    local no2=0 --带的牌2
    local is_first=true
    for i=1,6 do
        if pokers[i]["dianshu"]~=fourZhi then
            if is_first==true then
                no1=pokers[i]["dianshu"]
                is_first=false
            else
                no2=pokers[i]["dianshu"]
            end
        end
    end

    if no1==no2 then
        self:myPrint("带的两个值一样了")
    else
        for i=1,6 do
            if pokers[i]["dianshu"]==fourZhi then
                numFour=numFour+1
            end
            if numFour==4 then
                self._paiDa=fourZhi
                return true
            end
        end
    end

    return false
end

--是不是连对
function GameDDZ:isLianDui(pokers)
    local islianzi1=false
    local isduizi=false
    local shuNum=#pokers
    local hang, lie = math.modf(shuNum/2)     --整数部分是hang ,lie小数部分是列
    if lie~=0 then
        return false
    end

    local lianziNum=0
    for i=2,shuNum, 2 do        --每次+2看是不是顺子
        if pokers[i]["dianshu"]>14 then      --3-13 A=14 2=15 w=16,17
            self:myPrint("连对，顺子不能包含2和王")
            return false
    end
    if i==2 then
        lianziNum=pokers[i]["dianshu"]
    end
    if i>=4 then
        if lianziNum==pokers[i]["dianshu"]+1 then   --判断下一个是不是比上一个小一
            lianziNum=pokers[i]["dianshu"]
            if i==shuNum then
                islianzi1=true
            end
        else
            --self:myPrint("不是连对,顺子出错")
            return false
        end
    end
    end

    for i=1,shuNum  do  --看每两个是不是相等
        local zhenshu,is_shuangshu=math.modf(i/2)
        if is_shuangshu ==0 then
            if pokers[i]["dianshu"]~=pokers[i-1]["dianshu"] then
                isduizi=false
                --self:myPrint("不是连对,对子出错")
                break
            end
            if i==shuNum then
                isduizi=true
            end
        end
    end

    if islianzi1==true and isduizi==true then    --是顺子也是对子
        self._paiDa=pokers[1]["dianshu"]
        return true
    end

    return false
end


--是不是顺子
function GameDDZ:isLianZi(pokers)
    local shuNum=#pokers
    local lianziNum=0
    for i=1,shuNum do
        if pokers[i]["dianshu"]>14 then      --3-13 A=14 2=15 w=16,17
            self:myPrint("顺子不能包含2和王")
            return false
        end
        if i==1 then
            lianziNum=pokers[i]["dianshu"]
        end
        if i>=2 then
            if lianziNum==pokers[i]["dianshu"]+1 then   --判断下一个是不是比上一个小一
                lianziNum=pokers[i]["dianshu"]
                if i==shuNum then
                    self._paiDa=pokers[1]["dianshu"]
                    return true
                end
            else
                --print("不是顺子")
                return false
            end

        end
    end

    return false

end

--设置玩家信息
function GameDDZ:sheZhiXinxi(playerNum,name,vip,coin)
    local playerDuiHua=nil
    if playerNum==1 then
        playerDuiHua=self._playerXinXiPanel
    elseif playerNum==2 then
        playerDuiHua=self._player1XinXiPanel
    elseif playerNum==3 then
        playerDuiHua=self._player2XinXiPanel
    end
    playerDuiHua:getChildByName("name"):setString(name)
    playerDuiHua:getChildByName("vip"):setString(vip)
    playerDuiHua:getChildByName("coin"):setString(coin)
end

--发话 参数1 1：玩家； 2：下家； 3：上家 。参数2 1：不出； 2：不叫； 3：不抢； 4：叫地主； 5：抢地主
function GameDDZ:fahua(playerNum,talkNum)
    local playerDuiHua=nil
    if playerNum==1 then
        self._playerDuiHua:getChildByName("img"):loadTexture(talkWenZi[talkNum])
        playerDuiHua=self._playerDuiHua
    elseif playerNum==2 then
        self._player1DuiHua:getChildByName("img"):loadTexture(talkWenZi[talkNum])
        playerDuiHua=self._player1DuiHua
    elseif playerNum==3 then
        self._player2DuiHua:getChildByName("img"):loadTexture(talkWenZi[talkNum])
        playerDuiHua=self._player2DuiHua
    end
    playerDuiHua:setVisible(true)

    local function unReversal()
        playerDuiHua:setVisible(false)
    end
    local callfunc = cc.CallFunc:create(unReversal)
    playerDuiHua:runAction(cc.Sequence:create(cc.DelayTime:create(2),callfunc))


end

--删除玩家2出牌（上家）
function GameDDZ:shanchuChuPai2()
    self._player2FaPaiQu:removeAllChildren()
end

--玩家2出牌(上家)
function GameDDZ:chuPaiPlayer2()
    self._panduanPoker={}           --赋值
    --local t_pokers={}

    for k,v in pairs(self._player2Poker) do         --创建图片
        local shuNum=#self._player2Poker
        local poker0=DDZPoker.new(true,v["huase"],v["dianshu"],self)
        self._player2FaPaiQu:addChild(poker0,2)
        table.insert(self._panduanPoker,poker0)
    end

    self:pokerPaiXu(self._panduanPoker)   --排序
    for k,v in pairs(self._panduanPoker) do         --创建图片
        local shuNum=#self._panduanPoker
        v:setPosition(200+(shuNum/2-(shuNum-k))*50,470)
        v:setLocalZOrder(k+1)
    end

    --self:comparePokers()    --测试用

end

--删除玩家1出牌（下家）
function GameDDZ:shanchuChuPai1()
    self._player1FaPaiQu:removeAllChildren()
end

--玩家1出牌(下家)
function GameDDZ:chuPaiPlayer1()
    self._panduanPoker={}           --赋值
    --local t_pokers={}
    for k,v in pairs(self._player1Poker) do        --创建图片
        local shuNum=#self._player1Poker
        local poker0=DDZPoker.new(true,v["huase"],v["dianshu"],self)
        self._player1FaPaiQu:addChild(poker0,2)
        table.insert(self._panduanPoker,poker0)
    end

    self:pokerPaiXu(self._panduanPoker)   --排序
    for k,v in pairs(self._panduanPoker) do         --创建图片
        local shuNum=#self._panduanPoker
        v:setPosition(1080+(shuNum/2-(shuNum-k))*50,470)
        v:setLocalZOrder(k+1)
    end
end

--按钮隐藏
function GameDDZ:hitTable()
    self._table:setVisible(false)
end

--删除出牌
function GameDDZ:shanchuChuPai()
    self._myFaPaiQu:removeAllChildren()
    self._myFaPaiQu:setPosition(0,0)
end

--出牌
function GameDDZ:chuPai()
    local chupaiTable={}
    self:chuPaiPlayer1()   --测试出牌1
    self:shanchuChuPai2()           --暂时删除出牌
    self:chuPaiPlayer2()
    self:hitTable()        --隐藏桌子
    for k,v in pairs(self._myPoker) do      --确认要发的牌
        if v._isTaiQi==true then
            table.insert(chupaiTable,v)
    end
    end

    for k,v in pairs(chupaiTable) do        --创建图片
        local shuNum=#chupaiTable
        local poker0=DDZPoker.new(true,v._color,v._point,self)
        self._myFaPaiQu:addChild(poker0,2)
        poker0:setPosition(640+(shuNum/2-(shuNum-k))*50,50)
    end

    --测试用判断
    local ceshitable={}
    for k,v in pairs(chupaiTable) do        --创建图片
        ceshitable[k]={}
        ceshitable[k]["dianshu"]=v._panduanNum
    end
    self:myPrint("自己的出牌")
    self:dayinPaiZhong(self:paiZhongLei(ceshitable))
    ----------------

    for i = #self._myPoker, 1, -1 do        --删除扑克
        if self._myPoker[i]._isTaiQi==true then
            self._myPoker[i]:deleteSelf()
            table.remove(self._myPoker, i)
    end
    end

    self:pokerFangZhi() --扑克放置
    self._myFaPaiQu:runAction(cc.MoveTo:create(0.1,cc.p(0,120)))

end

--排序
function GameDDZ:pokerPaiXu(pokers)
    local function myComps(a, b)    --点数排序
        return a._myNum > b._myNum
    end
    table.sort(pokers, myComps)
end

--上抬
function GameDDZ:shangTai()
    self:myPrint("上抬调用了")
    for k,v in pairs(self._myPoker) do
        if v._isXuanZhong==true then
            if v._isTaiQi==false then
                v._isTaiQi=true --是否抬起
                v:runAction(cc.MoveBy:create(0,(cc.p(0,50))))
            end
        end

    end
end

--添加自己的扑克
function GameDDZ:addPlayerPoker(poker)
    self._playerPokerNum=self._playerPokerNum+1
    local poker0=DDZPoker.new(true,poker["huase"],poker["dianshu"],self)
    self._playerPoker:addChild(poker0,2)
    poker0:setPosition(640,360)
    poker0._isOther=false
    table.insert(self._myPoker,poker0)
end

--删除所有poker
function GameDDZ:deletePoker()
    self._myPoker={}
    self._playerPoker:deleteSelf()

end

--扑克放置
function GameDDZ:pokerFangZhi()
    local setX=-50
    local setY=88
    for k,v in pairs(self._myPoker) do
        local paiNum=table.getn(self._myPoker)
        if paiNum<=10 then
            v:setPosition(635+(paiNum/2-(paiNum-k))*95+setX,12+setY)
        else
            v:setPosition(600+(paiNum/2-(paiNum-k))*1050/paiNum+setX+60,12+setY)
        end
        v:setLocalZOrder(k+1)--paiNum-k+1)
    end
end

--设置地主牌3张
function GameDDZ:setDiZhuPai(pokerNum)
    self._dizhupai={}
    --    for i=1,3 do
    --        local poker0=DDZPoker.new(false,math.random(1,4),math.random(1,15),self)
    --        self._dizhupai[i]=poker0
    --        poker0:setPosition(567+(50*i)-43,632+5)
    --        self._layerInventory:addChild(poker0,2)
    --        poker0:setScale(0.4)
    --        self._dizhupai[i]=pokerNum[i]
    --    end

end

--斗地主打印
function GameDDZ:myPrint(str)
    if my_dayin then
        print(str)
    end
end

--斗地主打印
function GameDDZ:mydump(str)
    if my_dayin then
        dump(str)
    end
end


--打印牌种
function GameDDZ:dayinPaiZhong(num)
    print("发牌结果")
    if num==-1 then
        print("没有牌，或者错误牌")
    elseif num==0 then
        print("双王炸弹")
    elseif num==1 then
        print("炸弹")
    elseif num==2 then
        print("三带二")
    elseif num==3 then
        print("三带一")
    elseif num==4 then
        print("顺子")
    elseif num==5 then
        print("飞机")
    elseif num==6 then
        print("对子")
    elseif num==7 then
        print("单排")
    elseif num==8 then
        print("四带二")
    elseif num==9 then
        print("四代对子")
    elseif num==10 then
        print("三不带")
    elseif num==11 then
        print("连对")
    elseif num==12 then
        print("飞机带1")
    elseif num==13 then
        print("飞机带对子")
    end
    print("牌大："..self._paiDa)
end


--测试发出上家的牌种类
function GameDDZ:setTestPoker()
    local t_nums={}
    t_nums[1]=11
    t_nums[2]=5
    t_nums[3]=12
    t_nums[4]=13
    --local num=t_nums[math.random(1,4)]    --math.random(1,ddd_feiji2)
    local num=math.random(1,ddd_feiji2)     --math.random(1,ddd_feiji2)
    self._player2Poker={}

    if num==ddd_wangzhadan then
        for i=1,2 do        --
            local t_poker={}
            t_poker["huase"]=math.random(1,4)
            if i==1 then
                t_poker["dianshu"]=14
            else
                t_poker["dianshu"]=15
            end
            table.insert(self._player2Poker,t_poker)
        end
    end

    if num==ddd_zhadan then
        local t_num=math.random(1,13)
        for i=1,4 do
            local t_poker={}
            t_poker["huase"]=math.random(1,4)
            t_poker["dianshu"]=t_num
            table.insert(self._player2Poker,t_poker)
        end
    end

    if num==ddd_sandaier then
        local t_num=math.random(1,12)
        for i=1,5 do
            local t_poker={}
            t_poker["huase"]=math.random(1,4)
            if i<=3 then
                t_poker["dianshu"]=t_num
            else
                t_poker["dianshu"]=t_num+1
            end
            table.insert(self._player2Poker,t_poker)
        end
    end

    if num==ddd_sandaiyi then
        local t_num=math.random(1,12)
        for i=1,4 do
            local t_poker={}
            t_poker["huase"]=math.random(1,4)
            if i<=3 then
                t_poker["dianshu"]=t_num
            else
                t_poker["dianshu"]=t_num+1
            end
            table.insert(self._player2Poker,t_poker)
        end
    end


    if num==ddd_shunzi then
        local chang=math.random(5,7)
        local num=math.random(3,6)
        for i=num,num+chang do
            local t_poker={}
            t_poker["huase"]=math.random(1,4)
            t_poker["dianshu"]=i
            table.insert(self._player2Poker,t_poker)
        end
    end

    if num==ddd_feiji then
        local chang=2   --math.random(2,5)
        local num=math.random(3,8)
        local isLiangci=1

        for i=1,chang*3 do
            local t_poker={}
            t_poker["huase"]=math.random(1,4)
            if isLiangci<3 then
                t_poker["dianshu"]=num
                isLiangci=isLiangci+1
            else
                t_poker["dianshu"]=num
                num=num+1
                isLiangci=1
            end

            table.insert(self._player2Poker,t_poker)
        end

    end

    if num==ddd_duizi then
        local t_num=math.random(1,13)
        for i=1,2 do
            local t_poker={}
            t_poker["huase"]=math.random(1,4)
            t_poker["dianshu"]=t_num
            table.insert(self._player2Poker,t_poker)
        end
    end

    if num==ddd_danpai then
        local t_num=math.random(1,13)
        for i=1,1 do
            local t_poker={}
            t_poker["huase"]=math.random(1,4)
            t_poker["dianshu"]=t_num
            table.insert(self._player2Poker,t_poker)
        end
    end

    if num==ddd_sidaier then
        local t_num=math.random(1,11)
        for i=1,6 do
            local t_poker={}
            t_poker["huase"]=math.random(1,4)
            if i<=4 then
                t_poker["dianshu"]=t_num
            elseif i==5 then
                t_poker["dianshu"]=t_num+1
            else
                t_poker["dianshu"]=t_num+2
            end

            table.insert(self._player2Poker,t_poker)
        end
    end

    if num==ddd_sidaidui then
        local t_num=math.random(1,11)
        for i=1,8 do
            local t_poker={}
            t_poker["huase"]=math.random(1,4)
            if i<=4 then
                t_poker["dianshu"]=t_num
            elseif i<=6 then
                t_poker["dianshu"]=t_num+1
            else
                t_poker["dianshu"]=t_num+2
            end

            table.insert(self._player2Poker,t_poker)
        end
    end


    if num==ddd_sanbudai then
        local t_num=math.random(1,13)
        for i=1,3 do
            local t_poker={}
            t_poker["huase"]=math.random(1,4)
            t_poker["dianshu"]=t_num
            table.insert(self._player2Poker,t_poker)
        end
    end

    if num==ddd_liandui then
        local chang=math.random(3,4)
        local num=math.random(3,7)
        local isLiangci=false

        for i=1,chang*2 do
            local t_poker={}
            t_poker["huase"]=math.random(1,4)
            if isLiangci==false then
                isLiangci=true
                t_poker["dianshu"]=num
            else
                isLiangci=false
                t_poker["dianshu"]=num
                num=num+1
            end

            table.insert(self._player2Poker,t_poker)
        end
    end

    if num==ddd_feiji1 then
        local chang=2--math.random(2,4)
        local num=math.random(3,5)
        local isLiangci=1

        for i=1,chang*3 do      --飞机
            local t_poker={}
            t_poker["huase"]=math.random(1,4)
            if isLiangci<3 then
                t_poker["dianshu"]=num
                isLiangci=isLiangci+1
            else
                t_poker["dianshu"]=num
                num=num+1
                isLiangci=1
            end

            table.insert(self._player2Poker,t_poker)
        end

        for i=10,10+chang-1 do
            local t_poker={}
            t_poker["huase"]=math.random(1,4)
            t_poker["dianshu"]=i
            table.insert(self._player2Poker,t_poker)
        end

    end

    if num==ddd_feiji2 then
        local chang=2--math.random(2,4)
        local num=math.random(3,5)
        local isLiangci=1

        for i=1,chang*3 do      --飞机
            local t_poker={}
            t_poker["huase"]=math.random(1,4)
            if isLiangci<3 then
                t_poker["dianshu"]=num
                isLiangci=isLiangci+1
            else
                t_poker["dianshu"]=num
                num=num+1
                isLiangci=1
            end

            table.insert(self._player2Poker,t_poker)
        end

        local diyici=true
        local changNum=10
        for i=1,chang*2 do
            local t_poker={}
            t_poker["huase"]=math.random(1,4)
            t_poker["dianshu"]=changNum
            if diyici==true then
                diyici=false
            else
                diyici=true
                changNum=changNum+1
            end
            table.insert(self._player2Poker,t_poker)
        end
    end


end

--退出游戏
function GameDDZ:gotoGameChoice()
    uiManager:runScene("GameChoice")
end

function GameDDZ:onEnter()
    print( "GameDDZ:onEnter" )
    -- app.musicSound:playMusic("WUZIQI_MUSIC", true)

end

function GameDDZ:onExit()
--  app.musicSound:stopMusic()
end

return GameDDZ