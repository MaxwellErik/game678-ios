
local RegisterScene = class("RegisterScene", import(".BaseScene"))


RegisterScene.__index = RegisterScene
RegisterScene._uiLayer= nil

RegisterScene._accountTf= nil           --账号
RegisterScene._passwordTf0 = nil        --密码
RegisterScene._passwordTf1 = nil        --密码2

function RegisterScene:ctor()
    RegisterScene.super.ctor(self)
    self._uiLayer = cc.Layer:create()
    self:addChild(self._uiLayer)

    local layerInventory = ccs.GUIReader:getInstance():widgetFromJsonFile("HallUI/NewPlayerRegister.json")
    self._uiLayer:addChild(layerInventory)

--    self._accountTf=layerInventory:getChildByName("accountTf")
--    self._passwordTf0=layerInventory:getChildByName("passwordTf0")
--    self._passwordTf1=layerInventory:getChildByName("passwordTf1")
    
    
    local editBoxSize = cc.size(400, 70)
    local EditName = ccui.EditBox:create(editBoxSize, "HallUI/loginUI/textbg.png")
    EditName:setPosition(cc.p(570,415))
    EditName:setFontSize(10)
    EditName:setFontColor(cc.c3b(200,200,200))
    EditName:setPlaceHolder("输入账号")
    EditName:setPlaceholderFontColor(cc.c3b(255,255,255))
    EditName:setMaxLength(20)
    EditName:setReturnType(cc.KEYBOARD_RETURNTYPE_DONE )
    layerInventory:addChild(EditName,100)
    
    
    local EditPassword = ccui.EditBox:create(editBoxSize, "HallUI/loginUI/textbg.png")
    EditPassword:setPosition(cc.p(570,315))
    EditPassword:setFontSize(10)
    EditPassword:setFontColor(cc.c3b(200,200,200))
    EditPassword:setPlaceHolder("输入密码")
    EditPassword:setPlaceholderFontColor(cc.c3b(255,255,255))
    EditPassword:setMaxLength(20)
    EditPassword:setInputFlag(cc.EDITBOX_INPUT_FLAG_PASSWORD)
    EditPassword:setReturnType(cc.KEYBOARD_RETURNTYPE_DONE )
    layerInventory:addChild(EditPassword,100)
    
    local EditPassword2 = ccui.EditBox:create(editBoxSize, "HallUI/loginUI/textbg.png")
    EditPassword2:setPosition(cc.p(570,215))
    EditPassword2:setFontSize(10)
    EditPassword2:setFontColor(cc.c3b(200,200,200))
    EditPassword2:setPlaceHolder("确认密码")
    EditPassword2:setPlaceholderFontColor(cc.c3b(255,255,255))
    EditPassword2:setMaxLength(20)
    EditPassword2:setInputFlag(cc.EDITBOX_INPUT_FLAG_PASSWORD)
    EditPassword2:setReturnType(cc.KEYBOARD_RETURNTYPE_DONE )
    layerInventory:addChild(EditPassword2,100)
    
    self._accountTf=EditName
    self._passwordTf0=EditPassword
    self._passwordTf1=EditPassword2
    

    --返回按钮点击事件
    local function returnAccount(sender, eventType)
        if eventType == ccui.TouchEventType.ended then
            app.musicSound:playSound(SOUND_HALL_TOUCH)
            uiManager:runScene("MainScene","","fade",0.5,cc.c3b(15, 115, 200))
        end
    end

    --返回按钮
    local returnBtn = layerInventory:getChildByName("returnBtn")
    returnBtn:addTouchEventListener(returnAccount)

    --完成注册事件

    local function openLogin(sender, eventType)

        if eventType == ccui.TouchEventType.ended then
            local username = self._accountTf:getText()
            local pwd1 = self._passwordTf0:getText()
            local pwd2 = self._passwordTf1:getText()
            --判断账号密码是否为空
            if  username=="" or  pwd1=="" or pwd2=="" then
                MyToast.new(self,STR_LOGIN_REGISTER_NULL)
                --self:showTip(false, STR_LOGIN_REGISTER_NULL)
                return
            end
            
            --判断账号是否有非法字符
            if strLegalRegist(username) ~= true then
                MyToast.new(self,STR_LOGIN_ID_UNLEGAL)
                --self:showTip(false, STR_LOGIN_ID_UNLEGAL)
                return
            end

            if strLegalRegist(pwd1) ~= true then
                MyToast.new(self,STR_LOGIN_PWD_UNLEGAL)
                --self:showTip(false, STR_LOGIN_PWD_UNLEGAL)
                return
            end

            --判断两次密码是否相等
            if pwd1 ~= pwd2 then
                MyToast.new(self,STR_LOGIN_PASSWORD_NULL)
                --self:showTip(false, STR_LOGIN_PASSWORD_NULL)
                return
            end

            --判断输入的账号密码长度对不对
            if string.len(username) < LEN_LESS_ACCOUNTS or string.len(username) > LEN_LESS_MAX or string.len(pwd1) < LEN_LESS_PASSWORD then
                MyToast.new(self, string.format(STR_ACCOUNTS_PWD_LESS, LEN_LESS_ACCOUNTS, LEN_LESS_PASSWORD))
                --self:showTip(false, string.format(STR_ACCOUNTS_PWD_LESS, LEN_LESS_ACCOUNTS, LEN_LESS_PASSWORD))
                return;
            end

            --链接服务器
            self:addLoading()
            app.hallLogic:regist(self._accountTf:getText(),self._passwordTf0:getText(),self._passwordTf1:getText())           
        end
    end

    --完成注册按钮
    local accountBtn = layerInventory:getChildByName("completeBtn")
    accountBtn:addTouchEventListener(openLogin)



--    --快速登录
--    local quickCheck = layerInventory:getChildByName("quickCheck")
--    local quickCheckBg = layerInventory:getChildByName("quickRegisterImg")
--    --普通登录
--    local nomalCheck = layerInventory:getChildByName("nomalCheck")
--    local nomalCheckBg = layerInventory:getChildByName("registerImg")

    --选择背景
    local choiceBg = layerInventory:getChildByName("choiceBg")

--    local function quickS(isQuick)
--        if isQuick then
--            quickCheck:setSelected(true)
--            nomalCheck:setSelected(false)
--            quickCheckBg:loadTexture("HallUI/loginUI/registerChoiceCheck2.png")
--            nomalCheckBg:loadTexture("HallUI/loginUI/registerChoiceCheck1.png")
--            choiceBg:setFlippedX(true)
--        else
--            nomalCheck:setSelected(true)
--            quickCheck:setSelected(false)
--            quickCheckBg:loadTexture("HallUI/loginUI/registerChoiceCheck1.png")
--            nomalCheckBg:loadTexture("HallUI/loginUI/registerChoiceCheck2.png")
--            choiceBg:setFlippedX(false)
--        end
--
--    end
--
--    --快速登录事件
--    local function quickSelectedEvent(sender,eventType)
--        app.musicSound:playSound(SOUND_HALL_TOUCH)
--        if eventType == ccui.CheckBoxEventType.selected then
--            quickS(true)
--        elseif eventType == ccui.CheckBoxEventType.unselected then
--            quickS(true)
--        end
--    end
--    quickCheck:addEventListener(quickSelectedEvent)
--    quickCheck:setSelected(false)


--    --普通登录事件
--    local function nomalSelectedEvent(sender,eventType)
--        app.musicSound:playSound(SOUND_HALL_TOUCH)
--        if eventType == ccui.CheckBoxEventType.selected then
--            quickS(false)
--        elseif eventType == ccui.CheckBoxEventType.unselected then
--            quickS(false)
--        end
--    end
--    nomalCheck:addEventListener(nomalSelectedEvent)
--    nomalCheck:setSelected(true)

end
--网络连接返回
function RegisterScene:onHallNetConnected(bConnected)
end


function RegisterScene:onLoginSrvLoginSuccess(data)
    cc.UserDefault:getInstance():setStringForKey("account", self._accountTf:getText())
    cc.UserDefault:getInstance():setStringForKey("password",self._passwordTf0:getText())
    --登录成功
    self:delLoading()
    --切换到游戏选择页面
    uiManager:runScene("GameChoice","","fade",0.5,cc.c3b(15, 115, 200))
end


return RegisterScene