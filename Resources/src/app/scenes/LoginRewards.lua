
LoginRewards = class("LoginRewards", function()
    return display.newScene("LoginRewards")
end)


LoginRewards.__index = LoginRewards
LoginRewards._uiLayer= nil
LoginRewards._layerInventory = nil
LoginRewards._day = 0   --今天是第几天
LoginRewards._coin = 0  --今天领的金币
LoginRewards._allCoin = 0   --总共领的金币


function LoginRewards:ctor( a_day, a_coin, a_allCoin )
    app.musicSound:addSound( "win", "CoinAnimation/WinAnimation/coinWin.mp3" )

    self._uiLayer = cc.Layer:create()
    self:addChild(self._uiLayer)

    local layerInventory = ccs.GUIReader:getInstance():widgetFromJsonFile("HallUI/LoginRewards.json")
    self._uiLayer:addChild(layerInventory)
    
    self._layerInventory = layerInventory
        
    
    --领取点击事件
    local function lingQuEvent(sender, eventType)
        if eventType == ccui.TouchEventType.ended then
            app.musicSound:playSound(SOUND_HALL_TOUCH)
            app.hallLogic:userSignin()
        end
    end

    -- 领取按钮
    local lingQuBtn = layerInventory:getChildByName("lingQuBtn")
    lingQuBtn:addTouchEventListener(lingQuEvent)
    
    --关闭点击事件
    local function closeEvent(sender, eventType)
        if eventType == ccui.TouchEventType.ended then
           app.musicSound:playSound(SOUND_HALL_TOUCH)
           self:setVisible(false)
        end
    end

    --关闭按钮
    local closeBtn = layerInventory:getChildByName("qianDaoCloseBtn")
    closeBtn:addTouchEventListener(closeEvent)

    -- Vip事件
    local function vipEvent(sender, eventType)
        if eventType == ccui.TouchEventType.ended then
            app.musicSound:playSound(SOUND_HALL_TOUCH)
            local vipLayer = Vip.new()
            vipLayer:setParam(app.hallLogic._vipConf)
            vipLayer:show()
            vipLayer:setTag(VIP_TAG)
            self._uiLayer:addChild(vipLayer, 100)
        end
    end
    -- Vip按钮
    local VipBtn = layerInventory:getChildByName("VipBtn")
    VipBtn:addTouchEventListener(vipEvent)

    self:init( a_day, a_coin )
    
    self:setVisible(false)
end

function LoginRewards:init( a_day, a_coin )
    self._day = a_day
    self._coin = a_coin
    
    local t_day = a_day
    if t_day > 7 then
        t_day = 7
    end
    for i=1, 7 do
        local t_coin = self._layerInventory:getChildByName(i.."day")
        if i - t_day < 0 then
            t_coin:getChildByName("yiLingQu"):setVisible(true)
        else
            t_coin:getChildByName("yiLingQu"):setVisible(false)
        end
    end

    --签到的天数
    local dayNumber = self._layerInventory:getChildByName("day")
    dayNumber:setString(a_day)

    --总共领取的金币
    local coinNumber = self._layerInventory:getChildByName("coin")
    coinNumber:setString(a_coin)
    
    print("LoginRewards:init app.hallLogic.IsLingQu "..tostring(app.hallLogic.IsLingQu))
    
    local lingQuBtn = self._layerInventory:getChildByName("lingQuBtn")
    
    if app.hallLogic.IsLingQu then        
        lingQuBtn:setBright(false)
        lingQuBtn:setTouchEnabled(false)
    else
        lingQuBtn:setBright(true)
        lingQuBtn:setTouchEnabled(true)
    end
    
    local t_coin = self._layerInventory:getChildByName(t_day.."day")
    if app.hallLogic.IsLingQu then        
        t_coin:getChildByName("yiLingQu"):setVisible(true)
    else
        t_coin:getChildByName("yiLingQu"):setVisible(false)
    end 
end

--领取登陆奖励
function LoginRewards:lingQu()
    local lingQuBtn = self._layerInventory:getChildByName("lingQuBtn")

    local t_day = self._day
    if t_day > 7 then
        t_day = 7
    end
    local t_coin = self._layerInventory:getChildByName(t_day.."day")
    t_coin:getChildByName("yiLingQu"):setVisible(true)    

    lingQuBtn:setBright(false)
    lingQuBtn:setTouchEnabled(false)
    
    app.musicSound:playSound("win")
    --测试金币
    for i=1, 100 do
        local t_coin = WinCoin.new(self._layerInventory, false, true)
        self._layerInventory:addChild(t_coin)
    end
    --大金币
    local t_big = math.random(1, 3)
    for i=1, t_big do
        local t_coin = WinCoin.new(self._layerInventory, true, true)
        self._layerInventory:addChild(t_coin)
    end   

    ----------- YLADD ----------
    local seq = transition.sequence({
        cc.DelayTime:create(4.0),
        cc.CallFunc:create(function()
            app.musicSound:playSound(SOUND_HALL_TOUCH)
            self:setVisible(false) 
        end)
        })
    self:runAction(seq)
end

function  LoginRewards:onEnter()
    app.eventDispather:addListenerEvent(eventLoginSignOk, self, function(data)
        self:lingQu()
    end)
end

function LoginRewards:onExit()
	app.eventDispather:delListenerEvent(self)
end

return LoginRewards