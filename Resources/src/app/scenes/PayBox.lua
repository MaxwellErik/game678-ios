
PayBox = class("PayBox", function ()
	return cc.Layer:create()
end)



PayBox.__index = PayBox
PayBox._uiLayer= nil
PayBox._confirmBtn=nil             --确定按钮
PayBox._payBtn = nil               --购买按钮
PayBox._cancelBtn=nil              --取消按钮
PayBox._textLable=nil              --显示文字
PayBox._payNumber = nil            --购买的数量

  --参数一，是购买还是确定。参数二，显示文字。参数三 宝箱ID。参数四 宝箱的价格
function PayBox:ctor(state, text, id, price, owner)  
    self._uiLayer = cc.Layer:create()
    self:addChild(self._uiLayer)

    local layerInventory = ccs.GUIReader:getInstance():widgetFromJsonFile("HallUI/PayBox.json")
    self._uiLayer:addChild(layerInventory)
    
    self._textLable = ccui.Helper:seekWidgetByName(layerInventory,"typeSprite")
    self._confirmCallBack = confirm
    self._cancelBtnCallBack = cancel
    --------------点击事件
    --取消领取
   

    -----------------------

    --确定事件
    local function confirmBtnCallBack(sender, eventType)
        if eventType == ccui.TouchEventType.ended then
            local t_num = self:getPayNumber()
            if t_num == nil then
                MyToast.new(self, "请输入数字")
                return
            end
            if t_num <= 0 then
                MyToast.new(self, "请输入正确的数字")
                return
            end

            if t_num > text then
                MyToast.new(self, "你的宝箱不足，最多只能使用"..text.."个当前宝箱")
                return
            end

            app.hallLogic:openRedEnvelope(price, t_num)
            self:removeFromParent()
        end
    end
    
    self._confirmBtn = ccui.Helper:seekWidgetByName(layerInventory,"confirmBtn")    
    self._confirmBtn:addTouchEventListener(confirmBtnCallBack)


    --购买事件
    local function payBtnCallBack(sender, eventType)
        if eventType == ccui.TouchEventType.ended then
            --self:setVisible(false)
            local t_num = self:getPayNumber()
            if t_num == nil then
                MyToast.new(self, "请输入数字")
                return
            end
            if t_num <= 0 then
                MyToast.new(self, "请输入正确的数字")
                return
            end

            local t_coin = app.hallLogic._loginSuccessInfo.lUserScore
            local t_score = app.hallLogic._redEnvelopeInfo.items[id].ExchangeScore

            local t_number = math.floor(t_coin / t_score)

            if t_coin < t_score * t_num then
                MyToast.new(self, "你的金币不足，最多只能购买"..t_number.."个当前宝箱")
                return
            end

            app.hallLogic:buyRedEnvelope(app.hallLogic._redEnvelopeInfo.items[id].dwRMBValue,  t_num)
            self:removeFromParent()
        end
    end
    self._payBtn = ccui.Helper:seekWidgetByName(layerInventory,"payBtn")
    self._payBtn:addTouchEventListener(payBtnCallBack)
    
    
    --取消领取
    local function cancelBtnCallBack(sender, eventType)
        if eventType == ccui.TouchEventType.ended then
            self:removeFromParent()
        end
    end

    --取消按钮
    self._cancelBtn = ccui.Helper:seekWidgetByName(layerInventory,"cancleBtn")
    self._cancelBtn:addTouchEventListener(cancelBtnCallBack)
    ccui.Helper:seekWidgetByName(layerInventory,"closeBg"):addTouchEventListener(cancelBtnCallBack)

    --确定按钮
    if state == 1 then
        self._payBtn:setVisible(true)
        self._confirmBtn:setVisible(false)
    elseif state == 2 then
        self._payBtn:setVisible(false)
        self._confirmBtn:setVisible(true)
    end
    
    if price == 10 then
        self._textLable:loadTexture("HallUI/Pay/small.png")
    elseif price == 50 then
        self._textLable:loadTexture("HallUI/Pay/mid.png")
    elseif price == 100 then
        self._textLable:loadTexture("HallUI/Pay/big.png")
    end
    
   

   self._payNumber = ccui.Helper:seekWidgetByName(layerInventory, "payNumber")

   print("self._payNumber "..tostring(self._payNumber))
   self._payNumber:setString(text)      --设置文字
   
   owner:addChild(self)
end

function PayBox:showText()
	self:setVisible(true)
	self._time=0
end

function PayBox:setText(text)
    -- self._textLable:setString(text)
    if text == 10 then
        self._textLable:loadTexture("HallUI/Pay/small.png")
    elseif text == 50 then
        self._textLable:loadTexture("HallUI/Pay/mid.png")
    elseif text == 100 then
        self._textLable:loadTexture("HallUI/Pay/big.png")
    end
end

function PayBox:getPayNumber()
    local t_num = tonumber(self._payNumber:getString())
    return t_num
end

return PayBox