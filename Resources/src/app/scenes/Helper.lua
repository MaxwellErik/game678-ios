
Helper = class("Helper", function()
    return display.newLayer()
end)

function Helper:ctor()
    self._uiLayer = cc.Layer:create()
    self:addChild(self._uiLayer)

    local layerInventory = ccs.GUIReader:getInstance():widgetFromJsonFile("GameSelect/FanKui.json")
    self._uiLayer:addChild(layerInventory)

    self._BG = ccui.Helper:seekWidgetByName(layerInventory,"fankuiBg")
    self._BG:setTouchEnabled(true)
    self._bgPosX = self._BG:getPositionX()
    self._bgPosY = self._BG:getPositionY()

    local function closeTarget(sender, type)
        if type == ccui.TouchEventType.ended then
            self:dismiss()
        end
    end

    local closeBtn = ccui.Helper:seekWidgetByName(layerInventory,"closeBtn")
    closeBtn:addTouchEventListener(closeTarget)

    layerInventory:addTouchEventListener(closeTarget)
    
    local xialaPanel =ccui.Helper:seekWidgetByName(layerInventory, "xialaPanel")
    
    --反馈下拉点击事件
    local function fankuikaiqiBtnAccount(sender, eventType)
        if eventType == ccui.TouchEventType.ended then
            app.musicSound:playSound(SOUND_HALL_TOUCH)
            xialaPanel:setClippingEnabled(false)
        end
    end
    --反馈下拉按钮
    local fankuikaiqiBtn = ccui.Helper:seekWidgetByName(layerInventory, "kaiqi")
    fankuikaiqiBtn:addTouchEventListener(fankuikaiqiBtnAccount)
    
    -----------
    --qq点击事件
    local qqLabBtn = xialaPanel:getChildByName("qqLab")
    local qqMailLabBtn = xialaPanel:getChildByName("qqMailLab")
    local phonoLabBtn = xialaPanel:getChildByName("phonoLab")
    local fankuiType=1  --1qq 2qq邮箱 3电话号码
    local function qqLabBtnAccount(sender, eventType)
        if eventType == ccui.TouchEventType.ended then
            app.musicSound:playSound(SOUND_HALL_TOUCH)
            xialaPanel:setClippingEnabled(true)
            qqLabBtn:setPosition(cc.p(0,18))
            qqMailLabBtn:setPosition(cc.p(0,-14))
            phonoLabBtn:setPosition(cc.p(0,-49))
            if fankuiType~=1 then
                lianxiEdit:setText("")
            end          
            fankuiType=1           
        end
    end
    --qq按钮
    qqLabBtn:addTouchEventListener(qqLabBtnAccount)


    --qq邮箱点击事件
    local function qqMailLabBtnAccount(sender, eventType)
        if eventType == ccui.TouchEventType.ended then
            app.musicSound:playSound(SOUND_HALL_TOUCH)
            xialaPanel:setClippingEnabled(true)
            qqLabBtn:setPosition(cc.p(0,-14))
            qqMailLabBtn:setPosition(cc.p(0,18))
            phonoLabBtn:setPosition(cc.p(0,-49))
            if fankuiType~=2 then
                lianxiEdit:setText("")
            end  
            fankuiType=2
        end
    end
    --qq邮箱按钮
    qqMailLabBtn:addTouchEventListener(qqMailLabBtnAccount)


    --电话号码点击事件
    local function phonoLabBtnAccount(sender, eventType)
        if eventType == ccui.TouchEventType.ended then
            app.musicSound:playSound(SOUND_HALL_TOUCH)
            xialaPanel:setClippingEnabled(true)
            qqLabBtn:setPosition(cc.p(0,-14))
            qqMailLabBtn:setPosition(cc.p(0,-49))
            phonoLabBtn:setPosition(cc.p(0,18))
            if fankuiType~=3 then
                lianxiEdit:setText("")
            end  
            fankuiType=3
        end
    end
    --电话号码按钮
    phonoLabBtn:addTouchEventListener(phonoLabBtnAccount)

    --反馈标题
    local fankuikaiTitle = ccui.Helper:seekWidgetByName(self._BG,"titleText")
    --反馈内容
    local fankuiContent = ccui.Helper:seekWidgetByName(self._BG,"fankuiText")

    --反馈发送事件
    local function fasongBtnAccount(sender, eventType)
        if eventType == ccui.TouchEventType.ended then
            app.musicSound:playSound(SOUND_HALL_TOUCH)
            if fankuikaiTitle:getString()=="" or fankuiContent:getString()=="" then
                MyToast.new(self,STR_HCPY_FANKUI_MSG)
                return
            end
            app.hallLogic.szTitle=fankuikaiTitle:getString()
            app.hallLogic.szContent=fankuiContent:getString()               
            if fankuiType==1 then
                app.hallLogic.szQQ = self.lianxiEdit:getText()
                app.hallLogic.szEMail = ""
                app.hallLogic.szMobilePhone = ""
            elseif fankuiType==2 then
                app.hallLogic.szQQ = ""
                app.hallLogic.szEMail = self.lianxiEdit:getText()
                app.hallLogic.szMobilePhone = ""
            elseif fankuiType==2 then
                app.hallLogic.szQQ = ""
                app.hallLogic.szEMail = ""
                app.hallLogic.szMobilePhone = self.lianxiEdit:getText()
            end           
            app.hallLogic:goFankui()

        end
    end
    --反馈发送按钮
    local fasongBtn = ccui.Helper:seekWidgetByName(self._BG,"fasongBtn")
    fasongBtn:addTouchEventListener(fasongBtnAccount)

    self._BG:setPositionY(2000)
    self._bShow = false
end

function Helper:show()
    self._BG:stopAllActions()
    self._BG:runAction(cc.Sequence:create(cc.MoveTo:create(0.6, {['x']=self._bgPosX, ['y']=self._bgPosY}),
        cc.MoveBy:create(0.05, {['x']=0, ['y']=20}), cc.MoveBy:create(0.04, {['x']=0, ['y']=-20}), cc.CallFunc:create(function() 
            self._bShow = true
            
            --联系方式
            local editBoxSize = cc.size(200, 40)
            self.lianxiEdit = ccui.EditBox:create(editBoxSize, "HallUI/loginUI/textbg.png")
            self.lianxiEdit:setPosition(cc.p(693,246))
            self.lianxiEdit:setFontSize(15)
            self.lianxiEdit:setFontColor(cc.c3b(0,0,0))
            self.lianxiEdit:setPlaceHolder("联系方式")
            self.lianxiEdit:setReturnType(cc.KEYBOARD_RETURNTYPE_DONE )
            self._uiLayer:addChild(self.lianxiEdit,100)
        end)))
end

function Helper:dismiss()
    if self._bShow == false then
        return
    end

    if self.lianxiEdit then
        self._uiLayer:removeChild(self.lianxiEdit)
        self.lianxiEdit = nil
    end
    self._BG:stopAllActions()
    self._BG:runAction(cc.Sequence:create(cc.MoveBy:create(0.6,{['x']=0, ['y']=2000}), cc.CallFunc:create(function()
        self:removeFromParent()
    end)))
end

return Helper