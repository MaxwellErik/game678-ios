
Ranking = class("Ranking", function()
    return display.newScene()
end)


Ranking.__index = Ranking
Ranking._uiLayer= nil


Ranking._caifuZongBang = nil
Ranking._yueBang = nil
Ranking._zhouBang = nil
Ranking._riBang = nil
Ranking._guiZeShuoMing = nil
Ranking._showP = cc.p(646,575)
Ranking._closeP = cc.p(3000,-2000)


function Ranking:ctor()
    self._uiLayer = cc.Layer:create()
    self:addChild(self._uiLayer)

    local layerInventory = ccs.GUIReader:getInstance():widgetFromJsonFile("HallUI/Ranking.json")
    self._uiLayer:addChild(layerInventory)
    self._layerInventory = layerInventory
    
    layerInventory:setPositionY(2000)

    local caifuZCheck = layerInventory:getChildByName("caifuZCheck")        --选择框
    local yingliYCheck = layerInventory:getChildByName("yingliYCheck")
    local yingliZCheck = layerInventory:getChildByName("yingliZCheck")
    local yingliRCheck = layerInventory:getChildByName("yingliRCheck")
    local guiZeCheck = layerInventory:getChildByName("guiZeCheck")
    
    self._caifuZongBang = layerInventory:getChildByName("caifuZongBang")        --选择页面
    self._yueBang = layerInventory:getChildByName("yueBang")
    self._zhouBang = layerInventory:getChildByName("zhouBang")
    self._riBang = layerInventory:getChildByName("riBang")
    self._guiZeShuoMing = layerInventory:getChildByName("guiZeShuoMing")
    
    --财富总榜事件
    local function caifuZCheckEvent(sender,eventType)
        app.musicSound:playSound(SOUND_HALL_TOUCH)
        caifuZCheck:setSelected(true)
        yingliYCheck:setSelected(false)
        yingliZCheck:setSelected(false)
        yingliRCheck:setSelected(false)
        guiZeCheck:setSelected(false)
        self._caifuZongBang:setPosition(self._showP)
        self._yueBang:setPosition(self._closeP)
        self._zhouBang:setPosition(self._closeP)
        self._riBang:setPosition(self._closeP)
        self._guiZeShuoMing:setPosition(self._closeP)
        self:updateCaiFuRang()
    end  
    caifuZCheck:addEventListener(caifuZCheckEvent)
    caifuZCheck:setSelected(true)
    self._caifuZongBang:setPosition(self._showP)
    
    
    --月榜事件
    local function yingliYCheckEvent(sender,eventType)
        app.musicSound:playSound(SOUND_HALL_TOUCH)
        caifuZCheck:setSelected(false)
        yingliYCheck:setSelected(true)
        yingliZCheck:setSelected(false)
        yingliRCheck:setSelected(false)
        guiZeCheck:setSelected(false)
        self._caifuZongBang:setPosition(self._closeP)
        self._yueBang:setPosition(self._showP)
        self._zhouBang:setPosition(self._closeP)
        self._riBang:setPosition(self._closeP)
        self._guiZeShuoMing:setPosition(self._closeP)
        self:updateMonthRank()
    end  
    yingliYCheck:addEventListener(yingliYCheckEvent)
    yingliYCheck:setSelected(false)
    
    --周榜事件
    local function yingliZCheckEvent(sender,eventType)
        app.musicSound:playSound(SOUND_HALL_TOUCH)
        caifuZCheck:setSelected(false)
        yingliYCheck:setSelected(false)
        yingliZCheck:setSelected(true)
        yingliRCheck:setSelected(false)
        guiZeCheck:setSelected(false)
        self._caifuZongBang:setPosition(self._closeP)
        self._yueBang:setPosition(self._closeP)
        self._zhouBang:setPosition(self._showP)
        self._riBang:setPosition(self._closeP)
        self._guiZeShuoMing:setPosition(self._closeP)
        self:updateWeekRank()
    end  
    yingliZCheck:addEventListener(yingliZCheckEvent)
    yingliZCheck:setSelected(false)
    
    --日榜事件
    local function yingliRCheckEvent(sender,eventType)
        app.musicSound:playSound(SOUND_HALL_TOUCH)
        caifuZCheck:setSelected(false)
        yingliYCheck:setSelected(false)
        yingliZCheck:setSelected(false)
        yingliRCheck:setSelected(true)
        guiZeCheck:setSelected(false)
        self._caifuZongBang:setPosition(self._closeP)
        self._yueBang:setPosition(self._closeP)
        self._zhouBang:setPosition(self._closeP)
        self._riBang:setPosition(self._showP)
        self._guiZeShuoMing:setPosition(self._closeP)
        self:updateDayRank()
    end  
    yingliRCheck:addEventListener(yingliRCheckEvent)
    yingliRCheck:setSelected(false)
    
    --规则说明事件
    local function guiZeCheckEvent(sender,eventType)
        app.musicSound:playSound(SOUND_HALL_TOUCH)
        caifuZCheck:setSelected(false)
        yingliYCheck:setSelected(false)
        yingliZCheck:setSelected(false)
        yingliRCheck:setSelected(false)
        guiZeCheck:setSelected(true)
        self._caifuZongBang:setPosition(self._closeP)
        self._yueBang:setPosition(self._closeP)
        self._zhouBang:setPosition(self._closeP)
        self._riBang:setPosition(self._closeP)
        self._guiZeShuoMing:setPosition(cc.p(642,572))
    end  
    guiZeCheck:addEventListener(guiZeCheckEvent)
    guiZeCheck:setSelected(false)


    --返回按钮点击事件
    local function returnEvent(sender, eventType)
        if eventType == ccui.TouchEventType.ended then
            app.musicSound:playSound(SOUND_HALL_TOUCH)
            self:dismiss(false)
        end
    end

    --返回按钮
    local returnBtn = layerInventory:getChildByName("returnBtn")
    returnBtn:addTouchEventListener(returnEvent)
    
    app.eventDispather:addListenerEvent(eventLoginCoinRank, self, function(data)
        self:updateCaiFuRang()
    end)
    app.eventDispather:addListenerEvent(eventLoginYueRank, self, function(data)
        self:updateMonthRank()
    end)
    app.eventDispather:addListenerEvent(eventLoginZhouRank, self, function(data)
        self:updateWeekRank()
    end)
    app.eventDispather:addListenerEvent(eventLoginRiRank, self, function(data)
        self:updateDayRank()
    end)
    
    self:updateCaiFuRang()
    
    self._bShow = false
end

function Ranking:show()
    self._layerInventory:stopAllActions()
    self._layerInventory:runAction(cc.Sequence:create(cc.MoveTo:create(0.6, cc.p(0, 0)),
        cc.MoveBy:create(0.05, {['x']=0, ['y']=20}), cc.MoveBy:create(0.04, {['x']=0, ['y']=-20}), cc.CallFunc:create(function() 
            self._bShow = true
        end)))
end

function Ranking:dismiss()
    if self._bShow == false then
        return
    end

    self._layerInventory:stopAllActions()
    self._layerInventory:runAction(cc.Sequence:create(cc.MoveBy:create(0.6,{['x']=0, ['y']=2000}), cc.CallFunc:create(function()
        self._bShow = false
        self:removeFromParent()
    end)))
end

function Ranking:onExit()
	app.eventDispather:delListenerEvent(self)
end

function Ranking:updateCaiFuRang()
    if app.hallLogic._caifuRankMsg == nil then
    	app.hallLogic:sendCoinRank()
    	return
    end
    self:updateRank(app.hallLogic._caifuRankMsg, self._caifuZongBang:getChildByName("list"))
end

function Ranking:updateDayRank()
    if app.hallLogic._tianRankMsg == nil then
        app.hallLogic:sendDayRank()
        return
    end
    self:updateRank(app.hallLogic._tianRankMsg, self._riBang:getChildByName("list"))
end

function Ranking:updateWeekRank()
    if app.hallLogic._zhouRankMsg == nil then
        app.hallLogic:sendWeekRank()
        return
    end
    self:updateRank(app.hallLogic._zhouRankMsg, self._zhouBang:getChildByName("list"))
end

function Ranking:updateMonthRank()
    if app.hallLogic._yueRankMsg == nil then
        app.hallLogic:sendMonthRank()
        return
    end
    self:updateRank(app.hallLogic._yueRankMsg, self._yueBang:getChildByName("list"))
end

--排行 1，信息2，listview、
function Ranking:updateRank(msg, listview)
    if listview.isLoad == true or msg == nil then
    	return
    end
    
    listview.isLoad = true
    
    listview:removeAllChildren(true)
    for k,v in pairs(msg["UserInfo"]) do        
        local list={}
        local t_score=string.format("%f", v["lScore"])
        local tmps = string.split(t_score, '.')
        local t_test0 = RankingObject.new(k,tmps[1],v["wFaceID"],v["szNickname"],v["wGameKind"])
        listview:insertCustomItem(t_test0,k-1)
    end
end


return Ranking