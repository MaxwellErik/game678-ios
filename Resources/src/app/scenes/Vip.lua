
Vip = class("Vip", function ()
	return cc.Layer:create()
end)

VIP_NUMBER = 9

Vip.__index = Vip
Vip._uiLayer= nil
Vip._layerInventory = nil
Vip._owner = nil    


  --参数一，是否有确认取消按钮。参数二，显示文字。
function Vip:ctor( owner )  
    self._owner = owner

    self._uiLayer = cc.Layer:create()
    self:addChild(self._uiLayer)

    local layerInventory = ccs.GUIReader:getInstance():widgetFromJsonFile("Vip/Vip.json")
    self._uiLayer:addChild(layerInventory)
    self._layerInventory = layerInventory
    
    layerInventory:addTouchEventListener(function(sender, type) 
        if type == ccui.TouchEventType.ended then
        	self:dismiss()
        end
    end)

    self._BG = ccui.Helper:seekWidgetByName(layerInventory,"BG")
    self._BG:setTouchEnabled(true)
    self._bgPosY = self._BG:getPositionY()
    self._bgPosX = self._BG:getPositionX()
    self._BG:setPositionY(1500)
    
    --------------充值点击事件
    
    local function chongzhiBtnCallBack(sender, eventType)
        if eventType == ccui.TouchEventType.ended then
            app.musicSound:playSound(SOUND_HALL_TOUCH)
            
            self:chongZhi()
        end
    end
    local chongzhiBtn=ccui.Helper:seekWidgetByName(layerInventory,"chongzhi")
    chongzhiBtn:addTouchEventListener(chongzhiBtnCallBack)
   

    -----------------------
    --关闭事件
    local function closeBtnBtnCallBack(sender, eventType)
        if eventType == ccui.TouchEventType.ended then
            app.musicSound:playSound(SOUND_HALL_TOUCH)
            
            self:dismiss()
        end
    end

    --关闭按钮
    local closeBtnBtn = ccui.Helper:seekWidgetByName(layerInventory,"closeBtn")
    closeBtnBtn:addTouchEventListener(closeBtnBtnCallBack)
    
    self._bShow = false
end

--设置VIP页面各种参数
function Vip:setParam(vipconf)

    local t_now = app.hallLogic._loginSuccessInfo.obMemberInfo.cbMemberOrder
    local nowLevel = ccui.Helper:seekWidgetByName(self._BG, "nowLevel")
    nowLevel:setString(t_now)
    
    local t_next = t_now + 1
    if t_next > 9 then
        t_next = 9
    end
    local nextLevel = ccui.Helper:seekWidgetByName(self._BG, "nextLevel")
    nextLevel:setString(t_next)
    
    local chongzhiBtn=ccui.Helper:seekWidgetByName(self._BG,"chongzhi")
    
    local t_baocuntianshu = t_now
    ccui.Helper:seekWidgetByName(self._BG, "baocuntianshu"):setString(t_baocuntianshu)
    
    if vipconf ~= nil then
        local t_chongzhijiacheng = 0
        if t_now ~= 0 then
            t_chongzhijiacheng = vipconf[t_now].add
        end
        ccui.Helper:seekWidgetByName(self._BG, "chongzhijiacheng"):setString(t_chongzhijiacheng)
        
        local t_tishi = "累积充值"..vipconf[t_next].money.."元即可成为VIP"..t_next
        ccui.Helper:seekWidgetByName(self._BG, "tishi"):setString(t_tishi)

        for i=1,VIP_NUMBER do
            local t_money = vipconf[i].money
            local t_day = i
            local t_jiacheng = vipconf[i].add
            local t_vip = ccui.Helper:seekWidgetByName(self._BG, "VIP"..i)
            t_vip:setString(t_money.."元")
            t_vip:getChildByName("tianshu"):setString(t_day.."天")
            t_vip:getChildByName("jiacheng"):setString(t_jiacheng)
        end
    end    
end

function Vip:show()
    self._BG:stopAllActions()
    self._BG:runAction(cc.Sequence:create(cc.MoveTo:create(0.6, {['x']=self._bgPosX, ['y']=self._bgPosY}),
        cc.MoveBy:create(0.05, {['x']=0, ['y']=20}), cc.MoveBy:create(0.04, {['x']=0, ['y']=-20}), cc.CallFunc:create(function() 
            self._bShow = true
        end)))
end

function Vip:dismiss()
    if self._bShow == false then
        return
    end

    self._BG:stopAllActions()
    self._BG:runAction(cc.Sequence:create(cc.MoveBy:create(0.6,{['x']=0, ['y']=2000}), cc.CallFunc:create(function()
        self:removeFromParent()
    end)))
end

function Vip:chongZhi()
    self:dismiss()
    app.eventDispather:dispatherEvent(eventUiShowShop)
end

function Vip:onEnter()
    app.eventDispather:addListenerEvent(eventUpdateVipConf, self, function(data)
        self:setParam(data)
    end)
end

function Vip:onExit()
    app.eventDispather:delListenerEvent(self)
end

return Vip