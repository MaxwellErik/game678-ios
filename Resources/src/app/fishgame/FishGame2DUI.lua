local FishGame2DUI = class("FishGame2DUI", function()
    return display.newLayer()
end)

function FishGame2DUI:ctor()
    self:addNodeEventListener(cc.NODE_ENTER_FRAME_EVENT, handler(self, self._onInterval))
    self:addNodeEventListener(cc.NODE_TOUCH_EVENT, handler(self, self._onTouch))
    self:scheduleUpdate()
    self:setTouchEnabled(true)
    self.isBangEvent = false
    self:createMenuNode()
    self:createPaomadeng()

    self.checkLongTouch = false; --是否开始检测长按
    self.nCheckTime = 0; --检测的时间
    
    self.needShowBeforeTimer = false -- 是否需要显示开始倒计时
    self.BeforeTime = 0 -- 倒计时时间
    self.needShowTimer = false -- 是否需要显示晋级赛倒计时
    self.JinjiTime = 0 -- 晋级赛时间
    self.m_curTime = 0 -- 当前时间
    self.createrId = 0

    self.pTarget = nil -- 锁定的目标
    self.pTartgetPos = cc.p(test_cx, test_cy) -- 自动攻击位置
    self.nAutoAttackCheckTime = 0 -- 自动捕鱼检测的时间
    self.bAutoAttack = false -- 自动捕鱼
    self.nMaxInterval = CGameConfig.GetInstance().nMaxInterval
    self.m_pLockFishBubbles = {}
    self.m_hasGetPlayerData = false
    self._zouma = {}
    self._JinjiPromtionNeedScore = {}
    self._JinjiRewardScore = 0
    self._teamListData = {}
    -- 创建炮台 --
    self.panelCannons = {}
    for k, v in pairs(CGameConfig.GetInstance().CannonPos) do
        local id = k
        local posX = v.m_Position.x_
        local posY = v.m_Position.y_
        local direction = v.m_Direction
        local widgetCannon = LoadUIJson("fishgame2d/ui/FishGameUICannon.json")
        local panel1 = GetWidgetByName(widgetCannon, "Panel_1");
        local iconWaiting = GetWidgetByName(panel1, "iconWaiting");
        local bgCannon = GetWidgetByName(panel1, "bgCannon");
        local panelUI = GetWidgetByName(panel1, "panelUI");
        local btnSub = GetWidgetByName(panelUI, "btnSub");
        local btnPlus = GetWidgetByName(panelUI, "btnPlus");
        local bgUserinfo = GetWidgetByName(panelUI, "bgUserInfo");
        local labelCoin = GetWidgetByName(bgUserinfo, "labelCoin");
        local labelName = GetWidgetByName(bgUserinfo, "labelName");


        local cannon = GetWidgetByName(panel1, "cannon");
        cannon:setVisible(false)
        local labelCannon = GetWidgetByName(panel1, "labelCannon");
        labelCannon:setVisible(false)

        if posX == 0.32 then
            bgUserinfo:setPositionX(-180)
        else
            bgUserinfo:setPositionX(180)
        end

        if direction == 0 then
            -- 下面 --
            widgetCannon:setPosition(test_width * posX, 0)
            cannon:setPosition(0, test_height * (1 - posY))
            bgCannon:setRotation(0)
            panelUI:setPosition(0, 0)
            iconWaiting:setPosition(0, 100)

        elseif direction == 3.14 then
            -- 上面 --
            widgetCannon:setPosition(test_width * posX, test_height)
            cannon:setPosition(0, -test_height * posY)
            bgCannon:setRotation(180)

            panelUI:setPosition(0, -65)
            iconWaiting:setPosition(0, -100)
            bgUserinfo:setPositionY(15)
            labelCannon:setPosition(0, -55)
        end

        -- 添加炮台动画 --
        local nodeCannon = display.newNode()
        local size = cannon:getContentSize()
        nodeCannon:setPosition(size.width / 2, size.height / 2)
        nodeCannon:setRotation(180)

        cannon:addChild(nodeCannon)

        self:addChild(widgetCannon, 100)

        self.panelCannons[id] = {
            id = id,
            widgetCannon = widgetCannon,
            panel1 = panel1,
            panel1 = panel1,
            iconWaiting = iconWaiting,
            bgCannon = bgCannon,
            panelUI = panelUI,
            btnSub = btnSub,
            btnPlus = btnPlus,
            bgUserinfo = bgUserinfo,
            labelCoin = labelCoin,
            labelName = labelName,
            cannon = cannon,
            labelCannon = labelCannon,
            cannonDirection = direction,
            bUp = direction == 3.14,
            nodeCannon = nodeCannon,
        }

        self:resetChairStatus(id)
    end

    self.labelNotice = display.newTTFLabel({
        text = "",
        size = 48,
        align = cc.ui.TEXT_ALIGN_CENTER
    }):pos(test_cx, test_height * 0.5):hide():addTo(self, 100)
    self.labelFireNotice = display.newTTFLabel({
        text = "",
        size = 48,
        align = cc.ui.TEXT_ALIGN_CENTER
    }):pos(test_cx, test_height * 0.5):hide():addTo(self, 100)

    if cc.UserDefault:getInstance():getFloatForKey("first_enter_fish") ~= 1 then
        self:UpdateSoundSwitch(true)
        cc.UserDefault:getInstance():setFloatForKey("first_enter_fish", 1)
        cc.UserDefault:getInstance():setFloatForKey("sound", 1)
        cc.UserDefault:getInstance():setFloatForKey("soundEffect", 1)
    else
    self:UpdateSoundSwitch(cc.UserDefault:getInstance():getFloatForKey("sound") == 1
        or cc.UserDefault:getInstance():getFloatForKey("soundEffect") == 1)
    end
    self:UpdateAutoAttak(false)
    self:UpdateLockAttack(false)
    
    self.m_LastFireTime = 0

    local paomaStr = {
        [1] = "小提示：点击屏幕右边的“自动”按钮，让您捕鱼更轻松。",
        [2] = "小提示：点击屏幕右边的“锁定”按钮，可以自动锁定目标。在您炮右边的“换”，可以更换锁定目标。",
        [3] = "小提示：在您炮的两边的“+，-”按钮，可以增加或减少获得金币的倍数。",
        [4] = "小提示：打爆特殊物种，获得特殊的能力和更多的酬劳。",
    }

    local scheduler = cc.Director:getInstance():getScheduler()  
    local schedulerID = nil
    local tipsNum = 0
    schedulerID = scheduler:scheduleScriptFunc(function()  
        tipsNum = tipsNum + 1
        local y = tipsNum % 300

        if y == 0 then
            local m = math.floor(tipsNum / 300)
            self:onSysNotice(paomaStr[m])
            if m == #paomaStr then
                tipsNum = 0
            end
        end
    end,1,false)

    local function teamCallBack(sender, eventType)
        if eventType == ccui.TouchEventType.ended then
            if self.teamUI then
                return
            end

            local teamUI = ccs.GUIReader:getInstance():widgetFromJsonFile("fishgame2d/caijinEvent/teamEvent.json")
            self:addChild(teamUI, 129)
            self.teamUI = teamUI
            self:initTeamUI()
            self:refreshTeamList()
        end
    end

    --按钮
    self._team = ccui.Button:create("fishgame2d/caijinEvent/btn_fishTeam.png", "fishgame2d/caijinEvent/btn_fishTeam.png")
    self._team:setPosition(100,360)
    self:addChild(self._team)   
    self._team:setSwallowTouches(true)
    self._team:addTouchEventListener(teamCallBack)
    self._team:setVisible(false)
end

function FishGame2DUI:onEnter()
end

function FishGame2DUI:onExit() 
end

function FishGame2DUI:initTeamUI()

    local teamList = ccui.Helper:seekWidgetByName(self.teamUI, "ListView_teamList")
    self.teamList = teamList

    local inputID = ccui.Helper:seekWidgetByName(self.teamUI, "TextField_Input")

    local function CallBack(sender, eventType)
        if eventType == ccui.TouchEventType.ended then
            if sender.tag == 1 then
                local gameId = tonumber(inputID:getString()) or 0
                FishGame2DLogic.GetInstance():SendMsgInvite(gameId)
            elseif sender.tag == 2 then
                FishGame2DLogic.GetInstance():SendMsgLeaveTeam()
            elseif sender.tag == 3 then
                self.teamUI:removeFromParent()
                self.teamUI = nil
                self.teamList = nil
            end
        end
    end
    local inviteBtn = ccui.Helper:seekWidgetByName(self.teamUI, "Button_Invite")
    inviteBtn.tag = 1
    inviteBtn:addTouchEventListener(CallBack)

    local leaveBtn = ccui.Helper:seekWidgetByName(self.teamUI, "Button_LeaveTeam")
    leaveBtn.tag = 2
    leaveBtn:addTouchEventListener(CallBack)


    local closeBtn = ccui.Helper:seekWidgetByName(self.teamUI, "Button_Close")
    closeBtn.tag = 3
    closeBtn:addTouchEventListener(CallBack)
end

function FishGame2DUI:onEventInvited(info)
    local bg = display.newScale9Sprite("fishgame2d/caijinEvent/bg_caijing.png")
    bg:setContentSize(400, 200)
    bg:pos(640, 360)
    self:addChild(bg, 200)

    local text = nil
    if info.result == 0 then
        text = cc.LabelTTF:create("邀请发送成功！", "", 30)
    elseif info.result == 100 then
        text = cc.LabelTTF:create("找不到玩家信息", "", 30)
    elseif info.result == 101 then
        text = cc.LabelTTF:create("该玩家已在队伍", "", 30)
    elseif info.result == 102 then
        text = cc.LabelTTF:create("请不要频繁邀请同一玩家", "", 30)
    elseif info.result == 103 then
        text = cc.LabelTTF:create("非队长玩家不能发起邀请", "", 30)
    elseif info.result == 104 then
         text = cc.LabelTTF:create("不能邀请自己", "", 30)
    end
    text:pos(200, 100)
    bg:addChild(text)

    bg:runAction(transition.sequence({
        cc.DelayTime:create(1),
        cc.CallFunc:create(function(sender)
            sender:removeSelf()
        end)
    }))
end

function FishGame2DUI:onEventAgreeInvite(info)
    --我接受、拒绝 邀请
end

function FishGame2DUI:onEventLeaveTeam(info)
    --自己离队
    dump(info)
    local bg = display.newScale9Sprite("fishgame2d/caijinEvent/bg_caijing.png")
    bg:setContentSize(400, 200)
    bg:pos(640, 360)
    self:addChild(bg, 200)

    local text 
    if info.result == 100 then
        text = cc.LabelTTF:create("当前没有加入队伍", "", 30)
    else
        text = cc.LabelTTF:create("你已离开队伍", "", 30)
    end
    text:pos(200, 100)
    bg:addChild(text)

    self._teamListData = {}
    self:refreshTeamList()
    
    bg:runAction(transition.sequence({
        cc.DelayTime:create(1),
        cc.CallFunc:create(function(sender)
            sender:removeSelf()
        end)
    }))
end

function FishGame2DUI:onEventInviteInfo(info)
    --通知我被邀请了
    if self.createrId == info.createrid then
        return
    end

    self.createrId = info.createrid
    local bg = display.newScale9Sprite("fishgame2d/caijinEvent/bg_caijing.png")
    bg:setContentSize(600, 300)
    bg:pos(640, 360)
    self:addChild(bg, 200)

    local function CallBack(sender, eventType)
        if eventType == ccui.TouchEventType.ended then
            bg:removeFromParent()
            self.createrId = 0
            FishGame2DLogic.GetInstance():SendMsgAgreeTeamInvite(info.createrid, sender.tag)
        end
    end

    local text = cc.LabelTTF:create("玩家["..info.nickname.."]邀请你加入队伍！", "", 30)
    text:pos(300, 200)
    bg:addChild(text)

    local agreeBtn = ccui.Button:create("fishgame2d/caijinEvent/btn_team_com.png", "fishgame2d/caijinEvent/btn_team_com.png")
    agreeBtn:setTitleText("同意")
    agreeBtn:setTitleColor(cc.c3b(0,255,0))
    agreeBtn:setTitleFontSize(30)
    agreeBtn:setPosition(150,100)
    bg:addChild(agreeBtn)   
    agreeBtn:setSwallowTouches(true)
    agreeBtn.tag = 1
    agreeBtn:addTouchEventListener(CallBack)

    local denialBtn = ccui.Button:create("fishgame2d/caijinEvent/btn_team_com.png", "fishgame2d/caijinEvent/btn_team_com.png")
    denialBtn:setTitleText("拒绝")
    denialBtn:setTitleColor(cc.c3b(255,0,0))
    denialBtn:setTitleFontSize(30)
    denialBtn:setPosition(450,100)
    bg:addChild(denialBtn)
    denialBtn:setSwallowTouches(true)
    denialBtn.tag = 0
    denialBtn:addTouchEventListener(CallBack)

    bg:runAction(transition.sequence({
        cc.DelayTime:create(5),
        cc.CallFunc:create(function(sender)
            sender:removeSelf()
            self.createrId = 0
        end)
    }))
end

function FishGame2DUI:onEventDeleteTeammate(info)
    --玩家离队
    local deleteBg = display.newScale9Sprite("fishgame2d/caijinEvent/bg_caijing.png")
    deleteBg:setContentSize(500, 200)
    deleteBg:pos(640, 360)
    self:addChild(deleteBg, 200)

    local text = cc.LabelTTF:create("玩家["..info.nickname.."]离开队伍", "", 30)
    text:pos(250, 130)
    deleteBg:addChild(text)

    if info.is_team_dissolve == 1 or info.is_creater_leave == 1 then
        local text2 = cc.LabelTTF:create("队伍已解散", "", 30)
        text2:pos(250, 70)
        deleteBg:addChild(text2)
        self._teamListData = {}
    else
        for i = 1, #self._teamListData do
            if self._teamListData[i].id == info.leave_userid then
                table.remove(self._teamListData, i)
            end
        end
    end

    self:refreshTeamList()
    deleteBg:runAction(transition.sequence({
        cc.DelayTime:create(1),
        cc.CallFunc:create(function(sender)
            sender:removeSelf()
        end)
    }))
end

function FishGame2DUI:onEventInsertTeammate(info)
    --玩家加入队伍
    local bg = display.newScale9Sprite("fishgame2d/caijinEvent/bg_caijing.png")
    bg:setContentSize(500, 200)
    bg:pos(640, 360)
    self:addChild(bg, 200)

    local text = cc.LabelTTF:create("玩家["..info.nickname.."]加入队伍", "", 30)
    text:pos(250, 100)
    bg:addChild(text)

    bg:runAction(transition.sequence({
        cc.DelayTime:create(1),
        cc.CallFunc:create(function(sender)
            sender:removeSelf()
        end)
    }))
end

function FishGame2DUI:refreshTeamList()
    if not self.teamList then
        return
    end

    self.teamList:removeAllItems()
    
    for i = 1, #self._teamListData do
        local custom = ccui.Layout:create()
        custom:setContentSize(480, 45)

        local id = cc.LabelTTF:create(tostring(self._teamListData[i].id), "", 30)
        id:pos(75, 22)
        custom:addChild(id)

        local name = cc.LabelTTF:create(self._teamListData[i].nickname, "", 30)
        name:pos(340, 22)
        custom:addChild(name)

        self.teamList:pushBackCustomItem(custom)
    end
end

function FishGame2DUI:onRefreshTeamList(info)
    self._teamListData = {}
    
    for i = 1, info.count do
        table.insert(self._teamListData, {
            id = info.list_member[i].gameid,
            nickname = info.list_member[i].nickname
        })
    end
    self:refreshTeamList()
end

function FishGame2DUI:onEventBeforeTime(beforetime)
    if beforetime > 0 and beforetime <= 60 then
        self.BeforeTime = beforetime
        self.needShowBeforeTimer = true

        self.beforeTimeBg = display.newSprite("fishgame2d/caijinEvent/bg_daojishi.png")
        self.beforeTimeBg:pos(640, 360)
        self:addChild(self.beforeTimeBg)

        local text = display.newSprite("fishgame2d/caijinEvent/text_gameStart.png")
        text:pos(437, 110)
        self.beforeTimeBg:addChild(text)

        self.beforeTimeLabel = cc.LabelAtlas:_create("", "fishgame2d/caijinEvent/Label_Time.png", 38, 39, 48)
        self.beforeTimeLabel:setAnchorPoint(0.5, 0.5)
        self.beforeTimeLabel:pos(437,50)
        self.beforeTimeBg:addChild(self.beforeTimeLabel)
    end
end
    
--刷新排名数据
function FishGame2DUI:refreshRankData(count, rankData)
    if false == self.isBangEvent then
        return
    end
    if not self.rankList then return end

    self.rankList:removeAllItems()

    for i = 1, count do
        local custom = ccui.Layout:create()
        custom:setContentSize(550, 52)

        if i % 2 == 0 then
            local bg = display.newSprite("fishgame2d/caijinEvent/lable_lable.png")
            bg:pos(275, 26)
            custom:addChild(bg)
        end

        if rankData[i].rank < 4 then
            local icon = display.newSprite(string.format("fishgame2d/caijinEvent/icon_rank_%d.png", rankData[i].rank))
            icon:pos(50, 26)
            custom:addChild(icon)
        else
           local rank = cc.LabelTTF:create(tostring(rankData[i].rank), "", 30)
            rank:pos(50, 26)
            custom:addChild(rank)            
        end

        local name = cc.LabelTTF:create(tostring(rankData[i].nickname), "", 30)
        name:pos(200, 26)
        custom:addChild(name)

        local score = cc.LabelTTF:create(tostring(rankData[i].score), "", 30)
        score:pos(425, 26)
        custom:addChild(score)
        self.rankList:pushBackCustomItem(custom)
    end
end

--刷新我的比赛数据
function FishGame2DUI:refreshMyRank(rank, score)
    if false == self.isBangEvent then
        return
    end

    if self.myRank then
        self.myRank:setString(rank)
    end

    if self.myRankBg then
        self.myRankBg:removeAllChildren()
        local rank = cc.LabelTTF:create(tostring(rank), "", 30)
        rank:setColor(cc.c3b(10, 255, 10))
        rank:setAnchorPoint(0, 0.5)
        rank:pos(65, 35)
        self.myRankBg:addChild(rank)
    end

    if self.myScore then
        self.myScore:setString(score)
    end
end

--活动结束框
function FishGame2DUI:onLotteryReward(info)
    local layerBackground = display.newLayer()
    self:addChild(layerBackground, 129)   
    local animation = ccs.Armature:create("FishEvent")
    animation:align(display.CENTER, 640,600)
    animation:addTo(layerBackground)
    animation:getAnimation():play("Effect_SendGold")

    -- 添加帧事件回调
    animation:getAnimation():setMovementEventCallFunc(function(arm, eventType, movmentID)
        if eventType == ccs.MovementEventType.complete then
            layerBackground:removeFromParent()
        
            self.isBangEvent = false
            self.rewardImg:setVisible(true)
            self.rewardlistImg:setVisible(true)
            self.rewardList:setVisible(true)
            self.eventBangBg:setVisible(true)
            self.closeButton:setVisible(true)

            self.rankLayer:setVisible(false)
            self.rankButton:setVisible(false)
            
            self.rewardList:removeAllItems()

            for i = 1, info.llistCount do
                local custom = ccui.Layout:create()
                custom:setContentSize(550, 52)
                if i % 2 == 0 then
                    local bg = display.newSprite("fishgame2d/caijinEvent/lable_lable.png")
                    bg:pos(275, 26)
                    custom:addChild(bg)
                end

                if info.listInfo[i].rank < 4 then
                    local icon = display.newSprite(string.format("fishgame2d/caijinEvent/icon_rank_%d.png", info.listInfo[i].rank))
                    icon:pos(50, 26)
                    custom:addChild(icon)
                else
                    local rank = cc.LabelTTF:create(tostring(info.listInfo[i].rank), "", 26)
                    rank:pos(50, 26)
                    custom:addChild(rank)
                end

                local nickname = cc.LabelTTF:create(tostring(info.listInfo[i].nickname), "", 26)
                nickname:pos(200, 26)
                custom:addChild(nickname)

                local score = cc.LabelTTF:create(tostring(info.listInfo[i].prize_score), "", 26)
                score:pos(425, 26)
                custom:addChild(score)
                self.rewardList:pushBackCustomItem(custom)
            end
        end
    end)
end

--刷新彩金池
function FishGame2DUI:onEventSystemInfo(score)
    if not self.bangScore then
        return
    end
    self.bangScore:setString(string.format("%d", score))
end

function FishGame2DUI:removeBangUI()
    self.eventBangUI:removeFromParent()
    self.myRankBg:removeFromParent()
end

function FishGame2DUI:onLotteryBegan()
    if self.beforeTimeBg then
        self.beforeTimeBg:removeFromParent()
        self.beforeTimeBg = nil
        self.beforeTimeLabel = nil
        self.needShowBeforeTimer = false
    end
    self:refreshLotteryState(1)
end

function FishGame2DUI:onLotteryEnd()
    self:refreshLotteryState(4)
end

--更新蚌状态
function FishGame2DUI:refreshLotteryState(state)
    if not self.isBangEvent then
        self:showBangEvent()
    end

    local bang = {"bronze", "silver", "gold", "diamond"}
    local Bang = {"Bronze", "Silver", "Gold", "Diamond"}

    local layerBackground = display.newLayer()
    self:addChild(layerBackground, 129)   
    local animation = ccs.Armature:create("FishEvent")
    animation:align(display.CENTER, 640,630)
    animation:addTo(layerBackground)

    -- 添加帧事件回调
    animation:getAnimation():setMovementEventCallFunc(function(arm, eventType, movmentID)
        if eventType == ccs.MovementEventType.complete then
            layerBackground:removeFromParent()
        end
    end)
    self.bangImg:loadTexture("fishgame2d/caijinEvent/"..bang[app.FishRoomID+1]..state..".png")
    
    if 1 == state then
        animation:getAnimation():play("Effect_BangShow")
    elseif 2 == state or 3 == state then
        animation:getAnimation():play("Effect_BangOpen")
        if self.anim then
            self.anim:removeFromParent()
            self.anim = nil
        end
        local path = "Effect_"..Bang[app.FishRoomID+1]..state    
        self.anim = ccs.Armature:create("FishEvent")
        self.anim:align(display.CENTER, 640,630)
        self:addChild(self.anim, 128)
        self.anim:getAnimation():play(path)
    elseif 4 == state then
        animation:getAnimation():play("Effect_BangOpen")
        if self.anim then
            self.anim:removeFromParent()
            self.anim = nil
        end
    end
end

function FishGame2DUI:showBangEvent()
    self.isBangEvent = true
    self.isRankSel = false
    
    --animation --
    ccs.ArmatureDataManager:getInstance():addArmatureFileInfo("fishgame2d/caijinEvent/FishEvent/FishEvent.ExportJson")
    
    local myRankBg = display.newSprite("fishgame2d/caijinEvent/bg_rank_self.png")
    local myChairId = FishGame2DLogic.GetInstance():getMyChairId()
    if 0 == myChairId or 2 == myChairId then
        myRankBg:pos(1200, 110)
    else
        myRankBg:pos(300, 110)
    end
    self:addChild(myRankBg, 128)
    self.myRankBg = myRankBg

    local eventUI = ccs.GUIReader:getInstance():widgetFromJsonFile("fishgame2d/caijinEvent/CaiJingEvent.json")
    self:addChild(eventUI, 128)
    self.eventBangUI = eventUI

    self.bangImg = ccui.Helper:seekWidgetByName(eventUI, "Image_Bang")
    self.rewardImg = ccui.Helper:seekWidgetByName(eventUI, "Image_reward")
    self.rewardlistImg = ccui.Helper:seekWidgetByName(eventUI, "Image_rewardlist")
    --奖励列表
    local rewardList = ccui.Helper:seekWidgetByName(eventUI, "ListView_Reward")
    self.rewardList = rewardList

    local bangScore = ccui.Helper:seekWidgetByName(eventUI, "Label_BangScore")
    bangScore:enableOutline(cc.c4b(255,0,0,255), 2) --描边颜色为红色，描边宽度为2pix
    self.bangScore = bangScore

    --隐藏背景图片
    local eventBangBg = ccui.Helper:seekWidgetByName(eventUI, "Image_Bg")
    eventBangBg:setVisible(false)
    self.eventBangBg = eventBangBg

    --隐藏排名
    local rankLayer = ccui.Helper:seekWidgetByName(eventUI, "Panel_Rank")
    rankLayer:setVisible(false)
    self.rankLayer = rankLayer

    local rankList = ccui.Helper:seekWidgetByName(rankLayer, "ListView_RankList")
    self.rankList = rankList

    local myRank = ccui.Helper:seekWidgetByName(rankLayer, "Label_MyRank")
    self.myRank = myRank

    local myScore = ccui.Helper:seekWidgetByName(rankLayer, "Label_MyScore")
    self.myScore = myScore

    local function touchEventListener(sender, eventType)
        if eventType == ccui.TouchEventType.ended then
            if 1 == sender.tag then
                if self.isBangEvent == false then
                    return
                end
                
                self.isRankSel = not self.isRankSel
                rankLayer:setVisible(self.isRankSel)
                local s = 0
                if true == self.isRankSel then
                    s = 1
                end
                FishGame2DLogic.GetInstance():SendMsgLotteryList(s)
            else
                self:removeBangUI()
                return
            end
            eventBangBg:setVisible(self.isRankSel)
        end
    end

    --排名按钮
    self.rankButton = ccui.Helper:seekWidgetByName(eventUI, "Button_Rank")
    self.rankButton.tag = 1
    self.rankButton:addTouchEventListener(touchEventListener)

    --关闭按钮
    self.closeButton = ccui.Helper:seekWidgetByName(eventUI, "Button_Close")
    self.closeButton.tag = 2
    self.closeButton:addTouchEventListener(touchEventListener)
end

function FishGame2DUI:showMyApplyResult(result)
    dump(result)
    if result ~= 0 then
        return
    end

    --报名成功
    local resultImg = display.newSprite("fishgame2d/caijinEvent/label_bmcg.png")
    resultImg:pos(640, 410)
    self:addChild(resultImg, 128)

    self:showApplyBtn(false)

    resultImg:runAction(transition.sequence({
        cc.DelayTime:create(2),
        cc.CallFunc:create(function(sender)
            sender:removeSelf()
        end)
    }))
end

function FishGame2DUI:refreshMyPromotionScore(score)
    self.curCount:setString(tostring(score).."/")    
end

function FishGame2DUI:showApplyCount(count)
    if self.amIInGame then
        return
    end

    self.curCount:setString(tostring(count).."/")
    self.needCount:setString("20")
end

function FishGame2DUI:showApplyFull()
    self.curTurn = 1
    self.amIInGame = true

    local text = display.newSprite("fishgame2d/caijinEvent/label_turnbegan.png")
    text:pos(640, 360)
    self:addChild(text, 128)

    local labelTurn = cc.LabelAtlas:_create(tostring(self.curTurn), "fishgame2d/caijinEvent/jjs_num.png", 24, 37, 48)
    labelTurn:setAnchorPoint(0.5, 0.5)
    labelTurn:pos(55,20)
    text:addChild(labelTurn)
    
    text:runAction(transition.sequence({
        cc.DelayTime:create(5),
        cc.CallFunc:create(function(sender)
            sender:removeSelf()
        end)
    }))

    self.TurnImg:loadTexture("fishgame2d/caijinEvent/jjs_turn_"..self.curTurn..".png")
    self.TurnImg:setVisible(true)
    self.needShowTimer = true
    self.JinjiTime = self._JinjiPerTurnTime             
    self.remainTime:setVisible(true)
    self.proButton:setVisible(true)

    self.needCount:setString(tostring(self._JinjiPromtionNeedScore[self.curTurn]))
    self.rewardLabel:setString(tostring(self._JinjiRewardScore))
end

function FishGame2DUI:showMatchResult(info)
    self.rankList:removeAllItems()

    if 1 == info.is_upgrade then
        self.curTurn = self.curTurn + 1
        local text = nil
        if self.curTurn <= 5 then
            text = display.newSprite("fishgame2d/caijinEvent/label_turnbegan.png")

            local labelTurn = cc.LabelAtlas:_create(tostring(self.curTurn), "fishgame2d/caijinEvent/jjs_num.png", 24, 37, 48)
            labelTurn:setAnchorPoint(0.5, 0.5)
            labelTurn:pos(55,20)
            text:addChild(labelTurn)

            self.TurnImg:loadTexture("fishgame2d/caijinEvent/jjs_turn_"..self.curTurn..".png")
            self.needShowTimer = true
            self.JinjiTime = self._JinjiPerTurnTime
            
            self.curCount:setString("0/")
            self.needCount:setString(tostring(self._JinjiPromtionNeedScore[self.curTurn]))
        else
            text = display.newSprite("fishgame2d/caijinEvent/label_promotion.png")

            local reward = cc.LabelAtlas:_create(string.format("%d", info.prize_score), "fishgame2d/caijinEvent/jjs_num.png", 24, 37, 48)
            reward:setAnchorPoint(0, 0.5)
            reward:pos(530,20)
            text:addChild(reward)

            if self.isJinJiMatchOn then
                self.curCount:setString("0/")
                self.needCount:setString("20")
                self:showApplyBtn(self.isJinJiMatchOn)
            else
                self.needShowTimer = false
                self.eventUI:removeFromParent()
                self.eventUI = nil
            end
            self.amIInGame = false
        end

        text:pos(640, 360)
        self:addChild(text, 128)
    
        text:runAction(transition.sequence({
            cc.DelayTime:create(5),
            cc.CallFunc:create(function(sender)
                sender:removeSelf()
            end)
        }))
        
        return
    end

    self.curTurn = 1
    self.amIInGame = false
    self.curCount:setString("0/")
    self.needCount:setString("20")
    self:showApplyBtn(self.isJinJiMatchOn)
    
    if 0 ~= info.prize_score then
        local text = display.newSprite("fishgame2d/caijinEvent/label_outreward.png")
        local scoreStr = string.format("%d", info.prize_score)
        local reward = cc.LabelAtlas:_create(scoreStr, "fishgame2d/caijinEvent/jjs_num.png", 24, 37, 48)
        reward:setAnchorPoint(0.5, 0.5)
        reward:pos(575,20)
        text:addChild(reward)
        text:pos(640, 360)
        self:addChild(text, 128)

        text:runAction(transition.sequence({
            cc.DelayTime:create(5),
            cc.CallFunc:create(function(sender)
                sender:removeSelf()
            end)
        }))
    elseif self.isJinJiMatchOn then
        local text = display.newSprite("fishgame2d/caijinEvent/label_outnoreward.png")
        text:pos(640, 360)
        self:addChild(text, 128)

        text:runAction(transition.sequence({
            cc.DelayTime:create(5),
            cc.CallFunc:create(function(sender)
                sender:removeSelf()
            end)
        }))
    end
end

function FishGame2DUI:refreshPromoteList(info)
    if not self.rankList then
        return
    end

    self.rankList:removeAllItems()
    for i = 1, info.count do
        local custom = ccui.Layout:create()
        custom:setContentSize(550, 52)
        if i % 2 == 0 then
            local bg = display.newSprite("fishgame2d/caijinEvent/lable_lable.png")
            bg:pos(275, 26)
            custom:addChild(bg)
        end

        if i < 4 then
            local icon = display.newSprite(string.format("fishgame2d/caijinEvent/icon_rank_%d.png", i))
            icon:pos(110, 26)
            custom:addChild(icon)
        else
            local rank = cc.LabelTTF:create(tostring(i), "", 26)
            rank:pos(110, 26)
            custom:addChild(rank)            
        end

        local nickname = cc.LabelTTF:create(tostring(info.listInfo[i].nickname), "", 26)
        nickname:pos(390, 26)
        custom:addChild(nickname)
        self.rankList:pushBackCustomItem(custom)
    end
end

function FishGame2DUI:showApplyBtn(isShow)
    if isShow then
        self.applyButton:setVisible(true)
        self.proButton:setVisible(false)
        self.eventBg:setVisible(false)
        self.needShowTimer = false
        self.remainTime:setVisible(false)
        self.TurnImg:setVisible(false)
        self.proLayer:setVisible(false)
    else
        self.applyButton:setVisible(false)
    end
end

function FishGame2DUI:onJinJiMatchOver()
    self.isJinJiMatchOn = false
    self._team:setVisible(false)
    if self.amIInGame == false then
        self.eventUI:removeFromParent()
        self.eventUI = nil
    end
end

--加载晋级赛资源
function  FishGame2DUI:LoadJinJiMatchResource()
    local eventUI = ccs.GUIReader:getInstance():widgetFromJsonFile("fishgame2d/caijinEvent/PromotionEvent.json")
    self:addChild(eventUI, 128)
    self.eventUI = eventUI

    --背景图片
    local eventBg = ccui.Helper:seekWidgetByName(self.eventUI, "Image_Bg")
    eventBg:setVisible(false)
    self.eventBg = eventBg
        
    local light = ccui.Helper:seekWidgetByName(self.eventUI, "Image_ProLight")    
    light:runAction(cc.RepeatForever:create(cc.RotateBy:create(5, 360)))       
        
    --晋级名单Layer
    local proLayer = ccui.Helper:seekWidgetByName(self.eventUI, "Panel_Promotion")
    self.proLayer = proLayer

    --晋级列表
    local rankList = ccui.Helper:seekWidgetByName(proLayer, "ListView_RankList")
    self.rankList = rankList

    --时间
    self.remainTime = ccui.Helper:seekWidgetByName(self.eventUI, "Label_Time")

    --turn
    self.TurnImg = ccui.Helper:seekWidgetByName(self.eventUI, "Image_Turn")

    --奖金
    self.rewardLabel = ccui.Helper:seekWidgetByName(self.eventUI, "Label_Reward")

    --当前、总共数目
    self.curCount = ccui.Helper:seekWidgetByName(eventUI, "Label_CurScore")
    self.needCount = ccui.Helper:seekWidgetByName(eventUI, "Label_NeedScore")

    local function touchEventListener(sender, eventType)
        if eventType == ccui.TouchEventType.ended then
            if 1 == sender.tag then                
                self.isRankSel = not self.isRankSel
                proLayer:setVisible(self.isRankSel)
                eventBg:setVisible(self.isRankSel)
            else
                FishGame2DLogic.GetInstance():SendMsgApplyMatch()
            end
        end
    end

    --晋级名单按钮
    self.proButton = ccui.Helper:seekWidgetByName(eventUI, "Button_Protomtion")
    self.proButton.tag = 1
    self.proButton:addTouchEventListener(touchEventListener)
    self.proButton:setVisible(false)

    --报名按钮
    self.applyButton = ccui.Helper:seekWidgetByName(eventUI, "Button_Apply")
    self.applyButton.tag = 3
    self.applyButton:addTouchEventListener(touchEventListener)
    self.applyButton:setVisible(false)
end

--晋级赛开始
function FishGame2DUI:JinjiMatchStart()
    self._team:setVisible(true)
    --加载资源
    self.isJinJiMatchOn = true
    self:LoadJinJiMatchResource()
    self.proButton:setVisible(true)
    self.JinjiTime = self._JinjiPerTurnTime 
    self.needShowTimer = true
    self.remainTime:setVisible(true)
    self.rewardLabel:setString(tostring(self._JinjiRewardScore))
    self.needCount:setString(tostring(self._JinjiPromtionNeedScore[1]))
    
    self.curTurn = 1
    self.amIInGame = true
    self.TurnImg:loadTexture("fishgame2d/caijinEvent/jjs_turn_"..self.curTurn..".png")
    self.TurnImg:setVisible(true)

    local info = display.newSprite("fishgame2d/caijinEvent/label_turnbegan.png")

    local labelTurn = cc.LabelAtlas:_create(tostring(self.curTurn), "fishgame2d/caijinEvent/jjs_num.png", 24, 37, 48)
    labelTurn:setAnchorPoint(0.5, 0.5)
    labelTurn:pos(55,20)
    info:addChild(labelTurn)

    info:runAction(transition.sequence({
        cc.DelayTime:create(5),
        cc.CallFunc:create(function(sender)
            sender:removeSelf()
        end)
    }))
end

--晋级赛系统信息
function FishGame2DUI:onJinjiMatchSystemInfo(info)
    dump(info)
    self._JinjiPromtionNeedScore[1] = info.score1
    self._JinjiPromtionNeedScore[2] = info.score2
    self._JinjiPromtionNeedScore[3] = info.score3
    self._JinjiPromtionNeedScore[4] = info.score4
    self._JinjiPromtionNeedScore[5] = info.score5
    self._JinjiRewardScore = info.award_prize / 10000
    self._JinjiPerTurnTime = info.secPerTurn
    self.firstFiveMin = 0

    if info.gameStartSec > 0 then
        --比赛已经开始
        self.isJinJiMatchOn = true
        self:LoadJinJiMatchResource()
        self.rewardLabel:setString(tostring(self._JinjiRewardScore))

        if info.gameStartSec < info.secPerTurn then
            --前5分钟，直接进入比赛
            self.firstFiveMin = info.secPerTurn - info.gameStartSec
        end
        self._team:setVisible(true)
    elseif info.gameStartSec >= -600 then
        self._team:setVisible(true)
    end
end

--晋级赛个人信息
function FishGame2DUI:onJinjiMatchUserInfo(info)
    dump(info)

    if self.curCount and info.myCurScore ~= 0 then
        self.curCount:setString(tostring(info.myCurScore))
    end
    
    if self.firstFiveMin > 0 then
        self.curTurn = 1
        self.amIInGame = true
        self.TurnImg:loadTexture("fishgame2d/caijinEvent/jjs_turn_1.png")
        self.TurnImg:setVisible(true)
        self.needCount:setString(tostring(self._JinjiPromtionNeedScore[1]))

        self.JinjiTime = self.firstFiveMin
        self.needShowTimer = true
        self.remainTime:setVisible(true)
        self.proButton:setVisible(true)
        return
    end

    if info.match_level ~= 0 then
        self.curTurn = info.match_level
        self.amIInGame = true
        self.TurnImg:loadTexture("fishgame2d/caijinEvent/jjs_turn_"..self.curTurn..".png")
        self.TurnImg:setVisible(true)
        local score = self._JinjiPromtionNeedScore[self.curTurn]
        self.needCount:setString(tostring(score))
    end

    if info.match_surplus_second ~= 0 then
        self.JinjiTime = info.match_surplus_second
        self.needShowTimer = true
        self.remainTime:setVisible(true)
        self.proButton:setVisible(true)
    end

    if self.isJinJiMatchOn and info.match_level == 0 then
        self:showApplyBtn(true)
    end
end

function FishGame2DUI:onEventUpdatePlayerData()
    if not self.m_hasGetPlayerData then
        self.m_hasGetPlayerData = true
    end
end

--系统主动回调的方法，每帧调用的定时器
function FishGame2DUI:_onInterval(dt)
    --刷新活动开始前倒计时
    if self.needShowBeforeTimer then
        local time = math.floor(socket.gettime())
        if self.m_curTime ~= time then
            self.m_curTime = time
            self.BeforeTime = self.BeforeTime - 1
            
            if self.BeforeTime < 10 then
                local timeStr = "0"..tostring(self.BeforeTime)
                self.beforeTimeLabel:setString(timeStr)
            else
                local timeStr = tostring(self.BeforeTime)
                self.beforeTimeLabel:setString(timeStr)
            end

            if self.BeforeTime == 0 then
                self.needShowBeforeTimer = false
                self.m_curTime = 0
            end
        end
    end

    --刷新活动倒计时
    if self.needShowTimer then
        local time = math.floor(socket.gettime())
        if self.m_curTime ~= time then
            self.m_curTime = time
            self.JinjiTime = self.JinjiTime - 1

            local min = math.floor(self.JinjiTime / 60)
            local sec = self.JinjiTime % 60
            
            if sec < 10 then
                local timeStr = "0"..tostring(min)..":0"..tostring(sec)
                self.remainTime:setString(timeStr)
            else
                local timeStr = "0"..tostring(min)..":"..tostring(sec)
                self.remainTime:setString(timeStr)
            end

            
            if self.JinjiTime == 0 then
                self.needShowTimer = false
                self.m_curTime = 0
            end
        end
    end

    local logic = FishGame2DLogic.GetInstance()

    -- 更新镜像时刷新位置 --
    local mirrorShow = CGameConfig.GetInstance():MirrorShow()
    if self.mCurMirror ~= mirrorShow  and self.m_hasGetPlayerData then
        self.mCurMirror = mirrorShow
        for chairId = CHAIR_ID.START, CHAIR_ID.END do
            local player = logic:GetChairPlayer(chairId)
            if player then
                self:onPlayerEnter(player)
            else
                self:resetChairStatus(chairId)
            end

            local bubbles = self.m_pLockFishBubbles[chairId]
            if table.nums(bubbles or {}) ~= 0 then
                if not tolua.isnull(bubbles.mPanel) then
                    bubbles.mPanel:removeSelf()
                end
            end
        end
    end

    -- 更新炮台位置 --
    local myChairId = logic:getMyChairId()
    for chairId = CHAIR_ID.START, CHAIR_ID.END do
        local player = logic:GetChairPlayer(chairId)
        local playerPanel = self:GetChairPanel(chairId)

        if player and playerPanel then
            local lockFishId = player:GetLockFishID()

            -- 更新锁定鱼 --
            -- 当前没有锁定的鱼
            local lockFish
            if lockFishId ~= 0 then
                lockFish = MyObjectManager.GetInstance():FindFish(lockFishId)

                if lockFish then
                    local x, y = lockFish:GetPosition().x, lockFish:GetPosition().y
                    x, y = CGameConfig.GetInstance():ConvertCoord(x, y)


                    if myChairId == chairId then
                        self.pTartgetPos = cc.p(x, y)
                    end

                    local angle = self:updateCannonRotationToPosition(chairId, cc.p(x, y))


                    if CGameConfig.GetInstance():MirrorShow() then
                        player:GetCannon():SetCannonAngle(-angle - math.pi / 2)
                    else
                        player:GetCannon():SetCannonAngle(angle + math.pi / 2)
                    end

                    -- 更新位置 --
                    local _x, _y = CGameConfig.GetInstance():GetCannonPosition(chairId)
                    _x, _y = CGameConfig.GetInstance():ConvertCoord(_x, _y)

                    local cp = cc.p(_x, _y)

                    local bubbles = self.m_pLockFishBubbles[chairId]
                    if table.nums(bubbles or {}) == 0 then

                        self.m_pLockFishBubbles[chairId] = {}
                        bubbles = self.m_pLockFishBubbles[chairId]

                        bubbles.mMain = display.newSprite(string.format("fishgame2d/LockFish/lock_flag_%d.png", chairId + 1))
                        self:addChild(bubbles.mMain, 100)

                        bubbles.mSubs = {}
                    end

                    bubbles.mMain:setPosition(x, y)

                    if tolua.isnull(bubbles.mPanel) or bubbles.mFishId ~= lockFishId then
                        bubbles.mFishId = lockFishId

                        if not tolua.isnull(bubbles.mPanel) then
                            bubbles.mPanel:removeSelf()
                        end

                        bubbles.mPanel = display.newNode()
                        ccs.ArmatureDataManager:getInstance():addArmatureFileInfo("fishgame2d/animation/effect_gunsight/effect_gunsight.ExportJson")
                        local animation = ccs.Armature:create("effect_gunsight")
                        animation:getAnimation():play("Animation1")
                        animation:setAnchorPoint(0.5, 0.58)
                        bubbles.mPanel:addChild(animation)

                        if _x < 640 then
                            bubbles.mPanel:pos(cp.x - 220 * 0.8, cp.y + 120 * 0.8 * (playerPanel.bUp and -1 or 1))
                        else
                            bubbles.mPanel:pos(cp.x + 220 * 0.8, cp.y + 120 * 0.8 * (playerPanel.bUp and -1 or 1))
                        end
                        

                        local fishNode = display.newNode()
                        bubbles.mPanel:addChild(fishNode, -1)

                        local width, height = 0, 0

                        local fishConfig = lockFish:GetFishCofigData()
                        local visualData = CGameConfig.GetInstance():GetVisualConfigById(fishConfig.nVisualID)
                        if visualData then
                            for _, v in ipairs(visualData.ImageInfoLive) do
                                local animation = v.Image
                                local aniType = v.AniType
                                local ccsNode
                                if aniType == FISH_ANIMATION_TYPE.FRAME then
                                    ccs.ArmatureDataManager:getInstance():addArmatureFileInfo("fishgame2d/animation/" .. animation .. "/" .. animation .. ".ExportJson")
                                    ccsNode = ccs.Armature:create(animation)
                                    ccsNode:getAnimation():play(v.Name)

                                    width = math.max(ccsNode:getContentSize().width, width)
                                    height = math.max(ccsNode:getContentSize().height, height)
                                elseif aniType == FISH_ANIMATION_TYPE.SKELETON then
                                    ccsNode = sp.SkeletonAnimation:create("fishgame2d/animation/" .. animation .. "/" .. animation .. ".json",
                                        "fishgame2d/animation/" .. animation .. "/" .. animation .. ".atlas", 1)
                                    ccsNode:setAnimation(0, v.Name, true)

                                    width = 200
                                    height = 300
                                else
                                    ccs.ArmatureDataManager:getInstance():addArmatureFileInfo("fishgame2d/animation/" .. animation .. "/" .. animation .. ".ExportJson")
                                    ccsNode = ccs.Armature:create(animation)
                                    ccsNode:getAnimation():play(v.Name)

                                    width = math.max(ccsNode:getContentSize().width, width)
                                    height = math.max(ccsNode:getContentSize().height, height)
                                end
                                ccsNode:setScale(v.Scale, v.Scale)
                                ccsNode:setRotation(v.Direction)
                                ccsNode:setPosition(v.OffestX, v.OffestY)
                                fishNode:addChild(ccsNode)
                            end
                        end

                        if myChairId == chairId then
                            local panelBtn = ccs.GUIReader:getInstance():widgetFromJsonFile("fishgame2d/ui/fishGame2D_7.json")
                            local btnSwitch = GetWidgetByName(panelBtn, "Button_1");
                            btnSwitch:addTouchEventListener(handler(self, self._onBtnTouched_switchLockTarget))
                            panelBtn:pos(75, 0)
                            bubbles.mPanel:addChild(panelBtn, 1)
                        end

                        fishNode:setScale(math.min(150 / width, 150 / height))
                        self:addChild(bubbles.mPanel, 100)
                    end


                    local cannonPos = cp
                    local distance = CMathAide.CalcDistance(cannonPos.x, cannonPos.y, x, y) - 50
                    local angle = math.atan((cannonPos.y - y) / (cannonPos.x - x))
                    if cannonPos.x - x > 0 then
                        angle = angle + math.pi
                    end

                    local index = distance / 50
                    for i = 1, math.max(index, #bubbles.mSubs) do
                        local bubble = bubbles.mSubs[i]
                        if i > index then
                            if bubble then
                                bubble:removeSelf()
                                bubbles.mSubs[i] = nil
                            end
                        else
                            if not bubble then
                                bubble = display.newSprite("fishgame2d/LockFish/lock_line.png")
                                self:addChild(bubble, 100)
                            end
                            bubbles.mSubs[i] = bubble
                            bubble:setPosition(cannonPos.x + math.cos((angle)) * (distance * i / index + 50),
                                cannonPos.y + math.sin((angle)) * (i / index * distance + 50))
                        end
                    end
                end
            end

            if lockFish == nil then
                player:SetLockFishID(0)

                local bubbles = self.m_pLockFishBubbles[chairId]
                if bubbles then
                    if table.nums(bubbles) > 0 then
                        bubbles.mMain:removeSelf()
                        bubbles.mPanel:removeSelf()
                        for _, v in ipairs(bubbles.mSubs) do
                            v:removeSelf()
                        end
                    end

                    self.m_pLockFishBubbles[chairId] = {}
                end

                if myChairId == chairId then
                    self:UpdateLockAttack(false)
                end
            else
                if myChairId == chairId then
                    self:UpdateLockAttack(true)
                end
            end


            local cannon = player:GetCannon()
            local angle = cannon:GetCannonAngle()

            if CGameConfig.GetInstance():MirrorShow() then
                angle = angle
            else
                angle = angle + math.pi
            end

            if chairId == 2 or chairId == 3 then
                angle = -angle + math.pi
            end
 
            playerPanel.cannon:setRotation(math.deg(angle))
        else
            local bubbles = self.m_pLockFishBubbles[chairId]
            if bubbles then
                if table.nums(bubbles) > 0 then
                    bubbles.mMain:removeSelf()
                    bubbles.mPanel:removeSelf()
                    for _, v in ipairs(bubbles.mSubs) do
                        v:removeSelf()
                    end
                end
                self.m_pLockFishBubbles[chairId] = {}
            end
        end
    end

    -- 自动攻击 --
    if self.bAutoAttack then
        self.nAutoAttackCheckTime = self.nAutoAttackCheckTime + dt;
        if self.nAutoAttackCheckTime >= CGameConfig.GetInstance().nFireInterval / 1000 then
            self.nAutoAttackCheckTime = 0;

            self:_onFire(self.pTartgetPos);
        end
    elseif self.checkLongTouch then
        -- 按键后自动释放 --
        self.nCheckTime = self.nCheckTime + dt;
        if self.nCheckTime >= CGameConfig.GetInstance().nFireInterval / 1000 then
            self.nCheckTime = 0;
            self:_onFire(self.pTartgetPos);
        end
    end

    local timeNow = socket.gettime()
    if logic.m_dwLastFireTick ~= -1 then
        local leftTime = timeNow - logic.m_dwLastFireTick
        if leftTime * 1000 > self.nMaxInterval then
            app.table:sendStandup()
        elseif leftTime * 1000 > self.nMaxInterval - 10000 then
            self.labelFireNotice:setVisible(true)
            self.labelFireNotice:setString(string.format("由于您长时间没有发炮，系统将在 %d 秒后离开游戏！",
                math.ceil(-timeNow + logic.m_dwLastFireTick + self.nMaxInterval / 1000)))
        else
            self.labelFireNotice:setVisible(false)
        end
    else
        self.labelFireNotice:setVisible(false)
    end
end

function FishGame2DUI:createMenuNode()
    local _factor = 0.8
    local node = display.newNode()
    node:pos(0, test_height)

    local clippingNode_01 = display.newClippingRectangleNode(cc.rect(0, 0, 104 * _factor, 394 * _factor))
    local clippingNode_02 = display.newClippingRectangleNode(cc.rect(0, 0, 104 * _factor, 52 * _factor))
    clippingNode_01:pos(10, -456 * _factor)
    clippingNode_02:pos(10, -62 * _factor)


    local bgButtonList_01 = display.newScale9Sprite("fishgame2d/ui/bg_button_list.png")
    local bgButtonList_02 = display.newScale9Sprite("fishgame2d/ui/bg_button_list.png")
    bgButtonList_01:setScale(0.8)
    bgButtonList_02:setScale(0.8)

    bgButtonList_01:setAnchorPoint(0, 0)
    bgButtonList_02:setAnchorPoint(0, 1)
    bgButtonList_01:pos(0, 342 * _factor)
    bgButtonList_02:pos(0, 52 * _factor)

    clippingNode_01:addChild(bgButtonList_01)
    clippingNode_02:addChild(bgButtonList_02)

    local btnUp = cc.ui.UIPushButton.new({ normal = "fishgame2d/ui/btn_up.png", pressed = "fishgame2d/ui/btn_up_pressed.png", })
    local btnDown = cc.ui.UIPushButton.new({ normal = "fishgame2d/ui/btn_down.png", pressed = "fishgame2d/ui/btn_down_pressed.png", })
    local btnQuit = cc.ui.UIPushButton.new({ normal = "fishgame2d/ui/btn_quit.png", pressed = "fishgame2d/ui/btn_quit_pressed.png", })
    local btnSoundOff = cc.ui.UIPushButton.new({ normal = "fishgame2d/ui/btn_sound_off.png", pressed = "fishgame2d/ui/btn_sound_off_pressed.png", })
    local btnSoundOn = cc.ui.UIPushButton.new({ normal = "fishgame2d/ui/btn_sound_on.png", pressed = "fishgame2d/ui/btn_sound_on_pressed.png", })
    local btnInfo = cc.ui.UIPushButton.new({ normal = "fishgame2d/ui/btn_info.png", pressed = "fishgame2d/ui/btn_info_pressed.png", })
    btnUp:setScale(0.8)
    btnDown:setScale(0.8)
    btnQuit:setScale(0.8)
    btnSoundOff:setScale(0.8)
    btnSoundOn:setScale(0.8)
    btnInfo:setScale(0.8)


    local panelBtnAuto = display.newNode()
    local panelBtnLock = display.newNode()
    panelBtnAuto:setScale(0.8)
    panelBtnLock:setScale(0.8)
    local btnLock = cc.ui.UIPushButton.new({ normal = "fishgame2d/ui/btn_lock.png", pressed = "fishgame2d/ui/btn_lock.png", })
    local btnUnlock = cc.ui.UIPushButton.new({ normal = "fishgame2d/ui/btn_unlock.png", pressed = "fishgame2d/ui/btn_unlock.png", })
    local btnAuto = cc.ui.UIPushButton.new({ normal = "fishgame2d/ui/btn_auto.png", pressed = "fishgame2d/ui/btn_auto.png", })
    local btnHandle = cc.ui.UIPushButton.new({ normal = "fishgame2d/ui/btn_handle.png", pressed = "fishgame2d/ui/btn_handle.png", })


    btnUp:onButtonPressed(handler(self, self._onBtnPressed))
    btnDown:onButtonPressed(handler(self, self._onBtnPressed))
    btnQuit:onButtonPressed(handler(self, self._onBtnPressed))
    btnSoundOff:onButtonPressed(handler(self, self._onBtnPressed))
    btnSoundOn:onButtonPressed(handler(self, self._onBtnPressed))
    btnInfo:onButtonPressed(handler(self, self._onBtnPressed))
    btnLock:onButtonPressed(handler(self, self._onBtnPressed))
    btnUnlock:onButtonPressed(handler(self, self._onBtnPressed))
    btnAuto:onButtonPressed(handler(self, self._onBtnPressed))
    btnHandle:onButtonPressed(handler(self, self._onBtnPressed))

    btnQuit:onButtonPressed(handler(self, self._onBtnClicked_quit))
    btnSoundOff:onButtonPressed(handler(self, self._onBtnClicked_soundOff))
    btnSoundOn:onButtonPressed(handler(self, self._onBtnClicked_soundOn))
    btnInfo:onButtonPressed(handler(self, self._onBtnClicked_info))

    btnLock:onButtonPressed(handler(self, self._onBtnClicked_lock))
    btnUnlock:onButtonPressed(handler(self, self._onBtnClicked_unlock))
    btnAuto:onButtonPressed(handler(self, self._onBtnClicked_auto))
    btnHandle:onButtonPressed(handler(self, self._onBtnClicked_handle))


    local extend = false
    -- 往下 --
    btnUp:onButtonClicked(function(sender)
        if extend then return end
        extend = true
        node:stopAllActions()
        node:runAction(custom.FuncAction:create(0.2, function(sender, percent)
            bgButtonList_01:pos(0, 342 * (1 - percent) * _factor)

            btnQuit:pos(61 * _factor + 2, -63 * _factor - (161 * _factor - 53 * _factor) * percent)
            btnSoundOff:pos(61 * _factor + 2, -63 * _factor - (276 * _factor - 53 * _factor) * percent)
            btnSoundOn:pos(61 * _factor + 2, -63 * _factor - (276 * _factor - 53 * _factor) * percent)
            btnInfo:pos(61 * _factor + 2, -63 * _factor - (392 * _factor - 53 * _factor) * percent)

            btnUp:setVisible(false)
        end))
    end)

    -- 往上 --
    btnDown:onButtonClicked(function(sender)
        if not extend then return end
        extend = false
        node:stopAllActions()
        node:runAction(custom.FuncAction:create(0.2, function(sender, percent)
            bgButtonList_01:pos(0, 342 * percent * _factor)

            btnQuit:pos(61 * _factor + 2, -63 * _factor - (161 - 53) * (1 - percent) * _factor)
            btnSoundOff:pos(61 * _factor + 2, -63 * _factor - (276 - 53) * (1 - percent) * _factor)
            btnSoundOn:pos(61 * _factor + 2, -63 * _factor - (276 - 53) * (1 - percent) * _factor)
            btnInfo:pos(61 * _factor + 2, -63 * _factor - (392 - 53) * (1 - percent) * _factor)

            if percent == 1 then
                btnUp:setVisible(true)
            end
        end))
    end)

    btnUp:pos(61 * _factor + 2, -63 * _factor)
    btnDown:pos(61 * _factor + 2, -63 * _factor)
    btnQuit:pos(61 * _factor + 2, -63 * _factor)
    btnSoundOff:pos(61 * _factor + 2, -63 * _factor)
    btnSoundOn:pos(61 * _factor + 2, -63 * _factor)
    btnInfo:pos(61 * _factor + 2, -63 * _factor)
    panelBtnLock:pos(1171, -test_height + 273)
    panelBtnAuto:pos(1171, -test_height + 438)


    panelBtnLock:addChild(btnLock)
    panelBtnLock:addChild(btnUnlock)
    panelBtnAuto:addChild(btnAuto)
    panelBtnAuto:addChild(btnHandle)
    node:addChild(clippingNode_01)
    node:addChild(clippingNode_02)
    node:addChild(btnQuit)
    node:addChild(btnSoundOff)
    node:addChild(btnSoundOn)
    node:addChild(btnInfo)
    node:addChild(btnDown)
    node:addChild(btnUp)
    node:addChild(panelBtnAuto)
    node:addChild(panelBtnLock)
    self:addChild(node,999)

    self.btnSoundOff = btnSoundOff
    self.btnSoundOn = btnSoundOn
    self.btnLock = btnLock
    self.btnUnlock = btnUnlock
    self.btnAuto = btnAuto
    self.btnHandle = btnHandle
    self.panelBtnAuto = panelBtnAuto
    self.panelBtnLock = panelBtnLock

    return node
end

function FishGame2DUI:createPaomadeng()
    local bg = display.newSprite("fishgame2d/ui/paomadeng.png")
    bg:pos(600, 550)
    bg:setVisible(false)
    local iconNotice = display.newSprite("fishgame2d/ui/icon_notice.png")
    iconNotice:setAnchorPoint(0, 0.5)
    iconNotice:pos(6, 22)

    local clippingNode_01 = display.newClippingRectangleNode(cc.rect(0, 0, 640, 50))
    clippingNode_01:pos(50, 4)

    local label_zouma = display.newNode()

    clippingNode_01:addChild(label_zouma)
    bg:addChild(iconNotice)
    bg:addChild(clippingNode_01)
    self:addChild(bg,10)

    self._zoumaLabel = label_zouma
    self._panelPaomadeng = bg
end

function FishGame2DUI:_onTouch(event)
    if self.isBangEvent and self.isRankSel then
        self.isRankSel = false
        self.eventBangBg:setVisible(false)
        self.rankLayer:setVisible(false)
        FishGame2DLogic.GetInstance():SendMsgLotteryList(0)
    elseif self.isJinJiMatchOn then
        self.isRankSel = false
        self.eventBg:setVisible(false)
        self.proLayer:setVisible(false)
    end

    local name, x, y = event.name, event.x, event.y

    local logic = FishGame2DLogic.GetInstance()
    local myChairId = logic:getMyChairId()
    local player = logic:GetChairPlayer(myChairId)

    local lockFishId = player:GetLockFishID()

    -- 更新锁定鱼 --
    -- 当前没有锁定的鱼
    local lockFish
    if lockFishId ~= 0 then
        lockFish = MyObjectManager.GetInstance():FindFish(lockFishId)
    end

    local endPos = cc.p(x, y)
    if name == "began" then
        -- 玩家点下 --
        self.checkLongTouch = true;
        self.nCheckTime = 0;
        if not lockFish then
            self.pTartgetPos = endPos
        end
        self:_onFire(self.pTartgetPos)
        return true
    elseif name == "moved" then
        -- 玩家拖动手指 --
        if not lockFish then
            self.pTartgetPos = endPos
        end
    else
        -- 玩家抬起手指 --
        if not lockFish then
            self.pTartgetPos = endPos
        end
        self.checkLongTouch = false;
    end

    local myChairId = FishGame2DLogic.GetInstance():getMyChairId()
    local angle = self:updateCannonRotationToPosition(myChairId, self.pTartgetPos)
    local player = FishGame2DLogic.GetInstance():GetChairPlayer(myChairId)
    if not player then return end

    if CGameConfig.GetInstance():MirrorShow() then
        player:GetCannon():SetCannonAngle(-angle + math.pi / 2 + math.pi)
    else
        player:GetCannon():SetCannonAngle(angle + math.pi / 2)
    end

    return true
end

function FishGame2DUI:_onBtnPressed(sender, eventType)
    CSoundManager.GetInstance():PlayGameEffect(GameEffect.BUTTON_CLICK)
end

function FishGame2DUI:_onBtnClicked_quit(sender, eventType)
    app.table:sendStandup()
end

function FishGame2DUI:_onBtnClicked_soundOff(sender, eventType)
    self:SetSoundOn(true)
end

function FishGame2DUI:_onBtnClicked_soundOn(sender, eventType)
    self:SetSoundOn(false)
end

function FishGame2DUI:_onBtnClicked_info(sender, eventType)
    self:ShowHelp()
end

function FishGame2DUI:_onBtnTouched_cannonUp(sender, eventType)
    if eventType == ccui.TouchEventType.ended then
        CSoundManager.GetInstance():PlayGameEffect(GameEffect.BUTTON_CLICK)
        local myChairId = FishGame2DLogic.GetInstance():getMyChairId()
        local player = FishGame2DLogic.GetInstance():GetChairPlayer(myChairId)
        FishGame2DLogic.GetInstance():PlayerChangeCannon(sender.chairId, true)
    end
end

function FishGame2DUI:_onBtnTouched_cannonDown(sender, eventType)
    if eventType == ccui.TouchEventType.ended then
        CSoundManager.GetInstance():PlayGameEffect(GameEffect.BUTTON_CLICK)

        local myChairId = FishGame2DLogic.GetInstance():getMyChairId()
        local player = FishGame2DLogic.GetInstance():GetChairPlayer(myChairId)
        FishGame2DLogic.GetInstance():PlayerChangeCannon(sender.chairId, false)
    end
end

function FishGame2DUI:_onBtnClicked_lock(sender, eventType)
    self:SetLockFish(true)
end

function FishGame2DUI:_onBtnClicked_unlock(sender, eventType)
    self:SetLockFish(false)
end

function FishGame2DUI:_onBtnClicked_auto(sender, eventType)
    self:SetAutoAttack(true)
end

function FishGame2DUI:_onBtnClicked_handle(sender, eventType)
    self:SetAutoAttack(false)
end

function FishGame2DUI:_onBtnTouched_switchLockTarget(sender, eventType)
    if eventType == ccui.TouchEventType.began then
        CSoundManager.GetInstance():PlayGameEffect(GameEffect.BUTTON_CLICK)
    elseif eventType == ccui.TouchEventType.ended then
        FishGame2DLogic.GetInstance():SendMsgLockFish(true)
    end
end

function FishGame2DUI:_onFire(targetPos)

    local timeNow = TimeService.GetInstance():GetServerTick()--socket.gettime()
    if timeNow - self.m_LastFireTime < CGameConfig.GetInstance().nFireInterval then
        return
    end
    self.m_LastFireTime = timeNow

    local myChairId = FishGame2DLogic.GetInstance():getMyChairId()
    local angle = self:updateCannonRotationToPosition(myChairId, targetPos)
    local olld = angle
    if CGameConfig.GetInstance():MirrorShow() then
        angle = -angle + math.pi / 2 + math.pi
    else
        angle = angle + math.pi / 2
    end
    --    print("UI=>>>ONFIRE=>>", targetPos.x, targetPos.y, myChairId, angle, olld)
    if myChairId == 2 or myChairId == 3 then
        angle = math.pi - angle
    end
    FishGame2DLogic.GetInstance():playerFireTo(angle)
end

function FishGame2DUI:UpdateSoundSwitch(bSoundOn)
    self.m_bSoundOn = bSoundOn

    self.btnSoundOff:setVisible(not self.m_bSoundOn)
    self.btnSoundOn:setVisible(self.m_bSoundOn)
end

function FishGame2DUI:UpdateAutoAttak(bAuto)
    self.btnAuto:setVisible(not bAuto)
    self.btnHandle:setVisible(bAuto)

    if bAuto then
        if not self.animationAuto then
            ccs.ArmatureDataManager:getInstance():addArmatureFileInfo("fishgame2d/animation/fisn_zidong/fisn_zidong.ExportJson")
            local animationAuto = ccs.Armature:create("fisn_zidong")
            animationAuto:getAnimation():play("move")
            self.panelBtnAuto:addChild(animationAuto)
            self.animationAuto = animationAuto
        end
        self.animationAuto:setVisible(true)
    else
        if self.animationAuto then
            self.animationAuto:setVisible(false)
        end
    end
end

function FishGame2DUI:UpdateLockAttack(bLock)
    self.btnLock:setVisible(not bLock)
    self.btnUnlock:setVisible(bLock)

    if bLock then
        if not self.animationLock then
            ccs.ArmatureDataManager:getInstance():addArmatureFileInfo("fishgame2d/animation/fish_suoding/fish_suoding.ExportJson")
            local animationLock = ccs.Armature:create("fish_suoding")
            animationLock:getAnimation():play("move")
            self.panelBtnLock:addChild(animationLock)
            self.animationLock = animationLock
        end
        self.animationLock:setVisible(true)
    else
        if self.animationLock then
            self.animationLock:setVisible(false)
        end
    end
end

function FishGame2DUI:UpdatePlayerName(player)
    local chairId = player:GetChairId()
    local playerPanel = self:GetChairPanel(chairId)
    if not playerPanel then return end

    local name = player:GetNickName()
    playerPanel.labelName:setString(name)
end

--更新指定玩家的金币和宝石数量
function FishGame2DUI:UpdatePlayerScore(player)
    local chairId = player:GetChairId()
    local playerPanel = self:GetChairPanel(chairId)
    if not playerPanel then return
    end

    local score = player:GetScore()
    playerPanel.labelCoin:setString(score)
    playerPanel.labelCoin:setScale(math.min(1, 135 / playerPanel.labelCoin:getContentSize().width))
end

function FishGame2DUI:initNewBulletPosition(bullet, chairId)
    local bgCannon = self:GetChairPanel(chairId).bgCannon
    local size = bgCannon:getContentSize();

    local cp = cc.p(size.width / 2, size.height / 2);
    cp = bgCannon:convertToWorldSpace(cp); --获取到这个点转换到屏幕上的坐标
    bullet:setPosition(cp.x, cp.y);
end

--更新炮台的旋转角度，指定位置
function FishGame2DUI:updateCannonRotationToPosition(chairId, endPos)
    local cannonX, cannonY = CGameConfig.GetInstance():GetCannonPosition(chairId)
    cannonX, cannonY = CGameConfig.GetInstance():ConvertCoord(cannonX, cannonY)

    --计算两点之间的夹角
    local dx = endPos.x - cannonX
    local dy = endPos.y - cannonY;
    local dis = math.sqrt(math.pow(dx, 2) + math.pow(dy, 2)); --斜边长度
    local tan0 = dy / dx;

    local cos0 = dx / dis;
    local rad = math.acos(cos0);

    return rad
end

function FishGame2DUI:onTargetFishDied()
    self.pTarget = nil
end

function FishGame2DUI:UpdateCannon(player)
    local wChairID = player:GetChairId()

    -- 播放切换声音 --
    if wChairID == FishGame2DLogic.GetInstance():getMyChairId() then
        CSoundManager.GetInstance():PlayGameEffect(GameEffect.CANNON_SWITCH)
    end

    -- 切换图片 --
    local cannon = player:GetCannon()
    local image = cannon:GetArmatureName()
    local mul = cannon:GetBulletConfig().nMulriple
    local mulX, mulY = cannon:GetMultiplyOffset()
    local playerPanel = self:GetChairPanel(wChairID)
    local size = playerPanel.cannon:getContentSize();

    playerPanel.nodeCannon:removeAllChildren()
    ccs.ArmatureDataManager:getInstance():addArmatureFileInfo("fishgame2d/ui/" .. image .. "/" .. image .. ".ExportJson")
    
    local armature = ccs.Armature:create(image)
    armature:setTag(999)
    playerPanel.nodeCannon:addChild(armature)

    playerPanel.nodeCannon:setScale(0.1)
    playerPanel.nodeCannon:stopAllActions()
    playerPanel.nodeCannon:runAction(cc.ScaleTo:create(0.2, 1))

    -- 转换 --
    if playerPanel.bUp then
        mulY = -mulY
    end
    if CGameConfig.GetInstance():MirrorShow() then
        mulY = -mulY
    end
    
    playerPanel.labelCannon:setString(mul)

    -- 播放切换武器动画 --
    ccs.ArmatureDataManager:getInstance():addArmatureFileInfo("fishgame2d/animation/effect_weapons_replace/effect_weapons_replace.ExportJson")
    local effectWeaponReplace = ccs.Armature:create("effect_weapons_replace")
    effectWeaponReplace:getAnimation():play("effect_weapons_replace_animation")
    effectWeaponReplace:setPosition(size.width / 2, size.height / 2 + mulY);
    playerPanel.cannon:addChild(effectWeaponReplace)
    effectWeaponReplace:runAction(transition.sequence({
        cc.DelayTime:create(0.5),
        cc.CallFunc:create(function(sender)
            sender:removeSelf()
        end)
    }))
end

function FishGame2DUI:onPlayerEnter(player)
    local chairId = player:GetChairId()
    local playerPanel = self:GetChairPanel(chairId)
    if not playerPanel then return
    end

    if FishGame2DLogic.GetInstance():getMyChairId() == chairId then
        playerPanel.btnPlus:setVisible(true)
        playerPanel.btnSub:setVisible(true)

        playerPanel.btnPlus.chairId = chairId
        playerPanel.btnSub.chairId = chairId
        playerPanel.btnPlus:addTouchEventListener(handler(self, self._onBtnTouched_cannonUp))
        playerPanel.btnSub:addTouchEventListener(handler(self, self._onBtnTouched_cannonDown))

        if playerPanel.bgUserinfo:getPositionX() > 0 then
            playerPanel.bgUserinfo:setPositionX(245)
        else
            playerPanel.bgUserinfo:setPositionX(-245)
        end

        local _x, _y = CGameConfig.GetInstance():GetCannonPosition(chairId)
        _x, _y = CGameConfig.GetInstance():ConvertCoord(_x, _y)
        self.pTartgetPos = cc.p(_x, test_cy) -- 自动攻击位置
    else
        playerPanel.btnPlus:setVisible(false)
        playerPanel.btnSub:setVisible(false)
    end
    playerPanel.cannon:setVisible(true)
    playerPanel.labelCannon:setVisible(true)  
    playerPanel.bgUserinfo:setVisible(true)
    playerPanel.iconWaiting:setVisible(false)

    self:UpdatePlayerName(player)
    self:UpdatePlayerScore(player)
    self:UpdateCannon(player)
end

function FishGame2DUI:resetChairStatus(chairId)
    local playerPanel = self:GetChairPanel(chairId)

    if not playerPanel then return
    end

    playerPanel.iconWaiting:setVisible(true)

    playerPanel.cannon:setVisible(false)
    playerPanel.labelCannon:setVisible(false)  
    playerPanel.bgUserinfo:setVisible(false)
    playerPanel.btnPlus:setVisible(false)
    playerPanel.btnSub:setVisible(false)
end

function FishGame2DUI:onSysNotice(szString)
    table.insert(self._zouma, {
        showCount = 1,
        label = szString,
    })
   
    self:updateZouma()
end

function FishGame2DUI:updateZouma()
    self._panelPaomadeng:setVisible(false)
    if self._zoumaDoing then return
    end
 
    if #self._zouma <= 0 then return
    end

    if not self.bIsTimerOn then
        self._panelPaomadeng:setVisible(true)
    end

    local t_value = table.remove(self._zouma, 1)
    self._zoumaDoing = true

    local zouMaSpeed = 150
    local moveL = 640
    local zouMaL = 0

    local ChatUI = require("sdk.manager.ChatUI")
    self._zoumaLabel:removeAllChildren()
    local text, lenth = ChatUI:getRichText(t_value.label, 640) --走马长度600
    self._zoumaLabel:addChild(text)
    zouMaL = lenth + moveL
    self._zoumaLabel:setPosition(cc.p(zouMaL, 20))

    self._zoumaLabel:runAction(transition.sequence({
        cc.MoveBy:create(zouMaL / zouMaSpeed + 3, cc.p(-zouMaL * 1.5, 0)),
        cc.CallFunc:create(function()
            self._zoumaDoing = false
            self:updateZouma()
        end)
    }))
end

function FishGame2DUI:GetChairPanel(chairId)
    if CGameConfig.GetInstance():MirrorShow() then
        local _chairId = 0
        if chairId == 0 then
            _chairId = 2
        elseif chairId == 1 then
            _chairId = 3
        elseif chairId == 2 then
            _chairId = 0
        elseif chairId == 3 then
            _chairId = 1
        end

        return self.panelCannons[_chairId]
    else
        return self.panelCannons[chairId]
    end
end

function FishGame2DUI:SetSoundOn(bSoundOn)
    if bSoundOn then
        cc.UserDefault:getInstance():setFloatForKey("sound", 1)
        cc.UserDefault:getInstance():setFloatForKey("soundEffect", 1)

        CSoundManager.GetInstance():SetSound(true)
        CSoundManager.GetInstance():SetMusic(true)
    else
        cc.UserDefault:getInstance():setFloatForKey("sound", 0)
        cc.UserDefault:getInstance():setFloatForKey("soundEffect", 0)

        CSoundManager.GetInstance():SetSound(false)
        CSoundManager.GetInstance():SetMusic(false)
    end
    self:UpdateSoundSwitch(bSoundOn)
end

function FishGame2DUI:SetAutoAttack(bAuto)
    self.bAutoAttack = bAuto
    self:UpdateAutoAttak(bAuto)

    if bAuto then
        app.table:getGameServer():pauseCountDown()
    else
        app.table:getGameServer():continueCountDown()
    end
end

function FishGame2DUI:SetLockFish(bLock)
    FishGame2DLogic.GetInstance():SendMsgLockFish(bLock)
    --    self:UpdateLockAttack(bLock)
end

function FishGame2DUI:ShowDescription(szDes)
    self.labelNotice:setString(szDes)
    self.labelNotice:setOpacity(0xFF)
    self.labelNotice:setVisible(true)
    self.labelNotice:stopAllActions()
    self.labelNotice:runAction(transition.sequence({
        cc.DelayTime:create(1.0),
        cc.FadeOut:create(1),
        cc.CallFunc:create(function(sender)
            sender:setVisible(false)
        end)
    }))
end

function FishGame2DUI:ShowMyPosition()
    if self.m_shown then return
    end
    self.m_shown = true

    local chairId = FishGame2DLogic.GetInstance():getMyChairId()

    local panel = self:GetChairPanel(chairId)
    ccs.ArmatureDataManager:getInstance():addArmatureFileInfo("fishgame2d/ui/zsjian/zsjian.ExportJson")
    local zsjian = ccs.Armature:create("zsjian")
    zsjian:getAnimation():play("zsdonghua")
    zsjian:setAnchorPoint(0.4, 0)
    local x, y = panel.widgetCannon:getPosition()
    zsjian:setPosition(x, y - 10)
    zsjian:runAction(transition.sequence({
        cc.DelayTime:create(3),
        cc.FadeOut:create(0.5),
        cc.CallFunc:create(function(sender)
            sender:removeSelf()
        end),
    }))
    self:addChild(zsjian, -1)
    zsjian = nil
end

function FishGame2DUI:ShowHelp()
    local panelHelp = ccs.GUIReader:getInstance():widgetFromJsonFile("fishgame2d/ui/help/main.json")
    panelHelp:addTouchEventListener(function(sender, eventType)
        if eventType == ccui.TouchEventType.ended then
            sender:removeSelf()
        end
    end)
    self:addChild(panelHelp, 0xFFFFFF)
end

return FishGame2DUI
