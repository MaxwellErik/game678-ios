local MyComponent = import(".MyComponent")

local MyComponentEffect = class("MyComponentEffect", MyComponent)

function MyComponentEffect:ctor()
    MyComponentEffect.super.ctor(self)

    self.m_effects = {}
end

function MyComponentEffect:GetFamilyID() return MyComponentType.ECF_EFFECTMGR end

function MyComponentEffect:Add(_effect)
    table.insert(self.m_effects, _effect)
end

function MyComponentEffect:Execute(pTarget, list, bPretreating)
    local Score = 0
    local pOwner = self:GetOwner()
    for _, v in ipairs(list) do
        if pOwner == v then
            return Score, list
        end
    end

    if pOwner:GetState() < EOS_DEAD then
        table.insert(list, pOwner)

        for _, v in ipairs(self.m_effects) do
            Score, list = v:Execute(pOwner, pTarget, list, bPretreating)
        end
    end

    return Score, list
end

return MyComponentEffect