local Effect = import(".Effect")

-- 生成鱼
-- 参数１表示要生成的鱼的ＩＤ
-- 参数２表示要生成的鱼的批次
-- 参数３表示每个批次要生成的鱼的数量
-- 参数４表示每个批次之间的时间间隔
local EffectProduce = class("EffectProduce", Effect)

function EffectProduce:ctor()
    EffectProduce.super.ctor(self)

    self:SetEffectType(EffectType.ETP_PRODUCE)

    self.m_nParamCount = 4
end

function EffectProduce:Execute(pSelf, pTarget, list, bPretreating)
    if (pSelf == nil or bPretreating) then return 0, list end
    RaiseEvent("ProduceFish", self, pSelf);

    return 0, list
end

return EffectProduce