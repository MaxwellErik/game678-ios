local Effect = import(".Effect")

-- 抽奖效果展示
-- 参数１表示奖项, 0-7
-- 参数２表示实际效果 ０加金币 １加 BUFFER
-- 参数３ 在加金币时　３为０表示加固定的钱，参数４表示钱的数量
-- 					 ３为１表示加倍返钱，参数４表示钱的倍数
-- 在加ＢＵＦＦＥＲ时　３表示ＢＵＦＦＥＲ类型　４表示ＢＵＦＦＥＲ时间
local EffectAward = class("EffectAward", Effect)

function EffectAward:ctor()
    EffectAward.super.ctor(self)

    self:SetEffectType(EffectType.ETP_AWARD)
    self.m_nParamCount = 4
end

function EffectAward:Execute(pSelf, pTarget, list, bPretreating)
    if pSelf == nil then return 0, list end


    local lScore = 0;

    if (self:GetParam(1) == 0 and bPretreating) then

        if (self:GetParam(2) == 0) then
            lScore = self:GetParam(3);
        elseif (pTarget) then
            lScore = pTarget:GetScore() * self:GetParam(3);
        end
    end

    if (not bPretreating) then
        RaiseEvent("AdwardEvent", this, pSelf, pTarget);
    end
    return lScore, list;
end

return EffectAward