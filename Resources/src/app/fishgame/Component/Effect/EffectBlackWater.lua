local Effect = import(".Effect")

-- 乌贼墨汁效果
local EffectBlackWater = class("EffectBlackWater",Effect)

function EffectBlackWater:ctor()
    EffectBlackWater.super.ctor(self)

    self:SetEffectType(EffectType.ETP_BLACKWATER)

    self.m_nParamCount = 0
end

function EffectBlackWater:Execute(pSelf, pTarget, list, bPretreating)
    if (pSelf == nil or bPretreating) then return 0, list end
    RaiseEvent("BlackWater", this, pSelf);
    return 0, list
end

return EffectBlackWater