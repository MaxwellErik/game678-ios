local Effect = import(".Effect")

-- 增加ＢＵＦＦ
-- 参数１表示要增加的ＢＵＦＦ的范围， ０表示全部的鱼,１表示范围内的鱼,２表示指定类型的鱼
-- 参数１为１时表示杀死指定范围内的鱼，参数２表示半径;参数１为２时表示指定类型的鱼，参数２表示指定类型
-- 参数３表示要增加的ＢＵＦＦＥＲ类型
-- 参数４表示要增加的ＢＵＦＦＥＲ的参数
-- 参数５表示要增加的ＢＵＦＦＥＲ的时长
local EffectAddBuffer = class("EffectAddBuffer", Effect)

function EffectAddBuffer:ctor()
    EffectAddBuffer.super.ctor(self)

    self:SetEffectType(EffectType.ETP_ADDBUFFER)

    self.m_nParamCount = 5
end

function EffectAddBuffer:Execute(pSelf, pTarget, list, bPretreating)

    if (pSelf == nil or bPretreating) then return 0, list end

--    local pMgr = pSelf:GetMgr();
    local fishes =  fishgame.FishObjectManager:GetInstance():GetAllFishes()
--    for k, v in pairs(pMgr.m_fishes) do
    for k, v in ipairs(fishes) do
        if v:GetId() ~= pSelf:GetId() then

                local param_0 = self:GetParam(0)
                local param_1 = self:GetParam(1)
                local param_2 = self:GetParam(2)
                local param_3 = self:GetParam(3)
                local param_4 = self:GetParam(4)

                if (param_0 == 0) then
                    v:AddBuff(param_2, param_3, param_4)
                elseif (param_0 == 1) then
                    if (CMathAide.CalcDistance(pSelf:GetPosition().x, pSelf:GetPosition().y,
                        v:GetPosition().x, v:GetPosition().y) <= param_1) then
                        v:AddBuff(param_2, param_3, param_4)
                    end
                elseif param_0 == 2 then
                    if v:GetTypeID() == param_1 then
                        v:AddBuff(param_2, param_3, param_4)
                    end
                end
        end
    end

    return 0, list
end

return EffectAddBuffer