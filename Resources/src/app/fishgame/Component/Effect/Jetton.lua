Jetton = class("Jetton", function()
    return display.newNode()
end)

local SingleTickTime = 0.05
local StayTime = 3
local FadeOuteTime = 0.5
local JettonHeight = 5
local JettonWidth = 35
local JettonInterval = 10

Jetton.INTERVAL = 44
Jetton.COLOR = {
    RED = 0,
    GREEN = 1,
}

function Jetton:ctor(param)
    self:setCascadeOpacityEnabled(true)

    param = param or {}
    local wChairID = param.wChairID or 0
    local lScore = param.lScore or 0
    local nColor = param.nColor or 0
    local nCount = param.nCount or 0
    local bUp = param.bUp
    local onRevemoHandler = param.onRevemoHandler

    self.m_wChairID = wChairID
    self.m_nColor = nColor
    self.m_lScore = lScore
    self.m_nCount = nCount
    self.m_pJettons = {}
    self.bUp = bUp
    self.m_pRmovedHandler = onRevemoHandler

    for i = 1, nCount do
        local iconJetton = display.newSprite("jetton.png")

        if bUp then
            iconJetton:setAnchorPoint(0.5, 1)
            iconJetton:pos(0, -JettonHeight * (i - 1))
        else
            iconJetton:pos(0, JettonHeight * (i - 1))
            iconJetton:setAnchorPoint(0.5, 0)
        end

        iconJetton:setVisible(false)
        self:addChild(iconJetton)
        table.insert(self.m_pJettons,iconJetton)
    end

    local bgScoreName = nColor == Jetton.COLOR.GREEN and "jetton_bgc1.png" or "jetton_bgc2.png"
    local bgScore = display.newScale9Sprite(bgScoreName)
    bgScore:setCascadeOpacityEnabled(true)

    local labelScore = cc.ui.UILabel.newTTFLabel_({
        text = lScore,
        size = 24,
    })
    bgScore:setContentSize(labelScore:getContentSize().width + 10,labelScore:getContentSize().height)
    bgScore:setAnchorPoint(0.5,bUp and 1 or 0)
    labelScore:setAnchorPoint(0.5,0.5)
    bgScore:pos(0,bUp and -JettonHeight or JettonHeight)
    labelScore:pos(bgScore:getContentSize().width / 2,
        bgScore:getContentSize().height / 2)
    bgScore:addChild(labelScore)
    self:addChild(bgScore)

    self.bgScore = bgScore

    self:_doAction_Start()
end

function Jetton:_doAction_Start()
    local TotalTime =  math.min(SingleTickTime * #self.m_pJettons,1.5)

    local tickTime = TotalTime / #self.m_pJettons
    for k,v in ipairs(self.m_pJettons) do
        v:stopAllActions()
        v:runAction(transition.sequence({
            cc.DelayTime:create(tickTime * k),
            cc.CallFunc:create(function(sender)
                sender:setVisible(true)
            end)
        }))
    end

    self.bgScore:stopAllActions()
    self.bgScore:runAction(cc.MoveBy:create(TotalTime,cc.p(0, #self.m_pJettons * (self.bUp and -JettonHeight or JettonHeight))))

    self:stopAllActions()
    self:runAction(transition.sequence({
        cc.DelayTime:create(TotalTime),
        cc.DelayTime:create(StayTime),
        cc.FadeOut:create(FadeOuteTime),
        cc.CallFunc:create(function(sender)
            if self.m_pRmovedHandler then
                self.m_pRmovedHandler(self,self.m_wChairID)
            end
            sender:removeSelf()
        end)
    }))
end

function Jetton:GetColor()
    return self.m_nColor
end

return Jetton