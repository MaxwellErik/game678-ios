local BuffFactory = class("BuffFactory", {})

function BuffFactory:ctor()
    self.m_buffFactory = {}
end

function BuffFactory:CreateBuff(buffType)
    local _class = self.m_buffFactory[buffType]
    if _class then
        return _class.new()
    end
end

function BuffFactory:Register(buffType, _class)
    self.m_buffFactory[buffType] = _class
end

function BuffFactory:Clear()
    self.m_buffFactory = {}
end

return singleton(BuffFactory)