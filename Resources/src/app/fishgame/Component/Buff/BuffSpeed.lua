local Buff = import(".Buff")

local BuffSpeed = class("BuffSpeed", Buff)

function BuffSpeed:ctor()
    BuffSpeed.super.ctor(self)

    self:SetType(BuffType.EBT_CHANGESPEED)
end

function BuffSpeed:Clear()
    self.m_param = 1.0
end

function BuffSpeed:GetParam()
    return self.m_param
end
function BuffSpeed:OnCCEvent(pEvent)
    if (pEvent:GetID() == EME_QUERY_SPEED_MUL) then
        local pSpeed = pEvent:GetParam2()
        local callBack = pEvent:GetParam3()
        pSpeed = pSpeed * self.m_param;

        if callBack then
            callBack(pSpeed)
        end
    end
end

return BuffSpeed