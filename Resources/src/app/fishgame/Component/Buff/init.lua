BuffType = {
    EBT_NONE = 0,
    EBT_CHANGESPEED = 1, -- 改变速度
    EBT_DOUBLE_CANNON = 2, -- 双倍炮
    EBT_ION_CANNON = 3, -- 离子炮
    EBT_ADDMUL_BYHIT = 4, -- 被击吃子弹
}


local BuffFactory = import(".BuffFactory")
local BuffSpeed = import(".BuffSpeed")
local BuffDoubleCannon = import(".BuffDoubleCannon")
local BuffCIonCannon = import(".BuffCIonCannon")
local BuffAddMulByHit = import(".BuffAddMulByHit")


BuffFactory.GetInstance():Register(BuffType.EBT_CHANGESPEED, BuffSpeed)
BuffFactory.GetInstance():Register(BuffType.EBT_DOUBLE_CANNON, BuffDoubleCannon)
BuffFactory.GetInstance():Register(BuffType.EBT_ION_CANNON, BuffCIonCannon)
BuffFactory.GetInstance():Register(BuffType.EBT_ADDMUL_BYHIT, BuffAddMulByHit)

