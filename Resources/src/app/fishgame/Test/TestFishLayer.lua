local TestFishLayer = class("TestFishLayer", function()
    return display.newNode()
end)

function TestFishLayer:ctor()
    local editBoxSize = cc.size(200, 40)
    local lianxiEdit = ccui.EditBox:create(editBoxSize, "HallUI/loginUI/textbg.png")
    lianxiEdit:setPosition(test_cx, test_height * 0.15)
    lianxiEdit:setFontSize(64)
    lianxiEdit:setFontColor(cc.c3b(0xFF, 0, 0))
    lianxiEdit:setPlaceholderFontColor(cc.c3b(0xFF, 0xFF, 0xFF))
    lianxiEdit:setPlaceHolder("测试鱼类型ID")
    lianxiEdit:setReturnType(cc.KEYBOARD_RETURNTYPE_DONE)
    lianxiEdit:registerScriptEditBoxHandler(function(type, sender)
        if type == "return" then
            --            self:UpdatePathById(tonumber(sender:getText()))
            local id = tonumber(sender:getText())

            self:UpdateFishById(id)
            sender:setText("鱼类型ID：" .. sender:getText())
        end
    end)
    self:addChild(lianxiEdit, 100)

end

function TestFishLayer:UpdateFishById(nVisualID)
    if self.pFish ~= nil then
        self.pFish:Clear(true)
        self.pFish = nil
    end

    local finf = CGameConfig.GetInstance():GetFishDataById(nVisualID)
--    --
--    local pFish = Fish.new()
--    pFish:SetTypeID(finf.nTypeID)
--    pFish:SetFishType(0)
--    pFish:SetProbability(finf.fProbability)
--    pFish:SetBoundingBox(finf.nBoundBox)
--    pFish:SetLockLevel(finf.nLockLevel)
--    pFish:SetBroadCast(finf.bBroadCast)
--    pFish:SetName(finf.szName)
--    pFish:SetFishCofigData(finf)
--    pFish:GetCNode():addTo(self, ZORDER.FISH)
--    pFish:SetPosition(720, 450)
--    pFish:SetDirection(math.pi)
--    pFish:OnUpdate(0)
--    self.pFish = pFish

--        for _, v in ipairs(self._cnode.visualNodeaaaa or {}) do
--            v:removeSelf()
--        end
--        for _, v in ipairs(self._cnode.visualNode or {}) do
--            v.clear()
--        end

    if self._cnode then self._cnode:removeSelf() end

    self._cnode = display.newNode()
    self._cnode:setAnchorPoint(0.5, 0.5)
    self._cnode:pos(test_cx, test_height * 0.6)
    self._cnode:addTo(self)
--    self._cnode:setRotation(60)

    local visualId = tonumber(nVisualID)
        local visualData = CGameConfig.GetInstance():GetVisualConfigById(visualId)

        local FISH_ANIMATION_TYPE = {
            FRAME = 0,
            SKELETON = 1,
        }
        if visualData then
            for _, v in ipairs(visualData.ImageInfoDie) do
                local animation = v.Image
                local aniType = v.AniType
                local ccsFactory = GetCCSFactory();
                local ccsNode
                if aniType == FISH_ANIMATION_TYPE.FRAME then
                    ccsNode = ccsFactory.createCCSAniamtion(animation)
                elseif aniType == FISH_ANIMATION_TYPE.SKELETON then
                    ccsNode = ccsFactory.createSkeletonAnimation(animation)
                else
                    ccsNode = ccsFactory.createCCSAniamtion(animation)
                end
                ccsNode.playWithName(v.Name)
                ccsNode.setScale(v.Scale, v.Scale)
                ccsNode.setRotation(v.Direction)
                ccsNode.setPosition(v.OffestX, v.OffestY)
                self._cnode:addChild(ccsNode:getArmature())


                local labelActivity = display.newTTFLabel({
                    text = "asdfasdfsd",
                    size = 64,
                    align = cc.ui.TEXT_ALIGN_CENTER
                })
                labelActivity:pos(test_cx, test_height * 0.7)
                labelActivity:setVisible(false)

                ccsNode:getArmature():addChild(labelActivity, 1000)
            end

            local ccsNodaae = cc.DrawNode:create();
            self._cnode:addChild(ccsNodaae, 1)

            local temp = cc.DrawNode:create();
            self._cnode:addChild(temp, 2)
            temp:drawDot(cc.p(0,0), 80, cc.c4f(1, 0,0, 0.1));
            temp:drawLine(cc.p(-100,0),cc.p(100,0),cc.c4f(0, 1,0, 0.1))
            temp:drawLine(cc.p(0,-100),cc.p(0,100),cc.c4f(0, 1,0, 0.1))


            ccsNodaae:setScale(1280 / 1440)
            ccsNodaae:setScale(720 / 900)

            local config = CGameConfig.GetInstance().BBXMap[finf.nBoundBox]
            if not config then return end

            self.m_box = {}
            for _, v in ipairs(config.BBList) do
                dump(123123)
                table.insert(self.m_box, {
                    offsetX = v.nOffestX,
                    offsetY = v.nOffestY,
                    rad = v.fRadio,
                })

                for _, v in ipairs(self.m_box) do
                    ccsNodaae:drawDot(cc.p(v.offsetX, v.offsetY), v.rad, cc.c4f(1, 1, 1, 0.5));
                end

            end
        end
end

return TestFishLayer