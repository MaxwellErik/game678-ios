BgMusic = {
    BGM_NORMAL_1 = 0,
    BGM_NORMAL_2 = 1,
    BGM_NORMAL_3 = 2,
    BGM_NORMAL_4 = 3,
    BGM_COUNT = 4
};

GameEffect = {
    CANNON_SWITCH = 0,
    CASTING_NORMAL = 1,
    CASTING_ION = 2,
    CATCH = 3,
    CATCH1 = 4,
    CATCH2 = 5,
    FIRE = 6,
    IONFIRE = 7,
    INSERT = 8,
    AWARD = 9,
    BIGAWARD = 10,
    ROTARYTURN = 11,
    BINGO = 12,
    BINGO2 = 13,
    WAVE = 14,
    GAME_EFFECT_COUNT = 15,
    EFFECT_BULLET_BOMB = 19,
    BOMB_LASER = 16,
    BOMB_ELECTRIC = 17,
    BOMB_ELECTRIC = 17,
    EFFECT_PARTICAL_BIG_BOMB = 20,
    EFFECT_PARTICAL_BIG_FIREWORKS = 21,
    EFFECT_PARTICAL_FIREWORKS = 22,
    BUTTON_CLICK = 18,
};


local SoundPriority = {
    ["HIT"] = 0,
    ["Fish"] = 1,
    ["ATHER"] = 2,
    ["ATHER_II"] = 3,
    ["WAVE"] = 4,
    ["BUTTON"] = 5,
}

local SoundConfig = {
    ["Net0.mp3"] = { time = 0.1, occupationTime = 0, priority = SoundPriority.HIT },
    ["Net1.mp3"] = { time = 0.1, occupationTime = 0, priority = SoundPriority.HIT },
    ["Fire.mp3"] = { time = 0.2, occupationTime = 0.2, priority = SoundPriority.HIT },
    ["GunFire0.mp3"] = { time = 0.1, occupationTime = 0, priority = SoundPriority.ATHER },
    ["GunFire1.mp3"] = { time = 0.1, occupationTime = 0, priority = SoundPriority.ATHER },
    ["ChangeScore.mp3"] = { time = 0.2, occupationTime = 0, priority = SoundPriority.ATHER },
    ["MakeUP.mp3"] = { time = 0.5, occupationTime = 0, priority = SoundPriority.ATHER },
    ["ChangeType.mp3"] = { time = 0.1, occupationTime = 0, priority = SoundPriority.ATHER },
    ["award.mp3"] = { time = 0.8, occupationTime = 0.5, priority = SoundPriority.ATHER },
    ["bigAward.mp3"] = { time = 4, occupationTime = 4, priority = SoundPriority.ATHER_II },
    ["rotaryturn.mp3"] = { time = 0.1, occupationTime = 0, priority = SoundPriority.ATHER },
    ["CJ.mp3"] = { time = 5, occupationTime = 5, priority = SoundPriority.ATHER_II },
    ["TNNFDCLNV.mp3"] = { time = 2, occupationTime = 1, priority = SoundPriority.ATHER },
    ["surf.mp3"] = { time = 7, occupationTime = 7, priority = SoundPriority.WAVE },
    ["HaiLang.mp3"] = { time = 7, occupationTime = 7, priority = SoundPriority.WAVE },
    ["Fisha0.mp3"] = { time = 2, occupationTime = 1.8, priority = SoundPriority.Fish },
    ["Fisha1.mp3"] = { time = 2, occupationTime = 1.8, priority = SoundPriority.Fish },
    ["Fisha2.mp3"] = { time = 2, occupationTime = 1.8, priority = SoundPriority.Fish },
    ["Fisha3.mp3"] = { time = 1, occupationTime = 1, priority = SoundPriority.Fish },
    ["Fisha4.mp3"] = { time = 4, occupationTime = 3.8, priority = SoundPriority.Fish },
    ["Fisha5.mp3"] = { time = 3, occupationTime = 2.8, priority = SoundPriority.Fish },
    ["Fisha6.mp3"] = { time = 3, occupationTime = 2.8, priority = SoundPriority.Fish },
    ["Fisha7.mp3"] = { time = 2, occupationTime = 1.8, priority = SoundPriority.Fish },
    ["Fisha8.mp3"] = { time = 2, occupationTime = 1.8, priority = SoundPriority.Fish },
    ["Fisha9.mp3"] = { time = 1, occupationTime = 1, priority = SoundPriority.Fish },
    ["Fisha10.mp3"] = { time = 2, occupationTime = 1.8, priority = SoundPriority.Fish },
    ["Fisha11.mp3"] = { time = 1, occupationTime = 1, priority = SoundPriority.Fish },
    ["Fisha12.mp3"] = { time = 1, occupationTime = 1, priority = SoundPriority.Fish },
    ["Fisha13.mp3"] = { time = 2, occupationTime = 1.8, priority = SoundPriority.Fish },
    ["Fisha14.mp3"] = { time = 2, occupationTime = 1.8, priority = SoundPriority.Fish },
    ["Fisha15.mp3"] = { time = 2, occupationTime = 1.8, priority = SoundPriority.Fish },
    ["Fisha16.mp3"] = { time = 2, occupationTime = 1.8, priority = SoundPriority.Fish },
    ["Fisha17.mp3"] = { time = 1, occupationTime = 1, priority = SoundPriority.Fish },
    ["Hit0.mp3"] = { time = 0.1, occupationTime = 0, priority = SoundPriority.HIT },
    ["Hit1.mp3"] = { time = 0.1, occupationTime = 0, priority = SoundPriority.HIT },
    ["Hit2.mp3"] = { time = 1.5, occupationTime = 1, priority = SoundPriority.HIT },
    ["laser.mp3"] = { time = 0.4, occupationTime = 0, priority = SoundPriority.ATHER_II },
    ["electric.mp3"] = { time = 0.4, occupationTime = 0, priority = SoundPriority.ATHER_II },
    ["BigBang.mp3"] = { time = 1, occupationTime = 1, priority = SoundPriority.ATHER_II },
    ["Bigfireworks.mp3"] = { time = 1, occupationTime = 1, priority = SoundPriority.ATHER_II },
    ["fireworks.mp3"] = { time = 0.9, occupationTime = 0.5, priority = SoundPriority.ATHER_II },
    ["click.mp3"] = { time = 0.0, occupationTime = 0.0, priority = SoundPriority.BUTTON },
}


local CSoundManager = class("CSoundManager")

function CSoundManager:ctor()


    self.m_lastEffectType = nil
    self.m_lastFishEffectType = nil
    self.m_fishEffectTime = 0
    self.m_effectTime = 0


    self.m_soundCategory = {}

    self.m_nVolume = 0

    self.m_nCurnId = 0

    self.m_bSound = true
    self.m_bMusic = true

    self:LoadResource()
end


function CSoundManager:Init(logic)
    self.m_logic = logic

    self:LoadResource()
end

function CSoundManager:Clear()
    self.m_lastEffectType = nil
    self.m_lastFishEffectType = nil
    self.m_fishEffectTime = 0
    self.m_effectTime = 0
    self.m_logic = nil
end

function CSoundManager:LoadResource()
    self.m_bgms = {
        [BgMusic.BGM_NORMAL_1] = "bgm1.mp3",
        [BgMusic.BGM_NORMAL_2] = "bgm2.mp3",
        [BgMusic.BGM_NORMAL_3] = "bgm3.mp3",
        [BgMusic.BGM_NORMAL_4] = "bgm4.mp3",
    }
    self.m_effects = {
        [GameEffect.CANNON_SWITCH] = "MakeUP.mp3",
        [GameEffect.CASTING_NORMAL] = "Net0.mp3",
        [GameEffect.CASTING_ION] = "Net1.mp3",
        [GameEffect.CATCH] = "Hit0.mp3",
        [GameEffect.CATCH1] = "Hit1.mp3",
        [GameEffect.CATCH2] = "Hit2.mp3",
        [GameEffect.FIRE] = "", --Fire.mp3",
        [GameEffect.IONFIRE] = "GunFire1.mp3",
        [GameEffect.INSERT] = "ChangeScore.mp3",
        [GameEffect.AWARD] = "award.mp3",
        [GameEffect.BIGAWARD] = "bigAward.mp3",
        [GameEffect.ROTARYTURN] = "rotaryturn.mp3",
        [GameEffect.BINGO] = "CJ.mp3",
        [GameEffect.BINGO2] = "TNNFDCLNV.mp3",
        [GameEffect.WAVE] = "surf.mp3",
        [GameEffect.EFFECT_BULLET_BOMB] = "GunFire0.mp3",
        [GameEffect.BOMB_LASER] = "laser.mp3",
        [GameEffect.BOMB_ELECTRIC] = "electric.mp3",
        [GameEffect.BOMB_ELECTRIC] = "electric.mp3",
        [GameEffect.EFFECT_PARTICAL_BIG_BOMB] = "BigBang.mp3",
        [GameEffect.EFFECT_PARTICAL_BIG_FIREWORKS] = "Bigfireworks.mp3",
        [GameEffect.EFFECT_PARTICAL_FIREWORKS] = "fireworks.mp3",
        [GameEffect.BUTTON_CLICK] = "click.mp3",
    }

    self.m_fishSound = {
        [2] = "Fisha0.mp3",
        [3] = "Fisha1.mp3",
        [4] = "Fisha2.mp3",
        [5] = "Fisha3.mp3",
        [6] = "Fisha4.mp3",
        [9] = "Fisha5.mp3",
        [10] = "Fisha9.mp3",
        [11] = "Fisha6.mp3",
        [12] = "Fisha7.mp3",
        [13] = "Fisha8.mp3",
        [14] = "Fisha10.mp3",
        [15] = "Fisha11.mp3",
        [16] = "Fisha12.mp3",
        [17] = "Fisha13.mp3",
        [18] = "Fisha5.mp3",
        [19] = "Fisha14.mp3",
        [20] = "Fisha17.mp3",
        [21] = "Fisha15.mp3",
        [22] = "Fisha16.mp3",
        [23] = "Fisha13.mp3",
        [24] = "Fisha4.mp3",
    }

    for _, v in pairs(self.m_bgms) do
        app.musicSound:addMusic(v, "fishgame2d/sound/bgm/" .. v)
    end
    for _, v in pairs(self.m_effects) do
        app.musicSound:addSound(v, "fishgame2d/sound/effect/" .. v)
        cc.SimpleAudioEngine:getInstance():preloadEffect("fishgame2d/sound/effect/" .. v)
    end
    for _, v in pairs(self.m_fishSound) do
        app.musicSound:addSound(v, "fishgame2d/sound/effect/" .. v)
        cc.SimpleAudioEngine:getInstance():preloadEffect("fishgame2d/sound/effect/" .. v)
    end
end

function CSoundManager:PlayBackMusic(id)
    id = id or self.m_nCurnId or 0
    self.m_nCurnId = id
    local bgm = self.m_bgms[id + 1]
    if not bgm then return end
    if not self.m_bMusic then return end
    app.musicSound:playMusic(bgm, true)
end

function CSoundManager:StopBackMusic()
    app.musicSound:stopMusic()
end

function CSoundManager:PlayGameEffect(gameEffect)
    if not self.m_logic.m_resLoadFinished then return end
    if not self.m_bSound then return end

    local effect = self.m_effects[gameEffect]
    if not effect then return end
    local soundConfig = SoundConfig[effect]
    if not soundConfig then return end

    self.m_soundCategory[soundConfig.priority] = self.m_soundCategory[soundConfig.priority] or {
        time = 0,
        last_sound = nil,
    }

    local time = socket.gettime()
    local category = self.m_soundCategory[soundConfig.priority]
    if category.last_sound then
        local old = SoundConfig[category.last_sound]
        if time - category.time < old.time then
            return
        end
    end
    category.time = time
    category.last_sound = effect

    app.musicSound:playSound(effect)
end

function CSoundManager:PlayFishEffect(fishType)
    if not self.m_logic.m_resLoadFinished then return end
    if not self.m_bSound then return end

    local fushSound = self.m_fishSound[fishType]
    if not fushSound then return end
    local soundConfig = SoundConfig[fushSound]
    if not soundConfig then return end

    self.m_soundCategory[soundConfig.priority] = self.m_soundCategory[soundConfig.priority] or {
        time = 0,
        last_sound = nil,
    }

    local time = socket.gettime()
    local category = self.m_soundCategory[soundConfig.priority]
    if category.last_sound then
        local old = SoundConfig[category.last_sound]
        if time - category.time < old.time then
            return
        end
    end
    category.time = time
    category.last_sound = fushSound

    app.musicSound:playSound(fushSound)
end

function CSoundManager:MuteSwitch() end

function CSoundManager:bSound() return self.m_bSound end

function CSoundManager:bMusic() return self.m_bMusic end

function CSoundManager:SetSound(b)
    self.m_bSound = b
    if not b then
        app.musicSound:stopAllSound()
    end
end

function CSoundManager:SetMusic(b)
    self.m_bMusic = b
    if b then
        self:PlayBackMusic()
    else
        self:StopBackMusic()
    end
end

return singleton(CSoundManager)