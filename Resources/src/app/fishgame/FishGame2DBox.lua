--鱼身上的碰撞检测圆
function FishGame2DBox()
	local fbx = {};
	
	function fbx.clear()
		if FISHGAME_TEST_DRAW then
			local node = fbx.node;
			node:release();
			node:removeFromParent(true);
			fbx.node = nil;
		end
	end
	
	--初始化一个包围盒
	function fbx.init(x,y,rad)
		fbx.x = x;
		fbx.y = y;
		fbx.rad = rad;	
		if FISHGAME_TEST_DRAW then
			local node = cc.DrawNode:create();
			node:retain();
			fbx.node = node;
			fbx.draw();
		end			
	end
	--测试使用，绘制
	function fbx.draw()
		if FISHGAME_TEST_DRAW then
			local node = fbx.node;
			node:drawDot(cc.p(0,0), fbx.rad,cc.c4f(1,1,1,0.2));
		end
	end
	
	--测试使用，或者包围盒节点
	function fbx.getNode()
		return fbx.node;
	end
	
	--是否个另外一个圆相交
	function fbx.contactToOther(p,rad)
		local node = fbx.node;
		local x = node:getPositionX();
		local y = node:getPositionY();
		
		local cp = cc.p(x,y);
		cp = node:getParent():convertToWorldSpace(cp);
		local tRad = fbx.rad;
		
		local sumRad = rad + tRad;--两个圆，半径的和
		
		--求出两点之间的距离
		local dx = p.x - cp.x;
		local dy = p.y - cp.y;
		local dis = math.sqrt(math.pow(dx,2)+math.pow(dy,2));
--		Log("dis === "..dis);
		if(dis <= sumRad) then
			return true;
		end
		return false;
	end
	
	return fbx;
end