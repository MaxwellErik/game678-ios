
--捕鱼游戏的数据保存
function FishGame2DData()
	local f2d = {};
	
	--清理资源
	function f2d.clear()
		if f2d.pathData then
			for i = 1,#f2d.pathData do
				local fp = f2d.pathData[i];
				fp.clear();
				fp = nil;
			end
		end
		f2d.pathData = nil;--所有路径配置信息
		f2d.fishData = nil;--所有鱼的配置信息
		f2d.boxData = nil;--所有包围盒的信息
		f2d.bulletIndex = 0;--弹药的索引
	end

	--初始化,在一个主协程中
	function f2d.initInThread()
		f2d.clear();
		local path = GetXmlTable(FISHGAME2D_PATH_XML);
		local result = AnalysisFishGame2DPaths(path);
		f2d.pathData = result;
		local fish = GetXmlTable(FISHGAME2D_FISH_XML);
		f2d.fishData = fish;
		
		local box = GetXmlTable(FISHGAME2D_BOX_XML);
		f2d.boxData = box["BoundingBox"];
	end
	
	--获取一只鱼的数据
	function f2d.getFishData(id)
		return f2d.fishData[id];
	end
	
	--path的id从1开始
	function f2d.getPath(id)
		return f2d.pathData[id];
	end
	
	--获取包围盒的数据
	function f2d.getBoxData(id)
		return f2d.boxData[id];
	end
	
	function f2d.initBulletIndex()
		f2d.bulletIndex = f2d.bulletIndex+1;
		if f2d.bulletIndex >= 1000000000 then
			f2d.bulletIndex = 1;
		end
		return f2d.bulletIndex;
	end
	
	
	return f2d;
end

function InitFishData2D()
	local f2d = FishGame2DData();
	f2d.initInThread();
	Global["f2d"] = f2d;
end

function GetFishData2D()
	if not Global["f2d"] then
		InitFishData2D();
	end
	
	return Global["f2d"];
end

function ClearFishData2D()
	local f2d = Global["f2d"];
	if f2d then
		f2d.clear();
	end
	Global["f2d"] = nil;
end