local MyEvent = import(".MyEvent")
local EventManager = class("EventManager")

function EventManager:ctor()
    self.bEnabled_ = true
    self.mapHandler_ = {}
    self.m_listEvent = {}
end

function EventManager:RegisterEvent(_event, _handler)
    self.mapHandler_[_event] = self.mapHandler_[_event] or {}

    table.insert(self.mapHandler_[_event], _handler)
end

function EventManager:UnregisterEvent(_event, _handler)
    for k, v in ipairs(self.mapHandler_[_event]) do
        if v == _handler then
            table.remove(self.mapHandler_, k)
        end
    end
end

function EventManager:Clear()
    self.bEnabled_ = true
    self.mapHandler_ = {}
    self.m_listEvent = {}
end

function EventManager:ProcessEvent(_pEvent)
    if (not self.bEnabled_) then return end
    local _event = _pEvent:GetName()
    local _eventMap = self.mapHandler_[_event]
    if _eventMap then
        for k, v in ipairs(_eventMap) do
            v(_pEvent)
        end
    end
end

function EventManager:RaiseEvent(_eventName)
    local event = MyEvent.new(_eventName)
    self:ProcessEvent(event)
end

function EventManager:PostEvent(_pEvent)
    table.insert(self.m_listEvent, _pEvent)
end


function EventManager:Enable() end

function EventManager:OnUpdate(dt)
    local index = 1
    while index <= #self.m_listEvent do
        local pEvent = self.m_listEvent[index]
        self:ProcessEvent(pEvent)

        table.remove(index)
        pEvent = nil
    end
end

function EventManager:Enable(b)
    self.bEnabled_ = b
end

return singleton(EventManager)