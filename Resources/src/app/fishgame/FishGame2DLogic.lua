local FishGame2DLogic = class("FishGame2DLogic", import("sdk.IGame"))

function FishGame2DLogic:ctor() end

function FishGame2DLogic:init(scene)
    self._messageDeal = MessageDeal.new()
    self._messageDeal:parseConf("gameMsg/fishgame2d.json")
    app.table:registGameLogic(self)

    self._fishGame2DScene = scene

    CSoundManager.GetInstance():Init(self)
    MyObjectManager.GetInstance():Init(self)
    TimeService.GetInstance():Init(self)
    fishgame.FishObjectManager:GetInstance():RegisterBulletHitFishHandler(handler(self, self.onActionBulletHitFish))

    -- 自己最大子弹数量限制 --

    self.m_dwLastFireTick = -1
    self.m_nBulletCount = 0
    self.m_pChairs = {} -- 座位上的玩家信息
    self.m_EventRank = {} --比赛排名数据
    self.m_EventEndRank = {} -- 比赛结束排名数据

    -- 初始化用户信息 --
    self._userInfo = {}
    self._userInfo.dwUserID = app.userInfo.dwUserID

    self.m_bAllowFire = false

    self.m_bulletId = nil
end

function FishGame2DLogic:Clear()
    self:SendMsgEndGame()
    app.table:sendStandup()
    custom.PlatUtil:closeServer(CLIENT_GAME)

    app.table:registGameLogic(nil)
    self._messageDea = nil
    -- 删除消息接收 --
    CSoundManager.GetInstance():Clear(self)
    MyObjectManager.GetInstance():Clear()
    TimeService.GetInstance():Clear()

    self._userInfo = nil
    self._fishGame2DScene = nil
    self._playerInfo = {}
    self.m_bAllowFire = false
end

function FishGame2DLogic:_OnUpdate(dt, time)
    -- 到达同步时间后同步时间 --
    if time > TimeService.GetInstance():GetSyncTime() then
        TimeService.GetInstance():SetSyncTime(time)
        self:SendMsgSyncTime()
    end

    MyObjectManager.GetInstance():OnUpdate(dt)
end

-- 创建子弹
function FishGame2DLogic:CreateBullet(binf, pos, fDirection, CannonSet, CannonType, CannonMul, bForward)
    local pBullet = Bullet.new()
    pBullet:SetCannonSetType(CannonSet)
    pBullet:SetCannonType(CannonType)
    pBullet:SetCatchRadio(binf.nCatchRadio)

    -- move compent --
    local pMoveCompent = fishgame.MoveByDirection:create()
    pMoveCompent:SetSpeed(binf.nSpeed);
    pMoveCompent:SetDirection(fDirection);
    pMoveCompent:SetPosition(pos.x_, pos.y_);
    pMoveCompent:SetRebound(true);
    pBullet:SetMoveCompent(pMoveCompent);

    pMoveCompent = nil
    return pBullet;
end

function FishGame2DLogic:CreateChairBullet(wChairID, pos, dir, BulletMul, CannonType, bForward)
    local player = self:GetChairPlayer(wChairID)

    local binf = CGameConfig.GetInstance():GetBulletInfoByMulriple(BulletMul)
    local cannonSet = player:GetCannonSetType()
    local pBullet = self:CreateBullet(binf, pos, dir, cannonSet, CannonType, BulletMul, bForward)
    if pBullet then
        pBullet:SetChairID(wChairID)
    end

    return pBullet
end

--创建鱼到场景中
function FishGame2DLogic:CreateFish(finf, x, y, r, d, s, p, bTroop, ft)
    local pFish = Fish.new()
    pFish:SetVisualId(finf.nVisualID)
    pFish:SetBoundingBox(finf.nBoundBox)
    pFish:SetTypeID(finf.nTypeID)
    pFish:SetFishType(ft)
    pFish:SetFishCofigData(finf)

    if p >= 0 then
        local pMove = fishgame.MoveByPath:create()
        pMove:SetOffest(cc.p(x, y));
        pMove:SetDelay(d);
        pMove:SetPathID(p, bTroop);
        pMove:SetSpeed(s);
        pFish:SetMoveCompent(pMove);
        pMove = nil
    else
        local pMoveCompent = fishgame.MoveByDirection:create()
        pMoveCompent:SetSpeed(s);
        pMoveCompent:SetDelay(d);
        pMoveCompent:SetDirection(r);
        pMoveCompent:SetPosition(x, y);
        pMoveCompent:SetRebound(p == -1);
        pFish:SetMoveCompent(pMoveCompent);
        pMoveCompent = nil
    end
    -- TODO 添加默认的BUFF效果
    for _, v in ipairs(finf.BufferSet) do
        pFish:AddBuff(v.nTypeID, v.fParam, v.fLife)
    end
    -- TODO 添加效果
    if #finf.EffectSet > 0 then
        if ft == SpecialFishType.ESFT_KINGANDQUAN
                or ft == SpecialFishType.ESFT_KING
        then
            local pef = EffectFatory.GetInstance():CreateEffect(EffectType.ETP_KILL)
            if pef then
                pef:SetParam(0, 2)
                pef:SetParam(1, finf.nTypeID);

                local ist = CGameConfig.GetInstance().KingFishMap[finf.nTypeID]
                if ist then
                    pef:SetParam(2, ist.nMaxScore)
                end
                pFish:AddEffect(pef);
            end

            local pef = EffectFatory.GetInstance():CreateEffect(EffectType.ETP_ADDMONEY)
            if pef then
                pef:SetParam(0, 1);
                pef:SetParam(1, 10);
                pFish:AddEffect(pef)
            end
        end
        -- 配置带的效果 --
        for _, v in ipairs(finf.EffectSet) do
            local pef = EffectFatory.GetInstance():CreateEffect(v.nTypeID)
            if pef then
                local paramSize = pef:GetParamSize()
                for i = 0, paramSize - 1 do
                    local nValue = 0
                    if i < #v.nParam then
                        nValue = v.nParam[i + 1]
                    end

                    if (ft == SpecialFishType.ESFT_SANYUAN and i == 1) then
                        pef:SetParam(i, nValue * 3);
                    elseif (ft == SpecialFishType.ESFT_SIXI and i == 1) then
                        pef:SetParam(i, nValue * 4);
                    else
                        pef:SetParam(i, nValue);
                    end
                end
                pFish:AddEffect(pef);
            end
        end

        -- 特殊鱼 --
        if (ft == SpecialFishType.ESFT_KINGANDQUAN) then
            local pef = EffectFatory.GetInstance():CreateEffect(EffectType.ETP_PRODUCE)
            if (pef) then
                pef:SetParam(0, finf.nTypeID);
                pef:SetParam(1, 3);
                pef:SetParam(2, 30);
                pef:SetParam(3, 1);
                pFish:AddEffect(pef);
            end
        end
    end

    return pFish
end

function FishGame2DLogic:getMyBulletId()
    local chairId = self:getMyChairId()
    if not self.m_bulletId then
        self.m_bulletId = 1
    end

    local bulletId = chairId * 20000 + self.m_bulletId
    self.m_bulletId = self.m_bulletId + 1

    if self.m_bulletId >= 20000 then
        self.m_bulletId = 1
    end

    return bulletId
end

function FishGame2DLogic:GetChairPlayer(chairId)
    return self.m_pChairs[chairId]
end

function FishGame2DLogic:SetAllowFire(bAllowFire)
    self.m_bAllowFire = bAllowFire
end

function FishGame2DLogic:getMyChairId()
    -- dump(self._userInfo)
    -- print(" ----------..............***************&&&&&&&&&&&&&&&&&")
    -- print(self._userInfo.wChairID)
    return self._userInfo and self._userInfo.wChairID or -1
end

function FishGame2DLogic:PlayerChangeCannon(chairId, bAdd)
    self:SendMsgChangeCannon({
        wChairID = self:getMyChairId(),
        bAdd = bAdd,
    })
end

function FishGame2DLogic:playerFireTo(direction)
    -- 不允许开炮 --
    if not self.m_bAllowFire then return end

    self.m_dwLastFireTick = socket.gettime()

    local chairId = self:getMyChairId()
    local player = self:GetChairPlayer(chairId)
    if not player then return end

    -- 如果当前的金币不够，不能够发射子弹 --
    if player:GetScore() < CGameConfig.GetInstance().BulletVector[player:GetCannonMultiply() + 1].nMulriple then
        if not tolua.isnull(self._jnbibuzu) then return end
        self._jnbibuzu = BombBox.new(true, "您的金币不足，是否返回大厅进行充值？", function(event)
            app.table:sendStandup()
            -- uiManager:runScene("GameChoiceFish")
        end, function(sender)
            sender:removeSelf()
            self:GetUILayer():SetAutoAttack(false)
        end):addTo(self._fishGame2DScene, 1000)
        return
    end
    -- 如果当前自己的子弹数量大于最大子弹数量 --
    if self.m_nBulletCount > CGameConfig.GetInstance().nMaxBullet then
        self:GetUILayer():ShowDescription("炮管散热中，请等待炮管冷却后再发炮！")
        return
    end
    -- self.m_nBulletCount = self.m_nBulletCount + 1

    if chairId == 2 or chairId == 3 then
        player:GetCannon():SetCannonAngle(math.pi - direction)
    else
        player:GetCannon():SetCannonAngle(direction)
    end
    local playerPanel = self._fishGame2DScene.topui:GetChairPanel(chairId)
    playerPanel.nodeCannon:getChildByTag(999):getAnimation():play("move")--tag = 999 是炮管动画
    player:GetCannon():Fire()

    local bulletId = self:getMyBulletId()
    self:SendMsgFire(direction, bulletId)
    -- 创建子弹 --
    local x, y = CGameConfig.GetInstance():GetCannonPosition(chairId)
    local pBullet = self:CreateChairBullet(chairId,
        MyPoint.new(x, y),
        direction,
        player:GetCannonMultiply(),
        player:GetCannonType(), false)

    if pBullet then
        pBullet:SetState(EOS_LIVE)
        pBullet:SetId(bulletId);

        local nLockFishId = player:GetLockFishID()
        pBullet:SetTarget(nLockFishId)

        MyObjectManager.GetInstance():AddBullet(bulletId, pBullet)
    end
end

function FishGame2DLogic:GetFishLayer(fh)
    return self._fishGame2DScene.fishLayer
end

function FishGame2DLogic:GetUILayer(fh)
    return self._fishGame2DScene.topui
end

function FishGame2DLogic:KillFish(wChairID, pFish, BScore, FScore)
    if not pFish then return end

    local pBullet = Bullet.new()
    pBullet:SetScore(BScore);
    pBullet:SetChairID(wChairID);


    local list = {}

    local lScore, list = pFish:ExecuteEffects(pBullet, list, false);
    pFish:SetState(3)

    local finf = pFish:GetFishCofigData()

    if finf.szParticle ~= "0"
            or pFish:GetFishType() ~= SpecialFishType.ESFT_NORMAL
    then
        if finf.szParticle ~= "0" then
            app.eventDispather:dispatherEvent(FISHGAME2D_EVENT.ADD_EFFECT_PARTICAL, {
                xPos = pFish:GetPosition().x,
                yPos = pFish:GetPosition().y,
                name = finf.szParticle,
            })
        else
            app.eventDispather:dispatherEvent(FISHGAME2D_EVENT.ADD_EFFECT_PARTICAL, {
                xPos = pFish:GetPosition().x,
                yPos = pFish:GetPosition().y,
                name = "bomb",
            })
        end
    end

    local nMul = FScore / BScore
    if (finf.bShowBingo and nMul >= 80) then
        app.eventDispather:dispatherEvent(FISHGAME2D_EVENT.ADD_BINGO, {
            chair_id = wChairID,
            score = FScore,
        })
    end

    if (nMul >= 50) then
        local xPos = pFish:GetPosition().x
        local yPos = pFish:GetPosition().y
        app.eventDispather:dispatherEvent(FISHGAME2D_EVENT.ADD_EFFECT_PARTICAL, {
            xPos = xPos,
            yPos = yPos,
            name = "salute1",
        })
    end

    if (finf.bShakeScree) then
        app.eventDispather:dispatherEvent(FISHGAME2D_EVENT.ADD_EFFECT_SHAKE_SCREEN, {
            nMul = nMul,
        })
    end

    -- 播放声音 --
    if (FScore > 0) then
        CSoundManager.GetInstance():PlayFishEffect(pFish:GetTypeID())
    end
    if (wChairID == self:getMyChairId()) then
        if (nMul < 30) then
            CSoundManager.GetInstance():PlayGameEffect(GameEffect.CATCH);
        else
            CSoundManager.GetInstance():PlayGameEffect(nMul < 80 and GameEffect.CATCH1 or GameEffect.CATCH2)
        end
    end

    -- 鱼爆金币 --
    local posFish = pFish:GetPosition()
    local cannonX, cannonY = CGameConfig.GetInstance():GetCannonPosition(wChairID)
    app.eventDispather:dispatherEvent(FISHGAME2D_EVENT.ADD_FISH_GOLD, {
        fishX = posFish.x,
        fishY = posFish.y,
        cannonX = cannonX,
        cannonY = cannonY,
        score = FScore,
        baseScore = BScore,
    })

    -- 删除鱼 --
    for _, v in ipairs(list) do
        if v:GetId() ~= pFish:GetId() then
            self:KillFish(wChairID, v, BScore, 0)
        end
    end
end

function FishGame2DLogic:SendMsgInvite(gameId)
    local info = {
        inviteeid = gameId
    }
    self:SendMessage(MDM_GF_GAME, FISHGAME2D_SUB_C_REQUEST_CREATER_INVITE_MEMBER, info)
end

function FishGame2DLogic:SendMsgAgreeTeamInvite(gameId, flag)
    local info = {
        createrid = gameId,
        agree = flag
    }
    self:SendMessage(MDM_GF_GAME, FISHGAME2D_SUB_C_REQUEST_MEMBER_AGREE_CREATER, info)
end

function FishGame2DLogic:SendMsgLeaveTeam()
    local info = {
        gameid = 0
    }
    self:SendMessage(MDM_GF_GAME, FIHSGAME2D_SUB_C_REQUEST_LEAVE_TEAM, info)
end

function FishGame2DLogic:SendMessage(mainId, subId, msg)
    local msgStr = self._messageDeal:encodeMessage(mainId, subId, msg)
    app.table:sendDetailMessage(mainId, subId, msgStr)
end

function FishGame2DLogic:SendMsgSyncTime(_force)
    local info = {
        wChairID = self:getMyChairId(),
        dwClientTick = TimeService.GetInstance():GetClientTick()
    }
    self:SendMessage(MDM_GF_GAME, FISHGAME2D_SUB_C_TIME_SYNC, info)
end

function FishGame2DLogic:SendMsgChangeScore(up, All)
    --    IGameUserItem* pUser = m_pClientKernel->GetMeUserItem();

    local ccs = {}
    ccs.wChairID = self:getMyChairId()
    ccs.bAddAll = All;

    if (up) then
        --            SCORE sc = pUser->GetUserScore() - m_UserWastage[ccs.wChairID];
        ccs.bAdd = true;

    else
        ccs.bAdd = false;
    end

    --        m_dwLastCannonTick = timeGetTime();
    self:SendMessage(MDM_GF_GAME, FISHGAME2D_SUB_C_CHANGE_SCORE, ccs)
end

function FishGame2DLogic:SendMsgChangeCannon(msg)
    self:SendMessage(MDM_GF_GAME, FISHGAME2D_SUB_C_CHANAGE_CANNON, msg)
end

function FishGame2DLogic:SendMsgFire(direction, bulletId)
    local msg = {
        wChairID = self:getMyChairId(),
        fDirection = direction,
        dwFireTime = TimeService.GetInstance():GetServerTick(),
        dwClientID = bulletId,
    }
    dump(msg)
    self:SendMessage(MDM_GF_GAME, FISHGAME2D_SUB_C_FIRE, msg)
end

function FishGame2DLogic:SendMsgLockFish(bLock)
    local info = {
        wChairID = self:getMyChairId(),
        bLock = bLock
    }
    -- dump(info)
    self:SendMessage(MDM_GF_GAME, FISHGAME2D_SUB_C_LOCK_FISH, info)
end

function FishGame2DLogic:SendMsgLotteryList(s)
    local info = {
        state = s
    }
    self:SendMessage(MDM_GF_GAME, FIHSGAME2D_SUB_C_REQUEST_LOTTERY_LIST, info)
end

function FishGame2DLogic:SendMsgApplyMatch()
    local info = {
        userid =  app.userInfo.dwUserID
    }
    self:SendMessage(MDM_GF_GAME, FISHGAME2D_SUB_C_APPLY_MATCH_JINJI, info)
end

function FishGame2DLogic:SendMsgNetCast(chairId, bulletId, fishId)
    local msg = {
        dwServerID = 0,
        dwBulletID = bulletId,
        dwData = chairId,
        dwFishID = fishId,
    }
    self:SendMessage(MDM_GF_GAME, FISHGAME2D_SUB_C_NETCAST, msg)
end

function FishGame2DLogic:SendMsgChangeCannonset(playerInfo)
    -- print("--------------SendMsgChangeCannonset")
    local info = {
        wChairID = self:getMyChairId(),
        dwClientTick = TimeService.GetInstance():GetClientTick()
    }
    self:SendMessage(MDM_GF_GAME, FISHGAME2D_SUB_C_CHANGE_CANNONSET, info)
end

function FishGame2DLogic:SendMsgEndGame()
    self:SendMessage(MDM_GF_GAME, FISHGAME2D_SUB_C_ENDGAME, {})
end

function FishGame2DLogic:SendMsgBReady(playerInfo)
    self:SendMessage(MDM_GF_GAME, FISHGAME2D_SUB_C_BREADY, {})
end

function FishGame2DLogic:SendMsgTreasureEnd(playerInfo)
    local info = {
        wChairID = self:getMyChairId(),
        dwClientTick = TimeService.GetInstance():GetClientTick()
    }
    self:SendMessage(MDM_GF_GAME, FISHGAME2D_SUB_C_ENDGAME, info)
end

function FishGame2DLogic:onPlayerEnter(playerInfo)
    if playerInfo.wTableID == INVALID_TABLE then return end
    if playerInfo.wChairID == INVALID_CHAIR then return end
    if app.table._tableId ~= playerInfo.wTableID then return end
    -- dump(playerInfo)
    if self._userInfo.dwUserID == playerInfo.dwUserID then
        self._userInfo = playerInfo

        self:SendMsgSyncTime()

        if playerInfo.wChairID == 0
                or playerInfo.wChairID == 1
        then
            CGameConfig.GetInstance():SetMirror(true)
        else
            CGameConfig.GetInstance():SetMirror(false)
        end

        -- 更新老的玩家的信息 --
        for _, v in ipairs(app.table._playerList) do
            if v.wTableID == playerInfo.wTableID then
                if v.wChairID ~= playerInfo.wChairID then
                    local player = self.m_pChairs[v.wChairID]
                    if player then
                        player:SetNickName(v.nickName)
                        player:SetPlayerInfo(playerInfo)
                        player:SetUserId(v.dwUserID)
                        if v.lGrade > 0 then
                            player:SetScore(v.lGrade)
                        end
                        self._fishGame2DScene.topui:onPlayerEnter(player)
                    else
                        self:onPlayerEnter(v)
                    end
                end
            end
        end

        if CHAIR_ID.START <= playerInfo.wChairID
                and CHAIR_ID.END >= playerInfo.wChairID
        then
            if self.m_resLoadFinished then
                self._fishGame2DScene.topui:ShowMyPosition()
            end
        end
    end

    local chairId = playerInfo.wChairID
    local userId = playerInfo.dwUserID

    if CHAIR_ID.START <= chairId
            and CHAIR_ID.END >= chairId
    then
        for k, v in pairs(self.m_pChairs) do
            if v:GetUserId() == userId then
                self.m_pChairs[k] = nil
                self._fishGame2DScene.topui:resetChairStatus(k)
                break
            end
        end

        local player = Player.new(chairId, self)
        player:SetNickName(playerInfo.nickName)
        if playerInfo.UserScoreInfo.lGrade > 0 then
            player:SetScore(playerInfo.UserScoreInfo.lGrade)
        end
        player:SetPlayerInfo(playerInfo)
        player:SetUserId(userId)
        self.m_pChairs[chairId] = player

        self._fishGame2DScene.topui:onPlayerEnter(player)
    else
        for k, v in pairs(self.m_pChairs) do
            if v:GetUserId() == userId then
                self.m_pChairs[k] = nil
                self._fishGame2DScene.topui:resetChairStatus(k)
                break
            end
        end
    end
end

function FishGame2DLogic:sendRequistEvent()
    --请求比赛状态
    local info = {
        wChairID = self:getMyChairId(),
    }
    self:SendMessage(MDM_GF_GAME, FISHGAME2D_SUB_C_EVENT_STATE, info)
end

function FishGame2DLogic:onPlayerExit(dwUserID)
    if self._userInfo.dwUserID == dwUserID then
        uiManager:runScene("GameChoiceFish")
        app.musicSound:stopMusic()
    end

    for k, v in pairs(self.m_pChairs) do
        if v:GetUserId() == dwUserID then
            self.m_pChairs[k] = nil
            self._fishGame2DScene.topui:resetChairStatus(k)
            break
        end
    end
end

function FishGame2DLogic:onPlayerUpdate(player)
    if player.wTableID == INVALID_TABLE or player.wChairID == INVALID_CHAIR then

        for k, v in pairs(self.m_pChairs) do
            if v:GetUserId() == player.dwUserID then
                self.m_pChairs[k] = nil
                self._fishGame2DScene.topui:resetChairStatus(k)

                if self._userInfo.dwUserID == player.dwUserID then
                    if not tolua.isnull(self._alert) then return end
                    self._alert = BombBox.new(true, "您长时间未操作或您的网络出现异常，导致您与服务器连接断开，已将返回大厅。", function(event)
                        app.table:sendStandup()
                        -- uiManager:runScene("GameChoiceFish")
                        app.musicSound:stopMusic()
                    end, function(sender)
                        app.table:sendStandup()
                        -- uiManager:runScene("GameChoiceFish")
                        app.musicSound:stopMusic()
                    end):addTo(self._fishGame2DScene, 1000)
                    return
                end
                break
            end
        end
        return
    end

    if app.table._tableId ~= player.wTableID then
        return 
    end

    local hasPlayer = false
    for k, v in pairs(self.m_pChairs) do
        if v:GetUserId() == player.dwUserID then
            v:SetNickName(player.nickName)
            if player.lGrade and player.lGrade > 0 then
                -- dump(player)
                v:SetScore(player.lGrade)
            end
            v:SetPlayerInfo(player)
            self:GetUILayer():UpdatePlayerName(v)
            self:GetUILayer():UpdatePlayerScore(v)
            hasPlayer = true
            break
        end
    end
   
    if not hasPlayer then
        print("没有这个玩家???????????????")
        self:onPlayerEnter(player)
    else
        print("有这个玩家???????????????")
    end
end

function FishGame2DLogic:onDetailMessage(mainId, subId, msg)
    if not msg then return end

    -- print("FishGame2DLogic:onDetailMessage===>>mainId", mainId, "subId", subId)

    --游戏协议解析
    local msgDetail = self._messageDeal:decodeMessage(mainId, subId, msg)

    if not msgDetail then
        if mainId == MDM_GF_GAME then
            if subId == FISHGAME2D_SUB_S_BROADCAST_MATCH_ACCUMULATE_SYSTEM_STARTUP then
                self:onMsgLotterySystemStart()
            elseif subId == FISHGAME2D_SUB_S_BROADCAST_MATCH_ACCUMULATE_SYSTEM_OVER then
                self:onMsgLotterySystemEnd()
            elseif subId == FISHGAME2D_SUB_S_MATCH_JIN_JI_SYSTEM_STARTUP then
                self:onMsgJinJiSystemStart()
            elseif subId == FISHGAME2D_SUB_S_MATCH_JIN_JI_SYSTEM_OVER then
                self:onMsgJinJiSystemOver()
             elseif subId == FISHGAME2D_SUB_S_MATCH_JIN_JI_APPLY_FULL then
                self:onMsgJinJiApplyFull(msgDetail)
            end
        end
        return 
    end

    if mainId == MDM_GF_FRAME then
        if subId == SUB_GF_GAME_SCENE then
            dump(msgDetail)
        elseif subId == SUB_GF_GAME_STATUS then
            dump(msgDetail)
            return
        elseif subId == SUB_GF_SYSTEM_MESSAGE then
            self:OnSystemMessage(msgDetail)
            return
        end
    end

    -- 游戏命令 --
    if mainId ~= MDM_GF_GAME then
        return
    end

    if subId == FISHGAME2D_SUB_S_TIME_SYNC then
        self:onMsgTimeSync(msgDetail)
    elseif subId == FISHGAME2D_SUB_S_SEND_FISH then
        self:onMsgCreateFish(msgDetail)
    elseif subId == FISHGAME2D_SUB_S_USER_INFO then
        self:onMsgPlayerInfo(msgDetail)
    elseif subId == FISHGAME2D_SUB_S_CHANGE_SCORE then
        self:onMsgChangeScore(msgDetail)
    elseif subId == FISHGAME2D_SUB_S_CANNON_SET then
        self:onMsgCannonSet(msgDetail)
    elseif subId == FISHGAME2D_SUB_S_SEND_BULLET then
        self:onMsgCreateBullet(msgDetail)
    elseif subId == FISHGAME2D_SUB_S_KILL_FISH then
        self:onMsgKillFish(msgDetail)
    elseif subId == FISHGAME2D_SUB_S_KILL_BULLET then
        self:onMsgKillBullet(msgDetail)
    elseif subId == FISHGAME2D_SUB_S_SWITCH_SCENE then
        self:onMsgSwitchScene(msgDetail)
    elseif subId == FISHGAME2D_SUB_S_ALLOW_FIRE then
        self:onMsgAllowFire(msgDetail)
    elseif subId == FISHGAME2D_SUB_S_GAME_CONFIG then
        self:onMsgGameConfig(msgDetail)
    elseif subId == FISHGAME2D_SUB_S_LOCK_FISH then
        self:onMsgLockFish(msgDetail)
    elseif subId == FISHGAME2D_SUB_S_SEND_DES then
        self:onMsgSendDes(msgDetail)
    elseif subId == FISHGAME2D_SUB_S_FORCE_TIME_SYNC then
        self:onMsgForceTimeSync(msgDetail)
    elseif subId == FISHGAME2D_SUB_S_BULLET_SET then
        self:onMsgBulletSet(msgDetail)
    elseif subId == FISHGAME2D_SUB_S_ADD_BUFFER then
        self:onMsgAddBuffer(msgDetail)
    elseif subId == FISHGAME2D_SUB_S_FIRE_FAILE then
        self:onMsgFireFailed(msgDetail)
    elseif subId == FISHGAME2D_SUB_S_ACTIVITY_NOTIFY then
        self:onMsgActivityNotice(msgDetail)
    elseif subId == FISHGAME2D_SUB_S_FISH_MUL then
        self:onMsgFishMul(msgDetail)
    elseif subId == FISHGAME2DSUB_S_SEND_FISHS then
        self:onMsgCreateFishes(msgDetail)
    elseif subId == FISHGAME2D_SUB_S_ANDROID_UPD then
        self:onMsgAndroidUpd(msgDetail)
    --金蚌活动
    elseif subId == FISHGAME2D_SUB_S_MATCH_SYSTEM_SCORE_INFO then
        self:onMsgEventSystemInfo(msgDetail)
    elseif subId == FISHGAME2D_SUB_S_MATCH_PLAYER_SCORE_INFO then
        self:onMsgEventPlayerInfo(msgDetail)
    elseif subId == FISHGAME2D_SUB_S_BROADCAST_MATCH_AWARD_INFO then
        self:onMsgEventReward(msgDetail)
    elseif subId == FISHGAME2D_SUB_S_RESPONSE_MATCH_ACCUMULATE_BEFORE_STAT_TIME then
        self:onMsgLotteryBeforeTime(msgDetail)
    elseif subId == FISHGAME2D_SUB_S_RESPONSE_MATCH_ACCUMULATE_OPEN_STATA then
        self:onMsgLotteryState(msgDetail)
    elseif subId == FISHGAME2D_SUB_S_BROADCAST_MATCH_ACCUMULATE_MARQUEE then
        self:onSysNotice(msgDetail)
    --晋级赛
    elseif subId == FISHGAME2D_SUB_S_MATCH_JIN_JI_SYSTEM_READY then
        self:onMsgJinJiSystemReady(msgDetail)
    elseif subId == FISHGAME2D_SUB_S_MATCH_JIN_JI_APPLY_MATCH then
        self:onMsgJinJiApplyMatch(msgDetail)
    elseif subId == FISHGAME2D_SUB_S_MATCH_JIN_JI_APPLY_COUNT then
        self:onMsgJinJiApplyCount(msgDetail)
    elseif subId == FISHGAME2D_SUB_S_MATCH_JIN_JI_USER_UPGRADE_INFO then
        self:onMsgJinJiUserUpgrade(msgDetail)
    elseif subId == FISHGAME2D_SUB_S_MATCH_JIN_JI_SYSTEM_INFO then
        self:onMsgJinJiSystemInfo(msgDetail)
    elseif subId == FISHGAME2D_SUB_S_MATCH_JIN_JI_USER_MATCH_INFO then
        self:onMsgJinJiUserInfo(msgDetail)
    elseif subId == FISHGAME2D_SUB_S_MATCH_JIN_JI_USER_SCORE_INFO then
        self:onMsgJinJiUserScore(msgDetail)
    elseif subId == FISHGAME2D_SUB_S_MATCH_JIN_JI_PROMOTION_SCORE_INFO then
        self:onMsgJinJiPromotionScore(msgDetail)
    --组队
    elseif subId == FISHGAME2D_SUB_S_RESPONSE_CLIENT_CREATER_INVITE_MEMBER then
        self:onMsgTeamInvited(msgDetail)
    elseif subId == FISHGAME2D_SUB_S_RESPONSE_CLIENT_MEMBER_AGREE_CREATER then
        self:onMsgAgreeInvite(msgDetail)
    elseif subId == FISHGAME2D_SUB_S_RESPONSE_CLIENT_LEAVE_TEAM then
        self:onMsgPlayerLeaveTeam(msgDetail)
    elseif subId == FISHGAME2D_SUB_S_SEND_CLIENT_INVITE_INFO then
        self:onMsgInviteInfo(msgDetail)
    elseif subId == FISHGAME2D_SUB_S_SEND_CLIENT_DELETE_MEMBER_INFO then
        self:onMsgDeleteTeammate(msgDetail)
    elseif subId == FISHGAME2D_SUB_S_SEND_CLIENT_INSERT_MEMBER_INFO then
        self:onMsgInsertTeammate(msgDetail)
    elseif subId == FISHGAME2D_SUB_S_RESPONSE_CLIENT_TEAM_MEMBER_LIST then
        self:onMsgRefreshTeamList(msgDetail)
    end
end

function FishGame2DLogic:onTableStatus(bTableLock, bTablePlaying)
end

function FishGame2DLogic:onSysNotice(msg)
    self._fishGame2DScene.topui:onSysNotice(msg.szString)
end

function FishGame2DLogic:OnSystemMessage(msg)
    self._fishGame2DScene.topui:onSysNotice(msg.szString)
end

function FishGame2DLogic:onMsgEventSystemInfo(msgDetail)
    --彩金池数量
    app.eventDispather:dispatherEvent(FISHGAME2D_EVENT.EVENT_SYSTEM_INFO, msgDetail.llTotalPrizeScore)
end

function FishGame2DLogic:onMsgEventPlayerInfo(msgDetail)
    --玩家排名
    app.eventDispather:dispatherEvent(FISHGAME2D_EVENT.EVENT_PLAYER_INFO, msgDetail)
end

function FishGame2DLogic:onMsgLotterySystemStart()
    --彩金活动开始
    app.eventDispather:dispatherEvent(FISHGAME2D_EVENT.EVENT_LOTTERY_BEGAN, 0)
end

function FishGame2DLogic:onMsgLotterySystemEnd()
    -- 彩金活动结束
    app.eventDispather:dispatherEvent(FISHGAME2D_EVENT.EVENT_LOTTERY_END, 0)
end

function FishGame2DLogic:onMsgLotteryBeforeTime(msgDetail)
    --活动前倒计时
    app.eventDispather:dispatherEvent(FISHGAME2D_EVENT.EVENT_BEFORE_TIME, msgDetail)
end

function FishGame2DLogic:onMsgLotteryState(msgDetail)
    -- 比赛状态 通知金蚌活动状态
    app.eventDispather:dispatherEvent(FISHGAME2D_EVENT.EVENT_LOTTERY_OPEN_STATE, msgDetail)
end

function FishGame2DLogic:onMsgEventReward(msgDetail)
    -- 彩金比赛奖励
    app.eventDispather:dispatherEvent(FISHGAME2D_EVENT.EVENT_REWARD_INFO, msgDetail)
end

function FishGame2DLogic:onMsgJinJiSystemReady(msgDetail)
    --晋级赛开始前通知
    app.eventDispather:dispatherEvent(FISHGAME2D_EVENT.EVENT_JINJI_READY ,msgDetail)
end

function FishGame2DLogic:onMsgJinJiSystemStart()
    --晋级赛开始
    app.eventDispather:dispatherEvent(FISHGAME2D_EVENT.EVENT_JINJI_START, 0)
end

function FishGame2DLogic:onMsgJinJiApplyMatch(msgDetail)
    --报名
    app.eventDispather:dispatherEvent(FISHGAME2D_EVENT.EVENT_JINJI_APPLY_MATH ,msgDetail)
end

function FishGame2DLogic:onMsgJinJiApplyCount(msgDetail)
    --报名人数
    app.eventDispather:dispatherEvent(FISHGAME2D_EVENT.EVENT_JINJI_APPLY_COUNT ,msgDetail)
end

function FishGame2DLogic:onMsgJinJiApplyFull(msgDetail)
    --报名人数满，开始比赛
    app.eventDispather:dispatherEvent(FISHGAME2D_EVENT.EVENT_JINJI_APPLY_FULL ,0)
end

function FishGame2DLogic:onMsgJinJiUserScore(msgDetail)
    --玩家比赛分数
    app.eventDispather:dispatherEvent(FISHGAME2D_EVENT.EVENT_JINJI_SCORE_INFO ,msgDetail)
end

function FishGame2DLogic:onMsgJinJiPromotionScore(msgDetail)
    --晋级名单
    app.eventDispather:dispatherEvent(FISHGAME2D_EVENT.EVENT_JINJI_LIST ,msgDetail)
end

function FishGame2DLogic:onMsgJinJiUserUpgrade(msgDetail)
    --本轮是否晋级
    app.eventDispather:dispatherEvent(FISHGAME2D_EVENT.EVENT_JINJI_UPGRADE_INFO ,msgDetail)
end

function FishGame2DLogic:onMsgJinJiSystemOver()
    --晋级赛结束
    app.eventDispather:dispatherEvent(FISHGAME2D_EVENT.EVENT_JINJI_OVER ,0)
end

function FishGame2DLogic:onMsgJinJiSystemInfo(msgDetail)
    app.eventDispather:dispatherEvent(FISHGAME2D_EVENT.EVENT_JINJI_SYSTEM_INFO ,msgDetail)
end

function FishGame2DLogic:onMsgJinJiUserInfo(msgDetail)
    app.eventDispather:dispatherEvent(FISHGAME2D_EVENT.EVENT_JINJI_USER_INFO ,msgDetail)
end

function FishGame2DLogic:onMsgTeamInvited(msgDetail)
    app.eventDispather:dispatherEvent(FISHGAME2D_EVENT.EVENT_INVATE_TEAM ,msgDetail)
end

function FishGame2DLogic:onMsgAgreeInvite(msgDetail)
    app.eventDispather:dispatherEvent(FISHGAME2D_EVENT.EVENT_AGREE_INVITE ,msgDetail)
end

function FishGame2DLogic:onMsgPlayerLeaveTeam(msgDetail)
    app.eventDispather:dispatherEvent(FISHGAME2D_EVENT.EVENT_PLAYER_LEAVE_TEAM ,msgDetail)
end

function FishGame2DLogic:onMsgInviteInfo(msgDetail)
    app.eventDispather:dispatherEvent(FISHGAME2D_EVENT.EVENT_INVITE_INFO ,msgDetail)
end

function FishGame2DLogic:onMsgDeleteTeammate(msgDetail)
    app.eventDispather:dispatherEvent(FISHGAME2D_EVENT.EVENT_DELETE_TEAMMATE ,msgDetail)
end

function FishGame2DLogic:onMsgInsertTeammate(msgDetail)
    app.eventDispather:dispatherEvent(FISHGAME2D_EVENT.EVENT_INSERT_TEAMMATE ,msgDetail)
end

function FishGame2DLogic:onMsgRefreshTeamList(msgDetail)
    app.eventDispather:dispatherEvent(FISHGAME2D_EVENT.EVENT_REFRESH_TEAM_LIST ,msgDetail)
end

function FishGame2DLogic:onMsgTimeSync(msgDetail)
    if not self:getMyChairId() == msgDetail.char_id then return end
    local _timeService = TimeService.GetInstance()

    if not _timeService:BeReady() then
        local info = {}
        self:SendMsgBReady()
        self:sendRequistEvent()
    end

    _timeService:TimerSync(msgDetail.dwClientTick, msgDetail.dwServerTick);
    _timeService:SetSyncTime(socket.gettime())
end

function FishGame2DLogic:onMsgCreateFish(pss)
    local finf = CGameConfig.GetInstance():GetFishDataById(pss.nTypeID)
    if not finf then
        dump("没有找个nTypeID为" .. pss.nTypeID .. "的鱼。")
        return
    end

    local pFish = self:CreateFish(finf, pss.fOffestX, pss.fOffestY, pss.fDir, pss.fDelay, pss.FishSpeed, pss.nPathID, pss.bTroop, pss.FisType)
    if pFish then
        pFish:SetId(pss.dwFishID)
        pFish:SetCreateTick(pss.dwCreateTick)
        pFish:SetRefershID(pss.nRefershID)

        pFish:OnUpdate(TimeService.GetInstance():GetDelayTick(pss.dwCreateTick) / 1000)
        MyObjectManager.GetInstance():AddFish(pss.dwFishID, pFish)

        pFish = nil
    end

    if TimeService.GetInstance():GetDelayTick(pss.dwServerTick) > TimeService.GetInstance().MAX_DIFFRENCE then
        self:SendMsgSyncTime()
    end
end

function FishGame2DLogic:onMsgPlayerInfo(psu)
    local wChairID = psu.wChairID
    if not (CHAIR_ID.START <= wChairID and CHAIR_ID.END >= wChairID) then
        return
    end

    local player = self.m_pChairs[wChairID]

    if not player then
        player = Player.new(wChairID)
        if psu.lScore > 0 then
            player:SetScore(psu.lScore)
        end
        player:SetCannonMultiply(psu.nCannonMul)
        player:SetCannonType(psu.nCannonType)

        self.m_pChairs[wChairID] = player
        self._fishGame2DScene.topui:onPlayerEnter(player)
        return
    end
    if psu.lScore > 0 then
        player:SetScore(psu.lScore)
    end

    self:GetUILayer():UpdatePlayerScore(player)
    player:SetCannonMultiply(psu.nCannonMul)
    player:SetCannonType(psu.nCannonType)
    self:GetUILayer():UpdateCannon(player)
    --    m_UserWastage[psu->wChairID] = psu->lWastage;
end

function FishGame2DLogic:onMsgChangeScore(pss)
    local wChairID = pss.wChairID
    local player = self:GetChairPlayer(wChairID)
    if not player then return end
    --    player:setScore( pss.lWastageScore)
    dump(pss)
    player:SetScore(pss.lFishScore)
    self:GetUILayer():UpdatePlayerScore(player)

    if wChairID == self:getMyChairId()
            and pss.lFishScore > 0
    then
        CSoundManager.GetInstance():PlayGameEffect(GameEffect.INSERT);
    end
end

function FishGame2DLogic:onMsgCannonSet(pcd)
    -- TODO 怎么有8个
    local wChairID = pcd.wChairID
    local player = self:GetChairPlayer(wChairID)

    if not player then return end
    player:SetCannonSetType(app.FishRoomID)
    player:SetCannonType(pcd.cannonType)
    player:SetCannonMultiply(pcd.cannonMul)

    self:GetUILayer():UpdateCannon(player)
end

function FishGame2DLogic:onMsgCreateBullet(psb)
    -- 更新玩家积分信息 --
    local player = self:GetChairPlayer(psb.wChairID)
    if not player then
        dump("没有玩家")
        return
    end

    player:SetScore(psb.lScore)
    self:GetUILayer():UpdatePlayerScore(player)
    --    else

    -- 矫正子弹方向 --
    -- 更新当前自己最大的子弹数 --
    if self:getMyChairId() == psb.wChairID then
        if not self.m_bulletId then
            self.m_bulletId = psb.dwID - (psb.wChairID * 20000)
        end
        self.m_nBulletCount = self.m_nBulletCount + 1
        -- 播放声音 --
        CSoundManager.GetInstance():PlayGameEffect(GameEffect.FIRE)

    else
        -- 如果是新发射的子弹，调整炮台位置，并播放动作 --
        if psb.bNew then
            if psb.wChairID == 2 or psb.wChairID == 3 then
                player:GetCannon():SetCannonAngle(math.pi - psb.fDirection)
            else
                player:GetCannon():SetCannonAngle(psb.fDirection)
            end

            local playerPanel = self._fishGame2DScene.topui:GetChairPanel(psb.wChairID)
            playerPanel.nodeCannon:getChildByTag(999):getAnimation():play("move")--tag = 999 是炮管动画

            player:GetCannon():Fire()
        end

        local pBullet = self:CreateChairBullet(psb.wChairID, MyPoint.new(psb.fXpos, psb.fYPos),
            psb.fDirection, psb.nMultiply, psb.nCannonType, false)
        if pBullet then
            pBullet:SetState(EOS_LIVE)
            pBullet:SetId(psb.dwID);

            local nLockFishId = player:GetLockFishID()
            pBullet:SetTarget(nLockFishId)
    
            MyObjectManager.GetInstance():AddBullet(psb.dwID, pBullet)
        end
    end

    -- 同步时间 --
    if TimeService.GetInstance():GetDelayTick(psb.dwServerTick) > TimeService.GetInstance().MAX_DIFFRENCE then
        self:SendMsgSyncTime()
    end
end

function FishGame2DLogic:onMsgKillFish(pk)
    local wChairID = pk.wChairID
    local FScore = pk.lScore
    local BScore = pk.nBScoe
    local nMul = FScore / BScore
    local pFish = MyObjectManager.GetInstance():FindFish(pk.dwFishID)


    self:KillFish(pk.wChairID, pFish, pk.nBScoe, pk.lScore)
    -- 筹码效果 --
    app.eventDispather:dispatherEvent(FISHGAME2D_EVENT.ADD_JETTON, {
        wChairID = pk.wChairID,
        lScore = pk.lScore,
        BScore = BScore,
    })

    -- 增加积分 --
    local player = self:GetChairPlayer(pk.wChairID)
    player:SetScore(player:GetScore() + pk.lScore)
    self:GetUILayer():UpdatePlayerScore(player)
end

function FishGame2DLogic:onMsgKillBullet(msgDetail)
    local wChairID = msgDetail.wChairID
    local dwBulletID = msgDetail.dwBulletID

    -- 删除子弹 --
    if wChairID == self:getMyChairId() then
        self.m_nBulletCount = self.m_nBulletCount - 1
    end
    local bullet = MyObjectManager.GetInstance():FindBullet(dwBulletID)
    if bullet and bullet:getChairID() ==  wChairID then
        bullet:SetState(EOS_HIT)
    end
end

function FishGame2DLogic:onMsgSwitchScene(msg)
    dump(msg)
    if msg.bSwitching then
        CGameConfig.GetInstance():SetSwitchingScene(true)
        fishgame.FishObjectManager:GetInstance():SetSwitchingScene(true)
        self._fishGame2DScene:SetSwitchSceneStyle(msg.nst, function()
            CGameConfig.GetInstance():SetSwitchingScene(false)
            fishgame.FishObjectManager:GetInstance():SetSwitchingScene(false)
            MyObjectManager.GetInstance():RemoveAllFishes()
        end)
    else
        self._fishGame2DScene:SetSceneStyle(msg.nst)
    end
end

function FishGame2DLogic:onMsgAllowFire(msg)
    --    dump(msg)
    self:SetAllowFire(msg.m_bAllowFire)
end

function FishGame2DLogic:onMsgGameConfig(configData)
    -- dump(configData)
    CGameConfig.GetInstance().nChangeRatioUserScore = configData.nChangeRatioUserScore
    CGameConfig.GetInstance().nChangeRatioFishScore = configData.nChangeRatioFishScore
    CGameConfig.GetInstance().nExchangeOnce = configData.nExchangeOnce
    CGameConfig.GetInstance().nFireInterval = configData.nFireInterval
    CGameConfig.GetInstance().nMaxInterval = configData.nMaxInterval
    CGameConfig.GetInstance().nMinInterval = configData.nMinInterval
    CGameConfig.GetInstance().nShowGoldMinMul = configData.nShowGoldMinMul
    CGameConfig.GetInstance().nMaxBullet = configData.nMaxBulletCount
    CGameConfig.GetInstance().m_MaxCannon = configData.m_MaxCannon

    self:SendMsgChangeScore(true, true);
end

function FishGame2DLogic:onMsgLockFish(msgDetail)
    local player = self:GetChairPlayer(msgDetail.wChairID)
    if not player then return end

    player:SetLockFishID(msgDetail.dwLockID)
end

function FishGame2DLogic:onMsgSendDes(msg)
    local textDes = ""
    for i = 1, msg.nCount do
        local szDes = msg.szDes[i].szValue
        if i == 1 then
            textDes = szDes
        else
            textDes = textDes .. "\n" .. szDes
        end
    end
    self:GetUILayer():ShowDescription(textDes)
end

function FishGame2DLogic:onMsgForceTimeSync(msgDetail)
    self:SendMsgSyncTime(true)
end

function FishGame2DLogic:onMsgBulletSet(pbs)
    if pbs.bFirst then
        CGameConfig.GetInstance():ClearBulletSet()
    end

    local bbs = {
        nBulletSize = pbs.nBulletSize,
        nCannonType = pbs.nCannonType,
        nCatchRadio = pbs.nCatchRadio,
        nMaxCatch = pbs.nMaxCatch,
        nMulriple = pbs.nMulriple,
        nSpeed = pbs.nSpeed,
        ProbabilitySet = {}
    }

    CGameConfig.GetInstance():AddBulletSet(bbs)
    app.eventDispather:dispatherEvent(FISHGAME2D_EVENT.UPDATE_PLAYER_DATA)
end

function FishGame2DLogic:onMsgAddBuffer(pad)
    MyObjectManager.GetInstance():AddFishBUff(pad.nBufferType, pad.fBufferParam, pad.fBufferTime)
end

function FishGame2DLogic:onMsgFireFailed(msgDetail)
    self.m_nBulletCount = self.m_nBulletCount - 1
end

function FishGame2DLogic:onMsgActivityNotice(pcd)
    self:GetUILayer():ShowActivityNotice(pcd.nActivityTime,
        pcd.bStart,
        pcd.szFishName)
end

function FishGame2DLogic:onMsgFishMul(pCM)
    local pFish = MyObjectManager.GetInstance():FindFish(pCM.nFishID)
    if not pFish then return end

    pFish:SetScore(pCM.nMul)
end

function FishGame2DLogic:onMsgCreateFishes(msg)
    for i = 1, msg.dwFishCount do
        local pss = msg.Fishs[i]

        local finf = CGameConfig.GetInstance():GetFishDataById(pss.nTypeID)
        if not finf then
            dump("没有找个nTypeID为" .. pss.nTypeID .. "的鱼。")
            return
        end

        local pFish = self:CreateFish(finf, pss.fOffestX, pss.fOffestY, pss.fDir, pss.fDelay, pss.FishSpeed, pss.nPathID, pss.bTroop, pss.FisType)
        if pFish then
            pFish:SetId(pss.dwFishID)
            pFish:SetCreateTick(pss.dwCreateTick)
            pFish:SetRefershID(pss.nRefershID)

            pFish:OnUpdate(TimeService.GetInstance():GetDelayTick(pss.dwCreateTick) / 1000)
            MyObjectManager.GetInstance():AddFish(pss.dwFishID, pFish)

            pFish = nil
        end
    end

    --    if TimeService.GetInstance():GetDelayTick(pss.dwServerTick) > TimeService.GetInstance().MAX_DIFFRENCE then
    --        self:SendMsgSyncTime()
    --    end
end

function FishGame2DLogic:onMsgAndroidUpd(msg)
end

function FishGame2DLogic:onActionBulletHitFish(bullet, fish)
    self:SendMsgNetCast(bullet:getChairID(), bullet:GetId(), fish:GetId())

    if bullet:getChairID() == self:getMyChairId() then
        CSoundManager.GetInstance():PlayGameEffect(GameEffect.EFFECT_BULLET_BOMB);
    end
end

function FishGame2DLogic:_OnFinishLoadRes()
    self.m_resLoadFinished = true
    self.m_dwLastFireTick = socket.gettime()
    fishgame.FishObjectManager:GetInstance():SetGameLoaded(true)

    if self:getMyChairId() ~= -1 then
        self._fishGame2DScene.topui:ShowMyPosition()
    end
end

return singleton(FishGame2DLogic)
