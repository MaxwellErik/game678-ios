local FishGame2DLoadingLayer = class("FishGame2DLoadingLayer", function()
    return display.newLayer()
end)

local file_list = {
    "fishgame2d/animation/effect_bg_water/effect_bg_water0.png"
    ,"fishgame2d/animation/effect_bg_water/effect_bg_water1.png"
    ,"fishgame2d/animation/effect_bg_water/effect_bg_water2.png"
    ,"fishgame2d/animation/effect_fish_bomb/effect_fish_bomb0.png"
    ,"fishgame2d/animation/effect_gunsight/effect_gunsight0.png"
    ,"fishgame2d/animation/effect_transition_water/effect_transition_water0.png"
    ,"fishgame2d/animation/effect_transition_water/effect_transition_water1.png"
    ,"fishgame2d/animation/effect_weapons_replace/effect_weapons_replace0.png"
    ,"fishgame2d/animation/fish24_die/fish24_die0.png"
    ,"fishgame2d/animation/fish24_move/fish24_move0.png"
    ,"fishgame2d/animation/fish25_die/fish25_die0.png"
    ,"fishgame2d/animation/fish25_move/fish25_move0.png"
    ,"fishgame2d/animation/fish32_die_suo/fish32_die_suo0.png"
    ,"fishgame2d/animation/fish32_move/fish32_move0.png"
    ,"fishgame2d/animation/fish_1/fish_10.png"
    ,"fishgame2d/animation/fish_10/fish_100.png"
    ,"fishgame2d/animation/fish_11/fish_110.png"
    ,"fishgame2d/animation/fish_12/fish_120.png"
    ,"fishgame2d/animation/fish_13/fish_130.png"
    ,"fishgame2d/animation/fish_14/fish_140.png"
    ,"fishgame2d/animation/fish_15/fish_150.png"
    ,"fishgame2d/animation/fish_16/fish_160.png"
    ,"fishgame2d/animation/fish_17/fish_170.png"
    ,"fishgame2d/animation/fish_18/fish_180.png"
    ,"fishgame2d/animation/fish_19/fish_190.png"
    ,"fishgame2d/animation/fish_2/fish_20.png"
    ,"fishgame2d/animation/fish_21/fish_210.png"
    ,"fishgame2d/animation/fish_22/fish_220.png"
    ,"fishgame2d/animation/fish_23/fish_230.png"
    ,"fishgame2d/animation/fish_24/fish_240.png"
    ,"fishgame2d/animation/fish_24/fish_241.png"
    ,"fishgame2d/animation/fish_25/fish_250.png"
    ,"fishgame2d/animation/fish_25/fish_251.png"
    ,"fishgame2d/animation/fish_26/fish_260.png"
    ,"fishgame2d/animation/fish_28/fish_280.png"
    ,"fishgame2d/animation/fish_29/fish_290.png"
    ,"fishgame2d/animation/fish_3/fish_30.png"
    ,"fishgame2d/animation/fish_31/fish_31.png"
    ,"fishgame2d/animation/fish_32/fish_320.png"
    ,"fishgame2d/animation/fish_32/fish_321.png"
    ,"fishgame2d/animation/fish_32/fish_322.png"
    ,"fishgame2d/animation/fish_33/fish_330.png"
    ,"fishgame2d/animation/fish_34/fish_340.png"
    ,"fishgame2d/animation/fish_35/fish_350.png"
    ,"fishgame2d/animation/fish_36/fish_360.png"
    ,"fishgame2d/animation/fish_37/fish_370.png"
    ,"fishgame2d/animation/fish_38/fish_380.png"
    ,"fishgame2d/animation/fish_39/fish_390.png"
    ,"fishgame2d/animation/fish_4/fish_40.png"
    ,"fishgame2d/animation/fish_40/fish_400.png"
    ,"fishgame2d/animation/fish_41/fish_410.png"
    ,"fishgame2d/animation/fish_42/fish_420.png"
    ,"fishgame2d/animation/fish_43/fish_430.png"
    ,"fishgame2d/animation/fish_44/fish_440.png"
    ,"fishgame2d/animation/fish_45/fish_450.png"
    ,"fishgame2d/animation/fish_46/fish_460.png"
    ,"fishgame2d/animation/fish_47/fish_470.png"
    ,"fishgame2d/animation/fish_48/fish_480.png"
    ,"fishgame2d/animation/fish_49/fish_490.png"
    ,"fishgame2d/animation/fish_5/fish_50.png"
    ,"fishgame2d/animation/fish_50/fish_500.png"
    ,"fishgame2d/animation/fish_52/fish_520.png"
    ,"fishgame2d/animation/fish_55/fish_550.png"
    ,"fishgame2d/animation/fish_56/fish_560.png"
    ,"fishgame2d/animation/fish_6/fish_60.png"
    ,"fishgame2d/animation/fish_7/fish_70.png"
    ,"fishgame2d/animation/fish_8/fish_80.png"
    ,"fishgame2d/animation/fish_9/fish_90.png"
    ,"fishgame2d/animation/fish_effect_bomb_big_01/fish_effect_bomb_big_010.png"
    ,"fishgame2d/animation/fish_effect_bomb_big_01/fish_effect_bomb_big_011.png"
    ,"fishgame2d/animation/fish_effect_bomb_big_01/fish_effect_bomb_big_012.png"
    ,"fishgame2d/animation/fish_effect_bomb_big_02/fish_effect_bomb_big_020.png"
    ,"fishgame2d/animation/fish_effect_bomb_big_03/fish_effect_bomb_big_030.png"
    ,"fishgame2d/animation/fish_effect_bomb_big_04/fish_effect_bomb_big_040.png"
    ,"fishgame2d/animation/fish_effect_bomb_big_04/fish_effect_bomb_big_041.png"
    ,"fishgame2d/ui/fish_jinbi_1/fish_jinbi_10.png"
    ,"fishgame2d/caijinEvent/FishEvent/FishEvent0.png"
    ,"fishgame2d/caijinEvent/FishEvent/FishEvent1.png"
    ,"fishgame2d/caijinEvent/FishEvent/FishEvent2.png"
    ,"fishgame2d/caijinEvent/FishEvent/FishEvent3.png"
    ,"fishgame2d/caijinEvent/FishEvent/FishEvent4.png"
    ,"fishgame2d/caijinEvent/FishEvent/FishEvent5.png"
    ,"fishgame2d/caijinEvent/FishEvent/FishEvent6.png"
    ,"fishgame2d/caijinEvent/FishEvent/FishEvent7.png"
    ,"fishgame2d/caijinEvent/FishEvent/FishEvent8.png"
    ,"fishgame2d/caijinEvent/FishEvent/FishEvent9.png"
    ,"fishgame2d/caijinEvent/FishEvent/FishEvent10.png"
    ,"fishgame2d/caijinEvent/FishEvent/FishEvent11.png"
    ,"fishgame2d/caijinEvent/FishEvent/FishEvent12.png"
    ,"fishgame2d/caijinEvent/FishEvent/FishEvent13.png"
    ,"fishgame2d/caijinEvent/FishEvent/FishEvent14.png"
    ,"fishgame2d/caijinEvent/FishEvent/FishEvent15.png"
    ,"fishgame2d/caijinEvent/FishEvent/FishEvent16.png"
    ,"fishgame2d/caijinEvent/FishEvent/FishEvent17.png"
    ,"fishgame2d/caijinEvent/FishEvent/FishEvent18.png"
    ,"fishgame2d/caijinEvent/FishEvent/FishEvent19.png"
    ,"fishgame2d/caijinEvent/FishEvent/FishEvent20.png"
}

function FishGame2DLoadingLayer:ctor()
    local visibleSize = cc.Director:getInstance():getVisibleSize();
    local _width = visibleSize.width
    local _height = visibleSize.height
    local _cx =_width / 2
    local _cy =_height / 2




    self:setCascadeOpacityEnabled(true)
    self:addNodeEventListener(cc.NODE_ENTER_FRAME_EVENT, handler(self, self._onUpdate))
    -- background --
    self.background01 = display.newSprite("fishgame2d/ui/room_bg.png"):align(display.CENTER, test_cx, test_cy):addTo(self):runAction(cc.RepeatForever:create(transition.sequence({
        cc.MoveTo:create(10, cc.p(-test_cx, test_cy)),
        cc.CallFunc:create(function(sender)
            sender:pos(test_width + test_cx, test_cy)
        end),
        cc.MoveTo:create(10, cc.p(test_cx, test_cy)),
    })))
    self.background02 = display.newSprite("fishgame2d/ui/room_bg.png"):flipX(true):align(display.CENTER, test_width + test_cx, test_cy):addTo(self):runAction(cc.RepeatForever:create(transition.sequence({
        cc.MoveTo:create(20, cc.p(-test_cx, test_cy)),
        cc.CallFunc:create(function(sender)
            sender:pos(test_width + test_cx, test_cy)
        end)
    })))
    -- self.backgroundMask = display.newSprite("fishgame2d/ui/fish_loading_enter_bg_black.png"):align(display.CENTER, test_cx, test_cy):addTo(self):scale(2.5)
    -- loading bar --
    self.widget = ccs.GUIReader:getInstance():widgetFromJsonFile("fishgame2d/ui/fishGame2D_5.json"):addTo(self)
    self.widget:setCascadeOpacityEnabled(true)
    local loadingBar = ccui.Helper:seekWidgetByName(self.widget, "ProgressBar_1")
    loadingBar:setCascadeOpacityEnabled(true)
    loadingBar:setPercent(0)
    self.loadingBar = loadingBar

    --animation --
    local path = "fish_loading_enter"
    ccs.ArmatureDataManager:getInstance():addArmatureFileInfo("fishgame2d/ui/" .. path .. "/" .. path .. ".ExportJson")
    local layerBackground = display.newLayer():addTo(self)
    layerBackground:setCascadeOpacityEnabled(true)
    self.ani01 = ccs.Armature:create(path)
    self.ani01:align(display.CENTER, _cx,_cy)
    self.ani01:addTo(layerBackground)
    self.ani01:getAnimation():play("ani_01")
    self.ani03 = ccs.Armature:create(path)
    self.ani03:align(display.CENTER, _cx,_cy)
    self.ani03:addTo(layerBackground)
    self.ani03:getAnimation():play("ani_03")


    local x, y = loadingBar:getPosition()
    self.startX = x - loadingBar:getContentSize().width / 2
    self.ani03:setPosition(self.startX, y + 50)
end

function FishGame2DLoadingLayer:_onUpdate(dt)
    if self.fishResList then
        if #self.fishResList == 0 then
            self.fishResList = nil
            self:FinishLoad()
            return
        end
        if not self.currentLoad then
            self.currentLoad = table.remove(self.fishResList, 1)
            display.addImageAsync(self.currentLoad, function()
                self.currentLoad = nil
            end)
        end
        local rPercent = (self.mnCount - #self.fishResList) / self.mnCount

        self.loadingBar:setPercent(rPercent * 100)
        self.ani03:setPositionX(self.startX + self.loadingBar:getContentSize().width * rPercent)
    end
end

function FishGame2DLoadingLayer:StartLoad(_fishHandler)
    self._fishHandler = _fishHandler

    self.fishResList = clone(file_list)
    self.mnCount = #file_list

    self:scheduleUpdate()
end

function FishGame2DLoadingLayer:FinishLoad()
    self:unscheduleUpdate()
    self.loadingBar:runAction(cc.FadeOut:create(0.5))
    self:runAction(transition.sequence({
        cc.FadeOut:create(0.5),
        cc.CallFunc:create(function(sender)
            sender:removeSelf()
        end)
    }))

    if self._fishHandler then
        self._fishHandler()
    end
end

return FishGame2DLoadingLayer