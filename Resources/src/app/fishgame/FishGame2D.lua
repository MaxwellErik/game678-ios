local CGameConfig = import(".Manager.CGameConfig")
local FishGame2DLoadingLayer = import(".FishGame2DLoadingLayer")
local FishGame2DLogic = import(".FishGame2DLogic")
local FishGame2DUI = import(".FishGame2DUI")

local FishGame2D = class("FishGame2D", function()
    local scene = fishgame.MyScene:create()
    scene:setNodeEventEnabled(true)
    scene:setAutoCleanupEnabled()
    return scene
end)

local ChainConfig = {
    [E_Red] = { image = "fish_42", animation = "move" },
    [E_Blue] = { image = "fish_41", animation = "move" },
    [E_Light] = { image = "fish_40", animation = "move" },
}

local ParticalConfig = {
    ["salute1"] = { AniImage = "fish_effect_bomb_big_01", AniName = "bomb_01" },
    ["bubble"] = { AniImage = "fish_effect_bomb_big_02", AniName = "Animation1" },
    ["switch"] = { AniImage = "fish_effect_bomb_big_03", AniName = "Animation1" },
    ["bomb"] = { AniImage = "fish_effect_bomb_big_04", AniName = "Animation1" },
}

local BackgroundImage = {
    "fishgame2d/scene/Map_1.jpg",
    "fishgame2d/scene/Map_0.jpg",
    "fishgame2d/scene/Map_2.jpg",
    "fishgame2d/scene/Map_3.jpg",
}

local LAYER_ZORDER = {
    NONE = -2, -- 背景
    STATIC_BACKGROUND = -1, -- 背景
    OBJECT_EFFECT_DOWN = 0, -- 效果
    OBJECT_FISH = 1, -- 鱼
    OBJECT_FISH_TEST = 2, -- 鱼
    STATIC_WATER = 3, -- 水纹
    STATIC_SWITCH_BACKGROUND = 4, -- 切换场景背景
    STATIC_SWITCH_WATER = 5, -- 切换场景水纹
    STATIC_SWITCH_WAVE = 6, -- 切换场景波浪
    OBJECT_BULLET = 7, -- 子弹
    OBJECT_BULLET_DIE = 8, --子弹死亡效果;
    OBJECT_EFFECT_UP = 9, -- 效果
    TOP_UI = 10, -- UI
    BOUNDING_BOX = 11, -- 绑定盒
    LAYER_LOADING = 12, -- 资源加载层
    LAYER_TEST = 13, -- 测试
}

function FishGame2D:ctor()
    self:addNodeEventListener(cc.NODE_ENTER_FRAME_EVENT, handler(self, self._onInterval))

    self.m_pChains = {}
    self.m_pParticals = {}
    -- 筹码效果 --
    self.m_pJetton = {
        [0] = {},
        [1] = {},
        [2] = {},
        [3] = {},
    }

    -- 鱼的锁定效果 --
    self.m_pLockFishBubbles = {}

    -- 优化缓存列表 --
    -- 鱼摆摆 --
    self.m_FishLayerList = {}
    self.m_pFishLayer = display.newLayer():addTo(self, LAYER_ZORDER.OBJECT_FISH)
    -- 子弹 --
    self.m_BulletLayerList = {}
    self.m_pBulletLayer = display.newLayer():addTo(self, LAYER_ZORDER.OBJECT_BULLET)
    -- 效果 --
    self.m_EffectDownLayerList = {}
    self.m_EffectUpLayerList = {}
    self.m_pEffectDownLayer = display.newLayer():addTo(self, LAYER_ZORDER.OBJECT_EFFECT_DOWN)
    self.m_pEffectUpLayer = display.newLayer():addTo(self, LAYER_ZORDER.OBJECT_EFFECT_UP)

    -- UI层 --
    self.topui = FishGame2DUI.new():addTo(self, LAYER_ZORDER.TOP_UI)

    -- Loading 层 --
    FishGame2DLoadingLayer.new():addTo(self, LAYER_ZORDER.LAYER_LOADING):StartLoad(handler(self, self.StartGame))

    -- 设置默认场景 --
    --    self:SetSceneStyle(1)

    -- TEST FISH LAYER --
    -- local TestFishLayer = require("app.fishgame.Test.TestFishLayer")
    -- self:addChild(TestFishLayer.new(), 100)
end

function FishGame2D:onEnter()
    app.eventDispather:addListenerEvent(eventGameNetClosed, self, handler(self, self._OnEvent_GameNetClosed))
    app.eventDispather:addListenerEvent(eventGameNoPlay, self, handler(self, self._OnEvent_GameNoPlay))

    app.eventDispather:addListenerEvent(FISHGAME2D_EVENT.ADD_CHAIN, self, handler(self, self._OnEvent_AddChain))
    app.eventDispather:addListenerEvent(FISHGAME2D_EVENT.ADD_BINGO, self, handler(self, self._OnEvent_AddBingo))
    app.eventDispather:addListenerEvent(FISHGAME2D_EVENT.ADD_FISH_GOLD, self, handler(self, self._OnEvent_AddFishGold))
    app.eventDispather:addListenerEvent(FISHGAME2D_EVENT.ADD_JETTON, self, handler(self, self._OnEvent_AddJetton))
    app.eventDispather:addListenerEvent(FISHGAME2D_EVENT.ADD_EFFECT_PARTICAL, self, handler(self, self._OnEvent_AddEffectPartical))
    app.eventDispather:addListenerEvent(FISHGAME2D_EVENT.ADD_EFFECT_SHAKE_SCREEN, self, handler(self, self._OnEvent_AddEffectShakeScreen))

    -- 彩金活动
    app.eventDispather:addListenerEvent(FISHGAME2D_EVENT.EVENT_SYSTEM_INFO, self, handler(self, self._OnEvent_Event_System_Info))
    app.eventDispather:addListenerEvent(FISHGAME2D_EVENT.EVENT_PLAYER_INFO, self, handler(self, self._OnEvent_Event_Player_Info))  
    app.eventDispather:addListenerEvent(FISHGAME2D_EVENT.EVENT_REWARD_INFO, self, handler(self, self._OnEvent_Reward_info))
    app.eventDispather:addListenerEvent(FISHGAME2D_EVENT.EVENT_LOTTERY_BEGAN, self, handler(self, self._OnEvent_Lottery_began))
    app.eventDispather:addListenerEvent(FISHGAME2D_EVENT.EVENT_LOTTERY_END, self, handler(self, self._OnEvent_Lottery_end))
    app.eventDispather:addListenerEvent(FISHGAME2D_EVENT.EVENT_LOTTERY_OPEN_STATE, self, handler(self, self._OnEvent_Open_state))
    app.eventDispather:addListenerEvent(FISHGAME2D_EVENT.EVENT_BEFORE_TIME, self, handler(self, self._OnEvent_Before_time))
    --晋级赛
    app.eventDispather:addListenerEvent(FISHGAME2D_EVENT.EVENT_JINJI_READY, self, handler(self, self._OnEvent_Jinji_Ready))
    app.eventDispather:addListenerEvent(FISHGAME2D_EVENT.EVENT_JINJI_START, self, handler(self, self._OnEvent_Jinji_Start))
    app.eventDispather:addListenerEvent(FISHGAME2D_EVENT.EVENT_JINJI_APPLY_MATH, self, handler(self, self._OnEvent_Jinji_Apply_Match))
    app.eventDispather:addListenerEvent(FISHGAME2D_EVENT.EVENT_JINJI_APPLY_COUNT, self, handler(self, self._OnEvent_Jinji_Apply_Count))
    app.eventDispather:addListenerEvent(FISHGAME2D_EVENT.EVENT_JINJI_APPLY_FULL, self, handler(self, self._OnEvent_Jinji_Apply_Full))
    app.eventDispather:addListenerEvent(FISHGAME2D_EVENT.EVENT_JINJI_UPGRADE_INFO, self, handler(self, self._OnEvent_Jinji_Upgrade_info))
    app.eventDispather:addListenerEvent(FISHGAME2D_EVENT.EVENT_JINJI_OVER, self, handler(self, self._OnEvent_Jinji_over))
    app.eventDispather:addListenerEvent(FISHGAME2D_EVENT.EVENT_JINJI_SCORE_INFO, self, handler(self, self._OnEvent_Jinji_Score_info))
    app.eventDispather:addListenerEvent(FISHGAME2D_EVENT.EVENT_JINJI_LIST, self, handler(self, self._OnEvent_Jinji_List))
    app.eventDispather:addListenerEvent(FISHGAME2D_EVENT.EVENT_JINJI_SYSTEM_INFO, self, handler(self, self._OnEvent_Jinji_system_info))
    app.eventDispather:addListenerEvent(FISHGAME2D_EVENT.EVENT_JINJI_USER_INFO, self, handler(self, self._OnEvent_Jinji_user_info))
    --组队
    app.eventDispather:addListenerEvent(FISHGAME2D_EVENT.EVENT_INVATE_TEAM, self, handler(self, self._OnEvent_Invate_team))
    app.eventDispather:addListenerEvent(FISHGAME2D_EVENT.EVENT_AGREE_INVITE, self, handler(self, self._OnEvent_Agree_Invite))
    app.eventDispather:addListenerEvent(FISHGAME2D_EVENT.EVENT_PLAYER_LEAVE_TEAM, self, handler(self, self._OnEvent_leave_team))
    app.eventDispather:addListenerEvent(FISHGAME2D_EVENT.EVENT_INVITE_INFO, self, handler(self, self._OnEvent_invite_info))
    app.eventDispather:addListenerEvent(FISHGAME2D_EVENT.EVENT_DELETE_TEAMMATE, self, handler(self, self._OnEvent_Delete_teammate))
    app.eventDispather:addListenerEvent(FISHGAME2D_EVENT.EVENT_INSERT_TEAMMATE, self, handler(self, self._OnEvent_Insert_teammate))
    app.eventDispather:addListenerEvent(FISHGAME2D_EVENT.EVENT_REFRESH_TEAM_LIST, self, handler(self, self._OnEvent_Refresh_team_list))

    app.eventDispather:addListenerEvent(FISHGAME2D_EVENT.UPDATE_PLAYER_DATA, self, handler(self, self._OnEvent_Update_Player_Data))
    
    self:scheduleUpdate()

    FishGame2DLogic.GetInstance():init(self)
    cc.Director:getInstance():setAnimationInterval(1 / 30.0)
end

function FishGame2D:onExit()
    app.eventDispather:delListenerEvent(self)

    FishGame2DLogic.GetInstance():Clear()
    cc.Director:getInstance():setAnimationInterval(1 / 60.0)

    collectgarbage("collect")
end

function FishGame2D:_onInterval(_dt)
    local timeNow = socket.gettime()
    self.m_lastTick = self.m_lastTick or timeNow
    local dt = timeNow - self.m_lastTick
    self.m_lastTick = timeNow

    FishGame2DLogic.GetInstance():_OnUpdate(dt, timeNow)

    -- 增加链子效果 --
    local index = 1
    local count = 0
    while index <= #self.m_pChains do
        if count <= 5 then
            count = count + 1
            local chainInfo = self.m_pChains[index]
            self:ShowChain(chainInfo)
            table.remove(self.m_pChains, index)
        else
            index = index + 1
        end
    end

    -- 鱼的死亡光效 --
    local index, count = 1, 0
    while index <= #self.m_pParticals do
        if count <= 3 then
            count = count + 1
            local partical = self.m_pParticals[index]
            self:ShowEffectPartical(partical.fishX, partical.fishY, partical.name)

            table.remove(self.m_pParticals, index)
        else
            index = index + 1
        end
    end

    -- 处理震动屏幕效果 --
    if FishGame2DLogic.GetInstance().m_resLoadFinished
            and self.m_bShake
    then
        local base = math.max(1, 30 * self.m_bShake.range * self.m_bShake.time)
        local base_2 = base / 2
        self.m_bShake.time = self.m_bShake.time - dt
        if self.m_bShake.time <= 0 then
            self:setPosition(0, 0)
            self.m_bShake = nil
        else
            self:setPosition(math.random() * base - base_2,
                math.random() * base - base_2)
        end
    end
end

function FishGame2D:_onJettonRemoved(sender, wChairID)
    local pJetton = self.m_pJetton[wChairID]
    if not pJetton then return end

    local index = 1
    while index <= #pJetton do
        local v = pJetton[index]
        if v == sender then
            table.remove(pJetton, index)
        else
            v:runAction(cc.MoveBy:create(0.5, cc.p(Jetton.INTERVAL, 0)))
            index = index + 1
        end
    end
end

function FishGame2D:AddEffectDown(effect, key)
    if not self.m_EffectDownLayerList[key] then
        self.m_EffectDownLayerList[key] = display.newLayer():addTo(self.m_pEffectDownLayer)
    end
    self.m_EffectDownLayerList[key]:addChild(effect)
end

function FishGame2D:AddEffectUp(effect, key)

    if not self.m_EffectUpLayerList[key] then
        self.m_EffectUpLayerList[key] = display.newLayer():addTo(self.m_pEffectUpLayer)
    end
    self.m_EffectUpLayerList[key]:addChild(effect)
end

function FishGame2D:_OnEvent_GameNetClosed()
    BombBox.new(true, STR_HCPY_NET_CLOSE, function(event)
        -- app.table:sendStandup()
        uiManager:runScene("GameChoiceFish")
        app.musicSound:stopMusic()
    end, function(even)
        -- app.table:sendStandup()
        uiManager:runScene("GameChoiceFish")
        app.musicSound:stopMusic()
    end):addTo(self, 1000)
end

function FishGame2D:_OnEvent_GameNoPlay()
    BombBox.new(true, STR_NO_PLAY_NET_CLOSE, function(event)
        app.table:sendStandup()
        -- uiManager:runScene("GameChoiceFish")
        app.musicSound:stopMusic()
    end, function(even)
        app.table:sendStandup()
        -- uiManager:runScene("GameChoiceFish")
        app.musicSound:stopMusic()
    end):addTo(self, 1000)
end

function FishGame2D:_OnEvent_AddChain(_chain)
    if not _chain then return end
    if #self.m_pChains > 25 then return end


    local startX, startY = _chain.start_x, _chain.start_y
    local endX, endY = _chain.end_x, _chain.end_y
    startX, startY = CGameConfig.GetInstance():ConvertCoord(startX, startY)
    endX, endY = CGameConfig.GetInstance():ConvertCoord(endX, endY)

    table.insert(self.m_pChains, {
        startX = startX,
        startY = startY,
        endX = endX,
        endY = endY,
        lightType = _chain.type,
    })
end

function FishGame2D:_OnEvent_AddBingo(_bingo)
    self:ShowBingo(_bingo)
end

function FishGame2D:_OnEvent_AddFishGold(_fishGold)
    local fishX, fishY = _fishGold.fishX, _fishGold.fishY
    local cannonX, cannonY = _fishGold.cannonX, _fishGold.cannonY
    fishX, fishY = CGameConfig.GetInstance():ConvertCoord(fishX, fishY)
    cannonX, cannonY = CGameConfig.GetInstance():ConvertCoord(cannonX, cannonY)
    self:ShowFishGold(fishX, fishY, cannonX, cannonY, _fishGold.score, _fishGold.baseScore)
end

function FishGame2D:_OnEvent_AddJetton(_jetton)
    --    self:ShowEffectJetton(_jetton.wChairID, _jetton.lScore, _jetton.BScore)
end

function FishGame2D:_OnEvent_ShowLockFish(_lockFish)
    local chairId = _lockFish.chairId
    local fishId = _lockFish.fishId

    if self.m_pLockFishBubbles[chairId] ~= nil
            or fishId == 0
    then
        self:CleanLockFish()
        return
    end

    self:ShowLockFish(chairId, fishId)
end

function FishGame2D:_OnEvent_AddEffectPartical(_info)
    if not _info then return end
    if #self.m_pParticals > 10 then return end


    local fishX, fishY = _info.xPos or 0, _info.yPos or 0
    fishX, fishY = CGameConfig.GetInstance():ConvertCoord(fishX, fishY)

    table.insert(self.m_pParticals, {
        fishX = fishX,
        fishY = fishY,
        name = _info.name,
    })
end


function FishGame2D:_OnEvent_AddEffectShakeScreen(_info)
    local nMul = _info.nMul
    local time = math.max(0.5, math.min(nMul / 60, 1.5))
    local range = math.min(nMul / 50, 2)

    self.m_bShake = {
        time = time,
        range = range
    }
end

function FishGame2D:_OnEvent_Event_System_Info(score)
    self.topui:onEventSystemInfo(score)
end

function  FishGame2D:_OnEvent_Event_Player_Info(rankData)
    local myRank = rankData.llRank
    local myScore = rankData.llScore
    local listcount = rankData.llistCount

    self.topui:refreshMyRank(myRank, myScore)
    self.topui:refreshRankData(listcount, rankData.listScoreInfo)
end

function FishGame2D:_OnEvent_Lottery_began(info)
    self.topui:onLotteryBegan()
end

function FishGame2D:_OnEvent_Lottery_end(info)
    self.topui:onLotteryEnd()
end

function FishGame2D:_OnEvent_Before_time(info)
    self.topui:onEventBeforeTime(info.beforetime)
end

function FishGame2D:_OnEvent_Open_state(info)
    -- dump(info)
    self.topui:refreshLotteryState(info.openstate)
end

function FishGame2D:_OnEvent_Reward_info(info)
    self.topui:onLotteryReward(info)
end

function  FishGame2D:_OnEvent_Update_Player_Data(data)
    self.topui:onEventUpdatePlayerData()
end

function FishGame2D:_OnEvent_Jinji_Ready(info)
    self.topui:onSysNotice("公告："..info.timeRemain.."分钟后，捕鱼晋级赛活动将开启，捕鱼能手们做好准备哦！") 
end

function FishGame2D:_OnEvent_Jinji_Start(info)
    --比赛开始
    self.topui:JinjiMatchStart()
end

function FishGame2D:_OnEvent_Jinji_Apply_Match(info)
    self.topui:showMyApplyResult(info.applyResult)
end

function FishGame2D:_OnEvent_Jinji_Apply_Count(info)
    self.topui:showApplyCount(info.applyCount)
end

function FishGame2D:_OnEvent_Jinji_Apply_Full(info)
    self.topui:showApplyFull()
end

function FishGame2D:_OnEvent_Jinji_Score_info(info)
    self.topui:refreshMyPromotionScore(info.score)
end

function FishGame2D:_OnEvent_Jinji_List(info)
    self.topui:refreshPromoteList(info)
end

function FishGame2D:_OnEvent_Jinji_Upgrade_info(info)
    self.topui:showMatchResult(info)
end

function FishGame2D:_OnEvent_Jinji_over(info)
    --比赛结束 停止报名
    self.topui:onJinJiMatchOver()
end

function FishGame2D:_OnEvent_Jinji_system_info(info)
    self.topui:onJinjiMatchSystemInfo(info)
end

function FishGame2D:_OnEvent_Jinji_user_info(info)
    self.topui:onJinjiMatchUserInfo(info)
end

function FishGame2D:_OnEvent_Invate_team(info)
    self.topui:onEventInvited(info)
end

function FishGame2D:_OnEvent_Agree_Invite(info)
    self.topui:onEventAgreeInvite(info)
end

function FishGame2D:_OnEvent_leave_team(info)
    self.topui:onEventLeaveTeam(info)
end

function FishGame2D:_OnEvent_invite_info(info)
    self.topui:onEventInviteInfo(info)
end

function FishGame2D:_OnEvent_Delete_teammate(info)
    self.topui:onEventDeleteTeammate(info)
end

function FishGame2D:_OnEvent_Insert_teammate(info)
    self.topui:onEventInsertTeammate(info)
end

function FishGame2D:_OnEvent_Refresh_team_list(info)
    self.topui:onRefreshTeamList(info)
end

function FishGame2D:ShowChain(_chain)
    local distance = CMathAide.CalcDistance(_chain.startX, _chain.startY, _chain.endX, _chain.endY)
    local angle = CMathAide.CalcAngle(_chain.startX, _chain.startY, _chain.endX, _chain.endY)
    local config = ChainConfig[_chain.lightType]

    ccs.ArmatureDataManager:getInstance():addArmatureFileInfo("fishgame2d/animation/" .. config.image .. "/" .. config.image .. ".ExportJson")
    local animaNode = ccs.Armature:create(config.image)
    animaNode:getAnimation():play(config.animation)
    animaNode:setAnchorPoint(0.5, 0.5)
    animaNode:setScaleX(distance / animaNode:getContentSize().width)
    animaNode:pos((_chain.startX + _chain.endX) / 2, (_chain.startY + _chain.endY) / 2)
    animaNode:setRotation(-math.deg(angle) + 90)

    animaNode:runAction(transition.sequence({
        cc.DelayTime:create(0.5),
        cc.CallFunc:create(function(sender)
            sender:removeSelf()
        end)
    }))
    self:AddEffectDown(animaNode, config.image)

    if _chain.lightType == E_Light then
        CSoundManager.GetInstance():PlayGameEffect(GameEffect.BOMB_ELECTRIC);
    else
        CSoundManager.GetInstance():PlayGameEffect(GameEffect.BOMB_LASER);
    end
end

function FishGame2D:ShowBingo(_bingo)
    local chairId = _bingo.chair_id
    local score = _bingo.score

    local node = display.newNode()
    node:setCascadeOpacityEnabled(true)


    ccs.ArmatureDataManager:getInstance():addArmatureFileInfo("fishgame2d/animation/effect_bar_glod/effect_bar_glod.ExportJson")
    local animaNode = ccs.Armature:create("effect_bar_glod")
    animaNode:getAnimation():play("move")
    animaNode:setAnchorPoint(0.5, 0.5)
    animaNode:addTo(node)


    --  图片的大小 700 280
    local x, y = CGameConfig.GetInstance():GetShowBingonPosition(chairId)
    local labelScore = cc.LabelAtlas:_create("0123456789", "fishgame2d/ui/effect_bar_glod_numbers.png", 35, 46, 48)
    labelScore:setString(score or "0")
    labelScore:setAnchorPoint(1, 0.5)
    labelScore:pos(180, 3)
    labelScore:addTo(node)
    node:pos(x, y)
    self:AddEffectUp(node, "effect_bar_glod_numbers.png")

    node:setOpacity(0xFF * 0.4)
    node:setScale(0.3)
    node:runAction(transition.sequence({
        cc.Spawn:create({
            cc.FadeTo:create(0.55, 0xFF),
            transition.sequence({
                cc.ScaleTo:create(0.35, 1.06),
                cc.ScaleTo:create(0.05, 0.98),
                cc.ScaleTo:create(0.15, 1),
            }),
        }),
        cc.DelayTime:create(1.95),
        cc.CallFunc:create(function(sender)
            sender:removeSelf()
        end)
    }))
end

function FishGame2D:ShowFishGold(fishX, fishY, cannonX, cannonY, score, base)
    if score <= 0 then return end

    local coin_count = score / base
    if (coin_count > 20) then coin_count = 20 end

    local minradius = 40;
    local maxradius = math.max(140, math.min(20, coin_count * 10));

    for i = 1, coin_count do
        local radius = math.random(minradius, maxradius);
        local angle = math.random() * 180
        local angleSin = math.sin(angle)
        local angleCos = math.cos(angle)

        ccs.ArmatureDataManager:getInstance():addArmatureFileInfo("fishgame2d/ui/fish_jinbi_1/fish_jinbi_1.ExportJson")
        local icon = ccs.Armature:create("fish_jinbi_1")
        icon:getAnimation():play("move")
        local x = fishX + radius * angleSin
        local y = fishY + radius * angleCos
        icon:setPosition(x, y)
        icon:setScale(0.8 - math.random() * 0.1)
        icon:setRotation(math.random(90))

        local temp = radius + 20
        local dis_x = fishX + temp * angleSin
        local dis_y = fishY + temp * angleCos

        icon:runAction(transition.sequence({
            cc.EaseExponentialOut:create(cc.MoveTo:create(0.5, cc.p(dis_x, dis_y))),
            cc.DelayTime:create(0.2),
            cc.EaseExponentialIn:create(cc.MoveTo:create(1, cc.p(cannonX, cannonY))),
            cc.CallFunc:create(function(sender)
                sender:removeSelf()
            end)
        }))

        self:AddEffectUp(icon, "fish_jinbi_1")
    end

    local labelScore = cc.LabelAtlas:_create("0123456789", "fishgame2d/ui/gold_hit_numbers.png", 39, 50, 48)
    labelScore:setString(score or 0)
    labelScore:setAnchorPoint(0.5, 0.5)
    labelScore:pos(fishX, fishY)
    self:AddEffectUp(labelScore, "gold_hit_numbers.png")

    labelScore:setOpacity(63)
    labelScore:setScale(0.3)
    labelScore:runAction(transition.sequence({
        cc.Spawn:create({
            cc.FadeTo:create(0.1, 0xFF),
            transition.sequence({
                cc.ScaleTo:create(0.1, 1.3),
                cc.ScaleTo:create(0.15, 1),
            }),
        }),
        cc.DelayTime:create(0.73),
        cc.FadeTo:create(0.34, 198),
        cc.Spawn:create({
            cc.FadeTo:create(0.24, 25),
            cc.ScaleTo:create(0.24, 0.3),
        }),
        cc.CallFunc:create(function(sender)
            sender:removeSelf()
        end)
    }))
end

function FishGame2D:ShowEffectJetton(wChairID, lScore, baseScore)
    if lScore <= 0 then return end
    local pJetton = self.m_pJetton[wChairID]
    if not pJetton then return end

    -- 计算显示的筹码数量 --
    local nCount = 0
    local Max_Jetton_Count = 100
    if #pJetton == 0 then
        nCount = math.min(Max_Jetton_Count, math.max(1, math.floor(lScore / baseScore)))
    else
        local nMaxLayer = 1
        local max_score = 0
        for _, v in ipairs(pJetton) do
            if (v.m_lScore > max_score) then
                nMaxLayer = v.m_nCount
                max_score = v.m_lScore
            end
        end
        nCount = math.min(Max_Jetton_Count, math.max(1, math.floor(lScore * nMaxLayer / max_score)))
    end

    local panenCannon = FishGame2DLogic.GetInstance():GetUILayer():GetChairPanel(wChairID)
    local x = panenCannon.widgetCannon:getPositionX() - 150
    local y = panenCannon.widgetCannon:getPositionY()

    local lastJetton = pJetton[#pJetton]

    local color = Jetton.COLOR.GREEN
    if lastJetton and lastJetton:GetColor() == Jetton.COLOR.GREEN then
        color = Jetton.COLOR.RED
    end

    local jetton = Jetton.new({
        wChairID = wChairID,
        lScore = lScore,
        nColor = color,
        nCount = nCount,
        bUp = panenCannon.bUp,
        onRevemoHandler = handler(self, self._onJettonRemoved)
    })
    jetton:pos(x - #pJetton * Jetton.INTERVAL + Jetton.INTERVAL / 2, y)
    self:AddEffectUp(jetton, "-1")

    table.insert(pJetton, jetton)
end

function FishGame2D:ShowEffectPartical(x, y, name)
    local config = ParticalConfig[name]
    if not config then return end

    ccs.ArmatureDataManager:getInstance():addArmatureFileInfo("fishgame2d/animation/" .. config.AniImage .. "/" .. config.AniImage .. ".ExportJson")
    local animaNode = ccs.Armature:create(config.AniImage)
    animaNode:getAnimation():play(config.AniName)
    animaNode:setAnchorPoint(0.5, 0.5)
    if name == "salute1" then
        animaNode:setPosition(x, y + 270)
    else
        animaNode:setPosition(x, y)
    end
    animaNode:runAction(transition.sequence({
        cc.DelayTime:create(1.95),
        cc.CallFunc:create(function(sender)
            sender:removeSelf()
        end)
    }))

    self:AddEffectUp(animaNode, "effect_fish_bomb_big")
    if name == "bomb" then
        CSoundManager.GetInstance():PlayGameEffect(GameEffect.EFFECT_PARTICAL_BIG_BOMB)
    elseif name == "switch" then
        CSoundManager.GetInstance():PlayGameEffect(GameEffect.EFFECT_PARTICAL_FIREWORKS)
    else
        CSoundManager.GetInstance():PlayGameEffect(GameEffect.EFFECT_PARTICAL_BIG_FIREWORKS)
    end
end

function FishGame2D:StartGame()
    FishGame2DLogic.GetInstance():_OnFinishLoadRes()
end

function FishGame2D:SetSwitchSceneStyle(nId, _endHandler)
    local backgroundImage = self:GetBackgroundImage(nId)


    -- 播放声音 --
    CSoundManager.GetInstance():StopBackMusic();
    CSoundManager.GetInstance():PlayGameEffect(GameEffect.WAVE);


    local fSceneTime = 5
    local fSpeed = test_width / fSceneTime

    if self.m_background then
        self.m_background:runAction(transition.sequence({
            cc.DelayTime:create(fSceneTime),
            cc.CallFunc:create(function(sender)
                sender:removeSelf()
            end)
        }))
    end

    if self.m_bgWater
            and FishGame2DLogic.GetInstance().m_resLoadFinished
    then
        self.m_bgWater:runAction(transition.sequence({
            cc.DelayTime:create(fSceneTime),
            cc.CallFunc:create(function(sender)
                sender:removeSelf()
            end)
        }))
    end

    -- 创建切换动画 --
    local background = display.newSprite(backgroundImage)

    self:addChild(background, LAYER_ZORDER.STATIC_SWITCH_BACKGROUND)

    ccs.ArmatureDataManager:getInstance():addArmatureFileInfo("fishgame2d/animation/effect_bg_water/effect_bg_water.ExportJson")
    local bgWater = ccs.Armature:create("effect_bg_water")
    bgWater:getAnimation():play("effect_bg_water_animation")
    self:addChild(bgWater, LAYER_ZORDER.STATIC_SWITCH_WATER)

    -- 切换波浪 --
    ccs.ArmatureDataManager:getInstance():addArmatureFileInfo("fishgame2d/animation/effect_transition_water/effect_transition_water.ExportJson")
    local bgSwitchWave = ccs.Armature:create("effect_transition_water")
    bgSwitchWave:getAnimation():play("effect_transition_water_animation")
    self:addChild(bgSwitchWave, LAYER_ZORDER.STATIC_SWITCH_WAVE)

    self.m_background = background
    self.m_bgWater = bgWater


    if CGameConfig.GetInstance():MirrorShow() then
        background:setRotation(180)
        background:setAnchorPoint(0.5, 0.5)
        background:pos(-test_cx, test_cy)

        bgSwitchWave:setRotation(180)
        bgSwitchWave:setPosition(0, test_cy)
        bgWater:setRotation(180)
        bgWater:setPosition(-test_cx, test_cy)

        bgSwitchWave:runAction(transition.sequence({
            cc.MoveTo:create((test_width + 300) / fSpeed, cc.p(test_width + 300, test_cy)),
            cc.CallFunc:create(function(sender)
                sender:removeSelf()
            end)
        }))
        background:runAction(transition.sequence({
            cc.MoveTo:create(fSceneTime, cc.p(test_cx, test_cy)),
            cc.CallFunc:create(function(sender)
                if _endHandler then
                    _endHandler()
                end

                sender:setLocalZOrder(LAYER_ZORDER.STATIC_BACKGROUND)
            end)
        }))
        bgWater:runAction(transition.sequence({
            cc.MoveTo:create(fSceneTime, cc.p(test_cx, test_cy)),
            cc.CallFunc:create(function(sender)
                sender:setLocalZOrder(LAYER_ZORDER.STATIC_WATER)

                CSoundManager.GetInstance():PlayBackMusic(nId)
            end)
        }))
    else
        background:setAnchorPoint(0, 0)
        background:pos(test_width, 0)
        bgSwitchWave:setPosition(test_width, test_cy)
        bgWater:setPosition(test_width + test_cx, test_cy)

        bgSwitchWave:runAction(transition.sequence({
            cc.MoveTo:create((test_width + 300) / fSpeed, cc.p(-300, test_cy)),
            cc.CallFunc:create(function(sender)
                sender:removeSelf()
            end)
        }))
        background:runAction(transition.sequence({
            cc.MoveTo:create(fSceneTime, cc.p(0, 0)),
            cc.CallFunc:create(function(sender)
                if _endHandler then
                    _endHandler()
                end

                sender:setLocalZOrder(LAYER_ZORDER.STATIC_BACKGROUND)
            end)
        }))
        bgWater:runAction(transition.sequence({
            cc.MoveTo:create(fSceneTime, cc.p(test_cx, test_cy)),
            cc.CallFunc:create(function(sender)
                sender:setLocalZOrder(LAYER_ZORDER.STATIC_WATER)

                CSoundManager.GetInstance():PlayBackMusic(nId)
            end)
        }))
    end
end

function FishGame2D:SetSceneStyle(nId)
    CSoundManager.GetInstance():PlayBackMusic(nId)

    local backgroundImage = self:GetBackgroundImage(nId)

    if self.m_background then self.m_background:removeSelf() end
    if self.m_bgWater then self.m_bgWater:removeSelf() end

    ccs.ArmatureDataManager:getInstance():addArmatureFileInfo("fishgame2d/animation/effect_bg_water/effect_bg_water.ExportJson")
    self.m_bgWater = ccs.Armature:create("effect_bg_water"):align(display.CENTER, test_cx, test_cy):addTo(self, LAYER_ZORDER.STATIC_WATER)
    self.m_bgWater:getAnimation():play("effect_bg_water_animation")

    self.m_background = display.newSprite(backgroundImage):align(display.CENTER, test_cx, test_cy):addTo(self, LAYER_ZORDER.STATIC_BACKGROUND)

    if CGameConfig.GetInstance():MirrorShow() then
        self.m_background:setRotation(180)
    else
        self.m_background:setRotation(0)
    end
end

function FishGame2D:GetBackgroundImage(nId)
    local index = nId % #BackgroundImage + 1
    return BackgroundImage[index]
end

return FishGame2D;