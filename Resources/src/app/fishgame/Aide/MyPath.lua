local pi_2 = math.pi / 2

function CalcAngle(x1, y1, x2, y2)
    local x = x1 - x2
    local y = y1 - y2

    if y == 0 then
        if x1 < x2 then
            return pi_2
        else
            return -pi_2
        end
    end

    local deg = math.atan(x / y)
    if y < 0 then
        return -deg + math.pi
    else
        return -deg
    end
end

function CacBesier(x, y, count, per)
    if count == 3 then
        local x1 = x[1] + (x[2] - x[1]) * per
        local x2 = x[2] + (x[3] - x[2]) * per

        local y1 = y[1] + (y[2] - y[1]) * per
        local y2 = y[2] + (y[3] - y[2]) * per

        return x1 + (x2 - x1) * per, y1 + (y2 - y1) * per, CalcAngle(x1, y1, x2, y2) - pi_2
    else
        local x1 = x[1] + (x[2] - x[1]) * per
        local x2 = x[2] + (x[3] - x[2]) * per
        local x3 = x[3] + (x[4] - x[3]) * per

        local y1 = y[1] + (y[2] - y[1]) * per
        local y2 = y[2] + (y[3] - y[2]) * per
        local y3 = y[3] + (y[4] - y[3]) * per

        local xx1 = x1 + (x2 - x1) * per
        local xx2 = x2 + (x3 - x2) * per

        local yy1 = y1 + (y2 - y1) * per
        local yy2 = y2 + (y3 - y2) * per

        return xx1 + (xx2 - xx1) * per, yy1 + (yy2 - yy1) * per, CalcAngle(xx1, yy1, xx2, yy2) - pi_2
    end
end

function CacLine(x, y, percent)
--    return 0, 0, 0
        return x[1] + (x[2] - x[1]) * percent, y[1] + (y[2] - y[1]) * percent--, CalcAngle(x[1], y[1], x[2], y[2]) - pi_2
end

function CalCircle(centerX, centerY, radius, begin, fAngle, fAdd, percent)
    local absFAngle = math.abs(fAngle)
    local _radius = radius * (1 + fAdd * percent * absFAngle)
    local angle = begin + percent * absFAngle
    return centerX + _radius * math.cos(angle), centerY + _radius * math.sin(angle), angle + math.pi / 2
end

function RotationDot() end