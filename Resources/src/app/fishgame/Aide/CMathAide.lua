CMathAide = {}

local FactorialLookup = {}

FactorialLookup[0] = 1.0;
FactorialLookup[1] = 1.0;
FactorialLookup[2] = 2.0;
FactorialLookup[3] = 6.0;
FactorialLookup[4] = 24.0;
FactorialLookup[5] = 120.0;
FactorialLookup[6] = 720.0;
FactorialLookup[7] = 5040.0;
FactorialLookup[8] = 40320.0;
FactorialLookup[9] = 362880.0;
FactorialLookup[10] = 3628800.0;
FactorialLookup[11] = 39916800.0;
FactorialLookup[12] = 479001600.0;
FactorialLookup[13] = 6227020800.0;
FactorialLookup[14] = 87178291200.0;
FactorialLookup[15] = 1307674368000.0;
FactorialLookup[16] = 20922789888000.0;
FactorialLookup[17] = 355687428096000.0;
FactorialLookup[18] = 6402373705728000.0;
FactorialLookup[19] = 121645100408832000.0;
FactorialLookup[20] = 2432902008176640000.0;
FactorialLookup[21] = 51090942171709440000.0;
FactorialLookup[22] = 1124000727777607680000.0;
FactorialLookup[23] = 25852016738884976640000.0;
FactorialLookup[24] = 620448401733239439360000.0;
FactorialLookup[25] = 15511210043330985984000000.0;
FactorialLookup[26] = 403291461126605635584000000.0;
FactorialLookup[27] = 10888869450418352160768000000.0;
FactorialLookup[28] = 304888344611713860501504000000.0;
FactorialLookup[29] = 8841761993739701954543616000000.0;
FactorialLookup[30] = 265252859812191058636308480000000.0;
FactorialLookup[31] = 8222838654177922817725562880000000.0;
FactorialLookup[32] = 263130836933693530167218012160000000.0;


function CMathAide.BuildCircle(centerX, centerY, radius, FishCount)
    local FishPos = {}

    if FishCount <= 0 or radius == 0 then return FishPos end
    local cell_radian = 2 * math.pi / FishCount;
    for i = 1, FishCount do

        local pp = {}
        pp.m_Position.x_ = centerX + radius * math.cosf(i * cell_radian);
        pp.m_Position.y_ = centerY + radius * math.sinf(i * cell_radian);
        pp.m_Direction = cell_radian;
        table.inset(FishPos, pp)
    end
    return FishPos
end

function CMathAide.BuildLinear(initX, initY, initCount, fDistance)
    local TraceVector = {}

    if initCount < 2 then return end
    if fDistance <= 0.0 then return end

    local disTotal = CMathAide.CalcDistance(initX[initCount], initY[initCount], initX[1], initY[1]);
    if disTotal <= 0.0 then return end

    disTotal = math.min(disTotal, 2000)

    local tAngle = CMathAide.CalcAngle(initX[initCount], initY[initCount], initX[1], initY[1]) - math.pi / 2;

    local point = CMovePoint.new(MyPoint.new(initX[1], initY[1]), tAngle)
    table.insert(TraceVector, clone(point))

    local tfDis = 0.0
    while tfDis < disTotal do
        local size = #TraceVector

        point.m_Position.x_ = initX[1] + math.cos(tAngle) * (fDistance * size);
        point.m_Position.y_ = initY[1] + math.sin(tAngle) * (fDistance * size);
        point.m_Direction = tAngle;

        table.insert(TraceVector, clone(point))

        tfDis = CMathAide.CalcDistance(point.m_Position.x_, point.m_Position.y_, initX[1], initY[1]);
    end

    TraceVector[#TraceVector].m_Position.x_ = initX[initCount]
    TraceVector[#TraceVector].m_Position.y_ = initY[initCount]

    return TraceVector
end

function CMathAide.BuildBezier(initX, initY, initCount, fDistance)
    if (initCount < 3) then return end

    local TraceVector = {}

    local index = 0;

    local t = 0
    local count = initCount - 1
    local tfDis = fDistance;
    local tPos0 = CMovePoint.new()
    local tPos = CMovePoint.new()

    while (t < 1.0) do
        tPos.m_Position.x_ = 0.0
        tPos.m_Position.y_ = 0.0
        index = 0;
        while (index <= count) do
            local tempValue = math.pow(t, index) * math.pow(1. - t, count - index) * CMathAide.Combination(count, index);
            tPos.m_Position.x_ = tPos.m_Position.x_ + initX[index + 1] * tempValue;
            tPos.m_Position.y_ = tPos.m_Position.y_ + initY[index + 1] * tempValue;
            index = index + 1
        end

        local fSpace = 0.
        if #TraceVector > 0 then
            local backPos = TraceVector[#TraceVector]
            fSpace = CMathAide.CalcDistance(backPos.m_Position.x_, backPos.m_Position.y_, tPos.m_Position.x_, tPos.m_Position.y_);
        end


        if fSpace >= tfDis or #TraceVector == 0 then
            if #TraceVector > 0 then
                local temp_dis = CMathAide.CalcDistance(tPos.m_Position.x_, tPos.m_Position.y_, tPos0.m_Position.x_, tPos0.m_Position.y_);
                if (temp_dis ~= 0.0) then
                    local tempValue = (tPos.m_Position.x_ - tPos0.m_Position.x_) / temp_dis;
                    if ((tPos.m_Position.y_ - tPos0.m_Position.y_) >= 0.0) then
                        tPos.m_Direction = math.acos(tempValue);
                    else
                        tPos.m_Direction = -math.acos(tempValue);
                    end
                else
                    tPos.m_Direction = 1.0
                end
            else
                tPos.m_Direction = 1.0
            end
            table.insert(TraceVector, clone(tPos))

            tPos0.m_Position.x_ = tPos.m_Position.x_;
            tPos0.m_Position.y_ = tPos.m_Position.y_;
        end

        t = t + 0.00001
    end

    return TraceVector
end

function CMathAide.BuildBezier(initX, initY, posCount, fDistance,cpts)
    local outVector = {}
    local j = 0
    local step, t;
    t = 0;
    step = 1.0 / (cpts - 1)

    for i1 = 0, cpts - 1 do
        if ((1.0 - t) < 5e-6) then
            t = 1.0;
        end
        j = 0;

        local mp = CMovePoint.new(MyPoint.new(0, 0), 0)

        for i = 0, posCount - 1 do
            local basis = CMathAide.Bernstein(posCount - 1, i, t);
            mp.m_Position.x_ = mp.m_Position.x_ + basis * initX[j + 1];
            mp.m_Position.y_ = mp.m_Position.y_ + basis * initY[j + 1];
            j = j + 1;
        end
        if (#outVector > 0) then

            local last = outVector[i1 - 1 + 1];
            mp.m_Direction = CMathAide.CalcAngle(mp.m_Position.x_, mp.m_Position.y_, last.m_Position.x_, last.m_Position.y_) - math.pi / 2;
        else
            mp.m_Direction = 1.0
        end
        table.insert(outVector, mp)
        t = t + step;
    end

    return outVector
end

function CMathAide.BuildCirclePath(centerX, centerY, radius, begin, fAngle, nStep, fAdd)
    local FishPos = {}

    if (fAngle == 0.0 or radius == 0) then return end
    if (nStep < 1) then nStep = 1 end

    local nCir = 2 * math.pi * radius / nStep;
    local nCount = nCir * math.abs(fAngle) / (2 * math.pi);
    local cell_radian = 2 * math.pi / nCir * fAngle / math.abs(fAngle);

    local pLast = MyPoint.new(0, 0)
    for i = 0, nCount + 1 do
        local pp = CMovePoint.new(MyPoint.new(0, 0), 0)
        pp.m_Position.x_ = centerX + radius * math.cos(begin + i * cell_radian);
        pp.m_Position.y_ = centerY + radius * math.sin(begin + i * cell_radian);

        if (i == 0) then
            pp.m_Direction = begin + i * cell_radian + math.pi / 2;
        else
            pp.m_Direction = CMathAide.CalcAngle(pLast.x_, pLast.y_, pp.m_Position.x_, pp.m_Position.y_) + math.pi / 2;
        end

        pLast = pp.m_Position;

        if (fAdd ~= 0) then
            radius = radius + fAdd;
        end
        table.insert(FishPos, pp)
    end

    return FishPos
end

function CMathAide.Ni(n, i, t)
    local ni;
    local a1 = CMathAide.Factorial(n);
    local a2 = CMathAide.Factorial(i);
    local a3 = CMathAide.Factorial(n - i);
    ni = a1 / (a2 * a3);
    return ni;
end

function CMathAide.Bernstein(n, i, t)
    local basis;
    local ti;
    local tni;

    if (t == 0.0 and i == 0) then
        ti = 1.0;
    else
        ti = math.pow(t, i);
    end

    if (n == i and t == 1.0) then
        tni = 1.0;
    else
        tni = math.pow((1 - t), (n - i));
    end

    basis = CMathAide.Ni(n, i) * ti * tni;
    return basis;
end

function CMathAide.CalcDistance(x1, y1, x2, y2)
    return math.sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));
end

function CMathAide.Combination(count, r)
    return CMathAide.Factorial(count) / (CMathAide.Factorial(r) * CMathAide.Factorial(count - r));
end

function CMathAide.Factorial(number)
    if not (number >= 0 and number <= 32) then return end

    return FactorialLookup[number]
end

function CMathAide.CalcAngle(x1, y1, x2, y2)
    local fDistance = CMathAide.CalcDistance(x1, y1, x2, y2);
    if fDistance == 0.0 then return 0 end

    local cosValue = (x1 - x2) / fDistance;
    local angle = math.acos(cosValue);
    if (y1 < y2) then
        angle = 2 * math.pi - angle;
    end
    angle = angle + math.pi / 2

    return angle;
end

return CMathAide