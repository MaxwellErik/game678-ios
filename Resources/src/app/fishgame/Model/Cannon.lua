local Cannon = class("Cannon",{})

local CANNON_ANIMATION = {
    MOVE = "move",
}

Cannon.TYPE = {
    1, 2, 3, 4
}
Cannon._type = 1

function Cannon:ctor(player)
    self._player = player
    self._type = 1

    self._angle = math.pi
end

function Cannon:Clear()
    if self._cnode then
        self._cnode:removeSelf()
    end
    self._cnode = nil
end

function Cannon:getLevel()
    return 1
end

function Cannon:updateType(type)
    self:Clear()
end

function Cannon:SetCannonAngle(ang)
    self._angle = ang or 0;
end

--获取加农炮的旋转角度
function Cannon:GetCannonAngle()
    return self._angle or 0
end

-- 播放开火动画 --
function Cannon:Fire()
    if self._cnode then
        self._cnode.cannon:getAnimation():play(CANNON_ANIMATION.MOVE)
    end
end

function Cannon:update()
end

function Cannon:GetBulletConfig()
    return self.m_bulletConfig
end


function Cannon:GetMultiply()
    return self.m_nMultiply
end

function Cannon:GetMultiplyOffset()
    return self.m_nMultiplyOffsetX, self.m_nMultiplyOffsetY
end

function Cannon:GetPosition()
    return 0,0
end

function Cannon:GetCurcannonMultiply()
    return self._player:GetCannonMultiply()
end

function Cannon:GetArmatureName()
    local player = self._player

    local cannonSetType = player:GetCannonSetType()
    local cannonType = player:GetCannonType()
    local cannonMultiply = player:GetCannonMultiply()
    local cannonSet = CGameConfig.GetInstance().CannonSetArray[cannonSetType]
    local bulletConfig = CGameConfig.GetInstance().BulletVector[cannonMultiply + 1]

    local cannon = cannonSet.Sets[cannonType]
    
    self.m_bulletConfig = bulletConfig
    self.m_nMultiply = cannonMultiply
    self.m_nMultiplyOffsetX, self.m_nMultiplyOffsetY = cannon.Cannon.PosX, cannon.Cannon.PosY

    return cannon.Cannon.szResourceName
end

return Cannon