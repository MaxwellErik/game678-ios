FISH_ANIMATION_TYPE = {
    FRAME = 0,
    SKELETON = 1,
}

local Fish = class("Fish", function()
    return fishgame.Fish:Create()
end)

function Fish:ctor()
    self.m_effects = {}
end

function Fish:SetTypeID(y) self.m_nTypeId = y end

function Fish:GetTypeID() return self.m_nTypeId end

function Fish:SetFishType(id)
    self.m_FishType = id
end

function Fish:GetFishType(id) return self.m_FishType end

function Fish:SetFishCofigData(finf)
    self.m_fishConfigData = finf
end

function Fish:GetFishCofigData()
    return self.m_fishConfigData
end

function Fish:SetRefershID(id) self.m_nRefershID = id end

function Fish:GetRefershID() return self.m_nRefershID end
function Fish:SetScore(mul) self.m_nMul = mul end

function Fish:GetScore() return self.m_nMul end
function Fish:SetCreateTick(m)  self.m_nCreateTick =m end

function Fish:AddEffect(effct)
    table.insert(self.m_effects, effct)
end

function Fish:ExecuteEffects(pTarget, list, bPretreating)
    local Score = 0
    for _, v in ipairs(list) do
        if v:GetId() == self:GetId() then
            return Score, list
        end
    end

    if self:GetState() < EOS_DEAD then
        table.insert(list, self)

        for _, v in ipairs(self.m_effects) do
            Score, list = v:Execute(self, pTarget, list, bPretreating)
        end
    end

    return Score, list
end

return Fish