local Bullet = class("Bullet", function()
    return fishgame.Bullet:Create()
end)

function Bullet:ctor()
    self.ProbabilitySet = {}
    self.m_nMaxCatch = 0
    self.m_nCatchRadio = 0
    self.m_nCannonType = 0
    self.m_nCannonSetType = 0
    self.m_wChairID = 0
    self.m_nSize = 0
    self.m_bDouble = false
end

function Bullet:AddProbilitySet(ftp, pp)
    self.ProbabilitySet[ftp] = pp
end

function Bullet:GetProbilitySet(ftp)
    return self.ProbabilitySet[ftp] or MAX_PROBABILITY
end

function Bullet:GetMaxCatch() return self.m_nMaxCatch end

function Bullet:SetMaxCatch(b) self.m_nMaxCatch = b end

function Bullet:getChairID() return self.m_wChairID end

function Bullet:SetChairID(b) self.m_wChairID = b end

function Bullet:GetSize() return self.m_nSize end

function Bullet:SetSize(b) self.m_nSize = b end

function Bullet:bDouble() return self.m_bDouble end

function Bullet:setDouble(b) self.m_bDouble = b end

function Bullet:SetScore(b) self.m_nScore = b end

function Bullet:GetScore() return self.m_nScore end

return Bullet