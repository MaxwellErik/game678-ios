local Cannon = import(".Cannon")
local Player = class("Player", {})


function Player:ctor(wChairID, logic)

    self.m_Wastage = 0
    self.m_nCannonSetType = app.FishRoomID
    self.m_nCannonType = 0
    self.m_nCannonMultiply = 0

    self.m_CannonPos = MyPoint.new(0, 0)
    self.m_dwLastFireTick = 0
    self.m_dwLockFishID = 0
    self.m_bLocking = false
    self.LockBuffer = {}
    self.BulletCount = 0
    self.m_bCanFire = false


    self.m_chairId = wChairID
    self.m_nUserId = -1
    self.m_lScore = 0
    self.m_nickName = "???"



    self.m_pCannon = Cannon.new(self)
end

function Player:ClearSet(chairid)
    self:SetScore(0)
    self:SetProbability(MAX_PROBABILITY)
    self.m_Wastage = 0
    self.m_nCannonType = CGameConfig.GetInstance().BulletVector[0].nCannonType
    self.BulletCount = 0
    self.m_bCanFire = true

    local clientTime = TimeService.GetInstance():GetClientTick()
    self:SetCreateTick(clientTime)
    self:SetLastFireTick(clientTime)

    self.LockBuffer = {}
    self.m_dwLockFishID = 0;
end

function Player:SetCannonType(n) self.m_nCannonType = n end

function Player:GetCannonType() return self.m_nCannonType
end

function Player:AddWastage(s) self.m_Wastage = self.m_Wastage + s
end

function Player:GetWastage() return self.m_Wastage
end

function Player:SetCannonMultiply(n) self.m_nCannonMultiply = n
end

function Player:GetCannonMultiply() return self.m_nCannonMultiply
end


function Player:SetLastFireTick(dw) self.m_dwLastFireTick = dw
end

function Player:GetLastFireTick() return self.m_dwLastFireTick
end

function Player:SetLockFishID(id)
    self.m_dwLockFishID = id

    if (self.m_dwLockFishID ~= 0) then
        table.insert(self.LockBuffer, id)
    else
        self:ClearLockedBuffer()
    end
end

function Player:GetLockFishID() return self.m_dwLockFishID
end

function Player:HasLocked(id)
    for _, v in ipairs(self.LockBuffer) do
        if id == v then
            return true
        end
    end

    return false;
end

function Player:ClearLockedBuffer() self.LockBuffer = {}
end

function Player:bLocking() return self.m_bLocking
end

function Player:SetLocking(b) self.m_bLocking = b
end

function Player:ADDBulletCount(n) self.BulletCount = self.BulletCount + n
end

function Player:ClearBulletCount() self.BulletCount = 0
end

function Player:GetBulletCount() return self.BulletCountBulletCount
end

function Player:GetCannonSetType() return self.m_nCannonSetType
end

function Player:SetCannonSetType(n) self.m_nCannonSetType = n
end

function Player:CanFire() return self.m_bCanFire
end

function Player:SetCanFire(b) self.m_bCanFire = b
end

function Player:GetChairId() return self.m_chairId end

function Player:AddScore(s) self.m_lScore = self.m_lScore + s end

function Player:SetScore(s) 
    self.m_lScore = s 
end

function Player:GetScore() return self.m_lScore end

function Player:SetNickName(s) self.m_nickName = s end
function Player:GetNickName() return self.m_nickName end

function Player:SetUserId(s) self.m_nUserId = s end
function Player:GetUserId() return self.m_nUserId end

function Player:SetPlayerInfo(s) self.m_playerInfo = s end

function Player:GetPlayerInfo() return self.m_playerInfo end

function Player:GetCannon()
    return self.m_pCannon
end

function Player:Clear()
    self.m_pCannon:Clear()
end

return Player