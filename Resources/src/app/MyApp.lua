local MyApp = class("MyApp", cc.mvc.AppBase)
require("app.fishgame.FishGame2DConfig")

function MyApp:ctor()
    MyApp.super.ctor(self)
end

function MyApp:run()
    cc.FileUtils:getInstance():addSearchPath("res/")
    self:enterScene("GameChoiceFish")
end

function MyApp:enterFish()
	self:enterScene("GameChoiceFish")
end

return MyApp  