require("config")
require("cocos.init")
require("framework.init")
require("app.HallDefine")
require("app.scenes.HallBase")

--此文件为所有游戏的入口配置文件

--wKind:游戏ID gameName:游戏名称 module:游戏模块  entrance:游戏入口加载文件 dwClientVersion:游戏版本号
allGameConfs = {
	{["wKind"]=130, ["gameModule"]="fishgame2d.zip",["gameKey"]="fishgame2d",["gameName"]="捕鱼达人2d", ["module"]="fishgame.FishGame2D", ["entrance"]="app.fishgame.FishGame2DConfig", ["dwClientVersion"]=101056521},	
}