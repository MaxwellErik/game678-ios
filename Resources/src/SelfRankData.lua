if type(SelfRankDatals) ~= 'table' then
	_G.SelfRankDatals = 
	{
		[1] = {},
		[2] = {},
		[3] = {},
	}
end

function setSelfRankData(dwType, dwRanking, dwCatchScore, szEndTime)
	local rankData = {}	
	rankData.dwType = dwType
	rankData.dwRanking = dwRanking
	rankData.dwCatchScore = dwCatchScore
	rankData.szEndTime = szEndTime
	
	SelfRankDatals[dwType][#SelfRankDatals[dwType] + 1] = rankData

    return
end

function getSelfRankData()
	return SelfRankDatals
end