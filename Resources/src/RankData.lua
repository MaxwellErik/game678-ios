if type(RankDatals) ~= 'table' then
	_G.RankDatals = 
	{
		[1] = {},
		[2] = {},
		[3] = {},
	}
end

function setRankData(dwType, dwRanking, dwCatchScore, dwUserId, dwMatchCount, szNickName)--, szStartTime, szEndTime)
	local rankData = {}	
	rankData.dwType = dwType
	rankData.dwRanking = dwRanking
	rankData.dwCatchScore = dwCatchScore
	rankData.dwUserId = dwUserId
	rankData.dwMatchCount = dwMatchCount
	rankData.szNickName = szNickName
	-- rankData.szStartTime = szStartTime
	-- rankData.szEndTime = szEndTime

	RankDatals[dwType][dwRanking] = rankData

    return
end

function getRankData ()
	return RankDatals
end