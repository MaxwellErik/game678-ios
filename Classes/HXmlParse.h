﻿#ifndef __HXMLPARSE_H__
#define __HXMLPARSE_H__

#include "cocos2d.h"
#include "base/CCRef.h"
#include "deprecated/CCDictionary.h"
using namespace cocos2d;
class HXmlParse : public Ref,public SAXDelegator
{
public:
	static HXmlParse* parserWithFile(const char* tmxFile,bool isJumpHead=false);
	bool initXmlParse(const char* xmlName);

	void startElement(void *ctx,const char* name,const char** atts);
	void endElement(void *ctx,const char* name);
	void textHandler(void *ctx, const char *s, int len);

	std::string root_name;
	bool isJumpHeadData;
	__Dictionary *mDic;
	HXmlParse(void);
	~HXmlParse(void);
protected:
	std::string		startXmlElement;
	std::string		endXmlElement;
	std::string		currString;
	std::string		parentElement;
	__Dictionary	*m_currentDic;
	bool			m_bEnd;
	bool			m_bParentEnd;
};

#endif
