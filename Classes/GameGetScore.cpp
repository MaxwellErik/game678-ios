#include "GameGetScore.h"
#include "LobbySocketSink.h"
#include "Encrypt.h"
#include "SoundUtil.h"
#include "Encrypt.h"
#include "ClientSocketSink.h"
#include "GameLayerMove.h"

GameGetScore::GameGetScore( GameScene *pGameScene ):GameLayer(pGameScene)
{
	m_Select = 0;
}
GameGetScore::~GameGetScore()
{

}

GameGetScore* GameGetScore::create(GameScene *pGameScene)
{
	GameGetScore* temp = new GameGetScore(pGameScene);
	if(temp && temp->init())
	{
		temp->autorelease();
		return temp;
	}
	else
	{
		CC_SAFE_DELETE(temp);
		return NULL;
	}
}

void GameGetScore::FlashScore()
{
	if (m_BankScore != NULL)
	{
		char _score[128];
		sprintf(_score, "%lld", g_GlobalUnits.GetGolbalUserData().lInsureScore);
		m_BankScore->setString(_score);
	}
}

bool GameGetScore::init()
{
	if ( !Layer::init() )	return false;
	setLocalZOrder(10);

	Tools::addSpriteFrame("Common/GetScoreBank.plist");

	m_BackTga = 10;

	Sprite *pBG = Sprite::createWithSpriteFrameName("GetScore_back.png");
	pBG->setPosition(_STANDARD_SCREEN_CENTER_IN_PIXELS_);
	pBG->setTag(m_BackTga);
	addChild(pBG);

	//当前银行数量
	char _score[128];
	sprintf(_score,"%lld", g_GlobalUnits.GetGolbalUserData().lInsureScore);
	m_BankScore = createLabel(_score,Vec2(205, 419),30,Color3B(255,0,0));
	pBG->addChild(m_BankScore);

	//请输入欢乐豆
	Color3B colored;colored.r = 0;colored.g = 0;colored.b = 0;
	std::string strTrade = "\u8bf7\u8f93\u5165\u9152\u5427\u8c46\u6570\u91cf";
	Tools::StringTransform(strTrade);
 
    const Size inputSize = Size(500,40);
    m_ED_Score = cocos2d::ui::EditBox::create(inputSize, INPUT_BACKGROUND);
    m_ED_Score->setFontName(_GAME_FONT_NAME_1_);
    m_ED_Score->setFontSize(30);
    m_ED_Score->setFontColor(colored);
    m_ED_Score->setPlaceholderFontName(_GAME_FONT_NAME_1_);
    m_ED_Score->setPlaceHolder(strTrade.c_str());
    m_ED_Score->setPlaceholderFontSize(30);
    m_ED_Score->setPlaceholderFontColor(Color3B::RED);
    m_ED_Score->setInputMode(cocos2d::ui::EditBox::InputMode::NUMERIC);
	m_ED_Score->setPosition(ccpx(450, 356));
	m_ED_Score->setMaxLength(12);
	pBG->addChild(m_ED_Score);

	//请输入密码
	strTrade = "\u8bf7\u8f93\u5165\u94f6\u884c\u5bc6\u7801";
	Tools::StringTransform(strTrade);
    m_ED_Pass = cocos2d::ui::EditBox::create(inputSize, INPUT_BACKGROUND);
    m_ED_Pass->setFontName(_GAME_FONT_NAME_1_);
    m_ED_Pass->setFontSize(30);
    m_ED_Pass->setFontColor(colored);
    m_ED_Pass->setPlaceholderFontName(_GAME_FONT_NAME_1_);
    m_ED_Pass->setPlaceHolder(strTrade.c_str());
    m_ED_Pass->setPlaceholderFontSize(30);
    m_ED_Pass->setPlaceholderFontColor(Color3B::RED);
    m_ED_Pass->setInputMode(cocos2d::ui::EditBox::InputMode::ANY);
	m_ED_Pass->setPosition(ccpx(450, 146));
	m_ED_Pass->setMaxLength(28);
	pBG->addChild(m_ED_Pass);

	//按钮
	Menu* tempbtn = CreateButton("GetScoreFlash" ,ccpx(230,55),_BtnFlash);
	pBG->addChild(tempbtn);

	tempbtn = CreateButton("GetScoreOk" ,ccpx(520,55),_BtnOK);
	pBG->addChild(tempbtn);

	tempbtn = CreateButton("GetScore_Close" ,ccpx(700,530),_BtnClose);
	pBG->addChild(tempbtn);

	tempbtn = CreateButton("GetScore10w" ,ccpx(98,260),_10w);
	pBG->addChild(tempbtn);
	
	tempbtn = CreateButton("GetScore100w" ,ccpx(280,260),_100w);
	pBG->addChild(tempbtn);

	tempbtn = CreateButton("GetScore1000w" ,ccpx(460,260),_1000w);
	pBG->addChild(tempbtn);

	tempbtn = CreateButton("GetScoreAll" ,ccpx(640,260),_all);
	pBG->addChild(tempbtn);

	setTouchEnabled(true);

    auto listener = EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(true);
    listener->onTouchBegan = CC_CALLBACK_2(GameGetScore::onTouchBegan, this);
    listener->onTouchMoved = CC_CALLBACK_2(GameGetScore::onTouchMoved, this);
    listener->onTouchEnded = CC_CALLBACK_2(GameGetScore::onTouchEnded, this);
    Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(listener, this);
	return true;
}

void GameGetScore::onRemove()
{
	GameLayerMove::sharedGameLayerMoveSink()->ClearGetScore();
	m_pGameScene->removeChild(this);
}

void GameGetScore::onEnter()
{
	GameLayer::onEnter();
}

void GameGetScore::onExit()
{
	GameLayer::onExit();
	Tools::removeSpriteFrameCache("SendLayer.plist");
}

void GameGetScore::callbackBt(Ref *pSender )
{
	Node *pNode = (Node *)pSender;
	switch (pNode->getTag())
	{
	case _10w:
		{
			const char* p = NULL;
			LONGLONG num = 0;
			if (m_Select == 10)
			{
				if (m_ED_Score!= NULL)  p = m_ED_Score->getText();
				if (strcmp(p, "") == 0) num = 100000;
				else num = atol(p)+100000;
			}
			else
			{
				num = 100000;
				m_Select = 10;
			}
			if (num > g_GlobalUnits.GetGolbalUserData().lInsureScore)
			{
				num = g_GlobalUnits.GetGolbalUserData().lInsureScore;
			}
			char money[256];
			sprintf(money,"%lld",num);
			if (m_ED_Score!= NULL)	m_ED_Score->setText(money) ;
			break;
		}
	case _100w:
		{
			const char* p = NULL;
			LONGLONG num = 0;
			if (m_Select == 100)
			{
				if (m_ED_Score!= NULL)  p = m_ED_Score->getText();
				if (strcmp(p, "") == 0) num = 1000000;
				else num = atol(p)+1000000;
			}
			else
			{
				num = 1000000;
				m_Select = 100;
			}
			if (num > g_GlobalUnits.GetGolbalUserData().lInsureScore)
			{
				num = g_GlobalUnits.GetGolbalUserData().lInsureScore;
			}
			char money[256];
			sprintf(money,"%lld",num);
			if (m_ED_Score!= NULL)	m_ED_Score->setText(money) ;
			break;
		}
	case _1000w:
		{
			const char* p = NULL;
			LONGLONG num = 0;
			if (m_Select == 1000)
			{
				if (m_ED_Score!= NULL)  p = m_ED_Score->getText();
				if (strcmp(p, "") == 0) num = 10000000;
				else num = atol(p)+10000000;
			}
			else
			{
				num = 10000000;
				m_Select = 1000;
			}
			if (num > g_GlobalUnits.GetGolbalUserData().lInsureScore)
			{
				num = g_GlobalUnits.GetGolbalUserData().lInsureScore;
			}
			char money[256];
			sprintf(money,"%lld",num);
			if (m_ED_Score!= NULL)	m_ED_Score->setText(money) ;
			break;
		}
	case _all:
		{
			LONGLONG num = g_GlobalUnits.GetGolbalUserData().lInsureScore;
			char money[256];
			sprintf(money,"%lld",num);
			if (m_ED_Score!= NULL)	m_ED_Score->setText(money) ;
			break;
		}
	case _BtnOK:	//确定按钮
		{
			SoundUtil::sharedEngine()->playEffect("buttonMusic");
			const char* pbankscore = m_BankScore->getString().c_str();
			const char* score = m_ED_Score->getText();
			const char* pass = m_ED_Pass->getText();
			LONGLONG BankScore = 0;
			LONGLONG ED_GetScore = 0;
			if (strcmp(pbankscore,"") != 0) BankScore = atol(pbankscore);
			if (strcmp(score,"") != 0) ED_GetScore = atol(score);
			LONGLONG GetScore = 0;
			if (ED_GetScore > BankScore) GetScore = BankScore;
			else BankScore = ED_GetScore;
			if (BankScore < 1000)
			{
				AlertMessageLayer::createConfirm(this , "\u94f6\u884c\u53d6\u6b3e\u4e0d\u80fd\u5c11\u4e8e1000");
				return;
			}
			if (strcmp(pass,"") == 0)
			{
				AlertMessageLayer::createConfirm(this , "\u5bc6\u7801\u4e0d\u80fd\u4e3a\u7a7a");
				return;
			}
			CMD_GR_C_TakeGameScoreRequest temp;
			temp.cbActivityGame = 1;
			temp.lTakeScore = BankScore;
            
            char buff[50];
            memset(buff, 0, sizeof(buff));
            sprintf(buff, PWDHEAD, pass);
            CMD5Encrypt::EncryptData(buff, temp.szInsurePass);
            
			ClientSocketSink::sharedSocketSink()->SendData(MDM_GR_INSURE, SUB_GR_TAKE_GAME_SCORE_REQUEST , &temp , sizeof(CMD_GR_C_TakeGameScoreRequest));
			
			//onRemove();
			break;
		}
	case _BtnClose: //关闭界面
		{
			SoundUtil::sharedEngine()->playEffect("buttonMusic");
			onRemove();
			break;
		}
	case _BtnFlash:  //刷新欢乐豆
		{
			SoundUtil::sharedEngine()->playEffect("buttonMusic");
			CMD_GR_C_QueryInsureInfoRequest temp;
			temp.cbActivityGame = 1;
			ClientSocketSink::sharedSocketSink()->SendData(MDM_GR_INSURE, SUB_GR_QUERY_INSURE_INFO , &temp , sizeof(CMD_GR_C_QueryInsureInfoRequest));
			break;
		}
	}
}

Menu* GameGetScore::CreateButton( std::string szBtName ,const Vec2 &p , int tag )
{
	Menu *pBT = Tools::Button(StrToChar(szBtName+"_normal.png") , StrToChar(szBtName+"_click.png") , p, this , menu_selector(GameGetScore::callbackBt) , tag);
	return pBT;
}


Label * GameGetScore::createLabel(const char *szText ,const Vec2 &p, int fontSize ,Color3B color, bool bChinese)
{
	std::string szTemp = szText;
	return createLabel(szTemp , p , fontSize , color , bChinese);
}
Label * GameGetScore::createLabel(const std::string szText ,const Vec2 &p, int fontSize ,Color3B color, bool bChinese)
{

	std::string szTempTitle = szText;
	if (bChinese && szTempTitle.length() != 0)
	{
#ifdef _WIN32
		Tools::GBKToUTF8(szTempTitle , "gb2312" , "utf-8");
#endif // _WIN32
	}

	Label *pLable = Label::createWithSystemFont(szTempTitle.c_str(), _GAME_FONT_NAME_1_, fontSize);
	pLable->setHorizontalAlignment(TextHAlignment::LEFT);
	pLable->setColor(color);
	pLable->setAnchorPoint(Vec2(0,0));
	pLable->setPosition(ccppx(p));
	return pLable;
}
