﻿#include "CCGameScrollView.h"


CCCGameScrollView::CCCGameScrollView()
	: m_fAdjustSpeed(ADJUST_ANIM_VELOCITY)
	, m_bSetDirection(false)
	, m_nCurPage(0)
{

}

CCCGameScrollView::~CCCGameScrollView()
{

}



bool CCCGameScrollView::init()
{
	if (!ScrollView::init())
	{
		return false;
	}

	return true;
}


bool CCCGameScrollView::onTouchBegan( Touch *pTouch, Event *pEvent )
{
	m_BeginOffset = getContentOffset();
	return ScrollView::onTouchBegan(pTouch, pEvent);
}

void CCCGameScrollView::onTouchMoved( Touch *pTouch, Event *pEvent )
{
	ScrollView::onTouchMoved(pTouch, pEvent);
}

void CCCGameScrollView::onTouchEnded( Touch *pTouch, Event *pEvent )
{
	Vec2 touchPoint = pTouch->getLocationInView();
	touchPoint = Director::getInstance()->convertToGL( touchPoint );

	ScrollView::onTouchEnded(pTouch, pEvent);

	Vec2 m_EndOffset = getContentOffset();

	//点击Page的功能
	if (m_BeginOffset.equals(m_EndOffset))
	{
		int nPage = -1;
        if (_direction == ScrollView::Direction::HORIZONTAL)
		{
			nPage = std::abs(m_EndOffset.x / (int)m_CellSize.width);
		}
		else
		{
			nPage = std::abs(m_EndOffset.y / (int)m_CellSize.height);
		}
		CCCGameScrollViewDelegate *pDele = (CCCGameScrollViewDelegate *)_delegate;
		Node *pPgae = _container->getChildByTag(nPage);
		Rect rcContent;
		rcContent.origin = pPgae->getPosition();
		rcContent.size = pPgae->getContentSize();
		rcContent.origin.x -= rcContent.size.width / 2;
		rcContent.origin.y -= rcContent.size.height / 2;

		Vec2 pos = touchPoint;
        if (_direction == ScrollView::Direction::HORIZONTAL)
		{
			pos.x += nPage * m_CellSize.width;
		}
		else
		{
			pos.y -= nPage * m_CellSize.height;
		}

		if (rcContent.containsPoint(pos))
		{
			pDele->scrollViewClick(m_EndOffset, touchPoint, pPgae, nPage);
		}
		return ;
	}

	//自动调整
	adjustScrollView(m_BeginOffset, m_EndOffset);
}

void CCCGameScrollView::onTouchCancelled(Touch *pTouch, Event *pEvent )
{
	ScrollView::onTouchCancelled(pTouch, pEvent);

	Vec2 m_EndOffset = getContentOffset();
	adjustScrollView(m_BeginOffset, m_EndOffset);
}


void CCCGameScrollView::adjustScrollView( const cocos2d::Vec2 &oBegin, const cocos2d::Vec2 &oEnd)
{
	int nPage = 0;
	int nAdjustPage = 0;

    if (_direction == ScrollView::Direction::HORIZONTAL)
	{
		nPage = std::abs(oBegin.x / (int)m_CellSize.width);
		int nDis = oEnd.x - oBegin.x;

		if (nDis < -getViewSize().width / 5)
		{
			nAdjustPage = nPage + 1;
		}
		else if (nDis > getViewSize().width / 5)
		{
			nAdjustPage = nPage - 1;
		}
		else
		{
			nAdjustPage = nPage;
		}
	}
	else
	{
		nPage = std::abs(oBegin.y / (int)m_CellSize.height);
		int nDis = oEnd.y - oBegin.y;

		if (nDis < -getViewSize().height / 5)
		{
			nAdjustPage = nPage - 1;
		}
		else if (nDis > getViewSize().height / 5)
		{
			nAdjustPage = nPage + 1;
		}
		else
		{
			nAdjustPage = nPage;
		}
	}

	nAdjustPage = MIN(nAdjustPage, m_nPageCount - 1);
	nAdjustPage = MAX(nAdjustPage, 0);

	scrollToPage(nAdjustPage);
}


void CCCGameScrollView::scrollToPage( int nPage )
{
	// 关闭CCScrollView中的自调整
	unscheduleAllCallbacks();

	Vec2 oOffset = getContentOffset();
	// 调整位置
	Vec2 adjustPos;
    if (_direction == ScrollView::Direction::HORIZONTAL)
	{
		adjustPos = Vec2(-m_CellSize.width * nPage, 0);
	}
	else
	{
		adjustPos = Vec2(0, m_CellSize.height * nPage);
	}
	// 调整动画时间
	float adjustAnimDelay = adjustPos.distance(oOffset) / m_fAdjustSpeed;

	// 调整位置
	setContentOffsetInDuration(adjustPos, adjustAnimDelay);

	if (nPage != m_nCurPage)
	{
		m_nCurPage = nPage;
		scheduleOnce(schedule_selector(CCCGameScrollView::onScrollEnd), adjustAnimDelay);
	}
}

void CCCGameScrollView::onScrollEnd(float fDelay)
{
	CCCGameScrollViewDelegate *pDele = (CCCGameScrollViewDelegate *)_delegate;
	pDele->scrollViewScrollEnd(_container->getChildByTag(m_nCurPage), m_nCurPage);
}



void CCCGameScrollView::scrollToNextPage()
{
	int nCurPage = getCurPage();
	if (nCurPage >= m_nPageCount - 1)
	{
		return ;
	}
	scrollToPage(nCurPage + 1);
}

void CCCGameScrollView::scrollToPrePage()
{
	int nCurPage = getCurPage();
	if (nCurPage <= 0)
	{
		return ;
	}
	scrollToPage(nCurPage - 1);
}


bool CCCGameScrollView::createContainer(CCCGameScrollViewDelegate *pDele, int nCount, const cocos2d::Size &oSize )
{
	CCAssert(m_bSetDirection, "must call setDirection first!!!");
	m_nPageCount = nCount;
	m_CellSize = oSize;
	setDelegate(pDele);

	Layer *pContainer = Layer::create();

	Size winSize = Director::getInstance()->getVisibleSize();
	for (int i = 0; i < nCount; ++i)
	{
		Node *pNode = Node::create();
		pDele->scrollViewInitPage(this, pNode, i);

        if (_direction == ScrollView::Direction::HORIZONTAL)
		{
			pNode->setPosition(Vec2(winSize.width / 2 + i * oSize.width, winSize.height / 2));
		}
		else
		{
			pNode->setPosition(Vec2(winSize.width / 2, winSize.height / 2 - i * oSize.height));
		}
		Size oMaxSize;
        
        Vector<Node*> children = pNode->getChildren();
        for (Vector<Node*>::iterator it = children.begin(); it != children.end(); it++)
        {
			Node *pNode = *it;
			oMaxSize.width = MAX(oMaxSize.width, pNode->getContentSize().width);
			oMaxSize.height = MAX(oMaxSize.height, pNode->getContentSize().height);
		}
		pNode->setContentSize(oMaxSize);
		pNode->setTag(i);
		pContainer->addChild(pNode);
	}

	setContainer(pContainer);

	return true;
}

int CCCGameScrollView::getCurPage()
{
	return m_nCurPage;
}

void CCCGameScrollView::setCurPage( int nPage )
{
	CCAssert(m_nCurPage >= 0 && m_nCurPage < m_nPageCount, "");

    if (_direction == ScrollView::Direction::HORIZONTAL)
	{
		setContentOffset(Vec2(-nPage * m_CellSize.width, 0));
	}
	else
	{
		setContentOffset(Vec2(0, nPage * m_CellSize.height));
	}
	m_nCurPage = nPage;
}

int CCCGameScrollView::getPageCount()
{
	return m_nPageCount;
}

Node *CCCGameScrollView::getPage( int nPage )
{
	return _container->getChildByTag(nPage);
}

void CCCGameScrollView::setDirection( ScrollView::Direction eDirection )
{
    CCAssert(eDirection != ScrollView::Direction::BOTH, "Does not suppost kCCScrollViewDirectionBoth!!!");
	ScrollView::setDirection(eDirection);
	m_bSetDirection = true;
}
