#ifndef _GAMETABLE_H_
#define _GAMETABLE_H_

#include "CMD_Game.h"
#include "GameScene.h"
#include "LockPassWord.h"
#include "ui/UIScrollView.h"

struct UserArr
{
    WORD   tableID;
    WORD   chairID;
};
struct Broadcast
{
public:
    Broadcast()
    {
        ZeroMemory(szText,256);
    }
    char szText[256];
};

class GameTable : public GameLayer
{
public:
	virtual bool init();  
	static GameTable *create(GameScene *pGameScene);
	virtual ~GameTable();
	GameTable(GameScene *pGameScene);
	GameTable(const GameTable&);
	GameTable& operator = (const GameTable&);

private:
	virtual void onEnter();
	virtual void onExit();

    virtual bool onTouchBegan(Touch *pTouch, Event *pEvent);
    virtual void onTouchMoved(Touch *pTouch, Event *pEvent);
	virtual void onTouchEnded(Touch *pTouch, Event *pEvent){}
	virtual void onTouchCancelled(Touch *pTouch, Event *pEvent){}
    
    Menu* CreateButton(std::string szBtName ,const Vec2 &p, int tag );
	virtual void callbackBt(Ref *pSender);

public:
    void update(float dt);
    //用户状态
    void OnEventUserStatus(tagUserData * pUserData);
    void DeleteUser(tagUserData* pUserData);
    void AddUser(tagUserData* pUserData);
    void InitTableStatus(CMD_GR_TableInfo* TableStatus);
    void UpdateTableStatus(CMD_GR_TableStatus* TableStatus);
    
public:
    void OnCallMove();
    void initTable();

    void addTable2(Vec2 tablePos);
    void addTable3(Vec2 tablePos);
    void addTable4(Vec2 tablePos);
    void addTable6(Vec2 tablePos);
    void SetTitle(char* src);
    void SetTableName(char* _kind,char* roomname);
    void GoToKind(Ref *pSender);
    
    void AddGameBroadcast(char* msg);
    void CloseTable(char* text);
public:
    ui::ScrollView*     m_ScrollTabel;
    int                 m_BtnTga;
   
	Label*              m_Title;
	int                 m_GameName_Tga;
    int                 m_gameRoom_Tga;
    map<DWORD, UserArr> m_UserMap;
    map<int, Vec2>      m_NamePosMap;
    map<int, Vec2>      m_HandPosMap;
    WORD                m_table;
    WORD                m_chair;
	Menu*               m_QuickEnter;
    
    map<int, tagTableStatus>  m_TableStatusMap;
    
    vector<Broadcast>   m_BroadcastList;
    bool                m_isBroadcasting;
    Label*              m_MoveText;
    Sprite*             m_runbg;
    ClippingNode*       m_cNode;
    cocos2d::Size       m_clippingSize;
    
};

#endif
