#ifndef _GAMESETINGLAYER_H_
#define _GAMESETINGLAYER_H_

#include "GameScene.h"
#include "cocos-ext.h"
USING_NS_CC_EXT;

class GameSetingLayer : public GameLayer
{
public:
	virtual bool init();  
	static GameSetingLayer *create(GameScene *pGameScene);
	virtual ~GameSetingLayer();
	GameSetingLayer(GameScene *pGameScene);
	GameSetingLayer(const GameSetingLayer&);
	GameSetingLayer& operator = (const GameSetingLayer&);

private:
	virtual void onEnter();
	virtual void onExit();
	virtual bool onTouchBegan(Touch *pTouch, Event *pEvent){return true;}
	virtual void onTouchMoved(Touch *pTouch, Event *pEvent){}
	virtual void onTouchEnded(Touch *pTouch, Event *pEvent){}
	virtual void onTouchCancelled(Touch *pTouch, Event *pEvent){}
	// TextField 触发
	virtual bool onTextFieldAttachWithIME(TextFieldTTF*pSender){return false;}
	virtual bool onTextFieldDetachWithIME(TextFieldTTF*pSender){return false;}
	virtual bool onTextFieldInsertText(TextFieldTTF *pSender, const char *text, int nLen){return true;}
	virtual bool onTextFieldDeleteBackward(TextFieldTTF *pSender, const char *delText, int nLen){return true;}

	// IME 触发
	virtual void keyboardWillShow(IMEKeyboardNotificationInfo &info){}
	virtual void keyboardDidShow(IMEKeyboardNotificationInfo &info){}
	virtual void keyboardWillHide(IMEKeyboardNotificationInfo &info){}
	virtual void keyboardDidHide(IMEKeyboardNotificationInfo &info){}

	// 响应手机快捷键

	Label * createLabel(const char *szText ,const Vec2 &p, int fontSize ,Color3B color, bool bChinese =false);
	Label * createLabel(const std::string szText ,const Vec2 &p, int fontSize ,Color3B color, bool bChinese = false);

	void callbackBt(Ref *pSender );
    void valueChanged(Ref *sender, Control::EventType event);

	Menu* CreateButton(std::string szBtName ,const Vec2 &p , int tag );

public:
	void OnRemove();
	void actionShow();
	void actionMin();
};
#endif