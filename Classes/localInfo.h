//
//  systemInfo.h
//  game998
//
//  Created by maxwell on 16/9/2.
//
//

#ifndef LocalInfo_h
#define LocalInfo_h

#include <stdio.h>
#include <string>
typedef enum
{
    
    NetWorkTypeNone=0,
    
    NetWorkType2G=1,
    
    NetWorkType3G=2,
    
    NetWorkType4G=3,
    
    NetWorkTypeWiFI=5,
    
}NetWorkType;

class LocalInfo
{
public:
    static NetWorkType getCurNetInfo();
    static float getCurPowerInfo();
    static std::string getClientVersion();
    static std::string getMobileBrand();
    static std::string getMobileKind();
    static std::string getMobileSystemVersion();
};
#endif /* localInfo_h */
