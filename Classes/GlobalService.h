﻿#ifndef GLOBAL_SERVICE_HEAD_FILE
#define GLOBAL_SERVICE_HEAD_FILE

#pragma once

#pragma pack(1)


//////////////////////////////////////////////////////////////////////////

//数据库名字
const TCHAR szGameUserDB[]=TEXT("QPGameUserDB");						//用户数据库
const TCHAR	szTreasureDB[]=TEXT("QPTreasureDB");						//财富数据库
const TCHAR	szGameScoreDB[]=TEXT("QPGameScoreDB");						//积分数据库
const TCHAR szServerInfoDB[]=TEXT("QPServerInfoDB");					//房间数据库

//////////////////////////////////////////////////////////////////////////

//数据库信息
struct tagDataBaseInfo
{
	WORD							wDataBasePort;						//数据库端口
	IosDword							dwDataBaseAddr;						//数据库地址
	TCHAR							szDataBaseUser[32];					//数据库用户
	TCHAR							szDataBasePass[32];					//数据库密码
	TCHAR							szDataBaseName[32];					//数据库名字
};

//////////////////////////////////////////////////////////////////////////

//游戏服务属性
struct tagGameServiceAttrib
{
	WORD							wKindID;							//名称号码
	WORD							wChairCount;						//椅子数目
	BYTE							cbJoinInGame;						//游戏加入
	TCHAR							szDataBaseName[32];					//游戏库名
	TCHAR							szDescription[128];					//模块描述
	TCHAR							szKindName[KIND_LEN];				//类型名字
	TCHAR							szServerModuleName[MODULE_LEN];		//模块名字
	TCHAR							szClientModuleName[MODULE_LEN];		//模块名字
};

//游戏服务配置
struct tagGameServiceOption
{
	//房间属性
	WORD							wKindID;							//类型标识
	WORD							wSortID;							//排序标识
	WORD							wNodeID;							//站点号码
	WORD							wServerID;							//房间号码
	WORD							wTableCount;						//桌子数目
	WORD							wServerPort;						//房间端口
	WORD							wServerType;						//游戏类型
	WORD							wMaxConnect;						//最大连接
	IosDword							dwVideoServer;						//视频地址
	IosDword							dwDataBaseAddr;						//数据地址
	TCHAR							szServerAddr[SERVER_LEN];			//房间地址
	TCHAR							szDataBaseName[SERVER_LEN];			//数据名字
	TCHAR							szGameRoomName[SERVER_LEN];			//房间名称


	//积分限制
	WORD							wRevenue;							//游戏税收
	LONGLONG						lCellScore;							//单位积分
	LONGLONG						lLessScore;							//最低积分
	LONGLONG						lMaxScore;							//最高积分
	LONGLONG						lRestrictScore;						//限制积分

	//比赛配置
	int							lMatchDraw;							//比赛局数
	BYTE							cbControlStart;						//控制开始

	//扩展配置
	BYTE							cbHideUserInfo;						//隐藏信息
	BYTE							cbUnLookOnTag;						//限制旁观
	BYTE							cbUnSameIPPlay;						//限制同IP

	TCHAR							szKindName[KIND_LEN];				//类型名字
	WORD							wRoomType;							//房间类型
	WORD							wCircuitType;						//线路类型

	//服务器时间配置
	WORD							wOpenTime;							//开启时间
	WORD							wCloseTime;							//关闭时间

	WORD							wStartTableID;						//桌号的高位编号
	LONGLONG						lMinExchange;						//最少兑换
	LONGLONG						lMaxExchange;						//最大兑换
	LONGLONG						lMinAddChip;						//最少下注
	LONGLONG						lMaxAddChip;						//最大下注
};

//最少连接数
#define	LESS_CONNECT_COUNT			(0L)								//最少连接
//////////////////////////////////////////////////////////////////////////
#pragma pack()

#endif
