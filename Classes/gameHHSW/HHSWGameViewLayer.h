#ifndef _HHSWGAME_VIEW_LAYER_H_
#define _HHSWGAME_VIEW_LAYER_H_

#include "GameScene.h"
#include "FrameGameView.h"
#include "HHSW_CMD.h"
#include "HHSWGameLogic.h"
#include "ui/UIListView.h"

class CTimeTaskLayer;
class HHSWGameViewLayer : public IGameView
{
	struct tagApplyUser
	{
		tagApplyUser()
		{
			ZeroMemory(strUserName,128);
            wGender = 0;
			lUserScore = 0;
			bCompetition = false;
		}
		void Clear()
		{
			ZeroMemory(strUserName,128);
            wGender = 0;
			lUserScore = 0;
			bCompetition = false;
		}
		//玩家信息
		char							strUserName[128];						//玩家帐号
        BYTE                            wGender;
		LONGLONG						lUserScore;							//玩家金币
		bool							bCompetition;
	};

public:
	static HHSWGameViewLayer *create(GameScene *pGameScene);
	virtual ~HHSWGameViewLayer();
	HHSWGameViewLayer(GameScene *pGameScene);
	HHSWGameViewLayer(const HHSWGameViewLayer&);
	HHSWGameViewLayer& operator = (const HHSWGameViewLayer&);

	virtual bool init(); 
	virtual void onEnter();
	virtual void onExit();

	virtual bool onTouchBegan(Touch *pTouch, Event *pEvent);
	virtual void onTouchMoved(Touch *pTouch, Event *pEvent){};
	virtual void oinTouchEnded(Touch *pTouch, Event *pEvent){};

	virtual bool onTextFieldAttachWithIME(TextFieldTTF*pSender){return false;}
	virtual bool onTextFieldDetachWithIME(TextFieldTTF*pSender){return false;}
	virtual bool onTextFieldInsertText(TextFieldTTF *pSender, const char *text, int nLen){return true;}
	virtual bool onTextFieldDeleteBackward(TextFieldTTF *pSender, const char *delText, int nLen){return true;}

	virtual void keyboardWillShow(IMEKeyboardNotificationInfo &info){}
	virtual void keyboardDidShow(IMEKeyboardNotificationInfo &info){}
	virtual void keyboardWillHide(IMEKeyboardNotificationInfo &info){}
	virtual void keyboardDidHide(IMEKeyboardNotificationInfo &info){}
	virtual void DrawUserScore(){}	//绘制玩家分数
	virtual void GameEnd(){};
	virtual void ShowAddScoreBtn(bool bShow=true){};
	virtual void UpdateDrawUserScore(){};		//更新显示的游戏币数量

	void callbackBt( Ref *pSender );
//	virtual void keyMenuClicked(){}
	virtual void backLoginView(Ref *pSender);
	string  AddCommaToNum(LONG Num);

	//初始化
	void InitGame();
	void AddPlayerInfo();
	void AddButton();
    Menu* CreateButton(std::string szBtName ,const Vec2 &p , int tag);

	// 按钮事件管理
	void DialogConfirm(Ref *pSender);
	void DialogCancel(Ref *pSender);
	string GetCardStringName(BYTE card);

	//网络接口
public:
	void OnEventUserLeave( tagUserData * pUserData, WORD wChairID, bool bLookonUser );
	//用户进入
	virtual void OnEventUserEnter(tagUserData * pUserData, WORD wChairID, bool bLookonUser);
	//用户积分
	virtual void OnEventUserScore(tagUserData * pUserData, WORD wChairID, bool bLookonUser);
	//用户状态
	virtual void OnEventUserStatus(tagUserData * pUserData, WORD wChairID, bool bLookonUser);
	//游戏消息
	virtual bool OnGameMessage(WORD wSubCmdID, const void * pBuffer, WORD wDataSize);
	//场景消息
	virtual bool OnGameSceneMessage(BYTE cbGameStatus, const void * pBuffer, WORD wDataSize);

	virtual void OnReconnectAction(){};

	void OnQuit();

public:
	bool OnSubGameFree(const void * pBuffer, WORD wDataSize);
	bool OnSubGameStart(const void * pBuffer, WORD wDataSize);
	bool OnSubPlaceJetton(const void * pBuffer, WORD wDataSize);
	bool OnSubGameEnd(const void * pBuffer, WORD wDataSize);
    bool OnSubHistoryGameRecord(const void * pBuffer, WORD wDataSize);
	bool OnSubUserApplyBanker(const void * pBuffer, WORD wDataSize);
	bool OnSubUserCancelBanker(const void * pBuffer, WORD wDataSize);
	bool OnSubChangeBanker(const void * pBuffer, WORD wDataSize);

public:
	void UpdateTime(float fp);
	void StopTime();
	void StartTime(int _type, int _time);
	void SetBankerInfo(WORD wBanker,LONGLONG lScore);
	void PlaceUserJetton(BYTE cbViewIndex, LONGLONG lScoreCount);
	void SetMePlaceJetton(BYTE cbViewIndex, LONGLONG score);
	const char* GetGoldName(LONGLONG lScoreCount);
	void Select0();
	void Select100();
	void Select1000();
	void Select1w();
	void Select10w();
	void Select100w();
	void Select500w();
	void Select1000w();
	void GetAllWinArea(BYTE bcWinArea[],BYTE bcAreaCount,BYTE InArea);
	void SetArea(BYTE cbViewIndex,LONGLONG score);	//设置当前可下注数

	void RunAnim();
	void UpdateStartSlow(float fp);		 //开始慢
	void UpdateStartAddSpeed(float fp);	 //开始加速
	void UpdateStartAddSpeed1(float fp); //开始加速1
	void UpdateEndSlow(float fp);	//减速
	void UpdateEndSlow1(float fp);	//减速
	void UpdateStartAnim();
	void UpdateEndAnim();
	void UpdateRunAnim();	//更新动画
	void UpdateEnd(float fp);
	void DrawEndScore();
	void DrawRecord(bool IsBlink=false);
	void CloseBankList();
	void OpenBankList();
	void LoadSound();
	void PlayGameMusicBK();
	void PlayAnimSound(int _index);
	bool OnApplyBanker(int wParam);
	bool IsLookonMode();
	void DeleteBankList(tagApplyUser ApplyUser);
	void DrawBankList();
	void UpdateButtonContron();
	LONGLONG GetUserMaxJetton(BYTE cbJettonArea);
    void LayerMoveCallBack();
protected:
	HHSWGameLogic		m_GameLogic;
    ui::ListView*       m_listView;
	
    //图片
	Sprite*			m_BackSpr;
	Sprite*			m_ClockSpr;
	Sprite*			m_LightSpr[3];
	Sprite*			m_BankListBackSpr;
    
    Vec2            m_LightPos[32];
	//按钮
	Menu*				m_bn100;
	Menu*				m_bn1000;
	Menu*				m_bn1W;
	Menu*				m_bn10W;
	Menu*				m_bn100W;
	Menu*				m_bn500W;
	Menu*				m_bn1000W;
	Menu*				m_btApplyBanker;
	Menu*				m_btCancelBanker;
	Menu*				m_BankUp;
	Menu*				m_BankDown;
    Menu*				m_BtnGetScoreBtn;

	//标识
	int					m_TimeType_Tga;
	int					m_NickName_Tga;
	int					m_Glod_Tga;
	int					m_WinGlod_Tga;
	int					m_Head_Tga;
	int					m_BankNickName_Tga;
	int					m_BankGold_Tga;
	int					m_BankZhangJi_Tga;
	int					m_BankJuShu_Tga;
	int					m_TalbeGold_Tga[8];
	int					m_AllBetScore_Tga[8];
	int					m_AllMeBetScore_Tga[8];
	int					m_AllLimBetScore_Tga[8];
	int					m_BtnAnim_Tga;
	int					m_GameEndBack_Tga;
	int					m_RecordBlink_Tga;
	int					m_RecordSpr_Tga;

	//坐标
	Vec2				m_AllBetNumPos[8];
	Vec2				m_MeBetNumPos[8];
	Vec2				m_LimBetNumPos[8];
	Vec2				m_JettonPos[8];
	Vec2				m_BankListPos[4];


	//变量
	int					m_StartTime;
	LONGLONG			m_lMeStatisticScore;
	LONGLONG			m_lMeMaxScore;
	int					m_wBankerUser;
	LONGLONG			m_lBankerScore;
	int					m_wBankerTime;
	LONGLONG			m_lBankerWinScore;
	LONGLONG			m_lAllJettonScore[HHSW_AREA_COUNT+1];
	LONGLONG			m_lUserJettonScore[HHSW_AREA_COUNT+1];
	Rect				m_RectArea[8];
	LONGLONG			m_CurSelectGold;
    int                 m_cbResultIndex;    //开奖动物
    int                 m_cbSlowIndex;      //减速索引
	int					m_cbEndIndex;		//结束索引
	int					m_CurAnimIndex;		//当前动物
	int					m_CurTurn;			//当前是第几圈
	int					m_cbTotalIndex;     //当前最大索引
	LONGLONG			m_lPlayAllScore;
	LONGLONG			m_lBankerCurGameScore;
	LONGLONG			m_lPlayReturnScore;
	LONGLONG            m_lBankerRevenue;
    LONGLONG            m_lUserCurScore;
	vector<int>			m_RecordVec;
	bool				m_OpenBankList;
	bool				m_bMeApplyBanker;
	LONGLONG			m_lApplyBankerCondition;			//申请条件
	bool				m_LookMode;
	vector<tagApplyUser>m_BankUserList;
	int					m_BankListBeginIndex;
	LONGLONG			m_lAreaLimitScore;
    bool                m_IsLayerMove;
};

#endif
