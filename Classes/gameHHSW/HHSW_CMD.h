#ifndef CMD_HHSW_HEAD_FILE
#define CMD_HHSW_HEAD_FILE
#pragma pack(1)
//公共宏定义

#define HHSW_KIND_ID						109								//游戏 I D
#define HHSW_GAME_PLAYER					150								//游戏人数
#define GAME_NAME					TEXT("虎虎生威")                         //游戏名字

//版本信息
#define VERSION_SERVER			    PROCESS_VERSION(6,0,3)                  //程序版本
#define VERSION_CLIENT				PROCESS_VERSION(6,0,3)                  //程序版本
//状态定义
#define	HHSW_GS_PLACE_JETTON		GS_PLAYING                              //下注状态
#define	HHSW_GS_GAME_END			GS_PLAYING+1                            //结束状态
#define	HHSW_GS_MOVECARD_END		GS_PLAYING+2                        //结束状态

//区域索引
#define HHSW_ID_TIAN_MEN				1                                   //顺门
#define HHSW_ID_DI_MEN					2									//左边角
#define HHSW_ID_XUAN_MEN				3                                   //桥
#define HHSW_ID_HUANG_MEN				4									//对门

//玩家索引
#define HHSW_BANKER_INDEX				0									//庄家索引
#define HHSW_SHUN_MEN_INDEX				1									//顺门索引
#define HHSW_DUI_MEN_INDEX				2									//对门索引
#define HHSW_DAO_MEN_INDEX				3									//倒门索引
#define HHSW_HUAN_MEN_INDEX				4									//倒门索引

#define HHSW_AREA_COUNT					8									//区域数目
#define HHSW_BANKER_LIST_COUNT			3									//庄家列表数
//赔率定义
#define RATE_TWO_PAIR                   12									//对子赔率

//记录信息
struct HHSWtagServerGameRecord
{
	BYTE							bWinMen[HHSW_AREA_COUNT+1];				//顺门胜利
};

struct CMD_S_HHSW_ScoreHistory
{
    BYTE              cbScoreHistroy[50];          //历史信息
};

//////////////////////////////////////////////////////////////////////////
//服务器命令结构

#define SUB_S_HHSW_GAME_FREE				99									//游戏空闲
#define SUB_S_HHSW_GAME_START               100									//游戏开始
#define SUB_S_HHSW_PLACE_JETTON             101									//用户下注
#define SUB_S_HHSW_GAME_END                 102									//游戏结束
#define SUB_S_HHSW_APPLY_BANKER             103									//申请庄家
#define SUB_S_HHSW_CHANGE_BANKER			104									//切换庄家
#define SUB_S_HHSW_CHANGE_USER_SCORE		105									//更新积分
#define SUB_S_HHSW_PLACE_JETTON_FAIL		107									//下注失败
#define SUB_S_HHSW_CANCEL_BANKER			108									//取消申请
#define SUB_S_HHSW_CHECK_IMAGE              109									//历史记录
#define SUB_S_HHSW_ADMIN_COMMDN             110									//系统控制
#define SUB_S_HHSW_ADMIN_STORAGE            211                                 //库存通知
#define SUB_S_HHSW_ROBOT_BANKER             112                                 //机器人强制上庄

//失败结构
struct CMD_S_HHSW_PlaceJettonFail
{
	WORD							wPlaceUser;							//下注玩家
	BYTE							lJettonArea;						//下注区域
	LONGLONG						lPlaceScore;						//当前下注
};

//更新积分
struct CMD_S_HHSW_ChangeUserScore
{
	WORD							wChairID;							//椅子号码
	LONGLONG						lScore;								//玩家积分

	//庄家信息
	WORD							wCurrentBankerChairID;				//当前庄家
	BYTE							cbBankerTime;						//庄家局数
	LONGLONG						lCurrentBankerScore;				//庄家分数
};

//申请庄家
struct CMD_S_HHSW_ApplyBanker
{
	WORD							wApplyUser;							//申请玩家
    int								nRobBanker;							//抢庄的人
};

//取消申请
struct CMD_S_HHSW_CancelBanker
{
    CHAR							szCancelUser[32];					//取消玩家
    WORD							wCancelUser;						//取消玩家
    int                             bisRobBanker;						//是否是抢庄玩家
    int								nRobBanker;							//抢庄的人
};

//切换庄家
struct CMD_S_HHSW_ChangeBanker
{
	WORD							wBankerUser;						//当庄玩家
	LONGLONG						lBankerScore;						//庄家金币
    int								nRobBanker;							//抢庄的人
};

//游戏状态
struct CMD_S_HHSW_StatusFree
{
	//全局信息
	BYTE							cbTimeLeave;						//剩余时间

	//玩家信息
	LONGLONG						lUserMaxScore;							//玩家金币

	//庄家信息
	WORD							wBankerUser;						//当前庄家
	WORD							cbBankerTime;						//庄家局数
	LONGLONG						lBankerWinScore;					//庄家成绩
	LONGLONG						lBankerScore;						//庄家分数
	bool							bEnableSysBanker;					//系统做庄

	//控制信息
	LONGLONG						lApplyBankerCondition;				//申请条件
	LONGLONG						lAreaLimitScore;					//区域限制
	int								CheckImage;
};

//游戏状态
struct CMD_S_HHSW_StatusPlay
{
	//全局下注
	LONGLONG						lAllJettonScore[HHSW_AREA_COUNT];		//全体总注

	//玩家下注
	LONGLONG						lUserJettonScore[HHSW_AREA_COUNT];		//个人总注

	//玩家积分
	LONGLONG						lUserMaxScore;						//最大下注							

	//控制信息
	LONGLONG						lApplyBankerCondition;				//申请条件
	LONGLONG						lAreaLimitScore;					//区域限制

    //索引信息
    BYTE							cbMoveEndIndex;						//移动减速索引
    BYTE							cbResultIndex;						//结果索引
    BYTE							cbEndIndex;							//停止索引

	//庄家信息
	WORD							wBankerUser;						//当前庄家
	WORD							cbBankerTime;						//庄家局数
	LONGLONG						lBankerWinScore;					//庄家赢分
	LONGLONG						lBankerScore;						//庄家分数
	bool							bEnableSysBanker;					//系统做庄

	//结束信息
	LONGLONG						lEndBankerScore;					//庄家成绩
	LONGLONG						lEndUserScore;						//玩家成绩
	LONGLONG						lEndUserReturnScore;				//返回积分
	LONGLONG						lEndRevenue;						//游戏税收

	//全局信息
	BYTE							cbTimeLeave;						//剩余时间
	BYTE							cbGameStatus;						//游戏状态
    
    //玩家状态
    BYTE							cbUserStatus;						//用户状态
    int								nRobBanker;							//抢庄的个数

};

//游戏空闲
struct CMD_S_HHSW_GameFree
{
	BYTE							cbTimeLeave;						//剩余时间
};

//游戏开始
struct CMD_S_HHSW_GameStart
{
	WORD							wBankerUser;						//庄家位置
	LONGLONG						lBankerScore;						//庄家金币
	LONGLONG						lUserMaxScore;						//我的金币
	BYTE							cbTimeLeave;						//剩余时间
};

//用户下注
struct CMD_S_HHSW_PlaceJetton
{
	WORD							wChairID;							//用户位置
	BYTE							cbJettonArea;						//筹码区域
	LONGLONG						lJettonScore;						//加注数目
};

//游戏结束
struct CMD_S_HHSW_GameEnd
{
	//下局信息
	BYTE							cbTimeLeave;						//剩余时间

    //索引信息
    BYTE							cbMoveEndIndex;						//移动减速索引
    BYTE							cbResultIndex;						//结果索引
    BYTE							cbEndIndex;							//停止索引
    
	//庄家信息
	LONGLONG						lBankerScore;						//庄家成绩
	LONGLONG						lBankerTotallScore;					//庄家成绩
	INT								nBankerTime;						//做庄次数

	//玩家成绩
	LONGLONG						lUserScore;							//玩家成绩
	LONGLONG						lUserReturnScore;					//返回积分

	//全局信息
	LONGLONG						lRevenue;							//游戏税收
    
    //玩家下注信息
    LONGLONG							lUserBigBCScore;					//自己买大奔驰总数
    LONGLONG							lUserSmallBCScore;					//小奔驰
    LONGLONG							lUserBigBMScore;					//大宝马
    LONGLONG							lUserSmallBMScore;					//小宝马
    LONGLONG							lUserBigADScore;					//大奥迪
    LONGLONG							lUserSmallADScore;					//小奥迪
    LONGLONG							lUserBigDZScore;					//大大众
    LONGLONG							lUserSmallDZScore;					//小大众
};

//更新库存
struct CMD_S_HHSW_SystemStorage
{
	LONGLONG						lStorage;						//新库存值
	LONGLONG						lStorageDeduct;					//库存衰减
};


//////////////////////////////////////////////////////////////////////////
//客户端命令结构

#define SUB_C_HHSW_PLACE_JETTON				1									//用户下注
#define SUB_C_HHSW_APPLY_BANKER				2									//申请庄家
#define SUB_C_HHSW_CANCEL_BANKER			3									//取消申请
#define SUB_C_HHSW_CONTINUE_CARD			4									//继续发牌
#define SUB_C_HHSW_CHECK_IMAGE				5									//继续发牌
#define SUB_C_HHSW_ADMIN_COMMDN				6									//系统控制
#define SUB_C_HHSW_ADMIN_RESET_STORAGE		8                                  //清除库存
//用户下注
struct CMD_C_HHSW_PlaceJetton
{
	BYTE							cbJettonArea;						//筹码区域
	LONGLONG						lJettonScore;						//加注数目
};

struct CMD_C_HHSW_CheckImage
{
	int Index;
};

//////////////////////////////////////////////////////////////////////////

//服务器控制返回
#define	 S_CR_FAILURE				0		//失败
#define  S_CR_UPDATE_SUCCES			1		//更新成功
#define	 S_CR_SET_SUCCESS			2		//设置成功
#define  S_CR_CANCEL_SUCCESS		3		//取消成功
struct CMD_S_HHSW_ControlReturns
{
	BYTE cbReturnsType;				//回复类型
	BYTE cbControlArea;				//控制区域
	BYTE cbControlTimes;			//控制次数
};


//客户端控制申请
#define  C_CA_UPDATE				1		//更新
#define	 C_CA_SET					2		//设置
#define  C_CA_CANCELS				3		//取消
struct CMD_C_HHSW_ControlApplication
{
	BYTE cbControlAppType;			//申请类型
	BYTE cbControlArea;				//控制区域
	BYTE cbControlTimes;			//控制次数
};
//更新库存
struct CMD_C_HHSW_UpdateStorage
{
	LONGLONG						lStorage;						//新库存值
	LONGLONG						lStorageDeduct;					//库存衰减
};
//////////////////////////////////////////////////////////////////////////
#pragma pack()
#endif
