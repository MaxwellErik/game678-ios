#ifndef HHSW_GAME_LOGIC_HEAD_FILE
#define HHSW_GAME_LOGIC_HEAD_FILE
#include "GameLayer.h"
#include <vector>

enum emCardType_HHSW
{
	HHSW_CT_SPECIAL_DA_BEN = 1,
	HHSW_CT_SPECIAL_XIAO_BEN = 2,
	HHSW_CT_SPECIAL_DA_BAO_MA = 3,
	HHSW_CT_SPECIAL_XIAO_BAO_MA = 4,
	HHSW_CT_SPECIAL_DA_OUDI = 5	,	                	//特殊类型
	HHSW_CT_SPECIAL_XIAO_OUDI = 6	,						//特殊类型
	HHSW_CT_SPECIAL_DA_DAZONG = 7	,						//特殊类型
	HHSW_CT_SPECIAL_XIAO_DAZONG = 8	,						//特殊类型

};
//数值掩码
#define	LOGIC_MASK_COLOR			0xF0								//花色掩码
#define	LOGIC_MASK_VALUE			0x0F								//数值掩码

//排序类型
#define	ST_VALUE					1									//数值排序
#define	ST_NEW					    3									//数值排序
#define	ST_LOGIC					2									//逻辑排序

//扑克数目
#define CARD_COUNT					32									//扑克数目
//////////////////////////////////////////////////////////////////////////

//游戏逻辑
class HHSWGameLogic
{
	//变量定义
private:
	static const BYTE				m_cbCardListData[CARD_COUNT*2];		//扑克定义

public:
	HHSWGameLogic();
	virtual ~HHSWGameLogic();

	//类型函数
public:
	//获取数值
	BYTE GetCardValue(BYTE cbCardData) 
	{ 
		return cbCardData&LOGIC_MASK_VALUE; 
	}
	//获取花色
	BYTE GetCardColor(BYTE cbCardData)
	{
		return (cbCardData&LOGIC_MASK_COLOR)>>4;
	}

	//控制函数
public:
	//混乱扑克
	void RandCardList(BYTE cbCardBuffer[], BYTE cbBufferCount);
	//排列扑克
	void SortCardList(BYTE cbCardData[], BYTE cbCardCount, BYTE cbSortType);
	//逻辑函数
public:
	//获取牌点
	BYTE GetCardListPip(const BYTE cbCardData[], BYTE cbCardCount);
	//获取牌型
	BYTE GetCardType(const BYTE cbCardData[], BYTE cbCardCount,BYTE *bcOutCadData = NULL);
	//大小比较
	int CompareCard(const BYTE cbFirstCardData[], BYTE cbFirstCardCount,const BYTE cbNextCardData[], BYTE cbNextCardCount,BYTE &Multiple);
	//逻辑大小
	BYTE GetCardLogicValue(BYTE cbCardData);

	BYTE GetCardNewValue(BYTE cbCardData);
};

//////////////////////////////////////////////////////////////////////////

#endif
