#include "HHSWGameViewLayer.h"
#include "LobbyLayer.h"
#include "ClientSocketSink.h"
#include "HXmlParse.h"
#include "LocalDataUtil.h"
#include "JniSink.h"
#include "VisibleRect.h"
#include "HttpConstant.h"
#include "Screen.h"
#include "LobbySocketSink.h"
#include "LoginLayer.h"
#include "GameLayerMove.h"
#include "Convert.h"

#define Btn_Ready						201
#define Btn_BackToLobby					202
#define Btn_Seting						203
#define Btn_GetScoreBtn					204
#define Btn_100							205
#define Btn_1000						206
#define Btn_1W							207
#define Btn_10W							208
#define Btn_100w						209
#define Btn_500W						210
#define Btn_1000w						211
#define Btn_BankBtn						212
#define Btn_SBankOK						213
#define Btn_SBankcancle					214
#define btn_SBankdown					215
#define Btn_Uplist						216
#define Btn_Downlist					217

//时间标识
#define HHSW_IDI_FREE					99									//空闲时间
#define HHSW_IDI_PLACE_JETTON			100									//下注时间
#define HHSW_IDI_DISPATCH_CARD			301									//发牌时间
#define HHSW_IDI_OPEN_CARD				302								    //发牌时间
#define JETTON_COUNT					6									//筹码数目




HHSWGameViewLayer::HHSWGameViewLayer(GameScene *pGameScene):IGameView(pGameScene)
{
	m_lMeStatisticScore = 0;
	m_lMeMaxScore = 0;
	m_lBankerScore = 0;
	m_wBankerUser = 0;
	m_wBankerTime = 0;
	m_lBankerWinScore = 0;
	m_CurSelectGold = 0;
	m_CurAnimIndex = 0;
	m_lPlayAllScore = 0;
	m_lBankerCurGameScore = 0;
	m_lPlayReturnScore = 0;
	m_lBankerRevenue = 0;
	m_OpenBankList = false;
	m_bMeApplyBanker = false;
	m_lApplyBankerCondition=0;	
	m_LookMode = false;
	m_BankListBeginIndex = 0;
	m_lAreaLimitScore = 0;
	for (int i = 0; i < 9; i++)
	{
		m_lAllJettonScore[i] = 0;
		m_lUserJettonScore[i] = 0;
	}
	SetKindId(HHSW_KIND_ID);
}

HHSWGameViewLayer::~HHSWGameViewLayer() 
{

}

HHSWGameViewLayer *HHSWGameViewLayer::create(GameScene *pGameScene)
{
    HHSWGameViewLayer *pLayer = new HHSWGameViewLayer(pGameScene);
    if(pLayer && pLayer->init())
    {
        pLayer->autorelease();
        return pLayer;
    }
    else
    {
        CC_SAFE_DELETE(pLayer);
        return NULL;
    }
}

void HHSWGameViewLayer::onEnter ()
{	

}

void HHSWGameViewLayer::onExit()
{
	Tools::removeSpriteFrameCache("hhsw/HHSWRes.plist");
	IGameView::onExit();
}

bool HHSWGameViewLayer::init()
{
	if ( !Layer::init() )
	{
		return false;
	}
	setLocalZOrder(3);

	Tools::addSpriteFrame("hhsw/HHSWRes.plist");
	IGameView::onEnter();
	JniSink::share()->setIGameView(this);
	setGameStatus(GS_FREE);

	InitGame();
	AddButton();

	AddPlayerInfo();

	setTouchEnabled(true);
	auto listener = EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(true);
	listener->onTouchBegan = CC_CALLBACK_2(HHSWGameViewLayer::onTouchBegan, this);
	listener->onTouchMoved = CC_CALLBACK_2(HHSWGameViewLayer::onTouchMoved, this);
	listener->onTouchEnded = CC_CALLBACK_2(HHSWGameViewLayer::onTouchEnded, this);
	_eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);

	return true;
}


void HHSWGameViewLayer::InitGame()
{
	//背景图
	m_BackSpr = Sprite::create("hhsw/game_back.jpg");
	m_BackSpr->setPosition(_STANDARD_SCREEN_CENTER_IN_PIXELS_);
	addChild(m_BackSpr);

	//时间
	m_ClockSpr = Sprite::createWithSpriteFrameName("bg_time.png");
	m_ClockSpr->setPosition(Vec2(1100, 880));
	addChild(m_ClockSpr);
    
	//上庄列表
	m_BankListBackSpr = Sprite::createWithSpriteFrameName("bg_tongyongkuang3.png");
	m_BankListBackSpr->setPosition(Vec2(_STANDARD_SCREEN_CENTER_.x, 1500));
	addChild(m_BankListBackSpr,10000);

    Sprite* pBankTitle = Sprite::createWithSpriteFrameName("shangzhuangliebiao.png");
    pBankTitle->setScale(0.8f);
    pBankTitle->setPosition(Vec2(519,480));
    m_BankListBackSpr->addChild(pBankTitle);
    
    m_listView = ui::ListView::create();
    m_listView->setContentSize(Size(920, 380));
    m_listView->setDirection(ui::ScrollView::Direction::VERTICAL);
    m_listView->setPosition(Vec2(50,30));
    m_BankListBackSpr->addChild(m_listView);
    
    Sprite* bank = Sprite::createWithSpriteFrameName("label_bank.png");
    bank->setPosition(Vec2(500, 780));
    addChild(bank);
 
    Sprite* chipB = Sprite::createWithSpriteFrameName("choumadi.png");
    chipB->setPosition(Vec2(1215, 68));
    addChild(chipB);

    Sprite* chipT = Sprite::createWithSpriteFrameName("choumashang.png");
    chipT->setPosition(Vec2(1215, 15));
    addChild(chipT,300);

    for (int i = 0; i < 4; i++)
    {
        Sprite* labelBg = Sprite::createWithSpriteFrameName("bg_chip.png");
        labelBg->setPosition(Vec2(500, 715 - i * 110));
        addChild(labelBg);
    }
    
    m_IsLayerMove = false;
	//标识
	m_TimeType_Tga			= 10;
	m_NickName_Tga			= 11;
	m_Glod_Tga				= 12;
	m_WinGlod_Tga			= 13;
	m_Head_Tga				= 14;
	m_BankNickName_Tga		= 15;
	m_BankGold_Tga			= 16;
	m_BankZhangJi_Tga		= 17;
	m_BankJuShu_Tga			= 18;
	for (int i = 0; i < 8; i++)
	{
		m_TalbeGold_Tga[i] = 19+i;
		m_AllBetScore_Tga[i] = 27+i;
		m_AllMeBetScore_Tga[i] = 35+i;
		m_AllLimBetScore_Tga[i] = 43+i;
	}
	m_BtnAnim_Tga			= 51;
	m_GameEndBack_Tga		= 52;
	m_RecordBlink_Tga		= 53;
    m_RecordSpr_Tga         = 54;
    
	m_RectArea[0].setRect(640, 610, 225, 180);
	m_RectArea[2].setRect(880, 610, 225, 180);
	m_RectArea[4].setRect(1120, 610, 225, 180);
	m_RectArea[6].setRect(1360, 610, 225, 180);
	m_RectArea[1].setRect(640, 350, 225, 180);
	m_RectArea[3].setRect(880, 350, 225, 180);
	m_RectArea[5].setRect(1120, 350, 225, 180);
	m_RectArea[7].setRect(1360, 350, 225, 180);

	m_JettonPos[0] = Vec2(760,770);
	m_JettonPos[2] = Vec2(1000,770);
	m_JettonPos[4] = Vec2(1240,770);
	m_JettonPos[6] = Vec2(1480,770);
	m_JettonPos[1] = Vec2(760,510);
	m_JettonPos[3] = Vec2(1000,510);
	m_JettonPos[5] = Vec2(1240,510);
	m_JettonPos[7] = Vec2(1480,510);

	m_AllBetNumPos[0] = Vec2(760,625);
	m_AllBetNumPos[2] = Vec2(1000,625);
	m_AllBetNumPos[4] = Vec2(1240,625);
	m_AllBetNumPos[6] = Vec2(1480,625);
	m_AllBetNumPos[1] = Vec2(760,365);
	m_AllBetNumPos[3] = Vec2(1000,365);
	m_AllBetNumPos[5] = Vec2(1240,365);
	m_AllBetNumPos[7] = Vec2(1480,365);

	m_LimBetNumPos[0] = Vec2(535,425);
	m_LimBetNumPos[2] = Vec2(687,425);
	m_LimBetNumPos[4] = Vec2(836,425);
	m_LimBetNumPos[6] = Vec2(990,425);
	m_LimBetNumPos[1] = Vec2(535,257);
	m_LimBetNumPos[3] = Vec2(687,257);
	m_LimBetNumPos[5] = Vec2(836,257);
	m_LimBetNumPos[7] = Vec2(990,257);

	m_BankListPos[0]   = Vec2(20,200);
	m_BankListPos[1]   = Vec2(20,160);
	m_BankListPos[2]   = Vec2(20,120);
	m_BankListPos[3]   = Vec2(20,80);
    
    for (int i = 0; i < 8; i++)
    {
        char _str[128];
        sprintf(_str,"area_%d.png",i);
        Sprite* temp = Sprite::createWithSpriteFrameName(_str);
        temp->setPosition(Vec2(760 + (i / 2) * 240, 710 - (i % 2) * 260));
        addChild(temp);
    
        
        Sprite* goldBg = Sprite::createWithSpriteFrameName("bg_chip.png");
        goldBg->setPosition(Vec2(760 + (i / 2) * 240, 625 - (i % 2) * 260));
        addChild(goldBg);
    }
    
    for (int i = 0; i < 3; i++)
    {
        char _str[20];
        sprintf(_str,"di%d.png", i);
        m_LightSpr[i] = Sprite::createWithSpriteFrameName(_str);
        m_LightSpr[i]->setVisible(false);
        addChild(m_LightSpr[i],105);
    }
    
	//上横
	for (int i = 0; i < 11; i++)
	{
        m_LightPos[i] = Vec2(280 + i * 140, 980);

        char _str[128];
        sprintf(_str,"hhsw_%d.png",i%8);
        Sprite* temp = Sprite::createWithSpriteFrameName(_str);
        temp->setPosition(m_LightPos[i]);
        addChild(temp, 200);
        
        auto light = Sprite::createWithSpriteFrameName("di0.png");
        light->setPosition(m_LightPos[i]);
        addChild(light,100);
	}

	//右边
	int _count = 1;
	for (int i = 11; i < 17; i++)
	{
        m_LightPos[i] = Vec2(1680 ,980 - _count++ * 120);
        
        char _str[128];
        sprintf(_str,"hhsw_%d.png",i%8);
        Sprite* temp = Sprite::createWithSpriteFrameName(_str);
        temp->setPosition(m_LightPos[i]);
        addChild(temp, 200);
        
        auto light = Sprite::createWithSpriteFrameName("di0.png");
        light->setPosition(m_LightPos[i]);
        addChild(light,100);
    }

	//下面
	_count = 1;
	for (int i = 17; i < 27; i++)
	{
         m_LightPos[i] = Vec2(1680 - _count++ * 140, 260);

        char _str[128];
        sprintf(_str,"hhsw_%d.png",i%8);
        Sprite* temp = Sprite::createWithSpriteFrameName(_str);
        temp->setPosition(m_LightPos[i]);
        addChild(temp, 200);
        
        auto light = Sprite::createWithSpriteFrameName("di0.png");
        light->setPosition(m_LightPos[i]);
        addChild(light,100);
	}

	//左边
	_count = 1;
	for (int i = 27; i < 32; i++)
	{
        m_LightPos[i] = Vec2(280 ,260 + _count++ * 120);

        char _str[128];
        sprintf(_str,"hhsw_%d.png",i%8);
        Sprite* temp = Sprite::createWithSpriteFrameName(_str);
        temp->setPosition(m_LightPos[i]);
        addChild(temp, 200);
        
        auto light = Sprite::createWithSpriteFrameName("di0.png");
        light->setPosition(m_LightPos[i]);
        addChild(light,100);
	}

	LoadSound();
}

void HHSWGameViewLayer::LoadSound()
{
	for (int i = 1; i<= 9; i++)
	{
		SoundUtil::sharedEngine()->preloadEffect("sdb/sound/bk%d");
	}
	SoundUtil::sharedEngine()->preloadEffect("hhsw/sound/ADD_GOLD");
	SoundUtil::sharedEngine()->preloadEffect("hhsw/sound/ADD_GOLD_EX");
	SoundUtil::sharedEngine()->preloadEffect("hhsw/sound/bdog");
	SoundUtil::sharedEngine()->preloadEffect("hhsw/sound/bhorse");
	SoundUtil::sharedEngine()->preloadEffect("hhsw/sound/bsnake");
	SoundUtil::sharedEngine()->preloadEffect("hhsw/sound/btiger");
	SoundUtil::sharedEngine()->preloadEffect("hhsw/sound/END_DRAW");
	SoundUtil::sharedEngine()->preloadEffect("hhsw/sound/END_LOST");
	SoundUtil::sharedEngine()->preloadEffect("hhsw/sound/END_WIN");
	SoundUtil::sharedEngine()->preloadEffect("hhsw/sound/GAME_START");
	//SoundUtil::sharedEngine()->preloadEffect("hhsw/sound/idc_snd_");
	SoundUtil::sharedEngine()->preloadEffect("hhsw/sound/sdog");
	SoundUtil::sharedEngine()->preloadEffect("hhsw/sound/shorse");
	SoundUtil::sharedEngine()->preloadEffect("hhsw/sound/ssnake");
	SoundUtil::sharedEngine()->preloadEffect("hhsw/sound/stiger");
}

void HHSWGameViewLayer::AddButton()
{
	Menu* tempBtn = CreateButton("HHSWReturnback" ,Vec2(90,1020),Btn_BackToLobby);
	addChild(tempBtn , 0 , Btn_BackToLobby);

	tempBtn = CreateButton("HHSWSeting" ,Vec2(1830,1020),Btn_Seting);
	addChild(tempBtn , 0 , Btn_Seting);

	m_bn100 = CreateButton("chip_100" ,Vec2(675 ,60),Btn_100);
	addChild(m_bn100 , 100 , Btn_100);

	m_bn1000 = CreateButton("chip_1000" ,Vec2(855 ,60),Btn_1000);
	addChild(m_bn1000 , 100 , Btn_1000);

	m_bn1W = CreateButton("chip_1w" ,Vec2(1035 ,60),Btn_1W);
	addChild(m_bn1W , 100 , Btn_1W);

	m_bn10W = CreateButton("chip_10w" ,Vec2(1215 ,60),Btn_10W);
	addChild(m_bn10W , 100 , Btn_10W);

	m_bn100W = CreateButton("chip_100w" ,Vec2(1395 ,60),Btn_100w);
	addChild(m_bn100W , 100 , Btn_100w);

	m_bn500W = CreateButton("chip_500w" ,Vec2(1575 ,60),Btn_500W);
	addChild(m_bn500W , 100 , Btn_500W);

	m_bn1000W = CreateButton("chip_1000w" ,Vec2(1755 ,60),Btn_1000w);
	addChild(m_bn1000W , 100 , Btn_1000w);

	tempBtn = CreateButton("banklist" ,Vec2(1840,_STANDARD_SCREEN_CENTER_.y+50),Btn_BankBtn);
	addChild(tempBtn , 0 , Btn_BankBtn);

	m_btApplyBanker = CreateButton("hhswbank_call" ,Vec2(490,860),Btn_SBankOK);	//申请上庄
	addChild(m_btApplyBanker , 0 , Btn_SBankOK);

	m_btCancelBanker = CreateButton("hhswbank_cancle" ,Vec2(490,860),Btn_SBankcancle);	//取消上庄
	addChild(m_btCancelBanker , 0 , Btn_SBankcancle);
	m_btCancelBanker->setVisible(false);
}


// Touch 触发
bool HHSWGameViewLayer::onTouchBegan(Touch *pTouch, Event *pEvent)
{
    if (m_OpenBankList)
    {
        CloseBankList();
        return false;
    }
    
    Vec2 touchLocation = pTouch->getLocation(); // 返回GL坐标
	Vec2 localPos = convertToNodeSpace(touchLocation);
	if (getGameStatus() != HHSW_GS_PLACE_JETTON) return true;
	if (m_CurSelectGold == 0) return true;
	if ( GetMeChairID() == m_wBankerUser ) return true;
	
	int cbJettonArea = 0;
	LONGLONG lJettonScore = m_CurSelectGold;

	for (int i = 0; i < 8; i++)
	{
		if (m_RectArea[i].containsPoint(localPos)) 
		{
			log("x=%f, y= %f",localPos.x,localPos.y);
			SoundUtil::sharedEngine()->playEffect("buttonMusic");
			cbJettonArea = i+1;
			CMD_C_HHSW_PlaceJetton PlaceJetton;
			ZeroMemory(&PlaceJetton,sizeof(PlaceJetton));
			PlaceJetton.cbJettonArea=cbJettonArea;
			PlaceJetton.lJettonScore=lJettonScore;
			SendData(SUB_C_HHSW_PLACE_JETTON,&PlaceJetton,sizeof(PlaceJetton));
			break;
		}
	}

	return true;
}

bool HHSWGameViewLayer::IsLookonMode()
{
	return m_LookMode;
}

//申请消息
bool HHSWGameViewLayer::OnApplyBanker(int wParam)
{
    if (m_StartTime < 2)
        return true;
	//合法判断
	const tagUserData *pMeUserData = GetMeUserData();
	if (pMeUserData->lScore < m_lApplyBankerCondition)
    {
        char TipMessage[128] = {0};
        sprintf(TipMessage, "\u60a8\u7684\u9152\u5427\u8c46\u4e0d\u8db3\uff0c\u65e0\u6cd5\u4e0a\u5e84\uff0c\u4e0a\u5e84\u6761\u4ef6\u4e3a\uff1a%lld\u9152\u5427\u8c46",m_lApplyBankerCondition);
        AlertMessageLayer::createConfirm(TipMessage);
        return true;
    }
	//旁观判断
	if (IsLookonMode()) return true;
    
    m_btApplyBanker->setEnabled(false);
    m_btCancelBanker->setEnabled(false);

    if (wParam == 1)
	{
		//发送消息
		SendData(SUB_C_HHSW_APPLY_BANKER, NULL, 0);
	}
	else
	{
		//发送消息
		SendData(SUB_C_HHSW_CANCEL_BANKER, NULL, 0);
	}

	return true;
}

void HHSWGameViewLayer::callbackBt( Ref *pSender )
{
    if (m_OpenBankList)
    {
        CloseBankList();
        return;
    }
    
    Node *pNode = (Node *)pSender;
	switch (pNode->getTag())
	{
	case Btn_SBankcancle:
		{
			OnApplyBanker(0);
			break;
		}
	case Btn_SBankOK:
		{
			OnApplyBanker(1);
			break;
		}
	case Btn_BankBtn:
		{
			SoundUtil::sharedEngine()->playEffect("buttonMusic");
			if (m_OpenBankList == true)
                CloseBankList();
			else
                OpenBankList();
			break;
		}
	case Btn_GetScoreBtn:
		{	
			GetScoreForBank();
			break;
		}
	case Btn_BackToLobby: // 返回大厅按钮
		{
			SoundUtil::sharedEngine()->playEffect("buttonMusic");
			BackToLobby();
			break;
		}
	case Btn_Seting: //设置
		{
			SoundUtil::sharedEngine()->playEffect("buttonMusic");
			GameLayerMove::sharedGameLayerMoveSink()->OpenSeting();
			break;
		}
	case Btn_100:
		{
			Select100();
			break;
		}
	case Btn_1000:
		{
			Select1000();
			break;
		}
	case Btn_1W:
		{
			Select1w();
			break;
		}
	case Btn_10W:
		{
			Select10w();
			break;
		}
	case Btn_100w:
		{
			Select100w();
			break;
		}
	case Btn_500W:
		{
			Select500w();
			break;
		}
	case Btn_1000w:
		{
			Select1000w();
			break;
		}
	}
}

void HHSWGameViewLayer::AddPlayerInfo()
{
	if (ClientSocketSink::sharedSocketSink()->GetMeUserData() == NULL) return;
	int userid = ClientSocketSink::sharedSocketSink()->GetMeUserData()->dwUserID;
	const tagUserData* pUserData = ClientSocketSink::sharedSocketSink()->m_UserManager.SearchUserByUserID(userid);
	if (pUserData == NULL) return;

	//头像
	removeAllChildByTag(m_Head_Tga);
    ui::Scale9Sprite* playerInfoBg = ui::Scale9Sprite::createWithSpriteFrameName("bg_player_info.png");
    playerInfoBg->setContentSize(Size(450, 150));
    playerInfoBg->setPosition(Vec2(250, 80));
    addChild(playerInfoBg, 0, m_Head_Tga);
    
    Sprite* headBg = Sprite::createWithSpriteFrameName("bg_head_img.png");
    headBg->setPosition(Vec2(70, 75));
    playerInfoBg->addChild(headBg);
    
    string heads = g_GlobalUnits.getFace(pUserData->wGender, pUserData->lScore);
    Sprite* mHead = Sprite::createWithSpriteFrameName(heads);
    mHead->setPosition(Vec2(75, 75));
    mHead->setScale(0.9f);
    playerInfoBg->addChild(mHead);
    
    removeAllChildByTag(m_Glod_Tga);
    char strc[32]="";
    LONGLONG lMon = pUserData->lScore;
    m_lUserCurScore =pUserData->lScore;
    memset(strc , 0 , sizeof(strc));
    Tools::AddComma(lMon , strc);
    
    string goldstr = "酒吧豆: ";
    goldstr.append(strc);
    Label* mGoldFont;
    mGoldFont = Label::createWithSystemFont(goldstr,_GAME_FONT_NAME_1_,30);
    mGoldFont->setTag(m_Glod_Tga);
    playerInfoBg->addChild(mGoldFont);
    mGoldFont->setColor(Color3B::YELLOW);
    mGoldFont->setAnchorPoint(Vec2(0, 0.5f));
    mGoldFont->setPosition(Vec2(150, 75));
    
    //昵称
    removeAllChildByTag(m_NickName_Tga);
    string namestr = "昵称: ";
    namestr.append(gbk_utf8(pUserData->szNickName));
    Label* mNickfont = Label::createWithSystemFont(namestr, _GAME_FONT_NAME_1_, 30);
    mNickfont->setTag(m_NickName_Tga);
    playerInfoBg->addChild(mNickfont);
    mNickfont->setAnchorPoint(Vec2(0,0.5f));
    mNickfont->setPosition(Vec2(150, 115));
    
    //输赢金币
    removeAllChildByTag(m_WinGlod_Tga);
    Tools::AddComma(m_lMeStatisticScore, strc);
    string winStr = "成绩: ";
    winStr.append(strc);
    mGoldFont = Label::createWithSystemFont(winStr,_GAME_FONT_NAME_1_,30);
    mGoldFont->setAnchorPoint(Vec2(0,0.5f));
    mGoldFont->setTag(m_WinGlod_Tga);
    mGoldFont->setPosition(Vec2(150, 35));
    playerInfoBg->addChild(mGoldFont);
}

string HHSWGameViewLayer::AddCommaToNum(LONG Num)
{
	char _str[256];
	sprintf(_str,"%ld", Num);
	string _string = _str;
	auto step = _string.length()/3;
	for (int i = 1; i <= step; i++)
	{
		_string.insert(_string.length()-(i-1)-(i*3), ",");
	}
	return _string;
}


void HHSWGameViewLayer::StartTime(int _type,int _time)
{
	m_StartTime = _time;
	schedule(schedule_selector(HHSWGameViewLayer::UpdateTime), 1);
	m_ClockSpr->setVisible(true);
	UpdateTime(m_StartTime);

	removeAllChildByTag(m_TimeType_Tga);
	std::string szTempTitle;
	if (_type == HHSW_IDI_FREE)	szTempTitle = "state_2.png";	//空闲时间
	else if (_type == HHSW_IDI_PLACE_JETTON) szTempTitle = "state_3.png";  //下注时间
	else if (_type == HHSW_IDI_DISPATCH_CARD) szTempTitle = "state_1.png"; //开牌时间

	if (!szTempTitle.empty())
	{
		Sprite* temp = Sprite::createWithSpriteFrameName(szTempTitle.c_str());
		temp->setAnchorPoint(Vec2(1,0.5f));
		temp->setPosition(Vec2(1050, 880));
		temp->setTag(m_TimeType_Tga);
		addChild(temp);
	}
}

void HHSWGameViewLayer::StopTime()
{
	unschedule(schedule_selector(HHSWGameViewLayer::UpdateTime));
	m_ClockSpr->removeAllChildren();
	m_ClockSpr->setVisible(false);
}

void HHSWGameViewLayer::UpdateTime(float fp)
{
	if (m_StartTime < 0)
	{
        m_CurSelectGold = 0;
		unschedule(schedule_selector(HHSWGameViewLayer::UpdateTime));
		return;
	}
	m_ClockSpr->removeAllChildren();
    std::string str = StringUtils::toString(m_StartTime);
    if (m_StartTime < 10)
        str = "0" + str;
    LabelAtlas *time = LabelAtlas::create(str, "Common/time_2.png", 39, 51, '+');
    time->setAnchorPoint(Vec2(0, 0.5f));
    time->setPosition(Vec2(400, 30));
    m_ClockSpr->addChild(time);
    
	m_StartTime--;
}


//游戏空闲
bool HHSWGameViewLayer::OnSubGameFree(const void * pBuffer, WORD wDataSize)
{
	//效验数据
	ASSERT(wDataSize==sizeof(CMD_S_HHSW_GameFree));
	if (wDataSize!=sizeof(CMD_S_HHSW_GameFree)) return false;
	//消息处理
	CMD_S_HHSW_GameFree * pGameFree=(CMD_S_HHSW_GameFree *)pBuffer;

	//设置时间
	StartTime(HHSW_IDI_FREE,pGameFree->cbTimeLeave);

	//设置状态
	setGameStatus(GS_FREE);

	//清理桌面
	removeAllChildByTag(m_GameEndBack_Tga);
	for (int i = 0; i< 3; i++)
	{
        m_LightSpr[i]->setVisible(false);
		m_LightSpr[i]->stopAllActions();
	}
	removeAllChildByTag(m_RecordBlink_Tga);
	for (int i = 0; i < 8; i++)
	{
		removeAllChildByTag(m_AllMeBetScore_Tga[i]);
		removeAllChildByTag(m_TalbeGold_Tga[i]);
		removeAllChildByTag(m_AllLimBetScore_Tga[i]);
		removeAllChildByTag(m_AllBetScore_Tga[i]);
	}

	for (int i = 0; i < 9; i++)
	{
		m_lAllJettonScore[i] = 0;
		m_lUserJettonScore[i] = 0;
	}
	//更新控件
	UpdateButtonContron();

	return true;
}


//游戏开始
bool HHSWGameViewLayer::OnSubGameStart(const void * pBuffer, WORD wDataSize)
{
	//效验数据
	ASSERT(wDataSize==sizeof(CMD_S_HHSW_GameStart));
	if (wDataSize!=sizeof(CMD_S_HHSW_GameStart)) return false;

	//消息处理
	CMD_S_HHSW_GameStart * pGameStart=(CMD_S_HHSW_GameStart *)pBuffer;

	//庄家信息
	SetBankerInfo(pGameStart->wBankerUser,pGameStart->lBankerScore);

	//玩家信息
	m_lMeMaxScore=pGameStart->lUserMaxScore;

	//设置时间
	StartTime(HHSW_IDI_PLACE_JETTON,pGameStart->cbTimeLeave);

	//设置状态
	setGameStatus(HHSW_GS_PLACE_JETTON);

	WORD wJettonTime[HHSW_AREA_COUNT+1]= {0,40,30,20,10,5,5,5,5};

	//绘画数字
	for (int nAreaIndex=1; nAreaIndex<= HHSW_AREA_COUNT; ++nAreaIndex)
	{
		LONGLONG llMaxNum = 0L;
		//每门的下注数
		for (BYTE i=1;i <= HHSW_AREA_COUNT;++i)
		{
			if(i == nAreaIndex) continue;
			llMaxNum += m_lUserJettonScore[i];
		}
		llMaxNum += m_lBankerScore;
		llMaxNum = llMaxNum/wJettonTime[nAreaIndex] -m_lUserJettonScore[nAreaIndex];
//		SetArea(nAreaIndex-1,llMaxNum);
	}

	PlayGameMusicBK();
	SoundUtil::sharedEngine()->playEffect("hhsw/sound/GAME_START");

	//更新控制
	UpdateButtonContron();

	return true;
}

void HHSWGameViewLayer::PlayGameMusicBK()
{
	SoundUtil::sharedEngine()->stopBackMusic();
	int _rand = getRand(1,9);
	char tempsound[32];
	sprintf(tempsound,"sdb/sound/bk%d",_rand);
	SoundUtil::sharedEngine()->playBackMusic(tempsound,false);
    SoundUtil::sharedEngine()->setBackSoundVolume(g_GlobalUnits.m_fBackMusicValue);
    SoundUtil::sharedEngine()->setSoundVolume(g_GlobalUnits.m_fSoundValue);
}

//用户加注
bool HHSWGameViewLayer::OnSubPlaceJetton(const void * pBuffer, WORD wDataSize)
{
	//效验数据
	ASSERT(wDataSize==sizeof(CMD_S_HHSW_PlaceJetton));
	if (wDataSize!=sizeof(CMD_S_HHSW_PlaceJetton)) return false;

	CMD_S_HHSW_PlaceJetton * pPlaceJetton=(CMD_S_HHSW_PlaceJetton *)pBuffer;
    
	PlaceUserJetton(pPlaceJetton->cbJettonArea-1,pPlaceJetton->lJettonScore);

    
	if (GetMeChairID()==pPlaceJetton->wChairID)
	{
		SetMePlaceJetton(pPlaceJetton->cbJettonArea-1, pPlaceJetton->lJettonScore);
	}
	if (pPlaceJetton->lJettonScore>=1000000) 
	{
		SoundUtil::sharedEngine()->playEffect("hhsw/sound/ADD_GOLD_EX");
	}
	else
	{
		SoundUtil::sharedEngine()->playEffect("hhsw/sound/ADD_GOLD");
	}

	UpdateButtonContron();
	return true;
}

//设置筹码
void HHSWGameViewLayer::PlaceUserJetton(BYTE cbViewIndex, LONGLONG lScoreCount)
{
	//效验参数
	ASSERT(cbViewIndex <= HHSW_AREA_COUNT);
	if (cbViewIndex > HHSW_AREA_COUNT) return;

	LONGLONG score = 0;
	for (int i = 0; i < 8; i++)
	{
		if (i == cbViewIndex)
		{
			m_lAllJettonScore[i] += lScoreCount;
			score = m_lAllJettonScore[i];
			if(lScoreCount == 0) return;
			const char* name = GetGoldName(lScoreCount);
			Sprite* temp = Sprite::createWithSpriteFrameName(name);
            temp->setScale(0.3f);
			temp->setTag(m_TalbeGold_Tga[i]);
			int nXPos,nYPos;
			nXPos= m_JettonPos[i].x+getRand(-65,50);
			nYPos= m_JettonPos[i].y+getRand(-50,55);
			temp->setPosition(Vec2(nXPos,nYPos));
			addChild(temp);
			break;
		}
	}

	//区域总注显示
    auto child = getChildByTag(m_AllBetScore_Tga[cbViewIndex]);
    if (child)
    {
        child->removeFromParent();
    }


    char strc[32]="";
    sprintf(strc,"%lld", m_lAllJettonScore[cbViewIndex]);
    log("---------->>>>>%d, %lld",cbViewIndex, m_lAllJettonScore[cbViewIndex]);
    LabelAtlas* temp = LabelAtlas::create(strc, "Common/gold_self.png", 19, 24, '+');
    temp->setAnchorPoint(Vec2(0.5f, 0.5f));
    temp->setTag(m_AllBetScore_Tga[cbViewIndex]);
    temp->setPosition(m_AllBetNumPos[cbViewIndex]);
    addChild(temp,100);
}


void HHSWGameViewLayer::SetMePlaceJetton(BYTE cbViewIndex, LONGLONG score)
{
    
    m_lUserJettonScore[cbViewIndex] += score;
    
    removeAllChildByTag(m_AllMeBetScore_Tga[cbViewIndex]);
    
    char strc[32]="";
    sprintf(strc, "%lld", m_lUserJettonScore[cbViewIndex]);
    LabelAtlas* temp = LabelAtlas::create(strc, "Common/gold_self.png", 19, 24, '+');
    temp->setAnchorPoint(Vec2(0.5f, 0.5f));
    temp->setTag(m_AllMeBetScore_Tga[cbViewIndex]);
    temp->setPosition(m_AllBetNumPos[cbViewIndex] + Vec2(0, 100));
    addChild(temp);
}

void HHSWGameViewLayer::SetArea(BYTE cbViewIndex,LONGLONG score)	//设置当前可下注数
{
	//区域总注显示
    
    removeAllChildByTag(m_AllLimBetScore_Tga[cbViewIndex]);
    
    char strc[32]="";
    sprintf(strc, "%lld", score);
    LabelAtlas* temp = LabelAtlas::create(strc, "Common/gold_self.png", 19, 24, '+');
    temp->setAnchorPoint(Vec2(0.5f, 0.5f));
    temp->setTag(m_AllLimBetScore_Tga[cbViewIndex]);
    temp->setPosition(m_LimBetNumPos[cbViewIndex]);
    addChild(temp);
}


//游戏结束
bool HHSWGameViewLayer::OnSubGameEnd(const void * pBuffer, WORD wDataSize)
{
	//效验数据
	ASSERT(wDataSize==sizeof(CMD_S_HHSW_GameEnd));
	if (wDataSize!=sizeof(CMD_S_HHSW_GameEnd)) return false;

	//消息处理
	CMD_S_HHSW_GameEnd * pGameEnd=(CMD_S_HHSW_GameEnd *)pBuffer;

	//设置时间
	StartTime(HHSW_IDI_DISPATCH_CARD, pGameEnd->cbTimeLeave);

	m_lPlayAllScore = pGameEnd->lUserScore;
	m_lBankerCurGameScore = pGameEnd->lBankerScore;
	m_lPlayReturnScore = pGameEnd->lUserReturnScore;
	m_lBankerRevenue = pGameEnd->lRevenue;
	m_lMeStatisticScore+=m_lPlayAllScore;
	SoundUtil::sharedEngine()->stopBackMusic();
	//结果牌
   
    m_cbSlowIndex = pGameEnd->cbMoveEndIndex;
    m_cbResultIndex = pGameEnd->cbResultIndex;
    m_cbEndIndex = pGameEnd->cbEndIndex;
	RunAnim();

	//设置状态
	setGameStatus(HHSW_GS_GAME_END);

	//庄家信息
	m_wBankerTime=pGameEnd->nBankerTime;
	m_lBankerWinScore=pGameEnd->lBankerTotallScore;

	UpdateButtonContron();

	return true;
}

//游戏记录
bool HHSWGameViewLayer::OnSubHistoryGameRecord(const void * pBuffer, WORD wDataSize)
{
    //效验参数
    ASSERT(wDataSize%sizeof(CMD_S_HHSW_ScoreHistory)==0);
    if (wDataSize%sizeof(CMD_S_HHSW_ScoreHistory)!=0) return false;
    
    //设置记录
    WORD wRecordCount=wDataSize/sizeof(CMD_S_HHSW_ScoreHistory);
    for (WORD wIndex=0;wIndex<wRecordCount;wIndex++)
    {
        CMD_S_HHSW_ScoreHistory * pServerGameRecord=(((CMD_S_HHSW_ScoreHistory *)pBuffer)+wIndex);
        
        for (int i = 0; i < 50; i++)
        {
            if(pServerGameRecord->cbScoreHistroy[i] > 0)
            {
                m_RecordVec.push_back(pServerGameRecord->cbScoreHistroy[i]);
                if (m_RecordVec.size() > 8)
                {
                    auto font = m_RecordVec.begin();
                    m_RecordVec.erase(font);
                }
            }
        }
    }
    DrawRecord();
    return true;
}

void HHSWGameViewLayer::DrawRecord(bool IsBlink)
{
	removeAllChildByTag(m_RecordSpr_Tga);
	if (m_RecordVec.size() < 8)
	{
		for (int i = 0; i < m_RecordVec.size(); i++)
		{
			char _str[128];
			sprintf(_str, "hhsw_%d.png",m_RecordVec[i]-1);
			Sprite* temp = Sprite::createWithSpriteFrameName(_str);
			temp->setPosition(Vec2(90, 920 - i * 95));
			temp->setTag(m_RecordSpr_Tga);
			addChild(temp, 100);
		}
	}
	else
	{
		int Lim = 0;
        for (auto i = m_RecordVec.size()-8; i < m_RecordVec.size(); i++)
		{
			char _str[128];
			sprintf(_str, "hhsw_%d.png",m_RecordVec[i]-1);
			Sprite* temp = Sprite::createWithSpriteFrameName(_str);
			temp->setPosition(Vec2(90, 920 - Lim * 95));
			temp->setTag(m_RecordSpr_Tga);
			addChild(temp, 100);
			Lim++;
		}
	}

	//添加动画
	if (IsBlink)
	{
		if(m_RecordVec.size() == 0) return;
        ui::Scale9Sprite* temp = ui::Scale9Sprite::createWithSpriteFrameName("hhsw2_sel.png");
        temp->setContentSize(Size(130,130));
		if(m_RecordVec.size() <= 8)
            temp->setPosition(Vec2(90,920-(m_RecordVec.size()-1)*95));
		else
            temp->setPosition(Vec2(90,255));
		temp->setTag(m_RecordBlink_Tga);
		addChild(temp);
		temp->runAction(RepeatForever::create(Blink::create(0.8f, 1)));
	}
}


//申请做庄
bool HHSWGameViewLayer::OnSubUserApplyBanker(const void * pBuffer, WORD wDataSize)
{
	//效验数据
	ASSERT(wDataSize==sizeof(CMD_S_HHSW_ApplyBanker));
	if (wDataSize!=sizeof(CMD_S_HHSW_ApplyBanker)) return false;

	//消息处理
	CMD_S_HHSW_ApplyBanker * pApplyBanker=(CMD_S_HHSW_ApplyBanker *)pBuffer;

	//获取玩家
	const tagUserData * pClientUserItem = GetUserData(pApplyBanker->wApplyUser);
	if(pClientUserItem == NULL) return true;
	//插入玩家
	if (m_wBankerUser!=pApplyBanker->wApplyUser)
	{
		tagApplyUser temp;
		sprintf(temp.strUserName,"%s", pClientUserItem->szNickName);
		temp.lUserScore=pClientUserItem->lScore;
        temp.wGender = pClientUserItem->wGender;
		temp.bCompetition = false;
		m_BankUserList.push_back(temp);
	}

	//自己判断
	if (IsLookonMode()==false && GetMeChairID()==pApplyBanker->wApplyUser)
	{
		m_btApplyBanker->setVisible(false);
		m_btCancelBanker->setVisible(true);
        m_btCancelBanker->setEnabled(true);
	}

	//自己判断
	if (IsLookonMode()==false && GetMeChairID()==pApplyBanker->wApplyUser)
        m_bMeApplyBanker=true;

	//更新控件
	UpdateButtonContron();
	DrawBankList();

	return true;
}


void HHSWGameViewLayer::DeleteBankList(tagApplyUser ApplyUser)
{
	tagApplyUser temp;
	for (int i = 0; i < m_BankUserList.size(); i++)
	{
		if (strcmp(m_BankUserList[i].strUserName, ApplyUser.strUserName) == 0)
		{
			m_BankUserList.erase(m_BankUserList.begin()+i);
		}
	}
}

//取消做庄
bool HHSWGameViewLayer::OnSubUserCancelBanker(const void * pBuffer, WORD wDataSize)
{
	//效验数据
	ASSERT(wDataSize==sizeof(CMD_S_HHSW_CancelBanker));
	if (wDataSize!=sizeof(CMD_S_HHSW_CancelBanker)) return false;

	//消息处理
	CMD_S_HHSW_CancelBanker * pCancelBanker=(CMD_S_HHSW_CancelBanker *)pBuffer;

	tagApplyUser ApplyUser;
	sprintf(ApplyUser.strUserName,"%s", pCancelBanker->szCancelUser);
	ApplyUser.lUserScore=0;
	DeleteBankList(ApplyUser);

	//自己判断
	const tagUserData * pMeUserData= GetMeUserData();
	if (IsLookonMode() == false && pMeUserData->wChairID == pCancelBanker->wCancelUser)
	{
		m_btCancelBanker->setVisible(false);
		m_btApplyBanker->setVisible(true);
        m_btApplyBanker->setEnabled(true);
		m_bMeApplyBanker=false;
	}
	
	//更新控件
	UpdateButtonContron();
	DrawBankList();

	return true;
}


void HHSWGameViewLayer::DrawBankList()
{
    m_listView->removeAllItems();
    for (int i = 0; i < m_BankUserList.size(); i++)
    {
        ui::Layout* custom_item = ui::Layout::create();
        custom_item->setContentSize(Size(920, 120));
        
        Sprite* lable = Sprite::createWithSpriteFrameName("label_line.png");
        lable->setPosition(460, 0);
        custom_item->addChild(lable);
        
        //头像
        string heads = g_GlobalUnits.getFace(m_BankUserList[i].wGender, m_BankUserList[i].lUserScore);
        Sprite* mHead = Sprite::createWithSpriteFrameName(heads);
        mHead->setPosition(Vec2(90, 60));
        mHead->setScale(0.6f);
        custom_item->addChild(mHead);
        
        //昵称
        std::string szTempTitle = gbk_utf8(m_BankUserList[i].strUserName);
        Label* temp = Label::createWithSystemFont(szTempTitle.c_str(),_GAME_FONT_NAME_1_,40);
        temp->setAnchorPoint(Vec2(0,0.5f));
        
        if (!strcmp(GetMeUserData()->szNickName, m_BankUserList[i].strUserName))
            temp->setColor(Color3B::RED);
        else
            temp->setColor(Color3B::BLACK);
        temp->setPosition(Vec2(170, 60));
        custom_item->addChild(temp);
        
        
        Sprite* goldIcon = Sprite::createWithSpriteFrameName("gold_icon.png");
        goldIcon->setPosition(Vec2(620, 60));
        custom_item->addChild(goldIcon);
        char _score[32];
        if (m_BankUserList[i].lUserScore > 1000000000000)
        {
            sprintf(_score,"%.02lf兆", m_BankUserList[i].lUserScore / 1000000000000.0);
        }
        else if (m_BankUserList[i].lUserScore > 100000000)
        {
            sprintf(_score,"%.02lf亿", m_BankUserList[i].lUserScore / 100000000.0);
        }
        else if (m_BankUserList[i].lUserScore > 1000000)
        {
            sprintf(_score,"%.02lf万", m_BankUserList[i].lUserScore / 10000.0);
        }
        temp = Label::createWithSystemFont(_score,_GAME_FONT_NAME_1_,40);
        temp->setAnchorPoint(Vec2(0,0.5f));
        temp->setColor(Color3B::BLACK);
        temp->setPosition(Vec2(660, 60));
        custom_item->addChild(temp);
        m_listView->pushBackCustomItem(custom_item);
    }
}


//切换庄家
bool HHSWGameViewLayer::OnSubChangeBanker(const void * pBuffer, WORD wDataSize)
{
	//效验数据
	ASSERT(wDataSize==sizeof(CMD_S_HHSW_ChangeBanker));
	if (wDataSize!=sizeof(CMD_S_HHSW_ChangeBanker)) return false;

	//消息处理
	CMD_S_HHSW_ChangeBanker * pChangeBanker=(CMD_S_HHSW_ChangeBanker *)pBuffer;

	//自己判断
	if (m_wBankerUser==GetMeChairID() && IsLookonMode() == false && pChangeBanker->wBankerUser!=GetMeChairID()) 
	{
		m_bMeApplyBanker=false;
 	}
	else if (IsLookonMode() == false && pChangeBanker->wBankerUser==GetMeChairID())
	{
		m_bMeApplyBanker=true;
	}

	//庄家信息
	m_wBankerTime = 0;
	m_lBankerWinScore = 0;
	SetBankerInfo(pChangeBanker->wBankerUser,pChangeBanker->lBankerScore);

	//删除玩家
	if (m_wBankerUser!=INVALID_CHAIR)
	{
		tagApplyUser ApplyUser;
		sprintf(ApplyUser.strUserName,"%s", GetUserData(m_wBankerUser)->szNickName);
		ApplyUser.lUserScore=0;
		DeleteBankList(ApplyUser);
	}

	DrawBankList();
	//更新界面
	UpdateButtonContron();

    if (IsLookonMode() == false && pChangeBanker->wBankerUser==GetMeChairID())
    {
        m_btCancelBanker->setEnabled(false);
        m_btCancelBanker->setColor(Color3B(100,100,100));
    }
    
    return true;
}

//游戏消息
bool HHSWGameViewLayer::OnGameMessage(WORD wSubCmdID, const void * pBuffer, WORD wDataSize)
{
	log("RECEIVE MESSAGE CMD[%d]" , wSubCmdID);

	switch (wSubCmdID)
	{
	case SUB_S_HHSW_GAME_FREE:		//游戏空闲
		{
			return OnSubGameFree(pBuffer,wDataSize);
		}
	case SUB_S_HHSW_GAME_START:			//游戏开始
		{
			return OnSubGameStart(pBuffer,wDataSize);
		}
	case SUB_S_HHSW_PLACE_JETTON:	//用户加注
		{
			return OnSubPlaceJetton(pBuffer,wDataSize);
		}
	case SUB_S_HHSW_GAME_END:		//游戏结束
		{
			return OnSubGameEnd(pBuffer,wDataSize);
		}
    case SUB_S_HHSW_CHECK_IMAGE://历史记录
        {
            return OnSubHistoryGameRecord(pBuffer,wDataSize);
        }
	case SUB_S_HHSW_APPLY_BANKER:	//申请做庄
		{
			return OnSubUserApplyBanker(pBuffer, wDataSize);
		}
	case SUB_S_HHSW_CANCEL_BANKER:	//取消做庄
		{
			return OnSubUserCancelBanker(pBuffer, wDataSize);
		}
	case SUB_S_HHSW_CHANGE_BANKER:	//切换庄家
		{
			return OnSubChangeBanker(pBuffer, wDataSize);
		}
	}
	return true;
}



//场景消息
bool HHSWGameViewLayer::OnGameSceneMessage(BYTE cbGameStatus, const void * pBuffer, WORD wDataSize)
{
	switch (cbGameStatus)
	{
	case GS_FREE:
		{
			//效验数据
			ASSERT(wDataSize==sizeof(CMD_S_HHSW_StatusFree));
			if (wDataSize!=sizeof(CMD_S_HHSW_StatusFree)) return false;

			//消息处理
			CMD_S_HHSW_StatusFree * pStatusFree=(CMD_S_HHSW_StatusFree *)pBuffer;		

            //玩家信息
			m_lMeMaxScore=pStatusFree->lUserMaxScore;
            
            //庄家信息
			m_wBankerTime = pStatusFree->cbBankerTime;
			m_lBankerWinScore = pStatusFree->lBankerWinScore;
			SetBankerInfo(pStatusFree->wBankerUser,pStatusFree->lBankerScore);
            //控制信息
			m_lApplyBankerCondition=pStatusFree->lApplyBankerCondition;
			m_lAreaLimitScore=pStatusFree->lAreaLimitScore;
			
			PlayGameMusicBK();

			//设置时间
			StartTime(HHSW_IDI_FREE,pStatusFree->cbTimeLeave);

			if(IsLookonMode()==false && GetMeChairID() == m_wBankerUser)
			{
				m_bMeApplyBanker =true;
			}

			//更新控制
			UpdateButtonContron();
			return true;
		}
	case GS_PLAYING:		//游戏状态
	case HHSW_GS_GAME_END:	//结束状态
		{
			//效验数据
			ASSERT(wDataSize==sizeof(CMD_S_HHSW_StatusPlay));
			if (wDataSize!=sizeof(CMD_S_HHSW_StatusPlay)) return false;
			CMD_S_HHSW_StatusPlay * pStatusPlay=(CMD_S_HHSW_StatusPlay *)pBuffer;

			//下注信息
//			for (int nAreaIndex=1; nAreaIndex<=HHSW_AREA_COUNT; ++nAreaIndex)
//			{
//				PlaceUserJetton(nAreaIndex-1,pStatusPlay->lAllJettonScore[nAreaIndex]);
//				SetMePlaceJetton(nAreaIndex-1,pStatusPlay->lUserJettonScore[nAreaIndex]);
//			}
            
			//玩家积分
			m_lMeMaxScore=pStatusPlay->lUserMaxScore;			
					//控制信息
			m_lApplyBankerCondition=pStatusPlay->lApplyBankerCondition;
			m_lAreaLimitScore=pStatusPlay->lAreaLimitScore;

			if (pStatusPlay->cbGameStatus==HHSW_GS_GAME_END)
			{
				m_cbEndIndex = pStatusPlay->cbEndIndex;
				m_lPlayAllScore = pStatusPlay->lEndUserScore;
				m_lPlayReturnScore = pStatusPlay->lEndUserReturnScore;
				m_lBankerCurGameScore = pStatusPlay->lEndBankerScore;
				m_lBankerRevenue = pStatusPlay->lEndRevenue;
			}

			//庄家信息
			m_wBankerTime = pStatusPlay->cbBankerTime;
			m_lBankerWinScore = pStatusPlay->lBankerWinScore;
			SetBankerInfo(pStatusPlay->wBankerUser,pStatusPlay->lBankerScore);

			if(IsLookonMode()==false && GetMeChairID() == m_wBankerUser)
			{
				m_bMeApplyBanker =true;
			}

			WORD wJettonTime[HHSW_AREA_COUNT+1]= {0,40,30,20,10,5,5,5,5};

			//绘画数字
			for (int nAreaIndex=1; nAreaIndex<= HHSW_AREA_COUNT; ++nAreaIndex)
			{
				LONGLONG llMaxNum = 0L;
				//每门的下注数
				for (BYTE i=1;i <= HHSW_AREA_COUNT;++i)
				{
					if(i == nAreaIndex) continue;
					llMaxNum += m_lUserJettonScore[i];
				}
				llMaxNum += m_lBankerScore;
				llMaxNum = llMaxNum/wJettonTime[nAreaIndex] -m_lUserJettonScore[nAreaIndex];
//				SetArea(nAreaIndex-1,llMaxNum);
			}

			//设置时间
			if(pStatusPlay->cbGameStatus != HHSW_GS_GAME_END)
			{
				StartTime(HHSW_IDI_PLACE_JETTON, pStatusPlay->cbTimeLeave);
				PlayGameMusicBK();
			}

			//设置状态
			setGameStatus(pStatusPlay->cbGameStatus);

			//更新按钮
			UpdateButtonContron();
			return true;
		}
	}
	return true;
}

void HHSWGameViewLayer::backLoginView(Ref *pSender )
{
	IGameView::backLoginView(pSender);	
}

void HHSWGameViewLayer::OnQuit()
{
	m_pGameScene->removeChild(this);
}

Menu* HHSWGameViewLayer::CreateButton(std::string szBtName ,const Vec2 &p , int tag )
{
	return Tools::Button(StrToChar(szBtName+"_normal.png") , StrToChar(szBtName+"_click.png") , p, this , menu_selector(HHSWGameViewLayer::callbackBt) , tag);
}

// Alert Message 确认消息处理
void HHSWGameViewLayer::DialogConfirm(Ref *pSender)
{
	this->RemoveAlertMessageLayer();
	this->SetAllTouchEnabled(true);
}

// Alert Message 取消消息处理
void HHSWGameViewLayer::DialogCancel(Ref *pSender)
{
	this->RemoveAlertMessageLayer();
	this->SetAllTouchEnabled(true);
}

void HHSWGameViewLayer::OnEventUserScore( tagUserData * pUserData, WORD wChairID, bool bLookonUser )
{
	IGameView::OnEventUserScore(pUserData, wChairID, bLookonUser);
    AddPlayerInfo();
}

void HHSWGameViewLayer::OnEventUserStatus(tagUserData * pUserData, WORD wChairID, bool bLookonUser)
{
	IGameView::OnEventUserStatus(pUserData, wChairID, bLookonUser);
	AddPlayerInfo();
}

void HHSWGameViewLayer::OnEventUserEnter( tagUserData * pUserData, WORD wChairID, bool bLookonUser )
{
	AddPlayerInfo();
}

void HHSWGameViewLayer::OnEventUserLeave( tagUserData * pUserData, WORD wChairID, bool bLookonUser )
{
	AddPlayerInfo();
}

//设置庄家
void HHSWGameViewLayer::SetBankerInfo(WORD wBanker,LONGLONG lScore)
{
	m_wBankerUser=wBanker;
	m_lBankerScore=lScore;
	char name[32];
	int tableid = ClientSocketSink::sharedSocketSink()->GetMeUserData()->wTableID;
	tagUserData* pUserData = ClientSocketSink::sharedSocketSink()->m_UserManager.GetUserData(tableid,m_wBankerUser);
	if(m_wBankerUser == INVALID_CHAIR || pUserData == NULL) 
	{
		m_lBankerScore = 0;
		m_wBankerTime = 0;
		m_lBankerWinScore = 0;
		strcpy(name,"无人坐庄");
	}
	else 
	{
		m_lBankerScore=pUserData->lScore;
		sprintf(name, "%s", gbk_utf8(pUserData->szNickName).c_str());
	}

	//昵称
	removeAllChildByTag(m_BankNickName_Tga);
	std::string szTempTitle = name;
	Label* temp = Label::createWithSystemFont(szTempTitle,_GAME_FONT_NAME_1_,40);
	temp->setTag(m_BankNickName_Tga);
	temp->setColor(_GAME_FONT_COLOR_5_);
	temp->setPosition(Vec2(500,717));
	addChild(temp);

	//局数
    removeAllChildByTag(m_BankJuShu_Tga);
    
    Label* lable1 = Label::createWithSystemFont("连庄盘数:", _GAME_FONT_NAME_1_, 40);
    lable1->setPosition(Vec2(500, 660));
    addChild(lable1, 100, m_BankJuShu_Tga);
    
    char strc[32]="";
	sprintf(strc,"%d",m_wBankerTime);
    
    LabelAtlas* timeL = LabelAtlas::create(strc, "Common/gold_self.png", 19, 24, '+');
    timeL->setAnchorPoint(Vec2(0.5, 0.5));
    timeL->setPosition(Vec2(500, 605));
    timeL->setTag(m_BankJuShu_Tga);
    addChild(timeL,100);

	//金币
	removeAllChildByTag(m_BankGold_Tga);
    
    Label* lable2 = Label::createWithSystemFont("庄家财富:", _GAME_FONT_NAME_1_, 40);
    lable2->setPosition(Vec2(500, 550));
    addChild(lable2, 100, m_BankGold_Tga);
    
    ZeroMemory(strc, sizeof(strc));
    sprintf(strc,"%lld", m_lBankerScore);
    LabelAtlas* goldL = LabelAtlas::create(strc, "Common/gold_self.png", 19, 24, '+');
    goldL->setAnchorPoint(Vec2(0.5f, 0.5f));
    goldL->setPosition(Vec2(500, 495));
    goldL->setTag(m_BankGold_Tga);
    addChild(goldL,100);

	//战绩
	removeAllChildByTag(m_BankZhangJi_Tga);
    
    Label* lable3 = Label::createWithSystemFont("庄家输赢:", _GAME_FONT_NAME_1_, 40);
    lable3->setPosition(Vec2(500, 440));
    addChild(lable3, 100, m_BankZhangJi_Tga);
    
    ZeroMemory(strc, sizeof(strc));
    sprintf(strc,"%lld", m_lBankerWinScore);
    LabelAtlas* scoreL = LabelAtlas::create(strc, "Common/gold_self.png", 19, 24, '+');
    scoreL->setAnchorPoint(Vec2(0.5f, 0.5f));
    scoreL->setPosition(Vec2(500, 385));
    scoreL->setTag(m_BankZhangJi_Tga);
    addChild(scoreL,100);
}


const char* HHSWGameViewLayer::GetGoldName(LONGLONG lScoreCount)
{
    if(lScoreCount >= 10000000) return "chip_1000w_normal.png";
    else if(lScoreCount >= 5000000) return "chip_500w_normal.png";
    else if(lScoreCount >= 1000000) return "chip_100w_normal.png";
    else if(lScoreCount >= 100000) return "chip_10w_normal.png";
    else if(lScoreCount >= 10000) return "chip_1w_normal.png";
    else if(lScoreCount >= 1000) return "chip_1000_normal.png";
    else  return "chip_100_normal.png";
}


void HHSWGameViewLayer::Select0()
{
	m_CurSelectGold = 0;
	removeAllChildByTag(m_BtnAnim_Tga);
}

void HHSWGameViewLayer::Select100()
{
	m_CurSelectGold = 100;
	SoundUtil::sharedEngine()->playEffect("buttonMusic");
	removeAllChildByTag(m_BtnAnim_Tga);
	Sprite *pRing = Sprite::createWithSpriteFrameName("agoldselect.png");
	pRing->setPosition(Vec2(675 ,60));
	pRing->setTag(m_BtnAnim_Tga);
	addChild(pRing);
}
void HHSWGameViewLayer::Select1000()
{
	m_CurSelectGold = 1000;
	SoundUtil::sharedEngine()->playEffect("buttonMusic");
	removeAllChildByTag(m_BtnAnim_Tga);
	Sprite *pRing = Sprite::createWithSpriteFrameName("agoldselect.png");
	pRing->setPosition(Vec2(855 ,60));
	pRing->setTag(m_BtnAnim_Tga);
	addChild(pRing);  
}
void HHSWGameViewLayer::Select1w()
{
	m_CurSelectGold = 10000;
	SoundUtil::sharedEngine()->playEffect("buttonMusic");
	removeAllChildByTag(m_BtnAnim_Tga);
	Sprite *pRing = Sprite::createWithSpriteFrameName("agoldselect.png");
	pRing->setPosition(Vec2(1035 ,60));
	pRing->setTag(m_BtnAnim_Tga);
	addChild(pRing);  
}
void HHSWGameViewLayer::Select10w()
{
	m_CurSelectGold = 100000;
	SoundUtil::sharedEngine()->playEffect("buttonMusic");
	removeAllChildByTag(m_BtnAnim_Tga);
	Sprite *pRing = Sprite::createWithSpriteFrameName("agoldselect.png");
	pRing->setPosition(Vec2(1215 ,60));
	pRing->setTag(m_BtnAnim_Tga);
	addChild(pRing);  
}
void HHSWGameViewLayer::Select100w()
{
	m_CurSelectGold = 1000000;
	SoundUtil::sharedEngine()->playEffect("buttonMusic");
	removeAllChildByTag(m_BtnAnim_Tga);
	Sprite *pRing = Sprite::createWithSpriteFrameName("agoldselect.png");
	pRing->setPosition(Vec2(1395 ,60));
	pRing->setTag(m_BtnAnim_Tga);
	addChild(pRing);  
}
void HHSWGameViewLayer::Select500w()
{
	m_CurSelectGold = 5000000;
	SoundUtil::sharedEngine()->playEffect("buttonMusic");
	removeAllChildByTag(m_BtnAnim_Tga);
	Sprite *pRing = Sprite::createWithSpriteFrameName("agoldselect.png");
	pRing->setPosition(Vec2(1575 ,60));
	pRing->setTag(m_BtnAnim_Tga);
	addChild(pRing);
}

void HHSWGameViewLayer::Select1000w()
{
	m_CurSelectGold = 10000000;
	SoundUtil::sharedEngine()->playEffect("buttonMusic");
	removeAllChildByTag(m_BtnAnim_Tga);
	Sprite *pRing = Sprite::createWithSpriteFrameName("agoldselect.png");
	pRing->setPosition(Vec2(1755 ,60));
	pRing->setTag(m_BtnAnim_Tga);
	addChild(pRing);
}


void HHSWGameViewLayer::GetAllWinArea(BYTE bcWinArea[],BYTE bcAreaCount,BYTE InArea)
{
	if (InArea==0xFF) return ;
	ZeroMemory(bcWinArea,bcAreaCount);

	LONGLONG lMaxSocre = 0;

	for (int i = 0;i<32;i++)
	{
		BYTE bcOutCadDataWin[HHSW_AREA_COUNT];
		BYTE bcData[1];
		bcData[0]=i+1;
		m_GameLogic.GetCardType(bcData,1,bcOutCadDataWin);
		for (int j= 0;j<HHSW_AREA_COUNT;j++)
		{

			if(bcOutCadDataWin[j]>0&&j==InArea-1)
			{
				LONGLONG Score = 0; 
				for (int nAreaIndex=1; nAreaIndex<=HHSW_AREA_COUNT; ++nAreaIndex) 
				{
					if(bcOutCadDataWin[nAreaIndex-1]>1)
					{
						Score += m_lAllJettonScore[nAreaIndex]*(bcOutCadDataWin[nAreaIndex-1]);
					}
				}
				if(Score>=lMaxSocre)
				{
					lMaxSocre = Score;
					CopyMemory(bcWinArea,bcOutCadDataWin,bcAreaCount);
				}
				break;
			}
		}
	}
}

void HHSWGameViewLayer::RunAnim()
{
	m_CurAnimIndex = 0;
	m_CurTurn = 0;
    m_cbTotalIndex = 0;
	for (int i = 0; i< 3; i++)
	{
        m_LightSpr[i]->setVisible(false);
		m_LightSpr[i]->stopAllActions();
	}
	schedule(schedule_selector(HHSWGameViewLayer::UpdateStartSlow), 0.2f);
}

void HHSWGameViewLayer::UpdateStartSlow(float fp)	//开始慢
{
	SoundUtil::sharedEngine()->playWav("hhsw/sound/IDC_SND_SELGRID",false);
	int _rand = getRand(5,9);
	if (m_CurAnimIndex >= _rand)
	{
		unschedule(schedule_selector(HHSWGameViewLayer::UpdateStartSlow));
		schedule(schedule_selector(HHSWGameViewLayer::UpdateStartAddSpeed), 0.01f);
		return;
	}
    
    if (m_CurAnimIndex < 5)
    {
        UpdateStartAnim();
    }
    else
    {
        UpdateRunAnim();
    }
}

void HHSWGameViewLayer::UpdateStartAddSpeed(float fp)  //开始加速
{
	SoundUtil::sharedEngine()->playWav("hhsw/sound/IDC_SND_SELGRID",false);
	if (m_CurTurn>= 1)
	{
		unschedule(schedule_selector(HHSWGameViewLayer::UpdateStartAddSpeed));
		schedule(schedule_selector(HHSWGameViewLayer::UpdateStartAddSpeed1), 0.0001f);
		return;
	}
	UpdateRunAnim();
}

void HHSWGameViewLayer::UpdateStartAddSpeed1(float fp)  //开始加速1
{
	SoundUtil::sharedEngine()->playWav("hhsw/sound/IDC_SND_SELGRID",false);
	if (m_cbSlowIndex - 16 == m_cbTotalIndex)
	{
		unschedule(schedule_selector(HHSWGameViewLayer::UpdateStartAddSpeed1));
		schedule(schedule_selector(HHSWGameViewLayer::UpdateEndSlow), 0.05f);
		return;
	}
	UpdateRunAnim();
}


void HHSWGameViewLayer::UpdateEndSlow(float fp)	//结束减速
{
	SoundUtil::sharedEngine()->playWav("hhsw/sound/IDC_SND_SELGRID",false);

	if (m_cbSlowIndex == m_cbTotalIndex)
	{
		unschedule(schedule_selector(HHSWGameViewLayer::UpdateEndSlow));
		schedule(schedule_selector(HHSWGameViewLayer::UpdateEndSlow1), 0.4f);
		return;
	}
	UpdateRunAnim();
}

void HHSWGameViewLayer::UpdateEndSlow1(float fp)	//结束减速
{
	SoundUtil::sharedEngine()->playWav("hhsw/sound/IDC_SND_SELGRID",false);
	if (((m_cbEndIndex * 8) % 16) + m_cbResultIndex - 1 == m_CurAnimIndex % 16)
	{
		unschedule(schedule_selector(HHSWGameViewLayer::UpdateEndSlow1));
		schedule(schedule_selector(HHSWGameViewLayer::UpdateEnd), 1);
		return;
	}
	UpdateEndAnim();
}

void HHSWGameViewLayer::UpdateEnd(float fp)		//动画播放完毕,显示结束面板
{
	unschedule(schedule_selector(HHSWGameViewLayer::UpdateEnd));
	DrawEndScore();
	m_RecordVec.push_back(m_CurAnimIndex%8+1);
	DrawRecord(true);
	PlayAnimSound(m_CurAnimIndex%8);
}

void HHSWGameViewLayer::PlayAnimSound(int _index)
{
	switch (_index)
	{
	case 0:
		{
			SoundUtil::sharedEngine()->playEffect("hhsw/sound/btiger");
			break;
		}
	case 1:
		{
			SoundUtil::sharedEngine()->playEffect("hhsw/sound/stiger");
			break;
		}
	case 2:
		{
			SoundUtil::sharedEngine()->playEffect("hhsw/sound/bdog");
			break;
		}
	case 3:
		{
			SoundUtil::sharedEngine()->playEffect("hhsw/sound/sdog");
			break;
		}
	case 4:
		{
			SoundUtil::sharedEngine()->playEffect("hhsw/sound/bhorse");
			break;
		}
	case 5:
		{
			SoundUtil::sharedEngine()->playEffect("hhsw/sound/shorse");
			break;
		}
	case 6:
		{
			SoundUtil::sharedEngine()->playEffect("hhsw/sound/bsnake");
			break;
		}
	case 7:
		{
			SoundUtil::sharedEngine()->playEffect("hhsw/sound/ssnake");
			break;
		}
	}
}

void HHSWGameViewLayer::UpdateEndAnim()
{
	if (m_CurAnimIndex == 31)
	{
		m_CurAnimIndex = 0;
		m_CurTurn++;
	}
   
	for (int i = 0; i < 3; i++)
	{
        int cur = ((m_CurAnimIndex+i)+31)%32;
        m_LightSpr[i]->setPosition(m_LightPos[cur]);
	}
	m_CurAnimIndex++;
    m_cbTotalIndex++;
}

void HHSWGameViewLayer::UpdateStartAnim()
{
    if (m_CurAnimIndex == 31)
    {
        m_CurAnimIndex = 0;
    }
    
    for (int i = 0; i < 3; i++)
    {
        m_LightSpr[i]->setVisible(true);
        int cur = ((m_CurAnimIndex+i)+31)%32;
        m_LightSpr[i]->setPosition(m_LightPos[cur]);
    }
    m_CurAnimIndex++;
     m_cbTotalIndex++;
}

void HHSWGameViewLayer::UpdateRunAnim()
{
	if (m_CurAnimIndex == 31)
	{
		m_CurAnimIndex = 0;
		m_CurTurn++;
	}
    
	for (int i = 0; i < 3; i++)
	{
        int cur = ((m_CurAnimIndex+i)+31)%32;
        m_LightSpr[i]->setPosition(m_LightPos[cur]);
	}
	m_CurAnimIndex++;
     m_cbTotalIndex++;
}


void HHSWGameViewLayer::DrawEndScore()
{
	//结算面板
	Sprite* EndBack = Sprite::createWithSpriteFrameName("bg_tongyongkuang3.png");
	EndBack->setPosition(_STANDARD_SCREEN_CENTER_);
	EndBack->setTag(m_GameEndBack_Tga);
	addChild(EndBack,1000);	

    Sprite* title = Sprite::createWithSpriteFrameName("gameove.png");
    title->setPosition(Vec2(519, 480));
    EndBack->addChild(title);

    Sprite* spr = Sprite::createWithSpriteFrameName("bg_result.png");
    spr->setPosition(Vec2(519, 350));
    EndBack->addChild(spr);
    
    Sprite* title1 = Sprite::createWithSpriteFrameName("title.png");
    title1->setPosition(Vec2(250, 350));
    EndBack->addChild(title1);
    
	//开奖动物
	char endchar[128];
	sprintf(endchar, "hhsw_%d.png", m_cbResultIndex - 1);
	Sprite* temp = Sprite::createWithSpriteFrameName(endchar);
	temp->setPosition(Vec2(520,350));
	EndBack->addChild(temp);

	sprintf(endchar, "hhswtest_%d.png", m_cbResultIndex - 1);
	temp = Sprite::createWithSpriteFrameName(endchar);
	temp->setPosition(Vec2(700,350));
	EndBack->addChild(temp);

    Sprite* bottomSpr = Sprite::createWithSpriteFrameName("bg_result2.png");
    bottomSpr->setPosition(Vec2(519, 160));
    EndBack->addChild(bottomSpr);
    
	//自己
    char scoreStr[128];
    sprintf(scoreStr, "%lld", m_lPlayAllScore);
    LabelAtlas* selfScore = LabelAtlas::create(scoreStr, "Common/xiazhushuzi.png", 34, 46, '+');
    selfScore->setAnchorPoint(Vec2(0.5f, 0.5f));
    selfScore->setPosition(Vec2(500, 165));
    EndBack->addChild(selfScore);
    
	//自己返回分
     sprintf(scoreStr, "%lld", m_lPlayReturnScore);
    LabelAtlas* selfRScore = LabelAtlas::create(scoreStr, "Common/xiazhushuzi.png", 34, 46, '+');
    selfRScore->setAnchorPoint(Vec2(0.5f, 0.5f));
    selfRScore->setPosition(Vec2(835, 165));
    EndBack->addChild(selfRScore);

	//庄家
    sprintf(scoreStr, "%lld", m_lBankerCurGameScore);
    LabelAtlas* bankScore = LabelAtlas::create(scoreStr, "Common/xiazhushuzi.png", 34, 46, '+');
    bankScore->setAnchorPoint(Vec2(0.5f, 0.5f));
    bankScore->setPosition(Vec2(500, 80));
    EndBack->addChild(bankScore);

	//庄家返回分
    sprintf(scoreStr, "%lld", m_lBankerCurGameScore);
    LabelAtlas* bankRScore = LabelAtlas::create(scoreStr, "Common/xiazhushuzi.png", 34, 46, '+');
    bankRScore->setAnchorPoint(Vec2(0.5f, 0.5f));
    bankRScore->setPosition(Vec2(835, 80));
    EndBack->addChild(bankRScore);
}

void HHSWGameViewLayer::LayerMoveCallBack()
{
    m_IsLayerMove = false;
}

void HHSWGameViewLayer::CloseBankList()
{
    if (m_IsLayerMove)
        return;
    m_IsLayerMove = true;
    m_OpenBankList = false;
    MoveTo *leftMoveBy = MoveTo::create(0.2f, Vec2(960,1500));
    ActionInstant *func = CallFunc::create(CC_CALLBACK_0(HHSWGameViewLayer::LayerMoveCallBack,this));
    m_BankListBackSpr->runAction(Sequence::create(leftMoveBy, func, NULL));
}

void HHSWGameViewLayer::OpenBankList()
{
    if (m_IsLayerMove)
        return;
    m_IsLayerMove = true;
    m_OpenBankList = true;
    MoveTo *leftMoveBy = MoveTo::create(0.2f, _STANDARD_SCREEN_CENTER_);
    ActionInstant *func = CallFunc::create(CC_CALLBACK_0(HHSWGameViewLayer::LayerMoveCallBack,this));
    m_BankListBackSpr->runAction(Sequence::create(leftMoveBy, func, NULL));
}

//更新控制
void HHSWGameViewLayer::UpdateButtonContron()
{
	//置能判断
	bool bEnablePlaceJetton=true;

	if (getGameStatus()!= HHSW_GS_PLACE_JETTON)
	{
		bEnablePlaceJetton=false;
	}
	if (m_wBankerUser==GetMeChairID())
	{
		bEnablePlaceJetton=false;
	}
	if (IsLookonMode())
	{
		bEnablePlaceJetton=false;
	}

	//下注按钮
	if (bEnablePlaceJetton==true)
	{
		
		//计算积分
		LONGLONG lCurrentJetton=m_CurSelectGold;
		LONGLONG lLeaveScore=m_lMeMaxScore;
        //最大下注
        LONGLONG lUserMaxJetton = m_lUserCurScore;
        
		for (int nAreaIndex=1; nAreaIndex<=HHSW_AREA_COUNT; ++nAreaIndex)
        {
            lLeaveScore -= m_lUserJettonScore[nAreaIndex];
            lUserMaxJetton -= m_lUserJettonScore[nAreaIndex];
        }
	
        char strc[32]="";
        memset(strc , 0 , sizeof(strc));
        Tools::AddComma(lUserMaxJetton , strc);
        
        string goldstr = "酒吧豆: ";
        goldstr.append(strc);
        Node* node = getChildByTag(m_Head_Tga);
        Label* mGoldFont = (Label*)node->getChildByTag(m_Glod_Tga);
        mGoldFont->setString(goldstr);
        
        lLeaveScore = min((lLeaveScore),lUserMaxJetton); //用户可下分 和最大分比较 由于是五倍 

		//设置光标
		if (lCurrentJetton>lLeaveScore)
		{
			if (lLeaveScore>=10000000L) m_CurSelectGold = 10000000;
			else if (lLeaveScore>=5000000L) m_CurSelectGold = 5000000;
			else if (lLeaveScore>=1000000L) m_CurSelectGold = 1000000;
			else if (lLeaveScore>=500000L) m_CurSelectGold = 500000;
			else if (lLeaveScore>=100000L) m_CurSelectGold = 100000;
			else if (lLeaveScore>=10000L) m_CurSelectGold = 10000;
			else if (lLeaveScore>=1000L) m_CurSelectGold = 1000;
			else if (lLeaveScore>=100L) m_CurSelectGold = 100;
			else m_CurSelectGold = 0;
		}

		//控制按钮
		int iTimer = 1;
		if (lLeaveScore>=100*iTimer && lUserMaxJetton>=100*iTimer)
		{
			m_bn100->setEnabled(true); m_bn100->setColor(Color3B(255,255,255));
		}
		else
		{
			m_bn100->setEnabled(false); m_bn100->setColor(Color3B(100,100,100));
		}
		
		if (lLeaveScore>=1000*iTimer && lUserMaxJetton>=1000*iTimer)
		{
			m_bn1000->setEnabled(true); m_bn1000->setColor(Color3B(255,255,255));
		}
		else
		{
			m_bn1000->setEnabled(false); m_bn1000->setColor(Color3B(100,100,100));
		}
		
		if (lLeaveScore>=10000*iTimer && lUserMaxJetton>=10000*iTimer)
		{
			m_bn1W->setEnabled(true); m_bn1W->setColor(Color3B(255,255,255));
		}
		else
		{
			m_bn1W->setEnabled(false); m_bn1W->setColor(Color3B(100,100,100));
		}

		if (lLeaveScore>=100000*iTimer && lUserMaxJetton>=100000*iTimer)
		{
			m_bn10W->setEnabled(true); m_bn10W->setColor(Color3B(255,255,255));
		}
		else
		{
			m_bn10W->setEnabled(false); m_bn10W->setColor(Color3B(100,100,100));
		}
	
		if (lLeaveScore>=1000000*iTimer && lUserMaxJetton>=1000000*iTimer)
		{
			m_bn100W->setEnabled(true); m_bn100W->setColor(Color3B(255,255,255));
		}
		else
		{
			m_bn100W->setEnabled(false); m_bn100W->setColor(Color3B(100,100,100));
		}
		
		if (lLeaveScore>=5000000*iTimer && lUserMaxJetton>=5000000*iTimer)
		{
			m_bn500W->setEnabled(true); m_bn500W->setColor(Color3B(255,255,255));
		}
		else
		{
			m_bn500W->setEnabled(false); m_bn500W->setColor(Color3B(100,100,100));
		}	

		if (lLeaveScore>=10000000*iTimer && lUserMaxJetton>=10000000*iTimer)
		{
			m_bn1000W->setEnabled(true); m_bn1000W->setColor(Color3B(255,255,255));
		}
		else
		{
			m_bn1000W->setEnabled(false); m_bn1000W->setColor(Color3B(100,100,100));
		}	
	}
	else
	{
		//设置光标
		m_CurSelectGold = 0;
		removeAllChildByTag(m_BtnAnim_Tga);

		//禁止按钮
		m_bn100->setEnabled(false); m_bn100->setColor(Color3B(100,100,100));
		m_bn1000->setEnabled(false); m_bn1000->setColor(Color3B(100,100,100));
		m_bn1W->setEnabled(false); m_bn1W->setColor(Color3B(100,100,100));
		m_bn10W->setEnabled(false); m_bn10W->setColor(Color3B(100,100,100));
		m_bn100W->setEnabled(false); m_bn100W->setColor(Color3B(100,100,100));
		m_bn500W->setEnabled(false); m_bn500W->setColor(Color3B(100,100,100));
		m_bn1000W->setEnabled(false); m_bn1000W->setColor(Color3B(100,100,100));
	}

	if (m_wBankerUser == GetMeChairID())
	{
		m_btApplyBanker->setVisible(false);
		if (getGameStatus() == GS_FREE)
		{
			m_btCancelBanker->setVisible(true);
			m_btCancelBanker->setEnabled(true);
			m_btCancelBanker->setColor(Color3B(255,255,255));
		}
		else
		{
			m_btCancelBanker->setVisible(true);
			m_btCancelBanker->setEnabled(false);
			m_btCancelBanker->setColor(Color3B(100,100,100));
		}
	}
	else
	{
        if (getGameStatus() == HHSW_GS_PLACE_JETTON)
        {
            m_btApplyBanker->setEnabled(false);
            m_btApplyBanker->setColor(Color3B(100,100,100));
            m_btCancelBanker->setEnabled(false);
            m_btCancelBanker->setColor(Color3B(100,100,100));
            return;
        }
        
		if (m_bMeApplyBanker) //已经申请
		{
			m_btApplyBanker->setVisible(false);
			m_btCancelBanker->setVisible(true);
			m_btCancelBanker->setEnabled(true);
			m_btCancelBanker->setColor(Color3B(255,255,255));
		}
		else
		{
			m_btApplyBanker->setVisible(true);
            m_btApplyBanker->setEnabled(true);
			m_btCancelBanker->setVisible(false);
            m_btApplyBanker->setColor(Color3B(255,255,255));
		}
	}

	return;
}

//最大下注
LONGLONG HHSWGameViewLayer::GetUserMaxJetton(BYTE cbJettonArea)
{
	if (cbJettonArea==0xFF) return 0;

	//已下注额
	LONGLONG lNowJetton = 0;
	for (int nAreaIndex=0; nAreaIndex<HHSW_AREA_COUNT; ++nAreaIndex)
        lNowJetton += m_lUserJettonScore[nAreaIndex];
	//庄家金币
	LONGLONG lBankerScore = 0x7fffffffffffffff;
	if (m_wBankerUser!=INVALID_CHAIR) lBankerScore = m_lBankerScore;

	BYTE bcWinArea[HHSW_AREA_COUNT];
	LONGLONG LosScore = 0;
	LONGLONG WinScore = 0;
	GetAllWinArea(bcWinArea,HHSW_AREA_COUNT,cbJettonArea);

	for (int nAreaIndex=1; nAreaIndex<=HHSW_AREA_COUNT; ++nAreaIndex) 
	{
		if(bcWinArea[nAreaIndex-1]>1)
		{
			LosScore+=m_lAllJettonScore[nAreaIndex]*(bcWinArea[nAreaIndex-1]);
		}
		else
		{
			WinScore+=m_lAllJettonScore[nAreaIndex];
		}
	}

	LONGLONG lTemp = lBankerScore;
	lBankerScore = lBankerScore + WinScore - LosScore;

	if ( lBankerScore < 0 )
	{
		if (m_wBankerUser!=INVALID_CHAIR)
		{
			lBankerScore = m_lBankerScore;
		}
		else
		{
			lBankerScore = 0x7fffffffffffffff;
		}
	}

	//区域限制
	LONGLONG lMeMaxScore;

	if((m_lMeMaxScore - lNowJetton)>m_lAreaLimitScore)
	{
		lMeMaxScore= m_lAreaLimitScore;
	}
	else
	{
		lMeMaxScore = m_lMeMaxScore-lNowJetton;
		lMeMaxScore = lMeMaxScore;
	}

	//庄家限制
	if (lMeMaxScore > (lBankerScore)/(bcWinArea[cbJettonArea-1]))
	{
		lMeMaxScore = (lBankerScore)/(bcWinArea[cbJettonArea-1]);
	}

	//非零限制
	if (lMeMaxScore < 0)
	{
		lMeMaxScore = 0;
	}
	return lMeMaxScore;
}
