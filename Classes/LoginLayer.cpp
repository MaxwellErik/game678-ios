#include "LoginLayer.h"
#include "ClientSocketSink.h"
#include "LobbyLayer.h"
#include "LocalDataUtil.h"
#include "JniSink.h"
#include "LobbySocketSink.h"
#include "Encrypt.h"
#include "AlertMessageLayer.h"
#include "jsoncpp.h"
#include "GlobalUnits.h"
#include "GameBuyLayer.h"
#include "localInfo.h"
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
#include "GoodsByWeChatPay.h"
#endif
#include "GameLayerMove.h"
#include "cocos/network/HttpResponse.h"
#include "Convert.h"
#include "utils/utf8cpp/utf8.h"

#define BT_Login			0
#define BT_Register			1
#define BT_SavePsd			2
#define BT_NoSavePsd		3
#define BT_RememberPass		4
#define Reg_Register		6
#define Reg_Cancel			7
#define BT_Close			8
#define Bind_OK             9
#define Bind_Cancle			10
#define BT_AutouGou			12
#define WeChatPass_OK      13
#define WeChatPass_Cancel   14

#define BindWX_OK			17
#define BindWX_Cancle		18
#define BindWX_Close		19
#define BindWX_OpenWX		20

#define BT_LoginWX			21
#define BT_LoginTourist    22
#define BT_LoginAccount    23
#define BT_AccouctClose    24

LoginLayer::LoginLayer(GameScene *pGameScene):GameLayer(pGameScene)
{
	m_pUserName = NULL;
	m_pPassword = NULL;
    g_GlobalUnits.m_UserLoginInfo.isTourist = false;
}

LoginLayer::~LoginLayer()
{
}


LoginLayer *LoginLayer::create(GameScene *pGameScene)
{
    LoginLayer *pLayer = new LoginLayer(pGameScene);
    if(pLayer && pLayer->init())
    {
        pLayer->autorelease();
        return pLayer;
    }
    else
    {
        CC_SAFE_DELETE(pLayer);
        return NULL;
    }
}

void LoginLayer::onExit()
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    GameLayerMove::sharedGameLayerMoveSink()->SetLoginLayer(NULL);
#endif
	GameLayer::onExit();
	Tools::removeSpriteFrameCache("Login_View_Frame.plist");
}

void LoginLayer::close()
{
	this->runAction(Sequence::create(ScaleTo::create(0.2f , 0.1f),
		CallFunc::create(CC_CALLBACK_0(LoginLayer::onRemove, this)),
		NULL
		));
	
}

void LoginLayer::onRemove()
{
	JniSink::share()->setJniCallback(NULL);
    LoadLayer::removeLoadLayer(Director::getInstance()->getRunningScene());
	m_pGameScene->removeChild(this);
}

bool LoginLayer::onTouchBegan(Touch *pTouch, Event *pEvent)
{
    return true;
}

void LoginLayer::onTouchMoved(cocos2d::Touch *pTouch, cocos2d::Event *pEvent)
{

}

void LoginLayer::onTouchEnded(cocos2d::Touch *pTouch, cocos2d::Event *pEvent)
{
}

void LoginLayer::delayGetAndroidVersion(float dt)
{
    JniSink::share()->GetAndroidVersion();
}

bool LoginLayer::init()
{
	if ( !Layer::init() )	return false;

	SoundUtil::sharedEngine()->playBackMusic("denglu");
	g_GlobalUnits.m_fSoundValue = LocalDataUtil::GetSoundValue();
	g_GlobalUnits.m_fBackMusicValue = LocalDataUtil::GetBackMusicValue();
	SoundUtil::sharedEngine()->setSoundVolume(g_GlobalUnits.m_fSoundValue);
	SoundUtil::sharedEngine()->setBackSoundVolume(g_GlobalUnits.m_fBackMusicValue);
	LobbySocketSink::sharedSocketSink()->closeSocket();
	JniSink::share()->setJniCallback(this);
	GameLayer::onEnter();
	setTouchEnabled(true);
	
	g_GlobalUnits.m_bSingleGame = false;
	g_GlobalUnits.m_vcTransPropRecord.clear();
	g_GlobalUnits.m_ServerListManager.ClearAllData();
	g_GlobalUnits.cleanAllBuyRecord();
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    GameLayerMove::sharedGameLayerMoveSink()->SetLoginLayer(this);
#endif

	setLocalZOrder(10);
    Tools::addSpriteFrame("Common/common.plist");
    Tools::addSpriteFrame("Login_View_Frame.plist");

    Sprite *pView = Sprite::create("DaTing/DaTingBack.jpg");
    pView->setAnchorPoint(Vec2(0,0));
    pView->setPosition(Vec2(0,0));
    addChild(pView);
    
    Sprite* Logo = Sprite::createWithSpriteFrameName("login_icon.png");
    Logo->setPosition(_STANDARD_SCREEN_CENTER_);
    addChild(Logo);
    
    m_Tga_LoginBack = 10;
    m_Tga_BindBack_Tga= 11;
    m_Tga_RegBack = 12;
	m_Tga_TouchDisableMenu = 13;
    m_Tga_WechatBack_Tga = 14;
    
    InitLoginLayer();
    
    addButton();
	
    LobbySocketSink::sharedSocketSink()->SetLoginLayer(this);

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    LoadLayer::createLoad(m_pGameScene);
	JniSink::share()->SendToDeviceID();
    scheduleOnce(schedule_selector(LoginLayer::delayGetAndroidVersion), 1.0f);
	if (static_isFirstEnter)
        AutoSelectLine();
#endif    
    auto listener = EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(true);
    listener->onTouchBegan = CC_CALLBACK_2(LoginLayer::onTouchBegan,this);
    listener->onTouchMoved = CC_CALLBACK_2(LoginLayer::onTouchMoved,this);
    listener->onTouchEnded = CC_CALLBACK_2(LoginLayer::onTouchEnded,this);
    _eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);
    return true;
}

int LoginLayer::getRand(int start,int end)  
{
    //产生一个从start到end间的随机数
    srand((unsigned)time(0));
    int randNum = std::rand();
    int swpan = end - start + 1;
    int result = randNum % swpan + start;
    return result;
}

void LoginLayer::AutoSelectLine()	//自动选择线路
{
    std::string s1 = LocalInfo::getMobileBrand();
    std::string s2 = LocalInfo::getMobileKind();
    std::string s3 = LocalInfo::getMobileSystemVersion();
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    if (0 == g_GlobalUnits.m_SwitchYZ)
    {
        g_GlobalUnits.m_IP = "aa.game678.com";
        g_GlobalUnits.m_ServerPort = 8301;
        return;
    }
#endif

    if (/* DISABLES CODE */ (true))
    {
        g_GlobalUnits.m_IP = "192.168.137.210";//"45.249.244.235";
        g_GlobalUnits.m_ServerPort = 9001;
        return;
    }

    g_GlobalUnits.m_IPIndex = getRand(0, 4);
    int domainIndex = getRand(0, 2);
    string areas[] = {AREA1, AREA2, AREA3, AREA4, AREA5};
    string Domain[] = {LOGIN_IP1, LOGIN_IP2, LOGIN_IP3};
    g_GlobalUnits.m_IP = areas[g_GlobalUnits.m_IPIndex] + Domain[domainIndex];

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    g_GlobalUnits.m_ServerPort = getRand(IOS_PORT_START, IOS_PORT_END);
#else
     g_GlobalUnits.m_ServerPort = getRand(ANDROID_PORT_STRTR, ANDROID_PORT_END);
     showWechatLoginBtn(true);
#endif
}

void LoginLayer::ReconnectServer()
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    if (0 == g_GlobalUnits.m_SwitchYZ)
    {
        return;
    }
#endif
    
    g_GlobalUnits.m_IPIndex++;
    g_GlobalUnits.m_IPIndex %= 5;
    
    string areas[] = {AREA1, AREA2, AREA3, AREA4, AREA5};
    string Domain[] = {LOGIN_IP1, LOGIN_IP2, LOGIN_IP3};
    g_GlobalUnits.m_IP = areas[g_GlobalUnits.m_IPIndex] + Domain[getRand(0, 2)];
    
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    g_GlobalUnits.m_ServerPort = getRand(IOS_PORT_START, IOS_PORT_END);
#else
    g_GlobalUnits.m_ServerPort = getRand(ANDROID_PORT_STRTR, ANDROID_PORT_END);
#endif
}

void LoginLayer::onEnter()
{
	GameLayer::onEnter();
}

void LoginLayer::addButton()
{
    Menu* pTourist = CreateButton("bt_tourist", (Vec2(460,120)) , BT_LoginTourist);
    addChild(pTourist, 0, BT_LoginTourist);
    
    Menu* PAccount = CreateButton("bt_account", (Vec2(960,120)) , BT_LoginAccount);
    addChild(PAccount, 0, BT_LoginAccount);
    
    Menu* PWeChat = CreateButton("bt_weixin", (Vec2(1460,120)) , BT_LoginWX);
    addChild(PWeChat, 0, BT_LoginWX);
    
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    pTourist->setVisible(false);
    PAccount->setVisible(false);
    PWeChat->setVisible(false);
#endif
}

void LoginLayer::showWechatLoginBtn(bool show)
{
    auto pTourist = dynamic_cast<Menu*>(getChildByTag(BT_LoginTourist));
    if (pTourist)
        pTourist->setVisible(true);
    
    auto PAccount = dynamic_cast<Menu*>(getChildByTag(BT_LoginAccount));
    if (PAccount)
        PAccount->setVisible(true);
    
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    auto weChatBtn = dynamic_cast<Menu*>(getChildByTag(BT_LoginWX));
    if (weChatBtn)
        weChatBtn->setVisible(show);
#endif
}

void LoginLayer::addDisableMenu()
{
    auto sp = ui::Scale9Sprite::create("Common/bg_gray.png");
    sp->setContentSize(Size(1920,1080));
    auto pMenuItem = MenuItemSprite::create(sp, sp, sp);
    Menu* pTouchDisableMenu = Menu::create(pMenuItem, nullptr);
    pTouchDisableMenu->setPosition(Vec2(960, 540));
    pTouchDisableMenu->setTag(m_Tga_TouchDisableMenu);
    addChild(pTouchDisableMenu);
}

void LoginLayer::removeDisableMenu()
{
    removeAllChildByTag(m_Tga_TouchDisableMenu);
}

void LoginLayer::InitLoginLayer()
{
	initUserData();
	m_bRemeberPsd = true;
    
    removeAllChildByTag(m_Tga_LoginBack);
    
	Sprite *LoginBack = Sprite::createWithSpriteFrameName("bg_tongyongkuang1.png");
	LoginBack->setPosition(Vec2(_STANDARD_SCREEN_CENTER_.x, 2500));
	LoginBack->setTag(m_Tga_LoginBack);
	addChild(LoginBack, 10);

    Sprite* title = Sprite::createWithSpriteFrameName("title_login.png");
    title->setPosition(Vec2(535, 630));
    LoginBack->addChild(title);
    
  //记住密码
    m_AutoGou=Sprite::createWithSpriteFrameName("checklinegou_normal.png");
    m_AutoGou->setPosition(Vec2(280 , 220));
    LoginBack->addChild(m_AutoGou);
    
    m_rememberPass=CreateButton("checklinenogou", Vec2(355 , 220) , BT_RememberPass);
    LoginBack->addChild(m_rememberPass);
    
    Label *accountLabel = Label::createWithSystemFont("账号:", _GAME_FONT_NAME_1_, 46);
    accountLabel->setPosition(Vec2(150, 450));
    accountLabel->setColor(_GAME_FONT_COLOR_1_);
    LoginBack->addChild(accountLabel);

    Label *passLabel = Label::createWithSystemFont("密码:", _GAME_FONT_NAME_1_, 46);
    passLabel->setPosition(Vec2(150, 330));
    passLabel->setColor(_GAME_FONT_COLOR_1_);
    LoginBack->addChild(passLabel);
    
    ui::Scale9Sprite* accountBg = ui::Scale9Sprite::createWithSpriteFrameName("bg_shuru.png");
    accountBg->setContentSize(Size(700, 100));
    accountBg->setPosition(Vec2(575, 450));
    LoginBack->addChild(accountBg);
    
    ui::Scale9Sprite* passBg = ui::Scale9Sprite::createWithSpriteFrameName("bg_shuru.png");
    passBg->setContentSize(Size(700, 100));
    passBg->setPosition(Vec2(575, 330));
    LoginBack->addChild(passBg);
    
	std::string str = "请输入账号：";
	Tools::StringTransform(str);
    const Size inputSize = Size(620,100);
    m_pUserName = cocos2d::ui::EditBox::create(inputSize, INPUT_BACKGROUND);
    m_pUserName->setFontName(_GAME_FONT_NAME_1_);
    m_pUserName->setFontSize(46);
    m_pUserName->setFontColor(_GAME_FONT_COLOR_2_);
    m_pUserName->setPlaceholderFontName(_GAME_FONT_NAME_1_);
    m_pUserName->setPlaceHolder(str.c_str());
    m_pUserName->setPlaceholderFontSize(46);
    m_pUserName->setPlaceholderFontColor(_GAME_FONT_COLOR_2_);
    m_pUserName->setInputMode(cocos2d::ui::EditBox::InputMode::ANY);
	m_pUserName->setPosition(Vec2(575, 445));
	m_pUserName->setMaxLength(16);
	LoginBack->addChild(m_pUserName);
	TCHAR	szAccounts[NAME_LEN];			//登录帐号
	ZeroMemory(szAccounts,NAME_LEN);
	if(g_GlobalUnits.m_UserDataVec.size() > 0)
        m_pUserName->setText(g_GlobalUnits.m_UserDataVec[0].szAccounts);
	else
        m_pUserName->setText(szAccounts);

	str = "请输入密码：";
	Tools::StringTransform(str);

    m_pPassword = cocos2d::ui::EditBox::create(inputSize, INPUT_BACKGROUND);
    m_pPassword->setFontName(_GAME_FONT_NAME_1_);
    m_pPassword->setFontSize(46);
    m_pPassword->setFontColor(_GAME_FONT_COLOR_2_);
    m_pPassword->setPlaceholderFontName(_GAME_FONT_NAME_1_);
    m_pPassword->setPlaceHolder(str.c_str());
    m_pPassword->setPlaceholderFontSize(46);
    m_pPassword->setPlaceholderFontColor(_GAME_FONT_COLOR_2_);
    m_pPassword->setInputMode(cocos2d::ui::EditBox::InputMode::ANY);
    m_pPassword->setInputFlag(cocos2d::ui::EditBox::InputFlag::PASSWORD);
	m_pPassword->setPosition(Vec2(575, 325));
	m_pPassword->setMaxLength(20);
	LoginBack->addChild(m_pPassword);
	char pszPassword[PASS_LEN];
	ZeroMemory(pszPassword,PASS_LEN);
    bool isRemember = UserDefault::getInstance()->getBoolForKey("REMEMBER_PASSWORD");
	if(g_GlobalUnits.m_UserDataVec.size() > 0 && isRemember)
    {
        m_pPassword->setText(g_GlobalUnits.m_UserDataVec[0].pszPassword);
        m_AutoGou->setVisible(true);
    }
	else
    {
        m_pPassword->setText(pszPassword);
        m_AutoGou->setVisible(false);
    }
    
    //注册按钮
    Menu* pMenu = CreateButton("bt_register", (Vec2(323,100)) , BT_Register);
    LoginBack->addChild(pMenu, 0, BT_Register);
    
    //登陆按钮
    pMenu=CreateButton("bt_login", (Vec2(723,100)) , BT_Login);
    LoginBack->addChild(pMenu,0,BT_Login);
    
    //关闭按钮
    pMenu=CreateButton("btn_close", (Vec2(1000,610)) , BT_AccouctClose);
    LoginBack->addChild(pMenu,0, BT_AccouctClose);
}

void LoginLayer::OnExitLoginLayer()
{
	Node* LoginBack = getChildByTag(m_Tga_LoginBack);
	MoveTo *leftMoveBy = MoveTo::create(0.5f, Vec2(_STANDARD_SCREEN_CENTER_.x,-600));
	LoginBack->runAction(Sequence::create(leftMoveBy,NULL));
}

void LoginLayer::InitRegisterLayer()
{
    removeAllChildByTag(m_Tga_RegBack);
    
    ui::Scale9Sprite *RegisterBack = ui::Scale9Sprite::createWithSpriteFrameName("bg_tongyongkuang2.png");
    RegisterBack->setPosition(Vec2(_STANDARD_SCREEN_CENTER_.x,2000));
    RegisterBack->setContentSize(Size(1020, 1020));
    RegisterBack->setTag(m_Tga_RegBack);
    addChild(RegisterBack, 10);
    
    MoveTo *moveTo = MoveTo::create(0.5f, _STANDARD_SCREEN_CENTER_);
    RegisterBack->runAction(Sequence::create(moveTo,NULL));
    
    Sprite* title = Sprite::createWithSpriteFrameName("title_register.png");
    title->setPosition(Vec2(510, 960));
    RegisterBack->addChild(title);
    
    Menu *pMenu=CreateButton("bt_register1" , (Vec2(310,70)) , Reg_Register);
    RegisterBack->addChild(pMenu,0,Reg_Register);
    
    pMenu=CreateButton("bt_Login_cancel" , (Vec2(710, 70)) , Reg_Cancel);
    RegisterBack->addChild(pMenu,0,Reg_Cancel);
    
    const Size inputSize = Size(700,90);
    
    Label *account = Label::createWithSystemFont("账号:", _GAME_FONT_NAME_1_, 46);
    account->setAnchorPoint(Vec2(1, 0.5f));
    account->setPosition(Vec2(210, 850));
    account->setColor(_GAME_FONT_COLOR_1_);
    RegisterBack->addChild(account);
    
    ui::Scale9Sprite* accountBg = ui::Scale9Sprite::createWithSpriteFrameName("bg_shuru.png");
    accountBg->setContentSize(inputSize);
    accountBg->setPosition(Vec2(575, 850));
    RegisterBack->addChild(accountBg);
    
    m_pRegisterUserName = cocos2d::ui::EditBox::create(inputSize, INPUT_BACKGROUND);
    m_pRegisterUserName->setFontName(_GAME_FONT_NAME_1_);
    m_pRegisterUserName->setFontSize(46);
    m_pRegisterUserName->setFontColor(_GAME_FONT_COLOR_2_);
    m_pRegisterUserName->setPlaceholderFontName(_GAME_FONT_NAME_1_);
    m_pRegisterUserName->setPlaceHolder("请输入账号");
    m_pRegisterUserName->setPlaceholderFontSize(46);
    m_pRegisterUserName->setPlaceholderFontColor(_GAME_FONT_COLOR_2_);
    m_pRegisterUserName->setInputMode(cocos2d::ui::EditBox::InputMode::ANY);
    m_pRegisterUserName->setPosition(Vec2(600, 845));
    m_pRegisterUserName->setMaxLength(16);
    RegisterBack->addChild(m_pRegisterUserName);

    Label *nickName = Label::createWithSystemFont("昵称:", _GAME_FONT_NAME_1_, 46);
    nickName->setAnchorPoint(Vec2(1, 0.5f));
    nickName->setPosition(Vec2(210, 740));
    nickName->setColor(_GAME_FONT_COLOR_1_);
    RegisterBack->addChild(nickName);
    
    ui::Scale9Sprite* nickNameBg = ui::Scale9Sprite::createWithSpriteFrameName("bg_shuru.png");
    nickNameBg->setContentSize(inputSize);
    nickNameBg->setPosition(Vec2(575, 740));
    RegisterBack->addChild(nickNameBg);
    
    m_pNickName = cocos2d::ui::EditBox::create(inputSize, INPUT_BACKGROUND);
    m_pNickName->setFontName(_GAME_FONT_NAME_1_);
    m_pNickName->setFontSize(46);
    m_pNickName->setFontColor(_GAME_FONT_COLOR_2_);
    m_pNickName->setPlaceholderFontName(_GAME_FONT_NAME_1_);
    m_pNickName->setPlaceHolder("请输入昵称");
    m_pNickName->setPlaceholderFontSize(46);
    m_pNickName->setPlaceholderFontColor(_GAME_FONT_COLOR_2_);
    m_pNickName->setInputMode(cocos2d::ui::EditBox::InputMode::ANY);
    m_pNickName->setPosition(Vec2(600, 735));
    m_pNickName->setMaxLength(16);
    RegisterBack->addChild(m_pNickName);
    
    Label *pass = Label::createWithSystemFont("密码:", _GAME_FONT_NAME_1_, 46);
    pass->setAnchorPoint(Vec2(1, 0.5f));
    pass->setPosition(Vec2(210, 630));
    pass->setColor(_GAME_FONT_COLOR_1_);
    RegisterBack->addChild(pass);
    
    ui::Scale9Sprite* passBg = ui::Scale9Sprite::createWithSpriteFrameName("bg_shuru.png");
    passBg->setContentSize(inputSize);
    passBg->setPosition(Vec2(575, 630));
    RegisterBack->addChild(passBg);
    
    m_pRegisterPassword1 = cocos2d::ui::EditBox::create(inputSize, INPUT_BACKGROUND);
    m_pRegisterPassword1->setFontName(_GAME_FONT_NAME_2_);
    m_pRegisterPassword1->setFontSize(46);
    m_pRegisterPassword1->setFontColor(_GAME_FONT_COLOR_2_);
    m_pRegisterPassword1->setPlaceholderFontName(_GAME_FONT_NAME_2_);
    m_pRegisterPassword1->setPlaceHolder("请输入密码");
    m_pRegisterPassword1->setPlaceholderFontSize(46);
    m_pRegisterPassword1->setPlaceholderFontColor(_GAME_FONT_COLOR_2_);
    m_pRegisterPassword1->setInputMode(cocos2d::ui::EditBox::InputMode::ANY);
    m_pRegisterPassword1->setInputFlag(cocos2d::ui::EditBox::InputFlag::PASSWORD);
    m_pRegisterPassword1->setPosition(Vec2(600, 625));
    m_pRegisterPassword1->setMaxLength(16);
    RegisterBack->addChild(m_pRegisterPassword1);
 
    Label *pass1 = Label::createWithSystemFont("确认密码:", _GAME_FONT_NAME_1_, 46);
    pass1->setAnchorPoint(Vec2(1, 0.5f));
    pass1->setPosition(Vec2(210, 520));
    pass1->setColor(_GAME_FONT_COLOR_1_);
    RegisterBack->addChild(pass1);
    
    ui::Scale9Sprite* passBg1 = ui::Scale9Sprite::createWithSpriteFrameName("bg_shuru.png");
    passBg1->setContentSize(inputSize);
    passBg1->setPosition(Vec2(575, 520));
    RegisterBack->addChild(passBg1);

    m_pRegisterPassword2 = cocos2d::ui::EditBox::create(inputSize, INPUT_BACKGROUND);
    m_pRegisterPassword2->setFontName(_GAME_FONT_NAME_2_);
    m_pRegisterPassword2->setFontSize(46);
    m_pRegisterPassword2->setFontColor(_GAME_FONT_COLOR_2_);
    m_pRegisterPassword2->setPlaceholderFontName(_GAME_FONT_NAME_2_);
    m_pRegisterPassword2->setPlaceHolder("请再次输入密码");
    m_pRegisterPassword2->setPlaceholderFontSize(46);
    m_pRegisterPassword2->setPlaceholderFontColor(_GAME_FONT_COLOR_2_);
    m_pRegisterPassword2->setInputMode(cocos2d::ui::EditBox::InputMode::ANY);
    m_pRegisterPassword2->setInputFlag(cocos2d::ui::EditBox::InputFlag::PASSWORD);
    m_pRegisterPassword2->setPosition(Vec2(600, 515));
    m_pRegisterPassword2->setMaxLength(16);
    RegisterBack->addChild(m_pRegisterPassword2);

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    if (g_GlobalUnits.m_SwitchYZ == 0)
        return;
#endif
    
    Label *ID = Label::createWithSystemFont("身份证:", _GAME_FONT_NAME_1_, 46);
    ID->setAnchorPoint(Vec2(1, 0.5f));
    ID->setPosition(Vec2(210, 410));
    ID->setColor(_GAME_FONT_COLOR_1_);
    RegisterBack->addChild(ID);
    
    ui::Scale9Sprite* IDBg = ui::Scale9Sprite::createWithSpriteFrameName("bg_shuru.png");
    IDBg->setContentSize(inputSize);
    IDBg->setPosition(Vec2(575, 410));
    RegisterBack->addChild(IDBg);
    
    m_pIDCard = cocos2d::ui::EditBox::create(inputSize, INPUT_BACKGROUND);
    m_pIDCard->setFontName(_GAME_FONT_NAME_2_);
    m_pIDCard->setFontSize(46);
    m_pIDCard->setFontColor(_GAME_FONT_COLOR_2_);
    m_pIDCard->setPlaceholderFontName(_GAME_FONT_NAME_2_);
    m_pIDCard->setPlaceHolder("请输入身份证");
    m_pIDCard->setPlaceholderFontSize(46);
    m_pIDCard->setPlaceholderFontColor(_GAME_FONT_COLOR_2_);
    m_pIDCard->setInputMode(cocos2d::ui::EditBox::InputMode::ANY);
    m_pIDCard->setPosition(Vec2(600, 405));
    m_pIDCard->setMaxLength(18);
    RegisterBack->addChild(m_pIDCard);
 
    Label *Phone = Label::createWithSystemFont("手机号:", _GAME_FONT_NAME_1_, 46);
    Phone->setAnchorPoint(Vec2(1, 0.5f));
    Phone->setPosition(Vec2(210, 300));
    Phone->setColor(_GAME_FONT_COLOR_1_);
    RegisterBack->addChild(Phone);
    
    ui::Scale9Sprite* PhoneBg = ui::Scale9Sprite::createWithSpriteFrameName("bg_shuru.png");
    PhoneBg->setContentSize(inputSize);
    PhoneBg->setPosition(Vec2(575, 300));
    RegisterBack->addChild(PhoneBg);
    
    m_pPhone = cocos2d::ui::EditBox::create(inputSize, INPUT_BACKGROUND);
    m_pPhone->setFontName(_GAME_FONT_NAME_2_);
    m_pPhone->setFontSize(46);
    m_pPhone->setFontColor(_GAME_FONT_COLOR_2_);
    m_pPhone->setPlaceholderFontName(_GAME_FONT_NAME_2_);
    m_pPhone->setPlaceHolder("请输入手机号");
    m_pPhone->setPlaceholderFontSize(46);
    m_pPhone->setPlaceholderFontColor(_GAME_FONT_COLOR_2_);
    m_pPhone->setInputMode(cocos2d::ui::EditBox::InputMode::PHONE_NUMBER);
    m_pPhone->setPosition(Vec2(600, 295));
    m_pPhone->setMaxLength(11);
    RegisterBack->addChild(m_pPhone);
    
    Label *QQ = Label::createWithSystemFont("Q Q:", _GAME_FONT_NAME_1_, 46);
    QQ->setAnchorPoint(Vec2(1, 0.5f));
    QQ->setPosition(Vec2(210, 190));
    QQ->setColor(_GAME_FONT_COLOR_1_);
    RegisterBack->addChild(QQ);
    
    ui::Scale9Sprite* QQBg = ui::Scale9Sprite::createWithSpriteFrameName("bg_shuru.png");
    QQBg->setContentSize(inputSize);
    QQBg->setPosition(Vec2(575, 190));
    RegisterBack->addChild(QQBg);
    
    m_pQQ = cocos2d::ui::EditBox::create(inputSize, INPUT_BACKGROUND);
    m_pQQ->setFontName(_GAME_FONT_NAME_2_);
    m_pQQ->setFontSize(46);
    m_pQQ->setFontColor(_GAME_FONT_COLOR_2_);
    m_pQQ->setPlaceholderFontName(_GAME_FONT_NAME_2_);
    m_pQQ->setPlaceHolder("请输入QQ号");
    m_pQQ->setPlaceholderFontSize(46);
    m_pQQ->setPlaceholderFontColor(_GAME_FONT_COLOR_2_);
    m_pQQ->setInputMode(cocos2d::ui::EditBox::InputMode::NUMERIC);
    m_pQQ->setPosition(Vec2(600, 185));
    m_pQQ->setMaxLength(13);
    RegisterBack->addChild(m_pQQ);
}

void LoginLayer::InitWechatPass() //微信验证码
{
    removeAllChildByTag(m_Tga_WechatBack_Tga);
    ui::Scale9Sprite *BindBack = ui::Scale9Sprite::createWithSpriteFrameName("bg_tongyongkuang2.png");
    BindBack->setContentSize(Size(1020, 600));
    BindBack->setPosition(Vec2(_STANDARD_SCREEN_CENTER_.x,2000));
    BindBack->setTag(m_Tga_WechatBack_Tga);
    addChild(BindBack, 10);
    
    MoveTo *leftMoveBy = MoveTo::create(0.5f, _STANDARD_SCREEN_CENTER_);
    BindBack->runAction(Sequence::create(leftMoveBy,NULL));
    
    Label* title = Label::createWithSystemFont("微信验证码", "", 46);
    title->setColor(_GAME_FONT_COLOR_4_);
    title->setPosition(Vec2(510,550));
    BindBack->addChild(title);
    
    Label* lable = Label::createWithSystemFont("系统检测到您本次登录信息与上次不一致，为了您的账号安全，本次登录需要微信验证码！", "", 46);
    lable->setDimensions(800, 300);
    lable->setAlignment(cocos2d::TextHAlignment::LEFT);
    lable->setColor(_GAME_FONT_COLOR_1_);
    lable->setPosition(Vec2(510,350));
    BindBack->addChild(lable);
    
    ui::Scale9Sprite* bg = ui::Scale9Sprite::createWithSpriteFrameName("bg_shuru.png");
    bg->setContentSize(Size(780, 100));
    bg->setPosition(Vec2(510, 250));
    BindBack->addChild(bg);
    
    Menu* pNode = CreateButton("btn_queding", Vec2(310,80), WeChatPass_OK);
    BindBack->addChild(pNode ,0 , WeChatPass_OK);
    
    pNode = CreateButton("btn_quxiao", Vec2(710,80), WeChatPass_Cancel);
    BindBack->addChild(pNode ,0 , WeChatPass_Cancel);
    
    std::string str = "请输入微信验证码";
    Tools::StringTransform(str);
    const Size inputSize = Size(700,100);
    m_WeChatPass = cocos2d::ui::EditBox::create(inputSize, INPUT_BACKGROUND);
    m_WeChatPass->setFontName(_GAME_FONT_NAME_2_);
    m_WeChatPass->setFontSize(46);
    m_WeChatPass->setFontColor(_GAME_FONT_COLOR_1_);
    m_WeChatPass->setPlaceholderFontName(_GAME_FONT_NAME_2_);
    m_WeChatPass->setPlaceHolder(str.c_str());
    m_WeChatPass->setPlaceholderFontSize(46);
    m_WeChatPass->setPlaceholderFontColor(_GAME_FONT_COLOR_1_);
    m_WeChatPass->setInputMode(cocos2d::ui::EditBox::InputMode::ANY);
    m_WeChatPass->setPosition(Vec2(510, 245));
    m_WeChatPass->setMaxLength(18);
    BindBack->addChild(m_WeChatPass);
}

void LoginLayer::InitBindIdCard() //绑定身份证
{
    removeAllChildByTag(m_Tga_BindBack_Tga);
    ui::Scale9Sprite *BindBack = ui::Scale9Sprite::createWithSpriteFrameName("bg_tongyongkuang2.png");
    BindBack->setContentSize(Size(1020, 600));
    BindBack->setPosition(Vec2(_STANDARD_SCREEN_CENTER_.x,2000));
    BindBack->setTag(m_Tga_BindBack_Tga);
    addChild(BindBack, 10);
    
    MoveTo *leftMoveBy = MoveTo::create(0.5f, _STANDARD_SCREEN_CENTER_);
    BindBack->runAction(Sequence::create(leftMoveBy,NULL));
    
    Label* title = Label::createWithSystemFont("身份验证", "", 46);
    title->setColor(_GAME_FONT_COLOR_4_);
    title->setPosition(Vec2(510,550));
    BindBack->addChild(title);
    
    Label* lable = Label::createWithSystemFont("系统检测到您本次登录信息与上次不一致，为了您的账号安全，本次登录需要验证您的身份信息！", "", 46);
    lable->setDimensions(800, 300);
    lable->setAlignment(cocos2d::TextHAlignment::LEFT);
    lable->setColor(_GAME_FONT_COLOR_1_);
    lable->setPosition(Vec2(510,350));
    BindBack->addChild(lable);
    
    ui::Scale9Sprite* bg = ui::Scale9Sprite::createWithSpriteFrameName("bg_shuru.png");
    bg->setContentSize(Size(780, 100));
    bg->setPosition(Vec2(510, 250));
    BindBack->addChild(bg);
    
    Menu* pNode = CreateButton("btn_queding", Vec2(310,80), Bind_OK);
    BindBack->addChild(pNode ,0 , Bind_OK);
    
    pNode = CreateButton("btn_quxiao", Vec2(710,80), Bind_Cancle);
    BindBack->addChild(pNode ,0 , Bind_Cancle);
    
    std::string str = "请输入身份证";
    Tools::StringTransform(str);
    const Size inputSize = Size(700,100);
    m_BindIDCard = cocos2d::ui::EditBox::create(inputSize, INPUT_BACKGROUND);
    m_BindIDCard->setFontName(_GAME_FONT_NAME_2_);
    m_BindIDCard->setFontSize(46);
    m_BindIDCard->setFontColor(_GAME_FONT_COLOR_1_);
    m_BindIDCard->setPlaceholderFontName(_GAME_FONT_NAME_2_);
    m_BindIDCard->setPlaceHolder(str.c_str());
    m_BindIDCard->setPlaceholderFontSize(46);
    m_BindIDCard->setPlaceholderFontColor(_GAME_FONT_COLOR_1_);
    m_BindIDCard->setInputMode(cocos2d::ui::EditBox::InputMode::ANY);
    m_BindIDCard->setPosition(Vec2(510, 245));
    m_BindIDCard->setMaxLength(18);
    BindBack->addChild(m_BindIDCard);
}

void LoginLayer::OnExitLoginLayerToWechatPass()
{
    OnExitLoginLayer();
    InitBindIdCard();
}

void LoginLayer::OnExitLoginLayerToBind()
{
    LoadLayer::removeLoadLayer(m_pGameScene);
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    if (g_GlobalUnits.m_SwitchYZ == 0)
    {
        sendLoginDataToIDCard();
    }
    else
    {
        OnExitLoginLayer();
        InitBindIdCard();
    }
#else
    OnExitLoginLayer();
    InitBindIdCard();
#endif
}

void LoginLayer::OnExitLoginLayerToRegister()
{
    OnExitLoginLayer();
    InitRegisterLayer();
}

void LoginLayer::DialogConfirm(Ref *pSender)
{
	this->RemoveAlertMessageLayer();
	this->SetAllTouchEnabled(true);
}

// Alert Message 取消消息处理
void LoginLayer::DialogCancel(Ref *pSender)
{
	this->RemoveAlertMessageLayer();
	this->SetAllTouchEnabled(true);
}

void LoginLayer::OnExitCall(Ref* pObject)
{
	JniSink::share()->CloseGameWin();
}

void LoginLayer::UpdateloginWX(float deltaTime)
{
	LoadLayer::createLoad(m_pGameScene);
	unschedule(schedule_selector(LoginLayer::UpdateloginWX));
	char _unconid[32];
	int _code = 0;
	const char *d = ",";  
	char* _call;
	_call = strtok(m_contentTemp,d);
	if(_call == NULL) return;
	sprintf(_unconid,"%s", _call);

	_call=strtok(NULL,d);  
	if(_call == NULL) return;
	_code = atoi(_call);

	CMD_MB_LogonWeiXin LogonByAccounts;
	memset(&LogonByAccounts, 0x0, sizeof(LogonByAccounts));
	LogonByAccounts.dwPlazaVersion = VER_PLAZA_FRAME;
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    if (0 != g_GlobalUnits.m_ChannelID)
    {
         LogonByAccounts.dwPlatform = 2000 + g_GlobalUnits.m_ChannelID;
    }
    else
        LogonByAccounts.dwPlatform = PLATFORM_APPLE;
    
#else
    if (0 != g_GlobalUnits.m_ChannelID)
    {
        LogonByAccounts.dwPlatform = 3000 + g_GlobalUnits.m_ChannelID;
    }
    else
        LogonByAccounts.dwPlatform = PLATFORM_ANDROID;
    
#endif
	LogonByAccounts.wModuleID = 0;
	char szMachineID[33];
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
	const char* p = Application::getInstance()->GetUUID();
    if(p == NULL)  sprintf(szMachineID,"%s","test");
    else  CMD5Encrypt::EncryptData(p, szMachineID);
#else
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
	char* p = JniSink::share()->GetDeviceID();
    if(p == NULL)  sprintf(szMachineID,"%s","test");
    else  CMD5Encrypt::EncryptData(p, szMachineID);
#endif
#endif
	sprintf(LogonByAccounts.szMachineID,"%s",szMachineID);
	strcpy(LogonByAccounts.szUnionID, _unconid);
    LogonByAccounts.nTokenPass = _code;
    
    cocos2d::log("-------------_unconid = %s,   _code = %d", _unconid, _code);
	LobbySocketSink::sharedSocketSink()->ConnectServer();
	LobbySocketSink::sharedLoginSocket()->SendData(MDM_MB_LOGON,SUB_GP_LOGON_WEIXINMB,&LogonByAccounts,sizeof(LogonByAccounts));
}

void LoginLayer::IosGetAuditStateResponse()
{
    if (0 == g_GlobalUnits.m_SwitchYZ)
    {
        showWechatLoginBtn(false);
    }
    else
        showWechatLoginBtn(true);
    
    AutoSelectLine();
}

void LoginLayer::GetAndroidVersionSuc(const char *szMessage)
{
    //安卓版本
    CMD_MB_ANDROID_BERSION version;
    sprintf(version.szVersion, "%s", szMessage);
    LobbySocketSink::sharedSocketSink()->ConnectServer();
    LobbySocketSink::sharedLoginSocket()->SendData(MDM_MB_LOGON, SUB_MB_ANDROID_VERSION, &version, sizeof(version));
}

void LoginLayer::WXLoginSuc(const char *szMessage)
{
	if (szMessage == NULL) return;
	sprintf(m_contentTemp, "%s", szMessage);
    
	schedule(schedule_selector(LoginLayer::UpdateloginWX), 2);
}

void LoginLayer::IosWxGoginSuc(const char* unionid, int code)
{
    CMD_MB_LogonWeiXin LogonByAccounts;
    memset(&LogonByAccounts, 0x0, sizeof(LogonByAccounts));
    LogonByAccounts.dwPlazaVersion = VER_PLAZA_FRAME;
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    if (0 != g_GlobalUnits.m_ChannelID)
    {
        LogonByAccounts.dwPlatform = 2000 + g_GlobalUnits.m_ChannelID;
    }
    else
        LogonByAccounts.dwPlatform = PLATFORM_APPLE;
#else
    if (0 != g_GlobalUnits.m_ChannelID)
    {
        LogonByAccounts.dwPlatform = 3000 + g_GlobalUnits.m_ChannelID;
    }
    else
        LogonByAccounts.dwPlatform = PLATFORM_ANDROID;
#endif
    LogonByAccounts.wModuleID = 0;
    
    char szMachineID[33];
    ZeroMemory(szMachineID, 33);
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    char* p = const_cast<char*>(Application::getInstance()->GetUUID());
    if(p == NULL)  sprintf(szMachineID,"%s","test");
    else  CMD5Encrypt::EncryptData(p, szMachineID);
#else
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    char* p = JniSink::share()->GetDeviceID();
    if(p == NULL)  sprintf(szMachineID,"%s","test");
    else  CMD5Encrypt::EncryptData(p, szMachineID);
#endif
#endif
    
    sprintf(LogonByAccounts.szMachineID,"%s",szMachineID);
    sprintf(LogonByAccounts.szUnionID,"%s",unionid);
    LogonByAccounts.nTokenPass = code;
    if(LobbySocketSink::sharedSocketSink()->ConnectServer())
    {
        LobbySocketSink::sharedLoginSocket()->SendData(MDM_MB_LOGON,SUB_GP_LOGON_WEIXINMB,&LogonByAccounts,sizeof(LogonByAccounts));
    }
    else
    {
        LoadLayer::removeLoadLayer(m_pGameScene);
        AlertMessageLayer::createConfirm(this, _UNICODE_CONNECT_FAILT);
    }
}

//int LoginLayer::verifyIDCard(const char* input)
//{
//    if(strlen(input)!=18)
//        return 1;
//    int i;
//    for(i=0;i<17;i++)
//    {
//        if(input[i]<'0'||input[i]>'9')
//            return 2;
//    }
//    if(input[17]=='x' || (input[17]>='0' && input[17]<='9'));
//    else return 3;
//    
//    char year[5];
//    year[0]=input[6];
//    year[1]=input[7];
//    year[2]=input[8];
//    year[3]=input[9];
//    year[4]='\0';
//    unsigned int year_number=atoi(year);
//    if(year_number<1900||year_number>2100)
//        return 4;
//    char month[3];
//    month[0]=input[10];
//    month[1]=input[11];
//    month[2]='\0';
//    unsigned int month_number=atoi(month);
//    if(month_number<1||month_number>12)
//        return 5;
//    char day[3];
//    day[0]=input[12];
//    day[1]=input[13];
//    day[2]='\0';
//    unsigned int day_number=atoi(day);
//    if(((year_number%4==0) && (year_number%100!=0)) || (year_number%400==0))
//    {
//        if(2==month_number)
//        {
//            if(day_number<1 || day_number>29)
//                return 6;
//        }
//        else if(month_number==1 || month_number==3 || month_number==5 || month_number==7 || month_number==8 || month_number==10 || month_number==12)
//        {
//            if(day_number<1 || day_number>31)
//                return 6;
//        }
//        else
//        {
//            if(day_number<1 || day_number>30)
//                return 6;
//        }
//    }
//    else
//    {
//        if(month_number==2)
//        {
//            if(day_number<1 || day_number>28)
//                return 6;
//        }
//        else if(month_number==1 || month_number==3 || month_number==5 || month_number==7 || month_number==8 || month_number==10 || month_number==12)
//        {
//            if(day_number<1 || day_number>31)
//                return 6;
//        }
//        else
//        {
//            if(day_number<1 || day_number>30)
//                return 6;
//        }
//    }
//    char c;
//    int arrExp[] = {7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2};//加权因子
//    int arrValid[] = {1, 0, 10, 9, 8, 7, 6, 5, 4, 3, 2};//校验码 10 == x
//    int sum = 0, idx;
//    for(int i = 0; i < 17; i++){
//        // 对前17位数字与权值乘积求和
//        c = input[i];
//        sum += atoi(&c) * arrExp[i];
//    }
//    idx = sum % 11;
//    
//    if (10 == arrValid[idx])
//    {
//        if ('x' != input[17] && 'X' != input[17])
//        {
//             return 7;
//        }
//    }
//    else if (atoi(&input[17]) != arrValid[idx])
//    {
//        return 7;
//    }
//        
//    return 0;
//}

void LoginLayer::UpdateCreateLoad(float deltaTime)
{
    JniSink::share()->CTJ_WXLogin();
}

void LoginLayer::callbackBt( Ref* pObject )
{
	OnCloseIME();
	Menu * p = (Menu *)pObject;
	if (!p->isVisible()) return;
	switch(p->getTag())
	{
    case BT_AccouctClose:
        {
            Node* LoginBack = getChildByTag(m_Tga_LoginBack);
            MoveTo *moveTo = MoveTo::create(0.5f, Vec2(960,1500));
            LoginBack->runAction(Sequence::create(moveTo,NULL));
            removeDisableMenu();
            break;
        }
    case BT_LoginAccount://账号登录
        {
            addDisableMenu();
            Node* LoginBack = getChildByTag(m_Tga_LoginBack);
            MoveTo *leftMoveBy = MoveTo::create(0.5f, Vec2(960,540));
            LoginBack->runAction(Sequence::create(leftMoveBy,NULL));
            break;
        }
    case BT_LoginWX://微信登录
        {
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
            GoodsByWeChatPay_cpp::shared()->Game_WxLogin();
#else
            LoadLayer::create(m_pGameScene);
            scheduleOnce(schedule_selector(LoginLayer::UpdateCreateLoad),1);
#endif
            g_GlobalUnits.m_UserLoginInfo.isWxUser = true;
            g_GlobalUnits.m_UserLoginInfo.isTourist = false;
            break;
        }
    case BT_LoginTourist://游客登录
        {
            //YUNG
            LoadLayer::create(m_pGameScene);
            sendLoginTouristData();
            break;
        }
	case BT_RememberPass://记住密码
		{
            bool isRemember = UserDefault::getInstance()->getBoolForKey("REMEMBER_PASSWORD");
            UserDefault::getInstance()->setBoolForKey("REMEMBER_PASSWORD", !isRemember);
            m_AutoGou->setVisible(!isRemember);
            break;
		}
    case Reg_Cancel:	//取消注册按钮
        {
            SoundUtil::sharedEngine()->playEffect("buttonMusic");
            OnExitRegister();
            break;
        }
        case Reg_Register:	//发送注册信息按钮
        {
            SoundUtil::sharedEngine()->playEffect("buttonMusic");
            sendRegister();
            break;
        }
        case BT_Register://注册
        {
            SoundUtil::sharedEngine()->playEffect("buttonMusic");
            OnExitLoginLayerToRegister();
            break;
        }
	case BT_Login:	//登陆按钮
		{
            
			SoundUtil::sharedEngine()->playEffect("buttonMusic");
			if (strlen(m_pUserName->getText()) == 0)
			{
				//账号不能为空！
				AlertMessageLayer::createConfirm(this, "\u8d26\u53f7\u4e3a\u7a7a\u662f\u65e0\u6548\u7684\u54e6~~");
				return;
			}

			if(strlen(m_pPassword->getText()) < 6 || strlen(m_pPassword->getText()) > 20)
			{
				//密码必须是6-10为数字或字母
				AlertMessageLayer::createConfirm(this, "\u5bc6\u7801\u683c\u5f0f\u4e0d\u6b63\u786e\uff0c\u5fc5\u987b\u662f6-20\u4f4d\u6570\u5b57\u6216\u5b57\u6bcd");
				return;
			}
            
			LoadLayer::create(m_pGameScene);
			sendLoginData(utf8_gbk(m_pUserName->getText()).c_str() , m_pPassword->getText());
			break;
        }
        case Bind_OK://身份证确定
        {
            const char * IdCard  = m_BindIDCard->getText();
            if (strlen(IdCard) == 0)
            {
                AlertMessageLayer::createConfirm("\u8eab\u4efd\u8bc1\u4e0d\u80fd\u4e3a\u7a7a\uff01");
                return;
            }
           
            LoadLayer::create(m_pGameScene);
            sendLoginDataToIDCard();
            break;
        }
        case Bind_Cancle://身份证取消
        {
            SoundUtil::sharedEngine()->playEffect("buttonMusic");
            OnExitBindIdCard();
            break;
        }
        case WeChatPass_OK://微信验证码确定
        {
            const char * weChatPass  = m_WeChatPass->getText();
            if (strlen(weChatPass) == 0)
            {
                AlertMessageLayer::createConfirm("\u8eab\u4efd\u8bc1\u4e0d\u80fd\u4e3a\u7a7a\uff01");
                return;
            }
            
            LoadLayer::create(m_pGameScene);
            sendLoginDataToIDCard();
            break;
        }
        case WeChatPass_Cancel://微信验证码取消
        {
            SoundUtil::sharedEngine()->playEffect("buttonMusic");
            OnExitWeChatPass();
            break;
        }
    }
}

bool LoginLayer::IDCardCheck(char* input)
{
    if (strlen(input) == 0)
    {
        AlertMessageLayer::createConfirm("\u8eab\u4efd\u8bc1\u4e0d\u80fd\u4e3a\u7a7a\uff01");
        return false;
    }
    
    return true;
}

void LoginLayer::sendRegister()
{
    if (false == checkNameNomative(m_pRegisterUserName->getText()))
        return;

    if (false == checkNameNomative(m_pNickName->getText(), true))
        return;

    if(strlen(m_pRegisterPassword1->getText()) < 6 || strlen(m_pRegisterPassword1->getText()) > 10)
    {
        //密码格式不正确，必须是6-10位数字或字母
        AlertMessageLayer::createConfirm("\u5bc6\u7801\u683c\u5f0f\u4e0d\u6b63\u786e\uff0c\u5fc5\u987b\u662f6-10\u4f4d\u6570\u5b57\u6216\u5b57\u6bcd");
        return;
    }
    
    if (std::string(m_pRegisterPassword1->getText()) != std::string(m_pRegisterPassword2->getText()))
    {
        //两次密码不一致
        AlertMessageLayer::createConfirm("\u4e24\u6b21\u5bc6\u7801\u4e0d\u4e00\u81f4");
        return;
    }
    const char* IdCard = "";
    const char* QQ = "";
    const char* phoneNum = "";
    
    //身份证
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    if (g_GlobalUnits.m_SwitchYZ == 0)
    {
        IdCard = "111111111111111111";
        QQ = "11111";
        phoneNum = "11111111111";
    }
    else
    {
        IdCard = m_pIDCard->getText();
        QQ = m_pQQ->getText();
        phoneNum = m_pPhone->getText();
        if (!IDCardCheck(const_cast<char*>(IdCard)))
            return;
    }
#else
    IdCard = m_pIDCard->getText();
    QQ = m_pQQ->getText();
    phoneNum = m_pPhone->getText();
    if (!IDCardCheck(const_cast<char*>(IdCard)))
        return;
#endif
    
    //qq
    if (strlen(QQ) == 0)
    {
        AlertMessageLayer::createConfirm("QQ\u4e0d\u80fd\u4e3a\u7a7a\uff01");
        return;
    }
    
    if (strlen(phoneNum) != 11)
    {
        AlertMessageLayer::createConfirm("\u65e0\u6548\u624b\u673a\u53f7\uff01");
        return;
    }
    
    if(LobbySocketSink::sharedSocketSink()->ConnectServer())
    {
        LoadLayer::create(m_pGameScene);
        
        const char * szUserName = m_pRegisterUserName->getText();
        const char * szNickName = m_pNickName->getText();
        const char * szPassword = m_pRegisterPassword1->getText();
        
        CMD_MB_RegisterAccounts RegisterAccounts;
        memset(&RegisterAccounts, 0x00, sizeof(RegisterAccounts));
        
        RegisterAccounts.dwPlazaVersion = 1;
        
        strcpy(RegisterAccounts.szAccounts, utf8_gbk(szUserName).c_str());
        strcpy(RegisterAccounts.szNickname, utf8_gbk(szNickName).c_str());
        strcpy(RegisterAccounts.szCardid, IdCard);
        strcpy(RegisterAccounts.szQQNumber, QQ);
        strcpy(RegisterAccounts.szPhoneNumber, phoneNum);
        
        char szMachineID[33];
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
        const char* p = const_cast<char*>(Application::getInstance()->GetUUID());
        if(p == NULL)
            sprintf(szMachineID,"%s","test");
        else
            CMD5Encrypt::EncryptData(p, szMachineID);
        
        if (0 != g_GlobalUnits.m_ChannelID)
        {
            RegisterAccounts.dwPlatform = 2000 + g_GlobalUnits.m_ChannelID;
        }
        else
            RegisterAccounts.dwPlatform = PLATFORM_APPLE;
#else
        char* p = JniSink::share()->GetDeviceID();
        if(p == NULL)
            sprintf(szMachineID,"%s","test");
        else
            CMD5Encrypt::EncryptData(p, szMachineID);
        
        if (0 != g_GlobalUnits.m_ChannelID)
        {
            RegisterAccounts.dwPlatform = 3000 + g_GlobalUnits.m_ChannelID;
        }
        else
            RegisterAccounts.dwPlatform = PLATFORM_ANDROID;
#endif
        sprintf(RegisterAccounts.szMachineID,"%s",szMachineID);
        
        char buff[50];
        memset(buff, 0, sizeof(buff));
        sprintf(buff, PWDHEAD, szPassword);
        CMD5Encrypt::EncryptData(buff, RegisterAccounts.szPassWord);
        
        memset(buff, 0, sizeof(buff));
        sprintf(buff, PWDHEAD, szPassword);
        CMD5Encrypt::EncryptData(buff, RegisterAccounts.szBankPwd);
        
        LobbySocketSink::sharedLoginSocket()->SendData(MDM_MB_LOGON, SUB_MB_REGISTER_ACCOUNTS, &RegisterAccounts, sizeof(RegisterAccounts));
        //保存账号
        g_GlobalUnits.m_UserLoginInfo.isWxUser = false;
        sprintf(g_GlobalUnits.m_UserLoginInfo.szAccounts,"%s",utf8_gbk(m_pRegisterUserName->getText()).c_str());
        sprintf(g_GlobalUnits.m_UserLoginInfo.pszPassword,"%s",m_pRegisterPassword1->getText());
    }
    else
    {
        AlertMessageLayer::createConfirm(this, _UNICODE_CONNECT_FAILT);
    }
    strcpy(g_GlobalUnits.GetGolbalUserData().szAccounts, m_pRegisterUserName->getText());
}

void LoginLayer::sendLoginDataWeChatPass()
{
    const char * szUserName = m_pUserName->getText();
    const char * szPassword = m_pPassword->getText();
    const char * weChatPass = m_WeChatPass->getText();
    
    if(LobbySocketSink::sharedSocketSink()->ConnectServer())
    {
        CMD_MB_LogonAccounts LogonByAccounts;
        memset(&LogonByAccounts, 0x0, sizeof(LogonByAccounts));
        
        LogonByAccounts.wModuleID = 0;
        LogonByAccounts.dwPlazaVersion = VER_PLAZA_FRAME;
        
        char szMachineID[33];
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
        const char* p = const_cast<char*>(Application::getInstance()->GetUUID());
        if(p == NULL)  sprintf(szMachineID,"%s","test");
        else  CMD5Encrypt::EncryptData(p, szMachineID);
        
        if (0 != g_GlobalUnits.m_ChannelID)
        {
            LogonByAccounts.dwPlatform = 2000 + g_GlobalUnits.m_ChannelID;
        }
        else
            LogonByAccounts.dwPlatform = PLATFORM_APPLE;
        
#else
        char* p = JniSink::share()->GetDeviceID();
        if(p == NULL)  sprintf(szMachineID,"%s","test");
        else  CMD5Encrypt::EncryptData(p, szMachineID);
        
        if (0 != g_GlobalUnits.m_ChannelID)
        {
            LogonByAccounts.dwPlatform = 3000 + g_GlobalUnits.m_ChannelID;
        }
        else
            LogonByAccounts.dwPlatform = PLATFORM_ANDROID;
#endif
        sprintf(LogonByAccounts.szMachineID,"%s",szMachineID);
        strcpy(LogonByAccounts.szAccounts, utf8_gbk(szUserName).c_str());
        
        char buff[50];
        memset(buff, 0, sizeof(buff));
        sprintf(buff, PWDHEAD, szPassword);
        CMD5Encrypt::EncryptData(buff, LogonByAccounts.szPassword);
        
        LogonByAccounts.cbPassPortID = 1;
        sprintf(LogonByAccounts.szPassPortID,"%s",weChatPass);
        LobbySocketSink::sharedLoginSocket()->SendData(MDM_MB_LOGON,SUB_MB_LOGON_ACCOUNTS,&LogonByAccounts,sizeof(LogonByAccounts));
        //保存账号
        g_GlobalUnits.m_UserLoginInfo.isWxUser = false;
        sprintf(g_GlobalUnits.m_UserLoginInfo.szAccounts,"%s", utf8_gbk(m_pUserName->getText()).c_str());
        sprintf(g_GlobalUnits.m_UserLoginInfo.pszPassword,"%s",szPassword);
    }
    else
    {
        AlertMessageLayer::createConfirm(this, _UNICODE_CONNECT_FAILT);
    }
}

void LoginLayer::sendLoginDataToIDCard()
{
    const char * szUserName = m_pUserName->getText();
    const char * szPassword = m_pPassword->getText();
    const char * idCard;
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    if (g_GlobalUnits.m_SwitchYZ == 0)
        idCard = "111111111111111111";
    else
        idCard = m_BindIDCard->getText();
#else
    idCard = m_BindIDCard->getText();
#endif

    if(LobbySocketSink::sharedSocketSink()->ConnectServer())
    {
        CMD_MB_LogonAccounts LogonByAccounts;
        memset(&LogonByAccounts, 0x0, sizeof(LogonByAccounts));
        
        LogonByAccounts.wModuleID = 0;
        LogonByAccounts.dwPlazaVersion = VER_PLAZA_FRAME;
        
        char szMachineID[33];
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
        const char* p = const_cast<char*>(Application::getInstance()->GetUUID());
        if(p == NULL)  sprintf(szMachineID,"%s","test");
        else  CMD5Encrypt::EncryptData(p, szMachineID);
        
        if (0 != g_GlobalUnits.m_ChannelID)
        {
            LogonByAccounts.dwPlatform = 2000 + g_GlobalUnits.m_ChannelID;
        }
        else
            LogonByAccounts.dwPlatform = PLATFORM_APPLE;
        
#else
        char* p = JniSink::share()->GetDeviceID();
        if(p == NULL)  sprintf(szMachineID,"%s","test");
        else  CMD5Encrypt::EncryptData(p, szMachineID);
        
        if (0 != g_GlobalUnits.m_ChannelID)
        {
            LogonByAccounts.dwPlatform = 3000 + g_GlobalUnits.m_ChannelID;
        }
        else
            LogonByAccounts.dwPlatform = PLATFORM_ANDROID;
#endif
        sprintf(LogonByAccounts.szMachineID,"%s",szMachineID);
        strcpy(LogonByAccounts.szAccounts, utf8_gbk(szUserName).c_str());
        
        char buff[50];
        memset(buff, 0, sizeof(buff));
        sprintf(buff, PWDHEAD, szPassword);
        CMD5Encrypt::EncryptData(buff, LogonByAccounts.szPassword);
        
        LogonByAccounts.cbPassPortID = 1;
        sprintf(LogonByAccounts.szPassPortID,"%s",idCard);
        LobbySocketSink::sharedLoginSocket()->SendData(MDM_MB_LOGON,SUB_MB_LOGON_ACCOUNTS,&LogonByAccounts,sizeof(LogonByAccounts));
        //保存账号
        g_GlobalUnits.m_UserLoginInfo.isWxUser = false;
        sprintf(g_GlobalUnits.m_UserLoginInfo.szAccounts,"%s", utf8_gbk(m_pUserName->getText()).c_str());
        sprintf(g_GlobalUnits.m_UserLoginInfo.pszPassword,"%s",szPassword);
        
    }
    else
    {
        AlertMessageLayer::createConfirm(this, _UNICODE_CONNECT_FAILT);
    }
}

Menu* LoginLayer::CreateButton(std::string szBtName ,const Vec2 &p , int tag )
{
    Menu *pBT = Tools::Button(StrToChar(szBtName+"_normal.png") , StrToChar(szBtName+"_click.png") , p, this , menu_selector(LoginLayer::callbackBt) , tag);
	return pBT;
}

void LoginLayer::OnExitBindIdCard()
{
    Node* bindBack = getChildByTag(m_Tga_BindBack_Tga);
    MoveTo *tempmove = MoveTo::create(0.5f, Vec2(960,1500));
    bindBack->runAction(Sequence::create(tempmove,NULL));
    
    Node* LoginBack = getChildByTag(m_Tga_LoginBack);
    MoveTo *leftMoveBy = MoveTo::create(0.5f, Vec2(960,540));
    LoginBack->runAction(Sequence::create(leftMoveBy,NULL));
}

void LoginLayer::OnExitWeChatPass()
{
    Node* bindBack = getChildByTag(m_Tga_WechatBack_Tga);
    MoveTo *tempmove = MoveTo::create(0.5f, Vec2(960,1500));
    bindBack->runAction(Sequence::create(tempmove,NULL));
    
    Node* LoginBack = getChildByTag(m_Tga_LoginBack);
    MoveTo *leftMoveBy = MoveTo::create(0.5f, Vec2(960,540));
    LoginBack->runAction(Sequence::create(leftMoveBy,NULL));
}

void LoginLayer::OnExitRegister()
{
    Node* bindBack = getChildByTag(m_Tga_RegBack);
    MoveTo *tempmove = MoveTo::create(0.5f, Vec2(960,2000));
    bindBack->runAction(Sequence::create(tempmove,NULL));
    
    Node* LoginBack = getChildByTag(m_Tga_LoginBack);
    MoveTo *leftMoveBy = MoveTo::create(0.5f, Vec2(960,540));
    LoginBack->runAction(Sequence::create(leftMoveBy,NULL));
}

void LoginLayer::sendLoginData( const char * szUserName , const char * szPassword )
{
	if(LobbySocketSink::sharedSocketSink()->ConnectServer())
	{
		CMD_MB_LogonAccounts LogonByAccounts;
		memset(&LogonByAccounts, 0x0, sizeof(LogonByAccounts));
        
        LogonByAccounts.wModuleID = 0;
        LogonByAccounts.dwPlazaVersion = VER_PLAZA_FRAME;
        
		strcpy(LogonByAccounts.szAccounts, szUserName);
        
        char buff[50];
        memset(buff, 0, sizeof(buff));
        sprintf(buff, PWDHEAD, szPassword);
		CMD5Encrypt::EncryptData(buff, LogonByAccounts.szPassword);
        
        char szMachineID[33];
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
        const char* p = const_cast<char*>(Application::getInstance()->GetUUID());
        if(p == NULL)  sprintf(szMachineID,"%s","test");
        else  CMD5Encrypt::EncryptData(p, szMachineID);
        
        if (0 != g_GlobalUnits.m_ChannelID)
        {
            LogonByAccounts.dwPlatform = 2000 + g_GlobalUnits.m_ChannelID;
        }
        else
            LogonByAccounts.dwPlatform = PLATFORM_APPLE;
        
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
        char* p = JniSink::share()->GetDeviceID();
        if(p == NULL)  sprintf(szMachineID,"%s","test");
        else  CMD5Encrypt::EncryptData(p, szMachineID);
        
        if (0 != g_GlobalUnits.m_ChannelID)
        {
            LogonByAccounts.dwPlatform = 3000 + g_GlobalUnits.m_ChannelID;
        }
        else
            LogonByAccounts.dwPlatform = PLATFORM_ANDROID;
#endif
        sprintf(LogonByAccounts.szMachineID,"%s",szMachineID);
        LogonByAccounts.cbPassPortID = 0;
		ZeroMemory(LogonByAccounts.szPassPortID,19);
		LobbySocketSink::sharedLoginSocket()->SendData(MDM_MB_LOGON, SUB_MB_LOGON_ACCOUNTS,&LogonByAccounts,sizeof(LogonByAccounts));
		//保存账号
		g_GlobalUnits.m_UserLoginInfo.isWxUser = false;
        g_GlobalUnits.m_UserLoginInfo.isTourist = false;
		sprintf(g_GlobalUnits.m_UserLoginInfo.szAccounts,"%s",szUserName);
		sprintf(g_GlobalUnits.m_UserLoginInfo.pszPassword,"%s",szPassword);
        sprintf(g_GlobalUnits.m_UserLoginInfo.pszMD5Password,"%s",LogonByAccounts.szPassword);
	}
	else
	{
		LoadLayer::removeLoadLayer(m_pGameScene);
		AlertMessageLayer::createConfirm(this, _UNICODE_CONNECT_FAILT);
	}
}

//YUNG
void LoginLayer::sendLoginTouristData()
{
    if(LobbySocketSink::sharedSocketSink()->ConnectServer())
    {
        CMD_MB_LogonVisitor LogonByTourist;
        memset(&LogonByTourist, 0x0, sizeof(LogonByTourist));
        
        LogonByTourist.wModuleID = 0;
        LogonByTourist.dwPlazaVersion = VER_PLAZA_FRAME;
        
        char szMachineID[33];
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
        const char* p = const_cast<char*>(Application::getInstance()->GetUUID());
        if(p == NULL)
            sprintf(szMachineID,"%s","test");
        else
            CMD5Encrypt::EncryptData(p, szMachineID);
        
        if (0 != g_GlobalUnits.m_ChannelID)
        {
            LogonByTourist.dwPlatform = 2000 + g_GlobalUnits.m_ChannelID;
        }
        else
            LogonByTourist.dwPlatform = PLATFORM_APPLE;
        
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
        char* p = JniSink::share()->GetDeviceID();
        if(p == NULL)
            sprintf(szMachineID,"%s","test");
        else
            CMD5Encrypt::EncryptData(p, szMachineID);
        
        if (0 != g_GlobalUnits.m_ChannelID)
        {
            LogonByTourist.dwPlatform = 3000 + g_GlobalUnits.m_ChannelID;
        }
        else
            LogonByTourist.dwPlatform = PLATFORM_ANDROID;
#endif
        sprintf(LogonByTourist.szMachineID,"%s",szMachineID);
        LogonByTourist.cbPassPortID = 0;
        ZeroMemory(LogonByTourist.szPassPortID,19);
    
        LobbySocketSink::sharedLoginSocket()->SendData(MDM_MB_LOGON,SUB_MB_LOGON_TOURIST,&LogonByTourist,sizeof(LogonByTourist));
        //保存账号
        g_GlobalUnits.m_UserLoginInfo.isWxUser = false;
        g_GlobalUnits.m_UserLoginInfo.isTourist = true;
    }
    else
    {
        LoadLayer::removeLoadLayer(m_pGameScene);
        AlertMessageLayer::createConfirm(this, _UNICODE_CONNECT_FAILT);
    }
}

//加载用户数据
void LoginLayer::initUserData()
{
	//获取用户数量
	TCHAR	uesercount[NAME_LEN];	
	ZeroMemory(uesercount,NAME_LEN);
	LocalDataUtil::GetUsername(uesercount, DATA_KEY_UZ_USERCount);
    UserData data;
    char szAccounts[32];
    LocalDataUtil::GetUsername(szAccounts, DATA_KEY_UZ_USERNAME);
    LocalDataUtil::GetPassword(data.pszPassword, DATA_KEY_UZ_PASSWORD);
    
    sprintf(data.szAccounts, "%s", gbk_utf8(szAccounts).c_str());
    g_GlobalUnits.m_UserDataVec.push_back(data);
}

void LoginLayer::SetEnableIME(bool able)
{
	m_pPassword->setEnabled(able);
}

void LoginLayer::SetUserName(const char* strname)
{
	m_pUserName->setText(strname);
	for (int i = 0; i < g_GlobalUnits.m_UserDataVec.size(); i++)
	{
		if (strcmp(strname, g_GlobalUnits.m_UserDataVec[i].szAccounts) == 0)
		{
			m_pPassword->setText(g_GlobalUnits.m_UserDataVec[i].pszPassword);
			return;
		}
	}
	m_pPassword->setText("");
}

bool LoginLayer::checkNameNomative(const string& text, bool isNickname)
{
    if (text == "")
    {
        if (true == isNickname)
        {
            //昵称不能为空！
            AlertMessageLayer::createConfirm(this, "\u6635\u79f0\u4e0d\u80fd\u4e3a\u7a7a");
            
        }
        else
        {
            AlertMessageLayer::createConfirm(this, "\u8d26\u53f7\u4e3a\u7a7a\u662f\u65e0\u6548\u7684\u54e6~~");
        }
        return false;
    }
    
    //在苹果环境下需转为宽字符，因一个string中文长度为3，宽字符为1
    std::wstring name_wstring;
    name_wstring.reserve(text.length());
    utf8::utf8to16(text.begin(), text.end(),std::back_inserter(name_wstring));
    
    int chiniseSize = 0;
    int engnishSize = 0;
    for (int i =0; i != name_wstring.length(); i++) {
        // 判断是否为英文字母
        if (((name_wstring[i] >=48) && (name_wstring[i]<=57)) || ((name_wstring[i] >=65) && (name_wstring[i] <=90)) || ((name_wstring[i] >=97) && (name_wstring[i] <=122))) {
            engnishSize ++;
        }
        // 是否为汉字
        if (name_wstring[i] >127) {
            chiniseSize++;
        }
    }
    
    if (false == isNickname)
    {
        if (chiniseSize > 0)
        {
            AlertMessageLayer::createConfirm(this, "账号不能含有中文！");
            return false;
        }
        return true;
    }
    
    // 全部为汉字1~6
    if (((chiniseSize > 0) && (chiniseSize < 7)) && (engnishSize ==0)) {
        log("全部汉字：%d",chiniseSize);
    }
    // 全部英文2~10
    else if (((engnishSize >1) && (engnishSize < 17)) && (chiniseSize == 0)){
        log("全部英文：%d",engnishSize);
    }
    // 汉字加英文1~6
    else if (((chiniseSize >0) && (chiniseSize < 7)) && ((engnishSize > 0) && (engnishSize <17)) && (chiniseSize*3 + engnishSize < 17)) {
        log("汉字加英文：%d + %d",chiniseSize, engnishSize);
    }
    // 长度错误
    else {
        AlertMessageLayer::createConfirm(this, "昵称长度不能超过16个英文字符或6个汉字！");
        log("长度错误！%d + %d",chiniseSize, engnishSize);
        return false;
    }
    return true;
}
