﻿#ifndef CC_JNI_HELPER_H
#define CC_JNI_HELPER_H

#include "cocos2d.h"



extern "C"
{
	//调用JAVA的接口
	//参数1：消息类型 参数2：消息内容（暂用字符串）
	void callJavaCommand(const int command, const char *szParam);
}


#endif
