﻿#ifndef _Ox2GAME_VIEW_LAYER_H_
#define _Ox2GAME_VIEW_LAYER_H_

#include "GameScene.h"
#include "FrameGameView.h"
#include "Ox2GameLogic.h"
#include "CMD_Ox2.h"

#define TOP_TITLE_POINT CCPointMake(_STANDARD_SCREEN_CENTER_.x , _STANDARD_SCREEN_CENTER_.y+230)
#define TOP_BEATDRUMLEFT_POINT CCPointMake(80 , _STANDARD_SCREEN_SIZE_.height-150+52)
#define TOP_BEATDRUMRIGHT_POINT CCPointMake(150+30 , _STANDARD_SCREEN_SIZE_.height-150+60)
#define TOP_BEAM_POINT CCPointMake(185 , _STANDARD_SCREEN_SIZE_.height-93)
#define TOP_TEXTSHZ_POINT CCPointMake(_STANDARD_SCREEN_CENTER_.x-4 , _STANDARD_SCREEN_SIZE_.height-64)
#define TOP_BANNER_POINT CCPointMake(_STANDARD_SCREEN_SIZE_.width-189+12 , _STANDARD_SCREEN_SIZE_.height-20-26-40-3)

struct GameInfo
{
	BYTE				cbItem[ROW_COUNT][LINE_COUNT];
	LONGLONG			lWinScore;
	BYTE				cbAll;
	BYTE				cbMaryCount;
};

enum
{
	Ox2_TAG_URL_ADV=0,
	Ox2_TAG_URL_SM_LOG,		//短信发送log
	Ox2_TAG_VALID
};

class CTimeTaskLayer;
class Ox2GameViewLayer : public IGameView
{
public:

	enum enTag
	{
		TAG_CELL_LINE=0,
		TAG_CELL_LIGHT,
		TAG_USER_SCORE,
		TAG_TOP_TITLE,
		TAG_TOP_BEATDRUM_LEFT,
		TAG_TOP_BEATDRUM_RIGHT,
		TAG_TOP_TEXTSHZ,
		TAG_TOP_BANNER,
		TAG_BET_VIEWBG,
		TAG_BT_ADDSCORE,
		TAG_BT_TASKBOX,
		TAG_BTN_MENU,
		TAG_BTN_HELP,
		TAG_BT_ANI_ADDSCORE,
		TAG_TASK_STATE_BUBBLE,		//任务状态气泡
		TAG_BT_ADDSCORE1,
		TAG_MAX
	};

	static Ox2GameViewLayer *create(GameScene *pGameScene);
	
	virtual ~Ox2GameViewLayer();

	virtual bool init(); 
	virtual void onEnter();
	virtual void onExit();

	// default implements are used to call script callback if exist	
    virtual bool onTouchBegan(Touch *pTouch, Event *pEvent);
    virtual void onTouchMoved(Touch *pTouch, Event *pEvent);
    virtual void onTouchEnded(Touch *pTouch, Event *pEvent);

	// TextField 触发
	virtual bool onTextFieldAttachWithIME(TextFieldTTF*pSender);
	virtual bool onTextFieldDetachWithIME(TextFieldTTF*pSender);
	virtual bool onTextFieldInsertText(TextFieldTTF *pSender, const char *text, int nLen);
	virtual bool onTextFieldDeleteBackward(TextFieldTTF *pSender, const char *delText, int nLen);

	// IME 触发
	virtual void keyboardWillShow(IMEKeyboardNotificationInfo &info);
	virtual void keyboardDidShow(IMEKeyboardNotificationInfo &info);
	virtual void keyboardWillHide(IMEKeyboardNotificationInfo &info);
	virtual void keyboardDidHide(IMEKeyboardNotificationInfo &info);

	// 响应菜单快捷键
//	virtual void keyMenuClicked();
	virtual void backLoginView(Ref *pSender);
//	virtual void keyBackClicked();

	wstring s2ws1(const string& s);
	std::string WStrToUTF81(const std::wstring& str);
	void WStrToUTF81(std::string& dest, const wstring& src);
	string AddCommaToNum(LONG Num);

	//初始化
	void InitGame();
	void AddPlayerInfo();
	void AddButton();
	Menu* CreateButton(std::string szBtName ,const Vec2 &p , int tag);

	// 按钮事件管理
	void DialogConfirm(Ref *pSender);
	void DialogCancel(Ref *pSender);

	void callbackBt( Ref *pSender );

	void hiddenAll();

	virtual void DrawUserScore();	//绘制玩家分数

	virtual void GameEnd();

	virtual void ShowAddScoreBtn(bool bShow=true);

	virtual void UpdateDrawUserScore();		//更新显示的游戏币数量


	//网络接口
public:
	void OnEventUserLeave( tagUserData * pUserData, WORD wChairID, bool bLookonUser );
	//用户进入
	virtual void OnEventUserEnter(tagUserData * pUserData, WORD wChairID, bool bLookonUser);
	//用户积分
	virtual void OnEventUserScore(tagUserData * pUserData, WORD wChairID, bool bLookonUser);
	//用户状态
	virtual void OnEventUserStatus(tagUserData * pUserData, WORD wChairID, bool bLookonUser);
    //用户换桌
    virtual void OnEventUserChangeTable(tagUserData * pUserData, WORD wChairID, bool bLookonUser){};
    
    
	//游戏消息
	virtual bool OnGameMessage(WORD wSubCmdID, const void * pBuffer, WORD wDataSize);
	//场景消息
	virtual bool OnGameSceneMessage(BYTE cbGameStatus, const void * pBuffer, WORD wDataSize);

	virtual void OnReconnectAction(){};

	void OnQuit();

protected:	
	Ox2GameViewLayer(GameScene *pGameScene);

protected:
	Ox2GameLogic		m_GameLogic;
	MenuItemSprite*     m_pAddScoreBT;	//充值“+”号按钮
	Sprite*             m_BackSpr;
	Animate*			m_KuangAnimate;
	Menu*				m_BtnReadyPlay;		//开始按钮
	Menu*				m_BtnBackToLobby;	//返回按钮
	Menu*				m_BtnOpenCard;		//开牌按钮
	Menu*				m_BtnSeting;		//设置按钮
	Menu*				m_AutoOkBtn;		//托管
	Menu*				m_AutoCancleBtn;	//取消托管
	Menu*				m_CallBankYesBtn;
	Menu*				m_CallBankNoBtn;
	Sprite*             m_TG_BackSpr;
	Menu*				m_TG_5f1Btn;
	Menu*				m_TG_MaxBtn;
	Menu*				m_TG_2f1Btn;
	Menu*				m_TG_5f4Btn;
	Menu*				m_TG_RandBtn;
	Menu*				m_TG_CancelBtn;
	Menu*				m_BtnGetScoreBtn;

	Vec2				m_HeadPos[GAME_PLAYER];
	Vec2				m_ReadyPos[GAME_PLAYER];
	Vec2				m_GoldPos[GAME_PLAYER];
	Vec2				m_OpenPos[GAME_PLAYER];
	Vec2				m_CardType[GAME_PLAYER];
	Vec2				m_CardTypeNone[GAME_PLAYER];
	Vec2				m_BankTitlePos[GAME_PLAYER];

	LONGLONG			m_lTurnMaxScore;
	LONGLONG			m_lCellScore;
	bool				m_cbDynamicJoin;

	BYTE				m_HandCardData[MAX_COUNT];
	WORD				m_wBankerUser;
	WORD				m_wViewChairID[GAME_PLAYER];

	int					m_UpCardIndex;		 //正上方牌的索引
	int					m_LeftCardIndex1;	 //左上1
	int					m_LeftCardIndex2;	 //左上2
	int					m_DownCardIndex;	 //自己的牌索引
	int					m_RightCardIndex1;	 //左上1
	int					m_RightCardIndex2;	 //左上2

	//发牌后的索引 
	int					m_MyHandIndex;
	int					m_SendCardIndex;
	BYTE				m_MyHandCardData[5];

	Sprite*             m_OpenCardSpr[GAME_PLAYER];//开牌标志
	Sprite*             m_ClockSpr;

	Sprite*             m_GoldBtnSpr[4];

	int					m_StartTime;
	bool				m_bHaveNN;
	bool				m_bAuto;
	bool				m_TG_BackShow;
	int					m_TG_BetType;
	bool				m_TG_bRand;
	//tga
	int					m_ReadyTga[GAME_PLAYER];
	int					m_HeadTga[GAME_PLAYER];
	int					m_GlodTga[GAME_PLAYER];
	int					m_NickNameTga[GAME_PLAYER];
	int					m_PlayerCardTypeTga[GAME_PLAYER];
	int					m_BetTga[GAME_PLAYER];
	int					m_SendCardTga[GAME_PLAYER];
	int					m_DisCardTga[GAME_PLAYER];
	int					m_MyMoveOverCardTga;
	int					m_OpenCardTga[GAME_PLAYER];
	int					m_CallBankTga[GAME_PLAYER]; //叫庄标志
	int					m_GoldBtnTga[4];
	int					m_BankTga;
	int					m_TsTga;
public:
	static void reset();

	// 防止直接引用 
	Ox2GameViewLayer(const Ox2GameViewLayer&);
    Ox2GameViewLayer& operator = (const Ox2GameViewLayer&);

protected:
	GameInfo				m_GameInfo;


//发送网络消息

public:
	void SendGameStart();

	void SendReqTaskConfig();

	void SendReqTaskStatus();

	void SendReqRcvTask(int iIndex);	//领取任务奖励

	void SendReqRcvTaskByTaskID(WORD wTaskID);	//领取任务奖励

	void SendReqTransProp();

	void SendSendProp(DWORD dwRcvUserID ,WORD wPropID , WORD wAmount , LONG lPayscore);

	void SendOpenCard();

	void SendReqSendRecord();

	//托管
	void AutoOpenCard(float dt);

	void AutoStartGame(float dt);

	void AutoCallBank(float dt);

	void AutoBet(float dt);
    
    void addBetResult(LONGLONG score, Vec2 pos);
//接收网络消息
protected:
	bool OnBet(const void * pBuffer, WORD wDataSize);

	bool OnBetFail();

	//用户叫庄
	bool OnSubCallBanker(const void * pBuffer, WORD wDataSize);
	//游戏开始
	bool OnSubGameStart(const void * pBuffer, WORD wDataSize);
	//用户加注
	bool OnSubAddScore(const void * pBuffer, WORD wDataSize);
    //加注结果
    bool OnSubAddScoreRes(const void * pBuffer, WORD wDataSize);
    //发牌
	bool OnSubSendCard(const void * pBuffer, WORD wDataSize);
	//开牌
	bool OnSubOpenCard(const void * pBuffer, WORD wDataSize);
	//结束
	bool OnSubGameEnd(const void * pBuffer, WORD wDataSize);

	void dispatchCards(WORD chair, BYTE card);

	void CardMoveCallback();

	string GetCardStringName(BYTE card);

	void SendCard(float dt);

	void SetMyCardData(BYTE cbCardData[], BYTE cbCardCount);

	void SetPlayerCardData(WORD wchair, BYTE cbCardData[], BYTE cbCardCount);

	void SetPlayerCardDataNone(WORD wchair, BYTE cbCardData[], BYTE cbCardCount);

	void UpdateTime(float fp);

	void StopTime();

	void StartTime(int _time);

	void AddTitle(const char* _str = NULL);
};

#endif
