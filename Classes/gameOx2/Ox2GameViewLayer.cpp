﻿#include "Ox2GameViewLayer.h"
#include "LobbyLayer.h"
#include "ClientSocketSink.h"
#include "HXmlParse.h"
#include "LocalDataUtil.h"
#include "JniSink.h"
#include "VisibleRect.h"
#include "HttpConstant.h"
#include "Screen.h"
#include "LobbySocketSink.h"
#include "LoginLayer.h"
#include "GameLayerMove.h"
#include "Convert.h"

#define		MENU_INIT_POINT			(Vec2Make(-_STANDARD_SCREEN_CENTER_.x+68+30 , _STANDARD_SCREEN_CENTER_.y-68-20))

#define		ADV_SIZE				(CCSizeMake(520,105))
//宝箱坐标
#define		BOX_POINT				(Vec2(1195,70))

//最少筹码数
#define		MIN_PROP_PAY_SCORE			1
#define		ALL_ANIMATION_COUNT			5


#define		MAX_CREDIT					(9999999)
#define		MAX_BET						(9999)
#define		MAX_ADV_COUNT				(10)

#if (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
#define		BIG_AWARD_MULITY			10
#else
#define		BIG_AWARD_MULITY			30
#endif // _DEBUG

#define Btn_Ready						1
#define Btn_BackToLobby					2
#define Btn_OpenCard					3
#define Btn_Seting						4
#define Btn_AutoOk						5
#define Btn_AutoCancle					6
#define Btn_CallBankYesBtn				7
#define Btn_CallBankNoBtn				8
#define Btn_BetBtn						9	//9~12

#define Btn_TG_Max						15
#define Btn_TG_5f4						16
#define Btn_TG_2f1						17
#define Btn_TG_5f1						18
#define Btn_TG_Rand						19
#define Btn_TG_Cancel					20
#define Btn_GetScoreBtn					21


Ox2GameViewLayer::Ox2GameViewLayer(GameScene *pGameScene)
	:IGameView(pGameScene)
{
	ZeroMemory(&m_GameInfo , sizeof(m_GameInfo));
	m_GameState=enGameNormal;
	m_BtnReadyPlay = NULL;
	m_UpCardIndex = 0;
	m_LeftCardIndex1 = 0;
	m_LeftCardIndex2 = 0;
	m_RightCardIndex1 = 0;
	m_RightCardIndex2 = 0;
	m_DownCardIndex = 0;
	ZeroMemory(m_HandCardData,sizeof(m_HandCardData));
	m_MyHandIndex = 0;
	m_SendCardIndex = 0;
	ZeroMemory(m_MyHandCardData,sizeof(m_MyHandCardData));

	m_OpenCardSpr[0] = NULL;
	m_OpenCardSpr[1] = NULL;
	m_StartTime = 15;
	m_bHaveNN = false;
	m_cbDynamicJoin = false;
	m_bAuto = false;
	m_TG_BackSpr = NULL;
	m_TG_BackShow = false;

	m_TG_BackSpr = NULL;
	m_TG_5f1Btn = NULL;
	m_TG_MaxBtn = NULL;
	m_TG_2f1Btn = NULL;
	m_TG_5f4Btn = NULL;
	m_TG_RandBtn = NULL;
	m_TG_CancelBtn = NULL;
	m_TG_BetType = 0;
	m_TG_bRand = false;
	SetKindId(Ox2KIND_ID);
}

Ox2GameViewLayer::~Ox2GameViewLayer() 
{

}

Ox2GameViewLayer *Ox2GameViewLayer::create(GameScene *pGameScene)
{
    Ox2GameViewLayer *pLayer = new Ox2GameViewLayer(pGameScene);
    if(pLayer && pLayer->init())
    {
        pLayer->autorelease();
        return pLayer;
    }
    else
    {
        CC_SAFE_DELETE(pLayer);
        return NULL;
    }
}

bool Ox2GameViewLayer::init()
{
	if ( !Layer::init() )
	{
		return false;
	}
	setLocalZOrder(3);

    Tools::addSpriteFrame("Ox2/GameBack.plist" );
    Tools::addSpriteFrame("Common/CardSprite2.plist");
	SoundUtil::sharedEngine()->playBackMusic("Ox2/BACK_Ox2", true);
    SoundUtil::sharedEngine()->setBackSoundVolume(g_GlobalUnits.m_fBackMusicValue);
    SoundUtil::sharedEngine()->setSoundVolume(g_GlobalUnits.m_fSoundValue);
	IGameView::onEnter();
	JniSink::share()->setIGameView(this);
	setGameStatus(GS_FREE);

	InitGame();
	AddButton();

	AddPlayerInfo();
	setTouchEnabled(true);

    auto listener = EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(true);
    listener->onTouchBegan = CC_CALLBACK_2(Ox2GameViewLayer::onTouchBegan, this);
    listener->onTouchMoved = CC_CALLBACK_2(Ox2GameViewLayer::onTouchMoved, this);
    listener->onTouchEnded = CC_CALLBACK_2(Ox2GameViewLayer::onTouchEnded, this);
    _eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);
	return true;
}

void Ox2GameViewLayer::onEnter()
{	
	
}

void Ox2GameViewLayer::StartTime(int _time)
{
	m_StartTime = _time;
	schedule(schedule_selector(Ox2GameViewLayer::UpdateTime), 1);
	m_ClockSpr->setVisible(true);
	UpdateTime(m_StartTime);
}

void Ox2GameViewLayer::StopTime()
{
	unschedule(schedule_selector(Ox2GameViewLayer::UpdateTime));
	m_ClockSpr->removeAllChildren();
	m_ClockSpr->setVisible(false);
}

void Ox2GameViewLayer::UpdateTime(float fp)
{
	if (m_StartTime <= 0)
	{
		unschedule(schedule_selector(Ox2GameViewLayer::UpdateTime));
		if (m_GameState == enGameNormal ||
			m_GameState == enGameEnd)
		{
			GameLayerMove::sharedGameLayerMoveSink()->CloseSeting();
			RemoveAlertMessageLayer();
			SoundUtil::sharedEngine()->stopAllEffects();
			SoundUtil::sharedEngine()->stopBackMusic();
			ClientSocketSink::sharedSocketSink()->LeftGameReq();
			GameLayerMove::sharedGameLayerMoveSink()->CloseGetScore();
			ClientSocketSink::sharedSocketSink()->setFrameGameView(NULL);
			GameLayerMove::sharedGameLayerMoveSink()->GoGameToTable();
			
		}
		else if (m_GameState == enGameCallBank)
		{
			if(m_cbDynamicJoin == true) return;
			if(m_CallBankYesBtn->isVisible())
			{
				CMD_C_GameSetBank CallBanker;
				CallBanker.bBankUser = 1;
				SendData(SUB_C_SETBANK,&CallBanker,sizeof(CallBanker));
			}
			m_CallBankYesBtn->setVisible(false);
			m_CallBankNoBtn->setVisible(false);
		}
		else if (m_GameState == enGameBet)
		{
			if(m_cbDynamicJoin == true) return;

			Node * temp= getChildByTag(m_GoldBtnTga[0]);
			if (temp  == NULL) return;
			if(temp->isVisible() == false) return;

			LONGLONG lUserMaxScore;
			lUserMaxScore= m_lTurnMaxScore/4?m_lTurnMaxScore/4:1L;
			CMD_C_GameBet AddScore;
			AddScore.lBet=lUserMaxScore;
			SendData(SUB_C_BET,&AddScore,sizeof(AddScore));
            addBetResult(lUserMaxScore, m_GoldPos[1]);
			for (int i = 0; i < 4; i++)
			{
				Node * temp= getChildByTag(m_GoldBtnTga[i]);
				if (temp != NULL) temp->setVisible(false);
			}
		}
		else if (m_GameState == enGameSendCard ||
				 m_GameState == enGameOpenCard)
		{
			if(m_cbDynamicJoin == true) return;
			if (m_BtnOpenCard->isVisible())
			{
				SendOpenCard();
			}
		}
		return;
	}

	m_ClockSpr->removeAllChildren();
    std::string str = StringUtils::toString(m_StartTime);
    if (m_StartTime < 10)
        str = "0" + str;
    LabelAtlas *time = LabelAtlas::create(str, "Common/time.png", 33, 50, '+');
    time->setPosition(Vec2(22, 15));
    m_ClockSpr->addChild(time);

	m_StartTime--;

}

void Ox2GameViewLayer::onExit()
{
	Tools::removeSpriteFrameCache("Ox2/GameBack.plist");
	Tools::removeSpriteFrameCache("Common/CardSprite2.plist");
	IGameView::onExit();
}

// Touch 触发
bool Ox2GameViewLayer::onTouchBegan(Touch *pTouch, Event *pEvent)
{
	if (m_GameState == enGameBet)
	{
		//图片按钮
		Vec2 touchLocation = pTouch->getLocation();
		Vec2 localPos = convertToNodeSpace(touchLocation);
		for (int i = 0; i < 4; i++)
		{
			Node * temp= getChildByTag(m_GoldBtnTga[i]);
			if (temp == NULL) continue;
			if(temp->isVisible() == false) continue;
			Rect rc = Rect(m_GoldBtnSpr[i]->getPositionX() -
			m_GoldBtnSpr[i]->getContentSize().width * m_GoldBtnSpr[i]->getAnchorPoint().x,
			m_GoldBtnSpr[i]->getPositionY() - m_GoldBtnSpr[i]->getContentSize().height * m_GoldBtnSpr[i]->getAnchorPoint().y,
			m_GoldBtnSpr[i]->getContentSize().width, m_GoldBtnSpr[i]->getContentSize().height);
			bool isTouched = rc.containsPoint(localPos);
			if (isTouched)
			{
				for (int ic = 0; ic < 4; ic++)
				{
					Node * temp= getChildByTag(m_GoldBtnTga[ic]);
					if (temp != NULL)m_GoldBtnSpr[ic]->setVisible(false);
				}

				LONGLONG lUserMaxScore[4];
				ZeroMemory(lUserMaxScore,sizeof(lUserMaxScore));
				for (int ik =0; ik <4;ik++)
				{
                    LONGLONG lTemp = m_lTurnMaxScore/(ik+1);
					lUserMaxScore[ik]= lTemp > 1 ? lTemp : 1;
				}

				CMD_C_GameBet AddScore;
				AddScore.lBet=lUserMaxScore[i];
				AddScore.wCurrentUser = 1;
				
				SendData(SUB_C_BET,&AddScore,sizeof(AddScore));
                addBetResult(lUserMaxScore[i], m_GoldPos[1]);
				SoundUtil::sharedEngine()->playEffect("ADD_SCORE"); 
				return true;
			}
		}
	}


	return true;
}

void Ox2GameViewLayer::onTouchMoved(Touch *pTouch, Event *pEvent)
{
}

void Ox2GameViewLayer::onTouchEnded(Touch*pTouch, Event*pEvent)
{
}

// TextField 触发
bool Ox2GameViewLayer::onTextFieldAttachWithIME(TextFieldTTF *pSender)
{
	return false;
}

bool Ox2GameViewLayer::onTextFieldDetachWithIME(TextFieldTTF *pSender)
{
	return false;
}

bool Ox2GameViewLayer::onTextFieldInsertText(TextFieldTTF *pSender, const char *text, int nLen)
{
	return true;
}

bool Ox2GameViewLayer::onTextFieldDeleteBackward(TextFieldTTF *pSender, const char *delText, int nLen)
{
	return true;
}

// IME 触发
void Ox2GameViewLayer::keyboardWillShow(IMEKeyboardNotificationInfo& info)
{
}

void Ox2GameViewLayer::keyboardDidShow(IMEKeyboardNotificationInfo& info)
{
}

void Ox2GameViewLayer::keyboardWillHide(IMEKeyboardNotificationInfo& info)
{
    MoveTo *pMovAction = MoveTo::create(0.15f, Vec2::ZERO);
	Action *pActSeq = Sequence::create(pMovAction, NULL);
	runAction(pActSeq);
}

void Ox2GameViewLayer::keyboardDidHide(IMEKeyboardNotificationInfo& info)
{
}

// 按钮事件管理
// Alert Message 确认消息处理
void Ox2GameViewLayer::DialogConfirm(Ref *pSender)
{
	this->RemoveAlertMessageLayer();

	// 设定 触发
	this->SetAllTouchEnabled(true);
}

// Alert Message 取消消息处理
void Ox2GameViewLayer::DialogCancel(Ref *pSender)
{
	// 清除 Alert Message Layer
	this->RemoveAlertMessageLayer();

	// 设定 触发
	this->SetAllTouchEnabled(true);
}

void Ox2GameViewLayer::DrawUserScore()
{
}

void Ox2GameViewLayer::OnEventUserScore( tagUserData * pUserData, WORD wChairID, bool bLookonUser )
{
	if (m_GameState == enGameNormal)
	{
		//m_pUserGameInfo->lCredit = m_pUserGameInfo->lScore;
		//updateBet(m_pUserGameInfo->lBet , m_pUserGameInfo->lLine);
		//DrawUserScore();
	}	
	IGameView::OnEventUserScore(pUserData, wChairID, bLookonUser);
    AddPlayerInfo();
}

void Ox2GameViewLayer::OnEventUserStatus(tagUserData * pUserData, WORD wChairID, bool bLookonUser)
{
	IGameView::OnEventUserStatus(pUserData, wChairID, bLookonUser);
	AddPlayerInfo();
}

void Ox2GameViewLayer::OnEventUserEnter( tagUserData * pUserData, WORD wChairID, bool bLookonUser )
{
	AddPlayerInfo();
}

void Ox2GameViewLayer::OnEventUserLeave( tagUserData * pUserData, WORD wChairID, bool bLookonUser )
{
	AddPlayerInfo();
}

//游戏消息
bool Ox2GameViewLayer::OnGameMessage(WORD wSubCmdID, const void * pBuffer, WORD wDataSize)
{
	CCLOG("RECEIVE MESSAGE CMD[%d]" , wSubCmdID);
	switch (wSubCmdID)
	{
	case Ox2_SUB_S_BANK:	//用户叫庄
		return OnSubCallBanker(pBuffer,wDataSize);

	case Ox2_SUB_S_GAME_START:	//游戏开始
		return OnSubGameStart(pBuffer, wDataSize);

	case Ox2_SUB_S_BET:	//用户下注
        return OnSubAddScore(pBuffer,wDataSize);
            
    case SUB_S_ADD_SCORE:	//下注结果
        return OnSubAddScoreRes(pBuffer,wDataSize);
	
    case Ox2_SUB_S_SEND_CARD:	//发牌消息
		return OnSubSendCard(pBuffer, wDataSize);

	case Ox2_SUB_S_OPERATE_CARD:	//用户摊牌
		return OnSubOpenCard(pBuffer, wDataSize);

	case Ox2_SUB_S_GAME_END:	//游戏结束
		return OnSubGameEnd(pBuffer, wDataSize);
	}
	return true;
}

//场景消息
bool Ox2GameViewLayer::OnGameSceneMessage(BYTE cbGameStatus, const void * pBuffer, WORD wDataSize)
{
	setGameStatus(cbGameStatus);
	
	switch (getGameStatus())
	{
	case GS_FREE:
		{
			if (m_BtnReadyPlay != NULL)	m_BtnReadyPlay->setVisible(true);
            if (m_CallBankNoBtn != NULL)	m_CallBankNoBtn->setVisible(false);
            if (m_CallBankYesBtn != NULL)	m_CallBankYesBtn->setVisible(false);
			return true;
		}
	case Ox2_GS_TK_CALL:	// 叫庄状态
		{
			if (wDataSize!=sizeof(Ox2_CMD_S_StatusBank)) return false;
			Ox2_CMD_S_StatusBank * pStatusCall=(Ox2_CMD_S_StatusBank *)pBuffer;

			//用户信息
			for (WORD i=0;i<GAME_PLAYER;i++)
			{
				//视图位置
				m_wViewChairID[i]=g_GlobalUnits.SwitchViewChairID(i);
				int wtable = ClientSocketSink::sharedSocketSink()->GetMeUserData()->wTableID;
				const tagUserData* pUserData = ClientSocketSink::sharedSocketSink()->m_UserManager.GetUserData(wtable,i);
				if (pUserData == NULL) continue;
			}

			StartTime(5);
			m_GameState = enGameCallBank;
			SoundUtil::sharedEngine()->playEffect("GAME_START"); 
			removeAllChildByTag(m_MyMoveOverCardTga);
			for (int i = 0; i < GAME_PLAYER; i++)
			{
				removeChildByTag(m_ReadyTga[i]);
				SetPlayerCardData(0,0,0);
				removeAllChildByTag(m_PlayerCardTypeTga[i]);
				removeAllChildByTag(m_SendCardTga[i]);
				removeAllChildByTag(m_DisCardTga[i]);
				removeAllChildByTag(m_BetTga[i]);
				removeAllChildByTag(m_CallBankTga[i]);
			}
			for (int i = 0; i < 4; i++)
			{
				removeAllChildByTag(m_GoldBtnTga[i]);
			}

			AddTitle("\u7b49\u5f85\u73a9\u5bb6\u53eb\u5e84");
			if (m_BtnReadyPlay != NULL)
			{
				m_BtnReadyPlay->setVisible(false);
			}
			SetMyCardData(0,0);

			m_UpCardIndex = 0;
			m_LeftCardIndex1 = 0;
			m_LeftCardIndex2 = 0;
			m_RightCardIndex1 = 0;
			m_RightCardIndex2 = 0;
			m_DownCardIndex = 0;
			ZeroMemory(m_HandCardData,sizeof(m_HandCardData));
			m_MyHandIndex = 0;
			m_SendCardIndex = 0;
			ZeroMemory(m_MyHandCardData,sizeof(m_MyHandCardData));
			m_bHaveNN = false;
			//开始抢庄
			if (pStatusCall->wCallBanker == GetMeChairID())
			{
				if (m_bAuto)
				{
					m_CallBankYesBtn->setVisible(false);
					m_CallBankNoBtn->setVisible(false);
					schedule(schedule_selector(Ox2GameViewLayer::AutoCallBank), 1);
				}
				else
				{
					m_CallBankYesBtn->setVisible(true);
					m_CallBankNoBtn->setVisible(true);
				}
			}
			else
			{
				m_CallBankYesBtn->setVisible(false);
				m_CallBankNoBtn->setVisible(false);
			}

			return true;
		}
	case Ox2_GS_TK_SCORE:	//下注状态
		{
			if (wDataSize!=sizeof(Ox2_CMD_S_StatusBet)) return false;
			Ox2_CMD_S_StatusBet * pStatusScore=(Ox2_CMD_S_StatusBet *)pBuffer;
			m_GameState = enGameBet;
			//设置变量
			m_lTurnMaxScore=pStatusScore->lTurnMaxScore;
			m_wBankerUser=pStatusScore->wBankUser;	
			LONGLONG lTableScore[GAME_PLAYER];
			CopyMemory(lTableScore,pStatusScore->lBet,sizeof(lTableScore));
			//游戏信息

			WORD chair = g_GlobalUnits.SwitchViewChairID(m_wBankerUser);
			if (chair != 1) //自己不是庄家
			{
				if(m_cbDynamicJoin == true) return true;
				LONGLONG lUserMaxScore[4];
				ZeroMemory(lUserMaxScore,sizeof(lUserMaxScore));

				for (int ik=0;ik<4;ik++)
				{
                    LONGLONG lTemp = m_lTurnMaxScore/(ik+1);
					lUserMaxScore[ik]= lTemp > 1 ? lTemp:1;
				}
				for (int i = 0; i< 4; i++)
				{
					removeAllChildByTag(m_GoldBtnTga[i]);
					m_GoldBtnSpr[i] = Sprite::createWithSpriteFrameName("jetton.png");
					m_GoldBtnSpr[i]->setPosition(Vec2(480+i*320,390));
					addChild(m_GoldBtnSpr[i]);
					m_GoldBtnSpr[i]->setTag(m_GoldBtnTga[i]);
					char strcc[32]="";
					sprintf(strcc,"%lld", lUserMaxScore[i]);
                     LabelAtlas* font = LabelAtlas::create(strcc, "Common/time_2.png", 39, 51, '+');
                    font->setAnchorPoint(Vec2(0.5f, 0.5f));
					font->setPosition(Vec2(150, 37));
					m_GoldBtnSpr[i]->addChild(font);
				}
			}

			StartTime(15);
			return true;
		}
	case Ox2_GS_TK_PLAYING:
		{
			if (m_BtnReadyPlay != NULL)	m_BtnReadyPlay->setVisible(false);
			//效验数据
			if (wDataSize!=sizeof(Ox2_CMD_S_StatusPlay)) return false;
			Ox2_CMD_S_StatusPlay * pStatusPlay=(Ox2_CMD_S_StatusPlay *)pBuffer;
			m_lTurnMaxScore=pStatusPlay->lTurnMaxScore;
			m_wBankerUser=pStatusPlay->wBankUser;

			for (WORD i=0;i<GAME_PLAYER;i++)
			{
				m_wViewChairID[i]=g_GlobalUnits.SwitchViewChairID(i);
				int wtable = ClientSocketSink::sharedSocketSink()->GetMeUserData()->wTableID;
				const tagUserData* pUserData = ClientSocketSink::sharedSocketSink()->m_UserManager.GetUserData(wtable,i);
				if (pUserData == NULL) continue;
			}

			for (int i = 0; i < 2; i++)
			{
				if (pStatusPlay->lBet[i] <= 0) continue;
				WORD chair = m_wViewChairID[i];
				Sprite* temp = Sprite::createWithSpriteFrameName("Goldback_normal.png");
				temp->setPosition(m_GoldPos[chair]);
				temp->setTag(m_BetTga[chair]);
				addChild(temp);
				char strcc[32]="";
				LONGLONG lMon = pStatusPlay->lBet[i];
				memset(strcc , 0 , sizeof(strcc));
				Tools::AddComma(lMon , strcc);
                removeAllChildByTag(m_GoldBtnTga[i]);
                LabelAtlas* font = LabelAtlas::create(strcc, "Common/gold.png", 23, 32, '+');
                font->setAnchorPoint(Vec2(0.5f, 0.5f));
                font->setPosition(Vec2(150, 30));
				temp->addChild(font);
			}
			

			ZeroMemory(m_HandCardData,sizeof(m_HandCardData));
			CopyMemory(m_HandCardData,pStatusPlay->bCardData,sizeof(m_HandCardData));
			m_GameState = enGameSendCard;

			//设置扑克
			for (int i = 0; i < 5; i++)
			{
				for (WORD j= 0 ;j< GAME_PLAYER;j++)
				{
                    WORD chair = m_wViewChairID[j];
      
                    //有牌
                    if(m_HandCardData[i]!=0)
                    {
                        if (chair == 0)
                        {
                            Sprite* temp = Sprite::createWithSpriteFrameName("Ox2backCard.png");
                            temp->setTag(m_SendCardTga[0]);
                            addChild(temp);
                            temp->setPosition(Vec2(_STANDARD_SCREEN_CENTER_.x-120+m_UpCardIndex++*35, 615));
                        }
                        else
                        {
                            string _name = GetCardStringName(m_HandCardData[i]);
                            Sprite* temp = Sprite::createWithSpriteFrameName(_name.c_str());
                            temp->setPosition(Vec2(_STANDARD_SCREEN_CENTER_.x-90+i*60,175));
                            temp->setTag(m_MyMoveOverCardTga);		
                            addChild(temp);		
                        }
                    
					}
				}
			}

			for (WORD i=0;i<GAME_PLAYER;i++)
			{
				WORD wViewChairID=m_wViewChairID[i];
				if (wViewChairID == 0)
				{
					if(pStatusPlay->bOperateType[i]!=0xff)
					{
						Sprite* temp = Sprite::createWithSpriteFrameName("opencardok.png");
						temp->setPosition(m_OpenPos[wViewChairID]);
						temp->setLocalZOrder(100);
						temp->setTag(m_OpenCardTga[wViewChairID]);
						addChild(temp);
					}
				}
				else if (wViewChairID == 1)
				{
					if(pStatusPlay->bOperateType[i]!=0xff)
					{
						Sprite* temp = Sprite::createWithSpriteFrameName("opencardok.png");
						temp->setPosition(m_OpenPos[wViewChairID]);
						temp->setLocalZOrder(100);
						temp->setTag(m_OpenCardTga[wViewChairID]);
						addChild(temp);
					}
					else
					{
						m_BtnOpenCard->setVisible(true);
					}
				}
	
			}

			return true;
		}
	}	
	return true;
}

void Ox2GameViewLayer::SetMyCardData(BYTE cbCardData[], BYTE cbCardCount)
{
	removeAllChildByTag(m_MyMoveOverCardTga);
	if (cbCardCount == 0) return;
	for (int i = 0; i < cbCardCount; i++)
	{
		string _name = GetCardStringName(cbCardData[i]);
		Sprite* temp = Sprite::createWithSpriteFrameName(_name.c_str());
		if (i > 2)
            temp->setPosition(Vec2(_STANDARD_SCREEN_CENTER_.x-90+i*60,140));
		else
            temp->setPosition(Vec2(_STANDARD_SCREEN_CENTER_.x-120 + i*60,140));
		temp->setTag(m_DisCardTga[1]);
		addChild(temp);
	}
}

void Ox2GameViewLayer::SetPlayerCardDataNone(WORD wchair, BYTE cbCardData[], BYTE cbCardCount)
{
	switch(wchair)
	{
	case 0:
		{
			removeAllChildByTag(m_SendCardTga[0]);
			if (cbCardCount == 0) return;
			for (int i = 0; i < 5; i++)
			{
				string _name = GetCardStringName(cbCardData[i]);
				Sprite* temp = Sprite::createWithSpriteFrameName(_name.c_str());
				temp->setPosition(Vec2(_STANDARD_SCREEN_CENTER_.x-120+i*60, 880));
				temp->setTag(m_DisCardTga[0]);
				addChild(temp);
			}
			break;
		}
	}
}

void Ox2GameViewLayer::addBetResult(LONGLONG score, cocos2d::Vec2 pos)
{
    Sprite* temp = Sprite::createWithSpriteFrameName("Goldback_normal.png");
    temp->setPosition(pos);
    temp->setTag(m_BetTga[0]);
    addChild(temp);
    
    char strcc[32]="";
    memset(strcc , 0 , sizeof(strcc));
    Tools::AddComma(score , strcc);
    LabelAtlas* font = LabelAtlas::create(strcc, "Common/gold.png", 23, 32, '+');
    font->setAnchorPoint(Vec2(0.5f, 0.5f));
    font->setPosition(Vec2(150, 30));
    temp->addChild(font);
}

void Ox2GameViewLayer::SetPlayerCardData(WORD wchair, BYTE cbCardData[], BYTE cbCardCount)
{
	switch(wchair)
	{
	case 0:
		{
			removeAllChildByTag(m_SendCardTga[0]);
			if (cbCardCount == 0) return;
			//上面两张
			for (int i = 3; i < 5; i++)
			{
				string _name = GetCardStringName(cbCardData[i]);
				Sprite* temp = Sprite::createWithSpriteFrameName(_name.c_str());
				temp->setPosition(Vec2(_STANDARD_SCREEN_CENTER_.x-280 + i*80, 950));
				temp->setTag(m_DisCardTga[0]);
				addChild(temp);
			}
			//下面3张
			for (int i = 0; i < 3; i++)
			{
				string _name = GetCardStringName(cbCardData[i]);
				Sprite* temp = Sprite::createWithSpriteFrameName(_name.c_str());
				temp->setPosition(Vec2(_STANDARD_SCREEN_CENTER_.x-80+i*80, 880));
				temp->setTag(m_DisCardTga[0]);
				addChild(temp);
			}
			break;
		}
	}
}

void Ox2GameViewLayer::SendGameStart()
{
    removeAllChildByTag(m_TsTga);
    removeAllChildByTag(m_MyMoveOverCardTga);
    for (int i = 0; i < GAME_PLAYER; i++)
    {
        removeChildByTag(m_ReadyTga[i]);
        removeAllChildByTag(m_BetTga[i]);
        removeAllChildByTag(m_CallBankTga[i]);
    }
    for (int i = 0; i < 4; i++)
    {
        removeAllChildByTag(m_GoldBtnTga[i]);
    }
   
    if (m_BtnReadyPlay != NULL)
    {
        m_BtnReadyPlay->setVisible(false);
    }
    
    m_UpCardIndex = 0;
    m_LeftCardIndex1 = 0;
    m_LeftCardIndex2 = 0;
    m_RightCardIndex1 = 0;
    m_RightCardIndex2 = 0;
    m_DownCardIndex = 0;
    
	ClientSocketSink::sharedSocketSink()->SendData(MDM_GF_FRAME, SUB_GF_USER_READY);
	if (m_BtnReadyPlay != NULL)
	{
		m_BtnReadyPlay->setVisible(false);
	}
	StopTime();
}

void Ox2GameViewLayer::SendOpenCard()
{
	m_BtnOpenCard->setVisible(false);

	BYTE cardc[5];
	for (int n = 0; n < 5; n++)
	{
		cardc[n] = m_MyHandCardData[n];
	}
	BYTE bCardType=m_GameLogic.GetCardType(cardc,5);

	if(bCardType > 0)
	{
		//扑克数据
		BYTE bCardData[MAX_COUNT];
		CopyMemory(bCardData,cardc,sizeof(bCardData));
		//获取牛数据：并且进行排序
		bool bOx=m_GameLogic.GetOxCardEx(bCardData,MAX_COUNT);
		
		SetMyCardData(bCardData,5);

		__String* ns=__String::createWithFormat("niu_%d.png",bCardType);
		Sprite* temp;
		if (bCardType == 15) //炸弹
		{
			temp = Sprite::createWithSpriteFrameName("niu_zd.png");
		}
		else if (bCardType == 14) //五小
		{
			temp = Sprite::createWithSpriteFrameName("niu_5x.png");
		}
		else if (bCardType == 13) //金牛
		{
			temp = Sprite::createWithSpriteFrameName("niu_j.png");
		}
		else if (bCardType == 12) //银牛
		{
			temp = Sprite::createWithSpriteFrameName("niu_j.png");
		}
		else if (bCardType >= 10)
		{
			temp = Sprite::createWithSpriteFrameName("niuniu.png");
		}
		else
		{
			temp = Sprite::createWithSpriteFrameName(ns->getCString());
		}
		temp->setPosition(m_CardType[1]);
		temp->setTag(m_PlayerCardTypeTga[1]);
		addChild(temp);
	}
	else
	{
		Sprite* temp = Sprite::createWithSpriteFrameName("wuniu.png");
		temp->setPosition(m_CardType[1]);
		temp->setTag(m_PlayerCardTypeTga[1]);
		addChild(temp);
	}
	//发送消息
	const tagUserData * pUserInfo = GetMeUserData();
	CMD_C_OperateCard OxCard;
    OxCard.wCurrentUser = 1;
    OxCard.nCardCount = 5;
    ZeroMemory(OxCard.byCardData, sizeof(OxCard.byCardData));
    CopyMemory(OxCard.byCardData, m_HandCardData, sizeof(m_HandCardData));
    
	SendData(SUB_C_OPERATE_CARD,&OxCard,sizeof(OxCard));
}

bool Ox2GameViewLayer::OnBet( const void * pBuffer, WORD wDataSize )
{
	return true;
}

bool Ox2GameViewLayer::OnBetFail()
{
	//下注失败，您再试试吧
	AlertMessageLayer::createConfirm(this , "\u4e0b\u6ce8\u5931\u8d25\uff0c\u60a8\u518d\u8bd5\u8bd5\u5427");
	return true;
}

void Ox2GameViewLayer::AddTitle(const char* _str)
{
	removeAllChildByTag(m_TsTga);
	if (_str != NULL)
	{
        ui::Scale9Sprite*temp = ui::Scale9Sprite::createWithSpriteFrameName("Goldback_normal.png");
        temp->setContentSize(Size(500,80));
		temp->setPosition(Vec2(960,600));
		temp->setTag(m_TsTga);
		addChild(temp);
        Label *font = Label::createWithSystemFont(_str, _GAME_FONT_NAME_1_, 50);
        font->setColor(Color3B::WHITE);
        font->setAnchorPoint(Vec2(0.5f, 0.5f));
        font->setPosition(Vec2(250, 40));
		temp->addChild(font);
	}
}

//用户叫庄
bool Ox2GameViewLayer::OnSubCallBanker(const void * pBuffer, WORD wDataSize)
{
	//效验数据
	if (wDataSize!=sizeof(Ox2_CMD_S_CallBank)) return false;
	Ox2_CMD_S_CallBank * pCallBanker=(Ox2_CMD_S_CallBank *)pBuffer;

    //用户信息
    for (WORD i=0; i < GAME_PLAYER; i++)
    {
        //视图位置
        m_wViewChairID[i]=g_GlobalUnits.SwitchViewChairID(i);

        int wtable = ClientSocketSink::sharedSocketSink()->GetMeUserData()->wTableID;
        const tagUserData* pUserData = ClientSocketSink::sharedSocketSink()->m_UserManager.GetUserData(wtable,i);
        if (pUserData == NULL) continue;
    }

    StartTime(10);
    m_GameState = enGameCallBank;
    SoundUtil::sharedEngine()->playEffect("GAME_START"); 
    removeAllChildByTag(m_TsTga);
    
    //开始抢庄
    if (pCallBanker->wCallBanker == GetMeChairID())
    {
        if (m_bAuto)
        {
            m_CallBankYesBtn->setVisible(false);
            m_CallBankNoBtn->setVisible(false);
            schedule(schedule_selector(Ox2GameViewLayer::AutoCallBank), 1);
        }
        else
        {
            m_CallBankYesBtn->setVisible(true);
            m_CallBankNoBtn->setVisible(true);
        }
    }
    else
    {
        AddTitle("\u7b49\u5f85\u73a9\u5bb6\u53eb\u5e84");
        m_CallBankYesBtn->setVisible(false);
        m_CallBankNoBtn->setVisible(false);
    }

	return true;
}

bool Ox2GameViewLayer::OnSubGameStart( const void * pBuffer, WORD wDataSize )
{
	//效验数据
	if (wDataSize!=sizeof(Ox2_CMD_S_GameStart)) return false;
	Ox2_CMD_S_GameStart * pGameStart=(Ox2_CMD_S_GameStart *)pBuffer;
	m_lTurnMaxScore = pGameStart->lMaxScore;
	WORD chair = g_GlobalUnits.SwitchViewChairID(pGameStart->wCurrentUser);
	m_GameState = enGameCallBank;
    
	removeAllChildByTag(m_BankTga);
    SetPlayerCardData(0,0,0);
    SetMyCardData(0,0);
    
    for (int i = 0; i < GAME_PLAYER; i++)
	{
        removeAllChildByTag(m_SendCardTga[i]);
        removeAllChildByTag(m_DisCardTga[i]);
        removeAllChildByTag(m_PlayerCardTypeTga[i]);
		removeAllChildByTag(m_CallBankTga[i]);
        m_wViewChairID[i]=g_GlobalUnits.SwitchViewChairID(i);
	}
	StartTime(10);
    
    if (chair == 1)
    {
        m_CallBankNoBtn->setVisible(true);
        m_CallBankYesBtn->setVisible(true);
    }
    else
    {
        AddTitle("\u7b49\u5f85\u73a9\u5bb6\u53eb\u5e84");
    }

    ZeroMemory(m_HandCardData,sizeof(m_HandCardData));
    m_MyHandIndex = 0;
    m_SendCardIndex = 0;
    ZeroMemory(m_MyHandCardData,sizeof(m_MyHandCardData));
    m_bHaveNN = false;
   
    return true;
}

//用户下注
bool Ox2GameViewLayer::OnSubAddScore(const void * pBuffer, WORD wDataSize)
{
	//效验数据
	if (wDataSize!=sizeof(Ox2_CMD_S_GameBet)) return false;
	Ox2_CMD_S_GameBet * pAddScore=(Ox2_CMD_S_GameBet *)pBuffer;
    
    m_lTurnMaxScore = pAddScore->lMaxAddScore;
    m_wBankerUser = pAddScore->wBankerUser;

    removeAllChildByTag(m_TsTga);
    m_GameState = enGameBet;
    StartTime(10);
    //视图位置
	WORD chair = g_GlobalUnits.SwitchViewChairID(pAddScore->wBankerUser);
    Sprite* temp = Sprite::createWithSpriteFrameName("bank.png");
    temp->setPosition(m_BankTitlePos[chair]);
    temp->setTag(m_BankTga);
    addChild(temp);
    
    WORD wCurrentUser = g_GlobalUnits.SwitchViewChairID(pAddScore->wCurrentUser);
    if (chair == wCurrentUser)
    {
        AddTitle("\u7b49\u5f85\u73a9\u5bb6\u4e0b\u6ce8");
        return true;
    }
    
    if(m_cbDynamicJoin == true)
        return true;
    
    if (m_bAuto)
    {
        schedule(schedule_selector(Ox2GameViewLayer::AutoBet), 1);
    }
    else
    {
        LONGLONG lUserMaxScore[4];
        ZeroMemory(lUserMaxScore,sizeof(lUserMaxScore));

        for (int ik=0;ik<4;ik++)
        {
            LONGLONG lTemp=m_lTurnMaxScore / (ik + 1);
            lUserMaxScore[ik]= lTemp > 1 ? lTemp:1;
        }

        for (int i = 0; i< 4; i++)
        {
            removeAllChildByTag(m_GoldBtnTga[i]);
            m_GoldBtnSpr[i] = Sprite::createWithSpriteFrameName("Jetton.png");
            m_GoldBtnSpr[i]->setPosition(Vec2(480+i*320,390));
            addChild(m_GoldBtnSpr[i]);
            m_GoldBtnSpr[i]->setTag(m_GoldBtnTga[i]);
            char strcc[32]="";
            sprintf(strcc,"%lld", lUserMaxScore[i]);
            LabelAtlas* font = LabelAtlas::create(strcc, "Common/time_2.png", 39, 51, '+');
            font->setAnchorPoint(Vec2(0.5f, 0.5f));
            font->setPosition(Vec2(150, 37));
            m_GoldBtnSpr[i]->addChild(font);
        }
    }

    SoundUtil::sharedEngine()->playEffect("ADD_SCORE"); 
	return true;
}

//下注结果
bool Ox2GameViewLayer::OnSubAddScoreRes(const void * pBuffer, WORD wDataSize)
{
    //效验数据
    if (wDataSize!=sizeof(Ox2_CMD_S_GameBetRes)) return false;
    Ox2_CMD_S_GameBetRes * pAddScore=(Ox2_CMD_S_GameBetRes *)pBuffer;
    
    removeAllChildByTag(m_TsTga);
    
    addBetResult(pAddScore->lAddScore, m_GoldPos[0]);
    SoundUtil::sharedEngine()->playEffect("ADD_SCORE");
    return true;
}

//游戏结束
bool Ox2GameViewLayer::OnSubGameEnd(const void * pBuffer, WORD wDataSize)
{
	//效验参数
	if (wDataSize!= sizeof(Ox2_CMD_S_GameEnd)) return false;
	Ox2_CMD_S_GameEnd * pGameEnd=(Ox2_CMD_S_GameEnd *)pBuffer;
	m_GameState = enGameEnd;
	
    StopTime();
	AddTitle("\u7b49\u5f85\u73a9\u5bb6\u51c6\u5907");

	//清理数据
	m_BtnOpenCard->setVisible(false);
	for(WORD i=0;i<GAME_PLAYER;i++)
	{
		if(m_OpenCardSpr[i] != NULL) m_OpenCardSpr[i]->setVisible(false);
		removeAllChildByTag(m_OpenCardTga[i]);
		removeAllChildByTag(m_BetTga[i]);		
	}
	
	//状态设置
	setGameStatus(GS_FREE);

	//显示牌型
    WORD chair = (GetMeChairID() + 1) % 2;
    
    BYTE cardc[5];
    for (int n = 0; n < 5; n++)
    {
        cardc[n] = pGameEnd->cbCardData[chair][n];
    }

    BYTE bCardType=m_GameLogic.GetCardType(cardc,5);
    //扑克数据
    BYTE bCardData[MAX_COUNT];
    CopyMemory(bCardData,cardc,sizeof(bCardData));
    
    //有牛
    if(bCardType > 0)
    {
        //获取牛牛数据：并且进行排序
        bool bOx=m_GameLogic.GetOxCardEx(bCardData,MAX_COUNT);
        SetPlayerCardData(0, bCardData,5);
        __String* ns=__String::createWithFormat("niu_%d.png",bCardType);
        Sprite* temp;
        if (bCardType == 15) //炸弹
        {
            temp = Sprite::createWithSpriteFrameName("niu_zd.png");
            m_bHaveNN = true;
        }
        else if (bCardType == 14) //五小
        {
            temp = Sprite::createWithSpriteFrameName("niu_5x.png");
            m_bHaveNN = true;
        }
        else if (bCardType == 13) //金牛
        {
            temp = Sprite::createWithSpriteFrameName("niu_j.png");
            m_bHaveNN = true;
        }
        else if (bCardType == 12) //银牛
        {
            temp = Sprite::createWithSpriteFrameName("niu_j.png");
            m_bHaveNN = true;
        }
        else if (bCardType >= 10)
        {
            temp = Sprite::createWithSpriteFrameName("niuniu.png");
            m_bHaveNN = true;
        }
        else
        {
            temp = Sprite::createWithSpriteFrameName(ns->getCString());
        }
        temp->setPosition(m_CardType[0]);
        temp->setTag(m_PlayerCardTypeTga[0]);
        addChild(temp);
    }
    else //没牛
    {
        SetPlayerCardDataNone(0,bCardData,5);
        Sprite* temp = Sprite::createWithSpriteFrameName("wuniu.png");
        temp->setPosition(m_CardTypeNone[0]);
        temp->setTag(m_PlayerCardTypeTga[0]);
        addChild(temp);
    }

	
	if (m_bHaveNN) //
	{
		SoundUtil::sharedEngine()->playEffect("GAME_OXOX"); 
	}
	else 
	{
		//播放声音
		int chair = ClientSocketSink::sharedSocketSink()->GetMeUserData()->wChairID;
		if (pGameEnd->lGameScore[chair]>0L)
		{
			SoundUtil::sharedEngine()->playEffect("GAME_WIN"); 
		}
		else
		{
			SoundUtil::sharedEngine()->playEffect("GAME_LOST"); 
		}
	}

	//用户信息
	for (WORD i=0;i<GAME_PLAYER;i++)
	{
		//视图位置
		m_wViewChairID[i]=g_GlobalUnits.SwitchViewChairID(i);

		int wtable = ClientSocketSink::sharedSocketSink()->GetMeUserData()->wTableID;
		const tagUserData* pUserData = ClientSocketSink::sharedSocketSink()->m_UserManager.GetUserData(wtable,i);
		if (pUserData == NULL) continue;

		Sprite* temp = Sprite::createWithSpriteFrameName("Goldback_normal.png");
		temp->setPosition(m_GoldPos[m_wViewChairID[i]]);
		temp->setTag(m_BetTga[m_wViewChairID[i]]);
		addChild(temp);
		char strc[32]="";		
    
		if (pGameEnd->lGameScore[pUserData->wChairID]>0L)
		{
			sprintf(strc,"+%lld", pGameEnd->lGameScore[pUserData->wChairID]);
		}
		else
		{
			sprintf(strc,"%lld", pGameEnd->lGameScore[pUserData->wChairID]);
		}
        LabelAtlas* font = LabelAtlas::create(strc, "Common/gold.png", 23, 32, '+');
        font->setAnchorPoint(Vec2(0.5f, 0.5f));
        font->setPosition(Vec2(150, 30));
		temp->addChild(font);
	}	

	if (m_bAuto)
	{
		schedule(schedule_selector(Ox2GameViewLayer::AutoStartGame), 2);
	}
	else
	{
		m_BtnReadyPlay->setVisible(true);
        m_CallBankYesBtn->setVisible(false);
        m_CallBankNoBtn->setVisible(false);
		StartTime(15);
	}

	return true;
}

void Ox2GameViewLayer::AutoStartGame(float dt)
{
	unschedule(schedule_selector(Ox2GameViewLayer::AutoStartGame));
	SendGameStart();
}

void Ox2GameViewLayer::AutoCallBank(float dt)
{
	unschedule(schedule_selector(Ox2GameViewLayer::AutoCallBank));
	CMD_C_GameSetBank CallBanker;
	CallBanker.bBankUser = 1;
	SendData(SUB_C_SETBANK,&CallBanker,sizeof(CallBanker));
}

void Ox2GameViewLayer::AutoBet(float dt)
{
	SoundUtil::sharedEngine()->playEffect("ADD_SCORE");
	unschedule(schedule_selector(Ox2GameViewLayer::AutoBet));
	LONGLONG lUserMaxScore[4];
	ZeroMemory(lUserMaxScore,sizeof(lUserMaxScore));
	lUserMaxScore[0]= m_lTurnMaxScore/1?m_lTurnMaxScore/1:1L;
	lUserMaxScore[1]= m_lTurnMaxScore/2?m_lTurnMaxScore/2:1L;
	lUserMaxScore[2]= m_lTurnMaxScore/3?m_lTurnMaxScore/3:1L;
	lUserMaxScore[3]= m_lTurnMaxScore/4?m_lTurnMaxScore/4:1L;
	//随机
	if (m_TG_bRand) m_TG_BetType = rand()%4;

	CMD_C_GameBet AddScore;
	AddScore.lBet=lUserMaxScore[m_TG_BetType];
	SendData(SUB_C_BET,&AddScore,sizeof(AddScore));
	for (int i = 0; i < 4; i++)
	{
		Node * temp= getChildByTag(m_GoldBtnTga[i]);
		if (temp != NULL) temp->setVisible(false);
	}
}

//用户摊牌
bool Ox2GameViewLayer::OnSubOpenCard(const void * pBuffer, WORD wDataSize)
{
	//效验数据
	if (wDataSize!=sizeof(Ox2_CMD_S_OperateCard)) return false;
	Ox2_CMD_S_OperateCard * pOpenCard=(Ox2_CMD_S_OperateCard *)pBuffer;
    
	m_GameState = enGameOpenCard;
	SoundUtil::sharedEngine()->playEffect("PASS_CARD"); 
	WORD wID=pOpenCard->wCurrentUser;
	for (WORD i=0;i<GAME_PLAYER;i++)
	{
		//视图位置
		m_wViewChairID[i]= g_GlobalUnits.SwitchViewChairID(i);
	}
	WORD wViewChairID=m_wViewChairID[wID];
	if (m_cbDynamicJoin) return true;

	if (wViewChairID == 1) return true;
	Sprite* temp = Sprite::createWithSpriteFrameName("opencardok.png");
	temp->setPosition(m_OpenPos[wViewChairID]);
	temp->setLocalZOrder(100);
	temp->setTag(m_OpenCardTga[wViewChairID]);
	addChild(temp);

	return true;
}

void Ox2GameViewLayer::SendCard(float dt)
{
	//派发扑克
	if(m_SendCardIndex >= 5)
	{
		unschedule(schedule_selector(Ox2GameViewLayer::SendCard));
		if(!m_bAuto) m_BtnOpenCard->setVisible(true);
		StartTime(10);
		return;
	}
	SoundUtil::sharedEngine()->playEffect("SEND_CARD"); 

	////派发扑克
	for (WORD j = 0; j < GAME_PLAYER; j++)
	{
        WORD wViewChairID=m_wViewChairID[j];
        
        if (wViewChairID == 1)
        {
            dispatchCards(wViewChairID, m_HandCardData[m_SendCardIndex]);
        }
        else
        {
            dispatchCards(0, 0);
        }
	}

	m_SendCardIndex++;

}

//发牌消息
bool Ox2GameViewLayer::OnSubSendCard(const void * pBuffer, WORD wDataSize)
{
	//效验数据
	if (wDataSize!=sizeof(Ox2_CMD_S_SendCard)) return false;
	Ox2_CMD_S_SendCard * pSendCard=(Ox2_CMD_S_SendCard *)pBuffer;
    
    ZeroMemory(m_HandCardData,sizeof(m_HandCardData));
    CopyMemory(m_HandCardData,pSendCard->byCardData,sizeof(m_HandCardData));
    
	m_GameState = enGameSendCard;
    StartTime(10);
	schedule(schedule_selector(Ox2GameViewLayer::SendCard), 0.1f);
	return true;
}

void Ox2GameViewLayer::dispatchCards(WORD chair, BYTE card) 
{ 
	switch (chair) 
	{
	case 0:  //正上方
		{ 
			if(m_UpCardIndex >= 5) return;
			MoveTo *leftMoveBy = MoveTo::create(0.1f, Vec2(_STANDARD_SCREEN_CENTER_.x-120 + m_UpCardIndex++*60, 880));
			Sprite* temp = Sprite::createWithSpriteFrameName("Ox2backCard.png");
			temp->setTag(m_SendCardTga[0]);
			addChild(temp);
			temp->setPosition(Vec2(_STANDARD_SCREEN_CENTER_.x,_STANDARD_SCREEN_CENTER_.y));
			temp->runAction(leftMoveBy);
			
			break; 
		} 
	case 1: 
		{
			if(m_DownCardIndex >= 5) return;
			m_MyHandCardData[m_DownCardIndex] = card;
			MoveTo *leftMoveBy = MoveTo::create(0.1f, Vec2(_STANDARD_SCREEN_CENTER_.x-120 + m_DownCardIndex++*60, 140));
			ActionInstant *func = CallFunc::create(CC_CALLBACK_0(Ox2GameViewLayer::CardMoveCallback, this));
			Sprite* temp = Sprite::createWithSpriteFrameName("Ox2backCard.png");
			temp->setTag(m_SendCardTga[1]);
			addChild(temp);
			temp->setPosition(Vec2(_STANDARD_SCREEN_CENTER_.x,_STANDARD_SCREEN_CENTER_.y));
			temp->runAction(Sequence::create(leftMoveBy,func,NULL));
			break;
		} 
	default: 
		break; 
	} 
} 

void Ox2GameViewLayer::CardMoveCallback()
{
	AddTitle("\u7b49\u5f85\u73a9\u5bb6\u5f00\u724c");
	removeChildByTag(m_SendCardTga[1]);
	string _name = GetCardStringName(m_MyHandCardData[m_MyHandIndex]);
	Sprite* temp = Sprite::createWithSpriteFrameName(_name.c_str());
	temp->setPosition(Vec2(_STANDARD_SCREEN_CENTER_.x-120+m_MyHandIndex*60,140));
	temp->setTag(m_MyMoveOverCardTga);		
	addChild(temp);
	m_MyHandIndex++;
	if(m_DownCardIndex >= 5)
	{
		if (m_bAuto)
		{
			schedule(schedule_selector(Ox2GameViewLayer::AutoOpenCard), 1);
		}
	}
}

void Ox2GameViewLayer::AutoOpenCard(float dt)
{
	unschedule(schedule_selector(Ox2GameViewLayer::AutoOpenCard));
	SendOpenCard();
}

string Ox2GameViewLayer::GetCardStringName(BYTE card)
{
	if (card == 0) return "Ox2backCard.png";
	char tt[32];
	sprintf(tt,"%0x",card);
	BYTE _value = m_GameLogic.GetCardValue(card);
	BYTE _color = m_GameLogic.GetCardColor(card);
	string temp;
	if (_color == 0) temp = "fangkuai_";
	if (_color == 1) temp = "meihua_";
	if (_color == 2) temp = "hongtao_";
	if (_color == 3) temp = "heitao_";
	char _valstr[32];
	ZeroMemory(_valstr,32);
	if (card == 0x41) //小王
	{
		sprintf(_valstr,"xiaowang.png");
	}
	else if (card == 0x42)
	{
		sprintf(_valstr,"dawang.png");
	}
	else if (_value < 10)
	{
		sprintf(_valstr,"0%d.png",_value);
	}
	else 
	{
		sprintf(_valstr,"%d.png",_value);
	}

	temp+=_valstr;
	return temp;
}

void Ox2GameViewLayer::InitGame()
{
	//背景图
	m_BackSpr = Sprite::create("Common/common_gameBg.jpg");
	m_BackSpr->setPosition(_STANDARD_SCREEN_CENTER_IN_PIXELS_);
	addChild(m_BackSpr);

    Sprite *table = Sprite::createWithSpriteFrameName("game_back.png");
    table->setPosition(_STANDARD_SCREEN_CENTER_);
    m_BackSpr->addChild(table);
    
	m_ClockSpr = Sprite::createWithSpriteFrameName("timeback.png");
	m_ClockSpr->setPosition(Vec2(_STANDARD_SCREEN_CENTER_IN_PIXELS_.x+10 ,_STANDARD_SCREEN_CENTER_IN_PIXELS_.y-25));
	addChild(m_ClockSpr);
	StartTime(15);
	//tga
	for (int i = 0; i < GAME_PLAYER; i++)
	{
		m_ReadyTga[i] = 1000+i;
		m_HeadTga[i]  = 1010+i;
		m_GlodTga[i]  = 1020+i;
		m_NickNameTga[i] = 1030+i;
		m_PlayerCardTypeTga[i] = 1040+i;
		m_BetTga[i] = 1050+i;
		m_SendCardTga[i] = 1060+i;
		m_DisCardTga[i] = 1070+i;
		m_OpenCardTga[i] = 1080+i;
		m_CallBankTga[i] = 1090+i;
	}
	m_MyMoveOverCardTga = 1099;
	m_BankTga = 1099;
	m_TsTga = 1089;
	m_GoldBtnTga[0] = 1095;
	m_GoldBtnTga[1] = 1096;
	m_GoldBtnTga[2] = 1097;
	m_GoldBtnTga[3] = 1098;

    m_HeadPos[0] = Vec2(480,960);
    m_HeadPos[1] = Vec2(480,120);
    
    m_ReadyPos[0] = Vec2(750,960);
    m_ReadyPos[1] = Vec2(750,120);
    
	m_GoldPos[0] = Vec2(960,730);
	m_GoldPos[1] = Vec2(960,300);

	m_OpenPos[0] = Vec2(960,900);
	m_OpenPos[1] = Vec2(960,140);
	
	m_CardType[0] = Vec2(960,840);
	m_CardType[1] = Vec2(960,100);
	
	m_CardTypeNone[0] = Vec2(960,840);
	m_CardTypeNone[1] = Vec2(960,100);

	m_BankTitlePos[0] = Vec2(590,1030);
	m_BankTitlePos[1] = Vec2(590,190);
}

void Ox2GameViewLayer::AddButton()
{
	m_BtnReadyPlay = CreateButton("game_Start" ,Vec2(960,390),Btn_Ready);
	addChild(m_BtnReadyPlay , 0 , Btn_Ready);
	m_BtnReadyPlay->setVisible(false);

	m_BtnBackToLobby = CreateButton("Returnback" ,Vec2(90,1000),Btn_BackToLobby);
	addChild(m_BtnBackToLobby , 0 , Btn_BackToLobby);
	m_BtnBackToLobby->setVisible(true);

	m_BtnOpenCard = CreateButton("OpenCard" ,Vec2(960,390),Btn_OpenCard);
	addChild(m_BtnOpenCard , 0 , Btn_OpenCard);
	m_BtnOpenCard->setVisible(false);
	
	m_BtnSeting = CreateButton("seting" ,Vec2(1810,1000),Btn_Seting);
	addChild(m_BtnSeting , 0 , Btn_Seting);
	m_BtnSeting->setVisible(true);
	
	m_AutoOkBtn = CreateButton("AutoOk" ,Vec2(1230,60),Btn_AutoOk);
	addChild(m_AutoOkBtn , 0 , Btn_AutoOk);
	m_AutoOkBtn->setVisible(false);
	
	m_AutoCancleBtn = CreateButton("AutoCancle" ,Vec2(1230,60),Btn_AutoCancle);
	addChild(m_AutoCancleBtn , 0 , Btn_AutoCancle);
	m_AutoCancleBtn->setVisible(false);

	m_CallBankYesBtn = CreateButton("CallBankOk" ,Vec2(760,390),Btn_CallBankYesBtn);
	addChild(m_CallBankYesBtn , 0 , Btn_CallBankYesBtn);
	m_CallBankYesBtn->setVisible(false);
	
	m_CallBankNoBtn = CreateButton("CallBankCancel" ,Vec2(1160,390),Btn_CallBankNoBtn);
	addChild(m_CallBankNoBtn , 0 , Btn_CallBankNoBtn);
	m_CallBankNoBtn->setVisible(false);

	m_TG_MaxBtn = CreateButton("TG_Max" ,Vec2(395,227),Btn_TG_Max);
	addChild(m_TG_MaxBtn , 1001 , Btn_TG_Max);
	m_TG_MaxBtn->setVisible(false);
	
	m_TG_5f4Btn = CreateButton("TG5f4" ,Vec2(565,227),Btn_TG_5f4);
	addChild(m_TG_5f4Btn , 1001 , Btn_TG_5f4);
	m_TG_5f4Btn->setVisible(false);

	m_TG_2f1Btn = CreateButton("TG2f1" ,Vec2(735,227),Btn_TG_2f1);
	addChild(m_TG_2f1Btn , 1001 , Btn_TG_2f1);
	m_TG_2f1Btn->setVisible(false);

	m_TG_5f1Btn = CreateButton("TG5f1" ,Vec2(905,227),Btn_TG_5f1);
	addChild(m_TG_5f1Btn , 1001 , Btn_TG_5f1);
	m_TG_5f1Btn->setVisible(false);

	m_TG_RandBtn = CreateButton("TG_Rand" ,Vec2(505,130),Btn_TG_Rand);
	addChild(m_TG_RandBtn , 1001 , Btn_TG_Rand);
	m_TG_RandBtn->setVisible(false);

	m_TG_CancelBtn = CreateButton("TG_Cancel" ,Vec2(795,130),Btn_TG_Cancel);
	addChild(m_TG_CancelBtn , 1001 , Btn_TG_Cancel);
	m_TG_CancelBtn->setVisible(false);
}

Menu* Ox2GameViewLayer::CreateButton(std::string szBtName ,const Vec2 &p , int tag )
{
	return Tools::Button(StrToChar(szBtName+"_normal.png") , StrToChar(szBtName+"_click.png") , p, this , menu_selector(Ox2GameViewLayer::callbackBt) , tag);
}

void Ox2GameViewLayer::AddPlayerInfo()
{
	for (int i = 0; i < GAME_PLAYER; i++)
	{
		if (ClientSocketSink::sharedSocketSink()->GetMeUserData() == NULL)
		{
			return;
		}
//        auto socketSink = ClientSocketSink::sharedSocketSink();
//        auto meUserData = socketSink->GetMeUserData();
       
		int wtable = ClientSocketSink::sharedSocketSink()->GetMeUserData()->wTableID;
		if (wtable > 200) return;
		const tagUserData* pUserData = ClientSocketSink::sharedSocketSink()->m_UserManager.GetUserData(wtable,i);
		int chair = g_GlobalUnits.SwitchViewChairID(i);
		if (pUserData != NULL)
		{
			//头像
			removeAllChildByTag(m_HeadTga[chair]);
            Sprite* playerInfoBg = Sprite::createWithSpriteFrameName("bg_player_info.png");
            playerInfoBg->setPosition(m_HeadPos[chair]);
            addChild(playerInfoBg, 0, m_HeadTga[chair]);
            
            Sprite* headBg = Sprite::createWithSpriteFrameName("bg_head_img.png");
            headBg->setPosition(Vec2(70, 73));
            playerInfoBg->addChild(headBg);
            
            string heads = g_GlobalUnits.getFace(pUserData->wGender, pUserData->lScore);
            Sprite* mHead = Sprite::createWithSpriteFrameName(heads);
            mHead->setPosition(Vec2(70, 73));
            mHead->setScale(0.9f);
            playerInfoBg->addChild(mHead);
            
            //金币
            removeAllChildByTag(m_GlodTga[chair]);
            char strc[32]="";
            LONGLONG lMon = pUserData->lScore;
            memset(strc , 0 , sizeof(strc));
            Tools::AddComma(lMon , strc);
            Label* mGoldFont;
            mGoldFont = Label::createWithSystemFont(strc,_GAME_FONT_NAME_1_,30);
            mGoldFont->setTag(m_GlodTga[chair]);
            playerInfoBg->addChild(mGoldFont);
            mGoldFont->setColor(Color3B::YELLOW);
            mGoldFont->setAnchorPoint(Vec2(0, 0.5f));
            mGoldFont->setPosition(Vec2(140, 90));
            
            //昵称
            removeAllChildByTag(m_NickNameTga[chair]);
            Label* mNickfont;
            mNickfont = Label::createWithSystemFont(gbk_utf8(pUserData->szNickName),_GAME_FONT_NAME_1_,30);
            mNickfont->setTag(m_NickNameTga[chair]);
            playerInfoBg->addChild(mNickfont);
            mNickfont->setAnchorPoint(Vec2(0,0.5f));
            mNickfont->setPosition(Vec2(140, 40));
            
            //准备
            removeAllChildByTag(m_ReadyTga[chair]);
            Sprite* mReaderSpr = Sprite::createWithSpriteFrameName("game_ready.png");
            mReaderSpr->setPosition(m_ReadyPos[chair]);
            mReaderSpr->setTag(m_ReadyTga[chair]);
            addChild(mReaderSpr);
   
            if (pUserData->cbUserStatus == US_READY)
            {
				getChildByTag(m_ReadyTga[chair])->setVisible(true);
			}
			else getChildByTag(m_ReadyTga[chair])->setVisible(false);
		}
		else
		{
			if (chair == 1 && !g_GlobalUnits.m_bLeaveGameByServer) //如果是自己退出，就关闭游戏
			{
                backLoginView(nullptr);
				return;
			}
			removeAllChildByTag(m_HeadTga[chair]);
			removeAllChildByTag(m_GlodTga[chair]);
			removeAllChildByTag(m_NickNameTga[chair]);
			removeAllChildByTag(m_ReadyTga[chair]);
			removeAllChildByTag(m_SendCardTga[chair]);
		}
	}
}

string Ox2GameViewLayer::AddCommaToNum(LONG Num)
{
	char _str[256];
	sprintf(_str,"%ld", Num);
	string _string = _str;
	int step = _string.length()/3;
	for (int i = 1; i <= step; i++)
	{
		_string.insert(_string.length()-(i-1)-(i*3), ",");
	}
	return _string;
}

void Ox2GameViewLayer::SendReqTaskConfig()
{
	CMD_C_GP_GetTask GetTask;
	ZeroMemory(&GetTask , sizeof(GetTask));
	GetTask.dwUserID = g_GlobalUnits.GetGolbalUserData().dwUserID;
	GetTask.wKindID = Ox2KIND_ID;
	SendMobileData(SUB_C_GP_GETTASK , &GetTask , sizeof(GetTask));
}

void Ox2GameViewLayer::SendReqTaskStatus()
{
	CMD_C_GP_GetTaskStatus GetTask;
	ZeroMemory(&GetTask , sizeof(GetTask));
	GetTask.dwUserID = g_GlobalUnits.GetGolbalUserData().dwUserID;
	GetTask.wKindID = Ox2KIND_ID;
	SendMobileData(SUB_C_GP_TAST_STATUS , &GetTask , sizeof(GetTask));
}

void Ox2GameViewLayer::SendReqTransProp()
{
	CMD_C_GP_GetTransProps GetTransProps;
	ZeroMemory(&GetTransProps ,sizeof(GetTransProps));
	GetTransProps.wKindID = Ox2KIND_ID;
	SendMobileData(SUB_C_GP_GETTRANSPROP , &GetTransProps , sizeof(GetTransProps));
}

void Ox2GameViewLayer::SendSendProp( DWORD dwRcvUserID ,WORD wPropID , WORD wAmount , LONG lPayscore )
{
	CMD_C_GP_SendProp oSendProp;
	ZeroMemory(&oSendProp ,sizeof(oSendProp));
	oSendProp.dwUserID = g_GlobalUnits.GetUserID();
	oSendProp.dwRcvUserID = dwRcvUserID;
	oSendProp.wKindID = Ox2KIND_ID;
	oSendProp.wPropID = wPropID;
	oSendProp.lAmount = wAmount;
	oSendProp.lPayScore = lPayscore;
	oSendProp.cbPlatform = GAME_PLATFORM;
	SendMobileData(SUB_C_GP_SENDPROP , &oSendProp , sizeof(oSendProp));
}

void Ox2GameViewLayer::SendReqSendRecord()
{
	CMD_C_GP_GetSendRecord GetSendRecord;
	ZeroMemory(&GetSendRecord ,sizeof(GetSendRecord));
	GetSendRecord.dwUserID = g_GlobalUnits.GetUserID();
	GetSendRecord.wKindID = Ox2KIND_ID;
	GetSendRecord.wItemCount = SENDRECORD_COUNT;
	GetSendRecord.wPageIndex = 1;
	SendMobileData(SUB_C_GP_SENDRECORD , &GetSendRecord ,sizeof(GetSendRecord));
}

void Ox2GameViewLayer::callbackBt( CCObject *pSender )
{
	CCNode *pNode = (CCNode *)pSender;
	switch (pNode->getTag())
	{
	case Btn_GetScoreBtn:
		{	
			GetScoreForBank();
			break;
		}
	case Btn_TG_Max:	//托管下最大注
		{
			SoundUtil::sharedEngine()->playEffect("buttonMusic"); 
			m_TG_bRand = false;
			m_TG_BetType = 0;
			m_bAuto = true;
			m_AutoOkBtn->setVisible(false);
			m_AutoCancleBtn->setVisible(true);
			if (m_TG_BackSpr != NULL) m_TG_BackSpr->setVisible(false);
			if(m_TG_MaxBtn != NULL) m_TG_MaxBtn->setVisible(false);
			if(m_TG_5f4Btn != NULL) m_TG_5f4Btn->setVisible(false);
			if(m_TG_2f1Btn != NULL) m_TG_2f1Btn->setVisible(false);
			if(m_TG_5f1Btn != NULL) m_TG_5f1Btn->setVisible(false);
			if(m_TG_RandBtn != NULL) m_TG_RandBtn->setVisible(false);
			if(m_TG_CancelBtn != NULL) m_TG_CancelBtn->setVisible(false);
			m_TG_BackShow = false;
			break;
		}
	case Btn_TG_5f4:
		{
			SoundUtil::sharedEngine()->playEffect("buttonMusic");
			m_TG_bRand = false;
			m_TG_BetType = 1;
			m_bAuto = true;
			m_AutoOkBtn->setVisible(false);
			m_AutoCancleBtn->setVisible(true);	
			if (m_TG_BackSpr != NULL) m_TG_BackSpr->setVisible(false);
			if(m_TG_MaxBtn != NULL) m_TG_MaxBtn->setVisible(false);
			if(m_TG_5f4Btn != NULL) m_TG_5f4Btn->setVisible(false);
			if(m_TG_2f1Btn != NULL) m_TG_2f1Btn->setVisible(false);
			if(m_TG_5f1Btn != NULL) m_TG_5f1Btn->setVisible(false);
			if(m_TG_RandBtn != NULL) m_TG_RandBtn->setVisible(false);
			if(m_TG_CancelBtn != NULL) m_TG_CancelBtn->setVisible(false);
			m_TG_BackShow = false;
			break;
		}
	case Btn_TG_2f1:
		{
			SoundUtil::sharedEngine()->playEffect("buttonMusic");
			m_TG_bRand = false;
			m_TG_BetType = 2;
			m_bAuto = true;
			m_AutoOkBtn->setVisible(false);
			m_AutoCancleBtn->setVisible(true);
			if (m_TG_BackSpr != NULL) m_TG_BackSpr->setVisible(false);
			if(m_TG_MaxBtn != NULL) m_TG_MaxBtn->setVisible(false);
			if(m_TG_5f4Btn != NULL) m_TG_5f4Btn->setVisible(false);
			if(m_TG_2f1Btn != NULL) m_TG_2f1Btn->setVisible(false);
			if(m_TG_5f1Btn != NULL) m_TG_5f1Btn->setVisible(false);
			if(m_TG_RandBtn != NULL) m_TG_RandBtn->setVisible(false);
			if(m_TG_CancelBtn != NULL) m_TG_CancelBtn->setVisible(false);
			m_TG_BackShow = false;
			break;
		}
	case Btn_TG_5f1:
		{
			SoundUtil::sharedEngine()->playEffect("buttonMusic");
			m_TG_bRand = false;
			m_TG_BetType = 3;
			m_bAuto = true;
			m_AutoOkBtn->setVisible(false);
			m_AutoCancleBtn->setVisible(true);
			if (m_TG_BackSpr != NULL) m_TG_BackSpr->setVisible(false);
			if(m_TG_MaxBtn != NULL) m_TG_MaxBtn->setVisible(false);
			if(m_TG_5f4Btn != NULL) m_TG_5f4Btn->setVisible(false);
			if(m_TG_2f1Btn != NULL) m_TG_2f1Btn->setVisible(false);
			if(m_TG_5f1Btn != NULL) m_TG_5f1Btn->setVisible(false);
			if(m_TG_RandBtn != NULL) m_TG_RandBtn->setVisible(false);
			if(m_TG_CancelBtn != NULL) m_TG_CancelBtn->setVisible(false);
			m_TG_BackShow = false;
			break;
		}
	case Btn_TG_Rand:
		{
			SoundUtil::sharedEngine()->playEffect("buttonMusic");
			m_TG_bRand = true;
			m_TG_BetType = rand()%4;
			m_bAuto = true;
			m_AutoOkBtn->setVisible(false);
			m_AutoCancleBtn->setVisible(true);
			if (m_TG_BackSpr != NULL) m_TG_BackSpr->setVisible(false);
			if(m_TG_MaxBtn != NULL) m_TG_MaxBtn->setVisible(false);
			if(m_TG_5f4Btn != NULL) m_TG_5f4Btn->setVisible(false);
			if(m_TG_2f1Btn != NULL) m_TG_2f1Btn->setVisible(false);
			if(m_TG_5f1Btn != NULL) m_TG_5f1Btn->setVisible(false);
			if(m_TG_RandBtn != NULL) m_TG_RandBtn->setVisible(false);
			if(m_TG_CancelBtn != NULL) m_TG_CancelBtn->setVisible(false);
			m_TG_BackShow = false;
			break;
		}
	case Btn_TG_Cancel:
		{
			SoundUtil::sharedEngine()->playEffect("buttonMusic");
			if (m_TG_BackSpr != NULL) m_TG_BackSpr->setVisible(false);
			if(m_TG_MaxBtn != NULL) m_TG_MaxBtn->setVisible(false);
			if(m_TG_5f4Btn != NULL) m_TG_5f4Btn->setVisible(false);
			if(m_TG_2f1Btn != NULL) m_TG_2f1Btn->setVisible(false);
			if(m_TG_5f1Btn != NULL) m_TG_5f1Btn->setVisible(false);
			if(m_TG_RandBtn != NULL) m_TG_RandBtn->setVisible(false);
			if(m_TG_CancelBtn != NULL) m_TG_CancelBtn->setVisible(false);
			m_TG_BackShow = false;
			break;
		}
	case Btn_Ready:	//准备按钮
		{
			if(m_TG_BackShow) break;
			SendGameStart();
			break;
		}
	case Btn_BackToLobby: // 返回大厅按钮
		{
			SoundUtil::sharedEngine()->playEffect("buttonMusic");
			BackToLobby();
			break;
		}
	case Btn_OpenCard:	//开牌按钮
		{
			if(m_TG_BackShow) break;
			SendOpenCard();
			break;
		}
	case Btn_Seting: //设置
		{
			SoundUtil::sharedEngine()->playEffect("buttonMusic");
			GameLayerMove::sharedGameLayerMoveSink()->OpenSeting();
			break;
		}
	case Btn_AutoOk:	//托管
		{
			SoundUtil::sharedEngine()->playEffect("buttonMusic");
			if (m_TG_BackSpr == NULL)
			{
				m_TG_BackSpr = Sprite::createWithSpriteFrameName("TG_Back.png");
				m_TG_BackSpr->setPosition(Vec2(650,200));
				addChild(m_TG_BackSpr,1000);
			}
			else
			{
				m_TG_BackSpr->setVisible(true);
			}
			if(m_TG_MaxBtn != NULL) m_TG_MaxBtn->setVisible(true);
			if(m_TG_5f4Btn != NULL) m_TG_5f4Btn->setVisible(true);
			if(m_TG_2f1Btn != NULL) m_TG_2f1Btn->setVisible(true);
			if(m_TG_5f1Btn != NULL) m_TG_5f1Btn->setVisible(true);
			if(m_TG_RandBtn != NULL) m_TG_RandBtn->setVisible(true);
			if(m_TG_CancelBtn != NULL) m_TG_CancelBtn->setVisible(true);
			m_TG_BackShow = true;

			break;
		}		
	case Btn_AutoCancle:	//取消托管
		{
			SoundUtil::sharedEngine()->playEffect("buttonMusic");
			if (m_TG_BackSpr != NULL) m_TG_BackSpr->setVisible(false);
			if(m_TG_MaxBtn != NULL) m_TG_MaxBtn->setVisible(false);
			if(m_TG_5f4Btn != NULL) m_TG_5f4Btn->setVisible(false);
			if(m_TG_2f1Btn != NULL) m_TG_2f1Btn->setVisible(false);
			if(m_TG_5f1Btn != NULL) m_TG_5f1Btn->setVisible(false);
			if(m_TG_RandBtn != NULL) m_TG_RandBtn->setVisible(false);
			if(m_TG_CancelBtn != NULL) m_TG_CancelBtn->setVisible(false);
			m_TG_BackShow = false;
			m_TG_bRand = false;
			m_bAuto = false;
			m_AutoOkBtn->setVisible(true);
			m_AutoCancleBtn->setVisible(false);
			break;
		}
	case Btn_CallBankYesBtn: //抢庄
		{
			if(m_TG_BackShow) break;
			m_CallBankYesBtn->setVisible(false);
			m_CallBankNoBtn->setVisible(false);
			CMD_C_GameSetBank CallBanker;
			CallBanker.bBankUser = 1;
			SendData(SUB_C_SETBANK,&CallBanker,sizeof(CallBanker));
			break;
		}
	case Btn_CallBankNoBtn: //不抢庄
		{
			if(m_TG_BackShow) break;
			m_CallBankYesBtn->setVisible(false);
			m_CallBankNoBtn->setVisible(false);
			CMD_C_GameSetBank CallBanker;
			CallBanker.bBankUser = INVALID_CHAIR;
			SendData(SUB_C_SETBANK,&CallBanker,sizeof(CallBanker));
			break;
		}
	}
}

void Ox2GameViewLayer::backLoginView( Ref *pSender )
{
	IGameView::backLoginView(pSender);	
}

void Ox2GameViewLayer::GameEnd()
{
}

void Ox2GameViewLayer::ShowAddScoreBtn( bool bShow/*=true*/ )
{
	m_pAddScoreBT->setEnabled(bShow);
	removeAllChildByTag(TAG_BT_ANI_ADDSCORE);
}

void Ox2GameViewLayer::UpdateDrawUserScore()
{
}

void Ox2GameViewLayer::WStrToUTF81(std::string& dest, const wstring& src)
{
	dest.clear();
	for (size_t i = 0; i < src.size(); i++){
		wchar_t w = src[i];
		if (w <= 0x7f)
			dest.push_back((char)w);
		else if (w <= 0x7ff){
			dest.push_back(0xc0 | ((w >> 6)& 0x1f));
			dest.push_back(0x80| (w & 0x3f));
		}
		else if (w <= 0xffff){
			dest.push_back(0xe0 | ((w >> 12)& 0x0f));
			dest.push_back(0x80| ((w >> 6) & 0x3f));
			dest.push_back(0x80| (w & 0x3f));
		}
		else if (sizeof(wchar_t) > 2 && w <= 0x10ffff){
			dest.push_back(0xf0 | ((w >> 18)& 0x07)); // wchar_t 4-bytes situation
			dest.push_back(0x80| ((w >> 12) & 0x3f));
			dest.push_back(0x80| ((w >> 6) & 0x3f));
			dest.push_back(0x80| (w & 0x3f));
		}
		else
			dest.push_back('?');
	}
}
std::string Ox2GameViewLayer::WStrToUTF81(const std::wstring& str)
{
	std::string result;
	WStrToUTF81(result, str);
	return result;
}

wstring Ox2GameViewLayer::s2ws1(const string& s)
{
	setlocale(LC_ALL, "chs"); 
	const char* _Source = s.c_str();
	size_t _Dsize = s.size() + 1;
	wchar_t *_Dest = new wchar_t[_Dsize];
	wmemset(_Dest, 0, _Dsize);
	mbstowcs(_Dest,_Source,_Dsize);
	wstring result = _Dest;
	delete []_Dest;
	setlocale(LC_ALL, "C");
	return result;
}

void Ox2GameViewLayer::OnQuit()
{
	m_pGameScene->removeChild(this);
}
