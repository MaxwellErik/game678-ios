#ifndef CMD_OX2_HEAD_FILE
#define CMD_OX2_HEAD_FILE
#pragma pack(1)

#define _BIND_TO_CURRENT_VCLIBS_VERSION 1
//////////////////////////////////////////////////////////////////////////
//公共宏定义

#ifdef VIDEO_GAME
	#define Ox2KIND_ID							159																							//视频游戏ID
	#define Ox2GAME_NAME					TEXT("视频牛牛")																		//游戏名字
#else 
	#define Ox2KIND_ID							50																							//游戏 I D
	#define Ox2GAME_NAME					TEXT("牛牛")																				//游戏名字
#endif

#define MAX_TIMES						5

#define MAX_SCORE_RATE					12
#define APP_NAME						TEXT("Ox2")
#define GAME_GENRE						(GAME_GENRE_GOLD|GAME_GENRE_MATCH)							//游戏类型
#define REG_GLOBAL_OPTIONS		TEXT("Software\\ZZQPGame\\DZGameClient\\GlobalOption")		//游戏设置
#define GAME_PLAYER						2																								//游戏人数
#define MAX_COUNT						5	
#define MAX_RATE_ARRAY					10													//扑克数目
#define ME_CHAIRID						1
#define MAX_JETTON_AREA					4 

//服务器命令结构
#define Ox2_SUB_S_GAME_START				100								    //游戏开始
#define Ox2_SUB_S_BANK						101									//游戏定庄
#define Ox2_SUB_S_BET						102									//下注消息
#define SUB_S_ADD_SCORE                     103									//加注结果
#define Ox2_SUB_S_SEND_CARD					104									//发牌消息
#define Ox2_SUB_S_OPERATE_CARD				105									//用户操作
#define Ox2_SUB_S_GAME_END					106								    //游戏结束

//游戏状态
#define Ox2_GS_TK_FREE					GS_FREE									//空闲状态
#define Ox2_GS_TK_CALL                  GS_PLAYING								//抢庄状态
#define Ox2_GS_TK_SCORE					GS_PLAYING+1							//下注状态
#define Ox2_GS_TK_PLAYING				GS_PLAYING+2							//游戏进行


//空闲状态
struct Ox2_CMD_S_StatusFree
{   
	LONGLONG								lCellScore;
};

struct Ox2_CMD_S_StatusBank
{
	WORD								wCallBanker;						//叫庄用户
};

//游戏状态
struct Ox2_CMD_S_StatusBet
{
	//下注信息
	LONGLONG								lTurnMaxScore;						//最大下注
	//LONGLONG								lTurnLessScore;						//最小下注
	LONGLONG								lBet[GAME_PLAYER];			//下注数目
	WORD								wBankUser;						//庄家用户
};

//游戏状态
struct Ox2_CMD_S_StatusPlay
{
	//状态信息
	LONGLONG								lTurnMaxScore;						//最大下注
	//LONGLONG								lTurnLessScore;						//最小下注
	LONGLONG								lBet[GAME_PLAYER];			//下注数目
	WORD								wBankUser;						//庄家用户

	//扑克信息
	BYTE								bCardData[GAME_PLAYER][MAX_COUNT];//桌面扑克
	BYTE								bOperateType[GAME_PLAYER];				//牛牛数据	
	//历史积分
	//LONGLONG                            lAllTurnScore[GAME_PLAYER];			//总局得分
	//LONGLONG                            lLastTurnScore[GAME_PLAYER];		//上局得分
};


struct Ox2_CMD_S_GameStart
{
    //下注信息
    UINT32								lMaxScore;							//最大下注
    UINT32								lCellScore;							//单元下注
    UINT32								lCurrentTimes;						//当前倍数
    UINT32								lUserMaxScore;						//分数上限
    
    //用户信息
    WORD								wBankerUser;						//庄家用户
    WORD				 				wCurrentUser;						//当前玩家
};

//用户叫庄
struct Ox2_CMD_S_CallBank
{
	WORD								bStart;
    WORD								wCallBanker;						//叫庄用户
};

//用户下注
struct Ox2_CMD_S_GameBet
{
    WORD								wBankerUser;						//庄家用户
    WORD				 				wCurrentUser;						//当前玩家
    LONGLONG                            lMinAddScore;
    LONGLONG                            lMaxAddScore;
};

//下注结果
struct Ox2_CMD_S_GameBetRes
{
    WORD								wCurrentUser;						//当前用户
    WORD								wAddScoreUser;						//加注用户
    LONGLONG							lAddScore;							//加注数目
};

//发牌数据包
struct Ox2_CMD_S_SendCard
{
    WORD wCurrentUser;
    UINT nCardCount;
    BYTE byCardData[MAX_COUNT];
	//BYTE								cbCardData[GAME_PLAYER][MAX_COUNT];	//用户扑克
};

//用户摊牌
struct Ox2_CMD_S_OperateCard
{
    UINT32          bSucceed;
    WORD			wCurrentUser;							//摊牌用户
    WORD            wOpenCardUser;
};

//用户强退
struct Ox2_CMD_S_GameQuit
{
	WORD				 			wCurrentUser;						//当前用户
};

//游戏结束
struct Ox2_CMD_S_GameEnd
{
	LONGLONG							lGameScore[GAME_PLAYER];			//游戏得分
    LONGLONG							lGameTax[GAME_PLAYER];				//游戏税收
    UINT32								nCardCount[GAME_PLAYER];			//牌数目
    BYTE								cbCardData[GAME_PLAYER][MAX_COUNT];	//用户牌
    CHAR								szName[GAME_PLAYER][NAME_LEN];		// 玩家名字
};
//////////////////////////////////////////////////////////////////////////
//客户端命令结构

//游戏状态
#define SUB_C_SETBANK						1								//用户定庄
#define SUB_C_BET							2								//用户下注
#define SUB_C_OPERATE_CARD					3								//用户操作
#define SUB_C_LETBANK						5								//用户下庄

//游戏定庄
struct CMD_C_GameSetBank
{
	WORD 				 			bBankUser;						    //庄家用户
};

//游戏下注
struct CMD_C_GameBet
{
    WORD							wCurrentUser;						//加注玩家
	LONGLONG                        lBet;                               //下注数目
};

//用户摊牌
struct CMD_C_OperateCard
{
    WORD								wCurrentUser;
    UINT								nCardCount;
    BYTE								byCardData[MAX_COUNT];
};
//////////////////////////////////////////////////////////////////////////
#pragma pack()
#endif
