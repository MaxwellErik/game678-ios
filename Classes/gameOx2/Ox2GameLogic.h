﻿#ifndef _Ox2GAME_LOGIC_H_
#define _Ox2GAME_LOGIC_H_

#include "GameLayer.h"
#include <vector>

//宏定义

#define MAX_COUNT					5									//最大数目

//数值掩码
#define	LOGIC_MASK_COLOR			0xF0								//花色掩码
#define	LOGIC_MASK_VALUE			0x0F								//数值掩码

//扑克类型
#define OX_VALUE0					0									//混合牌型
#define OX_FOURKING					12									//天王牌型,银牛
#define OX_FIVEKING					13									//天王牌型，金牛
#define OX_HULU						14									//葫芦牌型
#define OX_BOMB						15									//炸弹牌型
//////////////////////////////////////////////////////////////////////////


class Ox2GameLogic
{
	//分析结构
	struct tagAnalyseResult
	{
		BYTE 							cbFourCount;						//四张数目
		BYTE 							cbThreeCount;						//三张数目
		BYTE 							cbDoubleCount;						//两张数目
		BYTE							cbSignedCount;						//单张数目
		BYTE 							cbFourLogicVolue[1];				//四张列表
		BYTE 							cbThreeLogicVolue[1];				//三张列表
		BYTE 							cbDoubleLogicVolue[2];				//两张列表
		BYTE 							cbSignedLogicVolue[5];				//单张列表
		BYTE							cbFourCardData[MAX_COUNT];			//四张列表
		BYTE							cbThreeCardData[MAX_COUNT];			//三张列表
		BYTE							cbDoubleCardData[MAX_COUNT];		//两张列表
		BYTE							cbSignedCardData[MAX_COUNT];		//单张数目
	};

private:
	static BYTE						m_cbCardListData[54];				//扑克定义

	//函数定义
public:
	//构造函数
	Ox2GameLogic();
	//析构函数
	virtual ~Ox2GameLogic();

	//类型函数
public:
	//获取当前排列的牌类型
	BYTE GetMyType(BYTE cbCardData[]);
	//获取类型
	BYTE GetCardType(BYTE cbCardData[], BYTE cbCardCount);

	bool AnalysebCardData(const BYTE cbCardData[], BYTE cbCardCount, tagAnalyseResult & AnalyseResult);
	//获取类型
	BYTE GetCardTypeMo(BYTE cbCardData[], BYTE cbCardCount);
	//获取数值
	BYTE GetCardValue(BYTE cbCardData) { return cbCardData&LOGIC_MASK_VALUE; }
	//BYTE GetCardValue(BYTE cbCardData)  { if(cbCardData&0x40)return (cbCardData&LOGIC_MASK_VALUE)+13; return cbCardData&LOGIC_MASK_VALUE; }
	//获取花色
	BYTE GetCardColor(BYTE cbCardData) {return (cbCardData&LOGIC_MASK_COLOR)>>4; }
	//获取倍数
	BYTE GetTimes(BYTE cbCardData[], BYTE cbCardCount);
	//获取牛牛
	bool GetOxCard(BYTE cbCardData[], BYTE cbCardCount);
	//获取整数
	bool IsIntValue(BYTE cbCardData[], BYTE cbCardCount);

	bool GetOxCardEx(BYTE cbCardData[], BYTE cbCardCount);
	//控制函数
public:
	//排列扑克
	void SortCardList(BYTE bCardData[], BYTE bCardCount);
	//混乱扑克
	void RandCardList(BYTE cbCardBuffer[], BYTE cbBufferCount);

	//功能函数
public:
	//逻辑数值
	BYTE GetCardLogicValue(BYTE cbCardData);
	//对比扑克
	bool CompareCard(BYTE cbFirstData[], BYTE cbNextData[], BYTE cbCardCount,bool FirstOX,bool NextOX);
	//对比扑克
	bool CompareCard(BYTE cbFirstData[], BYTE cbNextData[]);

	BYTE GetCardLogicNewValue(BYTE cbCardData);
};
#endif