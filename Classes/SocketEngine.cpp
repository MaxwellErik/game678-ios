﻿#include "SocketEngine.h"
//#include "GameFlowControl.h"
//#include "TableFrameSink.h"
//#include "Cell.h"






SocketEngine *							SocketEngine::m_pSocketEngine=NULL;
///////////////////////////////////////////////////////////////////////////////


SocketEngine::SocketEngine(void)
{
	m_pFrameGameView=NULL;
}


SocketEngine::~SocketEngine(void)
{

}

bool SocketEngine::SendData( WORD wSubCmdID )
{
	return SendData(wSubCmdID , NULL , 0);
}

bool SocketEngine::SendData( WORD wSubCmdID,const void * pData, WORD wDataSize )
{
	if (m_pFrameGameView != NULL)
	{
		return m_pFrameGameView->OnGameMessage(wSubCmdID,pData,wDataSize);
	}
	return false;
}

bool SocketEngine::SendMobileData( WORD wSubCmdID )
{
	return SendMobileData(wSubCmdID , NULL , 0);
}

bool SocketEngine::SendMobileData( WORD wSubCmdID, const void * pData, WORD wDataSize )
{
	return false;
}


bool SocketEngine::ReceiveData(WORD wMainCmdID , WORD wSubCmdID )
{
	return ReceiveData(wMainCmdID , wSubCmdID, NULL, 0 );
}

bool SocketEngine::ReceiveData(WORD wMainCmdID , WORD wSubCmdID,const void * pData, WORD wDataSize )
{
	return false;
}

void SocketEngine::setFrameGameView( FrameGameView *pFrameGameView )
{
	m_pFrameGameView = pFrameGameView;
}

SocketEngine * SocketEngine::sharedSocketEngine()
{
	if (m_pSocketEngine == NULL)
	{
		m_pSocketEngine = new SocketEngine();
	}
	return m_pSocketEngine;
}

bool SocketEngine::SendGameScene( const void * pData , WORD wDataSize )
{
	if (m_pFrameGameView != NULL)
	{
		return m_pFrameGameView->OnFrameMessage(SUB_GF_SCENE,pData,wDataSize);
	}
	return false;
}

