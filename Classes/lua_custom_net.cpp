#include "lua_custom_net.hpp"
#include "ExtentAction.h"
#include "PlatUtil.h"
#include "net/NetMessage.h"
#include "utils/CodeChange.h"
#include "utils/LLongCode.h"
#include "utils/CSVHelper.h"
#include "utils/XMLParser.h"
#include "cocos/scripting/lua-bindings/manual/tolua_fix.h"
#include "cocos/scripting/lua-bindings/manual/LuaBasicConversions.h"



int lua_custom_net_CodeChange_getUnicodeLen(lua_State* tolua_S)
{
    int argc = 0;
    CodeChange* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"CodeChange",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (CodeChange*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_custom_net_CodeChange_getUnicodeLen'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_custom_net_CodeChange_getUnicodeLen'", nullptr);
            return 0;
        }
        unsigned int ret = cobj->getUnicodeLen();
        tolua_pushnumber(tolua_S,(lua_Number)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "CodeChange:getUnicodeLen",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_custom_net_CodeChange_getUnicodeLen'.",&tolua_err);
#endif

    return 0;
}
int lua_custom_net_CodeChange_setUtf8String(lua_State* tolua_S)
{
    int argc = 0;
    CodeChange* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"CodeChange",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (CodeChange*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_custom_net_CodeChange_setUtf8String'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 2) 
    {
        const char* arg0;
        unsigned int arg1;

        std::string arg0_tmp; ok &= luaval_to_std_string(tolua_S, 2, &arg0_tmp, "CodeChange:setUtf8String"); arg0 = arg0_tmp.c_str();

        ok &= luaval_to_uint32(tolua_S, 3,&arg1, "CodeChange:setUtf8String");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_custom_net_CodeChange_setUtf8String'", nullptr);
            return 0;
        }
        cobj->setUtf8String(arg0, arg1);
        return 0;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "CodeChange:setUtf8String",argc, 2);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_custom_net_CodeChange_setUtf8String'.",&tolua_err);
#endif

    return 0;
}
int lua_custom_net_CodeChange_getUtf8Len(lua_State* tolua_S)
{
    int argc = 0;
    CodeChange* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"CodeChange",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (CodeChange*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_custom_net_CodeChange_getUtf8Len'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_custom_net_CodeChange_getUtf8Len'", nullptr);
            return 0;
        }
        unsigned int ret = cobj->getUtf8Len();
        tolua_pushnumber(tolua_S,(lua_Number)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "CodeChange:getUtf8Len",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_custom_net_CodeChange_getUtf8Len'.",&tolua_err);
#endif

    return 0;
}
int lua_custom_net_CodeChange_setUtf8Len(lua_State* tolua_S)
{
    int argc = 0;
    CodeChange* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"CodeChange",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (CodeChange*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_custom_net_CodeChange_setUtf8Len'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 1) 
    {
        unsigned int arg0;

        ok &= luaval_to_uint32(tolua_S, 2,&arg0, "CodeChange:setUtf8Len");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_custom_net_CodeChange_setUtf8Len'", nullptr);
            return 0;
        }
        cobj->setUtf8Len(arg0);
        return 0;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "CodeChange:setUtf8Len",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_custom_net_CodeChange_setUtf8Len'.",&tolua_err);
#endif

    return 0;
}
int lua_custom_net_CodeChange_getUnicodeByIndex(lua_State* tolua_S)
{
    int argc = 0;
    CodeChange* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"CodeChange",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (CodeChange*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_custom_net_CodeChange_getUnicodeByIndex'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 1) 
    {
        unsigned int arg0;

        ok &= luaval_to_uint32(tolua_S, 2,&arg0, "CodeChange:getUnicodeByIndex");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_custom_net_CodeChange_getUnicodeByIndex'", nullptr);
            return 0;
        }
        unsigned short ret = cobj->getUnicodeByIndex(arg0);
        tolua_pushnumber(tolua_S,(lua_Number)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "CodeChange:getUnicodeByIndex",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_custom_net_CodeChange_getUnicodeByIndex'.",&tolua_err);
#endif

    return 0;
}
int lua_custom_net_CodeChange_getUtf8ByteByIndex(lua_State* tolua_S)
{
    int argc = 0;
    CodeChange* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"CodeChange",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (CodeChange*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_custom_net_CodeChange_getUtf8ByteByIndex'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 1) 
    {
        unsigned int arg0;

        ok &= luaval_to_uint32(tolua_S, 2,&arg0, "CodeChange:getUtf8ByteByIndex");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_custom_net_CodeChange_getUtf8ByteByIndex'", nullptr);
            return 0;
        }
        uint16_t ret = cobj->getUtf8ByteByIndex(arg0);
        tolua_pushnumber(tolua_S,(lua_Number)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "CodeChange:getUtf8ByteByIndex",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_custom_net_CodeChange_getUtf8ByteByIndex'.",&tolua_err);
#endif

    return 0;
}
int lua_custom_net_CodeChange_setUtf8ByteByIndex(lua_State* tolua_S)
{
    int argc = 0;
    CodeChange* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"CodeChange",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (CodeChange*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_custom_net_CodeChange_setUtf8ByteByIndex'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 2) 
    {
        unsigned int arg0;
        uint16_t arg1;

        ok &= luaval_to_uint32(tolua_S, 2,&arg0, "CodeChange:setUtf8ByteByIndex");

        ok &= luaval_to_uint16(tolua_S, 3,&arg1, "CodeChange:setUtf8ByteByIndex");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_custom_net_CodeChange_setUtf8ByteByIndex'", nullptr);
            return 0;
        }
        cobj->setUtf8ByteByIndex(arg0, arg1);
        return 0;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "CodeChange:setUtf8ByteByIndex",argc, 2);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_custom_net_CodeChange_setUtf8ByteByIndex'.",&tolua_err);
#endif

    return 0;
}
int lua_custom_net_CodeChange_setUnicodeByIndex(lua_State* tolua_S)
{
    int argc = 0;
    CodeChange* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"CodeChange",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (CodeChange*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_custom_net_CodeChange_setUnicodeByIndex'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 2) 
    {
        unsigned int arg0;
        unsigned short arg1;

        ok &= luaval_to_uint32(tolua_S, 2,&arg0, "CodeChange:setUnicodeByIndex");

        ok &= luaval_to_ushort(tolua_S, 3, &arg1, "CodeChange:setUnicodeByIndex");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_custom_net_CodeChange_setUnicodeByIndex'", nullptr);
            return 0;
        }
        cobj->setUnicodeByIndex(arg0, arg1);
        return 0;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "CodeChange:setUnicodeByIndex",argc, 2);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_custom_net_CodeChange_setUnicodeByIndex'.",&tolua_err);
#endif

    return 0;
}
int lua_custom_net_CodeChange_setUnicodeLen(lua_State* tolua_S)
{
    int argc = 0;
    CodeChange* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"CodeChange",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (CodeChange*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_custom_net_CodeChange_setUnicodeLen'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 1) 
    {
        unsigned int arg0;

        ok &= luaval_to_uint32(tolua_S, 2,&arg0, "CodeChange:setUnicodeLen");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_custom_net_CodeChange_setUnicodeLen'", nullptr);
            return 0;
        }
        cobj->setUnicodeLen(arg0);
        return 0;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "CodeChange:setUnicodeLen",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_custom_net_CodeChange_setUnicodeLen'.",&tolua_err);
#endif

    return 0;
}
int lua_custom_net_CodeChange_constructor(lua_State* tolua_S)
{
    int argc = 0;
    CodeChange* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif



    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_custom_net_CodeChange_constructor'", nullptr);
            return 0;
        }
        cobj = new CodeChange();
        tolua_pushusertype(tolua_S,(void*)cobj,"CodeChange");
        tolua_register_gc(tolua_S,lua_gettop(tolua_S));
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "CodeChange:CodeChange",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_error(tolua_S,"#ferror in function 'lua_custom_net_CodeChange_constructor'.",&tolua_err);
#endif

    return 0;
}

static int lua_custom_net_CodeChange_finalize(lua_State* tolua_S)
{
    printf("luabindings: finalizing LUA object (CodeChange)");
    return 0;
}

int lua_register_custom_net_CodeChange(lua_State* tolua_S)
{
    tolua_usertype(tolua_S,"CodeChange");
    tolua_cclass(tolua_S,"CodeChange","CodeChange","",nullptr);

    tolua_beginmodule(tolua_S,"CodeChange");
        tolua_function(tolua_S,"new",lua_custom_net_CodeChange_constructor);
        tolua_function(tolua_S,"getUnicodeLen",lua_custom_net_CodeChange_getUnicodeLen);
        tolua_function(tolua_S,"setUtf8String",lua_custom_net_CodeChange_setUtf8String);
        tolua_function(tolua_S,"getUtf8Len",lua_custom_net_CodeChange_getUtf8Len);
        tolua_function(tolua_S,"setUtf8Len",lua_custom_net_CodeChange_setUtf8Len);
        tolua_function(tolua_S,"getUnicodeByIndex",lua_custom_net_CodeChange_getUnicodeByIndex);
        tolua_function(tolua_S,"getUtf8ByteByIndex",lua_custom_net_CodeChange_getUtf8ByteByIndex);
        tolua_function(tolua_S,"setUtf8ByteByIndex",lua_custom_net_CodeChange_setUtf8ByteByIndex);
        tolua_function(tolua_S,"setUnicodeByIndex",lua_custom_net_CodeChange_setUnicodeByIndex);
        tolua_function(tolua_S,"setUnicodeLen",lua_custom_net_CodeChange_setUnicodeLen);
    tolua_endmodule(tolua_S);
    std::string typeName = typeid(CodeChange).name();
    g_luaType[typeName] = "CodeChange";
    g_typeCast["CodeChange"] = "CodeChange";
    return 1;
}

static int lua_custom_net_FuncAction_finalize(lua_State* tolua_S)
{
    printf("luabindings: finalizing LUA object (FuncAction)");
    return 0;
}

int lua_register_custom_net_FuncAction(lua_State* tolua_S)
{
    tolua_usertype(tolua_S,"custom.FuncAction");
    tolua_cclass(tolua_S,"FuncAction","custom.FuncAction","cc.ActionInterval",nullptr);

    tolua_beginmodule(tolua_S,"FuncAction");
    tolua_endmodule(tolua_S);
    std::string typeName = typeid(custom::FuncAction).name();
    g_luaType[typeName] = "custom.FuncAction";
    g_typeCast["FuncAction"] = "custom.FuncAction";
    return 1;
}

int lua_custom_net_NetMessage_setMsgLen(lua_State* tolua_S)
{
    int argc = 0;
    custom::NetMessage* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"custom.NetMessage",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (custom::NetMessage*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_custom_net_NetMessage_setMsgLen'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 1) 
    {
        unsigned int arg0;

        ok &= luaval_to_uint32(tolua_S, 2,&arg0, "custom.NetMessage:setMsgLen");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_custom_net_NetMessage_setMsgLen'", nullptr);
            return 0;
        }
        cobj->setMsgLen(arg0);
        return 0;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "custom.NetMessage:setMsgLen",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_custom_net_NetMessage_setMsgLen'.",&tolua_err);
#endif

    return 0;
}
int lua_custom_net_NetMessage_clear(lua_State* tolua_S)
{
    int argc = 0;
    custom::NetMessage* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"custom.NetMessage",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (custom::NetMessage*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_custom_net_NetMessage_clear'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_custom_net_NetMessage_clear'", nullptr);
            return 0;
        }
        cobj->clear();
        return 0;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "custom.NetMessage:clear",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_custom_net_NetMessage_clear'.",&tolua_err);
#endif

    return 0;
}
int lua_custom_net_NetMessage_getMainID(lua_State* tolua_S)
{
    int argc = 0;
    custom::NetMessage* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"custom.NetMessage",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (custom::NetMessage*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_custom_net_NetMessage_getMainID'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_custom_net_NetMessage_getMainID'", nullptr);
            return 0;
        }
        unsigned int ret = cobj->getMainID();
        tolua_pushnumber(tolua_S,(lua_Number)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "custom.NetMessage:getMainID",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_custom_net_NetMessage_getMainID'.",&tolua_err);
#endif

    return 0;
}
int lua_custom_net_NetMessage_getMsgLen(lua_State* tolua_S)
{
    int argc = 0;
    custom::NetMessage* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"custom.NetMessage",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (custom::NetMessage*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_custom_net_NetMessage_getMsgLen'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_custom_net_NetMessage_getMsgLen'", nullptr);
            return 0;
        }
        unsigned int ret = cobj->getMsgLen();
        tolua_pushnumber(tolua_S,(lua_Number)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "custom.NetMessage:getMsgLen",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_custom_net_NetMessage_getMsgLen'.",&tolua_err);
#endif

    return 0;
}
int lua_custom_net_NetMessage_setClientId(lua_State* tolua_S)
{
    int argc = 0;
    custom::NetMessage* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"custom.NetMessage",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (custom::NetMessage*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_custom_net_NetMessage_setClientId'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 1) 
    {
        unsigned int arg0;

        ok &= luaval_to_uint32(tolua_S, 2,&arg0, "custom.NetMessage:setClientId");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_custom_net_NetMessage_setClientId'", nullptr);
            return 0;
        }
        cobj->setClientId(arg0);
        return 0;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "custom.NetMessage:setClientId",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_custom_net_NetMessage_setClientId'.",&tolua_err);
#endif

    return 0;
}
int lua_custom_net_NetMessage_setMainID(lua_State* tolua_S)
{
    int argc = 0;
    custom::NetMessage* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"custom.NetMessage",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (custom::NetMessage*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_custom_net_NetMessage_setMainID'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 1) 
    {
        unsigned int arg0;

        ok &= luaval_to_uint32(tolua_S, 2,&arg0, "custom.NetMessage:setMainID");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_custom_net_NetMessage_setMainID'", nullptr);
            return 0;
        }
        cobj->setMainID(arg0);
        return 0;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "custom.NetMessage:setMainID",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_custom_net_NetMessage_setMainID'.",&tolua_err);
#endif

    return 0;
}
int lua_custom_net_NetMessage_getDataAtIndex(lua_State* tolua_S)
{
    int argc = 0;
    custom::NetMessage* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"custom.NetMessage",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (custom::NetMessage*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_custom_net_NetMessage_getDataAtIndex'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 1) 
    {
        unsigned int arg0;

        ok &= luaval_to_uint32(tolua_S, 2,&arg0, "custom.NetMessage:getDataAtIndex");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_custom_net_NetMessage_getDataAtIndex'", nullptr);
            return 0;
        }
        uint16_t ret = cobj->getDataAtIndex(arg0);
        tolua_pushnumber(tolua_S,(lua_Number)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "custom.NetMessage:getDataAtIndex",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_custom_net_NetMessage_getDataAtIndex'.",&tolua_err);
#endif

    return 0;
}
int lua_custom_net_NetMessage_getClientId(lua_State* tolua_S)
{
    int argc = 0;
    custom::NetMessage* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"custom.NetMessage",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (custom::NetMessage*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_custom_net_NetMessage_getClientId'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_custom_net_NetMessage_getClientId'", nullptr);
            return 0;
        }
        unsigned int ret = cobj->getClientId();
        tolua_pushnumber(tolua_S,(lua_Number)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "custom.NetMessage:getClientId",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_custom_net_NetMessage_getClientId'.",&tolua_err);
#endif

    return 0;
}
int lua_custom_net_NetMessage_getSubId(lua_State* tolua_S)
{
    int argc = 0;
    custom::NetMessage* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"custom.NetMessage",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (custom::NetMessage*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_custom_net_NetMessage_getSubId'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_custom_net_NetMessage_getSubId'", nullptr);
            return 0;
        }
        unsigned int ret = cobj->getSubId();
        tolua_pushnumber(tolua_S,(lua_Number)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "custom.NetMessage:getSubId",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_custom_net_NetMessage_getSubId'.",&tolua_err);
#endif

    return 0;
}
int lua_custom_net_NetMessage_setSubId(lua_State* tolua_S)
{
    int argc = 0;
    custom::NetMessage* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"custom.NetMessage",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (custom::NetMessage*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_custom_net_NetMessage_setSubId'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 1) 
    {
        unsigned int arg0;

        ok &= luaval_to_uint32(tolua_S, 2,&arg0, "custom.NetMessage:setSubId");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_custom_net_NetMessage_setSubId'", nullptr);
            return 0;
        }
        cobj->setSubId(arg0);
        return 0;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "custom.NetMessage:setSubId",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_custom_net_NetMessage_setSubId'.",&tolua_err);
#endif

    return 0;
}
int lua_custom_net_NetMessage_setDataAtIndex(lua_State* tolua_S)
{
    int argc = 0;
    custom::NetMessage* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"custom.NetMessage",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (custom::NetMessage*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_custom_net_NetMessage_setDataAtIndex'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 2) 
    {
        unsigned int arg0;
        uint16_t arg1;

        ok &= luaval_to_uint32(tolua_S, 2,&arg0, "custom.NetMessage:setDataAtIndex");

        ok &= luaval_to_uint16(tolua_S, 3,&arg1, "custom.NetMessage:setDataAtIndex");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_custom_net_NetMessage_setDataAtIndex'", nullptr);
            return 0;
        }
        cobj->setDataAtIndex(arg0, arg1);
        return 0;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "custom.NetMessage:setDataAtIndex",argc, 2);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_custom_net_NetMessage_setDataAtIndex'.",&tolua_err);
#endif

    return 0;
}
int lua_custom_net_NetMessage_constructor(lua_State* tolua_S)
{
    int argc = 0;
    custom::NetMessage* cobj = nullptr;
    bool ok  = true;
#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif

    argc = lua_gettop(tolua_S)-1;
    do{
        if (argc == 5) {
            unsigned int arg0;
            ok &= luaval_to_uint32(tolua_S, 2,&arg0, "custom.NetMessage:NetMessage");

            if (!ok) { break; }
            unsigned int arg1;
            ok &= luaval_to_uint32(tolua_S, 3,&arg1, "custom.NetMessage:NetMessage");

            if (!ok) { break; }
            unsigned int arg2;
            ok &= luaval_to_uint32(tolua_S, 4,&arg2, "custom.NetMessage:NetMessage");

            if (!ok) { break; }
            char* arg3;
            std::string arg3_tmp; ok &= luaval_to_std_string(tolua_S, 5, &arg3_tmp, "custom.NetMessage:NetMessage"); arg3 = (char*)arg3_tmp.c_str();

            if (!ok) { break; }
            unsigned int arg4;
            ok &= luaval_to_uint32(tolua_S, 6,&arg4, "custom.NetMessage:NetMessage");

            if (!ok) { break; }
            cobj = new custom::NetMessage(arg0, arg1, arg2, arg3, arg4);
            tolua_pushusertype(tolua_S,(void*)cobj,"custom.NetMessage");
            tolua_register_gc(tolua_S,lua_gettop(tolua_S));
            return 1;
        }
    }while(0);
    ok  = true;
    do{
        if (argc == 0) {
            cobj = new custom::NetMessage();
            tolua_pushusertype(tolua_S,(void*)cobj,"custom.NetMessage");
            tolua_register_gc(tolua_S,lua_gettop(tolua_S));
            return 1;
        }
    }while(0);
    ok  = true;
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n",  "custom.NetMessage:NetMessage",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_error(tolua_S,"#ferror in function 'lua_custom_net_NetMessage_constructor'.",&tolua_err);
#endif

    return 0;
}

static int lua_custom_net_NetMessage_finalize(lua_State* tolua_S)
{
    printf("luabindings: finalizing LUA object (NetMessage)");
    return 0;
}

int lua_register_custom_net_NetMessage(lua_State* tolua_S)
{
    tolua_usertype(tolua_S,"custom.NetMessage");
    tolua_cclass(tolua_S,"NetMessage","custom.NetMessage","",nullptr);

    tolua_beginmodule(tolua_S,"NetMessage");
        tolua_function(tolua_S,"new",lua_custom_net_NetMessage_constructor);
        tolua_function(tolua_S,"setMsgLen",lua_custom_net_NetMessage_setMsgLen);
        tolua_function(tolua_S,"clear",lua_custom_net_NetMessage_clear);
        tolua_function(tolua_S,"getMainID",lua_custom_net_NetMessage_getMainID);
        tolua_function(tolua_S,"getMsgLen",lua_custom_net_NetMessage_getMsgLen);
        tolua_function(tolua_S,"setClientId",lua_custom_net_NetMessage_setClientId);
        tolua_function(tolua_S,"setMainID",lua_custom_net_NetMessage_setMainID);
        tolua_function(tolua_S,"getDataAtIndex",lua_custom_net_NetMessage_getDataAtIndex);
        tolua_function(tolua_S,"getClientId",lua_custom_net_NetMessage_getClientId);
        tolua_function(tolua_S,"getSubId",lua_custom_net_NetMessage_getSubId);
        tolua_function(tolua_S,"setSubId",lua_custom_net_NetMessage_setSubId);
        tolua_function(tolua_S,"setDataAtIndex",lua_custom_net_NetMessage_setDataAtIndex);
    tolua_endmodule(tolua_S);
    std::string typeName = typeid(custom::NetMessage).name();
    g_luaType[typeName] = "custom.NetMessage";
    g_typeCast["NetMessage"] = "custom.NetMessage";
    return 1;
}

int lua_custom_net_PlatUtil_connectServer(lua_State* tolua_S)
{
    int argc = 0;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif

#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertable(tolua_S,1,"PlatUtil",0,&tolua_err)) goto tolua_lerror;
#endif

    argc = lua_gettop(tolua_S) - 1;

    if (argc == 4)
    {
        unsigned int arg0;
        std::string arg1;
        unsigned int arg2;
        unsigned int arg3;
        ok &= luaval_to_uint32(tolua_S, 2,&arg0, "PlatUtil:connectServer");
        ok &= luaval_to_std_string(tolua_S, 3,&arg1, "PlatUtil:connectServer");
        ok &= luaval_to_uint32(tolua_S, 4,&arg2, "PlatUtil:connectServer");
        ok &= luaval_to_uint32(tolua_S, 5,&arg3, "PlatUtil:connectServer");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_custom_net_PlatUtil_connectServer'", nullptr);
            return 0;
        }
        PlatUtil::connectServer(arg0, arg1, arg2, arg3);
        return 0;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d\n ", "PlatUtil:connectServer",argc, 4);
    return 0;
#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_custom_net_PlatUtil_connectServer'.",&tolua_err);
#endif
    return 0;
}
int lua_custom_net_PlatUtil_closeServer(lua_State* tolua_S)
{
    int argc = 0;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif

#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertable(tolua_S,1,"PlatUtil",0,&tolua_err)) goto tolua_lerror;
#endif

    argc = lua_gettop(tolua_S) - 1;

    if (argc == 1)
    {
        unsigned int arg0;
        ok &= luaval_to_uint32(tolua_S, 2,&arg0, "PlatUtil:closeServer");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_custom_net_PlatUtil_closeServer'", nullptr);
            return 0;
        }
        PlatUtil::closeServer(arg0);
        return 0;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d\n ", "PlatUtil:closeServer",argc, 1);
    return 0;
#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_custom_net_PlatUtil_closeServer'.",&tolua_err);
#endif
    return 0;
}
int lua_custom_net_PlatUtil_uncompressZip(lua_State* tolua_S)
{
    int argc = 0;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif

#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertable(tolua_S,1,"PlatUtil",0,&tolua_err)) goto tolua_lerror;
#endif

    argc = lua_gettop(tolua_S) - 1;

    if (argc == 2)
    {
        std::string arg0;
        std::string arg1;
        ok &= luaval_to_std_string(tolua_S, 2,&arg0, "PlatUtil:uncompressZip");
        ok &= luaval_to_std_string(tolua_S, 3,&arg1, "PlatUtil:uncompressZip");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_custom_net_PlatUtil_uncompressZip'", nullptr);
            return 0;
        }
        bool ret = PlatUtil::uncompressZip(arg0, arg1);
        tolua_pushboolean(tolua_S,(bool)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d\n ", "PlatUtil:uncompressZip",argc, 2);
    return 0;
#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_custom_net_PlatUtil_uncompressZip'.",&tolua_err);
#endif
    return 0;
}
int lua_custom_net_PlatUtil_setMessageParsePerFrame(lua_State* tolua_S)
{
    int argc = 0;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif

#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertable(tolua_S,1,"PlatUtil",0,&tolua_err)) goto tolua_lerror;
#endif

    argc = lua_gettop(tolua_S) - 1;

    if (argc == 1)
    {
        unsigned int arg0;
        ok &= luaval_to_uint32(tolua_S, 2,&arg0, "PlatUtil:setMessageParsePerFrame");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_custom_net_PlatUtil_setMessageParsePerFrame'", nullptr);
            return 0;
        }
        PlatUtil::setMessageParsePerFrame(arg0);
        return 0;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d\n ", "PlatUtil:setMessageParsePerFrame",argc, 1);
    return 0;
#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_custom_net_PlatUtil_setMessageParsePerFrame'.",&tolua_err);
#endif
    return 0;
}
int lua_custom_net_PlatUtil_UnicodeToUtf8(lua_State* tolua_S)
{
    int argc = 0;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif

#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertable(tolua_S,1,"PlatUtil",0,&tolua_err)) goto tolua_lerror;
#endif

    argc = lua_gettop(tolua_S) - 1;

    if (argc == 1)
    {
        CodeChange* arg0;
        ok &= luaval_to_object<CodeChange>(tolua_S, 2, "CodeChange",&arg0);
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_custom_net_PlatUtil_UnicodeToUtf8'", nullptr);
            return 0;
        }
        std::string ret = PlatUtil::UnicodeToUtf8(arg0);
        tolua_pushcppstring(tolua_S,ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d\n ", "PlatUtil:UnicodeToUtf8",argc, 1);
    return 0;
#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_custom_net_PlatUtil_UnicodeToUtf8'.",&tolua_err);
#endif
    return 0;
}
int lua_custom_net_PlatUtil_getMD5String(lua_State* tolua_S)
{
    int argc = 0;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif

#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertable(tolua_S,1,"PlatUtil",0,&tolua_err)) goto tolua_lerror;
#endif

    argc = lua_gettop(tolua_S) - 1;

    if (argc == 1)
    {
        std::string arg0;
        ok &= luaval_to_std_string(tolua_S, 2,&arg0, "PlatUtil:getMD5String");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_custom_net_PlatUtil_getMD5String'", nullptr);
            return 0;
        }
        std::string ret = PlatUtil::getMD5String(arg0);
        tolua_pushcppstring(tolua_S,ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d\n ", "PlatUtil:getMD5String",argc, 1);
    return 0;
#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_custom_net_PlatUtil_getMD5String'.",&tolua_err);
#endif
    return 0;
}
int lua_custom_net_PlatUtil_Utf8ToUnicode(lua_State* tolua_S)
{
    int argc = 0;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif

#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertable(tolua_S,1,"PlatUtil",0,&tolua_err)) goto tolua_lerror;
#endif

    argc = lua_gettop(tolua_S) - 1;

    if (argc == 1)
    {
        CodeChange* arg0;
        ok &= luaval_to_object<CodeChange>(tolua_S, 2, "CodeChange",&arg0);
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_custom_net_PlatUtil_Utf8ToUnicode'", nullptr);
            return 0;
        }
        PlatUtil::Utf8ToUnicode(arg0);
        return 0;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d\n ", "PlatUtil:Utf8ToUnicode",argc, 1);
    return 0;
#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_custom_net_PlatUtil_Utf8ToUnicode'.",&tolua_err);
#endif
    return 0;
}
int lua_custom_net_PlatUtil_sendMessage(lua_State* tolua_S)
{
    int argc = 0;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif

#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertable(tolua_S,1,"PlatUtil",0,&tolua_err)) goto tolua_lerror;
#endif

    argc = lua_gettop(tolua_S) - 1;

    if (argc == 2)
    {
        unsigned int arg0;
        custom::NetMessage* arg1;
        ok &= luaval_to_uint32(tolua_S, 2,&arg0, "PlatUtil:sendMessage");
        ok &= luaval_to_object<custom::NetMessage>(tolua_S, 3, "custom.NetMessage",&arg1);
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_custom_net_PlatUtil_sendMessage'", nullptr);
            return 0;
        }
        PlatUtil::sendMessage(arg0, arg1);
        return 0;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d\n ", "PlatUtil:sendMessage",argc, 2);
    return 0;
#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_custom_net_PlatUtil_sendMessage'.",&tolua_err);
#endif
    return 0;
}
int lua_custom_net_PlatUtil_createDirectory(lua_State* tolua_S)
{
    int argc = 0;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif

#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertable(tolua_S,1,"PlatUtil",0,&tolua_err)) goto tolua_lerror;
#endif

    argc = lua_gettop(tolua_S) - 1;

    if (argc == 1)
    {
        const char* arg0;
        std::string arg0_tmp; ok &= luaval_to_std_string(tolua_S, 2, &arg0_tmp, "PlatUtil:createDirectory"); arg0 = arg0_tmp.c_str();
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_custom_net_PlatUtil_createDirectory'", nullptr);
            return 0;
        }
        bool ret = PlatUtil::createDirectory(arg0);
        tolua_pushboolean(tolua_S,(bool)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d\n ", "PlatUtil:createDirectory",argc, 1);
    return 0;
#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_custom_net_PlatUtil_createDirectory'.",&tolua_err);
#endif
    return 0;
}
static int lua_custom_net_PlatUtil_finalize(lua_State* tolua_S)
{
    printf("luabindings: finalizing LUA object (PlatUtil)");
    return 0;
}

int lua_register_custom_net_PlatUtil(lua_State* tolua_S)
{
    tolua_usertype(tolua_S,"PlatUtil");
    tolua_cclass(tolua_S,"PlatUtil","PlatUtil","",nullptr);

    tolua_beginmodule(tolua_S,"PlatUtil");
        tolua_function(tolua_S,"connectServer", lua_custom_net_PlatUtil_connectServer);
        tolua_function(tolua_S,"closeServer", lua_custom_net_PlatUtil_closeServer);
        tolua_function(tolua_S,"uncompressZip", lua_custom_net_PlatUtil_uncompressZip);
        tolua_function(tolua_S,"setMessageParsePerFrame", lua_custom_net_PlatUtil_setMessageParsePerFrame);
        tolua_function(tolua_S,"UnicodeToUtf8", lua_custom_net_PlatUtil_UnicodeToUtf8);
        tolua_function(tolua_S,"getMD5String", lua_custom_net_PlatUtil_getMD5String);
        tolua_function(tolua_S,"Utf8ToUnicode", lua_custom_net_PlatUtil_Utf8ToUnicode);
        tolua_function(tolua_S,"sendMessage", lua_custom_net_PlatUtil_sendMessage);
        tolua_function(tolua_S,"createDirectory", lua_custom_net_PlatUtil_createDirectory);
    tolua_endmodule(tolua_S);
    std::string typeName = typeid(PlatUtil).name();
    g_luaType[typeName] = "PlatUtil";
    g_typeCast["PlatUtil"] = "PlatUtil";
    return 1;
}

int lua_custom_net_LLongCode_getByteByIndex(lua_State* tolua_S)
{
    int argc = 0;
    LLongCode* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"LLongCode",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (LLongCode*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_custom_net_LLongCode_getByteByIndex'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 1) 
    {
        int arg0;

        ok &= luaval_to_int32(tolua_S, 2,(int *)&arg0, "LLongCode:getByteByIndex");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_custom_net_LLongCode_getByteByIndex'", nullptr);
            return 0;
        }
        uint16_t ret = cobj->getByteByIndex(arg0);
        tolua_pushnumber(tolua_S,(lua_Number)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "LLongCode:getByteByIndex",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_custom_net_LLongCode_getByteByIndex'.",&tolua_err);
#endif

    return 0;
}
int lua_custom_net_LLongCode_setLonglongValue(lua_State* tolua_S)
{
    int argc = 0;
    LLongCode* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"LLongCode",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (LLongCode*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_custom_net_LLongCode_setLonglongValue'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 1) 
    {
        long long arg0;

        ok &= luaval_to_long_long(tolua_S, 2,&arg0, "LLongCode:setLonglongValue");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_custom_net_LLongCode_setLonglongValue'", nullptr);
            return 0;
        }
        cobj->setLonglongValue(arg0);
        return 0;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "LLongCode:setLonglongValue",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_custom_net_LLongCode_setLonglongValue'.",&tolua_err);
#endif

    return 0;
}
int lua_custom_net_LLongCode_setByteByIndex(lua_State* tolua_S)
{
    int argc = 0;
    LLongCode* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"LLongCode",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (LLongCode*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_custom_net_LLongCode_setByteByIndex'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 2) 
    {
        uint16_t arg0;
        int arg1;

        ok &= luaval_to_uint16(tolua_S, 2,&arg0, "LLongCode:setByteByIndex");

        ok &= luaval_to_int32(tolua_S, 3,(int *)&arg1, "LLongCode:setByteByIndex");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_custom_net_LLongCode_setByteByIndex'", nullptr);
            return 0;
        }
        cobj->setByteByIndex(arg0, arg1);
        return 0;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "LLongCode:setByteByIndex",argc, 2);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_custom_net_LLongCode_setByteByIndex'.",&tolua_err);
#endif

    return 0;
}
int lua_custom_net_LLongCode_getLongLongValue(lua_State* tolua_S)
{
    int argc = 0;
    LLongCode* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"LLongCode",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (LLongCode*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_custom_net_LLongCode_getLongLongValue'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_custom_net_LLongCode_getLongLongValue'", nullptr);
            return 0;
        }
        long long ret = cobj->getLongLongValue();
        tolua_pushnumber(tolua_S,(lua_Number)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "LLongCode:getLongLongValue",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_custom_net_LLongCode_getLongLongValue'.",&tolua_err);
#endif

    return 0;
}
int lua_custom_net_LLongCode_constructor(lua_State* tolua_S)
{
    int argc = 0;
    LLongCode* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif



    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_custom_net_LLongCode_constructor'", nullptr);
            return 0;
        }
        cobj = new LLongCode();
        tolua_pushusertype(tolua_S,(void*)cobj,"LLongCode");
        tolua_register_gc(tolua_S,lua_gettop(tolua_S));
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "LLongCode:LLongCode",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_error(tolua_S,"#ferror in function 'lua_custom_net_LLongCode_constructor'.",&tolua_err);
#endif

    return 0;
}

static int lua_custom_net_LLongCode_finalize(lua_State* tolua_S)
{
    printf("luabindings: finalizing LUA object (LLongCode)");
    return 0;
}

int lua_register_custom_net_LLongCode(lua_State* tolua_S)
{
    tolua_usertype(tolua_S,"LLongCode");
    tolua_cclass(tolua_S,"LLongCode","LLongCode","",nullptr);

    tolua_beginmodule(tolua_S,"LLongCode");
        tolua_function(tolua_S,"new",lua_custom_net_LLongCode_constructor);
        tolua_function(tolua_S,"getByteByIndex",lua_custom_net_LLongCode_getByteByIndex);
        tolua_function(tolua_S,"setLonglongValue",lua_custom_net_LLongCode_setLonglongValue);
        tolua_function(tolua_S,"setByteByIndex",lua_custom_net_LLongCode_setByteByIndex);
        tolua_function(tolua_S,"getLongLongValue",lua_custom_net_LLongCode_getLongLongValue);
    tolua_endmodule(tolua_S);
    std::string typeName = typeid(LLongCode).name();
    g_luaType[typeName] = "LLongCode";
    g_typeCast["LLongCode"] = "LLongCode";
    return 1;
}

int lua_custom_net_CSVHelper_getTable(lua_State* tolua_S)
{
    int argc = 0;
    custom::CSVHelper* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"custom.CSVHelper",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (custom::CSVHelper*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_custom_net_CSVHelper_getTable'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 1) 
    {
        std::string arg0;

        ok &= luaval_to_std_string(tolua_S, 2,&arg0, "custom.CSVHelper:getTable");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_custom_net_CSVHelper_getTable'", nullptr);
            return 0;
        }
        cocos2d::ValueMap ret = cobj->getTable(arg0);
        ccvaluemap_to_luaval(tolua_S, ret);
        return 1;
    }
    if (argc == 2) 
    {
        std::string arg0;
        std::string arg1;

        ok &= luaval_to_std_string(tolua_S, 2,&arg0, "custom.CSVHelper:getTable");

        ok &= luaval_to_std_string(tolua_S, 3,&arg1, "custom.CSVHelper:getTable");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_custom_net_CSVHelper_getTable'", nullptr);
            return 0;
        }
        cocos2d::ValueMap ret = cobj->getTable(arg0, arg1);
        ccvaluemap_to_luaval(tolua_S, ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "custom.CSVHelper:getTable",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_custom_net_CSVHelper_getTable'.",&tolua_err);
#endif

    return 0;
}
int lua_custom_net_CSVHelper_init(lua_State* tolua_S)
{
    int argc = 0;
    custom::CSVHelper* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"custom.CSVHelper",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (custom::CSVHelper*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_custom_net_CSVHelper_init'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_custom_net_CSVHelper_init'", nullptr);
            return 0;
        }
        bool ret = cobj->init();
        tolua_pushboolean(tolua_S,(bool)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "custom.CSVHelper:init",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_custom_net_CSVHelper_init'.",&tolua_err);
#endif

    return 0;
}
int lua_custom_net_CSVHelper_create(lua_State* tolua_S)
{
    int argc = 0;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif

#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertable(tolua_S,1,"custom.CSVHelper",0,&tolua_err)) goto tolua_lerror;
#endif

    argc = lua_gettop(tolua_S) - 1;

    if (argc == 0)
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_custom_net_CSVHelper_create'", nullptr);
            return 0;
        }
        custom::CSVHelper* ret = custom::CSVHelper::create();
        object_to_luaval<custom::CSVHelper>(tolua_S, "custom.CSVHelper",(custom::CSVHelper*)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d\n ", "custom.CSVHelper:create",argc, 0);
    return 0;
#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_custom_net_CSVHelper_create'.",&tolua_err);
#endif
    return 0;
}
int lua_custom_net_CSVHelper_constructor(lua_State* tolua_S)
{
    int argc = 0;
    custom::CSVHelper* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif



    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_custom_net_CSVHelper_constructor'", nullptr);
            return 0;
        }
        cobj = new custom::CSVHelper();
        cobj->autorelease();
        int ID =  (int)cobj->_ID ;
        int* luaID =  &cobj->_luaID ;
        toluafix_pushusertype_ccobject(tolua_S, ID, luaID, (void*)cobj,"custom.CSVHelper");
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "custom.CSVHelper:CSVHelper",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_error(tolua_S,"#ferror in function 'lua_custom_net_CSVHelper_constructor'.",&tolua_err);
#endif

    return 0;
}

static int lua_custom_net_CSVHelper_finalize(lua_State* tolua_S)
{
    printf("luabindings: finalizing LUA object (CSVHelper)");
    return 0;
}

int lua_register_custom_net_CSVHelper(lua_State* tolua_S)
{
    tolua_usertype(tolua_S,"custom.CSVHelper");
    tolua_cclass(tolua_S,"CSVHelper","custom.CSVHelper","cc.Ref",nullptr);

    tolua_beginmodule(tolua_S,"CSVHelper");
        tolua_function(tolua_S,"new",lua_custom_net_CSVHelper_constructor);
        tolua_function(tolua_S,"getTable",lua_custom_net_CSVHelper_getTable);
        tolua_function(tolua_S,"init",lua_custom_net_CSVHelper_init);
        tolua_function(tolua_S,"create", lua_custom_net_CSVHelper_create);
    tolua_endmodule(tolua_S);
    std::string typeName = typeid(custom::CSVHelper).name();
    g_luaType[typeName] = "custom.CSVHelper";
    g_typeCast["CSVHelper"] = "custom.CSVHelper";
    return 1;
}

int lua_custom_net_XMLParser_init(lua_State* tolua_S)
{
    int argc = 0;
    custom::XMLParser* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"custom.XMLParser",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (custom::XMLParser*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_custom_net_XMLParser_init'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_custom_net_XMLParser_init'", nullptr);
            return 0;
        }
        bool ret = cobj->init();
        tolua_pushboolean(tolua_S,(bool)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "custom.XMLParser:init",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_custom_net_XMLParser_init'.",&tolua_err);
#endif

    return 0;
}
int lua_custom_net_XMLParser_parseXML(lua_State* tolua_S)
{
    int argc = 0;
    custom::XMLParser* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"custom.XMLParser",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (custom::XMLParser*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_custom_net_XMLParser_parseXML'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 1) 
    {
        std::string arg0;

        ok &= luaval_to_std_string(tolua_S, 2,&arg0, "custom.XMLParser:parseXML");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_custom_net_XMLParser_parseXML'", nullptr);
            return 0;
        }
        cocos2d::ValueMap ret = cobj->parseXML(arg0);
        ccvaluemap_to_luaval(tolua_S, ret);
        return 1;
    }
    if (argc == 2) 
    {
        std::string arg0;
        std::string arg1;

        ok &= luaval_to_std_string(tolua_S, 2,&arg0, "custom.XMLParser:parseXML");

        ok &= luaval_to_std_string(tolua_S, 3,&arg1, "custom.XMLParser:parseXML");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_custom_net_XMLParser_parseXML'", nullptr);
            return 0;
        }
        cocos2d::ValueMap ret = cobj->parseXML(arg0, arg1);
        ccvaluemap_to_luaval(tolua_S, ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "custom.XMLParser:parseXML",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_custom_net_XMLParser_parseXML'.",&tolua_err);
#endif

    return 0;
}
int lua_custom_net_XMLParser_create(lua_State* tolua_S)
{
    int argc = 0;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif

#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertable(tolua_S,1,"custom.XMLParser",0,&tolua_err)) goto tolua_lerror;
#endif

    argc = lua_gettop(tolua_S) - 1;

    if (argc == 0)
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_custom_net_XMLParser_create'", nullptr);
            return 0;
        }
        custom::XMLParser* ret = custom::XMLParser::create();
        object_to_luaval<custom::XMLParser>(tolua_S, "custom.XMLParser",(custom::XMLParser*)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d\n ", "custom.XMLParser:create",argc, 0);
    return 0;
#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_custom_net_XMLParser_create'.",&tolua_err);
#endif
    return 0;
}
int lua_custom_net_XMLParser_updateArmatureGLProgram(lua_State* tolua_S)
{
    int argc = 0;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif

#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertable(tolua_S,1,"custom.XMLParser",0,&tolua_err)) goto tolua_lerror;
#endif

    argc = lua_gettop(tolua_S) - 1;

    if (argc == 2)
    {
        cocostudio::Armature* arg0;
        cocos2d::GLProgram* arg1;
        ok &= luaval_to_object<cocostudio::Armature>(tolua_S, 2, "ccs.Armature",&arg0);
        ok &= luaval_to_object<cocos2d::GLProgram>(tolua_S, 3, "cc.GLProgram",&arg1);
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_custom_net_XMLParser_updateArmatureGLProgram'", nullptr);
            return 0;
        }
        custom::XMLParser::updateArmatureGLProgram(arg0, arg1);
        return 0;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d\n ", "custom.XMLParser:updateArmatureGLProgram",argc, 2);
    return 0;
#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_custom_net_XMLParser_updateArmatureGLProgram'.",&tolua_err);
#endif
    return 0;
}
int lua_custom_net_XMLParser_constructor(lua_State* tolua_S)
{
    int argc = 0;
    custom::XMLParser* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif



    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_custom_net_XMLParser_constructor'", nullptr);
            return 0;
        }
        cobj = new custom::XMLParser();
        cobj->autorelease();
        int ID =  (int)cobj->_ID ;
        int* luaID =  &cobj->_luaID ;
        toluafix_pushusertype_ccobject(tolua_S, ID, luaID, (void*)cobj,"custom.XMLParser");
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "custom.XMLParser:XMLParser",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_error(tolua_S,"#ferror in function 'lua_custom_net_XMLParser_constructor'.",&tolua_err);
#endif

    return 0;
}

static int lua_custom_net_XMLParser_finalize(lua_State* tolua_S)
{
    printf("luabindings: finalizing LUA object (XMLParser)");
    return 0;
}

int lua_register_custom_net_XMLParser(lua_State* tolua_S)
{
    tolua_usertype(tolua_S,"custom.XMLParser");
    tolua_cclass(tolua_S,"XMLParser","custom.XMLParser","cc.Ref",nullptr);

    tolua_beginmodule(tolua_S,"XMLParser");
        tolua_function(tolua_S,"new",lua_custom_net_XMLParser_constructor);
        tolua_function(tolua_S,"init",lua_custom_net_XMLParser_init);
        tolua_function(tolua_S,"parseXML",lua_custom_net_XMLParser_parseXML);
        tolua_function(tolua_S,"create", lua_custom_net_XMLParser_create);
        tolua_function(tolua_S,"updateArmatureGLProgram", lua_custom_net_XMLParser_updateArmatureGLProgram);
    tolua_endmodule(tolua_S);
    std::string typeName = typeid(custom::XMLParser).name();
    g_luaType[typeName] = "custom.XMLParser";
    g_typeCast["XMLParser"] = "custom.XMLParser";
    return 1;
}
TOLUA_API int register_all_custom_net(lua_State* tolua_S)
{
	tolua_open(tolua_S);
	
	tolua_module(tolua_S,"custom",0);
	tolua_beginmodule(tolua_S,"custom");

	lua_register_custom_net_XMLParser(tolua_S);
	lua_register_custom_net_NetMessage(tolua_S);
	lua_register_custom_net_LLongCode(tolua_S);
	lua_register_custom_net_CSVHelper(tolua_S);
	lua_register_custom_net_CodeChange(tolua_S);
	lua_register_custom_net_PlatUtil(tolua_S);
	lua_register_custom_net_FuncAction(tolua_S);

	tolua_endmodule(tolua_S);
	return 1;
}

