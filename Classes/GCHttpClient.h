﻿#ifndef __GC_HTTP_CLIENT_H__
#define __GC_HTTP_CLIENT_H__


#include <string>
#include <map>
using namespace std;

#ifdef _WIN32
#include "winsock.h"
#else
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <errno.h>
#endif


enum{HTTP_ERROR_NONE = 0,			//0，成功
	HTTP_ERROR_PARSEURL,			//1，解析URL失败
	HTTP_ERROR_CREATESOCKET,		//2，域名解析失败
	HTTP_ERROR_CONNECTHOST,			//3，连接服务器失败
	HTTP_ERROR_SENDREQ,				//4，发送数据失败
	HTTP_ERROR_RECVHTTPRESHEAD,		//5，接受回应包头失败
	HTTP_ERROR_GETHTTPFILESIZE,		//6，获取包头数据大小失败
	HTTP_ERROR_RECVHTTPFILE,		//7，接受包体数据失败
	HTTP_ERROR_REC_TIMEOUT,			//8，接受超时
	HTTP_ERROR_302FOUND,			//9，HTTP的302错误
	HTTP_ERROR_FILESIZE_ZERO,		//10，获取数据长度位0
	HTTP_ERROR_USER_STOP};			//11，外部中止

#define MAX_REC_TMP_LEN	4096


class IFileDownloader
{
public:
	virtual void CallBackFileSize(int iFileSize){};
	virtual void CallBackFileData(char *szData,int iLen){};
};

class GCHttpClient
{
public:
	GCHttpClient();
	virtual ~GCHttpClient();
		
	//获取网页内容			URL				内容		 内容长度		请求的附带数据		附带数据长度
	int GetWebsiteContent(char *szURL,char *szContent,int &iContentLen,char *szReqMsg = NULL,int iReqLen = 0);

	//下载文件
	int GetDownloadFile(char *szURL,IFileDownloader *pFileDownloader,int iBeginPos);

	//获取回应头
	char* GetHttpResHead(){return m_szHttpHeadRes;}

	//设置KEEPALVIE的最大时间
	void SetConnectiongKeepAlive(int iKeepAliveTime);

	//获取主机名
	string GetHostName(){return m_szHostName;}

	//获取端口号
	int GetHostPort(){return m_iPort;}

	//解析URL
	static void ParseURL(string strUrl,string &strHostName,string &strFilePath,int &iPort);

	//设置是否使用十六进制参数模式传输数据
	void SetUseHexMode(bool bUse);
	
	//检查是否使用了模式
	bool IsUseHexMode();

	//设置本地域名匹配解析方法
	static void AddDNSInfo(char *strHost,char *strIP);

	//外部设置中止
	void Stop(){m_bStop = true;}

	//获取最近一次的连接IP
	char* GetLastConnectIP(){return m_strLastConnectIP;}

	//获取302跳转URL
	char* Get302MoveUrl(){return m_szMoveUrl;}
private:			
	int ConnectServer(char *szServerIP,unsigned short iServerPort);
	int SendHttpReq(char *szReqMsg,int iReqLen);

	int ReadSocketNodeData();

	int GetHttpFileSize();

	string		m_szURL;			//传入的完整URL
	string		m_szHostName;		//ParseURL解析出的主机名
	int		m_iPort;			//ParseURL解析出的主机端口
	string		m_szFilePath;		//ParseURL解析出的下载文件路径

	int			m_iBeginPos;		

	int			m_sock;

	char		m_szRecTmp[MAX_REC_TMP_LEN];
	char		m_szHttpHeadRes[1024];	//HTTP的回应头
	int			m_iHttpHeadHaveRecLen;
	long			m_iHttpContentHaveRecLen;
	int			m_iHttpFileSize;
	bool		m_bRecOK;

	fd_set 			m_rfds;
	fd_set 			m_wfds;
	struct timeval	m_tv;


	char *m_pBlockResMsg;
	int *m_iBlockResMsgLen;


	string m_strLastHostName;
	time_t m_tmLastAlive;

	int TryHttpSendAndRec(char *szReqMsg,int iReqLen);
	int m_iKeepAliveTime;

	bool m_bServerConnectionClose;

	char *m_pHexBuffer;
	int m_iHexLen;


	IFileDownloader *m_pFileDownloader;

	const char* GetIpFromMapDns(char *strHost);
	static map<string,string> m_mapDns;

	bool m_bStop;

	char m_strLastConnectIP[32];

	string m_strHttpReqHostName;

	char m_szMoveUrl[256];
	bool CheckHttpHeadResMoveUrl();
};

#endif 
