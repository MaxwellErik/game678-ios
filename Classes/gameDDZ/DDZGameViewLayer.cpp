#include "DDZGameViewLayer.h"
#include "LobbyLayer.h"
#include "ClientSocketSink.h"
#include "HXmlParse.h"
#include "LocalDataUtil.h"
#include "JniSink.h"
#include "VisibleRect.h"
#include "HttpConstant.h"
#include "Screen.h"
#include "LobbySocketSink.h"
#include "LoginLayer.h"
#include "GameLayerMove.h"
#include "GameGetScore.h"
#include "DDZGameLogic.h"
#include "Convert.h"
#include "SimpleAudioEngine.h"

#define INVALID_LAND                   -1

#define Btn_Ready						1
#define Btn_BackToLobby					2
#define Btn_Seting						3
#define Btn_Pop							4
#define Btn_Pass						5
#define Btn_OutCard						6
#define Btn_GetScoreBtn					7
#define Btn_ReadyMing					8
#define Btn_CallLand                   9
#define Btn_NoCallLand                 10
#define Btn_Snatch                     11
#define Btn_NoSnatch                   12
#define Btn_AddScore                   13
#define Btn_NoAddScore                 14
#define Btn_AutoPlay                   15
#define Btn_CancelAuto                 16
#define Btn_MingPai                    17
#define Btn_ChangeTabel                18


typedef vector<BYTE> DDZvector_carddata;

DDZGameViewLayer::DDZGameViewLayer(GameScene *pGameScene)
	:IGameView(pGameScene)
{
	m_GameState=enGameNormal;
	SetKindId(DDZ_KIND_ID);
    m_StartTime = 0;
    m_wCurrentUser = 0;
    m_SendCardCount = 0;
    m_Multi = 0;
    m_LandUser = INVALID_LAND;
    m_CurGameState = 0;
    m_wCurOutCardUser = INVALID_LAND;
    m_NewTurn = 0;
    m_HintIndex = 0;
    m_QiangdizhuIdx = 0;
    m_bAutoPlay = false;
}

DDZGameViewLayer::~DDZGameViewLayer() 
{
    unscheduleUpdate();
}

DDZGameViewLayer *DDZGameViewLayer::create(GameScene *pGameScene)
{
	DDZGameViewLayer *pLayer = new DDZGameViewLayer(pGameScene);
	if(pLayer && pLayer->init())
	{
		pLayer->autorelease();
		return pLayer;
	}
	else
	{
		CC_SAFE_DELETE(pLayer);
		return NULL;
	}
}


bool DDZGameViewLayer::init()
{
	if ( !Layer::init() )
	{
		return false;
	}
	setLocalZOrder(3);

	Tools::addSpriteFrame("DDZ/ddz_res.plist" );
    Tools::addSpriteFrame("DDZ/ddz_anim.plist" );
	Tools::addSpriteFrame("Common/CardSprite2.plist");

    m_CurBackMusic = "DDZ/sound/MusicEx_Normal.wav";
    CocosDenshion::SimpleAudioEngine::getInstance()->playBackgroundMusic(m_CurBackMusic.c_str(), true);
    SoundUtil::sharedEngine()->setBackSoundVolume(g_GlobalUnits.m_fBackMusicValue);
    SoundUtil::sharedEngine()->setSoundVolume(g_GlobalUnits.m_fSoundValue);
	IGameView::onEnter();
	JniSink::share()->setIGameView(this);
	setGameStatus(GS_FREE);
    
    scheduleUpdate();
    
	InitGame();
	AddButton();
    
    for (int i = 0; i < GAME_PLAYER_DDZ; i++)
        addPlayer(i);
    
	auto listener = EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(true);
	listener->onTouchBegan = CC_CALLBACK_2(DDZGameViewLayer::onTouchBegan, this);
	listener->onTouchMoved = CC_CALLBACK_2(DDZGameViewLayer::onTouchMoved, this);
	listener->onTouchEnded = CC_CALLBACK_2(DDZGameViewLayer::onTouchEnded, this);
	_eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);

	setTouchEnabled(true);
	return true;
}


void DDZGameViewLayer::InitGame()
{
    for (int i = 0; i < GAME_PLAYER_DDZ; i++)
    {
        m_HeadTag[i] = 20 + i;
        m_ReadyTag[i] = 23 + i;
        m_roletypeTag[i] = 26 + i;
        m_AlertAnimTag[i] = 29;
        m_bMingPai[i] = 0;
        m_bAlert[i] = false;
    }
    m_TimeSTag = 50;//计时器tag
    m_CallLandTag= 51;
    m_DiCardTag[0] = 52;
    m_DiCardTag[1] = 53;
    m_DiCardTag[2] = 54;
    m_DiBackCardTag[0] = 55;
    m_DiBackCardTag[1] = 56;
    m_DiBackCardTag[2] = 57;
    m_resultTag        = 58;
    m_AddScoreTag      = 59;
    m_NoOutTag         = 60;
    m_AnimTga          = 61;
    //背景图
	m_BackSpr = Sprite::create("DDZ/DDZgame_back.jpg");
	m_BackSpr->setPosition(_STANDARD_SCREEN_CENTER_);
	addChild(m_BackSpr);
    
	m_ClockSpr = Sprite::createWithSpriteFrameName("timeBack.png");
    m_ClockSpr->setPosition(Vec2::ZERO);
	m_ClockSpr->setVisible(false);
	addChild(m_ClockSpr);
    
    m_MultiSpr = Sprite::createWithSpriteFrameName("icon_m.png");
    m_MultiSpr->setPosition(Vec2(1200, 1000));
    addChild(m_MultiSpr);
    m_MultiSpr->setVisible(false);
    
    m_MultiCnt = LabelAtlas::create("0", "DDZ/ddz_num.png", 50, 66, '0');
    m_MultiCnt->setAnchorPoint(Vec2(0, 0.5f));
    m_MultiCnt->setPosition(Vec2(1270, 1000));
    addChild(m_MultiCnt);
    m_MultiCnt->setVisible(false);
    
    m_CardControl[0].SetLayer(this,300,0);
    m_CardControl[1].SetLayer(this,500,1);
	m_CardControl[2].SetLayer(this,700,2);
	m_OutCardControl.SetLayer(this,900,1);
}

void DDZGameViewLayer::AddButton()
{
	Menu *back = CreateButton("Returnback_normal.png" ,Vec2(90,1000), Btn_BackToLobby);
	addChild(back);

	Menu* setting = CreateButton("seting_normal.png" ,Vec2(1810,1000), Btn_Seting);
	addChild(setting);
}

void DDZGameViewLayer::onEnter ()
{	

}

void DDZGameViewLayer::onExit()
{
	Tools::removeSpriteFrameCache("DDZ/ddz_res.plist");
    Tools::removeSpriteFrameCache("DDZ/ddz_anim.plist");
	Tools::removeSpriteFrameCache("Common/CardSprite2.plist");
	IGameView::onExit();
}

void DDZGameViewLayer::playOutCardSound(int chairId, int cardType, int cardValue)
{
    //判断性别
    const tagUserData* pUserData = GetUserData(chairId);
    if (pUserData == NULL)
    {
        assert(0 && "得不到玩家信息");
        return;
    }
    
    string soundStr = "";
    string SpecialStr = "";
    
    if (GENDER_BOY == pUserData->wGender)
        soundStr = "DDZ/sound/Man_";
    else
        soundStr = "DDZ/sound/Woman_";
    
    if (100 == cardType)
    {
        soundStr = soundStr + "baojing" + StringUtils::toString(cardValue);
        SoundUtil::sharedEngine()->playWav(soundStr.c_str(), false);
        return;
    }
    
    switch (cardType) {
        case CT_SINGLE://单牌类型
        {
            soundStr += StringUtils::toString(cardValue);
            SpecialStr = "DDZ/sound/Special_give";
            break;
        }
        case CT_DOUBLE://对牌类型
        {
            soundStr =  soundStr + "dui" + StringUtils::toString(cardValue);
            SpecialStr = "DDZ/sound/Special_give";
            break;
        }
        case CT_THREE://三条类型
        {
            soundStr =  soundStr + "tuple" + StringUtils::toString(cardValue);
            SpecialStr = "DDZ/sound/Special_give";
            break;
        }
        case CT_ONE_LINE://单连类型
        {
            soundStr =  soundStr + "shunzi";
            SpecialStr = "DDZ/sound/Special_give";
            OnAnimate(6);
            break;
        }
        case CT_DOUBLE_LINE://对连类型
        {
            soundStr =  soundStr + "liandui";
            SpecialStr = "DDZ/sound/Special_give";
            OnAnimate(5);
            break;
        }
        case CT_THREE_LINE://三连类型
        {
            soundStr =  soundStr + "liandui";
            SpecialStr = "DDZ/sound/Special_give";
            break;
        }
        case CT_THREE_LINE_TAKE_ONE://三带一单
        {
            soundStr =  soundStr + "sandaiyi";
            SpecialStr = "DDZ/sound/Special_give";
            break;
        }
        case CT_THREE_LINE_TAKE_DOUBLE://三带一对
        {
            soundStr =  soundStr + "sandaiyidui";
            SpecialStr = "DDZ/sound/Special_give";
            break;
        }
        case CT_FOUR_LINE_TAKE_ONE://四带两单
        {
            soundStr =  soundStr + "sidaier";
            SpecialStr = "DDZ/sound/Special_give";
            break;
        }
        case CT_FOUR_LINE_TAKE_DOUBLE://四带两对
        {
            soundStr =  soundStr + "sidailiangdui";
            SpecialStr = "DDZ/sound/Special_give";
            break;
        }
        case CT_BOMB_CARD://炸弹类型
        {
            soundStr =  soundStr + "zhadan";
            SpecialStr = "DDZ/sound/Special_Bomb";
            
            if ("DDZ/sound/MusicEx_Normal.wav" != m_CurBackMusic)
            {
                m_CurBackMusic = "DDZ/sound/MusicEx_Normal2.wav";
                CocosDenshion::SimpleAudioEngine::getInstance()->playBackgroundMusic(m_CurBackMusic.c_str(), false);
            }
            OnAnimate(7);
            break;
        }
        case CT_MISSILE_CARD://火箭类型
        {
            soundStr =  soundStr + "wangzha";
            SpecialStr = "DDZ/sound/Special_Bomb";
            
            if ("DDZ/sound/MusicEx_Normal.wav" != m_CurBackMusic)
            {
                m_CurBackMusic = "DDZ/sound/MusicEx_Normal2.wav";
                CocosDenshion::SimpleAudioEngine::getInstance()->playBackgroundMusic(m_CurBackMusic.c_str(), false);
            }
            OnAnimate(3);
            break;
        }
        case CT_PLANE://飞机类型(连三带两对)
        {
            soundStr =  soundStr + "feiji";
            SpecialStr = "DDZ/sound/Special_plane";
            OnAnimate(2);
            break;
        }
        default:
            break;
    }
    SoundUtil::sharedEngine()->playWav(SpecialStr.c_str(), false);
    SoundUtil::sharedEngine()->playWav(soundStr.c_str(), false);
}

void DDZGameViewLayer::playGameSound(int chairId, int soundType)
{
    //判断性别
    const tagUserData* pUserData = GetUserData(chairId);
    if (pUserData == NULL)
    {
        assert(0 && "得不到玩家信息");
        return;
    }
    string soundStr = "";
    switch (soundType) {
        case 0://叫地主
            soundStr = "Order";
            break;
        case 1://不叫
            soundStr = "NoOrder";
             break;
        case 2://抢地主1
            soundStr = "Rob1";
            break;
        case 3://抢地主2
            soundStr = "Rob2";
            break;
        case 4://抢地主3
            soundStr = "Rob3";
             break;
        case 5://不抢
            soundStr = "NoRob";
             break;
        case 6://加倍
            soundStr = "jiabei";
             break;
        case 7://不加倍
            soundStr = "bujiabei";
             break;
        case 8://明牌
            soundStr = "Share";
            break;
        case 9://不要1
            soundStr = "buyao1";
            break;
        case 10://不要2
            soundStr = "buyao2";
            break;
        case 11://不要3
            soundStr = "buyao3";
            break;
        case 12://不要4
            soundStr = "buyao4";
            break;
        default:
            break;
    }
    if (GENDER_BOY == pUserData->wGender)
    {
        soundStr = "DDZ/sound/Man_" + soundStr;
    }
    else
    {
        soundStr = "DDZ/sound/Woman_" + soundStr;
    }
    SoundUtil::sharedEngine()->playWav(soundStr.c_str(), false);
}

// Touch 触发
bool DDZGameViewLayer::onTouchBegan(Touch *pTouch, Event *pEvent)
{
	Vec2 touchLocation = pTouch->getLocation(); // 返回GL坐标
	Vec2 localPos = convertToNodeSpace(touchLocation);

	m_CardControl[1].ccTouchBegan(localPos);

	return true;
}

void DDZGameViewLayer::onTouchMoved(Touch *pTouch, Event *pEvent)
{
	Vec2 touchLocation = pTouch->getLocation(); // 返回GL坐标
	Vec2 localPos = convertToNodeSpace(touchLocation);
	m_CardControl[1].ccTouchMoved(localPos);
}

void DDZGameViewLayer::onTouchEnded(Touch*pTouch, Event*pEvent)
{
	Vec2 touchLocation = pTouch->getLocation(); // 返回GL坐标
	Vec2 localPos = convertToNodeSpace(touchLocation);
	m_CardControl[1].ccTouchEnded(localPos);
    UpdateOutCardButton();
}

// Alert Message 确认消息处理
void DDZGameViewLayer::DialogConfirm(Ref *pSender)
{
	this->RemoveAlertMessageLayer();
	this->SetAllTouchEnabled(true);
}

// Alert Message 取消消息处理
void DDZGameViewLayer::DialogCancel(Ref *pSender)
{
	this->RemoveAlertMessageLayer();
	this->SetAllTouchEnabled(true);
}

void DDZGameViewLayer::OnEventUserScore( tagUserData * pUserData, WORD wChairID, bool bLookonUser )
{
	IGameView::OnEventUserScore(pUserData, wChairID, bLookonUser);
    int viewChairID = g_GlobalUnits.SwitchViewChairID(wChairID);
    Node* headNode = getChildByTag(m_HeadTag[viewChairID]);
    if (nullptr == headNode)
        return;
    
    Label *gold = dynamic_cast<Label*>(headNode->getChildByTag(1));
    if (gold)
    {
        char strc[32]="";
        LONGLONG lMon = pUserData->lScore;
        memset(strc , 0 , sizeof(strc));
        Tools::AddComma(lMon , strc);
        
        gold->setString(strc);
    }
}

void DDZGameViewLayer::OnEventUserStatus(tagUserData * pUserData, WORD wChairID, bool bLookonUser)
{
	IGameView::OnEventUserStatus(pUserData, wChairID, bLookonUser);
    int viewChairID = g_GlobalUnits.SwitchViewChairID(wChairID);
    removeAllChildByTag(m_ReadyTag[viewChairID]);
    if (pUserData->cbUserStatus == US_READY)
    {
        if (1 == viewChairID)
            StopTime();
        
        Sprite* mReaderSpr = Sprite::createWithSpriteFrameName("icon_prepare.png");
        mReaderSpr->setPosition(getReadyPos(viewChairID));
        mReaderSpr->setTag(m_ReadyTag[viewChairID]);
        addChild(mReaderSpr);
    }
}

//用户换桌
void DDZGameViewLayer::OnEventUserChangeTable(tagUserData * pUserData, WORD wChairID, bool bLookonUser)
{
    auto myData = ClientSocketSink::sharedSocketSink()->GetMeUserData();
    if (nullptr == myData)
        return;
    
    for (int i = 0; i < 3; i++)
    {
        if (i == GetMeChairID())
            continue;
        
        const tagUserData* pUserData = GetUserData(i);
        if (nullptr == pUserData)
            continue;
        
        addPlayer(i);
    }
}

void DDZGameViewLayer::OnEventUserEnter( tagUserData * pUserData, WORD wChairID, bool bLookonUser )
{
    addPlayer(wChairID);
}

void DDZGameViewLayer::OnEventUserLeave( tagUserData * pUserData, WORD wChairID, bool bLookonUser )
{
    int viewChairID = g_GlobalUnits.SwitchViewChairID(wChairID);
    if (viewChairID == 1 && !g_GlobalUnits.m_bLeaveGameByServer)
    {
        backLoginView(nullptr);
        return;
    }
    else
    {
        removeAllChildByTag(m_ReadyTag[viewChairID]);
        removeAllChildByTag(m_HeadTag[viewChairID]);
    }
}

Vec2 DDZGameViewLayer::getHeadPos(WORD chairID)
{
    switch (chairID) {
        case 0: return Vec2(90 ,640);
        case 1: return Vec2(240,90);
        case 2: return Vec2(1830,640);
        default: return Vec2::ZERO;
    }
}

Vec2 DDZGameViewLayer::getHeadIconPos(WORD chairID)
{
    switch (chairID) {
        case 0: return Vec2(85 ,175);
        case 1: return Vec2(85,85);
        case 2: return Vec2(85,175);
        default: return Vec2::ZERO;
    }
}

Vec2 DDZGameViewLayer::getNickNamePos(WORD chairID)
{
    switch (chairID) {
        case 0: return Vec2(5, 70);
        case 1: return Vec2(180, 120);
        case 2: return Vec2(165, 70);
        default: return Vec2::ZERO;
    }
}

Vec2 DDZGameViewLayer::getGoldPos(WORD chairID)
{
    switch (chairID) {
        case 0: return Vec2(5 ,30);
        case 1: return Vec2(180, 60);
        case 2: return Vec2(165,30);
        default: return Vec2::ZERO;
    }
}

Vec2 DDZGameViewLayer::getReadyPos(WORD chairID)
{
    switch (chairID) {
        case 0: return Vec2(90,450);
        case 1: return Vec2(240,210);
        case 2: return Vec2(1830,450);
        default: return Vec2::ZERO;
    }
}

Vec2 DDZGameViewLayer::getTimerPos(WORD chairID)
{
    switch (chairID) {
        case 0: return Vec2(470,850);
        case 1: return Vec2(400,220);
        case 2: return Vec2(1450,850);
        default: return Vec2::ZERO;
    }
}

Vec2 DDZGameViewLayer::getRoleTypePos(WORD chairID)
{
    switch (chairID) {
        case 0: return Vec2(90,850);
        case 1: return Vec2(90,230);
        case 2: return Vec2(1830,850);
        default: return Vec2::ZERO;
    }
}

void DDZGameViewLayer::addPlayer(WORD chairID)
{
    if (nullptr == ClientSocketSink::sharedSocketSink()->GetMeUserData())
        return;
    
    const tagUserData* pUserData = GetUserData(chairID);
    if (nullptr == pUserData)
        return;
    
    int chair = g_GlobalUnits.SwitchViewChairID(pUserData->wChairID);
    Vec2 pos = getHeadPos(chair);
    
    //头像    
    ui::Scale9Sprite* mHeadback = ui::Scale9Sprite::createWithSpriteFrameName("bg_player_info.png");;
    if (1 == chair)
    {
        mHeadback->setContentSize(Size(470, 170));
    }
    else
    {
        mHeadback->setContentSize(Size(170, 260));
    }
    
    mHeadback->setTag(m_HeadTag[chair]);
    mHeadback->setPosition(pos);
    addChild(mHeadback, 10000);

    string heads = g_GlobalUnits.getFace(pUserData->wGender, pUserData->lScore);
    Sprite* mHead = Sprite::createWithSpriteFrameName(heads);
    mHead->setScale(0.9f);
    mHead->setPosition(getHeadIconPos(chair));
    mHeadback->addChild(mHead,10000);

    //金币
    Label* mGoldFont = nullptr;
    char strc[32]="";
    
    LONGLONG lMon = pUserData->lScore;
    memset(strc , 0 , sizeof(strc));
    Tools::AddComma(lMon , strc);
    if (1 == chair)
    {
        string goldstr = "酒吧豆: ";
        goldstr.append(strc);
        mGoldFont = Label::createWithSystemFont(goldstr,_GAME_FONT_NAME_1_,30);
    }
    else
    {
        mGoldFont = Label::createWithSystemFont(strc,_GAME_FONT_NAME_1_,30);
    }
    mGoldFont->setPosition(getGoldPos(chair));
    mGoldFont->setTag(1);
    mGoldFont->setColor(_GAME_FONT_COLOR_4_);
    mHeadback->addChild(mGoldFont);
    
    //昵称
    string nick = gbk_utf8(pUserData->szNickName);
    m_PlayerName[chairID] = nick;
    if (1 == chair)
        nick = "昵称: " + nick;
   
    Label* mNickfont = Label::createWithSystemFont(nick, _GAME_FONT_NAME_1_,30);
    mNickfont->setTag(2);
    if (2 == chair)
    {
        mGoldFont->setAnchorPoint(Vec2(1, 0.5f));
        mNickfont->setAnchorPoint(Vec2(1, 0.5f));
    }
    else
    {
        mGoldFont->setAnchorPoint(Vec2(0, 0.5f));
        mNickfont->setAnchorPoint(Vec2(0, 0.5f));
    }
    
    mNickfont->setPosition(getNickNamePos(chair));
        
    mHeadback->addChild(mNickfont);
    
    if (pUserData->cbUserStatus == US_READY)
    {
        Sprite* mReaderSpr = Sprite::createWithSpriteFrameName("icon_prepare.png");
        mReaderSpr->setPosition(getReadyPos(chair));
        mReaderSpr->setTag(m_ReadyTag[chair]);
        addChild(mReaderSpr);
    }
}

Menu* DDZGameViewLayer::CreateButton(const char* btnName ,const Vec2 &p, int tag)
{
    auto sp1 = Sprite::createWithSpriteFrameName(btnName);
    auto sp2 = Sprite::createWithSpriteFrameName(btnName);
    sp2->setColor(Color3B(150,150,150));
    sp2->setScale(0.96f);
    auto pMenuItem = MenuItemSprite::create(sp1, sp2,CC_CALLBACK_1(DDZGameViewLayer::callbackBt, this));
    pMenuItem->setTag(tag);
    Menu* menu = Menu::create(pMenuItem, NULL);
    menu->setPosition(p);
    return menu;
}

void DDZGameViewLayer::StartTime(int _time, int chair)
{
	m_StartTime = _time;
	schedule(schedule_selector(DDZGameViewLayer::UpdateTime), 1);
    m_ClockSpr->setPosition(getTimerPos(chair));
	m_ClockSpr->setVisible(true);
}

void DDZGameViewLayer::StopTime()
{
	unschedule(schedule_selector(DDZGameViewLayer::UpdateTime));
	m_ClockSpr->removeAllChildren();
	m_ClockSpr->setVisible(false);
	m_ClockSpr->removeAllChildren();
}

void DDZGameViewLayer::UpdateTime(float fp)
{
    if (m_StartTime > 0 && m_StartTime <= 5)
    {
        SoundUtil::sharedEngine()->playWav("DDZ/sound/Special_Remind", false);
    }
    
    if (m_StartTime <= 0 && GetMeChairID() == m_wCurrentUser)
	{
		if (m_CurGameState == GS_WK_FREE)
		{
			GameLayerMove::sharedGameLayerMoveSink()->CloseSeting();
			RemoveAlertMessageLayer();
			SoundUtil::sharedEngine()->stopAllEffects();
			SoundUtil::sharedEngine()->stopBackMusic();
			ClientSocketSink::sharedSocketSink()->LeftGameReq();
			GameLayerMove::sharedGameLayerMoveSink()->CloseGetScore();
			ClientSocketSink::sharedSocketSink()->setFrameGameView(NULL);
			GameLayerMove::sharedGameLayerMoveSink()->GoGameToKind();
		}
        else if (m_CurGameState == GS_WK_SENDCARD)
        {
            
        }
        else if (m_CurGameState == GS_WK_SCORE)
        {
            auto callLand = getChildByTag(Btn_CallLand);
            if (callLand)
                callLand->removeFromParent();
            
            auto noCallLand = getChildByTag(Btn_NoCallLand);
            if (noCallLand)
                noCallLand->removeFromParent();
            
            CMD_C_LandScore ladnScore;
            ladnScore.bLandScore = 0;
            SendData(SUB_C_LAND_SCORE, &ladnScore, sizeof(ladnScore));
        }
        else if(m_CurGameState == GS_WK_JIABEI)
        {
            auto Double = getChildByTag(Btn_AddScore);
            if (Double)
                Double->removeFromParent();
            
            auto noDouble = getChildByTag(Btn_NoAddScore);
            if (noDouble)
                noDouble->removeFromParent();
            
            CMD_C_LandScore ladnScore;
            ladnScore.bLandScore = 0;
            SendData(SUB_C_LAND_ISJIA, &ladnScore, sizeof(ladnScore));
        }
        else if (m_CurGameState == GS_WK_PLAYING)
        {
            AutoOpr();
        }
		return;
	}
	m_ClockSpr->removeAllChildren();
    
    std::string str = StringUtils::toString(m_StartTime);
    if (m_StartTime < 10)
        str = "0" + str;
    LabelAtlas *time = LabelAtlas::create(str, "Common/time_1.png", 23, 35, '+');
    time->setPosition(Vec2(20, 26));
    m_ClockSpr->addChild(time);
	time->setTag(m_TimeSTag);
	m_StartTime--;
}

void DDZGameViewLayer::AutoOpr()
{
    if (0 == m_TurnCardCount)
    {
        BYTE tempData = m_cbHandCardData[1][m_HandCardCount[1] - 1];
        m_CardControl[1].SetShootCard(&tempData, 1);
        onOutCard();
        return;
    }
    BYTE resultCardData[m_TurnCardCount];
    BYTE resultCount = 0;
    ZeroMemory(resultCardData, sizeof(resultCardData));
    
    m_GameLogic.HintOutCard(m_cbHandCardData[1], m_HandCardCount[1], m_TurnCardData, m_TurnCardCount, resultCardData, resultCount);
    m_CardControl[1].SetShootCard(resultCardData, resultCount);
    if (0 == resultCount)
    {
        auto out = getChildByTag(Btn_OutCard);
        if (out)
            out->removeFromParent();
        
        auto pass = getChildByTag(Btn_Pass);
        if (pass)
            pass->removeFromParent();
        
        auto pop = getChildByTag(Btn_Pop);
        if (pop)
            pop->removeFromParent();
        SendData(SUB_C_PASS_CARD);
    }
    else
        onOutCard();
}

void DDZGameViewLayer::showDiCard(BYTE card[])
{
    for (int i = 0; i < 3; i++)
    {
        string name = GetCardStringName(card[i]);
        Sprite* cardSpr = Sprite::createWithSpriteFrameName(name);
        cardSpr->setScale(0.5f);
        cardSpr->setPosition(Vec2(850 + i*110, 1000));
        addChild(cardSpr, 0, m_DiCardTag[i]);
        
        auto func = CallFunc::create([=]
            {
                cardSpr->runAction(Sequence::create(Show::create(), ScaleTo::create(0.05 + i / 20.0, -0.5,0.5), nullptr));
            });
        
        Sprite* backCard = dynamic_cast<Sprite*>(getChildByTag(m_DiBackCardTag[i]));
        
        backCard->runAction(Sequence::create(DelayTime::create(1) ,ScaleTo::create(0.05 + i / 20.0, -0.5,0.5f), Hide::create(), func, nullptr));
        
        cardSpr->setFlippedX(true);
    }
}

void DDZGameViewLayer::LeftCardMoveEnd(Node *pSender)
{
    removeChild(pSender);
    m_CardControl[0].SetCardData(m_cbHandCardData[0], m_SendCardCount+1);
}

void DDZGameViewLayer::MyCardMoveEnd(Node *pSender)
{
    removeChild(pSender);
    m_CardControl[1].SetCardData(m_cbHandCardData[1], m_SendCardCount+1);
}

void DDZGameViewLayer::RightCardMoveEnd(Node *pSender)
{
    removeChild(pSender);
    m_CardControl[2].SetCardData(m_cbHandCardData[2], m_SendCardCount+1);
}

void DDZGameViewLayer::SendCard(float dt)
{
    if (m_SendCardCount >= FARMER_CARD_COUNT-1)
    {
        unschedule(schedule_selector(DDZGameViewLayer::SendCard));
        SendData(SUB_C_SENDFINISH,NULL,0);
        return;
    }
    
    Vec2 targetPos = Vec2::ZERO;

    for (int i = 0; i < GAME_PLAYER_DDZ; i++)
    {
        MoveTo *leftMove = nullptr;
        ActionInstant *func = nullptr;
        
        if (0 == i)
        {
            targetPos = Vec2(250, 600);
            func = CallFuncN::create(CC_CALLBACK_1(DDZGameViewLayer::LeftCardMoveEnd, this));
        }
        else if (1 == i)
        {
            targetPos = Vec2(960, 250);
            func = CallFuncN::create(CC_CALLBACK_1(DDZGameViewLayer::MyCardMoveEnd, this));
        }
        else
        {
            targetPos = Vec2(1670, 600);
            func = CallFuncN::create(CC_CALLBACK_1(DDZGameViewLayer::RightCardMoveEnd, this));
        }
        
        leftMove = MoveTo::create(0.1f, targetPos);
        
        Sprite* temp = Sprite::createWithSpriteFrameName("backCard.png");
        temp->setScale(0.9f);
        addChild(temp);
        temp->setPosition(Vec2(_STANDARD_SCREEN_CENTER_.x,_STANDARD_SCREEN_CENTER_.y+25));
        temp->runAction(Sequence::create(leftMove, func, NULL));
    }
    SoundUtil::sharedEngine()->playEffect("SEND_CARD");
    m_SendCardCount++;
}

//发牌
bool DDZGameViewLayer::OnSubSendCard(const void* pBuffer, WORD wDataSize)
{
	ASSERT(wDataSize == sizeof(CMD_S_DDZ_SendCard));
	if (wDataSize != sizeof(CMD_S_DDZ_SendCard)) return false;
    ZeroMemory(m_cbHandCardData, sizeof(m_cbHandCardData));
    
    auto reusltbg = getChildByTag(m_resultTag);
    if (reusltbg)
        reusltbg->removeFromParent();

    //初始化数据
	CMD_S_DDZ_SendCard *pSendCard = (CMD_S_DDZ_SendCard *)pBuffer;
    m_CurGameState = GS_WK_SENDCARD;
    m_wCurrentUser = pSendCard->wCurrentUser;
    
    for (int i = 0; i < GAME_PLAYER_DDZ; i++)
        m_bMingPai[i] = pSendCard->bMingFlag[i];
    
    ZeroMemory(m_TurnCardData, sizeof(m_TurnCardData));
    ZeroMemory(m_HandCardCount, sizeof(m_HandCardCount));
    CopyMemory(m_cbHandCardData[1], pSendCard->bCardData, sizeof(BYTE)*FARMER_CARD_COUNT);
    m_wCurOutCardUser = INVALID_LAND;
    m_TurnCardCount = 0;
    m_Multi = 0;
    m_NewTurn = 0;
    return true;
}

//明牌按钮
bool DDZGameViewLayer::OnSubSendMing(const void* pBuffer, WORD wDataSize)
{
    ASSERT(wDataSize == sizeof(CMD_S_DDZ_SendMing));
    if (wDataSize != sizeof(CMD_S_DDZ_SendMing)) return false;

    CMD_S_DDZ_SendMing *pSendMing = (CMD_S_DDZ_SendMing *)pBuffer;
    m_Multi = pSendMing->wMulti;
    
    WORD viewChairId = g_GlobalUnits.SwitchViewChairID(pSendMing->wChairID);
    CopyMemory(m_cbHandCardData[viewChairId], pSendMing->bCardData, sizeof(BYTE)*LANDLOAD_CARD_COUNT);
    playGameSound(pSendMing->wChairID, 8);
    return true;
}

//牌数据发送完成
bool DDZGameViewLayer::OnSubSendOver(const void* pBuffer, WORD wDataSize)
{
    m_SendCardCount = 0;
    
    for (int i = 0; i < 3; i++)
    {
        Sprite* diCardback = Sprite::createWithSpriteFrameName("backCard.png");
        diCardback->setScale(0.5f);
        diCardback->setPosition(Vec2(850 + i*110, 1000));
        addChild(diCardback, 1, m_DiBackCardTag[i]);
    }
    
    if (!m_bMingPai[GetMeChairID()])
    {
        Menu *showCard = CreateButton("btn_show.png", Vec2(960, 350), Btn_MingPai);
        addChild(showCard, 200, Btn_MingPai);
    }
    schedule(schedule_selector(DDZGameViewLayer::SendCard), 0.10f);
    return true;
}

bool DDZGameViewLayer::OnSubSendFinish(const void* pBuffer, WORD wDataSize)
{
    ASSERT(wDataSize == sizeof(CMD_S_DDZ_LandScore));
    if (wDataSize != sizeof(CMD_S_DDZ_LandScore)) return false;

    auto ming = getChildByTag(Btn_MingPai);
    if (ming)
        ming->removeFromParent();
    
    CMD_S_DDZ_LandScore *pLandScore = (CMD_S_DDZ_LandScore *)pBuffer;
    
    for (int i = 0; i < GAME_PLAYER_DDZ; i++)
        m_HandCardCount[i] = FARMER_CARD_COUNT;
    
    m_wCurrentUser = pLandScore->wCurrentUser;
    m_CurGameState = GS_WK_SCORE;
    if (m_wCurrentUser == GetMeChairID())
    {
        Menu* callLand = CreateButton("btn_callland.png" ,Vec2(760, 350), Btn_CallLand);
        addChild(callLand , 100, Btn_CallLand);
        
        Menu *noCallLand = CreateButton("btn_noCallLand.png" ,Vec2(1160,350), Btn_NoCallLand);
        addChild(noCallLand , 100, Btn_NoCallLand);
    }
    
    StartTime(30, g_GlobalUnits.SwitchViewChairID(m_wCurrentUser));
    return true;
}

//重新发牌
bool DDZGameViewLayer::OnSubReSend(const void* pBuffer, WORD wDataSize)
{
    auto callLand = getChildByTag(Btn_CallLand);
    if (callLand)
        callLand->removeFromParent();
    
    auto noCallLand = getChildByTag(Btn_NoCallLand);
    if (noCallLand)
        noCallLand->removeFromParent();
    
    for (int i = 0; i < 3; i++)
        removeAllChildByTag(m_DiBackCardTag[i]);
    
    removeAllChildByTag(m_CallLandTag);

    return true;
}
//叫地主
bool DDZGameViewLayer::OnSubCallLand(const void* pBuffer, WORD wDataSize)
{
    ASSERT(wDataSize == sizeof(CMD_S_DDZ_LandScore));
    if (wDataSize != sizeof(CMD_S_DDZ_LandScore)) return false;
    
    CMD_S_DDZ_LandScore *pLandScore = (CMD_S_DDZ_LandScore *)pBuffer;
    
    removeAllChildByTag(m_CallLandTag);
    m_QiangdizhuIdx = 0;
    Vec2 pos[3] = {Vec2(600, 660), Vec2(960, 480), Vec2(1360, 660)};
    
    m_wCurrentUser = pLandScore->wCurrentUser;
    if(pLandScore->wCurrentUser == GetMeChairID())
    {
        if(pLandScore->m_bFlagDo)
        {
            Menu *snatch = CreateButton("btn_snatching.png" ,Vec2(760, 350), Btn_Snatch);
            addChild(snatch , 100, Btn_Snatch);
            
            Menu *noSnatch = CreateButton("btn_nosnatching.png" ,Vec2(1160, 350) , Btn_NoSnatch);
            addChild(noSnatch , 100, Btn_NoSnatch);
        }
        else
        {
            Menu* callLand = CreateButton("btn_callland.png" ,Vec2(760, 350), Btn_CallLand);
            addChild(callLand , 100, Btn_CallLand);
            
            Menu *noCallLand = CreateButton("btn_noCallLand.png" ,Vec2(1160,350), Btn_NoCallLand);
            addChild(noCallLand , 100, Btn_NoCallLand);
        }
    }
    
    string iconStr = "";
    if(pLandScore->m_bFlagDo)
    {
        m_LandUser = pLandScore->bLandUser;
        iconStr = "icon_call.png";
        playGameSound(pLandScore->bLandUser, 0);
    }
    else
    {
        iconStr = "icon_nocall.png";
        playGameSound(pLandScore->bLandUser, 1);
    }
    
    Sprite* callLandIcon = Sprite::createWithSpriteFrameName(iconStr);
    callLandIcon->setPosition(pos[g_GlobalUnits.SwitchViewChairID(pLandScore->bLandUser)]);
    callLandIcon->setTag(m_CallLandTag);
    addChild(callLandIcon);
    
    m_Multi = pLandScore->wMutli;
    m_MultiSpr->setVisible(true);
    m_MultiCnt->setVisible(true);
    m_MultiCnt->setString(StringUtils::toString(m_Multi));
    
    StartTime(30, g_GlobalUnits.SwitchViewChairID(m_wCurrentUser));
    return true;
}

//抢地主
bool DDZGameViewLayer::OnSubSnatchLand(const void* pBuffer, WORD wDataSize)
{
    ASSERT(wDataSize == sizeof(CMD_S_DDZ_LandScore));
    if (wDataSize != sizeof(CMD_S_DDZ_LandScore)) return false;

    removeAllChildByTag(m_CallLandTag);
    Vec2 pos[3] = {Vec2(600, 660), Vec2(960, 480), Vec2(1360, 660)};
    
    CMD_S_DDZ_LandScore *pLandScore = (CMD_S_DDZ_LandScore *)pBuffer;

    m_wCurrentUser = pLandScore->wCurrentUser;
    if (m_wCurrentUser == GetMeChairID())
    {
        Menu *snatch = CreateButton("btn_snatching.png" ,Vec2(760, 350), Btn_Snatch);
        addChild(snatch , 100, Btn_Snatch);
        
        Menu *noSnatch = CreateButton("btn_nosnatching.png" ,Vec2(1160, 350), Btn_NoSnatch);
        addChild(noSnatch , 100, Btn_NoSnatch);
    }
    
    string iconStr = "";
    if(pLandScore->m_bFlagDo)
    {
        iconStr = "icon_snatching.png";
        playGameSound(pLandScore->bLandUser, m_QiangdizhuIdx+2);
    }
    else
    {
        iconStr = "icon_nosnatching.png";
        playGameSound(pLandScore->bLandUser, 5);
    }
    m_QiangdizhuIdx++;
    
    Sprite* callLandIcon = Sprite::createWithSpriteFrameName(iconStr);
    callLandIcon->setPosition(pos[g_GlobalUnits.SwitchViewChairID(pLandScore->bLandUser)]);
    callLandIcon->setTag(m_CallLandTag);
    addChild(callLandIcon);
   
    m_Multi = pLandScore->wMutli;
    m_MultiCnt->setString(StringUtils::toString(m_Multi));

    StartTime(30, g_GlobalUnits.SwitchViewChairID(m_wCurrentUser));
    return true;
}

//加倍
bool DDZGameViewLayer::OnSubAddScore(const void* pBuffer, WORD wDataSize)
{
    ASSERT(wDataSize == sizeof(CMD_S_DDZ_GameStart));
    if (wDataSize != sizeof(CMD_S_DDZ_GameStart)) return false;
    
    CMD_S_DDZ_GameStart *pAddScore = (CMD_S_DDZ_GameStart *)pBuffer;
    m_LandUser = pAddScore->wLandUser;
    
    string iconStr = "";
    if (m_Multi != pAddScore->wMulti)
    {
        iconStr = "icon_snatching.png";
        playGameSound(m_wCurrentUser, m_QiangdizhuIdx+2);
    }
    else
    {
        if (pAddScore->bIsQiang)
        {
            iconStr = "icon_nosnatching.png";
            playGameSound(m_wCurrentUser, 5);
        }
        else
        {
            iconStr = "icon_call.png";
            playGameSound(m_wCurrentUser, 0);
        }
    }
    Vec2 pos[3] = {Vec2(600, 660), Vec2(960, 480), Vec2(1360, 660)};
    Sprite* callLandIcon = Sprite::createWithSpriteFrameName(iconStr);
    callLandIcon->setPosition(pos[g_GlobalUnits.SwitchViewChairID(m_wCurrentUser)]);
    callLandIcon->setTag(m_CallLandTag);
    addChild(callLandIcon);
    
    m_CurGameState = GS_WK_JIABEI;
    m_wCurrentUser = pAddScore->wCurrentUser;
    m_LandUser = pAddScore->wLandUser;
    
    for (int i = 0; i < GAME_PLAYER_DDZ; i++)
    {
        string roleStr = (pAddScore->wLandUser == i) ? "icon_landlord.png" : "icon_farmer.png";
        
        Sprite* roleType = Sprite::createWithSpriteFrameName(roleStr);
        auto viewChair = g_GlobalUnits.SwitchViewChairID(i);
        roleType->setPosition(getRoleTypePos(viewChair));
        addChild(roleType, 0, m_roletypeTag[i]);
    }
    
    showDiCard(pAddScore->bBackCard);
    
    auto landChair = g_GlobalUnits.SwitchViewChairID(m_LandUser);
    m_HandCardCount[landChair] = LANDLOAD_CARD_COUNT;
    
    if (1 == m_bMingPai[m_LandUser] || GetMeChairID() == m_LandUser)
    {
        for (int i = 0; i < 3; i ++)
            m_cbHandCardData[landChair][i+FARMER_CARD_COUNT] = pAddScore->bBackCard[i];
    }
    onSortCard(landChair);
    
    Menu *doubleBtn = CreateButton("btn_double.png", Vec2(760, 350), Btn_AddScore);
    addChild(doubleBtn, 100, Btn_AddScore);
    
    Menu *noDouble = CreateButton("btn_nodouble.png" ,Vec2(1160, 350), Btn_NoAddScore);
    addChild(noDouble , 100, Btn_NoAddScore);
    
    const tagUserData* pUserData = GetMeUserData();
    if (nullptr == pUserData && pUserData->lScore > pAddScore->wLimitJia)
    {
        doubleBtn->setEnabled(false);
        doubleBtn->setColor(Color3B(100, 100, 100));
    }
    
    m_Multi = pAddScore->wMulti;
    m_MultiCnt->setString(StringUtils::toString(m_Multi));

    StartTime(30, 1);
    return true;
}

//加倍结果
bool DDZGameViewLayer::OnSubAddScoreResult(const void* pBuffer, WORD wDataSize)
{
    ASSERT(wDataSize == sizeof(CMD_S_DDZ_UserJia));
    if (wDataSize != sizeof(CMD_S_DDZ_UserJia)) return false;
    
    removeAllChildByTag(m_CallLandTag);
    Vec2 pos[3] = {Vec2(600, 660), Vec2(960, 480), Vec2(1360, 660)};
    CMD_S_DDZ_UserJia *pAddScoreRes = (CMD_S_DDZ_UserJia *)pBuffer;
    
    auto viewChair = g_GlobalUnits.SwitchViewChairID(pAddScoreRes->wLandUser);
    string resStr = (1 == pAddScoreRes->bIsJia) ? "icon_double.png" : "icon_nodouble.png";
    Sprite* addSpr = Sprite::createWithSpriteFrameName(resStr);
    addSpr->setPosition(pos[viewChair]);
    addChild(addSpr, 10, m_AddScoreTag);
    
    if (1 == pAddScoreRes->bIsJia)
    {
        playGameSound(pAddScoreRes->wLandUser, 6);
    }
    else
        playGameSound(pAddScoreRes->wLandUser, 7);
    
    if (pAddScoreRes->wLandUser == GetMeChairID())
        StopTime();
    
    return true;
}

//明牌
bool DDZGameViewLayer::OnSubMingPai(const void *pBuffer, WORD wDataSize)
{
    //效验参数
    ASSERT(wDataSize==sizeof(CMD_S_DDZ_SendMing));
    if (wDataSize!=sizeof(CMD_S_DDZ_SendMing)) return false;
    CMD_S_DDZ_SendMing *pSendMing = (CMD_S_DDZ_SendMing *)pBuffer;
    
    m_bMingPai[pSendMing->wChairID] = 1;
    if (GetMeChairID() != pSendMing->wChairID)
    {
        auto viewChair = g_GlobalUnits.SwitchViewChairID(pSendMing->wChairID);
        
        m_HandCardCount[viewChair] = (0 == pSendMing->bCardData[FARMER_CARD_COUNT]) ? FARMER_CARD_COUNT : LANDLOAD_CARD_COUNT;
        
        CopyMemory(m_cbHandCardData[viewChair], pSendMing->bCardData, sizeof(pSendMing->bCardData));
        m_CardControl[viewChair].SetCardData(m_cbHandCardData[viewChair], m_HandCardCount[viewChair]);
    }
    
    int mult = pSendMing->wMulti;
    while (mult > 0) {
        m_Multi *= 2;
        mult--;
    }
    
    m_MultiCnt->setString(StringUtils::toString(m_Multi));
    playGameSound(pSendMing->wChairID, 8);
    return true;
}

//游戏开始
bool DDZGameViewLayer::OnSubGameStart(const void * pData, WORD wDataSize)
{
    //效验参数
    ASSERT(wDataSize==sizeof(CMD_S_DDZ_Start));
    if (wDataSize!=sizeof(CMD_S_DDZ_Start)) return false;
    
    //变量定义
    Vec2 pos[3] = {Vec2(600, 660), Vec2(960, 480), Vec2(1360, 660)};
    CMD_S_DDZ_Start * pGameStart = (CMD_S_DDZ_Start *)pData;
    
    m_CurGameState = GS_WK_PLAYING;
    for (int i = 0; i < 3; i++)
    {
        removeAllChildByTag(m_DiBackCardTag[i]);
    }
    setGameStatus(GS_WK_PLAYING);
    
    m_wCurrentUser = pGameStart->wCurrentUser;
    m_wCurOutCardUser = m_LandUser;
    
    string resStr = (1 == pGameStart->bIsJia) ? "icon_double.png" : "icon_nodouble.png";
    Sprite* addSpr = Sprite::createWithSpriteFrameName(resStr);
    addSpr->setPosition(pos[g_GlobalUnits.SwitchViewChairID(m_wCurrentUser)]);
    addChild(addSpr, 10, m_AddScoreTag);
    
    auto viewChair = g_GlobalUnits.SwitchViewChairID(m_LandUser);
    auto func = CallFunc::create(CC_CALLBACK_0(DDZGameViewLayer::removeAddDouble, this));
    addSpr->runAction(Sequence::create(DelayTime::create(2.0f), func, nullptr));
    
    if (1 == pGameStart->bIsJia)
    {
        playGameSound(m_wCurrentUser, 6);
    }
    else
        playGameSound(m_wCurrentUser, 7);
    
    if (GetMeChairID() == m_LandUser)
    {
        Menu *outcard = CreateButton("btn_out.png", Vec2(960, 350), Btn_OutCard);
        addChild(outcard, 200, Btn_OutCard);
        
        if (m_bAutoPlay)
        {
            AutoOpr();
        }
    }
    
    UpdateOutCardButton();
    StartTime(30, viewChair);
    return true;
}

//用户出牌
bool DDZGameViewLayer::OnSubOutCard(const void * pData, WORD wDataSize)
{
    CMD_S_DDZ_OutCard *pOutCard = (CMD_S_DDZ_OutCard *)pData;
    
    setGameStatus(GS_PLAYING);
    auto ming = getChildByTag(Btn_MingPai);
    if (ming)
        ming->removeFromParent();
    
    removeAllChildByTag(m_NoOutTag);
    removeAllChildByTag(m_AddScoreTag);
    
    m_HintIndex = 0;
    m_wCurrentUser = pOutCard->wCurrentUser;
    m_wCurOutCardUser = pOutCard->wOutCardUser;
    m_TurnCardCount = pOutCard->bCardCount;
    ZeroMemory(m_TurnCardData, sizeof(m_TurnCardData));
    CopyMemory(m_TurnCardData, pOutCard->bCardData, sizeof(pOutCard->bCardData));
   
    m_OutCardControl.ClearShowOutCard();
    m_OutCardControl.ShowOutCard(m_TurnCardData, m_TurnCardCount);
    
    auto cardType = m_GameLogic.GetCardType(m_TurnCardData, m_TurnCardCount);
    auto cardValue = m_GameLogic.GetCardValueForSound(m_TurnCardData[0]);
    playOutCardSound(pOutCard->wOutCardUser, cardType, cardValue);
    if (cardType == CT_MISSILE_CARD)
    {
        m_Multi *= 4;
         m_MultiCnt->setString(StringUtils::toString(m_Multi));
    }
    else if (cardType == CT_BOMB_CARD)
    {
        m_Multi *= 2;
         m_MultiCnt->setString(StringUtils::toString(m_Multi));
    }
    
    auto OutViewChair = g_GlobalUnits.SwitchViewChairID(m_wCurOutCardUser);
    if (1 == m_bMingPai[m_wCurOutCardUser] || GetMeChairID() == m_wCurOutCardUser)
        m_GameLogic.RemoveCard(m_TurnCardData, m_TurnCardCount, m_cbHandCardData[OutViewChair], m_HandCardCount[OutViewChair]);
    
    m_HandCardCount[OutViewChair] -= m_TurnCardCount;
    
    if (m_HandCardCount[OutViewChair] > 0)
        m_CardControl[OutViewChair].SetCardData(m_cbHandCardData[OutViewChair], m_HandCardCount[OutViewChair]);
    
    if (GetMeChairID() == m_wCurrentUser)
    {
        Menu *outcard = CreateButton("btn_out.png", Vec2(1260, 350), Btn_OutCard);
        addChild(outcard, 200, Btn_OutCard);
        outcard->setEnabled(false);
        outcard->setColor(Color3B(100,100,100));
        
        Menu *pop = CreateButton("btn_prot.png", Vec2(960, 350), Btn_Pop);
        addChild(pop, 200, Btn_Pop);
        
        Menu *nooutcard = CreateButton("btn_noout.png", Vec2(660, 350), Btn_Pass);
        addChild(nooutcard, 200, Btn_Pass);
        
        if (m_bAutoPlay)
        {
            AutoOpr();
        }
    }
    
    if (m_HandCardCount[OutViewChair] <= 5)
    {
        if (!m_bAlert[OutViewChair])
        {
            m_bAlert[OutViewChair] = true;
            LoadAlert(OutViewChair);
            SoundUtil::sharedEngine()->playWav("DDZ/sound/Special_alert", false);
        }
    }
    
    if (m_HandCardCount[OutViewChair] == 2)
    {
        playOutCardSound(pOutCard->wOutCardUser, 100, 2);
    }
    
    if (m_HandCardCount[OutViewChair] == 1)
    {
        playOutCardSound(pOutCard->wOutCardUser, 100, 1);
    }
    UpdateOutCardButton();
    StartTime(30, g_GlobalUnits.SwitchViewChairID(m_wCurrentUser));
    return true;
}

//用户放弃
bool DDZGameViewLayer::OnSubPassCard(const void * pData, WORD wDataSize)
{
    ASSERT(wDataSize == sizeof(CMD_S_DDZ_PassCard));
    if (wDataSize != sizeof(CMD_S_DDZ_PassCard)) return false;
    CMD_S_DDZ_PassCard *pPassCard = (CMD_S_DDZ_PassCard *)pData;
    
    removeAllChildByTag(m_NoOutTag);
    Vec2 pos[3] = {Vec2(600, 660), Vec2(960, 480), Vec2(1360, 660)};
    Sprite* pass = Sprite::createWithSpriteFrameName("icon_noout.png");
    auto viewChair = g_GlobalUnits.SwitchViewChairID(pPassCard->wPassUser);
    pass->setPosition(pos[viewChair]);
    addChild(pass, 10, m_NoOutTag);
    
    int rand = getRand(9, 12);
    playGameSound(pPassCard->wPassUser, rand);
    
    m_HintIndex = 0;
    m_NewTurn = pPassCard->bNewTurn;
    m_wCurrentUser = pPassCard->wCurrentUser;
    if (m_NewTurn)
    {
        ZeroMemory(m_TurnCardData, sizeof(m_TurnCardData));
        m_TurnCardCount = 0;
    }
    if (GetMeChairID() == m_wCurrentUser)
    {
        Menu *outcard = CreateButton("btn_out.png", Vec2(1260, 350), Btn_OutCard);
        addChild(outcard, 200, Btn_OutCard);
        
        Menu *pop = CreateButton("btn_prot.png", Vec2(960, 350), Btn_Pop);
        addChild(pop, 200, Btn_Pop);
        
        Menu *nooutcard = CreateButton("btn_noout.png", Vec2(660, 350), Btn_Pass);
        addChild(nooutcard, 200, Btn_Pass);
        
        if (m_bAutoPlay)
        {
            AutoOpr();
        }
    }
    
    StartTime(30, g_GlobalUnits.SwitchViewChairID(pPassCard->wCurrentUser));
    m_GameState = enGameOpenCard;
    UpdateOutCardButton();
    return true;
}

//游戏结束
bool DDZGameViewLayer::OnSubGameEnd(const void * pData, WORD wDataSize)
{
	ASSERT(wDataSize == sizeof(CMD_S_DDZ_GameEnd));
	if (wDataSize != sizeof(CMD_S_DDZ_GameEnd)) return false;
	CMD_S_DDZ_GameEnd *pGameEnd = (CMD_S_DDZ_GameEnd *)pData;
    
    //设置定时器
    m_CurGameState = GS_FREE;
    setGameStatus(GS_FREE);
    StartTime(30, g_GlobalUnits.SwitchViewChairID(GetMeChairID()));
    m_HintIndex = 0;
    
    for (int i = 0; i < 3; i++)
    {
        removeAllChildByTag(m_AlertAnimTag[i]);
        removeAllChildByTag(m_roletypeTag[i]);
        m_bAlert[i] = false;
    }
    
//    删除其他按钮
    removeAllChildByTag(m_CallLandTag);
    removeAllChildByTag(m_AddScoreTag);
    removeAllChildByTag(m_NoOutTag);
    
    auto callLand = getChildByTag(Btn_CallLand);
    if (callLand)
        callLand->removeFromParent();
    
    auto noCallLand = getChildByTag(Btn_NoCallLand);
    if (noCallLand)
        noCallLand->removeFromParent();
    
    auto snatch = getChildByTag(Btn_Snatch);
    if (snatch)
        snatch->removeFromParent();
    
    auto noSnatch = getChildByTag(Btn_NoSnatch);
    if (noSnatch)
        noSnatch->removeFromParent();
    
    auto Double = getChildByTag(Btn_AddScore);
    if (Double)
        Double->removeFromParent();
    
    auto noDouble = getChildByTag(Btn_NoAddScore);
    if (noDouble)
        noDouble->removeFromParent();
    
    auto sort = getChildByTag(Btn_MingPai);
    if (sort)
        sort->removeFromParent();
    
    auto out = getChildByTag(Btn_OutCard);
    if (out)
        out->removeFromParent();
    
    auto pass = getChildByTag(Btn_Pass);
    if (pass)
        pass->removeFromParent();
    
    auto pop = getChildByTag(Btn_Pop);
    if (pop)
        pop->removeFromParent();
    
    //显示开始按钮
    Menu *start = CreateButton("btn_start.png" ,Vec2(660 ,300), Btn_Ready);
    addChild(start , 200, Btn_Ready);
    
    Menu *mingStart = CreateButton("btn_showout.png" ,Vec2(960, 300), Btn_ReadyMing);
    addChild(mingStart, 200, Btn_ReadyMing);

    Menu *changeTable = CreateButton("btn_changeTable.png" ,Vec2(1260, 300), Btn_ChangeTabel);
    addChild(changeTable, 200, Btn_ChangeTabel);
    
    int index = 0;
    for (int i = 0; i < GAME_PLAYER_DDZ; i++) {
        auto viewChair = g_GlobalUnits.SwitchViewChairID(i);
        for (int j = 0; j < pGameEnd->bCardCount[i]; j++)
        {
            m_cbHandCardData[viewChair][j] = pGameEnd->bCardData[index];
            index++;
        }
        m_CardControl[viewChair].SetCardData(m_cbHandCardData[viewChair], pGameEnd->bCardCount[i]);
    }
    
    Sprite* bg = Sprite::createWithSpriteFrameName("bg_result.png");
    bg->setPosition(Vec2(960, 650));
    addChild(bg, 300, m_resultTag);
    bg->setVisible(false);
    
    Sprite* totalText = Sprite::createWithSpriteFrameName("text_total.png");
    totalText->setPosition(Vec2(360, 370));
    bg->addChild(totalText);
    
    Sprite* x = Sprite::createWithSpriteFrameName("icon_x.png");
    x->setPosition(Vec2(450, 370));
    bg->addChild(x);
    
    LabelAtlas *multi = LabelAtlas::create(StringUtils::toString(pGameEnd->wMulti), "DDZ/ddz_num2.png", 32, 37, '0');
    multi->setAnchorPoint(Vec2(0, 0.5f));
    multi->setPosition(Vec2(470, 370));
    bg->addChild(multi);
    
    int xc = 0;
    if (pGameEnd->wMingMulti > 1)
    {
        string mStr = "明牌x" + StringUtils::toString(pGameEnd->wMingMulti);
        Label *mBei = Label::createWithSystemFont(mStr, _GAME_FONT_NAME_1_, 46);
        mBei->setPosition(Vec2(140 + xc * 160, 310));
        bg->addChild(mBei);
        xc += 1;
    }
    
    if (pGameEnd->cbReason)
    {
        SoundUtil::sharedEngine()->playWav("DDZ/sound/Special_Escape", false);
    }
    else if (pGameEnd->bSpring > 1)
    {
        SoundUtil::sharedEngine()->playWav("DDZ/sound/Special_flower", false);
        Label *cBei = Label::createWithSystemFont("春天x3", _GAME_FONT_NAME_1_, 46);
        cBei->setPosition(Vec2(140 + xc * 160, 310));
        bg->addChild(cBei);
        xc += 1;
    }
    else if (pGameEnd->lGameScore[GetMeChairID()] > 0)
    {
        SoundUtil::sharedEngine()->playWav("DDZ/sound/MusicEx_Win", false);
    }
    else
    {
        SoundUtil::sharedEngine()->playWav("DDZ/sound/MusicEx_Lose", false);
    }
    
    for (int i = 0; i < GAME_PLAYER_DDZ; i++)
    {
        Label* name = Label::createWithSystemFont(m_PlayerName[i], _GAME_FONT_NAME_1_, 46);
        name->setPosition(Vec2(60, 235 - i * 70));
        name->setAnchorPoint(Vec2(0, 0.5f));
        name->setColor(Color3B(140, 71, 19));
        bg->addChild(name);
        
        string jiabeiStr = (pGameEnd->bJiaMulti[i] >= 2) ? "加倍x2" : "加倍x1";
        Label *mBei = Label::createWithSystemFont(jiabeiStr, _GAME_FONT_NAME_1_, 46);
        mBei->setPosition(Vec2(480, 235 - i * 70));
        bg->addChild(mBei);
        mBei->setColor(Color3B::GREEN);
    
        string scoreStr = StringUtils::toString(pGameEnd->lGameScore[i]);
        if (pGameEnd->lGameScore[i] > 0)
            scoreStr = "+" + scoreStr;
        
        Label *winScore = Label::createWithSystemFont(scoreStr, _GAME_FONT_NAME_1_, 46);
        winScore->setPosition(Vec2(700, 235 - i * 70));
        if (pGameEnd->lGameScore[i] > 0)
            winScore->setColor(Color3B::GREEN);
        else
            winScore->setColor(Color3B::RED);
        bg->addChild(winScore);
    }
    scheduleOnce(schedule_selector(DDZGameViewLayer::drawGameEnd), 3.0f);
    
    if (m_bAutoPlay)
    {
        m_bAutoPlay = false;
        auto autoBtn = getChildByTag(Btn_CancelAuto);
        if (autoBtn)
            autoBtn->removeFromParent();
        
        Menu *AutoPlay = CreateButton("btn_auto.png", Vec2(1770, 350), Btn_AutoPlay);
        addChild(AutoPlay, 200, Btn_AutoPlay);
    }
    return true;
}

//游戏消息
bool DDZGameViewLayer::OnGameMessage(WORD wSubCmdID, const void * pBuffer, WORD wDataSize)
{
	CCLOG("RECEIVE MESSAGE CMD[%d]" , wSubCmdID);
	switch (wSubCmdID)
	{
        case SUB_S_DDZ_SEND_CARD://发牌
        {
            return OnSubSendCard(pBuffer, wDataSize);
        }
        case SUB_S_DDZ_SEND_MING://玩家明牌数据
        {
            return OnSubSendMing(pBuffer, wDataSize);
        }
        case SUB_S_DDZ_SEND_OVER://牌数据发送完成
        {
            return OnSubSendOver(pBuffer, wDataSize);
        }
        case SUB_S_DDZ_SEND_FINISH://牌数据发送完成
        {
            return OnSubSendFinish(pBuffer, wDataSize);
        }
        case SUB_S_DDZ_RSEND_CARD:
        {
            return OnSubReSend(pBuffer, wDataSize);
        }
        case SUB_S_DDZ_LAND_SCORE://叫地主
        {
            return OnSubCallLand(pBuffer, wDataSize);
        }
        case SUB_S_DDZ_LAND_QIANG://抢地主
        {
            return OnSubSnatchLand(pBuffer, wDataSize);
        }
        case SUB_S_DDZ_GAME_JAIBEI://地主确定，显示加倍
        {
            return OnSubAddScore(pBuffer, wDataSize);
        }
        case SUB_S_DDZ_GAME_ISJIA://玩家加倍
        {
            return OnSubAddScoreResult(pBuffer, wDataSize);
        }
        case SUB_S_DDZ_GAME_START:		//游戏开始
		{
			return OnSubGameStart(pBuffer,wDataSize);
		}
	case SUB_S_DDZ_OUT_CARD://玩家出牌
		{
			return OnSubOutCard(pBuffer, wDataSize);
		}
	case SUB_S_DDZ_PASS_CARD://玩家过牌
		{
			return OnSubPassCard(pBuffer, wDataSize);
		}
	case SUB_S_DDZ_GAME_END://游戏结束
		{
			return OnSubGameEnd(pBuffer, wDataSize);
		}
        case SUB_S_DDZ_PLAY_MING:
        {
            return OnSubMingPai(pBuffer, wDataSize);
        }
	}
	return true;
}

//场景消息
bool DDZGameViewLayer::OnGameSceneMessage(BYTE cbGameStatus, const void * pBuffer, WORD wDataSize)
{
    if (m_bAutoPlay)
    {
        Menu *StopAutoPlay = CreateButton("btn_noauto.png", Vec2(1770, 350), Btn_CancelAuto);
        addChild(StopAutoPlay, 200, Btn_CancelAuto);
    }
    else
    {
        Menu *AutoPlay = CreateButton("btn_auto.png", Vec2(1770, 350), Btn_AutoPlay);
        addChild(AutoPlay, 200, Btn_AutoPlay);
    }
    
    switch (cbGameStatus)
	{
	case GS_FREE:			//空闲状态
		{		
			//效验数据
			ASSERT(wDataSize == sizeof(CMD_S_DDZ_StatusFree));
			if (wDataSize != sizeof(CMD_S_DDZ_StatusFree)) return false;
			setGameStatus(GS_FREE);
            m_CurGameState = GS_FREE;
            
			WORD wMeChairID = GetMeChairID();
			WORD ViewChairID = g_GlobalUnits.SwitchViewChairID(wMeChairID);

			StartTime(30, ViewChairID);

			//显示开始按钮
            Menu *start = CreateButton("btn_start.png" ,Vec2(660 ,300), Btn_Ready);
            addChild(start , 200, Btn_Ready);
            
            Menu *mingStart = CreateButton("btn_showout.png" ,Vec2(960, 300), Btn_ReadyMing);
            addChild(mingStart, 200, Btn_ReadyMing);
            
            Menu *changeTable = CreateButton("btn_changeTable.png" ,Vec2(1260, 300), Btn_ChangeTabel);
            addChild(changeTable, 200, Btn_ChangeTabel);
            return true;
		}
    case GS_WK_SCORE:
        {
            //效验数据
            ASSERT(wDataSize == sizeof(CMD_S_DDZ_StatusScore));
            if (wDataSize != sizeof(CMD_S_DDZ_StatusScore)) return false;
            
            CMD_S_DDZ_StatusScore *pStatusScore = (CMD_S_DDZ_StatusScore *)pBuffer;
            m_wCurrentUser = pStatusScore->wCurrentUser;

            for (int i = 0; i < GAME_PLAYER_DDZ; i++)
            {
                auto viewChair = g_GlobalUnits.SwitchViewChairID(i);
                
                CopyMemory(m_cbHandCardData[viewChair], pStatusScore->bCardData[i], sizeof(pStatusScore->bCardData[i]));
                
                m_HandCardCount[viewChair] = pStatusScore->bCardCount[i];
                
                m_bMingPai[i] = pStatusScore->bMingFlag[i];
                
                m_CardControl[viewChair].SetCardData(m_cbHandCardData[viewChair], m_HandCardCount[viewChair]);
            }

            if (1 == pStatusScore->bIsJiaoDiZhu && m_wCurrentUser == GetMeChairID())
            {
                Menu* callLand = CreateButton("btn_callland.png" ,Vec2(760, 350), Btn_CallLand);
                addChild(callLand , 100, Btn_CallLand);
                
                Menu *noCallLand = CreateButton("btn_noCallLand.png" ,Vec2(1160,350), Btn_NoCallLand);
                addChild(noCallLand , 100, Btn_NoCallLand);
            }
            
            StartTime(30, g_GlobalUnits.SwitchViewChairID(m_wCurrentUser));
            setGameStatus(GS_WK_SCORE);
            return true;
        }
	case GS_WK_JIABEI:
		{
			//效验数据
			ASSERT(wDataSize == sizeof(CMD_S_DDZ_StatusJiaBei));
			if (wDataSize != sizeof(CMD_S_DDZ_StatusJiaBei)) return false;

			CMD_S_DDZ_StatusJiaBei *pStatusAddScore = (CMD_S_DDZ_StatusJiaBei *)pBuffer;
            m_LandUser = pStatusAddScore->wLandUser;
            m_Multi = pStatusAddScore->wMulti;
            m_MultiCnt->setString(StringUtils::toString(m_Multi));
           
            if (1 == pStatusAddScore->bJiaBei[GetMeChairID()])
            {
                Menu *doubleBtn = CreateButton("btn_double.png", Vec2(760, 350), Btn_AddScore);
                addChild(doubleBtn, 100, Btn_AddScore);
                
                Menu *noDouble = CreateButton("btn_nodouble.png" ,Vec2(1160, 350), Btn_NoAddScore);
                addChild(noDouble , 100, Btn_NoAddScore);
                
                 if (GetMeUserData()->lScore > pStatusAddScore->wLimitJia)
                 {
                     doubleBtn->setEnabled(false);
                     doubleBtn->setColor(Color3B(100,100,100));
                 }
            }
            
            for (int i = 0; i < 3; i++)
            {
                string name = GetCardStringName(pStatusAddScore->bBackCard[i]);
                Sprite* cardSpr = Sprite::createWithSpriteFrameName(name);
                cardSpr->setScale(0.5f);
                cardSpr->setPosition(Vec2(850 + i*110, 1000));
                addChild(cardSpr, 0, m_DiCardTag[i]);
            }
            
            for (int i = 0; i < GAME_PLAYER_DDZ; i++)
            {
                auto viewChair = g_GlobalUnits.SwitchViewChairID(i);
                
                CopyMemory(m_cbHandCardData[viewChair], pStatusAddScore->bCardData[i], sizeof(pStatusAddScore->bCardData[i]));
                
                m_HandCardCount[viewChair] = pStatusAddScore->bCardCount[i];
                
                m_bMingPai[i] = pStatusAddScore->bMingFlag[i];
             
                m_CardControl[viewChair].SetCardData(m_cbHandCardData[viewChair], m_HandCardCount[viewChair]);
                
                string roleStr = (pStatusAddScore->wLandUser == i) ? "icon_landlord.png" : "icon_farmer.png";
                Sprite* roleType = Sprite::createWithSpriteFrameName(roleStr);
                roleType->setPosition(getRoleTypePos(viewChair));
                addChild(roleType, 0, m_roletypeTag[i]);
            }
            
            setGameStatus(GS_WK_JIABEI);
			StartTime(30, g_GlobalUnits.SwitchViewChairID(pStatusAddScore->wLandUser));
			return true;
		}
    case GS_WK_PLAYING:
        {
            //效验数据
            ASSERT(wDataSize == sizeof(CMD_S_DDZ_StatusPlay));
            if (wDataSize != sizeof(CMD_S_DDZ_StatusPlay)) return false;
            
            CMD_S_DDZ_StatusPlay *pStatusPlay = (CMD_S_DDZ_StatusPlay *)pBuffer;
            m_wCurrentUser = pStatusPlay->wCurrentUser;
            m_Multi = pStatusPlay->wMulti;
            m_Multi *= pStatusPlay->wBombTime;
            m_MultiCnt->setString(StringUtils::toString(m_Multi));
            
            for (int i = 0; i < GAME_PLAYER_DDZ; i++)
            {
                auto viewChair = g_GlobalUnits.SwitchViewChairID(i);
                
                CopyMemory(m_cbHandCardData[viewChair], pStatusPlay->bCardData[i], sizeof(pStatusPlay->bCardData[i]));
                
                m_HandCardCount[viewChair] = pStatusPlay->bCardCount[i];
                
                m_bMingPai[i] = pStatusPlay->bMingFlag[i];
                
                m_CardControl[viewChair].SetCardData(m_cbHandCardData[viewChair], m_HandCardCount[viewChair]);
                
                string roleStr = (pStatusPlay->wLandUser == i) ? "icon_landlord.png" : "icon_farmer.png";
                Sprite* roleType = Sprite::createWithSpriteFrameName(roleStr);
                roleType->setPosition(getRoleTypePos(viewChair));
                addChild(roleType, 0, m_roletypeTag[i]);
            }

            ZeroMemory(m_TurnCardData, sizeof(m_TurnCardData));
            CopyMemory(m_TurnCardData, pStatusPlay->bTurnCardData, sizeof(pStatusPlay->bTurnCardData));
            m_TurnCardCount = pStatusPlay->bTurnCardCount;
            
            m_OutCardControl.ClearShowOutCard();
            m_OutCardControl.ShowOutCard(m_TurnCardData, m_TurnCardCount);
            
            if (GetMeChairID() == m_wCurrentUser)
            {
                Menu *outcard = CreateButton("btn_out.png", Vec2(1260, 350), Btn_OutCard);
                addChild(outcard, 200, Btn_OutCard);
                outcard->setEnabled(false);
                outcard->setColor(Color3B(100,100,100));
                
                Menu *pop = CreateButton("btn_prot.png", Vec2(960, 350), Btn_Pop);
                addChild(pop, 200, Btn_Pop);
                
                Menu *nooutcard = CreateButton("btn_noout.png", Vec2(660, 350), Btn_Pass);
                addChild(nooutcard, 200, Btn_Pass);
            }
            m_wCurOutCardUser = pStatusPlay->wLastOutUser;
            
            UpdateOutCardButton();
            setGameStatus(GS_WK_PLAYING);
            StartTime(30, g_GlobalUnits.SwitchViewChairID(pStatusPlay->wCurrentUser));
            return true;
        }
            
    }
    return true;
}

string DDZGameViewLayer::GetCardStringName(BYTE card)
{
	if (card == 0) return "backCard.png";
	char tt[32];
	sprintf(tt,"%0x",card);
	BYTE _value = m_GameLogic.GetCardValue(card);
	BYTE _color = m_GameLogic.GetCardColor(card);
	string temp;
	if (_color == 0) temp = "fangkuai_";
	if (_color == 1) temp = "meihua_";
	if (_color == 2) temp = "hongtao_";
	if (_color == 3) temp = "heitao_";
	char _valstr[32];
	ZeroMemory(_valstr,32);
	if (card == 0x41) //小王
	{
		sprintf(_valstr,"xiaowang.png");
	}
	else if (card == 0x42)
	{
		sprintf(_valstr,"dawang.png");
	}
	else if (_value < 10)
	{
		sprintf(_valstr,"0%d.png",_value);
	}
	else 
	{
		sprintf(_valstr,"%d.png",_value);
	}

	temp+=_valstr;
	return temp;
}

void DDZGameViewLayer::callbackBt( Ref *pSender )
{
	Node *pNode = (Node *)pSender;
	switch (pNode->getTag())
	{
        case Btn_BackToLobby: // 返回大厅按钮
		{
			SoundUtil::sharedEngine()->playEffect("buttonMusic");
			BackToLobby();
			break;
		}
        case Btn_Seting: //设置
		{
			SoundUtil::sharedEngine()->playEffect("buttonMusic");
			GameLayerMove::sharedGameLayerMoveSink()->OpenSeting();
			break;
		}
        case Btn_Ready:
		{
			m_CardControl[0].RemoveAllCardData();
			m_CardControl[1].RemoveAllCardData();
            m_CardControl[2].RemoveAllCardData();
            m_OutCardControl.ClearShowOutCard();
            for (int i = 0; i < 3; i++)
            {
                removeAllChildByTag(m_DiCardTag[i]);
                removeAllChildByTag(m_DiBackCardTag[i]);
            }
            
			ClientSocketSink::sharedSocketSink()->SendData(MDM_GF_FRAME, SUB_GF_USER_READY);
            auto start = getChildByTag(Btn_Ready);
            if (start)
                start->removeFromParent();
            
            auto mingStart = getChildByTag(Btn_ReadyMing);
            if (mingStart)
                mingStart->removeFromParent();
            
            auto changeTable = getChildByTag(Btn_ChangeTabel);
            if (changeTable)
                changeTable->removeFromParent();
            
			break;
		}
        case Btn_ReadyMing:
        {
            m_CardControl[0].RemoveAllCardData();
            m_CardControl[1].RemoveAllCardData();
            m_CardControl[2].RemoveAllCardData();
            m_OutCardControl.ClearShowOutCard();
            CMD_C_Start start;
            start.wCurrentUser = GetMeChairID();
            start.bFlag = true;
            SendData(SUB_C_MINGPAI, &start, sizeof(start));
            ClientSocketSink::sharedSocketSink()->SendData(MDM_GF_FRAME, SUB_GF_USER_READY);
            
            auto star = getChildByTag(Btn_Ready);
            if (star)
                star->removeFromParent();
            
            auto mingStart = getChildByTag(Btn_ReadyMing);
            if (mingStart)
                mingStart->removeFromParent();
            
            auto changeTable = getChildByTag(Btn_ChangeTabel);
            if (changeTable)
                changeTable->removeFromParent();
        
            break;
        }
        case Btn_CallLand:
        {
            auto callLand = getChildByTag(Btn_CallLand);
            if (callLand)
                callLand->removeFromParent();
            
            auto noCallLand = getChildByTag(Btn_NoCallLand);
            if (noCallLand)
                noCallLand->removeFromParent();
            
            CMD_C_LandScore ladnScore;
            ladnScore.bLandScore = 1;
            SendData(SUB_C_LAND_SCORE, &ladnScore, sizeof(ladnScore));
            break;
        }
        case Btn_NoCallLand:
        {
            auto callLand = getChildByTag(Btn_CallLand);
            if (callLand)
                callLand->removeFromParent();
            
            auto noCallLand = getChildByTag(Btn_NoCallLand);
            if (noCallLand)
                noCallLand->removeFromParent();
            
            CMD_C_LandScore ladnScore;
            ladnScore.bLandScore = 0;
            SendData(SUB_C_LAND_SCORE, &ladnScore, sizeof(ladnScore));
            break;
        }
        case Btn_Snatch:
        {
            auto snatch = getChildByTag(Btn_Snatch);
            if (snatch)
                snatch->removeFromParent();
            
            auto noSnatch = getChildByTag(Btn_NoSnatch);
            if (noSnatch)
                noSnatch->removeFromParent();
            
            CMD_C_LandScore ladnScore;
            ladnScore.bLandScore = 1;
            SendData(SUB_C_LAND_ISQIANG, &ladnScore, sizeof(ladnScore));
            break;
        }
        case Btn_NoSnatch:
        {
            auto snatch = getChildByTag(Btn_Snatch);
            if (snatch)
                snatch->removeFromParent();
            
            auto noSnatch = getChildByTag(Btn_NoSnatch);
            if (noSnatch)
                noSnatch->removeFromParent();
            
            CMD_C_LandScore ladnScore;
            ladnScore.bLandScore = 0;
            SendData(SUB_C_LAND_ISQIANG, &ladnScore, sizeof(ladnScore));
            break;
        }
        case Btn_AddScore:
        {
            auto Double = getChildByTag(Btn_AddScore);
            if (Double)
                Double->removeFromParent();
            
            auto noDouble = getChildByTag(Btn_NoAddScore);
            if (noDouble)
                noDouble->removeFromParent();
            
            CMD_C_LandScore ladnScore;
            ladnScore.bLandScore = 1;
            SendData(SUB_C_LAND_ISJIA, &ladnScore, sizeof(ladnScore));
            break;
        }
        case Btn_NoAddScore:
        {
            auto Double = getChildByTag(Btn_AddScore);
            if (Double)
                Double->removeFromParent();
            
            auto noDouble = getChildByTag(Btn_NoAddScore);
            if (noDouble)
                noDouble->removeFromParent();
            
            CMD_C_LandScore ladnScore;
            ladnScore.bLandScore = 0;
            SendData(SUB_C_LAND_ISJIA, &ladnScore, sizeof(ladnScore));
            break;
        }
        case Btn_AutoPlay:
        {
            m_bAutoPlay = true;
            if (GetMeChairID() == m_wCurrentUser)
                m_StartTime = 1;
            
            auto autoBtn = getChildByTag(Btn_AutoPlay);
            if (autoBtn)
                autoBtn->removeFromParent();
            
            Menu *StopAutoPlay = CreateButton("btn_noauto.png", Vec2(1770, 350), Btn_CancelAuto);
            addChild(StopAutoPlay, 200, Btn_CancelAuto);
            break;
        }
        case Btn_CancelAuto:
        {
            m_bAutoPlay = false;
            auto autoBtn = getChildByTag(Btn_CancelAuto);
            if (autoBtn)
                autoBtn->removeFromParent();
            
            Menu *AutoPlay = CreateButton("btn_auto.png", Vec2(1770, 350), Btn_AutoPlay);
            addChild(AutoPlay, 200, Btn_AutoPlay);
            break;
        }
        case Btn_MingPai:
        {
            auto sort = getChildByTag(Btn_MingPai);
            if (sort)
                sort->removeFromParent();
            
            CMD_C_Ming ming;
            ming.wMulti=2;
            SendData(SUB_C_MING, &ming, sizeof(ming));
            break;
        }
        case Btn_Pop:
        {
            onPopCard();
            break;
        }
        case Btn_OutCard:
        {
            onOutCard();
            break;
        }
        case Btn_Pass:
        {
            auto out = getChildByTag(Btn_OutCard);
            if (out)
                out->removeFromParent();
            
            auto pass = getChildByTag(Btn_Pass);
            if (pass)
                pass->removeFromParent();
            
            auto pop = getChildByTag(Btn_Pop);
            if (pop)
                pop->removeFromParent();
            SendData(SUB_C_PASS_CARD);
            break;
        }
        case Btn_ChangeTabel:
        {
            CMD_GR_UserAutoSitReq req;
            req.ProcessVersion = 0;
            
            ClientSocketSink::sharedSocketSink()->SendData(MDM_GR_USER, SUB_GR_USER_AUTO_MATCH_REQ, &req, sizeof(req));
            break;
        }
    }
}

void DDZGameViewLayer::UpdateOutCardButton()
{
    BYTE shootCardCount = LANDLOAD_CARD_COUNT;
    BYTE shootCardData[LANDLOAD_CARD_COUNT];
    memset(shootCardData, 0, sizeof(shootCardData));
    m_CardControl[1].GetShootCard(shootCardData, shootCardCount);
    
    Menu* outCardBtn = dynamic_cast<Menu*>(getChildByTag(Btn_OutCard));
    if (nullptr == outCardBtn)
        return;

    if (shootCardCount <= 0)
    {
        outCardBtn->setEnabled(false);
        outCardBtn->setColor(Color3B(100, 100, 100));
        return;
    }
    
    //类型判断
    BYTE bCardType = m_GameLogic.GetCardType(shootCardData,shootCardCount);
    if (bCardType==CT_INVALID)
    {
        outCardBtn->setEnabled(false);
        outCardBtn->setColor(Color3B(100, 100, 100));
        return;
    }
    
    //跟牌判断
    if (GetMeChairID() != m_wCurOutCardUser)
    {
        bool canOut = m_GameLogic.CompareCard(shootCardData, m_TurnCardData, shootCardCount, m_TurnCardCount);
        if (!canOut)
        {
            outCardBtn->setEnabled(false);
            outCardBtn->setColor(Color3B(100, 100, 100));
            return;
        }
    }

    outCardBtn->setEnabled(true);
    outCardBtn->setColor(Color3B::WHITE);
}

void DDZGameViewLayer::onOutCard()
{
    auto out = getChildByTag(Btn_OutCard);
    if (out)
        out->removeFromParent();
    
    auto pass = getChildByTag(Btn_Pass);
    if (pass)
        pass->removeFromParent();
    
    auto pop = getChildByTag(Btn_Pop);
    if (pop)
        pop->removeFromParent();
    
    auto sort = getChildByTag(Btn_MingPai);
    if (sort)
        sort->removeFromParent();
    
    CMD_C_OutCard outCard;
    memset(&outCard, 0, sizeof(outCard));
    outCard.bCardCount = CountArray(outCard.bCardData);
    m_CardControl[1].GetShootCard(outCard.bCardData, outCard.bCardCount);
    SendData(SUB_C_OUT_CART, &outCard, sizeof(outCard)-sizeof(outCard.bCardData)+outCard.bCardCount*sizeof(BYTE));
}

void DDZGameViewLayer::onPopCard()
{
    BYTE resultCardData[m_TurnCardCount];
    BYTE resultCount = 0;
    ZeroMemory(resultCardData, sizeof(resultCardData));
    
    if (0 == m_HintIndex)
        CopyMemory(m_HintCardData, m_TurnCardData, sizeof(m_TurnCardData));
    
    m_GameLogic.HintOutCard(m_cbHandCardData[1], m_HandCardCount[1], m_HintCardData, m_TurnCardCount, resultCardData, resultCount);
    CopyMemory(m_HintCardData, resultCardData, sizeof(resultCardData));
    m_HintIndex++;
    
    if (0 == resultCount)
    {
        m_HintIndex = 0;
    }
   
    m_CardControl[1].SetShootCard(m_HintCardData, resultCount);
    UpdateOutCardButton();
}

void DDZGameViewLayer::onSortCard(WORD ChairId)
{
    m_GameLogic.SortCardList(m_cbHandCardData[ChairId], m_HandCardCount[ChairId]);
    m_CardControl[ChairId].SetCardData(m_cbHandCardData[ChairId], m_HandCardCount[ChairId]);
}

void DDZGameViewLayer::removeAddDouble()
{
    removeAllChildByTag(m_AddScoreTag);
}

void DDZGameViewLayer::drawGameEnd(float dt)
{
    m_MultiCnt->setVisible(false);
    m_MultiSpr->setVisible(false);
    
    auto bg = getChildByTag(m_resultTag);
    if (bg)
        bg->setVisible(true);
}

int DDZGameViewLayer::getRand(int start,int end)
{
    //产生一个从start到end间的随机数
    srand((unsigned)time(0));
    int randNum = std::rand();
    int swpan = end - start + 1;
    int result = randNum % swpan + start;
    return result;
}

void DDZGameViewLayer::update(float dt)
{
    if (!CocosDenshion::SimpleAudioEngine::getInstance()->isBackgroundMusicPlaying())
    {
        m_CurBackMusic = "DDZ/sound/MusicEx_Normal.wav";
        CocosDenshion::SimpleAudioEngine::getInstance()->playBackgroundMusic(m_CurBackMusic.c_str(), true);
    }
}

void DDZGameViewLayer::backLoginView( Ref *pSender )
{
	IGameView::backLoginView(pSender);	
}

void DDZGameViewLayer::OnQuit()
{
	m_pGameScene->removeChild(this);
}

void DDZGameViewLayer::LoadAlert(int chair)
{
    Vector<AnimationFrame*> arrayOfAnimationFrameNames;
    for(int i=0; i < 16; ++i)
    {
        char buffer[50]= {0};
        sprintf(buffer, "alert_%d.png", i);
        
        SpriteFrame *pSpriteFrame = SpriteFrameCache::getInstance()->getSpriteFrameByName(buffer);
        ValueMap userInfo;
        AnimationFrame *pAnimationFrame = AnimationFrame::create(pSpriteFrame, 0.5f, userInfo);
        arrayOfAnimationFrameNames.pushBack(pAnimationFrame);
    }
    //生成动画数据对象
    Animation *pAnimation = Animation::create(arrayOfAnimationFrameNames, 0.3f);
    //生成动画动作对象
    Animate* animate = Animate::create(pAnimation);
    
    Vec2 pos[3] = {Vec2(250, 1000), Vec2(250, 230), Vec2(1670, 1000)};
    Sprite* pSprite = Sprite::create();
    pSprite->setPosition(pos[chair]);
    this->addChild(pSprite, 10000,  m_AlertAnimTag[chair]);
    
    auto seq = Sequence::create(DelayTime::create(0.05f), animate, NULL);
    RepeatForever* repeat = RepeatForever::create(seq);//无限循环
    pSprite->runAction(repeat);

}

void DDZGameViewLayer::OnAnimate(int nCardType)
{
    switch (nCardType)
    {
        case 1: LoadAnim("spr", 9, false); break;
        case 2: LoadAnim("plane", 4, true); break;
        case 3: LoadAnim("rocket", 10 ,false); break;
        case 5: LoadAnim("liandui", 8, false); break;
        case 6: LoadAnim("shunzi", 11, false); break;
        case 7: LoadAnim("zadan", 8, false); break;
        default: break;
    }
}

void DDZGameViewLayer::LoadAnim(const char* name, int spriteCount, bool loop)
{
    removeAllChildByTag(m_AnimTga);
    Vector<AnimationFrame*> arrayOfAnimationFrameNames;
    for(int i=0; i < spriteCount; ++i)
    {
        char buffer[50]= {0};
        sprintf(buffer, "%s_%d.png",name, i);
        
        SpriteFrame *pSpriteFrame = SpriteFrameCache::getInstance()->getSpriteFrameByName(buffer);
        ValueMap userInfo;
        AnimationFrame *pAnimationFrame = AnimationFrame::create(pSpriteFrame, 0.5f, userInfo);
        arrayOfAnimationFrameNames.pushBack(pAnimationFrame);
    }
    //生成动画数据对象
    Animation *pAnimation = Animation::create(arrayOfAnimationFrameNames, 0.3f);
    //生成动画动作对象
    Animate* animate = Animate::create(pAnimation);
    
    Sprite* pSprite2 = Sprite::create();
    this->addChild(pSprite2,10000, m_AnimTga);
    pSprite2->setPosition(Vec2(_STANDARD_SCREEN_CENTER_.x, _STANDARD_SCREEN_CENTER_.y+120));
    ActionInstant *func = CallFuncN::create(CC_CALLBACK_1(DDZGameViewLayer::AnimEnd, this));

    if (loop)
    {
        pSprite2->setPosition(Vec2(2000, 660));
        auto seq = Sequence::create(DelayTime::create(0.05f), animate, NULL);
        auto spw = Spawn::create(seq, MoveTo::create(2.0f, Vec2(-1000, 660)), NULL);
         RepeatForever* repeat = RepeatForever::create(spw);//无限循环
        pSprite2->runAction(repeat);
    }
    else
    {
        pSprite2->runAction(Sequence::create(animate,func,NULL));
    }
}

void DDZGameViewLayer::AnimEnd(Node *pSender)
{
    removeChild(pSender);
}
