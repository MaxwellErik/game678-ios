#include "DDZCardControl.h"
#include "DDZGameLogic.h"

//构造函数
DDZ_CardControl::DDZ_CardControl()
{
	m_TouchDown = false;
	m_BeginCardIndex = 0;
	m_EndCardIndex = 100;
}

void DDZ_CardControl::SetLayer(GameLayer* _layer,int tga, int chair)
{
	m_layer = _layer;
    for (int i = 0; i < LANDLOAD_CARD_COUNT; i++)
	{
		m_CardTga[chair][i] = tga + chair * LANDLOAD_CARD_COUNT + i;
	}
    m_CardCountTga[chair] = tga+70;
	m_OutCardTga = tga+75;
	m_Chair = chair;
	m_IsLand = false;
}

//析构函数
DDZ_CardControl::~DDZ_CardControl()
{
}

void DDZ_CardControl::ClearCardData()
{
	for (int i = 0; i < m_CardData.size(); i++)
	{
		m_layer->removeChild(m_CardData[i].cardspr);
	}
	m_CardData.clear();
}

int DDZ_CardControl::GetCardCount()
{
	int r = 0;
	for (int i = 0; i < m_CardData.size(); i++)
	{
		if(m_CardData[i].cbCardData != 0) r++;
	}
	return r;
}

void DDZ_CardControl::SetOtherCardData(BYTE bCardData[], DWORD dwCardCount,bool setdata)
{
	m_CardCount = dwCardCount;

    if (setdata == false)
    {
        for (int i = 0; i < LANDLOAD_CARD_COUNT; i++)
        {
            m_layer->removeAllChildByTag(m_CardTga[m_Chair][i]);
        }
        m_CardData.clear();
        return;
    }
    
    for (int i = 0; i < LANDLOAD_CARD_COUNT; i++)
    {
        m_layer->removeAllChildByTag(m_CardTga[m_Chair][i]);
    }
    m_CardData.clear();
    
    for (int i = 0; i < m_CardCount; i++)
    {
        DDZ_tagCardItem temp;
        temp.cbCardData = bCardData[i];
        m_CardData.push_back(temp);
    }
    
    m_layer->removeAllChildByTag(m_CardCountTga[m_Chair]);
    
    if (m_CardCount <= 0)
        return;
    
    int yc = (m_CardCount/2.0f) * 40 + 100;
    int xc = (0 == m_Chair) ? 250 : 1670;
    int xc2 = (0 == m_Chair) ? 370 : 1550;
    
    for (int i = 0; i < m_CardCount; i++)
    {
        string _name = GetCardStringName(m_CardData[i].cbCardData);
        Sprite* temp = Sprite::createWithSpriteFrameName(_name.c_str());
        temp->setScale(0.6f);
        
        if (m_CardCount >= 10)
        {
            if (i >= 10)
                temp->setPosition(Vec2(xc2 , 1320 - i * 40));
            else
                temp->setPosition(Vec2(xc , 920 - i * 40));
        }
        else
        {
            temp->setPosition(Vec2(xc , 640 + yc - i * 40));
        }
        temp->setTag(m_CardTga[m_Chair][i]);
        m_layer->addChild(temp);
    }
    
    Label* remain = Label::createWithSystemFont("剩余        张牌", _GAME_FONT_NAME_1_, 46);
    remain->setColor(Color3B(28,158,255));
    remain->setPosition(Vec2(xc, 445));
    remain->setTag(m_CardCountTga[m_Chair]);
    m_layer->addChild(remain);
    
    std::string str = StringUtils::toString(m_CardCount);
    if (m_CardCount < 10)
        str = "0" + str;
    LabelAtlas *count = LabelAtlas::create(str, "Common/time.png", 33, 50, '+');
    count->setAnchorPoint(Vec2(0.5f, 0.5f));
    count->setPosition(Vec2(xc, 445));
    count->setTag(m_CardCountTga[m_Chair]);
    m_layer->addChild(count);
}

void DDZ_CardControl::RemoveAllCardData()
{
    ClearCardData();
}

void DDZ_CardControl::RemoveCardData(BYTE byCardData[], int nCardCount)
{
	if (0 == m_Chair || 2 == m_Chair)
	{
		m_CardCount -= nCardCount;
		SetOtherCardData(byCardData, m_CardCount);
	}
	else
	{
		for (int i = 0; i < LANDLOAD_CARD_COUNT; i++)
		{
			m_layer->removeAllChildByTag(m_CardTga[m_Chair][i]);
		}

		for (int i = 0; i < nCardCount; i++)
		{
			for (int j = 0; j < m_CardData.size(); j++)
			{
				if (byCardData[i] == m_CardData[j].cbCardData)
				{
					m_CardData.erase(m_CardData.begin()+j);
					break;
				}
			}
		}

		int xc = (m_CardData.size()/2.0f)*55-120;
		for (int i = 0; i < m_CardData.size(); i++)
		{
			string _name = GetCardStringName(m_CardData[i].cbCardData);
			Sprite* temp = Sprite::createWithSpriteFrameName(_name.c_str());
			temp->setPosition(Vec2(_STANDARD_SCREEN_CENTER_.x-xc+i*55,130));
			temp->setScale(0.92f);
			temp->setTag(m_CardTga[m_Chair][i]);
			m_layer->addChild(temp);
			m_CardData[i].cardspr = temp;
		}

	}
}

void DDZ_CardControl::SetMyCardData(BYTE bCardData[], DWORD dwCardCount,bool setdata)
{
	m_CardCount = dwCardCount;
	if (setdata == false)
	{
		for (int i = 0; i < 20; i++)
		{
			m_layer->removeAllChildByTag(m_CardTga[m_Chair][i]);
		}
		m_CardData.clear();
		return;
	}

	for (int i = 0; i < 20; i++)
	{
		m_layer->removeAllChildByTag(m_CardTga[m_Chair][i]);
	}
	m_CardData.clear();

	for (int i = 0; i < dwCardCount; i++)
	{
		DDZ_tagCardItem temp;
		temp.cbCardData = bCardData[i];
		m_CardData.push_back(temp);
	}

	int xc = (m_CardData.size()/2.0f)*60 - 220;
	for (int i = 0; i < m_CardData.size(); i++)
	{
		string _name = GetCardStringName(m_CardData[i].cbCardData);
		Sprite* temp = Sprite::createWithSpriteFrameName(_name.c_str());
        temp->setScale(0.9f);
		temp->setPosition(Vec2(_STANDARD_SCREEN_CENTER_.x - xc + i*60, CARD_DOWN));
		temp->setTag(m_CardTga[m_Chair][i]);
		m_layer->addChild(temp);
		m_CardData[i].cardspr = temp;
	}
}

void DDZ_CardControl::ClearShowOutCard()
{
	m_CardData.clear();
	m_layer->removeAllChildByTag(m_OutCardTga);
}

void DDZ_CardControl::ShowOutCard(BYTE bCardData[], DWORD dwCardCount)
{
	m_layer->removeAllChildByTag(m_OutCardTga);

    float cc = 0.1f;
    int xc = (dwCardCount/2.0f)*55-40;
    m_CardData.clear();
    for (int i = 0; i < dwCardCount; i++)
    {
        string _name = GetCardStringName(bCardData[i]);
        Sprite* temp = Sprite::createWithSpriteFrameName(_name.c_str());
        temp->setPosition(Vec2(_STANDARD_SCREEN_CENTER_.x-xc+i*55,720));
        temp->setScale(0.0f);
        temp->setTag(m_OutCardTga);		
        m_layer->addChild(temp);

        MoveTo *move = MoveTo::create(cc, Vec2(_STANDARD_SCREEN_CENTER_.x-xc+i*55,720));
        ScaleTo *scale = ScaleTo::create(cc, 0.9f);
        FiniteTimeAction * spawn =Spawn::create(move, scale, NULL);
        temp->runAction(spawn);
        cc+=0.05f;

        DDZ_tagCardItem tempcard;
        tempcard.cbCardData = bCardData[i];
        m_CardData.push_back(tempcard);
    }
}

void DDZ_CardControl::SetCardData(BYTE bCardData[], DWORD dwCardCount, bool setdata)
{
	if (1 == m_Chair)
	{
		SetMyCardData(bCardData, dwCardCount, setdata);
	}
    else
    {
        SetOtherCardData(bCardData, dwCardCount, setdata);
    }
}


//获取扑克
BYTE DDZ_CardControl::GetShootCard(BYTE cbCardData[], BYTE& cbBufferCount)
{
	//变量定义
	BYTE cbShootCount=0;

	//拷贝扑克
	for (BYTE i=0;i<m_CardData.size();i++) 
	{
		//效验参数
		ASSERT(cbBufferCount>cbShootCount);
		if (cbBufferCount<=cbShootCount) break;

		//拷贝扑克
		if (m_CardData[i].cardspr != nullptr && m_CardData[i].cardspr->getPositionY() == CARD_UP)
            cbCardData[cbShootCount++]=m_CardData[i].cbCardData;
	}
	cbBufferCount = cbShootCount;

	return cbShootCount;
}

void DDZ_CardControl::ccTouchBegan(Vec2 localPos)
{
    if ( m_CardData.size() <= 0)
        return;
    
	for (ssize_t i = m_CardData.size()-1; i >= 0; i--)
	{
		m_CardData[i].cardspr->setColor(Color3B(255,255,255));
		Rect rc = m_CardData[i].cardspr->getBoundingBox();
		bool isTouched = rc.containsPoint(localPos);
		if (isTouched)
		{
			m_CardData[i].cardspr->setColor(Color3B(100,100,100));
			m_BeginCardIndex = (int)i;
			m_TouchDown = true;
			return;
		}
	}
}
void DDZ_CardControl::ccTouchMoved(Vec2 localPos)
{
	if (m_TouchDown) 
	{
		for (ssize_t i = m_CardData.size()-1; i >= 0; i--)
		{
			Rect rc = m_CardData[i].cardspr->getBoundingBox();
			bool isTouched = rc.containsPoint(localPos);
			if (isTouched)
			{
				m_EndCardIndex = (int)i;
				break;;
			}
		}
		for (ssize_t i = m_CardData.size()-1; i >= 0; i--)
		{
			if(i >= m_CardData.size())break;
			if(m_CardData[i].cardspr != NULL) m_CardData[i].cardspr->setColor(Color3B(255,255,255));
		}

		if (m_BeginCardIndex > m_EndCardIndex) //往左
		{
			for (int i = m_EndCardIndex; i <= m_BeginCardIndex; i++)
			{
				if(i >= m_CardData.size())break;
				if(m_CardData[i].cardspr != NULL) m_CardData[i].cardspr->setColor(Color3B(100,100,100));
			}
		}
		else //往右
		{
			for (int i = m_BeginCardIndex; i <= m_EndCardIndex; i++)
			{
				if(i >= m_CardData.size())break;
				if(m_CardData[i].cardspr != NULL) m_CardData[i].cardspr->setColor(Color3B(100,100,100));
			}
		}
	}
}
void DDZ_CardControl::ccTouchEnded(Vec2 localPos)
{
	m_TouchDown = false;

	if (m_EndCardIndex == 100)
	{
		for (int i = (int)m_CardData.size()-1; i >= 0; i--)
		{
			Rect rc = m_CardData[i].cardspr->getBoundingBox();
			bool isTouched = rc.containsPoint(localPos);
			if (isTouched)
			{
				if (m_CardData[i].cardspr->getPositionY() == CARD_DOWN)
				{
					m_CardData[i].cardspr->setColor(Color3B(255,255,255));
					m_CardData[i].cardspr->setPositionY(CARD_UP);
				}
				else
				{
					m_CardData[i].cardspr->setColor(Color3B(255,255,255));
					m_CardData[i].cardspr->setPositionY(CARD_DOWN);
				}
				break;;
			}
		}
		return;
	}

	if (m_BeginCardIndex > m_EndCardIndex) //往左
	{
		for (int i = m_EndCardIndex; i <= m_BeginCardIndex; i++)
		{
			if(i >= m_CardData.size())break;
			if(m_CardData[i].cardspr != NULL)
			{
                if (m_CardData[i].cardspr->getPositionY() == CARD_DOWN)
                {
                    m_CardData[i].cardspr->setColor(Color3B(255,255,255));
                    m_CardData[i].cardspr->setPositionY(CARD_UP);
                }
                else
                {
                    m_CardData[i].cardspr->setColor(Color3B(255,255,255));
                    m_CardData[i].cardspr->setPositionY(CARD_DOWN);
                }
            }
        }
	}
	else //往右
	{
		for (int i = m_BeginCardIndex; i <= m_EndCardIndex; i++)
		{
			if(i >= m_CardData.size())break;
			if(m_CardData[i].cardspr != NULL)
			{
                if (m_CardData[i].cardspr->getPositionY() == CARD_DOWN)
                {
                    m_CardData[i].cardspr->setColor(Color3B(255,255,255));
                    m_CardData[i].cardspr->setPositionY(CARD_UP);
                }
                else
                {
                    m_CardData[i].cardspr->setColor(Color3B(255,255,255));
                    m_CardData[i].cardspr->setPositionY(CARD_DOWN);
                }
            }
        }
	}

	m_BeginCardIndex = 0;
	m_EndCardIndex = 100;
}

bool DDZ_CardControl::RemoveShootItem()
{
	vector<DDZ_tagCardItem>	TempCardData;
	for (int i = 0; i < m_CardData.size(); i++)
	{
		 if (m_CardData[i].cardspr->getPositionY() == CARD_DOWN)
		{
			DDZ_tagCardItem temp;
			temp.cbCardData = m_CardData[i].cbCardData;
			TempCardData.push_back(temp);
		}
		m_layer->removeChild(m_CardData[i].cardspr);
	}
	m_CardData.clear();

	int xc = (TempCardData.size()/2.0f)*50-80;
	for (int i = 0; i < TempCardData.size(); i++)
	{
		DDZ_tagCardItem temp;
		temp.cbCardData = TempCardData[i].cbCardData;
		string _name = GetCardStringName(TempCardData[i].cbCardData);
		Sprite* tempspr = Sprite::createWithSpriteFrameName(_name.c_str());
		tempspr->setPosition(Vec2(_STANDARD_SCREEN_CENTER_.x-xc+i*60,CARD_DOWN));
		tempspr->setTag(m_CardTga[m_Chair][i]);
		m_layer->addChild(tempspr);
		temp.cardspr = tempspr;
		m_CardData.push_back(temp);
	}

	m_CardCount = m_CardData.size();

	return true;
}

bool DDZ_CardControl::SetShootFirstCard()
{
    for (int i = 0; i < m_CardData.size(); i++)
    {
        m_CardData[i].cardspr->setPositionY(CARD_DOWN);
    }
    m_CardData[ m_CardData.size()-1].cardspr->setPositionY(CARD_UP);
    return true;
}

//设置扑克
bool DDZ_CardControl::SetShootCard(BYTE cbCardData[], BYTE cbCardCount)
{
	//设置牌不弹起
	if (cbCardCount == 0)
	{
		for (int i = 0; i < m_CardData.size(); i++)
		{
			m_CardData[i].cardspr->setPositionY(CARD_DOWN);
		}
	}
	else //弹起扑克
	{
		for (int i = 0; i < m_CardData.size(); i++)
		{
			m_CardData[i].cardspr->setPositionY(CARD_DOWN);
		}

		for (BYTE i=0;i<cbCardCount;i++)
		{
			for (BYTE j=0;j<m_CardData.size();j++)
			{
				if ((m_CardData[j].cardspr->getPositionY() == CARD_DOWN) && (m_CardData[j].cbCardData == cbCardData[i]))
				{
					m_CardData[j].cardspr->setPositionY(CARD_UP);
					break;
				}
			}
		}
	}

	return true;
}

WORD DDZ_CardControl::GetCardData(BYTE cbCardData[], UINT& wBufferCount)
{
	if (wBufferCount == 0)	return m_CardData.size();

	WORD wCardCount = wBufferCount < m_CardData.size()? wBufferCount:m_CardData.size();

	//拷贝扑克
	for( WORD i = 0; i < wCardCount; i++ )
        cbCardData[i] = m_CardData[i].cbCardData;

	wBufferCount = (UINT)(m_CardData.size());

	return wCardCount;
}

string DDZ_CardControl::GetCardStringName(BYTE card)
{
	if (card == 0)
	{
		return "backCard.png";
	}
	char tt[32];
	sprintf(tt,"%0x",card);
	BYTE _value = m_GameLogic.GetCardValue(card);
	BYTE _color = m_GameLogic.GetCardColor(card);
	string temp;
	if (_color == 0) temp = "fangkuai_";
	if (_color == 1) temp = "meihua_";
	if (_color == 2) temp = "hongtao_";
	if (_color == 3) temp = "heitao_";
	char _valstr[32];
	ZeroMemory(_valstr,32);
	if (card == 0x41) //小王
	{
		sprintf(_valstr,"xiaowang.png");
	}
	else if (card == 0x42)
	{
		sprintf(_valstr,"dawang.png");
	}
	else if (_value < 10)
	{
		sprintf(_valstr,"0%d.png",_value);
	}
	else 
	{
		sprintf(_valstr,"%d.png",_value);
	}

	temp+=_valstr;
	return temp;
}
