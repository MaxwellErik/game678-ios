#ifndef CMD_LAND_HEAD_FILE
#define CMD_LAND_HEAD_FILE

//////////////////////////////////////////////////////////////////////////
//公共宏定义


#pragma pack(push)
#pragma pack(1)

#define DDZ_KIND_ID					100                                 //游戏 I D
#define GAME_PLAYER_DDZ				3									//游戏人数
#define GAME_NAME_DDZ				TEXT("欢乐斗地主")						//游戏名字
#define GAME_GENRE					(GAME_GENRE_SCORE|GAME_GENRE_GOLD)	//游戏类型

/*
 *改动这里以调节欢乐斗地主的倍率 linuxer 09.04.05
 */
#define LANDLOAD_CARD_COUNT        20
#define FARMER_CARD_COUNT          17
#define GAME_MULTI					15									//游戏倍率

//游戏状态
#define GS_WK_FREE					GS_FREE								//等待开始
#define GS_WK_SENDCARD				GS_FREE+1							//发牌状态
#define GS_WK_SCORE					GS_PLAYING							//叫地主状态
#define GS_WK_JIABEI				GS_PLAYING+1						//加倍状态
#define GS_WK_PLAYING				GS_PLAYING+2						//游戏进行

//动画数值
#define CAT_MISSILE					1									//王炸
#define CAT_BOMB					2									//普炸
#define CAT_LIANDUI					3									//连对
#define CAT_SHUN					4									//顺子
#define CAT_PLANE					5									//飞机
#define CAT_ALARM					6									//警报
#define CAT_PASS					7									//没有牌大过上家


typedef enum
{
	en_Free=1,
	en_Playing,
	en_End
}en_State;

//音乐播放完成消息定义
#define WM_DSHOWNOTIFY	WM_USER+23
//////////////////////////////////////////////////////////////////////////
//服务器命令结构

#define SUB_S_DDZ_SETUSERCARD			333									//获取牌
#define SUB_S_DDZ_SEND_CARD				100									//发牌命令
#define SUB_S_DDZ_LAND_SCORE			101									//叫分命令
#define SUB_S_DDZ_LAND_QIANG			109									//抢地主
#define SUB_S_DDZ_SEND_FINISH			108									//发牌完成
#define SUB_S_DDZ_GIVE_LAND				107									//不叫地主
#define SUB_S_DDZ_GAME_JAIBEI			110									//游戏加倍
#define SUB_S_DDZ_GAME_ISJIA			111									//是否加倍
#define SUB_S_DDZ_GAME_START           102									//游戏开始
#define SUB_S_DDZ_OUT_CARD				103									//用户出牌
#define SUB_S_DDZ_PASS_CARD				104									//放弃出牌
#define SUB_S_DDZ_GAME_END				105									//游戏结束
#define SUB_S_DDZ_QUIT					106									//游戏请求
#define SUB_S_DDZ_RSEND_CARD			112									//重发扑克
#define SUB_S_DDZ_SEND_MING				113									//明牌数据
#define SUB_S_DDZ_SEND_OVER				114									//发牌完毕
#define SUB_S_DDZ_PLAY_MING				115									//过程名牌

struct CMD_S_CardData
{
	WORD wChairID;
	BYTE byCardData[20];
};

//游戏状态<空闲状态>
struct CMD_S_DDZ_StatusFree
{
	BYTE							cbUserStatus;	//用户状态
	INT32							lBaseScore;							//基础积分
	INT32							lMulti;								//倍率
	//兆
	INT32							lLastWinCount;						//胜利盘数
	INT32							lLastLostCount;						//失败盘数
	INT32							lLastDrawCount;						//和局盘数
	INT32							lLastFleeCount;						//断线数目

};

//游戏状态<叫分状态>
struct CMD_S_DDZ_StatusScore
{
	WORD							bLandScore;							//地主分数
	INT32							lBaseScore;							//基础积分
	WORD				 			wCurrentUser;						//当前玩家
	BYTE							bScoreInfo[3];						//叫分信息
	INT32							bIsJiaoDiZhu;
	INT32							bIsJiao[3];

	BYTE							bCardData[3][20];					//手上扑克
	BYTE							bCardCount[3];						//扑克数目
	INT32							bMingFlag[3];						//明牌标志

	INT32							lLastWinCount;						//胜利盘数
	INT32							lLastLostCount;						//失败盘数
	INT32							lLastDrawCount;						//和局盘数
	INT32							lLastFleeCount;						//断线数目
	bool							bisScore;							//是否积分房
};

//游戏状态<加倍状态>
struct CMD_S_DDZ_StatusJiaBei
{
	WORD							wLandUser;							//地主玩家
	WORD							wMulti;								//游戏倍率
	INT32							lBaseScore;							//基础积分
	WORD							bLandScore;							//地主分数
	WORD							wLimitJia;							//判断加倍分数
	INT32							bJiaBei[3];
	BYTE							bBackCard[3];						//底牌扑克
	BYTE							bCardData[3][20];					//手上扑克
	BYTE							bCardCount[3];						//扑克数目
	INT32							bMingFlag[3];						//明牌标志

	INT32							lLastWinCount;						//胜利盘数
	INT32							lLastLostCount;						//失败盘数
	INT32							lLastDrawCount;						//和局盘数
	INT32							lLastFleeCount;						//断线数目
	bool							bisScore;							//是否积分房
};

//游戏状态<游戏状态>
struct CMD_S_DDZ_StatusPlay
{
	WORD							wLandUser;							//地主玩家
	WORD							wMulti;								//游戏倍率
	WORD							wBombTime;							//炸弹倍数
	INT32							lBaseScore;							//基础积分
	WORD							bLandScore;							//地主分数
	WORD							wLastOutUser;						//出牌的人
	WORD				 			wCurrentUser;						//当前玩家
	BYTE							bBackCard[3];						//底牌扑克
	BYTE							bCardData[3][20];					//手上扑克
	BYTE							bCardCount[3];						//扑克数目
	INT32							bMingFlag[3];						//明牌标志
	BYTE							bTurnCardCount;						//基础出牌
	BYTE							bTurnCardData[20];					//出牌列表
	//兆
	INT32							lLastWinCount;						//胜利盘数
	INT32							lLastLostCount;						//失败盘数
	INT32							lLastDrawCount;						//和局盘数
	INT32							lLastFleeCount;						//断线数目
	bool							bisScore;							//是否积分房
};

//游戏状态<发牌状态>
struct CMD_S_DDZ_StatusSendCard
{

};


//发送扑克
struct CMD_S_DDZ_SendCard
{
	WORD				 			wCurrentUser;						//当前玩家
	WORD							wSendFirst;							//首发用户
	BYTE							bCardData[17];						//扑克列表
	BYTE							bFirstCard;							//随机扑克
	INT32							bMingFlag[3];						//明牌用户
};

//发送名牌数据
struct CMD_S_DDZ_SendMing
{
	WORD							wChairID;							//名牌玩家
	BYTE							bCardData[20];						//扑克数据<修正为20>
	WORD							wMulti;								//明牌倍率
};

//用户叫分
struct CMD_S_DDZ_LandScore
{
	WORD							bLandUser;							//叫分玩家
	WORD				 			wCurrentUser;						//当前玩家
	INT32							m_bFlagDo;							//是否叫地主
	WORD							wMutli;								//倍率
};

//用户加倍
struct CMD_S_DDZ_UserJia
{
	WORD							wLandUser;							//加倍玩家
	INT32							bIsJia;								//是否加倍
	WORD							wMutli;								//倍率
};

//开始加倍
struct CMD_S_DDZ_GameStart
{
	WORD				 			wLandUser;							//地主玩家
	WORD				 			wCurrentUser;						//当前玩家
	BYTE							bBackCard[3];						//底牌扑克
	WORD							wLimitJia;							//加倍条件
	WORD							wMulti;								//倍率
	INT32							bIsQiang;							//抢或者叫
	bool							bisScore;							//是否积分房间
};

//游戏开始
struct CMD_S_DDZ_Start
{
	WORD							wCurrentUser;						//目前玩家
	//WORD							wMutli;								//倍率
	INT32							bIsJia;								//是否加倍
};

//用户出牌
struct CMD_S_DDZ_OutCard
{
	BYTE							bCardCount;							//出牌数目
	WORD				 			wCurrentUser;						//当前玩家
	WORD							wOutCardUser;						//出牌玩家
	BYTE							bCardData[20];						//扑克列表
};

//放弃出牌
struct CMD_S_DDZ_PassCard
{
	BYTE							bNewTurn;							//一轮开始
	WORD				 			wPassUser;							//放弃玩家
	WORD				 			wCurrentUser;						//当前玩家
};

//游戏结束
struct CMD_S_DDZ_GameEnd
{
	BYTE							cbReason;							//结束类型
	INT32							lGameTax;							//游戏税收
	INT32							lGameScore[3];						//游戏积分
	BYTE							bCardCount[3];						//扑克数目
	BYTE							bCardData[54];						//扑克列表 
	WORD							wMulti;								//结束倍率
	BYTE							bSpring;							//春天判断
	WORD							wMingMulti;							//明牌倍率
	BYTE							bJiaMulti[3];						//加倍倍率
};

//////////////////////////////////////////////////////////////////////////
//客户端命令结构

#define SUB_C_GETUSERCARD			33									//获取牌
#define SUB_C_LAND_SCORE			1									//用户叫庄
#define SUB_C_OUT_CART				2									//用户出牌
#define SUB_C_PASS_CARD				3									//放弃出牌
#define SUB_C_QUIT                 4									//请求退出
#define SUB_C_MINGPAI				5									//明牌开始
#define SUB_C_SENDFINISH			6									//发牌完成
#define SUB_C_LAND_GIVEUP			7									//用户不叫
#define SUB_C_LAND_ISQIANG			8									//抢地主
#define SUB_C_LAND_ISJIA			9									//是否加倍
#define SUB_C_MING					10									//游戏明牌			

//开始类型
struct CMD_C_Start
{
	WORD							wCurrentUser;						//当前用户
	INT32							bFlag;								//开始类型 false:普通开始 true:明牌开始
};
//用户叫分
struct CMD_C_LandScore
{
	BYTE							bLandScore;							//地主分数 0表示不叫或不抢或不加倍,1表示叫地主或抢地主或加倍
};

//出牌数据包
struct CMD_C_OutCard
{
	BYTE							bCardCount;							//出牌数目
	BYTE							bCardData[20];						//扑克列表
};
struct CMD_QUIT_GAME
{
	BYTE  length;
	const char* cc;
};
struct CMD_C_Ming
{
	WORD							wMulti;								//明牌倍数
};
//////////////////////////////////////////////////////////////////////////

#pragma pack()


#endif
