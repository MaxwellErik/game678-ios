#ifndef _DDZCARDCONTROL_H_
#define _DDZCARDCONTROL_H_
#include <vector>

using namespace std;
#include "GameScene.h"
#include "DDZGameLogic.h"
#include "CMD_DDZ.h"

#define CARD_UP     170//牌弹起纵坐标
#define CARD_DOWN   130//牌没弹起纵坐标

struct DDZ_tagCardItem
{
public:
	DDZ_tagCardItem()
	{
		cardspr = NULL;
		cbCardData = 0;
	}
	Sprite*						cardspr;
	BYTE						cbCardData;
};

class DDZ_CardControl
{
public:
	DDZ_CardControl();
	~DDZ_CardControl();
	void SetLayer(GameLayer* _layer, int tga, int chair);

public:
	GameLayer*		m_layer;
	CGameLogicDDZ	m_GameLogic;
	vector<DDZ_tagCardItem>	m_CardData;
	int				m_CardTga[GAME_PLAYER_DDZ][LANDLOAD_CARD_COUNT];
    int				m_CardCountTga[GAME_PLAYER_DDZ];
	DWORD			m_CardCount;
	int				m_Chair;
	int				m_OutCardTga;
	bool			m_IsLand;
	bool			m_TouchDown;
	int				m_BeginCardIndex;
	int				m_EndCardIndex;

public:
	void SetLand(bool island){m_IsLand = island;}
	void SetCardData(BYTE bCardData[], DWORD dwCardCount,bool setdata = true);
	void SetOtherCardData(BYTE bCardData[], DWORD dwCardCount, bool setdata = true);
	void SetMyCardData(BYTE bCardData[], DWORD dwCardCount,bool setdata = true);
    
	void ShowOutCard(BYTE bCardData[], DWORD dwCardCount);

	BYTE GetShootCard(BYTE cbCardData[], BYTE& cbBufferCount);

	void ClearShowOutCard();
	WORD GetCardData(BYTE cbCardData[], UINT& wBufferCount);
	void ClearCardData();
	int GetCardCount();
	string GetCardStringName(BYTE card);

	void ccTouchBegan(Vec2 localPos);
	void ccTouchMoved(Vec2 localPos);
	void ccTouchEnded(Vec2 localPos);

	bool SetShootCard(BYTE cbCardData[], BYTE cbCardCount);
    bool SetShootFirstCard();
	bool RemoveShootItem();

	void RemoveCardData(BYTE byCardData[], int nCardCount);
	void RemoveAllCardData();
};
#endif
