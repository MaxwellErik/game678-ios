#include "DDZGameLogic.h"

//////////////////////////////////////////////////////////////////////////
//静态变量

//扑克数据
const BYTE	CGameLogicDDZ::m_bCardListData[54]=
{
	0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08,0x09,0x0A,0x0B,0x0C,0x0D,	//方块 A - K
	0x11,0x12,0x13,0x14,0x15,0x16,0x17,0x18,0x19,0x1A,0x1B,0x1C,0x1D,	//梅花 A - K
	0x21,0x22,0x23,0x24,0x25,0x26,0x27,0x28,0x29,0x2A,0x2B,0x2C,0x2D,	//红桃 A - K
	0x31,0x32,0x33,0x34,0x35,0x36,0x37,0x38,0x39,0x3A,0x3B,0x3C,0x3D,	//黑桃 A - K
	0x41,0x42
};

//////////////////////////////////////////////////////////////////////////

//构造函数
CGameLogicDDZ::CGameLogicDDZ()
{

}

//析构函数
CGameLogicDDZ::~CGameLogicDDZ()
{
}

//获取类型
BYTE CGameLogicDDZ::GetCardType(const BYTE bCardData[], BYTE bCardCount)
{
	//开始分析
	switch (bCardCount)
	{
	case 1: //单牌
		{
			return CT_SINGLE;
		}
	case 2:	//对牌和火箭
		{
			if ((bCardData[0]==0x42)&&(bCardData[1]==0x41)) return CT_MISSILE_CARD;
			return (GetCardLogicValue(bCardData[0])==GetCardLogicValue(bCardData[1]))?CT_DOUBLE:CT_INVALID;
		}
	case 3:
	case 4:	//连牌和炸弹
		{
			BYTE bLogicValue=GetCardLogicValue(bCardData[0]);
			BYTE i = 1;
			for (i=1;i<bCardCount;i++)
			{
				if (bLogicValue!=GetCardLogicValue(bCardData[i])) break;
			}
			if (i==bCardCount) return (bCardCount==3)?CT_THREE:CT_BOMB_CARD;
			if (bCardCount==3) return CT_INVALID;
			break;
		}
	}

	//其他牌型
	if (bCardCount>=4)
	{
		//分析扑克
		tagAnalyseResult AnalyseResult;
		AnalysebCardData(bCardData, bCardCount, AnalyseResult);

		//四牌判断
		if (AnalyseResult.bFourCount>0)
		{
			if ((AnalyseResult.bFourCount==1)&&(bCardCount==5)) return CT_FOUR_LINE_TAKE_ONE;
			if ((AnalyseResult.bFourCount==1)&&(AnalyseResult.bDoubleCount==1)&&(bCardCount==6)) return CT_FOUR_LINE_TAKE_ONE;
			if ((AnalyseResult.bFourCount==1)&&(AnalyseResult.bDoubleCount==2)&&(bCardCount==8)) return CT_FOUR_LINE_TAKE_DOUBLE;
			if ((AnalyseResult.bFourCount==1)&&(bCardCount==6)) return CT_FOUR_LINE_TAKE_DOUBLE;

			return CT_INVALID;
		}

		//三牌判断
		if (AnalyseResult.bThreeCount>0)
		{
			//连牌判断
			if (AnalyseResult.bThreeCount>1)
			{
				if (AnalyseResult.bThreeLogicVolue[0]==15) return CT_INVALID;
				for (BYTE i=1;i<AnalyseResult.bThreeCount;i++)
				{
					if (AnalyseResult.bThreeLogicVolue[i]!=(AnalyseResult.bThreeLogicVolue[0]-i)) return CT_INVALID;
				}
			}

			//牌形判断
			if (AnalyseResult.bThreeCount*3 == bCardCount)
                return CT_THREE_LINE;
			if (AnalyseResult.bSignedCount == 1 && 1 == AnalyseResult.bThreeCount && 4 == bCardCount)
                return CT_THREE_LINE_TAKE_ONE;
			if (AnalyseResult.bDoubleCount == 1 && 1 == AnalyseResult.bThreeCount && 5 == bCardCount)
                return CT_THREE_LINE_TAKE_DOUBLE;
			if (AnalyseResult.bThreeCount > 1)
            {
                if (AnalyseResult.bDoubleCount == AnalyseResult.bThreeCount && AnalyseResult.bSignedCount == 0 &&
                    AnalyseResult.bDoubleCount * 2 + AnalyseResult.bThreeCount * 3 == bCardCount)
                        return CT_PLANE;
                if (AnalyseResult.bSignedCount == AnalyseResult.bThreeCount && AnalyseResult.bDoubleCount == 0 &&
                    AnalyseResult.bSignedCount + AnalyseResult.bThreeCount * 3 == bCardCount)
                    return CT_PLANE;
            }
			return CT_INVALID;
		}

		//两张类型
		if (AnalyseResult.bDoubleCount>=3)
		{
			//二连判断
			if (AnalyseResult.bDoubleLogicVolue[0]!=15)
			{
				for (BYTE i=1;i<AnalyseResult.bDoubleCount;i++)
				{
					if (AnalyseResult.bDoubleLogicVolue[i]!=(AnalyseResult.bDoubleLogicVolue[0]-i)) return CT_INVALID;
				}
				if (AnalyseResult.bDoubleCount*2==bCardCount) return CT_DOUBLE_LINE;
			}

			return CT_INVALID;
		}
		
		//单张判断
		if ((AnalyseResult.bSignedCount>=5)&&(AnalyseResult.bSignedCount==bCardCount))
		{
			BYTE bLogicValue=GetCardLogicValue(bCardData[0]);
			if (bLogicValue>=15) return CT_INVALID;
			for (BYTE i=1;i<AnalyseResult.bSignedCount;i++)
			{
				if (GetCardLogicValue(bCardData[i])!=(bLogicValue-i)) return CT_INVALID;
			}

			return CT_ONE_LINE;
		}
		
		return CT_INVALID;
	}

	return CT_INVALID;
}

//排列扑克
void CGameLogicDDZ::SortCardList(BYTE bCardData[], BYTE bCardCount)
{
	//转换数值
	BYTE bLogicVolue[20];
    ZeroMemory(bLogicVolue, sizeof(bLogicVolue));
    
	for (BYTE i = 0; i < bCardCount; i++)
    {
        bLogicVolue[i] = GetCardLogicValue(bCardData[i]);
        log("--------%d", bLogicVolue[i]);
    }
	//排序操作
    BYTE i, j, temp, tempData;
    for (j = 0; j < bCardCount - 1; j++)
    {
        for (i = 0; i < bCardCount - 1 - j; i++)
        {
            if(bLogicVolue[i] < bLogicVolue[i + 1])
            {
                temp = bLogicVolue[i];
                bLogicVolue[i] = bLogicVolue[i + 1];
                bLogicVolue[i + 1] = temp;
                
                tempData = bCardData[i];
                bCardData[i] = bCardData[i + 1];
                bCardData[i + 1] = tempData;
            }
        }
    }
    
//	BYTE bTempData,bLast=bCardCount-1;
//    for (BYTE i = 0; i < bLast; i++)
//		{
//			if ((bLogicVolue[i] < bLogicVolue[i+1])||
//			   ((bLogicVolue[i]==bLogicVolue[i+1]) && (bCardData[i] < bCardData[i+1])))
//			{
//				//交换位置
//				bTempData=bCardData[i];
//				bCardData[i]=bCardData[i+1];
//				bCardData[i+1]=bTempData;
//				bTempData=bLogicVolue[i];
//				bLogicVolue[i]=bLogicVolue[i+1];
//				bLogicVolue[i+1]=bTempData;
//			}	
//		}
//		bLast--;

	return;
}

//混乱扑克
void CGameLogicDDZ::RandCardList(BYTE bCardBuffer[], BYTE bBufferCount)
{
	//混乱准备
	BYTE bCardData[sizeof(m_bCardListData)];
	CopyMemory(bCardData,m_bCardListData,sizeof(m_bCardListData));

	//混乱扑克
	BYTE bRandCount=0,bPosition=0;
	do
	{
		bPosition=rand()%(bBufferCount-bRandCount);
		bCardBuffer[bRandCount++]=bCardData[bPosition];
		bCardData[bPosition]=bCardData[bBufferCount-bRandCount];
	} while (bRandCount<bBufferCount);

	return;
}

//删除扑克
bool CGameLogicDDZ::RemoveCard(const BYTE bRemoveCard[], BYTE bRemoveCount, BYTE bCardData[], BYTE bCardCount)
{
	//检验数据
	ASSERT(bRemoveCount<=bCardCount);

	//定义变量
	BYTE bDeleteCount=0,bTempCardData[20];
	if (bCardCount>CountArray(bTempCardData)) return false;
	CopyMemory(bTempCardData,bCardData,bCardCount*sizeof(bCardData[0]));

	//置零扑克
	for (BYTE i=0;i<bRemoveCount;i++)
	{
		for (BYTE j=0;j<bCardCount;j++)
		{
			if (bRemoveCard[i]==bTempCardData[j])
			{
				bDeleteCount++;
				bTempCardData[j]=0;
				break;
			}
		}
	}
	if (bDeleteCount!=bRemoveCount) return false;

	//清理扑克
	BYTE bCardPos=0;
	for (BYTE i=0;i<bCardCount;i++)
	{
		if (bTempCardData[i]!=0) bCardData[bCardPos++]=bTempCardData[i];
	}

	return true;
}

//逻辑数值
BYTE CGameLogicDDZ::GetCardLogicValue(BYTE bCardData)
{
	//扑克属性
	BYTE bCardColor=GetCardColor(bCardData);
	BYTE bCardValue=GetCardValue(bCardData);

	//转换数值
	if (bCardColor==0x04) return bCardValue+15;
	return (bCardValue <= 2) ? (bCardValue+13) : bCardValue;
}

BYTE CGameLogicDDZ::GetCardValueForSound(BYTE bCardData)
{
    //扑克属性
    BYTE bCardColor=GetCardColor(bCardData);
    BYTE bCardValue=GetCardValue(bCardData);
    
    //转换数值
    if (bCardColor==0x04)
        return bCardValue+13;
    return bCardValue;
}

//对比扑克
bool CGameLogicDDZ::CompareCard(const BYTE bFirstList[], const BYTE bNextList[], BYTE bFirstCount, BYTE bNextCount)
{
	//获取类型
	BYTE bNextType=GetCardType(bNextList,bNextCount);
	BYTE bFirstType=GetCardType(bFirstList,bFirstCount);

	//类型判断
	if (bFirstType==CT_INVALID) return false;
	if (bFirstType==CT_MISSILE_CARD) return true;
	if (bNextType==CT_MISSILE_CARD) return false;

	//炸弹判断
	if ((bFirstType==CT_BOMB_CARD)&&(bNextType!=CT_BOMB_CARD)) return true;
	if ((bFirstType!=CT_BOMB_CARD)&&(bNextType==CT_BOMB_CARD)) return false;

	//规则判断
	if ((bFirstType!=bNextType)||(bFirstCount!=bNextCount)) return false;

	//开始对比
	switch (bNextType)
	{
	case CT_SINGLE:
	case CT_DOUBLE:
	case CT_THREE:
	case CT_ONE_LINE:
	case CT_DOUBLE_LINE:
	case CT_THREE_LINE:
	case CT_BOMB_CARD:
		{
			BYTE bFirstLogicValue=GetCardLogicValue(bFirstList[0]);
			BYTE bNextLogicValue=GetCardLogicValue(bNextList[0]);
			return bFirstLogicValue>bNextLogicValue;
		}
	case CT_THREE_LINE_TAKE_ONE:
	case CT_THREE_LINE_TAKE_DOUBLE:
		{
			tagAnalyseResult NextResult;
			tagAnalyseResult FirstResult;
			AnalysebCardData(bNextList,bNextCount,NextResult);
			AnalysebCardData(bFirstList,bFirstCount,FirstResult);
			return FirstResult.bThreeLogicVolue[0]>NextResult.bThreeLogicVolue[0];
		}
	case CT_FOUR_LINE_TAKE_ONE:
	case CT_FOUR_LINE_TAKE_DOUBLE:
		{
			tagAnalyseResult NextResult;
			tagAnalyseResult FirstResult;
			AnalysebCardData(bNextList,bNextCount,NextResult);
			AnalysebCardData(bFirstList,bFirstCount,FirstResult);
			return FirstResult.bFourLogicVolue[0]>NextResult.bFourLogicVolue[0];
		}
	}
	
	return false;
}

//分析扑克
void CGameLogicDDZ::AnalysebCardData(const BYTE bCardData[], BYTE bCardCount, tagAnalyseResult & AnalyseResult)
{
	//变量定义
	BYTE bSameCount=1,bCardValueTemp=0;
	BYTE bLogicValue=GetCardLogicValue(bCardData[0]);

	//设置结果
	memset(&AnalyseResult,0,sizeof(AnalyseResult));

	//扑克分析
	for (BYTE i=1;i<bCardCount;i++)
	{
		//获取扑克
		bCardValueTemp=GetCardLogicValue(bCardData[i]);
		if (bCardValueTemp==bLogicValue) bSameCount++;

		//保存结果
		if ((bCardValueTemp!=bLogicValue)||(i==(bCardCount-1)))
		{
			switch (bSameCount)
			{
			case 2:		//两张
				{
					CopyMemory(&AnalyseResult.m_bDCardData[AnalyseResult.bDoubleCount*2],&bCardData[i-2+((i==bCardCount-1&&bCardValueTemp==bLogicValue)?1:0)],2);
					AnalyseResult.bDoubleLogicVolue[AnalyseResult.bDoubleCount++]=bLogicValue;
					break;
				}
			case 3:		//三张
				{
					CopyMemory(&AnalyseResult.m_bTCardData[AnalyseResult.bThreeCount*3],&bCardData[i-3+((i==bCardCount-1&&bCardValueTemp==bLogicValue)?1:0)],3);
					AnalyseResult.bThreeLogicVolue[AnalyseResult.bThreeCount++]=bLogicValue;
					break;
				}
			case 4:		//四张
				{
					CopyMemory(&AnalyseResult.m_bFCardData[AnalyseResult.bFourCount*4],&bCardData[i-4+((i==bCardCount-1&&bCardValueTemp==bLogicValue)?1:0)],4);
					AnalyseResult.bFourLogicVolue[AnalyseResult.bFourCount++]=bLogicValue;
					break;
				}
			}
		}

		//设置变量
		if (bCardValueTemp!=bLogicValue)
		{
			if(bSameCount==1)
			{
				if(i!=bCardCount-1)
					AnalyseResult.m_bSCardData[AnalyseResult.bSignedCount++]=bCardData[i-1];
				else
				{
					AnalyseResult.m_bSCardData[AnalyseResult.bSignedCount++]=bCardData[i-1];
					AnalyseResult.m_bSCardData[AnalyseResult.bSignedCount++]=bCardData[i];
				}
			}
			else
			{
				if(i==bCardCount-1)
					AnalyseResult.m_bSCardData[AnalyseResult.bSignedCount++]=bCardData[i];
			}
			bSameCount=1;
			bLogicValue=bCardValueTemp;

		}
	}

	//单牌数目
	BYTE bOtherCount=AnalyseResult.bDoubleCount*2+AnalyseResult.bThreeCount*3+AnalyseResult.bFourCount*4;
	return;
}

void CGameLogicDDZ::OrderSortCardList(BYTE bCardData[],BYTE bCardCount)
{
	//得到具体的数值
	//保存中间变量
	BYTE bLogicVolue[20];
	BYTE bTempCardData[20];
	BYTE i = 0;
	for (i=0;i<bCardCount;i++)
		bLogicVolue[i]=GetCardLogicValue(bCardData[i]);
	for (i=0;i<bCardCount;i++)
		bTempCardData[i]=bCardData[i]; 

	
	bool bIsFourLink[20]={false};
	bool bIsThreeLink[20]={false};
	bool bIsTwoLink[20]={false};

	//四张提取并保存
	BYTE cbFourCount=0;
	for (i=0;i<bCardCount-3;i++)
	{
		if ( (bLogicVolue[i]==bLogicVolue[i+1]) && 
			(bLogicVolue[i]==bLogicVolue[i+2]) && 
			(bLogicVolue[i]==bLogicVolue[i+3]) )
		{
			bCardData[cbFourCount*4+0]=bTempCardData[i+0];
			bCardData[cbFourCount*4+1]=bTempCardData[i+1];
			bCardData[cbFourCount*4+2]=bTempCardData[i+2];
			bCardData[cbFourCount*4+3]=bTempCardData[i+3];
			cbFourCount++;
			bIsFourLink[i]=true;
			bIsFourLink[i+1]=true;
			bIsFourLink[i+2]=true;
			bIsFourLink[i+3]=true;
		}
	}
   
	//三张提取并保存
	BYTE cbThreeCount=0;
	BYTE Temp=cbFourCount*4;
	for (i=0;i<bCardCount-2;i++)
	{
		if ( (bLogicVolue[i]==bLogicVolue[i+1]) &&
			(bLogicVolue[i]==bLogicVolue[i+2]) &&
			!bIsFourLink[i])
		{
			bCardData[Temp+cbThreeCount*3+0]=bTempCardData[i+0];
			bCardData[Temp+cbThreeCount*3+1]=bTempCardData[i+1];
			bCardData[Temp+cbThreeCount*3+2]=bTempCardData[i+2];
			cbThreeCount++;
			bIsThreeLink[i]=true;
			bIsThreeLink[i+1]=true;
			bIsThreeLink[i+2]=true;


		}
	}

	//两张提取并保存
	BYTE cbTwoCount=0;
	Temp=cbFourCount*4+cbThreeCount*3;
	for (i=0;i<bCardCount-1;i++)
	{
		if ( (bLogicVolue[i]==bLogicVolue[i+1]) &&
			!bIsFourLink[i] && !bIsThreeLink[i] )
		{
			bCardData[Temp+cbTwoCount*2+0]=bTempCardData[i+0];
			bCardData[Temp+cbTwoCount*2+1]=bTempCardData[i+1];
			cbTwoCount++;
			bIsTwoLink[i]=true;
			bIsTwoLink[i+1]=true;
		}
	}

	//一张提取并保存
	BYTE cbOneCount=0;
	Temp=cbFourCount*4+cbThreeCount*3+cbTwoCount*2;
	for (i=0;i<bCardCount-1;i++)
	{
		if ( (i==15) && !bIsFourLink[i+1] && !bIsThreeLink[i+1] &&!bIsTwoLink[i+1])
		{
			bCardData[16]=bTempCardData[16];
		}
		if ( (bLogicVolue[i+0]!=bLogicVolue[i+1]) &&  
			!bIsFourLink[i] && 
			!bIsThreeLink[i] &&
			!bIsTwoLink[i] )
		{
			bCardData[Temp+cbOneCount*1+0]=bTempCardData[i];
			cbOneCount++;
		}
	}

	//如果有大小王，移动到数组头
	BYTE bMissileLocalIndex=0; //保存小王位置准备
	if (bLogicVolue[0]==17 && bLogicVolue[1]==16)
	{
		for (i=bCardCount-1;i>0;i--)
		{
			if (bLogicVolue[i]==16)
				bMissileLocalIndex=i;
			break;
		}
		for (i=bMissileLocalIndex;i>2;i-=2)
		{
			bCardData[i+0]=bCardData[i-2];
			bCardData[i-1]=bCardData[i-3];
		}
		if (i==2)
		{
			bCardData[i]=bCardData[0];
			bCardData[i-1]=bTempCardData[1];
			bCardData[i-2]=bTempCardData[0];
		}
		else if(i==1)
		{
			bCardData[1]=bTempCardData[1];
			bCardData[0]=bTempCardData[0];
		}
	}
	return;
}

//提示得到要出的牌  //hint
bool CGameLogicDDZ::HintOutCard(const BYTE bHandCardData[],BYTE bHandCardCount,const BYTE bFirstOutCardData[],BYTE bFirstOutCardCount,BYTE bResultCardData[], BYTE &bResultCardCount)
{

	//变量定义
	BYTE                            i=0;
	BYTE                            m_TempCard=0;                       //辅助比较变量
	BYTE							m_bTempSCardCount=0;				//扑克数目
	BYTE							m_bTempSCardData[20];				//手上扑克
	BYTE							m_bTempDCardCount=0;				//扑克数目
	BYTE							m_bTempDCardData[20];				//手上扑克
	BYTE							m_bTempTCardCount=0;				//扑克数目
	BYTE							m_bTempTCardData[20];				//手上扑克
	BYTE							m_bTempFCardCount=0;				//扑克数目
	BYTE							m_bTempFCardData[20];				//手上扑克

	//如果上家出的是大小王,则直接返回
	if ( (GetCardLogicValue(bFirstOutCardData[0])==17) && (GetCardLogicValue(bFirstOutCardData[1])==16) )
	{
		return false;
	}
	//如果上家出的牌不为0
	if(bFirstOutCardCount!=0)
	{	
		//获取单牌列表
		for(i=0;i<bHandCardCount;i++)
		{	
			BYTE m_GetCard=GetCardLogicValue(bHandCardData[i]);
			if(m_TempCard!=m_GetCard)
			{
				m_bTempSCardData[m_bTempSCardCount++]=bHandCardData[i];
				m_TempCard=m_GetCard;
			}
		}

		//获取对牌列表
		m_TempCard=0;
		for(i=0;i<bHandCardCount-1;i++)
		{	
			BYTE m_GetCard1=GetCardLogicValue(bHandCardData[i]);
			BYTE m_GetCard2=GetCardLogicValue(bHandCardData[i+1]);
			if(m_TempCard!=m_GetCard1&&m_GetCard1==m_GetCard2&&m_GetCard1<16)
			{
				m_bTempDCardData[m_bTempDCardCount++]=bHandCardData[i];
				m_bTempDCardData[m_bTempDCardCount++]=bHandCardData[i+1];
				m_TempCard=m_GetCard1;
			}
		}

		//获取三张牌列表
		m_TempCard=0;
		for(i=0;i<bHandCardCount-2;i++)
		{	
			BYTE m_GetCard1=GetCardLogicValue(bHandCardData[i]);
			BYTE m_GetCard2=GetCardLogicValue(bHandCardData[i+1]);
			BYTE m_GetCard3=GetCardLogicValue(bHandCardData[i+2]);
			if(m_TempCard!=m_GetCard1&&m_GetCard1==m_GetCard2&&m_GetCard1==m_GetCard3)
			{
				m_bTempTCardData[m_bTempTCardCount++]=bHandCardData[i+0];
				m_bTempTCardData[m_bTempTCardCount++]=bHandCardData[i+1];
				m_bTempTCardData[m_bTempTCardCount++]=bHandCardData[i+2];
				m_TempCard=m_GetCard1;
			}
		}

		//获取四张牌列表
		m_TempCard=0;
		for(i=0;i<bHandCardCount-3;i++)
		{	
			BYTE m_GetCard1=GetCardLogicValue(bHandCardData[i]);
			BYTE m_GetCard2= GetCardLogicValue(bHandCardData[i+1]);
			BYTE m_GetCard3= GetCardLogicValue(bHandCardData[i+2]);
			BYTE m_GetCard4= GetCardLogicValue(bHandCardData[i+3]);
			if(m_TempCard!=m_GetCard1&&m_GetCard1==m_GetCard2&&m_GetCard1==m_GetCard3&&m_GetCard1==m_GetCard4)
			{
				m_bTempFCardData[m_bTempFCardCount++]=bHandCardData[i];
				m_bTempFCardData[m_bTempFCardCount++]=bHandCardData[i+1];
				m_bTempFCardData[m_bTempFCardCount++]=bHandCardData[i+2];
				m_bTempFCardData[m_bTempFCardCount++]=bHandCardData[i+3];
				m_TempCard=m_GetCard1;
			}
		}


		//根据所出牌类型判断
		i=0;
		BYTE bTurnOutType= GetCardType(bFirstOutCardData,bFirstOutCardCount);
		switch(bTurnOutType)
		{
			//单张或单连
		case CT_SINGLE:
		case CT_ONE_LINE:
			for(i = m_bTempSCardCount; i > 0; i--)
			{
				if(i-bFirstOutCardCount>=0&& CompareCard(&m_bTempSCardData[i-bFirstOutCardCount],bFirstOutCardData,bFirstOutCardCount,bFirstOutCardCount))
				{
					//判断是不是最合理的--即得到的比上一家大的是不是在对牌列表之中
					bool m_bIsHaveCard=false;
					for(int j=0;j<m_bTempDCardCount;j++)
					{
						for(int n=0;n<bFirstOutCardCount;n++)
						{
							if( GetCardLogicValue(m_bTempSCardData[i-bFirstOutCardCount+n])== GetCardLogicValue(m_bTempDCardData[j]))
								m_bIsHaveCard=true;
						}
					}

					//把最合理的情况保存起来
					//1.如果得到的单张牌在对牌列表中，但由于没有得到合适的真正的单张牌，则将对牌列表中合适的选入一张，并继续往下检查，看是否有真正的单张牌！
					//2.如果得到的单张牌不在对牌列表中，则就将它选入，并停止往下选入
					if(bResultCardCount==0||!m_bIsHaveCard) 
					{
						CopyMemory(bResultCardData,&m_bTempSCardData[i-bFirstOutCardCount],bFirstOutCardCount);
						bResultCardCount=bFirstOutCardCount;
					}
					if(!m_bIsHaveCard)
						break;
				}
			}

			//如果得到了要选的牌，则返回真
			if (bResultCardCount!=0) 
			{
				return true;
			}
			break;
		case CT_DOUBLE:
		case CT_DOUBLE_LINE:
			for(i=m_bTempDCardCount;i>0;i--)
			{
				if(i-bFirstOutCardCount>=0&& CompareCard(&m_bTempDCardData[i-bFirstOutCardCount],bFirstOutCardData,bFirstOutCardCount,bFirstOutCardCount))
				{
					//判断是不是最合理的
					bool m_bIsHaveCard=false;
					for(int j=0;j<m_bTempTCardCount;j++)
					{
						for(int n=0;n<bFirstOutCardCount;n++)
						{
							if( GetCardLogicValue(m_bTempDCardData[i-bFirstOutCardCount+n])== GetCardLogicValue(m_bTempTCardData[j]))
								m_bIsHaveCard=true;
						}
					}

					if(bResultCardCount==0||!m_bIsHaveCard)
					{
						CopyMemory(bResultCardData,&m_bTempDCardData[i-bFirstOutCardCount],bFirstOutCardCount);
						bResultCardCount=bFirstOutCardCount;
						return true;
					}
					if(!m_bIsHaveCard)
						break;
				}
			}

			//如果得到了要选的牌，则返回真
			if (bResultCardCount!=0)
			{
				return true;
			}
			break;
		case CT_THREE:
		case CT_THREE_LINE:
			for(i=m_bTempTCardCount;i>0;i--)
			{
				if(i-bFirstOutCardCount>=0&& CompareCard(&m_bTempTCardData[i-bFirstOutCardCount],bFirstOutCardData,bFirstOutCardCount,bFirstOutCardCount))
				{
					//判断是不是最合理的
					bool m_bIsHaveCard=false;
					for(int j=0;j<m_bTempFCardCount;j++)
					{
						for(int n=0;n<bFirstOutCardCount;n++)
						{
							if( GetCardLogicValue(m_bTempTCardData[i-bFirstOutCardCount+n])== GetCardLogicValue(m_bTempFCardData[j]))
								m_bIsHaveCard=true;
						}
					}
					if(bResultCardCount==0||!m_bIsHaveCard)
					{
						CopyMemory(bResultCardData,&m_bTempTCardData[i-bFirstOutCardCount],bFirstOutCardCount);
						bResultCardCount=bFirstOutCardCount;
						return true;
					}
					if(!m_bIsHaveCard&&bResultCardCount!=0)
						break;
				}
			}

			//如果得到了要选的牌，则返回真
			if (bResultCardCount!=0)
			{
				return true;
			}
			break;
		case CT_THREE_LINE_TAKE_ONE:
		case CT_THREE_LINE_TAKE_DOUBLE: 
			{
				//分析扑克
				tagAnalyseResult AnalyseResult;
				 AnalysebCardData(bFirstOutCardData,bFirstOutCardCount,AnalyseResult);
				 int n = 0;

				//得到三个分析
				for(i=m_bTempTCardCount;i>0;i--)
				{
					if(i-AnalyseResult.bThreeCount*3>=0&& CompareCard(&m_bTempTCardData[i-AnalyseResult.bThreeCount*3],AnalyseResult.m_bTCardData,AnalyseResult.bThreeCount*3,AnalyseResult.bThreeCount*3))
					{
						//判断是不是最合理的
						bool m_bIsHaveCard=false;
						for(int j=0;j<m_bTempFCardCount;j++)
						{
							for(n=0;n<AnalyseResult.bThreeCount*3;n++)
							{
								if( GetCardLogicValue(m_bTempTCardData[i-AnalyseResult.bThreeCount*3+n])== GetCardLogicValue(m_bTempFCardData[j]))
									m_bIsHaveCard=true;
							}
						}
						if(bResultCardCount==0||!m_bIsHaveCard)
						{
							CopyMemory(bResultCardData,&m_bTempTCardData[i-AnalyseResult.bThreeCount*3],AnalyseResult.bThreeCount*3);
							bResultCardCount=AnalyseResult.bThreeCount*3;
						}
						if(!m_bIsHaveCard&&bResultCardCount!=0)
						{
							i=0;
							break;
						}
					}
				}

				//加上两个或一个牌分析
				if(bResultCardCount>0)
				{
					bool m_bIsHaveSame;
					if (bTurnOutType==CT_THREE_LINE_TAKE_DOUBLE) 
					{
						//加二两个分析
						for(int m=0;m<AnalyseResult.bDoubleCount;m++)
						{
							for(int j=0;j<m_bTempDCardCount/2;j++)
							{
								//判断是不是最合理的
								m_bIsHaveSame=false;
								for(n=0;n<bResultCardCount;n++)
								{
									//如果上面分析到的三个和现在对牌列表中有相同
									if( GetCardLogicValue(m_bTempDCardData[m_bTempDCardCount-j*2-1])== GetCardLogicValue(bResultCardData[n]))
									{
										m_bIsHaveSame=true;
									}
								}

								//如果在对牌列表中没有和上面分析到的三个中相同的对牌值
								if(!m_bIsHaveSame)
								{
									bool m_bIsHaveCard=false;
									for(int s=0;s<m_bTempTCardCount;s++)
									{
										for(n=0;n<bResultCardCount;n++)
										{
											//判断对牌列表中从最右的牌值和三张列牌表中最左边的值是否相同比较
											if( GetCardLogicValue(m_bTempDCardData[m_bTempDCardCount-j*2-1])== GetCardLogicValue(m_bTempTCardData[s]))//和s比较没多大意义
												m_bIsHaveCard=true;
										}
									}
									if(bResultCardCount==AnalyseResult.bThreeCount*3||!m_bIsHaveCard)
									{
										bResultCardData[AnalyseResult.bThreeCount*3+m*2]=m_bTempDCardData[m_bTempDCardCount-j*2-1];
										bResultCardData[AnalyseResult.bThreeCount*3+m*2+1]=m_bTempDCardData[m_bTempDCardCount-j*2-2];
										bResultCardCount=AnalyseResult.bThreeCount*3+(m+1)*2;
									}
									if(!m_bIsHaveCard)
									{
										n=bResultCardCount;
										j=m_bTempDCardCount/2;
										bResultCardCount=5;
										break;
									}
								}
							}
						}
					}
					//加上一个单牌分析  //重要，是加两个，还是加一个的取舍问题，返回值，在整过程序中都要考虑
					if (bTurnOutType==CT_THREE_LINE_TAKE_ONE)
					{
						for(int m=0;m<AnalyseResult.bSignedCount;m++)
						{
							for(int j=0;j<m_bTempSCardCount;j++)
							{
								//判断是不是最合理的
								m_bIsHaveSame=false;
								for(n=0;n<bResultCardCount;n++)
								{
									if( GetCardLogicValue(m_bTempSCardData[m_bTempSCardCount-j-1])== GetCardLogicValue(bResultCardData[n]))
									{
										m_bIsHaveSame=true;
									}
								}
								if(!m_bIsHaveSame)
								{
									bool m_bIsHaveCard=false;
									for(int s=0;s<m_bTempDCardCount;s++)
									{
										for(n=0;n<bResultCardCount;n++)
										{
											if( GetCardLogicValue(m_bTempSCardData[m_bTempSCardCount-j-1])== GetCardLogicValue(m_bTempDCardData[s]))
											m_bIsHaveCard=true;
										}
									}
									if(bResultCardCount==AnalyseResult.bThreeCount*3||!m_bIsHaveCard)
									{
										bResultCardData[AnalyseResult.bThreeCount*3+m]=m_bTempSCardData[m_bTempSCardCount-j-1];
										bResultCardCount=AnalyseResult.bThreeCount*3+m+1;
									}
									if(!m_bIsHaveCard)
									{
										n=bResultCardCount;
										j=m_bTempSCardCount;
										bResultCardCount=4;
										break;
									}
								}
							}
						}
					}
				}
           }

			//如果得到了要选的牌，则返回真
			if ( ((bResultCardCount==4) && (bTurnOutType==CT_THREE_LINE_TAKE_ONE)) || ((bResultCardCount==5) && (bTurnOutType==CT_THREE_LINE_TAKE_DOUBLE)) )
			{
				return true;
			}
			break;
		case CT_FOUR_LINE_TAKE_ONE:
		case CT_FOUR_LINE_TAKE_DOUBLE:
			{
				//分析扑克
				tagAnalyseResult AnalyseResult;
				AnalysebCardData(bFirstOutCardData,bFirstOutCardCount,AnalyseResult);
				int n = 0;

				for(i=m_bTempFCardCount;i>0;i--)
				{
					if(i-AnalyseResult.bFourCount*4>=0&& CompareCard(&m_bTempFCardData[i-AnalyseResult.bFourCount*4],bFirstOutCardData,AnalyseResult.bFourCount*4,AnalyseResult.bFourCount*4))
					{
						CopyMemory(bResultCardData,&m_bTempFCardData[i-AnalyseResult.bFourCount*4],AnalyseResult.bFourCount*4);
						bResultCardCount=AnalyseResult.bFourCount*4;
						i=0;
						break;  
					}
				}

				//上面得到四个，现在加一个或两个分析
				if(bResultCardCount>0)
				{
					bool m_bIsHaveSame;
					//分析得到两个
					if (bTurnOutType==CT_FOUR_LINE_TAKE_DOUBLE)
					{
						for(int m=0;m<AnalyseResult.bDoubleCount;m++)
						{
							for(int j=0;j<m_bTempDCardCount/2;j++)
							{
								//判断是不是最合理的
								m_bIsHaveSame=false;
								for(int n=0;n<bResultCardCount;n++)
								{
									if( GetCardLogicValue(m_bTempDCardData[m_bTempDCardCount-j*2-1])== GetCardLogicValue(bResultCardData[n]))
									{
										m_bIsHaveSame=true;
									}
								}
								if(!m_bIsHaveSame)
								{
									bool m_bIsHaveCard=false;
									for(int s=0;s<m_bTempTCardCount;s++)
									{
										for(n=0;n<bResultCardCount;n++)
										{
											if( GetCardLogicValue(m_bTempDCardData[m_bTempDCardCount-j*2-1])== GetCardLogicValue(m_bTempTCardData[s]))
											m_bIsHaveCard=true;
										}
									}
									if(bResultCardCount==AnalyseResult.bFourCount*4||!m_bIsHaveCard)
									{
										bResultCardData[AnalyseResult.bFourCount*4+m*2]=m_bTempDCardData[m_bTempDCardCount-j*2-1];
										bResultCardData[AnalyseResult.bFourCount*4+m*2+1]=m_bTempDCardData[m_bTempDCardCount-j*2-2];
										bResultCardCount=AnalyseResult.bFourCount*4+(m+1)*2;
									}
									if(!m_bIsHaveCard)
									{
										n=bResultCardCount;
										j=m_bTempDCardCount/2;
									}
								}
							}
						}
					}

					//上面得到了四个，现在分析得到单牌
					for(int m=0;m<AnalyseResult.bSignedCount;m++)
					{
						for(int j=0;j<m_bTempSCardCount;j++)
						{
							//判断是不是最合理的
							m_bIsHaveSame=false;
							for(n=0;n<bResultCardCount;n++)
							{
								if( GetCardLogicValue(m_bTempSCardData[m_bTempSCardCount-j-1])== GetCardLogicValue(bResultCardData[n]))
								{
									m_bIsHaveSame=true;
								}
							}
							if(!m_bIsHaveSame)
							{
								bool m_bIsHaveCard=false;
								for(int s=0;s<m_bTempDCardCount;s++)
								{
									for(n=0;n<bResultCardCount;n++)
									{
										if( GetCardLogicValue(m_bTempSCardData[m_bTempSCardCount-j-1])== GetCardLogicValue(m_bTempDCardData[j]))
											m_bIsHaveCard=true;
									}
								}
								if(bResultCardCount==AnalyseResult.bFourCount*4||!m_bIsHaveCard)
								{
									bResultCardData[AnalyseResult.bFourCount*4+m]=m_bTempSCardData[m_bTempSCardCount-j-1];
									bResultCardCount=AnalyseResult.bFourCount*4+m+1;
								}
								if(!m_bIsHaveCard)
								{
									n=bResultCardCount;
									j=m_bTempSCardCount;
								}
							}
						}
					}
				}
		 }
		 if (bResultCardCount!=0)
		 {
			 return true;
		 }
		break;
     } //Switch结束

		//判断火煎和炸弹的可能性
		if(bResultCardCount==0)
		{
			//判断炸弹的可能性
			if(m_bTempFCardCount>3)
			{
				for(i=m_bTempFCardCount-4;i>=0;i--)
				{
					if( CompareCard(&m_bTempFCardData[i],bFirstOutCardData,4,bFirstOutCardCount))
					{
						CopyMemory(bResultCardData,&m_bTempFCardData[i],4);
						bResultCardCount=4;
					}
					if (i==0)  //bug 否则 
					{
						break;
					}
				}
			}
			
			//得到合适的炸弹返回
			if (bResultCardCount!=0)
			{
				return true;
			}

			//判断火煎的可能性
			if(bResultCardCount==0)
			{
				if(bHandCardCount>1)
				{
					if( GetCardLogicValue(bHandCardData[0])>15&& GetCardLogicValue(bHandCardData[1])>15)
					{
						CopyMemory(bResultCardData,bHandCardData,2);
						bResultCardCount=2;
					}
				}
			}

			//得到火箭返回
			if (bResultCardCount!=0)
			{
				return true;
			}
		}
	}
	else
	{
		//如果上家出的是0个牌，则返回第一张!
		bResultCardCount=1;
		bResultCardData[0]=bHandCardData[bHandCardCount-1];
		//BYTE bCount = 0;

		//AnalyseFirstCard(bHandCardData,bHandCardCount,bResultCardData,bResultCardCount);
		return true;
	}
	return false;
}
//////////////////////////////////////////////////////////////////////////
