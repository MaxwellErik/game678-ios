﻿#ifndef _DDZCCGAME_VIEW_LAYER_H_
#define _DDZCCGAME_VIEW_LAYER_H_

#include "GameScene.h"
#include "FrameGameView.h"
#include "DDZGameLogic.h"
#include "CMD_DDZ.h"
#include "DDZCardControl.h"

class DDZGameViewLayer : public IGameView
{
public:
	static DDZGameViewLayer *create(GameScene *pGameScene);
	virtual ~DDZGameViewLayer();
	DDZGameViewLayer(GameScene *pGameScene);
	static void reset();
	DDZGameViewLayer(const DDZGameViewLayer&);
	DDZGameViewLayer& operator = (const DDZGameViewLayer&);

	virtual bool init(); 
	virtual void onEnter();
	virtual void onExit();
    virtual void backLoginView(Ref *pSender);
    
	virtual bool onTouchBegan(Touch *pTouch, Event *pEvent);
	virtual void onTouchMoved(Touch *pTouch, Event *pEvent);
	virtual void onTouchEnded(Touch *pTouch, Event *pEvent);

	virtual bool onTextFieldAttachWithIME(TextFieldTTF*pSender){return false;}
	virtual bool onTextFieldDetachWithIME(TextFieldTTF*pSender){return false;}
	virtual bool onTextFieldInsertText(TextFieldTTF *pSender, const char *text, int nLen){return true;}
	virtual bool onTextFieldDeleteBackward(TextFieldTTF *pSender, const char *delText, int nLen){return true;}

	virtual void keyboardWillShow(IMEKeyboardNotificationInfo &info){}
	virtual void keyboardDidShow(IMEKeyboardNotificationInfo &info){}
	virtual void keyboardWillHide(IMEKeyboardNotificationInfo &info){}
	virtual void keyboardDidHide(IMEKeyboardNotificationInfo &info){}

    
public:
	//初始化
	void InitGame();
	void AddButton();
	Menu* CreateButton(const char* btnName ,const Vec2 &p, int tag);
    void callbackBt( Ref *pSender );
    
    void addPlayer(WORD chairID);
    Vec2 getHeadPos(WORD chairID);
    Vec2 getHeadIconPos(WORD chairID);
    Vec2 getNickNamePos(WORD chairID);
    Vec2 getGoldPos(WORD chairID);
    Vec2 getReadyPos(WORD chairID);
    Vec2 getTimerPos(WORD chairID);
    Vec2 getRoleTypePos(WORD chairID);
    
    //定时器函数
    void UpdateTime(float fp);
    void StopTime();
    void StartTime(int _time, int chair);
    
    void playGameSound(int chairId, int soundType);
    void playOutCardSound(int chairId, int cardType, int cardValue);
    int getRand(int start,int end);
    void update(float dt);
    
    // 按钮事件管理
	void DialogConfirm(Ref *pSender);
	void DialogCancel(Ref *pSender);
	string GetCardStringName(BYTE card);
	
    void LoadAlert(int chair);
    void LoadAnim(const char* name, int spriteCount, bool loop);
    void OnAnimate(int nCardType);
    void AnimEnd(Node *pSender);
    //网络接口
public:
	void OnEventUserLeave( tagUserData * pUserData, WORD wChairID, bool bLookonUser );
	//用户进入
	virtual void OnEventUserEnter(tagUserData * pUserData, WORD wChairID, bool bLookonUser);
	//用户积分
	virtual void OnEventUserScore(tagUserData * pUserData, WORD wChairID, bool bLookonUser);
	//用户状态
	virtual void OnEventUserStatus(tagUserData * pUserData, WORD wChairID, bool bLookonUser);
    //用户换桌
    virtual void OnEventUserChangeTable(tagUserData * pUserData, WORD wChairID, bool bLookonUser);
    
	//游戏消息
	virtual bool OnGameMessage(WORD wSubCmdID, const void * pBuffer, WORD wDataSize);
	//场景消息
	virtual bool OnGameSceneMessage(BYTE cbGameStatus, const void * pBuffer, WORD wDataSize);

	virtual void OnReconnectAction(){};

	void OnQuit();

    //游戏消息处理
public:
    bool OnSubSendCard(const void* pBuffer, WORD wDataSize);//开始发牌
    bool OnSubSendMing(const void* pBuffer, WORD wDataSize);//明牌开始数据
	bool OnSubSendOver(const void* pBuffer, WORD wDataSize);//牌数据发送完成
    bool OnSubSendFinish(const void* pBuffer, WORD wDataSize);//牌发放完成
    bool OnSubReSend(const void* pBuffer, WORD wDataSize);//重新发牌
    bool OnSubCallLand(const void* pBuffer, WORD wDataSize);//叫地主
    bool OnSubSnatchLand(const void* pBuffer, WORD wDataSize);//抢地主
    bool OnSubAddScore(const void* pBuffer, WORD wDataSize);//加倍
    bool OnSubAddScoreResult(const void* pBuffer, WORD wDataSize);//加倍结果
    bool OnSubGameStart(const void * pData, WORD wDataSize);//开始游戏
    bool OnSubMingPai(const void * pData, WORD wDataSize);//明牌
	bool OnSubOutCard(const void * pData, WORD wDataSize);
	bool OnSubPassCard(const void * pData, WORD wDataSize);
	bool OnSubGameEnd(const void * pData, WORD wDataSize);

    void MyCardMoveEnd(Node *pSender);
    void LeftCardMoveEnd(Node *pSender);
    void RightCardMoveEnd(Node *pSender);
    void SendCard(float dt);
    void showDiCard(BYTE card[]);
    void onSortCard(WORD ChairId);
    void onOutCard();
    void onPopCard();
    void UpdateOutCardButton();
    void removeAddDouble();
    void drawGameEnd(float dt);
    void AutoOpr();
public:
	//图片
	Sprite*			m_BackSpr;
	Sprite*         m_ClockSpr;
    Sprite*         m_MultiSpr;
    
    LabelAtlas*     m_MultiCnt;
    
    BYTE                m_cbHandCardData[GAME_PLAYER_DDZ][LANDLOAD_CARD_COUNT];//手里扑克数据
    BYTE                m_HandCardCount[GAME_PLAYER_DDZ];                    //手机扑克数量
    DDZ_CardControl     m_CardControl[GAME_PLAYER_DDZ];             //扑克控制
    DDZ_CardControl     m_OutCardControl;                           //出牌控制
    BYTE                m_TurnCardCount;                            //出牌张数
    BYTE                m_TurnCardData[LANDLOAD_CARD_COUNT];        //出牌数据
    BYTE                m_HintCardData[LANDLOAD_CARD_COUNT];        //提示出牌数据

    BYTE                m_wCurOutCardUser;                          //当前出牌玩家
    BYTE                m_NewTurn;                                  //新的一轮
    string              m_PlayerName[GAME_PLAYER_DDZ];
    string              m_CurBackMusic;

    //tag
    int                 m_HeadTag[GAME_PLAYER_DDZ];
    int                 m_roletypeTag[GAME_PLAYER_DDZ];
    int                 m_ReadyTag[GAME_PLAYER_DDZ];
    int                 m_TimeSTag;             //计时器tag
    int                 m_CallLandTag;          //叫地主状态
    int                 m_AddScoreTag;          //加倍状态
    int                 m_NoOutTag;             //不出
    int                 m_DiBackCardTag[3];     //三张底牌背面
    int                 m_DiCardTag[3];         //三张底牌正面
    int                 m_resultTag;
    int                 m_AnimTga;
    int                 m_AlertAnimTag[GAME_PLAYER_DDZ];
    
    int                 m_StartTime;            //时间索引
    WORD                m_wCurrentUser;         //当前操作用户
    int                 m_LandUser;             //第一个叫地主用户
    int                 m_SendCardCount;        //发牌数量
    int                 m_Multi;                //当局倍数
    int                 m_bMingPai[GAME_PLAYER_DDZ];//玩家明牌标志
    int                 m_CurGameState;            //当前状态
    int                 m_QiangdizhuIdx;           
    int                 m_HintIndex;               //提示次数
    bool                m_bAutoPlay;
    bool                m_bAlert[GAME_PLAYER_DDZ];
protected:
    CGameLogicDDZ		m_GameLogic;
    
};

#endif
