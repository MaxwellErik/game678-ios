#ifndef _GameKind_H_
#define _GameKind_H_

#include "GameScene.h"
#include "cocos-ext.h"
#include "CCGameScrollView.h"
#include "ui/UIPageView.h"
#include "ui/UIButton.h"
#include "ui/CocosGUI.h"
class GameKind : public GameLayer,public cocos2d::extension::TableViewDataSource, public cocos2d::extension::TableViewDelegate
{
public:
	virtual bool init();  
	static GameKind *create(GameScene *pGameScene);
	virtual ~GameKind();
	GameKind(GameScene *pGameScene);
	GameKind(const GameKind&);
	GameKind& operator = (const GameKind&);

private:
	virtual void onEnter();
	virtual void onExit();
    virtual void update (float deltaTime);

	virtual bool onTouchBegan(Touch *pTouch, Event *pEvent);
    virtual void onTouchMoved(Touch *pTouch, Event *pEvent);
    virtual void onTouchEnded(Touch *pTouch, Event *pEvent);
	virtual void onTouchCancelled(Touch *pTouch, Event *pEvent){}
	
	virtual bool onTextFieldAttachWithIME(TextFieldTTF*pSender){return false;}
	virtual bool onTextFieldDetachWithIME(TextFieldTTF*pSender){return false;}
	virtual bool onTextFieldInsertText(TextFieldTTF *pSender, const char *text, int nLen){return true;}
	virtual bool onTextFieldDeleteBackward(TextFieldTTF *pSender, const char *delText, int nLen){return true;}

	//�϶�
	virtual void scrollViewDidScroll(cocos2d::extension::ScrollView* view){}
	virtual void scrollViewDidZoom(cocos2d::extension::ScrollView* view){}
	virtual void tableCellTouched(cocos2d::extension::TableView* table, cocos2d::extension::TableViewCell* cell);
	virtual cocos2d::Size tableCellSizeForIndex(cocos2d::extension::TableView *table, ssize_t idx);
	virtual cocos2d::extension::TableViewCell* tableCellAtIndex(cocos2d::extension::TableView *table,ssize_t idx);
	virtual ssize_t numberOfCellsInTableView(cocos2d::extension::TableView *table);

	virtual void keyboardWillShow(IMEKeyboardNotificationInfo &info){}
	virtual void keyboardDidShow(IMEKeyboardNotificationInfo &info){}
	virtual void keyboardWillHide(IMEKeyboardNotificationInfo &info){}
	virtual void keyboardDidHide(IMEKeyboardNotificationInfo &info){}

	Menu* CreateButton( std::string szBtName ,const Vec2 &p , int tag );
    int AddGame(Vec2 pos, tagGameKind GameKind, ui::Layout* page);
	virtual void callbackBt(Ref *pSender);

public:
	void backTologin();
	void addGameToPage();
	void AddText();

	void UpdateRoom(int KindID);
	void SortServerItem(VectorServer & RoomVec);
	void OpenGameList(int KindId);
	void AddRoomList(TableViewCell* cell, tagGameServer temp, int _index);
	const char* GetGameNameForKindId(int KindId);
	const char* GetGameIconForKindId(int KindId);
    const char* GetGameResourceFileName();
	void OnCallMove();
    void sendFishDataToLua();
    void menuCallback(cocos2d::Ref* pSender,ui::Widget::TouchEventType type);
    void enterRoomList();
    void updatePoint();
    int getRand(int start, int end);
public:
    ui::PageView *m_pageView;
    Sprite*      m_runbg;
    ClippingNode* m_cNode;
    cocos2d::Size          m_clippingSize;
    bool             m_isBoardcasting;
	TableView*   m_tableView;
	__Array*	 m_Arr;
	VectorServer m_GameRoomList;
	int			 m_RoomCount;
	vector<int>	 m_GameKindID;
    Label*       m_MoveText;
	int			 m_RoomTitleBackTga;
	int			 m_EnterRoomBtnTga;
	int			 m_BackBtnTga;
	int			 m_MsgTga;
    int          m_pointTag[10];
    long         m_curPage;
    int          m_GameTag;
};

#endif
