#ifndef GLOBAL_DEF_HEAD_FILE
#define GLOBAL_DEF_HEAD_FILE

#pragma once
#include "GlobalProperty.h"
#include "def.h"
#include <assert.h>
//////////////////////////////////////////////////////////////////////////
//公共定义

#define MAX_CHAIR						300								//最大椅子
#define MAX_CHAIR_NORMAL				8								//最大人数

#define MAX_ANDROID						256								//最大机器
#define MAX_CHAT_LEN					128								//聊天长度
#define LIMIT_CHAT_TIMES				1200							//限时聊天

#ifndef ASSERT
#define ASSERT(e)	assert(e)
#endif

#define ASSERT_RETURN(e , b) ASSERT(e); if (!(e)) return b;


//用户属性
#define DTP_GR_NICK_NAME			10									//用户昵称
#define DTP_GR_GROUP_NAME			11									//社团名字
#define DTP_GR_UNDER_WRITE			12									//个性签名
/////////////////////////////////////////////////////////////////////////////////////////
//宏定义

#define MAX_TASK						256									//最大任务数

//端口定义
#define PORT_VIDEO_SERVER				7600								//视频服务器
#define PORT_LOGON_SERVER				9101								//登陆服务器
#define PORT_CENTER_SERVER				9110								//中心服务器

//网络数据定义
#define SOCKET_VER						0x66								//网络版本
#define SOCKET_BUFFER					16384								//网络缓冲
#define SOCKET_PACKET					(SOCKET_BUFFER-sizeof(CMD_Head))//网络缓冲

/////////////////////////////////////////////////////////////////////////////////////////
#define Dating_GameKind_DDZ             100  //斗地主
#define Dating_GameKind_QBSQ            120  //千变双扣
#define Dating_GameKind_Ox2             130  //二人牛牛
#define Dating_GameKind_Ox6             160  //通比牛牛
#define Dating_GameKind_Ox4             165  //四人牛牛
#define Dating_GameKind_WZSZ            170  //温州四张
#define Dating_GameKind_SH              190  //港式五张
#define Dating_GameKind_WZLZ            200  //温州两张
#define Dating_GameKind_HHSW            210  //虎虎生威
#define Dating_GameKind_30S             220  //港澳30秒
#define Dating_GameKind_BRNN            230  //多人牛牛
#define Dating_GameKind_SDB             240  //多人十点半
#define Dating_GameKind_WZMJ            400  //温州麻将
#define Dating_GameKind_WKNH            610  //大闹天宫


//内核命令码
#define MDM_KN_COMMAND					0									//内核命令
#define SUB_KN_DETECT_SOCKET			1									//检测命令
#define SUB_KN_SHUT_DOWN_SOCKET			2									//中断网络

#define RMB_TO_SCORE					100									//人民币兑灵珠比率
#define RMB_TO_SLIVER					20000								//人民币兑欢乐豆比率

#pragma pack(1)

//网络内核
struct CMD_Info
{
	BYTE								cbVersion;							//版本标识
	BYTE								cbCheckCode;						//效验字段
	WORD								wPacketSize;						//数据大小
};

//网络命令
struct CMD_Command
{
	WORD								wMainCmdID;							//主命令码
	WORD								wSubCmdID;							//子命令码
};

//网络包头
struct CMD_Head
{
	CMD_Info							CmdInfo;							//基础结构
	CMD_Command							CommandInfo;						//命令信息
};

//网络包缓冲
struct CMD_Buffer
{
	CMD_Head							Head;								//数据包头
	BYTE								cbBuffer[SOCKET_PACKET];			//数据缓冲
};

//检测结构信息
struct CMD_KN_DetectSocket
{
	IosDword								dwSendTickCount;					//发送时间
	IosDword								dwRecvTickCount;					//接收时间
};

/////////////////////////////////////////////////////////////////////////////////////////

//IPC 数据定义
#define IPC_VER							1								//IPC 版本
#define IPC_IDENTIFIER					0x0001								//标识号码
#define IPC_PACKAGE						(10240-sizeof(IPC_Head))			//最大 IPC 包
#define IPC_BUFFER						(sizeof(IPC_Head)+IPC_PACKAGE)		//缓冲长度

//IPC 数据包头
struct IPC_Head
{
	WORD								wVersion;							//IPC 版本
	WORD								wDataSize;							//数据大小
	WORD								wMainCmdID;							//主命令码
	WORD								wSubCmdID;							//子命令码
};

//IPC 缓冲结构
struct IPC_Buffer
{
	IPC_Head							Head;								//数据包头
	BYTE								cbBuffer[IPC_PACKAGE];				//数据缓冲
};

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
//设备类型
#define DEVICE_TYPE_PC              0x00                                //PC
#define DEVICE_TYPE_ANDROID         0x10                                //Android
#define DEVICE_TYPE_ITOUCH          0x20                                //iTouch
#define DEVICE_TYPE_IPHONE          0x40                                //iPhone
#define DEVICE_TYPE_IPAD            0x80                                //iPad

//长度宏定义
#define TYPE_LEN						32									//种类长度
#define KIND_LEN						32									//类型长度
#define STATION_LEN						32									//站点长度
#define SERVER_LEN						32									//房间长度
#define MODULE_LEN						32									//进程长度

//性别定义
#define GENDER_NULL						0									//未知性别
#define GENDER_BOY						1									//男性性别
#define GENDER_GIRL						2									//女性性别

//游戏类型
#define GAME_GENRE_SCORE				0x0001								//欢乐豆类型
#define GAME_GENRE_GOLD					0x0002								//欢乐豆筹码类型
#define GAME_GENRE_MATCH				0x0004								//比赛类型
#define GAME_GENRE_EDUCATE				0x0008								//训练类型
#define GAME_GENRE_SCORE_CHIP			0x0010								//灵珠筹码类型
#define GAME_GENRE_LING					0x0020								//灵珠类型

//比赛类型
#define GAME_MATCH_TYPE_1				0x0001								//六人赛
#define GAME_MATCH_TYPE_2				0x0002								//十二人赛
#define GAME_MATCH_TYPE_3				0x0004								//二四人赛

//房间类型
#define	ROOM_GENRE_1					0x0001								//新手
#define	ROOM_GENRE_2					0x0002								//初级
#define	ROOM_GENRE_3					0x0003								//中级
#define	ROOM_GENRE_4					0x0004								//高级
#define	ROOM_GENRE_TEST					0x0005								//测试
#define	ROOM_GENRE_ACTION				0x0006								//活动

//OpenID
#define OPENFLAT_UZ						1								//悠哉游戏
#define OPENFLAT_QQ						2								//QQ帐号
#define OPENFLAT_SINA					3								//新浪微博
#define OPENFLAT_GUEST					4								//游客
#define OPENFLAT_XM						5								//小米
#define OPENFLAT_IOS					6								//苹果

//TODO
//平台类型(代理商)
#define PLATFORM_UZ						1								//悠哉游戏
#define PLATFORM_YW						2								//娱玩
#define PLATFORM_AYX					3								//爱游戏
#define PLATFORM_PGY					4								//苹果园
#define PLATFORM_QH360					5								//奇虎360
#define PLATFORM_QJLY					6								//曲江乐雅
#define PLATFORM_QQ						7								//腾讯
#define PLATFORM_CIB					8								//CIB
#define PLATFORM_JHWL					9								//巨煌网络
#define PLATFORM_YYW					10								//丫丫玩
#define PLATFORM_99L					11								//九九乐
#define PLATFORM_XM						12								//小米
#define PLATFORM_YDMM					13								//移动MM
#define PLATFORM_IOS					14								//苹果
#define PLATFORM_JSWS					15								//金山卫士
#define PLATFORM_JWTX					16								//九武游戏
#define PLATFORM_IOSJAIL				17								//IOS越狱
#define PLATFORM_MM_MIX					18								//移动MM
#define PLATFORM_ML						19								//魔灵
#define PLATFORM_FF						20								//凡飞

#define KIND_ID_JJDZ					2000								//奖金德州
#define KIND_ID_SHZ						2001								//水浒传


#define TASK_CONDITION_TYPE_LOGIN				1	//登录任务
#define TASK_CONDITION_TYPE_TURN				2	//局数任务
#define TASK_CONDITION_TYPE_WIN					3	//赢局任务
#define TASK_CONDITION_TYPE_WINSCORE			4	//赢分任务
#define TASK_CONDITION_TYPE_RECHARGE			5	//充值任务
#define TASK_CONDITION_TYPE_GAME				6	//游戏特有任务

#define TASK_STATUS_NONE						0		//初始
#define TASK_STATUS_FINISH						1		//完成
#define TASK_STATUS_PRIZE						2		//领取奖励

//游戏类型结构
struct tagGameType
{
	WORD								wSortID;							//排序号码
	WORD								wTypeID;							//种类号码
    WORD                                wClientID;              //客户端类型(0:全部，1：仅PC，2：仅手机)
	CHAR								szTypeName[TYPE_LEN];				//种类名字
};

//游戏名称结构
struct tagGameKind
{
    WORD                wSortID;                        //排序号码
    WORD                wTypeID;                        //类型号码
    WORD                wKindID;                        //名称号码
    WORD                cbIsGoldGame;                   //金币游戏
    UINT32              dwMaxVersion;                   //最新版本
    UINT32              dwOnLineCount;                  //在线数目
    CHAR                szKindName[KIND_LEN];           //游戏名字
    CHAR                szProcessName[MODULE_LEN];      //进程名字
    CHAR                szLinkName[KIND_LEN];           //进程连接
};

//游戏站点结构
struct tagGameStation
{
	WORD								wSortID;							//排序号码
	WORD								wKindID;							//名称号码
	WORD								wJoinID;							//挂接号码
	WORD								wStationID;							//站点号码
	CHAR								szStationName[STATION_LEN];			//站点名称
};

//游戏房间列表结构
struct tagGameServer
{
    WORD                wSortID;                       //排序号码
    WORD                wKindID;                       //名称号码
    WORD                wServerID;                     //房间号码
    WORD                wStationID;                    //站点号码
    WORD                wServerPort;                   //房间端口
    UINT32              dwServerAddr;                  //房间地址
    UINT32              dwServerAddr2;                 //房间地址
    UINT32              dwOnLineCount;                 //在线人数
    CHAR                szServerName[SERVER_LEN];      //房间名称
    WORD                wClientID;                      //客户端类型(0:全部，1：仅PC，2：仅手机)
};

//游戏桌子列表结构
struct tagGameTable
{
	WORD				wKindID;							//游戏ID
	WORD                wTableID;							//桌子ID
	WORD				wServerID;							//所属房间ID
	LONGLONG			lMinChip;							//最小下注
	LONGLONG			lMaxChip;							//最大下注
	LONGLONG			lMinExchange;						//最小兑换
	LONGLONG			lMaxExchange;						//最大兑换
	WORD				wChairCount;						//最大椅子数
	WORD				wUserCount;							//当前用户数量
};

//游戏级别结构
struct tagLevelItem
{
	LONGLONG			lLevelScore;						//级别积分
	wchar_t				szLevelName[16];					//级别描述
};
//////////////////////////////////////////////////////////////////////////

//用户状态定义
#define US_NULL			0x00								//没有状态
#define US_FREE			0x01								//站立状态
#define US_SIT			0x02								//坐下状态
#define US_READY		0x03								//同意状态
#define US_LOOKON		0x04								//旁观状态
#define US_PLAY			0x05								//游戏状态
#define US_OFFLINE		0x06								//断线状态

//长度宏定义
#define QQ_LEN                         18                                   //QQ号长度
#define TIME_LEN                       20                                   //时间长度
#define NAME_LEN						32                                  //名字长度
#define PASS_LEN						33									//密码长度
#define EMAIL_LEN						32									//邮箱长度
#define GROUP_LEN						32									//社团长度
#define COMPUTER_ID_LEN					33									//机器序列
#define UNDER_WRITE_LEN					63									//个性签名
#define MAX_RANKING_INFO                50                              //排行榜最大个数
#define TOKEN_LEN						33									//Token长度
#define MAX_ADDR                        32                                  //地址长度
#define OPEN_LEN						64									//OpenID长度
#define MAX_GAME_BUFFER					10
#define LEN_MOBILE_PHONE				12									//移动电话

struct tagUserBufferInfo
{
	DWORD                               dwUserID;							//用户编号
	WORD								wKindID;							//游戏编号
	WORD								wServerID;							//房间编号
	LONGLONG							lUserBuffer[MAX_GAME_BUFFER];		//玩家buffer
	LONGLONG							lTurnNumber;						//牌局编号
};

struct tagTaskInfo
{
	DWORD                               dwUserID;							//用户编号
	WORD								wKindID;							//游戏编号
	WORD								wServerID;							//房间编号
	WORD								wTaskID;							//任务编号
	BYTE								cType;								//奖励类型(1元宝 2经验值)
	LONGLONG							lNum;								//奖励数值
	LONGLONG							lTurnNumber;						//牌局编号
};
////用户积分
//struct tagMobileUserScore
//{
//	//积分信息
//	LONGLONG							lScore;								//用户分数
//
//	//输赢信息
//	IosDword							dwWinCount;							//胜利盘数
//	IosDword							dwLostCount;						//失败盘数
//	IosDword							dwDrawCount;						//和局盘数
//	IosDword							dwFleeCount;						//逃跑盘数
//
//	//全局信息
//	IosDword							dwExperience;						//用户经验
//};
//用户积分信息
struct tagUserScore
{
    LONGLONG            lScore;                //用户分数
    LONGLONG            lGameGold;             //游戏金币
    LONGLONG            lInsureScore;          //存储金币
    LONGLONG            lReturnValue;          //返利值
    UINT32              lWinCount;             //胜利盘数
    UINT32              lLostCount;            //失败盘数
    UINT32              lDrawCount;            //和局盘数
    UINT32              lFleeCount;            //断线数目
    UINT32              lExperience;           //用户经验
};

//用户状态信息
struct tagUserStatus
{
	WORD								wTableID;							//桌子号码
	WORD								wChairID;							//椅子位置
	BYTE								cbUserStatus;						//用户状态
};

//用户信息
struct tagUserInfoHead
{
    //用户属性
    WORD								wFaceID;							//头像索引
    UINT32								dwUserID;							//用户 I D
    UINT32								dwGameID;							//游戏 I D
    UINT32								dwGroupID;							//社团索引
    UINT32								dwUserRight;						//用户等级
    UINT32								lLoveliness;						//用户魅力
    UINT32								dwMasterRight;						//管理权限
    
    //用户属性
    BYTE								cbGender;							//用户性别
    BYTE								cbMemberOrder;						//会员等级
    BYTE								cbMasterOrder;						//管理等级
    double								dbLastLogonTime;                    //登录时间
    
    //用户状态
    WORD								wTableID;							//桌子号码
    WORD								wChairID;							//椅子位置
    BYTE								cbUserStatus;						//用户状态
    
    //用户积分
    tagUserScore						UserScoreInfo;						//积分信息
    
    //扩展信息
    UINT32								dwCustomFaceVer;					//上传头像
    UINT32								dwPropResidualTime[PROPERTY_COUNT];	//道具时间
    BYTE                                cbBanker;                           //是否商人
};

//用户信息结构
struct tagUserData
{
	//用户属性
	WORD                            wFaceID;							//头像索引
	DWORD                           dwCustomFaceVer;					//上传头像
	DWORD                           dwUserID;							//用户 I D
	DWORD                           dwGroupID;							//社团索引
	DWORD                           dwGameID;							//用户 I D
	DWORD                           dwUserRight;						//用户等级
	int                             lLoveliness;						//用户魅力
	DWORD                           dwMasterRight;						//管理权限
	CHAR							szName[NAME_LEN];					//用户名字
	CHAR							szNickName[NAME_LEN];				//用户昵称
	CHAR							szGroupName[GROUP_LEN];				//社团名字
	CHAR							szUnderWrite[UNDER_WRITE_LEN];		//个性签名

	//用户属性
	WORD							wGender;							//用户性别
	BYTE							cbMemberOrder;						//会员等级
	BYTE							cbMasterOrder;						//管理等级

	//用户积分
	LONGLONG						lInsureScore;						//消费金币
	LONGLONG						lGameGold;							//游戏金币
	LONGLONG						lScore;								//用户分数

	/***新增字段wenxue***/
	LONGLONG						lSilverScore;						//银币
	LONGLONG						lInsureSilverScore;					//银行银币

	LONGLONG						lIngot;								//元宝
	LONGLONG						lHappyBear;							//酒吧豆

	LONGLONG						lBodyScore;							//身上灵珠
	LONGLONG						lBodySilverScore;					//身上银币
	LONGLONG						lBodyChip;							//身上筹码

	int								lWinCount;							//胜利盘数
	int								lLostCount;							//失败盘数
	int								lDrawCount;							//和局盘数
	int								lFleeCount;							//断线数目
	int								lExperience;						//用户经验

	//用户状态
	WORD							wTableID;							//桌子号码
	WORD							wChairID;							//椅子位置
	BYTE							cbUserStatus;						//用户状态

	//其他信息
	BYTE							cbCompanion;						//用户关系
	DWORD                           dwPropResidualTime[PROPERTY_COUNT];	//道具时间

	LONGLONG						lLostMoney;							//游戏输分
	CHAR                            szUserAddress[MAX_ADDR];            //用户地址
    BYTE                            cbBanker;                           //是否商人
};

//////////////////////////////////////////////////////////////////////////

//机器序列号结构
struct tagClientSerial
{
	DWORD								dwSystemVer;						//系统版本
	DWORD								dwComputerID[3];					//机器序列
};

//配置缓冲结构
struct tagOptionBuffer
{
	BYTE								cbBufferLen;						//数据长度
	BYTE								cbOptionBuf[32];					//设置缓冲
};

//////////////////////////////////////////////////////////////////////////

const DWORD g_dwPacketKey=0xAABB2288;

//发送映射
const BYTE g_SendByteMap[256]=
{
    0x2B,0x84,0xE3,0xA0,0xF8,0xD0,0x53,0xF9,0x6C,0x4F,0x2D,0x49,0x20,0x39,0xDE,0x4E,
    0x75,0x06,0x25,0x01,0xE1,0xE2,0xD3,0x48,0xB8,0x4A,0x6B,0xA2,0x35,0xC0,0xE5,0x89,
    0x9B,0x3B,0xF5,0x69,0x03,0xEB,0x16,0x2F,0x15,0x90,0x98,0xE4,0xC3,0x6F,0x62,0xB9,
    0x2C,0x99,0xAD,0x60,0xBC,0x0F,0x11,0x74,0xBD,0x6E,0x23,0x3E,0xCD,0x00,0xC5,0xA5,
    0x86,0x0C,0xA6,0x7F,0x4D,0x80,0xCE,0x26,0x8D,0x8F,0x8C,0xBB,0x0E,0x88,0x28,0x18,
    0x76,0xEE,0x32,0xDF,0x42,0xE6,0xDD,0xF6,0xCC,0xC1,0x05,0x40,0xD8,0xD5,0x9E,0x67,
    0xA9,0x45,0xFF,0x81,0x85,0xC7,0x1F,0x19,0xD9,0x13,0x82,0xDA,0xC4,0xAC,0xF2,0x47,
    0x34,0x44,0xCB,0x59,0x78,0x1A,0x73,0xDB,0x7B,0xF3,0x7A,0x8E,0xFD,0xA4,0x36,0x94,
    0x79,0x07,0x29,0x22,0x7C,0x7D,0xC6,0xBA,0x8B,0xD7,0x43,0x5C,0x64,0xE9,0xF0,0xB2,
    0xDC,0xA8,0x97,0x91,0x56,0x0B,0xD1,0xE8,0x9C,0x12,0x77,0xFB,0x17,0xCA,0xD6,0x21,
    0x72,0xF7,0x41,0xAB,0x7E,0x87,0x09,0x71,0xFA,0xD2,0xEA,0xE7,0x92,0x1B,0x2A,0xAA,
    0x14,0xFC,0x51,0x65,0x1C,0xBE,0x33,0x96,0xF4,0xBF,0xAF,0x9F,0x38,0x5F,0x3A,0x57,
    0x24,0x2E,0xA3,0xB7,0x02,0x5A,0x1D,0xED,0x4C,0xB1,0x66,0xEC,0x5D,0x0D,0x27,0x58,
    0x1E,0xFE,0x93,0xA1,0x70,0x10,0xB4,0x5E,0xC2,0x8A,0x6D,0x63,0xC9,0x31,0xCF,0x30,
    0x08,0xB5,0x52,0x3F,0xAE,0xEF,0xE0,0xC8,0x9D,0xB6,0x4B,0xF1,0x04,0x61,0x6A,0x37,
    0x50,0x54,0xB3,0x3D,0xB0,0x9A,0xA7,0x83,0x5B,0x0A,0x3C,0x68,0x46,0xD4,0x95,0x55
};

//接收映射
const BYTE g_RecvByteMap[256]=
{
    0x3D,0x13,0xC4,0x24,0xEC,0x5A,0x11,0x81,0xE0,0xA6,0xF9,0x95,0x41,0xCD,0x4C,0x35,
    0xD5,0x36,0x99,0x69,0xB0,0x28,0x26,0x9C,0x4F,0x67,0x75,0xAD,0xB4,0xC6,0xD0,0x66,
    0x0C,0x9F,0x83,0x3A,0xC0,0x12,0x47,0xCE,0x4E,0x82,0xAE,0x00,0x30,0x0A,0xC1,0x27,
    0xDF,0xDD,0x52,0xB6,0x70,0x1C,0x7E,0xEF,0xBC,0x0D,0xBE,0x21,0xFA,0xF3,0x3B,0xE3,
    0x5B,0xA2,0x54,0x8A,0x71,0x61,0xFC,0x6F,0x17,0x0B,0x19,0xEA,0xC8,0x44,0x0F,0x09,
    0xF0,0xB2,0xE2,0x06,0xF1,0xFF,0x94,0xBF,0xCF,0x73,0xC5,0xF8,0x8B,0xCC,0xD7,0xBD,
    0x33,0xED,0x2E,0xDB,0x8C,0xB3,0xCA,0x5F,0xFB,0x23,0xEE,0x1A,0x08,0xDA,0x39,0x2D,
    0xD4,0xA7,0xA0,0x76,0x37,0x10,0x50,0x9A,0x74,0x80,0x7A,0x78,0x84,0x85,0xA4,0x43,
    0x45,0x63,0x6A,0xF7,0x01,0x64,0x40,0xA5,0x4D,0x1F,0xD9,0x88,0x4A,0x48,0x7B,0x49,
    0x29,0x93,0xAC,0xD2,0x7F,0xFE,0xB7,0x92,0x2A,0x31,0xF5,0x20,0x98,0xE8,0x5E,0xBB,
    0x03,0xD3,0x1B,0xC2,0x7D,0x3F,0x42,0xF6,0x91,0x60,0xAF,0xA3,0x6D,0x32,0xE4,0xBA,
    0xF4,0xC9,0x8F,0xF2,0xD6,0xE1,0xE9,0xC3,0x18,0x2F,0x87,0x4B,0x34,0x38,0xB5,0xB9,
    0x1D,0x59,0xD8,0x2C,0x6C,0x3E,0x86,0x65,0xE7,0xDC,0x9D,0x72,0x58,0x3C,0x46,0xDE,
    0x05,0x96,0xA9,0x16,0xFD,0x5D,0x9E,0x89,0x5C,0x68,0x6B,0x77,0x90,0x56,0x0E,0x53,
    0xE6,0x14,0x15,0x02,0x2B,0x1E,0x55,0xAB,0x97,0x8D,0xAA,0x25,0xCB,0xC7,0x51,0xE5,
    0x8E,0xEB,0x6E,0x79,0xB8,0x22,0x57,0xA1,0x04,0x07,0xA8,0x9B,0xB1,0x7C,0xD1,0x62
};

////接收映射
//const BYTE g_RecvByteMap[256]=
//{
//	0x51,0xA1,0x9E,0xB0,0x1E,0x83,0x1C,0x2D,0xE9,0x77,0x3D,0x13,0x93,0x10,0x45,0xFF,
//	0x6D,0xC9,0x20,0x2F,0x1B,0x82,0x1A,0x7D,0xF5,0xCF,0x52,0xA8,0xD2,0xA4,0xB4,0x0B,
//	0x31,0x97,0x57,0x19,0x34,0xDF,0x5B,0x41,0x58,0x49,0xAA,0x5F,0x0A,0xEF,0x88,0x01,
//	0xDC,0x95,0xD4,0xAF,0x7B,0xE3,0x11,0x8E,0x9D,0x16,0x61,0x8C,0x84,0x3C,0x1F,0x5A,
//	0x02,0x4F,0x39,0xFE,0x04,0x07,0x5C,0x8B,0xEE,0x66,0x33,0xC4,0xC8,0x59,0xB5,0x5D,
//	0xC2,0x6C,0xF6,0x4D,0xFB,0xAE,0x4A,0x4B,0xF3,0x35,0x2C,0xCA,0x21,0x78,0x3B,0x03,
//	0xFD,0x24,0xBD,0x25,0x37,0x29,0xAC,0x4E,0xF9,0x92,0x3A,0x32,0x4C,0xDA,0x06,0x5E,
//	0x00,0x94,0x60,0xEC,0x17,0x98,0xD7,0x3E,0xCB,0x6A,0xA9,0xD9,0x9C,0xBB,0x08,0x8F,
//	0x40,0xA0,0x6F,0x55,0x67,0x87,0x54,0x80,0xB2,0x36,0x47,0x22,0x44,0x63,0x05,0x6B,
//	0xF0,0x0F,0xC7,0x90,0xC5,0x65,0xE2,0x64,0xFA,0xD5,0xDB,0x12,0x7A,0x0E,0xD8,0x7E,
//	0x99,0xD1,0xE8,0xD6,0x86,0x27,0xBF,0xC1,0x6E,0xDE,0x9A,0x09,0x0D,0xAB,0xE1,0x91,
//	0x56,0xCD,0xB3,0x76,0x0C,0xC3,0xD3,0x9F,0x42,0xB6,0x9B,0xE5,0x23,0xA7,0xAD,0x18,
//	0xC6,0xF4,0xB8,0xBE,0x15,0x43,0x70,0xE0,0xE7,0xBC,0xF1,0xBA,0xA5,0xA6,0x53,0x75,
//	0xE4,0xEB,0xE6,0x85,0x14,0x48,0xDD,0x38,0x2A,0xCC,0x7F,0xB1,0xC0,0x71,0x96,0xF8,
//	0x3F,0x28,0xF2,0x69,0x74,0x68,0xB7,0xA3,0x50,0xD0,0x79,0x1D,0xFC,0xCE,0x8A,0x8D,
//	0x2E,0x62,0x30,0xEA,0xED,0x2B,0x26,0xB9,0x81,0x7C,0x46,0x89,0x73,0xA2,0xF7,0x72
//};


typedef struct AppInfo
{
	//声音
	bool		bMasterSwitch;	//总开关
	bool		bMusic;			//背景音乐
	bool		bSound;			//音效
	bool		bSpeech;		//语音

	int			iPosMusic;		//背景音乐声音的大小(0~1000)
	int			iPosSound;		//音效的大小(0~1000)
	int			iPosSpeech;		//说话声音的大小(0~1000)
}AppInfoDef;

#pragma pack()

//////////////////////////////////////////////////////////////////////////

#endif
