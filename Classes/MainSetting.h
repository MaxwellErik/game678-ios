#ifndef _MAINSETTING_H_
#define _MAINSETTING_H_

#include "GameScene.h"
#include "cocos-ext.h"
USING_NS_CC_EXT;

class MainSetting : public GameLayer
{
public:
	virtual bool init();  
	static MainSetting *create(GameScene *pGameScene);
	virtual ~MainSetting();
	MainSetting(GameScene *pGameScene);
	MainSetting(const MainSetting&);
	MainSetting& operator = (const MainSetting&);

public:
	virtual void onEnter();
	virtual void onExit();
	virtual bool onTouchBegan(Touch *pTouch, Event *pEvent){return true;}
	virtual void onTouchMoved(Touch *pTouch, Event *pEvent){}
	virtual void onTouchEnded(Touch *pTouch, Event *pEvent){}
	virtual void onTouchCancelled(Touch *pTouch, Event *pEvent){}

	void callbackBt(Ref *pSender );
    void valueChanged(Ref *sender, Control::EventType event);

	Menu* CreateButton(std::string szBtName ,const Vec2 &p , int tag );
    
public:
    void OnRemove();
};
#endif
