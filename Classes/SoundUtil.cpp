﻿#include "SoundUtil.h"

#include "SimpleAudioEngine.h"

#include "JniSink.h"

//#define _ANDROID_DEBUG
//#define	CLOSE_SOUND

SoundUtil*						SoundUtil::m_pSoundUtil=NULL;
///////////////////////////////////////////////////////////////////////////

SoundUtil::SoundUtil(void)
{
}


SoundUtil::~SoundUtil(void)
{
	delete m_pSoundUtil;
}

void SoundUtil::preloadEffect(const char* MusicName )
{
#ifdef CLOSE_SOUND
	return;
#endif

char szName[128]={0};
#if(CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
		sprintf(szName , "%s.mp3" , MusicName);
		CCLOG("%s" , szName);
		CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect(szName);
#endif
#if(CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
		sprintf(szName , "%s.mp3" , MusicName);
		CCLOG("%s" , szName);
		CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect(szName);
#endif 
#if(CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
		sprintf(szName , "%s.mp3" , MusicName);
		CCLOG("%s" , szName);
		CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect(szName);
#endif
		CCLOG("%s" , szName);
}

void SoundUtil::playEffect(const char* MusicName , bool bLoop)
{
#ifdef CLOSE_SOUND
	return;
#endif
#ifndef _ANDROID_DEBUG
#if(CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
	playMp3(MusicName, bLoop);
#endif 
#if(CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
	playMp3(MusicName, bLoop);
#endif 
#if(CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
	playMp3(MusicName, bLoop);
#endif

#endif
}

void SoundUtil::playMp3(const char* MusicName , bool bLoop )
{
#ifdef CLOSE_SOUND
	return;
#endif
char szName[128]={0};
sprintf(szName , "%s.mp3" , MusicName);
CocosDenshion::SimpleAudioEngine::getInstance()->playEffect(szName , bLoop);

}

void SoundUtil::playWav(const char* MusicName , bool bLoop )
{
#ifdef CLOSE_SOUND
	return;
#endif
	char szName[128]={0};
	sprintf(szName , "%s.wav" , MusicName);
	CocosDenshion::SimpleAudioEngine::getInstance()->playEffect(szName , bLoop);
}

void SoundUtil::playOgg(const char* MusicName , bool bLoop )
{
#ifdef CLOSE_SOUND
	return;
#endif
	char szName[128]={0};
	sprintf(szName , "%s.ogg" , MusicName);
	CocosDenshion::SimpleAudioEngine::getInstance()->playEffect(szName , bLoop);
}

// 播放 背景 音效
void SoundUtil::playBackMusic(const char *pMusicName,bool bLoop)
{
#ifdef CLOSE_SOUND
	return;
#endif

	char szName[128]={0};
#if(CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
	sprintf(szName , "%s.mp3" , pMusicName);
	CocosDenshion::SimpleAudioEngine::getInstance()->setBackgroundMusicVolume(g_GlobalUnits.m_fSoundValue);
	CocosDenshion::SimpleAudioEngine::getInstance()->playBackgroundMusic(szName , bLoop);

#endif
#if(CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
	sprintf(szName , "%s.mp3" , pMusicName);
	CocosDenshion::SimpleAudioEngine::sharedEngine()->setBackgroundMusicVolume(g_GlobalUnits.m_fSoundValue);
	CocosDenshion::SimpleAudioEngine::sharedEngine()->playBackgroundMusic(szName , bLoop);

#endif
#if(CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
	sprintf(szName , "%s.mp3" , pMusicName);
	CocosDenshion::SimpleAudioEngine::sharedEngine()->setBackgroundMusicVolume(g_GlobalUnits.m_fSoundValue);
	CocosDenshion::SimpleAudioEngine::sharedEngine()->playBackgroundMusic(szName , bLoop);

#endif
	CocosDenshion::SimpleAudioEngine::getInstance()->setBackgroundMusicVolume(g_GlobalUnits.m_fSoundValue);
	CocosDenshion::SimpleAudioEngine::getInstance()->playBackgroundMusic(szName , bLoop);
}

void SoundUtil::stopBackMusic(void)
{
	CocosDenshion::SimpleAudioEngine::getInstance()->stopBackgroundMusic();
}


void SoundUtil::setSoundVolume(float fv)
{
	CocosDenshion::SimpleAudioEngine::getInstance()->setEffectsVolume(fv);
}

void SoundUtil::setBackSoundVolume(float fv)
{
	CocosDenshion::SimpleAudioEngine::getInstance()->setBackgroundMusicVolume(fv);
}

SoundUtil * SoundUtil::sharedEngine(void)
{
	if (m_pSoundUtil == NULL)
	{
		m_pSoundUtil = new SoundUtil();
	}
	return m_pSoundUtil;
}

void SoundUtil::stopAllEffects()
{
	CocosDenshion::SimpleAudioEngine::getInstance()->stopAllEffects();
}

void SoundUtil::vibrate(int iTime)
{


}
