#ifndef _WZLZGAMEUSERLIST_H_
#define _WZLZGAMEUSERLIST_H_

#include "GameScene.h"
#include "cocos-ext.h"
#include "CCGameScrollView.h"

USING_NS_CC_EXT;

class WZLZGameSUserList : public GameLayer,public TableViewDataSource, public TableViewDelegate
{
	struct UserList
	{
		//玩家信息
		char							strUserName[128];						//玩家帐号
		LONGLONG						lUserScore;							//玩家金币
	};
public:
	virtual bool init();  
	static WZLZGameSUserList *create(GameScene *pGameScene);
	virtual ~WZLZGameSUserList();
	WZLZGameSUserList(GameScene *pGameScene);
	WZLZGameSUserList(const WZLZGameSUserList&);
	WZLZGameSUserList& operator = (const WZLZGameSUserList&);

private:
	virtual void onEnter();
	virtual void onExit();
	virtual void update (float deltaTime){}

	virtual bool onTouchBegan(Touch *pTouch, Event *pEvent);
	virtual void onTouchMoved(Touch *pTouch, Event *pEvent);
	virtual void onTouchEnded(Touch *pTouch, Event *pEvent);
	virtual void onTouchCancelled(Touch *pTouch, Event *pEvent);
	// TextField 触发
	virtual bool onTextFieldAttachWithIME(TextFieldTTF*pSender){return false;}
	virtual bool onTextFieldDetachWithIME(TextFieldTTF*pSender){return false;}
	virtual bool onTextFieldInsertText(TextFieldTTF *pSender, const char *text, int nLen){return true;}
	virtual bool onTextFieldDeleteBackward(TextFieldTTF *pSender, const char *delText, int nLen){return true;}

	//拖动
	virtual void scrollViewDidScroll(ScrollView* view){}
	virtual void scrollViewDidZoom(ScrollView* view){}
	virtual void tableCellTouched(TableView* table, TableViewCell* cell);
	virtual cocos2d::Size tableCellSizeForIndex(TableView *table, ssize_t idx);
	virtual TableViewCell* tableCellAtIndex(TableView *table, ssize_t idx);
	virtual ssize_t numberOfCellsInTableView(TableView *table);

	// IME 触发
	virtual void keyboardWillShow(IMEKeyboardNotificationInfo &info){}
	virtual void keyboardDidShow(IMEKeyboardNotificationInfo &info){}
	virtual void keyboardWillHide(IMEKeyboardNotificationInfo &info){}
	virtual void keyboardDidHide(IMEKeyboardNotificationInfo &info){}

	Menu* CreateButton( std::string szBtName ,const Vec2 &p , int tag );
	virtual void callbackBt(Ref *pSender);
	Label * createLabel(const char *szText ,const Vec2 &p, int fontSize ,Color3B color, bool bChinese=false);
	Label * createLabel(const std::string szText ,const Vec2 &p, int fontSize ,Color3B color, bool bChinese=false);
	void close();
	

public:
	void reloadData();
	void UpdataList();
	void onRemove();
public:
	__Array*		m_Arr;
	TableView*      m_tableView;
	vector<UserList>m_LineCount;
};

#endif
