﻿#include "WZLZGameLogic.h"

//////////////////////////////////////////////////////////////////////////

//扑克数据
const BYTE WZLZ_GameLogic::m_cbCardListData[CARD_COUNT]=
{
    0x0C,0x2C,			//方块Q, 红桃Q
    0x0B,0x2B,			//方块J, 红桃J
    0x0A,0x1A,0x2A,0x3A,//方块10, 梅花10, 红桃10, 黑桃10
    0x09,0x29,			//方块9, 红桃9
    0x07,0x17,0x27,0x37,//方块7, 梅花7, 红桃7, 黑桃7
    0x08,0x18,0x28,0x38,//方块8, 梅花8, 红桃8, 黑桃8
    0x06,0x16,0x26,0x36,//方块6, 梅花6, 红桃6, 黑桃6
    0x05,0x25,			//方块5, 红桃5
    0x04,0x14,0x24,0x34,//方块4, 梅花4, 红桃4, 黑桃4
    0x02,0x22,			//方块2, 红桃2
    0x31,				//黑桃A
    0x33				//黑桃3
};



//////////////////////////////////////////////////////////////////////////

//构造函数
WZLZ_GameLogic::WZLZ_GameLogic()
{
}

//析构函数
WZLZ_GameLogic::~WZLZ_GameLogic()
{
}

//混乱扑克
void WZLZ_GameLogic::RandCardList(BYTE cbCardBuffer[], BYTE cbBufferCount)
{
	//混乱准备
	BYTE cbCardData[CountArray(m_cbCardListData)];
	CopyMemory(cbCardData,m_cbCardListData,sizeof(m_cbCardListData));

	//混乱扑克
	BYTE cbRandCount=0,cbPosition=0;
	srand((unsigned)time(NULL)+rand()%33333333);
	do
	{
		cbPosition=rand()%(CountArray(cbCardData)-cbRandCount);
		cbCardBuffer[cbRandCount++]=cbCardData[cbPosition];
		cbCardData[cbPosition]=cbCardData[CountArray(cbCardData)-cbRandCount];
	} while (cbRandCount<cbBufferCount);

	return;
}

//获取牌型
BYTE WZLZ_GameLogic::GetCardType(const BYTE cbCardData[], BYTE cbCardCount)
{
	//合法判断
	ASSERT(2==cbCardCount);
	if (2!=cbCardCount) return CT_ERROR;

	//排序扑克
	BYTE cbCardDataSort[CARD_COUNT];
	CopyMemory(cbCardDataSort,cbCardData,sizeof(BYTE)*cbCardCount);
	SortCardList(cbCardDataSort,cbCardCount,ST_LOGIC);

    //获取点数
    BYTE cbFirstCardValue=GetCardValue(cbCardDataSort[0]);
    BYTE cbSecondCardValue=GetCardValue(cbCardDataSort[1]);
    
//    //获取花色
//    BYTE cbFistCardColor=GetCardColor(cbCardDataSort[0]);
//    BYTE cbSecondCardColor=GetCardColor(cbCardDataSort[1]);
    
    if (0x2C == cbCardDataSort[0] && 0x0C == cbCardDataSort[1]) return CT_SPECIAL_1;
    if (0x22 == cbCardDataSort[0] && 0x02 == cbCardDataSort[1]) return CT_SPECIAL_2;
    if (0x33 == cbCardDataSort[0] && 0x31 == cbCardDataSort[1]) return CT_SPECIAL_3;
    if (0x28 == cbCardDataSort[0] && 0x08 == cbCardDataSort[1]) return CT_SPECIAL_4;
    if (0x24 == cbCardDataSort[0] && 0x04 == cbCardDataSort[1]) return CT_SPECIAL_5;
    if (0x3A == cbCardDataSort[0] && 0x1A == cbCardDataSort[1]) return CT_SPECIAL_6;
    if (0x36 == cbCardDataSort[0] && 0x16 == cbCardDataSort[1]) return CT_SPECIAL_7;
    if (0x34 == cbCardDataSort[0] && 0x14 == cbCardDataSort[1]) return CT_SPECIAL_8;
    if (0x2B == cbCardDataSort[0] && 0x0B == cbCardDataSort[1]) return CT_SPECIAL_9;
    if (0x2A == cbCardDataSort[0] && 0x0A == cbCardDataSort[1]) return CT_SPECIAL_10;
    if (0x27 == cbCardDataSort[0] && 0x07 == cbCardDataSort[1]) return CT_SPECIAL_11;
    if (0x26 == cbCardDataSort[0] && 0x06 == cbCardDataSort[1]) return CT_SPECIAL_12;
    if (0x29 == cbCardDataSort[0] && 0x09 == cbCardDataSort[1]) return CT_SPECIAL_13;
    if (0x38 == cbCardDataSort[0] && 0x18 == cbCardDataSort[1]) return CT_SPECIAL_14;
    if (0x37 == cbCardDataSort[0] && 0x17 == cbCardDataSort[1]) return CT_SPECIAL_15;
    if (0x25 == cbCardDataSort[0] && 0x05 == cbCardDataSort[1]) return CT_SPECIAL_16;
    if ((12==cbFirstCardValue && 9==cbSecondCardValue) || (12==cbSecondCardValue && 9==cbFirstCardValue)) return CT_SPECIAL_17;
    if ((12==cbFirstCardValue && 8==cbSecondCardValue) || (12==cbSecondCardValue && 8==cbFirstCardValue)) return CT_SPECIAL_18;
    if ((2==cbFirstCardValue && 8==cbSecondCardValue) || (2==cbSecondCardValue && 8==cbFirstCardValue)) return CT_SPECIAL_19;
    
    //点数牌型
    return CT_POINT;
}

//大小比较
/*
cbNextCardData>cbFirstCardData  返回1
cbNextCardData<cbFirstCardData  返回-1
cbNextCardData==cbFirstCardData 返回0
*/

int WZLZ_GameLogic::CompareCard(const BYTE cbFirstCardData[], BYTE cbFirstCardCount,const BYTE cbNextCardData[], BYTE cbNextCardCount)
{
	//合法判断
	ASSERT(2==cbFirstCardCount && 2==cbNextCardCount);
	if (!(2==cbFirstCardCount && 2==cbNextCardCount)) return 0;

	//获取牌型
	BYTE cbFirstCardType=GetCardType(cbFirstCardData, cbFirstCardCount);
	BYTE cbNextCardType=GetCardType(cbNextCardData, cbNextCardCount);

	//牌型比较
	if (cbFirstCardType != cbNextCardType) 
	{
		if (cbNextCardType < cbFirstCardType) return 1;
		else return -1;
	}

	//特殊牌型判断
	if (CT_POINT!=cbFirstCardType)
	{
		if (cbFirstCardType==cbNextCardType)
			return 0;
		else if(cbFirstCardType>=CT_SPECIAL_6&&cbFirstCardType<=CT_SPECIAL_8&&cbNextCardType>=CT_SPECIAL_6&&cbFirstCardType<=CT_SPECIAL_8)
			return 0;
		else if(cbFirstCardType>=CT_SPECIAL_9&&cbFirstCardType<=CT_SPECIAL_12&&cbNextCardType>=CT_SPECIAL_9&&cbFirstCardType<=CT_SPECIAL_12)
			return 0;
		else if(cbFirstCardType>CT_SPECIAL_13&&cbFirstCardType<CT_SPECIAL_16&&cbNextCardType>CT_SPECIAL_13&&cbFirstCardType<CT_SPECIAL_16)
			return 0;
	}

	//获取点数
	BYTE cbFirstPip=GetCardListPip(cbFirstCardData, cbFirstCardCount);
	BYTE cbNextPip=GetCardListPip(cbNextCardData, cbNextCardCount);

	//点数比较
	if (cbFirstPip != cbNextPip)
	{
		if (cbNextPip > cbFirstPip) return 1;
		else return -1;
	}

	//零点判断
	if (0==cbFirstPip && 0==cbNextPip) return 0;

	//排序扑克
	BYTE cbFirstCardDataTmp[CARD_COUNT], cbNextCardDataTmp[CARD_COUNT];
	CopyMemory(cbFirstCardDataTmp,cbFirstCardData,sizeof(BYTE)*cbFirstCardCount);
	CopyMemory(cbNextCardDataTmp,cbNextCardData,sizeof(BYTE)*cbNextCardCount);
	SortCardList(cbFirstCardDataTmp,cbFirstCardCount,ST_LOGIC);
	SortCardList(cbNextCardDataTmp,cbNextCardCount,ST_LOGIC);

	//相等判断
	if (GetCardLogicValue(cbFirstCardDataTmp[0])==GetCardLogicValue(cbNextCardDataTmp[0])) return 0;
	else if (GetCardLogicValue(cbNextCardDataTmp[0]) > GetCardLogicValue(cbFirstCardDataTmp[0])) return 1;
	else return -1;

//	return 0;
}

//获取牌点
BYTE WZLZ_GameLogic::GetCardListPip(const BYTE cbCardData[], BYTE cbCardCount)
{
	//变量定义
	BYTE cbPipCount=0;

	//获取牌点
	BYTE cbCardValue=0;
	for (BYTE i=0;i<cbCardCount;i++)
	{
		cbCardValue=GetCardValue(cbCardData[i]);
		cbPipCount+=(1==cbCardValue ? 6 : cbCardValue);
	}

	return (cbPipCount%10);
}

//逻辑大小
BYTE WZLZ_GameLogic::GetCardLogicValue(BYTE cbCardData)
{
	//获取花色
	BYTE cbColor=GetCardColor(cbCardData);

	//获取数值
	BYTE cbValue=GetCardValue(cbCardData);

	//返回逻辑值
	if (12==cbValue && (0==cbColor || 2==cbColor)) return 8;

	if (2==cbValue && (0==cbColor || 2==cbColor)) return 7;

	if (8==cbValue && (0==cbColor || 2==cbColor)) return 6;

	if (4==cbValue && (0==cbColor || 2==cbColor)) return 5;

	if ((1==cbColor || 3==cbColor) && (10==cbValue || 6==cbValue || 4==cbValue)) return 4;

	if ((0==cbColor || 2==cbColor) && (10==cbValue || 6==cbValue || 7==cbValue)) return 3;
	if ((1==cbColor || 3==cbColor) && 11==cbValue) return 3;

	if ((1==cbColor || 3==cbColor) && (7==cbValue || 8==cbValue)) return 2;
	if ((0==cbColor || 2==cbColor) && (5==cbValue || 9==cbValue)) return 2;

	if (3==cbColor && (1==cbValue || 3==cbValue)) return 1;

	return 0;
}

//排列扑克
void WZLZ_GameLogic::SortCardList(BYTE cbCardData[], BYTE cbCardCount, BYTE cbSortType)
{
	//数目过虑
	if (cbCardCount==0) return;

	//转换数值
	BYTE cbSortValue[CARD_COUNT];
	if (ST_VALUE==cbSortType)
	{
		for (BYTE i=0;i<cbCardCount;i++) cbSortValue[i]=GetCardValue(cbCardData[i]);	
	}
	else 
	{
		for (BYTE i=0;i<cbCardCount;i++) cbSortValue[i]=GetCardLogicValue(cbCardData[i]);	
	}

	//排序操作
	bool bSorted=true;
	BYTE cbThreeCount,cbLast=cbCardCount-1;
	do
	{
		bSorted=true;
		for (BYTE i=0;i<cbLast;i++)
		{
			if ((cbSortValue[i]<cbSortValue[i+1])||
				((cbSortValue[i]==cbSortValue[i+1])&&(cbCardData[i]<cbCardData[i+1])))
			{
				//交换位置
				cbThreeCount=cbCardData[i];
				cbCardData[i]=cbCardData[i+1];
				cbCardData[i+1]=cbThreeCount;
				cbThreeCount=cbSortValue[i];
				cbSortValue[i]=cbSortValue[i+1];
				cbSortValue[i+1]=cbThreeCount;
				bSorted=false;
			}	
		}
		cbLast--;
	} while(bSorted==false);

	return;
}
