﻿#include "WZLZGameViewLayer.h"
#include "LobbyLayer.h"
#include "ClientSocketSink.h"
#include "HXmlParse.h"
#include "LocalDataUtil.h"
#include "JniSink.h"
#include "VisibleRect.h"
#include "HttpConstant.h"
#include "Screen.h"
#include "LobbySocketSink.h"
#include "LoginLayer.h"
#include "GameLayerMove.h"
#include "Convert.h"

#define Btn_Ready						1
#define Btn_BackToLobby					2
#define Btn_Seting						3
#define Btn_CallBank					4
#define Btn_QiangBank					5

#define Btn_1000						7
#define Btn_1w							8
#define Btn_10w							9
#define Btn_100w						10
#define Btn_500w						11

#define Btn_CancleBank					13
#define Btn_LZ							15
#define Btn_LzClose						16
#define Btn_BankBtn						19

WZLZGameViewLayer::WZLZGameViewLayer(GameScene *pGameScene):IGameView(pGameScene)
{
	//限制信息
	m_lMeMaxScore=0L;			
	m_lAreaLimitScore=0L;		
	m_lApplyBankerCondition=0L;
    m_nRobBanker = 0;
	for (int i = 0; i < AREA_COUNT; i++)
	{
		m_lUserJettonScore[i] = 0;
		m_lAllJettonScore[i]  = 0;
	}
	//个人下注
	ZeroMemory(m_lUserJettonScore,sizeof(m_lUserJettonScore));

	//庄家信息
	m_lBankerScore=0L;
	m_wCurrentBanker=0L;
	m_cbLeftCardCount=0;
	m_BankZhangJI = 0;
	m_BankJuShu = 0;

	//状态变量
	m_bBetSound = false;
	m_lPlayAllScore = 0;
	m_lBankerCurGameScore = 0;
	m_lMeStatisticScore = 0;
	m_bWinShunMen = false;
	m_bWinDuiMen = false;
	m_bWinDaoMen = false;
	m_lMeStatisticScore = 0;
	m_OpenBankList = false;
	m_StartTime = 0;
	m_bMeApplyBanker=false;
	m_bCanRobBanker = false;
	m_LookMode = false;
	m_BankListBeginIndex = 0;
	m_CurSelectGold = 0;
	m_SendCardCount = 0;
	m_SendCardOver = false;
	m_IsMeBank = false;
    m_LzShow = false;
    m_IsLayerMove = false;
	SetKindId(WZLZ_KIND_ID);
}

WZLZGameViewLayer::~WZLZGameViewLayer() 
{

}

WZLZGameViewLayer *WZLZGameViewLayer::create(GameScene *pGameScene)
{
    WZLZGameViewLayer *pLayer = new WZLZGameViewLayer(pGameScene);
    if(pLayer && pLayer->init())
    {
        pLayer->autorelease();
        return pLayer;
    }
    else
    {
        CC_SAFE_DELETE(pLayer);
        return NULL;
    }
}

void WZLZGameViewLayer::onEnter ()
{	

}

void WZLZGameViewLayer::onExit()
{
	Tools::removeSpriteFrameCache("WZLZ/wzlz_res.plist");
	Tools::removeSpriteFrameCache("Common/CardSprite2.plist");
	IGameView::onExit();
}

bool WZLZGameViewLayer::init()
{
	if ( !Layer::init() )
	{
		return false;
	}
	setLocalZOrder(3);

	Tools::addSpriteFrame("WZLZ/wzlz_res.plist");
	Tools::addSpriteFrame("Common/CardSprite2.plist");
	SoundUtil::sharedEngine()->playBackMusic("WZLZ/sound/BACK_GROUND", true);
	IGameView::onEnter();
	JniSink::share()->setIGameView(this);
	setGameStatus(GS_FREE);

	InitGame();
	AddButton();

	AddPlayerInfo();
    
	setTouchEnabled(true);
    auto listener = EventListenerTouchOneByOne::create();
    listener->onTouchBegan = CC_CALLBACK_2(WZLZGameViewLayer::onTouchBegan, this);
    listener->onTouchMoved = CC_CALLBACK_2(WZLZGameViewLayer::onTouchMoved, this);
    listener->onTouchEnded = CC_CALLBACK_2(WZLZGameViewLayer::onTouchEnded, this);
    Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(listener, this);
    
    return true;
}


void WZLZGameViewLayer::InitGame()
{
	//背景图
	m_BackSpr = Sprite::create("WZLZ/wzlz_back.jpg");
	m_BackSpr->setPosition(_STANDARD_SCREEN_CENTER_IN_PIXELS_);
	addChild(m_BackSpr);

    m_ClockSpr = Sprite::createWithSpriteFrameName("timeBack.png");
    m_ClockSpr->setPosition(Vec2(800, 965));
    m_ClockSpr->setVisible(false);
    addChild(m_ClockSpr);
    
    m_BankListBackSpr = Sprite::createWithSpriteFrameName("bg_tongyongkuang3.png");
	m_BankListBackSpr->setPosition(Vec2(960, 1500));
	addChild(m_BankListBackSpr,10000);

    Sprite* pBankTitle = Sprite::createWithSpriteFrameName("shangzhuangliebiao.png");
    pBankTitle->setScale(0.8f);
    pBankTitle->setPosition(Vec2(519,480));
    m_BankListBackSpr->addChild(pBankTitle);
    
    m_listView = ui::ListView::create();
    m_listView->setContentSize(Size(920, 380));
    m_listView->setDirection(ui::ScrollView::Direction::VERTICAL);
    m_listView->setPosition(Vec2(50,30));
    m_BankListBackSpr->addChild(m_listView);
    
    m_LzBack = Sprite::createWithSpriteFrameName("bg_tongyongkuang3.png");
    m_LzBack->setPosition(Vec2(_STANDARD_SCREEN_CENTER_.x, 1500));
    addChild(m_LzBack,10000);
    
    Sprite* lzTitle = Sprite::createWithSpriteFrameName("luzi.png");
    lzTitle->setScale(0.8f);
    lzTitle->setPosition(Vec2(519,480));
    m_LzBack->addChild(lzTitle);
    
    Label *shunmen = Label::createWithSystemFont("顺门", _GAME_FONT_NAME_1_, 46);
    shunmen->setPosition(Vec2(90, 320));
    shunmen->setColor(Color3B::BLACK);
    m_LzBack->addChild(shunmen);
 
    Label *daomen = Label::createWithSystemFont("倒门", _GAME_FONT_NAME_1_, 46);
    daomen->setPosition(Vec2(90, 210));
    daomen->setColor(Color3B::BLACK);
    m_LzBack->addChild(daomen);
    
    Label *duimen = Label::createWithSystemFont("对门", _GAME_FONT_NAME_1_, 46);
    duimen->setPosition(Vec2(90, 100));
    duimen->setColor(Color3B::BLACK);
    m_LzBack->addChild(duimen);
 
    //tga
    m_Head_Tga         = 99;
	m_NickName_Tga     = 100;
	m_Glod_Tga	       = 101;
	m_WinGlod_Tga      = 102;
	m_BankNickName_Tga = 103;
	m_BankGold_Tga     = 104;
	m_BankZhangJi_Tga  = 105;
	m_BankJuShu_Tga    = 106;
	m_JsBack_Tga	   = 107;
	m_JsXian_Tga	   = 108;
	m_JsBank_Tga	   = 109;
	m_CardBackIndex[0] = 110;
	m_CardBackIndex[1] = 111;
	m_CardBackIndex[2] = 112;
	m_CardBackIndex[3] = 113;
    
	m_BetScore_Tga[0]  = 114;
	m_BetScore_Tga[1]  = 115;
	m_BetScore_Tga[2]  = 116;
	m_BetScore_Tga[3]  = 117;
	m_BetScore_Tga[4]  = 118;
	m_BetScore_Tga[5]  = 119;
	
	m_BetScore1_Tga[0] = 121;
	m_BetScore1_Tga[1] = 123;
	m_BetScore1_Tga[2] = 124;
	m_BetScore1_Tga[3] = 125;
	m_BetScore1_Tga[4] = 126;
	m_BetScore1_Tga[5] = 127;
	
    m_TimeType_Tga     = 128;
	m_TimeIndex_Tga	   = 129;
	m_TableCard_Tga[0][0] = 130;
	m_TableCard_Tga[0][1] = 131;
	m_TableCard_Tga[1][0] = 132;
	m_TableCard_Tga[1][1] = 133;
	m_BtnAnim_Tga	   = 134;
	m_CardType_Tga	   = 135;
    m_bankBg_Tga    = 136;
    
	m_TableCard_Tga[2][0] = 151;
	m_TableCard_Tga[2][1] = 152;
	m_TableCard_Tga[3][0] = 153;
	m_TableCard_Tga[3][1] = 154;
	m_HandIndex		   = 155;
	m_AllScoreTga	   = 156;
    
	m_LightArea_Tga[0] = 157;
	m_LightArea_Tga[1] = 158;
	m_LightArea_Tga[2] = 159;
	m_LightArea_Tga[3] = 160;
	m_LightArea_Tga[4] = 161;
	m_LightArea_Tga[5] = 162;

	m_BankScoreTableTga= 165;
	m_ChangeBankTga    = 166;

	m_LzCell_Tga       = 1000;

	//pos
    m_BankNickNamePos  = Vec2(350,1050);
    m_BankGoldPos      = Vec2(350,1015);
    m_BankZhangJiPos   = Vec2(350,980);
    m_BankJuShuPos	   = Vec2(350,945);
    
    m_HeadPos          = Vec2(250,100);
    m_NickNamePos      = Vec2(150,115);
    m_GoldPos          = Vec2(150,75);
    m_WinGlodPos       = Vec2(150,35);
    
	m_TableBankScorePos= Vec2(1010,760);

	m_CardPos[0]	   = Vec2(900,870);
	m_CardPos[1]	   = Vec2(120,540);
	m_CardPos[2]	   = Vec2(900,160);
	m_CardPos[3]	   = Vec2(1620,540);

	m_CardTypePos[0]   = Vec2(980,910);
	m_CardTypePos[1]   = Vec2(200,580);
	m_CardTypePos[2]   = Vec2(980,200);
	m_CardTypePos[3]   = Vec2(1700,580);

    m_BetNumPos[ID_SHUN_MEN]     = Vec2(465, 600);
    m_BetNumPos[ID_JIAO_L]     = Vec2(465, 355);
    m_BetNumPos[ID_QIAO]     = Vec2(950, 720);
    m_BetNumPos[ID_DUI_MEN]     = Vec2(950, 355);
    m_BetNumPos[ID_DAO_MEN]     = Vec2(1440, 600);
    m_BetNumPos[ID_JIAO_R]     = Vec2(1440, 355);
    
    m_AreaSize[ID_SHUN_MEN] = Size(460, 350);
	m_AreaSize[ID_JIAO_L] = Size(460,230);
	m_AreaSize[ID_QIAO] = Size(475, 230);
	m_AreaSize[ID_DUI_MEN] = Size(475, 350);
	m_AreaSize[ID_DAO_MEN] = Size(460, 350);
	m_AreaSize[ID_JIAO_R] = Size(460,230);

    //点击区域
    m_AreaRect[ID_SHUN_MEN] = Rect(235, 540, 460, 350);
    m_AreaRect[ID_JIAO_L] = Rect(235, 295, 460,230);
    m_AreaRect[ID_QIAO] = Rect(715, 660, 475, 230);
    m_AreaRect[ID_DUI_MEN] = Rect(715, 295, 475, 350);
    m_AreaRect[ID_DAO_MEN] = Rect(1210, 540, 460, 350);
    m_AreaRect[ID_JIAO_R] = Rect(1210, 295, 460,230);
}

void WZLZGameViewLayer::AddButton()
{
	m_BtnBackToLobby = CreateButton("Returnback" ,Vec2(90,1000),Btn_BackToLobby);
	addChild(m_BtnBackToLobby , 0 , Btn_BackToLobby);

	m_BtnSeting = CreateButton("seting" ,Vec2(1830,1000),Btn_Seting);
	addChild(m_BtnSeting , 0 , Btn_Seting);

	m_BtnCallBank = CreateButton("BtnCallBank" ,Vec2(1600,1000),Btn_CallBank);
	addChild(m_BtnCallBank , 0 , Btn_CallBank);

	m_BtnQiangBank = CreateButton("BtnQiangBank" ,Vec2(1300,1000),Btn_QiangBank);
	addChild(m_BtnQiangBank , 0 , Btn_QiangBank);
    m_BtnQiangBank->setVisible(false);

	m_BtnCancelBank = CreateButton("BtnCancleBank" ,Vec2(1600,1000),Btn_CancleBank);
	addChild(m_BtnCancelBank , 0 , Btn_CancleBank);
	m_BtnCancelBank->setVisible(false);

	m_Gold1000 = CreateButton("chip_1000" ,Vec2(610 ,72),Btn_1000);
	addChild(m_Gold1000 , 0 , Btn_1000);

	m_Gold1w = CreateButton("chip_1w" ,Vec2(770 ,72),Btn_1w);
	addChild(m_Gold1w , 0 , Btn_1w);

	m_Gold10w = CreateButton("chip_10w" ,Vec2(930 ,72),Btn_10w);
	addChild(m_Gold10w , 0 , Btn_10w);

	m_Gold100w = CreateButton("chip_100w" ,Vec2(1090 ,72),Btn_100w);
	addChild(m_Gold100w , 0 , Btn_100w);

	m_Gold500w = CreateButton("chip_500w" ,Vec2(1250 ,72),Btn_500w);
	addChild(m_Gold500w , 0 , Btn_500w);

	m_BankBtn = CreateButton("bankBtn" ,Vec2(1750,70),Btn_BankBtn);
	addChild(m_BankBtn , 0 , Btn_BankBtn);

    m_BtnLZBtn = CreateButton("lz_btn" ,Vec2(1480,70),Btn_LZ);
    addChild(m_BtnLZBtn , 0 , Btn_LZ);
}


//设置庄家
void WZLZGameViewLayer::SetBankerInfo(WORD wBanker,LONGLONG lScore)
{
    removeAllChildByTag(m_BankNickName_Tga);
    removeAllChildByTag(m_BankGold_Tga);
    removeAllChildByTag(m_BankZhangJi_Tga);
    removeAllChildByTag(m_BankJuShu_Tga);
    removeAllChildByTag(m_bankBg_Tga);
    
    m_wCurrentBanker=wBanker;
	m_lBankerScore=lScore;
    ui::Scale9Sprite* playerInfoBg = ui::Scale9Sprite::createWithSpriteFrameName("bg_player_info.png");
    playerInfoBg->setContentSize(Size(520, 150));
    playerInfoBg->setPosition(Vec2(450, 1000));
    addChild(playerInfoBg, 0, m_bankBg_Tga);
    
    char name[256];
    int tableid = ClientSocketSink::sharedSocketSink()->GetMeUserData()->wTableID;
    tagUserData* pUserData = ClientSocketSink::sharedSocketSink()->m_UserManager.GetUserData(tableid,m_wCurrentBanker);
    if(m_wCurrentBanker == INVALID_CHAIR || pUserData == NULL)
    {
        m_BankZhangJI = 0;
        m_lBankerScore = 0;
        m_BankJuShu = 0;
        
        Label* temp = Label::createWithSystemFont("无人坐庄", _GAME_FONT_NAME_1_,46);
        temp->setColor(Color3B::RED);
        temp->setTag(m_BankNickName_Tga);
        temp->setPosition(Vec2(450, 1000));
        addChild(temp);
        return;
    }
    else
    {
        m_lBankerScore=lScore;
        sprintf(name, "%s", gbk_utf8(pUserData->szNickName).c_str());
    }
    
    string heads = g_GlobalUnits.getFace(pUserData->wGender, pUserData->lScore);
    Sprite* mHead = Sprite::createWithSpriteFrameName(heads);
    mHead->setPosition(Vec2(75, 75));
    mHead->setScale(0.9f);
    playerInfoBg->addChild(mHead);
    
    //昵称
    std::string szTempTitle;
    szTempTitle = "昵称: ";
    szTempTitle.append(name);
    
    Label* temp = Label::createWithSystemFont(szTempTitle.c_str(),_GAME_FONT_NAME_1_,30);
    temp->setAnchorPoint(Vec2(0,0.5f));
    temp->setTag(m_BankNickName_Tga);
    temp->setPosition(m_BankNickNamePos);
    addChild(temp);
    
    //金币
    char strc[32]="";
    Tools::AddComma(m_lBankerScore , strc);
    szTempTitle = "酒吧豆: ";
    szTempTitle.append(strc);
    temp = Label::createWithSystemFont(szTempTitle, _GAME_FONT_NAME_1_, 30);
    temp->setAnchorPoint(Vec2(0,0.5f));
    temp->setTag(m_BankGold_Tga);
    temp->setColor(_GAME_FONT_COLOR_4_);
    temp->setPosition(m_BankGoldPos);
    addChild(temp);
    
    //战绩
    Tools::AddComma(m_BankZhangJI , strc);
    szTempTitle = "战绩: ";
    szTempTitle.append(strc);
    temp = Label::createWithSystemFont(szTempTitle, _GAME_FONT_NAME_1_, 30);
    temp->setAnchorPoint(Vec2(0,0.5f));
    temp->setTag(m_BankZhangJi_Tga);
    temp->setColor(_GAME_FONT_COLOR_4_);
    temp->setPosition(m_BankZhangJiPos);
    addChild(temp);
    
    //局数
    sprintf(strc,"%d",m_BankJuShu);
    szTempTitle = "局数: ";
    szTempTitle.append(strc);
    temp = Label::createWithSystemFont(szTempTitle, _GAME_FONT_NAME_1_, 30);
    temp->setAnchorPoint(Vec2(0,0.5f));
    temp->setTag(m_BankJuShu_Tga);
    temp->setColor(_GAME_FONT_COLOR_4_);
    temp->setPosition(m_BankJuShuPos);
    addChild(temp);
}

// Touch 触发
bool WZLZGameViewLayer::onTouchBegan(Touch *pTouch, Event *pEvent)
{
	if(!isVisible()) return true;
    if (m_OpenBankList)
    {
        CloseBankList();
        return false;
    }
    else if(m_LzShow)
    {
        CloseLzList();
        return false;
    }
    if ( GetMeChairID() == m_wCurrentBanker ) return true;
	Vec2 touchLocation = pTouch->getLocation(); // 返回GL坐标
	Vec2 localPos = convertToNodeSpace(touchLocation);
	if (getGameStatus() != GS_PLACE_JETTON) return true;
	for (int i = 0; i < AREA_COUNT; i++)
	{
		bool isTouched = m_AreaRect[i].containsPoint(localPos);
		if (isTouched)
		{
			if (m_CurSelectGold == 0) return true;
			SoundUtil::sharedEngine()->playEffect("buttonMusic");
			//变量定义
			CMD_C_WZLZ_PlaceJetton PlaceJetton;
			ZeroMemory(&PlaceJetton,sizeof(PlaceJetton));

			//构造变量
			PlaceJetton.cbJettonArea= i + 1;
			PlaceJetton.lJettonScore=m_CurSelectGold;
			if (PlaceJetton.lJettonScore >=1000000) SoundUtil::sharedEngine()->playEffect("BR30S/ADD_GOLD_EX");
			else SoundUtil::sharedEngine()->playEffect("BR30S/bet");
			SendData(SUB_C_WZLZ_PLACE_JETTON,&PlaceJetton,sizeof(PlaceJetton));
			return true;
		}
	}

	return true;
}

//申请消息
void WZLZGameViewLayer::OnApplyBanker(int wParam)
{
	//合法判断
	const tagUserData * pClientUserItem = GetMeUserData();
	//旁观判断
	if (IsLookonMode()) return;

	if (wParam == 1)
	{
		if (pClientUserItem->lScore < m_lApplyBankerCondition)
		{
			char TipMessage[128] = {0};
			sprintf(TipMessage, "\u60a8\u7684\u6b22\u4e50\u8c46\u4e0d\u8db3\uff0c\u65e0\u6cd5\u4e0a\u5e84\uff0c\u4e0a\u5e84\u6761\u4ef6\u4e3a\uff1a%lld\u6b22\u4e50\u8c46",m_lApplyBankerCondition);
			AlertMessageLayer::createConfirm(TipMessage);
			return;
		}
		//发送消息
		SendData(SUB_C_WZLZ_APPLY_BANKER, NULL, 0);
	}
	else
	{
		//发送消息
		SendData(SUB_C_WZLZ_CANCEL_BANKER, NULL, 0);
	}

	//设置按钮
	UpdateButtonContron();
}

void WZLZGameViewLayer::callbackBt(Ref *pSender )
{
	Node *pNode = (Node *)pSender;
	switch (pNode->getTag())
	{
	case Btn_BackToLobby: // 返回大厅按钮
		{
			SoundUtil::sharedEngine()->playEffect("buttonMusic");
			BackToLobby();
			break;
		}
	case Btn_Seting: //设置
		{
			SoundUtil::sharedEngine()->playEffect("buttonMusic");
			GameLayerMove::sharedGameLayerMoveSink()->OpenSeting();
			break;
		}
	case Btn_CallBank: //申请上庄
		{
			SoundUtil::sharedEngine()->playEffect("buttonMusic");
			OnApplyBanker(1);
			break;
		}
	case Btn_CancleBank: //取消上庄
		{
			SoundUtil::sharedEngine()->playEffect("buttonMusic");
			OnApplyBanker(0);
			break;
		}
	case Btn_QiangBank:
		{
			SoundUtil::sharedEngine()->playEffect("buttonMusic");
			OnQiangBanker();
			break;
		}
	case Btn_BankBtn:
		{
			SoundUtil::sharedEngine()->playEffect("buttonMusic");
			if (m_OpenBankList == true)
                CloseBankList();
			else
                OpenBankList();
			break;;
		}
        case Btn_LZ:
        {
            SoundUtil::sharedEngine()->playEffect("buttonMusic");
            if (m_LzShow == false)
            {
                OpenLzList();
            }
            else
            {
                CloseLzList();
            }
            break;
        }
	case Btn_1000:
		{
			Select1000();
			break;
		}
	case Btn_1w:
		{
			Select1w();
			break;
		}
	case Btn_10w:
		{
			Select10w();
			break;
		}
	case Btn_100w:
		{
			Select100w();
			break;
		}
	case Btn_500w:
		{
			Select500w();
			break;
		}
	}
}

void WZLZGameViewLayer::OnQiangBanker()
{
	const tagUserData *pMeUserData = GetMeUserData();
	if (pMeUserData->lScore < m_lApplyBankerCondition + 3000000)
	{
		char TipMessage[128] = {0};
		sprintf(TipMessage, "\u60a8\u7684\u9152\u5427\u8c46\u4e0d\u8db3\uff0c\u65e0\u6cd5\u62a2\u5e84\uff0c\u62a2\u5e84\u6761\u4ef6\u4e3a\uff1a%lld\u9152\u5427\u8c46",m_lApplyBankerCondition + 3000000);
		AlertMessageLayer::createConfirm(TipMessage);
		return;
	}

	if(IsLookonMode()) return;

	//当前判断
	if (m_wCurrentBanker == GetMeChairID()) return;

	SendData(SUB_C_WZLZ_GRAB_BANKER, NULL, 0);

	//设置按钮
	UpdateButtonContron();
}

void WZLZGameViewLayer::Select1000()
{
	m_CurSelectGold = 1000;
	SoundUtil::sharedEngine()->playEffect("buttonMusic");
	removeAllChildByTag(m_BtnAnim_Tga);
	Sprite *temp = Sprite::createWithSpriteFrameName("agoldselect.png");
	temp->setTag(m_BtnAnim_Tga);
	temp->setPosition(Vec2(610,72));
	addChild(temp);
}
void WZLZGameViewLayer::Select1w()
{
	m_CurSelectGold = 10000;
	SoundUtil::sharedEngine()->playEffect("buttonMusic");
	removeAllChildByTag(m_BtnAnim_Tga);
	Sprite *temp = Sprite::createWithSpriteFrameName("agoldselect.png");
	temp->setTag(m_BtnAnim_Tga);
	temp->setPosition(Vec2(770,72));
	addChild(temp);
}
void WZLZGameViewLayer::Select10w()
{
	m_CurSelectGold = 100000;
	SoundUtil::sharedEngine()->playEffect("buttonMusic");
	removeAllChildByTag(m_BtnAnim_Tga);
	Sprite *temp = Sprite::createWithSpriteFrameName("agoldselect.png");
	temp->setTag(m_BtnAnim_Tga);
	temp->setPosition(Vec2(930,72));
	addChild(temp);
}
void WZLZGameViewLayer::Select100w()
{
	m_CurSelectGold = 1000000;
	SoundUtil::sharedEngine()->playEffect("buttonMusic");
	removeAllChildByTag(m_BtnAnim_Tga);
	Sprite *temp = Sprite::createWithSpriteFrameName("agoldselect.png");
	temp->setTag(m_BtnAnim_Tga);
	temp->setPosition(Vec2(1090,72));
	addChild(temp);
}
void WZLZGameViewLayer::Select500w()
{
	m_CurSelectGold = 5000000;
	SoundUtil::sharedEngine()->playEffect("buttonMusic");
	removeAllChildByTag(m_BtnAnim_Tga);
	Sprite *temp = Sprite::createWithSpriteFrameName("agoldselect.png");
	temp->setTag(m_BtnAnim_Tga);
	temp->setPosition(Vec2(1250,72));
	addChild(temp);
}

void WZLZGameViewLayer::AddPlayerInfo()
{
    if (ClientSocketSink::sharedSocketSink()->GetMeUserData() == NULL) return;
    int userid = (int)ClientSocketSink::sharedSocketSink()->GetMeUserData()->dwUserID;
    const tagUserData* pUserData = ClientSocketSink::sharedSocketSink()->m_UserManager.SearchUserByUserID(userid);
    if (pUserData == NULL) return;
    
    removeAllChildByTag(m_Head_Tga);
    //头像
    ui::Scale9Sprite* playerInfoBg = ui::Scale9Sprite::createWithSpriteFrameName("bg_player_info.png");
    playerInfoBg->setContentSize(Size(450, 150));
    playerInfoBg->setPosition(Vec2(m_HeadPos.x, m_HeadPos.y+5));
    addChild(playerInfoBg, 0, m_Head_Tga);
    
    string heads = g_GlobalUnits.getFace(pUserData->wGender, pUserData->lScore);
    Sprite* mHead = Sprite::createWithSpriteFrameName(heads);
    mHead->setPosition(Vec2(75, 75));
    mHead->setScale(0.9f);
    playerInfoBg->addChild(mHead);
    
    removeAllChildByTag(m_Glod_Tga);
    char strc[32]="";
    LONGLONG lMon = pUserData->lScore;
    memset(strc , 0 , sizeof(strc));
    Tools::AddComma(lMon , strc);
    
    string goldstr = "酒吧豆: ";
    goldstr.append(strc);
    Label* mGoldFont;
    mGoldFont = Label::createWithSystemFont(goldstr,_GAME_FONT_NAME_1_,30);
    mGoldFont->setTag(m_Glod_Tga);
    playerInfoBg->addChild(mGoldFont);
    mGoldFont->setColor(Color3B::YELLOW);
    mGoldFont->setAnchorPoint(Vec2(0, 0.5f));
    mGoldFont->setPosition(m_GoldPos);
    
    //昵称
    removeAllChildByTag(m_NickName_Tga);
    string namestr = "昵称: ";
    namestr.append(gbk_utf8(pUserData->szNickName));
    Label* mNickfont = Label::createWithSystemFont(namestr, _GAME_FONT_NAME_1_, 30);
    mNickfont->setTag(m_NickName_Tga);
    playerInfoBg->addChild(mNickfont);
    mNickfont->setAnchorPoint(Vec2(0,0.5f));
    mNickfont->setPosition(m_NickNamePos);
    
    //输赢金币
    removeAllChildByTag(m_WinGlod_Tga);
    Tools::AddComma(m_lMeStatisticScore, strc);
    string winStr = "成绩: ";
    winStr.append(strc);
    mGoldFont = Label::createWithSystemFont(winStr,_GAME_FONT_NAME_1_,30);
    mGoldFont->setAnchorPoint(Vec2(0,0.5f));
    mGoldFont->setTag(m_WinGlod_Tga);
    mGoldFont->setPosition(m_WinGlodPos);
    playerInfoBg->addChild(mGoldFont);
}

string WZLZGameViewLayer::AddCommaToNum(LONG Num)
{
	char _str[256];
	sprintf(_str,"%ld", Num);
	string _string = _str;
	int step = (int)_string.length()/3;
	for (int i = 1; i <= step; i++)
	{
		_string.insert(_string.length()-(i-1)-(i*3), ",");
	}
	return _string;
}


void WZLZGameViewLayer::StartTime(int _time, int _type)
{
	StopTime();
	m_StartTime = _time;
	schedule(schedule_selector(WZLZGameViewLayer::UpdateTime), 1);
    m_ClockSpr->setVisible(true);
	UpdateTime(m_StartTime);
    
    removeAllChildByTag(m_TimeType_Tga);
    std::string szTempTitle;
    if (_type == 0)
        szTempTitle = "time_free.png";//空闲时间
    else if(_type == 1)
        szTempTitle = "time_jetton.png";//下注时间
    else if(_type == 2)
        szTempTitle = "time_score.png";//开牌时间
    
    Sprite* temp = Sprite::createWithSpriteFrameName(szTempTitle.c_str());
    temp->setPosition(Vec2(800, 1040));
    temp->setTag(m_TimeType_Tga);
    addChild(temp);
}

void WZLZGameViewLayer::StopTime()
{
	unschedule(schedule_selector(WZLZGameViewLayer::UpdateTime));
    m_ClockSpr->removeAllChildren();
    m_ClockSpr->setVisible(false);
}

void WZLZGameViewLayer::UpdateTime(float fp)
{
	if (m_StartTime < 0)
	{
		return;
	}

    m_ClockSpr->removeAllChildren();
    auto child = getChildByTag(10000);
    if (child)
        child->removeFromParent();
    std::string str = StringUtils::toString(m_StartTime);
    if (m_StartTime < 10)
        str = "0" + str;
    LabelAtlas *time = LabelAtlas::create(str, "Common/time_1.png", 23, 35, '+');
    time->setPosition(Vec2(20, 25));
    time->setTag(10000);
    m_ClockSpr->addChild(time);
    m_StartTime--;
}

void WZLZGameViewLayer::SetMePlaceJetton(int chair, LONGLONG score)
{
    if (score <= 0)
        return;
	removeAllChildByTag(m_BetScore1_Tga[chair]);
    
    string scoreStr = StringUtils::toString(score);
    LabelAtlas *scoreL = LabelAtlas::create(scoreStr, "Common/gold_self.png", 19, 24, '+');
    scoreL->setAnchorPoint(Vec2(0.5f, 0.5f));
    scoreL->setTag(m_BetScore1_Tga[chair]);
    scoreL->setPosition(m_BetNumPos[chair]);
    addChild(scoreL);
}


void WZLZGameViewLayer::SetEndScore(int chair, LONGLONG score)
{
    if (score <= 0)
        return;
    
	removeAllChildByTag(m_BetScore_Tga[chair]);
    string scoreStr = StringUtils::toString(score);
    LabelAtlas *scoreL = LabelAtlas::create(scoreStr, "Common/xiazhushuzi.png", 34, 46, '+');
    scoreL->setAnchorPoint(Vec2(0.5f, 0.5f));
    scoreL->setTag(m_BetScore_Tga[chair]);
    scoreL->setPosition(m_BetNumPos[chair] + Vec2(0, 100));
    addChild(scoreL);
}

bool WZLZGameViewLayer::OnSubGameFree(const void * pBuffer, WORD wDataSize)
{
	//效验数据
	ASSERT(wDataSize==sizeof(CMD_S_WZLZ_GameFree));
	if (wDataSize!=sizeof(CMD_S_WZLZ_GameFree)) return false;
	//消息处理
    
	CMD_S_WZLZ_GameFree * pGameFree=(CMD_S_WZLZ_GameFree *)pBuffer;
    m_GameState = enGameNormal;
    
	//自己是庄家
	if (m_IsMeBank)
	{
		m_BtnCancelBank->setVisible(true);
		m_BtnCancelBank->setEnabled(true);
		m_BtnCancelBank->setColor(Color3B(255,255,255));
	}
    
    if (m_nRobBanker < 3 && m_bCanRobBanker && false == m_IsMeBank)
    {
        m_BtnQiangBank->setEnabled(true);
        m_BtnQiangBank->setColor(Color3B::WHITE);
    }
    else
    {
        m_BtnQiangBank->setEnabled(false);
        m_BtnQiangBank->setColor(Color3B(100, 100, 100));
    }
    
    //清理桌面
    removeAllChildByTag(m_JsBack_Tga);
	for (int i = 0; i < AREA_COUNT; i++)
	{
		removeAllChildByTag(m_LightArea_Tga[i]);
		removeAllChildByTag(m_BetScore_Tga[i]); 
		removeAllChildByTag(m_BetScore1_Tga[i]); 
		m_lUserJettonScore[i] = 0;
		m_lAllJettonScore[i] = 0;
	}
	for (int i = 0; i < 4; i++)
	{
		removeAllChildByTag(m_TableCard_Tga[i][0]);
		removeAllChildByTag(m_TableCard_Tga[i][1]);
	}
	removeAllChildByTag(m_CardType_Tga);
	removeAllChildByTag(m_AllScoreTga);
	ZeroMemory(m_cbTableCardArray,sizeof(m_cbTableCardArray));

	m_CurSelectGold = 0;

	UpdateButtonContron();
    
	StartTime(pGameFree->cbTimeLeave,0);

	return true;
}


void WZLZGameViewLayer::DrawEndScore()
{
	Sprite* TempSpr = Sprite::createWithSpriteFrameName("bg_tongyongkuang3.png");
	TempSpr->setPosition(_STANDARD_SCREEN_CENTER_);
	TempSpr->setTag(m_JsBack_Tga);
	addChild(TempSpr,1000);
    
    Sprite* title = Sprite::createWithSpriteFrameName("gamover.png");
    title->setPosition(Vec2(519,480));
    title->setScale(0.8f);
    TempSpr->addChild(title);
    
    Sprite* line = Sprite::createWithSpriteFrameName("label_line.png");
    line->setPosition(Vec2(519, 220));
    TempSpr->addChild(line);
    
    Label* selfLabel = Label::createWithSystemFont("本家", _GAME_FONT_NAME_1_, 50);
    selfLabel->setColor(Color3B::BLACK);
    selfLabel->setPosition(Vec2(200, 310));
    TempSpr->addChild(selfLabel);
    
    Label* otherLabel = Label::createWithSystemFont("庄家", _GAME_FONT_NAME_1_, 50);
    otherLabel->setColor(Color3B::BLACK);
    otherLabel->setPosition(Vec2(200, 130));
    TempSpr->addChild(otherLabel);
    
    TempSpr->removeChildByTag(m_JsBank_Tga);
    char _score[32];
    if (m_lBankerCurGameScore > 0)
        sprintf(_score,"+%lld", m_lBankerCurGameScore);
    else
        sprintf(_score,"%lld", m_lBankerCurGameScore);
    LabelAtlas *bankScore = LabelAtlas::create(_score, "Common/gold_result.png", 45, 62, '+');
    bankScore->setPosition(Vec2(400, 100));
    TempSpr->addChild(bankScore);
    
    TempSpr->removeChildByTag(m_JsXian_Tga);
    if (m_lPlayAllScore > 0)
        sprintf(_score,"+%lld", m_lPlayAllScore);
    else
        sprintf(_score,"%lld", m_lPlayAllScore);
    LabelAtlas *playerScore = LabelAtlas::create(_score, "Common/gold_result.png", 45, 62, '+');
    playerScore->setPosition(Vec2(400, 280));
    TempSpr->addChild(playerScore);
}


//游戏开始
bool WZLZGameViewLayer::OnSubGameStart(const void * pBuffer, WORD wDataSize)
{
	//效验数据
	ASSERT(wDataSize==sizeof(CMD_S_WZLZ_GameStart));
	if (wDataSize!=sizeof(CMD_S_WZLZ_GameStart)) return false;
    m_GameState = enGameBet;
    
	if (getChildByTag(m_ChangeBankTga) != NULL)
	{
		removeChildByTag(m_ChangeBankTga);
	}

	//消息处理
	CMD_S_WZLZ_GameStart * pGameStart=(CMD_S_WZLZ_GameStart *)pBuffer;

	//庄家信息
	SetBankerInfo(pGameStart->wBankerUser,pGameStart->lBankerScore);

	//玩家信息
	m_lMeMaxScore=pGameStart->lUserMaxScore;

	//设置时间
	StartTime(pGameStart->cbTimeLeave,1);

	//设置状态
	setGameStatus(GS_PLACE_JETTON);

	//自己是庄家
	if (m_IsMeBank)
	{
		m_BtnCancelBank->setVisible(true);
		m_BtnCancelBank->setEnabled(false);
		m_BtnCancelBank->setColor(Color3B(100,100,100));
	}
    m_BtnQiangBank->setEnabled(false);
    m_BtnQiangBank->setColor(Color3B(100,100,100));
    
	//更新控制
	UpdateButtonContron();

	SoundUtil::sharedEngine()->playEffect("WZLZ/sound/GAME_START"); 

	return true;
}


//用户加注
bool WZLZGameViewLayer::OnSubPlaceJetton(const void * pBuffer, WORD wDataSize,bool bGameMes)
{
	//效验数据
	ASSERT(wDataSize==sizeof(CMD_S_WZLZ_PlaceJetton));
	if (wDataSize!=sizeof(CMD_S_WZLZ_PlaceJetton)) return false;
	CMD_S_WZLZ_PlaceJetton * pPlaceJetton=(CMD_S_WZLZ_PlaceJetton *)pBuffer;
 
    if(pPlaceJetton->cbJettonArea > AREA_COUNT) return true;
    int areaIndex = pPlaceJetton->cbJettonArea - 1;
    
    m_lAllJettonScore[areaIndex] += pPlaceJetton->lJettonScore;
    SetEndScore(areaIndex, m_lAllJettonScore[areaIndex]);
    m_GameState = enGameBet;
	if (GetMeChairID()==pPlaceJetton->wChairID)
	{
		m_lUserJettonScore[areaIndex] += pPlaceJetton->lJettonScore;
		SetMePlaceJetton(areaIndex, m_lUserJettonScore[areaIndex]);
        UpdateButtonContron();
	}

	if (pPlaceJetton->lJettonScore >=1000000) 
	{
		SoundUtil::sharedEngine()->playEffect("WZLZ/sound/ADD_GOLD_EX"); 
	}
	else 
	{
		if (m_bBetSound == false)
		{
			m_bBetSound = true;
			SoundUtil::sharedEngine()->playEffect("WZLZ/sound/ADD_GOLD"); 
			schedule(schedule_selector(WZLZGameViewLayer::BetSound), 0.2f);
		}
	}

	return true;
}

void WZLZGameViewLayer::BetSound(float fp)
{
	unschedule(schedule_selector(WZLZGameViewLayer::BetSound));
	m_bBetSound = false;
}

//申请做庄
bool WZLZGameViewLayer::OnSubUserApplyBanker(const void * pBuffer, WORD wDataSize)
{
	//效验数据
	ASSERT(wDataSize==sizeof(CMD_S_WZLZ_ApplyBanker));
	if (wDataSize!=sizeof(CMD_S_WZLZ_ApplyBanker)) return false;
	CMD_S_WZLZ_ApplyBanker * pApplyBanker=(CMD_S_WZLZ_ApplyBanker *)pBuffer;
    m_nRobBanker = pApplyBanker->nRobBanker;
	const tagUserData * pClientUserItem = GetUserData(pApplyBanker->wApplyUser);
    
	//插入玩家
    if (m_wCurrentBanker!=pApplyBanker->wApplyUser)
    {
        tagApplyUser ApplyUser;
        sprintf(ApplyUser.strUserName,"%s", pClientUserItem->szNickName);
        ApplyUser.lUserScore=pClientUserItem->lScore;
        ApplyUser.wGender = pClientUserItem->wGender;
        ApplyUser.wUserID = pClientUserItem->dwUserID;
        m_BankUserList.push_back(ApplyUser);
    }
    
    tagUserData *pUserData = ClientSocketSink::sharedSocketSink()->GetMeUserData();
    if (nullptr != pUserData && m_BankUserList.size() > 2)
    {
        if (m_BankUserList[0].wUserID == pUserData->dwUserID || m_BankUserList[1].wUserID == pUserData->dwUserID)
        {
            m_bCanRobBanker = false;
        }
        else
        {
            m_bCanRobBanker = true;
        }
    }
    else
         m_bCanRobBanker = false;
   
    
    //自己判断
	if (IsLookonMode()==false && GetMeChairID() == pApplyBanker->wApplyUser)
	{
		m_BtnCallBank->setVisible(false);
		m_BtnQiangBank->setVisible(true);
		m_BtnCancelBank->setVisible(true);
        
        if (enGameNormal == m_GameState && m_nRobBanker < 3 && m_bCanRobBanker)
        {
            m_BtnQiangBank->setEnabled(true);
            m_BtnQiangBank->setColor(Color3B::WHITE);
        }
        else
        {
            m_BtnQiangBank->setEnabled(false);
            m_BtnQiangBank->setColor(Color3B(100, 100, 100));
        }
	}

	//更新控件
	DrawBankList();
	UpdateButtonContron();

	return true;
}


bool WZLZGameViewLayer::OnSubQiangBanker(const void * pBuffer, WORD wDataSize)
{
	//效验数据
	ASSERT(wDataSize==sizeof(CMD_S_WZLZ_ApplyBanker));
	if (wDataSize!=sizeof(CMD_S_WZLZ_ApplyBanker)) return false;
	CMD_S_WZLZ_ApplyBanker * pApplyBanker=(CMD_S_WZLZ_ApplyBanker *)pBuffer;
    m_nRobBanker = pApplyBanker->nRobBanker;
    
	//插入玩家
    int tableid = ClientSocketSink::sharedSocketSink()->GetMeUserData()->wTableID;
    tagUserData* pUserData = ClientSocketSink::sharedSocketSink()->m_UserManager.GetUserData(tableid, pApplyBanker->wApplyUser);
    
    int i = 0;
    for (i = 0; i < m_BankUserList.size(); i++)
    {
        if (pUserData->dwUserID == m_BankUserList[i].wUserID)
            break;
    }
    
    swap(m_BankUserList[m_nRobBanker], m_BankUserList[i]);
    
    tagUserData *pMyData = ClientSocketSink::sharedSocketSink()->GetMeUserData();
    if (nullptr != pMyData && m_BankUserList.size() > 2)
    {
        if (m_BankUserList[0].wUserID == pMyData->dwUserID || m_BankUserList[1].wUserID == pMyData->dwUserID)
        {
            m_bCanRobBanker = false;
        }
        else
        {
            m_bCanRobBanker = true;
        }
    }
    else
        m_bCanRobBanker = false;
    
    //自己判断
    if (IsLookonMode()==false && GetMeChairID()==pApplyBanker->wApplyUser)
	{
		m_BtnCallBank->setVisible(false);
		m_BtnQiangBank->setVisible(false);
		m_BtnCancelBank->setVisible(true);
        m_bCanRobBanker = false;
	}
	//更新控件
	UpdateButtonContron();
    DrawBankList();
    
	return true;
}


//取消做庄
bool WZLZGameViewLayer::OnSubUserCancelBanker(const void * pBuffer, WORD wDataSize)
{
	//效验数据
	ASSERT(wDataSize==sizeof(CMD_S_WZLZ_CancelBanker));
	if (wDataSize!=sizeof(CMD_S_WZLZ_CancelBanker)) return false;
	CMD_S_WZLZ_CancelBanker * pCancelBanker=(CMD_S_WZLZ_CancelBanker *)pBuffer;
    m_nRobBanker = pCancelBanker->nRobBanker;
    
	//删除玩家
	tagApplyUser ApplyUser;
	sprintf(ApplyUser.strUserName,"%s", pCancelBanker->szCancelUser);
	ApplyUser.lUserScore=0;
	DeleteBankList(ApplyUser);

	//自己判断
	const tagUserData * pMeUserData= GetMeUserData();
    if (IsLookonMode() == false && strcmp(pMeUserData->szNickName, pCancelBanker->szCancelUser) == 0)
	{
		m_BtnQiangBank->setVisible(false);
		m_BtnCancelBank->setVisible(false);
		m_BtnCallBank->setVisible(true);
        m_bCanRobBanker = false;
	}
    
    if (nullptr != pMeUserData && m_BankUserList.size() > 2)
    {
        if (m_BankUserList[0].wUserID == pMeUserData->dwUserID || m_BankUserList[1].wUserID == pMeUserData->dwUserID)
        {
            m_bCanRobBanker = false;
        }
        else
        {
            m_bCanRobBanker = true;
        }
    }
    else
        m_bCanRobBanker = false;

    
    if (enGameNormal == m_GameState && m_nRobBanker < 3 && m_bCanRobBanker)
    {
        m_BtnQiangBank->setEnabled(true);
        m_BtnQiangBank->setColor(Color3B::WHITE);
    }
    else
    {
        m_BtnQiangBank->setEnabled(false);
        m_BtnQiangBank->setColor(Color3B(100, 100, 100));
    }
    
    //更新控件
	UpdateButtonContron();

	DrawBankList();

	return true;
}

//切换庄家
bool WZLZGameViewLayer::OnSubChangeBanker(const void * pBuffer, WORD wDataSize)
{
	//效验数据
	ASSERT(wDataSize==sizeof(CMD_S_WZLZ_ChangeBanker));
	if (wDataSize!=sizeof(CMD_S_WZLZ_ChangeBanker)) return false;

	//消息处理
	CMD_S_WZLZ_ChangeBanker * pChangeBanker=(CMD_S_WZLZ_ChangeBanker *)pBuffer;
    m_nRobBanker = pChangeBanker->nRobBanker;
	//庄家信息
	m_BankZhangJI = 0;
	m_BankJuShu = 0;
	SetBankerInfo(pChangeBanker->wBankerUser,pChangeBanker->lBankerScore);

	removeAllChildByTag(m_JsBack_Tga);
	if (getChildByTag(m_ChangeBankTga) != NULL)
	{
		removeChildByTag(m_ChangeBankTga);
	}
    Label* ChangeBankSpr = Label::createWithSystemFont("轮换庄家", "", 46);
	ChangeBankSpr->setTag(m_ChangeBankTga);
	ChangeBankSpr->setPosition(_STANDARD_SCREEN_CENTER_IN_PIXELS_);
	addChild(ChangeBankSpr,100);

	//自己是庄家
	if (m_IsMeBank == true)
	{
		m_BtnCancelBank->setVisible(false);
		m_BtnCallBank->setVisible(true);
		m_BtnQiangBank->setVisible(false);
	}

	m_IsMeBank = false;

	//自己判断
	if (IsLookonMode() == false && pChangeBanker->wBankerUser==GetMeChairID())
	{
		m_BtnCancelBank->setVisible(true);
		m_BtnCancelBank->setEnabled(false);
		m_BtnCancelBank->setColor(Color3B(100,100,100));
		m_BtnCallBank->setVisible(false);
		m_BtnQiangBank->setVisible(false);
		m_BankZhangJI = 0;
		m_BankJuShu = 0;
		m_IsMeBank = true;
	}

	//删除玩家
	if (m_wCurrentBanker!=INVALID_CHAIR)
	{
		tagApplyUser ApplyUser;
		sprintf(ApplyUser.strUserName,"%s", GetUserData(m_wCurrentBanker)->szNickName);
		ApplyUser.lUserScore=0;
		DeleteBankList(ApplyUser);
	}
    
    tagUserData *pUserData = ClientSocketSink::sharedSocketSink()->GetMeUserData();
    if (nullptr != pUserData && m_BankUserList.size() > 2)
    {
        if (m_BankUserList[0].wUserID == pUserData->dwUserID || m_BankUserList[1].wUserID == pUserData->dwUserID)
        {
            m_bCanRobBanker = false;
        }
        else
        {
            m_bCanRobBanker = true;
        }
    }
    else
        m_bCanRobBanker = false;
    
    if (enGameNormal == m_GameState && m_nRobBanker < 3 && m_bCanRobBanker)
    {
        m_BtnQiangBank->setEnabled(true);
        m_BtnQiangBank->setColor(Color3B::WHITE);
    }
    else
    {
        m_BtnQiangBank->setEnabled(false);
        m_BtnQiangBank->setColor(Color3B(100, 100, 100));
    }
    
    //更新界面
	UpdateButtonContron();
    
	DrawBankList();

	return true;
}


Animate* WZLZGameViewLayer::CreateAnim(char* filename)
{
    Vector<AnimationFrame*> arrayOfAnimationFrameNames;
    
	for(int i = 1; i <= 31; ++i)
	{
		char buffer[50]= {0};
		if(i < 10) sprintf(buffer, "%s_00%d.png",filename,i);
		else sprintf(buffer, "%s_0%d.png",filename,i);
		//生成精灵帧
		SpriteFrame *pSpriteFrame = SpriteFrameCache::getInstance()->getSpriteFrameByName(buffer);
		//生成动画帧
        ValueMap userInfo;

        AnimationFrame *pAnimationFrame = AnimationFrame::create(pSpriteFrame, 0.5f,userInfo);
        arrayOfAnimationFrameNames.pushBack(pAnimationFrame);
	}
	//生成动画数据对象
	Animation *pAnimation = Animation::create(arrayOfAnimationFrameNames, 0.3f);
	//生成动画动作对象
	Animate* anim = Animate::create(pAnimation);
	return anim;
}

void WZLZGameViewLayer::PlayShaiZi()
{
	m_PostCardIndex=(m_bcfirstShowCard-1)%4;
    string _name = GetCardStringName(m_bcfirstShowCard);
    Sprite* temp = Sprite::createWithSpriteFrameName(_name.c_str());
    temp->setPosition(_STANDARD_SCREEN_CENTER_);
    addChild(temp,1000);
    
    ActionInstant *func = CallFuncN::create(CC_CALLBACK_1(WZLZGameViewLayer::ShaiZiAnimEnd, this));
	DelayTime* dt = DelayTime::create(2);
	temp->runAction(Sequence::create(dt,func,NULL));
}

void WZLZGameViewLayer::ShaiZiAnimEnd(Node* pSender)
{
    pSender->removeFromParent();
	SendCard();
}


//游戏结束
bool WZLZGameViewLayer::OnSubGameEnd(const void * pBuffer, WORD wDataSize)
{
	//效验数据
	ASSERT(wDataSize==sizeof(CMD_S_WZLZ_GameEnd));
	if (wDataSize!=sizeof(CMD_S_WZLZ_GameEnd)) return false;

	//消息处理
	CMD_S_WZLZ_GameEnd * pGameEnd=(CMD_S_WZLZ_GameEnd *)pBuffer;

	m_CurSelectGold = 0;
    m_GameState = enGameEnd;
	//设置时间
	StartTime(pGameEnd->cbTimeLeave,2);

	//扑克信息
	SetCardInfo(pGameEnd->cbTableCardArray);

	m_bcfirstShowCard = pGameEnd->bcFirstCard;
	m_BankJuShu = pGameEnd->nBankerTime;
	m_BankZhangJI = pGameEnd->lBankerTotallScore;

	//成绩信息
	m_lPlayAllScore = pGameEnd->lUserScore;
	m_lBankerCurGameScore = pGameEnd->lBankerScore;
	m_lMeStatisticScore += m_lPlayAllScore;

	//打色子
	PlayShaiZi();

	//设置状态
	setGameStatus(GS_GAME_END);	   

	UpdateButtonContron();

	return true;
}


void WZLZGameViewLayer::SetWinArea(float fp) 	//设置赢得区域
{
	DeduceWinner(m_bWinShunMen, m_bWinDuiMen, m_bWinDaoMen);	

    //门判断
    if (true==m_bWinDaoMen)
    {
        removeAllChildByTag(m_LightArea_Tga[ID_DAO_MEN]);
        Vec2 pos = Vec2(m_AreaRect[ID_DAO_MEN].getMidX(), m_AreaRect[ID_DAO_MEN].getMidY());
        ui::Scale9Sprite* temp = ui::Scale9Sprite::createWithSpriteFrameName("wzlz_win.png");
        temp->setContentSize(m_AreaSize[ID_DAO_MEN]);
        temp->setPosition(pos);	
        temp->setTag(m_LightArea_Tga[ID_DAO_MEN]);
        addChild(temp);
        ActionInterval *actionBlink = Blink::create(8, 15);
        temp->runAction(Sequence::create(actionBlink,NULL));
    }
    
    if (true==m_bWinShunMen)
    {
        removeAllChildByTag(m_LightArea_Tga[ID_SHUN_MEN]);
        Vec2 pos = Vec2(m_AreaRect[ID_SHUN_MEN].getMidX(), m_AreaRect[ID_SHUN_MEN].getMidY());
        ui::Scale9Sprite* temp = ui::Scale9Sprite::createWithSpriteFrameName("wzlz_win.png");
        temp->setContentSize(m_AreaSize[ID_SHUN_MEN]);
        temp->setPosition(pos);	
        temp->setTag(m_LightArea_Tga[ID_SHUN_MEN]);
        addChild(temp);
        ActionInterval *actionBlink = Blink::create(8, 15);
        temp->runAction(Sequence::create(actionBlink,NULL));
    }
    if (true==m_bWinDuiMen)
    {
        removeAllChildByTag(m_LightArea_Tga[ID_DUI_MEN]);
        Vec2 pos = Vec2(m_AreaRect[ID_DUI_MEN].getMidX(), m_AreaRect[ID_DUI_MEN].getMidY());
        ui::Scale9Sprite* temp = ui::Scale9Sprite::createWithSpriteFrameName("wzlz_win.png");
        temp->setContentSize(m_AreaSize[ID_DUI_MEN]);
        temp->setPosition(pos);	
        temp->setTag(m_LightArea_Tga[ID_DUI_MEN]);
        addChild(temp);
        ActionInterval *actionBlink = Blink::create(8, 15);
        temp->runAction(Sequence::create(actionBlink,NULL));
    }
    //角判断
    if ((true==m_bWinShunMen) && (true==m_bWinDuiMen))
    {
        removeAllChildByTag(m_LightArea_Tga[ID_JIAO_L]);
        Vec2 pos = Vec2(m_AreaRect[ID_JIAO_L].getMidX(), m_AreaRect[ID_JIAO_L].getMidY());
        ui::Scale9Sprite* temp = ui::Scale9Sprite::createWithSpriteFrameName("wzlz_win.png");
        temp->setContentSize(m_AreaSize[ID_JIAO_L]);
        temp->setPosition(pos);	
        temp->setTag(m_LightArea_Tga[ID_JIAO_L]);
        addChild(temp);
        ActionInterval *actionBlink = Blink::create(8, 15);
        temp->runAction(Sequence::create(actionBlink,NULL));
    }
    if ((true==m_bWinShunMen) && (true==m_bWinDaoMen))
    {
        removeAllChildByTag(m_LightArea_Tga[ID_QIAO]);
        Vec2 pos = Vec2(m_AreaRect[ID_QIAO].getMidX(), m_AreaRect[ID_QIAO].getMidY());
        ui::Scale9Sprite* temp = ui::Scale9Sprite::createWithSpriteFrameName("wzlz_win.png");
        temp->setContentSize(m_AreaSize[ID_QIAO]);
        temp->setPosition(pos);
        temp->setTag(m_LightArea_Tga[ID_QIAO]);
        addChild(temp);
        ActionInterval *actionBlink = Blink::create(8, 15);
        temp->runAction(Sequence::create(actionBlink,NULL));
    }
    if ((true==m_bWinDuiMen) && (true==m_bWinDaoMen))
    {
        removeAllChildByTag(m_LightArea_Tga[ID_JIAO_R]);
        Vec2 pos = Vec2(m_AreaRect[ID_JIAO_R].getMidX(), m_AreaRect[ID_JIAO_R].getMidY());
        ui::Scale9Sprite* temp = ui::Scale9Sprite::createWithSpriteFrameName("wzlz_win.png");
        temp->setContentSize(m_AreaSize[ID_JIAO_R]);
        temp->setPosition(pos);	
        temp->setTag(m_LightArea_Tga[ID_JIAO_R]);
        addChild(temp);
        ActionInterval *actionBlink = Blink::create(8, 15);
        temp->runAction(Sequence::create(actionBlink,NULL));
    }
}

//设置扑克
void WZLZGameViewLayer::SetCardInfo(BYTE cbTableCardArray[4][2])
{
	if (cbTableCardArray!=NULL)
	{
		CopyMemory(m_cbTableCardArray,cbTableCardArray,sizeof(m_cbTableCardArray));
	}
	else
	{
		ZeroMemory(m_cbTableCardArray,sizeof(m_cbTableCardArray));
	}
}

void WZLZGameViewLayer::SendCard()
{
	m_SendCardCount = 0;
	m_SendCardNum = 0;
	m_SendCardOver = true;
	UpdataSendCard(0);
}


void WZLZGameViewLayer::UpdataSendCard(float fp)
{
	//第一个发牌的人
	if (m_SendCardOver == true)
	{
		m_OpenCardIndex = 1;
		m_PostCardIndex = m_PostCardIndex%4;
		SoundUtil::sharedEngine()->playEffect("SEND_CARD");
		m_SendCardOver = false;
		MoveTo *leftMoveBy = MoveTo::create(0.4f, Vec2(m_CardPos[m_PostCardIndex].x+m_SendCardCount*45,m_CardPos[m_PostCardIndex].y));
        ActionInstant *func = CallFuncN::create(CC_CALLBACK_1(WZLZGameViewLayer::CardMoveCallback, this));
		Sprite* temp = Sprite::createWithSpriteFrameName("backCard.png");
		temp->setPosition(_STANDARD_SCREEN_CENTER_);
		temp->setTag(m_TableCard_Tga[m_PostCardIndex][0]);
		temp->setAnchorPoint(Vec2(0,0));
		temp->setTag(m_CardBackIndex[m_PostCardIndex]);
		temp->setScaleY(0.7f);
		temp->setScaleX(0.7f);
		addChild(temp);
		temp->runAction(Sequence::create(leftMoveBy,func,NULL));
	}
}


void WZLZGameViewLayer::CardMoveCallback(Node *pSender)
{
	if(m_SendCardCount > 3) removeChild(pSender);
	m_PostCardIndex = m_PostCardIndex%4;
	int cc = 0;
	if(m_SendCardCount > 3) cc = 1;

	if (m_SendCardCount > 3)
	{
		string _name = GetCardStringName(m_cbTableCardArray[m_PostCardIndex][cc]);
		Sprite* temp = Sprite::createWithSpriteFrameName(_name.c_str());
        if (nullptr == temp)
            return;
        
		temp->setPosition(Vec2(m_CardPos[m_PostCardIndex].x+(cc)*45,m_CardPos[m_PostCardIndex].y));
		temp->setAnchorPoint(Vec2(0,0));
		temp->setTag(m_TableCard_Tga[m_PostCardIndex][cc]);
		temp->setScaleY(0.7f);
		temp->setScaleX(0.7f);
		addChild(temp,100);
	}
	if (m_SendCardCount == 7)
	{
		m_OpenNum = 0;
		UpdataOpenCard();
		return;
	}
	m_SendCardCount++;
	m_PostCardIndex++;
	m_PostCardIndex = m_PostCardIndex%4;
	if(m_SendCardCount > 3) cc = 1;
	MoveTo *leftMoveBy = MoveTo::create(0.4f, Vec2(m_CardPos[m_PostCardIndex].x+cc*45,m_CardPos[m_PostCardIndex].y));
	ActionInstant *func = CallFuncN::create(CC_CALLBACK_1(WZLZGameViewLayer::CardMoveCallback, this));
	Sprite* temp = Sprite::createWithSpriteFrameName("backCard.png");
	temp->setPosition(_STANDARD_SCREEN_CENTER_);
	temp->setAnchorPoint(Vec2(0,0));
	temp->setTag(m_TableCard_Tga[m_PostCardIndex][cc]);
	temp->setScaleY(0.7f);
	temp->setScaleX(0.7f);
	addChild(temp);
	temp->runAction(Sequence::create(leftMoveBy,func,NULL));
	SoundUtil::sharedEngine()->playEffect("SEND_CARD");
}

//开牌
void WZLZGameViewLayer::UpdataOpenCard()
{
	m_OpenCardIndex = m_OpenCardIndex%4;
	removeChildByTag(m_CardBackIndex[m_OpenCardIndex]);

	Sprite* temp = Sprite::createWithSpriteFrameName("HAND_L.png");
	temp->setPosition(Vec2(m_CardPos[m_OpenCardIndex].x-13,m_CardPos[m_OpenCardIndex].y+65));
	temp->setTag(m_HandIndex);
	addChild(temp,1);

	string _name = GetCardStringName(m_cbTableCardArray[m_OpenCardIndex][0]);
	temp = Sprite::createWithSpriteFrameName(_name.c_str());
	temp->setPosition(Vec2(m_CardPos[m_OpenCardIndex].x,m_CardPos[m_OpenCardIndex].y));
	temp->setAnchorPoint(Vec2(0,0));
	temp->setTag(m_TableCard_Tga[m_OpenCardIndex][0]);
	temp->setScaleY(0.7f);
	temp->setScaleX(0.7f);
	addChild(temp);

	Node* twocard = getChildByTag(m_TableCard_Tga[m_OpenCardIndex][1]);
	if(twocard == NULL) return;
    Sprite* rhand = Sprite::createWithSpriteFrameName("HAND_R.png");
    rhand->setPosition(Vec2(192,40));
    rhand->setTag(m_HandIndex);
    rhand->setScale(1.3f);
    twocard->addChild(rhand,1);
    
	ActionInterval *action = MoveTo::create(0.8f, Vec2(m_CardPos[m_OpenCardIndex].x+90,m_CardPos[m_OpenCardIndex].y-30));
	ActionInterval *action_back = MoveTo::create(0.8f, Vec2(m_CardPos[m_OpenCardIndex].x+40,m_CardPos[m_OpenCardIndex].y));
	ActionInstant *func = CallFuncN::create(CC_CALLBACK_1(WZLZGameViewLayer::OpenCardEnd, this));
	ActionInterval *act = Sequence::create(action, action_back, func, NULL);
	twocard->runAction(act);
}

void WZLZGameViewLayer::OpenCardEnd(Node* pSender)
{
	removeAllChildByTag(m_HandIndex);
    auto child = pSender->getChildByTag(m_HandIndex);
    if (child)
        child->removeFromParent();
    
	BYTE bCardType=m_GameLogic.GetCardType(m_cbTableCardArray[m_OpenCardIndex],2);
	if(bCardType < CT_POINT && bCardType > 0)
	{
		char _name[32];
		sprintf(_name,"FLAG_%d.png", bCardType);
		Sprite* temp = Sprite::createWithSpriteFrameName(_name);
		temp->setPosition(m_CardTypePos[m_OpenCardIndex]);
		temp->setTag(m_CardType_Tga);
		addChild(temp, 101);
	}
    else
    {
        BYTE bCardPoint = m_GameLogic.GetCardListPip(m_cbTableCardArray[m_OpenCardIndex],2);
        char _name[32];
        sprintf(_name,"POINT_%d.png", bCardPoint);
        Sprite* temp = Sprite::createWithSpriteFrameName(_name);
        temp->setPosition(m_CardTypePos[m_OpenCardIndex]);
        temp->setTag(m_CardType_Tga);
        addChild(temp, 101);
    }

	m_OpenNum++;
	if (m_OpenNum == 4) //开牌结速
	{
		SetWinArea(0);
        
        SendData(SUB_C_WZLZ_RECORD, NULL, 0);
		//播放声音
		if (m_lPlayAllScore>0) SoundUtil::sharedEngine()->playEffect("WZLZ/sound/END_WIN"); 
		else if (m_lPlayAllScore<0) SoundUtil::sharedEngine()->playEffect("WZLZ/sound/END_LOST");
		else SoundUtil::sharedEngine()->playEffect("WZLZ/sound/END_DRAW");
        
        ActionInstant *func = CallFunc::create(CC_CALLBACK_0(WZLZGameViewLayer::DrawEndScore, this));
        DelayTime* dt = DelayTime::create(5);
        this->runAction(Sequence::create(dt,func,NULL));
		return;
	}
	m_OpenCardIndex++;
	UpdataOpenCard();
}

//推断赢家
void WZLZGameViewLayer::DeduceWinner(bool &bWinShunMen, bool &bWinDuiMen, bool &bWinDaoMen)
{
	//大小比较
	if (m_GameLogic.CompareCard(m_cbTableCardArray[BANKER_INDEX],2,m_cbTableCardArray[SHUN_MEN_INDEX],2)==1)
	{
		bWinShunMen = true;
	}
	else
	{
		bWinShunMen = false;
	}
	if (m_GameLogic.CompareCard(m_cbTableCardArray[BANKER_INDEX],2,m_cbTableCardArray[DUI_MEN_INDEX],2)==1)
	{
		bWinDuiMen = true;
	}
	else
	{
		bWinDuiMen = false;
	}
	if (m_GameLogic.CompareCard(m_cbTableCardArray[BANKER_INDEX],2,m_cbTableCardArray[DAO_MEN_INDEX],2)==1)
	{
		bWinDaoMen = true;
	}
	else
	{
		bWinDaoMen = false;
	}
}

void WZLZGameViewLayer::SetGameHistory(bool bWinShunMen, bool bWinDaoMen, bool bWinDuiMen)
{
	WZLZtagClientGameRecord GameRecord;
	GameRecord.bWinShunMen=bWinShunMen;
	GameRecord.bWinDuiMen =bWinDuiMen;
	GameRecord.bWinDaoMen=bWinDaoMen;
    m_GameRecordArrary.push_back(GameRecord);
    if (m_GameRecordArrary.size() > 10)
    {
        auto font = m_GameRecordArrary.begin();
        m_GameRecordArrary.erase(font);
    }
}

//游戏记录
bool WZLZGameViewLayer::OnSubGameRecord(const void * pBuffer, WORD wDataSize)
{
	//效验参数
	ASSERT(wDataSize%sizeof(WZLZtagServerGameRecord)==0);
	if (wDataSize%sizeof(WZLZtagServerGameRecord)!=0) return false;

	//设置记录
    m_GameRecordArrary.clear();
    
	WORD wRecordCount=wDataSize/sizeof(WZLZtagServerGameRecord);
	for (WORD wIndex = 0; wIndex < wRecordCount; wIndex++)
	{
		WZLZtagServerGameRecord * pServerGameRecord=(((WZLZtagServerGameRecord *)pBuffer)+wIndex);

		SetGameHistory(pServerGameRecord->bWinShunMen, pServerGameRecord->bWinDaoMen, pServerGameRecord->bWinDuiMen);
	}
	UpdataGameHistory();

	return true;
}


//历史记录
void WZLZGameViewLayer::UpdataGameHistory()
{
    int sx = 181, sy = 320;
    int tga = 0;
    
	//清理所有
	for (int i = 0; i < 10; i++)
	{
		Node * temp = m_LzBack->getChildByTag(m_LzCell_Tga+tga);
		if (temp != NULL) m_LzBack->removeChild(temp,false);
		temp = m_LzBack->getChildByTag(m_LzCell_Tga+tga+1);
		if (temp != NULL) m_LzBack->removeChild(temp,false);
		temp = m_LzBack->getChildByTag(m_LzCell_Tga+tga+2);
		if (temp != NULL) m_LzBack->removeChild(temp,false);
		tga+=3;
	}

	int _count = 0;
	tga = 0;
    
	for (int i = 0; i < m_GameRecordArrary.size(); i++)
	{
		string name = "";
		if(m_GameRecordArrary[i].bWinShunMen) name = "gou.png";
		else name = "cha.png";
		Sprite* temp = Sprite::createWithSpriteFrameName(name.c_str());
		temp->setPosition(Vec2(sx+_count*82,sy));
		temp->setTag(m_LzCell_Tga+tga);
		m_LzBack->addChild(temp);

		if(m_GameRecordArrary[i].bWinDaoMen) name = "gou.png";
		else name = "cha.png";
		temp = Sprite::createWithSpriteFrameName(name.c_str());
		temp->setPosition(Vec2(sx+_count*82,sy-110));
		temp->setTag(m_LzCell_Tga+tga+1);
		m_LzBack->addChild(temp);

		if(m_GameRecordArrary[i].bWinDuiMen) name = "gou.png";
		else name = "cha.png";
		temp = Sprite::createWithSpriteFrameName(name.c_str());
		temp->setPosition(Vec2(sx+_count*82,sy-(2*110)));
		temp->setTag(m_LzCell_Tga+tga+2);
		m_LzBack->addChild(temp);

		_count++;
		tga+=3;
	}
}

bool WZLZGameViewLayer::OnSubBetFull(const void * pBuffer, WORD wDataSize)
{
    m_GameState = enGameBet;
	return true;
}

//游戏消息
bool WZLZGameViewLayer::OnGameMessage(WORD wSubCmdID, const void * pBuffer, WORD wDataSize)
{
	CCLOG("RECEIVE MESSAGE CMD[%d]" , wSubCmdID);
	switch (wSubCmdID)
	{
	case SUB_S_WZLZ_GAME_FREE:		//游戏空闲
		{
			return OnSubGameFree(pBuffer,wDataSize);
		}
	case SUB_S_WZLZ_GAME_START:		//游戏开始
		{
			return OnSubGameStart(pBuffer,wDataSize);
		}
	case SUB_S_WZLZ_PLACE_JETTON:	//用户加注
		{
			return OnSubPlaceJetton(pBuffer,wDataSize,true);
		}
	case SUB_S_WZLZ_JETTONFULL://下注满
		{
			return OnSubBetFull(pBuffer,wDataSize);
		}
	case SUB_S_WZLZ_APPLY_BANKER:	//申请做庄
		{
			return OnSubUserApplyBanker(pBuffer, wDataSize);
		}
	case SUB_S_WZLZ_QIANG_BANKER:	//抢庄
		{
			return OnSubQiangBanker(pBuffer,wDataSize);
		}
	case SUB_S_WZLZ_CANCEL_BANKER:	//取消做庄
		{
			return OnSubUserCancelBanker(pBuffer, wDataSize);
		}
	case SUB_S_WZLZ_CHANGE_BANKER:	//切换庄家
		{
			return OnSubChangeBanker(pBuffer, wDataSize);
		}
	case SUB_S_WZLZ_GAME_END:		//游戏结束
		{
			return OnSubGameEnd(pBuffer,wDataSize);
		}
	case SUB_S_WZLZ_SEND_RECORD:		//游戏记录
		{
			return OnSubGameRecord(pBuffer,wDataSize);
		}
	case SUB_S_WZLZ_APPLY_FAILURE:
		{
			m_bMeApplyBanker=false;
			m_bCanRobBanker=false;
			UpdateButtonContron();
			return true;
		}
	}
	return true;
}



//场景消息
bool WZLZGameViewLayer::OnGameSceneMessage(BYTE cbGameStatus, const void * pBuffer, WORD wDataSize)
{
	switch (cbGameStatus)
	{
	case GS_FREE: //空闲状态
		{
			//效验数据
			ASSERT(wDataSize==sizeof(CMD_S_WZLZ_StatusFree));
			if (wDataSize!=sizeof(CMD_S_WZLZ_StatusFree)) return false;
			CMD_S_WZLZ_StatusFree * pStatusFree=(CMD_S_WZLZ_StatusFree *)pBuffer;
            m_nRobBanker = pStatusFree->nRobBanker;
            m_BankUserList.clear();
			//设置时间
			StartTime(pStatusFree->cbTimeLeave, 0);
            m_GameState = enGameNormal;
            
			//玩家信息
			m_lMeMaxScore=pStatusFree->lUserMaxScore;
            
			//庄家信息
			m_bEnableSysBanker=pStatusFree->bEnableSysBanker;
			m_BankZhangJI = pStatusFree->lBankerWinScore;
			m_BankJuShu = pStatusFree->cbBankerTime;
			SetBankerInfo(pStatusFree->wBankerUser,pStatusFree->lBankerScore);

			//控制信息
			m_lApplyBankerCondition=pStatusFree->lApplyBankerCondition;
			m_lAreaLimitScore=pStatusFree->lAreaLimitScore;
			UpdateButtonContron();
			
			break;
		}
	case GS_PLACE_JETTON:		//游戏状态
	case GS_GAME_END:		//结束状态
		{
			//效验数据
			ASSERT(wDataSize==sizeof(CMD_S_WZLZ_StatusPlay));
			if (wDataSize!=sizeof(CMD_S_WZLZ_StatusPlay)) return false;
			CMD_S_WZLZ_StatusPlay * pStatusPlay=(CMD_S_WZLZ_StatusPlay *)pBuffer;
            m_nRobBanker = pStatusPlay->nRobBanker;
            m_BankUserList.clear();
			//下注信息
			for (int nAreaIndex=0; nAreaIndex < AREA_COUNT; ++nAreaIndex)
			{
				SetEndScore(nAreaIndex, pStatusPlay->lAllJettonScore[nAreaIndex+1]);
				SetMePlaceJetton(nAreaIndex, pStatusPlay->lUserJettonScore[nAreaIndex+1]);
			}

			//玩家积分
			m_lMeMaxScore=pStatusPlay->lUserMaxScore;			
            m_GameState = enGameOpenCard;
            
			//控制信息
			m_lApplyBankerCondition=pStatusPlay->lApplyBankerCondition;
			m_lAreaLimitScore=pStatusPlay->lAreaLimitScore;

			if (pStatusPlay->cbGameStatus==GS_GAME_END)
			{
				//扑克信息
				SetCardInfo(pStatusPlay->cbTableCardArray);

				for (int i = 0; i < 4; i++)
				{
					removeAllChildByTag(m_TableCard_Tga[i][0]);
					removeAllChildByTag(m_TableCard_Tga[i][1]);
				}

				for (int i = 0;i<4;i++)
				{
					string _name = GetCardStringName(m_cbTableCardArray[i][0]);
					Sprite* temp = Sprite::createWithSpriteFrameName(_name.c_str());
					temp->setPosition(Vec2(m_CardPos[i].x+(0)*45,m_CardPos[i].y));
					temp->setAnchorPoint(Vec2(0,0));
					temp->setTag(m_TableCard_Tga[i][0]);
					temp->setScaleY(0.7f);
					temp->setScaleX(0.7f);
					addChild(temp,100);

					_name = GetCardStringName(m_cbTableCardArray[i][1]);
					temp = Sprite::createWithSpriteFrameName(_name.c_str());
					temp->setPosition(Vec2(m_CardPos[i].x+(1)*45,m_CardPos[i].y));
					temp->setAnchorPoint(Vec2(0,0));
					temp->setTag(m_TableCard_Tga[i][1]);
					temp->setScaleY(0.7f);
					temp->setScaleX(0.7f);
					addChild(temp,100);
				}

				////设置成绩
				//成绩信息
				m_lPlayAllScore = pStatusPlay->lEndUserScore;
				m_lBankerCurGameScore = pStatusPlay->lEndBankerScore;
				m_lMeStatisticScore += m_lPlayAllScore;

				SetWinArea(0);

				//播放声音
				if (m_lPlayAllScore>0) SoundUtil::sharedEngine()->playEffect("WZLZ/sound/END_WIN");
				else if (m_lPlayAllScore<0) SoundUtil::sharedEngine()->playEffect("WZLZ/sound/END_LOST");
				else SoundUtil::sharedEngine()->playEffect("WZLZ/sound/END_DRAW"); 
				DrawEndScore();

			}
			else
			{
				SetCardInfo(NULL);
			}

			//庄家信息
			m_BankZhangJI = pStatusPlay->lBankerWinScore;
			m_BankJuShu = pStatusPlay->cbBankerTime;
			SetBankerInfo(pStatusPlay->wBankerUser,pStatusPlay->lBankerScore);
			m_bEnableSysBanker=pStatusPlay->bEnableSysBanker;
			
			//设置状态
			setGameStatus(pStatusPlay->cbGameStatus);

			//设置时间
			StartTime(15, pStatusPlay->cbGameStatus==GS_GAME_END?2:1);

			//更新按钮
			UpdateButtonContron();
			break;
		}
	}
	return true;
}


string WZLZGameViewLayer::GetCardStringName(BYTE card)
{
	if (card == 0) return "Ox2backCard.png";
	char tt[32];
	sprintf(tt,"%0x",card);
	BYTE _value = m_GameLogic.GetCardValue(card);
	BYTE _color = m_GameLogic.GetCardColor(card);
	string temp;
	if (_color == 0) temp = "fangkuai_";
	if (_color == 1) temp = "meihua_";
	if (_color == 2) temp = "hongtao_";
	if (_color == 3) temp = "heitao_";
	char _valstr[32];
	ZeroMemory(_valstr,32);
	if (card == 0x41) //小王
	{
		sprintf(_valstr,"xiaowang.png");
	}
	else if (card == 0x42)
	{
		sprintf(_valstr,"dawang.png");
	}
	else if (_value < 10)
	{
		sprintf(_valstr,"0%d.png",_value);
	}
	else 
	{
		sprintf(_valstr,"%d.png",_value);
	}

	temp+=_valstr;
	return temp;
}

//更新控制
void WZLZGameViewLayer::UpdateButtonContron()
{
	//不是下注状态,自己是庄家
	if (getGameStatus() != GS_PLACE_JETTON || m_wCurrentBanker == GetMeChairID())
	{
		removeAllChildByTag(m_BtnAnim_Tga);
		m_Gold1000->setEnabled(false);
		m_Gold1w->setEnabled(false);
		m_Gold10w->setEnabled(false);
		m_Gold100w->setEnabled(false);
		m_Gold500w->setEnabled(false);
		m_Gold1000->setColor(Color3B(100,100,100));
		m_Gold1w->setColor(Color3B(100,100,100));
		m_Gold10w->setColor(Color3B(100,100,100));
		m_Gold100w->setColor(Color3B(100,100,100));
		m_Gold500w->setColor(Color3B(100,100,100));
	}
	else
	{
        if (0 == m_lBankerScore || IsLookonMode())
            return;
        
		//计算积分
		LONGLONG lLeaveScore=m_lMeMaxScore;
		for (int nAreaIndex=0; nAreaIndex<AREA_COUNT; ++nAreaIndex) 
		{
			lLeaveScore -= m_lUserJettonScore[nAreaIndex];
		}

		//最大下注
		LONGLONG lUserMaxJetton = GetUserMaxJetton(0xFF);
		if (lLeaveScore > lUserMaxJetton) lLeaveScore = lUserMaxJetton;

		//控制按钮
		int iTimer = 1;
		if(lLeaveScore>=1000*iTimer && lUserMaxJetton>=1000*iTimer)
		{
			m_Gold1000->setEnabled(true);
			m_Gold1000->setColor(Color3B(255,255,255));
		}
		else
		{
			m_Gold1000->setEnabled(false);
			m_Gold1000->setColor(Color3B(100,100,100));
		}
		if(lLeaveScore>=10000*iTimer && lUserMaxJetton>=10000*iTimer)
		{
			m_Gold1w->setEnabled(true);
			m_Gold1w->setColor(Color3B(255,255,255));
		}
		else
		{
			m_Gold1w->setEnabled(false);
			m_Gold1w->setColor(Color3B(100,100,100));
		}
		if(lLeaveScore>=100000*iTimer && lUserMaxJetton>=100000*iTimer)
		{
			m_Gold10w->setEnabled(true);
			m_Gold10w->setColor(Color3B(255,255,255));
		}
		else
		{
			m_Gold10w->setEnabled(false);
			m_Gold10w->setColor(Color3B(100,100,100));
		}
        
		if(lLeaveScore>=1000000*iTimer && lUserMaxJetton>=1000000*iTimer)
		{
			m_Gold100w->setEnabled(true);
			m_Gold100w->setColor(Color3B(255,255,255));
		}
		else
		{
			m_Gold100w->setEnabled(false);
			m_Gold100w->setColor(Color3B(100,100,100));
		}
		if(lLeaveScore>=5000000*iTimer && lUserMaxJetton>=5000000*iTimer)
		{
			m_Gold500w->setEnabled(true);
			m_Gold500w->setColor(Color3B(255,255,255));
		}
		else
		{
			m_Gold500w->setEnabled(false);
			m_Gold500w->setColor(Color3B(100,100,100));
		}
	}

	return;
}


//最大下注
LONGLONG WZLZGameViewLayer::GetUserMaxJetton(BYTE cbArea)
{
	//变量定义
	LONGLONG lMeMaxScore = 0L;

	//已下注额
	LONGLONG lNowJetton = 0;
	ASSERT(AREA_COUNT<=CountArray(m_lUserJettonScore));
	for (int nAreaIndex=0; nAreaIndex<AREA_COUNT; ++nAreaIndex) lNowJetton += m_lUserJettonScore[nAreaIndex];

	//庄家金币
	LONGLONG lBankerScore=2147483647;
	if (m_wCurrentBanker!=INVALID_CHAIR) lBankerScore=m_lBankerScore;
	for (int nAreaIndex=0; nAreaIndex<AREA_COUNT; ++nAreaIndex) lBankerScore-=m_lAllJettonScore[nAreaIndex];

	//区域限制
	LONGLONG lAreaLimitScore = 0;
	if (cbArea == 0xFF)
	{
		for (int nAreaIndex=0; nAreaIndex < AREA_COUNT; ++nAreaIndex)
		{
			LONGLONG lAreaLeaveScore = m_lAreaLimitScore-m_lAllJettonScore[nAreaIndex];
			if (lAreaLeaveScore > lAreaLimitScore)
				lAreaLimitScore = lAreaLeaveScore;
		}
	}
	else
	{
		lAreaLimitScore = m_lAreaLimitScore - m_lAllJettonScore[cbArea];
	}

	lMeMaxScore= m_lMeMaxScore-lNowJetton < lAreaLimitScore? m_lMeMaxScore-lNowJetton:lAreaLimitScore;

	//庄家限制
	lMeMaxScore= lMeMaxScore < lBankerScore ? lMeMaxScore:lBankerScore;

	//非零限制
	//ASSERT(lMeMaxScore >= 0);
	lMeMaxScore =lMeMaxScore > 0 ? lMeMaxScore:0;

	return lMeMaxScore;
}

void WZLZGameViewLayer::DeleteBankList(tagApplyUser ApplyUser)
{
    for (int i = 0; i < m_BankUserList.size(); i++)
    {
        if (strcmp(m_BankUserList[i].strUserName, ApplyUser.strUserName) == 0)
        {
            m_BankUserList.erase(m_BankUserList.begin()+i);
            return;
        }
    }
}

void WZLZGameViewLayer::DrawBankList()
{
    m_listView->removeAllItems();
    for (int i = 0; i < m_BankUserList.size(); i++)
    {
        ui::Layout* custom_item = ui::Layout::create();
        custom_item->setContentSize(Size(920, 120));
        
        Sprite* lable = Sprite::createWithSpriteFrameName("label_line.png");
        lable->setPosition(460, 0);
        custom_item->addChild(lable);
        
        //头像
        string heads = g_GlobalUnits.getFace(m_BankUserList[i].wGender, m_BankUserList[i].lUserScore);
        Sprite* mHead = Sprite::createWithSpriteFrameName(heads);
        mHead->setPosition(Vec2(90, 60));
        mHead->setScale(0.6f);
        custom_item->addChild(mHead);
        
        //昵称
        std::string szTempTitle = gbk_utf8(m_BankUserList[i].strUserName);
        Label* temp = Label::createWithSystemFont(szTempTitle.c_str(),_GAME_FONT_NAME_1_,40);
        temp->setAnchorPoint(Vec2(0,0.5f));
        
        if (!strcmp(GetMeUserData()->szNickName, m_BankUserList[i].strUserName))
            temp->setColor(Color3B::RED);
        else
            temp->setColor(Color3B::BLACK);
        temp->setPosition(Vec2(170, 60));
        custom_item->addChild(temp);
        
        Sprite* goldIcon = Sprite::createWithSpriteFrameName("gold_icon.png");
        goldIcon->setPosition(Vec2(620, 60));
        custom_item->addChild(goldIcon);
        char _score[32];
        if (m_BankUserList[i].lUserScore > 1000000000000)
        {
            sprintf(_score,"%.02lf兆", m_BankUserList[i].lUserScore / 1000000000000.0);
        }
        else if (m_BankUserList[i].lUserScore > 100000000)
        {
            sprintf(_score,"%.02lf亿", m_BankUserList[i].lUserScore / 100000000.0);
        }
        else if (m_BankUserList[i].lUserScore > 1000000)
        {
            sprintf(_score,"%.02lf万", m_BankUserList[i].lUserScore / 10000.0);
        }
        temp = Label::createWithSystemFont(_score,_GAME_FONT_NAME_1_,40);
        temp->setAnchorPoint(Vec2(0,0.5f));
        temp->setColor(Color3B::BLACK);
        temp->setPosition(Vec2(660, 60));
        custom_item->addChild(temp);
        m_listView->pushBackCustomItem(custom_item);
    }
}


bool WZLZGameViewLayer::IsLookonMode()
{
    return m_LookMode;
}

void WZLZGameViewLayer::backLoginView( Ref *pSender )
{
	IGameView::backLoginView(pSender);	
}

void WZLZGameViewLayer::OnQuit()
{
	m_pGameScene->removeChild(this);
}

Menu* WZLZGameViewLayer::CreateButton(std::string szBtName ,const Vec2 &p , int tag )
{
	return Tools::Button(StrToChar(szBtName+"_normal.png") , StrToChar(szBtName+"_click.png") , p, this , menu_selector(WZLZGameViewLayer::callbackBt) , tag);
}

// Alert Message 确认消息处理
void WZLZGameViewLayer::DialogConfirm(Ref *pSender)
{
	this->RemoveAlertMessageLayer();
	this->SetAllTouchEnabled(true);
}

// Alert Message 取消消息处理
void WZLZGameViewLayer::DialogCancel(Ref *pSender)
{
	this->RemoveAlertMessageLayer();
	this->SetAllTouchEnabled(true);
}

void WZLZGameViewLayer::OnEventUserScore( tagUserData * pUserData, WORD wChairID, bool bLookonUser )
{
	IGameView::OnEventUserScore(pUserData, wChairID, bLookonUser);
    AddPlayerInfo();
}

void WZLZGameViewLayer::OnEventUserStatus(tagUserData * pUserData, WORD wChairID, bool bLookonUser)
{
	IGameView::OnEventUserStatus(pUserData, wChairID, bLookonUser);
	AddPlayerInfo();
}

void WZLZGameViewLayer::OnEventUserEnter( tagUserData * pUserData, WORD wChairID, bool bLookonUser )
{
	AddPlayerInfo();
}

void WZLZGameViewLayer::OnEventUserLeave( tagUserData * pUserData, WORD wChairID, bool bLookonUser )
{
	AddPlayerInfo();
}

void WZLZGameViewLayer::LayerMoveCallBack()
{
    m_IsLayerMove = false;
}

void WZLZGameViewLayer::CloseLzList()
{
    if (m_IsLayerMove)
        return;
    m_IsLayerMove = true;
    m_LzShow = false;
    MoveTo *leftMoveBy = MoveTo::create(0.2f, Vec2(_STANDARD_SCREEN_CENTER_.x, 1500));
    ActionInstant *func = CallFunc::create(CC_CALLBACK_0(WZLZGameViewLayer::LayerMoveCallBack,this));
    m_LzBack->runAction(Sequence::create(leftMoveBy, func, NULL));
}

void WZLZGameViewLayer::OpenLzList()
{
    if (m_IsLayerMove)
        return;
    m_IsLayerMove = true;
    m_LzShow = true;
    MoveTo *leftMoveBy = MoveTo::create(0.2f, _STANDARD_SCREEN_CENTER_);
    ActionInstant *func = CallFunc::create(CC_CALLBACK_0(WZLZGameViewLayer::LayerMoveCallBack,this));
    m_LzBack->runAction(Sequence::create(leftMoveBy, func, NULL));
}

void WZLZGameViewLayer::CloseBankList()
{
    if (m_IsLayerMove)
        return;
    m_IsLayerMove = true;
    m_OpenBankList = false;
    MoveTo *leftMoveBy = MoveTo::create(0.2f, Vec2(960,1500));
    ActionInstant *func = CallFunc::create(CC_CALLBACK_0(WZLZGameViewLayer::LayerMoveCallBack,this));
    m_BankListBackSpr->runAction(Sequence::create(leftMoveBy, func, NULL));
}
void WZLZGameViewLayer::OpenBankList()
{
    if (m_IsLayerMove)
        return;
    m_IsLayerMove = true;
    m_OpenBankList = true;
    MoveTo *leftMoveBy = MoveTo::create(0.2f, _STANDARD_SCREEN_CENTER_);
    ActionInstant *func = CallFunc::create(CC_CALLBACK_0(WZLZGameViewLayer::LayerMoveCallBack,this));
    m_BankListBackSpr->runAction(Sequence::create(leftMoveBy, func, NULL));
}
