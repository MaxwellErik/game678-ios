#ifndef _WZLZGAME_VIEW_LAYER_H_
#define _WZLZGAME_VIEW_LAYER_H_

#include "GameScene.h"
#include "FrameGameView.h"
#include "WZLZGameLogic.h"
#include "CMD_WZLZ.h"
#include "WZLZGameUserList.h"
#include "ui/UIListView.h"

class CTimeTaskLayer;
class WZLZGameViewLayer : public IGameView
{
	struct tagApplyUser
	{
		tagApplyUser()
		{
			ZeroMemory(strUserName,128);
            wGender = 0;
            wUserID = 0;
			lUserScore = 0;
		}
		void Clear()
		{
			ZeroMemory(strUserName,128);
            wGender = 0;
            wUserID = 0;
			lUserScore = 0;

		}
		//玩家信息
		char							strUserName[128];						//玩家帐号
        BYTE                            wGender;
        DWORD                           wUserID;
		LONGLONG						lUserScore;							//玩家金币
	};

public:
	static WZLZGameViewLayer *create(GameScene *pGameScene);
	virtual ~WZLZGameViewLayer();
	WZLZGameViewLayer(GameScene *pGameScene);
	WZLZGameViewLayer(const WZLZGameViewLayer&);
	WZLZGameViewLayer& operator = (const WZLZGameViewLayer&);

	virtual bool init(); 
	virtual void onEnter();
	virtual void onExit();

    virtual bool onTouchBegan(Touch *pTouch, Event *pEvent);
	virtual void onTouchMoved(Touch *pTouch, Event *pEvent){};
	virtual void onTouchEnded(Touch *pTouch, Event *pEvent){};

	virtual bool onTextFieldAttachWithIME(TextFieldTTF*pSender){return false;}
	virtual bool onTextFieldDetachWithIME(TextFieldTTF*pSender){return false;}
	virtual bool onTextFieldInsertText(TextFieldTTF *pSender, const char *text, int nLen){return true;}
	virtual bool onTextFieldDeleteBackward(TextFieldTTF *pSender, const char *delText, int nLen){return true;}

	virtual void keyboardWillShow(IMEKeyboardNotificationInfo &info){}
	virtual void keyboardDidShow(IMEKeyboardNotificationInfo &info){}
	virtual void keyboardWillHide(IMEKeyboardNotificationInfo &info){}
	virtual void keyboardDidHide(IMEKeyboardNotificationInfo &info){}
	virtual void DrawUserScore(){}	//绘制玩家分数
	virtual void GameEnd(){};
	virtual void ShowAddScoreBtn(bool bShow=true){};
	virtual void UpdateDrawUserScore(){};		//更新显示的游戏币数量

	void callbackBt( Ref *pSender );

	virtual void backLoginView(Ref *pSender);
	string  AddCommaToNum(LONG Num);

	//初始化
	void InitGame();
	void AddPlayerInfo();
	void AddButton();
	Menu* CreateButton(std::string szBtName ,const Vec2 &p , int tag);

	// 按钮事件管理
	void DialogConfirm(Ref *pSender);
	void DialogCancel(Ref *pSender);
	string GetCardStringName(BYTE card);
	//网络接口
public:
	void OnEventUserLeave( tagUserData * pUserData, WORD wChairID, bool bLookonUser );
	//用户进入
	virtual void OnEventUserEnter(tagUserData * pUserData, WORD wChairID, bool bLookonUser);
	//用户积分
	virtual void OnEventUserScore(tagUserData * pUserData, WORD wChairID, bool bLookonUser);
	//用户状态
	virtual void OnEventUserStatus(tagUserData * pUserData, WORD wChairID, bool bLookonUser);
    //用户换桌
    virtual void OnEventUserChangeTable(tagUserData * pUserData, WORD wChairID, bool bLookonUser){};
    
	//游戏消息
	virtual bool OnGameMessage(WORD wSubCmdID, const void * pBuffer, WORD wDataSize);
	//场景消息
	virtual bool OnGameSceneMessage(BYTE cbGameStatus, const void * pBuffer, WORD wDataSize);

	virtual void OnReconnectAction(){};

	void OnQuit();

public:
	void UpdateTime(float fp);
	void StopTime();
	void StartTime(int _time, int _type);

	void SetBankerInfo(WORD wBanker,LONGLONG lScore);
	void SetEndScore(int chair, LONGLONG score);
	void SetMePlaceJetton(int chair, LONGLONG score);
	bool IsLookonMode();
	void DeleteBankList(tagApplyUser ApplyUser);
	void DrawBankList();
	void UpdateButtonContron();
	LONGLONG GetUserMaxJetton(BYTE cbArea);
	void SetCardInfo(BYTE cbTableCardArray[4][2]);
	void SendCard();
	void UpdataOpenCard();
	void UpdataSendCard(float fp);
	void CardMoveCallback(Node *pSender);
	void CardMoveCallback2(Node *pSender,void*data);
	void CardMoveCallback3(Node *pSender,void*data);
	void CardMoveCallback4(Node *pSender,void*data);
	void DeduceWinner(bool &bWinShunMen, bool &bWinDuiMen, bool &bWinDaoMen);
	void SetGameHistory(bool bWinShunMen, bool bWinDaoMen, bool bWinDuiMen);
	void UpdataGameHistory();
	void CloseBankList();
	void OpenBankList();
    void CloseLzList();
    void OpenLzList();
	void SetWinArea(float fp);
	void DrawEndScore();
	void BetSound(float fp);
	void Select1000();
	void Select1w();
	void Select10w();
	void Select100w();
	void Select500w();
	void PlayShaiZi();
	Animate* CreateAnim(char* filename);
	void ShaiZiAnimEnd(Node* pSender);
	void OpenCardEnd(Node* pSender);
	void OnQiangBanker();
    void LayerMoveCallBack();
public:
	bool OnSubGameFree(const void * pBuffer, WORD wDataSize);
	bool OnSubGameStart(const void * pBuffer, WORD wDataSize);
	bool OnSubPlaceJetton(const void * pBuffer, WORD wDataSize,bool bGameMes);
	bool OnSubUserApplyBanker(const void * pBuffer, WORD wDataSize);
	bool OnSubQiangBanker(const void * pBuffer, WORD wDataSize);
	bool OnSubUserCancelBanker(const void * pBuffer, WORD wDataSize);
	bool OnSubChangeBanker(const void * pBuffer, WORD wDataSize);
	bool OnSubGameEnd(const void * pBuffer, WORD wDataSize);
	bool OnSubGameRecord(const void * pBuffer, WORD wDataSize);
	bool OnSubBetFull(const void * pBuffer, WORD wDataSize);

	void OnApplyBanker(int wParam);

protected:
	WZLZ_GameLogic		m_GameLogic;
    ui::ListView*       m_listView;
	//图片
	Sprite*			m_BackSpr;
	Sprite*			m_LzBack;
	Sprite*			m_BankListBackSpr;
    Sprite*         m_ClockSpr;
	//按钮
	Menu*				m_BtnBackToLobby;
	Menu*				m_BtnSeting;
	Menu*				m_BtnCallBank;
	Menu*				m_BtnQiangBank;
	Menu*				m_BtnCancelBank;
	Menu*				m_Gold1000;
	Menu*				m_Gold1w;
	Menu*				m_Gold10w;
	Menu*				m_Gold100w;
	Menu*				m_Gold500w;
	Menu*				m_BtnClose;
	Menu*				m_BankBtn;
    Menu*				m_BankUp;
	Menu*				m_BankDown;
	Menu*				m_BtnLZBtn;

	//索引
    int                 m_Head_Tga;
	int					m_NickName_Tga;
	int					m_Glod_Tga;
	int					m_WinGlod_Tga;
	int					m_BankNickName_Tga;	//庄家信息
	int					m_BankGold_Tga;
	int					m_BankZhangJi_Tga;
	int					m_BankJuShu_Tga;
    int                 m_bankBg_Tga;
	int					m_BetScore_Tga[AREA_COUNT];	//下注额度
	int					m_BetScore1_Tga[AREA_COUNT];	
	int					m_BtnAnim_Tga;
	int					m_TableCard_Tga[4][2];
	int					m_CardType_Tga;
	int					m_TimeIndex_Tga;
	int					m_LzCell_Tga;
	int					m_LightArea_Tga[AREA_COUNT];
	int					m_JsBack_Tga;
	int					m_JsXian_Tga;
	int					m_JsBank_Tga;
	int					m_CardBackIndex[4];
	int					m_HandIndex;
	int					m_AllScoreTga;
	int					m_BankScoreTableTga;
	int					m_ChangeBankTga;
    int					m_TimeType_Tga;
    
	//位置
	Vec2				m_HeadPos;          //自己的信息
	Vec2				m_NickNamePos;      //自己的信息
    Vec2				m_GoldPos;          //自己的信息
    Vec2				m_WinGlodPos;
	Vec2				m_BankNickNamePos;	//庄家信息
	Vec2				m_BankGoldPos;
	Vec2				m_BankZhangJiPos;
	Vec2				m_BankJuShuPos;
	Vec2				m_BetNumPos[AREA_COUNT];
	Vec2				m_CardPos[4];
	Vec2				m_CardTypePos[4];
	Vec2				m_TableBankScorePos;
    
    Rect                m_AreaRect[AREA_COUNT];
    Size				m_AreaSize[AREA_COUNT];
    
	int					m_OpenNum;
	int					m_StartTime;
	bool				m_OpenBankList;
    bool                m_LzShow;
    bool                m_IsLayerMove;
	LONGLONG			m_lPlayAllScore;
	LONGLONG			m_lBankerCurGameScore;
	LONGLONG			m_lMeStatisticScore;
	BYTE				m_bcfirstShowCard;
	int					m_PostCardIndex;
	int					m_OpenCardIndex;
	bool							m_bBetSound;
	bool							m_bWinShunMen;
	bool							m_bWinDuiMen;
	bool							m_bWinDaoMen;
	bool							m_SendCardOver;
	int								m_SendCardCount;
	int								m_SendCardNum;
	BYTE							m_cbTableCardArray[4][2];
	int								m_CurSelectGold;
	int								m_BankListBeginIndex;
	LONGLONG						m_lMeMaxScore;						//最大下注
	LONGLONG						m_lAreaLimitScore;					//区域限制
	LONGLONG						m_lApplyBankerCondition;			//申请条件
	LONGLONG						m_lUserJettonScore[AREA_COUNT];		//个人总注
	LONGLONG						m_lBankerScore;						//庄家积分
	LONGLONG						m_BankZhangJI;
	int								m_BankJuShu;
	WORD							m_wCurrentBanker;					//当前庄家
	BYTE							m_cbLeftCardCount;					//扑克数目
	bool							m_bEnableSysBanker;					//系统做庄
	int								m_nNotScoreCnt;						//没下注局数
	bool							m_bMeApplyBanker;					//申请标识
    int                             m_nRobBanker;
	bool                            m_bCanRobBanker;
	LONGLONG						m_lAllJettonScore[AREA_COUNT];
	bool							m_LookMode;
	vector<tagApplyUser>			m_BankUserList;
	vector<WZLZtagClientGameRecord> m_GameRecordArrary;
	bool							m_IsMeBank;
};

#endif
