﻿#include "HXmlParse.h"
#include "platform/CCSAXParser.h"

HXmlParse::HXmlParse(void)
{
	m_bParentEnd=true;
	m_bEnd=true;
	m_currentDic= NULL;
}


HXmlParse::~HXmlParse(void)
{
}

HXmlParse* HXmlParse::parserWithFile(const char* tmxFile,bool isJumpHead)
{
	HXmlParse *pRet=new HXmlParse();
	pRet->isJumpHeadData=isJumpHead;
	if(pRet->initXmlParse(tmxFile)){
		pRet->autorelease();
		return pRet;
	}
	CC_SAFE_DELETE(pRet);
	return NULL;
}

bool HXmlParse::initXmlParse(const char* xmlName)
{
	mDic=__Dictionary::create();
	SAXParser _pair;
	if(false==_pair.init("UTF-8"))
	{
		CCLOG("---please use utf-8");
		return false;
	}
	_pair.setDelegator(this);
	std::string _path = FileUtils::getInstance()->fullPathForFilename(xmlName);
	CCLOG("%s",_path.c_str());
	return _pair.parse(_path.c_str());
}

void HXmlParse::startElement(void *ctx,const char* name,const char** atts)
{
	CC_UNUSED_PARAM(ctx);
	startXmlElement=(char*)name;
	if(!isJumpHeadData){	//跳过数据头(false)
		isJumpHeadData=true;
		root_name=startXmlElement;
		return;
	}
	if (m_bParentEnd)
	{
		m_bParentEnd=false;
		parentElement = startXmlElement;
		if (startXmlElement == endXmlElement)
		{
			void * pValue = mDic->objectForKey(startXmlElement);
			if (pValue)
			{
				__Array *pArray = (__Array *)pValue;
				pArray->addObject(m_currentDic);
			}
			else
			{
				__Array *pArray = __Array::create();
				pArray->addObject(m_currentDic);
				mDic->setObject(pArray ,startXmlElement);
			}
		}
		else if(m_currentDic!=NULL)
		{
			mDic->setObject(m_currentDic ,endXmlElement);
		}
		m_currentDic=__Dictionary::create();
	}
	if(atts){	 //有属性值
		int i=0;
		__String *datastr;
		while(atts[i])//atts存有name的属性（单数是属性名，双数是数值）
		{	
			datastr=__String::create("");
			//	 CCString datastr;
			std::string keystr(atts[i]);//属性名
			datastr->_string=atts[i+1];//数值
			m_currentDic->setObject(datastr,keystr);	 //setobject（）传进去Object是传引用，所以每次调用都要不同的对象。（要不然会覆盖）
			i=i+2;
		}
	}
	m_bEnd=false;
}

void HXmlParse::endElement(void *ctx,const char* name)
{
	CC_UNUSED_PARAM(ctx);
	m_bEnd=true;
	endXmlElement=(char*)name;
	if(endXmlElement==root_name){
		isJumpHeadData=false;
		root_name="";
		m_bParentEnd=true;
		void * pValue = mDic->objectForKey(parentElement);
		if (pValue)
		{
			__Array *pArray = (__Array *)pValue;
			pArray->addObject(m_currentDic);
		}
		else
		{
			mDic->setObject(m_currentDic ,parentElement);
		}
		return;
	}
	if (endXmlElement == parentElement)
	{
		m_bParentEnd=true;
	}
}

void HXmlParse::textHandler(void *ctx, const char *s, int len)
{
	CC_UNUSED_PARAM(ctx);
	currString=std::string((char*)s,0,len);
	__String *ccstr=__String::create("");
	ccstr->_string=currString;
	if(root_name!=""){
		m_currentDic->setObject(ccstr,startXmlElement);
	}
}
