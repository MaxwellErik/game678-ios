﻿#ifndef _ALERT_MESSAGE_LAYER_H_
#define _ALERT_MESSAGE_LAYER_H_

#include "GameScene.h"


// 需预加载
// "Image/AlertMessageLayer/AlertMessageLayer.plist"

class AlertMessageLayer : public GameLayer
{
public:
	// Confirm
	static AlertMessageLayer *createConfirm(GameLayer *pLayer, const char *pInfo);
	//
	static AlertMessageLayer *createConfirm(GameLayer *pLayer, const char *pInfo, SEL_MenuHandler ConfirmSEL);

	// Cancel
	static AlertMessageLayer *createCancel(GameLayer *pLayer, const char *pInfo);

	// Confrim and Cancel
	static AlertMessageLayer *createConfirmAndCancel(GameLayer *pLayer, const char *pInfo);
	// Confrim and Cancel
	static AlertMessageLayer *createConfirmAndCancel(GameLayer *pLayer, const char *pInfo , SEL_MenuHandler confirmSEL);
	// Confrim and Cancel
	static AlertMessageLayer *createConfirmAndCancel(GameLayer *pLayer, const char *pInfo , SEL_MenuHandler confirmSEL, SEL_MenuHandler cancelSEL);

	static AlertMessageLayer *createConfirm(const char *pInfo);

    virtual ~AlertMessageLayer();

	virtual bool onTouchBegan(Touch *pTouch, Event *pEvent);
	virtual void onTouchEnded(Touch *pTouch, Event *pEvent);
    
    virtual void onEnter();
	virtual void onExit();

protected:

	AlertMessageLayer(GameScene *pGameScene);

private:

	void SetMessage( const char *pInfo1, Node *Node, SEL_MenuHandler ConfirmSEL, SEL_MenuHandler CancelSEL);
    
    Sprite *m_pBack;

	//换行Label
    Label *m_pShowLabel;
    
	// 防止直接引用 
	AlertMessageLayer(const AlertMessageLayer&);
    AlertMessageLayer& operator = (const AlertMessageLayer&);
};

class LoadLayer : public GameLayer
{
public:

	LoadLayer(GameScene *pGameScene);
	~LoadLayer();

	char m_szMessage[128];
	bool m_bShowMessage;

public:
	static LoadLayer * create( GameScene *pGameScene );
	static LoadLayer * create( GameScene *pGameScene , const char * szMessage );
	static LoadLayer * createConnnect( GameScene *pGameScene );
	static LoadLayer * createLoad( GameScene *pGameScene );
	static void removeLoadLayer(Scene *pGameScene);

	virtual void onEnter();
	virtual void onExit();

	virtual bool onTouchBegan(Touch *pTouch, Event *pEvent);
	virtual void onTouchEnded(Touch *pTouch, Event *pEvent);
};

//充值Loading
class LoadPayLayer : public GameLayer
{
public:

	LoadPayLayer(GameScene *pGameScene);
	~LoadPayLayer();
	char m_szMessage[128];
public:
	static LoadPayLayer * create( GameScene *pGameScene );
	static void removeLoadLayer(Scene *pGameScene = NULL);
	virtual void onEnter();
	virtual void onExit();
	virtual bool onTouchBegan(Touch *pTouch, Event *pEvent);
	virtual void onTouchEnded(Touch *pTouch, Event *pEvent);
};

#endif
