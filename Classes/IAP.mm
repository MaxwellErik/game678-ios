 

#import "IAP.h"
#import "cocos2d.h"
#import "NSBuy.h"

#define ProductID_IAP_1_1 @"6Yuan"//
#define ProductID_IAP_1_2 @"12Yuan" //
#define ProductID_IAP_1_3 @"18Yuan" //
#define ProductID_IAP_1_4 @"25Yuan" //
#define ProductID_IAP_1_5 @"30Yuan" //
#define ProductID_IAP_1_6 @"xt.gold.6" //
#define ProductID_IAP_1_7 @"xt.gold.7" //
#define ProductID_IAP_1_8 @"xt.gold.8" //

#define ProductID_IAP_2_1 @"com.mx.6w.coins"
#define ProductID_IAP_2_2 @"com.mx.18W.coins" //
#define ProductID_IAP_2_3 @"com.mx.30w.coins" //
#define ProductID_IAP_2_4 @"com.mx.68w.coins" //
#define ProductID_IAP_2_5 @"com.mx.168w.coins" //

@implementation IAP



-(id)init
{
    if ((self = [super init])) {
        //---------------------
        //----监听购买结果
        [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
    }
    return self;
}

-(void)buy:(int)type
{
    buyType = type;
    if ([SKPaymentQueue canMakePayments]) {
        [self RequestProductData];
        NSLog(@"允许程序内付费购买");
    }
    else
    {
        NSLog(@"不允许程序内付费购买");
        UIAlertView *alerView =  [[UIAlertView alloc] initWithTitle:@"Alert"
                                                            message:@"You can‘t purchase in app store（你没允许应用程序内购买）"
                                                           delegate:nil cancelButtonTitle:NSLocalizedString(@"Close（关闭）",nil) otherButtonTitles:nil];
        
        [alerView show];
        [alerView release];
        
    }
}

-(bool)CanMakePay
{
    return [SKPaymentQueue canMakePayments];
}

-(void)RequestProductData
{
    NSString *str = [NSString stringWithFormat:@"dd啊啊啊啊"];
    
    const char *szBuf = [str UTF8String];
    
    
    NSLog(@"---------请求对应的产品信息------------");
    NSArray *product = nil;
    switch (buyType) {
        case IAP_1_1:
            product=[[NSArray alloc] initWithObjects:ProductID_IAP_1_1,nil];
            break;
        case IAP_1_2:
            product=[[NSArray alloc] initWithObjects:ProductID_IAP_1_2,nil];
            break;
        case IAP_1_3:
            product=[[NSArray alloc] initWithObjects:ProductID_IAP_1_3,nil];
            break;
        case IAP_1_4:
            product=[[NSArray alloc] initWithObjects:ProductID_IAP_1_4,nil];
            break;
        case IAP_1_5:
            product=[[NSArray alloc] initWithObjects:ProductID_IAP_1_5,nil];
        case IAP_1_6:
            product=[[NSArray alloc] initWithObjects:ProductID_IAP_1_6,nil];
            break;
        case IAP_1_7:
            product=[[NSArray alloc] initWithObjects:ProductID_IAP_1_7,nil];
            break;
        case IAP_1_8:
            product=[[NSArray alloc] initWithObjects:ProductID_IAP_1_8,nil];
            break;
            
        case IAP_2_1:
            product=[[NSArray alloc] initWithObjects:ProductID_IAP_2_1,nil];
            break;
        case IAP_2_2:
            product=[[NSArray alloc] initWithObjects:ProductID_IAP_2_2,nil];
            break;
        case IAP_2_3:
            product=[[NSArray alloc] initWithObjects:ProductID_IAP_2_3,nil];
            break;
        case IAP_2_4:
            product=[[NSArray alloc] initWithObjects:ProductID_IAP_2_4,nil];
            break;
        case IAP_2_5:
            product=[[NSArray alloc] initWithObjects:ProductID_IAP_2_5,nil];
            break;
            
        default:
            break;
    }
    NSSet *nsset = [NSSet setWithArray:product];
    SKProductsRequest *request=[[SKProductsRequest alloc] initWithProductIdentifiers: nsset];
    request.delegate=self;
    [request start];
    [product release];
}
//<SKProductsRequestDelegate> 请求协议
//收到的产品信息
- (void)productsRequest:(SKProductsRequest *)request didReceiveResponse:(SKProductsResponse *)response{
    
    NSLog(@"-----------收到产品反馈信息--------------");
    NSArray *myProduct = response.products;
    NSLog(@"产品Product ID:%@",response.invalidProductIdentifiers);
    NSLog(@"产品付费数量: %lu", (unsigned long)[myProduct count]);
    // populate UI
    for(SKProduct *product in myProduct){
        NSLog(@"product info");
        NSLog(@"SKProduct 描述信息%@", [product description]);
        NSLog(@"产品标题 %@" , product.localizedTitle);
        NSLog(@"产品描述信息: %@" , product.localizedDescription);
        NSLog(@"价格: %@" , product.price);
        NSLog(@"Product id: %@" , product.productIdentifier);
    }
    SKPayment *payment = nil;
    switch (buyType) {
        case IAP_1_1:
            payment  = [SKPayment paymentWithProductIdentifier:ProductID_IAP_1_1];    //
            break;
        case IAP_1_2:
            payment  = [SKPayment paymentWithProductIdentifier:ProductID_IAP_1_2];    //
            break;
        case IAP_1_3:
            payment  = [SKPayment paymentWithProductIdentifier:ProductID_IAP_1_3];    //
            break;
        case IAP_1_4:
            payment  = [SKPayment paymentWithProductIdentifier:ProductID_IAP_1_4];    //
            break;
        case IAP_1_5:
            payment  = [SKPayment paymentWithProductIdentifier:ProductID_IAP_1_5];    //
            break;
        case IAP_1_6:
            payment  = [SKPayment paymentWithProductIdentifier:ProductID_IAP_1_6];    //
            break;
        case IAP_1_7:
            payment  = [SKPayment paymentWithProductIdentifier:ProductID_IAP_1_7];    //
            break;
        case IAP_1_8:
            payment  = [SKPayment paymentWithProductIdentifier:ProductID_IAP_1_8];    //
            break;
            
        case IAP_2_1:
            payment  = [SKPayment paymentWithProductIdentifier:ProductID_IAP_2_1];    //
            break;
        case IAP_2_2:
            payment  = [SKPayment paymentWithProductIdentifier:ProductID_IAP_2_2];    //
            break;
        case IAP_2_3:
            payment  = [SKPayment paymentWithProductIdentifier:ProductID_IAP_2_3];    //
            break;
        case IAP_2_4:
            payment  = [SKPayment paymentWithProductIdentifier:ProductID_IAP_2_4];    //
            break;
        case IAP_2_5:
            payment  = [SKPayment paymentWithProductIdentifier:ProductID_IAP_2_5];    //
            break;

        default:
            break;
    }
    NSLog(@"---------发送购买请求------------");
    [[SKPaymentQueue defaultQueue] addPayment:payment];
    [request autorelease];
    
}
- (void)requestProUpgradeProductData
{
    NSLog(@"------请求升级数据---------");
    NSSet *productIdentifiers = [NSSet setWithObject:@"com.productid"];
    SKProductsRequest* productsRequest = [[SKProductsRequest alloc] initWithProductIdentifiers:productIdentifiers];
    productsRequest.delegate = self;
    [productsRequest start];
    
}
//弹出错误信息
- (void)request:(SKRequest *)request didFailWithError:(NSError *)error{
    NSLog(@"-------弹出错误信息----------");
    UIAlertView *alerView =  [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Alert",NULL) message:[error localizedDescription]
                                                       delegate:nil cancelButtonTitle:NSLocalizedString(@"Close",nil) otherButtonTitles:nil];
    [alerView show];
    [alerView release];
}

-(void) requestDidFinish:(SKRequest *)request
{
    
    NSLog(@"----------反馈信息结束--------------");
    
}

-(void) PurchasedTransaction: (SKPaymentTransaction *)transaction{
    NSLog(@"-----PurchasedTransaction----");
    NSArray *transactions =[[NSArray alloc] initWithObjects:transaction, nil];
    [self paymentQueue:[SKPaymentQueue defaultQueue] updatedTransactions:transactions];
    [transactions release];
}

//<SKPaymentTransactionObserver> 千万不要忘记绑定，代码如下：
//----监听购买结果

- (void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray *)transactions//交易结果
{
    NSLog(@"-----paymentQueue--------");
    for (SKPaymentTransaction *transaction in transactions)
    {
        switch (transaction.transactionState)
        {
            case SKPaymentTransactionStatePurchased://交易完成
            {
                [self completeTransaction:transaction];
                NSLog(@"-----交易完成 --------");

                NSString *str = [[NSString alloc] initWithData:transaction.transactionReceipt encoding:NSUTF8StringEncoding];
                const char *pJson = [str UTF8String];
                CppBuy::sharedCppBuy()->OnBuyFinish(true, pJson);
                NSLog(@"JSON=%@", str);
                break;
            }
            case SKPaymentTransactionStateFailed://交易失败
            {
                [self failedTransaction:transaction];
                NSLog(@"%@", transaction.error.description);
                NSLog(@"-----交易失败 --------");
                
                //下面如果显示错误描述，请填写[transaction.error.description UTF8String]
                CppBuy::sharedCppBuy()->OnBuyFinish(false, "购买失败了，请稍后重试。");
                break;
            }
            case SKPaymentTransactionStateRestored://已经购买过该商品
            {
                [self restoreTransaction:transaction];
                NSLog(@"-----已经购买过该商品 --------");
            }
            case SKPaymentTransactionStatePurchasing:      //商品添加进列表
            {
                NSLog(@"-----商品添加进列表 --------");
                break;
            }
            default:
                break;
        }
    }
}
- (void) completeTransaction: (SKPaymentTransaction *)transaction

{
    NSLog(@"-----completeTransaction--------");
    // Your application should implement these two methods.
    NSString *product = transaction.payment.productIdentifier;
    if ([product length] > 0) {
        
        NSArray *tt = [product componentsSeparatedByString:@"."];
        NSString *bookid = [tt lastObject];
        if ([bookid length] > 0) {
            [self recordTransaction:bookid];
            [self provideContent:bookid];
        }
    }
    
    // Remove the transaction from the payment queue.
    
    [[SKPaymentQueue defaultQueue] finishTransaction: transaction];
    
}

//记录交易
-(void)recordTransaction:(NSString *)product{
    NSLog(@"-----记录交易--------");
}

//处理下载内容
-(void)provideContent:(NSString *)product{
    NSLog(@"-----下载--------");
}

- (void) failedTransaction: (SKPaymentTransaction *)transaction{
    NSLog(@"失败");
    if (transaction.error.code != SKErrorPaymentCancelled)
    {
    }
    [[SKPaymentQueue defaultQueue] finishTransaction: transaction];
    
    
}
-(void) paymentQueueRestoreCompletedTransactionsFinished: (SKPaymentTransaction *)transaction{
    
}

- (void) restoreTransaction: (SKPaymentTransaction *)transaction

{
    NSLog(@" 交易恢复处理");
    
}

-(void) paymentQueue:(SKPaymentQueue *) paymentQueue restoreCompletedTransactionsFailedWithError:(NSError *)error{
    NSLog(@"-------paymentQueue----");
}


#pragma mark connection delegate
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    NSLog(@"%@",  [[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding] autorelease]);
}
- (void)connectionDidFinishLoading:(NSURLConnection *)connection{
    
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response{
    switch([(NSHTTPURLResponse *)response statusCode]) {
        case 200:
        case 206:
            break;
        case 304:
            break;
        case 400:
            break;
        case 404:
            break;
        case 416:
            break;
        case 403:
            break;
        case 401:
        case 500:
            break;
        default:
            break;
    }
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    NSLog(@"test");
}

-(void)dealloc
{
    [[SKPaymentQueue defaultQueue] removeTransactionObserver:self];//解除监听
    [super dealloc];
}

@end


