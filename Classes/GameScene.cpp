﻿#include "GameScene.h"

GameScene::GameScene()
{

}

GameScene::~GameScene()
{

}

GameScene *GameScene::create(void)
{
    GameScene *pGameScene = new GameScene();

    if(pGameScene && pGameScene->init())
    {
        pGameScene->autorelease();
        return pGameScene;
    }
    else
    {
        CC_SAFE_DELETE(pGameScene);
        return NULL;
    }
}

// 清除 Alert Message Layer
void GameScene::RemoveAlertMessageLayer(void)
{
    Vector<Node*> children = this->getChildren();
	if(!children.empty() && (this->getChildrenCount() > 0))
    {
        Ref* pObject = NULL;

        for(Vector<Node*>::iterator it = children.begin(); it != children.end(); it++)
        {
            pObject = *it;
			if(pObject != NULL)
			{
				AlertMessageLayer *ptempAlertMessageLayer = dynamic_cast<AlertMessageLayer*>(pObject);
				if (ptempAlertMessageLayer != NULL)
				{
					this->removeChild(ptempAlertMessageLayer);
				}
			}
        }
    }
}
