#include "GameRank.h"
#include "LobbySocketSink.h"
#include "ClientSocketSink.h"
#include "GameLayerMove.h"
#include "Convert.h"

#define	Btn_Close			1
#define	Btn_InfoClose		2

GameRank::GameRank(GameScene *pGameScene):GameLayer(pGameScene)
{
    m_UserInfoBg = nullptr;
    m_listView = nullptr;
}

GameRank::~GameRank()
{

}

bool GameRank::init()
{
	if ( !Layer::init())
	{
		return false;
	}
	GameLayer::onEnter();
	setLocalZOrder(100);
    Tools::addSpriteFrame("rankUI.plist");

    auto sp = ui::Scale9Sprite::create("Common/bg_gray.png");
    sp->setContentSize(Size(1920,1080));
    auto pMenuItem = MenuItemSprite::create(sp, sp, sp);
    m_ColorBg = Menu::create(pMenuItem, nullptr);
    m_ColorBg->setPosition(Vec2(960, 540));
    addChild(m_ColorBg);
    
    Sprite* bg = Sprite::createWithSpriteFrameName("bg_rank.png");
    bg->setPosition(_STANDARD_SCREEN_CENTER_);
    addChild(bg, 10);
    
    Menu* closeBtn = CreateButton("btn_close", Vec2(1610, 900),Btn_Close);
    addChild(closeBtn,10);
    
    m_listView = ui::ListView::create();
    m_listView->setContentSize(Size(1220, 560));
    m_listView->setDirection(ui::ScrollView::Direction::VERTICAL);
    m_listView->setPosition(Vec2(350,150));
    addChild(m_listView, 10);
    
    m_UserInfoBg = Sprite::createWithSpriteFrameName("bg_playerInfo.png");
    m_UserInfoBg->setVisible(false);
    m_UserInfoBg->setPosition(_STANDARD_SCREEN_CENTER_);
    addChild(m_UserInfoBg, 100);
    
    Color3B c = Color3B(216, 137, 254);
    Label* nickName = Label::createWithSystemFont("", _GAME_FONT_NAME_1_, 40);
    nickName->setAnchorPoint(Vec2(0,0.5f));
    nickName->setPosition(Vec2(260, 420));
    nickName->setColor(c);
    m_UserInfoBg->addChild(nickName, 0, 10);
    
    Label* gameID = Label::createWithSystemFont("", _GAME_FONT_NAME_1_, 40);
    gameID->setAnchorPoint(Vec2(0,0.5f));
    gameID->setPosition(Vec2(260, 336));
    gameID->setColor(c);
    m_UserInfoBg->addChild(gameID, 0, 11);
    
    Label* sex = Label::createWithSystemFont("", _GAME_FONT_NAME_1_, 40);
    sex->setAnchorPoint(Vec2(0,0.5f));
    sex->setPosition(Vec2(260, 254));
    sex->setColor(c);
    m_UserInfoBg->addChild(sex, 0, 12);
    
    Label* info = Label::createWithSystemFont("", _GAME_FONT_NAME_1_, 40);
    info->setDimensions(660,100);
    info->setColor(c);
    info->setVerticalAlignment(cocos2d::TextVAlignment::TOP);
    info->setAlignment(cocos2d::TextHAlignment::LEFT);
    info->setAnchorPoint(Vec2(0,0.5f));
    info->setPosition(Vec2(260, 110));
    m_UserInfoBg->addChild(info, 0, 13);
    
    Menu* close = CreateButton("btn_close", Vec2(980, 580),Btn_InfoClose);
    m_UserInfoBg->addChild(close);
    
	setTouchEnabled(true);
    auto listener = EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(true);
    listener->onTouchBegan = CC_CALLBACK_2(GameRank::onTouchBegan, this);
    listener->onTouchMoved = CC_CALLBACK_2(GameRank::onTouchMoved, this);
    listener->onTouchEnded = CC_CALLBACK_2(GameRank::onTouchEnded, this);
    Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(listener, this);
	return true;
}

void GameRank::showUserInfo(int rank)
{
    Label* nickName = dynamic_cast<Label*>(m_UserInfoBg->getChildByTag(10));
    if (nickName)
        nickName->setString(gbk_utf8(m_rankInfo[rank].szNickName));
    
    string idStr = StringUtils::toString(m_rankInfo[rank].dwGameID);
    Label* gameID = dynamic_cast<Label*>(m_UserInfoBg->getChildByTag(11));
    if (gameID)
        gameID->setString(idStr);

    string sexStr = GENDER_BOY == m_rankInfo[rank].byGender ? "男" : "女";
    Label* sex = dynamic_cast<Label*>(m_UserInfoBg->getChildByTag(12));
    if (sex)
        sex->setString(sexStr);
    
    Label* info = dynamic_cast<Label*>(m_UserInfoBg->getChildByTag(13));
    if (info)
        info->setString(gbk_utf8(m_rankInfo[rank].szUnderWrite));

    m_ColorBg->setLocalZOrder(20);
    m_UserInfoBg->setVisible(true);
}

void GameRank::menuCallback(Ref* pSender, ui::Widget::TouchEventType type)
{
    ui::Button *btn = (ui::Button *)pSender;
    if (type == ui::Widget::TouchEventType::BEGAN)
    {
        btn->setScale(1.03f);
    }
    else if (type == ui::Widget::TouchEventType::MOVED)
    {
        btn->setScale(1.0f);
    }
    else if (type == ui::Widget::TouchEventType::ENDED)
    {
        btn->setScale(1.0f);
        showUserInfo(btn->getTag());
    }
    else if (type == ui::Widget::TouchEventType::CANCELED)
    {
        btn->setScale(1.0f);
    }
}

void GameRank::setRankData(CMD_RankingQuery_Result* rankData)
{
    m_listView->removeAllItems();
    m_rankInfo.clear();
    Color3B c = Color3B(97,  8,  178);
    int rankCount = sizeof(*rankData) / sizeof(tagRankingInfo);
    for (int i = 0; i < rankCount; i++)
    {
        tagRankingInfo info = rankData->info[i];
        m_rankInfo[info.llRank] = info;
        
        ui::Layout* custom_item = ui::Layout::create();
        custom_item->setContentSize(Size(1220, 110));
        
        ui::Button* btn = ui::Button::create("DaTing/label_rank_info.png","DaTing/label_rank_info.png");
        btn->addTouchEventListener(CC_CALLBACK_2(GameRank::menuCallback,this));
        btn->setTag(info.llRank);
        btn->setPosition(Vec2(610, 55));
        custom_item->addChild(btn);
        
        //名次
        if (info.llRank <= 3)
        {
            string str = "rank_no" + StringUtils::toString(info.llRank) + ".png";
            Sprite* rankNum = Sprite::createWithSpriteFrameName(str);
            rankNum->setPosition(Vec2(125, 55));
            custom_item->addChild(rankNum);
        }
        else
        {
            LabelAtlas* rankNum = LabelAtlas::create(StringUtils::toString(info.llRank), "Common/rank_number.png", 33, 45, '0');
            rankNum->setPosition(Vec2(125, 55));
            rankNum->setAnchorPoint(Vec2(0.5f, 0.5f));
            custom_item->addChild(rankNum);
        }
        
        //游戏ID
        string idStr = StringUtils::toString(info.dwGameID);
        Label *gameID = Label::createWithSystemFont(idStr, _GAME_FONT_NAME_1_, 40);
        gameID->setPosition(Vec2(350, 55));
        gameID->setColor(c);
        custom_item->addChild(gameID);
        
        //昵称
        std::string szTempTitle = gbk_utf8(info.szNickName);
        Label* nickname = Label::createWithSystemFont(szTempTitle.c_str(),_GAME_FONT_NAME_1_,40);
        nickname->setPosition(Vec2(690, 55));
        nickname->setColor(c);
        custom_item->addChild(nickname);
        
        string goldStr = StringUtils::toString(info.llScore);
        Label* temp = Label::createWithSystemFont(goldStr,_GAME_FONT_NAME_1_,40);
        temp->setAnchorPoint(Vec2(0,0.5f));
        temp->setColor(_GAME_FONT_COLOR_5_);
        temp->setPosition(Vec2(890, 55));
        custom_item->addChild(temp);
        m_listView->pushBackCustomItem(custom_item);
    }
}

void GameRank::callbackBt( Ref *pSender )
{
    Node *pNode = (Node *)pSender;
    switch (pNode->getTag())
    {
        case Btn_Close:
        {
            SoundUtil::sharedEngine()->playEffect("buttonMusic");
            m_pGameScene->removeChild(this);
            break;
        }
        case Btn_InfoClose:
        {
            SoundUtil::sharedEngine()->playEffect("buttonMusic");
            m_ColorBg->setLocalZOrder(0);
            m_UserInfoBg->setVisible(false);
        }
    }
}

Menu* GameRank::CreateButton(std::string szBtName ,const Vec2 &p , int tag)
{
    Menu *pBT = Tools::Button(StrToChar(szBtName+"_normal.png") , StrToChar(szBtName+"_click.png") , p, this , menu_selector(GameRank::callbackBt) , tag);
    return pBT;
}

GameRank* GameRank::create(GameScene *pGameScene)
{
	GameRank* temp = new GameRank(pGameScene);
	if(temp && temp->init())
	{
		temp->autorelease();
		return temp;
	}
	else
	{
		CC_SAFE_DELETE(temp);
		return NULL;
	}
}

void GameRank::onEnter()
{
	GameLayer::onEnter();
}

void GameRank::onExit()
{
	GameLayer::onExit();
    Tools::removeSpriteFrameCache("rankUI.plist");
}
