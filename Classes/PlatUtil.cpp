#include "PlatUtil.h"
#include "net/TcpManager.h"
#include "net/MsgManager.h"
#include "utils/Utils.h"
#include "quick-src/extra/crypto/CCCrypto.h"
#include "utils/utf8cpp/utf8.h"
#include "luaTargPool.h"
#include "CCLuaEngine.h"
extern "C"{
#include <lua.h>
#include <lualib.h>
#include <lauxlib.h>
}

#include "external/unzip/unzip.h"
#include <zlib.h>
#include "cocos2d.h"
USING_NS_CC;

#define ZIPBUFFER_SIZE    8192
#define ZIPMAX_FILENAME   512

#if CC_PLATFORM_ANDROID == CC_TARGET_PLATFORM
#include "android/jni/Java_org_cocos2dx_lib_Cocos2dxHelper.h"
#endif

USING_NS_CC_EXTRA;

void PlatUtil::connectServer(UINT clientId, string ip, UINT port, UINT heartDelayTime)    //连接服务器
{
	log("connectServer clienId:%d  ip:%s  port:%d", clientId, ip.c_str(), port);
    auto client = TcpManager::instance()->addClient(clientId);
    if (heartDelayTime > 0)
        client->setHeartDelayTime(heartDelayTime);
    client->connect(ip, port);
}

void PlatUtil::sendMessage(UINT clientId, NetMessage *msg)   //发送协议
{
	if (msg->getMsgLen() == 0)
	{
		MsgManager::instance()->sendMessage(clientId, msg->getMainID(), msg->getSubId());
	}
	else
	{
		MsgManager::instance()->sendMessage(clientId, msg->getMainID(), msg->getSubId(), (char*)msg->_data, msg->getMsgLen());
	}	
}

void PlatUtil::Utf8ToUnicode(CodeChange *str)      //字符串编码转换
{
	Utils::Utf8ToUnicode(str);
}

string PlatUtil::UnicodeToUtf8(CodeChange *str)		//字符换编码转换
{
	return Utils::UnicodeToUtf8(str);
}

string PlatUtil::getMD5String(string str)
{
	return Crypto::MD5String((char*)str.c_str(), str.length());
}

void PlatUtil::closeServer(UINT clientId)
{
	TcpManager::instance()->delClient(clientId);
}

void PlatUtil::setMessageParsePerFrame(UINT data)
{
	MsgManager::instance()->setParseCountPerFrame(data);
}

bool PlatUtil::uncompressZip(string zipFilePath, string dirPath)
{
	// Open the zip file
	string outFileName = zipFilePath;
	unzFile zipfile = unzOpen(outFileName.c_str());
	if (!zipfile)
	{
		CCLOG("can not open downloaded zip file %s", outFileName.c_str());
		return false;
	}

	// Get info about the zip file
	unz_global_info global_info;
	if (unzGetGlobalInfo(zipfile, &global_info) != UNZ_OK)
	{
		CCLOG("can not read file global info of %s", outFileName.c_str());
		unzClose(zipfile);
		return false;
	}

	// Buffer to hold data read from the zip file
	char readBuffer[ZIPBUFFER_SIZE];

	CCLOG("start uncompressing");

	// Loop to extract all files.
	uLong i;
	for (i = 0; i < global_info.number_entry; ++i)
	{
		// Get info about current file.
		unz_file_info fileInfo;
		char fileName[ZIPMAX_FILENAME];
		if (unzGetCurrentFileInfo(zipfile,
			&fileInfo,
			fileName,
			ZIPMAX_FILENAME,
			nullptr,
			0,
			nullptr,
			0) != UNZ_OK)
		{
			CCLOG("can not read file info");
			unzClose(zipfile);
			return false;
		}

		const string fullPath = dirPath + fileName;

		// Check if this entry is a directory or a file.
		const size_t filenameLength = strlen(fileName);
		if (fileName[filenameLength - 1] == '/')
		{
			// Entry is a direcotry, so create it.
			// If the directory exists, it will failed scilently.
			if (!createDirectory(fullPath.c_str()))
			{
				CCLOG("can not create directory %s", fullPath.c_str());
				unzClose(zipfile);
				return false;
			}
		}
		else
		{
			//There are not directory entry in some case.
			//So we need to test whether the file directory exists when uncompressing file entry
			//, if does not exist then create directory
			const string fileNameStr(fileName);

			size_t startIndex = 0;

			size_t index = fileNameStr.find("/", startIndex);

			while (index != std::string::npos)
			{
				const string dir = dirPath + fileNameStr.substr(0, index);

				FILE *out = fopen(dir.c_str(), "r");

				if (!out)
				{
					if (!createDirectory(dir.c_str()))
					{
						CCLOG("can not create directory %s", dir.c_str());
						unzClose(zipfile);
						return false;
					}
					else
					{
						CCLOG("create directory %s", dir.c_str());
					}
				}
				else
				{
					fclose(out);
				}

				startIndex = index + 1;

				index = fileNameStr.find("/", startIndex);

			}



			// Entry is a file, so extract it.

			// Open current file.
			if (unzOpenCurrentFile(zipfile) != UNZ_OK)
			{
				CCLOG("can not open file %s", fileName);
				unzClose(zipfile);
				return false;
			}

			// Create a file to store current file.
			FILE *out = fopen(fullPath.c_str(), "wb");
			if (!out)
			{
				CCLOG("can not open destination file %s", fullPath.c_str());
				unzCloseCurrentFile(zipfile);
				unzClose(zipfile);
				return false;
			}

			// Write current file content to destinate file.
			int error = UNZ_OK;
			do
			{
				error = unzReadCurrentFile(zipfile, readBuffer, ZIPBUFFER_SIZE);
				if (error < 0)
				{
					CCLOG("can not read zip file %s, error code is %d", fileName, error);
					unzCloseCurrentFile(zipfile);
					unzClose(zipfile);
					return false;
				}

				if (error > 0)
				{
					fwrite(readBuffer, error, 1, out);
				}
			} while (error > 0);

			fclose(out);
		}

		unzCloseCurrentFile(zipfile);

		// Goto next entry listed in the zip file.
		if ((i + 1) < global_info.number_entry)
		{
			if (unzGoToNextFile(zipfile) != UNZ_OK)
			{
				CCLOG("can not read next file");
				unzClose(zipfile);
				return false;
			}
		}
	}

	CCLOG("end uncompressing");
	unzClose(zipfile);

	return true;
}

/*
* Create a direcotry is platform depended.
*/
bool PlatUtil::createDirectory(const char *path)
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WINRT) || (CC_TARGET_PLATFORM == CC_PLATFORM_WP8)
	return FileUtils::getInstance()->createDirectory(_storagePath.c_str());
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
	BOOL ret = CreateDirectoryA(path, nullptr);
	if (!ret && ERROR_ALREADY_EXISTS != GetLastError())
	{
		return false;
	}
	return true;
#else
	mode_t processMask = umask(0);
	int ret = mkdir(path, S_IRWXU | S_IRWXG | S_IRWXO);
	umask(processMask);
	if (ret != 0 && (errno != EEXIST))
	{
		return false;
	}

	return true;
#endif


}


void PlatUtil::onMessage(UINT mainCmdId, UINT subCmdId, NetMessage msg)   //协议下行
{
	auto engine = LuaEngine::getInstance();
	lua_State* L = engine->getLuaStack()->getLuaState();
	string strPath = FileUtils::getInstance()->fullPathForFilename("src/sdk/CTLua.lua");
	engine->executeScriptFile(strPath.c_str());

	luaTargPool ArgPoolObj;
	ArgPoolObj.AddArg(msg);
	ArgPoolObj.CallLua(L, "onMessage", ArgPoolObj);
}

void PlatUtil::onNetConnected(UINT clientId, bool bConnect)   //服务器连接结果
{
	auto engine = LuaEngine::getInstance();
	lua_State* L = engine->getLuaStack()->getLuaState();
	string strPath = FileUtils::getInstance()->fullPathForFilename("src/sdk/CTLua.lua");
	engine->executeScriptFile(strPath.c_str());

	luaTargPool ArgPoolObj;
	ArgPoolObj.AddArg(clientId);  
	ArgPoolObj.AddArg(bConnect); 
	ArgPoolObj.CallLua(L, "onNetConnected", ArgPoolObj);
    
//    lua_getglobal(L, "onNetConnected");
//    lua_pushinteger(L, clientId);
//    lua_pushboolean(L, bConnect);
//    lua_pcall(L, 2, 0, 0);
}

void PlatUtil::onNetClosed(UINT clientId)
{
	auto engine = LuaEngine::getInstance();
	lua_State* L = engine->getLuaStack()->getLuaState();
	string strPath = FileUtils::getInstance()->fullPathForFilename("src/sdk/CTLua.lua");
	engine->executeScriptFile(strPath.c_str());

	luaTargPool ArgPoolObj;
	ArgPoolObj.AddArg(clientId);  
	ArgPoolObj.CallLua(L, "onNetClosed", ArgPoolObj);
}
