
#ifndef __COCOS2D_X_EXTRA_H_
#define __COCOS2D_X_EXTRA_H_

#include "cocos2d.h"
#include <string>

#ifndef CC_LUA_ENGINE_ENABLED
#define CC_LUA_ENGINE_ENABLED 1
#endif

using namespace std;
using namespace cocos2d;

#define NS_CC_EXTRA_BEGIN namespace cocos2d { namespace extra {
#define NS_CC_EXTRA_END   }}
#define USING_NS_CC_EXTRA using namespace cocos2d::extra

#endif /* __COCOS2D_X_EXTRA_H_ */
