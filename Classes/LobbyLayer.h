﻿#ifndef _LOOBY_LAYER_H_
#define _LOOBY_LAYER_H_

#include "GameScene.h"
#include "cocos-ext.h"
#include "GameKind.h"
#include "GameTable.h"
#include "GameBank.h"
#include "GameBankPass.h"
#include "MainSetting.h"
#include "GameUserInfo.h"
#include "LoginLayer.h"

#define SettingBtn			1
#define AddMoneyBtn			2
#define BindBtn            3
#define AddSendBtn			4
#define AddFlashBtn			5
#define RankBtn             7

#define QuitRoom			8
#define KeFuBtn             9
#define EnterRoom			10
#define LockGou				11
#define LockNoGou			12
#define LockOk				13
#define LockCanCel			14
#define PassWordOk			15
#define PassWordCancel		16

#define BRGame_Enter		19
#define BD_Self_Mac			20
#define goldBgTag           26
#define goldLabelTag        27

static bool static_isFirstEnter = true;

struct RoomInfo
{
	int tga;
	Sprite* roombackSpr;
	Sprite* roomNum;
	Sprite* typeback;
	Sprite* nickname;
	tagGameServer gameserver;
};



class LobbyLayer : public GameLayer
{
public:
	 virtual bool init();  
	static LobbyLayer *create(GameScene *pGameScene);
	virtual ~LobbyLayer();
	virtual void onEnter();
	virtual void onExit();
	virtual void updateUserInfo(float deltaTime);

    virtual bool onTouchBegan(Touch *pTouch, Event *pEvent);
    virtual void onTouchMoved(Touch *pTouch, Event *pEvent){}
    virtual void onTouchEnded(Touch *pTouch, Event *pEvent){}
	virtual void onTouchCancelled(Touch *pTouch, Event *pEvent){}
	// TextField 触发
	virtual bool onTextFieldAttachWithIME(TextFieldTTF*pSender){return false;}
	virtual bool onTextFieldDetachWithIME(TextFieldTTF*pSender){return false;}
	virtual bool onTextFieldInsertText(TextFieldTTF *pSender, const char *text, int nLen){return true;}
	virtual bool onTextFieldDeleteBackward(TextFieldTTF *pSender, const char *delText, int nLen){return true;}
	// IME 触发
	virtual void keyboardWillShow(IMEKeyboardNotificationInfo &info){}
	virtual void keyboardDidShow(IMEKeyboardNotificationInfo &info){}
	virtual void keyboardWillHide(IMEKeyboardNotificationInfo &info){}
	virtual void keyboardDidHide(IMEKeyboardNotificationInfo &info){}

    void AddGameBoardCast();
    void LoadLoginLayer(float dt);
	void AddRoom(Node  *pView, Vec2 pos,tagGameServer ServerItem);
	void AddUserInfo(Node *pView);
	void AddText();
    void AddGameBroadcast(char *msg);
	void AddButton();
	Menu* CreateButton(std::string szBtName ,const Vec2 &p , int tag);
	void callbackBt(Ref* pObject);
	void setRcvLayer(GameLayer *pGameLayer);
	string AddCommaToNum(LONG Num);

	void UpdataLobbyScore();
	void FlashBtn();
    void showUserInfo();
    void sendRefreshBankScore();
    
	void SetBankDisable(bool ebale = false);
	void SetCheckPhone(bool _check);
    
    void OnIosRechargeSuccess(int payType);
	void UpdateUserData();
    void SendHeartData(float dt);
    void SendGameHeart(float dt);
    void SendBoardCastData(float dt);
    void OnSendFishFreecoin();
    void playBackMusic(float dt);
    
    void SendFishFreecoin(EventCustom* event);
    void changeAccount(EventCustom* event);
	void GoToLogin(Ref *pSender);
	void DialogConfirm(Ref *pSender);
	void DialogCancel(Ref *pSender);
    
    void StartGameHeart();
    void StopGameHeart();
    void SendHeart();
    void SendBoardCastReq();
    void GetServerAddress();
    void OpenBank();

	//进入房间
	void AddEnterRoomTime();
	void KillEnterRoomTime(float deltaTime);
	void KillEnterRoomTime(bool removeLoad = true);

	void AddEnterGameTime();
	void KillEnterGameTime(float deltaTime);
	void KillEnterGameTime();

	//显示绑定用户手机界面
	void UpdataGame();

	void GoToRoom(Ref *pSender);
    
    void modefiySuccess();
    
    void initGameUserInfo();
    
    void removeTips(Ref *sender);
    
    void OnRecvRankData(CMD_RankingQuery_Result* rank);
    
    void UpdateGameTableUserStatus(tagUserData * pUserData);
protected:	
	LobbyLayer(GameScene *pGameScene);

private:
	// 防止直接引用 
	LobbyLayer(const LobbyLayer&);
    LobbyLayer& operator = (const LobbyLayer&);
    
    Sprite*         m_netInfo;
    Sprite*         m_powerBg;
    Sprite*         m_power;
    Label*          m_timeLabel;
    
    GameUserInfo*   m_userInfo;
	GameLayer*		m_pRcvLayer;
	GameBank*		m_pBankUILayer;
    MainSetting*    m_Mainsetting;

	GameKind*		 m_GameKind;
	GameTable*		 m_GameTable;
    LoginLayer*      m_LoginLayer;
	GameBankPass*    m_BankPass;
    LockPassWord*    m_LockPassWord;
};

#endif
