﻿#include "AlertMessageLayer.h"
#include "Tools.h"
#include "ClientSocketSink.h"

#define _ALERT_MESSAGE_LOOP_TICKET_							(0.5f)

const Vec2 _ALERT_BT_POSITION_[3] =
{	
	// 确认键 取消键 同时
	Vec2(_STANDARD_SCREEN_CENTER_.x - 200, _STANDARD_SCREEN_CENTER_.y - 100),
	// 取消键 取消键 同时
	Vec2(_STANDARD_SCREEN_CENTER_.x + 200, _STANDARD_SCREEN_CENTER_.y - 100),
	// 取消键 取消键 单独
	Vec2(_STANDARD_SCREEN_CENTER_.x, _STANDARD_SCREEN_CENTER_.y - 100),
};

#define	_ALERT_MESSAGE_POINT_ (ccpx(_STANDARD_SCREEN_CENTER_.x, _STANDARD_SCREEN_CENTER_.y+45))

#ifdef _WIN32

#define NET_CONNECT_TEXT								"网络连接中"
#define LOAD_RESOURCE_TEXT								"正在加载资源"

#else

#define NET_CONNECT_TEXT								"\u7f51\u7edc\u8fde\u63a5\u4e2d"
#define LOAD_RESOURCE_TEXT								"\u6b63\u5728\u52a0\u8f7d\u8d44\u6e90"

#endif // _WIN32



AlertMessageLayer::AlertMessageLayer(GameScene *pGameScene)
	:GameLayer(pGameScene)
{
	// 设成 上层 显示
	this->setLocalZOrder(60000);

	this->m_pShowLabel = NULL;
}

// 提示
AlertMessageLayer::~AlertMessageLayer()
{
}

AlertMessageLayer *AlertMessageLayer::createConfirm(GameLayer *pLayer,const char *pInfo, SEL_MenuHandler ConfirmSEL)
{
	pLayer->m_pGameScene->RemoveAlertMessageLayer();

	AlertMessageLayer *pAlert = new AlertMessageLayer(pLayer->m_pGameScene);

	if(pAlert && pAlert->init())
	{
		pAlert->onEnter();
		pAlert->SetMessage(pInfo, pLayer, ConfirmSEL, NULL);

		pAlert->autorelease();

		return pAlert;
	}
	else
	{
		CC_SAFE_DELETE(pAlert);
		return NULL;
	}
}

// Cancel
AlertMessageLayer *AlertMessageLayer::createConfirm(GameLayer *pLayer, const char *pInfo)
{
	// 清除 Alert Message Layer
	pLayer->m_pGameScene->RemoveAlertMessageLayer();

	AlertMessageLayer *pAlert = new AlertMessageLayer(pLayer->m_pGameScene);

	if(pAlert && pAlert->init())
	{
		pAlert->onEnter();
		pAlert->SetMessage(pInfo, pLayer, menu_selector(GameLayer::DialogConfirm), NULL);

		pAlert->autorelease();

		return pAlert;
	}
	else
	{
		CC_SAFE_DELETE(pAlert);
		return NULL;
	}
}

AlertMessageLayer * AlertMessageLayer::createConfirm(const char *pInfo)
{
	GameScene *pGameScene = (GameScene *)Director::getInstance()->getRunningScene();
	// 清除 Alert Message Layer
	pGameScene->RemoveAlertMessageLayer();

	AlertMessageLayer *pAlert = new AlertMessageLayer(pGameScene);

	if(pAlert && pAlert->init())
	{
		pAlert->onEnter();
		pAlert->SetMessage(pInfo, pAlert, menu_selector(AlertMessageLayer::DialogConfirm), NULL);

		pAlert->autorelease();

		return pAlert;
	}
	else
	{
		CC_SAFE_DELETE(pAlert);
		return NULL;
	}
}

// Confrim and Cancel
AlertMessageLayer *AlertMessageLayer::createCancel(GameLayer *pLayer, const char *pInfo)
{
	// 清除 Alert Message Layer
	pLayer->m_pGameScene->RemoveAlertMessageLayer();

	AlertMessageLayer *pAlert = new AlertMessageLayer(pLayer->m_pGameScene);

	if(pAlert && pAlert->init())
	{
		pAlert->onEnter();
		pAlert->SetMessage(pInfo, pLayer, NULL, menu_selector(GameLayer::DialogCancel));

		pAlert->autorelease();

		return pAlert;
	}
	else
	{
		CC_SAFE_DELETE(pAlert);
		return NULL;
	}

}

AlertMessageLayer *AlertMessageLayer::createConfirmAndCancel(GameLayer *pLayer, const char *pInfo)
{
	return createConfirmAndCancel(pLayer , pInfo , menu_selector(GameLayer::DialogConfirm));
}

AlertMessageLayer * AlertMessageLayer::createConfirmAndCancel( GameLayer *pLayer, const char *pInfo , SEL_MenuHandler confirmSEL)
{
// 	// 清除 Alert Message Layer
	return AlertMessageLayer::createConfirmAndCancel(pLayer , pInfo , confirmSEL, menu_selector(GameLayer::DialogCancel));
}

AlertMessageLayer * AlertMessageLayer::createConfirmAndCancel( GameLayer *pLayer, const char *pInfo , SEL_MenuHandler confirmSEL, SEL_MenuHandler cancelSEL)
{
	// 清除 Alert Message Layer
	pLayer->m_pGameScene->RemoveAlertMessageLayer();

	AlertMessageLayer *pAlert = new AlertMessageLayer(pLayer->m_pGameScene);

	if(pAlert && pAlert->init())
	{
		pAlert->onEnter();
		pAlert->SetMessage(pInfo, pLayer, confirmSEL, cancelSEL);

		pAlert->autorelease();

		return pAlert;
	}
	else
	{
		CC_SAFE_DELETE(pAlert);
		return NULL;
	}
}

void AlertMessageLayer::onEnter()
{
	GameLayer::onEnter();
	setTouchEnabled(true);
    auto listener = EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(true);
    listener->onTouchBegan = CC_CALLBACK_2(AlertMessageLayer::onTouchBegan, this);
    listener->onTouchEnded = CC_CALLBACK_2(AlertMessageLayer::onTouchEnded, this);
     _eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);
}

void AlertMessageLayer::onExit()
{
	GameLayer::onExit();
}

void AlertMessageLayer::SetMessage(const char *pInfo, Node *pNode, SEL_MenuHandler ConfirmSEL, SEL_MenuHandler CancelSEL)
{
	//添加底板
    ui::Scale9Sprite* colorBg = ui::Scale9Sprite::create("Common/bg_gray.png");
    colorBg->setContentSize(Size(1920, 1080));
    colorBg->setPosition(_STANDARD_SCREEN_CENTER_);
    addChild(colorBg);

    // 设定 背景
    this->m_pBack = Sprite::createWithSpriteFrameName("bg_tongyongkuang2.png");
    this->m_pBack->setPosition(_STANDARD_SCREEN_CENTER_IN_PIXELS_);
    addChild(this->m_pBack);
    
    m_pShowLabel = Label::createWithSystemFont(pInfo, _GAME_FONT_NAME_2_, 46);
    m_pShowLabel->setPosition(Vec2(430, 270));
    m_pShowLabel->setAnchorPoint(Vec2(0.5f, 0.5f));
    m_pBack->addChild(m_pShowLabel);
    m_pShowLabel->setVerticalAlignment(cocos2d::TextVAlignment::TOP);
    m_pShowLabel->setAlignment(cocos2d::TextHAlignment::LEFT);

    if (m_pShowLabel->getContentSize().width > m_pBack->getContentSize().width - 20)
        m_pShowLabel->setDimensions(m_pBack->getContentSize().width - 20,0);
    
    
    
	// 确认 取消 提示
	if(ConfirmSEL && CancelSEL)
	{  
		// 多行
		Menu *ptempConfirmButton = Tools::Button("btn_queding_normal.png", "btn_queding_normal.png", _ALERT_BT_POSITION_[0], pNode, ConfirmSEL);
		addChild(ptempConfirmButton);

		Menu *ptempCancelButton = Tools::Button("btn_quxiao_normal.png", "btn_quxiao_normal.png", _ALERT_BT_POSITION_[1], pNode, CancelSEL);
		addChild(ptempCancelButton);
	} 
	// 确认 提示
	else if(ConfirmSEL)
	{   
		Menu *ptempConfirmButton = Tools::Button("btn_queding_normal.png", "btn_queding_normal.png", _ALERT_BT_POSITION_[2], pNode, ConfirmSEL);
		addChild(ptempConfirmButton);
	} 
	// 取消 提示
	else if(CancelSEL)
	{  
		Menu *ptempCancelButton = Tools::Button("btn_quxiao_normal.png", "btn_quxiao_normal.png", _ALERT_BT_POSITION_[2], pNode, CancelSEL);
		addChild(ptempCancelButton);
	} 

}

bool AlertMessageLayer::onTouchBegan(Touch *pTouch, Event *pEvent )
{
	return true;
}

void AlertMessageLayer::onTouchEnded(Touch *pTouch, Event *pEvent )
{
	
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void LoadLayer::onEnter()
{
	GameLayer::onEnter();
	setLocalZOrder(100000);
    
    g_GlobalUnits.m_isLoadLayerShow = true;
    ui::Scale9Sprite* colorBg = ui::Scale9Sprite::create("Common/bg_gray.png");
    colorBg->setContentSize(Size(1920, 1080));
    colorBg->setPosition(_STANDARD_SCREEN_CENTER_);
    addChild(colorBg);

	if (m_bShowMessage)
	{
		std::string szTempTitle = m_szMessage;
#ifdef _WIN32
		Tools::GBKToUTF8(szTempTitle , "gb2312" , "utf-8");
#endif // _WIN32
		Label *pLable = Label::createWithSystemFont(szTempTitle.c_str(), _GAME_FONT_NAME_1_, 46);
		
		pLable->setPosition(ccpx(650 , 300));
		addChild(pLable,10000);
	}
	
	Sprite *pRing = Sprite::createWithSpriteFrameName("net_connect_bigring.png");
	pRing->setPosition(_STANDARD_SCREEN_CENTER_IN_PIXELS_);
	addChild(pRing);
	pRing->runAction(RepeatForever::create(RotateBy::create(1, -360)));
	setTouchEnabled(true);
    
    auto listener = EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(true);
    listener->onTouchBegan = CC_CALLBACK_2(LoadLayer::onTouchBegan, this);
    listener->onTouchEnded = CC_CALLBACK_2(LoadLayer::onTouchEnded, this);
    _eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);
}

void LoadLayer::onExit()
{
    g_GlobalUnits.m_isLoadLayerShow = false;
	GameLayer::onExit();
	stopAllActions();
}

LoadLayer::LoadLayer( GameScene *pGameScene ):GameLayer(pGameScene)
{
	m_bShowMessage = false;
	memset(m_szMessage ,0, sizeof(m_szMessage));
}


LoadLayer::~LoadLayer()
{
}


LoadLayer * LoadLayer::create( GameScene *pGameScene )
{
	removeLoadLayer(pGameScene);
	LoadLayer *pLayer = new LoadLayer(pGameScene);

	if(pLayer && pLayer->init())
	{
		pLayer->autorelease();
		pLayer->onEnter();
		return pLayer;
	}
	else
	{
		CC_SAFE_DELETE(pLayer);
		return NULL;
	}
}

LoadLayer * LoadLayer::create( GameScene *pGameScene , const char * szMessage )
{
	removeLoadLayer(pGameScene);
	LoadLayer *pLayer = new LoadLayer(pGameScene);

	if(pLayer && pLayer->init())
	{
		pLayer->autorelease();
		strcpy(pLayer->m_szMessage , szMessage);
		pLayer->m_bShowMessage = true;
		pLayer->onEnter();
		return pLayer;
	}
	else
	{
		CC_SAFE_DELETE(pLayer);
		return NULL;
	}
    
	return pLayer;
}

LoadLayer * LoadLayer::createConnnect( GameScene *pGameScene )
{
	return create(pGameScene , NET_CONNECT_TEXT);
}

LoadLayer * LoadLayer::createLoad( GameScene *pGameScene )
{
	return create(pGameScene , "");
}

void LoadLayer::removeLoadLayer(Scene *pGameScene)
{
    Vector<Node*> children =pGameScene->getChildren();
    
	if((!children.empty()) && (children.size() > 0))
	{
		Ref* pObject = NULL;
        for (Vector<Node*>::iterator it = children.begin(); it != children.end(); it++)
        {
            pObject = *it;
			if(pObject != NULL)
			{
				LoadLayer* pLayer = dynamic_cast<LoadLayer *>(pObject);
				if (pLayer != NULL)
				{
					pGameScene->removeChild(pLayer);
				}
			}
		}
	}
}

bool LoadLayer::onTouchBegan(Touch *pTouch, Event *pEvent )
{
	return true;
}

void LoadLayer::onTouchEnded(Touch *pTouch, Event *pEvent )
{

}

void LoadPayLayer::onEnter()
{
	GameLayer::onEnter();
	
    ui::Scale9Sprite* colorBg = ui::Scale9Sprite::create("Common/bg_gray.png");
    colorBg->setContentSize(Size(1920, 1080));
    colorBg->setPosition(_STANDARD_SCREEN_CENTER_);
    addChild(colorBg);
    
	Sprite *pBG = Sprite::createWithSpriteFrameName("net_connect_bg.png");
	pBG->setPosition(_STANDARD_SCREEN_CENTER_IN_PIXELS_);
	addChild(pBG);


	Sprite *pRing = Sprite::createWithSpriteFrameName("net_connect_bigring.png");
	pRing->setPosition(_STANDARD_SCREEN_CENTER_IN_PIXELS_);
	addChild(pRing);
	pRing->runAction(RepeatForever::create(RotateBy::create(1, -360)));
	
    setTouchEnabled(true);
    auto listener = EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(true);
    listener->onTouchBegan = CC_CALLBACK_2(LoadPayLayer::onTouchBegan, this);
    listener->onTouchMoved = CC_CALLBACK_2(LoadPayLayer::onTouchMoved, this);
    listener->onTouchEnded = CC_CALLBACK_2(LoadPayLayer::onTouchEnded, this);
    Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(listener, this);
}

void LoadPayLayer::onExit()
{
	GameLayer::onExit();
	stopAllActions();
}

LoadPayLayer::LoadPayLayer( GameScene *pGameScene ):GameLayer(pGameScene)
{
	memset(m_szMessage ,0, sizeof(m_szMessage));
}


LoadPayLayer::~LoadPayLayer()
{
}


LoadPayLayer * LoadPayLayer::create( GameScene *pGameScene )
{
	removeLoadLayer(pGameScene);
	LoadPayLayer *pLayer = new LoadPayLayer(pGameScene);

	if(pLayer && pLayer->init())
	{
		pLayer->autorelease();
		pLayer->onEnter();
		return pLayer;
	}
	else
	{
		CC_SAFE_DELETE(pLayer);
		return NULL;
	}
}


void LoadPayLayer::removeLoadLayer(Scene *pGameScene)
{
	if (pGameScene == NULL)
	{
		pGameScene = Director::getInstance()->getRunningScene();
	}
    Vector<Node*> children = pGameScene->getChildren();
	if((!children.empty()) && (children.size() > 0))
	{
		Ref* pObject = NULL;

        for (Vector<Node*>::iterator it = children.begin(); it != children.end(); it++)
        {
            pObject = *it;
			if(pObject != NULL)
			{
				LoadPayLayer* pLayer = dynamic_cast<LoadPayLayer *>(pObject);
				if (pLayer != NULL)
				{
					pGameScene->removeChild(pLayer);
				}
			}
		}
	}
}

bool LoadPayLayer::onTouchBegan(Touch *pTouch, Event *pEvent )
{
	return true;
}

void LoadPayLayer::onTouchEnded(Touch *pTouch, Event *pEvent )
{

}
