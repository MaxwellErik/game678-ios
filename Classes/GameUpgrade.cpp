#include "GameUpgrade.h"
#include "LobbySocketSink.h"
#include "ClientSocketSink.h"
#include "LocalDataUtil.h"
#include "GameLayerMove.h"
#include "GameFishSendEvent.h"

#define	Btn_Close			1
#define Btn_upgrade			2


GameUpgrade::GameUpgrade(GameScene *pGameScene):GameLayer(pGameScene)
{
}

GameUpgrade::~GameUpgrade()
{

}


Menu* GameUpgrade::CreateButton(std::string szBtName ,const Vec2 &p , int tag )
{
	Menu *pBT = Tools::Button(StrToChar(szBtName+"_normal.png") , StrToChar(szBtName+"_click.png") , p, this , menu_selector(GameUpgrade::callbackBt) , tag);
	return pBT;
}

bool GameUpgrade::init()
{
	if ( !Layer::init() )
	{
		return false;
	}
	GameLayer::onEnter();
	setLocalZOrder(100);

    ui::Scale9Sprite* colorBg = ui::Scale9Sprite::create("Common/bg_gray.png");
    colorBg->setContentSize(Size(1920, 1080));
    colorBg->setPosition(_STANDARD_SCREEN_CENTER_);
    addChild(colorBg);
    
    Sprite* bg = Sprite::createWithSpriteFrameName("bg_tongyongkuang1.png");
    bg->setPosition(_STANDARD_SCREEN_CENTER_);
    addChild(bg);
    
    Sprite *title = Sprite::createWithSpriteFrameName("title_zhongyaotishi.png");
    title->setPosition(Vec2(535, 630));
    bg->addChild(title);
    
    Label* text = Label::createWithSystemFont("为了您更好的体验游戏及账号的安全,", _GAME_FONT_NAME_1_, 46);
    text->setPosition(Vec2(535, 450));
    bg->addChild(text);
    
    Label* text1 = Label::createWithSystemFont("建议您升级为正式账号!"/*"升级账号, 获得20000欢乐豆！"*/, _GAME_FONT_NAME_1_, 46);
    text1->setPosition(Vec2(400, 380));
    bg->addChild(text1);
    
    Menu * btnCancel = CreateButton("btn_giveup", Vec2(760, 350), Btn_Close);
    addChild(btnCancel);

    Menu * btnConfirm = CreateButton("btn_upgradeRight", Vec2(1160, 350), Btn_upgrade);
    addChild(btnConfirm);

	actionShow();
	setTouchEnabled(true);
    auto listener = EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(true);
    listener->onTouchBegan = CC_CALLBACK_2(GameUpgrade::onTouchBegan, this);
    listener->onTouchMoved = CC_CALLBACK_2(GameUpgrade::onTouchMoved, this);
    listener->onTouchEnded = CC_CALLBACK_2(GameUpgrade::onTouchEnded, this);
    Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(listener, this);
	return true;
}

void GameUpgrade::callbackBt( Ref *pSender )
{
	Node *pNode = (Node *)pSender;
	switch (pNode->getTag())
	{
	case Btn_Close:
		{
			SoundUtil::sharedEngine()->playEffect("buttonMusic");
			actionMin();
            if (g_GlobalUnits.m_FreeAccount/* && 1 == g_GlobalUnits.m_FreeWeixin*/)
            {
                GameFishSendEvent::create(m_pGameScene);
            }
            break;
		}
    case Btn_upgrade:
        {
            LobbyLayer * lobbyLayer = GameLayerMove::sharedGameLayerMoveSink()->m_LobbyLayer;
            lobbyLayer->initGameUserInfo();

            SoundUtil::sharedEngine()->playEffect("buttonMusic");
            actionMin();
            break;
        }
	}
}

void GameUpgrade::OnRemove()
{
	GameLayerMove::sharedGameLayerMoveSink()->ClearSeting();
	m_pGameScene->removeChild(this);
}

GameUpgrade* GameUpgrade::create(GameScene *pGameScene)
{
	GameUpgrade* temp = new GameUpgrade(pGameScene);
	if(temp && temp->init())
	{
		temp->autorelease();
		return temp;
	}
	else
	{
		CC_SAFE_DELETE(temp);
		return NULL;
	}
}

void GameUpgrade::onEnter()
{
	GameLayer::onEnter();
}

void GameUpgrade::onExit()
{
	GameLayer::onExit();
}

void GameUpgrade::actionShow()
{
	setScale(0.1f);
	this->runAction(Sequence::create(ScaleTo::create(0.2f , 1.f),NULL));
}


void GameUpgrade::actionMin()
{
   this->runAction(Sequence::create(ScaleTo::create(0.2f , 0.1f),CallFunc::create(CC_CALLBACK_0(GameUpgrade::OnRemove, this)),NULL));
}
