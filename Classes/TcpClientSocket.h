﻿#ifndef _TCP_CLIENT_SOCKET_H_
#define _TCP_CLIENT_SOCKET_H_

// 注意 仅支持 单线程

#include "cocos2d.h"
#include <string>
#include <vector>

#if(CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)

	#include <WINSOCK2.H>
	#pragma comment(lib, "wsock32.lib")

#else
	
	#include <fcntl.h>
	#include <unistd.h>
	#include <stdio.h>
	#include <string.h>
	#include <errno.h>
	#include <stdlib.h>

	#include <sys/socket.h>
	#include <sys/types.h>
	#include <sys/stat.h>

	#include <netdb.h>
	#include <netinet/in.h>
	#include <netinet/tcp.h> 
	#include <arpa/inet.h>

#endif

#define _TCP_CLIENT_SOCKET_LOOP_TICKET_ (1.0f / 33.0f)

#include "GlobalDef.h"
#define SOCKET_ERROR -1
#define SOCKET int
#define INVALID_SOCKET	-1

#pragma pack(1)
struct ClientData
{
	LONGLONG Time;
	char key[33];
	char MD5[33];
	unsigned int Ret;
};
#pragma pack()

struct TcpCallback
{
	//连接事件
public:
	//连接事件
	virtual bool OnEventTCPSocketLink(){return true;}
	//关闭事件
	virtual bool OnEventTCPSocketShut(){return true;}
	//读取事件
	virtual bool OnEventTCPSocketRead(CMD_Command Command, VOID * pData, WORD wDataSize){return true;}
    
    virtual bool OnEventTCPConnectError() = 0;
    
    virtual void ConnectToServer() { }
};


struct MsgStruct
{
    WORD mainId;
    WORD subId;
    BYTE data[SOCKET_BUFFER];
    WORD dataLen;
};

class TcpClientSocket
{
public:
    
	TcpClientSocket(int iReadBufferLen = SOCKET_BUFFER);
	virtual ~TcpClientSocket();
    std::vector<MsgStruct> notSendMsg;
	void encrypt_code(char* buf,char* xorkeys,int xorlen,int count);
	int safeconnect(const char* ip,int port,time_t Unixdate);

	bool TestConnectServer(const char *pServerIP,unsigned short ServerPort);

	bool ConnectServer(const char *pServerIP,unsigned short ServerPort, TcpCallback *pTcpCallback);
    bool ThreadConnectServer(const char* pServerIP,unsigned short ServerPort);
    bool SendNotSendData();
#if (CC_TARGET_PLATFORM==CC_PLATFORM_IOS)
    bool IOSConnectServer(const char *pServerIP,unsigned short ServerPort);
#endif
    
    virtual void CloseSocket();

	// 传回 是否 连线中
	bool IsConnect(void);
	
	bool SendPack(unsigned char *pPackBuffer, int PackLength);
	virtual bool RecvPack();
	bool RecvPack(CMD_Command &Command, char* pData, WORD &wDataSize);


	//发送函数
	virtual bool SendData(unsigned short wMainCmdID, unsigned short wSubCmdID);
	//发送函数
	virtual bool SendData(unsigned short wMainCmdID, unsigned short wSubCmdID, void * const pData, unsigned short wDataSize);
	
	// 清除 已使用的缓存
	void CleanRecvBuffer(int UsedBuuferLength);

	//加密函数
protected:
	//解密数据
	WORD CrevasseBuffer(BYTE pcbDataBuffer[], unsigned short wDataSize);
	//加密数据
	WORD EncryptBuffer(BYTE pcbDataBuffer[], unsigned short wDataSize, unsigned short wBufferSize);
    
    bool hasError();
    
    UINT32 m_lastSendTime = 0;
    UINT32 m_lastRecvTime = 0;
    bool bSendNotSend = false;
    bool isTimeOut = false;
    bool isError = false;
    
	//加密数据
protected:
	BYTE							m_cbSendRound;						//字节映射
	BYTE							m_cbRecvRound;						//字节映射
	IosDword							m_dwSendXorKey;						//发送密钥
	IosDword							m_dwRecvXorKey;						//接收密钥
    bool                            m_bConnecting = false;
protected:
	TcpCallback						*m_pTcpCallback;					//接收消息回调
	int	m_RecvBufferLength;
	int	m_RecvBufferUsedCounter;
	char *m_pRecvBuffer;

	//计数变量
protected:
	IosDword							m_dwSendTickCount;					//发送时间
	IosDword							m_dwRecvTickCount;					//接收时间
	IosDword							m_dwSendPacketCount;				//发送计数
	IosDword							m_dwRecvPacketCount;				//接受计数

	//接收变量
protected:
	unsigned short							m_wRecvSize;						//接收长度
	unsigned char							m_cbRecvBuf[SOCKET_BUFFER*10];		//接收缓冲

	//内联函数
protected:
	//字节映射
	inline unsigned short SeedRandMap(unsigned short wSeed);
	//发送映射
	inline unsigned char MapSendByte(unsigned char cbData);
	//接收映射
	inline unsigned char MapRecvByte(unsigned char cbData);

    
public:
    void SetSocketLink(TcpCallback *link)
    {
        m_pTcpCallback = link;
    }

protected:

	#if (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)

		SOCKET  m_Socket;

	#else

		int  m_Socket;

	#endif
}; 

#endif
