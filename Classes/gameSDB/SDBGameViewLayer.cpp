#include "SDBGameViewLayer.h"
#include "LobbyLayer.h"
#include "ClientSocketSink.h"
#include "HXmlParse.h"
#include "LocalDataUtil.h"
#include "JniSink.h"
#include "VisibleRect.h"
#include "HttpConstant.h"
#include "Screen.h"
#include "LobbySocketSink.h"
#include "GameLayerMove.h"
#include "Convert.h"

#define Btn_Seting			199
#define Btn_BackToLobby		200
#define Btn_100				201
#define Btn_1000			202
#define Btn_1W				203
#define Btn_10W				204
#define Btn_100w			205
#define Btn_500W			206
#define Btn_1000W			207
#define GetCard				208
#define StopCard			209
#define ApplyBanker			210
#define CancleBanker		212
#define RobBanker			213
#define Btn_Uplist			214
#define Btn_Downlist		215
#define Btn_BankBtn			216
#define Btn_GetScoreBtn		217


SDBGameViewLayer::SDBGameViewLayer(GameScene *pGameScene)
	:IGameView(pGameScene)
{
	m_LookMode = false;
	m_bScoreFull = false;
	m_bisSendCard = false;
	m_bisShowButton = false;
	m_BankCardIndex = 0;
	m_lTotalPlaceJetton = 0;
	m_lMePlaceJetton = 0;
	m_lApplyBankerCondition = 0;
	m_lAreaLimitScore = 0;
	m_lUserMaxScore = 0;
	m_lAreaLimitScoreEx = 0;
	m_lTotalPlaceJettonEx = 0;
	m_bStopGet = 255;
	m_nWantedCardCount = 1;
	m_BankListBeginIndex = 0;
	m_CurSelectGold = 0;
	m_nRobBanker = 0;
	m_IsMeBank = false;
	m_wBankerTimes = 0;
	m_lBankerWinScore = 0;
	m_lMeWinScore = 0;
	m_SoundS = true;
    m_timeType = 0;
	SetKindId(SDB_KIND_ID);
}

SDBGameViewLayer::~SDBGameViewLayer() 
{

}

SDBGameViewLayer *SDBGameViewLayer::create(GameScene *pGameScene)
{
	SDBGameViewLayer *pLayer = new SDBGameViewLayer(pGameScene);
	if(pLayer && pLayer->init())
	{
		pLayer->autorelease();
		return pLayer;
	}
	else
	{
		CC_SAFE_DELETE(pLayer);
		return NULL;
	}
}


bool SDBGameViewLayer::init()
{
	if ( !Layer::init() )
	{
		return false;
	}
	setLocalZOrder(3);
    Tools::addSpriteFrame("sdb/SDB.plist");
    Tools::addSpriteFrame("Common/CardSprite2.plist");
	IGameView::onEnter();
	JniSink::share()->setIGameView(this);
	setGameStatus(GS_FREE);

	InitGame();
	AddButton();

	AddPlayerInfo();

	setTouchEnabled(true);
    
    auto listener = EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(true);
    listener->onTouchBegan = CC_CALLBACK_2(SDBGameViewLayer::onTouchBegan, this);
    listener->onTouchMoved = CC_CALLBACK_2(SDBGameViewLayer::onTouchMoved, this);
    listener->onTouchEnded = CC_CALLBACK_2(SDBGameViewLayer::onTouchEnded, this);
    _eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);
    return true;
}

void SDBGameViewLayer::onEnter()
{	

}

void SDBGameViewLayer::onExit()
{
	Tools::removeSpriteFrameCache("sdb/SDB.plist");
	Tools::removeSpriteFrameCache("Common/CardSprite2.plist");
	IGameView::onExit();
}

// Touch 触发
bool SDBGameViewLayer::onTouchBegan(Touch *pTouch, Event *pEvent)
{
    if (m_OpenBankList)
    {
        CloseBankList();
        return false;
    }
   
    if (m_timeType != SDB_IDI_PLACE_JETTON)
        return true;
    
    if (m_bScoreFull)
        return true;
    
    if (m_CurSelectGold <= 0)
        return true;
    
    Rect jetRect = Rect(400, 450, 1090, 300);
    Vec2 touchLocation = pTouch->getLocation(); // 返回GL坐标
    Vec2 localPos = convertToNodeSpace(touchLocation);
    if (jetRect.containsPoint(localPos))
    {
        LONGLONG lLeaveScore = min((m_lUserMaxScore-m_lMePlaceJetton), (m_lAreaLimitScore-m_lTotalPlaceJetton));
        if (lLeaveScore > m_CurSelectGold)//选择下注小于还可下注
        {
            PlaceJetton(m_CurSelectGold);
        }
        else
        {
            if (lLeaveScore >= 5000000)
            {
                 PlaceJetton(5000000);
            }
            else if (lLeaveScore >= 1000000)
            {
                PlaceJetton(1000000);
            }
            else if (lLeaveScore >= 100000)
            {
                PlaceJetton(100000);
            }else if (lLeaveScore >= 10000)
            {
                PlaceJetton(10000);
            }
            else if (lLeaveScore >= 1000)
            {
                PlaceJetton(1000);
            }
            else if (lLeaveScore>=100)
            {
                PlaceJetton(100);
            }
        }
    }
	return true;
}

void SDBGameViewLayer::onTouchMoved(Touch *pTouch, Event *pEvent)
{
}

void SDBGameViewLayer::onTouchEnded(Touch*pTouch, Event*pEvent)
{
}

// Alert Message 确认消息处理
void SDBGameViewLayer::DialogConfirm(Ref *pSender)
{
	this->RemoveAlertMessageLayer();
	this->SetAllTouchEnabled(true);
}

// Alert Message 取消消息处理
void SDBGameViewLayer::DialogCancel(Ref *pSender)
{
	this->RemoveAlertMessageLayer();
	this->SetAllTouchEnabled(true);
}

void SDBGameViewLayer::OnEventUserScore( tagUserData * pUserData, WORD wChairID, bool bLookonUser )
{
	IGameView::OnEventUserScore(pUserData, wChairID, bLookonUser);
    AddPlayerInfo();
}

void SDBGameViewLayer::OnEventUserStatus(tagUserData * pUserData, WORD wChairID, bool bLookonUser)
{
	IGameView::OnEventUserStatus(pUserData, wChairID, bLookonUser);
	AddPlayerInfo();
}

void SDBGameViewLayer::OnEventUserEnter( tagUserData * pUserData, WORD wChairID, bool bLookonUser )
{
	AddPlayerInfo();
}

void SDBGameViewLayer::OnEventUserLeave( tagUserData * pUserData, WORD wChairID, bool bLookonUser )
{
	AddPlayerInfo();
}

void SDBGameViewLayer::callbackBt( Ref *pSender )
{
    if (m_OpenBankList)
    {
        CloseBankList();
        return;
    }
    
    Node *pNode = (Node *)pSender;
	switch (pNode->getTag())
	{
	case Btn_GetScoreBtn:
		{	
			GetScoreForBank();
			break;
		}
	case Btn_BackToLobby:	//返回
		{
			SoundUtil::sharedEngine()->playEffect("buttonMusic");
			BackToLobby();
			break;
		}
	case Btn_Seting:
		{
			SoundUtil::sharedEngine()->playEffect("buttonMusic");
			GameLayerMove::sharedGameLayerMoveSink()->OpenSeting();
			break;
		}
	case Btn_100:			//下注
		{
			Select100();
			break;
		}
	case Btn_1000:
		{
            Select1000();
			break;
		}
	case Btn_1W:
		{
            Select1w();
			break;
		}
	case Btn_10W:
		{
            Select10w();
			break;
		}
	case Btn_100w:
		{
            Select100w();
			break;
		}
	case Btn_500W:
		{
            Select500w();
			break;
		}
	case Btn_1000W:
		{
            Select1000w();
			break;
		}
	case GetCard:
		{
			SoundUtil::sharedEngine()->playEffect("buttonMusic");
			OnBnGetCard();
			break;
		}
	case StopCard:
		{
			SoundUtil::sharedEngine()->playEffect("buttonMusic");
			OnBnStopGet();
			break;
		}
	case Btn_Downlist:
		{
			SoundUtil::sharedEngine()->playEffect("buttonMusic");
			if(m_BankListBeginIndex+1 < m_BankUserList.size()) m_BankListBeginIndex++;
			DrawBankList();
			break;
		}
	case Btn_Uplist:
		{
			SoundUtil::sharedEngine()->playEffect("buttonMusic");
			if (m_BankListBeginIndex-1 >= 0) m_BankListBeginIndex--;
			DrawBankList();
			break;
		}
	case Btn_BankBtn:
		{
			SoundUtil::sharedEngine()->playEffect("buttonMusic");
			if (m_OpenBankList == true)	CloseBankList();
			else OpenBankList();
			break;
		}
	case ApplyBanker: //申请上庄
		{
			SoundUtil::sharedEngine()->playEffect("buttonMusic");
			OnApplyBanker(1);
			break;
		}
	case CancleBanker: //我要下庄
		{
			SoundUtil::sharedEngine()->playEffect("buttonMusic");
            if (m_IsMeBank)
                OnApplyBanker(2);
            else
                OnApplyBanker(3);
			break;
		}
	case RobBanker:
		{
			SoundUtil::sharedEngine()->playEffect("buttonMusic");
            auto pUserData = GetMeUserData();
            if (pUserData == NULL)
                return;
            if (m_cbGameStatus != GS_FREE)
            {
                AlertMessageLayer::createConfirm("只能在空闲时间抢庄！");
                return;
            }
            if (GetMeChairID() == m_wBankerUser)
            {
                 AlertMessageLayer::createConfirm("您已经是当前庄家了！");
                return;
            }
            if (m_nRobBanker >= SDB_MAX_ROB_BANKER)
            {
                AlertMessageLayer::createConfirm("上庄列表前三名不能抢庄！");
                return;
            }
            if (pUserData->lScore < m_lApplyBankerCondition+3000000)
            {
                char TipMessage[128] = {0};
                sprintf(TipMessage, "\u60a8\u7684\u9152\u5427\u8c46\u4e0d\u8db3\uff0c\u65e0\u6cd5\u62a2\u5e84\uff0c\u62a2\u5e84\u6761\u4ef6\u4e3a\uff1a%lld\u9152\u5427\u8c46",m_lApplyBankerCondition+3000000);
                AlertMessageLayer::createConfirm(TipMessage);
                return;
            }
            
            m_btRobBanker->setEnabled(false);
			SendData(SUB_C_SDB_ROB_BANKER);
			break;
		}
	}
}

//申请消息
void SDBGameViewLayer::OnApplyBanker(int wParam)
{
	//合法判断
	const tagUserData * pClientUserItem = GetMeUserData();
	//旁观判断
	if (IsLookonMode()) return;

	if (wParam == 1)
	{

		if (pClientUserItem->lScore < m_lApplyBankerCondition)
		{
			char TipMessage[128] = {0};
			sprintf(TipMessage, "\u60a8\u7684\u9152\u5427\u8c46\u4e0d\u8db3\uff0c\u65e0\u6cd5\u4e0a\u5e84\uff0c\u4e0a\u5e84\u6761\u4ef6\u4e3a\uff1a%lld\u9152\u5427\u8c46",m_lApplyBankerCondition);
			AlertMessageLayer::createConfirm(TipMessage);
			return;
		}
		//发送消息
		SendData(SUB_C_SDB_APPLY_BANKER);
	}
    else if (wParam == 2)
    {
        SendData(SUB_C_SDB_CANCEL_BANKER);
    }
	else if (wParam == 3)
	{
		//发送消息
		SendData(SUB_C_SDB_CANCEL_APPLY);
	}

	//设置按钮
	UpdateControl();
}

void SDBGameViewLayer::LayerMoveCallBack()
{
    m_IsLayerMove = false;
}

void SDBGameViewLayer::CloseBankList()
{
    if (m_IsLayerMove)
        return;
    
    m_IsLayerMove = true;
    m_OpenBankList = false;
    MoveTo *leftMoveBy = MoveTo::create(0.2f, Vec2(960,1500));
    ActionInstant *func = CallFunc::create(CC_CALLBACK_0(SDBGameViewLayer::LayerMoveCallBack,this));
    m_BankListBackSpr->runAction(Sequence::create(leftMoveBy, func, NULL));
}

void SDBGameViewLayer::OpenBankList()
{
    if (m_IsLayerMove)
        return;
    
    m_IsLayerMove = true;
    m_OpenBankList = true;
    MoveTo *leftMoveBy = MoveTo::create(0.2f, _STANDARD_SCREEN_CENTER_);
    ActionInstant *func = CallFunc::create(CC_CALLBACK_0(SDBGameViewLayer::LayerMoveCallBack,this));
    m_BankListBackSpr->runAction(Sequence::create(leftMoveBy, func, NULL));
}

void SDBGameViewLayer::OnBnStopGet()
{
	m_bnGetCard->setVisible(false);
	m_bnStopGet->setVisible(false);

	if (GetMeChairID() != INVALID_CHAIR)
	{
		SendData(SUB_C_SDB_STOP_GET);		
	}
}

void SDBGameViewLayer::OnTimerCheckState()
{
	BYTE cbGameStatus = m_cbGameStatus;
	if((cbGameStatus == SDB_GS_BANKER_CARD && m_wBankerUser == GetMeChairID()) || (cbGameStatus == SDB_GS_PLAYER_CARD && m_lMePlaceJetton > 0))
	{
		CMD_C_SDB_CheckState CheckState;
		CheckState.cbClientState = ecsNormal;
		CheckState.cbStopGet = m_bStopGet;
		CheckState.cbWantedCardCount = m_nWantedCardCount;

		if(!m_bnGetCard->isVisible() && m_bStopGet != 1)
		{
			if(m_nWantedCardCount == m_PlayerCardList.size())
			{
				m_bnGetCard->setVisible(true);
				m_bnGetCard->setEnabled(true);
				m_bnGetCard->setColor(Color3B(255,255,255));
				m_bnStopGet->setVisible(true);
				m_bnStopGet->setEnabled(true);
				m_bnStopGet->setColor(Color3B(255,255,255));
			}
			if(m_nWantedCardCount == m_PlayerCardList.size())
			{
				CheckState.cbClientState = ecsButtonAbnormal;
			}
			else
			{
				CheckState.cbClientState = ecsButtonCardAbnormal;
			}
		}
		else if(m_nWantedCardCount != m_PlayerCardList.size())
		{
			CheckState.cbClientState = ecsCardAbnormal;
		}
		SendData(SUB_C_SDB_CHECK_STATE,&CheckState,sizeof(CheckState));
	}	
}

void SDBGameViewLayer::StartCheck()
{
	BYTE cbGameStatus = m_cbGameStatus;
	if(((cbGameStatus == SDB_GS_BANKER_CARD) && (m_wBankerUser == GetMeChairID())) || ((cbGameStatus == SDB_GS_PLAYER_CARD) && m_lMePlaceJetton > 0))
	{
		OnTimerCheckState();
	}
}

void SDBGameViewLayer::OnBnGetCard()
{
	m_bnGetCard->setVisible(false);
	m_bnStopGet->setVisible(false);

	if (GetMeChairID() != INVALID_CHAIR)
	{
		//开始状态检测
		m_nWantedCardCount ++;
		StartCheck();
		SendData(SUB_C_SDB_GET_CARD);
	}
}

void SDBGameViewLayer::Select100()
{
    m_CurSelectGold = 100;
    SoundUtil::sharedEngine()->playEffect("buttonMusic");
    removeAllChildByTag(m_BtnAnim_Tga);
    Sprite *temp = Sprite::createWithSpriteFrameName("agoldselect.png");
    temp->setTag(m_BtnAnim_Tga);
    temp->setPosition(Vec2(540,80));
    addChild(temp);
}
void SDBGameViewLayer::Select1000()
{
    m_CurSelectGold = 1000;
    SoundUtil::sharedEngine()->playEffect("buttonMusic");
    removeAllChildByTag(m_BtnAnim_Tga);
    Sprite *temp = Sprite::createWithSpriteFrameName("agoldselect.png");
    temp->setTag(m_BtnAnim_Tga);
    temp->setPosition(Vec2(680,80));
    addChild(temp);
}
void SDBGameViewLayer::Select1w()
{
    m_CurSelectGold = 10000;
    SoundUtil::sharedEngine()->playEffect("buttonMusic");
    removeAllChildByTag(m_BtnAnim_Tga);
    Sprite *temp = Sprite::createWithSpriteFrameName("agoldselect.png");
    temp->setTag(m_BtnAnim_Tga);
    temp->setPosition(Vec2(820,80));
    addChild(temp);
}
void SDBGameViewLayer::Select10w()
{
    m_CurSelectGold = 100000;
    SoundUtil::sharedEngine()->playEffect("buttonMusic");
    removeAllChildByTag(m_BtnAnim_Tga);
    Sprite *temp = Sprite::createWithSpriteFrameName("agoldselect.png");
    temp->setTag(m_BtnAnim_Tga);
    temp->setPosition(Vec2(960,80));
    addChild(temp);
}
void SDBGameViewLayer::Select100w()
{
    m_CurSelectGold = 1000000;
    SoundUtil::sharedEngine()->playEffect("buttonMusic");
    removeAllChildByTag(m_BtnAnim_Tga);
    Sprite *temp = Sprite::createWithSpriteFrameName("agoldselect.png");
    temp->setTag(m_BtnAnim_Tga);
    temp->setPosition(Vec2(1100,80));
    addChild(temp);
}
void SDBGameViewLayer::Select500w()
{
    m_CurSelectGold = 5000000;
    SoundUtil::sharedEngine()->playEffect("buttonMusic");
    removeAllChildByTag(m_BtnAnim_Tga);
    Sprite *temp = Sprite::createWithSpriteFrameName("agoldselect.png");
    temp->setTag(m_BtnAnim_Tga);
    temp->setPosition(Vec2(1240,80));
    addChild(temp);
}
void SDBGameViewLayer::Select1000w()
{
    m_CurSelectGold = 10000000;
    SoundUtil::sharedEngine()->playEffect("buttonMusic");
    removeAllChildByTag(m_BtnAnim_Tga);
    Sprite *temp = Sprite::createWithSpriteFrameName("agoldselect.png");
    temp->setTag(m_BtnAnim_Tga);
    temp->setPosition(Vec2(1380,80));
    addChild(temp);
}

//自己下注
void SDBGameViewLayer::PlaceJetton(LONGLONG lAddScore)
{
    if (GetMeChairID() == m_wBankerUser)
        return;
    
	if (GetMeChairID() != INVALID_CHAIR)
	{
        m_lTotalPlaceJettonEx = m_lTotalPlaceJetton;
		CMD_C_SDB_PlaceJetton AddScore;
		AddScore.lJettonScore = lAddScore;
		SendData(SUB_C_SDB_PLACE_JETTON, &AddScore, sizeof(CMD_C_SDB_PlaceJetton));
	}
	UpdateControl();
}

//游戏开始
bool SDBGameViewLayer::OnSubGameStart(const void * pBuffer, WORD wDataSize)
{
	ASSERT(wDataSize == sizeof(CMD_S_SDB_GameStart));
	if (wDataSize != sizeof(CMD_S_SDB_GameStart)) return false;
	CMD_S_SDB_GameStart *pGameStart = (CMD_S_SDB_GameStart *)pBuffer;

	m_bStopGet = 255;
	
	m_cbGameStatus = GS_PLAYING;
    m_GameState = enGameBet;
	StartTime(GetMeChairID(),SDB_IDI_PLACE_JETTON,pGameStart->byTimeLeave);
	m_lAreaLimitScore = pGameStart->lAreaLimitScore;
	m_lUserMaxScore = pGameStart->lUserMaxScore;
	m_lAreaLimitScoreEx = pGameStart->lAreaLimitScore;
	m_wBankerUser = pGameStart->wBankerUser;

	SetBankerInfo(pGameStart->wBankerUser,0);
	DrawBetInfo();
	UpdateControl();
	PlayGameMusicBK();
    
    //自己是庄家
    if (m_IsMeBank)
    {
        m_bnCancelBanker->setVisible(true);
        m_bnCancelBanker->setEnabled(false);
        m_bnCancelBanker->setColor(Color3B(100,100,100));
    }
    m_btRobBanker->setEnabled(false);
    m_btRobBanker->setColor(Color3B(100,100,100));
    
    return true;
}

//加注结果
bool SDBGameViewLayer::OnSubAddScore(const void * pBuffer, WORD wDataSize)
{
	ASSERT(wDataSize == sizeof(CMD_S_SDB_PlaceJetton));
	if (wDataSize != sizeof(CMD_S_SDB_PlaceJetton))
		return false;
    m_GameState = enGameBet;
	CMD_S_SDB_PlaceJetton *pAddScore = (CMD_S_SDB_PlaceJetton *)pBuffer;
	OnAddScore(pAddScore->wChairID, pAddScore->lJettonScore);

	if (m_SoundS)
	{
		SoundUtil::sharedEngine()->playEffect("sdb/sound/Add_score");
		m_SoundS = false;
		schedule(schedule_selector(SDBGameViewLayer::PlaySoundTime),0.2f);
	}

	DrawBetInfo();
	return true;
}

void SDBGameViewLayer::PlaySoundTime(float deltaTime)
{
	unschedule(schedule_selector(SDBGameViewLayer::PlaySoundTime));
	m_SoundS = true;
}

void SDBGameViewLayer::UpdataAllScore()
{
	LONGLONG score = m_lTotalPlaceJetton;
	if(m_lTotalPlaceJetton < 0) return;
	removeAllChildByTag(m_AllScoreTga);
    
    char strc[32]="";
    sprintf(strc, "%lld", score);
    
    LabelAtlas* temp = LabelAtlas::create(strc, "Common/gold_total.png", 45, 56, '+');
    //temp->setAnchorPoint(Vec2(0.5f, 0.5f));
    temp->setTag(m_AllScoreTga);
    temp->setPosition(Vec2(1200, 770));
    addChild(temp);
}


void SDBGameViewLayer::UpdataMeJetton()
{
	LONGLONG score = m_lMePlaceJetton;
	removeAllChildByTag(m_MeScore_Tga);

    char strc[32]="";
    sprintf(strc, "%lld", score);

    Sprite * bg = Sprite::createWithSpriteFrameName("xiazhudi.png");
    bg->setPosition(Vec2(960, 435));
    bg->setTag(m_MeScore_Tga);
    addChild(bg);
    
    LabelAtlas* temp = LabelAtlas::create(strc, "Common/gold_total.png", 45, 56, '+');
    temp->setAnchorPoint(Vec2(0.5f, 0.5f));
    temp->setTag(m_MeScore_Tga);
    temp->setPosition(Vec2(960, 435));
    addChild(temp,100);
}

//玩家下注
void SDBGameViewLayer::OnAddScore(WORD wChairID, LONGLONG lAddScore)
{
	//设置炸弹
	m_lTotalPlaceJetton += lAddScore;
	m_lTotalPlaceJettonEx = m_lTotalPlaceJetton;
	string JettonName;
	Vec2 MeJettonPos;
    if(lAddScore == 100)
	{
		MeJettonPos = m_bn100->getPosition();
		JettonName= "chip_100_normal.png";
	}
    else if(lAddScore == 1000)
    {
        MeJettonPos = m_bn1000->getPosition();
        JettonName= "chip_1000_normal.png";
    }
    else if(lAddScore == 10000)
	{
		MeJettonPos = m_bn1W->getPosition();
		JettonName= "chip_1w_normal.png";
	}
	else if(lAddScore == 100000)
	{
		MeJettonPos = m_bn10W->getPosition();
		JettonName= "chip_10w_normal.png";
	}
	else if(lAddScore == 1000000)
	{
		MeJettonPos = m_bn100W->getPosition();
		JettonName= "chip_100w_normal.png";
	}
	else if(lAddScore == 5000000)
	{
		MeJettonPos = m_bn500W->getPosition();
		JettonName= "chip_500w_normal.png";
	}
    else if(lAddScore == 10000000)
    {
        MeJettonPos = m_bn1000W->getPosition();
        JettonName= "chip_1000w_normal.png";
    }
    
    if (wChairID == GetMeChairID()) //自己
	{
		m_LookMode = false;
		m_lMePlaceJetton += lAddScore;
		Sprite* temp = Sprite::createWithSpriteFrameName(JettonName.c_str());
        temp->setScale(0.5f);
		temp->setPosition(MeJettonPos);
		temp->setTag(m_Jetton_Tga);
		addChild(temp);
		int endx = getRand(-400, 400);
		int endy = getRand(-50, 50);
		MoveTo *leftMoveBy = MoveTo::create(0.5f, Vec2(_STANDARD_SCREEN_CENTER_.x+endx, 640+endy));
		ActionInterval * easeSineOut = EaseSineOut::create(leftMoveBy);
		int anlge = getRand(0,360);
		ActionInterval * rotate = RotateTo::create(0.5f, anlge);
		temp->runAction(Spawn::create(easeSineOut,rotate,NULL));
		UpdataMeJetton();
	}
	else	//别的玩家
	{
		Sprite* temp = Sprite::createWithSpriteFrameName(JettonName.c_str());
        temp->setScale(0.5f);
		int posx = getRand(100,1000);
		temp->setTag(m_Jetton_Tga);
		temp->setPosition(Vec2(posx,768));
		addChild(temp);
		int endx = getRand(-400, 400);
		int endy = getRand(-50, 50);
		MoveTo *leftMoveBy = MoveTo::create(0.5f, Vec2(_STANDARD_SCREEN_CENTER_.x+endx, 640+endy));
		ActionInterval * easeSineOut = EaseSineOut::create(leftMoveBy);
		int anlge = getRand(0,360);
		ActionInterval * rotate = RotateTo::create(0.5f, anlge);
		temp->runAction(Spawn::create(easeSineOut,rotate,NULL));
	}
	UpdataAllScore();
	UpdateControl();
}

void SDBGameViewLayer::OnAddScoreGameStatus(WORD wChairID, LONGLONG lAddScore)
{
	if(lAddScore <= 0) return;
	string JettonName;
	Vec2 MeJettonPos;
    if(lAddScore == 100)
    {
        MeJettonPos = m_bn100->getPosition();
        JettonName= "chip_100_normal.png";
    }
    if(lAddScore == 1000)
	{
		MeJettonPos = m_bn1000->getPosition();
		JettonName= "chip_1000_normal.png";
	}
	else if(lAddScore == 10000)
	{
		MeJettonPos = m_bn1W->getPosition();
		JettonName= "chip_1w_normal.png";
	}
	else if(lAddScore == 100000)
	{
		MeJettonPos = m_bn10W->getPosition();
		JettonName= "chip_10w_normal.png";
	}
	else if(lAddScore == 1000000)
	{
		MeJettonPos = m_bn100W->getPosition();
		JettonName= "chip_100w_normal.png";
	}
	else if(lAddScore == 5000000)
	{
		MeJettonPos = m_bn500W->getPosition();
		JettonName= "chip_500w_normal.png";
	}
    else if(lAddScore == 10000000)
    {
        MeJettonPos = m_bn1000W->getPosition();
        JettonName= "chip_1000w_normal.png";
    }
    
    if (wChairID == GetMeChairID()) //自己
	{
		m_LookMode = false;
		m_lMePlaceJetton += lAddScore;
		Sprite* temp = Sprite::createWithSpriteFrameName(JettonName.c_str());
        temp->setScale(0.5f);
		temp->setPosition(MeJettonPos);
		temp->setTag(m_Jetton_Tga);
		addChild(temp);
		int endx = getRand(-400,400);
		int endy = getRand(-50,50);
		MoveTo *leftMoveBy = MoveTo::create(0.2f, Vec2(_STANDARD_SCREEN_CENTER_.x+endx, 640+endy));
		temp->runAction(leftMoveBy);
		UpdataMeJetton();
	}
	else	//别的玩家
	{
		Sprite* temp = Sprite::createWithSpriteFrameName(JettonName.c_str());
        temp->setScale(0.5f);
		int posx = getRand(100,1000);
		temp->setTag(m_Jetton_Tga);
		temp->setPosition(Vec2(posx,768));
		addChild(temp);
		int endx = getRand(-400,400);
		int endy = getRand(-50,50);
		MoveTo *leftMoveBy = MoveTo::create(0.2f, Vec2(_STANDARD_SCREEN_CENTER_.x+endx, 640+endy));
		temp->runAction(leftMoveBy);
	}
	UpdataAllScore();
	UpdateControl();
}

//加注失败
bool SDBGameViewLayer::OnSubAddScoreFailed(const void * pBuffer, WORD wDataSize)
{
	ASSERT(wDataSize == sizeof(CMD_S_SDB_AddScoreFailed));
	if (wDataSize != sizeof(CMD_S_SDB_AddScoreFailed))
		return false;
	CMD_S_SDB_AddScoreFailed *pAddScoreFailed = (CMD_S_SDB_AddScoreFailed *)pBuffer;
	if (IsLookonMode() || (GetMeChairID() != pAddScoreFailed->wAddUser))
        return true;
    
    m_GameState = enGameBet;
	UpdateControl();

	return true;
}

//加注分满
bool SDBGameViewLayer::OnSubScoreFull(const void * pBuffer, WORD wDataSize)
{
	ASSERT(wDataSize == sizeof(CMD_S_SDB_ScoreFull));
	if (wDataSize != sizeof(CMD_S_SDB_ScoreFull)) return false;
	CMD_S_SDB_ScoreFull *pScoreFull = (CMD_S_SDB_ScoreFull *)pBuffer;
    
    m_GameState = enGameBet;
    m_bScoreFull = true;
	m_lAreaLimitScore = 0;
	m_lUserMaxScore = 0;
	UpdateControl();

	StartTime(GetMeChairID(), SDB_IDI_PLACE_JETTON, pScoreFull->byTimeLeave);

	Sprite* temp = Sprite::createWithSpriteFrameName("score_full.png");
	temp->setPosition(Vec2(_STANDARD_SCREEN_CENTER_.x,_STANDARD_SCREEN_CENTER_.y+100));
	temp->setLocalZOrder(1000);
	temp->setTag(m_BetFullTga);
	addChild(temp);
	
    //播放游戏声音
	SoundUtil::sharedEngine()->playEffect("sdb/sound/remind");
	return true;
}


//发牌消息
bool SDBGameViewLayer::OnSubSendCard(const void * pBuffer, WORD wDataSize)
{
	ASSERT(wDataSize == sizeof(CMD_S_SDB_SendCard));
	if (wDataSize != sizeof(CMD_S_SDB_SendCard)) return false;
	CMD_S_SDB_SendCard *pSendCard = (CMD_S_SDB_SendCard *)pBuffer;

	removeAllChildByTag(m_BetFullTga);

	m_cbGameStatus = SDB_GS_BANKER_CARD;
    m_GameState = enGameSendCard;
	if (!IsLookonMode())
	{
		OnSendCard(pSendCard->wCurrentUser, pSendCard->byCardData);
	}
	else
	{
		OnSendCard(pSendCard->wCurrentUser, 0x00);
	}

	return true;
}

//是否为庄家
bool SDBGameViewLayer::IsBankerUser()
{
	return (GetMeChairID() == m_wBankerUser);
}

//发第一张牌
void SDBGameViewLayer::OnSendCard(WORD wChairID, BYTE byCardData)
{
	m_BankerCardList.push_back(0x00);
	if ((byCardData != 0x00) || IsBankerUser())
	{
		m_PlayerCardList.push_back(byCardData);
		MoveTo *leftMoveBy = MoveTo::create(0.2f, Vec2(_STANDARD_SCREEN_CENTER_.x, 300));
        ActionInstant *func = CallFunc::create(CC_CALLBACK_0(SDBGameViewLayer::CardMoveCallback, this));
		Sprite* temp = Sprite::createWithSpriteFrameName("SDBbackCard.png");
        temp->setScale(0.7f);
		temp->setTag(m_Send_Player_CardTga);
		addChild(temp);
		temp->setPosition(_STANDARD_SCREEN_CENTER_);
		temp->runAction(Sequence::create(leftMoveBy,func,NULL));
		SoundUtil::sharedEngine()->playEffect("sdb/sound/send_card");
	}

		MoveTo *leftMoveBy = MoveTo::create(0.2f, Vec2(_STANDARD_SCREEN_CENTER_.x, 830));
		ActionInstant *func = CallFunc::create(CC_CALLBACK_0(SDBGameViewLayer::CardMoveCallback1, this));
		Sprite* temp = Sprite::createWithSpriteFrameName("SDBbackCard.png");
        temp->setScale(0.7f);
		temp->setTag(m_Send_Bank_CardTga);
		addChild(temp);
		temp->setPosition(Vec2(_STANDARD_SCREEN_CENTER_.x,_STANDARD_SCREEN_CENTER_.y));
		temp->runAction(Sequence::create(leftMoveBy,func,NULL));
		SoundUtil::sharedEngine()->playEffect("sdb/sound/send_card");
	
	m_bisSendCard = true;

	if (IsBankerUser())
	{
		m_bisShowButton = true;
		StartTime(GetMeChairID(),SDB_IDI_PLAYER_CARD,SDB_TIME_PLAYER_CARD);
	}
	else
	{
		StartTime(GetMeChairID(),SDB_IDI_PLAYER_CARD,SDB_TIME_BANKER_CARD);
		UpdateControl();
	}
}


void SDBGameViewLayer::CardMoveCallback()  //两个参数
{
	removeAllChildByTag(m_Send_Player_CardTga);
	removeAllChildByTag(m_Send_Player_CardEndTga);
	for (int i = 0; i < m_PlayerCardList.size(); i++)
	{
		BYTE card = m_PlayerCardList[i];
		string _name = GetCardStringName(card);
		Sprite* temp = Sprite::createWithSpriteFrameName(_name.c_str());
        temp->setScale(0.7f);
		int w = 0;
		if(m_PlayerCardList.size() == 1) w = 0;
		else if(m_PlayerCardList.size() == 2) w = 25;
		else if(m_PlayerCardList.size() == 3) w = 50;
		else if(m_PlayerCardList.size() == 4) w = 75;
		else if(m_PlayerCardList.size() == 5) w = 100;
		temp->setPosition(Vec2(960+i*50-w,300));
		temp->setTag(m_Send_Player_CardEndTga);		
		addChild(temp);
	}

	BYTE byCardData[MAX_COUNT] = {0};
	auto nCardCount = m_PlayerCardList.size();
	if(nCardCount == 0) return;
	for (int i = 0; i < m_PlayerCardList.size(); i++)
	{
		byCardData[i] = m_PlayerCardList[i];
	}
	int nCardPoint;
  
	auto nCardType = m_GameLogic.GetCardType(byCardData, nCardCount, &nCardPoint);
	removeAllChildByTag(m_CardTypeAnimTga);
	if (nCardType == (int)(CGameLogicSDB::SDB_CT_1)) //爆点
	{
        
        Sprite* bg = Sprite::createWithSpriteFrameName("paixingdi.png");
        bg->setPosition(Vec2(960, 250));
        bg->setTag(m_BankCardTypeAnimTga);
        addChild(bg);

        Sprite* temp = Sprite::createWithSpriteFrameName("effect_full.png");
		temp->setPosition(Vec2(134,50));
		temp->setTag(m_CardTypeAnimTga);
		bg->addChild(temp,100);
        
        ScaleTo * move = ScaleTo::create(0.5, 1.2f);
        ScaleTo * move1 = ScaleTo::create(0.5, 0.8f);
        FiniteTimeAction* seq = Sequence::create(move,move1,NULL);
        RepeatForever * repeatForever =RepeatForever::create((ActionInterval* )seq);
        temp->runAction(repeatForever);
        
		m_bnGetCard->setVisible(false);
	}
	else if (nCardType >= (int)(CGameLogicSDB::SDB_CT_3))
	{
        Sprite* temp;
        if (nCardType == (int)CGameLogicSDB::SDB_CT_5)
            temp = Sprite::createWithSpriteFrameName("effect_fivetiger.png");
        
		else if (nCardType == (int)CGameLogicSDB::SDB_CT_4)
            temp = Sprite::createWithSpriteFrameName("effect_fivesmall.png");
        
        else
			temp = Sprite::createWithSpriteFrameName("effect_105.png");
		
        Sprite* bg = Sprite::createWithSpriteFrameName("paixingdi.png");
        bg->setPosition(Vec2(960, 250));
        bg->setTag(m_BankCardTypeAnimTga);
        addChild(bg);
 
        temp->setPosition(Vec2(134,50));
        temp->setTag(m_CardTypeAnimTga);
        bg->addChild(temp,100);
        
        ScaleTo * move = ScaleTo::create(0.5, 1.2f);
        ScaleTo * move1 = ScaleTo::create(0.5, 0.8f);
        FiniteTimeAction* seq = Sequence::create(move,move1,NULL);
        RepeatForever * repeatForever =RepeatForever::create((ActionInterval* )seq);
        temp->runAction(repeatForever);
        m_bnGetCard->setVisible(false);
    }
    
}

void SDBGameViewLayer::CardMoveCallback1()
{
	removeAllChildByTag(m_Send_Bank_CardTga);
	removeAllChildByTag(m_Send_Bank_CardEndTga);
	for (int i = 0; i < m_BankerCardList.size(); i++)
	{
		BYTE card = m_BankerCardList[i];
		string _name = GetCardStringName(card);
		Sprite* temp = Sprite::createWithSpriteFrameName(_name.c_str());
        temp->setScale(0.7f);
		int w = 0;
        if(m_PlayerCardList.size() == 1) w = 0;
        else if(m_PlayerCardList.size() == 2) w = 25;
        else if(m_PlayerCardList.size() == 3) w = 50;
        else if(m_PlayerCardList.size() == 4) w = 75;
        else if(m_PlayerCardList.size() == 5) w = 100;
        temp->setPosition(Vec2(960+i*50-w,830));
		temp->setTag(m_Send_Bank_CardEndTga);		
		addChild(temp);
	}
}

//庄家开始要牌
void SDBGameViewLayer::OnBankerBeginGet()
{
	m_bScoreFull = FALSE;
	if (IsLookonMode())	return;
	UpdateControl();
}

//闲家开始要牌
void SDBGameViewLayer::OnPlayerBeginGet()
{
	m_bScoreFull = FALSE;
	if (IsLookonMode())
        return;

	UpdateControl();
}

//开始要牌
bool SDBGameViewLayer::OnSubBeginGet(const void * pBuffer, WORD wDataSize)
{
	ASSERT(wDataSize == sizeof(CMD_S_SDB_BeginGet));
	if (wDataSize != sizeof(CMD_S_SDB_BeginGet)) return false;
	CMD_S_SDB_BeginGet *pBeginGet = (CMD_S_SDB_BeginGet *)pBuffer;
    m_GameState = enGameSendCard;
	
    if (pBeginGet->bBankerUser)
	{
		m_cbGameStatus = SDB_GS_BANKER_CARD;
		//开始状态检测
		StartTime(GetMeChairID(), SDB_IDI_BANKER_CARD, pBeginGet->byTimeLeave);
		OnBankerBeginGet();

	}
	else
	{
		m_cbGameStatus = SDB_GS_PLAYER_CARD;
		StartTime(GetMeChairID(), SDB_IDI_PLAYER_CARD, pBeginGet->byTimeLeave);
		OnPlayerBeginGet();
	}
	return true;
}


//玩家要牌
bool SDBGameViewLayer::OnSubGetCard(const void * pBuffer, WORD wDataSize)
{
	ASSERT(wDataSize == sizeof(CMD_S_SDB_GetCard));
	if (wDataSize != sizeof(CMD_S_SDB_GetCard)) return false;
	CMD_S_SDB_GetCard *pGetCard = (CMD_S_SDB_GetCard *)pBuffer;
    
    m_GameState = enGameOpenCard;
	if (pGetCard->wCurrentUser == GetMeChairID())
	{
		m_PlayerCardList.push_back(pGetCard->byCardData);
		MoveTo *leftMoveBy = MoveTo::create(0.2f, Vec2(_STANDARD_SCREEN_CENTER_.x, 200));
		ActionInstant *func = CallFunc::create(CC_CALLBACK_0(SDBGameViewLayer::CardMoveCallback, this));
		Sprite* temp = Sprite::createWithSpriteFrameName("SDBbackCard.png");
		temp->setTag(m_Send_Player_CardTga);
		addChild(temp);
		temp->setPosition(Vec2(_STANDARD_SCREEN_CENTER_.x,_STANDARD_SCREEN_CENTER_.y));
		temp->runAction(Sequence::create(leftMoveBy,func,NULL));
	}
	else
	{
		m_BankerCardList.push_back(pGetCard->byCardData);
		MoveTo *leftMoveBy = MoveTo::create(0.2f, Vec2(_STANDARD_SCREEN_CENTER_.x, 650));
		ActionInstant *func = CallFunc::create(CC_CALLBACK_0(SDBGameViewLayer::CardMoveCallback1, this));
		Sprite* temp = Sprite::createWithSpriteFrameName("SDBbackCard.png");
		temp->setTag(m_Send_Bank_CardTga);
		addChild(temp);
		temp->setPosition(Vec2(_STANDARD_SCREEN_CENTER_.x,_STANDARD_SCREEN_CENTER_.y));
		temp->runAction(Sequence::create(leftMoveBy,func,NULL));
	}

	return true;
}

//是否停牌
void SDBGameViewLayer::OnStopGet(bool bStopGet)
{
	if (bStopGet)
	{
		m_bnGetCard->setVisible(false);
		m_bnStopGet->setVisible(false);
		if (GetMeChairID() == m_wBankerUser)
		{
			m_bnGetCard->setVisible(true);
			m_bnStopGet->setVisible(true);
			m_bnGetCard->setEnabled(false);
			m_bnGetCard->setColor(Color3B(100,100,100));
			m_bnStopGet->setEnabled(true);
			m_bnStopGet->setColor(Color3B(255,255,255));
		}
		StartEffectPlayer();
	}
	else
	{
		m_bnGetCard->setVisible(true);
		m_bnStopGet->setVisible(true);
		m_bnGetCard->setEnabled(true);
		m_bnStopGet->setEnabled(true);
		m_bnGetCard->setColor(Color3B(255,255,255));
		m_bnStopGet->setColor(Color3B(255,255,255));
	}
}

//是否停牌
bool SDBGameViewLayer::OnSubStopGet(const void * pBuffer, WORD wDataSize)
{
	ASSERT(wDataSize == sizeof(CMD_S_SDB_StopGet));
	if (wDataSize != sizeof(CMD_S_SDB_StopGet)) return false;
	CMD_S_SDB_StopGet *pStopGet = (CMD_S_SDB_StopGet *)pBuffer;

    m_GameState = enGameOpenCard;
	if (!IsLookonMode())
	{
		m_bStopGet = pStopGet->bStopGet;

        OnStopGet(pStopGet->bStopGet);
	}

	return true;
}

void SDBGameViewLayer::showEndScore(Ref *pSender)
{
    Sprite * bg = (Sprite*)pSender;
    bg->setVisible(true);
}

void SDBGameViewLayer::SetWinScore(INT nWinType, LONGLONG lBankerScore, LONGLONG lPlayerScore,LONGLONG lReturnScore)
{
	removeAllChildByTag(m_EndScoreBack_Tga);
	Sprite* temp = Sprite::createWithSpriteFrameName("bg_tongyongkuang3.png");
	temp->setPosition(_STANDARD_SCREEN_CENTER_);
	temp->setTag(m_EndScoreBack_Tga);
    temp->setVisible(false);
    addChild(temp,110);
    CallFuncN * func = CallFuncN::create(CC_CALLBACK_1(SDBGameViewLayer::showEndScore, this));
    temp->runAction(Sequence::create(DelayTime::create(4.0f), func, NULL));
    
    Sprite* title = Sprite::createWithSpriteFrameName("gamover.png");
    title->setPosition(Vec2(519, 480));
    temp->addChild(title);

	//庄家得分
    char strc[128]="";
    ZeroMemory(strc, sizeof(strc));
    if (lBankerScore > 0)
        sprintf(strc,"+%lld", lBankerScore);
    else
        sprintf(strc,"%lld", lBankerScore);
    
    Label * bankLabel = Label::createWithSystemFont("庄家得分:", _GAME_FONT_NAME_1_, 50);
    bankLabel->setAnchorPoint(Vec2(1, 0.5f));
    bankLabel->setPosition(Vec2(520, 340));
    bankLabel->setColor(Color3B(127,68, 26));
    temp->addChild(bankLabel);
    
    LabelAtlas* banktemp = LabelAtlas::create(strc, "Common/gold_result.png", 45, 62, '+');
    banktemp->setAnchorPoint(Vec2(0, 0.5f));
    banktemp->setPosition(Vec2(530,340));
    temp->addChild(banktemp);

    Sprite* line = Sprite::createWithSpriteFrameName("label_line.png");
    line->setPosition(Vec2(519, 280));
    temp->addChild(line);
    
	//玩家得分
    ZeroMemory(strc, sizeof(strc));
    if (lPlayerScore > 0)
        sprintf(strc,"+%lld", lPlayerScore);
    else
        sprintf(strc,"%lld", lPlayerScore);

    Label * playerLabel = Label::createWithSystemFont("玩家得分:", _GAME_FONT_NAME_1_, 50);
    playerLabel->setAnchorPoint(Vec2(1, 0.5f));
    playerLabel->setPosition(Vec2(520, 220));
    playerLabel->setColor(Color3B(127,68, 26));
    temp->addChild(playerLabel);
    
    LabelAtlas* mGoldFont = LabelAtlas::create(strc, "Common/gold_result.png", 45, 62, '+');
    mGoldFont->setAnchorPoint(Vec2(0, 0.5f));
    mGoldFont->setPosition(Vec2(530,220));
    temp->addChild(mGoldFont);

    Sprite* line2 = Sprite::createWithSpriteFrameName("label_line.png");
    line2->setPosition(Vec2(519, 160));
    temp->addChild(line2);
    
	//返回分
    ZeroMemory(strc, sizeof(strc));
    if (lReturnScore > 0)
        sprintf(strc,"+%lld", lReturnScore);
    else
        sprintf(strc,"%lld", lReturnScore);

    Label * returnLabel = Label::createWithSystemFont("返回分:", _GAME_FONT_NAME_1_, 50);
    returnLabel->setAnchorPoint(Vec2(1, 0.5f));
    returnLabel->setPosition(Vec2(520, 100));
    returnLabel->setColor(Color3B(127,68, 26));
    temp->addChild(returnLabel);
 
    LabelAtlas* returnFont = LabelAtlas::create(strc, "Common/gold_result.png", 45, 62, '+');
    returnFont->setAnchorPoint(Vec2(0, 0.5f));
    returnFont->setPosition(Vec2(530,100));
    temp->addChild(returnFont);
}

//闲家牌型
void SDBGameViewLayer::StartEffectPlayer()
{
	BYTE byCardData[MAX_COUNT] = {0};
	ssize_t nCardCount = m_PlayerCardList.size();
	if(nCardCount == 0) return;
	for (int i = 0; i < m_PlayerCardList.size(); i++)
	{
		byCardData[i] = m_PlayerCardList[i];
	}
	int nCardPoint;
	int nCardType = m_GameLogic.GetCardType(byCardData, nCardCount, &nCardPoint);

	removeAllChildByTag(m_CardTypeAnimTga);
    
    Sprite* temp;
	
    if (nCardType == (int)(CGameLogicSDB::SDB_CT_1)) //爆点
	{
		temp = Sprite::createWithSpriteFrameName("effect_full.png");
	}
    else if (nCardType >= (int)(CGameLogicSDB::SDB_CT_3))
    {
        if (nCardType == (int)CGameLogicSDB::SDB_CT_5)
            temp = Sprite::createWithSpriteFrameName("effect_fivetiger.png");
        
        else if (nCardType == (int)CGameLogicSDB::SDB_CT_4)
            temp = Sprite::createWithSpriteFrameName("effect_fivesmall.png");
        
        else
            temp = Sprite::createWithSpriteFrameName("effect_105.png");
    }
    else
	{
		div_t div_result;
		div_result = div(nCardPoint, 10);
		if ((div_result.quot < 0) || (div_result.quot > 10)) return;
        char point[32];
        ZeroMemory(point, 32);
        if (div_result.rem == 5)
            sprintf(point, "effect_%d%d.png", div_result.quot, div_result.rem);
        else
            sprintf(point, "effect_%d.png", div_result.quot);
        
        temp = Sprite::createWithSpriteFrameName(point);
	}
    
    Sprite* bg = Sprite::createWithSpriteFrameName("paixingdi.png");
    bg->setPosition(Vec2(960, 250));
    bg->setTag(m_CardTypeAnimTga);
    addChild(bg);
    
    temp->setPosition(Vec2(134,50));
    bg->addChild(temp,100);
    
    ScaleTo * move = ScaleTo::create(0.3, 1.2f);
    ScaleTo * move1 = ScaleTo::create(0.3, 0.8f);
    FiniteTimeAction* seq = Sequence::create(move,move1,NULL);
    RepeatForever * repeatForever =RepeatForever::create((ActionInterval* )seq);
    temp->runAction(repeatForever);
}


//庄家牌型
void SDBGameViewLayer::StartEffectBanker()
{
	BYTE byCardData[MAX_COUNT] = {0};
	long nCardCount = m_BankerCardList.size();
	if(nCardCount == 0) return;
	for (int i = 0; i < m_BankerCardList.size(); i++)
	{
		byCardData[i] = m_BankerCardList[i];
	}
	int nCardPoint;
	int nCardType = m_GameLogic.GetCardType(byCardData, nCardCount, &nCardPoint);

	removeAllChildByTag(m_BankCardTypeAnimTga);
    Sprite* temp;

    if (nCardType == (int)(CGameLogicSDB::SDB_CT_1)) //爆点
    {
        temp = Sprite::createWithSpriteFrameName("effect_full.png");
    }
    else if (nCardType >= (int)(CGameLogicSDB::SDB_CT_3))
    {
        if (nCardType == (int)CGameLogicSDB::SDB_CT_5)
            temp = Sprite::createWithSpriteFrameName("effect_fivetiger.png");
        
        else if (nCardType == (int)CGameLogicSDB::SDB_CT_4)
            temp = Sprite::createWithSpriteFrameName("effect_fivesmall.png");
        
        else
            temp = Sprite::createWithSpriteFrameName("effect_105.png");
    }
    else
    {
        div_t div_result;
        div_result = div(nCardPoint, 10);
        if ((div_result.quot < 0) || (div_result.quot > 10)) return;
        char point[32];
        ZeroMemory(point, 32);
        if (div_result.rem == 5)
            sprintf(point, "effect_%d%d.png", div_result.quot, div_result.rem);
        else
            sprintf(point, "effect_%d.png", div_result.quot);
        
        temp = Sprite::createWithSpriteFrameName(point);
    }
    
    Sprite* bg = Sprite::createWithSpriteFrameName("paixingdi.png");
    bg->setPosition(Vec2(960, 780));
    bg->setTag(m_BankCardTypeAnimTga);
    addChild(bg);
    
    temp->setPosition(Vec2(134,50));
    bg->addChild(temp,100);
    
    ScaleTo * move = ScaleTo::create(0.3, 1.2f);
    ScaleTo * move1 = ScaleTo::create(0.3, 0.8f);
    FiniteTimeAction* seq = Sequence::create(move,move1,NULL);
    RepeatForever * repeatForever =RepeatForever::create((ActionInterval* )seq);
    temp->runAction(repeatForever);
}

//游戏结束
bool SDBGameViewLayer::OnSubGameEnd(const void * pBuffer, WORD wDataSize)
{
	ASSERT(wDataSize == sizeof(CMD_S_SDB_GameEnd));
	if (wDataSize != sizeof(CMD_S_SDB_GameEnd)) return false;
	CMD_S_SDB_GameEnd *pGameEnd = (CMD_S_SDB_GameEnd *)pBuffer;

    m_GameState = enGameEnd;
	StartEffectPlayer();
	m_cbGameStatus = SDB_GS_GAME_END;
	StartTime(GetMeChairID(), SDB_IDI_GAME_END, pGameEnd->byTimeLeave);

	m_lMeWinScore += pGameEnd->lPlayerWinScore;
	m_lBankerWinScore += pGameEnd->lBankerWinScore;

	if (pGameEnd->lPlayerWinScore >= 0)
	{
		SoundUtil::sharedEngine()->playEffect("sdb/sound/win");
		SetWinScore(pGameEnd->nWinType,pGameEnd->lBankerWinScore, pGameEnd->lPlayerWinScore,m_lMePlaceJetton);
	}
	else
	{
		SoundUtil::sharedEngine()->playEffect("sdb/sound/lost");
		SetWinScore(pGameEnd->nWinType,pGameEnd->lBankerWinScore, pGameEnd->lPlayerWinScore,0);
	}

	if (GetMeChairID() != m_wBankerUser)
	{
		m_BankerCardList.clear();
		for (int i = 0; i < pGameEnd->nBankerCardCount; i++)
		{
			m_BankerCardList.push_back(pGameEnd->byBankerCardData[i]);
		}

		removeAllChildByTag(m_Send_Bank_CardTga);
		removeAllChildByTag(m_Send_Bank_CardEndTga);
        
		for (int i = 0; i < m_BankerCardList.size(); i++)
		{
			BYTE card = m_BankerCardList[i];
			string _name = GetCardStringName(card);
			Sprite* temp = Sprite::createWithSpriteFrameName(_name.c_str());
			temp->setScale(0.7f);
			int w = 0;
			if(m_BankerCardList.size() == 1) w = 0;
			else if(m_BankerCardList.size() == 2) w = 25;
			else if(m_BankerCardList.size() == 3) w = 50;
			else if(m_BankerCardList.size() == 4) w = 75;
			else if(m_BankerCardList.size() == 5) w = 100;
			temp->setPosition(Vec2(960+i*50-w,830));
			temp->setTag(m_Send_Bank_CardEndTga);		
			addChild(temp);
		}
		StartEffectBanker();
	}


	UpdateControl();
	
	return true;
}


//游戏空闲
bool SDBGameViewLayer::OnSubGameFree(const void * pBuffer, WORD wDataSize)
{
	ASSERT(wDataSize == sizeof(CMD_S_SDB_GameFree));
	if (wDataSize != sizeof(CMD_S_SDB_GameFree)) return false;
	CMD_S_SDB_GameFree *pGameFree = (CMD_S_SDB_GameFree *)pBuffer;

	m_cbGameStatus = GS_FREE;
    m_GameState = enGameNormal;
	StartTime(GetMeChairID(), SDB_IDI_FREE, pGameFree->byTimeLeave);
	m_wBankerTimes = pGameFree->wBankerTimes;
	m_lBankerWinScore = pGameFree->lBankerWinScore;

	m_lTotalPlaceJetton = 0;//其他玩家下注数目
	m_lMePlaceJetton = 0;//自己下注数目
	m_lTotalPlaceJettonEx = 0;//其他玩家下注数目
	m_lAreaLimitScoreEx = 0;
	m_bStopGet = 255;
	m_nWantedCardCount = 1;

	//清理数据
	removeAllChildByTag(m_EndScoreBack_Tga);
	removeAllChildByTag(m_Jetton_Tga);
	removeAllChildByTag(m_Send_Bank_CardTga);
	removeAllChildByTag(m_Send_Bank_CardEndTga);
	removeAllChildByTag(m_Send_Player_CardTga);
	removeAllChildByTag(m_Send_Player_CardEndTga);
	removeAllChildByTag(m_AllScoreTga);
	removeAllChildByTag(m_MeScore_Tga);
	removeAllChildByTag(m_CardTypeAnimTga);
	removeAllChildByTag(m_BankCardTypeAnimTga);
	m_BankerCardList.clear();
	m_PlayerCardList.clear();
	UpdateControl();
    
    //自己是庄家
    if (m_IsMeBank)
    {
        m_bnCancelBanker->setVisible(true);
        m_bnCancelBanker->setEnabled(true);
        m_bnCancelBanker->setColor(Color3B(255,255,255));
    }
    
    if (m_nRobBanker < 3 && m_bCanRobBanker && false == m_IsMeBank)
    {
        m_btRobBanker->setEnabled(true);
        m_btRobBanker->setColor(Color3B::WHITE);
    }
    else
    {
        m_btRobBanker->setEnabled(false);
        m_btRobBanker->setColor(Color3B(100, 100, 100));
    }
    
    return true;
}


void SDBGameViewLayer::OnCheckState(CMD_S_SDB_CheckState *pCheckState)
{
	m_lMePlaceJetton = pCheckState->lPlaceJetton;
	m_bStopGet = pCheckState->cbStopGet;
	OnStopGet(m_bStopGet == 1);

	m_PlayerCardList.clear();
	for (int i = 0; i < pCheckState->cbCardCount; i++)
	{
		m_PlayerCardList.push_back(pCheckState->cbCardData[i]);
	}
}

bool SDBGameViewLayer::OnSubCheckState(const void * pBuffer, WORD wDataSize)
{
	ASSERT(wDataSize == sizeof(CMD_S_SDB_CheckState));
	if (wDataSize != sizeof(CMD_S_SDB_CheckState))return false;
	CMD_S_SDB_CheckState *pCheckState = (CMD_S_SDB_CheckState *)pBuffer;
	OnCheckState(pCheckState);
	return true;
}

//申请做庄
bool SDBGameViewLayer::OnSubApplyBanker(const void * pBuffer, WORD wDataSize)
{
	//效验数据
	ASSERT(wDataSize == sizeof(CMD_S_SDB_ApplyBanker));
	if (wDataSize != sizeof(CMD_S_SDB_ApplyBanker)) return false;
	CMD_S_SDB_ApplyBanker *pApplyBanker = (CMD_S_SDB_ApplyBanker *)pBuffer;

	const tagUserData * pClientUserItem = GetUserData(pApplyBanker->wApplyUser);
	if(pClientUserItem == NULL) return true;
	//插入玩家
	if (m_wBankerUser != pApplyBanker->wApplyUser)
	{
		tagApplyUser temp;
        temp.userChairID = pApplyBanker->wApplyUser;
		sprintf(temp.strUserName,"%s", pClientUserItem->szNickName);
		temp.lUserScore=pClientUserItem->lScore;
        temp.wUserID = pClientUserItem->dwUserID;
        temp.wGender = pClientUserItem->wGender;
		m_BankUserList.push_back(temp);
	}

    tagUserData *pUserData = ClientSocketSink::sharedSocketSink()->GetMeUserData();
    if (nullptr != pUserData && m_BankUserList.size() > 2)
    {
        if (m_BankUserList[0].wUserID == pUserData->dwUserID || m_BankUserList[1].wUserID == pUserData->dwUserID)
        {
            m_bCanRobBanker = false;
        }
        else
        {
            m_bCanRobBanker = true;
        }
    }
    else
        m_bCanRobBanker = false;
    
    //自己判断
    if (IsLookonMode()==false && GetMeChairID()==pApplyBanker->wApplyUser)
    {
        getApplyBanker()->setVisible(false);
        m_btRobBanker->setVisible(true);
        m_bnCancelBanker->setVisible(true);
        
        if (enGameNormal == m_GameState && m_nRobBanker < 3 && m_bCanRobBanker)
        {
            m_btRobBanker->setEnabled(true);
            m_btRobBanker->setColor(Color3B::WHITE);
        }
        else
        {
            m_btRobBanker->setEnabled(false);
            m_btRobBanker->setColor(Color3B(100, 100, 100));
        }
    }
    
    //更新控件
    DrawBankList();

	UpdateControl();

	return true;
}


void SDBGameViewLayer::DrawBankList()
{
    m_listView->removeAllItems();
    for (int i = 0; i < m_BankUserList.size(); i++)
    {
        ui::Layout* custom_item = ui::Layout::create();
        custom_item->setContentSize(Size(920, 120));
        
        Sprite* lable = Sprite::createWithSpriteFrameName("label_line.png");
        lable->setPosition(460, 0);
        custom_item->addChild(lable);
        
        //头像
        string heads = g_GlobalUnits.getFace(m_BankUserList[i].wGender, m_BankUserList[i].lUserScore);
        Sprite* mHead = Sprite::createWithSpriteFrameName(heads);
        mHead->setPosition(Vec2(90, 60));
        mHead->setScale(0.6f);
        custom_item->addChild(mHead);
        
        //昵称
        std::string szTempTitle = gbk_utf8(m_BankUserList[i].strUserName);
        Label* temp = Label::createWithSystemFont(szTempTitle.c_str(),_GAME_FONT_NAME_1_,40);
        temp->setAnchorPoint(Vec2(0,0.5f));
        
        if (!strcmp(GetMeUserData()->szNickName, m_BankUserList[i].strUserName))
            temp->setColor(Color3B::RED);
        else
            temp->setColor(Color3B::BLACK);
        temp->setPosition(Vec2(170, 60));
        custom_item->addChild(temp);
        
        
        Sprite* goldIcon = Sprite::createWithSpriteFrameName("gold_icon.png");
        goldIcon->setPosition(Vec2(620, 60));
        custom_item->addChild(goldIcon);
        char _score[32];
        if (m_BankUserList[i].lUserScore > 1000000000000)
        {
            sprintf(_score,"%.02lf兆", m_BankUserList[i].lUserScore / 1000000000000.0);
        }
        else if (m_BankUserList[i].lUserScore > 100000000)
        {
            sprintf(_score,"%.02lf亿", m_BankUserList[i].lUserScore / 100000000.0);
        }
        else if (m_BankUserList[i].lUserScore > 1000000)
        {
            sprintf(_score,"%.02lf万", m_BankUserList[i].lUserScore / 10000.0);
        }
        temp = Label::createWithSystemFont(_score,_GAME_FONT_NAME_1_,40);
        temp->setAnchorPoint(Vec2(0,0.5f));
        temp->setColor(Color3B::BLACK);
        temp->setPosition(Vec2(660, 60));
        custom_item->addChild(temp);
        m_listView->pushBackCustomItem(custom_item);
    }
}


void SDBGameViewLayer::DeleteBankList(tagApplyUser ApplyUser)
{
	for (int i = 0; i < m_BankUserList.size(); i++)
	{
		if (m_BankUserList[i].userChairID == ApplyUser.userChairID)
		{
			m_BankUserList.erase(m_BankUserList.begin()+i);
		}
	}
}

//取消申请
bool SDBGameViewLayer::OnSubCancelApply(const void * pBuffer, WORD wDataSize)
{
	ASSERT(wDataSize == sizeof(CMD_S_SDB_CancelApply));
	if (wDataSize != sizeof(CMD_S_SDB_CancelApply)) return false;

	CMD_S_SDB_CancelApply *pCancelBanker = (CMD_S_SDB_CancelApply *)pBuffer;
	m_nRobBanker = pCancelBanker->nRobBanker;
    
	//删除玩家
    tagApplyUser ApplyUser;
    ApplyUser.userChairID = pCancelBanker->wApplyUser;
	ApplyUser.lUserScore=0;
	DeleteBankList(ApplyUser);

    //自己判断
    const tagUserData * pMeUserData= GetMeUserData();
    if (IsLookonMode() == false && pMeUserData->wChairID == pCancelBanker->wApplyUser)
    {
        m_btRobBanker->setVisible(false);
        m_bnCancelBanker->setVisible(false);
        getApplyBanker()->setVisible(true);
        m_bCanRobBanker = false;
    }
    
    tagUserData *pUserData = ClientSocketSink::sharedSocketSink()->GetMeUserData();
    if (nullptr != pUserData && m_BankUserList.size() > 2)
    {
        if (m_BankUserList[0].wUserID == pUserData->dwUserID || m_BankUserList[1].wUserID == pUserData->dwUserID)
        {
            m_bCanRobBanker = false;
        }
        else
        {
            m_bCanRobBanker = true;
        }
    }
    else
        m_bCanRobBanker = false;
    
    if (enGameNormal == m_GameState && m_nRobBanker < 3 && m_bCanRobBanker)
    {
        m_btRobBanker->setEnabled(true);
        m_btRobBanker->setColor(Color3B::WHITE);
    }
    else
    {
        m_btRobBanker->setEnabled(false);
        m_btRobBanker->setColor(Color3B(100, 100, 100));
    }
    
    //更新控件
	UpdateControl();

	DrawBankList();

	return true;
}

//切换庄家
bool SDBGameViewLayer::OnSubChangeBanker(const void * pBuffer, WORD wDataSize)
{
	ASSERT(wDataSize == sizeof(CMD_S_SDB_ChangeBanker));
	if (wDataSize != sizeof(CMD_S_SDB_ChangeBanker)) return false;
	CMD_S_SDB_ChangeBanker *pChangeBanker = (CMD_S_SDB_ChangeBanker *)pBuffer;
	m_nRobBanker = pChangeBanker->nRobBanker;

	//庄家信息
	m_BankZhangJI = 0;
	m_BankJuShu = 0;
	m_wBankerTimes = 0;
	m_lBankerWinScore = 0;
	SetBankerInfo(pChangeBanker->wBankerUser,0);
	
    //自己是庄家
    if (m_IsMeBank == true)
    {
        m_bnCancelBanker->setVisible(false);
        getApplyBanker()->setVisible(true);
        m_btRobBanker->setVisible(false);
    }
    
    m_IsMeBank = false;
	//自己判断
	if (pChangeBanker->wBankerUser == GetMeChairID())
	{
		m_wBankerTimes = 0;
		m_lBankerWinScore = 0;
		m_BankZhangJI = 0;
		m_BankJuShu = 0;
		m_IsMeBank = true;
        m_bnCancelBanker->setEnabled(true);
	}

	//删除玩家
	if (m_wBankerUser!=INVALID_CHAIR)
	{
		tagApplyUser ApplyUser;
        ApplyUser.userChairID = m_wBankerUser;
		ApplyUser.lUserScore=0;
		DeleteBankList(ApplyUser);
	}

    tagUserData *pUserData = ClientSocketSink::sharedSocketSink()->GetMeUserData();
    if (nullptr != pUserData && m_BankUserList.size() > 2)
    {
        if (m_BankUserList[0].wUserID == pUserData->dwUserID || m_BankUserList[1].wUserID == pUserData->dwUserID)
        {
            m_bCanRobBanker = false;
        }
        else
        {
            m_bCanRobBanker = true;
        }
    }
    else
        m_bCanRobBanker = false;
    
    if (enGameNormal == m_GameState && m_nRobBanker < 3 && m_bCanRobBanker)
    {
        m_btRobBanker->setEnabled(true);
        m_btRobBanker->setColor(Color3B::WHITE);
    }
    else
    {
        m_btRobBanker->setEnabled(false);
        m_btRobBanker->setColor(Color3B(100, 100, 100));
    }
    
	//更新界面
	UpdateControl();
	DrawBankList();

	return true;
}

//设置庄家
void SDBGameViewLayer::SetBankerInfo(WORD wBanker,LONGLONG lScore)
{
    removeAllChildByTag(m_BankNickName_Tga);
    removeAllChildByTag(m_BankGold_Tga);
    removeAllChildByTag(m_BankZhangJi_Tga);
    removeAllChildByTag(m_BankJuShu_Tga);
    removeAllChildByTag(m_bankBg_Tga);
    
    m_wBankerUser = wBanker;
    
    ui::Scale9Sprite* playerInfoBg = ui::Scale9Sprite::createWithSpriteFrameName("bg_player_info.png");
    playerInfoBg->setContentSize(Size(520, 150));
    playerInfoBg->setPosition(Vec2(450, 1000));
    addChild(playerInfoBg, 0, m_bankBg_Tga);
    
    char name[256];
    int tableid = ClientSocketSink::sharedSocketSink()->GetMeUserData()->wTableID;
    tagUserData* pUserData = ClientSocketSink::sharedSocketSink()->m_UserManager.GetUserData(tableid,m_wBankerUser);
    if(m_wBankerUser == INVALID_CHAIR || pUserData == NULL)
    {
        m_BankZhangJI = 0;
        m_lBankerScore = 0;
        m_BankJuShu = 0;
        
        Label* temp = Label::createWithSystemFont("无人坐庄", _GAME_FONT_NAME_1_,46);
        temp->setColor(Color3B::RED);
        temp->setTag(m_BankNickName_Tga);
        temp->setPosition(Vec2(450, 1000));
        addChild(temp);
        return;
    }
    else
    {
        m_lBankerScore=pUserData->lScore;
        sprintf(name, "%s", gbk_utf8(pUserData->szNickName).c_str());
    }
    
    string heads = g_GlobalUnits.getFace(pUserData->wGender, pUserData->lScore);
    Sprite* mHead = Sprite::createWithSpriteFrameName(heads);
    mHead->setPosition(Vec2(75, 75));
    mHead->setScale(0.9f);
    playerInfoBg->addChild(mHead);
    
    //昵称
    std::string szTempTitle;
    szTempTitle = "昵称: ";
    szTempTitle.append(name);
    
    Label* temp = Label::createWithSystemFont(szTempTitle.c_str(),_GAME_FONT_NAME_1_,30);
    temp->setAnchorPoint(Vec2(0,0.5f));
    temp->setTag(m_BankNickName_Tga);
    temp->setPosition(m_BankNickNamePos);
    addChild(temp);
    
    //金币
    char strc[32]="";
    Tools::AddComma(m_lBankerScore , strc);
    szTempTitle = "酒吧豆: ";
    szTempTitle.append(strc);
    temp = Label::createWithSystemFont(szTempTitle, _GAME_FONT_NAME_1_, 30);
    temp->setAnchorPoint(Vec2(0,0.5f));
    temp->setTag(m_BankGold_Tga);
    temp->setColor(_GAME_FONT_COLOR_4_);
    temp->setPosition(m_BankGoldPos);
    addChild(temp);
    
    //战绩
    Tools::AddComma(m_lBankerWinScore , strc);
    szTempTitle = "成绩: ";
    szTempTitle.append(strc);
    temp = Label::createWithSystemFont(szTempTitle, _GAME_FONT_NAME_1_, 30);
    temp->setAnchorPoint(Vec2(0,0.5f));
    temp->setTag(m_BankZhangJi_Tga);
    temp->setColor(_GAME_FONT_COLOR_4_);
    temp->setPosition(m_BankZhangJiPos);
    addChild(temp);
    
    //局数
    sprintf(strc,"%d",m_wBankerTimes);
    szTempTitle = "局数: ";
    szTempTitle.append(strc);
    temp = Label::createWithSystemFont(szTempTitle, _GAME_FONT_NAME_1_, 30);
    temp->setAnchorPoint(Vec2(0,0.5f));
    temp->setTag(m_BankJuShu_Tga);
    temp->setColor(_GAME_FONT_COLOR_4_);
    temp->setPosition(m_BankJuShuPos);
    addChild(temp);
    
    UpdateControl();
}


bool SDBGameViewLayer::OnSubRobBanker( const void * pBuffer, WORD wDataSize )
{
    ASSERT(wDataSize == sizeof(CMD_S_SDB_RobBanker));
	if (wDataSize != sizeof(CMD_S_SDB_RobBanker)) return false;

	CMD_S_SDB_RobBanker *pRobBanker = (CMD_S_SDB_RobBanker *)pBuffer;
    m_nRobBanker = pRobBanker->nRobBanker;
    
    //插入玩家
    int tableid = ClientSocketSink::sharedSocketSink()->GetMeUserData()->wTableID;
    tagUserData* pUserData = ClientSocketSink::sharedSocketSink()->m_UserManager.GetUserData(tableid, pRobBanker->wApplyUser);
    
    int i = 0;
    for (i = 0; i < m_BankUserList.size(); i++)
    {
        if (pUserData->dwUserID == m_BankUserList[i].wUserID)
            break;
    }
    
    swap(m_BankUserList[m_nRobBanker], m_BankUserList[i]);
    
    tagUserData *pMyData = ClientSocketSink::sharedSocketSink()->GetMeUserData();
    if (nullptr != pMyData && m_BankUserList.size() > 2)
    {
        if (m_BankUserList[0].wUserID == pMyData->dwUserID || m_BankUserList[1].wUserID == pMyData->dwUserID)
        {
            m_bCanRobBanker = false;
        }
        else
        {
            m_bCanRobBanker = true;
        }
    }
    else
        m_bCanRobBanker = false;
    
    //自己判断
    if (IsLookonMode()==false && GetMeChairID()==pRobBanker->wApplyUser)
    {
        getApplyBanker()->setVisible(false);
        m_btRobBanker->setVisible(false);
        m_bnCancelBanker->setVisible(true);
        m_bCanRobBanker = false;
    }
    
    DrawBankList();
	return true;
}

//游戏消息
bool SDBGameViewLayer::OnGameMessage(WORD wSubCmdID, const void * pData, WORD wDataSize)
{
	log("RECEIVE MESSAGE CMD[%d]" , wSubCmdID);
	switch (wSubCmdID)
	{
	case SUB_S_SDB_GAME_START://游戏开始
		{
			return OnSubGameStart(pData, wDataSize);
		}
		break;
	case SUB_S_SDB_PLACE_JETTON://玩家加注
		{
			return OnSubAddScore(pData, wDataSize);
		}
		break;
	case SUB_S_SDB_PLACE_JETTON_FAILED://加注失败
		{
			return OnSubAddScoreFailed(pData, wDataSize);
		}
		break;
	case SUB_S_SDB_SCORE_FULL://下注分满
		{
			return OnSubScoreFull(pData, wDataSize);
		}
		break;
	case SUB_S_SDB_SEND_CARD://发牌消息
		{
			return OnSubSendCard(pData, wDataSize);
		}
		break;
	case SUB_S_SDB_BEGIN_GET://开始要牌
		{
			return OnSubBeginGet(pData, wDataSize);
		}
		break;
	case SUB_S_SDB_GET_CARD://玩家要牌
		{
			return OnSubGetCard(pData, wDataSize);
		}
		break;
	case SUB_S_SDB_STOP_GET://是否停牌
		{
			return OnSubStopGet(pData, wDataSize);
		}
		break;
	case SUB_S_SDB_GAME_END://游戏结束
		{
			return OnSubGameEnd(pData, wDataSize);
		}
		break;
	case SUB_S_SDB_GAME_FREE://游戏空闲
		{
			return OnSubGameFree(pData, wDataSize);
		}
		break;
	case SUB_S_SDB_APPLY_BANKER://申请上庄
		{
			return OnSubApplyBanker(pData, wDataSize);
		}
		break;
	case SUB_S_SDB_CANCEL_APPLY://取消申请
		{
			return OnSubCancelApply(pData, wDataSize);
		}
		break;
	case SUB_S_SDB_CHANGE_BANKER://切换庄家
		{
			return OnSubChangeBanker(pData, wDataSize);
		}
		break;
	case SUB_S_SDB_ROB_BANKER:
		{
			return OnSubRobBanker(pData, wDataSize); 
		}
		break;
	case SUB_S_SDB_TAOPAO:
		{
			return true;
		}
		break;
	case SUB_S_SDB_CHECK_STATE:
		{
			return OnSubCheckState(pData, wDataSize);
		}
		break;
	}
	return true;
}

void SDBGameViewLayer::DrawBetInfo()
{
	//金币
	removeAllChildByTag(m_lAreaLimitScoreExTga);
    
	char strc[128]="";
	char str[128]="";
    Tools::AddComma(m_lTotalPlaceJetton , strc);
    sprintf(str, "当前下注: %s", strc);
	Label* mGoldFont = Label::createWithSystemFont(str,_GAME_FONT_NAME_1_,46);
	mGoldFont->setAnchorPoint(Vec2(0,0.5f));
	mGoldFont->setTag(m_lAreaLimitScoreExTga);
	mGoldFont->setPosition(Vec2(1400,290));
	mGoldFont->setColor(Color3B(90,222,185));
	addChild(mGoldFont);

	Tools::AddComma(m_lAreaLimitScoreEx-m_lTotalPlaceJetton , strc);
    sprintf(str, "还可下注: %s", strc);
	mGoldFont = Label::createWithSystemFont(str,_GAME_FONT_NAME_1_,46);
	mGoldFont->setAnchorPoint(Vec2(0,0.5f));
	mGoldFont->setTag(m_lAreaLimitScoreExTga);
	mGoldFont->setPosition(Vec2(1400,220));
	mGoldFont->setColor(Color3B(90,222,185));
	addChild(mGoldFont);
}

//空闲状态
void SDBGameViewLayer::OnStatusFree(CMD_S_SDB_StatusFree *pStatusFree)
{
	m_wBankerUser = pStatusFree->wBankerUser;
	m_wBankerTimes = pStatusFree->wBankerTimes;
	m_lBankerWinScore = pStatusFree->lBankerWinScore;
	m_lApplyBankerCondition = pStatusFree->lApplyBankerCondition;
	m_nRobBanker = pStatusFree->nRobBanker;
	m_lAreaLimitScoreEx = pStatusFree->lAreaLimitScore;

	SetBankerInfo(m_wBankerUser,0);
	UpdateControl();
}

//下注状态
void SDBGameViewLayer::OnStatusPlace(CMD_S_SDB_StatusPlace *pStatusPlace)
{
	m_wBankerUser = pStatusPlace->StatusFree.wBankerUser;
	m_wBankerTimes = pStatusPlace->StatusFree.wBankerTimes;
	m_lBankerWinScore = pStatusPlace->StatusFree.lBankerWinScore;
	m_lApplyBankerCondition = pStatusPlace->StatusFree.lApplyBankerCondition;
	m_nRobBanker = pStatusPlace->StatusFree.nRobBanker;
	m_lAreaLimitScoreEx = pStatusPlace->StatusFree.lAreaLimitScore;

	m_lMePlaceJetton = pStatusPlace->lPlaceJetton[GetMeChairID()];
	m_lTotalPlaceJetton = pStatusPlace->lTotalJetton;
	m_lTotalPlaceJettonEx = m_lTotalPlaceJetton;
	SetBankerInfo(m_wBankerUser,0);
	for (WORD wChairID=0; wChairID<SDB_GAME_PLAYER; wChairID++)
	{
		if (pStatusPlace->lPlaceJetton[wChairID] <= 0) continue;

		LONGLONG score = pStatusPlace->lPlaceJetton[wChairID];
		vector<LONGLONG>	m_EndScore;
		LONGLONG num = 0; bool isadd = false;
		num = score/1000000000; if(num != 0){isadd = true; m_EndScore.push_back(num);}
		num = score/100000000%10; if(num != 0){isadd = true; m_EndScore.push_back(num);}else{if(isadd)m_EndScore.push_back(num);}
		num = score/10000000%10; if(num != 0){isadd = true; m_EndScore.push_back(num);}else{if(isadd)m_EndScore.push_back(num);}
		num = score/1000000%10; if(num != 0){isadd = true; m_EndScore.push_back(num);}else{if(isadd)m_EndScore.push_back(num);}
		num = score/100000%10; if(num != 0){isadd = true; m_EndScore.push_back(num);}else{if(isadd)m_EndScore.push_back(num);}
		num = score/10000%10; if(num != 0){isadd = true; m_EndScore.push_back(num);}else{if(isadd)m_EndScore.push_back(num);}
		num = score/1000%10; if(num != 0){isadd = true; m_EndScore.push_back(num);}else{if(isadd)m_EndScore.push_back(num);}
		num = score/100%10; if(num != 0){isadd = true; m_EndScore.push_back(num);}else{if(isadd)m_EndScore.push_back(num);}
		num = score/10%10; if(num != 0){isadd = true; m_EndScore.push_back(num);}else{if(isadd)m_EndScore.push_back(num);}
		num = score%10; if(num != 0){isadd = true; m_EndScore.push_back(num);}else{if(isadd)m_EndScore.push_back(num);}
		
		int countc = 0;
		for (int i = m_EndScore.size()-1; i >= 0; i--)
		{
			if(countc == 2 && m_EndScore[i] != 0)
			{
				for (int j = 0; j< m_EndScore[i]; j++)
				{
					OnAddScoreGameStatus(wChairID,100);
				}
			}
			if(countc == 3 && m_EndScore[i] != 0)
			{
				for (int j = 0; j< m_EndScore[i]; j++)
				{
					OnAddScoreGameStatus(wChairID,1000);
				}
			}
			if(countc == 4 && m_EndScore[i] != 0)
			{
				for (int j = 0; j< m_EndScore[i]; j++)
				{
					OnAddScoreGameStatus(wChairID,10000);
				}
			}
			if(countc == 5 && m_EndScore[i] != 0)
			{
				for (int j = 0; j< m_EndScore[i]; j++)
				{
					OnAddScoreGameStatus(wChairID,100000);
				}
			}
			if(countc == 6 && m_EndScore[i] != 0)
			{
				for (int j = 0; j< m_EndScore[i]; j++)
				{
					OnAddScoreGameStatus(wChairID,1000000);
				}
			}
			if(countc == 7 && m_EndScore[i] != 0)
			{
				for (int j = 0; j< m_EndScore[i]; j++)
				{
					OnAddScoreGameStatus(wChairID,10000000);
				}
			}
			if(countc == 8 && m_EndScore[i] != 0)
			{
				for (int j = 0; j< m_EndScore[i]; j++)
				{
					OnAddScoreGameStatus(wChairID,10000000);
				}
			}
			countc++;
		}
	}
	UpdateControl();
}


//庄家要牌状态
void SDBGameViewLayer::OnStatusBanker(CMD_S_SDB_StatusBanker *pStatusBanker)
{
	m_wBankerUser = pStatusBanker->StatusFree.wBankerUser;
	m_wBankerTimes = pStatusBanker->StatusFree.wBankerTimes;
	m_lBankerWinScore = pStatusBanker->StatusFree.lBankerWinScore;
	m_lApplyBankerCondition = pStatusBanker->StatusFree.lApplyBankerCondition;
	m_nRobBanker = pStatusBanker->StatusFree.nRobBanker;
	m_lAreaLimitScoreEx = pStatusBanker->StatusFree.lAreaLimitScore;

	m_lMePlaceJetton = pStatusBanker->lPlaceJetton[GetMeChairID()];
	m_lTotalPlaceJetton = pStatusBanker->lTotalJetton;
	m_lTotalPlaceJettonEx = m_lTotalPlaceJetton;

	SetBankerInfo(m_wBankerUser,0);

	for (WORD wChairID=0; wChairID<SDB_GAME_PLAYER; wChairID++)
	{
		if (pStatusBanker->lPlaceJetton[wChairID] <= 0) continue;
		LONGLONG score = pStatusBanker->lPlaceJetton[wChairID];
		vector<LONGLONG>	m_EndScore;
		LONGLONG num = 0; bool isadd = false;
		num = score/1000000000; if(num != 0){isadd = true; m_EndScore.push_back(num);}
		num = score/100000000%10; if(num != 0){isadd = true; m_EndScore.push_back(num);}else{if(isadd)m_EndScore.push_back(num);}
		num = score/10000000%10; if(num != 0){isadd = true; m_EndScore.push_back(num);}else{if(isadd)m_EndScore.push_back(num);}
		num = score/1000000%10; if(num != 0){isadd = true; m_EndScore.push_back(num);}else{if(isadd)m_EndScore.push_back(num);}
		num = score/100000%10; if(num != 0){isadd = true; m_EndScore.push_back(num);}else{if(isadd)m_EndScore.push_back(num);}
		num = score/10000%10; if(num != 0){isadd = true; m_EndScore.push_back(num);}else{if(isadd)m_EndScore.push_back(num);}
		num = score/1000%10; if(num != 0){isadd = true; m_EndScore.push_back(num);}else{if(isadd)m_EndScore.push_back(num);}
		num = score/100%10; if(num != 0){isadd = true; m_EndScore.push_back(num);}else{if(isadd)m_EndScore.push_back(num);}
		num = score/10%10; if(num != 0){isadd = true; m_EndScore.push_back(num);}else{if(isadd)m_EndScore.push_back(num);}
		num = score%10; if(num != 0){isadd = true; m_EndScore.push_back(num);}else{if(isadd)m_EndScore.push_back(num);}
	
		int countc = 0;
		for (int i = m_EndScore.size()-1; i >= 0; i--)
		{
			if(countc == 2 && m_EndScore[i] != 0)
			{
				for (int j = 0; j< m_EndScore[i]; j++)
				{
					OnAddScoreGameStatus(wChairID,100);
				}
			}
			if(countc == 3 && m_EndScore[i] != 0)
			{
				for (int j = 0; j< m_EndScore[i]; j++)
				{
					OnAddScoreGameStatus(wChairID,1000);
				}
			}
			if(countc == 4 && m_EndScore[i] != 0)
			{
				for (int j = 0; j< m_EndScore[i]; j++)
				{
					OnAddScoreGameStatus(wChairID,10000);
				}
			}
			if(countc == 5 && m_EndScore[i] != 0)
			{
				for (int j = 0; j< m_EndScore[i]; j++)
				{
					OnAddScoreGameStatus(wChairID,100000);
				}
			}
			if(countc == 6 && m_EndScore[i] != 0)
			{
				for (int j = 0; j< m_EndScore[i]; j++)
				{
					OnAddScoreGameStatus(wChairID,1000000);
				}
			}
			if(countc == 7 && m_EndScore[i] != 0)
			{
				for (int j = 0; j< m_EndScore[i]; j++)
				{
					OnAddScoreGameStatus(wChairID,10000000);
				}
			}
			if(countc == 8 && m_EndScore[i] != 0)
			{
				for (int j = 0; j< m_EndScore[i]; j++)
				{
					OnAddScoreGameStatus(wChairID,10000000);
				}
			}
			countc++;
		}
	}
	
	m_BankerCardList.clear();
	m_PlayerCardList.clear();
	for (int i = 0; i < pStatusBanker->nBankerCardCount; i++)
	{
		m_BankerCardList.push_back(pStatusBanker->byBankerCardData[i]);
	}
	removeAllChildByTag(m_Send_Bank_CardTga);
	removeAllChildByTag(m_Send_Bank_CardEndTga);
	for (int i = 0; i < m_BankerCardList.size(); i++)
	{
		BYTE card = m_BankerCardList[i];
		string _name = GetCardStringName(card);
		Sprite* temp = Sprite::createWithSpriteFrameName(_name.c_str());
		temp->setScale(0.7f);
		int w = 0;
		if(m_BankerCardList.size() == 1) w = 0;
		else if(m_BankerCardList.size() == 2) w = 25;
		else if(m_BankerCardList.size() == 3) w = 50;
		else if(m_BankerCardList.size() == 4) w = 75;
		else if(m_BankerCardList.size() == 5) w = 100;
		temp->setPosition(Vec2(960+i*50-w,830));
		temp->setTag(m_Send_Bank_CardEndTga);		
		addChild(temp);
	}
	//StartEffectBanker();


	for (int i = 0; i < pStatusBanker->nPlayerCardCount; i++)
	{
		m_PlayerCardList.push_back(pStatusBanker->byPlayerCardData[i]);
	}
	removeAllChildByTag(m_Send_Player_CardTga);
	removeAllChildByTag(m_Send_Player_CardEndTga);
	for (int i = 0; i < m_PlayerCardList.size(); i++)
	{
		BYTE card = m_PlayerCardList[i];
		string _name = GetCardStringName(card);
		Sprite* temp = Sprite::createWithSpriteFrameName(_name.c_str());
		int w = 0;
		if(m_PlayerCardList.size() == 1) w = 0;
		else if(m_PlayerCardList.size() == 2) w = 25;
		else if(m_PlayerCardList.size() == 3) w = 50;
		else if(m_PlayerCardList.size() == 4) w = 75;
		else if(m_PlayerCardList.size() == 5) w = 100;
		temp->setPosition(Vec2(960+i*50-w,300));
		temp->setScale(0.7f);
		temp->setTag(m_Send_Player_CardEndTga);		
		addChild(temp);
	}

	UpdateControl();
	if (pStatusBanker->bStopGet)
	{
		m_bnGetCard->setVisible(false);
		m_bnStopGet->setVisible(false);
	}
}

//闲家要牌状态
void SDBGameViewLayer::OnStatusPlayer(CMD_S_SDB_StatusPlayer *pStatusPlayer)
{
	m_wBankerUser = pStatusPlayer->StatusFree.wBankerUser;
	m_wBankerTimes = pStatusPlayer->StatusFree.wBankerTimes;
	m_lBankerWinScore = pStatusPlayer->StatusFree.lBankerWinScore;
	m_lApplyBankerCondition = pStatusPlayer->StatusFree.lApplyBankerCondition;
	m_nRobBanker = pStatusPlayer->StatusFree.nRobBanker;
	m_lAreaLimitScoreEx = pStatusPlayer->StatusFree.lAreaLimitScore;

	m_lMePlaceJetton = pStatusPlayer->lPlaceJetton[GetMeChairID()];
	m_lTotalPlaceJetton = pStatusPlayer->lTotalJetton;
	m_lTotalPlaceJettonEx = m_lTotalPlaceJetton;
	for (WORD wChairID=0; wChairID<SDB_GAME_PLAYER; wChairID++)
	{
		if (pStatusPlayer->lPlaceJetton[wChairID] <= 0) continue;
		LONGLONG score = pStatusPlayer->lPlaceJetton[wChairID];
		vector<LONGLONG>	m_EndScore;
		LONGLONG num = 0; bool isadd = false;
		num = score/1000000000; if(num != 0){isadd = true; m_EndScore.push_back(num);}
		num = score/100000000%10; if(num != 0){isadd = true; m_EndScore.push_back(num);}else{if(isadd)m_EndScore.push_back(num);}
		num = score/10000000%10; if(num != 0){isadd = true; m_EndScore.push_back(num);}else{if(isadd)m_EndScore.push_back(num);}
		num = score/1000000%10; if(num != 0){isadd = true; m_EndScore.push_back(num);}else{if(isadd)m_EndScore.push_back(num);}
		num = score/100000%10; if(num != 0){isadd = true; m_EndScore.push_back(num);}else{if(isadd)m_EndScore.push_back(num);}
		num = score/10000%10; if(num != 0){isadd = true; m_EndScore.push_back(num);}else{if(isadd)m_EndScore.push_back(num);}
		num = score/1000%10; if(num != 0){isadd = true; m_EndScore.push_back(num);}else{if(isadd)m_EndScore.push_back(num);}
		num = score/100%10; if(num != 0){isadd = true; m_EndScore.push_back(num);}else{if(isadd)m_EndScore.push_back(num);}
		num = score/10%10; if(num != 0){isadd = true; m_EndScore.push_back(num);}else{if(isadd)m_EndScore.push_back(num);}
		num = score%10; if(num != 0){isadd = true; m_EndScore.push_back(num);}else{if(isadd)m_EndScore.push_back(num);}
		
		int countc = 0;
		for (int i = m_EndScore.size()-1; i >= 0; i--)
		{
			if(countc == 2 && m_EndScore[i] != 0)
			{
				for (int j = 0; j< m_EndScore[i]; j++)
				{
					OnAddScoreGameStatus(wChairID,100);
				}
			}
			if(countc == 3 && m_EndScore[i] != 0)
			{
				for (int j = 0; j< m_EndScore[i]; j++)
				{
					OnAddScoreGameStatus(wChairID,1000);
				}
			}
			if(countc == 4 && m_EndScore[i] != 0)
			{
				for (int j = 0; j< m_EndScore[i]; j++)
				{
					OnAddScoreGameStatus(wChairID,10000);
				}
			}
			if(countc == 5 && m_EndScore[i] != 0)
			{
				for (int j = 0; j< m_EndScore[i]; j++)
				{
					OnAddScoreGameStatus(wChairID,100000);
				}
			}
			if(countc == 6 && m_EndScore[i] != 0)
			{
				for (int j = 0; j< m_EndScore[i]; j++)
				{
					OnAddScoreGameStatus(wChairID,1000000);
				}
			}
			if(countc == 7 && m_EndScore[i] != 0)
			{
				for (int j = 0; j< m_EndScore[i]; j++)
				{
					OnAddScoreGameStatus(wChairID,10000000);
				}
			}
			if(countc == 8 && m_EndScore[i] != 0)
			{
				for (int j = 0; j< m_EndScore[i]; j++)
				{
					OnAddScoreGameStatus(wChairID,10000000);
				}
			}
			countc++;
		}
	}

	m_BankerCardList.clear();
	m_PlayerCardList.clear();
	for (int i = 0; i < pStatusPlayer->nBankerCardCount; i++)
	{
		m_BankerCardList.push_back(pStatusPlayer->byBankerCardData[i]);
	}
	removeAllChildByTag(m_Send_Bank_CardTga);
	removeAllChildByTag(m_Send_Bank_CardEndTga);
	for (int i = 0; i < m_BankerCardList.size(); i++)
	{
		BYTE card = m_BankerCardList[i];
		string _name = GetCardStringName(card);
		Sprite* temp = Sprite::createWithSpriteFrameName(_name.c_str());
		temp->setScale(0.7f);
		int w = 0;
		if(m_BankerCardList.size() == 1) w = 0;
		else if(m_BankerCardList.size() == 2) w = 25;
		else if(m_BankerCardList.size() == 3) w = 50;
		else if(m_BankerCardList.size() == 4) w = 75;
		else if(m_BankerCardList.size() == 5) w = 100;
		temp->setPosition(Vec2(960+i*50-w,830));
		temp->setTag(m_Send_Bank_CardEndTga);		
		addChild(temp);
	}
	//StartEffectBanker();


	for (int i = 0; i < pStatusPlayer->nPlayerCardCount; i++)
	{
		m_PlayerCardList.push_back(pStatusPlayer->byPlayerCardData[i]);
	}
	removeAllChildByTag(m_Send_Player_CardTga);
	removeAllChildByTag(m_Send_Player_CardEndTga);
	for (int i = 0; i < m_PlayerCardList.size(); i++)
	{
		BYTE card = m_PlayerCardList[i];
		string _name = GetCardStringName(card);
		Sprite* temp = Sprite::createWithSpriteFrameName(_name.c_str());
		int w = 0;
		if(m_PlayerCardList.size() == 1) w = 0;
		else if(m_PlayerCardList.size() == 2) w = 25;
		else if(m_PlayerCardList.size() == 3) w = 50;
		else if(m_PlayerCardList.size() == 4) w = 75;
		else if(m_PlayerCardList.size() == 5) w = 100;
		temp->setPosition(Vec2(960+i*50-w,300));
		temp->setScale(0.7f);
		temp->setTag(m_Send_Player_CardEndTga);		
		addChild(temp);
	}

	UpdateControl();
	if (pStatusPlayer->bStopGet)
	{
		m_bnGetCard->setVisible(false);
		m_bnStopGet->setVisible(false);
	}
}


//结算状态
void SDBGameViewLayer::OnStatusGameEnd(CMD_S_SDB_StatusGameEnd *pGameEnd)
{
	m_wBankerUser = pGameEnd->StatusFree.wBankerUser;
	m_wBankerTimes = pGameEnd->StatusFree.wBankerTimes;
	m_lBankerWinScore = pGameEnd->StatusFree.lBankerWinScore;
	m_lApplyBankerCondition = pGameEnd->StatusFree.lApplyBankerCondition;
	m_nRobBanker = pGameEnd->StatusFree.nRobBanker;

	m_lMePlaceJetton = pGameEnd->lPlaceJetton[GetMeChairID()];
	m_lTotalPlaceJetton = pGameEnd->lTotalJetton;
	m_lTotalPlaceJettonEx = m_lTotalPlaceJetton;
	SetBankerInfo(m_wBankerUser,0);
	for (WORD wChairID=0; wChairID<SDB_GAME_PLAYER; wChairID++)
	{
		if (pGameEnd->lPlaceJetton[wChairID] <= 0) continue;
		LONGLONG score = pGameEnd->lPlaceJetton[wChairID];
		vector<LONGLONG>	m_EndScore;
		LONGLONG num = 0; bool isadd = false;
		num = score/1000000000; if(num != 0){isadd = true; m_EndScore.push_back(num);}
		num = score/100000000%10; if(num != 0){isadd = true; m_EndScore.push_back(num);}else{if(isadd)m_EndScore.push_back(num);}
		num = score/10000000%10; if(num != 0){isadd = true; m_EndScore.push_back(num);}else{if(isadd)m_EndScore.push_back(num);}
		num = score/1000000%10; if(num != 0){isadd = true; m_EndScore.push_back(num);}else{if(isadd)m_EndScore.push_back(num);}
		num = score/100000%10; if(num != 0){isadd = true; m_EndScore.push_back(num);}else{if(isadd)m_EndScore.push_back(num);}
		num = score/10000%10; if(num != 0){isadd = true; m_EndScore.push_back(num);}else{if(isadd)m_EndScore.push_back(num);}
		num = score/1000%10; if(num != 0){isadd = true; m_EndScore.push_back(num);}else{if(isadd)m_EndScore.push_back(num);}
		num = score/100%10; if(num != 0){isadd = true; m_EndScore.push_back(num);}else{if(isadd)m_EndScore.push_back(num);}
		num = score/10%10; if(num != 0){isadd = true; m_EndScore.push_back(num);}else{if(isadd)m_EndScore.push_back(num);}
		num = score%10; if(num != 0){isadd = true; m_EndScore.push_back(num);}else{if(isadd)m_EndScore.push_back(num);}
		
		int countc = 0;
		for (int i = m_EndScore.size()-1; i >= 0; i--)
		{
			if(countc == 2 && m_EndScore[i] != 0)
			{
				for (int j = 0; j< m_EndScore[i]; j++)
				{
					OnAddScoreGameStatus(wChairID,100);
				}
			}
			if(countc == 3 && m_EndScore[i] != 0)
			{
				for (int j = 0; j< m_EndScore[i]; j++)
				{
					OnAddScoreGameStatus(wChairID,1000);
				}
			}
			if(countc == 4 && m_EndScore[i] != 0)
			{
				for (int j = 0; j< m_EndScore[i]; j++)
				{
					OnAddScoreGameStatus(wChairID,10000);
				}
			}
			if(countc == 5 && m_EndScore[i] != 0)
			{
				for (int j = 0; j< m_EndScore[i]; j++)
				{
					OnAddScoreGameStatus(wChairID,100000);
				}
			}
			if(countc == 6 && m_EndScore[i] != 0)
			{
				for (int j = 0; j< m_EndScore[i]; j++)
				{
					OnAddScoreGameStatus(wChairID,1000000);
				}
			}
			if(countc == 7 && m_EndScore[i] != 0)
			{
				for (int j = 0; j< m_EndScore[i]; j++)
				{
					OnAddScoreGameStatus(wChairID,10000000);
				}
			}
			if(countc == 8 && m_EndScore[i] != 0)
			{
				for (int j = 0; j< m_EndScore[i]; j++)
				{
					OnAddScoreGameStatus(wChairID,10000000);
				}
			}
			countc++;
		}
	}

	m_BankerCardList.clear();
	m_PlayerCardList.clear();
	for (int i = 0; i < pGameEnd->nBankerCardCount; i++)
	{
		m_BankerCardList.push_back(pGameEnd->byBankerCardData[i]);
	}
	removeAllChildByTag(m_Send_Bank_CardTga);
	removeAllChildByTag(m_Send_Bank_CardEndTga);
	for (int i = 0; i < m_BankerCardList.size(); i++)
	{
		BYTE card = m_BankerCardList[i];
		string _name = GetCardStringName(card);
		Sprite* temp = Sprite::createWithSpriteFrameName(_name.c_str());
		temp->setScale(0.7f);
		int w = 0;
		if(m_BankerCardList.size() == 1) w = 0;
		else if(m_BankerCardList.size() == 2) w = 25;
		else if(m_BankerCardList.size() == 3) w = 50;
		else if(m_BankerCardList.size() == 4) w = 75;
		else if(m_BankerCardList.size() == 5) w = 100;
		temp->setPosition(Vec2(960+i*50-w,830));
		temp->setTag(m_Send_Bank_CardEndTga);		
		addChild(temp);
	}
	//StartEffectBanker();


	for (int i = 0; i < pGameEnd->nPlayerCardCount; i++)
	{
		m_PlayerCardList.push_back(pGameEnd->byPlayerCardData[i]);
	}
	removeAllChildByTag(m_Send_Player_CardTga);
	removeAllChildByTag(m_Send_Player_CardEndTga);
	for (int i = 0; i < m_PlayerCardList.size(); i++)
	{
		BYTE card = m_PlayerCardList[i];
		string _name = GetCardStringName(card);
		Sprite* temp = Sprite::createWithSpriteFrameName(_name.c_str());
		int w = 0;
		if(m_PlayerCardList.size() == 1) w = 0;
		else if(m_PlayerCardList.size() == 2) w = 25;
		else if(m_PlayerCardList.size() == 3) w = 50;
		else if(m_PlayerCardList.size() == 4) w = 75;
		else if(m_PlayerCardList.size() == 5) w = 100;
		temp->setPosition(Vec2(960+i*50-w,300));
		temp->setScale(0.7f);
		temp->setTag(m_Send_Player_CardEndTga);		
		addChild(temp);
	}
	UpdateControl();
}

//场景消息
bool SDBGameViewLayer::OnGameSceneMessage(BYTE cbGameStatus, const void * pData, WORD wDataSize)
{
	switch (cbGameStatus)
	{
	case GS_FREE:				//空闲状态
		{
			if (wDataSize != sizeof(CMD_S_SDB_StatusFree))
				return false;
			CMD_S_SDB_StatusFree *pStatusFree = (CMD_S_SDB_StatusFree *)pData;
			m_cbGameStatus = GS_FREE;
            m_GameState = enGameNormal;
			StartTime(GetMeChairID(), SDB_IDI_FREE, pStatusFree->byTimeLeave);
			OnStatusFree(pStatusFree);
			break;
		}
	case GS_PLAYING:			//游戏状态
		{
			if (wDataSize != sizeof(CMD_S_SDB_StatusPlace))
				return false;
			CMD_S_SDB_StatusPlace *pStatusPlace = (CMD_S_SDB_StatusPlace *)pData;
			m_cbGameStatus = GS_PLAYING;
            m_GameState = enGameBet;
			StartTime(GetMeChairID(), SDB_IDI_PLACE_JETTON, pStatusPlace->StatusFree.byTimeLeave);
			OnStatusPlace(pStatusPlace);
			break;
		}
	case SDB_GS_BANKER_CARD:	//结束状态
		{
			if (wDataSize != sizeof(CMD_S_SDB_StatusBanker))
				return false;

			CMD_S_SDB_StatusBanker *pStatusBanker = (CMD_S_SDB_StatusBanker *)pData;
			m_cbGameStatus = SDB_GS_BANKER_CARD;
            m_GameState = enGameBet;
			StartTime(GetMeChairID(), SDB_IDI_BANKER_CARD, pStatusBanker->StatusFree.byTimeLeave);
			OnStatusBanker(pStatusBanker);
			break;
		}
	case SDB_GS_PLAYER_CARD:
		{
			if (wDataSize != sizeof(CMD_S_SDB_StatusPlayer))
				return false;
			CMD_S_SDB_StatusPlayer *pStatusPlayer = (CMD_S_SDB_StatusPlayer *)pData;
			m_cbGameStatus = SDB_GS_PLAYER_CARD;
            m_GameState = enGameBet;
			StartTime(GetMeChairID(), SDB_IDI_PLAYER_CARD, pStatusPlayer->StatusFree.byTimeLeave);
			OnStatusPlayer(pStatusPlayer);
			break;
		}
	case SDB_GS_GAME_END:
		{
			if (wDataSize != sizeof(CMD_S_SDB_StatusGameEnd))
				return false;
			CMD_S_SDB_StatusGameEnd *pGameEnd = (CMD_S_SDB_StatusGameEnd *)pData;
			m_cbGameStatus = SDB_GS_GAME_END;
            m_GameState = enGameBet;
			StartTime(GetMeChairID(), SDB_IDI_GAME_END, pGameEnd->StatusFree.byTimeLeave);
			OnStatusGameEnd(pGameEnd);
			break;
		}
	}
	return true;
}

void SDBGameViewLayer::InitGame()
{
	//背景图
	m_BackSpr = Sprite::create("sdb/game_back.jpg");
	m_BackSpr->setPosition(_STANDARD_SCREEN_CENTER_IN_PIXELS_);
	addChild(m_BackSpr);

	m_ClockSpr = Sprite::createWithSpriteFrameName("SDB_TimeBack.png");
	m_ClockSpr->setPosition(Vec2(1000, 965));
	m_ClockSpr->setVisible(false);
	addChild(m_ClockSpr);

    Sprite *chipBg = Sprite::createWithSpriteFrameName("chip_bottom.png");
    chipBg->setPosition(Vec2(_STANDARD_SCREEN_CENTER_.x, 0));
    chipBg->setAnchorPoint(Vec2(0.5f, 0));
    addChild(chipBg);
    
    Sprite* chipTop = Sprite::createWithSpriteFrameName("chip_top.png");
    chipTop->setPosition(Vec2(_STANDARD_SCREEN_CENTER_.x, 0));
    chipTop->setAnchorPoint(Vec2(0.5f, 0));
    addChild(chipTop, 100);
    
    m_BankListBackSpr = Sprite::createWithSpriteFrameName("bg_tongyongkuang3.png");
    m_BankListBackSpr->setPosition(Vec2(_STANDARD_SCREEN_CENTER_.x, 1500));
    addChild(m_BankListBackSpr,10000);
    
    Sprite* pBankTitle = Sprite::createWithSpriteFrameName("shangzhuangliebiao.png");
    pBankTitle->setScale(0.8f);
    pBankTitle->setPosition(Vec2(519,480));
    m_BankListBackSpr->addChild(pBankTitle);
  
    m_listView = ui::ListView::create();
    m_listView->setContentSize(Size(920, 380));
    m_listView->setDirection(ui::ScrollView::Direction::VERTICAL);
    m_listView->setPosition(Vec2(50,30));
    m_BankListBackSpr->addChild(m_listView);
  
    //初始标识
	m_Head_Tga = 10;
	m_Head_NickName_Tga = 11;
	m_Head_Gold_Tga		= 12;
	m_Head_WinScore_Tga	= 13;
	m_TimeType_Tga		= 14;
	m_Jetton_Tga		= 15;
	m_Send_Bank_CardTga	= 16;
	m_Send_Player_CardTga=17;
	m_Send_Bank_CardEndTga=18;
	m_Send_Player_CardEndTga=19;
	m_AllScoreTga		= 20;
	m_EndScoreBack_Tga	= 21;
	m_GoldAnim[0]		= 22;
	m_GoldAnim[1]		= 23;
	m_GoldAnim[2]		= 24;
	m_GoldAnim[3]		= 25;
	m_GoldAnim[4]		= 26;
	m_GoldAnim[5]		= 27;
	m_GoldAnim[6]		= 28;
    
	m_BankNickName_Tga	= 37;
	m_BankGold_Tga		= 38;
	m_BankZhangJi_Tga	= 39;
	m_BankJuShu_Tga		= 40;
	m_CardTypeAnimTga   = 41;
	m_BankCardTypeAnimTga=42;
	m_lAreaLimitScoreExTga=50;
	m_BetFullTga		= 51;
    m_bankBg_Tga       = 52;
    m_MeScore_Tga      = 53;
    m_BtnAnim_Tga      = 54;

	//坐标
	m_Head_Pos          = Vec2(250,100);
	m_Head_NickNamePos  = Vec2(150,115);
	m_Head_GoldPos      = Vec2(150,75);
	m_Head_WinScorePos  = Vec2(150,35);

    m_BankNickNamePos   = Vec2(350,1050);
    m_BankGoldPos       = Vec2(350,1015);
    m_BankZhangJiPos    = Vec2(350,980);
    m_BankJuShuPos      = Vec2(350,945);
    
    m_BankListPos[0]    = Vec2(25,180);
	m_BankListPos[1]    = Vec2(25,140);
	m_BankListPos[2]    = Vec2(25,100);
	m_BankListPos[3]    = Vec2(25,60);
    m_IsLayerMove = false;
}

void SDBGameViewLayer::AddButton()
{
	m_btSeting = CreateButton("seting" ,Vec2(1830,1000),Btn_Seting);
	addChild(m_btSeting , 0 , Btn_Seting);

	m_btReturnBack = CreateButton("Returnback" ,Vec2(90,1000),Btn_BackToLobby);
	addChild(m_btReturnBack , 0 , Btn_BackToLobby);

    m_bn100 = CreateButton("chip_100" ,Vec2(540,80),Btn_100);
    addChild(m_bn100 , 1, Btn_100);
    
    m_bn1000 = CreateButton("chip_1000" ,Vec2(680,80),Btn_1000);
    addChild(m_bn1000 , 1, Btn_1000);
    
    m_bn1W = CreateButton("chip_1w" ,Vec2(820,80),Btn_1W);
    addChild(m_bn1W , 1, Btn_1W);
    
    m_bn10W = CreateButton("chip_10w" ,Vec2(960,80),Btn_10W);
    addChild(m_bn10W , 1, Btn_10W);
    
    m_bn100W = CreateButton("chip_100w" ,Vec2(1100,80),Btn_100w);
    addChild(m_bn100W , 1, Btn_100w);
    
    m_bn500W = CreateButton("chip_500w" ,Vec2(1240,80),Btn_500W);
    addChild(m_bn500W , 1, Btn_500W);

    m_bn1000W = CreateButton("chip_1000w" ,Vec2(1380,80),Btn_1000W);
    addChild(m_bn1000W , 1, Btn_1000W);
    
    m_bnGetCard = CreateButton("GetCard" ,Vec2(200 ,300),GetCard);
	addChild(m_bnGetCard , 0 , GetCard);
	m_bnGetCard->setVisible(false);
	
	m_bnStopGet = CreateButton("GetCardStop" ,Vec2(500 ,300),StopCard);
	addChild(m_bnStopGet , 0 , StopCard);
	m_bnStopGet->setVisible(false);

	Menu *applyBanker = CreateButton("BtnCallBank" ,Vec2(1600 ,1000),ApplyBanker);//申请上庄
	addChild(applyBanker , 0 , ApplyBanker);
    
	m_btRobBanker = CreateButton("BtnQiangBank" ,Vec2(1290 ,1000),RobBanker);  //抢庄
	addChild(m_btRobBanker , 0 , RobBanker);
	m_btRobBanker->setVisible(false);

	m_bnCancelBanker = CreateButton("BtnCancleBank" ,Vec2(1600 ,1000),CancleBanker); //我要下庄
	addChild(m_bnCancelBanker , 0 , CancleBanker);
	m_bnCancelBanker->setVisible(false);

	m_BankBtn = CreateButton("bankBtn" ,Vec2(1730,80),Btn_BankBtn);
	addChild(m_BankBtn , 0 , Btn_BankBtn);
}

Menu* SDBGameViewLayer::CreateButton(std::string szBtName ,const Vec2 &p , int tag )
{
	return Tools::Button(StrToChar(szBtName+"_normal.png") , StrToChar(szBtName+"_click.png") , p, this , menu_selector(SDBGameViewLayer::callbackBt) , tag);
}

void SDBGameViewLayer::AddPlayerInfo()
{
    const tagUserData* pUserData = ClientSocketSink::sharedSocketSink()->GetMeUserData();
    if (pUserData == NULL) return;
    
    removeAllChildByTag(m_Head_Tga);
    //头像
    ui::Scale9Sprite* playerInfoBg = ui::Scale9Sprite::createWithSpriteFrameName("bg_player_info.png");
    playerInfoBg->setContentSize(Size(450, 150));
    playerInfoBg->setPosition(Vec2(m_Head_Pos.x, m_Head_Pos.y+5));
    addChild(playerInfoBg, 0, m_Head_Tga);
    
    Sprite* headBg = Sprite::createWithSpriteFrameName("bg_head_img.png");
    headBg->setPosition(Vec2(70, 75));
    playerInfoBg->addChild(headBg);
    
    string heads = g_GlobalUnits.getFace(pUserData->wGender, pUserData->lScore);
    Sprite* mHead = Sprite::createWithSpriteFrameName(heads);
    mHead->setPosition(Vec2(75, 75));
    mHead->setScale(0.9f);
    playerInfoBg->addChild(mHead);
    
    removeAllChildByTag(m_Head_Gold_Tga);
    char strc[32]="";
    LONGLONG lMon = pUserData->lScore;
    memset(strc , 0 , sizeof(strc));
    Tools::AddComma(lMon , strc);
    
    string goldstr = "酒吧豆: ";
    goldstr.append(strc);
    Label* mGoldFont;
    mGoldFont = Label::createWithSystemFont(goldstr,_GAME_FONT_NAME_1_,30);
    mGoldFont->setTag(m_Head_Gold_Tga);
    playerInfoBg->addChild(mGoldFont);
    mGoldFont->setColor(Color3B::YELLOW);
    mGoldFont->setAnchorPoint(Vec2(0, 0.5f));
    mGoldFont->setPosition(m_Head_GoldPos);
    
    //昵称
    removeAllChildByTag(m_Head_NickName_Tga);
    string namestr = "昵称: ";
    namestr.append(gbk_utf8(pUserData->szNickName));
    Label* mNickfont = Label::createWithSystemFont(namestr, _GAME_FONT_NAME_1_, 30);
    mNickfont->setTag(m_Head_NickName_Tga);
    playerInfoBg->addChild(mNickfont);
    mNickfont->setAnchorPoint(Vec2(0,0.5f));
    mNickfont->setPosition(m_Head_NickNamePos);
    
    //输赢金币
    removeAllChildByTag(m_Head_WinScore_Tga);
    Tools::AddComma(m_lMeWinScore, strc);
    string winStr = "成绩: ";
    winStr.append(strc);
    mGoldFont = Label::createWithSystemFont(winStr,_GAME_FONT_NAME_1_,30);
    mGoldFont->setAnchorPoint(Vec2(0,0.5f));
    mGoldFont->setTag(m_Head_WinScore_Tga);
    mGoldFont->setPosition(m_Head_WinScorePos);
    playerInfoBg->addChild(mGoldFont);
}

string SDBGameViewLayer::AddCommaToNum(LONG Num)
{
    char _str[256];
	sprintf(_str,"%ld", Num);
	string _string = _str;
	ssize_t step = _string.length()/3;
	for (int i = 1; i <= step; i++)
	{
		_string.insert(_string.length()-(i-1)-(i*3), ",");
	}
	return _string;
}

void SDBGameViewLayer::backLoginView( Ref *pSender )
{
	IGameView::backLoginView(pSender);	
}

void SDBGameViewLayer::OnQuit()
{
	m_pGameScene->removeChild(this);
}


void SDBGameViewLayer::StartTime(int chair, int _type,int _time)
{
	m_StartTime = _time;
	schedule(schedule_selector(SDBGameViewLayer::UpdateTime), 1);
	m_ClockSpr->setVisible(true);
	UpdateTime(m_StartTime);
    m_timeType = _type;
    
	removeAllChildByTag(m_TimeType_Tga);
	std::string szTempTitle;
	if (_type == SDB_IDI_PLACE_JETTON)
        szTempTitle = "time_jetton.png";	//下注时间
	else if (_type == SDB_IDI_PLAYER_CARD) szTempTitle = "time_player.png";
	else if (_type == SDB_IDI_BANKER_CARD) szTempTitle = "time_banker.png";
	else if (_type == SDB_IDI_GAME_END) szTempTitle = "time_score.png";
	else if (_type == SDB_IDI_FREE) szTempTitle = "time_free.png";
	
	Sprite* temp = Sprite::createWithSpriteFrameName(szTempTitle.c_str());
	temp->setPosition(Vec2(1000, 1040));
	temp->setTag(m_TimeType_Tga);
	addChild(temp);
}

void SDBGameViewLayer::StopTime()
{
	unschedule(schedule_selector(SDBGameViewLayer::UpdateTime));
	m_ClockSpr->removeAllChildren();
	m_ClockSpr->setVisible(false);
}

void SDBGameViewLayer::UpdateTime(float fp)
{
	if (m_StartTime < 0)
	{
		unschedule(schedule_selector(SDBGameViewLayer::UpdateTime));
		return;
	}
	m_ClockSpr->removeAllChildren();
    
    std::string str = StringUtils::toString(m_StartTime);
    if (m_StartTime < 10)
        str = "0" + str;
    LabelAtlas *time = LabelAtlas::create(str, "Common/time_1.png", 23, 35, '+');
    time->setPosition(Vec2(20, 25));
    m_ClockSpr->addChild(time);
    
    m_StartTime--;
}

bool SDBGameViewLayer::IsLookonMode()
{
	return m_LookMode;
}

string SDBGameViewLayer::GetCardStringName(BYTE card)
{
	if (card == 0) return "SDBbackCard.png";
	char tt[32];
	sprintf(tt,"%0x",card);
	BYTE _value = m_GameLogic.GetCardValue(card);
	BYTE _color = m_GameLogic.GetCardColor(card);
	string temp;
	if (_color == 0) temp = "fangkuai_";
	if (_color == 1) temp = "meihua_";
	if (_color == 2) temp = "hongtao_";
	if (_color == 3) temp = "heitao_";
	char _valstr[32];
	ZeroMemory(_valstr,32);
	if (card == 0x41) //小王
	{
		sprintf(_valstr,"xiaowang.png");
	}
	else if (card == 0x42)
	{
		sprintf(_valstr,"dawang.png");
	}
	else if (_value < 10)
	{
		sprintf(_valstr,"0%d.png",_value);
	}
	else 
	{
		sprintf(_valstr,"%d.png",_value);
	}

	temp+=_valstr;
	return temp;
}

//更新控件状态
void SDBGameViewLayer::UpdateControl()
{
	if ((m_cbGameStatus != GS_PLAYING) || (m_wBankerUser == INVALID_CHAIR))
	{
		m_bn100->setEnabled(FALSE);
        m_bn1000->setEnabled(FALSE);
		m_bn1W->setEnabled(FALSE);
		m_bn10W->setEnabled(FALSE);
		m_bn100W->setEnabled(FALSE);
		m_bn500W->setEnabled(FALSE);
        m_bn1000W->setEnabled(FALSE);
        
        m_bn100->setColor(Color3B(100,100,100));
		m_bn1000->setColor(Color3B(100,100,100));
		m_bn1W->setColor(Color3B(100,100,100));
		m_bn10W->setColor(Color3B(100,100,100));
		m_bn100W->setColor(Color3B(100,100,100));
		m_bn500W->setColor(Color3B(100,100,100));
        m_bn1000W->setColor(Color3B(100,100,100));
        
		removeAllChildByTag(m_GoldAnim[0]);
		removeAllChildByTag(m_GoldAnim[1]);
		removeAllChildByTag(m_GoldAnim[2]);
		removeAllChildByTag(m_GoldAnim[3]);
		removeAllChildByTag(m_GoldAnim[4]);
		removeAllChildByTag(m_GoldAnim[5]);
		removeAllChildByTag(m_GoldAnim[6]);
	}
	m_bnGetCard->setVisible(false);
	m_bnStopGet->setVisible(false);

	switch (m_cbGameStatus)
	{
	case GS_PLAYING:
		{
			if ((GetMeChairID() != m_wBankerUser) && (m_wBankerUser != INVALID_CHAIR))
			{

                m_bn100->setEnabled(true);
                m_bn100->setColor(Color3B(255,255,255));

                m_bn1000->setEnabled(true);
                m_bn1000->setColor(Color3B(255,255,255));

                m_bn1W->setEnabled(true);
                m_bn1W->setColor(Color3B(255,255,255));

                m_bn10W->setEnabled(true);
                m_bn10W->setColor(Color3B(255,255,255));
                m_bn100W->setEnabled(true);
                m_bn100W->setColor(Color3B(255,255,255));

                m_bn500W->setEnabled(true);
                m_bn500W->setColor(Color3B(255,255,255));

                m_bn1000W->setEnabled(true);
                m_bn1000W->setColor(Color3B(255,255,255));
			}
            break;
		}
	case SDB_GS_BANKER_CARD:
		{
			if (m_wBankerUser == GetMeChairID())
			{
				m_bnGetCard->setVisible(true);
				BYTE byCardData[MAX_COUNT] = {0};
				int nCardCount = MAX_COUNT;
				for (int i = 0; i < m_PlayerCardList.size(); i++)
				{
					byCardData[i] = m_PlayerCardList[i];
				}
				INT nCardType = m_GameLogic.GetCardType(byCardData, nCardCount);
				if (nCardType!=(int)(CGameLogicSDB::SDB_CT_1))
				{
					m_bnGetCard->setEnabled(true);
					m_bnGetCard->setColor(Color3B(255,255,255));
				}
				else
				{
					m_bnGetCard->setEnabled(false);
					m_bnGetCard->setColor(Color3B(100,100,100));
				}
				m_bnStopGet->setVisible(true);
			}
		}
		break;
	case SDB_GS_PLAYER_CARD:   //----wcj  停止要牌
		{
			if ((m_wBankerUser != GetMeChairID()) && (m_PlayerCardList.size() > 0) && (m_bStopGet!=1))
			{
				m_bnGetCard->setVisible(true);
				m_bnGetCard->setEnabled(true);
				m_bnGetCard->setColor(Color3B(255,255,255));
				m_bnStopGet->setVisible(true);
				m_bnStopGet->setEnabled(true);
				m_bnStopGet->setColor(Color3B(255,255,255));
			}
            break;
		}
	default: break;
	}
}
Menu* SDBGameViewLayer::getApplyBanker()
{
    Menu *ret = dynamic_cast<Menu*>(getChildByTag(ApplyBanker));
    return ret;
}

void SDBGameViewLayer::PlayGameMusicBK()
{
	SoundUtil::sharedEngine()->stopBackMusic();
	int _rand = getRand(1,9);
	char tempsound[32];
	sprintf(tempsound,"sdb/sound/bk%d",_rand);
	SoundUtil::sharedEngine()->playBackMusic(tempsound,false);
    SoundUtil::sharedEngine()->setBackSoundVolume(g_GlobalUnits.m_fBackMusicValue);
    SoundUtil::sharedEngine()->setSoundVolume(g_GlobalUnits.m_fSoundValue);
}

void SDBGameViewLayer::StopGameMusicBk()
{
	SoundUtil::sharedEngine()->stopAllEffects();
}
