#include "SDBGameLogic.h"
#include "GameSDB_CMD.h"

CGameLogicBase::enumSortMode CGameLogicBase::m_eSortMode = eNormal;
CGameLogicBase::CGameLogicBase(void)
{
	srand(GetTickCountTo());
	m_byCardData.clear();
}

CGameLogicBase::~CGameLogicBase(void)
{
}

unsigned int CGameLogicBase::GetTickCountTo()
{
	struct timeval tv;
	if (gettimeofday(&tv, 0)) {
		return 0;
	}
	return (tv.tv_sec * 1000 + tv.tv_usec / 1000);
}

//洗牌
void CGameLogicBase::RandomCardData()
{
	random_shuffle(m_byCardData.begin(), m_byCardData.end());
}

//获取牌值
BYTE CGameLogicBase::GetCardValue(const BYTE& byCardData)
{
	return (byCardData & 0x0F);
}

//获取牌花色
BYTE CGameLogicBase::GetCardColor(const BYTE& byCardData)
{
	return ( (byCardData & 0xF0) >> 4 );
}

//获取牌花色
int CGameLogicBase::GetCardLogic(const BYTE& byCardData)
{
	BYTE byCardValue = GetCardValue(byCardData);

	//大小王
	if ((byCardData == 0x41) || (byCardData == 0x42))
		return (byCardValue+15);

	return byCardValue;
}

//获取逻辑值
int CGameLogicBase::GetLogicValue(const BYTE& byCardData)
{
	BYTE byCardValue = GetCardValue(byCardData);

	//大小王
	if ((byCardData == 0x41) || (byCardData == 0x42))
		return (byCardValue+15);
	//A、2
	if((byCardValue == 0x01) || (byCardValue == 0x02))
		return (byCardValue+13);

	return byCardValue;
}

//排序函数
bool CGameLogicBase::fncmp(const BYTE& lhs, const BYTE& rhs)
{
	switch (m_eSortMode)
	{
	case eAscending:		{return (lhs<rhs); break;}
	case eAscendingColor:	{return (GetCardColor(lhs)<GetCardColor(rhs)); break;}
	case eAscendingValue:	{return (GetCardValue(lhs)<GetCardValue(rhs)); break;}
	case eAscendingLogic:	{return (GetCardLogic(lhs)<GetCardLogic(rhs)); break;}
	case eAscLogicValue:	{return (GetLogicValue(lhs)<GetLogicValue(rhs)); break;}
	case eDescending:		{return (lhs>rhs); break;}
	case eDescendingColor:	{return (GetCardColor(lhs)>GetCardColor(rhs)); break;}
	case eDescendingValue:	{return (GetCardValue(lhs)>GetCardValue(rhs)); break;}
	case eDescendingLogic:	{return (GetCardLogic(lhs)>GetCardLogic(rhs)); break;}
	case eDesLogicValue:	{return (GetLogicValue(lhs)>GetLogicValue(rhs)); break;}
	default: break;	
	}

	return false;
}

//排序牌
bool CGameLogicBase::SortCardData(enumSortMode eSortMode, const int nStart, const int nEnd)
{
	return SortCardData(eSortMode, m_byCardData, nStart, nEnd);
}

//排序牌
bool CGameLogicBase::SortCardData(enumSortMode eSortMode, vector_carddata& byCardData, const int nStart, const int nEnd)
{
	if ((nStart == -1) && (nEnd == -1))
	{
		m_eSortMode = eSortMode;
		sort(byCardData.begin(), byCardData.end(), fncmp);
		return TRUE;
	}
	else
	{
		if (nStart >= nEnd) return FALSE;
		if (nStart < 0) return FALSE;
		if (nEnd > (int)(byCardData.size())) return FALSE;
	}

	m_eSortMode = eSortMode;
	sort(byCardData.begin()+nStart, byCardData.begin()+nEnd, fncmp);

	return TRUE;
}

//获取牌数据
bool CGameLogicBase::GetCardData(vector_carddata& byCardData, const int nStart, const int nEnd)
{
	return GetCardData(byCardData, m_byCardData, nStart, nEnd);
}

//获取牌数据
bool CGameLogicBase::GetCardData(vector_carddata& vtCardDataDest, const vector_carddata& vtCardDataSrc, const int nStart, const int nEnd)
{
	vtCardDataDest.clear();
	if ((nStart == -1) && (nEnd == -1))
	{
		vtCardDataDest.assign(vtCardDataSrc.begin(), vtCardDataSrc.end());
		return TRUE;
	}
	else
	{
		if (nStart >= nEnd) return FALSE;
		if (nStart < 0) return FALSE;
		if (nEnd > (int)(vtCardDataSrc.size())) return FALSE;
	}

	vtCardDataDest.assign(vtCardDataSrc.begin()+nStart, vtCardDataSrc.begin()+nEnd);

	return TRUE;
}

//设置牌数据
void CGameLogicBase::SetCardData(vector_carddata& byCardData, const BYTE byAddCardData[], const int nAddCardCount)
{
	byCardData.clear();
	AddCardData(byCardData, byAddCardData, nAddCardCount);
}

//设置牌数据
void CGameLogicBase::SetCardData(const BYTE byAddCardData[], const int nAddCardCount)
{
	SetCardData(m_byCardData, byAddCardData, nAddCardCount);
}

//添加牌数据
void CGameLogicBase::AddCardData(const BYTE byAddCardData[], const int nAddCardCount)
{
	AddCardData(m_byCardData, byAddCardData, nAddCardCount);
}

//添加牌数据
void CGameLogicBase::AddCardData(vector_carddata& byCardData, const BYTE byAddCardData[], const int nAddCardCount)
{
	byCardData.insert(byCardData.end(), byAddCardData, byAddCardData+nAddCardCount);
}

//删除牌数据
bool CGameLogicBase::RemoveCardData(const BYTE byRemoveCardData[], const int nRemoveCardCount)
{
	return RemoveCardData(m_byCardData, byRemoveCardData, nRemoveCardCount);
}

//删除牌数据
bool CGameLogicBase::RemoveCardData(vector_carddata& byCardData, const BYTE byRemoveCardData[], const int nRemoveCardCount)
{
	int i = 0;
	vector_carddata::iterator itvector;
	bool bIsValid = TRUE;
	for (i=0; i<nRemoveCardCount; i++)
	{
		itvector = find(byCardData.begin(), byCardData.end(), byRemoveCardData[i]);
		if (itvector == byCardData.end())
		{
			bIsValid = FALSE;
			break;
		}
	}

	if (!bIsValid) return FALSE;

	for (i=0; i<nRemoveCardCount; i++)
	{
		itvector = find(byCardData.begin(), byCardData.end(), byRemoveCardData[i]);
		byCardData.erase(itvector);
	}
	return TRUE;
}

/////////////////////////////////////////////////////////////////////

CGameLogicSDB::CGameLogicSDB(void)
{
	ResetCardData();
}

CGameLogicSDB::~CGameLogicSDB(void)
{
}

//复位牌数据
void CGameLogicSDB::ResetCardData()
{
	//扑克数据
	const BYTE byCardData[]=
	{
		0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08,0x09,0x0A,0x0B,0x0C,0x0D,	//方块 A - K
		0x11,0x12,0x13,0x14,0x15,0x16,0x17,0x18,0x19,0x1A,0x1B,0x1C,0x1D,	//梅花 A - K
		0x21,0x22,0x23,0x24,0x25,0x26,0x27,0x28,0x29,0x2A,0x2B,0x2C,0x2D,	//红桃 A - K
		0x31,0x32,0x33,0x34,0x35,0x36,0x37,0x38,0x39,0x3A,0x3B,0x3C,0x3D,	//黑桃 A - K
//		0x41,0x42															//大、小王
	};
	m_byCardData.clear();
	for (int i=0; i<SDB_GAME_PLAYER/10; i++)
	{
		AddCardData(byCardData, _countof(byCardData));
	}
}

//获取牌值
int CGameLogicSDB::GetCardValueSDB(BYTE byCardData)
{
	int nCardValue = GetCardValue(byCardData);
	if ((nCardValue > 0x0A) || (GetCardColor(byCardData) > 0x03))
	{
		nCardValue = 5;
	}
	else
	{
		nCardValue *= 10;
	}

	return nCardValue;
}

//财神变换
bool CGameLogicSDB::MagicCardData(const BYTE bySrcCardData[], const int nCardCount, BYTE byDestCardData[])
{
	return TRUE;
}

//获取牌型
int CGameLogicSDB::GetCardType(const BYTE byCardData[], const long nCardCount, int* pResult)
{
	int nCardType = 0;
	int i = 0;
	bool bTiger = TRUE;
	int nCardValue = 0;

	int nCardPoint = 0;
	const int nHalfPoint = 5;

	try
	{
		if (nCardCount <= 0)
			throw SDB_CT_ERROR;
		for (i = 0; i < nCardCount; i++)
		{
			if (byCardData[i] == 0x00)
				continue;
			nCardValue = GetCardValue(byCardData[i]);
			if ((nCardValue > 0x0A))
			{
				nCardPoint += nHalfPoint;
			}
			else
			{
				bTiger = false;
				nCardPoint += 10 * nCardValue;
			}
		}

		if (nCardPoint > 105)
			throw SDB_CT_1;
		if ((nCardCount == 5) && (nCardPoint < 105))
		{
			nCardPoint = 0;
			throw SDB_CT_4;
		}
		if (nCardPoint == 105)
			throw SDB_CT_3;
		else
			throw SDB_CT_2;
	}
	catch (enumCardType eCardType)
	{
		nCardType = (int)eCardType;
	}
	catch (...)
	{
		nCardType = (int)SDB_CT_ERROR;
	}

	if (pResult != NULL)
	{
		*((int *)pResult) = nCardPoint;
	}

	return nCardType;
}

//比较大小
int CGameLogicSDB::CompareCardData(const BYTE byFirstCardData[], const int nFirstCardCount,
	const BYTE bySecondCardData[], const int nSecondCardCount)
{
	int nResult = 0;
	int nFirstType = 0;
	int nSecondType = 0;
	int nFirstLogicValue = 0;
	int nSecondLogicValue = 0;

	nFirstType = GetCardType(byFirstCardData, nFirstCardCount, &nFirstLogicValue);
	nSecondType = GetCardType(bySecondCardData, nSecondCardCount, &nSecondLogicValue);

	nResult = nFirstType - nSecondType;
	if ((nResult == 0) && (nFirstType == (int)SDB_CT_2))
	{
		nResult = nFirstLogicValue - nSecondLogicValue;
	}

	return nResult;
}

//比较大小
int CGameLogicSDB::CompareCardData(int nFirstType, int nFirstValue, int nSecondType, int nSecondValue)
{
	int nResult = 0;
	nResult = nFirstType - nSecondType;
	if ((nResult == 0) && (nFirstType == (int)SDB_CT_2))
	{
		nResult = nFirstValue - nSecondValue;
	}

	return nResult;
}

//获取类型描述
bool CGameLogicSDB::GetTypeDescribe(int nCardType, string& strTypeDescribe, int pResult)
{
	strTypeDescribe = "";
	switch (nCardType)
	{
	case SDB_CT_5:
		strTypeDescribe = "五虎将";
		break;
	case SDB_CT_4:
		strTypeDescribe = "五小牌";
		break;
	case SDB_CT_3:
		strTypeDescribe = "十点半";
		break;
	case SDB_CT_2:
		strTypeDescribe = "点牌";
		break;
	case SDB_CT_1:
		strTypeDescribe = "爆点";
		break;
	}

	return TRUE;
}

//获取牌
BYTE CGameLogicSDB::GetCardDataOfIndex(int nIndex)
{
	if (nIndex < 0 || nIndex >= (int)(m_byCardData.size()))
		return 0x00;

	return (m_byCardData.at(nIndex));
}




