#ifndef _SDBGAME_LOGIC_H_
#define _SDBGAME_LOGIC_H_

#include "GameLayer.h"
#include <vector>

#ifndef _countof
#define _countof(array) (sizeof(array)/sizeof(array[0]))
#endif
#include <vector>
#include <algorithm>
using namespace std;

class CGameLogicBase
{
	typedef BYTE (*CallBackCustom)(const BYTE byCardData);
public:
	//排序方式
	enum enumSortMode
	{
		eNormal = 0,		//不排序
		eAscending=1,			//升序
		eAscendingColor=2,	//按花色升序
		eAscendingValue=3,	//按牌值升序
		eAscendingLogic=4,	//按逻辑升序
		eAscLogicValue=5,		//按逻辑值升序
		eDescending=6,		//降序
		eDescendingColor=7,	//按花色降序
		eDescendingValue=8,	//按牌值降序
		eDescendingLogic=9,	//按逻辑降序
		eDesLogicValue=10,		//按逻辑值降序
		eSortModeCount=11
	};
	typedef vector<BYTE> vector_carddata;

private:
	//排序函数
	static bool fncmp(const BYTE& lhs, const BYTE& rhs);
	CallBackCustom m_pfnCustom;

public:
	CGameLogicBase(void);
	virtual ~CGameLogicBase(void);

	//通用函数
public:
	//洗牌
	void RandomCardData();
	//获取牌值
	static BYTE GetCardValue(const BYTE& byCardData);
	//获取牌花色
	static BYTE GetCardColor(const BYTE& byCardData);
	//获取牌花色
	static int GetCardLogic(const BYTE& byCardData);
	//获取逻辑值
	static int	GetLogicValue(const BYTE& byCardData);
	//排序牌数据
	bool SortCardData(enumSortMode eSortMode, const int nStart=-1, const int nEnd=-1);
	bool SortCardData(enumSortMode eSortMode, vector_carddata& byCardData, const int nStart=-1, const int nEnd=-1);
	//获取牌数据
	bool GetCardData(vector_carddata& byCardData, const int nStart=-1, const int nEnd=-1);
	bool GetCardData(vector_carddata& vtCardDataDest, const vector_carddata& vtCardDataSrc, const int nStart=-1, const int nEnd=-1);
	//设置牌数据
	void SetCardData(const BYTE byAddCardData[], const int nAddCardCount);
	void SetCardData(vector_carddata& byCardData, const BYTE byAddCardData[], const int nAddCardCount);
	//添加牌数据
	void AddCardData(const BYTE byAddCardData[], const int nAddCardCount);
	void AddCardData(vector_carddata& byCardData, const BYTE byAddCardData[], const int nAddCardCount);
	//删除牌数据
	bool RemoveCardData(const BYTE byRemoveCardData[], const int nRemoveCardCount);
	bool RemoveCardData(vector_carddata& byCardData, const BYTE byRemoveCardData[], const int nRemoveCardCount);

	unsigned int GetTickCountTo();
	//特有函数
public:
	//复位牌数据
//	virtual void ResetCardData() = NULL;
	//获取最大牌数量
//	virtual int GetMaxCardCount() = NULL;
	//获取最大财神牌数量
//	virtual int GetMaxMagicCardCount() = NULL;
	//财神变换
	//virtual bool MagicCardData(const BYTE bySrcCardData[], const int nCardCount, BYTE byDestCardData[]) = NULL;
	//获取牌型
//	virtual int GetCardType(const BYTE byCardData[], const int nCardCount, int* pResult=0) = NULL;
	//比较大小
	//virtual int CompareCardData(const BYTE byFirstCardData[], const int nFirstCardCount,
	//	const BYTE bySecondCardData[], const int nSecondCardCount) = NULL;//return (first>second);
	//获取类型描述
	//virtual bool GetTypeDescribe(int nCardType, string& strTypeDescribe, int pResult=0) = NULL;

protected:
	static enumSortMode		m_eSortMode;			//牌排序方式
	vector_carddata			m_byCardData;			//牌数组
};


class CGameLogicSDB : public CGameLogicBase
{
public:
	enum enumCardType
	{
		SDB_CT_ERROR = 0,	//错误类型
		SDB_CT_1 = 1,		//爆点
		SDB_CT_2 = 2,		//点牌
		SDB_CT_3 = 3,		//十点半
		SDB_CT_4 = 4,		//五小
		SDB_CT_5 = 5,		//五虎将
	};

	enum enumCardCount
	{
		eMaxCardCount = 5,//牌数量
		eMaxMagicCardCount = 0,//财神数量
	};

public:
	CGameLogicSDB(void);
	virtual ~CGameLogicSDB(void);

	//继承特有函数 多态性
public:
	//复位牌数据
	virtual void ResetCardData();
	//获取最大牌数量
	virtual int GetMaxCardCount(){ return (int)eMaxCardCount; }
	//获取最大财神牌数量
	virtual int GetMaxMagicCardCount(){ return (int)eMaxMagicCardCount; }
	//财神变换
	virtual bool MagicCardData(const BYTE bySrcCardData[], const int nCardCount, BYTE byDestCardData[]);
	//获取牌型
	virtual int GetCardType(const BYTE byCardData[], const long nCardCount, int* pResult=0);
	//比较大小
	virtual int CompareCardData(const BYTE byFirstCardData[], const int nFirstCardCount,
		const BYTE bySecondCardData[], const int nSecondCardCount);//return (first>second);
	//比较大小
	int CompareCardData(int nFirstType, int nFirstValue, int nSecondType, int nSecondValue);
	//获取类型描述
	virtual bool GetTypeDescribe(int nCardType, string& strTypeDescribe, int pResult=0);

public:
	//获取牌
	BYTE GetCardDataOfIndex(int nIndex);
	//获取牌数量
	int GetCardCount(){ return (int)(m_byCardData.size()); }
	//获取牌值
	int GetCardValueSDB(BYTE byCardData);
};


#endif

