#ifndef _SDBGAME_VIEW_LAYER_H_
#define _SDBGAME_VIEW_LAYER_H_

#include "GameScene.h"
#include "FrameGameView.h"
#include "GameSDB_CMD.h"
#include "SDBGameLogic.h"
#include "ui/UIListView.h"

class CTimeTaskLayer;
class SDBGameViewLayer : public IGameView
{
	struct tagApplyUser
	{
		//玩家信息
        int                             userChairID;
		char							strUserName[128];					//玩家帐号
        BYTE                            wGender;
        DWORD                           wUserID;
		LONGLONG						lUserScore;							//玩家金币
	};
public:
	SDBGameViewLayer(const SDBGameViewLayer&);
	SDBGameViewLayer& operator = (const SDBGameViewLayer&);
	SDBGameViewLayer(GameScene *pGameScene);
	static SDBGameViewLayer *create(GameScene *pGameScene);
	virtual ~SDBGameViewLayer();

	virtual bool init(); 
	virtual void onEnter();
	virtual void onExit();

	virtual bool onTouchBegan(Touch *pTouch, Event *pEvent);
	virtual void onTouchMoved(Touch *pTouch, Event *pEvent);
	virtual void onTouchEnded(Touch *pTouch, Event *pEvent);

	virtual bool onTextFieldAttachWithIME(TextFieldTTF*pSender){return false;}
	virtual bool onTextFieldDetachWithIME(TextFieldTTF*pSender){return false;}
	virtual bool onTextFieldInsertText(TextFieldTTF *pSender, const char *text, int nLen){return true;}
	virtual bool onTextFieldDeleteBackward(TextFieldTTF *pSender, const char *delText, int nLen){return true;}

	virtual void keyboardWillShow(IMEKeyboardNotificationInfo &info){}
	virtual void keyboardDidShow(IMEKeyboardNotificationInfo &info){}
	virtual void keyboardWillHide(IMEKeyboardNotificationInfo &info){}
	virtual void keyboardDidHide(IMEKeyboardNotificationInfo &info){}
	virtual void DrawUserScore(){}	//绘制玩家分数
	virtual void GameEnd(){};
	virtual void ShowAddScoreBtn(bool bShow=true){};
	virtual void UpdateDrawUserScore(){};		//更新显示的游戏币数量
	void callbackBt( Ref *pSender );
	virtual void backLoginView(Ref* pSender);
	string  AddCommaToNum(LONG Num);

	//初始化
	void InitGame();
	void AddPlayerInfo();
	void AddButton();
	Menu* CreateButton(std::string szBtName ,const Vec2 &p , int tag);

	// 按钮事件管理
	void DialogConfirm(Ref *pSender);
	void DialogCancel(Ref *pSender);
	

	void CloseBankList();
	void OpenBankList();

	//网络接口
public:
	void OnEventUserLeave( tagUserData * pUserData, WORD wChairID, bool bLookonUser );
	//用户进入
	virtual void OnEventUserEnter(tagUserData * pUserData, WORD wChairID, bool bLookonUser);
	//用户积分
	virtual void OnEventUserScore(tagUserData * pUserData, WORD wChairID, bool bLookonUser);
	//用户状态
	virtual void OnEventUserStatus(tagUserData * pUserData, WORD wChairID, bool bLookonUser);
    //用户换桌
    virtual void OnEventUserChangeTable(tagUserData * pUserData, WORD wChairID, bool bLookonUser){};
	//游戏消息
	virtual bool OnGameMessage(WORD wSubCmdID, const void * pData, WORD wDataSize);
	//场景消息
	virtual bool OnGameSceneMessage(BYTE cbGameStatus, const void * pData, WORD wDataSize);

	void OnQuit();
	void StartTime(int chair, int _type,int _time);
	void StopTime();
	void UpdateTime(float fp);

public:
    void Select100();
    void Select1000();
    void Select1w();
    void Select10w();
    void Select100w();
    void Select500w();
    void Select1000w();
    bool OnSubGameStart(const void * pBuffer, WORD wDataSize);
	bool OnSubAddScore(const void * pBuffer, WORD wDataSize);
	bool OnSubAddScoreFailed(const void * pBuffer, WORD wDataSize);
	bool OnSubScoreFull(const void * pBuffer, WORD wDataSize);
	bool OnSubSendCard(const void * pBuffer, WORD wDataSize);
	bool OnSubBeginGet(const void * pBuffer, WORD wDataSize);			//开始要牌
	bool OnSubGetCard(const void * pBuffer, WORD wDataSize);			//玩家要牌
	bool OnSubStopGet(const void * pBuffer, WORD wDataSize);			//是否停牌
	bool OnSubGameEnd(const void * pBuffer, WORD wDataSize);			//游戏结束
	bool OnSubGameFree(const void * pBuffer, WORD wDataSize);
	bool OnSubApplyBanker(const void * pBuffer, WORD wDataSize);		//申请上庄
	bool OnSubCancelApply(const void * pBuffer, WORD wDataSize);
	bool OnSubChangeBanker(const void * pBuffer, WORD wDataSize);
	bool OnSubGameResult(const void * pBuffer, WORD wDataSize);
	bool OnSubRobBanker( const void * pBuffer, WORD wDataSize );
	bool OnSubCheckState(const void * pBuffer, WORD wDataSize);

	void OnBnGetCard();
	void OnBnStopGet();

	void OnStatusFree(CMD_S_SDB_StatusFree *pStatusFree);
	void OnStatusPlace(CMD_S_SDB_StatusPlace *pStatusPlace);
	void OnStatusBanker(CMD_S_SDB_StatusBanker *pStatusBanker);
	void OnStatusPlayer(CMD_S_SDB_StatusPlayer *pStatusPlayer);
	void OnStatusGameEnd(CMD_S_SDB_StatusGameEnd *pGameEnd);


public:
	void OnAddScore(WORD wChairID, LONGLONG lAddScore);
	void OnAddScoreGameStatus(WORD wChairID, LONGLONG lAddScore);
	void UpdateControl();
	void OnSendCard(WORD wChairID, BYTE byCardData);
	bool IsBankerUser();
	bool IsLookonMode();
	void CardMoveCallback();
	void CardMoveCallback1();
	string GetCardStringName(BYTE card);
	void OnBankerBeginGet();
	void OnPlayerBeginGet();
	void PlaceJetton(LONGLONG lAddScore);
	void UpdataAllScore();
	void UpdataMeJetton();
	void SetWinType(INT nWinType);
	void SetWinScore(INT nWinType, LONGLONG lBankerScore, LONGLONG lPlayerScore,LONGLONG lReturnScore);
    void showEndScore(Ref *pSender);
	void OnStopGet(bool bStopGet);
	void StartCheck();
	void OnTimerCheckState();
	void OnCheckState(CMD_S_SDB_CheckState *pCheckState);
	void StartEffectPlayer();
	void StartEffectBanker();
	void OnApplyBanker(WORD wChairID);
	void DrawBankList();
	void DeleteBankList(tagApplyUser ApplyUser);
	void OnApplyBanker(int wParam);
	void SetBankerInfo(WORD wBanker,LONGLONG lScore);
	void DrawBetInfo();
	void PlayGameMusicBK();
	void StopGameMusicBk();
	void PlaySoundTime(float deltaTime);
    void LayerMoveCallBack();
    
    Menu * getApplyBanker();
protected:
	//对象
	CGameLogicSDB		m_GameLogic;

	//图片
	Sprite*			m_BackSpr;
	Sprite*			m_ClockSpr;
	Sprite*			m_BankListBackSpr;

    ui::ListView*       m_listView;
	
    //按钮
	Menu*				m_btReturnBack;	//返回按钮
	Menu*				m_btSeting;
    Menu*				m_bn100;
	Menu*				m_bn1000;
	Menu*				m_bn1W;
	Menu*				m_bn10W;
	Menu*				m_bn100W;
	Menu*				m_bn500W;
    Menu*				m_bn1000W;
	Menu*				m_bnGetCard;
	Menu*				m_bnStopGet;
	Menu*				m_bnCancelBanker;	//申请下庄
	Menu*				m_btRobBanker;		//抢庄
	Menu*				m_BankBtn;
	Menu*				m_BankUp;
	Menu*				m_BankDown;
	Menu*				m_BtnGetScoreBtn;

	//坐标
	Vec2				m_Head_Pos;
	Vec2				m_Head_NickNamePos;
	Vec2				m_Head_GoldPos;
	Vec2				m_Head_WinScorePos;
	Vec2				m_BankListPos[4];
    Vec2				m_BankNickNamePos;	//庄家信息
    Vec2				m_BankGoldPos;
    Vec2				m_BankZhangJiPos;
    Vec2				m_BankJuShuPos;
    
	//标识
	int					m_Head_Tga;
	int					m_Head_NickName_Tga;
	int					m_Head_Gold_Tga;
	int					m_Head_WinScore_Tga;
	int					m_TimeType_Tga;
	int					m_Jetton_Tga;
	int					m_Send_Bank_CardTga;
	int					m_Send_Bank_CardEndTga;
	int					m_Send_Player_CardTga;
	int					m_Send_Player_CardEndTga;
	int					m_AllScoreTga;
	int					m_MeScore_Tga;
	int					m_EndScoreBack_Tga;
	int					m_GoldAnim[7];
	int					m_CardTypeAnimTga;
	int					m_BankCardTypeAnimTga;
	int					m_BankNickName_Tga;
	int					m_BankGold_Tga;
	int					m_BankZhangJi_Tga;
	int					m_BankJuShu_Tga;
    int                 m_bankBg_Tga;
	int					m_lAreaLimitScoreExTga;
	int					m_BetFullTga;
    int                 m_BtnAnim_Tga;
	//下注信息
    LONGLONG            m_CurSelectGold;
	LONGLONG			m_lApplyBankerCondition;//上庄条件
	LONGLONG			m_lAreaLimitScore;//区域限制
	LONGLONG			m_lUserMaxScore;//玩家下注限制
	LONGLONG			m_lTotalPlaceJetton;//玩家下注数目
	LONGLONG			m_lMePlaceJetton;//自己下注数目
	LONGLONG			m_lAreaLimitScoreEx;//区域限制用于绘制
	LONGLONG			m_lTotalPlaceJettonEx;//玩家下注数目用于绘制
	BYTE				m_cbGameStatus;
	int					m_StartTime;
	bool				m_bScoreFull;
	bool				m_LookMode;
	int					m_wBankerUser;
	vector<BYTE>		m_BankerCardList;	//庄家牌
	vector<BYTE>		m_PlayerCardList;   //玩家牌
	bool				m_bisSendCard;
	bool				m_bisShowButton;
	int					m_BankCardIndex;

	int					m_wBankerTimes;
	LONGLONG			m_lBankerWinScore;
	LONGLONG			m_lMeWinScore;
	BYTE				m_bStopGet;
	int					m_nWantedCardCount;
    int                 m_timeType;
	vector<tagApplyUser>m_BankUserList;
	//int					m_BankUserListSize;
	int					m_BankListBeginIndex;
	bool				m_OpenBankList;
	LONGLONG			m_lBankerScore;
	int					m_BankZhangJI;
	int					m_BankJuShu;
	int					m_nRobBanker;
    bool                m_bCanRobBanker;
	bool				m_IsMeBank;
	bool				m_SoundS;
    bool                m_IsLayerMove;

};

#endif
