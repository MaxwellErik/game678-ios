#ifndef CMD_PROTOCOL_HEAD_FILE
#define CMD_PROTOCOL_HEAD_FILE
#pragma pack(1)
//////////////////////////////////////////////////////////////////////////
//公共宏定义

#define SDB_KIND_ID							240									//游戏 I D
#define SDB_GAME_PLAYER_MAX					300									//游戏最大人数
#define SDB_GAME_NAME						TEXT("多人十点半")                    //游戏名字
#define SDB_GAME_GENRE						(GAME_GENRE_GOLD|GAME_GENRE_MATCH)	//游戏类型
#define SDB_CARD_COUNT						540									//扑克总数
#define SDB_GAME_PLAYER						200									//游戏人数
#define MAX_COUNT                           5									//扑克数目

//组件属性
#define VERSION_SERVER                      PROCESS_VERSION(6,0,3)				//程序版本
#define VERSION_CLIENT                      PROCESS_VERSION(6,0,3)				//程序版本

//游戏状态
#define SDB_GS_PLACE_JETTON				GS_PLAYING								//游戏进行
#define SDB_GS_BANKER_CARD				(GS_PLAYING+1)							//游戏进行
#define SDB_GS_PLAYER_CARD				(GS_PLAYING+2)							//游戏进行
#define SDB_GS_GAME_END					(GS_PLAYING+3)							//游戏进行
#define SDB_GS_SCORE_FULL				(GS_PLAYING+4)							//游戏进行

//////////////////////////////////////////////////////////////////////////
//服务器命令结构

#define SUB_S_SDB_GAME_START				100									//游戏开始
#define SUB_S_SDB_PLACE_JETTON				101									//玩家加注
#define SUB_S_SDB_PLACE_JETTON_FAILED		102									//加注失败
#define SUB_S_SDB_SEND_CARD					103									//发牌消息
#define SUB_S_SDB_BEGIN_GET					104									//开始要牌
#define SUB_S_SDB_GET_CARD					105									//玩家要牌
#define SUB_S_SDB_STOP_GET					106									//停止要牌
#define SUB_S_SDB_GAME_END					107									//游戏结束
#define SUB_S_SDB_GAME_FREE					108									//游戏空闲
#define SUB_S_SDB_APPLY_BANKER				109									//申请上庄
#define SUB_S_SDB_CANCEL_APPLY				110									//取消申请
#define SUB_S_SDB_CHANGE_BANKER				111									//我要下庄
#define SUB_S_SDB_GAME_RESULT				112									//游戏结果
#define SUB_S_SDB_SCORE_FULL				113									//下注分满
#define SUB_S_SDB_ROB_BANKER				114									//申请抢庄
#define SUB_S_SDB_TAOPAO					115
#define SUB_S_SDB_ROBOT_BANKER				116									//机器人上庄通知
#define SUB_S_SDB_ROBOT_GET_MONEY			117									//机器人取钱
#define SUB_S_SDB_ROBOT_GRAB_BANKER			118									//机器人抢庄通知
#define SUB_S_SDB_CHECK_STATE				200									//确认状态
#define SUB_S_SDB_CHECK_PLACE_JETTON		201									//核实下注数目

//下注时间
#define SDB_IDI_FREE					1									//空闲时间
#define SDB_TIME_FREE					5									//空闲时间
//下注时间
#define SDB_IDI_PLACE_JETTON			2									//下注时间
#define SDB_TIME_PLACE_JETTON			20									//下注时间
#define SDB_TIME_SCORE_FULL				3

//庄家要牌时间
#define SDB_IDI_BANKER_CARD				3
#define SDB_TIME_BANKER_CARD			20
//闲家要牌时间
#define SDB_IDI_PLAYER_CARD				4
#define SDB_TIME_PLAYER_CARD			20
//结束时间
#define SDB_IDI_GAME_END				5									//结束时间
#define SDB_TIME_GAME_END				10									//结束时间
//强制结束
#define SDB_IDI_GAME_END_ABNORMAL		6									//结束时间
#define SDB_TIME_GAME_END_ABNORMAL		1									//结束时间

#define SDB_MAX_ROB_BANKER				3									//最大抢庄人

//空闲状态
struct CMD_S_SDB_StatusFree
{
	//全局信息
	BYTE							byTimeLeave;						//剩余时间
	LONGLONG						lApplyBankerCondition;				//申请条件
	//庄家信息
	WORD							wBankerUser;						//当前庄家
	WORD							wBankerTimes;						//庄家局数
	LONGLONG						lBankerWinScore;					//庄家成绩
	int								nRobBanker;							//抢庄人数
	LONGLONG						lAreaLimitScore;					//区域限制
};

//下注状态
struct CMD_S_SDB_StatusPlace
{
	CMD_S_SDB_StatusFree StatusFree;
	LONGLONG lPlaceJetton[SDB_GAME_PLAYER];			//玩家下注
	LONGLONG lTotalJetton;							//下注总数
};

//庄家要牌状态
struct CMD_S_SDB_StatusBanker
{
	CMD_S_SDB_StatusFree StatusFree;
	LONGLONG lPlaceJetton[SDB_GAME_PLAYER];			//玩家下注
	LONGLONG lTotalJetton;							//下注总数
	BYTE byBankerCardData[MAX_COUNT];
	int nBankerCardCount;
	BYTE byPlayerCardData[MAX_COUNT];
	int nPlayerCardCount;
	int bStopGet;
};

//闲家要牌状态
struct CMD_S_SDB_StatusPlayer
{
	CMD_S_SDB_StatusFree StatusFree;
	LONGLONG lPlaceJetton[SDB_GAME_PLAYER];			//玩家下注
	LONGLONG lTotalJetton;							//下注总数
	BYTE byBankerCardData[MAX_COUNT];
	int nBankerCardCount;
	BYTE byPlayerCardData[MAX_COUNT];
	int nPlayerCardCount;
	int bStopGet;
};

//结束状态
struct CMD_S_SDB_StatusGameEnd
{
	CMD_S_SDB_StatusFree StatusFree;
	LONGLONG lPlaceJetton[SDB_GAME_PLAYER];			//玩家下注
	LONGLONG lTotalJetton;							//下注总数
	BYTE byBankerCardData[MAX_COUNT];
	int nBankerCardCount;
	BYTE byPlayerCardData[MAX_COUNT];
	int nPlayerCardCount;
};



//游戏开始
struct CMD_S_SDB_GameStart
{
	//全局信息
	BYTE							byTimeLeave;						//剩余时间
	//下注信息
	LONGLONG						lAreaLimitScore;					//区域限制
	LONGLONG						lUserMaxScore;						//分数上限

	//用户信息
	WORD							wBankerUser;						//庄家用户
};

//用户下注
struct CMD_S_SDB_PlaceJetton
{
	WORD							wChairID;						//当前用户
	LONGLONG						lJettonScore;						//当前倍数
	LONGLONG						lMaxScore;						//最大筹码
};

//下注分满
struct CMD_S_SDB_ScoreFull
{
	BYTE							byTimeLeave;
};

//用户下注
struct CMD_S_SDB_AddScoreFailed
{
	WORD							wAddUser;						//当前用户
	LONGLONG						lAddScore;						//当前倍数
};

//系统发牌
struct CMD_S_SDB_SendCard
{
	WORD							wCurrentUser;					//要牌用户
	BYTE							byCardData;						//牌数据
};

//开始要牌
struct CMD_S_SDB_BeginGet
{
	BYTE							byTimeLeave;					//剩余时间
	int                             bBankerUser;					//是否为庄家
};

//玩家要牌
struct CMD_S_SDB_GetCard
{
	WORD							wCurrentUser;					//要牌用户
	BYTE							byCardData;						//牌数据
};

//停止要牌
struct CMD_S_SDB_StopGet
{
	int                             bStopGet;						//是否还能要牌
	BYTE							bSendCard;						//是否是要牌阶段
};

//游戏结束
struct CMD_S_SDB_GameEnd
{
	BYTE							byTimeLeave;					//剩余时间
	int								nWinType;
	LONGLONG						lBankerWinScore;				//庄家得分
	LONGLONG						lPlayerWinScore;				//玩家得分
	LONGLONG						lBankerRevenue;					//游戏税收
	LONGLONG						lPlayerRevenue;					//游戏税收
	BYTE							byBankerCardData[MAX_COUNT];	//比牌用户
	int							nBankerCardCount;				//庄家牌数量
	LONGLONG						iBankerTotallScore;					//庄家累计输赢分数
	int								nBankerTime;						//做庄次数
};

//游戏空闲
struct CMD_S_SDB_GameFree
{
	BYTE							byTimeLeave;					//剩余时间
	WORD							wBankerTimes;//坐庄次数
	LONGLONG						lBankerWinScore;	//庄家成绩
};

//申请上庄
struct CMD_S_SDB_ApplyBanker
{
	WORD							wApplyUser;			//申请玩家
	int								nRobBanker;			//抢庄的人数
};

//取消申请
struct CMD_S_SDB_CancelApply
{
	WORD							wApplyUser;			//申请玩家
	int								nRobBanker;			//抢庄的人数
};

//我要下庄
struct CMD_S_SDB_ChangeBanker
{
	WORD							wBankerUser;			//庄家
	int								nRobBanker;			//抢庄的人数
};
//申请抢庄
struct CMD_S_SDB_RobBanker
{
	WORD							wApplyUser;			//申请抢庄
	int								nRobBanker;			//抢庄的人数
};
//玩家结果
struct tagGameResult
{
	WORD wChairID;
	BYTE byCardType;
	BYTE byCardPoint;
	LONGLONG lWinScore;
};
struct CMD_S_RobotGetMoney
{
	LONGLONG							lMoney;								//金额
};
//确认状态
struct CMD_S_SDB_CheckState
{
	LONGLONG						lPlaceJetton;						//玩家下注
	BYTE							cbStopGet;							//是否已停止要牌
	BYTE							cbCardCount;						//玩家牌数
	BYTE							cbCardData[MAX_COUNT];				//玩家牌信息
};
//////////////////////////////////////////////////////////////////////////

//客户端命令结构
#define SUB_C_SDB_PLACE_JETTON				1									//用户加注
#define SUB_C_SDB_GET_CARD					2									//玩家要牌
#define SUB_C_SDB_STOP_GET					3									//停止要牌
#define SUB_C_SDB_APPLY_BANKER				4									//申请上庄
#define SUB_C_SDB_CANCEL_APPLY				5									//取消申请
#define SUB_C_SDB_CANCEL_BANKER				6									//我要下庄
#define SUB_C_SDB_MANAGE_CONTROL			7									//输赢控制
#define SUB_C_SDB_ROB_BANKER				8									//抢庄
#define SUB_C_SDB_CHECK_STATE				9									//确认状态
//用户加注
struct CMD_C_SDB_PlaceJetton
{
	LONGLONG							lJettonScore;								//加注数目
};

//输赢控制
struct CMD_C_SDB_ManageControl
{
	int bWin;
	WORD wChairID;
	WORD wTableID;
};
//确认状态
struct CMD_C_SDB_CheckState
{
	BYTE							cbClientState;							//客户端状态
	BYTE							cbStopGet;								//是否已停牌
	BYTE							cbWantedCardCount;						//应该几张牌
};
//客户端状态
enum EnumClientState
{
	ecsNormal = 0,															//正常
	ecsButtonAbnormal = 1,													//按钮显示异常
	ecsCardAbnormal = 2,													//牌显示异常
	ecsButtonCardAbnormal = 3												//按钮、牌显示异常
};
#pragma pack()
//////////////////////////////////////////////////////////////////////////
#endif
