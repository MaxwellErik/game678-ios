﻿#include "GameLayer.h"

#include "GameScene.h"

#include "SimpleAudioEngine.h"

GameLayer::GameLayer(GameScene *pGameScene)
{
	this->m_pGameScene = pGameScene;

	this->m_pGameScene->addChild(this);
}

GameLayer::~GameLayer()
{
	
}

void GameLayer::onEnter()
{
	#ifdef DEBUG

		CCTextureCache::sharedTextureCache()->dumpCachedTextureInfo();
		
	#endif

	Layer::onEnter();
}

void GameLayer::onExit()
{
	// 停止 所有 触发
	this->SetAllTouchEnabled(false);
	this->SetAllKeypadDisabled();

	// 释放 所有执行绪
	this->unscheduleAllCallbacks();
	//GCWebClient::shareWebClient()->clearAll();
	Layer::onExit();
}

// default implements are used to call script callback if exist
bool GameLayer::onTouchBegan(Touch *pTouch, Event *pEvent)
{
	return Layer::onTouchBegan(pTouch, pEvent);
}

void GameLayer::onTouchMoved(Touch *pTouch, Event *pEvent)
{
	Layer::onTouchMoved(pTouch, pEvent);
}

void GameLayer::onTouchEnded(Touch *pTouch, Event *pEvent)
{
	Layer::onTouchEnded(pTouch, pEvent);
}

void GameLayer::onTouchCancelled(Touch *pTouch, Event *pEvent)
{
	Layer::onTouchCancelled(pTouch, pEvent);
}

// default implements are used to call script callback if exist
void GameLayer::onTouchesBegan(__Set *pTouches, Event *pEvent)
{
	Layer::ccTouchesBegan(pTouches, pEvent);
}

void GameLayer::onTouchesMoved(__Set *pTouches, Event *pEvent)
{
	Layer::ccTouchesMoved(pTouches, pEvent);
}

void GameLayer::onTouchesEnded(__Set *pTouches, Event *pEvent)
{
	Layer::ccTouchesEnded(pTouches, pEvent);
}

void GameLayer::onTouchesCancelled(__Set *pTouches, Event *pEvent)
{
	Layer::ccTouchesCancelled(pTouches, pEvent);
}

void GameLayer::SetMusicVolume(float fv)
{
	CocosDenshion::SimpleAudioEngine::getInstance()->setBackgroundMusicVolume(fv);
	CocosDenshion::SimpleAudioEngine::getInstance()->setEffectsVolume(fv);
}

// 播放 背景 音效
void GameLayer::PlayBackMusic(const char *pMusicName)
{
	CocosDenshion::SimpleAudioEngine::getInstance()->playBackgroundMusic(pMusicName);
}

void GameLayer::PlayBackMusic(const char *pMusicName, bool bloop)
{
	CocosDenshion::SimpleAudioEngine::getInstance()->playBackgroundMusic(pMusicName, bloop);
}


void GameLayer::StopBackMusic(void)
{
	CocosDenshion::SimpleAudioEngine::getInstance()->stopBackgroundMusic();
}

// 播放 背景 音效
void GameLayer::PlayEffect(const char *pSoundName)
{
	CocosDenshion::SimpleAudioEngine::getInstance()->playEffect(pSoundName);
}

// 切换 主Layer
void GameLayer::ReplaceMainGameLayer(GameLayer *pNewMainLayer)
{
	// 更换 Game Scene
	Director::getInstance()->replaceScene(pNewMainLayer->m_pGameScene);
}

// 清除 Alert Message Layer
void GameLayer::RemoveAlertMessageLayer(void)
{
	this->m_pGameScene->RemoveAlertMessageLayer();
}

//void GameLayer::didAccelerate(CCAcceleration*pAccelerationValue)
//{
//	CCLayer::didAccelerate(pAccelerationValue);
//}

/**If isTouchEnabled, this method is called onEnter. Override it to change the
way CCLayer receives touch events.
( Default: CCTouchDispatcher::sharedDispatcher()->addStandardDelegate(this,0); )
Example:
void CCLayer::registerWithTouchDispatcher()
{
CCTouchDispatcher::sharedDispatcher()->addTargetedDelegate(this,INT_MIN+1,true);
}
@since v0.8.0
*/


/**Register script touch events handler */
void GameLayer::registerScriptTouchHandler(int nHandler, bool bIsMultiTouches, int nPriority, bool bSwallowsTouches)
{
	registerScriptTouchHandler(nHandler, bIsMultiTouches, nPriority, bSwallowsTouches);
}

/**Unregister script touch events handler */
void GameLayer::unregisterScriptTouchHandler(void)
{
	unregisterScriptTouchHandler();
}

/**whether or not it will receive Touch events.
You can enable / disable touch events with this property.
Only the touches of this node will be affected. This "method" is not propagated to it's children.
@since v0.8.1
*/
bool GameLayer::isTouchEnabled()
{
	return Layer::isTouchEnabled();
}

void GameLayer::setTouchEnabled(bool value)
{
	Layer::setTouchEnabled(value);
}
    
void GameLayer::setTouchMode(Touch::DispatchMode mode)
{
	Layer::setTouchMode(mode);
}

Touch::DispatchMode GameLayer::getTouchMode()
{
	return Layer::getTouchMode();
}
    
/**priority of the touch events. Default is 0 */
void GameLayer::setTouchPriority(int priority)
{
//	Layer::setTouchPriority(priority);
}

int GameLayer::getTouchPriority()
{
	return 0;
//	return Layer::getTouchPriority();
}

void GameLayer::SetAllTouchEnabled(bool IsTouch)
{
	this->setTouchEnabled(IsTouch);
    
    Vector<Node*> children = this->getChildren();
	if(!children.empty() && (children.size() > 0))
    {
        Ref* pObject = NULL;
//        CCARRAY_FOREACH(this->m_pChildren, pObject)
        for (Vector<Node*>::iterator it = children.begin(); it != children.end(); it++)
        {
            pObject = *it;
			if(pObject != NULL)
			{
				Layer* pLayer = dynamic_cast<Layer*>(pObject);
				if (pLayer != NULL)
				{
					pLayer->setTouchEnabled(IsTouch);
				}
			}
        }
    }
}

void GameLayer::SetAllKeypadDisabled(void)
{
	this->setKeypadEnabled(false);
    Vector<Node*> children = this->getChildren();
    if(!children.empty() && (children.size() > 0))
    {
        Ref* pObject = NULL;
//        CCARRAY_FOREACH(this->m_pChildren, pObject)
        for (Vector<Node*>::iterator it = children.begin(); it != children.end(); it++)
        {
            pObject = *it;
            if(pObject != NULL)
            {
                Layer* pLayer = dynamic_cast<Layer*>(pObject);
                if (pLayer != NULL)
				{
					pLayer->setKeypadEnabled(false);
				}
			}
        }
    }
}

void GameLayer::removeAllChildByTag( int tag )
{
	Node *pNode=NULL;
	do 
	{
		pNode = getChildByTag(tag);
		if(pNode != NULL)
		{
			removeChildByTag(tag, true);
		}
		pNode = getChildByTag(tag);
	} while (pNode != NULL);
}

// Alert Message 确认消息处理
void GameLayer::DialogConfirm(Ref *pSender)
{
	this->RemoveAlertMessageLayer();
}

// Alert Message 取消消息处理
void GameLayer::DialogCancel(Ref *pSender)
{
	this->RemoveAlertMessageLayer();
}
