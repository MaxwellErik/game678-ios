#ifndef _GameBANKPASS_H_
#define _GameBANKPASS_H_

#include "GameScene.h"
#include "cocos-ext.h"

//银行
class GameBankPass : public GameLayer
{
public:
	virtual bool init();  
	static GameBankPass *create(GameScene *pGameScene);
	virtual ~GameBankPass();
	GameBankPass(GameScene *pGameScene);
	GameBankPass(const GameBankPass&);
	GameBankPass& operator = (const GameBankPass&);
	enum Tag
	{
		_BtnCancel,
		_BtnOk,
	};

public:
    cocos2d::ui::EditBox*	m_Pass;

public:
	virtual void onEnter();
	virtual void onExit();
	virtual bool onTouchBegan(Touch *pTouch, Event *pEvent){return true;}
	virtual void onTouchEnded(Touch *pTouch, Event *pEvent){}
	virtual void onTouchMoved(Touch *pTouch, Event *pEvent){}

	virtual void callbackBt(Ref *pSender);

	Menu* CreateButton( std::string szBtName ,const Vec2 &p , int tag );
	void close();
	void onRemove();
};
#endif