﻿#pragma once

#ifndef _H_CCGAMESCROLLVIEW_H_
#define _H_CCGAMESCROLLVIEW_H_

#include "cocos2d.h"
#include "cocos-ext.h"
USING_NS_CC;
USING_NS_CC_EXT;

// 校正滑动动画速度
#define ADJUST_ANIM_VELOCITY 2000

class CCCGameScrollViewDelegate
	: public cocos2d::extension::ScrollViewDelegate
{
public:
	virtual bool scrollViewInitPage(cocos2d::Node *pScroll, cocos2d::Node *pPage, int nPage) = 0;
	virtual void scrollViewClick(const Vec2 &oOffset, const Vec2 &oPoint , Node *pPage, int nPage ) = 0;
	virtual void scrollViewScrollEnd(Node *pPage, int nPage) = 0;
};

class CCCGameScrollView
	: public cocos2d::extension::ScrollView
{
public:
	CCCGameScrollView();
	~CCCGameScrollView();
public:
	CREATE_FUNC(CCCGameScrollView);

	bool init();

	bool createContainer(CCCGameScrollViewDelegate *pDele, int nCount, const cocos2d::Size &oSize );

	virtual bool onTouchBegan( cocos2d::Touch *pTouch, cocos2d::Event *pEvent );

	virtual void onTouchMoved( cocos2d::Touch *pTouch, cocos2d::Event *pEvent );

	virtual void onTouchEnded( cocos2d::Touch *pTouch, cocos2d::Event *pEvent );

	virtual void onTouchCancelled( cocos2d::Touch *pTouch, cocos2d::Event *pEvent );

	virtual void setDirection(Direction eDirection);

	void setCurPage(int nPage);
	void scrollToPage(int nPage);
	void scrollToNextPage();
	void scrollToPrePage();
	int getPageCount();
	int getCurPage();
	Node *getPage(int nPage);
protected:
	void adjustScrollView(const cocos2d::Vec2 &oBegin, const cocos2d::Vec2 &oEnd);

	virtual void onScrollEnd(float fDelay);
protected:
	int m_nPageCount;
	int m_nPrePage;
	cocos2d::Vec2 m_BeginOffset;
	cocos2d::Size m_CellSize;
	float m_fAdjustSpeed;
	bool m_bSetDirection;
	int m_nCurPage;
};

#endif

