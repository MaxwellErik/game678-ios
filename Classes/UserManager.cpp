﻿#include "UserManager.h"


CUserManager::CUserManager(void)
{
}


CUserManager::~CUserManager(void)
{
}

tagUserData *CUserManager::AddUserItem( tagUserData & UserData )
{
	tagUserData *pUserData = NULL;

	//容器中有的话直接返回
	for (int i=0; i<(int)m_vecUserData.size(); i++)
	{
		if (m_vecUserData[i]->dwUserID == UserData.dwUserID)
		{
			return m_vecUserData[i];
		}

	}
	//没有的话添加进容器
	pUserData = new tagUserData;
	CopyMemory(pUserData, &UserData, sizeof(tagUserData));
	m_vecUserData.push_back(pUserData);
	return pUserData;
}

bool CUserManager::DeleteUserItem( tagUserData *pUserData )
{
	for (int i=0; i<(int)m_vecUserData.size(); i++)
	{
		if (m_vecUserData[i]->dwUserID == pUserData->dwUserID)
		{
			delete m_vecUserData[i];
			m_vecUserData[i] = NULL;
			m_vecUserData.erase(m_vecUserData.begin() + i);
			return true;
		}
	}
	return false;
}

bool CUserManager::DeleteUserItem( DWORD dwUserID )
{
	for (int i=0; i<(int)m_vecUserData.size(); i++)
	{
		if (m_vecUserData[i]->dwUserID == dwUserID)
		{
			delete m_vecUserData[i];
			m_vecUserData[i] = NULL;
			m_vecUserData.erase(m_vecUserData.begin() + i);
			return true;
		}
	}
	return false;
}

bool CUserManager::UpdateUserItemScore( tagUserData *pUserData, const tagUserScore * pUserScore )
{
	//效验参数
	CC_ASSERT(pUserScore!=NULL);

	//设置变量
	pUserData->lScore=pUserScore->lScore;
	//pUserData->lSilverScore=pUserScore->lSilverScore;
	//pUserData->lWinCount=pUserScore->lWinCount;
	//pUserData->lLostCount=pUserScore->lLostCount;
	//pUserData->lDrawCount=pUserScore->lDrawCount;
	//pUserData->lFleeCount=pUserScore->lFleeCount;
	//pUserData->lExperience=pUserScore->lExperience;
	//pUserData->lHappyBear = pUserScore->lHappyBear;
	//pUserData->lBodySilverScore = pUserScore->lBodySilverScore;
	//pUserData->lBodyChip = pUserScore->lBodyChip;
	//pUserData->lBodyScore = pUserScore->lBodyScore;
	////游戏输分
	//pUserData->lLostMoney=pUserScore->lLostMoney;

	return true;
}


bool CUserManager::UpdateUserItemStatus( tagUserData * pUserData, const tagUserStatus * pUserStatus )
{
	CC_ASSERT(pUserStatus!=NULL);
	//设置变量
	pUserData->wTableID=pUserStatus->wTableID;
	pUserData->wChairID=pUserStatus->wChairID;
	pUserData->cbUserStatus=pUserStatus->cbUserStatus;
	return true;
}


tagUserData * CUserManager::EnumUserItem( WORD wEnumIndex )
{
	CC_ASSERT(m_vecUserData.size() > wEnumIndex);
	if (m_vecUserData.size() <= wEnumIndex)
	{
		return NULL;
	}
	return m_vecUserData[wEnumIndex];
}

tagUserData * CUserManager::SearchUserByUserID( DWORD dwUserID )
{
	for (int i=0; i<(int)m_vecUserData.size(); i++)
	{
		if (m_vecUserData[i]->dwUserID == dwUserID)
		{
			return m_vecUserData[i];
		}
	}
	return NULL;
}

bool CUserManager::DeleteAllUser()
{
	for (int i=0; i<(int)m_vecUserData.size(); i++)
	{
		delete m_vecUserData[i];
		m_vecUserData[i] = NULL;
	}
	m_vecUserData.clear();
	return true;
}

WORD CUserManager::GetTableCount( WORD wTableID )
{
	WORD wTableCount = 0;
	for (int i=0; i<(int)m_vecUserData.size(); i++)
	{
		if (m_vecUserData[i]->wTableID == wTableID)
		{
			wTableCount++;
		}
	}
	return wTableCount;
}

tagUserData * CUserManager::GetUserData( WORD wTableID, WORD wChairID )
{
	for (int i=0; i<(int)m_vecUserData.size(); i++)
	{
		if (m_vecUserData[i]->wTableID == wTableID && m_vecUserData[i]->wChairID == wChairID)
		{
			return m_vecUserData[i];
		}
	}
	return NULL;
}
