#include "GameBank.h"
#include "LobbySocketSink.h"
#include "Encrypt.h"
#include "GameBankSuc.h"
#include "SoundUtil.h"
#include "Convert.h"

#define Bank_GameBack	1999
#define Bank_GameOx2	2000
#define Bank_GameOx6	2001
#define Bank_GameSH		2002
#define Bank_GameBRNN	2003
#define Bank_GameDDZ	2004
#define Bank_GameWZLZ	2005
#define Bank_Game30S	2006
#define Bank_GameWKNH	2007
#define Bank_GameHHSW	2008
#define Bank_GameSDB	2009
#define Bank_GameQBSQ	2010
#define Bank_GameWZMJ	2011
#define Bank_GameWZQ	2012
#define Bank_GameDNTG	2013
#define Bank_GameJCBY	2014
#define Bank_GameOx4	2015

#define Tag_FlashMoney	2016


GameBank::GameBank( GameScene *pGameScene ):GameLayer(pGameScene)
{
    m_CurIndex = 0;
    m_Score = 0;
    m_BankScore = 0;
    m_tableView = NULL;
    m_passBgmenu = nullptr;
    m_pass = nullptr;
    m_passBg = nullptr;
    ZeroMemory(m_GameKindName,128);
    sprintf(m_GameKindName,"");
    m_passStr = "";
    m_GameKindID = 0;
    m_BtnClickIndex = 0;
    m_bStartFlash = false;
}
GameBank::~GameBank()
{
    
}

void GameBank::SetGameKindName(char* kindname, int kindid)
{
    int btn = GetGameKindBtnTga(kindid);
    Node* temp = m_saveLayer->getChildByTag(btn);
    if (temp != NULL)
    {
        m_saveLayer->removeChildByTag(Bank_GameBack);
        Sprite* tempx = Sprite::createWithSpriteFrameName("bank_game_select.png");
        Vec2 posc = temp->getPosition();
        tempx->setPosition(Vec2(posc.x,posc.y+20));
        tempx->setTag(Bank_GameBack);
        m_saveLayer->addChild(tempx,1001);
    }
    m_GameKindID = kindid;
}

GameBank* GameBank::create(GameScene *pGameScene)
{
    GameBank* temp = new GameBank(pGameScene);
    if(temp && temp->init())
    {
        temp->autorelease();
        return temp;
    }
    else
    {
        CC_SAFE_DELETE(temp);
        return NULL;
    }
}


void GameBank::reloadData()
{
    if (m_tableView != NULL)
    {
        m_tableView->reloadData();
    }
}

cocos2d::Size GameBank::tableCellSizeForIndex( cocos2d::extension::TableView *table, ssize_t idx )
{
    return Size(1920,100);
}

ssize_t GameBank::numberOfCellsInTableView(TableView *table)
{
    return g_GlobalUnits.m_SendRecordVec.size();
}

void GameBank::initCell(TableViewCell* cell , ssize_t iCellIndex )
{
    if (iCellIndex>=(int)g_GlobalUnits.m_SendRecordVec.size()) return;
    tagMBLogTransfer *pRecord = &g_GlobalUnits.m_SendRecordVec[iCellIndex];
    
    Label* timeLabel = Label::createWithSystemFont(pRecord->szCollectDate, _GAME_FONT_NAME_1_, 46);
    timeLabel->setAnchorPoint(Vec2(0, 0.5f));
    timeLabel->setPosition(Vec2(120, 50));
    cell->addChild(timeLabel);
    
    string operTypeStr = StringUtils::toString(pRecord->dwTargetGameID);
    string operName = pRecord->szTargetAccounts;
    string goldstr = StringUtils::toString(pRecord->llSwapScore);
    
    Label* oprLabel = Label::createWithSystemFont(operTypeStr, _GAME_FONT_NAME_1_, 46);
    oprLabel->setPosition(Vec2(1160, 50));
    cell->addChild(oprLabel);
    
    Label* goldLabel = Label::createWithSystemFont(goldstr, _GAME_FONT_NAME_1_, 46);
    goldLabel->setPosition(Vec2(1600, 50));
    cell->addChild(goldLabel);
    
    Label* nameLabel = Label::createWithSystemFont(gbk_utf8(operName.c_str()), _GAME_FONT_NAME_1_, 46);
    nameLabel->setPosition(Vec2(760, 50));
    cell->addChild(nameLabel);
}

void GameBank::initSaveLayer()
{
    
    m_saveLayer = Layer::create();
    addChild(m_saveLayer);
    
    Label *label1 = Label::createWithSystemFont("保险柜酒吧豆:",_GAME_FONT_NAME_1_,46);
    label1->setColor(_GAME_FONT_COLOR_1_);
    label1->setAnchorPoint(Vec2(1,0.5f));
    label1->setPosition(Vec2(355,795));
    m_saveLayer->addChild(label1);
    
    Sprite * bg1 = Sprite::createWithSpriteFrameName("bg_shuru.png");
    bg1->setAnchorPoint(Vec2(0,0.5f));
    bg1->setPosition(Vec2(380,800));
    m_saveLayer->addChild(bg1);
    
    char strcc[32]="";
    LONGLONG lMon = g_GlobalUnits.GetGolbalUserData().lInsureScore;
    memset(strcc , 0 , sizeof(strcc));
    Tools::AddComma(lMon , strcc);
    Label *font1=Label::createWithSystemFont(strcc,_GAME_FONT_NAME_1_,46);
    font1->setTag(Tag_Save_BankScore);
    font1->setAnchorPoint(Vec2(0,0.5f));
    font1->setPosition(Vec2(400,795));
    m_saveLayer->addChild(font1);
    
    Label *label2 = Label::createWithSystemFont("游戏酒吧豆:",_GAME_FONT_NAME_1_,46);
    label2->setColor(_GAME_FONT_COLOR_1_);
    label2->setAnchorPoint(Vec2(1,0.5f));
    label2->setPosition(Vec2(355,675));
    m_saveLayer->addChild(label2);
    
    Sprite * bg2 = Sprite::createWithSpriteFrameName("bg_shuru.png");
    bg2->setAnchorPoint(Vec2(0, 0.5f));
    bg2->setPosition(Vec2(380,680));
    m_saveLayer->addChild(bg2);
    
    char strc[32]="";
    lMon = g_GlobalUnits.GetGolbalUserData().lScore;
    memset(strc , 0 , sizeof(strc));
    Tools::AddComma(lMon , strc);
    Label *font=Label::createWithSystemFont(strc,_GAME_FONT_NAME_1_,46);
    font->setTag(Tag_Save_Score);
    font->setAnchorPoint(Vec2(0,0.5f));
    font->setPosition(Vec2(400,675));
    m_saveLayer->addChild(font);
    
    Label *label3 = Label::createWithSystemFont("存取数额:",_GAME_FONT_NAME_1_,46);
    label3->setColor(_GAME_FONT_COLOR_1_);
    label3->setAnchorPoint(Vec2(1,0.5f));
    label3->setPosition(Vec2(355,555));
    m_saveLayer->addChild(label3);
    
    Sprite * bg3 = Sprite::createWithSpriteFrameName("bg_shuru.png");
    bg3->setAnchorPoint(Vec2(0, 0.5f));
    bg3->setPosition(Vec2(380,560));
    m_saveLayer->addChild(bg3);
    
    const Size inputSize = Size(440,100);
    std::string strTrade = "\u8bf7\u8f93\u5165\u6570\u989d\uff1a";
    Tools::StringTransform(strTrade);
    
    m_Ed_Save_Trade = cocos2d::ui::EditBox::create(inputSize, INPUT_BACKGROUND);
    m_Ed_Save_Trade->setFontName(_GAME_FONT_NAME_1_);
    m_Ed_Save_Trade->setFontSize(46);
    m_Ed_Save_Trade->setPlaceholderFontName(_GAME_FONT_NAME_1_);
    m_Ed_Save_Trade->setPlaceHolder(strTrade.c_str());
    m_Ed_Save_Trade->setPlaceholderFontSize(46);
    m_Ed_Save_Trade->setAnchorPoint(Vec2(0, 0.5f));
    m_Ed_Save_Trade->setPosition(Vec2(400,560));
    m_Ed_Save_Trade->setMaxLength(10);
    m_Ed_Save_Trade->setInputMode(cocos2d::ui::EditBox::InputMode::NUMERIC);
    m_Ed_Save_Trade->setTag(Tag_Save_EdScore);
    m_saveLayer->addChild(m_Ed_Save_Trade);
    
    const char* names[] = {"一百万", "五百万", "一千万","全部取出", "五千万", "一亿", "清零", "全部存入"};
    for (int i = w100; i <= wAllSave; i ++)
    {
        float posx = 200 + (i % 4) * 200;
        float posy = 410 - (i / 4) * 130;
        Menu* pNode = CreateButton("doushu", Vec2(posx, posy), i);
        m_saveLayer->addChild(pNode ,100 , w100);
        
        Label* l1 = Label::createWithSystemFont(names[i], _GAME_FONT_NAME_1_, 44);
        l1->setPosition(posx, posy);
        m_saveLayer->addChild(l1, 101);
    }
    
    auto GetMoneyBtn = CreateButton("GetMoney",Vec2(230,120),Tag_Get_BtnGet);
    m_saveLayer->addChild(GetMoneyBtn,100,Tag_Get_BtnGet);
    
    auto SaveMoneyBtn = CreateButton("SaveMoney",Vec2(500,120),Tag_Save_BtnSave);
    m_saveLayer->addChild(SaveMoneyBtn,100,Tag_Save_BtnSave);
    
    auto FlashMoneyBtn = CreateButton("flashMoney",Vec2(770,120),Tag_FlashMoney);
    m_saveLayer->addChild(FlashMoneyBtn,100,Tag_FlashMoney);
    
    Label *title = Label::createWithSystemFont("请选择操作的游戏", _GAME_FONT_NAME_1_, 50);
    title->setTag(Tag_Save_MoneyOp);
    title->setPosition(Vec2(1400, 850));
    m_saveLayer->addChild(title);
    
    int num = 0;
    int cel = 0;
    for (int i = 0; i< g_GlobalUnits.m_ServerListManager.m_vcKind.size(); i++)
    {
        tagGameKind  *pRecord = &g_GlobalUnits.m_ServerListManager.m_vcKind[i];
        Vec2 pos = Vec2(1050+num * 180, 700-cel*240);
        bool isok = AddGameKind(pRecord->wKindID, pos);
        if (isok)
        {
            num++;
            if (num % 5 == 0)
            {
                cel++;
                num = 0;
            }
        }
    }
    Sprite* tempx = Sprite::createWithSpriteFrameName("bank_game_select.png");
    Node * node = m_saveLayer->getChildByTag(Bank_GameWZMJ);
    tempx->setPosition(node->getPosition() + Vec2(0,20));
    tempx->setTag(Bank_GameBack);
    m_saveLayer->addChild(tempx,1001);
    m_GameKindID = Dating_GameKind_WZMJ;
}

void GameBank::initSendLayer()
{
    m_sendLayer = Layer::create();
    addChild(m_sendLayer);
    
    Label *label1 = Label::createWithSystemFont("保险柜酒吧豆:",_GAME_FONT_NAME_1_,46);
    label1->setColor(_GAME_FONT_COLOR_1_);
    label1->setAnchorPoint(Vec2(1,0.5f));
    label1->setPosition(Vec2(700,795));
    m_sendLayer->addChild(label1);
    
    Sprite * bg1 = Sprite::createWithSpriteFrameName("bg_shuru.png");
    bg1->setAnchorPoint(Vec2(0, 0.5f));
    bg1->setPosition(Vec2(720,800));
    m_sendLayer->addChild(bg1);
    
    char _gold[32];
    memset(_gold , 0 , sizeof(_gold));
    Tools::AddComma(g_GlobalUnits.GetGolbalUserData().lInsureScore , _gold);
    Label *bankgold=Label::createWithSystemFont(_gold,_GAME_FONT_NAME_1_,46);
    bankgold->setTag(Tag_Send_BankGold);
    bankgold->setAnchorPoint(Vec2(0,0.5f));
    bankgold->setPosition(Vec2(740,795));
    m_sendLayer->addChild(bankgold);
    
    Label *label2 = Label::createWithSystemFont("对方游戏ID:",_GAME_FONT_NAME_1_,46);
    label2->setColor(_GAME_FONT_COLOR_1_);
    label2->setAnchorPoint(Vec2(1,0.5f));
    label2->setPosition(Vec2(700,675));
    m_sendLayer->addChild(label2);
    
    Sprite * bg2 = Sprite::createWithSpriteFrameName("bg_shuru.png");
    bg2->setAnchorPoint(Vec2(0, 0.5f));
    bg2->setPosition(Vec2(720,680));
    m_sendLayer->addChild(bg2);
    
    std::string strTrade = "\u8bf7\u8f93\u5165\u6e38\u620f\u0049\u0044\uff1a";
    Tools::StringTransform(strTrade);
    
    const Size inputSize = Size(440,100);
    m_Ed_Send_UserID = cocos2d::ui::EditBox::create(inputSize, INPUT_BACKGROUND);
    m_Ed_Send_UserID->setFontName(_GAME_FONT_NAME_1_);
    m_Ed_Send_UserID->setFontSize(46);
    m_Ed_Send_UserID->setPlaceholderFontName(_GAME_FONT_NAME_1_);
    m_Ed_Send_UserID->setPlaceHolder(strTrade.c_str());
    m_Ed_Send_UserID->setPlaceholderFontSize(46);
    m_Ed_Send_UserID->setMaxLength(10);
    m_Ed_Send_UserID->setInputMode(cocos2d::ui::EditBox::InputMode::NUMERIC);
    m_Ed_Send_UserID->setPosition(Vec2(740,675));
    m_Ed_Send_UserID->setAnchorPoint(Vec2(0,0.5f));
    m_Ed_Send_UserID->setTag(Tag_Send_EdUserID);
    m_sendLayer->addChild(m_Ed_Send_UserID);
    
    
    Label *label3 = Label::createWithSystemFont("用户昵称:",_GAME_FONT_NAME_1_,46);
    label3->setColor(_GAME_FONT_COLOR_1_);
    label3->setAnchorPoint(Vec2(1,0.5f));
    label3->setPosition(Vec2(700,555));
    m_sendLayer->addChild(label3);
    
    Sprite * bg3 = Sprite::createWithSpriteFrameName("bg_shuru.png");
    bg3->setAnchorPoint(Vec2(0, 0.5f));
    bg3->setPosition(Vec2(720,560));
    m_sendLayer->addChild(bg3);
    
    Label *font1=Label::createWithSystemFont("",_GAME_FONT_NAME_1_,46);
    font1->setTag(Tag_Send_NickName);
    font1->setAnchorPoint(Vec2(0,0.5f));
    font1->setPosition(Vec2(745,555));
    m_sendLayer->addChild(font1);
    
    Label *label4 = Label::createWithSystemFont("酒吧豆数:",_GAME_FONT_NAME_1_,46);
    label4->setColor(_GAME_FONT_COLOR_1_);
    label4->setAnchorPoint(Vec2(1,0.5f));
    label4->setPosition(Vec2(700,435));
    m_sendLayer->addChild(label4);
    
    Sprite * bg4 = Sprite::createWithSpriteFrameName("bg_shuru.png");
    bg4->setAnchorPoint(Vec2(0, 0.5f));
    bg4->setPosition(Vec2(720,440));
    m_sendLayer->addChild(bg4);
    
    std::string strTradec = "\u8bf7\u8f93\u5165\u6570\u989d\uff1a";
    Tools::StringTransform(strTradec);
    
    m_Ed_Send_Trade = cocos2d::ui::EditBox::create(inputSize, INPUT_BACKGROUND);
    m_Ed_Send_Trade->setFontName(_GAME_FONT_NAME_1_);
    m_Ed_Send_Trade->setFontSize(46);
    m_Ed_Send_Trade->setPlaceholderFontName(_GAME_FONT_NAME_1_);
    m_Ed_Send_Trade->setPlaceHolder(strTradec.c_str());
    m_Ed_Send_Trade->setPlaceholderFontSize(46);
    m_Ed_Send_Trade->setMaxLength(10);
    m_Ed_Send_Trade->setInputMode(cocos2d::ui::EditBox::InputMode::NUMERIC);
    m_Ed_Send_Trade->setPosition(Vec2(740,435));
    m_Ed_Send_Trade->setAnchorPoint(Vec2(0,0.5f));
    m_Ed_Send_Trade->setTag(Tag_Send_EdScore);
    m_sendLayer->addChild(m_Ed_Send_Trade);
    
    auto FlashMoneyBtn = CreateButton("flashMoney",Vec2(1350,800),Tag_FlashMoney);
    m_sendLayer->addChild(FlashMoneyBtn,100,Tag_FlashMoney);
    
    auto BtnNickName = CreateButton("checkNickName", Vec2(1350,555), Tag_Send_NickBtn);
    m_sendLayer->addChild(BtnNickName ,100 , Tag_Send_NickBtn);
    
    const char* names[] = {"一百万", "五百万", "一千万","", "五千万", "一亿", "清零"};
    for (int i = w100; i < wAllSave; i ++)
    {
        if (i == wAllGet)
            continue;
        float posx = i < wAllGet ? 410 : 210;
        Menu* pNode = CreateButton("doushu", Vec2(posx + 200 * i, 300), i);
        m_sendLayer->addChild(pNode ,100 , w100);
        
        Label* l1 = Label::createWithSystemFont(names[i], _GAME_FONT_NAME_1_, 44);
        l1->setPosition(posx + 200 * i, 300);
        m_sendLayer->addChild(l1, 101);
    }
    
    auto BtnSend = CreateButton("bt_confirm", Vec2(910,150), Tag_Send_Ok);
    m_sendLayer->addChild(BtnSend ,100 , Tag_Send_Ok);
}

void GameBank::initRecordLayer()
{
    m_recordLayer = Layer::create();
    addChild(m_recordLayer);
    
    string names[] = {"日期", "对方昵称", "对方ID", "数量"};
    Vec2 titlePos[] = {Vec2(320, 820), Vec2(760, 820), Vec2(1160, 820), Vec2(1600, 820)};
    Vec2 linePos[] = {Vec2(560, 820), Vec2(960, 820), Vec2(1360, 820)};
    for (int i = 0; i < 4; i++)
    {
        Label* title = Label::createWithSystemFont(names[i], _GAME_FONT_NAME_1_, 46);
        title->setColor(_GAME_FONT_COLOR_4_);
        title->setPosition(titlePos[i]);
        m_recordLayer->addChild(title);
    }
    for (int i = 0; i < 3; i++)
    {
        Sprite* line = Sprite::createWithSpriteFrameName("fendun.png");
        line->setPosition(linePos[i]);
        m_recordLayer->addChild(line);
    }
    
    Size wh = Size(1920,730);
    m_tableView = TableView::create(this, Size(wh.width , wh.height));
    m_tableView->setDirection(ScrollView::Direction::VERTICAL);
    m_tableView->setVerticalFillOrder(TableView::VerticalFillOrder::TOP_DOWN);
    m_tableView->setPosition(0,25);
    m_tableView->setDelegate(this);
    m_tableView->setTag(Tag_RecordTable);
    m_recordLayer->addChild(m_tableView,1000);
}

bool GameBank::init()
{
    if (!Layer::init())	return false;
    
    Tools::addSpriteFrame("SendLayer.plist");
    setLocalZOrder(10);
    setTouchEnabled(true);
        
    Sprite *pBG = Sprite::create("DaTing/RoomBack.jpg");
    pBG->setPosition(_STANDARD_SCREEN_CENTER_);
    addChild(pBG);
    
    for (int i = 0; i < 3; i++)
    {
        m_TagBtn[i] = 50+i;
    }
    m_TagTitle = 100;
    m_TagBankBack = 101;
    
    m_BtnPos[0] = Vec2(400,980);
    m_BtnPos[1] = Vec2(850,980);
    m_BtnPos[2] = Vec2(1300,980);
    
    m_BtnName[0] = "SaveBtn.png";
    m_BtnName[1] = "SendBtn.png";
    m_BtnName[2] = "RecordBtn.png";
    m_BtnName1[0] = "SaveBtn1.png";
    m_BtnName1[1] = "SendBtn1.png";
    m_BtnName1[2] = "RecordBtn1.png";
    
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    if (g_GlobalUnits.m_SwitchYZ == 0)
    {
        Sprite *temp = Sprite::createWithSpriteFrameName(m_BtnName[0].c_str());
        temp->setPosition(m_BtnPos[1]);
        temp->setTag(m_TagBtn[0]);
        addChild(temp);
    }
    else
    {
        for (int i = 0; i < 3; i++)
        {
            Sprite *temp = Sprite::createWithSpriteFrameName(m_BtnName[i].c_str());
            temp->setPosition(m_BtnPos[i]);
            temp->setTag(m_TagBtn[i]);
            addChild(temp);
        }
    }
#else
    for (int i = 0; i < 3; i++)
    {
        Sprite *temp = Sprite::createWithSpriteFrameName(m_BtnName[i].c_str());
        temp->setPosition(m_BtnPos[i]);
        temp->setTag(m_TagBtn[i]);
        addChild(temp);
    }
#endif
    
    auto pNode = CreateButton("btn_close" ,Vec2(1800,980),Tag_Back);
    addChild(pNode , 100 , Tag_Back);
    
    initSaveLayer();
    initRecordLayer();
    initSendLayer();
    
    OpenSave();
    
    auto listener = EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(true);
    listener->onTouchBegan = CC_CALLBACK_2(GameBank::onTouchBegan, this);
    listener->onTouchMoved = CC_CALLBACK_2(GameBank::onTouchMoved, this);
    listener->onTouchEnded = CC_CALLBACK_2(GameBank::onTouchEnded, this);
    _eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);
    
    return true;
}

void GameBank::close()
{
    g_GlobalUnits.m_IsBankOpen = false;
    LobbySocketSink::sharedSocketSink()->SetBankLayer(NULL);
    m_pGameScene->removeChild(this);
}

void GameBank::onRemove()
{
    m_pGameScene->removeChild(this);
}

void GameBank::onEnter()
{
    GameLayer::onEnter();
}

void GameBank::DialogConfirm( Ref *pSender )
{
    GameLayer::DialogConfirm(pSender);
}
void GameBank::onExit()
{
    Tools::removeSpriteFrameCache("SendLayer.plist");
    GameLayer::onExit();
}

void GameBank::onTouchMoved(Touch *pTouch, Event *pEvent )
{
    if (m_tableView != NULL)
    {
        m_tableView->onTouchMoved(pTouch, pEvent);
    }
}

TableViewCell* GameBank::tableCellAtIndex(TableView *table, ssize_t idx)
{
    TableViewCell *cell = table->cellAtIndex(idx);
    if (!cell) {
        cell = new TableViewCell();
        cell->autorelease();
        initCell(cell , idx);
    }
    return cell;
}

bool GameBank::onTouchBegan(Touch *pTouch, Event *pEvent )
{
    if (m_tableView != NULL)
    {
        m_tableView->onTouchBegan(pTouch, pEvent);
    }
    
    Vec2 touchLocation = pTouch->getLocation();
    Vec2 localPos = convertToNodeSpace(touchLocation);
    for (int i = 0; i < 4; i++)
    {
        Node * temp= getChildByTag(m_TagBtn[i]);
        if (temp == NULL) continue;
        Rect rc = Rect(temp->getPositionX() - temp->getContentSize().width * temp->getAnchorPoint().x,
                       temp->getPositionY() - temp->getContentSize().height * temp->getAnchorPoint().y,
                       temp->getContentSize().width, temp->getContentSize().height);
        bool isTouched = rc.containsPoint(localPos);
        if (isTouched)
        {
            if (m_CurIndex == i) break;
            else if (i == 0) OpenSave();
            else if (i == 1)
            {
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
                if (g_GlobalUnits.m_SwitchYZ == 0)
                    AlertMessageLayer::createConfirm("\u6b64\u529f\u80fd\u5df2\u7ecf\u4e0b\u67b6\uff01");
                else
                    OpenSend();
#else
                OpenSend();
#endif
            }
            else if (i == 2)
            {
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
                if (g_GlobalUnits.m_SwitchYZ == 0)
                    AlertMessageLayer::createConfirm("\u6b64\u529f\u80fd\u5df2\u7ecf\u4e0b\u67b6\uff01");
                else
                    OpenRecord();
#else
                OpenRecord();
#endif
            }
            SoundUtil::sharedEngine()->playEffect("buttonMusic");
        }
    }
    
    return isVisible();
}

void GameBank::ClearState(int _index)
{
    removeAllChildByTag(m_TagTitle);
    Sprite *temp;
    
    removeAllChildByTag(m_TagBtn[m_CurIndex]);
    temp = Sprite::createWithSpriteFrameName(m_BtnName[m_CurIndex].c_str());
    temp->setPosition(m_BtnPos[m_CurIndex]);
    temp->setTag(m_TagBtn[m_CurIndex]);
    addChild(temp);
    
    m_CurIndex = _index;
    temp = Sprite::createWithSpriteFrameName(m_BtnName1[m_CurIndex].c_str());
    temp->setPosition(m_BtnPos[m_CurIndex]);
    temp->setTag(m_TagBtn[m_CurIndex]);
    addChild(temp);
}

void GameBank::OpenSave()
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    if (g_GlobalUnits.m_SwitchYZ != 0)
        ClearState(0);
#else
     ClearState(0);
#endif
    m_saveLayer->setVisible(true);
    m_sendLayer->setVisible(false);
    m_recordLayer->setVisible(false);
}

void GameBank::OpenSend()
{
    ClearState(1);
    m_UserType = 0;
    m_saveLayer->setVisible(false);
    m_sendLayer->setVisible(true);
    m_recordLayer->setVisible(false);
}
void GameBank::OpenRecord()
{
    ClearState(2);
    OnSendRecord();
    
    m_saveLayer->setVisible(false);
    m_sendLayer->setVisible(false);
    m_recordLayer->setVisible(true);
}

void GameBank::onTouchEnded(Touch *pTouch, Event *pEvent )
{
    if (m_tableView != NULL)
    {
        m_tableView->onTouchEnded(pTouch, pEvent);
    }
}

void GameBank::OnSendRecord()
{
    g_GlobalUnits.m_SendRecordVec.clear();
    CMD_MB_BankRecord temp;
    ZeroMemory(&temp, sizeof(temp));
    
    struct timeval tv;
    gettimeofday(&tv, 0);
    struct tm *lTime = localtime(&tv.tv_sec);
    
    sprintf( temp.request.szCollectDate, "%d-%02d-%02d", lTime->tm_year + 1900, lTime->tm_mon + 1, lTime->tm_mday);
    
    temp.request.dwUserID = g_GlobalUnits.GetUserID();
    LobbySocketSink::sharedSocketSink()->ConnectServer();
    LobbySocketSink::sharedLoginSocket()->SendData(MDM_GF_BANK,SUB_MB_BANK_RECORD, &temp, sizeof(CMD_MB_BankRecord));
    LoadLayer::create(m_pGameScene);
}

void GameBank::OnSendModifyPassWord()
{
    const char *OldPsw = m_Ed_OldPsw->getText();
    const char *NewPsw = m_Ed_NewPsw->getText();
    const char *NewPsw1 = m_Ed_NewPsw1->getText();
    if (strcmp(NewPsw, NewPsw1) != 0)
    {
        AlertMessageLayer::createConfirm(this , "\u4e24\u6b21\u8f93\u5165\u7684\u5bc6\u7801\u4e0d\u4e00\u6837\uff01");
        return;
    }
    CMD_GF_ModifyPassword temp;
    temp.dwUserID = g_GlobalUnits.GetUserID();
    temp.nType = 2;
    
    char buff[50];
    memset(buff, 0, sizeof(buff));
    sprintf(buff, PWDHEAD, OldPsw);
    CMD5Encrypt::EncryptData(buff, temp.szOldPwd);
 
    memset(buff, 0, sizeof(buff));
    sprintf(buff, PWDHEAD, NewPsw);
    CMD5Encrypt::EncryptData(buff, temp.szNewPwd);
    
    LobbySocketSink::sharedSocketSink()->ConnectServer();
    LobbySocketSink::sharedLoginSocket()->SendData(MDM_GF_BANK, SUB_GF_MODIFY_PASSWORD, &temp, sizeof(CMD_GF_ModifyPassword));
    LoadLayer::create(m_pGameScene);
}

void GameBank::OnSendSave()
{
    if (g_GlobalUnits.GetGolbalUserData().lScore <= 0)
    {
        AlertMessageLayer::createConfirm(this , "\u60a8\u8d26\u53f7\u4e0a\u7684\u91d1\u989d\u4e3a\u96f6\uff0c\u65e0\u9700\u5b58\u5165");
        return;
    }
    const char *pUserScore = m_Ed_Save_Trade->getText();
    if (strcmp(pUserScore,"") == 0)
    {
        AlertMessageLayer::createConfirm(this , "\u8bf7\u8f93\u5165\u9152\u5427\u8c46\u6570\u989d");
        return;
    }
    LONGLONG lAmount = atol(pUserScore);
    if (lAmount > g_GlobalUnits.GetGolbalUserData().lScore)
    {
        lAmount = g_GlobalUnits.GetGolbalUserData().lScore;
    }
    CMD_GP_BankStoreScore temp;
    temp.dwUserID = g_GlobalUnits.GetGolbalUserData().dwUserID;
    temp.wKindID = m_GameKindID;
    temp.lStorageValue = lAmount;
    temp.lToken = g_GlobalUnits.GetGolbalUserData().lWebToken;
    if(LobbySocketSink::sharedSocketSink()->ConnectServer())
        LobbySocketSink::sharedLoginSocket()->SendData(MDM_GF_BANK,SUB_MB_BANK_STORAGE, &temp, sizeof(CMD_GP_BankStoreScore));
    LoadLayer::create(m_pGameScene);
}

void GameBank::FlashSave(bool suc,int _index)
{
    if (suc)
    {
        char strcc[32]="";
        LONGLONG lMon = g_GlobalUnits.GetGolbalUserData().lInsureScore;
        memset(strcc , 0 , sizeof(strcc));
        Tools::AddComma(lMon , strcc);
        
        Label* saveMoney = (Label*)(m_saveLayer->getChildByTag(Tag_Save_BankScore));
        if (saveMoney) saveMoney->setString(strcc);
        Label* sendMoney = (Label*)(m_sendLayer->getChildByTag(Tag_Send_BankGold));
        if (sendMoney) sendMoney->setString(strcc);
        
        char strc[32]="";
        lMon = g_GlobalUnits.GetGolbalUserData().lScore;
        memset(strc , 0 , sizeof(strc));
        Tools::AddComma(lMon , strc);
        
        Label* gameMoney = (Label*)(m_saveLayer->getChildByTag(Tag_Save_Score));
        if (gameMoney) gameMoney->setString(strc);
        
        if (m_bStartFlash == true)
        {
            m_bStartFlash = false;
            AlertMessageLayer::createConfirm(this , "\u9152\u5427\u8c46\u5237\u65b0\u6210\u529f\uff01");
        }
    }
    else
    {
        AlertMessageLayer::createConfirm(this , "\u64cd\u4f5c\u5931\u8d25\uff01");
    }
}

void GameBank::OnSendGet()
{
    if (g_GlobalUnits.GetGolbalUserData().lInsureScore <= 0)
    {
        AlertMessageLayer::createConfirm(this , "\u94f6\u884c\u4f59\u989d\u4e0d\u8db3\uff0c\u8bf7\u5145\u503c\uff01");
        return;
    }
    const char *pUserScore = m_Ed_Save_Trade->getText();
    if (strcmp(pUserScore, "") == 0)
    {
        AlertMessageLayer::createConfirm(this , "\u8bf7\u8f93\u5165\u9152\u5427\u8c46\u6570\u989d");
        return;
    }
    LONGLONG lAmount = atol(pUserScore);
    if (lAmount > g_GlobalUnits.GetGolbalUserData().lInsureScore)
    {
        lAmount = g_GlobalUnits.GetGolbalUserData().lInsureScore;
    }
    CMD_GP_BankGetScore temp;
    temp.wKindID = m_GameKindID;
    temp.dwUserID = g_GlobalUnits.GetUserID();
    temp.lGetValue = lAmount;
    temp.lToken = g_GlobalUnits.GetGolbalUserData().lWebToken;
    sprintf(temp.szPassword,"%s",g_GlobalUnits.m_BankPassWord);
    LobbySocketSink::sharedSocketSink()->ConnectServer();
    LobbySocketSink::sharedLoginSocket()->SendData(MDM_GF_BANK,SUB_MB_BANK_GET,&temp, sizeof(CMD_GP_BankGetScore));
    LoadLayer::create(m_pGameScene);
}

void GameBank::OnClickSend()
{
    const char *pID = m_Ed_Send_UserID->getText();
    const char *pUserScore = m_Ed_Send_Trade->getText();
    if (strlen(pID) == 0)
    {
        AlertMessageLayer::createConfirm(this , "\u8bf7\u8f93\u5165\u7528\u6237ID");
        return;
    }
    if (strlen(pUserScore) == 0)
    {
        AlertMessageLayer::createConfirm(this , "\u8d60\u9001\u6570\u91cf\u4e3a\u7a7a\u662f\u4e0d\u80fd\u8d60\u9001\u6210\u529f\u7684\u54e6~~");
        return;
    }
    LONGLONG lAmount = atoi(pUserScore);
    if (lAmount < 20000)
    {
        AlertMessageLayer::createConfirm(this , "\u8d60\u9001\u6570\u989d\u4e0d\u80fd\u4f4e\u4e8e100000");
        return;
    }
    DWORD userId = atol(pID);
    if (lAmount > g_GlobalUnits.GetGolbalUserData().lInsureScore)
    {
        AlertMessageLayer::createConfirm(this , "\u60a8\u7684\u94f6\u884c\u4f59\u989d\u4e0d\u8db3\uff01");
        return;
    }
    
    if (m_NickNameText.empty())
    {
        char msg[256];
        sprintf(msg,"\u8bf7\u5148\u68c0\u6d4b\u7528\u6237\u540d\uff0c\u786e\u5b9a\u8f6c\u8d26\u5bf9\u8c61\u65e0\u8bef\uff01");
        AlertMessageLayer::createConfirm(this, msg);
        return;
    }
    else if (m_UserType == 4)
    {
        if( 0 == g_GlobalUnits.GetGolbalUserData().bBindWeChat)
        {
            char msg[256];
        sprintf(msg,"%s","\u4e3a\u4e86\u60a8\u7684\u8d26\u53f7\u5b89\u5168\uff0c\u9700\u8981\u7ed1\u5b9a\u5fae\u4fe1\u53f7\u540e\u624d\u80fd\u8fdb\u884c\u8d60\u9001\u64cd\u4f5c\uff01\u8bf7\u7ed1\u5b9a\u5fae\u4fe1\u5e76\u91cd\u65b0\u767b\u5f55\u540e\u518d\u6b21\u5c1d\u8bd5\u8d60\u9001\uff01");
            AlertMessageLayer::createConfirmAndCancel(this, msg);
            return;
        }
        
        if (m_passStr.empty())
        {
            showPassLayer();
            return;
        }
    }
    
    char msg[256];
    sprintf(msg,"\u60a8\u5c06\u8d60\u9001 %lld \u9152\u5427\u8c46\u7ed9 %s\uff08ID:%lu\uff09",lAmount, m_NickNameText.c_str(),userId);
    AlertMessageLayer::createConfirmAndCancel(this, msg, menu_selector(GameBank::OnSendSend));
}

void GameBank::showPassLayer()
{
    // 设定 背景
    auto sp1 = ui::Scale9Sprite::create("Common/bg_touming.png");
    sp1->setContentSize(Size(1920,1080));
    auto pMenuItem1 = MenuItemSprite::create(sp1, sp1, nullptr);
    m_passBgmenu = Menu::create(pMenuItem1, NULL);
    m_passBgmenu->setPosition(Vec2(960, 540));
    m_sendLayer->addChild(m_passBgmenu, 999);
    
    m_passBg = Sprite::createWithSpriteFrameName("bg_tongyongkuang2.png");
    m_passBg->setPosition(_STANDARD_SCREEN_CENTER_IN_PIXELS_);
    m_sendLayer->addChild(m_passBg, 1000);
    
    ui::Scale9Sprite* passBg = ui::Scale9Sprite::createWithSpriteFrameName("bg_shuru.png");
    passBg->setContentSize(Size(550, 100));
    passBg->setPosition(Vec2(400, 270));
    m_passBg->addChild(passBg);
    
    std::string str = "请输入微信验证码：";
    Tools::StringTransform(str);
    const Size inputSize = Size(450,100);
    m_pass = cocos2d::ui::EditBox::create(inputSize, INPUT_BACKGROUND);
    m_pass->setFontName(_GAME_FONT_NAME_1_);
    m_pass->setFontSize(46);
    m_pass->setFontColor(_GAME_FONT_COLOR_2_);
    m_pass->setPlaceholderFontName(_GAME_FONT_NAME_1_);
    m_pass->setPlaceHolder(str.c_str());
    m_pass->setPlaceholderFontSize(46);
    m_pass->setPlaceholderFontColor(_GAME_FONT_COLOR_2_);
    m_pass->setInputMode(cocos2d::ui::EditBox::InputMode::ANY);
    m_pass->setPosition(Vec2(400, 270));
    m_pass->setMaxLength(20);
    m_passBg->addChild(m_pass);
    
    Menu *btn_confirm = CreateButton("btn_queding" ,Vec2(230,90), Tag_WeChatok);
    m_passBg->addChild(btn_confirm , 0 , Tag_WeChatok);
    
    Menu *btn_cancel = CreateButton("btn_quxiao" ,Vec2(600,90), Tag_WeCharCancel);
    m_passBg->addChild(btn_cancel , 0 , Tag_WeCharCancel);
}

void GameBank::OnSendSend(Ref *pSender)
{
    this->RemoveAlertMessageLayer();
    this->SetAllTouchEnabled(true);
    const char *pID = m_Ed_Send_UserID->getText();
    const char *pSc = m_Ed_Send_Trade->getText();
    m_SendScore = atol(pSc);
    
    CMD_MB_BankPresent	temp;
    temp.dwUserID = g_GlobalUnits.GetGolbalUserData().dwUserID;
    temp.lPresentValue = atol(pSc);
    temp.lToken = g_GlobalUnits.GetGolbalUserData().lWebToken;
    sprintf(temp.szPassword,"%s",g_GlobalUnits.m_BankPassWord);
    if (!m_passStr.empty())
        sprintf(temp.szValidateCode,"%s", m_passStr.c_str());
    else
        sprintf(temp.szValidateCode,"%d", 0);
    
    temp.dwGameID = atoi(pID);
    
    m_passStr.clear();
    LobbySocketSink::sharedSocketSink()->ConnectServer();
    LobbySocketSink::sharedLoginSocket()->SendData(MDM_GF_BANK, SUB_MB_BANK_PRESENT,&temp, sizeof(CMD_MB_BankPresent));
    LoadLayer::create(m_pGameScene);
}

void GameBank::OnSendNickName()
{
    const char *pID = m_Ed_Send_UserID->getText();
    if (strlen(pID) == 0)
    {
        AlertMessageLayer::createConfirm(this , "\u8bf7\u8f93\u5165\u7528\u6237ID");
        return;
    }
    m_RecvUserID = atoi(pID);
    CMD_GP_QueryUserInfoRequest_MB	temp;
    temp.dwTargetGameID = atoi(pID);
    temp.dwUserID = g_GlobalUnits.GetUserID();
    temp.lInsureScore = 0;
    LobbySocketSink::sharedSocketSink()->ConnectServer();
    LobbySocketSink::sharedLoginSocket()->SendData(MDM_GF_BANK, SUB_MB_GET_NICKNAME,&temp, sizeof(CMD_GP_QueryUserInfoRequest_MB));
    LoadLayer::create(m_pGameScene);
}

void GameBank::SetNickName(const char* nickname, Color3B color, int nType)
{
    m_UserType = nType;
    Label *temp = (Label *)m_sendLayer->getChildByTag(Tag_Send_NickName);
    if (temp != NULL)
    {
        m_NickNameText = gbk_utf8(nickname);
#ifdef _WIN32
        Tools::GBKToUTF8(m_NickNameText , "gb2312" , "utf-8");
#endif
        temp->setString(m_NickNameText);
        temp->setColor(color);
    }
}

void GameBank::callbackBt( Ref *pSender )
{
    Node *pNode = (Node *)pSender;
    if (m_CurIndex == 0)
    {
        IsClick(pNode->getTag());
    }
    
    switch (pNode->getTag())
    {
        case Tag_WeChatok:
        {
            const char *pID = m_Ed_Send_UserID->getText();
            const char *pUserScore = m_Ed_Send_Trade->getText();
            
            LONGLONG lAmount = atoi(pUserScore);
            DWORD userId = atol(pID);
            char msg[256];
            
            sprintf(msg,"\u60a8\u5c06\u8d60\u9001 %lld \u9152\u5427\u8c46\u7ed9 %s\uff08ID:%lu\uff09", lAmount, m_NickNameText.c_str(), userId);
            AlertMessageLayer::createConfirmAndCancel(this, msg, menu_selector(GameBank::OnSendSend));
        }
        case Tag_WeCharCancel:
        {
            if (nullptr != m_passBgmenu)
            {
                m_passStr = m_pass->getText();
                m_passBgmenu->removeFromParent();
                m_passBg->removeFromParent();
                m_passBgmenu = nullptr;
                m_pass = nullptr;
                m_passBg = nullptr;
            }
            break;
        }
        case Tag_FlashMoney:
        {
            m_bStartFlash = true;
            CMD_GP_QueryGameInsureInfo temp;
            temp.dwUserID = g_GlobalUnits.GetUserID();
            temp.wKindID = m_GameKindID;
            sprintf(temp.szPassword,"%s",g_GlobalUnits.m_BankPassWord);
            temp.lToken = g_GlobalUnits.GetGolbalUserData().lWebToken;
            if(LobbySocketSink::sharedSocketSink()->ConnectServer())
            {
                LobbySocketSink::sharedLoginSocket()->SendData(MDM_GF_BANK,SUB_MB_BANK_QUERY,&temp, sizeof(CMD_GP_QueryGameInsureInfo));
                LoadLayer::create(m_pGameScene);
            }
            
            break;
        }
        case Tag_Modify_Ok:
        {
            SoundUtil::sharedEngine()->playEffect("buttonMusic");
            OnSendModifyPassWord();
            break;
        }
        case wAllSave:
        {
            SoundUtil::sharedEngine()->playEffect("buttonMusic");
            LONGLONG lScore = g_GlobalUnits.GetGolbalUserData().lScore;
            char strc[32]="";
            sprintf(strc,"%lld",lScore);
            if (m_CurIndex == 0 && m_Ed_Save_Trade!= NULL) m_Ed_Save_Trade->setText(strc) ;
            break;
        }
        case wAllGet:
        {
            SoundUtil::sharedEngine()->playEffect("buttonMusic");
            LONGLONG lScore = g_GlobalUnits.GetGolbalUserData().lInsureScore;
            char strc[32]="";
            sprintf(strc,"%lld",lScore);
            if (m_CurIndex == 0 && m_Ed_Save_Trade!= NULL) m_Ed_Save_Trade->setText(strc) ;
            break;
        }
        case Tag_Back:
        {
            SoundUtil::sharedEngine()->playEffect("buttonMusic");
            close();
            g_GlobalUnits.GetGolbalUserData().lScore = 0;
            break;
        }
        case Tag_Save_BtnSave:
        {
            SoundUtil::sharedEngine()->playEffect("buttonMusic");
            OnSendSave();
            break;
        }
        case Tag_Get_BtnGet:
        {
            SoundUtil::sharedEngine()->playEffect("buttonMusic");
            OnSendGet();
            break;
        }
        case Tag_Send_NickBtn:
        {
            SoundUtil::sharedEngine()->playEffect("buttonMusic");
            OnSendNickName();
            break;
        }
        case Tag_Send_Ok:
        {
            SoundUtil::sharedEngine()->playEffect("buttonMusic");
            OnClickSend();
            break;
        }
        case w100:
        {
            SoundUtil::sharedEngine()->playEffect("buttonMusic");
            if (m_BtnClickIndex == 100)
            {
                const char* p = NULL;
                if (m_CurIndex == 0 && m_Ed_Save_Trade!= NULL) p = m_Ed_Save_Trade->getText();
                else if (m_CurIndex == 1 && m_Ed_Send_Trade!= NULL)  p = m_Ed_Send_Trade->getText();
                LONGLONG num = 1000000;
                if (strcmp(p,"") != 0) num = atol(p)+num;
                
                char money[256];
                sprintf(money,"%lld",num);
                if (m_CurIndex == 0 && m_Ed_Save_Trade!= NULL)	m_Ed_Save_Trade->setText(money) ;
                else if (m_CurIndex == 1 && m_Ed_Send_Trade!= NULL) m_Ed_Send_Trade->setText(money) ;
            }
            else
            {
                LONGLONG num = 1000000;
                
                char money[256];
                sprintf(money,"%lld",num);
                if (m_CurIndex == 0 && m_Ed_Save_Trade!= NULL) m_Ed_Save_Trade->setText(money) ;
                else if (m_CurIndex == 1 && m_Ed_Send_Trade!= NULL) m_Ed_Send_Trade->setText(money) ;
            }
            m_BtnClickIndex = 100;
            break;
        }
        case w500:
        {
            SoundUtil::sharedEngine()->playEffect("buttonMusic");
            if (m_BtnClickIndex == 500)
            {
                const char* p = NULL;
                if (m_CurIndex == 0 && m_Ed_Save_Trade!= NULL) p = m_Ed_Save_Trade->getText();
                else if (m_CurIndex == 1 && m_Ed_Send_Trade!= NULL)  p = m_Ed_Send_Trade->getText();
                LONGLONG num = 5000000;
                if (strcmp(p,"") != 0) num = atol(p)+num;
                
                char money[256];
                sprintf(money,"%lld",num);
                if (m_CurIndex == 0 && m_Ed_Save_Trade!= NULL)	m_Ed_Save_Trade->setText(money) ;
                else if (m_CurIndex == 1 && m_Ed_Send_Trade!= NULL) m_Ed_Send_Trade->setText(money) ;
            }
            else
            {
                LONGLONG num = 5000000;
                
                char money[256];
                sprintf(money,"%lld",num);
                if (m_CurIndex == 0 && m_Ed_Save_Trade!= NULL) m_Ed_Save_Trade->setText(money) ;
                else if (m_CurIndex == 1 && m_Ed_Send_Trade!= NULL) m_Ed_Send_Trade->setText(money) ;
            }
            m_BtnClickIndex = 500;
            break;
        }
        case w1000:
        {
            SoundUtil::sharedEngine()->playEffect("buttonMusic");
            if (m_BtnClickIndex == 1000)
            {
                const char* p = NULL;
                if (m_CurIndex == 0 && m_Ed_Save_Trade!= NULL) p = m_Ed_Save_Trade->getText();
                else if (m_CurIndex == 1 && m_Ed_Send_Trade!= NULL)  p = m_Ed_Send_Trade->getText();
                LONGLONG num = 10000000;
                if (strcmp(p,"") != 0) num = atol(p)+num;
                
                char money[256];
                sprintf(money,"%lld",num);
                if (m_CurIndex == 0 && m_Ed_Save_Trade!= NULL)	m_Ed_Save_Trade->setText(money) ;
                else if (m_CurIndex == 1 && m_Ed_Send_Trade!= NULL) m_Ed_Send_Trade->setText(money) ;
            }
            else
            {
                LONGLONG num = 10000000;
                
                char money[256];
                sprintf(money,"%lld",num);
                if (m_CurIndex == 0 && m_Ed_Save_Trade!= NULL) m_Ed_Save_Trade->setText(money) ;
                else if (m_CurIndex == 1 && m_Ed_Send_Trade!= NULL) m_Ed_Send_Trade->setText(money) ;
            }
            m_BtnClickIndex = 1000;
            break;
        }
        case w5000:
        {
            SoundUtil::sharedEngine()->playEffect("buttonMusic");
            if (m_BtnClickIndex == 5000)
            {
                const char* p = NULL;
                if (m_CurIndex == 0 && m_Ed_Save_Trade!= NULL) p = m_Ed_Save_Trade->getText();
                else if (m_CurIndex == 1 && m_Ed_Send_Trade!= NULL)  p = m_Ed_Send_Trade->getText();
                LONGLONG num = 50000000;
                if (strcmp(p,"") != 0) num = atol(p)+num;
                
                char money[256];
                sprintf(money,"%lld",num);
                if (m_CurIndex == 0 && m_Ed_Save_Trade!= NULL)	m_Ed_Save_Trade->setText(money) ;
                else if (m_CurIndex == 1 && m_Ed_Send_Trade!= NULL) m_Ed_Send_Trade->setText(money) ;
            }
            else
            {
                LONGLONG num = 50000000;
                
                char money[256];
                sprintf(money,"%lld",num);
                if (m_CurIndex == 0 && m_Ed_Save_Trade!= NULL) m_Ed_Save_Trade->setText(money) ;
                else if (m_CurIndex == 1 && m_Ed_Send_Trade!= NULL) m_Ed_Send_Trade->setText(money) ;
            }
            m_BtnClickIndex = 5000;
            break;
        }
        case y1:
        {
            SoundUtil::sharedEngine()->playEffect("buttonMusic");
            if (m_BtnClickIndex == 10000)
            {
                const char* p = NULL;
                if (m_CurIndex == 0 && m_Ed_Save_Trade!= NULL) p = m_Ed_Save_Trade->getText();
                else if (m_CurIndex == 1 && m_Ed_Send_Trade!= NULL)  p = m_Ed_Send_Trade->getText();
                LONGLONG num = 100000000;
                if (strcmp(p,"") != 0) num = atol(p)+num;
                
                char money[256];
                sprintf(money,"%lld",num);
                if (m_CurIndex == 0 && m_Ed_Save_Trade!= NULL)	m_Ed_Save_Trade->setText(money) ;
                else if (m_CurIndex == 1 && m_Ed_Send_Trade!= NULL) m_Ed_Send_Trade->setText(money) ;
            }
            else
            {
                LONGLONG num = 100000000;
                
                char money[256];
                sprintf(money,"%lld",num);
                if (m_CurIndex == 0 && m_Ed_Save_Trade!= NULL) m_Ed_Save_Trade->setText(money) ;
                else if (m_CurIndex == 1 && m_Ed_Send_Trade!= NULL) m_Ed_Send_Trade->setText(money) ;
            }
            m_BtnClickIndex = 10000;
            break;
        }
        case clean:
        {
            SoundUtil::sharedEngine()->playEffect("buttonMusic");

            if (m_CurIndex == 0 && m_Ed_Save_Trade!= NULL) m_Ed_Save_Trade->setText("") ;
            else if (m_CurIndex == 1 && m_Ed_Send_Trade!= NULL) m_Ed_Send_Trade->setText("") ;
            m_BtnClickIndex = 0;
            break;
        }
    }
}

Menu* GameBank::CreateButton( std::string szBtName ,const Vec2 &p , int tag )
{
    Menu *pBT = Tools::Button(StrToChar(szBtName+"_normal.png") , StrToChar(szBtName+"_click.png") , p, this , menu_selector(GameBank::callbackBt) , tag);
    return pBT;
}

Label * GameBank::createLabel(const char *szText ,const Vec2 &p, int fontSize ,Color3B color, bool bChinese)
{
    std::string szTemp = szText;
    return createLabel(szTemp , p , fontSize , color , bChinese);
}
Label * GameBank::createLabel(const std::string szText ,const Vec2 &p, int fontSize ,Color3B color, bool bChinese)
{
    
    std::string szTempTitle = szText;
    if (bChinese && szTempTitle.length() != 0)
    {
#ifdef _WIN32
        Tools::GBKToUTF8(szTempTitle , "gb2312" , "utf-8");
#endif // _WIN32
    }
    
    Label *pLable = Label::createWithSystemFont(szTempTitle.c_str(), _GAME_FONT_NAME_1_, fontSize);
    pLable->setHorizontalAlignment(TextHAlignment::LEFT);
    pLable->setColor(color);
    pLable->setAnchorPoint(Vec2(0,0));
    pLable->setPosition(ccppx(p));
    return pLable;
}

bool GameBank::AddGameKind(int _kind, Vec2 pos)
{
    if (_kind == Dating_GameKind_SH)  //梭哈
    {
        Menu* pNode = CreateButton("bank_game_hlwz", pos, Bank_GameSH);
        m_saveLayer->addChild(pNode ,100 , Bank_GameSH);
        return true;
    }
    else if(_kind == Dating_GameKind_Ox2) //2rnn
    {
        Menu* pNode = CreateButton("bank_game_ernn", pos, Bank_GameOx2);
        m_saveLayer->addChild(pNode ,100 , Bank_GameOx2);
        return true;
    }
    else if(_kind == Dating_GameKind_Ox6)  //tbnn
    {
        Menu* pNode = CreateButton("bank_game_lrtb", pos, Bank_GameOx6);
        m_saveLayer->addChild(pNode ,100 , Bank_GameOx6);
        return true;
    }
    else if (_kind == Dating_GameKind_BRNN)
    {
        Menu* pNode = CreateButton("bank_game_brnn", pos, Bank_GameBRNN);
        m_saveLayer->addChild(pNode ,100 , Bank_GameBRNN);
        return true;
    }
    else if (_kind == Dating_GameKind_30S)
    {
        Menu* pNode = CreateButton("bank_game_30s", pos, Bank_Game30S);
        m_saveLayer->addChild(pNode ,100 , Bank_Game30S);
        return true;
    }
    else if (_kind == Dating_GameKind_WKNH)
    {
        Menu* pNode = CreateButton("bank_game_wknh", pos, Bank_GameWKNH);
        m_saveLayer->addChild(pNode ,100 , Bank_GameWKNH);
        return true;
    }
    else if (_kind == Dating_GameKind_HHSW)
    {
        Menu* pNode = CreateButton("bank_game_hhsw", pos, Bank_GameHHSW);
        m_saveLayer->addChild(pNode ,100 , Bank_GameHHSW);
        return true;
    }
    else if (_kind == Dating_GameKind_SDB)
    {
        Menu* pNode = CreateButton("bank_game_sdb", pos, Bank_GameSDB);
        m_saveLayer->addChild(pNode ,100 , Bank_GameSDB);
        return true;
    }
    else if (_kind == Dating_GameKind_QBSQ)
    {
        Menu* pNode = CreateButton("bank_game_qbsk", pos, Bank_GameQBSQ);
        m_saveLayer->addChild(pNode ,100 , Bank_GameQBSQ);
        return true;
    }
    else if (_kind == Dating_GameKind_WZMJ)
    {
        Menu* pNode = CreateButton("bank_game_wzmj", pos, Bank_GameWZMJ);
        m_saveLayer->addChild(pNode ,100 , Bank_GameWZMJ);
        return true;
    }
    else if (_kind == Dating_GameKind_Ox4)
    {
        Menu* pNode = CreateButton("bank_game_ox4", pos, Bank_GameOx4);
        m_saveLayer->addChild(pNode, 100, Bank_GameOx4);
        return true;
    }
    else if (_kind == Dating_GameKind_WZLZ)
    {
        Menu* pNode = CreateButton("bank_game_wzlz", pos, Bank_GameWZLZ);
        m_saveLayer->addChild(pNode ,100 , Bank_GameWZLZ);
        return true;
    }
    else if (_kind == Dating_GameKind_DDZ)
    {
        Menu* pNode = CreateButton("bank_game_ddz", pos, Bank_GameDDZ);
        m_saveLayer->addChild(pNode ,100 , Bank_GameDDZ);
        return true;
    }
    
    return false;
}

void GameBank::IsClick(int BtnTga)
{
    Node * temp= NULL;
    int kindid = 0;
    if (BtnTga ==	   Bank_GameOx2) {temp = m_saveLayer->getChildByTag(Bank_GameOx2); kindid = Dating_GameKind_Ox2;}
    else if (BtnTga == Bank_GameOx6) {temp = m_saveLayer->getChildByTag(Bank_GameOx6); kindid = Dating_GameKind_Ox6;}
    else if (BtnTga == Bank_GameSH)  {temp = m_saveLayer->getChildByTag(Bank_GameSH); kindid = Dating_GameKind_SH;}
    else if (BtnTga == Bank_GameBRNN) {temp = m_saveLayer->getChildByTag(Bank_GameBRNN); kindid = Dating_GameKind_BRNN;}
    else if (BtnTga == Bank_Game30S) {temp = m_saveLayer->getChildByTag(Bank_Game30S); kindid = Dating_GameKind_30S;}
    else if (BtnTga == Bank_GameWKNH) {temp = m_saveLayer->getChildByTag(Bank_GameWKNH); kindid = Dating_GameKind_WKNH;}
    else if (BtnTga == Bank_GameHHSW) {temp = m_saveLayer->getChildByTag(Bank_GameHHSW); kindid = Dating_GameKind_HHSW;}
    else if (BtnTga == Bank_GameSDB) {temp = m_saveLayer->getChildByTag(Bank_GameSDB); kindid = Dating_GameKind_SDB;}
    else if (BtnTga == Bank_GameQBSQ) {temp = m_saveLayer->getChildByTag(Bank_GameQBSQ); kindid = Dating_GameKind_QBSQ;}
    else if (BtnTga == Bank_GameWZMJ) {temp = m_saveLayer->getChildByTag(Bank_GameWZMJ); kindid = Dating_GameKind_WZMJ;}
    else if (BtnTga == Bank_GameWZLZ) {temp = m_saveLayer->getChildByTag(Bank_GameWZLZ); kindid = Dating_GameKind_WZLZ;}
    else if (BtnTga == Bank_GameOx4) { temp = m_saveLayer->getChildByTag(Bank_GameOx4); kindid = Dating_GameKind_Ox4; }
     else if (BtnTga == Bank_GameDDZ) { temp = m_saveLayer->getChildByTag(Bank_GameDDZ); kindid = Dating_GameKind_DDZ; }
    if (kindid != 0) m_GameKindID = kindid;
    
    if (temp != NULL)
    {
        m_saveLayer->removeChildByTag(Bank_GameBack);
        Sprite* tempx = Sprite::createWithSpriteFrameName("bank_game_select.png");
        Vec2 posc = temp->getPosition();
        tempx->setPosition(Vec2(posc.x,posc.y+20));
        tempx->setTag(Bank_GameBack);
        m_saveLayer->addChild(tempx,1001);
        SoundUtil::sharedEngine()->playEffect("buttonMusic");
        
        CMD_GP_QueryGameInsureInfo temp;
        temp.dwUserID = g_GlobalUnits.GetGolbalUserData().dwUserID;
        temp.wKindID = kindid;
        sprintf(temp.szPassword,"%s",g_GlobalUnits.m_BankPassWord);
        temp.lToken = g_GlobalUnits.GetGolbalUserData().lWebToken;
        if(LobbySocketSink::sharedSocketSink()->ConnectServer())
        {
            LobbySocketSink::sharedLoginSocket()->SendData(MDM_GF_BANK,SUB_MB_BANK_QUERY,&temp, sizeof(CMD_GP_QueryGameInsureInfo));
            LoadLayer::create(m_pGameScene);
        }
    }
}

int GameBank::GetGameKindBtnTga(int kindid)
{
    if (kindid == Dating_GameKind_Ox2) return Bank_GameOx2;
    else if (kindid == Dating_GameKind_Ox6) return Bank_GameOx6;
    else if (kindid == Dating_GameKind_SH) return Bank_GameSH;
    else if (kindid == Dating_GameKind_BRNN) return Bank_GameBRNN;
    else if (kindid == Dating_GameKind_30S) return Bank_Game30S;
    else if (kindid == Dating_GameKind_WKNH) return Bank_GameWKNH;
    else if (kindid == Dating_GameKind_HHSW) return Bank_GameHHSW;
    else if (kindid == Dating_GameKind_SDB) return Bank_GameSDB;
    else if (kindid == Dating_GameKind_QBSQ) return Bank_GameQBSQ;
    else if (kindid == Dating_GameKind_WZMJ) return Bank_GameWZMJ;
    else if (kindid == Dating_GameKind_WZLZ) return Bank_GameWZLZ;
    else if (kindid == Dating_GameKind_Ox4) return Bank_GameOx4;
    else if (kindid == Dating_GameKind_DDZ) return Bank_GameDDZ;
    return 0;
}

void GameBank::SendSuc(LONGLONG lSwapScore, LONGLONG lRevenue, const char* time)
{
    char _gold[32];
    memset(_gold , 0 , sizeof(_gold));
    Tools::AddComma(g_GlobalUnits.GetGolbalUserData().lInsureScore, _gold);
    
    Label *bankgold = (Label*)m_sendLayer->getChildByTag(Tag_Send_BankGold);
    bankgold->setString(_gold);
    
    ui::EditBox *userId = (ui::EditBox*)m_sendLayer->getChildByTag(Tag_Send_EdUserID);
    userId->setText("");
    
    Label *nickname = (Label*)m_sendLayer->getChildByTag(Tag_Send_NickName);
    nickname->setString("");
    
    ui::EditBox *sendScore = (ui::EditBox*)m_sendLayer->getChildByTag(Tag_Send_EdScore);
    sendScore->setText("");
    
    GameBankSuc* banksuc = GameBankSuc::create(m_pGameScene);
    banksuc->SetData(g_GlobalUnits.GetGolbalUserData().dwGameID, g_GlobalUnits.GetGolbalUserData().szNickName, m_RecvUserID, m_NickNameText.c_str(), lSwapScore, lRevenue, time);
}
