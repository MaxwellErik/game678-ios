//
//  CPasteboard.hpp
//  复制到剪切板
//
//  Created by mac on 16/5/27.
//  Copyright © 2016年 hixiaosan. All rights reserved.
//

#ifndef CPasteboard_hpp
#define CPasteboard_hpp
#include <string>

class CPasteboard
{
public:
    static void setPasteboardText(const std::string &szText);
    static std::string getPasteboardText();
};

#endif /* CPasteboard_hpp */
