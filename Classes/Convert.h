#ifndef _Convert_H_
#define _Convert_H_
#include <vector>
#include <string>

#include "cocos2d.h"

typedef std::vector<unsigned short> u2string;
typedef std::string					u1string;

/**
 * 计算utf8字符串长度
 * @param utf8 utf8字符串 
 * @return 字符串长度
 */
int utf8_len(const char *utf8);

int utf8_cmp(const char* str1, const char* str2);

/**
 * 计算ucs2字符串长度
 * @param ucs2 ucs2字符串 
 * @return 字符串长度
 */
int ucs2_len(const unsigned short* ucs2);

/**
 * 将utf8字符串转换为ucs2字符串
 * @param utf8 待转换的utf8字符串
 * @return 转换后ucs2字符串
 */
u2string utf8_ucs2(const char* utf8);

/**
 * 将gbk字符串转换为utf8字符串
 * @param gbk 待转换的gbk字符串
 * @return 转换后utf8字符串
 */
u1string gbk_utf8(const char* gbk);

/**
 * 将utf8字符串转换为gbk字符串
 * @param utf8 待转换的utf8字符串
 * @return 转换后gbk字符串
 */
u1string utf8_gbk(const char* utf8);

//////////////////////////////////////////////////////////////////////////
// 辅助函数

#define a_u8(str)	gbk_utf8(str).c_str()
#define u8_a(str)	utf8_gbk(str).c_str()

#define u8_2(utf8)	&utf8_ucs2(utf8)[0]
#define u8_2s(utf8)	utf8_ucs2(utf8)

#endif // _Convert_H_
