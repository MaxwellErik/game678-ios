#ifndef _GameGETSCORE_H_
#define _GameGETSCORE_H_

#include "GameScene.h"
#include "cocos-ext.h"

//����
class GameGetScore : public GameLayer
{
public:
	virtual bool init();  
	static GameGetScore *create(GameScene *pGameScene);
	virtual ~GameGetScore();
	GameGetScore(GameScene *pGameScene);
	GameGetScore(const GameGetScore&);
	GameGetScore& operator = (const GameGetScore&);
	enum Tag
	{
		_BtnFlash,
		_BtnOK,
		_BtnClose,
		_10w,
		_100w,
		_1000w,
		_all,
	};

public:
    cocos2d::ui::EditBox*	m_ED_Score;
    cocos2d::ui::EditBox*	m_ED_Pass;
	int                     m_BackTga;
	Label*                  m_BankScore;
	int                     m_Select;

public:
	void FlashScore();
	virtual void onEnter();
	virtual void onExit();
	virtual bool onTouchBegan(Touch *pTouch, Event *pEvent){return true;}
	virtual void onTouchEnded(Touch *pTouch, Event *pEvent){}
	virtual void onTouchMoved(Touch *pTouch, Event *pEvent){}

	virtual void callbackBt(Ref *pSender);

	Menu* CreateButton( std::string szBtName ,const Vec2 &p , int tag );
	void onRemove();
	Label * createLabel(const char *szText ,const Vec2 &p, int fontSize ,Color3B color, bool bChinese=false);
	Label * createLabel(const std::string szText ,const Vec2 &p, int fontSize ,Color3B color, bool bChinese=false);
};
#endif