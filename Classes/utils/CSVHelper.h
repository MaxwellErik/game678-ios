#pragma once
#ifndef __CSVHELPER__H__
#define __CSVHELPER__H__

#include "cocos2d.h"
using namespace std;
USING_NS_CC;
namespace custom{
	class CSVHelper : public Ref
	{
	public:
		CSVHelper();
		~CSVHelper();

		static CSVHelper* create();
		bool init();
		//解析一张表，如果index>=0那么证明只获取指定行
		ValueMap getTable(string path, string gIndex = "");
	private:
		void rowSplit(std::vector<std::string> &rows, const std::string &content, const char &rowSeperator);
	};
}

#endif // !__CSVHELPER__H__