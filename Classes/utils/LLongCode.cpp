﻿#include "LLongCode.h"
#include <iostream>

LLongCode::LLongCode()
{
	memset(_llongValue, 0, sizeof(LONGLONG));
}

LLongCode::~LLongCode()
{

}

void LLongCode::setLonglongValue(LONGLONG data)
{
	memcpy(&_llongValue, &data, sizeof(LONGLONG));
}

void LLongCode::setByteByIndex(BYTE val, int index)
{
	_llongValue[index] = val;
}

BYTE LLongCode::getByteByIndex(int index)
{
	return _llongValue[index];
}

LONGLONG LLongCode::getLongLongValue()
{
	LONGLONG retData = 0;
	memcpy(&retData, _llongValue, sizeof(LONGLONG));
	return retData;
}