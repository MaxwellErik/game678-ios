#ifndef __CodeChange_h__
#define __CodeChange_h__

#include "GameBase.h"

#define UNICODE_MAX_LEN		512
#define UTF8_MAX_LEN		1024

class CodeChange
{
public:
	CodeChange();
	virtual ~CodeChange();

	void setUtf8ByteByIndex(UINT index, BYTE data);
	void setUnicodeByIndex(UINT index, WORD data);
	BYTE getUtf8ByteByIndex(UINT index);
	WORD getUnicodeByIndex(UINT index);

	void setUtf8String(const char* pData, UINT len);
	void setUnicodeLen(UINT len);
	void setUtf8Len(UINT len);
	UINT getUnicodeLen();
	UINT getUtf8Len();
public:
	WORD _unicodeArray[UNICODE_MAX_LEN];
	BYTE  _utf8Array[UTF8_MAX_LEN];

	UINT _unicodeLen;
	UINT _utf8Len;
};

#endif