﻿#include "CodeChange.h"
#include "cocos2d.h"
USING_NS_CC;

CodeChange::CodeChange()
{
	_utf8Len = 0;
	_unicodeLen = 0;

	memset(_unicodeArray, 0, sizeof(WORD)*UNICODE_MAX_LEN);
	memset(_utf8Array, 0, sizeof(BYTE)*UTF8_MAX_LEN);
}

CodeChange::~CodeChange()
{

}

void CodeChange::setUtf8ByteByIndex(UINT index, BYTE data)
{
	if (index < 0 || index >= UTF8_MAX_LEN)
	{
		return;
	}

	_utf8Array[index] = data;
}

void CodeChange::setUnicodeByIndex(UINT index, WORD data)
{
	if (index < 0 || index >= UNICODE_MAX_LEN)
	{
		return;
	}

	_unicodeArray[index] = data;
}

BYTE CodeChange::getUtf8ByteByIndex(UINT index)
{
	if (index < 0 || index >= UTF8_MAX_LEN)
	{
		return 0;
	}
	return _utf8Array[index];
}

WORD CodeChange::getUnicodeByIndex(UINT index)
{
	if (index >= UNICODE_MAX_LEN || index < 0)
	{
		return 0;
	}

	return _unicodeArray[index];
}

void CodeChange::setUtf8String(const char* pData, UINT len)
{
	for (int i = 0; i < len; i++)
	{
		_utf8Array[i] = pData[i];
	}

	_utf8Len = len;
}

void CodeChange::setUnicodeLen(UINT len)
{
	_unicodeLen = len;
}

void CodeChange::setUtf8Len(UINT len)
{
	_utf8Len = len;
}

UINT CodeChange::getUnicodeLen()
{
	return _unicodeLen;
}

UINT CodeChange::getUtf8Len()
{
	return _utf8Len;
}