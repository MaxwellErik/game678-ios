#ifndef __Utils_h__
#define __Utils_h__

#include "../utils/GameBase.h"
#include "CodeChange.h"

#include <iostream>
using namespace std;

class Utils
{
public:
	static void Utf8ToUnicode(CodeChange *str);
	static string UnicodeToUtf8(CodeChange *str);
};

#endif