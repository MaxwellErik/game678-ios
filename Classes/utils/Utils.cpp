#include "Utils.h"
#include <vector>
#include "utf8cpp/utf8.h"
#include "cocos2d.h"
#include "Convert.h"
USING_NS_CC;
using namespace std;


void Utils::Utf8ToUnicode(CodeChange *str)
{
	UINT strLen = str->getUtf8Len();
	vector<CTCHAR> utf16line;
	utf8::utf8to16(&str->_utf8Array[0], &str->_utf8Array[strLen], back_inserter(utf16line));

	str->setUnicodeLen(utf16line.size());
	for (int i = 0; i < utf16line.size(); i++)
	{
		str->setUnicodeByIndex(i, utf16line[i]);
	}
}

string Utils::UnicodeToUtf8(CodeChange *str)
{
	int strLen = str->getUnicodeLen();

	
	std::string utf8line;
	utf8::utf16to8(&str->_unicodeArray[0], &str->_unicodeArray[strLen-1], back_inserter(utf8line));

	str->setUtf8Len(utf8line.size());
	str->setUtf8String(utf8line.c_str(), utf8line.size());

	return utf8line;
}