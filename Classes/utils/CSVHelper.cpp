#include "CSVHelper.h"

#include <iostream>
#include <sstream>
#include <fstream>
#include <cstdlib>
#include <stdlib.h>
#include <stdio.h>

#include "../manual/CCLuaValue.h"
namespace custom{
	//切一个字符串
	void split(string& s, string& delim, vector<string>* ret)
	{
		size_t last = 0;
		size_t index = s.find_first_of(delim, last);
		while (index != string::npos)
		{
			ret->push_back(s.substr(last, index - last));
			last = index + 1;
			index = s.find_first_of(delim, last);
		}
		if (index - last > 0)
		{
			ret->push_back(s.substr(last, index - last));
		}
	}

	CSVHelper::CSVHelper()
	{
	}


	CSVHelper::~CSVHelper()
	{
	}

	bool CSVHelper::init()
	{
		return true;
	}

	void CSVHelper::rowSplit(std::vector<std::string> &rows, const std::string &content, const char &rowSeperator)
	{
		std::string::size_type lastIndex = content.find_first_not_of(rowSeperator, 0);
		std::string::size_type currentIndex = content.find_first_of(rowSeperator, lastIndex);

		while (std::string::npos != currentIndex || std::string::npos != lastIndex) {
			rows.push_back(content.substr(lastIndex, currentIndex - lastIndex));
			lastIndex = content.find_first_not_of(rowSeperator, currentIndex);
			currentIndex = content.find_first_of(rowSeperator, lastIndex);
		}
	}
	//解析一行
	ValueMap AnalysisOneRow(vector<string> v, vector<string> argNames)
	{
		ValueMap rowDict;
		vector<string>::iterator p;
		int index = 0;
		for (p = v.begin(); p != v.end(); p++)
		{
			string a = *p;
			string arg = argNames[index];
			rowDict[arg] = a;
			index++;
		}
		return rowDict;
	}

	CSVHelper* CSVHelper::create()
	{
		CSVHelper* ret = new CSVHelper();
		if (ret->init())
		{
			ret->autorelease();
			return ret;
		}
		CC_SAFE_DELETE(ret);
		return NULL;
	}

	ValueMap CSVHelper::getTable(string path, string gIndex /* = "" */)
	{
		FileUtils *fu = FileUtils::getInstance();
		string wPath = fu->fullPathForFilename(path);

		ValueMap dict;

		Data data = fu->getDataFromFile(wPath.c_str());

		unsigned char* pBuffer = NULL;
		ssize_t size = 0;
		pBuffer = data.getBytes();
		size = data.getSize();

		std::string tmpStr = (char*)pBuffer;
		std::string fileContent = tmpStr.substr(0, size);

		vector<string> line;
		rowSplit(line, fileContent, '\n');

		//用于存放字段名称
		vector<string> argNames;
		//当前在读第几行，默认从0开始
		int row = 0;

		string s;
		for (unsigned int i = 0; i < line.size(); ++i)
		{
			s = line[i];
			if (s[s.length() - 1] == '\r') {
				s = s.substr(0, s.length() - 1);
			}
			if (s == "" || s.empty())
			{
				continue;
			}
			vector<string> v;
			string sep = ",";

			split(s, sep, &v);
			if (row == 2)
			{
				//第一行默认为所有字段的名称
				vector<string>::iterator p;
				for (p = v.begin(); p != v.end(); p++)//遍历整个vector，拿到所有的字段
				{
					string a = *p;
					argNames.push_back(a);
				}
				row++;
				continue;//解析完毕之后继续
			}
			if (row == 0)
			{
				//完全的注释行
				row++;
				continue;
			}
			if (row == 1)
			{
				//预留特殊
				row++;
				continue;
			}
			string sid = v[0];//获取当前行的sid
			ValueMap rowDict;//单独一行的dict
			bool found = false;
			if (gIndex != "")
			{
				if (gIndex != sid)
				{
					continue;
				}
				ValueMap adic = AnalysisOneRow(v, argNames);
				rowDict = adic;
				found = true;
			}
			if (found)
			{
				dict = rowDict;
				return dict;
			}

			rowDict = AnalysisOneRow(v, argNames);
			dict[sid] = rowDict;
			row++;

		}
		return dict;
	}
}
