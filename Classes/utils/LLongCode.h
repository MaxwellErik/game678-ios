#ifndef __LLongCode_h__
#define __LLongCode_h__

#include "GameBase.h"

class LLongCode
{
public:
	LLongCode();
	virtual ~LLongCode();

	void setLonglongValue(LONGLONG data);
	void setByteByIndex(BYTE val, int index);
	BYTE getByteByIndex(int index);
	LONGLONG getLongLongValue();
public:
	BYTE _llongValue[8];
};

#endif