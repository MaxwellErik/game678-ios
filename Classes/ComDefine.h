﻿#ifndef _COM_DEFINE_H_
#define _COM_DEFINE_H_


#ifdef _WIN32
#ifdef __LP64__
typedef int                 IosDword;
#else
typedef unsigned long       IosDword;
#endif
#endif

/////////////////////////////////////////////////////////////////////////////////////
#ifndef _WIN32
typedef char                CHAR;
typedef signed char         INT8;
typedef unsigned char       UCHAR;
typedef unsigned char       UINT8;
typedef unsigned char       BYTE;
typedef short               SHORT;
typedef signed short        INT16;
typedef unsigned short      USHORT;
typedef unsigned short      UINT16;
typedef unsigned short      WORD;
typedef int                 INT;
typedef signed int          INT32;
//typedef unsigned int        UINT;
typedef unsigned int        UINT32;
typedef long                LONG;
typedef unsigned long       ULONG;
typedef unsigned long       DWORD;
#ifdef __LP64__
typedef int                 IosDword;
#else
typedef unsigned long       IosDword;
#endif
typedef long long           LONGLONG;
typedef long long           LONG64;
typedef signed long long    INT64;
typedef unsigned long long  ULONGLONG;
typedef unsigned long long  DWORDLONG;
typedef unsigned long long  ULONG64;
typedef unsigned long long  DWORD64;
typedef unsigned long long  UINT64;
typedef void				VOID;
typedef unsigned long long  ULONG_PTR, *PULONG_PTR;
typedef ULONG_PTR DWORD_PTR, *PDWORD_PTR;
typedef DWORD				COLORREF;

#ifndef TEXT
#define TEXT(quote) quote         // r_winnt
#endif

#ifndef _TCHAR_DEFINED
typedef char TCHAR, *PTCHAR;
typedef unsigned char TBYTE , *PTBYTE ;
#define _TCHAR_DEFINED
#endif /* !_TCHAR_DEFINED */

#ifndef FALSE
#define FALSE               0
#endif

#ifndef TRUE
#define TRUE                1
#endif



#ifndef MAKEWORD
#define MAKEWORD(a, b)      ((WORD)(((BYTE)(((DWORD_PTR)(a)) & 0xff)) | ((WORD)((BYTE)(((DWORD_PTR)(b)) & 0xff))) << 8))
#define MAKELONG(a, b)      ((LONG)(((WORD)(((DWORD_PTR)(a)) & 0xffff)) | ((DWORD)((WORD)(((DWORD_PTR)(b)) & 0xffff))) << 16))
#define LOWORD(l)           ((WORD)(((DWORD_PTR)(l)) & 0xffff))
#define HIWORD(l)           ((WORD)((((DWORD_PTR)(l)) >> 16) & 0xffff))
#define LOBYTE(w)           ((BYTE)(((DWORD_PTR)(w)) & 0xff))
#define HIBYTE(w)           ((BYTE)((((DWORD_PTR)(w)) >> 8) & 0xff))
#endif

#ifndef GUID_DEFINED
#define GUID_DEFINED
#if defined(__midl)
typedef struct {
	unsigned long  Data1;
	unsigned short Data2;
	unsigned short Data3;
	byte           Data4[ 8 ];
} GUID;
#else
typedef struct _GUID {
	unsigned long  Data1;
	unsigned short Data2;
	unsigned short Data3;
	unsigned char  Data4[ 8 ];
} GUID;
#endif
#endif



#define MoveMemory(Destination,Source,Length) memmove((Destination),(Source),(Length))
#define CopyMemory(Destination,Source,Length) memcpy((Destination),(Source),(Length))
#define FillMemory(Destination,Length,Fill) memset((Destination),(Fill),(Length))
#define ZeroMemory(Destination,Length) memset((Destination),0,(Length))

#define lstrcpyn(Destination,Source,Length) strcpy(Destination, Source)

//删除指针
#define SafeDelete(pData) { try { delete pData; } catch (...) { ASSERT(FALSE); } pData=0; }

//删除数组
#define SafeDeleteArray(pData) { try { delete [] pData; } catch (...) { ASSERT(FALSE); } pData=0; }


#endif

//数组维数
#ifndef CountArray
#define CountArray(Array) (sizeof(Array)/sizeof(Array[0]))
#endif


//无效数值
#ifndef INVALID_BYTE


#define INVALID_BYTE				((BYTE)(0xFF))						//无效数值
#define INVALID_WORD				((WORD)(0xFFFF))					//无效数值
#define INVALID_DWORD				((DWORD)(0xFFFFFFFF))				//无效数值

//无效数值
#define INVALID_TABLE				INVALID_WORD						//无效桌子
#define INVALID_CHAIR				INVALID_WORD						//无效椅子


#endif

#define PRODUCT_VER					6									//产品版本

//模块版本
#define PROCESS_VERSION(cbMainVer,cbSubVer,cbBuildVer)					\
	(DWORD)(														\
	(((BYTE)(PRODUCT_VER))<<24)+									\
	(((BYTE)(cbMainVer))<<16)+										\
	((BYTE)(cbSubVer)<<8)+											\
	(BYTE)(cbBuildVer))


#endif
