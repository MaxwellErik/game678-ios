﻿#ifndef _LOGIN_LAYER_H_
#define _LOGIN_LAYER_H_

#include "GameScene.h"
#include "LobbyView.h"
#include "GameScene.h"
#include "cocos-ext.h"
#include "JniSink.h"

class NewGameLayer;
class LoginLayer : public GameLayer, public JniCallback
{
public:
	static LoginLayer *create(GameScene *pGameScene);
	LoginLayer(const LoginLayer&);
	LoginLayer(GameScene *pGameScenee);
	LoginLayer& operator = (const LoginLayer&);

	virtual ~LoginLayer();

	virtual bool init();  
	virtual void onEnter();
	virtual void onExit();

    virtual bool onTouchBegan(Touch *pTouch, Event *pEvent);
	virtual void onTouchMoved(Touch *pTouch, Event *pEvent);
	virtual void onTouchEnded(Touch *pTouch, Event *pEvent);

	// TextField 触发
	virtual bool onTextFieldAttachWithIME(TextFieldTTF*pSender){return false;}
	virtual bool onTextFieldDetachWithIME(TextFieldTTF*pSender){return false;}
	virtual bool onTextFieldInsertText(TextFieldTTF *pSender, const char *text, int nLen){return true;}
	virtual bool onTextFieldDeleteBackward(TextFieldTTF *pSender, const char *delText, int nLen){return true;}

	// IME 触发
	virtual void keyboardWillShow(IMEKeyboardNotificationInfo &info){}
	virtual void keyboardDidShow(IMEKeyboardNotificationInfo &info){}
	virtual void keyboardWillHide(IMEKeyboardNotificationInfo &info){}
	virtual void keyboardDidHide(IMEKeyboardNotificationInfo &info){}
	// 响应手机快捷键

	virtual void OnOpenIME(){}
	virtual void OnCloseIME(){}

	virtual void WXLoginSuc(const char *szMessage);
    
    virtual void GetAndroidVersionSuc(const char *szMessage);
	//充值成功
	virtual void paycallback(const char *szOrderNum){}
	//订单号获取成功
	virtual void ordernumcallback(const char *szOrderNum){}
	//分享完成
	virtual void shareCallback(int type , bool bSuccess){}	

public:
	void onRemove();
	void close();
    void addDisableMenu();
    void removeDisableMenu();
	void InitLoginLayer();
    void InitBindIdCard();
    void InitWechatPass();
    void InitRegisterLayer();
	void OnExitLoginLayer();
    void OnExitLoginLayerToBind();
    void OnExitLoginLayerToWechatPass();
    void OnExitLoginLayerToRegister();
    void OnExitRegister();
    void OnExitBindIdCard();
    void OnExitWeChatPass();
    
    void UpdateCreateLoad(float deltaTime);
    void addButton();
    void showWechatLoginBtn(bool show);
    
	Menu* CreateButton(std::string szBtName ,const Vec2 &p , int tag);
	void callbackBt( Ref* pObject );
//    int verifyIDCard(const char* input);
    void initUserData();
	void SetUserName(const char* strname);
	void SetEnableIME(bool able);
    
    void sendRegister();
	void sendLoginData(const char * szUserName , const char * szPassword); //发送登陆消息
    void sendLoginTouristData();
    void sendLoginDataToIDCard();
    void sendLoginDataWeChatPass();
    
    bool IDCardCheck(char* input);
    
	void OnExitCall(Ref* pObject);
	void DialogConfirm(Ref *pSender);
	void DialogCancel(Ref *pSender);

    void ReconnectServer();
	void AutoSelectLine();	//自动选择线路
	int getRand(int start,int end);
	void UpdateloginWX(float deltaTime);
    void IosWxGoginSuc(const char* unionid, int code);
    void IosGetAuditStateResponse();
    bool checkNameNomative(const string& text, bool isNickname = false);
    void delayGetAndroidVersion(float dt);
public:
	int		m_Tga_LoginBack;	//登录背景标记
    int     m_Tga_TouchDisableMenu;
    int     m_Tga_BindBack_Tga;
    int     m_Tga_WechatBack_Tga;
	bool	m_bRemeberPsd;
	int		m_cbGender;
    int     m_Tga_RegBack;
    
	Menu* m_rememberPass;//记住密码
	Sprite* m_AutoGou;

	cocos2d::ui::EditBox*	m_pUserName;
	cocos2d::ui::EditBox*	m_pPassword;
	cocos2d::ui::EditBox*	m_pRegisterUserName;
	cocos2d::ui::EditBox*	m_pRegisterPassword1;
	cocos2d::ui::EditBox*	m_pRegisterPassword2;
	cocos2d::ui::EditBox*	m_pIDCard;
	cocos2d::ui::EditBox*   m_pPhone;
	cocos2d::ui::EditBox*	m_pQQ;
	cocos2d::ui::EditBox*	m_BindIDCard;
	cocos2d::ui::EditBox*	m_WeChatPass;
	cocos2d::ui::EditBox*	m_pNickName;

	char				m_contentTemp[256];
};

#endif
