﻿#ifndef _Ox66GAME_VIEW_LAYER_H_
#define _Ox66GAME_VIEW_LAYER_H_

#include "GameScene.h"
#include "Ox6GameLogic.h"
#include "FrameGameView.h"
#include "CMD_Ox6.h"
#include "ui/UIListView.h"

struct Ox6_GameInfo
{
	BYTE				cbItem[ROW_COUNT][LINE_COUNT];
	LONGLONG			lWinScore;
	BYTE				cbAll;
	BYTE				cbMaryCount;
};

enum
{
	Ox6_TAG_URL_ADV=0,
	Ox6_TAG_URL_SM_LOG,		//短信发送log
	Ox6_TAG_VALID
};

class CTimeTaskLayer;
class Ox6GameViewLayer : public IGameView
{
public:

	enum enTag
	{
		TAG_CELL_LINE=0,
		TAG_CELL_LIGHT,
		TAG_USER_SCORE,
		TAG_TOP_TITLE,
		TAG_TOP_BEATDRUM_LEFT,
		TAG_TOP_BEATDRUM_RIGHT,
		TAG_TOP_TEXTSHZ,
		TAG_TOP_BANNER,
		TAG_BET_VIEWBG,
		TAG_BT_ADDSCORE,
		TAG_BT_TASKBOX,
		TAG_BTN_MENU,
		TAG_BTN_HELP,
		TAG_BT_ANI_ADDSCORE,
		TAG_TASK_STATE_BUBBLE,		//任务状态气泡
		TAG_BT_ADDSCORE1,
		TAG_MAX
	};

	static Ox6GameViewLayer *create(GameScene *pGameScene);
	
	virtual ~Ox6GameViewLayer();
	virtual bool init(); 
	virtual void onEnter();
	virtual void onExit();

	// default implements are used to call script callback if exist	
	virtual bool onTouchBegan(Touch *pTouch, Event *pEvent);
	virtual void onTouchMoved(Touch *pTouch, Event *pEvent);
	virtual void onTouchEnded(Touch *pTouch, Event *pEvent);

	// TextField 触发
	virtual bool onTextFieldAttachWithIME(TextFieldTTF*pSender);
	virtual bool onTextFieldDetachWithIME(TextFieldTTF*pSender);
	virtual bool onTextFieldInsertText(TextFieldTTF *pSender, const char *text, int nLen);
	virtual bool onTextFieldDeleteBackward(TextFieldTTF *pSender, const char *delText, int nLen);

	// IME 触发
	virtual void keyboardWillShow(IMEKeyboardNotificationInfo &info);
	virtual void keyboardDidShow(IMEKeyboardNotificationInfo &info);
	virtual void keyboardWillHide(IMEKeyboardNotificationInfo &info);
	virtual void keyboardDidHide(IMEKeyboardNotificationInfo &info);

	// 响应菜单快捷键
	virtual void backLoginView(Ref *pSender);

	wstring s2ws1(const string& s);
	std::string WStrToUTF81(const std::wstring& str);
	void WStrToUTF81(std::string& dest, const wstring& src);
	string AddCommaToNum(LONG Num);

	//初始化
	void InitGame();
	void AddPlayerInfo();
    void SendCaijinMsg();
	void ChangePlayerInfo(tagUserData * pUserDatac);
	void DeletePlayer(tagUserData * pUserDatac);
	void AddButton();
	Menu* CreateButton(std::string szBtName ,const Vec2 &p , int tag);

	// 按钮事件管理
	void DialogConfirm(Ref *pSender);
	void DialogCancel(Ref *pSender);

	void callbackBt(Ref *pSender );

	void hiddenAll();

	virtual void GameEnd();

	virtual void ShowAddScoreBtn(bool bShow=true);

	virtual void UpdateDrawUserScore();		//更新显示的游戏币数量

	void OnQuit();

	//网络接口
public:
	void OnEventUserLeave( tagUserData * pUserData, WORD wChairID, bool bLookonUser );
	//用户进入
	virtual void OnEventUserEnter(tagUserData * pUserData, WORD wChairID, bool bLookonUser);
	//用户积分
	virtual void OnEventUserScore(tagUserData * pUserData, WORD wChairID, bool bLookonUser);
	//用户状态
	virtual void OnEventUserStatus(tagUserData * pUserData, WORD wChairID, bool bLookonUser);
    //用户换桌
    virtual void OnEventUserChangeTable(tagUserData * pUserData, WORD wChairID, bool bLookonUser){};
	//游戏消息
	virtual bool OnGameMessage(WORD wSubCmdID, const void * pBuffer, WORD wDataSize);
	//场景消息
	virtual bool OnGameSceneMessage(BYTE cbGameStatus, const void * pBuffer, WORD wDataSize);

	virtual void OnReconnectAction(){};

protected:	

	Ox6GameViewLayer(GameScene *pGameScene);

protected:
	Ox6GameLogic		m_GameLogic;
	MenuItemSprite*     m_pAddScoreBT;	//充值“+”号按钮
	Sprite*             m_BackSpr;
    Sprite*             m_CaijinBackSpr;//彩金池背景
    Sprite*             m_CaijinInfoBg;
    Sprite*             m_CaijinRewardBG;
	Animate*			m_KuangAnimate;
    ui::ListView*       m_listView;
	Menu*				m_BtnReadyPlay;		//开始按钮
	Menu*				m_BtnBackToLobby;	//返回按钮
	Menu*				m_BtnOpenCard;		//开牌按钮
	Menu*				m_BtnSeting;		//设置按钮
	Menu*				m_AutoOkBtn;		//托管
	Menu*				m_AutoCancleBtn;	//取消托管
	Menu*				m_BtnGetScoreBtn;
    Menu*               m_BtnReward;
    Menu*               m_BtnInfo;
    
	Point				m_HeadPos[Ox6_GAME_PLAYER];
	Point				m_ReadyPos[Ox6_GAME_PLAYER];
	Point				m_GoldPos[Ox6_GAME_PLAYER];
	Point				m_OpenPos[Ox6_GAME_PLAYER];
	Point				m_CardType[Ox6_GAME_PLAYER];
	Point				m_CardTypeNone[Ox6_GAME_PLAYER];

	LONGLONG			m_lTurnMaxScore;
	LONGLONG			m_lCellScore;
    LONGLONG            m_lLotteryWeek;
    Label*              m_LabelLottery;
    LabelAtlas*         m_LotteryTotal;
    
	bool				m_cbDynamicJoin;
    bool				m_LookMode;
	BYTE				m_HandCardData[Ox6_MAXCOUNT];
	BYTE				m_cbPlayStatus[Ox6_GAME_PLAYER];		//用户状态
	WORD				m_wBankerUser;
	WORD				m_wViewChairID[Ox6_GAME_PLAYER];

	int					m_UpCardIndex;		 //正上方牌的索引
	int					m_LeftCardIndex1;	 //左上1
	int					m_LeftCardIndex2;	 //左上2
	int					m_DownCardIndex;	 //自己的牌索引
	int					m_RightCardIndex1;	 //左上1
	int					m_RightCardIndex2;	 //左上2

	//发牌后的索引 
	int					m_MyHandIndex;
	int					m_SendCardIndex;
	BYTE				m_MyHandCardData[5];

	Sprite*			m_OpenCardSpr[Ox6_GAME_PLAYER];//开牌标志
	Sprite*			m_ClockSpr;

	int					m_StartTime;
	bool				m_bHaveNN;
	bool				m_bAuto;

	//tga
	int					m_ReadyTga[6];
	int					m_HeadTga[6];
	int					m_GlodTga[6];
	int					m_NickNameTga[6];
	int					m_PlayerCardTypeTga[6];
	int					m_BetTga[6];
	int					m_SendCardTga[6];
	int					m_DisCardTga[6];
	int					m_MyMoveOverCardTga;
	int					m_OpenCardTga[6];
    
    vector<CMD_GF_LotteryPlayer> m_lotterplayer;
public:
	static void reset();

	// 防止直接引用 
	Ox6GameViewLayer(const Ox6GameViewLayer&);
    Ox6GameViewLayer& operator = (const Ox6GameViewLayer&);

protected:
	Ox6_GameInfo				m_GameInfo;


//发送网络消息

public:
	void SendGameStart();

	void SendReqTaskConfig();

	void SendReqTaskStatus();

	void SendReqRcvTask(int iIndex);	//领取任务奖励

	void SendReqRcvTaskByTaskID(WORD wTaskID);	//领取任务奖励

	void SendReqTransProp();

	void SendSendProp(DWORD dwRcvUserID ,WORD wPropID , WORD wAmount , LONG lPayscore);

	void SendOpenCard();

	void SendReqSendRecord();

	void AutoOpenCard(float dt);

	void AutoStartGame(float dt);
    
    void DrawWinList();
//接收网络消息
protected:
    bool IsLookonMode();
    
	bool OnBet(const void * pBuffer, WORD wDataSize);

	bool OnBetFail();

	bool OnSubGameStart(const void * pBuffer, WORD wDataSize);

	bool OnSubSendCard(const void * pBuffer, WORD wDataSize);

	void dispatchCards(WORD chair, BYTE card);

	void CardMoveCallback();

	string GetCardStringName(BYTE card);

	void SendCard(float dt);

	bool OnSubOpenCard(const void * pBuffer, WORD wDataSize);

	bool OnSubGameEnd(const void * pBuffer, WORD wDataSize);

	void SetMyCardData(BYTE cbCardData[], BYTE cbCardCount);

	void SetPlayerCardData(WORD wchair, BYTE cbCardData[], BYTE cbCardCount);

	void SetPlayerCardDataNone(WORD wchair, BYTE cbCardData[], BYTE cbCardCount);
    
    bool OnSubCaijinInfo(const void * pBuffer, WORD wDataSize);
    
    bool OnSubPersonCaijin(const void * pBuffer, WORD wDataSize);
    
	void UpdateTime(float fp);

	void StopTime();

	void StartTime(int _time);



	void GetNumber(LONGLONG _score, vector<int>& _EndScore);
};

#endif
