#ifndef CMD_OX6_HEAD_FILE
#define CMD_OX6_HEAD_FILE

#pragma pack(1)

//////////////////////////////////////////////////////////////////////////
//公共宏定义

#define Ox6_KIND_ID						29  									//游戏 I D
#define Ox6_GAME_PLAYER					6										//游戏人数
#define GAME_NAME					TEXT("通比牛牛")						//游戏名字

#define GAME_GENRE					(GAME_GENRE_GOLD|GAME_GENRE_MATCH)		//游戏类型
#define Ox6_MAXCOUNT					5										//扑克数目

//结束原因
#define Ox6_GER_NO_PLAYER				0x10									//没有玩家

//游戏状态
#define Ox6_GS_TK_FREE					GS_FREE									//等待开始
#define Ox6_GS_TK_CALL					GS_PLAYING								//叫庄状态
#define Ox6_GS_TK_SCORE					GS_PLAYING+1						//下注状态
#define Ox6_GS_TK_PLAYING				GS_PLAYING+2						//游戏进行

//用户状态
#define USEX_NULL                   0                                       //用户状态
#define USEX_PLAYING                1                                       //用户状态
#define USEX_DYNAMIC                2                                       //用户状态   

#define SERVER_LEN						32 
#define LEN_MACHINE_ID                  33

#define Ox6_MAX_CAIJIN_NUM			10				//彩金列表用户
#define Ox6_MAX_RANKING_NUM			5				//奖次

enum enType
{
	enGameID=0,
	enMachine,
};

//彩金得主信息
struct Ox6_CajinInfo
{
	LONGLONG lGetCajin;
	char nickName[32*2];
	char cardType[15*2];
};
////
#define IDM_ADMIN_COMMDN WM_USER+1000
/////////////////////////////////////////////////////////////////////////////////////
#define IDM_ADMIN_STORAGE_QUERY           IDM_ADMIN_COMMDN+1
#define IDM_ADMIN_STORAGE_UPDATE          IDM_ADMIN_COMMDN+2
#define IDM_ADMIN_CONTROL_ENABLE_QUERY    IDM_ADMIN_COMMDN+3
#define IDM_ADMIN_CONTROL_ENABLE_UPDATE   IDM_ADMIN_COMMDN+4
#define IDM_ADMIN_CONTROL_ADD             IDM_ADMIN_COMMDN+5
#define IDM_ADMIN_CONTROL_QUERY           IDM_ADMIN_COMMDN+6
#define IDM_ADMIN_CONTROL_DELETE          IDM_ADMIN_COMMDN+7
#define IDM_ADMIN_PRE_QUERY               IDM_ADMIN_COMMDN+8

#define IDM_ADMIN_FLOAT_ENABLE_QUERY      IDM_ADMIN_COMMDN+9
#define IDM_ADMIN_FLOAT_ENABLE_UPDATE     IDM_ADMIN_COMMDN+10
#define IDM_ADMIN_FLOAT_ADD               IDM_ADMIN_COMMDN+11
#define IDM_ADMIN_FLOAT_QUERY             IDM_ADMIN_COMMDN+12
#define IDM_ADMIN_FLOAT_DELETE            IDM_ADMIN_COMMDN+13
//////////////////////////////////////////////////////////////////////////////////////
//服务器命令结构

#define SUB_S_Ox6_GAME_START				100									//游戏开始
#define SUB_S_Ox6_CALL_BANKER				101									//用户叫庄
#define SUB_S_Ox6_ADD_SCORE					102									//加注
#define SUB_S_Ox6_ADD_SCORE_RES				103									//加注结果
#define SUB_S_Ox6_SEND_CARD					104									//发牌消息
#define SUB_S_Ox6_OPEN_CARD					105									//用户摊牌
#define SUB_S_Ox6_GAME_END					106									//游戏结束

#define SUB_S_ADMIN_CONTROL_ENABLE      107
#define SUB_S_ADMIN_CONTROL             108
#define SUB_S_ADMIN_CONTROL_ADD         109
#define SUB_S_ADMIN_COMTROL_DELETE      110
#define SUB_S_ADMIN_FLOAT_ENABLE        111
#define SUB_S_ADMIN_FLOAT               112
#define SUB_S_ADMIN_FLOAT_ADD           113
#define SUB_S_ADMIN_FLOAT_DELETE        114
#define SUB_S_ADMIN_STORAGE             115

#define SUB_S_Ox6_ADMIN_ALL_DELETE          116


struct CMD_S_Ox6_AllCaijin
{
	LONGLONG lCaijin;
};
//游戏状态
struct CMD_S_Ox6_StatusFree
{
    WORD								wBankerUser;						//庄家用户
    WORD				 				wCurrentUser;						//当前玩家
    LONGLONG							lCellScore;							//基础积分
};

//游戏状态
struct CMD_S_Ox6_StatusCall
{
	WORD							    	wCallBanker;						//叫庄用户
	BYTE							        cbPlayStatus[Ox6_GAME_PLAYER];          //用户状态
};

//游戏状态
struct CMD_S_Ox6_StatusScore
{
	//下注信息
	LONGLONG								lTurnMaxScore;						//最大下注
	//LONGLONG								lTurnLessScore;						//最小下注
	LONGLONG								lTableScore[Ox6_GAME_PLAYER];			//下注数目
	BYTE								    cbPlayStatus[Ox6_GAME_PLAYER];          //用户状态
	WORD							    	wBankerUser;						//庄家用户
};

//游戏状态
struct CMD_S_Ox6_StatusPlay
{
    //状态信息
    WORD								wBankerUser;						//庄家用户
    WORD				 				wCurrentUser;						//当前玩家
    UINT32								bOpenCard[Ox6_GAME_PLAYER];
    BYTE								byCardData[Ox6_MAXCOUNT];
    UINT32								nCardCount[Ox6_GAME_PLAYER];
};
//用户叫庄
struct CMD_S_Ox6_CallBanker
{
	WORD							     	wCallBanker;						//叫庄用户
	bool							    	bFirstTimes;						//首次叫庄
};

//游戏开始
struct CMD_S_Ox6_GameStart
{
    //下注信息
    UINT32								lMaxScore;							//最大下注
    UINT32								lCellScore;							//单元下注
    UINT32								lCurrentTimes;						//当前倍数
    UINT32								lUserMaxScore;						//分数上限
    
    //用户信息
    WORD								wBankerUser;						//庄家用户
    WORD				 				wCurrentUser;						//当前玩家
};

//用户下注
struct CMD_S_Ox6_AddScore
{
	WORD							    	wAddScoreUser;						//加注用户
	LONGLONG								lAddScoreCount;						//加注数目
};

//游戏结束
struct CMD_S_Ox6_GameEnd
{
	
	LONGLONG							lGameScore[Ox6_GAME_PLAYER];                //游戏得分
	LONGLONG							lGameTax[Ox6_GAME_PLAYER];                  //游戏税收
    UINT32								nCardCount[Ox6_GAME_PLAYER];                //牌数目

    BYTE							    cbCardData[Ox6_GAME_PLAYER][Ox6_MAXCOUNT];  //用户扑克
    CHAR								szName[Ox6_GAME_PLAYER][NAME_LEN];          //玩家名字
    LONGLONG							lLottery[Ox6_GAME_PLAYER];                  //彩金得分
};

//发牌数据包
struct CMD_S_Ox6_SendCard
{
    WORD wCurrentUser;
    BYTE byCardData[Ox6_MAXCOUNT];
    UINT32 nCardCount[Ox6_GAME_PLAYER];
};

//用户退出
struct CMD_S_Ox6_PlayerExit
{
	WORD						      		wPlayerID;							//退出用户
};

//用户摊牌
struct CMD_S_Ox6_Open_Card
{
    UINT32 bSucceed;
    WORD wCurrentUser;
    WORD wOpenCardUser;
};

struct CMD_S_Ox6_AdminStorage
{
	LONGLONG                           lStorage;
	int                                nStorageDeduct;    
};

struct CMD_S_Ox6_AdminControlEnable
{
	BYTE                               cbEnable;
};

struct CMD_S_Ox6_AdminControl
{
	BYTE                              cbType;   //0为ID,1为机器码，2为IP
	IosDword                          dwGameID; //控制ID
	INT                               nPercent; //概率
	INT                               nTimes;   //控制次数，-1表示不限次数
	char                              szMachineID[LEN_MACHINE_ID]; //控制机器码                                   
};

struct CMD_S_Ox6_AdminFloat
{
	LONGLONG                           lStorage;//库存下限
	int                                nPercent ;                      
};

struct CMD_S_Ox6_AdminFloatEnable
{
	BYTE                               cbEnable;
};
//////////////////////////////////////////////////////////////////////////
//客户端命令结构
#define SUB_C_Ox6_CALL_BANKER				1									//用户叫庄
#define SUB_C_Ox6_ADD_SCORE					2									//用户加注
#define SUB_C_Ox6_OPEN_CARD					3									//用户摊牌


#define SUB_C_Ox6_ADMIN_STORAGE_UPDATE            4
#define SUB_C_Ox6_ADMIN_CONTROL_ENABLE_QUERY      5
#define SUB_C_Ox6_ADMIN_CONTROL_ENABLE_UPDATE     6
#define SUB_C_Ox6_ADMIN_CONTROL_QUERY             7
#define SUB_C_Ox6_ADMIN_CONTROL_ADD               8
#define SUB_C_Ox6_ADMIN_CONTROL_DELETE            9
#define SUB_C_Ox6_ADMIN_PRE_QUERY                 10
#define SUB_C_Ox6_ADMIN_FLOAT_ENABLE_QUERY        11
#define SUB_C_Ox6_ADMIN_FLOAT_ENABLE_UPDATE       12
#define SUB_C_Ox6_ADMIN_FLOAT_QUERY               13
#define SUB_C_Ox6_ADMIN_FLOAT_ADD                 14
#define SUB_C_Ox6_ADMIN_FLOAT_DELETE              15
#define SUB_C_Ox6_ADMIN_STORAGE_QUERY             16


#define SUB_C_Ox6_ADMIN_ALL_DELETE                17
//用户叫庄
struct CMD_C_Ox6_CallBanker
{
	BYTE							    	bBanker;							//做庄标志
};

//用户加注
struct CMD_C_Ox6_AddScore
{
	LONGLONG                            nServriValue;
	LONGLONG								lScore;								//加注数目
};

//用户摊牌
struct CMD_C_Ox6_OxCard
{
    WORD								wCurrentUser;
    UINT32								nCardCount;
    BYTE								byCardData[Ox6_MAXCOUNT];
};

struct CMD_C_Ox6_AdminStorageUpdate
{
	LONGLONG                           lStorage;
	INT                                nStorageDeduct;                             
};

struct CMD_C_Ox6_AdminControlEnableUpdate
{
	BYTE                              cbEnable;                                                   
};

struct CMD_C_Ox6_AdminControlAdd
{
	BYTE                              cbType;   //0为ID,1为机器码，2为IP
	DWORD                             dwGameID; //控制ID
	INT                               nPercent; //概率
	INT                               nTimes;   //控制次数，-1表示不限次数
	char                              szMachineID[LEN_MACHINE_ID]; //控制机器码                                   
};

struct CMD_C_Ox6_AdminControlDelete
{
	BYTE                              cbType;   //0为ID,1为机器码，2为IP
	IosDword                          dwGameID; //控制ID
	char                              szMachineID[LEN_MACHINE_ID]; //控制机器码                                   
};

////////////////////////////////////////////////////////////////////////////////////////
struct CMD_C_Ox6_AdminFloatEnableUpdate
{
	BYTE                              cbEnable;                                                   
};
struct CMD_C_Ox6_AdminFloatAdd
{
	LONGLONG                           lStorage;//库存下限
	int                                nPercent ;                      
};

struct CMD_C_Ox6_AdminFloatDelete
{
	LONGLONG                           lStorage;//库存下限
};
//////////////////////////////////////////////////////////////////////////

#pragma pack()
#endif
