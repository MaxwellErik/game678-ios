﻿#include "Ox6GameViewLayer.h"
#include "LobbyLayer.h"
#include "ClientSocketSink.h"
#include "HXmlParse.h"
#include "LocalDataUtil.h"
#include "JniSink.h"
#include "VisibleRect.h"
#include "HttpConstant.h"
#include "Screen.h"
#include "LobbySocketSink.h"
#include "LoginLayer.h"
#include "GameLayerMove.h"
#include "Ox6GameLogic.h"
#include "Convert.h"
#include "PacketAide.h"

#define		MENU_INIT_POINT			(Vec2Make(-_STANDARD_SCREEN_CENTER_.x+68+30 , _STANDARD_SCREEN_CENTER_.y-68-20))

#define		ADV_SIZE				(CCSizeMake(520,105))
//宝箱坐标
#define		BOX_POINT				(Vec2(1195,70))

//最少筹码数
#define		MIN_PROP_PAY_SCORE			1
#define		ALL_ANIMATION_COUNT			5


#define		MAX_CREDIT					(9999999)
#define		MAX_BET						(9999)
#define		MAX_ADV_COUNT				(10)

#if (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
#define		BIG_AWARD_MULITY			10
#else
#define		BIG_AWARD_MULITY			30
#endif // _DEBUG

#define Btn_Ready						1
#define Btn_BackToLobby					2
#define Btn_OpenCard					3
#define Btn_Seting						4
#define Btn_AutoOk						5
#define Btn_AutoCancle					6
#define Btn_GetScoreBtn					7
#define Btn_CaijinInfo                 8
#define Btn_CaijinReward               9

Ox6GameViewLayer::Ox6GameViewLayer(GameScene *pGameScene)
	:IGameView(pGameScene)
{
	ZeroMemory(&m_GameInfo , sizeof(m_GameInfo));
	m_GameState=enGameNormal;
	m_BtnReadyPlay = NULL;
	m_UpCardIndex = 0;
	m_LeftCardIndex1 = 0;
	m_LeftCardIndex2 = 0;
	m_RightCardIndex1 = 0;
	m_RightCardIndex2 = 0;
	m_DownCardIndex = 0;
    m_lLotteryWeek = 0;
	ZeroMemory(m_HandCardData,sizeof(m_HandCardData));
	m_MyHandIndex = 0;
	m_SendCardIndex = 0;
	ZeroMemory(m_MyHandCardData,sizeof(m_MyHandCardData));
    for (int i = 0; i < Ox6_GAME_PLAYER; i++)
    {
        m_OpenCardSpr[i] = NULL;
        m_cbPlayStatus[i] = false;
    }
    m_LabelLottery = nullptr;
    m_LotteryTotal = nullptr;
    
	m_StartTime = 15;
	m_bHaveNN = false;
	m_cbDynamicJoin = true;
	m_bAuto = false;
    m_LookMode = false;
	SetKindId(Ox6_KIND_ID);
    m_lotterplayer.resize(10);
    m_lotterplayer.clear();
}

Ox6GameViewLayer::~Ox6GameViewLayer() 
{
}

Ox6GameViewLayer *Ox6GameViewLayer::create(GameScene *pGameScene)
{
    Ox6GameViewLayer *pLayer = new Ox6GameViewLayer(pGameScene);
    if(pLayer && pLayer->init())
    {
        pLayer->autorelease();
        return pLayer;
    }
    else
    {
        CC_SAFE_DELETE(pLayer);
        return NULL;
    }
}


bool Ox6GameViewLayer::init()
{
	if ( !Layer::init() )
	{
		return false;
	}
	setLocalZOrder(3);
	setPosition(Vec2(0,0));
	setGameStatus(GS_FREE);
	Tools::addSpriteFrame("Ox6/GameBack.plist" );
	Tools::addSpriteFrame("Common/CardSprite2.plist");
	SoundUtil::sharedEngine()->playBackMusic("Ox6/back_ox6", true);
    SoundUtil::sharedEngine()->setBackSoundVolume(g_GlobalUnits.m_fBackMusicValue);
    SoundUtil::sharedEngine()->setSoundVolume(g_GlobalUnits.m_fSoundValue);
	IGameView::onEnter();
	JniSink::share()->setIGameView(this);
	setGameStatus(GS_FREE);
	InitGame();
	AddButton();
    
    SendCaijinMsg();
	
    StartTime(30);
	setTouchEnabled(true);

	auto listener = EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(true);
	listener->onTouchBegan = CC_CALLBACK_2(Ox6GameViewLayer::onTouchBegan, this);
	listener->onTouchMoved = CC_CALLBACK_2(Ox6GameViewLayer::onTouchMoved, this);
	listener->onTouchEnded = CC_CALLBACK_2(Ox6GameViewLayer::onTouchEnded, this);
	_eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);

	AddPlayerInfo();
	return true;
}

void Ox6GameViewLayer::onEnter()
{	
}

void Ox6GameViewLayer::StartTime(int _time)
{
	m_StartTime = _time;
	schedule(schedule_selector(Ox6GameViewLayer::UpdateTime), 1);
	m_ClockSpr->setVisible(true);
	UpdateTime(m_StartTime);
}

void Ox6GameViewLayer::StopTime()
{
	unschedule(schedule_selector(Ox6GameViewLayer::UpdateTime));
	m_ClockSpr->removeAllChildren();
	m_ClockSpr->setVisible(false);
}

void Ox6GameViewLayer::UpdateTime(float fp)
{
	if (m_StartTime <= 0)
	{
		if (m_GameState == enGameNormal ||
			m_GameState == enGameEnd)
		{
			unschedule(schedule_selector(Ox6GameViewLayer::UpdateTime));
			GameLayerMove::sharedGameLayerMoveSink()->CloseSeting();
			GameLayerMove::sharedGameLayerMoveSink()->CloseGetScore();
			RemoveAlertMessageLayer();
			ClientSocketSink::sharedSocketSink()->setFrameGameView(NULL);
			SoundUtil::sharedEngine()->stopAllEffects();
			SoundUtil::sharedEngine()->stopBackMusic();
			ClientSocketSink::sharedSocketSink()->LeftGameReq();
			GameLayerMove::sharedGameLayerMoveSink()->GoGameToTable();
			
		}
		else if (m_GameState == enGameSendCard ||
				 m_GameState == enGameOpenCard)
		{
			SendOpenCard();
		}
		return;
	}

	m_ClockSpr->removeAllChildren();
    std::string str = StringUtils::toString(m_StartTime);
    if (m_StartTime < 10)
        str = "0" + str;
    LabelAtlas *time = LabelAtlas::create(str, "Common/time.png", 33, 50, '+');
    time->setPosition(Vec2(22, 15));
    m_ClockSpr->addChild(time);

	m_StartTime--;

}

void Ox6GameViewLayer::onExit()
{
	Tools::removeSpriteFrameCache("Ox6/GameBack.plist");
	Tools::removeSpriteFrameCache("Common/CardSprite2.plist");
	IGameView::onExit();
}

// Touch 触发
bool Ox6GameViewLayer::onTouchBegan(Touch *pTouch, Event *pEvent)
{
    if (m_CaijinRewardBG->isVisible())
    {
        m_CaijinRewardBG->setVisible(false);
        return false;
    }
    if (m_CaijinInfoBg->isVisible())
    {
        m_CaijinInfoBg->setVisible(false);
        return false;
    }
	return true;
}

void Ox6GameViewLayer::onTouchMoved(Touch *pTouch, Event *pEvent)
{
}

void Ox6GameViewLayer::onTouchEnded(Touch*pTouch, Event*pEvent)
{
}

// TextField 触发
bool Ox6GameViewLayer::onTextFieldAttachWithIME(TextFieldTTF *pSender)
{
	return false;
}

bool Ox6GameViewLayer::onTextFieldDetachWithIME(TextFieldTTF *pSender)
{
	return false;
}

bool Ox6GameViewLayer::onTextFieldInsertText(TextFieldTTF *pSender, const char *text, int nLen)
{
	return true;
}

bool Ox6GameViewLayer::onTextFieldDeleteBackward(TextFieldTTF *pSender, const char *delText, int nLen)
{
	return true;
}

// IME 触发
void Ox6GameViewLayer::keyboardWillShow(IMEKeyboardNotificationInfo& info)
{
}

void Ox6GameViewLayer::keyboardDidShow(IMEKeyboardNotificationInfo& info)
{
}

void Ox6GameViewLayer::keyboardWillHide(IMEKeyboardNotificationInfo& info)
{
	MoveTo *pMovAction = MoveTo::create(0.15f, Vec2(0,0));
	Action *pActSeq = Sequence::create(pMovAction, NULL);
	runAction(pActSeq);
}

void Ox6GameViewLayer::keyboardDidHide(IMEKeyboardNotificationInfo& info)
{
}


// 按钮事件管理
// Alert Message 确认消息处理
void Ox6GameViewLayer::DialogConfirm(Ref *pSender)
{
	this->RemoveAlertMessageLayer();

	// 设定 触发
	this->SetAllTouchEnabled(true);

// 	// 播放 确认 音效
}

// Alert Message 取消消息处理
void Ox6GameViewLayer::DialogCancel(Ref *pSender)
{
	// 清除 Alert Message Layer
	this->RemoveAlertMessageLayer();

	// 设定 触发
	this->SetAllTouchEnabled(true);
}

void Ox6GameViewLayer::OnEventUserScore( tagUserData * pUserData, WORD wChairID, bool bLookonUser )
{
	IGameView::OnEventUserScore(pUserData, wChairID, bLookonUser);
    AddPlayerInfo();
}

void Ox6GameViewLayer::OnEventUserStatus(tagUserData * pUserData, WORD wChairID, bool bLookonUser)
{
	IGameView::OnEventUserStatus(pUserData, wChairID, bLookonUser);
	AddPlayerInfo();
}

void Ox6GameViewLayer::OnEventUserEnter( tagUserData * pUserData, WORD wChairID, bool bLookonUser )
{
	AddPlayerInfo();
}

void Ox6GameViewLayer::OnEventUserLeave( tagUserData * pUserData, WORD wChairID, bool bLookonUser )
{
	AddPlayerInfo();
}

//游戏消息
bool Ox6GameViewLayer::OnGameMessage(WORD wSubCmdID, const void * pBuffer, WORD wDataSize)
{
	CCLOG("RECEIVE MESSAGE CMD[%d]" , wSubCmdID);
	switch (wSubCmdID)
	{
	case SUB_S_Ox6_GAME_START:	//游戏开始
		return OnSubGameStart(pBuffer, wDataSize);
    
    case SUB_S_Ox6_ADD_SCORE_RES:
        return true;
            
	case SUB_S_Ox6_SEND_CARD:	//发牌消息
		return OnSubSendCard(pBuffer, wDataSize);

	case SUB_S_Ox6_OPEN_CARD://用户摊牌
		return OnSubOpenCard(pBuffer, wDataSize);

	case SUB_S_Ox6_GAME_END:	//游戏结束
		return OnSubGameEnd(pBuffer, wDataSize);
            
    case SUB_GF_LOTTERY:
        return OnSubCaijinInfo(pBuffer, wDataSize);
            
    case SUB_GF_LOTTERY_WEEK:
        return OnSubPersonCaijin(pBuffer, wDataSize);
            
	}
	return true;
}

void Ox6GameViewLayer::GetNumber(LONGLONG _score, vector<int>& _EndScore)
{
	if (_score == 0)
	{
		_EndScore.push_back(0);
		return;
	}
	LONGLONG _abs = 0;
	if (_score < 0)
	{
		_abs = 0-_score;
	}
	else
	{
		_abs = _score;
	}


	LONGLONG score = _abs;
	int num = 0; bool isadd = false;
	num = score/1000000000; if(num != 0){isadd = true; _EndScore.push_back(num);}
	num = score/100000000%10; if(num != 0){isadd = true; _EndScore.push_back(num);}else{if(isadd)_EndScore.push_back(num);}
	num = score/10000000%10; if(num != 0){isadd = true; _EndScore.push_back(num);}else{if(isadd)_EndScore.push_back(num);}
	num = score/1000000%10; if(num != 0){isadd = true; _EndScore.push_back(num);}else{if(isadd)_EndScore.push_back(num);}
	num = score/100000%10; if(num != 0){isadd = true; _EndScore.push_back(num);}else{if(isadd)_EndScore.push_back(num);}
	num = score/10000%10; if(num != 0){isadd = true; _EndScore.push_back(num);}else{if(isadd)_EndScore.push_back(num);}
	num = score/1000%10; if(num != 0){isadd = true; _EndScore.push_back(num);}else{if(isadd)_EndScore.push_back(num);}
	num = score/100%10; if(num != 0){isadd = true; _EndScore.push_back(num);}else{if(isadd)_EndScore.push_back(num);}
	num = score/10%10; if(num != 0){isadd = true; _EndScore.push_back(num);}else{if(isadd)_EndScore.push_back(num);}
	num = score%10; if(num != 0){isadd = true; _EndScore.push_back(num);}else{if(isadd)_EndScore.push_back(num);}
}

bool Ox6GameViewLayer::IsLookonMode()
{
    return m_LookMode;
}

//场景消息
bool Ox6GameViewLayer::OnGameSceneMessage(BYTE cbGameStatus, const void * pBuffer, WORD wDataSize)
{
    if (m_AutoOkBtn != NULL)
        m_AutoOkBtn->setVisible(!m_bAuto);
    if (m_AutoCancleBtn != NULL)
        m_AutoCancleBtn->setVisible(m_bAuto);
 
    setGameStatus(cbGameStatus);
	switch (getGameStatus())
	{
	case GS_FREE:
		{
            m_GameState = enGameNormal;
			if (wDataSize!=sizeof(CMD_S_Ox6_StatusFree)) return false;

			if (m_BtnReadyPlay != NULL)	m_BtnReadyPlay->setVisible(true);
			if (m_BtnOpenCard != NULL) m_BtnOpenCard->setVisible(false);
			return true;
		}
	case GS_PLAYING:	// 叫庄状态
		{
             m_GameState = enGameSendCard;
			for (WORD i=0;i< Ox6_GAME_PLAYER;i++)
			{
				//视图位置
				m_wViewChairID[i]=g_GlobalUnits.SwitchViewChairID(i);
			}
			if (m_BtnReadyPlay != NULL) m_BtnReadyPlay->setVisible(false);
			if (m_BtnOpenCard != NULL) m_BtnOpenCard->setVisible(false);

			m_cbDynamicJoin = true;
			return true;
		}
	case GS_PLAYING+1:	// 下注状态
		{
             m_GameState = enGameSendCard;
			m_cbDynamicJoin = true;
			for (WORD i=0;i< Ox6_GAME_PLAYER;i++)
			{
				//视图位置
				m_wViewChairID[i]= g_GlobalUnits.SwitchViewChairID(i);
			}
			if (m_BtnReadyPlay != NULL)	m_BtnReadyPlay->setVisible(false);
			if (m_BtnOpenCard != NULL) m_BtnOpenCard->setVisible(false);

			if (wDataSize!=sizeof(CMD_S_Ox6_StatusScore)) return false;
			CMD_S_Ox6_StatusScore * pStatusScore=(CMD_S_Ox6_StatusScore *)pBuffer;
			m_lTurnMaxScore=pStatusScore->lTurnMaxScore;
			m_wBankerUser = pStatusScore->wBankerUser;
			//底住
			removeAllChildByTag(m_MyMoveOverCardTga);
			for (int i = 0; i < Ox6_GAME_PLAYER; i++)
			{
				removeChildByTag(m_ReadyTga[i]);
				SetPlayerCardData(0,0,0);
				removeAllChildByTag(m_PlayerCardTypeTga[i]);
				removeAllChildByTag(m_SendCardTga[i]);
				removeAllChildByTag(m_DisCardTga[i]);
				removeAllChildByTag(m_BetTga[0]);
			}

			if (m_BtnReadyPlay != NULL)	m_BtnReadyPlay->setVisible(false);
			if (m_BtnOpenCard != NULL) m_BtnOpenCard->setVisible(false);
			SetMyCardData(0,0);

			m_UpCardIndex = 0;
			m_LeftCardIndex1 = 0;
			m_LeftCardIndex2 = 0;
			m_RightCardIndex1 = 0;
			m_RightCardIndex2 = 0;
			m_DownCardIndex = 0;
			ZeroMemory(m_HandCardData,sizeof(m_HandCardData));
			m_MyHandIndex = 0;
			m_SendCardIndex = 0;
			ZeroMemory(m_MyHandCardData,sizeof(m_MyHandCardData));
			m_bHaveNN = false;
			return true;
		}
		return true;
	case GS_PLAYING+2:	// 游戏状态 
		{
			//效验数据
			if (wDataSize!=sizeof(CMD_S_Ox6_StatusPlay)) return false;
			CMD_S_Ox6_StatusPlay * pStatusPlay=(CMD_S_Ox6_StatusPlay *)pBuffer;
            
            m_GameState = enGameSendCard;
			m_cbDynamicJoin = true;
			for (WORD i=0;i< Ox6_GAME_PLAYER;i++)
			{
				//视图位置
				m_wViewChairID[i]= g_GlobalUnits.SwitchViewChairID(i);
			}
			if (m_BtnReadyPlay != NULL)	m_BtnReadyPlay->setVisible(false);
			if (m_BtnOpenCard != NULL) m_BtnOpenCard->setVisible(false);

			m_wBankerUser = pStatusPlay->wBankerUser;
			for (WORD i=0;i<Ox6_GAME_PLAYER;i++)
			{
				//视图位置
				m_wViewChairID[i]=g_GlobalUnits.SwitchViewChairID(i);
				if (!m_cbPlayStatus[i]) continue;
				//游戏信息
				Sprite* temp = Sprite::createWithSpriteFrameName("Goldback_normal.png");
				temp->setPosition(m_GoldPos[m_wViewChairID[i]]);
				temp->setTag(m_BetTga[m_wViewChairID[i]]);
				addChild(temp);
				char strcc[32]="";
				sprintf(strcc,"%lld ", m_lTurnMaxScore);
				Label *font=Label::createWithSystemFont(strcc,_GAME_FONT_NAME_1_,28);
				Color3B color;
				color.r = 255; 
				color.g = 253; 
				color.b = 103; 
				font->setColor(color); 
				font->setPosition(Vec2(70,28));
				temp->addChild(font);
				CopyMemory(m_HandCardData,pStatusPlay->byCardData,MAX_COUNT);
			}
			m_GameState = enGameSendCard;
			schedule(schedule_selector(Ox6GameViewLayer::SendCard), 0.15f);

			return true;
		}
	}	
	return true;
}

void Ox6GameViewLayer::SetMyCardData(BYTE cbCardData[], BYTE cbCardCount)
{
	removeAllChildByTag(m_MyMoveOverCardTga);
	if (cbCardCount == 0) return;
	for (int i = 0; i < cbCardCount; i++)
	{
		string _name = GetCardStringName(cbCardData[i]);
		Sprite* temp = Sprite::createWithSpriteFrameName(_name.c_str());
		if (i > 2)
            temp->setPosition(Vec2(_STANDARD_SCREEN_CENTER_.x-80+i*60,140));
		else
            temp->setPosition(Vec2(_STANDARD_SCREEN_CENTER_.x-120+i*60,140));
		temp->setTag(m_DisCardTga[3]);
		addChild(temp);
	}
}

void Ox6GameViewLayer::SetPlayerCardDataNone(WORD wchair, BYTE cbCardData[], BYTE cbCardCount)
{
	switch(wchair)
	{
	case 0:
		{
			removeAllChildByTag(m_SendCardTga[0]);
			if (cbCardCount == 0) return;
			for (int i = 0; i < 5; i++)
			{
				string _name = GetCardStringName(cbCardData[i]);
				Sprite* temp = Sprite::createWithSpriteFrameName(_name.c_str());
                temp->setScale(0.7f);
				temp->setPosition(Vec2(_STANDARD_SCREEN_CENTER_.x-80 + i*40, 900));
				temp->setTag(m_DisCardTga[0]);
				addChild(temp);
			}
			break;
		}
	case 1:
		{
			removeAllChildByTag(m_SendCardTga[1]);
			if (cbCardCount == 0) return;
		
			for (int i = 0; i < 5; i++)
			{
				string _name = GetCardStringName(cbCardData[i]);
				Sprite* temp = Sprite::createWithSpriteFrameName(_name.c_str());
                temp->setScale(0.7f);
				temp->setPosition(Vec2(460 + i*40, 740));
				temp->setTag(m_DisCardTga[1]);
				addChild(temp);
			}
			break;
		}
	case 2:
		{
			removeAllChildByTag(m_SendCardTga[2]);
			if (cbCardCount == 0) return;
			for (int i = 0; i < 5; i++)
			{
				string _name = GetCardStringName(cbCardData[i]);
				Sprite* temp = Sprite::createWithSpriteFrameName(_name.c_str());
                temp->setScale(0.7f);
				temp->setPosition(Vec2(460+i*40, 440));
				temp->setTag(m_DisCardTga[2]);
				addChild(temp);
			}
			break;
		}
	case 4:
		{
			removeAllChildByTag(m_SendCardTga[4]);
			if (cbCardCount == 0) return;
			for (int i = 0; i < 5; i++)
			{
				string _name = GetCardStringName(cbCardData[i]);
				Sprite* temp = Sprite::createWithSpriteFrameName(_name.c_str());
                temp->setScale(0.7f);
				temp->setPosition(Vec2(1300 + i*40, 440));
				temp->setTag(m_DisCardTga[4]);
				addChild(temp);
			}
			break;
		}
	case 5:
		{
			removeAllChildByTag(m_SendCardTga[5]);
			if (cbCardCount == 0) return;
			for (int i = 0; i < 5; i++)
			{
				string _name = GetCardStringName(cbCardData[i]);
				Sprite* temp = Sprite::createWithSpriteFrameName(_name.c_str());
                temp->setScale(0.7f);
				temp->setPosition(Vec2(1300 + i*40, 740));
				temp->setTag(m_DisCardTga[5]);
				addChild(temp);
			}
			break;
		}
	}
}

void Ox6GameViewLayer::SetPlayerCardData(WORD wchair, BYTE cbCardData[], BYTE cbCardCount)
{
	switch(wchair)
	{
	case 0:
		{
			removeAllChildByTag(m_SendCardTga[0]);
			if (cbCardCount == 0) return;
			//上面两张
			for (int i = 3; i < 5; i++)
			{
				string _name = GetCardStringName(cbCardData[i]);
				Sprite* temp = Sprite::createWithSpriteFrameName(_name.c_str());
                temp->setScale(0.7f);
				temp->setPosition(Vec2(_STANDARD_SCREEN_CENTER_.x-210+ i*60, 960));
				temp->setTag(m_DisCardTga[0]);
				addChild(temp);
			}
			//下面3张
			for (int i = 0; i < 3; i++)
			{
				string _name = GetCardStringName(cbCardData[i]);
				Sprite* temp = Sprite::createWithSpriteFrameName(_name.c_str());
                temp->setScale(0.7f);
				temp->setPosition(Vec2(_STANDARD_SCREEN_CENTER_.x-60+i*60, 900));
				temp->setTag(m_DisCardTga[0]);
				addChild(temp);
			}
			break;
		}
	case 1:
		{
			removeAllChildByTag(m_SendCardTga[1]);
			if (cbCardCount == 0) return;
			//上面两张
			for (int i = 3; i < 5; i++)
			{
				string _name = GetCardStringName(cbCardData[i]);
				Sprite* temp = Sprite::createWithSpriteFrameName(_name.c_str());
                temp->setScale(0.7f);
				temp->setPosition(Vec2(330+i*60, 800));
				temp->setTag(m_DisCardTga[1]);
				addChild(temp);
			}
			//下面3张
			for (int i = 0; i < 3; i++)
			{
				string _name = GetCardStringName(cbCardData[i]);
				Sprite* temp = Sprite::createWithSpriteFrameName(_name.c_str());
                temp->setScale(0.7f);
				temp->setPosition(Vec2(480+i*60, 740));
				temp->setTag(m_DisCardTga[1]);
				addChild(temp);
			}
			break;
		}
	case 2:
		{
			removeAllChildByTag(m_SendCardTga[2]);
			if (cbCardCount == 0) return;
			//上面两张
			for (int i = 3; i < 5; i++)
			{
				string _name = GetCardStringName(cbCardData[i]);
				Sprite* temp = Sprite::createWithSpriteFrameName(_name.c_str());
                temp->setScale(0.7f);
				temp->setPosition(Vec2(330+i*60, 500));
				temp->setTag(m_DisCardTga[2]);
				addChild(temp);
			}
			//下面3张
			for (int i = 0; i < 3; i++)
			{
				string _name = GetCardStringName(cbCardData[i]);
				Sprite* temp = Sprite::createWithSpriteFrameName(_name.c_str());
                temp->setScale(0.7f);
				temp->setPosition(Vec2(480+i*60, 440));
				temp->setTag(m_DisCardTga[2]);
				addChild(temp);
			}
			break;
		}
	case 4:
		{
			removeAllChildByTag(m_SendCardTga[4]);
			if (cbCardCount == 0) return;
			//上面两张
			for (int i = 3; i < 5; i++)
			{
				string _name = GetCardStringName(cbCardData[i]);
				Sprite* temp = Sprite::createWithSpriteFrameName(_name.c_str());
                temp->setScale(0.7f);
				temp->setPosition(Vec2(1170+i*60, 500));
				temp->setTag(m_DisCardTga[4]);
				addChild(temp);
			}
			//下面3张
			for (int i = 0; i < 3; i++)
			{
				string _name = GetCardStringName(cbCardData[i]);
				Sprite* temp = Sprite::createWithSpriteFrameName(_name.c_str());
                temp->setScale(0.7f);
				temp->setPosition(Vec2(1320+i*60, 440));
				temp->setTag(m_DisCardTga[4]);
				addChild(temp);
			}
			break;
		}
	case 5:
		{
			removeAllChildByTag(m_SendCardTga[5]);
			if (cbCardCount == 0) return;
			//上面两张
			for (int i = 3; i < 5; i++)
			{
				string _name = GetCardStringName(cbCardData[i]);
				Sprite* temp = Sprite::createWithSpriteFrameName(_name.c_str());
                temp->setScale(0.7f);
				temp->setPosition(Vec2(1170+i*60, 800));
				temp->setTag(m_DisCardTga[5]);
				addChild(temp);
			}
			//下面3张
			for (int i = 0; i < 3; i++)
			{
				string _name = GetCardStringName(cbCardData[i]);
				Sprite* temp = Sprite::createWithSpriteFrameName(_name.c_str());
                temp->setScale(0.7f);
				temp->setPosition(Vec2(1320 + i*60, 740));
				temp->setTag(m_DisCardTga[5]);
				addChild(temp);
			}
			break;
		}
	}
}

void Ox6GameViewLayer::SendGameStart()
{
	ClientSocketSink::sharedSocketSink()->SendData(MDM_GF_FRAME, SUB_GF_USER_READY);
	if (m_BtnReadyPlay != NULL) 	m_BtnReadyPlay->setVisible(false);
	if (m_BtnOpenCard != NULL) m_BtnOpenCard->setVisible(false);
	StopTime();
}

void Ox6GameViewLayer::SendOpenCard()
{
    m_BtnOpenCard->setVisible(false);
    
    BYTE bCardData[Ox6_MAXCOUNT];
    int proCount = 0;
    for (int n = 0; n < 5; n++)
    {
        if (m_MyHandCardData[n] == 0)
            return;
    }
    //扑克数据
    m_GameLogic.SortCardByLogic(bCardData, Ox6_MAXCOUNT);
    BYTE bCardType = m_GameLogic.GetPromptType(bCardData ,proCount, m_MyHandCardData, Ox6_MAXCOUNT);
    if(bCardType > 1)
    {
        SetMyCardData(bCardData,5);
		
        Sprite* temp;
        
        if (bCardType == Ox6GameLogic::ECardType::ECT_ZD)
        {
            temp = Sprite::createWithSpriteFrameName("niu_zd.png");
        }
        else if(bCardType == Ox6GameLogic::ECardType::ECT_SW)
        {
            temp = Sprite::createWithSpriteFrameName("niu_swnn.png");
        }
        else if(bCardType == Ox6GameLogic::ECardType::ECT_HL)
        {
            temp = Sprite::createWithSpriteFrameName("niu_hlnn.png");
        }
        else if(bCardType == Ox6GameLogic::ECardType::ECT_ST)
        {
            temp = Sprite::createWithSpriteFrameName("niu_stnn.png");
        }
        else if(bCardType == Ox6GameLogic::ECardType::ECT_WJ)
        {
            temp = Sprite::createWithSpriteFrameName("niu_wjnn.png");
        }
        else if(bCardType == Ox6GameLogic::ECardType::ECT_DYN)
        {
            temp = Sprite::createWithSpriteFrameName("niu_dwyn.png");
        }
        else if(bCardType == Ox6GameLogic::ECardType::ECT_XYN)
        {
            temp = Sprite::createWithSpriteFrameName("niu_xwyn.png");
        }
        else if(bCardType == Ox6GameLogic::ECardType::ECT_NN)
        {
            temp = Sprite::createWithSpriteFrameName("niuniu.png");
        }
        else if(bCardType == Ox6GameLogic::ECardType::ECT_DWNN || bCardType == Ox6GameLogic::ECardType::ECT_SWNN)
        {
            temp = Sprite::createWithSpriteFrameName("niu_dwrn.png");
        }
        else if(bCardType == Ox6GameLogic::ECardType::ECT_XWNN)
        {
            temp = Sprite::createWithSpriteFrameName("niu_xwrn.png");
        }
		else
        {
            int n = 0;
            if (bCardType >= Ox6GameLogic::ECardType::ECT_XN9)
                n = 9;
            else if (bCardType >= Ox6GameLogic::ECardType::ECT_XN8)
                n = 8;
            else if (bCardType >= Ox6GameLogic::ECardType::ECT_XN7)
                n = 7;
            else if (bCardType >= Ox6GameLogic::ECardType::ECT_XN6)
                n = 6;
            else if (bCardType >= Ox6GameLogic::ECardType::ECT_XN5)
                n = 5;
            else if (bCardType >= Ox6GameLogic::ECardType::ECT_XN4)
                n = 4;
            else if (bCardType >= Ox6GameLogic::ECardType::ECT_XN3)
                n = 3;
            else if (bCardType >= Ox6GameLogic::ECardType::ECT_XN2)
                n = 2;
            else
                n = 1;
             __String* ns=__String::createWithFormat("niu_%d.png",n);
            temp = Sprite::createWithSpriteFrameName(ns->getCString());
        }
		temp->setPosition(m_CardType[3]);
		temp->setTag(m_PlayerCardTypeTga[3]);
		addChild(temp);
	}
	else
	{
		Sprite* temp = Sprite::createWithSpriteFrameName("wuniu.png");
		temp->setPosition(m_CardType[3]);
		temp->setTag(m_PlayerCardTypeTga[3]);
		addChild(temp);
	}

	CMD_C_Ox6_OxCard OxCard;
	OxCard.nCardCount = MAX_COUNT;
	OxCard.wCurrentUser = GetMeChairID();
    ZeroMemory(OxCard.byCardData, sizeof(OxCard.byCardData));
    CopyMemory(OxCard.byCardData, m_HandCardData, sizeof(OxCard.byCardData));
	SendData(SUB_C_Ox6_OPEN_CARD,&OxCard,sizeof(OxCard));
}

bool Ox6GameViewLayer::OnSubCaijinInfo(const void * pBuffer, WORD wDataSize)
{
    //效验参数
    if (wDataSize < sizeof(CMD_GF_Lottery)) return false;
    PACKET_AIDE_DATA(pBuffer);
    
    LONGLONG totalScore = (LONGLONG)packet.read8Byte();
    char strc[32]="";
    memset(strc , 0 , sizeof(strc));
    Tools::AddComma(totalScore , strc);
    m_LotteryTotal->setString(strc);
    int count = packet.read4Byte();              //彩金玩家总量
    int PlayerCount = packet.read4Byte();        //彩金玩家数量
    
    BYTE* buffer = (((BYTE *)pBuffer)+16);
    for (int i = 0; i < PlayerCount; i++)
    {
        CMD_GF_LotteryPlayer* lPlayer = (((CMD_GF_LotteryPlayer *)buffer)+i);
       
        if (PlayerCount == 1)
            m_lotterplayer.insert(m_lotterplayer.begin(), *lPlayer);
        else
             m_lotterplayer.push_back(*lPlayer);
        
        if (m_lotterplayer.size() > 10)
        {
            m_lotterplayer.pop_back();
        }
    }
    
    if (PlayerCount > 0)
        DrawWinList();
    
    m_CaijinBackSpr->setVisible(true);
    return true;
}

bool Ox6GameViewLayer::OnSubPersonCaijin(const void * pBuffer, WORD wDataSize)
{
    //效验参数
    if (wDataSize!= sizeof(CMD_GF_LotteryWeek)) return false;
    CMD_GF_LotteryWeek * pLotteryWeek = (CMD_GF_LotteryWeek *)pBuffer;
    
    m_CaijinBackSpr->setVisible(true);
    m_lLotteryWeek = pLotteryWeek->Score;
    string str = StringUtils::toString(m_lLotteryWeek);
    m_LabelLottery->setString(str);
    return true;
}

bool Ox6GameViewLayer::OnBet( const void * pBuffer, WORD wDataSize )
{
	return true;
}

bool Ox6GameViewLayer::OnBetFail()
{
	//下注失败，您再试试吧
	AlertMessageLayer::createConfirm(this , "\u4e0b\u6ce8\u5931\u8d25\uff0c\u60a8\u518d\u8bd5\u8bd5\u5427");
	return true;
}

bool Ox6GameViewLayer::OnSubGameStart( const void * pBuffer, WORD wDataSize )
{
	m_cbDynamicJoin = false;
	m_GameState = enGameSendCard;
	SoundUtil::sharedEngine()->playEffect("GAME_START"); 
	removeAllChildByTag(m_MyMoveOverCardTga);
	for (int i = 0; i < Ox6_GAME_PLAYER; i++)
	{
		removeChildByTag(m_ReadyTga[i]);
		SetPlayerCardData(0,0,0);
		removeAllChildByTag(m_PlayerCardTypeTga[i]);
		removeAllChildByTag(m_SendCardTga[i]);
		removeAllChildByTag(m_DisCardTga[i]);
		removeAllChildByTag(m_BetTga[i]);
	}

	if (m_BtnReadyPlay != NULL) m_BtnReadyPlay->setVisible(false);
	if (m_BtnOpenCard != NULL) m_BtnOpenCard->setVisible(false);

	SetMyCardData(0,0);
	
	m_UpCardIndex = 0;
	m_LeftCardIndex1 = 0;
	m_LeftCardIndex2 = 0;
	m_RightCardIndex1 = 0;
	m_RightCardIndex2 = 0;
	m_DownCardIndex = 0;
	ZeroMemory(m_HandCardData,sizeof(m_HandCardData));
	m_MyHandIndex = 0;
	m_SendCardIndex = 0;
	ZeroMemory(m_MyHandCardData,sizeof(m_MyHandCardData));
	m_bHaveNN = false;

	//效验数据
	if (wDataSize!=sizeof(CMD_S_Ox6_GameStart)) return false;
	CMD_S_Ox6_GameStart * pGameStart=(CMD_S_Ox6_GameStart *)pBuffer;
	m_lTurnMaxScore = pGameStart->lMaxScore;
	m_lCellScore = pGameStart->lCellScore;
	m_wBankerUser=pGameStart->wBankerUser;
	
	//用户信息
	for (WORD i=0;i<Ox6_GAME_PLAYER;i++)
	{
		//视图位置
		m_wViewChairID[i]=g_GlobalUnits.SwitchViewChairID(i);
		//游戏信息
        const tagUserData* pUserData = GetUserData(i);
        if (pUserData == NULL)
            m_cbPlayStatus[i]=false;
        else
            m_cbPlayStatus[i]=true;
	}
	return true;
}

//游戏结束
bool Ox6GameViewLayer::OnSubGameEnd(const void * pBuffer, WORD wDataSize)
{
	//效验参数
	if (wDataSize!= sizeof(CMD_S_Ox6_GameEnd)) return false;
	CMD_S_Ox6_GameEnd * pGameEnd=(CMD_S_Ox6_GameEnd *)pBuffer;
	m_GameState = enGameEnd;
	StopTime();

	//清理数据
	m_BtnOpenCard->setVisible(false);
	for(WORD i=0;i<Ox6_GAME_PLAYER;i++)
	{
		if(m_OpenCardSpr[i] != NULL)
            m_OpenCardSpr[i]->setVisible(false);
		removeAllChildByTag(m_OpenCardTga[i]);
		removeAllChildByTag(m_BetTga[i]);		
	}
    
	//状态设置
	setGameStatus(GS_FREE);


	//显示牌型
    for (WORD i=0;i<Ox6_GAME_PLAYER;i++)
    {
        if (!m_cbPlayStatus[i]) continue;
        
        WORD wViewChairID=m_wViewChairID[i];
        if (wViewChairID == 3)
            continue;
        if (pGameEnd->cbCardData[i][0] == 0)
            continue;
        
        BYTE bCardData[Ox6_MAXCOUNT];
        int proCount = 0;

        //扑克数据
        m_GameLogic.GetPromptType(bCardData, proCount, pGameEnd->cbCardData[i], Ox6_MAXCOUNT);
        BYTE bCardType=m_GameLogic.GetCardType(bCardData, 5);
        
        //有牛
        if(bCardType > 1)
        {
            SetPlayerCardData(wViewChairID, bCardData, 5);
            Sprite* temp;
            
            if (bCardType == Ox6GameLogic::ECardType::ECT_ZD)
            {
                temp = Sprite::createWithSpriteFrameName("niu_zd.png");
            }
            else if(bCardType == Ox6GameLogic::ECardType::ECT_SW)
            {
                temp = Sprite::createWithSpriteFrameName("niu_swnn.png");
            }
            else if(bCardType == Ox6GameLogic::ECardType::ECT_HL)
            {
                temp = Sprite::createWithSpriteFrameName("niu_hlnn.png");
            }
            else if(bCardType == Ox6GameLogic::ECardType::ECT_ST)
            {
                temp = Sprite::createWithSpriteFrameName("niu_stnn.png");
            }
            else if(bCardType == Ox6GameLogic::ECardType::ECT_WJ)
            {
                temp = Sprite::createWithSpriteFrameName("niu_wjnn.png");
            }
            else if(bCardType == Ox6GameLogic::ECardType::ECT_DYN)
            {
                temp = Sprite::createWithSpriteFrameName("niu_dwyn.png");
            }
            else if(bCardType == Ox6GameLogic::ECardType::ECT_XYN)
            {
                temp = Sprite::createWithSpriteFrameName("niu_xwyn.png");
            }
            else if(bCardType == Ox6GameLogic::ECardType::ECT_NN)
            {
                temp = Sprite::createWithSpriteFrameName("niuniu.png");
            }
            else if(bCardType == Ox6GameLogic::ECardType::ECT_DWNN || bCardType == Ox6GameLogic::ECardType::ECT_SWNN)
            {
                temp = Sprite::createWithSpriteFrameName("niu_dwrn.png");
            }
            else if(bCardType == Ox6GameLogic::ECardType::ECT_XWNN)
            {
                temp = Sprite::createWithSpriteFrameName("niu_xwrn.png");
            }
            else
            {
                int n = 0;
                if (bCardType >= Ox6GameLogic::ECardType::ECT_XN9)
                    n = 9;
                else if (bCardType >= Ox6GameLogic::ECardType::ECT_XN8)
                    n = 8;
                else if (bCardType >= Ox6GameLogic::ECardType::ECT_XN7)
                    n = 7;
                else if (bCardType >= Ox6GameLogic::ECardType::ECT_XN6)
                    n = 6;
                else if (bCardType >= Ox6GameLogic::ECardType::ECT_XN5)
                    n = 5;
                else if (bCardType >= Ox6GameLogic::ECardType::ECT_XN4)
                    n = 4;
                else if (bCardType >= Ox6GameLogic::ECardType::ECT_XN3)
                    n = 3;
                else if (bCardType >= Ox6GameLogic::ECardType::ECT_XN2)
                    n = 2;
                else
                    n = 1;
                __String* ns=__String::createWithFormat("niu_%d.png",n);
                temp = Sprite::createWithSpriteFrameName(ns->getCString());
            }
        
            temp->setPosition(m_CardType[wViewChairID]);
            temp->setTag(m_PlayerCardTypeTga[wViewChairID]);
            addChild(temp);
        }
        else //没牛
        {
            SetPlayerCardDataNone(wViewChairID,bCardData,5);
			Sprite* temp = Sprite::createWithSpriteFrameName("wuniu.png");
			temp->setPosition(m_CardTypeNone[wViewChairID]);
			temp->setTag(m_PlayerCardTypeTga[wViewChairID]);
			addChild(temp);
		}
	}
	
	if (m_bHaveNN) //
	{
		SoundUtil::sharedEngine()->playEffect("GAME_OXOX");
	}
	else 
	{
		//播放声音
		int chair = ClientSocketSink::sharedSocketSink()->GetMeUserData()->wChairID;
		if (pGameEnd->lGameScore[chair]>0L)
		{
			SoundUtil::sharedEngine()->playEffect("GAME_WIN"); 
		}
		else
		{
			SoundUtil::sharedEngine()->playEffect("GAME_LOST"); 
		}
	}

	//用户信息
    LONGLONG myWinScore = 0;
	for (WORD i=0;i<Ox6_GAME_PLAYER;i++)
	{
		//视图位置
		m_wViewChairID[i]=g_GlobalUnits.SwitchViewChairID(i);
        if (!m_cbPlayStatus[i]) continue;

        if (m_wViewChairID[i] == 3)
            myWinScore = pGameEnd->lGameScore[i];
        
		Sprite* temp = Sprite::createWithSpriteFrameName("Goldback_normal.png");
		temp->setPosition(m_GoldPos[m_wViewChairID[i]]);
		temp->setTag(m_BetTga[m_wViewChairID[i]]);
		addChild(temp);
		char strc[32]="";
        if (pGameEnd->lGameScore[i] > 0L)
		{
			sprintf(strc,"+%lld", pGameEnd->lGameScore[i] + pGameEnd->lLottery[i]);
		}
		else
		{
			sprintf(strc,"%lld", pGameEnd->lGameScore[i] + pGameEnd->lLottery[i]);
		}
		Label *font=Label::createWithSystemFont(strc,_GAME_FONT_NAME_1_,40);
		Color3B color;
		color.r = 255; 
		color.g = 253; 
		color.b = 103; 
		font->setColor(color); 
		font->setPosition(Vec2(105,28));
		temp->addChild(font);
	}	

    auto userData = ClientSocketSink::sharedSocketSink()->GetMeUserData();
    auto myScore = userData->lScore + myWinScore;
    if (myScore > 0)
    {
        if (m_bAuto)
        {
            schedule(schedule_selector(Ox6GameViewLayer::AutoStartGame), 2);
        }
        else
        {
            m_BtnReadyPlay->setVisible(true);
            m_BtnOpenCard->setVisible(false);
            StartTime(15);
        }
    }


	return true;
}

void Ox6GameViewLayer::AutoStartGame(float dt)
{
	unschedule(schedule_selector(Ox6GameViewLayer::AutoStartGame));
	SendGameStart();
}

//用户摊牌
bool Ox6GameViewLayer::OnSubOpenCard(const void * pBuffer, WORD wDataSize)
{
	//效验数据
	if (wDataSize!=sizeof(CMD_S_Ox6_Open_Card)) return false;
	CMD_S_Ox6_Open_Card * pOpenCard=(CMD_S_Ox6_Open_Card *)pBuffer;
	m_GameState = enGameOpenCard;
	SoundUtil::sharedEngine()->playEffect("PASS_CARD"); 
	if (pOpenCard->wOpenCardUser == pOpenCard->wCurrentUser)
        return true;
    
    WORD wID=pOpenCard->wOpenCardUser;
	
	WORD wViewChairID=m_wViewChairID[wID];

	if (m_cbDynamicJoin) return true;
	Sprite* temp = Sprite::createWithSpriteFrameName("opencardok.png");
	temp->setPosition(m_OpenPos[wViewChairID]);
	temp->setLocalZOrder(100);
	temp->setTag(m_OpenCardTga[wViewChairID]);
	addChild(temp);

	return true;
}

void Ox6GameViewLayer::SendCard(float dt)
{
	//派发扑克
	if(m_SendCardIndex >= 5)
	{
		unschedule(schedule_selector(Ox6GameViewLayer::SendCard));
		if(!m_bAuto)
		{
			if(m_cbDynamicJoin == true)
			{
				m_BtnOpenCard->setVisible(false);
			}
			else 
			{
				m_BtnOpenCard->setVisible(true);
			}
		}
		StartTime(15);
		return;
	}
	SoundUtil::sharedEngine()->playEffect("SEND_CARD"); 
	for (WORD j= 0 ;j< Ox6_GAME_PLAYER;j++)
	{
		WORD w=j%Ox6_GAME_PLAYER;
		//该位置有用户游戏中
		if (m_cbPlayStatus[w]==true)
		{
			WORD wViewChairID=m_wViewChairID[w];
			//有牌
			if(wViewChairID == g_GlobalUnits.SwitchViewChairID(GetMeChairID()))
			{
				dispatchCards(wViewChairID,m_HandCardData[m_SendCardIndex]);
			}
            else
            {
                dispatchCards(wViewChairID, 0);
            }
		}
	}
	m_SendCardIndex++;
}

//发牌消息
bool Ox6GameViewLayer::OnSubSendCard(const void * pBuffer, WORD wDataSize)
{
	//效验数据
	if (wDataSize!=sizeof(CMD_S_Ox6_SendCard)) return false;
	CMD_S_Ox6_SendCard * pSendCard=(CMD_S_Ox6_SendCard *)pBuffer;
	CopyMemory(m_HandCardData,pSendCard->byCardData,sizeof(m_HandCardData));

	m_GameState = enGameSendCard;
	schedule(schedule_selector(Ox6GameViewLayer::SendCard), 0.15f);

	return true;
}

void Ox6GameViewLayer::dispatchCards(WORD chair, BYTE card) 
{ 
	switch (chair) 
	{ 
	case 0:  //正上方
		{ 
			if(m_UpCardIndex >= 5) return;
			MoveTo *leftMoveBy = MoveTo::create(0.2f, Vec2(_STANDARD_SCREEN_CENTER_.x-80 + m_UpCardIndex++*40, 900));
			Sprite* temp = Sprite::createWithSpriteFrameName("Ox6backCard.png");
            temp->setScale(0.7f);
			temp->setTag(m_SendCardTga[0]);
			addChild(temp);
			temp->setPosition(_STANDARD_SCREEN_CENTER_);
			temp->runAction(leftMoveBy);
			
			break; 
		} 
	case 1: //左上1
		{
			if(m_LeftCardIndex1 >= 5) return;
			MoveTo *leftMoveBy = MoveTo::create(0.2f, Vec2(460+m_LeftCardIndex1++*40, 740));
			Sprite* temp = Sprite::createWithSpriteFrameName("Ox6backCard.png");
            temp->setScale(0.7f);
			temp->setTag(m_SendCardTga[1]);
			addChild(temp);
			temp->setPosition(Vec2(_STANDARD_SCREEN_CENTER_.x,_STANDARD_SCREEN_CENTER_.y));
			temp->runAction(leftMoveBy);
			break; 
		} 
	case 2: //左上2
		{
			if(m_LeftCardIndex2 >= 5) return;
			MoveTo *leftMoveBy = MoveTo::create(0.2f, Vec2(460+m_LeftCardIndex2++*40, 440));
			Sprite* temp = Sprite::createWithSpriteFrameName("Ox6backCard.png");
            temp->setScale(0.7f);
			temp->setTag(m_SendCardTga[2]);
			addChild(temp);
			temp->setPosition(_STANDARD_SCREEN_CENTER_);
			temp->runAction(leftMoveBy);
			
			break; 
		} 
	case 3:
		{
			if(m_DownCardIndex >= 5) return;
			m_MyHandCardData[m_DownCardIndex] = card;
			MoveTo *leftMoveBy = MoveTo::create(0.2f, Vec2(_STANDARD_SCREEN_CENTER_.x-80 + m_DownCardIndex++*40, 140));
			ActionInstant *func = CallFunc::create(CC_CALLBACK_0(Ox6GameViewLayer::CardMoveCallback, this));
			Sprite* temp = Sprite::createWithSpriteFrameName("Ox6backCard.png");
            temp->setScale(0.7f);
            temp->setTag(m_SendCardTga[3]);
			addChild(temp);
			temp->setPosition(_STANDARD_SCREEN_CENTER_);
			temp->runAction(Sequence::create(leftMoveBy,func,NULL));
			break;
		}
	case 4: //右上1
		{
			if(m_RightCardIndex1 >= 5) return;
			MoveTo *leftMoveBy = MoveTo::create(0.2f, Vec2(1460-m_RightCardIndex1++*40, 440));
			Sprite* temp = Sprite::createWithSpriteFrameName("Ox6backCard.png");
            temp->setScale(0.7f);
            temp->setTag(m_SendCardTga[4]);
			addChild(temp);
			temp->setPosition(_STANDARD_SCREEN_CENTER_);
			temp->runAction(leftMoveBy);
			
			break; 
		} 
	case 5: //右上2
		{
			if(m_RightCardIndex2 >= 5) return;
			MoveTo *leftMoveBy = MoveTo::create(0.2f, Vec2(1460 - m_RightCardIndex2++*40, 740));
			Sprite* temp = Sprite::createWithSpriteFrameName("Ox6backCard.png");
            temp->setScale(0.7f);
            temp->setTag(m_SendCardTga[5]);
			addChild(temp);
			temp->setPosition(_STANDARD_SCREEN_CENTER_);
			temp->runAction(leftMoveBy);
			break; 
		} 
	default: 
		break; 
	} 
} 

void Ox6GameViewLayer::CardMoveCallback()
{
	//pSender->setVisible(false);
	removeChildByTag(m_SendCardTga[3]);
	string _name = GetCardStringName(m_MyHandCardData[m_MyHandIndex]);
	Sprite* temp = Sprite::createWithSpriteFrameName(_name.c_str());
	temp->setPosition(Vec2(_STANDARD_SCREEN_CENTER_.x- 120 + m_MyHandIndex*60,140));
	temp->setTag(m_MyMoveOverCardTga);		
	addChild(temp);
	m_MyHandIndex++;
	if(m_DownCardIndex >= 5)
	{
		if (m_bAuto)
		{
			schedule(schedule_selector(Ox6GameViewLayer::AutoOpenCard), 1);
		}
	}
}

void Ox6GameViewLayer::AutoOpenCard(float dt)
{
	unschedule(schedule_selector(Ox6GameViewLayer::AutoOpenCard));
	SendOpenCard();
}

string Ox6GameViewLayer::GetCardStringName(BYTE card)
{
	char tt[32];
	sprintf(tt,"%0x",card);
	BYTE _value = m_GameLogic.GetValue(card);
	BYTE _color = m_GameLogic.GetColor(card);
	string temp;
	if (_color == 0) temp = "fangkuai_";
	if (_color == 1) temp = "meihua_";
	if (_color == 2) temp = "hongtao_";
	if (_color == 3) temp = "heitao_";
	char _valstr[32];
	ZeroMemory(_valstr,32);
	if (_color == 4 && _value == 1) //小王
	{
		sprintf(_valstr,"xiaowang.png");
	}
	else if (_color == 4 && _value == 2)
	{
		sprintf(_valstr,"dawang.png");
	}
	else if (_value < 10)
	{
		sprintf(_valstr,"0%d.png",_value);
	}
	else 
	{
		sprintf(_valstr,"%d.png",_value);
	}

	temp+=_valstr;
	return temp;
}

void Ox6GameViewLayer::SendCaijinMsg()
{
    CMD_GF_RequestLotteWeek lotteryWeek;
    lotteryWeek.dwUserID = g_GlobalUnits.GetUserID();
    
    ClientSocketSink::sharedSocketSink()->SendData(MDM_GF_LOTTERY, SUB_GF_REQUEST_LOTTEWEEK, &lotteryWeek, sizeof(lotteryWeek));
    
    CMD_GF_RequestLottery lottery;
    lottery.dwUserID = g_GlobalUnits.GetUserID();
    ClientSocketSink::sharedSocketSink()->SendData(MDM_GF_LOTTERY, SUB_GF_REQUEST_LOTTERY, &lottery, sizeof(lottery));
}

void Ox6GameViewLayer::InitGame()
{
	//背景图
    m_BackSpr = Sprite::create("Common/common_gameBg.jpg");
    m_BackSpr->setPosition(_STANDARD_SCREEN_CENTER_IN_PIXELS_);
    addChild(m_BackSpr);
    
    Sprite *table = Sprite::createWithSpriteFrameName("game_back.png");
    table->setPosition(Vec2(_STANDARD_SCREEN_CENTER_.x- 20,_STANDARD_SCREEN_CENTER_.y+20));
    m_BackSpr->addChild(table);
    
	m_ClockSpr = Sprite::createWithSpriteFrameName("timeback.png");
	m_ClockSpr->setPosition(Vec2(_STANDARD_SCREEN_CENTER_.x+5 ,_STANDARD_SCREEN_CENTER_.y-50));
	addChild(m_ClockSpr);
    
    m_CaijinBackSpr = Sprite::createWithSpriteFrameName("bg_caijin.png");
    m_CaijinBackSpr->setPosition(Vec2(1500, 950));
    addChild(m_CaijinBackSpr);
    m_CaijinBackSpr->setVisible(false);
    
    m_CaijinInfoBg = Sprite::createWithSpriteFrameName("tip_contect.png");
    m_CaijinInfoBg->setPosition(_STANDARD_SCREEN_CENTER_);
    addChild(m_CaijinInfoBg,999);
    m_CaijinInfoBg->setVisible(false);
    
    m_CaijinRewardBG = Sprite::createWithSpriteFrameName("bg_winbg.png");
    m_CaijinRewardBG->setPosition(_STANDARD_SCREEN_CENTER_);
    addChild(m_CaijinRewardBG,999);
    m_CaijinRewardBG->setVisible(false);
    
    m_listView = ui::ListView::create();
    m_listView->setContentSize(Size(640, 360));
    m_listView->setDirection(ui::ScrollView::Direction::VERTICAL);
    m_listView->setPosition(Vec2(60,60));
    m_CaijinRewardBG->addChild(m_listView);
    
    m_LabelLottery = Label::createWithSystemFont("0", _GAME_FONT_NAME_1_, 40);
    m_LabelLottery->setAnchorPoint(Vec2(0, 0.5));
    m_LabelLottery->setPosition(Vec2(240, 130));
    m_CaijinBackSpr->addChild(m_LabelLottery);
    
    m_LotteryTotal = LabelAtlas::create("0", "Common/xiazhushuzi.png", 34, 46, '+');
    m_LotteryTotal->setAnchorPoint(Vec2(0.5, 0));
    m_LotteryTotal->setPosition(Vec2(235, 155));
    m_CaijinBackSpr->addChild(m_LotteryTotal);
    
	//tga
	for (int i = 0; i < 6; i++)
	{
		m_ReadyTga[i] = 100+i;
		m_HeadTga[i]  = 110+i;
		m_GlodTga[i]  = 120+i;
		m_NickNameTga[i] = 130+i;
		m_PlayerCardTypeTga[i] = 140+i;
		m_BetTga[i] = 150+i;
		m_SendCardTga[i] = 160+i;
		m_DisCardTga[i] = 170+i;
		m_OpenCardTga[i] = 180+i;
	}
	m_MyMoveOverCardTga = 99;

	m_HeadPos[0] = Vec2(480,960);
	m_HeadPos[1] = Vec2(180,740);
	m_HeadPos[2] = Vec2(180,440);
	m_HeadPos[3] = Vec2(480,120);
	m_HeadPos[4] = Vec2(1740,440);
	m_HeadPos[5] = Vec2(1740,740);

	m_ReadyPos[0] = Vec2(780,960);
	m_ReadyPos[1] = Vec2(250,640);
	m_ReadyPos[2] = Vec2(250,340);
	m_ReadyPos[3] = Vec2(700,150);
	m_ReadyPos[4] = Vec2(1670,340);
	m_ReadyPos[5] = Vec2(1670,640);

	m_GoldPos[0] = Vec2(480,820);
	m_GoldPos[1] = Vec2(180,880);
	m_GoldPos[2] = Vec2(180,580);
	m_GoldPos[3] = Vec2(480,260);
	m_GoldPos[4] = Vec2(1740,580);
	m_GoldPos[5] = Vec2(1740,880);

	m_OpenPos[0] = Vec2(960,900);
	m_OpenPos[1] = Vec2(540,740);
	m_OpenPos[2] = Vec2(540,440);
	m_OpenPos[3] = Vec2(960,140);
	m_OpenPos[4] = Vec2(1380,440);
	m_OpenPos[5] = Vec2(1380,740);
	
	m_CardType[0] = Vec2(960,870);
	m_CardType[1] = Vec2(540,710);
	m_CardType[2] = Vec2(540,410);
	m_CardType[3] = Vec2(960,110);
	m_CardType[4] = Vec2(1380,410);
	m_CardType[5] = Vec2(1380,710);

    m_CardTypeNone[0] = Vec2(960,870);
    m_CardTypeNone[1] = Vec2(540,710);
    m_CardTypeNone[2] = Vec2(540,410);
    m_CardTypeNone[3] = Vec2(960,110);
    m_CardTypeNone[4] = Vec2(1380,410);
    m_CardTypeNone[5] = Vec2(1380,710);
}

void Ox6GameViewLayer::AddButton()
{
	m_BtnReadyPlay = CreateButton("game_Start" ,Vec2(960,360),Btn_Ready);
	addChild(m_BtnReadyPlay , 0 , Btn_Ready);
	m_BtnReadyPlay->setVisible(false);

	m_BtnBackToLobby = CreateButton("Returnback" ,Vec2(90,1000),Btn_BackToLobby);
	addChild(m_BtnBackToLobby , 0 , Btn_BackToLobby);
	m_BtnBackToLobby->setVisible(true);

	m_BtnOpenCard = CreateButton("OpenCard" ,Vec2(960,360),Btn_OpenCard);
	addChild(m_BtnOpenCard , 0 , Btn_OpenCard);
	m_BtnOpenCard->setVisible(false);
	
	m_BtnSeting = CreateButton("seting" ,Vec2(1830,1000),Btn_Seting);
	addChild(m_BtnSeting , 0 , Btn_Seting);
	m_BtnSeting->setVisible(true);
	
	m_AutoOkBtn = CreateButton("AutoOk" ,Vec2(1800,80),Btn_AutoOk);
	addChild(m_AutoOkBtn , 0 , Btn_AutoOk);
	m_AutoOkBtn->setVisible(false);
	
	m_AutoCancleBtn = CreateButton("AutoCancle" ,Vec2(1800,80),Btn_AutoCancle);
	addChild(m_AutoCancleBtn , 0 , Btn_AutoCancle);
	m_AutoCancleBtn->setVisible(false);
    
    m_BtnReward =CreateButton("btn_zjhjwj" ,Vec2(250,60),Btn_CaijinReward);
    m_CaijinBackSpr->addChild(m_BtnReward , 0 , Btn_CaijinReward);
    
    m_BtnInfo =CreateButton("btn_xq" ,Vec2(360,250),Btn_CaijinInfo);
    m_CaijinBackSpr->addChild(m_BtnInfo , 0 , Btn_CaijinInfo);
}

Menu* Ox6GameViewLayer::CreateButton(std::string szBtName ,const Vec2 &p , int tag )
{
	return Tools::Button(StrToChar(szBtName+"_normal.png") , StrToChar(szBtName+"_click.png") , p, this , menu_selector(Ox6GameViewLayer::callbackBt) , tag);
}

void Ox6GameViewLayer::DeletePlayer(tagUserData * pUserDatac)
{
	const tagUserData* pUserData = ClientSocketSink::sharedSocketSink()->m_UserManager.SearchUserByUserID(pUserDatac->dwUserID);
	int chair = g_GlobalUnits.SwitchViewChairID(pUserData->wChairID);
	if (pUserData != NULL)
	{
		removeAllChildByTag(m_HeadTga[chair]);
		removeAllChildByTag(m_GlodTga[chair]);
		removeAllChildByTag(m_NickNameTga[chair]);
		removeAllChildByTag(m_ReadyTga[chair]);
		removeAllChildByTag(m_SendCardTga[chair]);
	}
}

void Ox6GameViewLayer::DrawWinList()
{
    SoundUtil::sharedEngine()->playEffect("hello");
    if (m_lotterplayer.size() <= 0)
        return;
    
    m_listView->removeAllItems();
    vector<CMD_GF_LotteryPlayer>::iterator it = m_lotterplayer.begin();
    
    do
    {
        ui::Layout* custom_item = ui::Layout::create();
        custom_item->setContentSize(Size(640, 60));
        
        //昵称
        std::string szTempTitle = gbk_utf8(it->PlayerName);
        Label* temp = Label::createWithSystemFont(szTempTitle.c_str(),_GAME_FONT_NAME_1_,35);
        temp->setAnchorPoint(Vec2(0,0.5f));
        
        if (!strcmp(GetMeUserData()->szNickName, it->PlayerName))
            temp->setColor(Color3B::RED);
        else
            temp->setColor(Color3B::WHITE);
        temp->setPosition(Vec2(5, 30));
        custom_item->addChild(temp);
        
        string cardType = "";
        switch (it->Type) {
            case Ox6GameLogic::ELotteryType::ELT_WJ:
                cardType = "五金";
                break;
            case Ox6GameLogic::ELotteryType::ELT_ZD:
                cardType = "炸弹";
                break;
            case Ox6GameLogic::ELotteryType::ELT_HL:
                cardType = "葫芦";
                break;
            case Ox6GameLogic::ELotteryType::ELT_ST:
                cardType = "三条";
                break;
            case Ox6GameLogic::ELotteryType::ELT_SW:
                cardType = "双王";
                break;
            default:
                break;
        }
        
        temp = Label::createWithSystemFont(cardType, _GAME_FONT_NAME_2_, 35);
        temp->setColor(Color3B::WHITE);
        temp->setPosition(Vec2(300, 30));
        custom_item->addChild(temp);
        
        char _score[32];
        sprintf(_score,"%lld", it->RewardScore);
        temp = Label::createWithSystemFont(_score,_GAME_FONT_NAME_1_,35);
        temp->setColor(Color3B::WHITE);
        temp->setPosition(Vec2(530, 30));
        custom_item->addChild(temp);
        m_listView->pushBackCustomItem(custom_item);
        
         it ++;
    }
    while (it != m_lotterplayer.end());
}

void Ox6GameViewLayer::ChangePlayerInfo(tagUserData * pUserDatac)
{
	const tagUserData* pUserData = ClientSocketSink::sharedSocketSink()->m_UserManager.SearchUserByUserID(pUserDatac->dwUserID);
	int chair = g_GlobalUnits.SwitchViewChairID(pUserData->wChairID);
	if (pUserData != NULL)
	{
		//头像
		removeAllChildByTag(m_HeadTga[chair]);
		Sprite* mHead;
		if(chair == 3)mHead = Sprite::createWithSpriteFrameName("head1.png");
		else mHead = Sprite::createWithSpriteFrameName("head2.png");
		mHead->setPosition(m_HeadPos[chair]);
		mHead->setTag(m_HeadTga[chair]);
		addChild(mHead);

		//金币
		removeAllChildByTag(m_GlodTga[chair]);
		char strc[32]="";
		LONGLONG lMon = pUserData->lScore;
		memset(strc , 0 , sizeof(strc));
		Tools::AddComma(lMon , strc);
		Label* mGoldFont;
		if(chair == 3)mGoldFont = Label::createWithSystemFont(strc,_GAME_FONT_NAME_1_,30);
		else mGoldFont = Label::createWithSystemFont(strc,_GAME_FONT_NAME_1_,20); 
		mGoldFont->setTag(m_GlodTga[chair]);
		addChild(mGoldFont);
		Color3B colorc; colorc.r = 252; colorc.g = 255; colorc.b = 0;
		mGoldFont->setColor(colorc); 
		if(chair == 3)mGoldFont->setPosition(Vec2(m_HeadPos[chair].x+420, m_HeadPos[chair].y-56));
		else mGoldFont->setPosition(Vec2(m_HeadPos[chair].x, m_HeadPos[chair].y-100));
		
		//昵称
		removeAllChildByTag(m_NickNameTga[chair]);
		Label* mNickfont;
		mNickfont = Label::createWithSystemFont(gbk_utf8(pUserData->szNickName), _GAME_FONT_NAME_1_,30);
		mNickfont->setTag(m_NickNameTga[chair]);
		addChild(mNickfont);
		Color3B color;
		if(chair == 3){ color.r = 255; color.g = 255; color.b = 255; }
		else {color.r = 252; color.g = 255; color.b = 0; }
		mNickfont->setColor(color); 
		if(chair == 3)mNickfont->setPosition(Vec2(m_HeadPos[chair].x+200, m_HeadPos[chair].y-50));
		else mNickfont->setPosition(Vec2(m_HeadPos[chair].x, m_HeadPos[chair].y-75));

		//准备
		removeAllChildByTag(m_ReadyTga[chair]);
		Sprite* mReaderSpr = Sprite::createWithSpriteFrameName("game_ready.png");
		mReaderSpr->setPosition(m_ReadyPos[chair]);
		mReaderSpr->setTag(m_ReadyTga[chair]);
		addChild(mReaderSpr);

		if (pUserData->cbUserStatus == US_READY)
		{
			getChildByTag(m_ReadyTga[chair])->setVisible(true);
		}
		else getChildByTag(m_ReadyTga[chair])->setVisible(false);
	}

}


void Ox6GameViewLayer::AddPlayerInfo()
{
	for (int i = 0; i < Ox6_GAME_PLAYER; i++)
	{
		if (ClientSocketSink::sharedSocketSink()->GetMeUserData() == NULL)
		{
			return;
		}
		int wtable = ClientSocketSink::sharedSocketSink()->GetMeUserData()->wTableID;
		const tagUserData* pUserData = ClientSocketSink::sharedSocketSink()->m_UserManager.GetUserData(wtable,i);
		int chair = g_GlobalUnits.SwitchViewChairID(i);
		if (pUserData != NULL)
		{
			//头像
			removeAllChildByTag(m_HeadTga[chair]);
            Sprite* playerInfoBg = Sprite::createWithSpriteFrameName("bg_player_info.png");
            playerInfoBg->setPosition(m_HeadPos[chair]);
            addChild(playerInfoBg, 0, m_HeadTga[chair]);
            
            Sprite* headBg = Sprite::createWithSpriteFrameName("bg_head_img.png");
            headBg->setPosition(Vec2(70, 73));
            playerInfoBg->addChild(headBg);
            
            string heads = g_GlobalUnits.getFace(pUserData->wGender, pUserData->lScore);
            Sprite* mHead = Sprite::createWithSpriteFrameName(heads);
            mHead->setPosition(Vec2(70, 73));
            mHead->setScale(0.9f);
            playerInfoBg->addChild(mHead);
            
            //金币
            removeAllChildByTag(m_GlodTga[chair]);
            char strc[32]="";
            LONGLONG lMon = pUserData->lScore;
            memset(strc , 0 , sizeof(strc));
            Tools::AddComma(lMon , strc);
            Label* mGoldFont;
            mGoldFont = Label::createWithSystemFont(strc,_GAME_FONT_NAME_1_,30);
            mGoldFont->setTag(m_GlodTga[chair]);
            playerInfoBg->addChild(mGoldFont);
            mGoldFont->setColor(Color3B::YELLOW);
            mGoldFont->setAnchorPoint(Vec2(0, 0.5f));
            mGoldFont->setPosition(Vec2(140, 90));
            
            //昵称
            removeAllChildByTag(m_NickNameTga[chair]);
            Label* mNickfont;
            mNickfont = Label::createWithSystemFont(gbk_utf8(pUserData->szNickName), _GAME_FONT_NAME_1_,30);
            mNickfont->setTag(m_NickNameTga[chair]);
            playerInfoBg->addChild(mNickfont);
            mNickfont->setAnchorPoint(Vec2(0,0.5f));
            mNickfont->setPosition(Vec2(140, 40));
            
			//准备
			removeAllChildByTag(m_ReadyTga[chair]);
			Sprite* mReaderSpr = Sprite::createWithSpriteFrameName("game_ready.png");
			mReaderSpr->setPosition(m_ReadyPos[chair]);
			mReaderSpr->setTag(m_ReadyTga[chair]);
			addChild(mReaderSpr);

			if (pUserData->cbUserStatus == US_READY)
			{
                removeAllChildByTag(m_SendCardTga[chair]);
				getChildByTag(m_ReadyTga[chair])->setVisible(true);
			}
			else getChildByTag(m_ReadyTga[chair])->setVisible(false);
		}
		else
		{
            if (chair == 3 && !g_GlobalUnits.m_bLeaveGameByServer)
            {
                backLoginView(nullptr);
                return;
            }
			removeAllChildByTag(m_HeadTga[chair]);
			removeAllChildByTag(m_GlodTga[chair]);
			removeAllChildByTag(m_NickNameTga[chair]);
			removeAllChildByTag(m_ReadyTga[chair]);
			removeAllChildByTag(m_SendCardTga[chair]);
		}
	}
}

string Ox6GameViewLayer::AddCommaToNum(LONG Num)
{
	char _str[256];
	sprintf(_str,"%ld", Num);
	string _string = _str;
	auto step = _string.length()/3;
	for (int i = 1; i <= step; i++)
	{
		_string.insert(_string.length()-(i-1)-(i*3), ",");
	}
	return _string;
}

void Ox6GameViewLayer::SendReqTaskConfig()
{
	CMD_C_GP_GetTask GetTask;
	ZeroMemory(&GetTask , sizeof(GetTask));
	GetTask.dwUserID = g_GlobalUnits.GetGolbalUserData().dwUserID;
	GetTask.wKindID = Ox6_KIND_ID;
	SendMobileData(SUB_C_GP_GETTASK , &GetTask , sizeof(GetTask));
}

void Ox6GameViewLayer::SendReqTaskStatus()
{
	CMD_C_GP_GetTaskStatus GetTask;
	ZeroMemory(&GetTask , sizeof(GetTask));
	GetTask.dwUserID = g_GlobalUnits.GetGolbalUserData().dwUserID;
	GetTask.wKindID = Ox6_KIND_ID;
	SendMobileData(SUB_C_GP_TAST_STATUS , &GetTask , sizeof(GetTask));
}

void Ox6GameViewLayer::SendReqTransProp()
{
	CMD_C_GP_GetTransProps GetTransProps;
	ZeroMemory(&GetTransProps ,sizeof(GetTransProps));
	GetTransProps.wKindID = Ox6_KIND_ID;
	SendMobileData(SUB_C_GP_GETTRANSPROP , &GetTransProps , sizeof(GetTransProps));
}

void Ox6GameViewLayer::SendSendProp( DWORD dwRcvUserID ,WORD wPropID , WORD wAmount , LONG lPayscore )
{
	CMD_C_GP_SendProp oSendProp;
	ZeroMemory(&oSendProp ,sizeof(oSendProp));
	oSendProp.dwUserID = g_GlobalUnits.GetUserID();
	oSendProp.dwRcvUserID = dwRcvUserID;
	oSendProp.wKindID = Ox6_KIND_ID;
	oSendProp.wPropID = wPropID;
	oSendProp.lAmount = wAmount;
	oSendProp.lPayScore = lPayscore;
	oSendProp.cbPlatform = GAME_PLATFORM;
	SendMobileData(SUB_C_GP_SENDPROP , &oSendProp , sizeof(oSendProp));
}

void Ox6GameViewLayer::SendReqSendRecord()
{
	CMD_C_GP_GetSendRecord GetSendRecord;
	ZeroMemory(&GetSendRecord ,sizeof(GetSendRecord));
	GetSendRecord.dwUserID = g_GlobalUnits.GetUserID();
	GetSendRecord.wKindID = Ox6_KIND_ID;
	GetSendRecord.wItemCount = SENDRECORD_COUNT;
	GetSendRecord.wPageIndex = 1;
	SendMobileData(SUB_C_GP_SENDRECORD , &GetSendRecord ,sizeof(GetSendRecord));
}

void Ox6GameViewLayer::callbackBt( Ref *pSender )
{
	Node *pNode = (Node *)pSender;
	switch (pNode->getTag())
	{
	case Btn_GetScoreBtn:
		{	
			GetScoreForBank();
			break;
		}
	case Btn_Ready:	//准备按钮
		{
			SendGameStart();
			break;
		}
	case Btn_BackToLobby: // 返回大厅按钮
		{
			SoundUtil::sharedEngine()->playEffect("buttonMusic");
			BackToLobby();
			break;
		}
	case Btn_OpenCard:	//开牌按钮
		{
			SendOpenCard();
			break;
		}
	case Btn_Seting: //设置
		{
			SoundUtil::sharedEngine()->playEffect("buttonMusic");
			GameLayerMove::sharedGameLayerMoveSink()->OpenSeting();
			break;
		}
	case Btn_AutoOk:	//托管
		{
			m_bAuto = true;
			m_AutoOkBtn->setVisible(false);
			m_AutoCancleBtn->setVisible(true);
            
            if (m_GameState == enGameNormal || m_GameState == enGameEnd)
            {
                if (m_BtnReadyPlay != NULL && m_BtnReadyPlay->isVisible())
                    SendGameStart();
            }
            else if (m_GameState == enGameSendCard || m_GameState == enGameOpenCard)
            {
                if (m_BtnOpenCard != nullptr && m_BtnOpenCard->isVisible())
                    SendOpenCard();
            }
            break;
		}		
	case Btn_AutoCancle:	//取消托管
		{
			m_bAuto = false;
			m_AutoOkBtn->setVisible(true);
			m_AutoCancleBtn->setVisible(false);
			break;
		}
    case Btn_CaijinReward:
        {
            m_CaijinRewardBG->setVisible(!(m_CaijinRewardBG->isVisible()));
            break;
        }
    case Btn_CaijinInfo:
        {
            bool isVisible = m_CaijinInfoBg->isVisible();
            m_CaijinInfoBg->setVisible(!isVisible);
            break;
        }
	case TAG_BTN_MENU:
		{
			break;
		}
	case TAG_BTN_HELP:
		{
			break;
		}
	case TAG_BT_ADDSCORE1:
		{
			break;
		}
	}
}

void Ox6GameViewLayer::backLoginView( Ref *pSender )
{
	IGameView::backLoginView(pSender);	
}

void Ox6GameViewLayer::GameEnd()
{
}

void Ox6GameViewLayer::ShowAddScoreBtn( bool bShow/*=true*/ )
{
	m_pAddScoreBT->setEnabled(bShow);
	removeAllChildByTag(TAG_BT_ANI_ADDSCORE);
}

void Ox6GameViewLayer::UpdateDrawUserScore()
{
}

void Ox6GameViewLayer::WStrToUTF81(std::string& dest, const wstring& src)
{
	dest.clear();
	for (size_t i = 0; i < src.size(); i++){
		wchar_t w = src[i];
		if (w <= 0x7f)
			dest.push_back((char)w);
		else if (w <= 0x7ff){
			dest.push_back(0xc0 | ((w >> 6)& 0x1f));
			dest.push_back(0x80| (w & 0x3f));
		}
		else if (w <= 0xffff){
			dest.push_back(0xe0 | ((w >> 12)& 0x0f));
			dest.push_back(0x80| ((w >> 6) & 0x3f));
			dest.push_back(0x80| (w & 0x3f));
		}
		else if (sizeof(wchar_t) > 2 && w <= 0x10ffff){
			dest.push_back(0xf0 | ((w >> 18)& 0x07)); // wchar_t 4-bytes situation
			dest.push_back(0x80| ((w >> 12) & 0x3f));
			dest.push_back(0x80| ((w >> 6) & 0x3f));
			dest.push_back(0x80| (w & 0x3f));
		}
		else
			dest.push_back('?');
	}
}
std::string Ox6GameViewLayer::WStrToUTF81(const std::wstring& str)
{
	std::string result;
	WStrToUTF81(result, str);
	return result;
}

wstring Ox6GameViewLayer::s2ws1(const string& s)
{
	setlocale(LC_ALL, "chs"); 
	const char* _Source = s.c_str();
	size_t _Dsize = s.size() + 1;
	wchar_t *_Dest = new wchar_t[_Dsize];
	wmemset(_Dest, 0, _Dsize);
	mbstowcs(_Dest,_Source,_Dsize);
	wstring result = _Dest;
	delete []_Dest;
	setlocale(LC_ALL, "C");
	return result;
}

void Ox6GameViewLayer::OnQuit()
{
	m_pGameScene->removeChild(this);
}
