#include <stdlib.h>
#include "Ox6GameLogic.h"


const BYTE Ox6GameLogic::mCardData[CARD_FULL] =
{
    0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08,0x09,0x0A,0x0B,0x0C,0x0D,
    0x11,0x12,0x13,0x14,0x15,0x16,0x17,0x18,0x19,0x1A,0x1B,0x1C,0x1D,
    0x21,0x22,0x23,0x24,0x25,0x26,0x27,0x28,0x29,0x2A,0x2B,0x2C,0x2D,
    0x31,0x32,0x33,0x34,0x35,0x36,0x37,0x38,0x39,0x3A,0x3B,0x3C,0x3D,
    0x41,0x42,
};

Ox6GameLogic::Ox6GameLogic()
{
}

Ox6GameLogic::~Ox6GameLogic()
{
}

void Ox6GameLogic::ShuffleCard(BYTE CardData[], const int Count)
{
    BYTE Temp[CARD_FULL] = { 0 };
    
    memcpy(Temp, mCardData, sizeof(mCardData[0]) * CARD_FULL);
    
    BYTE Index = 0, Num = 0;
    for (BYTE i = 0; i < Count; ++i)
    {
        Index = rand() % (CARD_FULL - Num);
        CardData[i] = Temp[Index];
        Temp[Index] = Temp[CARD_FULL - Num - 1];
        Num++;
    }
}

//return (L>R);
int Ox6GameLogic::CompareCard(const BYTE LCardData[], const int LCount, const BYTE RCardData[], const int RCount)
{
    if (LCount != HAND_CARD)
        return -1;
    if (RCount != HAND_CARD)
        return 1;
    
    int Result = 0;
    BYTE LType = GetCardType(LCardData, LCount);
    BYTE RType = GetCardType(RCardData, RCount);
    
    Result = LType - RType;
    if (Result == 0)
    {
        BYTE LTempData[HAND_CARD] = { 0 };
        BYTE RTempData[HAND_CARD] = { 0 };
        
        memcpy(LTempData, LCardData, LCount);
        SortCardByLogic(LTempData, LCount);
        
        memcpy(RTempData, RCardData, RCount);
        SortCardByLogic(RTempData, RCount);
        
        Result = GetLogic(LTempData[4]) - GetLogic(RTempData[4]);
        if (Result == 0)
            Result = GetColor(LTempData[4]) - GetColor(RTempData[4]);
    }
    
    return Result;
}

BYTE Ox6GameLogic::GetCardType(const BYTE CardData[], const int Count)
{
    if (Count != HAND_CARD || CardData == NULL)
        return ECT_NULL;
    
    BYTE PromptData[HAND_CARD] = { 0 };
    BYTE Result = AnalysisCard(CardData, Count, PromptData);
    
    return Result;
}

//FIXME:
BYTE Ox6GameLogic::GetPromptType(BYTE PromptData[], int& PromptCount, const BYTE SrcData[], const int SrcCount)
{
    if (SrcCount != HAND_CARD || SrcData == NULL)
        return ECT_NULL;
    if (PromptData == NULL)
        return ECT_NULL;
    
    PromptCount = 5;
    BYTE Result = AnalysisCard(SrcData, SrcCount, PromptData);
    
    return Result;
}

BYTE Ox6GameLogic::AnalysisCard(const BYTE CardData[], const int Count, BYTE PromptData[])
{
    BYTE WCount = 0, WType = 0;
    BYTE Sum = 0, OtherSum = 0, TestSum = 0;
    BYTE CardLogic[HAND_CARD] = { 0 };
    BYTE GameLogic[HAND_CARD] = { 0 };
    
    for (BYTE i = 0; i < Count; ++i)
    {
        if (CardData[i] == 0x41 || CardData[i] == 0x42)
        {
            WType = GetValue(CardData[i]);
            WCount++;
        }
        CardLogic[i] = GetLogic(CardData[i]);
        GameLogic[i] = GetGameLogic(CardData[i]);
        PromptData[i] = CardData[i];
    }
    
    SortCard(CardLogic, Count);
    if (CardLogic[0] > 10)
    {
        if (WCount == 2 && CardLogic[0] == CardLogic[2])
        {
            return ECT_SW;
        }
        else if (WCount == 0)
        {
            if (CardLogic[0] == CardLogic[3] || CardLogic[1] == CardLogic[4])
                return ECT_ZD;
            else if (CardLogic[0] == CardLogic[2])
                return (CardLogic[3] == CardLogic[4]) ? ECT_HL : ECT_ST;
            else if (CardLogic[1] == CardLogic[3])
                return ECT_ST;
            else if (CardLogic[2] == CardLogic[4])
                return (CardLogic[0] == CardLogic[1]) ? ECT_HL : ECT_ST;
            
            return ECT_WJ;
        }
    }
    
    if (WCount == 0)
    {
        for (BYTE i = 0; i < Count - 2; ++i)
        {
            Sum += GameLogic[i];
            for (BYTE j = i + 1; j < Count; ++j)
            {
                Sum += GameLogic[j];
                for (BYTE k = j + 1; k < Count; ++k)
                {
                    Sum += GameLogic[k];
                    if (Sum % 10 == 0)
                    {
                        OtherSum = 0;
                        PromptData[0] = CardData[i];
                        PromptData[1] = CardData[j];
                        PromptData[2] = CardData[k];
                        for (BYTE m = 0, n = 3; m < Count; ++m)
                        {
                            if (m == i || m == j || m == k)
                                continue;
                            
                            OtherSum += GameLogic[m];
                            PromptData[n++] = CardData[m];
                        }
                        
                        OtherSum %= 10;
                        return ((OtherSum == 0) ? ECT_NN : (ECT_WN + 3 * OtherSum));
                    }
                    
                    Sum -= GameLogic[k];
                }
                
                Sum -= GameLogic[j];
            }
            
            Sum = 0;
        }
        
        return ECT_WN;
    }
    else if (WCount == 1)
    {
        BYTE Left = 0;
        BYTE MaxIndex[3] = { 0 };
        for (BYTE i = 0; i < Count - 1; ++i)
        {
            if (CardData[i] == 0x41 || CardData[i] == 0x42)
                continue;
            
            Sum += GameLogic[i];
            for (BYTE j = i + 1; j < Count; ++j)
            {
                if (CardData[j] == 0x41 || CardData[j] == 0x42)
                    continue;
                
                Sum += GameLogic[j];
                Left = Sum % 10;
                if (Left > MaxIndex[0])
                {
                    MaxIndex[0] = Left;
                    MaxIndex[1] = i;
                    MaxIndex[2] = j;
                }
                
                if (Left == 0)
                {
                    TestSum = 0;
                    for (BYTE m = 0, n = 0; m < Count; ++m)
                    {
                        if (m == i || m == j)
                            continue;
                        PromptData[n++] = CardData[m];
                        TestSum += (GetColor(CardData[m]) == 4) ? 10 : GameLogic[m];
                    }
                    PromptData[3] = CardData[i];
                    PromptData[4] = CardData[j];
                    if (TestSum % 10 == 0)
                        return (WType == 1) ? ECT_XYN : ECT_DYN;
                    return (WType == 1) ? ECT_XWNN : ECT_DWNN;
                }
                
                for (BYTE k = j + 1; k < Count; ++k)
                {
                    if (CardData[k] == 0x41 || CardData[k] == 0x42)
                        continue;
                    
                    Sum += GameLogic[k];
                    if (Sum % 10 == 0)
                    {
                        TestSum = 0;
                        PromptData[0] = CardData[i];
                        PromptData[1] = CardData[j];
                        PromptData[2] = CardData[k];
                        for (BYTE m = 0, n = 3; m < Count; ++m)
                        {
                            if (m == i || m == j || m == k)
                                continue;
                            PromptData[n++] = CardData[m];
                            TestSum += (GetColor(CardData[m]) == 4) ? 10 : GameLogic[m];
                        }
                        
                        if (TestSum % 10 == 0)
                            return (WType == 1) ? ECT_XYN : ECT_DYN;
                        return (WType == 1) ? ECT_XWNN : ECT_DWNN;
                    }
                    
                    Sum -= GameLogic[k];
                }
                
                Sum -= GameLogic[j];
            }
            
            Sum = 0;
        }
        
        for (BYTE m = 0, n = 0; m < Count; ++m)
        {
            if (m == MaxIndex[1] || m == MaxIndex[2])
                continue;
            PromptData[n++] = CardData[m];
        }
        PromptData[3] = CardData[MaxIndex[1]];
        PromptData[4] = CardData[MaxIndex[2]];
        return (ECT_WN + 3 * (MaxIndex[0] - 1) + WType);
    }
    else if (WCount == 2)
    {
        TestSum = 0;
        PromptData[0] = 0x41;
        for (BYTE m = 0, n = 1; m < Count; ++m)
        {
            if (CardData[m] == 0x42 || CardData[m] == 0x41)
                continue;
            PromptData[n++] = CardData[m];
            TestSum += GameLogic[m];
        }
        PromptData[4] = 0x42;
        
        if (TestSum % 10 == 0)
        {
            for (BYTE m = 0, n = 0; m < Count; ++m)
            {
                if (CardData[m] == 0x42 || CardData[m] == 0x41)
                    continue;
                PromptData[n++] = CardData[m];
            }
            PromptData[3] = 0x41;
            PromptData[4] = 0x42;
            return ECT_DYN;
        }
        
        return ECT_SWNN;
    }
    
    return ECT_WN;
}

BYTE Ox6GameLogic::GetLotteryType(const BYTE Type, const BYTE CardData[], const int Count)
{
    if (Type < ECT_WJ || Type > ECT_ZD)
        return ELT_NULL;
    if (Count != HAND_CARD || CardData == NULL)
        return ELT_NULL;
    
    switch (Type)
    {
        case ECT_WJ:
            return ELT_WJ;
            break;
        case ECT_ST:
            return ELT_ST;
            break;
        case ECT_HL:
            return ELT_HL;
            break;
        case ECT_SW:
            return ELT_SW;
            break;
        case ECT_ZD:
            return ELT_ZD;
            break;
        default:
            break;
    }
    
    return ELT_NULL;
}

BYTE Ox6GameLogic::GetValue(const BYTE CardData)
{
    return CardData & VALUE_MASK;
}

BYTE Ox6GameLogic::GetColor(const BYTE CardData)
{
    return (CardData & COLOR_MASK) >> 4;
}

BYTE Ox6GameLogic::GetLogic(const BYTE CardData)
{
    BYTE Value = GetValue(CardData);
    
    if (CardData == 0x41 || CardData == 0x42)
        return Value + 13;
    
    return Value;
}

BYTE Ox6GameLogic::GetGameLogic(const BYTE CardData)
{
    BYTE Value = GetLogic(CardData);
    return Value > 10 ? 10 : Value;
}

BYTE Ox6GameLogic::GetGameLogicEx(const BYTE CardLogic)
{
    return CardLogic > 10 ? 10 : CardLogic;
}

// FIXME: Repeat
bool Ox6GameLogic::VerifyCard(const BYTE CardData)
{
    const BYTE Value = (CardData & VALUE_MASK);
    const BYTE Color = (CardData & COLOR_MASK) >> 4;
    
    if ((Color >= 0 && Color <= 3) && (Value >= 1 && Value <= 13))
        return true;
    else if (CardData == 0x41 || CardData == 0x42)
        return true;
    
    return false;
}

bool Ox6GameLogic::TestRemove(BYTE LCardData[], const int LCount, const BYTE RCardData[], const int RCount)
{
    if (LCount < RCount)
        return false;
    if (LCardData == NULL || RCardData == NULL)
        return false;
    
    BYTE TempData[HAND_CARD] = { 0 };
    memcpy(TempData, LCardData, HAND_CARD);
    int Count = 0;
    for (BYTE i = 0; i < RCount; ++i)
    {
        for (BYTE j = 0; j < LCount; ++j)
        {
            if (RCardData[i] == TempData[j])
            {
                TempData[j] = 0;
                ++Count;
            }
        }
    }
    
    return Count == RCount;
}

void Ox6GameLogic::SortCard(BYTE CardData[], const int Count)
{
    BYTE Temp;
    for (BYTE j = 0; j < Count - 1; ++j)
    {
        for (BYTE i = 0; i < Count - 1 - j; ++i)
        {
            if (CardData[i] > CardData[i + 1])
            {
                Temp = CardData[i];
                CardData[i] = CardData[i + 1];
                CardData[i + 1] = Temp;
            }
        }
    }
}

void Ox6GameLogic::SortCardByLogic(BYTE CardData[], const int Count)
{
    BYTE Temp;
    for (BYTE j = 0; j < Count - 1; ++j)
    {
        for (BYTE i = 0; i < Count - 1 - j; ++i)
        {
            if (GetLogic(CardData[i]) > GetLogic(CardData[i + 1]))
            {
                Temp = CardData[i];
                CardData[i] = CardData[i + 1];
                CardData[i + 1] = Temp;
            }
        }
    }
}

void Ox6GameLogic::SwitchToLogic(const BYTE CardData[], BYTE OutData[], const int Count, const int Type /*= 0*/)
{
    BYTE Data;
    for (BYTE i = 0; i < Count; ++i)
    {
        Data = (Type == 0) ? GetLogic(CardData[i]) : GetGameLogic(CardData[i]);
        OutData[i] = Data;
    }
}

int Ox6GameLogic::GetTimes(const BYTE Type)
{
    if (Type <= ECT_NULL || Type > ECT_ZD)
        return 0;
    
    if (Type <= ECT_N8)
        return 1;
    if (Type == ECT_XN9 || Type == ECT_DN9 || Type == ECT_N9)
        return 2;
    if (Type == ECT_XWNN || Type == ECT_DWNN || Type == ECT_SWNN || Type == ECT_NN)
        return 3;
    if (Type == ECT_XYN ||
        Type == ECT_DYN ||
        Type == ECT_WJ ||
        Type == ECT_ST ||
        Type == ECT_HL ||
        Type == ECT_SW ||
        Type == ECT_ZD)
        return 4;
    
    return 0;
}



