#ifndef __GAME_LOGICNN_H
#define __GAME_LOGICNN_H
#pragma once

#define CARD_FULL		54
#define HAND_CARD		5

#define COLOR_MASK		0xF0
#define VALUE_MASK		0x0F

typedef unsigned char	BYTE;


class Ox6GameLogic
{
public:
    enum ELotteryType
    {
        ELT_NULL,
        ELT_WJ,				//五金牛牛
        ELT_ZD,				//炸弹牛牛
        ELT_HL,				//葫芦牛牛
        ELT_ST,				//三条牛牛
        ELT_SW,				//双王牛牛
    };
    
    enum ECardType
    {
        ECT_NULL	= 0,
        ECT_WN		= 1,	//无牛
        ECT_XN1		= 2,	//小王牛一
        ECT_DN1		= 3,	//大王牛一
        ECT_N1		= 4,	//牛一
        ECT_XN2		= 5,	//小王牛二
        ECT_DN2		= 6,	//大王牛二
        ECT_N2		= 7,	//牛二
        ECT_XN3		= 8,	//小王牛三
        ECT_DN3		= 9,	//大王牛三
        ECT_N3		= 10,	//牛三
        ECT_XN4		= 11,	//小王牛四
        ECT_DN4		= 12,	//大王牛四
        ECT_N4		= 13,	//牛四
        ECT_XN5		= 14,	//小王牛五
        ECT_DN5		= 15,	//大王牛五
        ECT_N5		= 16,	//牛五
        ECT_XN6		= 17,	//小王牛六
        ECT_DN6		= 18,	//大王牛六
        ECT_N6		= 19,	//牛六
        ECT_XN7		= 20,	//小王牛七
        ECT_DN7		= 21,	//大王牛七
        ECT_N7		= 22,	//牛七
        ECT_XN8		= 23,	//小王牛八
        ECT_DN8		= 24,	//大王牛八
        ECT_N8		= 25,	//牛八
        ECT_XN9		= 26,	//小王牛九
        ECT_DN9		= 27,	//大王牛九
        ECT_N9		= 28,	//牛九
        ECT_XWNN	= 29,	//小王牛牛
        ECT_DWNN	= 30,	//大王牛牛
        ECT_SWNN	= 31,	//双王牛牛, 大王牛牛
        ECT_NN		= 32,	//普通牛牛
        
        ECT_XYN		= 33,	//小王硬牛
        ECT_DYN		= 34,	//大王硬牛
        ECT_WJ		= 35,	//五金牛牛
        ECT_ST		= 36,	//三条牛牛
        ECT_HL		= 37,	//葫芦牛牛
        ECT_SW		= 38,	//双王牛牛
        ECT_ZD		= 39,	//炸弹牛牛
    };
    
    Ox6GameLogic();
    virtual ~Ox6GameLogic();
    
    void ShuffleCard(BYTE CardData[], const int Count);
    int CompareCard(const BYTE LCardData[], const int LCount, const BYTE RCardData[], const int RCount); //return (L>R);
    
    BYTE GetCardType(const BYTE CardData[], const int Count);
    BYTE GetPromptType(BYTE PromptData[], int& PromptCount, const BYTE SrcData[], const int SrcCount);
    BYTE GetLotteryType(const BYTE Type, const BYTE CardData[], const int Count);
    //FIXME: Load Config
    int GetTimes(const BYTE Type);
    
    BYTE GetValue(const BYTE CardData);
    BYTE GetColor(const BYTE CardData);
    BYTE GetLogic(const BYTE CardData);//1,2,3,4,5,6,7,8,9,10,11,12,13,14,15
    BYTE GetGameLogic(const BYTE CardData);//1,2,3,4,5,6,7,8,9,10,10,10,10,10,10
    BYTE GetGameLogicEx(const BYTE CardLogic);
    bool VerifyCard(const BYTE CardData);
    bool TestRemove(BYTE LCardData[], const int LCount, const BYTE RCardData[], const int RCount);
    
    
    void SwitchToLogic(const BYTE CardData[], BYTE OutData[], const int Count, const int Type = 0);//0: Logic, 1: GameLogic
    void SortCard(BYTE CardData[], const int Count);//Left < Right
    void SortCardByLogic(BYTE CardData[], const int Count);//Left < Right
    
private:
    BYTE AnalysisCard(const BYTE CardData[], const int Count, BYTE PromptData[]);
    
    static const BYTE	mCardData[];
};


#endif			// __GAME_LOGICNN_H



