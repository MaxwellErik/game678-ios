﻿#include "TcpClientSocket.h"
#include "md5To.h"
#include "cocos2d.h"
#include <regex>



#if (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)

#include <ws2tcpip.h>

#else

#include<netdb.h>
#include <sys/time.h>
unsigned long GetTickCount()
{
	struct timeval tv;
	if (gettimeofday(&tv, 0)) {
		return 0;
	}
    
	return (tv.tv_sec * 1000 + tv.tv_usec / 1000);
}
#endif
//#if (CC_TARGET_PLATFORM == CC_PALTFORM_ANDROID)
#define SOCKET int
#define INVALID_SOCKET	-1

// 设定 Socket 为强迫关闭
void SetSocketLingerOFF(int SocketFD)
{
    // 强制关闭
    linger m_sLinger;

    m_sLinger.l_onoff = 1;
    m_sLinger.l_linger = 0;

    setsockopt(SocketFD, SOL_SOCKET, SO_LINGER, (const char *) &m_sLinger, sizeof(m_sLinger));

    // 服务器 bind 设定
    int option = 1;
    setsockopt(SocketFD, SOL_SOCKET, SO_REUSEADDR, (char *) &option, sizeof(option));

    // No Delay
    setsockopt(SocketFD, IPPROTO_TCP, TCP_NODELAY, (char *) &option, sizeof(option));

    // 设定 非阻塞
    int flag = fcntl(SocketFD, F_GETFL, 0);
    fcntl(SocketFD, F_SETFL, flag | O_NONBLOCK);
}

bool IsIPV4(const std::string& strIp)
{
    const static std::regex pattern("^(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[0-9]{1,2})(\.(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[0-9]{1,2})){3}$");
    return std::regex_match(strIp, pattern);
}

TcpClientSocket::TcpClientSocket(int ReadBufferLen)
{

	#if (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)

		// Socket Init
		WSADATA wsaData;
		int ReturnValue = WSAStartup(MAKEWORD(2,2), &wsaData);

	#endif

	this->m_pRecvBuffer = new char[ReadBufferLen * 2];
	this->m_RecvBufferLength = ReadBufferLen;
	this->m_RecvBufferUsedCounter = 0;

	this->m_dwSendPacketCount = 0;
	this->m_dwSendXorKey = 0;

	this->m_pTcpCallback = NULL;
	
    this->m_Socket = -1;

}

TcpClientSocket::~TcpClientSocket()
{	
	delete [] this->m_pRecvBuffer;
}


void TcpClientSocket::encrypt_code(char* buf,char* xorkeys,int xorlen,int count)
{
	int i;
	for (i=0;i<=count-1;i++)
	{
		buf[i]=buf[i] ^ xorkeys[i % xorlen];

	}
}

int TcpClientSocket::safeconnect(const char* ip,int port,time_t Unixdate)
{
	int s;
	char buf[20];
	struct sockaddr_in sock;
	fd_set rset,wset;
	struct timeval tv;
	tv.tv_sec=10;
	s=socket(AF_INET,SOCK_STREAM,0);
	if(-1==s) return false;

	SetSocketLingerOFF(s);

	sock.sin_family=PF_INET;
	sock.sin_port  =htons(port);
	sock.sin_addr.s_addr=inet_addr(ip);


	char * g_xorkey="BF3FB36ABA9741F1";
	char md5str[33]={0};
	char keystr[8]={0};

	int nNetTimeout=0;
	time_t d1,d2;


	fd_set rd;
	int ret;
	ret =2;
	ClientData r_client={0};
	MD5To iMD5; 

	nNetTimeout=5000;//1秒，

	nNetTimeout=5000;//1秒，
	connect(s,(struct sockaddr*)&sock, sizeof(struct sockaddr_in));

	tv.tv_sec = 0;
	tv.tv_usec = 50;
	FD_ZERO(&rd);
	FD_SET(s, &rd);
	if(select(s + 1, &rd, NULL, NULL, &tv) < 0) 
	{
		shutdown(s, 2);
		close(s);
		s = -1;

		return ret;
	} 
	
	time_t now_time,now_time1;
	now_time = time(NULL);
	long num = -1;
	while (num == -1)
	{
		 num = recv(s, (char*)&r_client, sizeof(r_client), 0);
		 now_time1 = time(NULL);
		 if(now_time1-now_time > 3)
		 {
			break;
		 }
	}
	//int num = recv(s,(char*)&r_client,sizeof(r_client),0);
	int abc = sizeof(r_client);
	if (num==sizeof(r_client)) 
	{
        
		encrypt_code((char*)&r_client, (char*)"BF3FB36ABA9741F1", 16, sizeof(r_client));
		time(&d1);
		long dadd=abs(d1-Unixdate);
		if (dadd>300) //大于5分钟
		{
			shutdown(s, 2);
			close(s);
			s = -1;

			ret=1;
			return ret;
		}

		r_client.Time=Unixdate;//DateTimeToUnix(d1);
		memcpy(keystr,&r_client.Time,8);
		encrypt_code((char*)&r_client.key,keystr,8,sizeof(r_client.key)-1);
		iMD5.GenerateMD5((unsigned char*)&r_client.key,sizeof(r_client.key)-1);

		for(int j = 0; j < 16; j++ )
		{
			sprintf( md5str + j * 2, "%02x", ((unsigned char*)iMD5.m_data)[j]);
		}

		memcpy(&r_client.MD5,md5str,strlen(md5str));
		encrypt_code((char*)&r_client, (char*)"BF3FB36ABA9741F1", 16, sizeof(r_client));

		send(s,(char*)&r_client,sizeof(r_client),0);
		if (recv(s,(char*)&r_client,sizeof(r_client),0) ==sizeof(r_client)) 
		{
			encrypt_code((char*)&r_client, (char*)"BF3FB36ABA9741F1", 16, sizeof(r_client));
			if (r_client.Ret==0)
			{
				ret=0;
			}
			shutdown(s, 2);
			close(s);
			s = -1;

			return ret;
		} 
	}
    return 0;
}


bool TcpClientSocket::TestConnectServer(const char *pServerIP,unsigned short ServerPort)
{
	int s;
	char buf[20];
	struct sockaddr_in sock;
	fd_set rset,wset;
	struct timeval tv;
	tv.tv_sec=10;
	s=socket(AF_INET,SOCK_STREAM,0);
	if(-1==s) return false;

	SetSocketLingerOFF(s);

	sock.sin_family=PF_INET;
	sock.sin_port  =htons(ServerPort);
	sock.sin_addr.s_addr=inet_addr(pServerIP);
	if(connect(s,(struct sockaddr*)&sock, sizeof(struct sockaddr_in)) == -1)
	{
		//DWORD ddr = GetLastError();
		struct timeval tv;
		fd_set writefds;
		tv.tv_sec = 1;
		tv.tv_usec = 0;
		FD_ZERO(&writefds);
		FD_SET(s, &writefds);
		if(select(s+1,NULL,&writefds,NULL,&tv)>0)
		{
			int _error = 1;
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
			socklen_t len = sizeof(int);
#else
			int len = sizeof(int);
#endif
			getsockopt(s, SOL_SOCKET, SO_ERROR,(char*)&_error, &len);
			if(_error==0)
			{
				shutdown(s, 2);
				close(s);
				s = -1;

				return true;
			}
			else
			{
				shutdown(s, 2);
				close(s);
				s = -1;

				return false;
			}
		}
		else
		{
			shutdown(s, 2);
			close(s);
			s = -1;
		
            return false;
		}
	}
	else
	{
		shutdown(s, 2);
		close(s);
		s = -1;

		return true;
	}
	return false;
}

bool TcpClientSocket::hasError()
{

    int err = errno;
    
    if(err != EINPROGRESS && err != EAGAIN)
    {
        return true;
    }
    return false;
}
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
bool TcpClientSocket::IOSConnectServer(const char *pServerIP, unsigned short ServerPort)
{
    // 检查参数
    if(pServerIP == 0 || strlen(pServerIP) > 15) return -1;
    
    char pszHost[NI_MAXHOST], szService[NI_MAXSERV];
    
    int iAddrType = 0;
    if(iAddrType == 0 && IsIPV4(pServerIP)) iAddrType = 1;
    if(iAddrType == 0 && IsIPV4(pServerIP)) iAddrType = 2;
    
    if (iAddrType != 0)
    {
        struct sockaddr_in sa_in;
        int flags;
        int err;
        
        sa_in.sin_family = iAddrType == 1 ? AF_INET : AF_INET6;
        sa_in.sin_port = htons(ServerPort);
        sa_in.sin_addr.s_addr = inet_addr(pServerIP);
        
        flags = 0;
        
        err = getnameinfo((struct sockaddr *)(&sa_in), sizeof(struct sockaddr),
                          pszHost, sizeof(pszHost), szService, sizeof(szService), flags);
        if(err != 0)
        {
            return false;
        }
    }
    else
    {
        snprintf(pszHost, NI_MAXHOST, "%s",pServerIP);
        snprintf(szService, NI_MAXSERV, "%d",ServerPort);
    }

    int sockfd , ret;
    struct addrinfo hints, * res, *ressave ;
    memset(&hints , 0, sizeof( hints));
    
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_protocol = IPPROTO_IP;
    
    if (0 != (ret = getaddrinfo(pszHost, szService, &hints, &res)))
    {
        return -1;
    }
    
    ressave = res ;
    while (NULL != res)
    {
        if (-1 == (sockfd = socket( res->ai_family, res-> ai_socktype, res ->ai_protocol)))
        {
            res = res ->ai_next;
            continue;
        }
        // 设置为非阻塞方式
        fcntl(sockfd, F_SETFL, O_NONBLOCK);
        
        if (SOCKET_ERROR == ::connect (sockfd, res->ai_addr, res-> ai_addrlen))
        {
            if (hasError())
            {
                CloseSocket();
                res = res ->ai_next;
                continue;
            }
            else
            {
                timeval timeout;
                timeout.tv_sec    = 5;
                timeout.tv_usec    = 0;
                fd_set writeset, exceptset, readset;
                FD_ZERO(&writeset);
                FD_ZERO(&exceptset);
                FD_ZERO(&readset);
                FD_SET(sockfd, &writeset);
                FD_SET(sockfd, &exceptset);
                FD_SET(sockfd, &readset);
                
                int ret = select(FD_SETSIZE, &readset, &writeset, &exceptset, &timeout);
                
                if (FD_ISSET(sockfd, &readset)) {
                    printf("%s", "success");
                }
                
                if (ret == 0 || ret < 0)
                {
                    CloseSocket();
                    res = res ->ai_next;
                    continue;
                }
                else
                {
                    ret = FD_ISSET(sockfd, &exceptset);
                    if(ret)
                    {
                        CloseSocket();
                        res = res ->ai_next;
                        continue;
                    }
                }
            }
        }
        
        break;
    }
    
    freeaddrinfo(ressave);
    
    if (NULL == res)
    {
        return -1;
    }
    
    

    m_Socket = sockfd;
    return true;
}
#endif
    
bool TcpClientSocket::ConnectServer(const char *pServerIP, unsigned short ServerPort, TcpCallback *pTcpCallback)
{
    if (m_bConnecting == true) {
        return true;
    }
    m_bConnecting = true;
    
    this->m_pTcpCallback = pTcpCallback;
    auto _pthread = new std::thread(&TcpClientSocket::ThreadConnectServer, this, pServerIP, ServerPort);
    
    _pthread->detach();
    return true;
}
    
    
bool TcpClientSocket::SendNotSendData()
{
    bSendNotSend = true;
    bool bSuccess = true;
    for (vector<MsgStruct>::iterator iter = this->notSendMsg.begin();
         iter != this->notSendMsg.end();)
    {
        if (iter->dataLen) {
            bSuccess = this->SendData(iter->mainId, iter->subId, iter->data, iter->dataLen);
        } else {
            bSuccess = this->SendData(iter->mainId, iter->subId);
        }
        
        if (bSuccess == false)
        {
            bSendNotSend = false;
            notSendMsg.clear();
            return false;
        }
        iter = this->notSendMsg.erase(iter);
    }

    bSendNotSend = false;

    return true;
}
    
bool TcpClientSocket::ThreadConnectServer(const char* pServerIP,unsigned short ServerPort)
{
	this->CloseSocket();
	
	 struct hostent *remoteHostEnt = gethostbyname(pServerIP);
	const char *sLOGINIP = NULL;
	if (remoteHostEnt == NULL)
	{
        if (1 == safeconnect(pServerIP, ServerPort, 5))
            return true;
        
		sLOGINIP = "183.60.201.29";
	}
	else
	{
		struct in_addr *remoteInAddr = (struct in_addr *) remoteHostEnt->h_addr_list[0];
		sLOGINIP = inet_ntoa(*remoteInAddr);
	}

    
    //设置参数
    m_wRecvSize = 0;
    m_cbSendRound = 0;
    m_cbRecvRound = 0;
    m_dwSendXorKey = 0x12345678;
    m_dwRecvXorKey = 0x12345678;
    m_dwSendTickCount = GetTickCount() / 1000L;
    m_dwRecvTickCount = GetTickCount() / 1000L;
    
    cocos2d::log("serverIP：%s， port num:%d",pServerIP, ServerPort);
    
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    IOSConnectServer(pServerIP, ServerPort);
#else
    struct sockaddr_in 	addrServer;
    SOCKET tempSocket = INVALID_SOCKET;
    
    memset(&addrServer, 0, sizeof(addrServer));
    addrServer.sin_family = AF_INET;
    addrServer.sin_port = htons(ServerPort);
    addrServer.sin_addr.s_addr = inet_addr(sLOGINIP);
    
    if((tempSocket = socket(AF_INET,SOCK_STREAM,0)) < 0)
    {
    	this->m_pTcpCallback->OnEventTCPConnectError();
    	m_bConnecting = false;
        return false;
    }

    fcntl(tempSocket, F_SETFL, O_NONBLOCK);
    bool isConnectError = false;
    if(SOCKET_ERROR == connect(tempSocket, (struct sockaddr *)&addrServer, sizeof(addrServer)))
    {

    	cocos2d::log("%s", "cccccccc");
    	do{
			if (hasError())
			{
				isConnectError = true;
				break;
			}
			cocos2d::log("%s", "dddd");
			timeval timeout;
			timeout.tv_sec    = 5;
			timeout.tv_usec    = 0;
			fd_set writeset, exceptset;
			FD_ZERO(&writeset);
			FD_ZERO(&exceptset);
			FD_SET(tempSocket, &writeset);
			FD_SET(tempSocket, &exceptset);

			int ret = select(FD_SETSIZE, nullptr, &writeset, &exceptset, &timeout);
			if (ret == 0 || ret < 0)
			{
				isConnectError = true;
				break;
			}

			ret = FD_ISSET(tempSocket, &exceptset);
			if(ret)
			{
				isConnectError = true;
				break;
			}

    	} while(0);

    	if (isConnectError) {
    		cocos2d::log("%s","bbbb");
			this->m_pTcpCallback->OnEventTCPConnectError();
			m_bConnecting = false;
			return false;
    	}


    }
    cocos2d::log("%s","xxxxx");
    
    this->m_Socket = tempSocket;
#endif
    if (this->m_pTcpCallback != NULL)
    {
        if (SendNotSendData())
        	this->m_pTcpCallback->OnEventTCPSocketLink();
    }
    m_bConnecting = false;
    return true;
}
    
void TcpClientSocket::CloseSocket()
{
//    cocos2d::log("~~~~~close socket");
	// 接收 缓存索引 归零
    // 接收 缓存索引 归零
    m_RecvBufferUsedCounter = 0;
    m_cbSendRound = 0;
    m_cbRecvRound = 0;
    m_dwSendPacketCount = 0;
    m_dwRecvXorKey = 0;
    m_dwSendXorKey = 0;

	if(this->m_Socket < 0)
	{
		return;
	}
	
    shutdown(this->m_Socket, 2);
    close(this->m_Socket);
    this->m_Socket = -1;

	if (m_pTcpCallback != NULL)
	{
        if (isTimeOut || isError) {
            isTimeOut = false;
            isError = false;
            m_pTcpCallback->OnEventTCPConnectError();
        }
		
	}	
}

bool TcpClientSocket::IsConnect(void)
{
    if(this->m_Socket >= 0)
    {
        return true;
    }
    else
    {
        return false;
    }
}

bool TcpClientSocket::RecvPack()
{

		if(!this->IsConnect())
		{
			return false;
		}

		int tempRecvLength = recv(this->m_Socket, (char *)this->m_cbRecvBuf + this->m_wRecvSize, sizeof(this->m_cbRecvBuf) - this->m_wRecvSize, 0);

        // 错误 处理
        if(tempRecvLength < 0)
        {
            // 由于是非阻塞的模式,所以当errno为EAGAIN时,表示当前缓冲区已无数据可读
            // 在这里就当作是该次事件已处理处.
            //即当buflen < 0 且 errno == EAGAIN时，表示没有数据了。(读/写都是这样)
            if(errno == EAGAIN || errno == EINTR)  
            {
                return true;
            }
            
            isError = true;
            // 关闭 socket
            this->CloseSocket();		

            return false;	
        }
        else if(tempRecvLength == 0)
        {
            isError = true;
            // 关闭 socket
            this->CloseSocket();		

            return false;	
        }
		
		this->m_RecvBufferUsedCounter += tempRecvLength;

		m_wRecvSize += tempRecvLength;
		m_dwRecvTickCount = GetTickCount() / 1000L;

		//变量定义
		WORD wPacketSize = 0;
		BYTE cbDataBuffer[SOCKET_BUFFER+sizeof(CMD_Head)];
		CMD_Head * pHead = (CMD_Head *)m_cbRecvBuf;

		while (m_wRecvSize >= sizeof(CMD_Head))
		{
			//效验参数
			wPacketSize = pHead->CmdInfo.wPacketSize;
			//ASSERT(pHead->CmdInfo.cbVersion == SOCKET_VER);
			
			if (pHead->CmdInfo.cbVersion != SOCKET_VER)
			{
				this->CloseSocket();		

				return false;
			}
			if (wPacketSize > (SOCKET_BUFFER + sizeof(CMD_Head)))
			{
				this->CloseSocket();		

				return false;
			}
			if (m_wRecvSize < wPacketSize) return true;

			//拷贝数据
			m_dwRecvPacketCount++;
			memcpy(cbDataBuffer, m_cbRecvBuf, wPacketSize);
			m_wRecvSize -= wPacketSize;
			memmove(m_cbRecvBuf, m_cbRecvBuf + wPacketSize, m_wRecvSize);

			//解密数据
			WORD wRealySize = CrevasseBuffer(cbDataBuffer, wPacketSize);
//			ASSERT(wRealySize >= sizeof(CMD_Head));

			//解释数据
			WORD wDataSize = wRealySize - sizeof(CMD_Head);
			void * pDataBuffer = cbDataBuffer + sizeof(CMD_Head);
			CMD_Command Command = ((CMD_Head *)cbDataBuffer)->CommandInfo;

			//内核命令
			if (Command.wMainCmdID == MDM_KN_COMMAND)
			{
				switch (Command.wSubCmdID)
				{
				case SUB_KN_DETECT_SOCKET:	//网络检测
					{
						//发送数据
						SendData(MDM_KN_COMMAND, SUB_KN_DETECT_SOCKET, pDataBuffer, wDataSize);
						break;
					}
				}
				continue;
			}

			//处理数据
			bool bSuccess = this->m_pTcpCallback->OnEventTCPSocketRead(Command, pDataBuffer, wDataSize);
			if (bSuccess == false)
			{
#ifdef _DEBUG
			cocos2d::CCMessageBox("数据包不匹配!", "警告");
#endif
				this->CloseSocket();		

				return false;
			}
		};
    m_lastSendTime = -1;
    m_lastRecvTime = time(NULL);
	return true;
}

// 清除 已使用的缓存
void TcpClientSocket::CleanRecvBuffer(int UsedBuuferLength)
{
	// 防呆
	if(UsedBuuferLength <= 0)
	{
		// 不处理 任何事情
		return;
	}

	// 清除 全部缓存
	if(this->m_RecvBufferUsedCounter <= UsedBuuferLength)
	{
		this->m_RecvBufferUsedCounter = 0;
		return;
	}

	// Copy 未使用 缓存
	int tempUnusedBufferCounter = this->m_RecvBufferUsedCounter - UsedBuuferLength;

	char *ptempBuffer = new char[tempUnusedBufferCounter];

	memcpy(ptempBuffer, this->m_pRecvBuffer + UsedBuuferLength, tempUnusedBufferCounter);
	memcpy(this->m_pRecvBuffer, ptempBuffer, tempUnusedBufferCounter);

	delete [] ptempBuffer;

	this->m_RecvBufferUsedCounter = tempUnusedBufferCounter;
}

bool TcpClientSocket::SendPack(unsigned char *pPackBuffer, int PackLength)
{
	if(!this->IsConnect())
	{
		return false;
	}
	if(PackLength <= 0)
	{			
		return false;
	}

	if(pPackBuffer == NULL)
	{			
		return false;
	}	 
	unsigned int tempIndex = 0;
	while(PackLength > 0)
	{
		long SendBytes = send(this->m_Socket, (const char *)(pPackBuffer + tempIndex), PackLength, 0);

        // 错误处理
        if(SendBytes < 0)
        {
            // 由于是非阻塞的模式,所以当errno为EAGAIN时,表示当前缓冲区已无数据可读
            // 在这里就当作是该次事件已处理处.
            //即当buflen < 0 且 errno == EAGAIN时，表示没有数据了。(读/写都是这样)
            cocos2d::log("-------socket send Error， errno = %d", errno);
            
            if(errno == EAGAIN || errno == EINTR)  
            {
                break;
            }
            isError = true;
            // 关闭 socket
            this->CloseSocket();		

            return false;	
        }
        else if(SendBytes == 0) 
        {
            isError = true;
            // 关闭 socket
            this->CloseSocket();		

            return false;									
        }


		tempIndex += SendBytes;

		// 防呆
		if(PackLength >= SendBytes)
		{
			PackLength -= SendBytes;
		}
		else
		{
			PackLength = 0;
		}
	}
    m_lastSendTime = time(NULL);
    
    
    cocos2d::log("%s", "消息发送成功！");
	return true;
}



bool TcpClientSocket::SendData( unsigned short wMainCmdID, unsigned short wSubCmdID )
{
	//效验状态
	if(!this->IsConnect())
	{
		if (bSendNotSend) {
			CloseSocket();
			return false;
		}

        MsgStruct msg;
        memset(&msg, 0, sizeof(msg));
        msg.mainId = wMainCmdID;
        msg.subId = wSubCmdID;
        msg.dataLen = 0;
        this->notSendMsg.push_back(msg);
        this->m_pTcpCallback->ConnectToServer();
		return true;
	}
    cocos2d::log("----------->>>>>send Data mainId = %d, subID = %d", wMainCmdID, wSubCmdID);
	//构造数据
	BYTE cbDataBuffer[SOCKET_BUFFER];
	CMD_Head * pHead = (CMD_Head *)cbDataBuffer;
	pHead->CommandInfo.wMainCmdID = wMainCmdID;
	pHead->CommandInfo.wSubCmdID = wSubCmdID;

	//加密数据
	unsigned short wSendSize = EncryptBuffer(cbDataBuffer, sizeof(CMD_Head), sizeof(cbDataBuffer));

	//发送数据
	return SendPack(cbDataBuffer, wSendSize);
}

bool TcpClientSocket::SendData( unsigned short wMainCmdID, unsigned short wSubCmdID, void * const pData, unsigned short wDataSize )
{
	//效验状态
	if(!this->IsConnect())
	{
        if (bSendNotSend) {
            CloseSocket();
            return false;
        }
        MsgStruct msg;
        memset(&msg, 0, sizeof(msg));
        msg.mainId = wMainCmdID;
        msg.subId = wSubCmdID;
        msg.dataLen = wDataSize;
        memcpy(msg.data, pData, wDataSize);
        this->notSendMsg.push_back(msg);
        if (!m_bConnecting) {
            this->m_pTcpCallback->ConnectToServer();
        }
        
		return true;
	}

	if (wDataSize > SOCKET_BUFFER) return false;

    cocos2d::log("----------->>>>>send Data mainId = %d, subID = %d", wMainCmdID, wSubCmdID);
    
	//构造数据
	BYTE cbDataBuffer[SOCKET_BUFFER];
	CMD_Head * pHead = (CMD_Head *)cbDataBuffer;
	pHead->CommandInfo.wMainCmdID = wMainCmdID;
	pHead->CommandInfo.wSubCmdID = wSubCmdID;
	if (wDataSize > 0)
	{
		memcpy(pHead + 1, pData, wDataSize);
	}

	//加密数据
	WORD wSendSize = EncryptBuffer(cbDataBuffer, sizeof(CMD_Head) + wDataSize, sizeof(cbDataBuffer));

	//发送数据
	return SendPack(cbDataBuffer, wSendSize);
}

WORD TcpClientSocket::EncryptBuffer(BYTE pcbDataBuffer[], WORD wDataSize, WORD wBufferSize)
{
    ASSERT(wDataSize>=sizeof(CMD_Head));
    ASSERT(wBufferSize>=(wDataSize+2*sizeof(uint32_t)));
    ASSERT(wDataSize<=(sizeof(CMD_Head)+SOCKET_PACKET));
    

    WORD wEncryptSize=wDataSize-sizeof(CMD_Command),wSnapCount=0;
    if ((wEncryptSize%sizeof(unsigned int))!=0)
    {
        wSnapCount=sizeof(uint32_t)-wEncryptSize%sizeof(uint32_t);
        memset(pcbDataBuffer+sizeof(CMD_Info)+wEncryptSize,0,wSnapCount);
    }
    

    BYTE cbCheckCode=0;
    for (WORD i=sizeof(CMD_Info);i<wDataSize;i++)
    {
        cbCheckCode+=pcbDataBuffer[i];
        pcbDataBuffer[i]=MapSendByte(pcbDataBuffer[i]);
    }
    
    CMD_Head * pHead=(CMD_Head *)pcbDataBuffer;
    pHead->CmdInfo.cbCheckCode=~cbCheckCode+1;
    pHead->CmdInfo.wPacketSize=wDataSize;
    pHead->CmdInfo.cbVersion=SOCKET_VER;
    
    uint32_t dwXorKey=m_dwSendXorKey;
    if (m_dwSendPacketCount==0)
    {
        dwXorKey = 0xaa9d3159;
        m_dwSendXorKey=dwXorKey;
        m_dwRecvXorKey=dwXorKey;
    }
    
    
    WORD * pwSeed=(WORD *)(pcbDataBuffer+sizeof(CMD_Info));
    uint32_t * pdwXor=(uint32_t *)(pcbDataBuffer+sizeof(CMD_Info));
    WORD wEncrypCount=(wEncryptSize+wSnapCount)/sizeof(uint32_t);
    for (int i=0;i<wEncrypCount;i++)
    {
        *pdwXor++^=dwXorKey;
        dwXorKey=SeedRandMap(*pwSeed++);
        dwXorKey|=((uint32_t)SeedRandMap(*pwSeed++))<<16;
        dwXorKey^=g_dwPacketKey;
    }
    
    m_dwSendPacketCount++;
    m_dwSendXorKey=dwXorKey;
    
    return wDataSize;
}

WORD TcpClientSocket::CrevasseBuffer(BYTE pcbDataBuffer[], WORD wDataSize)
{

    ASSERT(m_dwSendPacketCount>0);
    ASSERT(wDataSize>=sizeof(CMD_Head));
    ASSERT(((CMD_Head *)pcbDataBuffer)->CmdInfo.wPacketSize==wDataSize);
    
    WORD wSnapCount=0;
    if ((wDataSize%sizeof(uint32_t))!=0)
    {
        wSnapCount=sizeof(uint32_t)-wDataSize%sizeof(uint32_t);
        memset(pcbDataBuffer+wDataSize,0,wSnapCount);
    }
    
    
    uint32_t dwXorKey=m_dwRecvXorKey;
    uint32_t * pdwXor=(uint32_t *)(pcbDataBuffer+sizeof(CMD_Info));
    WORD  * pwSeed=(WORD *)(pcbDataBuffer+sizeof(CMD_Info));
    WORD wEncrypCount=(wDataSize+wSnapCount-sizeof(CMD_Info))/4;
    for (WORD i=0;i<wEncrypCount;i++)
    {
        if ((i==(wEncrypCount-1))&&(wSnapCount>0))
        {
            BYTE * pcbKey=((BYTE *)&m_dwRecvXorKey)+sizeof(uint32_t)-wSnapCount;
            CopyMemory(pcbDataBuffer+wDataSize,pcbKey,wSnapCount);
        }
        dwXorKey=SeedRandMap(*pwSeed++);
        dwXorKey|=((uint32_t)SeedRandMap(*pwSeed++))<<16;
        dwXorKey^=g_dwPacketKey;
        *pdwXor++^=m_dwRecvXorKey;
        m_dwRecvXorKey=dwXorKey;
    }
    
    
    CMD_Head * pHead=(CMD_Head *)pcbDataBuffer;
    BYTE cbCheckCode=pHead->CmdInfo.cbCheckCode;
    for (int i=sizeof(CMD_Info);i<wDataSize;i++)
    {
        pcbDataBuffer[i]=MapRecvByte(pcbDataBuffer[i]);
        cbCheckCode+=pcbDataBuffer[i];
    }
    if (cbCheckCode!=0) throw TEXT(" ˝æ›∞¸–ß—È¬Î¥ÌŒÛ");
    
    return wDataSize;
}

    
    //随机映射
unsigned short TcpClientSocket::SeedRandMap(unsigned short wSeed)
{
	IosDword dwHold = wSeed;
	return (unsigned short)((dwHold = dwHold * 241103L + 2533101L) >> 16);
}

//映射发送数据
unsigned char TcpClientSocket::MapSendByte(unsigned char const cbData)
{
	unsigned char cbMap = g_SendByteMap[(unsigned char)(cbData+m_cbSendRound)];
	m_cbSendRound += 3;
	return cbMap;
}

//映射接收数据
BYTE TcpClientSocket::MapRecvByte(unsigned char const cbData)
{
	unsigned char cbMap = g_RecvByteMap[cbData] - m_cbRecvRound;
	m_cbRecvRound += 3;
	return cbMap;
}



