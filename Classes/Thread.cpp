﻿#include "Thread.h"
#include "cocos2d.h"


CThread::CThread(void)
{
}


CThread::~CThread(void)
{
}

int CThread::Start()
{
	int errCode=0;  
	do {  
		pthread_attr_t tAttr;  
		errCode=pthread_attr_init(&tAttr);  
		CC_BREAK_IF(errCode!=0);  
		errCode=pthread_attr_setdetachstate(&tAttr, PTHREAD_CREATE_DETACHED);  
		if(errCode!=0)  
		{  
			pthread_attr_destroy(&tAttr);  
			break;  
		}  
		errCode=pthread_create(&m_thread, &tAttr, thread_funcation, this);  

	} while (0);  
	return errCode;  
}

void* CThread::thread_funcation( void *arg )
{
	CThread *p = (CThread *)arg;
	p->Run();
	return NULL;
}

void CThread::Run()
{
	CCLOG("启动线程");
}

void CThread::Stop()
{
	pthread_kill(m_thread, 0);
}
