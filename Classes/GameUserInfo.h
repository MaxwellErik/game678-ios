#ifndef _GameUSERINFO_H_
#define _GameUSERINFO_H_

#include "GameScene.h"
#include "cocos-ext.h"

//����
class GameUserInfo : public GameLayer
{
public:
	virtual bool init();  
	static GameUserInfo *create(GameScene *pGameScene);
	virtual ~GameUserInfo();
	GameUserInfo(GameScene *pGameScene);
	GameUserInfo(const GameUserInfo&);
	GameUserInfo& operator = (const GameUserInfo&);
	enum Tag
	{
		_BtnClose,
        _BtnUpgrade,
        _BtnChangeBank,
        _BtnChangeLogin,
        _BtnLogin,
        _BtnBank,
	};
    
public:
    cocos2d::ui::EditBox*	m_Account;
    cocos2d::ui::EditBox*	m_NickName;
    cocos2d::ui::EditBox*	m_RegPass;
    cocos2d::ui::EditBox*	m_RegPassA;
    cocos2d::ui::EditBox*	m_LoginPassOld;
    cocos2d::ui::EditBox*	m_LoginPass;
    cocos2d::ui::EditBox*	m_LoginPassA;
    cocos2d::ui::EditBox*	m_BankPassOld;
    cocos2d::ui::EditBox*	m_BankPass;
    cocos2d::ui::EditBox*	m_BankPassA;
    cocos2d::ui::EditBox*	m_PassOld;
    cocos2d::ui::EditBox*	m_IDNum;
    cocos2d::ui::EditBox*	m_QQ;
    cocos2d::ui::EditBox*	m_Phone;
    
	int                     m_BackTga;
    Layer                   *m_registerLayer;
    Layer                   *m_LoginLayer;
    Layer                   *m_BankLayer;
    
    Menu                    *m_ChangeLoginPass;
    Menu                    *m_ChangeBankPass;
public:
	virtual void onEnter();
	virtual void onExit();
	virtual bool onTouchBegan(Touch *pTouch, Event *pEvent){return true;}
	virtual void onTouchEnded(Touch *pTouch, Event *pEvent){}
	virtual void onTouchMoved(Touch *pTouch, Event *pEvent){}

	virtual void callbackBt(Ref *pSender);
    
    void initRegeist();
    void initChangeLoginPass();
    void initChangeBankPass();
//    int verifyIDCard(const char* input);
    bool checkNameNomative(const string& text, bool isNickname = false);
	Menu* CreateButton( std::string szBtName ,const Vec2 &p , int tag );
	void close();
	void onRemove();
	void SetUserInfo(char* nickname, int gameid, int faceid);
};
#endif
