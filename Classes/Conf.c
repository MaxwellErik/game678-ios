#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include "conf.h"

#define	MAX_STRING_LEN			400

int handle(const char * head, const char *tail, char *param_name, char *param_value);

char EatWhitespace( FILE *f )
{
	char c;
	for( c = getc(f); isspace(c)&&('\n'!= c); c = getc(f) )
		;
	return c;
}

char EatComment( FILE *f )
{
	char c;
  	for( c = getc(f); ('\n'!=c)&&(c != EOF); c = getc(f) )
    	;
  	return c;
} /* EatComment */

int Section(FILE *f, char *cur_section)
{
	char	section[MAX_STRING_LEN];
	char	c;
  	int  	end;
	
	end = 0;
  	c = EatWhitespace(f);
	while(c != EOF)
	{
    	switch( c )
      	{
      	case ']':                       /* Found the closing bracket.         */
        	section[end] = '\0';
        	if( end == 0 )              /* Don't allow an empty name.       */
        	{
         		printf("Empty section name in configuration file.\n");
          		return -1;
          	}
        	EatComment(f);     			/* Finish off the line.             */
        	strncpy(cur_section, section, MAX_STRING_LEN);
        	return 0;
   		case '\n':
      		printf("Can't fine \"]\" in configuration file.\n");
      		return -1;
      	default:                        /* All else are a valid name chars.   */
        	if( isspace( c ) )              /* One space per whitespace region. */
          	{
          		section[end++] = ' ';
          		c = EatWhitespace(f);
          	}
        	else                            /* All others copy verbatim.        */
          	{
          		section[end++] = c;
          		c = getc(f);
          	}
          	if (end >= MAX_STRING_LEN)
          	{
          		printf("Parameter name is too long.\n");
          		return -1;
          	}
          	break;
         }
	}
	return -1;
}

int Parameter(FILE *f, char c, char *param_name, char *param_value)
{
	char	name[MAX_STRING_LEN];
	char	value[MAX_STRING_LEN];
	int 	end;
	int		flag;
	
	/* parser parameter name */
	flag = 0;
	end = 0;
	do
	{
   		switch( c )
    	{
      	case '=':                 /* Equal sign marks end of param name. */
        	name[end] = '\0';         /* Mark end of string & advance.   */
        	if (end == 0)              /* Don't allow an empty name.      */
        	{
        		printf("Invalid parameter name in config file.\n");
          		return -1;
        	}
        	
        	if (name[end-1] == ' ') name[end-1] = '\0';
        	flag = 1;
        	break;

      	case '\n':                /* Find continuation char, else error. */
			name[end] = '\0';
          	printf("Ignoring badly formed line in configuration file: %s\n", name);
          	return 0;

      	case '\0':                /* Shouldn't have EOF within param name. */
      	case EOF:
        	name[end] = '\0';
        	printf("Unexpected end-of-file at: %s\n", name);
        	return 0;

      	default:
        	if( isspace( c ) )     /* One ' ' per whitespace region.       */
          	{
          		name[end++] = ' ';
          		c = EatWhitespace(f);
          	}
        	else                   /* All others verbatim.                 */
          	{
          		name[end++] = c;
          		c = getc(f);
          	}
          	if (end >= MAX_STRING_LEN)
          	{
          		printf("Parameter name is too long.\n");
          		return -1;
          	}
		}
    }while( flag != 1);
    
	/* parser parameter value */
	end = 0;
	flag = 0;
	c = EatWhitespace(f);  /* Again, trim leading whitespace. */
	do
    {
		switch( c )
      	{
      	case '\r':              /* Explicitly remove '\r' because the older */
        	c = getc(f);   /* version called fgets_slash() which also  */
        	break;                /* removes them.                            */

      	case '\n':              /* Marks end of value unless there's a '\'. */
      	case EOF:
			value[end] = '\0';
        	if (value[end-1] == ' ') value[end-1] = '\0';
			flag = 1;
        	break;
      	default:               /* All others verbatim.  Note that spaces do */
        	if( isspace( c ) )     /* One ' ' per whitespace region.       */
          	{
          		value[end++] = ' ';
          		c = EatWhitespace(f);
          	}
        	else                   /* All others verbatim.                 */
          	{
          		value[end++] = c;
          		c = getc(f);
          	}
          	if (end >= MAX_STRING_LEN)
          	{
          		printf("Parameter value is too long.\n");
          		return -1;
          	}
        	break;
      	}
    }while( flag != 1);

   return handle(name, value, param_name, param_value);
}

int ParseConfigFile(char *filename, char *section,  char *name, char *value)
{
	FILE 	*f;
	char 	c;
	char	cur_section[MAX_STRING_LEN];
	int		found;

	found = 0;
	f = fopen(filename, "rb");
	if(f ==NULL)
		return -1;

	memset(cur_section, 0, MAX_STRING_LEN);
	c = EatWhitespace(f);
	while(c != EOF && found == 0)
	{
    	switch( c )
      	{
      		case '\n':                        /* Blank line. */
        	c = EatWhitespace(f);
        	break;

      	case ';':                         /* Comment line. */
      	case '#':
        	c = EatComment(f);
        	break;

      	case '[':                         /* Section Header. */
        	if(Section(f, cur_section) < 0)
			{
				fclose(f);
          		return -1;
			}
        	c = EatWhitespace(f);
       		break;
       	case ']':							/* should not find ']' in here */
          	printf("Ignoring badly formed line in configuration file\n");
        	c = EatWhitespace(f);
        	break;

      	case '\\':                        /* Bogus backslash. */
        	c = EatWhitespace(f);
        	break;

      	default:                   			/* Parameter line. */
      		if (section == 0)
      		{
      			if (cur_section[0] == 0)
      		    	if( Parameter(f, c, name, value) >= 0 )
          				found = 1;
      		}
      		else
      		{
      			if (strncmp(cur_section, section, MAX_STRING_LEN) == 0)
      			{
      		    	if( Parameter(f, c, name, value) >= 0 )
						found = 1;
          		}
      		}
        	c = EatWhitespace(f);
        	break;
        }
    }
	fclose(f);
	if (found)
		return 0;
	else
		return -1;
}

int handle(const char * head, const char *tail, char *param_name, char *param_value)
{
 	if(strcmp(param_name, head) == 0)
	{
		memcpy(param_value,tail, strlen(tail));
		return 0;
	}
	else
		return -1;
}

int GetValueInt(int *value,  char *name, char *filename, char *section, char *defval)
{
	char	temp_value[MAX_STRING_LEN];
	memset(temp_value, 0, sizeof(temp_value));
	if(ParseConfigFile(filename, section, name, temp_value) == 0)
	{
		*value = atoi(temp_value);
		return 0;
	}
	else
	{
		if (defval);
			*value = atoi(defval);
		return -1;
	}
}

int GetValueShort(short int *value,  char *name, char *filename, char *section, char *defval)
{
	char	temp_value[MAX_STRING_LEN];
	memset(temp_value, 0, sizeof(temp_value));
	if(ParseConfigFile(filename, section, name, temp_value) == 0)
	{
		*value = (short)atoi(temp_value);
		return 0;
	}
	else
	{
		if (defval);
			*value = (short)atoi(defval);
		return -1;
	}
}

int GetValueFloat(float *value,  char *name, char *filename, char *section, char *defval)
{
	char	temp_value[MAX_STRING_LEN];
	memset(temp_value, 0, sizeof(temp_value));
	if(ParseConfigFile(filename, section, name, temp_value) == 0)
	{
		*value = (float)atof(temp_value);
		return 0;
	}
	else
	{
		if (defval);
			*value = (float)atof(defval);
		return -1;
	}
}

int GetValueStr(char *value,  char *name, char *filename, char *section, char *defval)
{
	char	temp_value[MAX_STRING_LEN];
	memset(temp_value, 0, sizeof(temp_value));
	if(ParseConfigFile(filename, section, name, temp_value) == 0)
	{
		strcpy(value, temp_value);
		return 0;
	}
	else
	{
		if (defval)
			strcpy(value, defval);
		return -1;
	}
}

int GetValueIPToLong(unsigned long *value,  char *name, char *filename, char *section, char *defval)
{
	return 0;
}

int GetValueHex(long *value,  char *name, char *filename, char *section, char *defval)
{
	char	temp_value[MAX_STRING_LEN];
	memset(temp_value, 0, sizeof(temp_value));
	if(ParseConfigFile(filename, section, name, temp_value) == 0)
	{
		*value = strtol(temp_value, 0, 16);
		return 0;
	}
	else
	{
		if (defval)
			*value = strtol(defval,0 ,16);
		return -1;
	}
}
