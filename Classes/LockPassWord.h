#ifndef _LockPassWord_H_
#define _LockPassWord_H_

#include "GameScene.h"
#include "cocos-ext.h"

class LockPassWord : public GameLayer
{
public:
	virtual bool init();  
	static LockPassWord *create(GameScene *pGameScene);
	virtual ~LockPassWord();
	LockPassWord(GameScene *pGameScene);
	LockPassWord(const LockPassWord&);
	LockPassWord& operator = (const LockPassWord&);

private:
	virtual void onEnter();
	virtual void onExit();

    virtual bool onTouchBegan(Touch *pTouch, Event *pEvent);
    virtual void onTouchMoved(Touch *pTouch, Event *pEvent);
    virtual void onTouchEnded(Touch *pTouch, Event *pEvent);
	virtual void onTouchCancelled(Touch *pTouch, Event *pEvent){}
	// TextField ����
	virtual bool onTextFieldAttachWithIME(TextFieldTTF*pSender){return false;}
	virtual bool onTextFieldDetachWithIME(TextFieldTTF*pSender){return false;}
	virtual bool onTextFieldInsertText(TextFieldTTF *pSender, const char *text, int nLen){return true;}
	virtual bool onTextFieldDeleteBackward(TextFieldTTF *pSender, const char *delText, int nLen){return true;}

	// IME ����
	virtual void keyboardWillShow(IMEKeyboardNotificationInfo &info){}
	virtual void keyboardDidShow(IMEKeyboardNotificationInfo &info){}
	virtual void keyboardWillHide(IMEKeyboardNotificationInfo &info){}
	virtual void keyboardDidHide(IMEKeyboardNotificationInfo &info){}

	Menu* CreateButton(std::string szBtName ,const Vec2 &p , int tag );
	virtual void callbackBt(Ref *pSender);


public:
	void ShowTableLock(bool _show);
	void SetTableArr(int tableId, int chairid);

public:
	Menu* m_LockOk;
	Menu* m_LockCanCel;
	bool	m_IsHavePsw;
	bool	m_TableLockShow;
    cocos2d::ui::EditBox* m_pUserPassWord;
	int     m_chairid;
	int     m_tableid;
    EventListenerTouchOneByOne* listener;
};
#endif
