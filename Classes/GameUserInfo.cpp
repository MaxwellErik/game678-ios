#include "GameUserInfo.h"
#include "LobbySocketSink.h"
#include "Encrypt.h"
#include "SoundUtil.h"
#include "GameLayerMove.h"
#include "Convert.h"
#include "utils/utf8cpp/utf8.h"

GameUserInfo::GameUserInfo( GameScene *pGameScene ):GameLayer(pGameScene)
{

}
GameUserInfo::~GameUserInfo()
{

}

GameUserInfo* GameUserInfo::create(GameScene *pGameScene)
{
	GameUserInfo* temp = new GameUserInfo(pGameScene);
	if(temp && temp->init())
	{
		temp->autorelease();
		return temp;
	}
	else
	{
		CC_SAFE_DELETE(temp);
		return NULL;
	}
}

bool GameUserInfo::init()
{
	if ( !Layer::init() )	return false;
	setLocalZOrder(10);

	m_BackTga = 10;

	Sprite *pBG = Sprite::create("DaTing/RoomBack.jpg");
	pBG->setPosition(_STANDARD_SCREEN_CENTER_IN_PIXELS_);
	pBG->setTag(m_BackTga);
	addChild(pBG);

    Sprite *title = Sprite::createWithSpriteFrameName("title_gerenzhongxin.png");
    title->setPosition(Vec2(960, 1000));
    pBG->addChild(title);

    Menu* pNode = CreateButton("btn_close", Vec2(1800,980), _BtnClose);
	addChild(pNode ,0 , _BtnClose);

    ui::Scale9Sprite* leftBg = ui::Scale9Sprite::createWithSpriteFrameName("bg_shuru.png");
    leftBg->setContentSize(Size(590, 700));
    leftBg->setPosition(Vec2(440,450));
    addChild(leftBg);

    ui::Scale9Sprite* rightBg = ui::Scale9Sprite::createWithSpriteFrameName("bg_shuru.png");
    rightBg->setContentSize(Size(1020, 850));
    rightBg->setPosition(Vec2(1250,470));
    addChild(rightBg);
    
    initRegeist();
    initChangeLoginPass();
    initChangeBankPass();
    
    if (g_GlobalUnits.m_UserLoginInfo.isTourist)
    {
        m_LoginLayer->setVisible(false);
        m_BankLayer->setVisible(false);
    }
    else if (g_GlobalUnits.m_UserLoginInfo.isWxUser)
    {
        m_registerLayer->setVisible(false);
        m_LoginLayer->setVisible(false);
    }
    else
    {
        m_registerLayer->setVisible(false);
        m_BankLayer->setVisible(false);
    }
    
	setTouchEnabled(true);
    auto listener = EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(true);
    listener->onTouchBegan = CC_CALLBACK_2(GameUserInfo::onTouchBegan, this);
    Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(listener, this);

	return true;
}

void GameUserInfo::initRegeist()
{
    m_registerLayer = Layer::create();
    addChild(m_registerLayer);
    const Size inputSize = Size(700,85);
    
    Label *account = Label::createWithSystemFont("账号: ", _GAME_FONT_NAME_1_, 46);
    account->setColor(_GAME_FONT_COLOR_1_);
    account->setAnchorPoint(Vec2(1, 0.5f));
    account->setPosition(Vec2(980, 830));
    m_registerLayer->addChild(account);
    
    ui::Scale9Sprite* accountBg = ui::Scale9Sprite::createWithSpriteFrameName("bg_shuru.png");
    accountBg->setContentSize(inputSize);
    accountBg->setPosition(Vec2(1350, 830));
    m_registerLayer->addChild(accountBg);
  
    m_Account = cocos2d::ui::EditBox::create(inputSize, INPUT_BACKGROUND);
    m_Account->setFontName(_GAME_FONT_NAME_1_);
    m_Account->setFontSize(46);
    m_Account->setFontColor(Color3B::WHITE);
    m_Account->setPlaceholderFontName(_GAME_FONT_NAME_1_);
    m_Account->setPlaceHolder("请输入账号");
    m_Account->setPlaceholderFontSize(46);
    m_Account->setInputMode(cocos2d::ui::EditBox::InputMode::ANY);
    m_Account->setPlaceholderFontColor(_GAME_FONT_COLOR_2_);
    m_Account->setPosition(Vec2(1370, 825));
    m_Account->setMaxLength(16);
    m_registerLayer->addChild(m_Account);
    
    Label *nickName = Label::createWithSystemFont("昵称: ", _GAME_FONT_NAME_1_, 46);
    nickName->setColor(_GAME_FONT_COLOR_1_);
    nickName->setAnchorPoint(Vec2(1, 0.5f));
    nickName->setPosition(Vec2(980, 730));
    m_registerLayer->addChild(nickName);
    
    ui::Scale9Sprite* nickNameBg = ui::Scale9Sprite::createWithSpriteFrameName("bg_shuru.png");
    nickNameBg->setContentSize(inputSize);
    nickNameBg->setPosition(Vec2(1350, 730));
    m_registerLayer->addChild(nickNameBg);
    
    m_NickName = cocos2d::ui::EditBox::create(inputSize, INPUT_BACKGROUND);
    m_NickName->setFontName(_GAME_FONT_NAME_1_);
    m_NickName->setFontSize(46);
    m_NickName->setFontColor(Color3B::WHITE);
    m_NickName->setPlaceholderFontName(_GAME_FONT_NAME_1_);
    m_NickName->setPlaceHolder("请输入昵称");
    m_NickName->setPlaceholderFontSize(46);
    m_NickName->setInputMode(cocos2d::ui::EditBox::InputMode::ANY);
    m_NickName->setPlaceholderFontColor(_GAME_FONT_COLOR_2_);
    m_NickName->setPosition(Vec2(1370, 725));
    m_NickName->setMaxLength(16);
    m_registerLayer->addChild(m_NickName);
    
    //设置密码
    Label *pass = Label::createWithSystemFont("设置密码: ", _GAME_FONT_NAME_1_, 46);
    pass->setColor(_GAME_FONT_COLOR_1_);
    pass->setAnchorPoint(Vec2(1, 0.5f));
    pass->setPosition(Vec2(980, 630));
    m_registerLayer->addChild(pass);
    
    ui::Scale9Sprite* passBg = ui::Scale9Sprite::createWithSpriteFrameName("bg_shuru.png");
    passBg->setContentSize(inputSize);
    passBg->setPosition(Vec2(1350, 630));
    m_registerLayer->addChild(passBg);
    
    m_RegPass = cocos2d::ui::EditBox::create(inputSize, INPUT_BACKGROUND);
    m_RegPass->setFontName(_GAME_FONT_NAME_1_);
    m_RegPass->setFontSize(46);
    m_RegPass->setFontColor(Color3B::WHITE);
    m_RegPass->setPlaceholderFontName(_GAME_FONT_NAME_1_);
    m_RegPass->setPlaceHolder("请输入密码");
    m_RegPass->setPlaceholderFontSize(46);
    m_RegPass->setInputMode(cocos2d::ui::EditBox::InputMode::ANY);
    m_RegPass->setInputFlag(cocos2d::ui::EditBox::InputFlag::PASSWORD);
    m_RegPass->setPlaceholderFontColor(_GAME_FONT_COLOR_2_);
    m_RegPass->setPosition(Vec2(1370, 625));
    m_RegPass->setMaxLength(15);
    m_registerLayer->addChild(m_RegPass);
    
    //确认密码
    Label *passA = Label::createWithSystemFont("确认密码: ", _GAME_FONT_NAME_1_, 46);
    passA->setColor(_GAME_FONT_COLOR_1_);
    passA->setAnchorPoint(Vec2(1, 0.5f));
    passA->setPosition(Vec2(980, 530));
    m_registerLayer->addChild(passA);
    
    ui::Scale9Sprite* passABg = ui::Scale9Sprite::createWithSpriteFrameName("bg_shuru.png");
    passABg->setContentSize(inputSize);
    passABg->setPosition(Vec2(1350, 530));
    m_registerLayer->addChild(passABg);
    
    m_RegPassA = cocos2d::ui::EditBox::create(inputSize, INPUT_BACKGROUND);
    m_RegPassA->setFontName(_GAME_FONT_NAME_1_);
    m_RegPassA->setFontSize(46);
    m_RegPassA->setFontColor(Color3B::WHITE);
    m_RegPassA->setPlaceholderFontName(_GAME_FONT_NAME_1_);
    m_RegPassA->setPlaceHolder("请再次确认密码");
    m_RegPassA->setPlaceholderFontSize(46);
    m_RegPassA->setInputMode(cocos2d::ui::EditBox::InputMode::ANY);
    m_RegPassA->setInputFlag(cocos2d::ui::EditBox::InputFlag::PASSWORD);
    m_RegPassA->setPlaceholderFontColor(_GAME_FONT_COLOR_2_);
    m_RegPassA->setPosition(Vec2(1370, 525));
    m_RegPassA->setMaxLength(15);
    m_registerLayer->addChild(m_RegPassA);

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    if (g_GlobalUnits.m_SwitchYZ != 0)
    {
        //身份证
        Label *ID = Label::createWithSystemFont("身份证: ", _GAME_FONT_NAME_1_, 46);
        ID->setColor(_GAME_FONT_COLOR_1_);
        ID->setAnchorPoint(Vec2(1, 0.5f));
        ID->setPosition(Vec2(980, 430));
        m_registerLayer->addChild(ID);
        
        ui::Scale9Sprite* IDBg = ui::Scale9Sprite::createWithSpriteFrameName("bg_shuru.png");
        IDBg->setContentSize(inputSize);
        IDBg->setPosition(Vec2(1350, 430));
        m_registerLayer->addChild(IDBg);
        
        m_IDNum = cocos2d::ui::EditBox::create(inputSize, INPUT_BACKGROUND);
        m_IDNum->setFontName(_GAME_FONT_NAME_1_);
        m_IDNum->setFontSize(46);
        m_IDNum->setFontColor(Color3B::WHITE);
        m_IDNum->setPlaceholderFontName(_GAME_FONT_NAME_1_);
        m_IDNum->setPlaceHolder("请输入身份证");
        m_IDNum->setPlaceholderFontSize(46);
        m_IDNum->setInputMode(cocos2d::ui::EditBox::InputMode::ANY);
        m_IDNum->setPlaceholderFontColor(_GAME_FONT_COLOR_2_);
        m_IDNum->setPosition(Vec2(1370, 425));
        m_IDNum->setMaxLength(18);
        m_registerLayer->addChild(m_IDNum);
        
        //phone
        Label *phone = Label::createWithSystemFont("手机号: ", _GAME_FONT_NAME_1_, 46);
        phone->setColor(_GAME_FONT_COLOR_1_);
        phone->setAnchorPoint(Vec2(1, 0.5f));
        phone->setPosition(Vec2(980, 330));
        m_registerLayer->addChild(phone);
        
        ui::Scale9Sprite* phoneBg = ui::Scale9Sprite::createWithSpriteFrameName("bg_shuru.png");
        phoneBg->setContentSize(inputSize);
        phoneBg->setPosition(Vec2(1350, 330));
        m_registerLayer->addChild(phoneBg);
        
        m_Phone = cocos2d::ui::EditBox::create(inputSize, INPUT_BACKGROUND);
        m_Phone->setFontName(_GAME_FONT_NAME_1_);
        m_Phone->setFontSize(46);
        m_Phone->setFontColor(Color3B::WHITE);
        m_Phone->setPlaceholderFontName(_GAME_FONT_NAME_1_);
        m_Phone->setPlaceHolder("请输入手机号");
        m_Phone->setPlaceholderFontSize(46);
        m_Phone->setInputMode(cocos2d::ui::EditBox::InputMode::DECIMAL);
        m_Phone->setPlaceholderFontColor(_GAME_FONT_COLOR_2_);
        m_Phone->setPosition(Vec2(1370, 325));
        m_Phone->setMaxLength(11);
        m_registerLayer->addChild(m_Phone);
        //QQ
      
        Label *QQ = Label::createWithSystemFont("Q Q: ", _GAME_FONT_NAME_1_, 46);
        QQ->setColor(_GAME_FONT_COLOR_1_);
        QQ->setAnchorPoint(Vec2(1, 0.5f));
        QQ->setPosition(Vec2(980, 230));
        m_registerLayer->addChild(QQ);
        
        ui::Scale9Sprite* QQBg = ui::Scale9Sprite::createWithSpriteFrameName("bg_shuru.png");
        QQBg->setContentSize(inputSize);
        QQBg->setPosition(Vec2(1350, 230));
        m_registerLayer->addChild(QQBg);
        
        m_QQ = cocos2d::ui::EditBox::create(inputSize, INPUT_BACKGROUND);
        m_QQ->setFontName(_GAME_FONT_NAME_1_);
        m_QQ->setFontSize(46);
        m_QQ->setFontColor(Color3B::WHITE);
        m_QQ->setPlaceholderFontName(_GAME_FONT_NAME_1_);
        m_QQ->setPlaceHolder("请输入QQ号");
        m_QQ->setPlaceholderFontSize(46);
        m_QQ->setInputMode(cocos2d::ui::EditBox::InputMode::DECIMAL);
        m_QQ->setPlaceholderFontColor(_GAME_FONT_COLOR_2_);
        m_QQ->setPosition(Vec2(1370, 225));
        m_QQ->setMaxLength(18);
        m_registerLayer->addChild(m_QQ);
    }
#else
    //身份证
    Label *ID = Label::createWithSystemFont("身份证: ", _GAME_FONT_NAME_1_, 46);
    ID->setColor(_GAME_FONT_COLOR_1_);
    ID->setAnchorPoint(Vec2(1, 0.5f));
    ID->setPosition(Vec2(980, 430));
    m_registerLayer->addChild(ID);
    
    ui::Scale9Sprite* IDBg = ui::Scale9Sprite::createWithSpriteFrameName("bg_shuru.png");
    IDBg->setContentSize(inputSize);
    IDBg->setPosition(Vec2(1350, 430));
    m_registerLayer->addChild(IDBg);
    
    m_IDNum = cocos2d::ui::EditBox::create(inputSize, INPUT_BACKGROUND);
    m_IDNum->setFontName(_GAME_FONT_NAME_1_);
    m_IDNum->setFontSize(46);
    m_IDNum->setFontColor(Color3B::WHITE);
    m_IDNum->setPlaceholderFontName(_GAME_FONT_NAME_1_);
    m_IDNum->setPlaceHolder("请输入身份证");
    m_IDNum->setPlaceholderFontSize(46);
    m_IDNum->setInputMode(cocos2d::ui::EditBox::InputMode::ANY);
    m_IDNum->setPlaceholderFontColor(_GAME_FONT_COLOR_2_);
    m_IDNum->setPosition(Vec2(1370, 425));
    m_IDNum->setMaxLength(18);
    m_registerLayer->addChild(m_IDNum);
    
    //phone
    Label *phone = Label::createWithSystemFont("手机号: ", _GAME_FONT_NAME_1_, 46);
    phone->setColor(_GAME_FONT_COLOR_1_);
    phone->setAnchorPoint(Vec2(1, 0.5f));
    phone->setPosition(Vec2(980, 330));
    m_registerLayer->addChild(phone);
    
    ui::Scale9Sprite* phoneBg = ui::Scale9Sprite::createWithSpriteFrameName("bg_shuru.png");
    phoneBg->setContentSize(inputSize);
    phoneBg->setPosition(Vec2(1350, 330));
    m_registerLayer->addChild(phoneBg);
    
    m_Phone = cocos2d::ui::EditBox::create(inputSize, INPUT_BACKGROUND);
    m_Phone->setFontName(_GAME_FONT_NAME_1_);
    m_Phone->setFontSize(46);
    m_Phone->setFontColor(Color3B::WHITE);
    m_Phone->setPlaceholderFontName(_GAME_FONT_NAME_1_);
    m_Phone->setPlaceHolder("请输入手机号");
    m_Phone->setPlaceholderFontSize(46);
    m_Phone->setInputMode(cocos2d::ui::EditBox::InputMode::DECIMAL);
    m_Phone->setPlaceholderFontColor(_GAME_FONT_COLOR_2_);
    m_Phone->setPosition(Vec2(1370, 325));
    m_Phone->setMaxLength(11);
    m_registerLayer->addChild(m_Phone);
    //QQ
    
    Label *QQ = Label::createWithSystemFont("Q Q: ", _GAME_FONT_NAME_1_, 46);
    QQ->setColor(_GAME_FONT_COLOR_1_);
    QQ->setAnchorPoint(Vec2(1, 0.5f));
    QQ->setPosition(Vec2(980, 230));
    m_registerLayer->addChild(QQ);
    
    ui::Scale9Sprite* QQBg = ui::Scale9Sprite::createWithSpriteFrameName("bg_shuru.png");
    QQBg->setContentSize(inputSize);
    QQBg->setPosition(Vec2(1350, 230));
    m_registerLayer->addChild(QQBg);
    
    m_QQ = cocos2d::ui::EditBox::create(inputSize, INPUT_BACKGROUND);
    m_QQ->setFontName(_GAME_FONT_NAME_1_);
    m_QQ->setFontSize(46);
    m_QQ->setFontColor(Color3B::WHITE);
    m_QQ->setPlaceholderFontName(_GAME_FONT_NAME_1_);
    m_QQ->setPlaceHolder("请输入QQ号");
    m_QQ->setPlaceholderFontSize(46);
    m_QQ->setInputMode(cocos2d::ui::EditBox::InputMode::DECIMAL);
    m_QQ->setPlaceholderFontColor(_GAME_FONT_COLOR_2_);
    m_QQ->setPosition(Vec2(1370, 225));
    m_QQ->setMaxLength(18);
    m_registerLayer->addChild(m_QQ);
#endif
    
    Menu* upgradeBtn = CreateButton("btn_queding", Vec2(1250, 120), _BtnUpgrade);
    m_registerLayer->addChild(upgradeBtn);
}

void GameUserInfo::initChangeBankPass()
{
    m_BankLayer = Layer::create();
    addChild(m_BankLayer);
    
    Label *pass = Label::createWithSystemFont("旧密码: ", _GAME_FONT_NAME_1_, 46);
    pass->setColor(_GAME_FONT_COLOR_1_);
    pass->setPosition(Vec2(880, 700));
    m_BankLayer->addChild(pass);
    
    ui::Scale9Sprite* passOldBg = ui::Scale9Sprite::createWithSpriteFrameName("bg_shuru.png");
    passOldBg->setContentSize(Size(700, 100));
    passOldBg->setPosition(Vec2(1350, 700));
    m_BankLayer->addChild(passOldBg);
    
    const Size inputSize = Size(620,100);
    m_BankPassOld = cocos2d::ui::EditBox::create(inputSize, INPUT_BACKGROUND);
    m_BankPassOld->setFontName(_GAME_FONT_NAME_1_);
    m_BankPassOld->setFontSize(46);
    m_BankPassOld->setFontColor(Color3B::WHITE);
    m_BankPassOld->setPlaceholderFontName(_GAME_FONT_NAME_1_);
    m_BankPassOld->setPlaceHolder("请输入旧保险柜密码");
    m_BankPassOld->setPlaceholderFontSize(46);
    m_BankPassOld->setInputMode(cocos2d::ui::EditBox::InputMode::ANY);
    m_BankPassOld->setInputFlag(cocos2d::ui::EditBox::InputFlag::PASSWORD);
    m_BankPassOld->setPlaceholderFontColor(_GAME_FONT_COLOR_2_);
    m_BankPassOld->setPosition(Vec2(1350, 700));
    m_BankPassOld->setMaxLength(15);
    m_BankLayer->addChild(m_BankPassOld);
    
    //设置密码
    Label *passN = Label::createWithSystemFont("新密码: ", _GAME_FONT_NAME_1_, 46);
    passN->setColor(_GAME_FONT_COLOR_1_);
    passN->setPosition(Vec2(880, 500));
    m_BankLayer->addChild(passN);
    
    ui::Scale9Sprite* passBg = ui::Scale9Sprite::createWithSpriteFrameName("bg_shuru.png");
    passBg->setContentSize(Size(700, 100));
    passBg->setPosition(Vec2(1350, 500));
    m_BankLayer->addChild(passBg);
    
    m_BankPass = cocos2d::ui::EditBox::create(inputSize, INPUT_BACKGROUND);
    m_BankPass->setFontName(_GAME_FONT_NAME_1_);
    m_BankPass->setFontSize(46);
    m_BankPass->setFontColor(Color3B::WHITE);
    m_BankPass->setPlaceholderFontName(_GAME_FONT_NAME_1_);
    m_BankPass->setPlaceHolder("请输入新保险柜密码");
    m_BankPass->setPlaceholderFontSize(46);
    m_BankPass->setInputMode(cocos2d::ui::EditBox::InputMode::ANY);
    m_BankPass->setInputFlag(cocos2d::ui::EditBox::InputFlag::PASSWORD);
    m_BankPass->setPlaceholderFontColor(_GAME_FONT_COLOR_2_);
    m_BankPass->setPosition(Vec2(1350, 500));
    m_BankPass->setMaxLength(15);
    m_BankLayer->addChild(m_BankPass);
    
    //确认密码
    Label *passA = Label::createWithSystemFont("确认密码: ", _GAME_FONT_NAME_1_, 46);
    passA->setColor(_GAME_FONT_COLOR_1_);
    passA->setPosition(Vec2(880, 380));
    m_BankLayer->addChild(passA);
    
    ui::Scale9Sprite* passABg = ui::Scale9Sprite::createWithSpriteFrameName("bg_shuru.png");
    passABg->setContentSize(Size(700, 100));
    passABg->setPosition(Vec2(1350, 380));
    m_BankLayer->addChild(passABg);
    
    m_BankPassA = cocos2d::ui::EditBox::create(inputSize, INPUT_BACKGROUND);
    m_BankPassA->setFontName(_GAME_FONT_NAME_1_);
    m_BankPassA->setFontSize(46);
    m_BankPassA->setFontColor(Color3B::WHITE);
    m_BankPassA->setPlaceholderFontName(_GAME_FONT_NAME_1_);
    m_BankPassA->setPlaceHolder("请再次确认密码");
    m_BankPassA->setPlaceholderFontSize(46);
    m_BankPassA->setInputMode(cocos2d::ui::EditBox::InputMode::ANY);
    m_BankPassA->setInputFlag(cocos2d::ui::EditBox::InputFlag::PASSWORD);
    m_BankPassA->setPlaceholderFontColor(_GAME_FONT_COLOR_2_);
    m_BankPassA->setPosition(Vec2(1350, 380));
    m_BankPassA->setMaxLength(15);
    m_BankLayer->addChild(m_BankPassA);
    
    Menu* upgradeBtn = CreateButton("btn_queding", Vec2(1250, 200), _BtnChangeBank);
    m_BankLayer->addChild(upgradeBtn);
}

void GameUserInfo::initChangeLoginPass()
{
    m_LoginLayer = Layer::create();
    addChild(m_LoginLayer);
    
    Label *pass = Label::createWithSystemFont("旧密码: ", _GAME_FONT_NAME_1_, 46);
    pass->setColor(_GAME_FONT_COLOR_1_);
    pass->setPosition(Vec2(880, 700));
    m_LoginLayer->addChild(pass);
    
    ui::Scale9Sprite* passOldBg = ui::Scale9Sprite::createWithSpriteFrameName("bg_shuru.png");
    passOldBg->setContentSize(Size(700, 100));
    passOldBg->setPosition(Vec2(1350, 700));
    m_LoginLayer->addChild(passOldBg);
    
    const Size inputSize = Size(620,100);
    m_LoginPassOld = cocos2d::ui::EditBox::create(inputSize, INPUT_BACKGROUND);
    m_LoginPassOld->setFontName(_GAME_FONT_NAME_1_);
    m_LoginPassOld->setFontSize(46);
    m_LoginPassOld->setFontColor(Color3B::WHITE);
    m_LoginPassOld->setPlaceholderFontName(_GAME_FONT_NAME_1_);
    m_LoginPassOld->setPlaceHolder("请输入旧登录密码");
    m_LoginPassOld->setPlaceholderFontSize(46);
    m_LoginPassOld->setInputMode(cocos2d::ui::EditBox::InputMode::ANY);
    m_LoginPassOld->setInputFlag(cocos2d::ui::EditBox::InputFlag::PASSWORD);
    m_LoginPassOld->setPlaceholderFontColor(_GAME_FONT_COLOR_2_);
    m_LoginPassOld->setPosition(Vec2(1350, 700));
    m_LoginPassOld->setMaxLength(15);
    m_LoginLayer->addChild(m_LoginPassOld);
    
    //设置密码
    Label *passN = Label::createWithSystemFont("新密码: ", _GAME_FONT_NAME_1_, 46);
    passN->setColor(_GAME_FONT_COLOR_1_);
    passN->setPosition(Vec2(880, 500));
    m_LoginLayer->addChild(passN);
    
    ui::Scale9Sprite* passBg = ui::Scale9Sprite::createWithSpriteFrameName("bg_shuru.png");
    passBg->setContentSize(Size(700, 100));
    passBg->setPosition(Vec2(1350, 500));
    m_LoginLayer->addChild(passBg);
    
    m_LoginPass = cocos2d::ui::EditBox::create(inputSize, INPUT_BACKGROUND);
    m_LoginPass->setFontName(_GAME_FONT_NAME_1_);
    m_LoginPass->setFontSize(46);
    m_LoginPass->setFontColor(Color3B::WHITE);
    m_LoginPass->setPlaceholderFontName(_GAME_FONT_NAME_1_);
    m_LoginPass->setPlaceHolder("请输入新登录密码");
    m_LoginPass->setPlaceholderFontSize(46);
    m_LoginPass->setInputMode(cocos2d::ui::EditBox::InputMode::ANY);
    m_LoginPass->setInputFlag(cocos2d::ui::EditBox::InputFlag::PASSWORD);
    m_LoginPass->setPlaceholderFontColor(_GAME_FONT_COLOR_2_);
    m_LoginPass->setPosition(Vec2(1350, 500));
    m_LoginPass->setMaxLength(15);
    m_LoginLayer->addChild(m_LoginPass);
    
    //确认密码
    Label *passA = Label::createWithSystemFont("确认密码: ", _GAME_FONT_NAME_1_, 46);
    passA->setColor(_GAME_FONT_COLOR_1_);
    passA->setPosition(Vec2(880, 380));
    m_LoginLayer->addChild(passA);
    
    ui::Scale9Sprite* passABg = ui::Scale9Sprite::createWithSpriteFrameName("bg_shuru.png");
    passABg->setContentSize(Size(700, 100));
    passABg->setPosition(Vec2(1350, 380));
    m_LoginLayer->addChild(passABg);
    
    m_LoginPassA = cocos2d::ui::EditBox::create(inputSize, INPUT_BACKGROUND);
    m_LoginPassA->setFontName(_GAME_FONT_NAME_1_);
    m_LoginPassA->setFontSize(46);
    m_LoginPassA->setFontColor(Color3B::WHITE);
    m_LoginPassA->setPlaceholderFontName(_GAME_FONT_NAME_1_);
    m_LoginPassA->setPlaceHolder("请再次确认密码");
    m_LoginPassA->setPlaceholderFontSize(46);
    m_LoginPassA->setInputMode(cocos2d::ui::EditBox::InputMode::ANY);
    m_LoginPassA->setInputFlag(cocos2d::ui::EditBox::InputFlag::PASSWORD);
    m_LoginPassA->setPlaceholderFontColor(_GAME_FONT_COLOR_2_);
    m_LoginPassA->setPosition(Vec2(1350, 380));
    m_LoginPassA->setMaxLength(15);
    m_LoginLayer->addChild(m_LoginPassA);
    
    Menu* upgradeBtn = CreateButton("btn_queding", Vec2(1250, 200), _BtnChangeLogin);
    m_LoginLayer->addChild(upgradeBtn);
}

void GameUserInfo::SetUserInfo(char* nickname, int gameid, int faceid)
{
	Node* temp = getChildByTag(m_BackTga);
	if(temp == NULL) return;

    Sprite* headBg = Sprite::createWithSpriteFrameName("touxiangkuang.png");
    headBg->setPosition(Vec2(445, 620));
    headBg->setScale(1.5f);
    addChild(headBg, 2, 2003);
    
    ClippingNode *node = ClippingNode::create(Sprite::createWithSpriteFrameName("touxiang.png"));
    node->setInverted(false);
    node->setAlphaThreshold(0.5f);
    node->setPosition(Vec2(445, 620));
    addChild(node,1, 2006);
    
    string heads = g_GlobalUnits.getFace(g_GlobalUnits.GetGolbalUserData().wGender, g_GlobalUnits.GetGolbalUserData().lInsureScore);
    Sprite* mHead = Sprite::createWithSpriteFrameName(heads);
    mHead->setPosition(Vec2::ZERO);
    mHead->setTag(2000);
    node->addChild(mHead);
    node->setScale(1.5f);
    
    Sprite* goldBg = Sprite::createWithSpriteFrameName("label_money.png");
    goldBg->setPosition(Vec2(445,430));
    addChild(goldBg);
    
    auto str = StringUtils::toString(g_GlobalUnits.GetGolbalUserData().lInsureScore);
    Label* goldLabel = Label::createWithSystemFont(str, _GAME_FONT_NAME_1_, 46);
    goldLabel->setAnchorPoint(Vec2(0, 0.5f));
    goldLabel->setPosition(Vec2(295, 430));
    addChild(goldLabel);
    
    if (g_GlobalUnits.m_UserLoginInfo.isTourist)
    {
        char TempStr[128]={0};
        Label *pLabel = nullptr;
        
        pLabel= Label::createWithSystemFont("账号: 游客登录", _GAME_FONT_NAME_1_, 46);
        pLabel->setPosition(Vec2(445, 300));
        temp->addChild(pLabel);
        
        //id
        sprintf(TempStr , "ID: %d", gameid);
        pLabel= Label::createWithSystemFont(TempStr, _GAME_FONT_NAME_1_, 46);
        pLabel->setPosition(Vec2(445, 200));
        temp->addChild(pLabel);
    }
    else if (g_GlobalUnits.m_UserLoginInfo.isWxUser)
    {
        char TempStr[128]={0};
        Label *pLabel = nullptr;
        
        sprintf(TempStr , "账号: %s", g_GlobalUnits.GetGolbalUserData().szNickName);
        pLabel= Label::createWithSystemFont(TempStr, _GAME_FONT_NAME_1_, 46);
        pLabel->setPosition(Vec2(445, 300));
        temp->addChild(pLabel);

        //id
        sprintf(TempStr , "ID: %d", gameid);
        pLabel= Label::createWithSystemFont(TempStr, _GAME_FONT_NAME_1_, 46);
        pLabel->setPosition(Vec2(445, 200));
        temp->addChild(pLabel);
    }
    else
    {
        m_ChangeLoginPass = CreateButton("btn_changeAccountPass", Vec2(300, 250), _BtnLogin);
        addChild(m_ChangeLoginPass, 10);
        m_ChangeLoginPass->setEnabled(false);
        m_ChangeLoginPass->setColor(Color3B(100, 100, 100));
        
        m_ChangeBankPass = CreateButton("btn_changebankPass", Vec2(590, 250), _BtnBank);
        addChild(m_ChangeBankPass);
    }
}

void GameUserInfo::close()
{
    this->removeFromParent();
}

void GameUserInfo::onRemove()
{
    this->removeFromParent();
}

void GameUserInfo::onEnter()
{
	GameLayer::onEnter();
}

void GameUserInfo::onExit()
{
	GameLayer::onExit();
	Tools::removeSpriteFrameCache("GameUserInfo.plist");
}

void GameUserInfo::callbackBt(Ref *pSender )
{
	Node *pNode = (Node *)pSender;
	switch (pNode->getTag())
	{
	case _BtnClose:
		{
			SoundUtil::sharedEngine()->playEffect("buttonMusic");
			onRemove();
			break;
		}
    case _BtnUpgrade:
        {
            SoundUtil::sharedEngine()->playEffect("buttonMusic");
            const char* IDCard;
            const char* QQNum;
            const char* phoneNum;
            
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
            if (g_GlobalUnits.m_SwitchYZ == 0)
            {
                IDCard = "111111111111111111";
                QQNum = "1111111111";
                phoneNum = "11111111111";
            }
            else
            {
                IDCard = m_IDNum->getText();
                QQNum = m_QQ->getText();
                phoneNum = m_Phone->getText();
            }
#else
            IDCard = m_IDNum->getText();
            QQNum = m_QQ->getText();
            phoneNum = m_Phone->getText();
#endif
            const char* pass = m_RegPass->getText();
            const char* passA = m_RegPassA->getText();
            const char* account = m_Account->getText();
            const char* nickName = m_NickName->getText();
            
            if (false == checkNameNomative(account))
                return;
            
            if (false == checkNameNomative(nickName, true))
                return;

            if (strlen(pass) == 0)
            {
                AlertMessageLayer::createConfirm("\u5bc6\u7801\u4e0d\u80fd\u4e3a\u7a7a\uff01");
                return;
            }
            
            if (strcmp(pass, passA) != 0)
            {
                AlertMessageLayer::createConfirm("\u5bc6\u7801\u4e0e\u786e\u8ba4\u5bc6\u7801\u4e0d\u540c\uff01");
                return;
            }
            
            if (strlen(IDCard) == 0)
            {
                AlertMessageLayer::createConfirm("\u8eab\u4efd\u8bc1\u4e0d\u80fd\u4e3a\u7a7a\uff01");
                return;
            }

            //qq
            if (strlen(QQNum) == 0)
            {
                AlertMessageLayer::createConfirm("QQ\u4e0d\u80fd\u4e3a\u7a7a\uff01");
                return;
            }
            
            if (strlen(phoneNum) != 11)
            {
                AlertMessageLayer::createConfirm("\u65e0\u6548\u624b\u673a\u53f7\uff01");
                return;
            }
            
            CMD_MB_ModificationVisitor midify;
            memset(&midify, 0, sizeof(CMD_MB_ModificationVisitor));
            sprintf(midify.szAccounts,"%s",utf8_gbk(account).c_str());
            sprintf(midify.szNickname,"%s",utf8_gbk(nickName).c_str());
            
            char buff[50];
            char md5Buff[50];
            
            memset(buff, 0, sizeof(buff));
            memset(md5Buff, 0, sizeof(buff));
            sprintf(buff, PWDHEAD, pass);
            CMD5Encrypt::EncryptData(buff, md5Buff);
            
            sprintf(midify.szPassword, "%s", md5Buff);
            char szMachineID[33];
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
            const char* p = Application::getInstance()->GetUUID();
            if(p == NULL)  sprintf(szMachineID,"%s","test");
            else  CMD5Encrypt::EncryptData(p, szMachineID);
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
            char* p = JniSink::share()->GetDeviceID();
            if(p == NULL)  sprintf(szMachineID,"%s","test");
            else  CMD5Encrypt::EncryptData(p, szMachineID);
#endif
            sprintf(midify.szMachineID,"%s",szMachineID);
            sprintf(midify.szIDCard,"%s",IDCard);
            sprintf(midify.szQQNumber,"%s",QQNum);
            sprintf(midify.szPhoneNum,"%s",phoneNum);
            
            midify.lToken = g_GlobalUnits.GetGolbalUserData().lWebToken;
            LobbySocketSink::sharedSocketSink()->ConnectServer();
            LobbySocketSink::sharedLoginSocket()->SendData(MDM_MB_LOGON, SUB_MB_MODIFICATION_VISITOR, &midify,sizeof(CMD_MB_ModificationVisitor));
            
            sprintf(g_GlobalUnits.m_UserLoginInfo.szAccounts,"%s", utf8_gbk(m_Account->getText()).c_str());
            sprintf(g_GlobalUnits.m_UserLoginInfo.pszPassword,"%s",pass);
            LoadLayer::create(m_pGameScene);
            break;
        }
        case _BtnLogin:
        {
            m_ChangeLoginPass->setEnabled(false);
            m_ChangeLoginPass->setColor(Color3B(100, 100, 100));
            m_ChangeBankPass->setEnabled(true);
            m_ChangeBankPass->setColor(Color3B::WHITE);
            m_LoginLayer->setVisible(true);
            m_BankLayer->setVisible(false);
            
            break;
        }
        case _BtnBank:
        {
            m_ChangeLoginPass->setEnabled(true);
            m_ChangeLoginPass->setColor(Color3B::WHITE);
            m_ChangeBankPass->setEnabled(false);
            m_ChangeBankPass->setColor(Color3B(100, 100, 100));
            m_LoginLayer->setVisible(false);
            m_BankLayer->setVisible(true);
            
            break;
        }
        case _BtnChangeLogin:
        {
            const char* oldPass = m_LoginPassOld->getText();
            const char* pass = m_LoginPass->getText();
            const char* passA = m_LoginPassA->getText();
            if (strlen(pass) == 0)
            {
                AlertMessageLayer::createConfirm("\u65b0\u5bc6\u7801\u4e0d\u80fd\u4e3a\u7a7a\uff01");
                return;
            }
            
            if (strcmp(pass, passA) != 0)
            {
                AlertMessageLayer::createConfirm("\u4e24\u6b21\u8f93\u5165\u7684\u5bc6\u7801\u4e0d\u4e00\u6837\uff01");
                return;
            }
            CMD_GF_ModifyPassword temp;
            temp.dwUserID = g_GlobalUnits.GetUserID();
            temp.nType = 1;
            
            char buff[50];
            memset(buff, 0, sizeof(buff));
            sprintf(buff, PWDHEAD, oldPass);
            CMD5Encrypt::EncryptData(buff, temp.szOldPwd);
            
            memset(buff, 0, sizeof(buff));
            sprintf(buff, PWDHEAD, pass);
            CMD5Encrypt::EncryptData(buff, temp.szNewPwd);
            
            temp.lToken = g_GlobalUnits.GetGolbalUserData().lWebToken;
            
            LobbySocketSink::sharedSocketSink()->ConnectServer();
            LobbySocketSink::sharedLoginSocket()->SendData(MDM_GF_BANK, SUB_GF_MODIFY_PASSWORD, &temp, sizeof(CMD_GF_ModifyPassword));
            
            sprintf(g_GlobalUnits.m_UserLoginInfo.pszPassword,"%s",pass);
            g_GlobalUnits.InsertUserData(g_GlobalUnits.m_UserLoginInfo);
            g_GlobalUnits.SaveUserData();
            break;
        }
        case _BtnChangeBank:
        {
            const char* oldPass = m_BankPassOld->getText();
            const char* pass = m_BankPass->getText();
            const char* passA = m_BankPassA->getText();
            if (strlen(pass) == 0)
            {
                AlertMessageLayer::createConfirm("\u65b0\u5bc6\u7801\u4e0d\u80fd\u4e3a\u7a7a\uff01");
                return;
            }
            
            if (strcmp(pass, passA) != 0)
            {
                AlertMessageLayer::createConfirm("\u4e24\u6b21\u8f93\u5165\u7684\u5bc6\u7801\u4e0d\u4e00\u6837\uff01");
                return;
            }
            CMD_GF_ModifyPassword temp;
            temp.dwUserID = g_GlobalUnits.GetUserID();
            temp.nType = 2;
            
            char buff[50];
            memset(buff, 0, sizeof(buff));
            sprintf(buff, PWDHEAD, oldPass);
            CMD5Encrypt::EncryptData(buff, temp.szOldPwd);
            
            memset(buff, 0, sizeof(buff));
            sprintf(buff, PWDHEAD, pass);
            CMD5Encrypt::EncryptData(buff, temp.szNewPwd);
            
            temp.lToken = g_GlobalUnits.GetGolbalUserData().lWebToken;
            
            LobbySocketSink::sharedSocketSink()->ConnectServer();
            LobbySocketSink::sharedLoginSocket()->SendData(MDM_GF_BANK, SUB_GF_MODIFY_PASSWORD, &temp, sizeof(CMD_GF_ModifyPassword));
            break;
        }
    }
}

Menu* GameUserInfo::CreateButton( std::string szBtName ,const Vec2 &p , int tag )
{
	Menu *pBT = Tools::Button(StrToChar(szBtName+"_normal.png") , StrToChar(szBtName+"_click.png") , p, this , menu_selector(GameUserInfo::callbackBt) , tag);
	return pBT;
}

bool GameUserInfo::checkNameNomative(const string& text, bool isNickname)
{
    if (text == "")
    {
        if (true == isNickname)
        {
            //昵称不能为空！
            AlertMessageLayer::createConfirm(this, "\u6635\u79f0\u4e0d\u80fd\u4e3a\u7a7a");
            
        }
        else
        {
            AlertMessageLayer::createConfirm(this, "\u8d26\u53f7\u4e3a\u7a7a\u662f\u65e0\u6548\u7684\u54e6~~");
        }
        return false;
    }
    
    //在苹果环境下需转为宽字符，因一个string中文长度为3，宽字符为1
    std::wstring name_wstring;
    name_wstring.reserve(text.length());
    utf8::utf8to16(text.begin(), text.end(),std::back_inserter(name_wstring));
    
    int chiniseSize = 0;
    int engnishSize = 0;
    for (int i =0; i != name_wstring.length(); i++) {
        // 判断是否为英文字母
        if (((name_wstring[i] >=48) && (name_wstring[i]<=57)) || ((name_wstring[i] >=65) && (name_wstring[i] <=90)) || ((name_wstring[i] >=97) && (name_wstring[i] <=122))) {
            engnishSize ++;
        }
        // 是否为汉字
        if (name_wstring[i] >127) {
            chiniseSize++;
        }
    }
    
    if (false == isNickname)
    {
        if (chiniseSize > 0)
        {
            AlertMessageLayer::createConfirm(this, "账号不能含有中文！");
            return false;
        }
        return true;
    }
    
    // 全部为汉字1~6
    if (((chiniseSize > 0) && (chiniseSize < 7)) && (engnishSize ==0)) {
        log("全部汉字：%d",chiniseSize);
    }
    // 全部英文2~10
    else if (((engnishSize >1) && (engnishSize < 17)) && (chiniseSize == 0)){
        log("全部英文：%d",engnishSize);
    }
    // 汉字加英文1~6
    else if (((chiniseSize >0) && (chiniseSize < 7)) && ((engnishSize > 0) && (engnishSize <17)) && (chiniseSize*3 + engnishSize < 17)) {
        log("汉字加英文：%d + %d",chiniseSize, engnishSize);
    }
    // 长度错误
    else {
        AlertMessageLayer::createConfirm(this, "昵称长度不能超过16个英文字符或6个汉字！");
        log("长度错误！%d + %d",chiniseSize, engnishSize);
        return false;
    }
    return true;
}
