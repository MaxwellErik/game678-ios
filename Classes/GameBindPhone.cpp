#include "GameBindPhone.h"
#include "LobbySocketSink.h"
#include "Encrypt.h"
#include "ClientSocketSink.h"
#include "JniSink.h"
#define		TABLE_CONTENT_SIZE			(CCSizeMake(776, 360))
#define		EXPLAIN_CLOSE_SIZE			(CCSizeMake(124, 57))
#define		EXPLAIN_ARROW_SIZE			(CCSizeMake(78, 102))

GameBindPhone::GameBindPhone( GameScene *pGameScene ):GameLayer(pGameScene)
{

}
GameBindPhone::~GameBindPhone()
{

}

GameBindPhone* GameBindPhone::create(GameScene *pGameScene)
{
	GameBindPhone* temp = new GameBindPhone(pGameScene);
	if(temp && temp->init())
	{
		temp->autorelease();
		return temp;
	}
	else
	{
		CC_SAFE_DELETE(temp);
		return NULL;
	}
}

bool GameBindPhone::init()
{
	if ( !Layer::init() )	return false;
	setLocalZOrder(10);
	Tools::addSpriteFrame("GameBindPhone.plist");
	setTouchEnabled(true);
    
    ui::Scale9Sprite* colorBg = ui::Scale9Sprite::create("Common/bg_gray.png");
    colorBg->setContentSize(Size(1920, 1080));
    colorBg->setPosition(_STANDARD_SCREEN_CENTER_);
    addChild(colorBg);

    auto listener = EventListenerTouchOneByOne::create();
    listener->onTouchBegan = CC_CALLBACK_2(GameBindPhone::onTouchBegan, this);
    listener->onTouchMoved = CC_CALLBACK_2(GameBindPhone::onTouchMoved, this);
    listener->onTouchEnded = CC_CALLBACK_2(GameBindPhone::onTouchEnded, this);
    Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(listener, this);
	return true;
}

void GameBindPhone::PoPLock()
{
	Sprite *pBG = Sprite::createWithSpriteFrameName("GameBindBack.png");
	pBG->setPosition(_STANDARD_SCREEN_CENTER_IN_PIXELS_);
	pBG->setTag(m_BackTga);
	addChild(pBG);

	Sprite* text = Sprite::createWithSpriteFrameName("GameBindText1.png");
	text->setPosition(_STANDARD_SCREEN_CENTER_IN_PIXELS_);
	text->setPosition(Vec2(300,150));
	pBG->addChild(text);

	Menu* pNode = CreateButton("GameBindDownOk", Vec2(480,300), _BtnDownOk);
	addChild(pNode ,0 , _BtnOk);

	pNode = CreateButton("GameBindBtnCancle", Vec2(780,300), _BtnCancel);
	addChild(pNode ,0 , _BtnCancel);
}

void GameBindPhone::PoPNeedUnLock()
{
	Sprite *pBG = Sprite::createWithSpriteFrameName("GameBindBack.png");
	pBG->setPosition(_STANDARD_SCREEN_CENTER_IN_PIXELS_);
	pBG->setTag(m_BackTga);
	addChild(pBG);

	Sprite* text = Sprite::createWithSpriteFrameName("GameBindText2.png");
	text->setPosition(_STANDARD_SCREEN_CENTER_IN_PIXELS_);
	text->setPosition(Vec2(300,150));
	pBG->addChild(text);

	Menu* pNode = CreateButton("GameBindBtnOk", Vec2(640,300), _BtnOk);
	addChild(pNode ,0 , _BtnOk);
}

void GameBindPhone::close()
{
	m_pGameScene->removeChild(this);
}

void GameBindPhone::onRemove()
{
	m_pGameScene->removeChild(this);
}

void GameBindPhone::onEnter()
{
	GameLayer::onEnter();
}

void GameBindPhone::onExit()
{
	GameLayer::onExit();
	Tools::removeSpriteFrameCache("GameBindPhone.plist");
}

void GameBindPhone::callbackBt(Ref *pSender )
{
	Node *pNode = (Node *)pSender;
	switch (pNode->getTag())
	{
	case _BtnCancel: //取消
		{
			SoundUtil::sharedEngine()->playEffect("buttonMusic");
			onRemove();
			break;
		}
	case _BtnDownOk:    //下载确定
		{
			SoundUtil::sharedEngine()->playEffect("buttonMusic");
			//JniSink::share()->SendLockPhone(g_GlobalUnits.GetGolbalUserData().dwUserID);
			//http://app.game621.com/GameLocker.apk 
			JniSink::share()->OpenSystemWeb("http://app.game776.com/GameLocker.apk");
			onRemove();
			break;
		}
	case _BtnOk:
		{
			onRemove();
			g_GlobalUnits.m_ServerListManager.ClearAllData();
			g_GlobalUnits.m_UserDataVec.clear();
			ClientSocketSink::sharedSocketSink()->CleaAllUser();
			ClientSocketSink::sharedSocketSink()->closeSocket();
			LobbySocketSink::sharedSocketSink()->closeSocket();
			LoginLayer::create(m_pGameScene);
			break;
		}
	}
}

Menu* GameBindPhone::CreateButton( std::string szBtName ,const Vec2 &p , int tag )
{
	Menu *pBT = Tools::Button(StrToChar(szBtName+"_normal.png") , StrToChar(szBtName+"_click.png") , p, this , menu_selector(GameBindPhone::callbackBt) , tag);
	return pBT;
}


Label * GameBindPhone::createLabel(const char *szText ,const Vec2 &p, int fontSize ,Color3B color, bool bChinese)
{
	std::string szTemp = szText;
	return createLabel(szTemp , p , fontSize , color , bChinese);
}
Label * GameBindPhone::createLabel(const std::string szText ,const Vec2 &p, int fontSize ,Color3B color, bool bChinese)
{

	std::string szTempTitle = szText;
	if (bChinese && szTempTitle.length() != 0)
	{
#ifdef _WIN32
		Tools::GBKToUTF8(szTempTitle , "gb2312" , "utf-8");
#endif // _WIN32
	}

	Label *pLable = Label::createWithSystemFont(szTempTitle.c_str(), _GAME_FONT_NAME_1_, fontSize);
    pLable->setHorizontalAlignment(TextHAlignment::LEFT);
	pLable->setColor(color);
	pLable->setAnchorPoint(Vec2(0,0));
	pLable->setPosition(ccppx(p));
	return pLable;
}
