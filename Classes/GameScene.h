﻿#ifndef _GAME_SCENE_H_
#define _GAME_SCENE_H_

#include "GameLayer.h"

#include "Tools.h"
#include "md5.h"
#include "DataBase64.h"

#include "AlertMessageLayer.h"



class GameScene : public Scene
{
public:

	GameScene();
	virtual ~GameScene();

	static GameScene *create(void);

	// 清除 Alert Message Layer
	void RemoveAlertMessageLayer(void);

protected:

	// 防止直接引用 
	GameScene(const GameScene&);
    GameScene& operator = (const GameScene&);
};

#endif
