#include "lua_fishgame.hpp"
#include "fishgame.h"
#include "tolua_fix.h"
#include "LuaBasicConversions.h"



int lua_fishgame_MyScene_create(lua_State* tolua_S)
{
    int argc = 0;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif

#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertable(tolua_S,1,"fishgame.MyScene",0,&tolua_err)) goto tolua_lerror;
#endif

    argc = lua_gettop(tolua_S) - 1;

    if (argc == 0)
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_fishgame_MyScene_create'", nullptr);
            return 0;
        }
        fishgame::MyScene* ret = fishgame::MyScene::create();
        object_to_luaval<fishgame::MyScene>(tolua_S, "fishgame.MyScene",(fishgame::MyScene*)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d\n ", "fishgame.MyScene:create",argc, 0);
    return 0;
#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_fishgame_MyScene_create'.",&tolua_err);
#endif
    return 0;
}
static int lua_fishgame_MyScene_finalize(lua_State* tolua_S)
{
    printf("luabindings: finalizing LUA object (MyScene)");
    return 0;
}

int lua_register_fishgame_MyScene(lua_State* tolua_S)
{
    tolua_usertype(tolua_S,"fishgame.MyScene");
    tolua_cclass(tolua_S,"MyScene","fishgame.MyScene","cc.Scene",nullptr);

    tolua_beginmodule(tolua_S,"MyScene");
        tolua_function(tolua_S,"create", lua_fishgame_MyScene_create);
    tolua_endmodule(tolua_S);
    std::string typeName = typeid(fishgame::MyScene).name();
    g_luaType[typeName] = "fishgame.MyScene";
    g_typeCast["MyScene"] = "fishgame.MyScene";
    return 1;
}

int lua_fishgame_FishObjectManager_FindFish(lua_State* tolua_S)
{
    int argc = 0;
    fishgame::FishObjectManager* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"fishgame.FishObjectManager",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (fishgame::FishObjectManager*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_fishgame_FishObjectManager_FindFish'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 1) 
    {
        unsigned long arg0;

        ok &= luaval_to_ulong(tolua_S, 2, &arg0, "fishgame.FishObjectManager:FindFish");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_fishgame_FishObjectManager_FindFish'", nullptr);
            return 0;
        }
        fishgame::Fish* ret = cobj->FindFish(arg0);
        object_to_luaval<fishgame::Fish>(tolua_S, "fishgame.Fish",(fishgame::Fish*)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "fishgame.FishObjectManager:FindFish",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_fishgame_FishObjectManager_FindFish'.",&tolua_err);
#endif

    return 0;
}
int lua_fishgame_FishObjectManager_ConvertCoortToScreenSize(lua_State* tolua_S)
{
    int argc = 0;
    fishgame::FishObjectManager* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"fishgame.FishObjectManager",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (fishgame::FishObjectManager*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_fishgame_FishObjectManager_ConvertCoortToScreenSize'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 2) 
    {
        float* arg0;
        float* arg1;

        #pragma warning NO CONVERSION TO NATIVE FOR float*
		ok = false;

        #pragma warning NO CONVERSION TO NATIVE FOR float*
		ok = false;
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_fishgame_FishObjectManager_ConvertCoortToScreenSize'", nullptr);
            return 0;
        }
        cobj->ConvertCoortToScreenSize(arg0, arg1);
        return 0;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "fishgame.FishObjectManager:ConvertCoortToScreenSize",argc, 2);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_fishgame_FishObjectManager_ConvertCoortToScreenSize'.",&tolua_err);
#endif

    return 0;
}
int lua_fishgame_FishObjectManager_Init(lua_State* tolua_S)
{
    int argc = 0;
    fishgame::FishObjectManager* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"fishgame.FishObjectManager",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (fishgame::FishObjectManager*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_fishgame_FishObjectManager_Init'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 4) 
    {
        int arg0;
        int arg1;
        int arg2;
        int arg3;

        ok &= luaval_to_int32(tolua_S, 2,(int *)&arg0, "fishgame.FishObjectManager:Init");

        ok &= luaval_to_int32(tolua_S, 3,(int *)&arg1, "fishgame.FishObjectManager:Init");

        ok &= luaval_to_int32(tolua_S, 4,(int *)&arg2, "fishgame.FishObjectManager:Init");

        ok &= luaval_to_int32(tolua_S, 5,(int *)&arg3, "fishgame.FishObjectManager:Init");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_fishgame_FishObjectManager_Init'", nullptr);
            return 0;
        }
        cobj->Init(arg0, arg1, arg2, arg3);
        return 0;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "fishgame.FishObjectManager:Init",argc, 4);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_fishgame_FishObjectManager_Init'.",&tolua_err);
#endif

    return 0;
}
int lua_fishgame_FishObjectManager_IsSwitchingScene(lua_State* tolua_S)
{
    int argc = 0;
    fishgame::FishObjectManager* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"fishgame.FishObjectManager",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (fishgame::FishObjectManager*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_fishgame_FishObjectManager_IsSwitchingScene'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_fishgame_FishObjectManager_IsSwitchingScene'", nullptr);
            return 0;
        }
        bool ret = cobj->IsSwitchingScene();
        tolua_pushboolean(tolua_S,(bool)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "fishgame.FishObjectManager:IsSwitchingScene",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_fishgame_FishObjectManager_IsSwitchingScene'.",&tolua_err);
#endif

    return 0;
}
int lua_fishgame_FishObjectManager_GetServerWidth(lua_State* tolua_S)
{
    int argc = 0;
    fishgame::FishObjectManager* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"fishgame.FishObjectManager",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (fishgame::FishObjectManager*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_fishgame_FishObjectManager_GetServerWidth'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_fishgame_FishObjectManager_GetServerWidth'", nullptr);
            return 0;
        }
        int ret = cobj->GetServerWidth();
        tolua_pushnumber(tolua_S,(lua_Number)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "fishgame.FishObjectManager:GetServerWidth",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_fishgame_FishObjectManager_GetServerWidth'.",&tolua_err);
#endif

    return 0;
}
int lua_fishgame_FishObjectManager_MirrowShow(lua_State* tolua_S)
{
    int argc = 0;
    fishgame::FishObjectManager* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"fishgame.FishObjectManager",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (fishgame::FishObjectManager*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_fishgame_FishObjectManager_MirrowShow'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_fishgame_FishObjectManager_MirrowShow'", nullptr);
            return 0;
        }
        bool ret = cobj->MirrowShow();
        tolua_pushboolean(tolua_S,(bool)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "fishgame.FishObjectManager:MirrowShow",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_fishgame_FishObjectManager_MirrowShow'.",&tolua_err);
#endif

    return 0;
}
int lua_fishgame_FishObjectManager_FindBullet(lua_State* tolua_S)
{
    int argc = 0;
    fishgame::FishObjectManager* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"fishgame.FishObjectManager",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (fishgame::FishObjectManager*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_fishgame_FishObjectManager_FindBullet'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 1) 
    {
        unsigned long arg0;

        ok &= luaval_to_ulong(tolua_S, 2, &arg0, "fishgame.FishObjectManager:FindBullet");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_fishgame_FishObjectManager_FindBullet'", nullptr);
            return 0;
        }
        fishgame::Bullet* ret = cobj->FindBullet(arg0);
        object_to_luaval<fishgame::Bullet>(tolua_S, "fishgame.Bullet",(fishgame::Bullet*)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "fishgame.FishObjectManager:FindBullet",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_fishgame_FishObjectManager_FindBullet'.",&tolua_err);
#endif

    return 0;
}
int lua_fishgame_FishObjectManager_SetGameLoaded(lua_State* tolua_S)
{
    int argc = 0;
    fishgame::FishObjectManager* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"fishgame.FishObjectManager",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (fishgame::FishObjectManager*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_fishgame_FishObjectManager_SetGameLoaded'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 1) 
    {
        bool arg0;

        ok &= luaval_to_boolean(tolua_S, 2,&arg0, "fishgame.FishObjectManager:SetGameLoaded");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_fishgame_FishObjectManager_SetGameLoaded'", nullptr);
            return 0;
        }
        cobj->SetGameLoaded(arg0);
        return 0;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "fishgame.FishObjectManager:SetGameLoaded",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_fishgame_FishObjectManager_SetGameLoaded'.",&tolua_err);
#endif

    return 0;
}
int lua_fishgame_FishObjectManager_AddFishBuff(lua_State* tolua_S)
{
    int argc = 0;
    fishgame::FishObjectManager* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"fishgame.FishObjectManager",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (fishgame::FishObjectManager*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_fishgame_FishObjectManager_AddFishBuff'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 3) 
    {
        int arg0;
        double arg1;
        double arg2;

        ok &= luaval_to_int32(tolua_S, 2,(int *)&arg0, "fishgame.FishObjectManager:AddFishBuff");

        ok &= luaval_to_number(tolua_S, 3,&arg1, "fishgame.FishObjectManager:AddFishBuff");

        ok &= luaval_to_number(tolua_S, 4,&arg2, "fishgame.FishObjectManager:AddFishBuff");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_fishgame_FishObjectManager_AddFishBuff'", nullptr);
            return 0;
        }
        cobj->AddFishBuff(arg0, arg1, arg2);
        return 0;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "fishgame.FishObjectManager:AddFishBuff",argc, 3);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_fishgame_FishObjectManager_AddFishBuff'.",&tolua_err);
#endif

    return 0;
}
int lua_fishgame_FishObjectManager_RemoveAllBullets(lua_State* tolua_S)
{
    int argc = 0;
    fishgame::FishObjectManager* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"fishgame.FishObjectManager",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (fishgame::FishObjectManager*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_fishgame_FishObjectManager_RemoveAllBullets'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 1) 
    {
        bool arg0;

        ok &= luaval_to_boolean(tolua_S, 2,&arg0, "fishgame.FishObjectManager:RemoveAllBullets");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_fishgame_FishObjectManager_RemoveAllBullets'", nullptr);
            return 0;
        }
        bool ret = cobj->RemoveAllBullets(arg0);
        tolua_pushboolean(tolua_S,(bool)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "fishgame.FishObjectManager:RemoveAllBullets",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_fishgame_FishObjectManager_RemoveAllBullets'.",&tolua_err);
#endif

    return 0;
}
int lua_fishgame_FishObjectManager_SetSwitchingScene(lua_State* tolua_S)
{
    int argc = 0;
    fishgame::FishObjectManager* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"fishgame.FishObjectManager",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (fishgame::FishObjectManager*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_fishgame_FishObjectManager_SetSwitchingScene'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 1) 
    {
        bool arg0;

        ok &= luaval_to_boolean(tolua_S, 2,&arg0, "fishgame.FishObjectManager:SetSwitchingScene");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_fishgame_FishObjectManager_SetSwitchingScene'", nullptr);
            return 0;
        }
        cobj->SetSwitchingScene(arg0);
        return 0;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "fishgame.FishObjectManager:SetSwitchingScene",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_fishgame_FishObjectManager_SetSwitchingScene'.",&tolua_err);
#endif

    return 0;
}
int lua_fishgame_FishObjectManager_Clear(lua_State* tolua_S)
{
    int argc = 0;
    fishgame::FishObjectManager* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"fishgame.FishObjectManager",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (fishgame::FishObjectManager*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_fishgame_FishObjectManager_Clear'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_fishgame_FishObjectManager_Clear'", nullptr);
            return 0;
        }
        cobj->Clear();
        return 0;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "fishgame.FishObjectManager:Clear",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_fishgame_FishObjectManager_Clear'.",&tolua_err);
#endif

    return 0;
}
int lua_fishgame_FishObjectManager_IsGameLoaded(lua_State* tolua_S)
{
    int argc = 0;
    fishgame::FishObjectManager* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"fishgame.FishObjectManager",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (fishgame::FishObjectManager*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_fishgame_FishObjectManager_IsGameLoaded'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_fishgame_FishObjectManager_IsGameLoaded'", nullptr);
            return 0;
        }
        bool ret = cobj->IsGameLoaded();
        tolua_pushboolean(tolua_S,(bool)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "fishgame.FishObjectManager:IsGameLoaded",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_fishgame_FishObjectManager_IsGameLoaded'.",&tolua_err);
#endif

    return 0;
}
int lua_fishgame_FishObjectManager_RemoveAllFishes(lua_State* tolua_S)
{
    int argc = 0;
    fishgame::FishObjectManager* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"fishgame.FishObjectManager",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (fishgame::FishObjectManager*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_fishgame_FishObjectManager_RemoveAllFishes'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 1) 
    {
        bool arg0;

        ok &= luaval_to_boolean(tolua_S, 2,&arg0, "fishgame.FishObjectManager:RemoveAllFishes");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_fishgame_FishObjectManager_RemoveAllFishes'", nullptr);
            return 0;
        }
        bool ret = cobj->RemoveAllFishes(arg0);
        tolua_pushboolean(tolua_S,(bool)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "fishgame.FishObjectManager:RemoveAllFishes",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_fishgame_FishObjectManager_RemoveAllFishes'.",&tolua_err);
#endif

    return 0;
}
int lua_fishgame_FishObjectManager_AddFish(lua_State* tolua_S)
{
    int argc = 0;
    fishgame::FishObjectManager* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"fishgame.FishObjectManager",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (fishgame::FishObjectManager*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_fishgame_FishObjectManager_AddFish'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 1) 
    {
        fishgame::Fish* arg0;

        ok &= luaval_to_object<fishgame::Fish>(tolua_S, 2, "fishgame.Fish",&arg0);
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_fishgame_FishObjectManager_AddFish'", nullptr);
            return 0;
        }
        bool ret = cobj->AddFish(arg0);
        tolua_pushboolean(tolua_S,(bool)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "fishgame.FishObjectManager:AddFish",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_fishgame_FishObjectManager_AddFish'.",&tolua_err);
#endif

    return 0;
}
int lua_fishgame_FishObjectManager_ConvertCoord(lua_State* tolua_S)
{
    int argc = 0;
    fishgame::FishObjectManager* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"fishgame.FishObjectManager",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (fishgame::FishObjectManager*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_fishgame_FishObjectManager_ConvertCoord'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 2) 
    {
        float* arg0;
        float* arg1;

        #pragma warning NO CONVERSION TO NATIVE FOR float*
		ok = false;

        #pragma warning NO CONVERSION TO NATIVE FOR float*
		ok = false;
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_fishgame_FishObjectManager_ConvertCoord'", nullptr);
            return 0;
        }
        cobj->ConvertCoord(arg0, arg1);
        return 0;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "fishgame.FishObjectManager:ConvertCoord",argc, 2);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_fishgame_FishObjectManager_ConvertCoord'.",&tolua_err);
#endif

    return 0;
}
int lua_fishgame_FishObjectManager_GetAllFishes(lua_State* tolua_S)
{
    int argc = 0;
    fishgame::FishObjectManager* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"fishgame.FishObjectManager",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (fishgame::FishObjectManager*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_fishgame_FishObjectManager_GetAllFishes'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_fishgame_FishObjectManager_GetAllFishes'", nullptr);
            return 0;
        }
        cocos2d::Vector<fishgame::Fish *> ret = cobj->GetAllFishes();
        ccvector_to_luaval(tolua_S, ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "fishgame.FishObjectManager:GetAllFishes",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_fishgame_FishObjectManager_GetAllFishes'.",&tolua_err);
#endif

    return 0;
}
int lua_fishgame_FishObjectManager_AddBullet(lua_State* tolua_S)
{
    int argc = 0;
    fishgame::FishObjectManager* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"fishgame.FishObjectManager",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (fishgame::FishObjectManager*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_fishgame_FishObjectManager_AddBullet'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 1) 
    {
        fishgame::Bullet* arg0;

        ok &= luaval_to_object<fishgame::Bullet>(tolua_S, 2, "fishgame.Bullet",&arg0);
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_fishgame_FishObjectManager_AddBullet'", nullptr);
            return 0;
        }
        bool ret = cobj->AddBullet(arg0);
        tolua_pushboolean(tolua_S,(bool)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "fishgame.FishObjectManager:AddBullet",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_fishgame_FishObjectManager_AddBullet'.",&tolua_err);
#endif

    return 0;
}
int lua_fishgame_FishObjectManager_GetServerHeight(lua_State* tolua_S)
{
    int argc = 0;
    fishgame::FishObjectManager* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"fishgame.FishObjectManager",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (fishgame::FishObjectManager*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_fishgame_FishObjectManager_GetServerHeight'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_fishgame_FishObjectManager_GetServerHeight'", nullptr);
            return 0;
        }
        int ret = cobj->GetServerHeight();
        tolua_pushnumber(tolua_S,(lua_Number)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "fishgame.FishObjectManager:GetServerHeight",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_fishgame_FishObjectManager_GetServerHeight'.",&tolua_err);
#endif

    return 0;
}
int lua_fishgame_FishObjectManager_OnUpdate(lua_State* tolua_S)
{
    int argc = 0;
    fishgame::FishObjectManager* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"fishgame.FishObjectManager",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (fishgame::FishObjectManager*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_fishgame_FishObjectManager_OnUpdate'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 1) 
    {
        double arg0;

        ok &= luaval_to_number(tolua_S, 2,&arg0, "fishgame.FishObjectManager:OnUpdate");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_fishgame_FishObjectManager_OnUpdate'", nullptr);
            return 0;
        }
        bool ret = cobj->OnUpdate(arg0);
        tolua_pushboolean(tolua_S,(bool)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "fishgame.FishObjectManager:OnUpdate",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_fishgame_FishObjectManager_OnUpdate'.",&tolua_err);
#endif

    return 0;
}
int lua_fishgame_FishObjectManager_ConvertCoortToCleientPosition(lua_State* tolua_S)
{
    int argc = 0;
    fishgame::FishObjectManager* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"fishgame.FishObjectManager",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (fishgame::FishObjectManager*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_fishgame_FishObjectManager_ConvertCoortToCleientPosition'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 2) 
    {
        float* arg0;
        float* arg1;

        #pragma warning NO CONVERSION TO NATIVE FOR float*
		ok = false;

        #pragma warning NO CONVERSION TO NATIVE FOR float*
		ok = false;
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_fishgame_FishObjectManager_ConvertCoortToCleientPosition'", nullptr);
            return 0;
        }
        cobj->ConvertCoortToCleientPosition(arg0, arg1);
        return 0;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "fishgame.FishObjectManager:ConvertCoortToCleientPosition",argc, 2);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_fishgame_FishObjectManager_ConvertCoortToCleientPosition'.",&tolua_err);
#endif

    return 0;
}
int lua_fishgame_FishObjectManager_ConvertDirection(lua_State* tolua_S)
{
    int argc = 0;
    fishgame::FishObjectManager* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"fishgame.FishObjectManager",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (fishgame::FishObjectManager*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_fishgame_FishObjectManager_ConvertDirection'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 1) 
    {
        double arg0;

        ok &= luaval_to_number(tolua_S, 2,&arg0, "fishgame.FishObjectManager:ConvertDirection");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_fishgame_FishObjectManager_ConvertDirection'", nullptr);
            return 0;
        }
        double ret = cobj->ConvertDirection(arg0);
        tolua_pushnumber(tolua_S,(lua_Number)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "fishgame.FishObjectManager:ConvertDirection",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_fishgame_FishObjectManager_ConvertDirection'.",&tolua_err);
#endif

    return 0;
}
int lua_fishgame_FishObjectManager_SetMirrowShow(lua_State* tolua_S)
{
    int argc = 0;
    fishgame::FishObjectManager* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"fishgame.FishObjectManager",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (fishgame::FishObjectManager*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_fishgame_FishObjectManager_SetMirrowShow'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 1) 
    {
        bool arg0;

        ok &= luaval_to_boolean(tolua_S, 2,&arg0, "fishgame.FishObjectManager:SetMirrowShow");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_fishgame_FishObjectManager_SetMirrowShow'", nullptr);
            return 0;
        }
        cobj->SetMirrowShow(arg0);
        return 0;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "fishgame.FishObjectManager:SetMirrowShow",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_fishgame_FishObjectManager_SetMirrowShow'.",&tolua_err);
#endif

    return 0;
}
int lua_fishgame_FishObjectManager_ConvertMirrorCoord(lua_State* tolua_S)
{
    int argc = 0;
    fishgame::FishObjectManager* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"fishgame.FishObjectManager",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (fishgame::FishObjectManager*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_fishgame_FishObjectManager_ConvertMirrorCoord'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 2) 
    {
        float* arg0;
        float* arg1;

        #pragma warning NO CONVERSION TO NATIVE FOR float*
		ok = false;

        #pragma warning NO CONVERSION TO NATIVE FOR float*
		ok = false;
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_fishgame_FishObjectManager_ConvertMirrorCoord'", nullptr);
            return 0;
        }
        cobj->ConvertMirrorCoord(arg0, arg1);
        return 0;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "fishgame.FishObjectManager:ConvertMirrorCoord",argc, 2);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_fishgame_FishObjectManager_ConvertMirrorCoord'.",&tolua_err);
#endif

    return 0;
}
int lua_fishgame_FishObjectManager_DestoryInstace(lua_State* tolua_S)
{
    int argc = 0;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif

#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertable(tolua_S,1,"fishgame.FishObjectManager",0,&tolua_err)) goto tolua_lerror;
#endif

    argc = lua_gettop(tolua_S) - 1;

    if (argc == 0)
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_fishgame_FishObjectManager_DestoryInstace'", nullptr);
            return 0;
        }
        fishgame::FishObjectManager::DestoryInstace();
        return 0;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d\n ", "fishgame.FishObjectManager:DestoryInstace",argc, 0);
    return 0;
#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_fishgame_FishObjectManager_DestoryInstace'.",&tolua_err);
#endif
    return 0;
}
int lua_fishgame_FishObjectManager_GetInstance(lua_State* tolua_S)
{
    int argc = 0;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif

#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertable(tolua_S,1,"fishgame.FishObjectManager",0,&tolua_err)) goto tolua_lerror;
#endif

    argc = lua_gettop(tolua_S) - 1;

    if (argc == 0)
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_fishgame_FishObjectManager_GetInstance'", nullptr);
            return 0;
        }
        fishgame::FishObjectManager* ret = fishgame::FishObjectManager::GetInstance();
        object_to_luaval<fishgame::FishObjectManager>(tolua_S, "fishgame.FishObjectManager",(fishgame::FishObjectManager*)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d\n ", "fishgame.FishObjectManager:GetInstance",argc, 0);
    return 0;
#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_fishgame_FishObjectManager_GetInstance'.",&tolua_err);
#endif
    return 0;
}
static int lua_fishgame_FishObjectManager_finalize(lua_State* tolua_S)
{
    printf("luabindings: finalizing LUA object (FishObjectManager)");
    return 0;
}

int lua_register_fishgame_FishObjectManager(lua_State* tolua_S)
{
    tolua_usertype(tolua_S,"fishgame.FishObjectManager");
    tolua_cclass(tolua_S,"FishObjectManager","fishgame.FishObjectManager","cc.Ref",nullptr);

    tolua_beginmodule(tolua_S,"FishObjectManager");
        tolua_function(tolua_S,"FindFish",lua_fishgame_FishObjectManager_FindFish);
        tolua_function(tolua_S,"ConvertCoortToScreenSize",lua_fishgame_FishObjectManager_ConvertCoortToScreenSize);
        tolua_function(tolua_S,"Init",lua_fishgame_FishObjectManager_Init);
        tolua_function(tolua_S,"IsSwitchingScene",lua_fishgame_FishObjectManager_IsSwitchingScene);
        tolua_function(tolua_S,"GetServerWidth",lua_fishgame_FishObjectManager_GetServerWidth);
        tolua_function(tolua_S,"MirrowShow",lua_fishgame_FishObjectManager_MirrowShow);
        tolua_function(tolua_S,"FindBullet",lua_fishgame_FishObjectManager_FindBullet);
        tolua_function(tolua_S,"SetGameLoaded",lua_fishgame_FishObjectManager_SetGameLoaded);
        tolua_function(tolua_S,"AddFishBuff",lua_fishgame_FishObjectManager_AddFishBuff);
        tolua_function(tolua_S,"RemoveAllBullets",lua_fishgame_FishObjectManager_RemoveAllBullets);
        tolua_function(tolua_S,"SetSwitchingScene",lua_fishgame_FishObjectManager_SetSwitchingScene);
        tolua_function(tolua_S,"Clear",lua_fishgame_FishObjectManager_Clear);
        tolua_function(tolua_S,"IsGameLoaded",lua_fishgame_FishObjectManager_IsGameLoaded);
        tolua_function(tolua_S,"RemoveAllFishes",lua_fishgame_FishObjectManager_RemoveAllFishes);
        tolua_function(tolua_S,"AddFish",lua_fishgame_FishObjectManager_AddFish);
        tolua_function(tolua_S,"ConvertCoord",lua_fishgame_FishObjectManager_ConvertCoord);
        tolua_function(tolua_S,"GetAllFishes",lua_fishgame_FishObjectManager_GetAllFishes);
        tolua_function(tolua_S,"AddBullet",lua_fishgame_FishObjectManager_AddBullet);
        tolua_function(tolua_S,"GetServerHeight",lua_fishgame_FishObjectManager_GetServerHeight);
        tolua_function(tolua_S,"OnUpdate",lua_fishgame_FishObjectManager_OnUpdate);
        tolua_function(tolua_S,"ConvertCoortToCleientPosition",lua_fishgame_FishObjectManager_ConvertCoortToCleientPosition);
        tolua_function(tolua_S,"ConvertDirection",lua_fishgame_FishObjectManager_ConvertDirection);
        tolua_function(tolua_S,"SetMirrowShow",lua_fishgame_FishObjectManager_SetMirrowShow);
        tolua_function(tolua_S,"ConvertMirrorCoord",lua_fishgame_FishObjectManager_ConvertMirrorCoord);
        tolua_function(tolua_S,"DestoryInstace", lua_fishgame_FishObjectManager_DestoryInstace);
        tolua_function(tolua_S,"GetInstance", lua_fishgame_FishObjectManager_GetInstance);
    tolua_endmodule(tolua_S);
    std::string typeName = typeid(fishgame::FishObjectManager).name();
    g_luaType[typeName] = "fishgame.FishObjectManager";
    g_typeCast["FishObjectManager"] = "fishgame.FishObjectManager";
    return 1;
}

int lua_fishgame_FishUtils_CalcAngle(lua_State* tolua_S)
{
    int argc = 0;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif

#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertable(tolua_S,1,"fishgame.FishUtils",0,&tolua_err)) goto tolua_lerror;
#endif

    argc = lua_gettop(tolua_S) - 1;

    if (argc == 4)
    {
        double arg0;
        double arg1;
        double arg2;
        double arg3;
        ok &= luaval_to_number(tolua_S, 2,&arg0, "fishgame.FishUtils:CalcAngle");
        ok &= luaval_to_number(tolua_S, 3,&arg1, "fishgame.FishUtils:CalcAngle");
        ok &= luaval_to_number(tolua_S, 4,&arg2, "fishgame.FishUtils:CalcAngle");
        ok &= luaval_to_number(tolua_S, 5,&arg3, "fishgame.FishUtils:CalcAngle");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_fishgame_FishUtils_CalcAngle'", nullptr);
            return 0;
        }
        double ret = fishgame::FishUtils::CalcAngle(arg0, arg1, arg2, arg3);
        tolua_pushnumber(tolua_S,(lua_Number)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d\n ", "fishgame.FishUtils:CalcAngle",argc, 4);
    return 0;
#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_fishgame_FishUtils_CalcAngle'.",&tolua_err);
#endif
    return 0;
}
int lua_fishgame_FishUtils_CalCircle(lua_State* tolua_S)
{
    int argc = 0;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif

#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertable(tolua_S,1,"fishgame.FishUtils",0,&tolua_err)) goto tolua_lerror;
#endif

    argc = lua_gettop(tolua_S) - 1;

    if (argc == 10)
    {
        double arg0;
        double arg1;
        double arg2;
        double arg3;
        double arg4;
        double arg5;
        double arg6;
        float* arg7;
        float* arg8;
        float* arg9;
        ok &= luaval_to_number(tolua_S, 2,&arg0, "fishgame.FishUtils:CalCircle");
        ok &= luaval_to_number(tolua_S, 3,&arg1, "fishgame.FishUtils:CalCircle");
        ok &= luaval_to_number(tolua_S, 4,&arg2, "fishgame.FishUtils:CalCircle");
        ok &= luaval_to_number(tolua_S, 5,&arg3, "fishgame.FishUtils:CalCircle");
        ok &= luaval_to_number(tolua_S, 6,&arg4, "fishgame.FishUtils:CalCircle");
        ok &= luaval_to_number(tolua_S, 7,&arg5, "fishgame.FishUtils:CalCircle");
        ok &= luaval_to_number(tolua_S, 8,&arg6, "fishgame.FishUtils:CalCircle");
        #pragma warning NO CONVERSION TO NATIVE FOR float*
		ok = false;
        #pragma warning NO CONVERSION TO NATIVE FOR float*
		ok = false;
        #pragma warning NO CONVERSION TO NATIVE FOR float*
		ok = false;
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_fishgame_FishUtils_CalCircle'", nullptr);
            return 0;
        }
        fishgame::FishUtils::CalCircle(arg0, arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9);
        return 0;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d\n ", "fishgame.FishUtils:CalCircle",argc, 10);
    return 0;
#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_fishgame_FishUtils_CalCircle'.",&tolua_err);
#endif
    return 0;
}
static int lua_fishgame_FishUtils_finalize(lua_State* tolua_S)
{
    printf("luabindings: finalizing LUA object (FishUtils)");
    return 0;
}

int lua_register_fishgame_FishUtils(lua_State* tolua_S)
{
    tolua_usertype(tolua_S,"fishgame.FishUtils");
    tolua_cclass(tolua_S,"FishUtils","fishgame.FishUtils","",nullptr);

    tolua_beginmodule(tolua_S,"FishUtils");
        tolua_function(tolua_S,"CalcAngle", lua_fishgame_FishUtils_CalcAngle);
        tolua_function(tolua_S,"CalCircle", lua_fishgame_FishUtils_CalCircle);
    tolua_endmodule(tolua_S);
    std::string typeName = typeid(fishgame::FishUtils).name();
    g_luaType[typeName] = "fishgame.FishUtils";
    g_typeCast["FishUtils"] = "fishgame.FishUtils";
    return 1;
}

int lua_fishgame_MyObject_SetMoveCompent(lua_State* tolua_S)
{
    int argc = 0;
    fishgame::MyObject* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"fishgame.MyObject",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (fishgame::MyObject*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_fishgame_MyObject_SetMoveCompent'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 1) 
    {
        fishgame::MoveCompent* arg0;

        ok &= luaval_to_object<fishgame::MoveCompent>(tolua_S, 2, "fishgame.MoveCompent",&arg0);
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_fishgame_MyObject_SetMoveCompent'", nullptr);
            return 0;
        }
        cobj->SetMoveCompent(arg0);
        return 0;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "fishgame.MyObject:SetMoveCompent",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_fishgame_MyObject_SetMoveCompent'.",&tolua_err);
#endif

    return 0;
}
int lua_fishgame_MyObject_GetOwner(lua_State* tolua_S)
{
    int argc = 0;
    fishgame::MyObject* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"fishgame.MyObject",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (fishgame::MyObject*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_fishgame_MyObject_GetOwner'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_fishgame_MyObject_GetOwner'", nullptr);
            return 0;
        }
        fishgame::MyObject* ret = cobj->GetOwner();
        object_to_luaval<fishgame::MyObject>(tolua_S, "fishgame.MyObject",(fishgame::MyObject*)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "fishgame.MyObject:GetOwner",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_fishgame_MyObject_GetOwner'.",&tolua_err);
#endif

    return 0;
}
int lua_fishgame_MyObject_SetOwner(lua_State* tolua_S)
{
    int argc = 0;
    fishgame::MyObject* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"fishgame.MyObject",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (fishgame::MyObject*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_fishgame_MyObject_SetOwner'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 1) 
    {
        fishgame::MyObject* arg0;

        ok &= luaval_to_object<fishgame::MyObject>(tolua_S, 2, "fishgame.MyObject",&arg0);
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_fishgame_MyObject_SetOwner'", nullptr);
            return 0;
        }
        cobj->SetOwner(arg0);
        return 0;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "fishgame.MyObject:SetOwner",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_fishgame_MyObject_SetOwner'.",&tolua_err);
#endif

    return 0;
}
int lua_fishgame_MyObject_OnMoveEnd(lua_State* tolua_S)
{
    int argc = 0;
    fishgame::MyObject* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"fishgame.MyObject",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (fishgame::MyObject*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_fishgame_MyObject_OnMoveEnd'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_fishgame_MyObject_OnMoveEnd'", nullptr);
            return 0;
        }
        cobj->OnMoveEnd();
        return 0;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "fishgame.MyObject:OnMoveEnd",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_fishgame_MyObject_OnMoveEnd'.",&tolua_err);
#endif

    return 0;
}
int lua_fishgame_MyObject_GetMoveCompent(lua_State* tolua_S)
{
    int argc = 0;
    fishgame::MyObject* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"fishgame.MyObject",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (fishgame::MyObject*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_fishgame_MyObject_GetMoveCompent'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_fishgame_MyObject_GetMoveCompent'", nullptr);
            return 0;
        }
        fishgame::MoveCompent* ret = cobj->GetMoveCompent();
        object_to_luaval<fishgame::MoveCompent>(tolua_S, "fishgame.MoveCompent",(fishgame::MoveCompent*)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "fishgame.MyObject:GetMoveCompent",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_fishgame_MyObject_GetMoveCompent'.",&tolua_err);
#endif

    return 0;
}
int lua_fishgame_MyObject_SetId(lua_State* tolua_S)
{
    int argc = 0;
    fishgame::MyObject* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"fishgame.MyObject",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (fishgame::MyObject*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_fishgame_MyObject_SetId'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 1) 
    {
        unsigned long arg0;

        ok &= luaval_to_ulong(tolua_S, 2, &arg0, "fishgame.MyObject:SetId");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_fishgame_MyObject_SetId'", nullptr);
            return 0;
        }
        cobj->SetId(arg0);
        return 0;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "fishgame.MyObject:SetId",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_fishgame_MyObject_SetId'.",&tolua_err);
#endif

    return 0;
}
int lua_fishgame_MyObject_OnClear(lua_State* tolua_S)
{
    int argc = 0;
    fishgame::MyObject* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"fishgame.MyObject",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (fishgame::MyObject*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_fishgame_MyObject_OnClear'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 1) 
    {
        bool arg0;

        ok &= luaval_to_boolean(tolua_S, 2,&arg0, "fishgame.MyObject:OnClear");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_fishgame_MyObject_OnClear'", nullptr);
            return 0;
        }
        cobj->OnClear(arg0);
        return 0;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "fishgame.MyObject:OnClear",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_fishgame_MyObject_OnClear'.",&tolua_err);
#endif

    return 0;
}
int lua_fishgame_MyObject_SetManager(lua_State* tolua_S)
{
    int argc = 0;
    fishgame::MyObject* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"fishgame.MyObject",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (fishgame::MyObject*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_fishgame_MyObject_SetManager'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 1) 
    {
        fishgame::FishObjectManager* arg0;

        ok &= luaval_to_object<fishgame::FishObjectManager>(tolua_S, 2, "fishgame.FishObjectManager",&arg0);
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_fishgame_MyObject_SetManager'", nullptr);
            return 0;
        }
        cobj->SetManager(arg0);
        return 0;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "fishgame.MyObject:SetManager",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_fishgame_MyObject_SetManager'.",&tolua_err);
#endif

    return 0;
}
int lua_fishgame_MyObject_GetTarget(lua_State* tolua_S)
{
    int argc = 0;
    fishgame::MyObject* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"fishgame.MyObject",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (fishgame::MyObject*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_fishgame_MyObject_GetTarget'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_fishgame_MyObject_GetTarget'", nullptr);
            return 0;
        }
        int ret = cobj->GetTarget();
        tolua_pushnumber(tolua_S,(lua_Number)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "fishgame.MyObject:GetTarget",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_fishgame_MyObject_GetTarget'.",&tolua_err);
#endif

    return 0;
}
int lua_fishgame_MyObject_GetDirection(lua_State* tolua_S)
{
    int argc = 0;
    fishgame::MyObject* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"fishgame.MyObject",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (fishgame::MyObject*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_fishgame_MyObject_GetDirection'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_fishgame_MyObject_GetDirection'", nullptr);
            return 0;
        }
        double ret = cobj->GetDirection();
        tolua_pushnumber(tolua_S,(lua_Number)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "fishgame.MyObject:GetDirection",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_fishgame_MyObject_GetDirection'.",&tolua_err);
#endif

    return 0;
}
int lua_fishgame_MyObject_SetState(lua_State* tolua_S)
{
    int argc = 0;
    fishgame::MyObject* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"fishgame.MyObject",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (fishgame::MyObject*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_fishgame_MyObject_SetState'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 1) 
    {
        int arg0;

        ok &= luaval_to_int32(tolua_S, 2,(int *)&arg0, "fishgame.MyObject:SetState");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_fishgame_MyObject_SetState'", nullptr);
            return 0;
        }
        cobj->SetState(arg0);
        return 0;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "fishgame.MyObject:SetState",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_fishgame_MyObject_SetState'.",&tolua_err);
#endif

    return 0;
}
int lua_fishgame_MyObject_SetDirection(lua_State* tolua_S)
{
    int argc = 0;
    fishgame::MyObject* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"fishgame.MyObject",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (fishgame::MyObject*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_fishgame_MyObject_SetDirection'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 1) 
    {
        double arg0;

        ok &= luaval_to_number(tolua_S, 2,&arg0, "fishgame.MyObject:SetDirection");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_fishgame_MyObject_SetDirection'", nullptr);
            return 0;
        }
        cobj->SetDirection(arg0);
        return 0;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "fishgame.MyObject:SetDirection",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_fishgame_MyObject_SetDirection'.",&tolua_err);
#endif

    return 0;
}
int lua_fishgame_MyObject_Clear(lua_State* tolua_S)
{
    int argc = 0;
    fishgame::MyObject* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"fishgame.MyObject",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (fishgame::MyObject*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_fishgame_MyObject_Clear'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 1) 
    {
        bool arg0;

        ok &= luaval_to_boolean(tolua_S, 2,&arg0, "fishgame.MyObject:Clear");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_fishgame_MyObject_Clear'", nullptr);
            return 0;
        }
        cobj->Clear(arg0);
        return 0;
    }
    if (argc == 2) 
    {
        bool arg0;
        bool arg1;

        ok &= luaval_to_boolean(tolua_S, 2,&arg0, "fishgame.MyObject:Clear");

        ok &= luaval_to_boolean(tolua_S, 3,&arg1, "fishgame.MyObject:Clear");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_fishgame_MyObject_Clear'", nullptr);
            return 0;
        }
        cobj->Clear(arg0, arg1);
        return 0;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "fishgame.MyObject:Clear",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_fishgame_MyObject_Clear'.",&tolua_err);
#endif

    return 0;
}
int lua_fishgame_MyObject_GetId(lua_State* tolua_S)
{
    int argc = 0;
    fishgame::MyObject* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"fishgame.MyObject",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (fishgame::MyObject*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_fishgame_MyObject_GetId'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_fishgame_MyObject_GetId'", nullptr);
            return 0;
        }
        unsigned long ret = cobj->GetId();
        tolua_pushnumber(tolua_S,(lua_Number)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "fishgame.MyObject:GetId",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_fishgame_MyObject_GetId'.",&tolua_err);
#endif

    return 0;
}
int lua_fishgame_MyObject_GetManager(lua_State* tolua_S)
{
    int argc = 0;
    fishgame::MyObject* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"fishgame.MyObject",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (fishgame::MyObject*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_fishgame_MyObject_GetManager'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_fishgame_MyObject_GetManager'", nullptr);
            return 0;
        }
        fishgame::FishObjectManager* ret = cobj->GetManager();
        object_to_luaval<fishgame::FishObjectManager>(tolua_S, "fishgame.FishObjectManager",(fishgame::FishObjectManager*)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "fishgame.MyObject:GetManager",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_fishgame_MyObject_GetManager'.",&tolua_err);
#endif

    return 0;
}
int lua_fishgame_MyObject_AddBuff(lua_State* tolua_S)
{
    int argc = 0;
    fishgame::MyObject* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"fishgame.MyObject",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (fishgame::MyObject*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_fishgame_MyObject_AddBuff'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 3) 
    {
        int arg0;
        double arg1;
        double arg2;

        ok &= luaval_to_int32(tolua_S, 2,(int *)&arg0, "fishgame.MyObject:AddBuff");

        ok &= luaval_to_number(tolua_S, 3,&arg1, "fishgame.MyObject:AddBuff");

        ok &= luaval_to_number(tolua_S, 4,&arg2, "fishgame.MyObject:AddBuff");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_fishgame_MyObject_AddBuff'", nullptr);
            return 0;
        }
        cobj->AddBuff(arg0, arg1, arg2);
        return 0;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "fishgame.MyObject:AddBuff",argc, 3);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_fishgame_MyObject_AddBuff'.",&tolua_err);
#endif

    return 0;
}
int lua_fishgame_MyObject_GetState(lua_State* tolua_S)
{
    int argc = 0;
    fishgame::MyObject* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"fishgame.MyObject",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (fishgame::MyObject*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_fishgame_MyObject_GetState'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_fishgame_MyObject_GetState'", nullptr);
            return 0;
        }
        int ret = cobj->GetState();
        tolua_pushnumber(tolua_S,(lua_Number)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "fishgame.MyObject:GetState",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_fishgame_MyObject_GetState'.",&tolua_err);
#endif

    return 0;
}
int lua_fishgame_MyObject_SetTarget(lua_State* tolua_S)
{
    int argc = 0;
    fishgame::MyObject* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"fishgame.MyObject",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (fishgame::MyObject*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_fishgame_MyObject_SetTarget'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 1) 
    {
        int arg0;

        ok &= luaval_to_int32(tolua_S, 2,(int *)&arg0, "fishgame.MyObject:SetTarget");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_fishgame_MyObject_SetTarget'", nullptr);
            return 0;
        }
        cobj->SetTarget(arg0);
        return 0;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "fishgame.MyObject:SetTarget",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_fishgame_MyObject_SetTarget'.",&tolua_err);
#endif

    return 0;
}
int lua_fishgame_MyObject_GetType(lua_State* tolua_S)
{
    int argc = 0;
    fishgame::MyObject* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"fishgame.MyObject",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (fishgame::MyObject*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_fishgame_MyObject_GetType'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_fishgame_MyObject_GetType'", nullptr);
            return 0;
        }
        int ret = cobj->GetType();
        tolua_pushnumber(tolua_S,(lua_Number)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "fishgame.MyObject:GetType",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_fishgame_MyObject_GetType'.",&tolua_err);
#endif

    return 0;
}
int lua_fishgame_MyObject_OnUpdate(lua_State* tolua_S)
{
    int argc = 0;
    fishgame::MyObject* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"fishgame.MyObject",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (fishgame::MyObject*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_fishgame_MyObject_OnUpdate'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 1) 
    {
        double arg0;

        ok &= luaval_to_number(tolua_S, 2,&arg0, "fishgame.MyObject:OnUpdate");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_fishgame_MyObject_OnUpdate'", nullptr);
            return 0;
        }
        cobj->OnUpdate(arg0);
        return 0;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "fishgame.MyObject:OnUpdate",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_fishgame_MyObject_OnUpdate'.",&tolua_err);
#endif

    return 0;
}
int lua_fishgame_MyObject_SetPosition(lua_State* tolua_S)
{
    int argc = 0;
    fishgame::MyObject* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"fishgame.MyObject",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (fishgame::MyObject*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_fishgame_MyObject_SetPosition'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 2) 
    {
        double arg0;
        double arg1;

        ok &= luaval_to_number(tolua_S, 2,&arg0, "fishgame.MyObject:SetPosition");

        ok &= luaval_to_number(tolua_S, 3,&arg1, "fishgame.MyObject:SetPosition");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_fishgame_MyObject_SetPosition'", nullptr);
            return 0;
        }
        cobj->SetPosition(arg0, arg1);
        return 0;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "fishgame.MyObject:SetPosition",argc, 2);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_fishgame_MyObject_SetPosition'.",&tolua_err);
#endif

    return 0;
}
int lua_fishgame_MyObject_InSideScreen(lua_State* tolua_S)
{
    int argc = 0;
    fishgame::MyObject* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"fishgame.MyObject",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (fishgame::MyObject*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_fishgame_MyObject_InSideScreen'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_fishgame_MyObject_InSideScreen'", nullptr);
            return 0;
        }
        bool ret = cobj->InSideScreen();
        tolua_pushboolean(tolua_S,(bool)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "fishgame.MyObject:InSideScreen",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_fishgame_MyObject_InSideScreen'.",&tolua_err);
#endif

    return 0;
}
int lua_fishgame_MyObject_GetPosition(lua_State* tolua_S)
{
    int argc = 0;
    fishgame::MyObject* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"fishgame.MyObject",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (fishgame::MyObject*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_fishgame_MyObject_GetPosition'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_fishgame_MyObject_GetPosition'", nullptr);
            return 0;
        }
        cocos2d::Point ret = cobj->GetPosition();
        point_to_luaval(tolua_S, ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "fishgame.MyObject:GetPosition",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_fishgame_MyObject_GetPosition'.",&tolua_err);
#endif

    return 0;
}
static int lua_fishgame_MyObject_finalize(lua_State* tolua_S)
{
    printf("luabindings: finalizing LUA object (MyObject)");
    return 0;
}

int lua_register_fishgame_MyObject(lua_State* tolua_S)
{
    tolua_usertype(tolua_S,"fishgame.MyObject");
    tolua_cclass(tolua_S,"MyObject","fishgame.MyObject","cc.Ref",nullptr);

    tolua_beginmodule(tolua_S,"MyObject");
        tolua_function(tolua_S,"SetMoveCompent",lua_fishgame_MyObject_SetMoveCompent);
        tolua_function(tolua_S,"GetOwner",lua_fishgame_MyObject_GetOwner);
        tolua_function(tolua_S,"SetOwner",lua_fishgame_MyObject_SetOwner);
        tolua_function(tolua_S,"OnMoveEnd",lua_fishgame_MyObject_OnMoveEnd);
        tolua_function(tolua_S,"GetMoveCompent",lua_fishgame_MyObject_GetMoveCompent);
        tolua_function(tolua_S,"SetId",lua_fishgame_MyObject_SetId);
        tolua_function(tolua_S,"OnClear",lua_fishgame_MyObject_OnClear);
        tolua_function(tolua_S,"SetManager",lua_fishgame_MyObject_SetManager);
        tolua_function(tolua_S,"GetTarget",lua_fishgame_MyObject_GetTarget);
        tolua_function(tolua_S,"GetDirection",lua_fishgame_MyObject_GetDirection);
        tolua_function(tolua_S,"SetState",lua_fishgame_MyObject_SetState);
        tolua_function(tolua_S,"SetDirection",lua_fishgame_MyObject_SetDirection);
        tolua_function(tolua_S,"Clear",lua_fishgame_MyObject_Clear);
        tolua_function(tolua_S,"GetId",lua_fishgame_MyObject_GetId);
        tolua_function(tolua_S,"GetManager",lua_fishgame_MyObject_GetManager);
        tolua_function(tolua_S,"AddBuff",lua_fishgame_MyObject_AddBuff);
        tolua_function(tolua_S,"GetState",lua_fishgame_MyObject_GetState);
        tolua_function(tolua_S,"SetTarget",lua_fishgame_MyObject_SetTarget);
        tolua_function(tolua_S,"GetType",lua_fishgame_MyObject_GetType);
        tolua_function(tolua_S,"OnUpdate",lua_fishgame_MyObject_OnUpdate);
        tolua_function(tolua_S,"SetPosition",lua_fishgame_MyObject_SetPosition);
        tolua_function(tolua_S,"InSideScreen",lua_fishgame_MyObject_InSideScreen);
        tolua_function(tolua_S,"GetPosition",lua_fishgame_MyObject_GetPosition);
    tolua_endmodule(tolua_S);
    std::string typeName = typeid(fishgame::MyObject).name();
    g_luaType[typeName] = "fishgame.MyObject";
    g_typeCast["MyObject"] = "fishgame.MyObject";
    return 1;
}

int lua_fishgame_Fish_OnUpdate(lua_State* tolua_S)
{
    int argc = 0;
    fishgame::Fish* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"fishgame.Fish",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (fishgame::Fish*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_fishgame_Fish_OnUpdate'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 1) 
    {
        double arg0;

        ok &= luaval_to_number(tolua_S, 2,&arg0, "fishgame.Fish:OnUpdate");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_fishgame_Fish_OnUpdate'", nullptr);
            return 0;
        }
        cobj->OnUpdate(arg0);
        return 0;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "fishgame.Fish:OnUpdate",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_fishgame_Fish_OnUpdate'.",&tolua_err);
#endif

    return 0;
}
int lua_fishgame_Fish_GetVisualId(lua_State* tolua_S)
{
    int argc = 0;
    fishgame::Fish* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"fishgame.Fish",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (fishgame::Fish*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_fishgame_Fish_GetVisualId'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_fishgame_Fish_GetVisualId'", nullptr);
            return 0;
        }
        int ret = cobj->GetVisualId();
        tolua_pushnumber(tolua_S,(lua_Number)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "fishgame.Fish:GetVisualId",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_fishgame_Fish_GetVisualId'.",&tolua_err);
#endif

    return 0;
}
int lua_fishgame_Fish_GetMaxRadio(lua_State* tolua_S)
{
    int argc = 0;
    fishgame::Fish* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"fishgame.Fish",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (fishgame::Fish*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_fishgame_Fish_GetMaxRadio'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_fishgame_Fish_GetMaxRadio'", nullptr);
            return 0;
        }
        int ret = cobj->GetMaxRadio();
        tolua_pushnumber(tolua_S,(lua_Number)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "fishgame.Fish:GetMaxRadio",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_fishgame_Fish_GetMaxRadio'.",&tolua_err);
#endif

    return 0;
}
int lua_fishgame_Fish_OnHit(lua_State* tolua_S)
{
    int argc = 0;
    fishgame::Fish* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"fishgame.Fish",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (fishgame::Fish*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_fishgame_Fish_OnHit'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_fishgame_Fish_OnHit'", nullptr);
            return 0;
        }
        cobj->OnHit();
        return 0;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "fishgame.Fish:OnHit",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_fishgame_Fish_OnHit'.",&tolua_err);
#endif

    return 0;
}
int lua_fishgame_Fish_SetVisualId(lua_State* tolua_S)
{
    int argc = 0;
    fishgame::Fish* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"fishgame.Fish",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (fishgame::Fish*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_fishgame_Fish_SetVisualId'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 1) 
    {
        int arg0;

        ok &= luaval_to_int32(tolua_S, 2,(int *)&arg0, "fishgame.Fish:SetVisualId");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_fishgame_Fish_SetVisualId'", nullptr);
            return 0;
        }
        cobj->SetVisualId(arg0);
        return 0;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "fishgame.Fish:SetVisualId",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_fishgame_Fish_SetVisualId'.",&tolua_err);
#endif

    return 0;
}
int lua_fishgame_Fish_GetBoundingBox(lua_State* tolua_S)
{
    int argc = 0;
    fishgame::Fish* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"fishgame.Fish",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (fishgame::Fish*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_fishgame_Fish_GetBoundingBox'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_fishgame_Fish_GetBoundingBox'", nullptr);
            return 0;
        }
        int ret = cobj->GetBoundingBox();
        tolua_pushnumber(tolua_S,(lua_Number)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "fishgame.Fish:GetBoundingBox",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_fishgame_Fish_GetBoundingBox'.",&tolua_err);
#endif

    return 0;
}
int lua_fishgame_Fish_SetBoundingBox(lua_State* tolua_S)
{
    int argc = 0;
    fishgame::Fish* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"fishgame.Fish",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (fishgame::Fish*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_fishgame_Fish_SetBoundingBox'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 1) 
    {
        int arg0;

        ok &= luaval_to_int32(tolua_S, 2,(int *)&arg0, "fishgame.Fish:SetBoundingBox");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_fishgame_Fish_SetBoundingBox'", nullptr);
            return 0;
        }
        cobj->SetBoundingBox(arg0);
        return 0;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "fishgame.Fish:SetBoundingBox",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_fishgame_Fish_SetBoundingBox'.",&tolua_err);
#endif

    return 0;
}
int lua_fishgame_Fish_Create(lua_State* tolua_S)
{
    int argc = 0;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif

#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertable(tolua_S,1,"fishgame.Fish",0,&tolua_err)) goto tolua_lerror;
#endif

    argc = lua_gettop(tolua_S) - 1;

    if (argc == 0)
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_fishgame_Fish_Create'", nullptr);
            return 0;
        }
        fishgame::Fish* ret = fishgame::Fish::Create();
        object_to_luaval<fishgame::Fish>(tolua_S, "fishgame.Fish",(fishgame::Fish*)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d\n ", "fishgame.Fish:Create",argc, 0);
    return 0;
#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_fishgame_Fish_Create'.",&tolua_err);
#endif
    return 0;
}
static int lua_fishgame_Fish_finalize(lua_State* tolua_S)
{
    printf("luabindings: finalizing LUA object (Fish)");
    return 0;
}

int lua_register_fishgame_Fish(lua_State* tolua_S)
{
    tolua_usertype(tolua_S,"fishgame.Fish");
    tolua_cclass(tolua_S,"Fish","fishgame.Fish","fishgame.MyObject",nullptr);

    tolua_beginmodule(tolua_S,"Fish");
        tolua_function(tolua_S,"OnUpdate",lua_fishgame_Fish_OnUpdate);
        tolua_function(tolua_S,"GetVisualId",lua_fishgame_Fish_GetVisualId);
        tolua_function(tolua_S,"GetMaxRadio",lua_fishgame_Fish_GetMaxRadio);
        tolua_function(tolua_S,"OnHit",lua_fishgame_Fish_OnHit);
        tolua_function(tolua_S,"SetVisualId",lua_fishgame_Fish_SetVisualId);
        tolua_function(tolua_S,"GetBoundingBox",lua_fishgame_Fish_GetBoundingBox);
        tolua_function(tolua_S,"SetBoundingBox",lua_fishgame_Fish_SetBoundingBox);
        tolua_function(tolua_S,"Create", lua_fishgame_Fish_Create);
    tolua_endmodule(tolua_S);
    std::string typeName = typeid(fishgame::Fish).name();
    g_luaType[typeName] = "fishgame.Fish";
    g_typeCast["Fish"] = "fishgame.Fish";
    return 1;
}

int lua_fishgame_Bullet_SetCatchRadio(lua_State* tolua_S)
{
    int argc = 0;
    fishgame::Bullet* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"fishgame.Bullet",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (fishgame::Bullet*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj)
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_fishgame_Bullet_SetCatchRadio'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 1) 
    {
        int arg0;

        ok &= luaval_to_int32(tolua_S, 2,(int *)&arg0, "fishgame.Bullet:SetCatchRadio");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_fishgame_Bullet_SetCatchRadio'", nullptr);
            return 0;
        }
        cobj->SetCatchRadio(arg0);
        return 0;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "fishgame.Bullet:SetCatchRadio",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_fishgame_Bullet_SetCatchRadio'.",&tolua_err);
#endif

    return 0;
}
int lua_fishgame_Bullet_GetCannonSetType(lua_State* tolua_S)
{
    int argc = 0;
    fishgame::Bullet* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"fishgame.Bullet",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (fishgame::Bullet*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_fishgame_Bullet_GetCannonSetType'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_fishgame_Bullet_GetCannonSetType'", nullptr);
            return 0;
        }
        int ret = cobj->GetCannonSetType();
        tolua_pushnumber(tolua_S,(lua_Number)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "fishgame.Bullet:GetCannonSetType",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_fishgame_Bullet_GetCannonSetType'.",&tolua_err);
#endif

    return 0;
}

int lua_fishgame_Bullet_GetCatchRadio(lua_State* tolua_S)
{
    int argc = 0;
    fishgame::Bullet* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"fishgame.Bullet",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (fishgame::Bullet*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_fishgame_Bullet_GetCatchRadio'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_fishgame_Bullet_GetCatchRadio'", nullptr);
            return 0;
        }
        int ret = cobj->GetCatchRadio();
        tolua_pushnumber(tolua_S,(lua_Number)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "fishgame.Bullet:GetCatchRadio",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_fishgame_Bullet_GetCatchRadio'.",&tolua_err);
#endif

    return 0;
}
int lua_fishgame_Bullet_OnUpdate(lua_State* tolua_S)
{
    int argc = 0;
    fishgame::Bullet* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"fishgame.Bullet",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (fishgame::Bullet*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_fishgame_Bullet_OnUpdate'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 1) 
    {
        double arg0;

        ok &= luaval_to_number(tolua_S, 2,&arg0, "fishgame.Bullet:OnUpdate");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_fishgame_Bullet_OnUpdate'", nullptr);
            return 0;
        }
        cobj->OnUpdate(arg0);
        return 0;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "fishgame.Bullet:OnUpdate",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_fishgame_Bullet_OnUpdate'.",&tolua_err);
#endif

    return 0;
}
int lua_fishgame_Bullet_SetCannonSetType(lua_State* tolua_S)
{
    int argc = 0;
    fishgame::Bullet* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"fishgame.Bullet",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (fishgame::Bullet*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_fishgame_Bullet_SetCannonSetType'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 1) 
    {
        int arg0;

        ok &= luaval_to_int32(tolua_S, 2,(int *)&arg0, "fishgame.Bullet:SetCannonSetType");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_fishgame_Bullet_SetCannonSetType'", nullptr);
            return 0;
        }
        cobj->SetCannonSetType(arg0);
        return 0;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "fishgame.Bullet:SetCannonSetType",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_fishgame_Bullet_SetCannonSetType'.",&tolua_err);
#endif

    return 0;
}
int lua_fishgame_Bullet_GetCannonType(lua_State* tolua_S)
{
    int argc = 0;
    fishgame::Bullet* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"fishgame.Bullet",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (fishgame::Bullet*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_fishgame_Bullet_GetCannonType'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_fishgame_Bullet_GetCannonType'", nullptr);
            return 0;
        }
        int ret = cobj->GetCannonType();
        tolua_pushnumber(tolua_S,(lua_Number)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "fishgame.Bullet:GetCannonType",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_fishgame_Bullet_GetCannonType'.",&tolua_err);
#endif

    return 0;
}
int lua_fishgame_Bullet_SetCannonType(lua_State* tolua_S)
{
    int argc = 0;
    fishgame::Bullet* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"fishgame.Bullet",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (fishgame::Bullet*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_fishgame_Bullet_SetCannonType'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 1) 
    {
        int arg0;

        ok &= luaval_to_int32(tolua_S, 2,(int *)&arg0, "fishgame.Bullet:SetCannonType");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_fishgame_Bullet_SetCannonType'", nullptr);
            return 0;
        }
        cobj->SetCannonType(arg0);
        return 0;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "fishgame.Bullet:SetCannonType",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_fishgame_Bullet_SetCannonType'.",&tolua_err);
#endif

    return 0;
}
int lua_fishgame_Bullet_SetState(lua_State* tolua_S)
{
    int argc = 0;
    fishgame::Bullet* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"fishgame.Bullet",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (fishgame::Bullet*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_fishgame_Bullet_SetState'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 1) 
    {
        int arg0;

        ok &= luaval_to_int32(tolua_S, 2,(int *)&arg0, "fishgame.Bullet:SetState");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_fishgame_Bullet_SetState'", nullptr);
            return 0;
        }
        cobj->SetState(arg0);
        return 0;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "fishgame.Bullet:SetState",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_fishgame_Bullet_SetState'.",&tolua_err);
#endif

    return 0;
}
int lua_fishgame_Bullet_Create(lua_State* tolua_S)
{
    int argc = 0;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif

#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertable(tolua_S,1,"fishgame.Bullet",0,&tolua_err)) goto tolua_lerror;
#endif

    argc = lua_gettop(tolua_S) - 1;

    if (argc == 0)
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_fishgame_Bullet_Create'", nullptr);
            return 0;
        }
        fishgame::Bullet* ret = fishgame::Bullet::Create();
        object_to_luaval<fishgame::Bullet>(tolua_S, "fishgame.Bullet",(fishgame::Bullet*)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d\n ", "fishgame.Bullet:Create",argc, 0);
    return 0;
#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_fishgame_Bullet_Create'.",&tolua_err);
#endif
    return 0;
}
static int lua_fishgame_Bullet_finalize(lua_State* tolua_S)
{
    printf("luabindings: finalizing LUA object (Bullet)");
    return 0;
}

int lua_register_fishgame_Bullet(lua_State* tolua_S)
{
    tolua_usertype(tolua_S,"fishgame.Bullet");
    tolua_cclass(tolua_S,"Bullet","fishgame.Bullet","fishgame.MyObject",nullptr);

    tolua_beginmodule(tolua_S,"Bullet");
        tolua_function(tolua_S,"SetCatchRadio",lua_fishgame_Bullet_SetCatchRadio);
        tolua_function(tolua_S,"GetCannonSetType",lua_fishgame_Bullet_GetCannonSetType);
        tolua_function(tolua_S,"GetCatchRadio",lua_fishgame_Bullet_GetCatchRadio);
        tolua_function(tolua_S,"OnUpdate",lua_fishgame_Bullet_OnUpdate);
        tolua_function(tolua_S,"SetCannonSetType",lua_fishgame_Bullet_SetCannonSetType);
        tolua_function(tolua_S,"GetCannonType",lua_fishgame_Bullet_GetCannonType);
        tolua_function(tolua_S,"SetCannonType",lua_fishgame_Bullet_SetCannonType);
        tolua_function(tolua_S,"SetState",lua_fishgame_Bullet_SetState);
        tolua_function(tolua_S,"Create", lua_fishgame_Bullet_Create);
    tolua_endmodule(tolua_S);
    std::string typeName = typeid(fishgame::Bullet).name();
    g_luaType[typeName] = "fishgame.Bullet";
    g_typeCast["Bullet"] = "fishgame.Bullet";
    return 1;
}

int lua_fishgame_MoveCompent_GetOffest(lua_State* tolua_S)
{
    int argc = 0;
    fishgame::MoveCompent* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"fishgame.MoveCompent",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (fishgame::MoveCompent*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_fishgame_MoveCompent_GetOffest'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_fishgame_MoveCompent_GetOffest'", nullptr);
            return 0;
        }
        const cocos2d::Point& ret = cobj->GetOffest();
        point_to_luaval(tolua_S, ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "fishgame.MoveCompent:GetOffest",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_fishgame_MoveCompent_GetOffest'.",&tolua_err);
#endif

    return 0;
}
int lua_fishgame_MoveCompent_GetDelay(lua_State* tolua_S)
{
    int argc = 0;
    fishgame::MoveCompent* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"fishgame.MoveCompent",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (fishgame::MoveCompent*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_fishgame_MoveCompent_GetDelay'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_fishgame_MoveCompent_GetDelay'", nullptr);
            return 0;
        }
        double ret = cobj->GetDelay();
        tolua_pushnumber(tolua_S,(lua_Number)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "fishgame.MoveCompent:GetDelay",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_fishgame_MoveCompent_GetDelay'.",&tolua_err);
#endif

    return 0;
}
int lua_fishgame_MoveCompent_bTroop(lua_State* tolua_S)
{
    int argc = 0;
    fishgame::MoveCompent* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"fishgame.MoveCompent",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (fishgame::MoveCompent*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_fishgame_MoveCompent_bTroop'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_fishgame_MoveCompent_bTroop'", nullptr);
            return 0;
        }
        bool ret = cobj->bTroop();
        tolua_pushboolean(tolua_S,(bool)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "fishgame.MoveCompent:bTroop",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_fishgame_MoveCompent_bTroop'.",&tolua_err);
#endif

    return 0;
}
int lua_fishgame_MoveCompent_GetSpeed(lua_State* tolua_S)
{
    int argc = 0;
    fishgame::MoveCompent* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"fishgame.MoveCompent",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (fishgame::MoveCompent*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_fishgame_MoveCompent_GetSpeed'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_fishgame_MoveCompent_GetSpeed'", nullptr);
            return 0;
        }
        double ret = cobj->GetSpeed();
        tolua_pushnumber(tolua_S,(lua_Number)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "fishgame.MoveCompent:GetSpeed",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_fishgame_MoveCompent_GetSpeed'.",&tolua_err);
#endif

    return 0;
}
int lua_fishgame_MoveCompent_GetOwner(lua_State* tolua_S)
{
    int argc = 0;
    fishgame::MoveCompent* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"fishgame.MoveCompent",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (fishgame::MoveCompent*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_fishgame_MoveCompent_GetOwner'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_fishgame_MoveCompent_GetOwner'", nullptr);
            return 0;
        }
        fishgame::MyObject* ret = cobj->GetOwner();
        object_to_luaval<fishgame::MyObject>(tolua_S, "fishgame.MyObject",(fishgame::MyObject*)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "fishgame.MoveCompent:GetOwner",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_fishgame_MoveCompent_GetOwner'.",&tolua_err);
#endif

    return 0;
}
int lua_fishgame_MoveCompent_SetOwner(lua_State* tolua_S)
{
    int argc = 0;
    fishgame::MoveCompent* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"fishgame.MoveCompent",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (fishgame::MoveCompent*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_fishgame_MoveCompent_SetOwner'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 1) 
    {
        fishgame::MyObject* arg0;

        ok &= luaval_to_object<fishgame::MyObject>(tolua_S, 2, "fishgame.MyObject",&arg0);
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_fishgame_MoveCompent_SetOwner'", nullptr);
            return 0;
        }
        cobj->SetOwner(arg0);
        return 0;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "fishgame.MoveCompent:SetOwner",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_fishgame_MoveCompent_SetOwner'.",&tolua_err);
#endif

    return 0;
}
int lua_fishgame_MoveCompent_IsPaused(lua_State* tolua_S)
{
    int argc = 0;
    fishgame::MoveCompent* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"fishgame.MoveCompent",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (fishgame::MoveCompent*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_fishgame_MoveCompent_IsPaused'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_fishgame_MoveCompent_IsPaused'", nullptr);
            return 0;
        }
        bool ret = cobj->IsPaused();
        tolua_pushboolean(tolua_S,(bool)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "fishgame.MoveCompent:IsPaused",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_fishgame_MoveCompent_IsPaused'.",&tolua_err);
#endif

    return 0;
}
int lua_fishgame_MoveCompent_SetPathID(lua_State* tolua_S)
{
    int argc = 0;
    fishgame::MoveCompent* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"fishgame.MoveCompent",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (fishgame::MoveCompent*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_fishgame_MoveCompent_SetPathID'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 1) 
    {
        int arg0;

        ok &= luaval_to_int32(tolua_S, 2,(int *)&arg0, "fishgame.MoveCompent:SetPathID");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_fishgame_MoveCompent_SetPathID'", nullptr);
            return 0;
        }
        cobj->SetPathID(arg0);
        return 0;
    }
    if (argc == 2) 
    {
        int arg0;
        bool arg1;

        ok &= luaval_to_int32(tolua_S, 2,(int *)&arg0, "fishgame.MoveCompent:SetPathID");

        ok &= luaval_to_boolean(tolua_S, 3,&arg1, "fishgame.MoveCompent:SetPathID");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_fishgame_MoveCompent_SetPathID'", nullptr);
            return 0;
        }
        cobj->SetPathID(arg0, arg1);
        return 0;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "fishgame.MoveCompent:SetPathID",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_fishgame_MoveCompent_SetPathID'.",&tolua_err);
#endif

    return 0;
}
int lua_fishgame_MoveCompent_HasBeginMove(lua_State* tolua_S)
{
    int argc = 0;
    fishgame::MoveCompent* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"fishgame.MoveCompent",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (fishgame::MoveCompent*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_fishgame_MoveCompent_HasBeginMove'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_fishgame_MoveCompent_HasBeginMove'", nullptr);
            return 0;
        }
        bool ret = cobj->HasBeginMove();
        tolua_pushboolean(tolua_S,(bool)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "fishgame.MoveCompent:HasBeginMove",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_fishgame_MoveCompent_HasBeginMove'.",&tolua_err);
#endif

    return 0;
}
int lua_fishgame_MoveCompent_OnUpdate(lua_State* tolua_S)
{
    int argc = 0;
    fishgame::MoveCompent* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"fishgame.MoveCompent",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (fishgame::MoveCompent*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_fishgame_MoveCompent_OnUpdate'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 1) 
    {
        double arg0;

        ok &= luaval_to_number(tolua_S, 2,&arg0, "fishgame.MoveCompent:OnUpdate");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_fishgame_MoveCompent_OnUpdate'", nullptr);
            return 0;
        }
        cobj->OnUpdate(arg0);
        return 0;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "fishgame.MoveCompent:OnUpdate",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_fishgame_MoveCompent_OnUpdate'.",&tolua_err);
#endif

    return 0;
}
int lua_fishgame_MoveCompent_IsEndPath(lua_State* tolua_S)
{
    int argc = 0;
    fishgame::MoveCompent* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"fishgame.MoveCompent",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (fishgame::MoveCompent*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_fishgame_MoveCompent_IsEndPath'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_fishgame_MoveCompent_IsEndPath'", nullptr);
            return 0;
        }
        bool ret = cobj->IsEndPath();
        tolua_pushboolean(tolua_S,(bool)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "fishgame.MoveCompent:IsEndPath",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_fishgame_MoveCompent_IsEndPath'.",&tolua_err);
#endif

    return 0;
}
int lua_fishgame_MoveCompent_Rebound(lua_State* tolua_S)
{
    int argc = 0;
    fishgame::MoveCompent* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"fishgame.MoveCompent",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (fishgame::MoveCompent*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_fishgame_MoveCompent_Rebound'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_fishgame_MoveCompent_Rebound'", nullptr);
            return 0;
        }
        bool ret = cobj->Rebound();
        tolua_pushboolean(tolua_S,(bool)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "fishgame.MoveCompent:Rebound",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_fishgame_MoveCompent_Rebound'.",&tolua_err);
#endif

    return 0;
}
int lua_fishgame_MoveCompent_SetDirection(lua_State* tolua_S)
{
    int argc = 0;
    fishgame::MoveCompent* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"fishgame.MoveCompent",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (fishgame::MoveCompent*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_fishgame_MoveCompent_SetDirection'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 1) 
    {
        double arg0;

        ok &= luaval_to_number(tolua_S, 2,&arg0, "fishgame.MoveCompent:SetDirection");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_fishgame_MoveCompent_SetDirection'", nullptr);
            return 0;
        }
        cobj->SetDirection(arg0);
        return 0;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "fishgame.MoveCompent:SetDirection",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_fishgame_MoveCompent_SetDirection'.",&tolua_err);
#endif

    return 0;
}
int lua_fishgame_MoveCompent_SetPause(lua_State* tolua_S)
{
    int argc = 0;
    fishgame::MoveCompent* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"fishgame.MoveCompent",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (fishgame::MoveCompent*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_fishgame_MoveCompent_SetPause'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_fishgame_MoveCompent_SetPause'", nullptr);
            return 0;
        }
        cobj->SetPause();
        return 0;
    }
    if (argc == 1) 
    {
        bool arg0;

        ok &= luaval_to_boolean(tolua_S, 2,&arg0, "fishgame.MoveCompent:SetPause");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_fishgame_MoveCompent_SetPause'", nullptr);
            return 0;
        }
        cobj->SetPause(arg0);
        return 0;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "fishgame.MoveCompent:SetPause",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_fishgame_MoveCompent_SetPause'.",&tolua_err);
#endif

    return 0;
}
int lua_fishgame_MoveCompent_OnDetach(lua_State* tolua_S)
{
    int argc = 0;
    fishgame::MoveCompent* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"fishgame.MoveCompent",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (fishgame::MoveCompent*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_fishgame_MoveCompent_OnDetach'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_fishgame_MoveCompent_OnDetach'", nullptr);
            return 0;
        }
        cobj->OnDetach();
        return 0;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "fishgame.MoveCompent:OnDetach",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_fishgame_MoveCompent_OnDetach'.",&tolua_err);
#endif

    return 0;
}
int lua_fishgame_MoveCompent_GetPathID(lua_State* tolua_S)
{
    int argc = 0;
    fishgame::MoveCompent* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"fishgame.MoveCompent",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (fishgame::MoveCompent*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_fishgame_MoveCompent_GetPathID'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_fishgame_MoveCompent_GetPathID'", nullptr);
            return 0;
        }
        int ret = cobj->GetPathID();
        tolua_pushnumber(tolua_S,(lua_Number)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "fishgame.MoveCompent:GetPathID",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_fishgame_MoveCompent_GetPathID'.",&tolua_err);
#endif

    return 0;
}
int lua_fishgame_MoveCompent_SetDelay(lua_State* tolua_S)
{
    int argc = 0;
    fishgame::MoveCompent* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"fishgame.MoveCompent",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (fishgame::MoveCompent*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_fishgame_MoveCompent_SetDelay'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 1) 
    {
        double arg0;

        ok &= luaval_to_number(tolua_S, 2,&arg0, "fishgame.MoveCompent:SetDelay");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_fishgame_MoveCompent_SetDelay'", nullptr);
            return 0;
        }
        cobj->SetDelay(arg0);
        return 0;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "fishgame.MoveCompent:SetDelay",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_fishgame_MoveCompent_SetDelay'.",&tolua_err);
#endif

    return 0;
}
int lua_fishgame_MoveCompent_SetRebound(lua_State* tolua_S)
{
    int argc = 0;
    fishgame::MoveCompent* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"fishgame.MoveCompent",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (fishgame::MoveCompent*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_fishgame_MoveCompent_SetRebound'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 1) 
    {
        bool arg0;

        ok &= luaval_to_boolean(tolua_S, 2,&arg0, "fishgame.MoveCompent:SetRebound");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_fishgame_MoveCompent_SetRebound'", nullptr);
            return 0;
        }
        cobj->SetRebound(arg0);
        return 0;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "fishgame.MoveCompent:SetRebound",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_fishgame_MoveCompent_SetRebound'.",&tolua_err);
#endif

    return 0;
}
int lua_fishgame_MoveCompent_SetSpeed(lua_State* tolua_S)
{
    int argc = 0;
    fishgame::MoveCompent* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"fishgame.MoveCompent",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (fishgame::MoveCompent*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_fishgame_MoveCompent_SetSpeed'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 1) 
    {
        double arg0;

        ok &= luaval_to_number(tolua_S, 2,&arg0, "fishgame.MoveCompent:SetSpeed");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_fishgame_MoveCompent_SetSpeed'", nullptr);
            return 0;
        }
        cobj->SetSpeed(arg0);
        return 0;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "fishgame.MoveCompent:SetSpeed",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_fishgame_MoveCompent_SetSpeed'.",&tolua_err);
#endif

    return 0;
}
int lua_fishgame_MoveCompent_InitMove(lua_State* tolua_S)
{
    int argc = 0;
    fishgame::MoveCompent* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"fishgame.MoveCompent",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (fishgame::MoveCompent*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_fishgame_MoveCompent_InitMove'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_fishgame_MoveCompent_InitMove'", nullptr);
            return 0;
        }
        cobj->InitMove();
        return 0;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "fishgame.MoveCompent:InitMove",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_fishgame_MoveCompent_InitMove'.",&tolua_err);
#endif

    return 0;
}
int lua_fishgame_MoveCompent_OnAttach(lua_State* tolua_S)
{
    int argc = 0;
    fishgame::MoveCompent* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"fishgame.MoveCompent",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (fishgame::MoveCompent*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_fishgame_MoveCompent_OnAttach'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_fishgame_MoveCompent_OnAttach'", nullptr);
            return 0;
        }
        cobj->OnAttach();
        return 0;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "fishgame.MoveCompent:OnAttach",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_fishgame_MoveCompent_OnAttach'.",&tolua_err);
#endif

    return 0;
}
int lua_fishgame_MoveCompent_SetOffest(lua_State* tolua_S)
{
    int argc = 0;
    fishgame::MoveCompent* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"fishgame.MoveCompent",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (fishgame::MoveCompent*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_fishgame_MoveCompent_SetOffest'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 1) 
    {
        cocos2d::Point arg0;

        ok &= luaval_to_point(tolua_S, 2, &arg0, "fishgame.MoveCompent:SetOffest");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_fishgame_MoveCompent_SetOffest'", nullptr);
            return 0;
        }
        cobj->SetOffest(arg0);
        return 0;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "fishgame.MoveCompent:SetOffest",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_fishgame_MoveCompent_SetOffest'.",&tolua_err);
#endif

    return 0;
}
int lua_fishgame_MoveCompent_SetEndPath(lua_State* tolua_S)
{
    int argc = 0;
    fishgame::MoveCompent* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"fishgame.MoveCompent",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (fishgame::MoveCompent*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_fishgame_MoveCompent_SetEndPath'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 1) 
    {
        bool arg0;

        ok &= luaval_to_boolean(tolua_S, 2,&arg0, "fishgame.MoveCompent:SetEndPath");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_fishgame_MoveCompent_SetEndPath'", nullptr);
            return 0;
        }
        cobj->SetEndPath(arg0);
        return 0;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "fishgame.MoveCompent:SetEndPath",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_fishgame_MoveCompent_SetEndPath'.",&tolua_err);
#endif

    return 0;
}
int lua_fishgame_MoveCompent_SetPosition(lua_State* tolua_S)
{
    int argc = 0;
    fishgame::MoveCompent* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"fishgame.MoveCompent",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (fishgame::MoveCompent*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_fishgame_MoveCompent_SetPosition'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 2) 
    {
        double arg0;
        double arg1;

        ok &= luaval_to_number(tolua_S, 2,&arg0, "fishgame.MoveCompent:SetPosition");

        ok &= luaval_to_number(tolua_S, 3,&arg1, "fishgame.MoveCompent:SetPosition");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_fishgame_MoveCompent_SetPosition'", nullptr);
            return 0;
        }
        cobj->SetPosition(arg0, arg1);
        return 0;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "fishgame.MoveCompent:SetPosition",argc, 2);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_fishgame_MoveCompent_SetPosition'.",&tolua_err);
#endif

    return 0;
}
static int lua_fishgame_MoveCompent_finalize(lua_State* tolua_S)
{
    printf("luabindings: finalizing LUA object (MoveCompent)");
    return 0;
}

int lua_register_fishgame_MoveCompent(lua_State* tolua_S)
{
    tolua_usertype(tolua_S,"fishgame.MoveCompent");
    tolua_cclass(tolua_S,"MoveCompent","fishgame.MoveCompent","cc.Ref",nullptr);

    tolua_beginmodule(tolua_S,"MoveCompent");
        tolua_function(tolua_S,"GetOffest",lua_fishgame_MoveCompent_GetOffest);
        tolua_function(tolua_S,"GetDelay",lua_fishgame_MoveCompent_GetDelay);
        tolua_function(tolua_S,"bTroop",lua_fishgame_MoveCompent_bTroop);
        tolua_function(tolua_S,"GetSpeed",lua_fishgame_MoveCompent_GetSpeed);
        tolua_function(tolua_S,"GetOwner",lua_fishgame_MoveCompent_GetOwner);
        tolua_function(tolua_S,"SetOwner",lua_fishgame_MoveCompent_SetOwner);
        tolua_function(tolua_S,"IsPaused",lua_fishgame_MoveCompent_IsPaused);
        tolua_function(tolua_S,"SetPathID",lua_fishgame_MoveCompent_SetPathID);
        tolua_function(tolua_S,"HasBeginMove",lua_fishgame_MoveCompent_HasBeginMove);
        tolua_function(tolua_S,"OnUpdate",lua_fishgame_MoveCompent_OnUpdate);
        tolua_function(tolua_S,"IsEndPath",lua_fishgame_MoveCompent_IsEndPath);
        tolua_function(tolua_S,"Rebound",lua_fishgame_MoveCompent_Rebound);
        tolua_function(tolua_S,"SetDirection",lua_fishgame_MoveCompent_SetDirection);
        tolua_function(tolua_S,"SetPause",lua_fishgame_MoveCompent_SetPause);
        tolua_function(tolua_S,"OnDetach",lua_fishgame_MoveCompent_OnDetach);
        tolua_function(tolua_S,"GetPathID",lua_fishgame_MoveCompent_GetPathID);
        tolua_function(tolua_S,"SetDelay",lua_fishgame_MoveCompent_SetDelay);
        tolua_function(tolua_S,"SetRebound",lua_fishgame_MoveCompent_SetRebound);
        tolua_function(tolua_S,"SetSpeed",lua_fishgame_MoveCompent_SetSpeed);
        tolua_function(tolua_S,"InitMove",lua_fishgame_MoveCompent_InitMove);
        tolua_function(tolua_S,"OnAttach",lua_fishgame_MoveCompent_OnAttach);
        tolua_function(tolua_S,"SetOffest",lua_fishgame_MoveCompent_SetOffest);
        tolua_function(tolua_S,"SetEndPath",lua_fishgame_MoveCompent_SetEndPath);
        tolua_function(tolua_S,"SetPosition",lua_fishgame_MoveCompent_SetPosition);
    tolua_endmodule(tolua_S);
    std::string typeName = typeid(fishgame::MoveCompent).name();
    g_luaType[typeName] = "fishgame.MoveCompent";
    g_typeCast["MoveCompent"] = "fishgame.MoveCompent";
    return 1;
}

int lua_fishgame_MoveByPath_OnUpdate(lua_State* tolua_S)
{
    int argc = 0;
    fishgame::MoveByPath* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"fishgame.MoveByPath",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (fishgame::MoveByPath*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_fishgame_MoveByPath_OnUpdate'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 1) 
    {
        double arg0;

        ok &= luaval_to_number(tolua_S, 2,&arg0, "fishgame.MoveByPath:OnUpdate");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_fishgame_MoveByPath_OnUpdate'", nullptr);
            return 0;
        }
        cobj->OnUpdate(arg0);
        return 0;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "fishgame.MoveByPath:OnUpdate",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_fishgame_MoveByPath_OnUpdate'.",&tolua_err);
#endif

    return 0;
}
int lua_fishgame_MoveByPath_OnDetach(lua_State* tolua_S)
{
    int argc = 0;
    fishgame::MoveByPath* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"fishgame.MoveByPath",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (fishgame::MoveByPath*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_fishgame_MoveByPath_OnDetach'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_fishgame_MoveByPath_OnDetach'", nullptr);
            return 0;
        }
        cobj->OnDetach();
        return 0;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "fishgame.MoveByPath:OnDetach",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_fishgame_MoveByPath_OnDetach'.",&tolua_err);
#endif

    return 0;
}
int lua_fishgame_MoveByPath_InitMove(lua_State* tolua_S)
{
    int argc = 0;
    fishgame::MoveByPath* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"fishgame.MoveByPath",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (fishgame::MoveByPath*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_fishgame_MoveByPath_InitMove'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_fishgame_MoveByPath_InitMove'", nullptr);
            return 0;
        }
        cobj->InitMove();
        return 0;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "fishgame.MoveByPath:InitMove",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_fishgame_MoveByPath_InitMove'.",&tolua_err);
#endif

    return 0;
}
int lua_fishgame_MoveByPath_create(lua_State* tolua_S)
{
    int argc = 0;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif

#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertable(tolua_S,1,"fishgame.MoveByPath",0,&tolua_err)) goto tolua_lerror;
#endif

    argc = lua_gettop(tolua_S) - 1;

    if (argc == 0)
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_fishgame_MoveByPath_create'", nullptr);
            return 0;
        }
        fishgame::MoveByPath* ret = fishgame::MoveByPath::create();
        object_to_luaval<fishgame::MoveByPath>(tolua_S, "fishgame.MoveByPath",(fishgame::MoveByPath*)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d\n ", "fishgame.MoveByPath:create",argc, 0);
    return 0;
#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_fishgame_MoveByPath_create'.",&tolua_err);
#endif
    return 0;
}
static int lua_fishgame_MoveByPath_finalize(lua_State* tolua_S)
{
    printf("luabindings: finalizing LUA object (MoveByPath)");
    return 0;
}

int lua_register_fishgame_MoveByPath(lua_State* tolua_S)
{
    tolua_usertype(tolua_S,"fishgame.MoveByPath");
    tolua_cclass(tolua_S,"MoveByPath","fishgame.MoveByPath","fishgame.MoveCompent",nullptr);

    tolua_beginmodule(tolua_S,"MoveByPath");
        tolua_function(tolua_S,"OnUpdate",lua_fishgame_MoveByPath_OnUpdate);
        tolua_function(tolua_S,"OnDetach",lua_fishgame_MoveByPath_OnDetach);
        tolua_function(tolua_S,"InitMove",lua_fishgame_MoveByPath_InitMove);
        tolua_function(tolua_S,"create", lua_fishgame_MoveByPath_create);
    tolua_endmodule(tolua_S);
    std::string typeName = typeid(fishgame::MoveByPath).name();
    g_luaType[typeName] = "fishgame.MoveByPath";
    g_typeCast["MoveByPath"] = "fishgame.MoveByPath";
    return 1;
}

int lua_fishgame_MoveByDirection_OnUpdate(lua_State* tolua_S)
{
    int argc = 0;
    fishgame::MoveByDirection* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"fishgame.MoveByDirection",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (fishgame::MoveByDirection*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_fishgame_MoveByDirection_OnUpdate'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 1) 
    {
        double arg0;

        ok &= luaval_to_number(tolua_S, 2,&arg0, "fishgame.MoveByDirection:OnUpdate");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_fishgame_MoveByDirection_OnUpdate'", nullptr);
            return 0;
        }
        cobj->OnUpdate(arg0);
        return 0;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "fishgame.MoveByDirection:OnUpdate",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_fishgame_MoveByDirection_OnUpdate'.",&tolua_err);
#endif

    return 0;
}
int lua_fishgame_MoveByDirection_OnDetach(lua_State* tolua_S)
{
    int argc = 0;
    fishgame::MoveByDirection* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"fishgame.MoveByDirection",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (fishgame::MoveByDirection*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_fishgame_MoveByDirection_OnDetach'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_fishgame_MoveByDirection_OnDetach'", nullptr);
            return 0;
        }
        cobj->OnDetach();
        return 0;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "fishgame.MoveByDirection:OnDetach",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_fishgame_MoveByDirection_OnDetach'.",&tolua_err);
#endif

    return 0;
}
int lua_fishgame_MoveByDirection_InitMove(lua_State* tolua_S)
{
    int argc = 0;
    fishgame::MoveByDirection* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"fishgame.MoveByDirection",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (fishgame::MoveByDirection*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_fishgame_MoveByDirection_InitMove'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_fishgame_MoveByDirection_InitMove'", nullptr);
            return 0;
        }
        cobj->InitMove();
        return 0;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "fishgame.MoveByDirection:InitMove",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_fishgame_MoveByDirection_InitMove'.",&tolua_err);
#endif

    return 0;
}
int lua_fishgame_MoveByDirection_create(lua_State* tolua_S)
{
    int argc = 0;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif

#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertable(tolua_S,1,"fishgame.MoveByDirection",0,&tolua_err)) goto tolua_lerror;
#endif

    argc = lua_gettop(tolua_S) - 1;

    if (argc == 0)
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_fishgame_MoveByDirection_create'", nullptr);
            return 0;
        }
        fishgame::MoveByDirection* ret = fishgame::MoveByDirection::create();
        object_to_luaval<fishgame::MoveByDirection>(tolua_S, "fishgame.MoveByDirection",(fishgame::MoveByDirection*)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d\n ", "fishgame.MoveByDirection:create",argc, 0);
    return 0;
#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_fishgame_MoveByDirection_create'.",&tolua_err);
#endif
    return 0;
}
static int lua_fishgame_MoveByDirection_finalize(lua_State* tolua_S)
{
    printf("luabindings: finalizing LUA object (MoveByDirection)");
    return 0;
}

int lua_register_fishgame_MoveByDirection(lua_State* tolua_S)
{
    tolua_usertype(tolua_S,"fishgame.MoveByDirection");
    tolua_cclass(tolua_S,"MoveByDirection","fishgame.MoveByDirection","fishgame.MoveCompent",nullptr);

    tolua_beginmodule(tolua_S,"MoveByDirection");
        tolua_function(tolua_S,"OnUpdate",lua_fishgame_MoveByDirection_OnUpdate);
        tolua_function(tolua_S,"OnDetach",lua_fishgame_MoveByDirection_OnDetach);
        tolua_function(tolua_S,"InitMove",lua_fishgame_MoveByDirection_InitMove);
        tolua_function(tolua_S,"create", lua_fishgame_MoveByDirection_create);
    tolua_endmodule(tolua_S);
    std::string typeName = typeid(fishgame::MoveByDirection).name();
    g_luaType[typeName] = "fishgame.MoveByDirection";
    g_typeCast["MoveByDirection"] = "fishgame.MoveByDirection";
    return 1;
}
TOLUA_API int register_all_fishgame(lua_State* tolua_S)
{
	tolua_open(tolua_S);
	
	tolua_module(tolua_S,"fishgame",0);
	tolua_beginmodule(tolua_S,"fishgame");

	lua_register_fishgame_FishObjectManager(tolua_S);
	lua_register_fishgame_MyObject(tolua_S);
	lua_register_fishgame_Bullet(tolua_S);
	lua_register_fishgame_MoveCompent(tolua_S);
	lua_register_fishgame_MoveByDirection(tolua_S);
	lua_register_fishgame_Fish(tolua_S);
	lua_register_fishgame_MoveByPath(tolua_S);
	lua_register_fishgame_FishUtils(tolua_S);
	lua_register_fishgame_MyScene(tolua_S);

	tolua_endmodule(tolua_S);
	return 1;
}

