//
//  CPasteboard.cpp
//  复制到剪切板
//
//  Created by mac on 16/5/27.
//  Copyright © 2016年 hixiaosan. All rights reserved.
//

#include "CPasteboard.h"
#import <UIKit/UIKit.h>

void CPasteboard::setPasteboardText(const std::string &szText)
{
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    pasteboard.string = [[NSString alloc] initWithUTF8String:szText.c_str()];
    
}

std::string CPasteboard::getPasteboardText()
{
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    std::string retString = [pasteboard.string cStringUsingEncoding:NSUTF8StringEncoding];
    
    return retString;
}