﻿#ifndef CMD_PLAZA_HEAD_FILE
#define CMD_PLAZA_HEAD_FILE
#pragma pack(1)

#ifdef CC_TARGET_PLATFORM
#include "ComDefine.h"
#endif

//////////////////////////////////////////////////////////////////////////

//广场版本
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
#define VER_PLAZA_LOW               4                                   //广场版本
#define VER_PLAZA_HIGH              23                                  //广场版本
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
#define VER_PLAZA_LOW               4                                   //广场版本
#define VER_PLAZA_HIGH              24                                  //广场版本
#endif

#define VER_PLAZA_FRAME             MAKELONG(VER_PLAZA_LOW,VER_PLAZA_HIGH)

#define REGISTER_KEY					"$WEF#D23dff$#%VDS#442"			//openid注册KEY

//程序版本
#define VERSION_FRAME				PROCESS_VERSION(6,0,3)				//框架版本
#define VERSION_PLAZA				PROCESS_VERSION(10,0,3)				//大厅版本
#define VERSION_MOBILE_ANDROID		PROCESS_VERSION(6,0,3)				//手机版本
#define VERSION_MOBILE_IOS			PROCESS_VERSION(6,0,3)				//手机版本

//////////////////////////////////////////////////////////////////////////
//登录错误标识

#define ERC_GP_LOGON_SUCCESS			0								//登陆成功
#define ERC_GP_ACCOUNTS_NOT_EXIST		1								//帐号不存在
#define ERC_GP_LONG_NULLITY				2								//禁止登录
#define ERC_GP_PASSWORD_ERCOR			3								//密码错误

//////////////////////////////////////////////////////////////////////////
//登录命令码

#define MDM_GP_LOGON					1								//广场登录

//#define SUB_GP_LOGON_ACCOUNTS			1								//帐号登录
//#define SUB_GP_LOGON_USERID				2								//I D 登录
#define SUB_GP_REGISTER_ACCOUNTS		3								//注册帐号
#define SUB_MB_REGISTER_ACCOUNTSWITHSPREADERID  4
#define SUB_GP_UPLOAD_CUSTOM_FACE		4								//定义头像
#define SUB_GP_UPDATE_USER_SCORE		5								//读取金币
#define SUB_GP_LOAD_FRIEND				6								//读取好友
#define SUB_GP_SILIAO					7								//私聊
#define SUB_GP_HEARTBEAT				8								//心跳
#define SUB_GP_FRIEDCOMPANION			9								//好友关系
#define SUB_GP_STOREHAYYPBEAR			10								//存欢乐豆
#define SUB_GP_GETHAYYPBEAR			    11								//取欢乐豆
#define SUB_GP_LINK_SERVER			    2								//连接服务器
#define SUB_GP_BANK_TRANSFER			13								//银行转账
#define SUB_GP_LOGON_OPENID				14								//OpenID登录
#define SUB_GP_REGISTER_OPENID			15								//OpenID注册

#define SUB_GP_LOGON_FINISH				102								//登陆完成
#define SUB_GP_UPDATE_USER_SCORE_FINISH	103								//读取金币完成
#define SUB_GP_LOAD_FRIEND_RES			104								//读取好友回应
#define SUB_GP_OPERATE_BANK_FINISH     	105								//操作银行完成
#define SUB_GP_LINK_SERVER_FINISH     	106								//连接服务器完成
#define SUB_GP_REGISTER_SUCCESS			107								//注册帐号成功
#define SUB_GP_REGISTER_OPENID_SUCCESS	108								//OPENID注册成功


//登录命令
#define MDM_MB_LOGON				100									//广场登录

//登录模式
#define SUB_MB_LOGON_GAMEID			1									//I D 登录
#define SUB_MB_LOGON_ACCOUNTS		2									//帐号登录
#define SUB_MB_LOGON_TOURIST        3                                 //游客登录 YUNG
#define SUB_MB_REGISTER_ACCOUNTS	4									//注册帐号

#define SUB_MB_GET_AUDIT_STATE      7                                   //请求审核状态信息
#define SUB_MB_RES_AUDIT_STATE      8                                   //审核状态返回
#define SUB_GP_LOGON_WEIXINMB            9                              //微信登陆
#define SUB_MB_APPLE_PAY_REQUEST    10                              //临时使用于苹果审核充值请求
#define SUB_MB_APPLE_PAY_RESPONSE    11                             //临时使用于苹果审核充值返回
#define SUB_MB_ANDROID_VERSION       13                             //发送安卓版本
#define SUB_MB_ANDROID_VERSION_RESPONSE       14                    //安卓版本返回


//登录结果
#define SUB_MB_LOGON_SUCCESS		100									//登录成功
#define SUB_MB_LOGON_FAILURE		101									//登录失败
#define SUB_MB_LOGON_WEIXIN_SUCCESS 107                                 //登陆成功
#define SUB_GP_VALIDATE_PASSPORT_ID 106                                 //登录失败
//升级提示
#define SUB_MB_UPDATE_NOTIFY		200									//升级提示
#define SUB_MB_BANK_OPEN			201									//银行允许
#define SUB_MB_BANK_CLOSE			202									//银行禁止


#define SUB_MB_MODIFICATION_VISITOR    5                //客户端请求游客转正
#define SUB_MB_MODIFICA_VISITOR_SUCCESS    150                //服务器返回游客转正成功


struct CMD_MB_AUDIT_STATE
{
    CHAR            szVersion[10];      //客服端版本
};

struct CMD_MB_ANDROID_BERSION
{
    CHAR            szVersion[10];      //客服端版本
};

//客户端请求游客转正
struct CMD_MB_ModificationVisitor
{
    CHAR            szMachineID[PASS_LEN];      //机器标识
    CHAR            szPassword[PASS_LEN];       //登录密码
    CHAR            szAccounts[NAME_LEN];       //登录帐号
    CHAR            szNickname[NAME_LEN];       //昵称
    CHAR            szIDCard[20];               //身份证
    CHAR            szQQNumber[QQ_LEN];         //QQ号码
    CHAR            szPhoneNum[16];             //手机号
    LONGLONG        lToken;                     //验证码
};

//服务器返回游客转正成功
struct CMD_GP_ModificaVisitorSuccess
{
    CHAR              szPassword[PASS_LEN];    //登录密码
    CHAR              szAccounts[NAME_LEN];    //登录帐号
    CHAR              szNickname[NAME_LEN];    //昵称
    UINT32            dwAddInsureScore;        //游客转正奖励
};

//心跳包 SUB_GP_HEARTBEAT
struct CMD_GP_Heartbeat
{
	IosDword								dwUserID;						//用户 I D
};

//读取好友 SUB_GP_LOAD_FRIEND
struct CMD_GP_LoadFriend
{
	IosDword								dwUserID;						//用户 I D
};

//微信帐号登录
struct CMD_MB_LogonWeiXin
{
	//系统信息
	WORD              wModuleID;                        //模块标识
	UINT32            dwPlazaVersion;                   //广场版本

	//登录信息
	CHAR              szMachineID[LEN_MACHINE_ID];      //机器标识
    CHAR              szUnionID[32];                    //微信标识
    int               nTokenPass;                       //临时密码
	UINT32            dwPlatform;                       //平台ID(0：PC,非0：手机)
};

//帐号登录
struct CMD_MB_LogonAccounts
{
    //系统信息
    WORD                wModuleID;                      //模块标识
    UINT32              dwPlazaVersion;                 //广场版本
    
    //登录信息
    TCHAR               szPassword[LEN_MD5];            //登录密码
    TCHAR               szAccounts[LEN_ACCOUNTS];       //登录帐号
    //连接信息
    TCHAR               szMachineID[LEN_MACHINE_ID];    //机器标识
    BYTE                cbPassPortID;
    TCHAR               szPassPortID[19];
    UINT32              dwPlatform;                     //平台ID(0：PC,非0：手机)
};

//游客帐号登录
struct CMD_MB_LogonVisitor
{
    //系统信息
    WORD                wModuleID;                      //模块标识
    UINT32              dwPlazaVersion;                 //广场版本
    //连接信息
    TCHAR               szMachineID[LEN_MACHINE_ID];    //机器标识
    BYTE                cbPassPortID;
    TCHAR               szPassPortID[19];
    UINT32              dwPlatform;                     //平台ID(0：PC,非0：手机)

};

struct CMD_MB_LogonSuccess
{
	WORD			wFaceID;							//头像标识
	BYTE            cbGender;							//用户性别
	IosDword		dwUserID;							//用户 I D
	IosDword		dwGameID;							//游戏 I D
	IosDword		dwExperience;						//经验数值
	IosDword		dwLoveLiness;						//用户魅力
	CHAR			szNickName[32];                     //用户昵称
    double          fLastLogonTime;
    CHAR            szLogonPass[PASS_LEN];
    WORD            wLogonType;                         //用户类型 0、普通用户 1、游客
    LONGLONG        lInsureScore;                       //银行币
    LONGLONG        lScore;
    LONGLONG        lWebToken;                          //网页验证用
    WORD            wPayType;                           //充值类型 0、APPLE  1、WEB
    BYTE            cbBindPhone;                        //是否已绑定手机
    BYTE            cbBindWeixin;                       //是否已绑定微信
    BYTE            cbBanker;                           //是否是商人
    BYTE            registerOrLogin;                    //注册：1   登录：0
};

//微信登录成功
struct CMD_MB_WeiXin_LogonSuccess
{
	WORD              wFaceID;              //头像标识
	BYTE              cbGender;              //用户性别
	IosDword             dwUserID;              //用户 I D
	IosDword             dwGameID;              //游戏 I D
	IosDword             dwExperience;            //经验数值
	IosDword             dwLoveLiness;            //用户魅力
	BYTE              cbChangeInsurePass;                 //是否需要修改保险箱密码
	CHAR              szNickName[LEN_NICKNAME];      //用户昵称
	CHAR              szLogonPass[LEN_PASSWORD];
};

//登录失败
struct CMD_MB_LogonFailure
{
	int							lResultCode;						//错误代码
	CHAR							szDescribeString[128];				//描述消息
};
//OpenID登录
struct CMD_GP_LogonByOpenID
{
	IosDword								dwPlazaVersion;					//广场版本
	WORD								wKindID;						//游戏ID
	CHAR								szOpenID[OPEN_LEN];					//开放ID
	BYTE								cbOpenPlat;			//开放平台
	BYTE								cbPlatform;			//代理平台
	CHAR								szUserToken[PASS_LEN];			//访问TOKEN
	CHAR								szPassword[PASS_LEN];			//密码
};

struct CMD_GP_UpdateUserData
{
	IosDword								dwUserID;						//用户 I D
	WORD								wKindID;						//游戏ID
};
//存欢乐豆
struct CMD_GP_StoreHappyBear
{
	IosDword                               dwUserID;                       //用户ID
	LONGLONG                            lHappyBear;                     //欢乐豆数量
	CHAR                               szPassword[PASS_LEN];           //银行密码
};
//取欢乐豆
struct CMD_GP_GetHappyBear
{
	IosDword                               dwUserID;                       //用户ID
	LONGLONG                            lHappyBear;                     //欢乐豆数量
	CHAR                               szPassword[PASS_LEN];           //银行密码
};


struct CMD_GP_BankTransfer
{
	IosDword								dwSendUserID;					//赠送者
	LONGLONG							lFlowerPay;						//赠送金额
	CHAR								szRcvUserAccounts[NAME_LEN];	//接收者
	CHAR								szPassWord[PASS_LEN];			//银行密码
};

//连接房间结果 SUB_GP_LINK_SERVER_FINISH
struct CMD_GP_LinkServerRes
{
	WORD							wKindID;
	WORD							wServerID;
	WORD							wRt;								//为1时添加房间
	tagGameServer					gameServer;							
};

//私聊
struct CMD_GP_SiLiaoData
{
	IosDword								dwUserID;						//用户 I D
	IosDword								dwUserIDFriend;					//聊天对象 I D
	CHAR								szChatMessage[MAX_CHAT_LEN];	//聊天信息长度
};
//好友用户关系
struct CMD_GP_Friend
{
	IosDword								dwUserID;						//用户 I D
	IosDword								dwUserIDFriend;					//聊天对象 I D
	WORD								wCompanion;						//关系 0为没有关系 1为好友 2为黑名单(未使用)
};

struct CMD_GP_UpdateUserDataFinish
{
	LONGLONG							lScore;
	LONGLONG							lInsureScore;
	LONGLONG							lSilverScore;
	LONGLONG							lInsureSilverScore;
	int								lExperience;						//用户经验
	LONGLONG							lIngot;
	CHAR								szNickName[NAME_LEN];				//用户昵称
	WORD								wFaceID;							//头像标识
	LONGLONG							lLoveliness;						//魅力值
	INT									iGiftTicket;						//礼券
};

//I D 登录
struct CMD_GP_LogonByUserID
{
	IosDword								dwPlazaVersion;					//广场版本
	WORD								wKindID;						//游戏ID
	IosDword								dwUserID;						//用户 I D
	BYTE								cbOpenPlat;			//开放平台
	BYTE								cbPlatform;			//代理平台
	CHAR								szPassWord[PASS_LEN];			//登录密码
	CHAR								szMachineID[33];
};

//注册帐号
struct CMD_MB_RegisterAccounts
{
    WORD                wFaceID;                        //头像标识
    BYTE                cbGender;                       //用户性别
    UINT32              dwPlazaVersion;                 //广场版本
    CHAR                szSpreader[NAME_LEN];           //推广人名
    CHAR                szAccounts[NAME_LEN];           //用户名
    CHAR                szPassWord[PASS_LEN];           //登录密码
    CHAR                szBankPwd[PASS_LEN];            //银行密码
    CHAR                szNickname[NAME_LEN];           //昵称
    CHAR                szUnderWrite[63];               //个性签名
    CHAR                szCardid[20];                   //身份证号码
    CHAR                szPhoneNumber[16];              //手机号码
    CHAR                szMachineID[LEN_MACHINE_ID];    //机器标识
    CHAR                szQQNumber[QQ_LEN];             //QQ号码
    UINT32              dwPlatform;                     //平台ID(0：PC,非0：手机)
};

//OpenID注册
struct CMD_GP_RegisterOpenID
{
	IosDword							dwPlazaVersion;					//广场版本
	WORD								wKindID;						//游戏ID
	TCHAR								szOpenID[OPEN_LEN];				//登录帐号
	TCHAR								szNickName[NAME_LEN];			//登录帐号
	BYTE								cbOpenPlat;						//开放平台
	BYTE								cbPlatform;						//代理平台
	char								szMD5Result[33];				//认证编码
};

//登陆成功
struct CMD_GP_LogonSuccess
{
	WORD								wFaceID;						//头像索引
	WORD								wGender;						//用户性别
	BYTE								cbMember;						//会员等级
	IosDword							dwUserID;						//用户 I D
	IosDword							dwGameID;						//游戏 I D
	IosDword							dwExperience;					//用户经验
	IosDword							dwLoveliness;					//魅力值
	
	
	//扩展信息
	IosDword							dwCustomFaceVer;				//头像版本
	BYTE								cbMoorMachine;					//锁定机器
	WORD								wFriedFieldID;					//炒场ID

};

//登陆失败
struct CMD_GP_LogonError
{
	int								lErrorCode;						//错误代码
    BYTE                            cbValidateType;                 //验证方式（0：身份验证，1：短信验证，2：动态口令）
	CHAR							szErrorDescribe[128];			//错误消息
};



//临时使用于苹果审核充值请求
struct CMD_MB_ApplePayRequest
{
    UINT32        userid;
    UINT32        score;      //游戏币
    UINT32        pass;
};

//临时使用于苹果审核充值返回
struct CMD_MB_ApplePayResponse
{
    UINT32        result;      //0正确，其他错误
    UINT32        score;      //游戏币
};

//////////////////////////////////////////////////////////////////////////
//游戏列表命令码

#define MDM_GP_SERVER_LIST				2								//列表信息

//#define SUB_GP_LIST_TYPE				100								//类型列表
#define SUB_GP_LIST_KIND				101								//种类列表
#define SUB_GP_LIST_STATION				102								//站点列表
#define SUB_GP_LIST_SERVER				103								//房间列表
#define SUB_GP_LIST_SERVER_TYPE			100								//房间类型列表
#define SUB_GP_LIST_FINISH				200								//发送完成
#define SUB_GP_LIST_CONFIG				106								//列表配置
//PTR_WORK	2012/04/24........begin
#define SUB_GP_LIST_FRIEND				107								//好友列表
//PTR_WORK	2012/04/24........end
#define SUB_GP_SEND_MAIL_RES			108								//发送留言回应
#define SUB_GP_SEND_FRIEND_RES			109								//添加好有回应
#define SUB_GP_LIST_TABLE				110								//桌子列表


//列表配置
struct CMD_GP_ListConfig
{
	BYTE								bShowOnLineCount;				//显示人数
};

//////////////////////////////////////////////////////////////////////////
//系统命令码

#define MDM_GP_SYSTEM					3								//系统命令

#define SUB_GP_VERSION					100								//版本通知
#define SUB_SP_SYSTEM_MSG				101								//系统消息

//版本通知
struct CMD_GP_Version
{
	BYTE								bNewVersion;					//更新版本
	BYTE								bAllowConnect;					//允许连接
};

//////////////////////////////////////////////////////////////////////////

#define MDM_GP_USER						4								//用户信息

#define SUB_GP_USER_UPLOAD_FACE			100								//上传头像
#define SUB_GP_USER_DOWNLOAD_FACE		101								//下载头像
#define SUB_GP_UPLOAD_FACE_RESULT		102								//上传结果
#define SUB_GP_DELETE_FACE_RESULT		103								//删除结果
#define SUB_GP_CUSTOM_FACE_DELETE		104								//删除头像
#define SUB_GP_MODIFY_INDIVIDUAL		105								//个人资料
#define SUB_GP_MODIFY_INDIVIDUAL_RESULT	106								//修改结果
#define SUB_GP_SAFE_BIND				107								//玩家绑定
#define SUB_GP_SAFE_UNBIND				108								//解除绑定
#define SUB_GP_SEND_FRIENDMESSAGE_RES	109								//发送留言
#define SUB_GP_CLEAR_TOKEN				110								//清除TOKEN

#define SUB_GP_FORWARDSERVER            120                             //获取转发服务器地址
#define SUB_GP_FORWARDSERVER_RESULT     121                             //返回转发服务器地址
#define SUB_GP_HEARBEAT                 202                             //心跳
#define SUB_GP_HEARBEAT_RESULT          203                             //心跳返回

#define SUB_GM_BROADCAST_REQ      204                                   //请求GM广播消息
#define SUB_GM_BROADCAST_RES      205                                   //响应GM广播消息
#define SUB_GM_BROADCAST_END      206                                   //广播消息结束

#define SUB_RANKING_QUERY        207                //排行榜查询请求
#define SUB_RANKING_RESPONSE      208                //排行榜应答

#define SUB_FISH_FREECOIN_REQ           209                             //捕鱼活动查询
#define SUB_FISH_FREECOIN_RES           210                             //捕鱼活动查询

struct CMD_Fish_Freecoin
{
    UINT32                       dwUserID;          //玩家ID
};

struct CMD_Fish_Freecoin_Result
{
    BYTE    freeAccount;                  //1：帐号有资格领取
    BYTE    freeWeixin;                   //1：微信有资格领取
};

struct CMD_GP_Broadcast
{
    UINT32              LastTime;                   //时间
};

struct CMD_GP_Broadcast_Result
{
    CHAR              Content[1024 + 1];      //消息内容
    CHAR              Scope[128 + 1];        //游戏类型多个以逗号隔开,为空表示全服
};

struct CMD_GP_Broadcast_End
{
    UINT32              LastTime;                   //时间
};

struct CMD_GP_ForwardServerResult
{
    CHAR   Address[64];   //转发服务器地址
};

struct tagRankingInfo
{
    LONGLONG             llRank;                            //排名
    UINT32               dwUserID;
    UINT32               dwGameID;
    BYTE                 byGender;                          //性别
    LONGLONG             llScore;                           //金币总数
    CHAR                 szNickName[NAME_LEN];              //用户名字
    CHAR                 szUnderWrite[UNDER_WRITE_LEN];    //个性签名
};

struct CMD_RankingQuery_Result
{
    tagRankingInfo    info[MAX_RANKING_INFO];
};

struct CMD_GP_Hearbeat
{
    UINT32                           dwUserID;          //玩家ID
    UINT32                           dwHearbeat;          //心跳次数
    LONGLONG                        llToken;          //心跳验证
};

struct CMD_GP_Hearbeat_Result
{
    UINT32                           dwResult;          //结果(0：成功。1：用户在其它地方登录。3：帐号安全关闭)
};

//清除TOKEN
struct CMD_GP_ClearToken
{
	IosDword								dwUserID;						//玩家ID
};

//绑定帐号
struct CMD_GP_SafeBind
{
	IosDword								dwUserID;						//玩家ID
	tagClientSerial						ClientSerial;					//机器序列
	CHAR								szPassWord[PASS_LEN];			//登录密码
};

//解除绑定
struct CMD_GP_SafUnBind
{
	IosDword								dwUserID;						//玩家ID
	tagClientSerial						ClientSerial;					//机器序列
	CHAR								szPassWord[PASS_LEN];			//登录密码
};

//解除绑定
struct CMD_GP_BindResult
{
	BYTE								cbSuccess;						//玩家ID
	//CHAR								szErrorDescribe[128];			//登录密码
};

//个人资料
struct CMD_GP_ModifyIndividual
{
	IosDword							dwUserID;							//玩家 ID
	CHAR							szNickname[NAME_LEN];				//用户昵称
	int								nGender;							//玩家性别
	int								nAge;								//玩家年龄
	CHAR							szAddress[64];						//玩家地址
	CHAR							szUnderWrite[32];					//个性签名
	CHAR							szPassword[33];						//玩家密码
};

//定义头像
struct CMD_GP_UploadCustomFace
{
	IosDword								dwUserID;						//玩家 ID
	WORD								wCurrentSize;					//当前大小
	bool								bLastPacket;					//最后标识
	bool								bFirstPacket;					//第一个标识
	BYTE								bFaceData[2048];				//头像数据
};

//下载成功
struct CMD_GP_DownloadFaceSuccess
{
	IosDword							dwToltalSize;						//总共大小
	IosDword							dwCurrentSize;						//当前大小
	IosDword							dwUserID;							//玩家 ID
	BYTE							bFaceData[2048];					//头像数据
};

//下载头像
struct CMD_GP_DownloadFace
{
	IosDword							dwUserID;							//玩家 ID
};

//上传结果
struct CMD_GP_UploadFaceResult
{
	CHAR							szDescribeMsg[128];					//描述信息
	IosDword							dwFaceVer;							//头像版本
	bool							bOperateSuccess;					//成功标识
};

//删除结果
struct CMD_GP_DeleteFaceResult
{
	CHAR							szDescribeMsg[128];					//描述信息
	IosDword							dwFaceVer;							//头像版本
	bool							bOperateSuccess;					//成功标识
};

//删除消息
struct CMD_GP_CustomFaceDelete
{
	IosDword							dwUserID;							//玩家 ID
	IosDword							dwFaceVer;							//头像版本
};

//修改结果
struct CMD_GP_ModifyIndividualResult
{
	CHAR							szDescribeMsg[128];					//描述信息
	bool							bOperateSuccess;					//成功标识
};


struct CMD_GP_SendMailRes
{
	int							lErrorCode;								//错误代码
	CHAR							szErrorDescribe[128];					//错误消息
};
//

struct CMD_GP_AddFriendRes
{
	IosDword							dwUserIDSelf;						//发起者 I D
	IosDword							dwUserIDFriend;						//用户 I D
	WORD							wCompanion;							//关系 0为没有关系 1为好友 2为黑名单(未使用)
	CHAR							szErrorDescribe[128];
	int							lErrorCode;							//错误代码
	bool							bOperateSuccess;					//成功标识
	/*
		返回0表示 添加成功                                        
		返回1表示 你不能加同级会员                        
		返回2表示 对不起，你只能加一个比自己大的会员  
		返回3表示 你不是会员，不能添加好友
		*/
};



struct DBR_GP_LoadMessageRes 
{
	CHAR							szNickName[5][NAME_LEN];			//用户昵称
	CHAR							szTime[5][32];						//留言时间
	CHAR							szMessage[5][128];					//留言信息
	WORD							wCount;								//信息数量
};
struct DBR_GP_OperateBankResult 
{
	bool                           bResult;                             //操作结果
	LONGLONG                       lSilverScore;                        //
	LONGLONG                       lInsureSilverScore;                  //
	CHAR                          szMessage[128];
};

//帐号注册
struct CMD_S_GP_RegisterSuccess
{
	IosDword							dwUserID;
	CHAR							szPassWord[PASS_LEN];				//登录密码
	BYTE							cbOpenPlat;							//开放平台
	CHAR							szUserToken[TOKEN_LEN];					//登录Token
};

//帐号注册
struct CMD_S_GP_RegisterOpenIDSuccess
{
	IosDword							dwUserID;
	char							szOpenID[OPEN_LEN];						//
	BYTE							cbOpenPlat;							//开放平台
	CHAR							szUserToken[TOKEN_LEN];					//登录Token
	CHAR							szPassword[PASS_LEN];					//登录密码
};

#pragma pack()
//////////////////////////////////////////////////////////////////////////

#endif
