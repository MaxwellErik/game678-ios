//
//  localInfo.cpp
//  game998
//
//  Created by maxwell on 16/9/2.
//
//

#include "localInfo.h"
#include "cocos2d.h"
#include <string>
#include <cstdlib>
#include "CCplatformConfig.h"
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
#include "systemInfo.h"
#else
#include <jni.h>
#include "platform/android/jni/JniHelper.h"
#include <android/log.h>
#include "JniSink.h"
#endif

USING_NS_CC;
using namespace std;

NetWorkType LocalInfo::getCurNetInfo()
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    return SystemInfo::getCurNetInfo();
#else
    JniMethodInfo method;
	bool isHave = JniHelper::getStaticMethodInfo(method,/*JniMethodInfo的引用*/
												 "tools/LocalInfo",/*类的路径*/
												 "GetNetworkType",/*函数名*/
												 "()Ljava/lang/String;");/*函数类型简写*/

	NetWorkType nt_type = NetWorkTypeNone;
	string strNetworkType = "";
	if (isHave)
	{
		jstring str = (jstring)method.env->CallStaticObjectMethod(method.classID, method.methodID);
		strNetworkType = JniHelper::jstring2string(str);

		method.env->DeleteLocalRef(method.classID);
		method.env->DeleteLocalRef(str);
	}

	if(!strNetworkType.empty())
	{
		if(strNetworkType == "WIFI")
		 nt_type = NetWorkTypeWiFI;
		else if(strNetworkType == "2G")
		 nt_type = NetWorkType2G;
		else if(strNetworkType == "3G")
		 nt_type = NetWorkType3G;
		else if(strNetworkType == "4G")
		 nt_type = NetWorkType4G;
	}

	return nt_type;
#endif
}

float LocalInfo::getCurPowerInfo()
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    return SystemInfo::getCurPowerInfo();
#else
    JniMethodInfo method;
     float powerPercent = 0.f;
     bool isHave = JniHelper::getStaticMethodInfo(method,/*JniMethodInfo的引用*/
                                                      "tools/LocalInfo",/*类的路径*/
                                                      "GetPowerPercent",/*函数名*/
                                                      "()F");/*函数类型简写*/

     if (isHave){
      jfloat jpercent = method.env->CallStaticFloatMethod(method.classID, method.methodID);
      powerPercent = (float)jpercent;

      method.env->DeleteLocalRef(method.classID);
     }

     return powerPercent;
#endif
}

std::string LocalInfo::getClientVersion()
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    return SystemInfo::getClientVersion();
#else
	return "";
#endif
}

std::string LocalInfo::getMobileBrand()
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    return SystemInfo::getMobileBrand();
#else
    return JniSink::share()->GetMobileBrand();
#endif
}

std::string LocalInfo::getMobileKind()
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    return SystemInfo::getMobileKind();
#else
     return JniSink::share()->GetMobileKind();
#endif
}

std::string LocalInfo::getMobileSystemVersion()
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    return SystemInfo::getMobileSystemVersion();
#else
     return JniSink::share()->GetMobileSystemVersion();
#endif
}
