﻿#ifndef ENCRYPT_HEAD_FILE
#define ENCRYPT_HEAD_FILE

#include "cocos2d.h"
//////////////////////////////////////////////////////////////////////////

#include "GlobalProperty.h"

//MD5 加密类
class CMD5Encrypt
{
	//函数定义
private:
	//构造函数
	CMD5Encrypt() {}

	//功能函数
public:
	//生成密文
	static void EncryptData(const char * pszSrcData, char szMD5Result[33]);
};

//////////////////////////////////////////////////////////////////////////

//异或加密类
class CXOREncrypt
{
	//函数定义
private:
	//构造函数
	CXOREncrypt() {}

	//功能函数
public:
	//生成密文
	static WORD EncryptData(const char * pszSrcData, char * pszEncrypData, WORD wSize);
	//解开密文
	static WORD CrevasseData(const char * pszEncrypData, char * pszSrcData, WORD wSize);
};

//////////////////////////////////////////////////////////////////////////

#endif
