//
//  DownloadGameResource.hpp
//  game998
//
//  Created by maxwell on 2016/12/11.
//
//

#ifndef DownloadGameResource_hpp
#define DownloadGameResource_hpp

#include <stdio.h>

#include "cocos2d.h"
#include "extensions/cocos-ext.h"


class DownLoadGameResource : public cocos2d::Layer, public cocos2d::extension::AssetsManagerDelegateProtocol
{
public:
    DownLoadGameResource();
    virtual ~DownLoadGameResource();
    
    virtual bool init();
    
    void setFileName(std::string fileName);
    
    void upgrade(float dt);	//检查版本更新
    void reset();	 //重置版本
    
    void close(float dt);
    virtual void onError(cocos2d::extension::AssetsManager::ErrorCode errorCode);	 //错误信息
    virtual void onProgress(int percent);	//更新下载进度
    virtual void onSuccess();	 //下载成功
    CREATE_FUNC(DownLoadGameResource);
private:
    cocos2d::extension::AssetsManager* getAssetManager();
    void initDownloadDir();	 //创建下载目录
    
private:
    std::string _pathToSave;
    cocos2d::Label *_showDownloadInfo;
    std::string _fileName;
    cocos2d::extension::AssetsManager *_assetManager;
};

#endif /* DownloadGameResource_hpp */
