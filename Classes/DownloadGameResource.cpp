//
//  DownloadGameResource.cpp
//  game998
//
//  Created by maxwell on 2016/12/11.
//
//

#include "DownloadGameResource.hpp"
#include "CCLuaEngine.h"

#include <dirent.h>
#include <sys/stat.h>
#include "GameLayerMove.h"

USING_NS_CC;
USING_NS_CC_EXT;

DownLoadGameResource::DownLoadGameResource():
_pathToSave(""),
_assetManager(nullptr),
_showDownloadInfo(NULL)
{
    
}

DownLoadGameResource::~DownLoadGameResource()
{
    AssetsManager* assetManager = getAssetManager();
    CC_SAFE_DELETE(assetManager);
}

bool DownLoadGameResource::init()
{
    if (!Layer::init())
    {
        return false;
    }
    Size winSize = Director::getInstance()->getWinSize();
    _showDownloadInfo = Label::createWithSystemFont("检查更新资源！", "Arial", 46);
    _showDownloadInfo->setColor(_GAME_FONT_COLOR_6_);
    this->addChild(_showDownloadInfo, 999);
    _showDownloadInfo->setPosition(Vec2(winSize.width / 2, winSize.height / 2 - 20));
    
    auto sp = ui::Scale9Sprite::create("Common/bg_gray.png");
    sp->setContentSize(winSize);
    auto pMenuItem = MenuItemSprite::create(sp, sp, sp);
    Menu* pTouchDisableMenu = Menu::create(pMenuItem, nullptr);
    pTouchDisableMenu->setPosition(Vec2(winSize.width / 2, winSize.height / 2));
    addChild(pTouchDisableMenu);
    
    initDownloadDir();
    return true;
}

void DownLoadGameResource::setFileName(std::string fileName)
{
    _fileName = fileName;
    if (!_fileName.empty())
    {        
        scheduleOnce(schedule_selector(DownLoadGameResource::upgrade), 0.2f);
    }
}

void DownLoadGameResource::close(float dt)
{
    this->removeFromParent();
}

void DownLoadGameResource::onError(AssetsManager::ErrorCode errorCode)
{
    if (errorCode == AssetsManager::ErrorCode::NO_NEW_VERSION)
    {
        GameLayerMove::sharedGameLayerMoveSink()->EnterRoomList();
        this->removeFromParent();
    }
    else if (errorCode == AssetsManager::ErrorCode::NETWORK)
    {
        _showDownloadInfo->setString("获取服务器版本失败，请检测网络");
        scheduleOnce(schedule_selector(DownLoadGameResource::close), 2.0f);
    }
    else if (errorCode == AssetsManager::ErrorCode::CREATE_FILE)
    {
        _showDownloadInfo->setString("创建文件错误， 退出客服端重试！");
        scheduleOnce(schedule_selector(DownLoadGameResource::close), 2.0f);
        reset();
    }
    else if (errorCode == AssetsManager::ErrorCode::UNCOMPRESS)
    {
        _showDownloadInfo->setString("更新完成！");
        GameLayerMove::sharedGameLayerMoveSink()->EnterRoomList();
        scheduleOnce(schedule_selector(DownLoadGameResource::close), 0.5f);
    }
}

void DownLoadGameResource::onProgress(int percent)
{
    if (percent < 0)
        return;
    char progress[40];
    snprintf(progress, 40, "正在加载资源 %d%%", percent);
    _showDownloadInfo->setString(progress);
    
}

void DownLoadGameResource::onSuccess()
{
    log("download success");
    _showDownloadInfo->setString("资源加载完成！");
    std::string path = FileUtils::getInstance()->getWritablePath() + _fileName;
    
    GameLayerMove::sharedGameLayerMoveSink()->EnterRoomList();
    this->removeFromParent();
}

AssetsManager* DownLoadGameResource::getAssetManager()
{
    if (!_assetManager)
    {
        char url[100];
        char version[100];

        sprintf(url, "https://download.game678.com/m/%s.zip", _fileName.c_str());
        sprintf(version, "https://update.game678.com/m/version_%s.txt", _fileName.c_str());
        
        _assetManager = new AssetsManager(url, version, _pathToSave.c_str());
        _assetManager->setDelegate(this);
        _assetManager->setConnectionTimeout(8);
    }
    return _assetManager;
}

void DownLoadGameResource::initDownloadDir()
{
    log("initDownloadDir");
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    _pathToSave = FileUtils::getInstance()->getWritablePath() + "Resources/";
#else
    _pathToSave = FileUtils::getInstance()->getWritablePath() + "assets/";
#endif
    _pathToSave += _fileName;
    log("Path: %s", _pathToSave.c_str());

    DIR *pDir = NULL;
    pDir = opendir(_pathToSave.c_str());
    if (!pDir)
    {
        mkdir(_pathToSave.c_str(), S_IRWXU | S_IRWXG | S_IRWXO);
    }

    log("initDownloadDir end");
}

void DownLoadGameResource::reset()
{
    // Remove downloaded files

    std::string command = "rm -r ";
    
    command += "\"" + _pathToSave + "\"";
    system(command.c_str());

    getAssetManager()->deleteVersion();
    initDownloadDir();
}

void DownLoadGameResource::upgrade(float dt)
{
    getAssetManager()->update();
}
