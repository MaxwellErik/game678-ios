#include "GameTable.h"
#include "LobbySocketSink.h"
#include "ClientSocketSink.h"
#include "GameLayerMove.h"
#include "Convert.h"

#define LOCK_STATUS_TGA    1
#define PLAY_STATUS_TGA    2
#define HEADIMG_TGA        200
#define NAME_TGA           400
#define HAND_TGA           600
#define CHAIR_BEGAIN_TGA   1000

GameTable::GameTable(GameScene *pGameScene):GameLayer(pGameScene)
{
	m_ScrollTabel = NULL;
	m_Title = NULL;
	m_QuickEnter = NULL;
    m_table = INVALID_TABLE;
    m_chair = INVALID_CHAIR;
    
    m_MoveText          = nullptr;
    m_runbg             = nullptr;
    m_cNode             = NULL;
    m_isBroadcasting    = false;
}

GameTable::~GameTable()
{
    log("~GameTable");
}

void GameTable::OnCallMove()
{
    setVisible(false);
}

void GameTable::OnEventUserStatus(tagUserData * pUserData)
{
    switch (pUserData->cbUserStatus) {
        case US_FREE:
        {
            DeleteUser(pUserData);
            break;
        }
        case US_SIT:
        {
            AddUser(pUserData);
            break;
        }
        case US_READY:
        {
            //举手
            Node* table = m_ScrollTabel->getChildByTag(pUserData->wTableID);
            if (nullptr != table)
            {
                Vec2 pos = table->getPosition();
                Sprite* HandUp = Sprite::createWithSpriteFrameName("handup.png");
                HandUp->setPosition(m_HandPosMap[pUserData->wChairID] + pos);
                m_ScrollTabel->addChild(HandUp, 10, HAND_TGA + pUserData->wTableID * g_GlobalUnits.m_wChairCount + pUserData->wChairID);
            }
            break;
        }
        default:
            break;
    }

}

void GameTable::AddUser(tagUserData* pUserData)
{
    if(pUserData == NULL) return;
    
    UserArr user;
    memset(&user, 0, sizeof(user));
    user.chairID = pUserData->wChairID;
    user.tableID = pUserData->wTableID;
    m_UserMap[pUserData->dwUserID] = user;
    
    int tga = CHAIR_BEGAIN_TGA + pUserData->wTableID * g_GlobalUnits.m_wChairCount + pUserData->wChairID;
    auto chair = m_ScrollTabel->getChildByTag(tga);
    if (chair)
    {
        int headTag = HEADIMG_TGA + tga - CHAIR_BEGAIN_TGA;
        auto head = m_ScrollTabel->getChildByTag(headTag);
        if (head)
            head->removeFromParent();
        
        string heads = g_GlobalUnits.getFace(pUserData->wGender, pUserData->lScore);
        Sprite* headImage = Sprite::createWithSpriteFrameName(heads);
        headImage->setPosition(chair->getPosition());
        m_ScrollTabel->addChild(headImage, 10, headTag);
        
        //昵称
        int nameTag = NAME_TGA + tga - CHAIR_BEGAIN_TGA;
        auto name = m_ScrollTabel->getChildByTag(nameTag);
        if (name)
            name->removeFromParent();
 
        Label* font = Label::createWithSystemFont(gbk_utf8(pUserData->szNickName),_GAME_FONT_NAME_1_,28);
        font->setPosition(m_NamePosMap[pUserData->wChairID] + chair->getPosition());
        
        if (pUserData->cbMemberOrder > 0 || pUserData->cbBanker)
            font->setColor(Color3B::RED);
        else
            font->setColor(_GAME_FONT_COLOR_2_);
        m_ScrollTabel->addChild(font, 10, nameTag);
        
        if (US_READY == pUserData->cbUserStatus)
        {
            Node* table = m_ScrollTabel->getChildByTag(pUserData->wTableID);
            if (nullptr == table)
                return;

            Vec2 pos = table->getPosition();
            Sprite* HandUp = Sprite::createWithSpriteFrameName("handup.png");
            HandUp->setPosition(m_HandPosMap[pUserData->wChairID] + pos);
            m_ScrollTabel->addChild(HandUp, 10, HAND_TGA + pUserData->wTableID * g_GlobalUnits.m_wChairCount + pUserData->wChairID);
        }
    }
}

void GameTable::DeleteUser(tagUserData* pUserData)
{
	if(pUserData == NULL) return;
    int nameTag = NAME_TGA +  m_UserMap[pUserData->dwUserID].tableID * g_GlobalUnits.m_wChairCount + m_UserMap[pUserData->dwUserID].chairID;
    auto name = m_ScrollTabel->getChildByTag(nameTag);
    if (name)
        name->removeFromParent();
    
    int headTag = HEADIMG_TGA +  m_UserMap[pUserData->dwUserID].tableID * g_GlobalUnits.m_wChairCount + m_UserMap[pUserData->dwUserID].chairID;
    auto head = m_ScrollTabel->getChildByTag(headTag);
    if (head)
        head->removeFromParent();
 
    int handTag = HAND_TGA +  m_UserMap[pUserData->dwUserID].tableID * g_GlobalUnits.m_wChairCount + m_UserMap[pUserData->dwUserID].chairID;
    auto hand = m_ScrollTabel->getChildByTag(handTag);
    if (hand)
        hand->removeFromParent();
    
    m_UserMap.erase(pUserData->dwUserID);
    
}

void GameTable::callbackBt(Ref *pSender)
{
	Node *pNode = (Node *)pSender;
	int tga = pNode->getTag();
	if (tga == QuitRoom) //退出房间
	{
		SoundUtil::sharedEngine()->playEffect("buttonMusic");
		ClientSocketSink::sharedSocketSink()->closeSocket();
		GameLayerMove::sharedGameLayerMoveSink()->GoTableToKind();
		ClientSocketSink::sharedSocketSink()->CleaAllUser();
		return;
	}
	else if (tga == EnterRoom) //快速加入
	{
		GameLayerMove::sharedGameLayerMoveSink()->CloseUserInfo();
		SoundUtil::sharedEngine()->playEffect("buttonMusic");
		LoadLayer::create(m_pGameScene);
		GameLayerMove::sharedGameLayerMoveSink()->AddEnterGameTime();
		ClientSocketSink::sharedSocketSink()->SitdownReq();
		return;
	}

	int table = INVALID_TABLE;
	int chair = INVALID_CHAIR;
	int chair_count = g_GlobalUnits.m_wChairCount;

	if (chair_count == 2)
	{
		table = (tga - CHAIR_BEGAIN_TGA)/2;
		chair = (tga - CHAIR_BEGAIN_TGA)%2;
	}
    else if (chair_count == 3)
    {
        table = (tga - CHAIR_BEGAIN_TGA)/3;
        chair = (tga - CHAIR_BEGAIN_TGA)%3;
    }
    else if (chair_count == 4)
	{
		table = (tga - CHAIR_BEGAIN_TGA)/4;
		chair = (tga - CHAIR_BEGAIN_TGA)%4;
	}
	else if (chair_count == 6)
	{
		table = (tga - CHAIR_BEGAIN_TGA)/6;
		chair = (tga - CHAIR_BEGAIN_TGA)%6;
	}
    
    //先判断桌子是否有锁
    Sprite* tableSpr = dynamic_cast<Sprite*>(m_ScrollTabel->getChildByTag(table));
    if (nullptr == tableSpr)
        return;
    
    auto child = tableSpr->getChildByTag(LOCK_STATUS_TGA);

    if (nullptr != child) //当前桌子有锁
    {
        auto lockPass = LockPassWord::create(m_pGameScene);
        lockPass->SetTableArr(table,chair);
        return;
    }
    
    SoundUtil::sharedEngine()->playEffect((char*)"buttonMusic");
	LoadLayer::create(m_pGameScene);
	GameLayerMove::sharedGameLayerMoveSink()->AddEnterGameTime();
	GameLayerMove::sharedGameLayerMoveSink()->CloseUserInfo();
    ClientSocketSink::sharedSocketSink()->SitdownReq(table, chair);
}

void GameTable::InitTableStatus(CMD_GR_TableInfo* TableStatus)
{
    m_TableStatusMap.clear();
    for (int i = 0; i < TableStatus->wTableCount; i++)
    {
        m_TableStatusMap[i] = TableStatus->TableStatus[i];
    }
}

void GameTable::UpdateTableStatus(CMD_GR_TableStatus* TableStatus)
{
    if (nullptr == m_ScrollTabel)
        return;
    
    Sprite* table = dynamic_cast<Sprite*>(m_ScrollTabel->getChildByTag(TableStatus->wTableID));
    if (nullptr != table)
    {
        Sprite* lock = dynamic_cast<Sprite*>(table->getChildByTag(LOCK_STATUS_TGA));
        if (lock)
            lock->removeFromParent();

        Sprite* gameing = dynamic_cast<Sprite*>(table->getChildByTag(PLAY_STATUS_TGA));
        if (gameing)
            gameing->removeFromParent();
 
        if (1 == TableStatus->bTableLock)
        {
            lock = Sprite::createWithSpriteFrameName("lock.png");
            lock->setPosition(Vec2(120,60));
            table->addChild(lock, 1, LOCK_STATUS_TGA);
        }
        
        if(1 == TableStatus->bPlayStatus)
        {
            for (int i = 0; i < g_GlobalUnits.m_wChairCount; i++)
            {
                int tag = HAND_TGA + TableStatus->wTableID * g_GlobalUnits.m_wChairCount + i;
                auto hand = m_ScrollTabel->getChildByTag(tag);
                if (hand)
                    hand->removeFromParent();
            }
            
            gameing = Sprite::createWithSpriteFrameName("gameing.png");
            gameing->setPosition(Vec2(120,112));
            table->addChild(gameing, 1, PLAY_STATUS_TGA);
        }
    }
}

void GameTable::addTable2(Vec2 tablePos)
{
    Menu *chair0 = CreateButton("chairup" ,Vec2(tablePos.x,tablePos.y + 220), m_BtnTga);
    m_ScrollTabel->addChild(chair0, 0, m_BtnTga++);
    
    Menu *chair1 = CreateButton("chairdown" ,Vec2(tablePos.x,tablePos.y - 220), m_BtnTga);
    m_ScrollTabel->addChild(chair1, 0, m_BtnTga++);
}

void GameTable::addTable3(Vec2 tablePos)
{
    Menu *chair0 = CreateButton("chairleftdown" ,Vec2(tablePos.x - 190,tablePos.y - 110), m_BtnTga);
    m_ScrollTabel->addChild(chair0, 0, m_BtnTga++);

    Menu *chair1 = CreateButton("chairrightdown" ,Vec2(tablePos.x + 190,tablePos.y - 110), m_BtnTga);
    m_ScrollTabel->addChild(chair1, 0, m_BtnTga++);
    
    Menu *chair2 = CreateButton("chairup" ,Vec2(tablePos.x, tablePos.y+220), m_BtnTga);
    m_ScrollTabel->addChild(chair2, 0, m_BtnTga++);
}

void GameTable::addTable4(Vec2 tablePos)
{
    Menu *chair0 = CreateButton("chairleft" ,Vec2(tablePos.x-220,tablePos.y), m_BtnTga);
    m_ScrollTabel->addChild(chair0, 0, m_BtnTga++);
    //1
    Menu *chair1 = CreateButton("chairdown" ,Vec2(tablePos.x,tablePos.y-220), m_BtnTga);
    m_ScrollTabel->addChild(chair1, 0, m_BtnTga++);
    //2
    Menu *chair2 = CreateButton("chairright" ,Vec2(tablePos.x+220,tablePos.y ), m_BtnTga);
    m_ScrollTabel->addChild(chair2, 0, m_BtnTga++);
    //3
    Menu *chair3 = CreateButton("chairup" ,Vec2(tablePos.x,tablePos.y+220), m_BtnTga);
    m_ScrollTabel->addChild(chair3, 0, m_BtnTga++);

}

void GameTable::addTable6(Vec2 tablePos)
{
    Menu *chair0 = CreateButton("chairup" ,Vec2(tablePos.x, tablePos.y+220), m_BtnTga);
    m_ScrollTabel->addChild(chair0, 0, m_BtnTga++);
    //1
    Menu *chair1 = CreateButton("chairleftup" ,Vec2(tablePos.x-190, tablePos.y+110), m_BtnTga);
    m_ScrollTabel->addChild(chair1, 0, m_BtnTga++);
    //2
    Menu *chair2 = CreateButton("chairleftdown" ,Vec2(tablePos.x-190, tablePos.y-110), m_BtnTga);
    m_ScrollTabel->addChild(chair2,0, m_BtnTga++);
    //3
    Menu *chair3 = CreateButton("chairdown" ,Vec2(tablePos.x, tablePos.y-220), m_BtnTga);
    m_ScrollTabel->addChild(chair3, 0, m_BtnTga++);
    //4
    Menu *chair4 = CreateButton("chairrightdown" ,Vec2(tablePos.x+190, tablePos.y-110), m_BtnTga);
    m_ScrollTabel->addChild(chair4, 0, m_BtnTga++);
    //5
    Menu *chair5 = CreateButton("chairrightup" ,Vec2(tablePos.x+190, tablePos.y+110), m_BtnTga);
    m_ScrollTabel->addChild(chair5, 0, m_BtnTga++);
}

void GameTable::initTable()
{
    if (nullptr != m_ScrollTabel)
        m_ScrollTabel->removeAllChildren();
    m_UserMap.clear();
    m_NamePosMap.clear();
    m_HandPosMap.clear();
    m_BroadcastList.clear();
    
    int chair_count = g_GlobalUnits.m_wChairCount;
    int table_count = g_GlobalUnits.m_wTableCount;
    m_BtnTga = CHAIR_BEGAIN_TGA;
    
    if (chair_count > 6)
        return;

    if (6 == chair_count)
    {
        m_NamePosMap[0] = Vec2(0, 100);
        m_NamePosMap[1] = Vec2(0, 100);
        m_NamePosMap[2] = Vec2(0, -100);
        m_NamePosMap[3] = Vec2(0, -100);
        m_NamePosMap[4] = Vec2(0, -100);
        m_NamePosMap[5] = Vec2(0, 100);
        
        m_HandPosMap[0] = Vec2(0, 80);
        m_HandPosMap[1] = Vec2(- 60, 40);
        m_HandPosMap[2] = Vec2(-60, - 40);
        m_HandPosMap[3] = Vec2(0, -80);
        m_HandPosMap[4] = Vec2(60, -40);
        m_HandPosMap[5] = Vec2(60, 40);
        
    }
    else if (4 == chair_count)
    {
        m_NamePosMap[0] = Vec2(0, -100);
        m_NamePosMap[1] = Vec2(0, -100);
        m_NamePosMap[2] = Vec2(0, 100);
        m_NamePosMap[3] = Vec2(0, 100);
        
        m_HandPosMap[0] = Vec2(-80, 0);
        m_HandPosMap[1] = Vec2(0, -80);
        m_HandPosMap[2] = Vec2(80, 0);
        m_HandPosMap[3] = Vec2(0, 80);
    }
    else if (3 == chair_count)
    {
        m_NamePosMap[0] = Vec2(0, -100);
        m_NamePosMap[1] = Vec2(0, -100);
        m_NamePosMap[2] = Vec2(0, 100);
        
        m_HandPosMap[0] = Vec2(-60, -40);
        m_HandPosMap[1] = Vec2(60, -40);
        m_HandPosMap[2] = Vec2(0, 80);
    }
    else
    {
        m_NamePosMap[0] = Vec2(0, 100);
        m_NamePosMap[1] = Vec2(0, -100);
        
        m_HandPosMap[0] = Vec2(0, 80);
        m_HandPosMap[1] = Vec2(0, -80);
    }
    
    //初始化滚动列表
    Size viewSize = Director::getInstance()->getWinSize();
    viewSize.height = 860;
    
    Size innerSize = Director::getInstance()->getWinSize();
    int totalRow = 0;
    if (chair_count == 6)
        totalRow = (table_count % 2) + (table_count / 2);
    
    else
    {
        int rol = 0 == (table_count % 3) ? 0 : 1;
        totalRow = (table_count / 3) + rol;
    }
    innerSize.height = totalRow * 860;
    
    m_ScrollTabel->setContentSize(viewSize);
    m_ScrollTabel->setInnerContainerSize(innerSize);
    
    int row = 0, col = 0, widthSpace = 0, heighSpace = 860;
    Vec2 startPos = Vec2::ZERO;
    
    //放置桌子
    for (int i = 0; i < table_count; i++)
    {
        if (6 == chair_count)
        {
            row = i / 2;
            col = i % 2;
            widthSpace = 800;
            startPos = Vec2(560, 500);
        }
        else
        {
            row = i / 3;
            col = i % 3;
            widthSpace = 560;
            startPos = Vec2(400, 500);
        }
        
        Sprite* tableSpr;
        if (6 == chair_count)
            tableSpr = Sprite::createWithSpriteFrameName("ox6_table.png");
        else if (3 == chair_count)
            tableSpr = Sprite::createWithSpriteFrameName("ox2_table.png");
        else
            tableSpr = Sprite::createWithSpriteFrameName("ox2_table.png");
        
        Vec2 tablePos = Vec2(startPos.x + col * widthSpace, startPos.y + (totalRow - row - 1) * heighSpace);
        tableSpr->setPosition(tablePos);
        tableSpr->setTag(i);
        m_ScrollTabel->addChild(tableSpr);
        
        //初始化桌子锁定状态
        if (1 == m_TableStatusMap[i].bTableLock)
        {
            Sprite* lock = Sprite::createWithSpriteFrameName("lock.png");
            lock->setPosition(Vec2(120,60));
            tableSpr->addChild(lock, 1, LOCK_STATUS_TGA);
        }
        //初始化桌子游戏状态
        if(1 == m_TableStatusMap[i].bPlayStatus)
        {
            Sprite* gameing = Sprite::createWithSpriteFrameName("gameing.png");
            gameing->setPosition(Vec2(120,112));
            tableSpr->addChild(gameing, 1, PLAY_STATUS_TGA);
        }
        
        auto NoStr = StringUtils::toString(i + 1);
        Label* tableNum = Label::createWithSystemFont(NoStr ,_GAME_FONT_NAME_1_, 46);
        tableNum->setColor(_GAME_FONT_COLOR_2_);
        tableNum->setPosition(Vec2(tablePos.x, tablePos.y - 380));
        m_ScrollTabel->addChild(tableNum);
        
        if (6 == chair_count)
        {
            addTable6(tablePos);
        }
        else if (4 == chair_count)
        {
            addTable4(tablePos);
        }
        else if (3 == chair_count)
        {
            addTable3(tablePos);
        }
        else
        {
            addTable2(tablePos);
        }
    }
    
    //加载玩家状态
    for (int i = 0; i < ClientSocketSink::sharedSocketSink()->m_UserManager.GetOnLineCount(); i++)
    {
        tagUserData* pUserData =  ClientSocketSink::sharedSocketSink()->m_UserManager.EnumUserItem(i);
        int chair = pUserData->wChairID;
        int table = pUserData->wTableID;
        if (table == INVALID_TABLE || chair == INVALID_CHAIR) continue;
        AddUser(pUserData);
    }
}

bool GameTable::init()
{
	if (!Layer::init())
	{
		return false;
	}
	setPosition(Vec2(_STANDARD_SCREEN_SIZE_.width,0));
	Sprite *pView = Sprite::create("DaTing/RoomBack.jpg");
	pView->setAnchorPoint(Vec2(0,0));
	pView->setPosition(Vec2(0,0));
	addChild(pView);

	setTouchEnabled(true);

	m_GameName_Tga = 100;
    m_gameRoom_Tga = 101;

	Menu *btn_back = CreateButton("GameRoomBack" ,Vec2(1800,990), QuitRoom);
	addChild(btn_back , 0 , QuitRoom);

	m_QuickEnter = CreateButton("quickEnter" ,Vec2(1570,990), EnterRoom);
	addChild(m_QuickEnter , 0 , EnterRoom);

    m_ScrollTabel = ui::ScrollView::create();
    m_ScrollTabel->setDirection(cocos2d::ui::ScrollView::Direction::VERTICAL);
    m_ScrollTabel->setInertiaScrollEnabled(true);
    addChild(m_ScrollTabel);
    
    //滚动字幕
    m_runbg = Sprite::createWithSpriteFrameName("gonggaodi.png");
    m_runbg->setPosition(Vec2(960, 880));
    addChild(m_runbg);
    m_runbg->setVisible(false);
    
    Sprite* laba = Sprite::createWithSpriteFrameName("laba.png");
    laba->setPosition(Vec2(200, 31));
    m_runbg->addChild(laba);
    
    m_clippingSize = Size(820, 64);
    auto draw = DrawNode::create();
    draw->drawRect(Vec2::ZERO, Vec2(0,m_clippingSize.height),Vec2(m_clippingSize.width,m_clippingSize.height), Vec2(m_clippingSize.width,0), Color4F(255,255,255,255));
    m_cNode = ClippingNode::create();
    m_cNode->setInverted(false);
    m_cNode->setStencil(draw);
    m_cNode->setPosition(Vec2(240,0));
    m_runbg->addChild(m_cNode);
    
    auto listener = EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(true);
    listener->onTouchBegan = CC_CALLBACK_2(GameTable::onTouchBegan, this);
    listener->onTouchMoved = CC_CALLBACK_2(GameTable::onTouchMoved, this);
    listener->onTouchEnded = CC_CALLBACK_2(GameTable::onTouchEnded, this);
    _eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);
    
	return true;
}

bool GameTable::onTouchBegan(cocos2d::Touch *pTouch, cocos2d::Event *pEvent)
{
    if (GameLayerMove::sharedGameLayerMoveSink()->GetLayerIndex() == 1)
    {
        return true;
    }
    return false;
}

void GameTable::onTouchMoved(cocos2d::Touch *pTouch, cocos2d::Event *pEvent)
{
}

GameTable* GameTable::create(GameScene *pGameScene)
{
	GameTable* temp = new GameTable(pGameScene);
	if(temp && temp->init())
	{
		temp->autorelease();
		return temp;
	}
	else
	{
		CC_SAFE_DELETE(temp);
		return NULL;
	}
}

void GameTable::onEnter()
{
	GameLayer::onEnter();
}

void GameTable::onExit()
{
	GameLayer::onExit();
}

Menu* GameTable::CreateButton(std::string szBtName ,const Vec2 &p, int tag )
{
    auto sp1 = Sprite::createWithSpriteFrameName(StrToChar(szBtName+"_normal.png"));
    auto sp2 = Sprite::createWithSpriteFrameName(StrToChar(szBtName+"_normal.png"));
    sp2->setColor(Color3B(150,150,150));
    sp2->setScale(0.96f);
    auto pMenuItem1 = MenuItemSprite::create(sp1, sp2,CC_CALLBACK_1(GameTable::callbackBt, this));
    pMenuItem1->setTag(tag);
// 创建菜单对象，是存放菜单项的容器
    Menu* menu = Menu::create(pMenuItem1, NULL);
    menu->setPosition(p);
    return menu;
}

void GameTable::SetTitle(char* src)
{
	if (m_Title == NULL)
	{
		m_Title = Label::createWithSystemFont(src,_GAME_FONT_NAME_1_,30);
		m_Title->setAnchorPoint(Vec2(0,0));
		m_Title->setPosition(Vec2(20,580));

		m_Title->setColor(_GAME_FONT_COLOR_5_);
		addChild(m_Title);  
	}
	else
	{
		m_Title->setString(src);
	}
}

void GameTable::SetTableName(char* _kind, char* roomname)
{
	removeAllChildByTag(m_GameName_Tga);
    removeAllChildByTag(m_gameRoom_Tga);
    if (g_GlobalUnits.m_wChairCount < 100)
    {
        Label* font = Label::createWithSystemFont(_kind, _GAME_FONT_NAME_1_, 50);
        font->setColor(_GAME_FONT_COLOR_5_);
        font->setPosition(Vec2(100,990));
        font->setTag(m_GameName_Tga);
        font->setAnchorPoint(Vec2(0,0.5f));
        addChild(font);
    }
    
    Sprite* game = Sprite::createWithSpriteFrameName(roomname);
	game->setPosition(Vec2(700,990));
	game->setTag(m_gameRoom_Tga);
	game->setAnchorPoint(Vec2(0,0.5f));
	addChild(game);
}

void GameTable::GoToKind(Ref *pSender)
{
	this->RemoveAlertMessageLayer();
	this->SetAllTouchEnabled(true);
	GameLayerMove::sharedGameLayerMoveSink()->GoTableToKind();
}

void GameTable::update(float dt)
{
    if (m_BroadcastList.size() <= 0)
        return;
    
    int newX = m_MoveText->getPositionX() - 4;
    if (newX <= m_cNode->getPosition().x - m_MoveText->getContentSize().width-20)
    {
        m_runbg->setVisible(false);
    }
    if (newX <= m_cNode->getPosition().x - m_MoveText->getContentSize().width * 3)//如果滚动到这个位置，重置
    {
        m_BroadcastList.erase(m_BroadcastList.begin());
        
        if (m_BroadcastList.size() > 0)
        {
            m_runbg->setVisible(true);
            m_MoveText->setString(m_BroadcastList[0].szText);
            newX = m_clippingSize.width + m_MoveText->getContentSize().width/2;
        }
        else
        {
            unscheduleUpdate();
            m_isBroadcasting = false;
        }
    }
    m_MoveText->setPositionX(newX);
}

void GameTable::AddGameBroadcast(char* msg)
{
    Broadcast broadcast;
    sprintf(broadcast.szText, "%s", msg);
    m_BroadcastList.push_back(broadcast);
    
    if (m_BroadcastList.size() <= 0 || m_isBroadcasting)
        return;
    
    scheduleUpdate();
    m_runbg->setVisible(true);
    m_isBroadcasting = true;
    
    std::string szTempTitle = m_BroadcastList[0].szText;
    m_MoveText = Label::createWithSystemFont(szTempTitle.c_str(),_GAME_FONT_NAME_1_,46);
    m_MoveText->setColor(_GAME_FONT_COLOR_5_);
    m_MoveText->setPosition(Vec2(m_clippingSize.width+m_MoveText->getContentSize().width/2, 32));
    m_cNode->addChild(m_MoveText);
}

void GameTable::CloseTable(char* text)
{
	AlertMessageLayer::createConfirm(this, text,
		menu_selector(GameTable::GoToKind));
}
