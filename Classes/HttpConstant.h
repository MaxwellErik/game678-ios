#ifndef HTTP_CONSTANT_H
#define HTTP_CONSTANT_H

#define URL_REQ_PASSTIME											1			//计算过去时间
#define URL_REQ_STARTTIME											2			//同步开始时间
#define URL_REQ_INITTIME											3			//初始化时间

///////////////////////////////////////////////////////////////////////////////////////////////

//#define CE_SHI

#define SHARE_URL								"http://tb.12wanwan.com/"
#define WEI_URL									"http://wap.1000buyu.com/wei2/index.html?%s"

//获取服务器时间
#ifdef CE_SHI
	#define TIME_URL							"http://tbpay.12wanwan.com/time.jsp"//测试
#else
	#define TIME_URL							"http://tbpay.12wanwan.com/time.jsp"//正式1
	//#define TIME_URL							"http://tbpay.12wanwan.com/time.jsp"//正式2
#endif
//


//跑马灯消息
#ifdef CE_SHI
	#define NOTICE_URL						"http://tbpay.12wanwan.com/msg/mobileClientMsg.do"	//测试
#else
	#define NOTICE_URL						"http://tbpay.12wanwan.com/msg/mobileClientMsg.do"	//正式1
	//#define NOTICE_URL							"http://tbpay.12wanwan.com/msg/mobileClientMsg.do"	//正式2
#endif



//统计短信发送
#ifdef CE_SHI
	#define			SM_LOG_URL		"http://tbpay.12wanwan.com/msg/saveShortMsg.do"
#else
	#define			SM_LOG_URL		"http://tbpay.12wanwan.com/msg/saveShortMsg.do"			//正式1
	//#define			SM_LOG_URL		"http://tbpay.12wanwan.com/msg/saveShortMsg.do"	//正式2
#endif

//#define SERVER_URL			"http://116.236.169.10:8080/appstore/"
#ifdef CE_SHI
	#define			SERVER_URL			"http://mpay.12wanwan.com"
#else
	#define			SERVER_URL			"http://mpay.12wanwan.com"
#endif

#define ORDER_URL					std::string(std::string(SERVER_URL) + std::string("/Bill/Create.aspx?")).c_str()
#define ANDROID_ORDER_URL			std::string(std::string(SERVER_URL) + std::string("/Bill/Create.aspx?")).c_str()

//补单
#define PATCH_ORDER_URL				std::string(std::string(SERVER_URL) + std::string("/Bill/Create.aspx?")).c_str()

#endif
