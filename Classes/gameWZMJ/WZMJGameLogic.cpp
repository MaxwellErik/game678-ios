
#include "WZMJGameLogic.h"
#include <assert.h>
#include <stdlib.h>
#include <string.h>
//////////////////////////////////////////////////////////////////////////
//静态变量

//扑克数据
const BYTE WZMJGameLogic::m_cbCardDataArray[MAX_REPERTORY]=
{
	0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08,0x09,						//万子
	0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08,0x09,						//万子
	0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08,0x09,						//万子
	0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08,0x09,						//万子
	0x11,0x12,0x13,0x14,0x15,0x16,0x17,0x18,0x19,						//索子
	0x11,0x12,0x13,0x14,0x15,0x16,0x17,0x18,0x19,						//索子
	0x11,0x12,0x13,0x14,0x15,0x16,0x17,0x18,0x19,						//索子
	0x11,0x12,0x13,0x14,0x15,0x16,0x17,0x18,0x19,						//索子
	0x21,0x22,0x23,0x24,0x25,0x26,0x27,0x28,0x29,						//同子
	0x21,0x22,0x23,0x24,0x25,0x26,0x27,0x28,0x29,						//同子
	0x21,0x22,0x23,0x24,0x25,0x26,0x27,0x28,0x29,						//同子
	0x21,0x22,0x23,0x24,0x25,0x26,0x27,0x28,0x29,						//同子
	0x31,0x32,0x33,0x34,0x35,0x36,0x37,									//番子
	0x31,0x32,0x33,0x34,0x35,0x36,0x37,									//番子
	0x31,0x32,0x33,0x34,0x35,0x36,0x37,									//番子
	0x31,0x32,0x33,0x34,0x35,0x36,0x37,									//番子
};

//////////////////////////////////////////////////////////////////////////////////////////
/*

//牌型1-八对
{
	0x05,0x05,0x08,0x08,0x11,0x11,0x15,0x15,0x22,0x22,0x23,0x23,0x33,0x33,0x38,0x39,
};

//牌型2-清一色 万
{
    0x01,0x01,0x01,0x02,0x03,0x03,0x04,0x04,0x04,0x05,0x05,0x06,0x06,0x07,0x08,0x09,
};

//牌型3-清一色 筒
{0x21,0x21,0x23,0x23,0x24,0x25,0x25,0x26,0x26,0x27,0x27,0x28,0x29,0x29,0x29,
};

//牌型4-正常牌型
{
 0x01,0x01,0x05,0x05,0x06,0x07,0x13,0x14,0x15,0x16,0x17,0x22,0x22,0x27,0x28,0x29,
};



*/

//////////////////////////////////////////////////////////////////////////

//构造函数
WZMJGameLogic::WZMJGameLogic()
{
	//AfxMessageBox("ok");
}

//析构函数
WZMJGameLogic::~WZMJGameLogic()
{
}

//混乱扑克
void WZMJGameLogic::RandCardData(BYTE cbCardData[], BYTE cbMaxCount)
{
	//混乱准备
	BYTE cbCardDataTemp[CountArray(m_cbCardDataArray)];
	CopyMemory(cbCardDataTemp,m_cbCardDataArray,sizeof(m_cbCardDataArray));


	//混乱扑克
	BYTE cbRandCount=0,cbPosition=0;
	do
	{
		cbPosition=rand()%(cbMaxCount-cbRandCount);
		cbCardData[cbRandCount++]=cbCardDataTemp[cbPosition];
		cbCardDataTemp[cbPosition]=cbCardDataTemp[cbMaxCount-cbRandCount];
	} while (cbRandCount<cbMaxCount);
	
	//替换王牌
	if(m_cbKingCardData != 0xFF)
	{
		for(int i=0; i<MAX_REPERTORY; i++)
		{
			if(cbCardData[i] == m_cbKingCardData)
			{
				cbCardData[i] = CARD_KING_DATA_MASK;
			}
		}
	}

	//如果财神不是白板的话 就要替换财神的原身
	if((m_cbKingCardData != 0x37)&&(m_cbKingCardData!=0XFF))
	{
		for(int i=0; i<MAX_REPERTORY; i++)
		{
			if(cbCardData[i] == 0x37)
			{
				cbCardData[i] = m_cbKingCardData;
			}
		}
	}
	return;
}

//删除扑克
bool WZMJGameLogic::RemoveCard(BYTE cbCardIndex[MAX_INDEX], BYTE cbRemoveCard)
{
	//效验扑克
	assert(IsValidCard(cbRemoveCard));

	//删除扑克
	BYTE cbRemoveIndex=SwitchToCardIndex(cbRemoveCard);
	if (cbCardIndex[cbRemoveIndex]>0)
	{
		cbCardIndex[cbRemoveIndex]--;
		return true;
	}

	return false;
}

//删除扑克
bool WZMJGameLogic::RemoveCard(BYTE cbCardIndex[MAX_INDEX], BYTE cbRemoveCard[], BYTE cbRemoveCount)
{
	//删除扑克
	for (BYTE i=0;i<cbRemoveCount;i++)
	{
		//删除扑克
		BYTE cbRemoveIndex=SwitchToCardIndex(cbRemoveCard[i]);
		if (cbCardIndex[cbRemoveIndex]==0)
		{
			//错误断言
			//assert(FALSE);

			//还原删除
			for (BYTE j=0;j<i;j++) 
			{
				assert(IsValidCard(cbRemoveCard[j]));
				cbCardIndex[SwitchToCardIndex(cbRemoveCard[j])]++;
			}

			return false;
		}
		else 
		{
			//删除扑克
			--cbCardIndex[cbRemoveIndex];
		}
	}

	return true;
}

//删除扑克
bool WZMJGameLogic::RemoveCard(BYTE cbCardData[], BYTE cbCardCount, BYTE cbRemoveCard[], BYTE cbRemoveCount)
{
	//检验数据
	//assert(cbCardCount<=MAX_COUNT);
	assert(cbRemoveCount<=cbCardCount);

	//定义变量
	BYTE cbDeleteCount=0,cbTempCardData[MAX_COUNT_WZMJ];
	if (cbCardCount>CountArray(cbTempCardData))
		return false;
	CopyMemory(cbTempCardData,cbCardData,cbCardCount*sizeof(cbCardData[0]));

	//置零扑克
	for (BYTE i=0;i<cbRemoveCount;i++)
	{
		for (BYTE j=0;j<cbCardCount;j++)
		{
			if (cbRemoveCard[i]==cbTempCardData[j])
			{
				cbDeleteCount++;
				cbTempCardData[j]=0;
				break;
			}
		}
	}

	//成功判断
	if (cbDeleteCount!=cbRemoveCount) 
	{
		assert(FALSE);
		return false;
	}

	//清理扑克
	BYTE cbCardPos=0;
	for (BYTE i=0;i<cbCardCount;i++)
	{
		if (cbTempCardData[i]!=0) 
			cbCardData[cbCardPos++]=cbTempCardData[i];
	}

	return true;
}

//有效判断
bool WZMJGameLogic::IsValidCard(BYTE cbCardData)
{
	if(cbCardData ==CARD_KING_DATA_MASK)
	{
		return true;
	}
	BYTE cbValue=(cbCardData&MASK_VALUE);
	BYTE cbColor=(cbCardData&MASK_COLOR)>>4;
	return (((cbValue>=1)&&(cbValue<=9)&&(cbColor<=2))||((cbValue>=1)&&(cbValue<=7)&&(cbColor==3)));
}

//扑克数目
BYTE WZMJGameLogic::GetCardCount(BYTE cbCardIndex[MAX_INDEX])
{
	//数目统计
	BYTE cbCardCount=0;
	for (BYTE i=0;i<MAX_INDEX;i++) 
		cbCardCount+=cbCardIndex[i];

	return cbCardCount;
}

//获取组合
BYTE WZMJGameLogic::GetWeaveCard(BYTE cbWeaveKind, BYTE cbCenterCard, BYTE cbCardBuffer[4])
{
	//组合扑克
	switch (cbWeaveKind)
	{
	case WIK_LEFT:		//上牌操作
		{
			//设置变量
			cbCardBuffer[0]=cbCenterCard+1;
			cbCardBuffer[1]=cbCenterCard+2;
			cbCardBuffer[2]=cbCenterCard;

			return 3;
		}
	case WIK_RIGHT:		//上牌操作
		{
			//设置变量
			cbCardBuffer[0]=cbCenterCard-2;
			cbCardBuffer[1]=cbCenterCard-1;
			cbCardBuffer[2]=cbCenterCard;

			return 3;
		}
	case WIK_CENTER:	//上牌操作
		{
			//设置变量
			cbCardBuffer[0]=cbCenterCard-1;
			cbCardBuffer[1]=cbCenterCard;
			cbCardBuffer[2]=cbCenterCard+1;

			return 3;
		}
	case WIK_PENG:		//碰牌操作
		{
			//设置变量
			cbCardBuffer[0]=cbCenterCard;
			cbCardBuffer[1]=cbCenterCard;
			cbCardBuffer[2]=cbCenterCard;

			return 3;
		}
	case WIK_GANG:		//杠牌操作
		{
			//设置变量
			cbCardBuffer[0]=cbCenterCard;
			cbCardBuffer[1]=cbCenterCard;
			cbCardBuffer[2]=cbCenterCard;
			cbCardBuffer[3]=cbCenterCard;

			return 4;
		}
	default:
		{
			assert(FALSE);
		}
	}

	return 0;
}

//动作等级
BYTE WZMJGameLogic::GetUserActionRank(BYTE cbUserAction)
{
	//胡牌等级
	if (cbUserAction&WIK_CHI_HU) { return 4; }

	//杠牌等级
	if (cbUserAction&WIK_GANG) { return 3; }

	//碰牌等级
	if (cbUserAction&WIK_PENG) { return 2; }

	//上牌等级
	if (cbUserAction&(WIK_RIGHT|WIK_CENTER|WIK_LEFT)) { return 1; }

	return 0;
}

//胡牌等级
BYTE WZMJGameLogic::GetChiHuActionRank(tagChiHuResult & ChiHuResult)
{
	//变量定义
	BYTE cbChiHuOrder=0;
	WORD wChiHuRight=ChiHuResult.wChiHuRight;
	WORD wChiHuKind=(ChiHuResult.wChiHuKind&0xFF00)>>4;

	//大胡升级
	for (BYTE i=0;i<8;i++)
	{
		wChiHuKind>>=1;
		if ((wChiHuKind&0x0001)!=0) 
			cbChiHuOrder++;
	}

	//权位升级
	for (BYTE i=0;i<16;i++)
	{
		wChiHuRight>>=1;
		if ((wChiHuRight&0x0001)!=0) 
			cbChiHuOrder++;
	}

	return cbChiHuOrder;
}

//吃牌判断
BYTE WZMJGameLogic::EstimateEatCard(BYTE cbCardIndex[MAX_INDEX], BYTE cbCurrentCard)
{
	//王牌过滤
	if(IsKingCardData(cbCurrentCard))
	{
		return WIK_NULL;
	}

	//参数效验
	assert(IsValidCard(cbCurrentCard));

	//过滤判断
	//番子无连
	if (cbCurrentCard>=0x31) return WIK_NULL;

	//变量定义
	BYTE cbExcursion[3]={0,1,2};
	BYTE cbItemKind[3]={WIK_LEFT,WIK_CENTER,WIK_RIGHT};

	//吃牌判断
	BYTE cbEatKind=0,cbFirstIndex=0;
	BYTE cbCurrentIndex=SwitchToCardIndex(cbCurrentCard);
	for (BYTE i=0;i<CountArray(cbItemKind);i++)
	{
		BYTE cbValueIndex=cbCurrentIndex%9;
		if ((cbValueIndex>=cbExcursion[i])&&((cbValueIndex-cbExcursion[i])<=6))
		{
			//吃牌判断
			cbFirstIndex=cbCurrentIndex-cbExcursion[i];
			if ((cbCurrentIndex!=cbFirstIndex)&&(cbCardIndex[cbFirstIndex]==0))
				continue;
			if ((cbCurrentIndex!=(cbFirstIndex+1))&&(cbCardIndex[cbFirstIndex+1]==0))
				continue;
			if ((cbCurrentIndex!=(cbFirstIndex+2))&&(cbCardIndex[cbFirstIndex+2]==0))
				continue;

			//设置类型
			cbEatKind|=cbItemKind[i];
		}
	}

	return cbEatKind;
}

//碰牌判断
BYTE WZMJGameLogic::EstimatePengCard(BYTE cbCardIndex[MAX_INDEX], BYTE cbCurrentCard)
{
	//王牌过滤
	if(IsKingCardData(cbCurrentCard))
	{
		return WIK_NULL;
	}

	//参数效验
	assert(IsValidCard(cbCurrentCard));

	//碰牌判断
	return (cbCardIndex[SwitchToCardIndex(cbCurrentCard)]>=2)?WIK_PENG:WIK_NULL;
}

//杠牌判断
BYTE WZMJGameLogic::EstimateGangCard(BYTE cbCardIndex[MAX_INDEX], BYTE cbCurrentCard)
{
	//王牌过滤
	if(IsKingCardData(cbCurrentCard))
	{
		return WIK_NULL;
	}
	//参数效验
	assert(IsValidCard(cbCurrentCard));

	//杠牌判断
	return (cbCardIndex[SwitchToCardIndex(cbCurrentCard)]==3)?WIK_GANG:WIK_NULL;
}

//杠牌分析
BYTE WZMJGameLogic::AnalyseGangCard(BYTE cbCardIndex[MAX_INDEX], tagWeaveItem WeaveItem[], BYTE cbWeaveCount, tagGangCardResult & GangCardResult)
{
	//设置变量
	BYTE cbActionMask=WIK_NULL;
	memset(&GangCardResult, 0, sizeof(GangCardResult));

	//手上杠牌 过滤财神
	for (BYTE i=0;i<MAX_INDEX-1;i++)
	{
		if (cbCardIndex[i] >= 4)
		{
			cbActionMask|=WIK_GANG;
			GangCardResult.cbCardData[GangCardResult.cbCardCount]=WIK_GANG;
			GangCardResult.cbCardData[GangCardResult.cbCardCount++]=SwitchToCardData(i);
		}
	}

	//组合杠牌
	for (BYTE i=0;i<cbWeaveCount;i++)
	{
		if (WeaveItem[i].cbWeaveKind==WIK_PENG)
		{
			if (cbCardIndex[SwitchToCardIndex(WeaveItem[i].cbCenterCard)]>=1)
			{
				cbActionMask|=WIK_GANG;
				GangCardResult.cbCardData[GangCardResult.cbCardCount]=WIK_GANG;
				GangCardResult.cbCardData[GangCardResult.cbCardCount++]=WeaveItem[i].cbCenterCard;
			}
		}
	}

	return cbActionMask;
}
//分析扑克
bool WZMJGameLogic::AnalyseCard(BYTE cbCardIndex[MAX_INDEX], tagWeaveItem WeaveItem[], BYTE cbWeaveCount, CAnalyseItemArray & AnalyseItemArray)
{
	//计算数目
	BYTE cbCardCount=0;
	for (BYTE i=0;i<MAX_INDEX;i++) 
		cbCardCount+=cbCardIndex[i];

	//效验数目
	//assert((cbCardCount>=2)&&(cbCardCount<=MAX_COUNT)&&((cbCardCount-2)%3==0));
	if ((cbCardCount<2)||(cbCardCount>MAX_COUNT_WZMJ)||((cbCardCount-2)%3!=0))	return false;

	//变量定义
	BYTE cbKindItemCount=0;
	tagKindItem KindItem[MAX_COUNT_WZMJ-2];
	memset(KindItem, 0, sizeof(KindItem));

	//需求判断
	BYTE cbLessKindItem=(cbCardCount-2)/3;
	assert((cbLessKindItem+cbWeaveCount)==5);

	//单吊判断
	if (cbLessKindItem==0)
	{
		//效验参数
		assert((cbCardCount==2)&&(cbWeaveCount==5));
		if(cbCardIndex[CARD_KING_INDEX] == 2) //金雀 财神作眼牌
		{
			//变量定义
			tagAnalyseItem AnalyseItem;
			memset(&AnalyseItem, 0, sizeof(AnalyseItem));
			//设置结果
			for (BYTE j=0;j<cbWeaveCount;j++)
			{
				AnalyseItem.cbWeaveKind[j]=WeaveItem[j].cbWeaveKind;
				AnalyseItem.cbCenterCard[j]=WeaveItem[j].cbCenterCard;
			}
			AnalyseItem.cbCardEye=SwitchToCardData(CARD_KING_INDEX);

			//插入结果
			AnalyseItemArray.push_back(AnalyseItem);
			return true;

		}

		if (cbCardIndex[CARD_KING_INDEX] == 1) //王牌+单张
		{
			//变量定义
			tagAnalyseItem AnalyseItem;
			memset(&AnalyseItem, 0, sizeof(AnalyseItem));
			//设置结果
			for (BYTE j=0;j<cbWeaveCount;j++)
			{
				AnalyseItem.cbWeaveKind[j]=WeaveItem[j].cbWeaveKind;
				AnalyseItem.cbCenterCard[j]=WeaveItem[j].cbCenterCard;
			}
			for (BYTE i=0;i<CARD_KING_INDEX;i++)
			{
				if (cbCardIndex[i]==1)
				{
					AnalyseItem.cbCardEye=SwitchToCardData(i);
				}
			}
			//插入结果
			AnalyseItemArray.push_back(AnalyseItem);
			return true;
		}

		//牌眼判断
		for (BYTE i=0;i<MAX_INDEX;i++)
		{
			if (cbCardIndex[i]==2)
			{
				//变量定义
				tagAnalyseItem AnalyseItem;
				memset(&AnalyseItem, 0, sizeof(AnalyseItem));

				//设置结果
				for (BYTE j=0;j<cbWeaveCount;j++)
				{
					AnalyseItem.cbWeaveKind[j]=WeaveItem[j].cbWeaveKind;
					AnalyseItem.cbCenterCard[j]=WeaveItem[j].cbCenterCard;
				}
				AnalyseItem.cbCardEye=SwitchToCardData(i);

				//插入结果
				AnalyseItemArray.push_back(AnalyseItem);
				return true;
			}
		}
		return false;
	}


	//拆分分析
	if (cbCardCount>=3)
	{
		for (BYTE i=0;i<MAX_INDEX;i++)
		{
			//同牌判断
			if (cbCardIndex[i]>=3)
			{
				KindItem[cbKindItemCount].cbCenterCard=i;
				KindItem[cbKindItemCount].cbCardIndex[0]=i;
				KindItem[cbKindItemCount].cbCardIndex[1]=i;
				KindItem[cbKindItemCount].cbCardIndex[2]=i;
				KindItem[cbKindItemCount++].cbWeaveKind=WIK_PENG;
			}

			//连牌判断
			if ((i<(MAX_INDEX-2-7))&&(cbCardIndex[i]>0)&&((i%9)<7))
			{
				for (BYTE j=1;j<=cbCardIndex[i];j++)
				{
					if ((cbCardIndex[i+1]>=j)&&(cbCardIndex[i+2]>=j))
					{
						KindItem[cbKindItemCount].cbCenterCard=i;
						KindItem[cbKindItemCount].cbCardIndex[0]=i;
						KindItem[cbKindItemCount].cbCardIndex[1]=i+1;
						KindItem[cbKindItemCount].cbCardIndex[2]=i+2;
						KindItem[cbKindItemCount++].cbWeaveKind=WIK_LEFT;
					}
				}
			}
		}
	}

	//组合分析
	if (cbKindItemCount>=cbLessKindItem)
	{
		//变量定义
		BYTE cbCardIndexTemp[MAX_INDEX];
		memset(cbCardIndexTemp, 0, sizeof(cbCardIndexTemp));

		//变量定义
		BYTE cbIndex[MAX_WEAVE]={0,1,2,3,4};
		tagKindItem * pKindItem[MAX_WEAVE];
		BYTE i = 0;
		memset(&pKindItem, 0, sizeof(pKindItem));

		//开始组合
		do
		{
			//设置变量
			CopyMemory(cbCardIndexTemp,cbCardIndex,sizeof(cbCardIndexTemp));
			for (i=0;i<cbLessKindItem;i++)
				pKindItem[i]=&KindItem[cbIndex[i]];

			//数量判断
			bool bEnoughCard=true;
			for (i=0;i<cbLessKindItem*3;i++)
			{
				//存在判断
				BYTE cbCardIndex=pKindItem[i/3]->cbCardIndex[i%3]; 
				if (cbCardIndexTemp[cbCardIndex]==0)
				{
					bEnoughCard=false;
					break;
				}
				else 
					cbCardIndexTemp[cbCardIndex]--;
			}

			//胡牌判断
			if (bEnoughCard==true)
			{
				//牌眼判断
				BYTE cbCardEye=0;
				for (i=0;i<MAX_INDEX;i++)
				{
					if (cbCardIndexTemp[i]==2)
					{
						cbCardEye=SwitchToCardData(i);
						break;
					}
				}

				//组合类型
				if (cbCardEye!=0)
				{
					//变量定义
					tagAnalyseItem AnalyseItem;
					memset(&AnalyseItem, 0, sizeof(AnalyseItem));

					//设置组合
					for (i=0;i<cbWeaveCount;i++)
					{
						AnalyseItem.cbWeaveKind[i]=WeaveItem[i].cbWeaveKind;
						AnalyseItem.cbCenterCard[i]=WeaveItem[i].cbCenterCard;
					}

					//设置牌型
					for (i=0;i<cbLessKindItem;i++) 
					{
						AnalyseItem.cbWeaveKind[i+cbWeaveCount]=pKindItem[i]->cbWeaveKind;
						AnalyseItem.cbCenterCard[i+cbWeaveCount]=pKindItem[i]->cbCenterCard;
					}

					//设置牌眼
					AnalyseItem.cbCardEye=cbCardEye;

					//插入结果
					AnalyseItemArray.push_back(AnalyseItem);
				}
			}

			//设置索引
			if (cbIndex[cbLessKindItem-1]==(cbKindItemCount-1))
			{
				for (i=cbLessKindItem-1;i>0;i--)
				{
					if ((cbIndex[i-1]+1)!=cbIndex[i])
					{
						BYTE cbNewIndex=cbIndex[i-1];
						for (BYTE j=(i-1);j<cbLessKindItem;j++) 
							cbIndex[j]=cbNewIndex+j-i+2;
						break;
					}
				}
				if (i==0)
					break;
			}
			else
				cbIndex[cbLessKindItem-1]++;

		} while (true);

	}

	return (AnalyseItemArray.size()>0);
}


//吃胡分析
BYTE WZMJGameLogic::ChiHu(BYTE cbCardIndex[MAX_INDEX], tagWeaveItem WeaveItem[],BYTE cbWeaveCount,WORD wChiHuRight, tagChiHuResult & ChiHuResult)
{
	//变量定义
	WORD wChiHuKind=CHK_NULL;
	static CAnalyseItemArray AnalyseItemArray;

	//设置变量
	AnalyseItemArray.clear();
	memset(&ChiHuResult, 0, sizeof(ChiHuResult));

	//构造扑克
	BYTE cbCardIndexTemp[MAX_INDEX];
	CopyMemory(cbCardIndexTemp,cbCardIndex,sizeof(cbCardIndexTemp));

	//分析扑克
	AnalyseCard(cbCardIndexTemp,WeaveItem,cbWeaveCount,AnalyseItemArray);

	//胡牌分析
	if (AnalyseItemArray.size()>0)
	{
		//牌型分析
		for (int i=0;i<AnalyseItemArray.size();i++)
		{
			//变量定义
			bool bLianCard=false,bPengCard=false,bGangCard =false;
			tagAnalyseItem * pAnalyseItem=&AnalyseItemArray[i];

			//牌型分析
			for (BYTE j=0;j<CountArray(pAnalyseItem->cbWeaveKind);j++)
			{
				BYTE cbWeaveKind=pAnalyseItem->cbWeaveKind[j];
				bPengCard=((cbWeaveKind&(WIK_GANG|WIK_PENG))!=0)?true:bPengCard;
				bLianCard=((cbWeaveKind&(WIK_LEFT|WIK_CENTER|WIK_RIGHT))!=0)?true:bLianCard;
			}
			//牌型判断
			assert((bLianCard==true)||(bPengCard==true));	
			//基胡
			if ((bLianCard==true)&&(bPengCard==true)) 
				wChiHuKind|=CHK_JI_HU;
			if ((bLianCard==true)&&(bGangCard==false)) 
				wChiHuKind|=CHK_JI_HU;
			if((bLianCard==false)&&(bPengCard==true))
				wChiHuKind|=CHK_JI_HU;
		}
	}

	//结果判断
	if (wChiHuKind!=CHK_NULL)
	{
		//设置结果
		ChiHuResult.wChiHuKind=wChiHuKind;
		ChiHuResult.wChiHuRight=wChiHuRight;
		return WIK_CHI_HU;
	}

	return WIK_NULL;
}

//硬胡
BYTE WZMJGameLogic::YingHu(BYTE cbCardIndex[MAX_INDEX], tagWeaveItem WeaveItem[], BYTE cbWeaveCount, WORD wChiHuRight, tagChiHuResult & ChiHuResult)
{
	//定义变量
	WORD wChiHuKind = WIK_NULL;
	
	//无财神
	if(cbCardIndex[CARD_KING_INDEX] == 0)
	{
		//调用吃胡函数
		wChiHuKind = ChiHu(cbCardIndex,WeaveItem,cbWeaveCount,wChiHuRight,ChiHuResult);

		//判断结果
		if(wChiHuKind != WIK_NULL)
		{
			//增加权位
			ChiHuResult.wChiHuRight |= (wChiHuRight|CHR_YING_HU);
			return WIK_CHI_HU;
		}
	}
	else   //有财神
	{
		//记录财神和白板的个数
		BYTE cbTempKingIndex= cbCardIndex[CARD_KING_INDEX];
		BYTE cbTempBaiIndex = cbCardIndex[SwitchToCardIndex(m_cbKingCardData)];

		//拷贝临时变量
		BYTE cbTempCardIndex[MAX_INDEX];
		CopyMemory(cbTempCardIndex,cbCardIndex,sizeof(cbTempCardIndex));

		//变换索引
		cbTempCardIndex[CARD_KING_INDEX] =0;
		cbTempCardIndex[SwitchToCardIndex(m_cbKingCardData)] += cbTempKingIndex;

		//定义变量
		static CAnalyseItemArray AnalyseItemArray2;

		//设置变量
		AnalyseItemArray2.clear();
		memset(&ChiHuResult, 0, sizeof(ChiHuResult));

		//分析扑克
		AnalyseCard(cbTempCardIndex,WeaveItem,cbWeaveCount,AnalyseItemArray2);

		//胡牌分析
		if (AnalyseItemArray2.size()>0)
		{
			//牌型分析
			for (int i=0; i < AnalyseItemArray2.size(); i++)
			{
				//变量定义
				bool bLianCard=false,bPengCard=false,bGangCard =false;
				tagAnalyseItem * pAnalyseItem=&AnalyseItemArray2[i];

				//牌型分析
				for (BYTE j=0;j<CountArray(pAnalyseItem->cbWeaveKind);j++)
				{
					BYTE cbWeaveKind=pAnalyseItem->cbWeaveKind[j];
					bPengCard=((cbWeaveKind&(WIK_GANG|WIK_PENG))!=0)?true:bPengCard;
					bLianCard=((cbWeaveKind&(WIK_LEFT|WIK_CENTER|WIK_RIGHT))!=0)?true:bLianCard;
				}
				//牌型判断
				assert((bLianCard==true)||(bPengCard==true));	
				
				//基胡
				if ((bLianCard==true)&&(bPengCard==true)) 
					wChiHuKind|=CHK_JI_HU;
				if ((bLianCard==true)&&(bGangCard==false)) 
					wChiHuKind|=CHK_JI_HU;
				if((bLianCard==false)&&(bPengCard==true))
					wChiHuKind|=CHK_JI_HU;

				//眼牌判断
				if(pAnalyseItem->cbCardEye != m_cbKingCardData)  //眼牌不是财神
				{
					//判断结果
					if(wChiHuKind != WIK_NULL)
					{
						ChiHuResult.wChiHuKind=wChiHuKind;
						//增加权位
						ChiHuResult.wChiHuRight |=(wChiHuRight|CHR_YING_HU);
						return WIK_CHI_HU;
					}
				}
				if(pAnalyseItem->cbCardEye == m_cbKingCardData) //眼牌是财神
				{
					if (cbTempKingIndex>=2 ||cbTempBaiIndex>=2)
					{
						//设置结果
						ChiHuResult.wChiHuKind=wChiHuKind;
						ChiHuResult.wChiHuRight |=(wChiHuRight|CHR_YING_HU);
						return WIK_CHI_HU;
					}
				}
			}
		}
		//软八对
		if(IsBaDui(cbTempCardIndex,WeaveItem,cbWeaveCount) == true)
		{
			ChiHuResult.wChiHuKind=CHK_BA_DUI;
			ChiHuResult.wChiHuRight |=(wChiHuRight|CHR_YING_HU);
			return WIK_CHI_HU;            
		}

		if (wChiHuKind !=WIK_NULL)
		{
			ChiHuResult.wChiHuKind=wChiHuKind;
			ChiHuResult.wChiHuRight |=(wChiHuRight|CHR_RUAN_HU);
			return WIK_CHI_HU;
		}
		return WIK_NULL;
	}

	return WIK_NULL;

}
BYTE WZMJGameLogic::RuanHu(BYTE cbCardIndex[MAX_INDEX], tagWeaveItem WeaveItem[], BYTE cbWeaveCount, WORD wChiHuRight, tagChiHuResult & ChiHuResult)
{
	//定义变量
	WORD wCHiHuKind = WIK_NULL;

	//过滤无财神
	if (cbCardIndex[CARD_KING_INDEX] ==0) return WIK_NULL;

	//过滤3金倒
	if(cbCardIndex[CARD_KING_INDEX] > 3) return WIK_NULL;

	//记录财神个数
	BYTE cbTempKingIndex= cbCardIndex[CARD_KING_INDEX];

	//拷贝临时变量
	BYTE cbTempCardIndex[MAX_INDEX];
	CopyMemory(cbTempCardIndex,cbCardIndex,sizeof(cbTempCardIndex));

	//财神索引置零
	cbTempCardIndex[CARD_KING_INDEX]=0;

	//先判断软八对　硬胡
	if(cbTempKingIndex>=1)
	{
		//王牌递减
		cbTempKingIndex--;

		//循环替换
		for (BYTE i=0;i<CARD_KING_INDEX;i++)
		{
			//替换增加
			cbTempCardIndex[i]++;

			//重复判断
			if(cbTempKingIndex>=1)
			{
				//王牌递减
				cbTempKingIndex--;

				//循环替换
				for (BYTE j=0;j<CARD_KING_INDEX;j++)
				{
					//替换增加
					cbTempCardIndex[j]++;

					if(cbTempKingIndex>=1)
					{
						//王牌递减
						cbTempKingIndex--;

						//循环替换
						for (BYTE k=0;k<CARD_KING_INDEX;k++)
						{
							//替换增加
							cbTempCardIndex[k]++;

							//判断软８对　硬胡
							if(IsBaDui(cbTempCardIndex,WeaveItem,cbWeaveCount)==true)
							{
								ChiHuResult.wChiHuKind = CHK_BA_DUI;
								ChiHuResult.wChiHuRight =(wChiHuRight|CHR_YING_HU);
								return WIK_CHI_HU;
							}

							//替换还原
							cbTempCardIndex[k]--;
						}

						//王牌还原
						cbTempKingIndex++;
					}
					else
					{
						//判断软８对　硬胡
						if(IsBaDui(cbTempCardIndex,WeaveItem,cbWeaveCount)==true)
						{
							ChiHuResult.wChiHuKind = CHK_BA_DUI;
							ChiHuResult.wChiHuRight =(wChiHuRight|CHR_YING_HU);
							return WIK_CHI_HU;
						}
					}
					//替换还原
					cbTempCardIndex[j]--;
				}
				//王牌还原
				cbTempKingIndex++;
			}
			else
			{
				//判断软８对　硬胡
				if(IsBaDui(cbTempCardIndex,WeaveItem,cbWeaveCount)==true)
				{
					ChiHuResult.wChiHuKind = CHK_BA_DUI;
					ChiHuResult.wChiHuRight =(wChiHuRight|CHR_YING_HU);
					return WIK_CHI_HU;
				}
			}
			//替换还原
			cbTempCardIndex[i]--;
		}
		//王牌还原
		cbTempKingIndex++;

	}


	//再判断软胡
	if(cbTempKingIndex>=1)
	{
		//王牌递减
		cbTempKingIndex--;

		//循环替换
		for (BYTE i=0;i<CARD_KING_INDEX;i++)
		{
			//替换增加
			cbTempCardIndex[i]++;

			//重复判断
			if(cbTempKingIndex>=1)
			{
				//王牌递减
				cbTempKingIndex--;


				//循环替换
				for (BYTE j=0;j<CARD_KING_INDEX;j++)
				{
					//替换增加
					cbTempCardIndex[j]++;

					//重复判断
					if(cbTempKingIndex>=1)
					{
						//王牌递减
						cbTempKingIndex--;

						//循环替换
						for (BYTE k=0;k<CARD_KING_INDEX;k++)
						{
							//替换增加
							cbTempCardIndex[k]++;

							//胡牌判断
							wCHiHuKind = ChiHu(cbTempCardIndex,WeaveItem,cbWeaveCount,wChiHuRight,ChiHuResult);                    
							if(wCHiHuKind!=CHK_NULL)
							{
								ChiHuResult.wChiHuRight =(wChiHuRight|CHR_RUAN_HU);
								return WIK_CHI_HU;
							}
							//替换还原
							cbTempCardIndex[k]--;                    
						}
						//王牌还原
						cbTempKingIndex++;
					}
					else
					{
						//吃胡判断
						wCHiHuKind = ChiHu(cbTempCardIndex,WeaveItem,cbWeaveCount,wChiHuRight,ChiHuResult);
						if(wCHiHuKind!=CHK_NULL)
						{
							ChiHuResult.wChiHuRight =(wChiHuRight|CHR_RUAN_HU);
							return WIK_CHI_HU;
						}

					}                    
					//替换还原
					cbTempCardIndex[j]--;                    
				}
				//王牌还原
				cbTempKingIndex++;
			}
			else
			{
				//吃胡判断
				wCHiHuKind = ChiHu(cbTempCardIndex,WeaveItem,cbWeaveCount,wChiHuRight,ChiHuResult);
				if(wCHiHuKind!=CHK_NULL)
				{
					ChiHuResult.wChiHuRight =(wChiHuRight|CHR_RUAN_HU);
					return WIK_CHI_HU;
				}
			}
			//替换还原
			cbTempCardIndex[i]--;
		}
		//王牌还原
		cbTempKingIndex++;
	}
	return WIK_NULL;

}


BYTE WZMJGameLogic::AnalyseChiHuCard(BYTE cbCardIndex[MAX_INDEX], tagWeaveItem WeaveItem[], BYTE cbWeaveCount, BYTE cbCurrentCard, WORD wChiHuRight, tagChiHuResult & ChiHuResult)
{
	//变量定义
	WORD wChiHuKind=CHK_NULL;

	//设置变量
	memset(&ChiHuResult, 0, sizeof(ChiHuResult));

	//构造扑克
	BYTE cbCardIndexTemp[MAX_INDEX];
	CopyMemory(cbCardIndexTemp,cbCardIndex,sizeof(cbCardIndexTemp));

	//插入扑克
	if (cbCurrentCard!=0)
		cbCardIndexTemp[SwitchToCardIndex(cbCurrentCard)]++;
	
	//自模权位 ->硬胡
	if(cbCurrentCard==0)
	{
		wChiHuRight|= CHR_ZI_MO;
		wChiHuRight |= CHR_YING_HU; 
	}
	//权位处理 单吊->双翻
	if (cbWeaveCount==5)
	{
		wChiHuRight|= CHR_DAN_DIAO;
		wChiHuRight |= CHR_SHUANG_FAN;
	}
	
	//天胡　->双翻
	if(wChiHuRight&CHR_TIAN)
	{
        wChiHuRight |= CHR_SHUANG_FAN;
	}

	//地胡 ->双翻
	if (wChiHuRight&CHR_DI)
	{
		wChiHuRight |= CHR_SHUANG_FAN;
	}


	//抢杠胡
	if (wChiHuRight&CHR_QIANG_GANG)
	{
		wChiHuRight |= CHR_YING_HU;
	}
	//权位调整 杠胡+自摸 = 杠上花
	if((wChiHuRight&CHR_QIANG_GANG)&&(wChiHuRight&CHR_ZI_MO))
	{
		wChiHuRight &= ~CHR_QIANG_GANG;
		wChiHuRight |= CHR_GANG_FLOWER;
		wChiHuRight |= CHR_YING_HU;
	}
	//清一色
	if(IsQingYiSe(cbCardIndex,WeaveItem,cbWeaveCount)==true)
	{
		wChiHuRight|=CHR_QING_YI_SE;
		wChiHuRight |=CHR_SHUANG_FAN;
	}
	//硬八对
	if (IsBaDui(cbCardIndexTemp,WeaveItem,cbWeaveCount)==true)
	{
		ChiHuResult.wChiHuKind = CHK_BA_DUI;
		ChiHuResult.wChiHuRight =( wChiHuRight|CHR_SHUANG_FAN);
		return WIK_CHI_HU;
	}

	//硬胡判断
	wChiHuKind=YingHu(cbCardIndexTemp,WeaveItem,cbWeaveCount,wChiHuRight,ChiHuResult);
	if(wChiHuKind != WIK_NULL)
	{
		if(cbCardIndex[CARD_KING_INDEX]==3)
		{
			if(ChiHuResult.wChiHuRight&CHR_YING_HU)
			{
				ChiHuResult.wChiHuRight |=(~CHR_YING_HU);
			}
			ChiHuResult.wChiHuRight |=(CHR_THREE_KING|CHR_SHUANG_FAN);
		}
		return WIK_CHI_HU;
	}

	//剩下的是软胡
	wChiHuKind = RuanHu(cbCardIndexTemp,WeaveItem,cbWeaveCount,wChiHuRight,ChiHuResult);
	if(wChiHuKind != WIK_NULL)
	{
		if(cbCardIndex[CARD_KING_INDEX]==3)
		{
			if(ChiHuResult.wChiHuRight&CHR_YING_HU)
			{
				ChiHuResult.wChiHuRight |=(~CHR_YING_HU);
			}
			if(ChiHuResult.wChiHuRight&CHR_RUAN_HU)
			{
				ChiHuResult.wChiHuRight |=(~CHR_RUAN_HU);
			}

			ChiHuResult.wChiHuRight |=(CHR_THREE_KING|CHR_SHUANG_FAN);
		}

		return WIK_CHI_HU;
	}
	//3金倒
	if(cbCardIndex[CARD_KING_INDEX] == 3)
	{
		ChiHuResult.wChiHuKind = CHK_JI_HU;
		ChiHuResult.wChiHuRight=(CHR_THREE_KING |CHR_YING_HU);
		return WIK_CHI_HU; 
	}

	return WIK_NULL;
}

//是否听牌
BYTE WZMJGameLogic::IsTingCard( const BYTE cbCardIndex[MAX_INDEX], tagWeaveItem WeaveItem[], BYTE cbWeaveCount )
{
	//复制数据
	BYTE cbCardIndexTemp[MAX_INDEX];
	CopyMemory( cbCardIndexTemp,cbCardIndex,sizeof(cbCardIndexTemp) );

	tagChiHuResult ChiHuResult;
	//牌型权位
	WORD wChiHuRight=0;
	for( BYTE i = 0; i < MAX_INDEX; i++ )
	{
		BYTE cbCurrentCard = SwitchToCardData( i );
		if( WIK_CHI_HU == AnalyseChiHuCard( cbCardIndexTemp,WeaveItem,cbWeaveCount,cbCurrentCard,wChiHuRight,ChiHuResult ) )
			return cbCurrentCard;
	}
	return 0;
}

//扑克转换
BYTE WZMJGameLogic::SwitchToCardData(BYTE cbCardIndex)
{
	assert(cbCardIndex<MAX_INDEX);
	//判断王牌
	if(IsKingCardIndex(cbCardIndex))
	{
		return CARD_KING_DATA_MASK;
	}

	assert(cbCardIndex<34);
	return ((cbCardIndex/9)<<4)|(cbCardIndex%9+1);
}

//扑克转换
BYTE WZMJGameLogic::SwitchToCardIndex(BYTE cbCardData)
{
	//判断王牌
	if(IsKingCardData(cbCardData))
	{
		return CARD_KING_INDEX;
	}

	return ((cbCardData&MASK_COLOR)>>4)*9+(cbCardData&MASK_VALUE)-1;
}

//扑克转换
BYTE WZMJGameLogic::SwitchToCardData(BYTE cbCardIndex[MAX_INDEX], BYTE cbCardData[MAX_COUNT_WZMJ],BYTE bMaxCount)
{
	//转换扑克 王牌放到最前面
	BYTE bPosition=0;
	if(cbCardIndex[CARD_KING_INDEX] > 0)
	{
		for (BYTE j=0;j<cbCardIndex[CARD_KING_INDEX];j++)
		{
			assert(bPosition<bMaxCount);
			cbCardData[bPosition++]=SwitchToCardData(CARD_KING_INDEX);
		}
	}

	for (BYTE i=0;i<MAX_INDEX;i++) 
	{
		if(i == CARD_KING_INDEX)
		{
			continue;
		}
		if (cbCardIndex[i]!=0)
		{
			for (BYTE j=0;j<cbCardIndex[i];j++)
			{
				cbCardData[bPosition++]=SwitchToCardData(i);
			}
		}
	}

	return bPosition;
}

//扑克转换
BYTE WZMJGameLogic::SwitchToCardIndex(BYTE cbCardData[], BYTE cbCardCount, BYTE cbCardIndex[MAX_INDEX])
{
	//设置变量
	memset(cbCardIndex, 0, sizeof(BYTE)*MAX_INDEX);

	//转换扑克
	for (BYTE i=0;i<cbCardCount;i++)
	{
		cbCardIndex[SwitchToCardIndex(cbCardData[i])]++;
	}

	return cbCardCount;
}
//设置王牌
void WZMJGameLogic::SetKingCardData(BYTE cbKingCardData)
{
	if(cbKingCardData != 0xFF)
	{
		assert(IsValidCard(cbKingCardData));
	}

	m_cbKingCardData = cbKingCardData;
}

//判断王牌
bool WZMJGameLogic::IsKingCardData(BYTE cbCardData)
{
	if(m_cbKingCardData == 0xFF)
	{
		return false;
	}

	return (cbCardData == CARD_KING_DATA_MASK? true : false);
}
//王牌索引
bool WZMJGameLogic::IsKingCardIndex(BYTE cbCardIndex)
{
	return (cbCardIndex == CARD_KING_INDEX ? true : false);
}

//计算积分
LONG WZMJGameLogic::CalScore(tagChiHuResult & ChihuResult)
{
	//胡牌校验
	assert(ChihuResult.wChiHuKind!=CHK_NULL);
	if (ChihuResult.wChiHuKind== CHK_NULL) return 0;

	//双翻
	if(ChihuResult.wChiHuRight&CHR_SHUANG_FAN) return 4;

	//硬胡
	if(ChihuResult.wChiHuRight&CHR_YING_HU) return 2;

	//软胡
	if(ChihuResult.wChiHuRight&CHR_RUAN_HU) return 1;

	//错误情况
	assert(FALSE);

	return 0;
}

//八对牌
bool WZMJGameLogic::IsBaDui(BYTE cbCardIndex[MAX_INDEX], tagWeaveItem WeaveItem[], BYTE cbWeaveCount)
{
	//组合判断
	if (cbWeaveCount!=0) 
		return false;
	int NumOfsingle =0;
	//扑克判断
	for (BYTE i=0;i<MAX_INDEX;i++)
	{
		BYTE cbCardCount=cbCardIndex[i];
		if (cbCardCount%2 ==1)
		{
			NumOfsingle++;
			if (NumOfsingle>1)
			{
				return false;
			}
		}
	}
	//if ((cbCardCount!=0)&&(cbCardCount!=2)&&(cbCardCount!=4)) 
	return true;

	//return true;

}

//清一色牌
bool WZMJGameLogic::IsQingYiSe(BYTE cbCardIndex[MAX_INDEX], tagWeaveItem WeaveItem[], BYTE cbItemCount)
{
	//胡牌判断
	BYTE cbCardColor=0xFF;
	for (BYTE i=0;i<MAX_INDEX-1;i++)
	{
		if (cbCardIndex[i]!=0)
		{
			//花色判断
			if (cbCardColor!=0xFF) return false;

			//设置花色
			cbCardColor=(SwitchToCardData(i)&MASK_COLOR);

			//设置索引
			i=(i/9+1)*9-1;
		}
	}

	//组合判断
	for (BYTE i=0;i<cbItemCount;i++)
	{
		BYTE cbCenterCard=WeaveItem[i].cbCenterCard;
		if ((cbCenterCard&MASK_COLOR)!=cbCardColor) return false;
	}

	return true;
}
//////////////////////////////////////////////////////////////////////////
