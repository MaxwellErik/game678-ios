//
//  WZMJCdardControl.cpp
//  550game
//
//  Created by maxwell on 16/7/7.
//
//

#include "WZMJCardControl.hpp"
#include "WZMJGameLogic.h"
#include "CMD_WZMJ.h"


BYTE WZMJCardControl::g_cbMaskValue = 0x0F;
BYTE WZMJCardControl::g_cbMaskColor = 0xF0;
BYTE WZMJCardControl::g_cbCardBack = 0x43;
float WZMJCardControl::g_fCardSpace = 22.f;
#define CARD_WIDTH 90
#define CARD_HEIGHT 145

BYTE WZMJCardControl::g_cbCardData[CARD_MAXCOUNT] =
{
    0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09,						//万子
    0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x19,						//索子
    0x21, 0x22, 0x23, 0x24, 0x25, 0x26, 0x27, 0x28, 0x29,						//同子
    0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37,
};

WZMJCardControl::WZMJCardControl()
{
}

void WZMJCardControl::SetLayer(GameLayer* _layer,int tga, int chair)
{
    m_layer = _layer;
    for (int i = 0; i < MAX_COUNT_WZMJ; i++)
    {
        m_CardTga[i] = tga+i;
    }
    m_TopCardBackSprTga = tga+50;
    m_OutCardTga = tga+52;
    m_Chair = chair;
    m_CardCount = MAX_COUNT_WZMJ;
  }

WZMJCardControl::~WZMJCardControl()
{
}

void WZMJCardControl::ClearCardData()
{
    for (int i = 0; i < m_CardData.size(); i++)
    {
        m_layer->removeChild(m_CardData[i].cardspr);
    }
    m_CardData.clear();
}

int WZMJCardControl::GetCardCount()
{
    int r = 0;
    for (int i = 0; i < m_CardData.size(); i++)
    {
        if(m_CardData[i].cbCardData != 0) r++;
    }
    return r;
}

void WZMJCardControl::SetTopCardData(WORD dwCardCount, BYTE cbCurrentCard)
{
    for (int i = 0; i < m_CardCount; i++)
    {
        auto child = m_layer->getChildByTag(m_CardTga[i]);
        if (child)
            child->removeFromParent();
    }
    m_CardData.clear();
    
    for (int i = 0; i < dwCardCount; i++)
    {
        tagMJCardItem temp;
        temp.cbCardData = 0;
        temp.bShoot = false;
        m_CardData.push_back(temp);
    }
    
    ZeroMemory(&m_CurrentCard, sizeof(m_CurrentCard));
    m_CurrentCard.cbCardData = 0;
    m_CurrentCard.bShoot = false;
    
    float nXScreenPos = (5 - dwCardCount / 3)*(CARD_WIDTH*0.8f*3) + 500;
    float nYScreenPos = 950.0f;
    
    BYTE wCount = dwCardCount;
    if (cbCurrentCard != 0)
        wCount = dwCardCount - 1;
    for (WORD i = 0; i < wCount; i++)
    {
        Sprite* pCard = nullptr;
        
        pCard = GetCardSprite(m_CardData[i].cbCardData, true);
        pCard->setScale(0.8f);
        pCard->setPosition(Vec2(nXScreenPos, nYScreenPos));
        pCard->setTag(m_CardTga[i]);
        m_layer->addChild(pCard);
        
        m_CardData[i].cardspr = pCard;
        
        nXScreenPos += CARD_WIDTH*0.8;
        if (m_CurrentCard.cbCardData != 0 && i == dwCardCount - 2)
             nXScreenPos += 10;
    }
    
    if (cbCurrentCard != 0)
    {
        nXScreenPos += 12;
        Sprite* pCard = GetCardSprite(m_CurrentCard.cbCardData, true);
        pCard->setScale(0.8f);
        pCard->setPosition(Vec2(nXScreenPos, nYScreenPos));
        pCard->setTag(m_CardTga[wCount]);
        m_layer->addChild(pCard);
        m_CardData[wCount].cardspr = pCard;
        
        
    }
     m_CardCount = dwCardCount;
 }

void WZMJCardControl::SetMyCardData(const BYTE bCardData[], WORD dwCardCount, BYTE cbCurrentCard)
{
    m_CardCount = dwCardCount;
    for (int i = 0; i < MAX_COUNT_WZMJ; i++)
    {
        auto child = m_layer->getChildByTag(m_CardTga[i]);
        if (child)
            child->removeFromParent();
    }
    m_CardData.clear();
    
    for (int i = 0; i < m_CardCount; i++)
    {
        tagMJCardItem temp;
        temp.cbCardData = bCardData[i];
        temp.bShoot = false;
        m_CardData.push_back(temp);
    }
        
    ZeroMemory(&m_CurrentCard, sizeof(m_CurrentCard));
    m_CurrentCard.cbCardData = cbCurrentCard;
    m_CurrentCard.bShoot = false;
    
   	float nXScreenPos = ( 5 - m_CardCount / 3) * (CARD_WIDTH*3) + 390;
    float nYScreenPos = 100.0f;
    
    int curCardIndex = -1;
    
    for (WORD i = 0; i < m_CardCount; i++)
    {
        if (m_CurrentCard.cbCardData == m_CardData[i].cbCardData && curCardIndex == -1)
        {
            curCardIndex = i;
            continue;
        }
        Sprite* pCard;
        if (CARD_KING_DATA_MASK == m_CardData[i].cbCardData)
        {
            pCard = GetCardSprite(m_cbMagicCard, true);
            
            Sprite* pCaiShenFlag = Sprite::createWithSpriteFrameName("CaiShen.png");
            pCaiShenFlag->setAnchorPoint(Vec2(0.0f, 1.0f));
            pCaiShenFlag->setPosition(Vec2(3, CARD_HEIGHT - 20));
            pCard->addChild(pCaiShenFlag);
          
        }
        else if (m_cbMagicCard == m_CardData[i].cbCardData)
        {
            pCard = GetCardSprite(CARD_SPACE_DATA_MASK, true);
        }
        else
        {
            pCard = GetCardSprite(m_CardData[i].cbCardData, true);
        }
        
        pCard->setPosition(Vec2(nXScreenPos, nYScreenPos));
        pCard->setTag(m_CardTga[i]);
        m_layer->addChild(pCard);
        m_CardData[i].cardspr = pCard;
        
        nXScreenPos += CARD_WIDTH;
    }
    
    if (m_CurrentCard.cbCardData != 0)
    {
         nXScreenPos += 12;
        Sprite* pCard;
        if (CARD_KING_DATA_MASK == m_CurrentCard.cbCardData)
        {
            pCard = pCard = GetCardSprite(m_cbMagicCard, true);
            Sprite* pCaiShenFlag = Sprite::createWithSpriteFrameName("CaiShen.png");
            pCaiShenFlag->setAnchorPoint(Vec2(0.0f, 1.0f));
            pCaiShenFlag->setPosition(Vec2(3, CARD_HEIGHT - 20));
            pCard->addChild(pCaiShenFlag);
        }
        else if (m_cbMagicCard == m_CurrentCard.cbCardData)
        {
           pCard = GetCardSprite(CARD_SPACE_DATA_MASK, true);
        }
        else
        {
             pCard = GetCardSprite(m_CurrentCard.cbCardData, true);
        }
        
        pCard->setPosition(Vec2(nXScreenPos, nYScreenPos));
        pCard->setTag(m_CardTga[curCardIndex]);
        m_layer->addChild(pCard);
        m_CardData[curCardIndex].cardspr = pCard;
    }
}

void WZMJCardControl::ClearShowOutCard()
{
    m_layer->removeAllChildByTag(m_OutCardTga);
}

void WZMJCardControl::SetCardData(const BYTE cbCardData[], WORD wCardCount, BYTE cbCurrentCard)
{
    if (m_Chair == 0)
    {
        SetTopCardData(wCardCount, cbCurrentCard);
    }
    else
    {
        SetMyCardData(cbCardData, wCardCount, cbCurrentCard);
    }
}

BYTE WZMJCardControl::GetShootCard(BYTE cbCardData)
{
    BYTE cbShootCount=0;
    return cbShootCount;
}

BYTE WZMJCardControl::Box2d(Vec2 localPos)
{
    for (ssize_t i = m_CardData.size()-1; i >= 0; i--)
    {
        if (m_CardData[i].cardspr == nullptr)
        {
            break;
        }
        Rect rc = m_CardData[i].cardspr->getBoundingBox();
        bool isTouched = rc.containsPoint(localPos);
        if (isTouched)
        {
            m_CardData[i].bShoot = !m_CardData[i].bShoot;
            if (m_CardData[i].bShoot == false)
            {
                return m_CardData[i].cbCardData;
            }
            else
                m_CardData[i].cardspr->setPositionY(120);
        }
        else
        {
            m_CardData[i].bShoot = false;
            m_CardData[i].cardspr->setPositionY(100);
        }
    }
    return 0;
}

bool WZMJCardControl::RemoveShootItem()
{
    vector<tagMJCardItem>	TempCardData;
    for (int i = 0; i < m_CardData.size(); i++)
    {
        if (m_CardData[i].bShoot == false)
        {
            tagMJCardItem temp;
            temp.bShoot = false;
            temp.cbCardData = m_CardData[i].cbCardData;
            TempCardData.push_back(temp);
        }
        m_layer->removeChild(m_CardData[i].cardspr);
    }
    m_CardData.clear();
    
    int xc = (TempCardData.size()/2.0f)*50-80;
    for (int i = 0; i < TempCardData.size(); i++)
    {
        tagMJCardItem temp;
        temp.bShoot = false;
        temp.cbCardData = TempCardData[i].cbCardData;
        Sprite* tempspr = GetCardSprite(TempCardData[i].cbCardData);
        tempspr->setPosition(Vec2(_STANDARD_SCREEN_CENTER_.x-xc+i*50,90));
        tempspr->setTag(m_CardTga[i]);
        m_layer->addChild(tempspr);
        temp.cardspr = tempspr;
        m_CardData.push_back(temp);
    }
    
    m_CardCount = m_CardData.size();
    
    return true;
}

bool WZMJCardControl::SetShootCard(BYTE cbCardData)
{
    return true;
}

WORD WZMJCardControl::GetCardData(BYTE cbCardData[], WORD wBufferCount)
{
    if (wBufferCount == 0)	return m_CardData.size();
    
    WORD wCardCount = wBufferCount < m_CardData.size()? wBufferCount:m_CardData.size();
    
    for( WORD i = 0; i < wCardCount; i++ ) cbCardData[i] = m_CardData[i].cbCardData;
    return wCardCount;
}

//是否财神
bool WZMJCardControl::IsMagicCard(const BYTE cbMagicCard)
{
    if (m_cbMagicCard != 0 && cbMagicCard != 0)
    {
        if (m_cbMagicCard >= 0x38 && m_cbMagicCard <= 0x3B)
            return ((cbMagicCard >= 0x38) && (cbMagicCard <= 0x3B));
        else if (m_cbMagicCard >= 0x3C && m_cbMagicCard <= 0x3F)
            return ((cbMagicCard >= 0x3C) && (cbMagicCard <= 0x3F));
        else
            return (cbMagicCard == m_cbMagicCard);
    }
    return false;
}

Sprite* WZMJCardControl::GetCardSprite(BYTE card, bool isStandCard)
{
    Sprite* cardSpr = nullptr;
    if (card == 0)
    {
        if(isStandCard)
            cardSpr = Sprite::createWithSpriteFrameName("wzmj_41.png");
        else
            cardSpr = Sprite::createWithSpriteFrameName("wzmj_43.png");
    }
    else
    {
        int iOffY = 0;
        if (isStandCard)
        {
            cardSpr = Sprite::createWithSpriteFrameName("wzmj_42.png");
            iOffY = -12;
        }
        else
        {
            cardSpr = Sprite::createWithSpriteFrameName("wzmj_44.png");
            iOffY = 12;
        }
        
        int color = GetCardColor(card) >> 4;
        int value = GetCardValue(card);
        
        string str = "wzmj_" + StringUtils::toString(color*10 + value) + ".png";
        Sprite *c = Sprite::createWithSpriteFrameName(str.c_str());
        c->setPosition(cardSpr->getContentSize().width / 2, cardSpr->getContentSize().height / 2 + iOffY);
        cardSpr->addChild(c);
    }
    return cardSpr;
}

//玩家吃牌
bool WZMJCardControl::OnEventUserAction(const tagSelectCardInfo_WZMJ SelectInfo[MAX_COUNT_WZMJ], BYTE cbInfoCount)
{
    //初始化
    m_cbSelectInfoCount = cbInfoCount;
    CopyMemory(m_SelectCardInfo, SelectInfo, sizeof(tagSelectCardInfo_WZMJ)*cbInfoCount);
    
    //设置变量
    m_cbCurSelectIndex = CountArray(m_SelectCardInfo);
    return m_cbSelectInfoCount == 1;
}

//获取玩家操作结果
void WZMJCardControl::GetUserSelectResult(tagSelectCardInfo_WZMJ &SelectInfo)
{

    ASSERT(m_cbSelectInfoCount > 0);
    if (m_cbSelectInfoCount == 0) return;
    
    if (m_cbCurSelectIndex < CountArray(m_SelectCardInfo))
        SelectInfo = m_SelectCardInfo[m_cbCurSelectIndex];
    else
        SelectInfo = m_SelectCardInfo[0];

    return;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////

WZMJOutCardItem::WZMJOutCardItem()
{}

WZMJOutCardItem::~WZMJOutCardItem()
{}

void WZMJOutCardItem::SetLayer(Layer *_layer, int tga)
{
    m_layer = _layer;
    for (int i = 0; i < MAX_INDEX; i++)
    {
        m_MyOutTag[i] = tga+i;
        m_OtherOutTag[i] = tga+50+i;
    }
}

void WZMJOutCardItem::SetTopCardData(const BYTE bCardData)
{
    tagMJCardItem temp;
    temp.cbCardData = bCardData;
    temp.bShoot = false;
    m_OhterCardData.push_back(temp);
    
    auto count = (int)m_OhterCardData.size();
    auto row = (count-1) / 16;
    auto col = (count-1) % 16;
    
    float nXScreenPos = 500.0f + col * CARD_WIDTH*0.8f;
    float nYScreenPos = 800.0f - row * CARD_HEIGHT*0.8f;
    

    Sprite* pCard = nullptr;
        
    if (bCardData == m_cbMagicCard)
        pCard = GetCardSprite(CARD_SPACE_DATA_MASK, false);
    
    else if (bCardData == CARD_KING_DATA_MASK)
        pCard = GetCardSprite(m_cbMagicCard, false);
    
    else
        pCard = GetCardSprite(bCardData, false);
    
    pCard->setScale(0.8f);
    pCard->setPosition(Vec2(nXScreenPos, nYScreenPos));
    pCard->setTag(m_OtherOutTag[count-1]);
    m_layer->addChild(pCard, row);
    m_OhterCardData[count-1].cardspr = pCard;
}

void WZMJOutCardItem::SetMyCardData(const BYTE bCardData)
{
    tagMJCardItem temp;
    temp.cbCardData = bCardData;
    temp.bShoot = false;
    m_MyCardData.push_back(temp);
    
    auto count = (int)m_MyCardData.size();
    auto row = (count-1) / 16;
    auto col = (count-1) % 16;
    
    float nXScreenPos = 480.0f + col * CARD_WIDTH*0.8f;
    float nYScreenPos = 280.0f + row * CARD_HEIGHT*0.8f;
    
    
    Sprite* pCard = nullptr;
    if (bCardData == m_cbMagicCard)
        pCard = GetCardSprite(CARD_SPACE_DATA_MASK, false);
    else if (bCardData == CARD_KING_DATA_MASK)
         pCard = GetCardSprite(m_cbMagicCard, false);
    else
        pCard = GetCardSprite(bCardData, false);
        
    pCard->setScale(0.8f);
    pCard->setPosition(Vec2(nXScreenPos, nYScreenPos));
    pCard->setTag(m_MyOutTag[count-1]);
    m_layer->addChild(pCard, 5-row);
    m_MyCardData[count-1].cardspr = pCard;
}

void WZMJOutCardItem::removeMyLastCard()
{
    m_MyCardData.pop_back();
    if (m_MyOutTag[m_MyCardData.size()] != -1)
    {
        auto child = m_layer->getChildByTag(m_MyOutTag[m_MyCardData.size()]);
        if (child)
            child->removeFromParent();
    }
}

void WZMJOutCardItem::removeOtherLastCard()
{
    m_OhterCardData.pop_back();
    if (m_OtherOutTag[m_OhterCardData.size()] != -1)
    {
        auto child = m_layer->getChildByTag(m_OtherOutTag[m_OhterCardData.size()]);
        if (child)
            child->removeFromParent();
    }
}

void WZMJOutCardItem::clearAllDataAndCard()
{
    m_layer->removeAllChildren();
    m_OhterCardData.clear();
    m_MyCardData.clear();
//    while (m_MyCardData.size() >0 ) {
//        removeMyLastCard();
//    }
//    
//    while (m_OhterCardData.size() > 0) {
//        removeOtherLastCard();
//    }
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////

WZMJWeaveCard::WZMJWeaveCard()
{}

WZMJWeaveCard::~WZMJWeaveCard()
{}

void WZMJWeaveCard::SetLayer(Layer *_layer, int tga)
{
    m_layer = _layer;
    
    for (int i = 0; i < MAX_INDEX; i++)
    {
        m_MyWeaveTag[i] = tga+i;
        m_OtherWeaveTag[i] = tga+200+i;
    }
    m_ControlPoint[0] = 0;
    m_ControlPoint[1] = 0;
    m_TopCardBackSprTga = tga+50;
    m_OutCardTga = tga+52;
}

void WZMJWeaveCard::SetTopCardData(BYTE cardData[], bool publicCard)
{
    float nXExcusionV = CARD_WIDTH*0.8f;
    
    float nXScreenPos = m_ControlPoint[0] * (nXExcusionV * 3.0f + 10) + 200;
    float nYScreenPos =950.0f;
    
    //前三扑克
    for (int i = 0; i < 3; i++)
    {
        Sprite* pCard = nullptr;
        if (publicCard)
            pCard = GetCardSprite(cardData[i], false);
        
        else
            pCard = GetCardSprite(0, false);
        
        pCard->setScale(0.8f);
        pCard->setTag(m_OtherWeaveTag[m_ControlPoint[0]*4+i]);
        pCard->setPosition(Vec2(nXScreenPos + nXExcusionV*i+5, nYScreenPos));
        m_layer->addChild(pCard);
    }
    //第四扑克
    if (cardData[3] == 0) return;

    Sprite* pCard = nullptr;
    if (publicCard)
        pCard = GetCardSprite(cardData[3], false);
    else
        pCard = GetCardSprite(0, false);
        
    pCard->setScale(0.8f);
    pCard->setTag(m_OtherWeaveTag[m_ControlPoint[0]*4+3]);
    pCard->setPosition(Vec2(nXScreenPos + nXExcusionV+5, nYScreenPos + 25));
    m_layer->addChild(pCard);
}

void WZMJWeaveCard::SetMyCardData(BYTE cardData[], bool publicCard)
{
    float nXExcusionV = CARD_WIDTH;
    
    float nXScreenPos = m_ControlPoint[1] * (nXExcusionV * 3.0f + 10)+50;
    float nYScreenPos =100.0f;
    
    //前三扑克
    for (int i = 0; i < 3; i++)
    {
        Sprite* pCard = nullptr;
        if (publicCard)
            pCard = GetCardSprite(cardData[i], false);
        
        else
            pCard = GetCardSprite(0, false);
        
        pCard->setTag(m_MyWeaveTag[m_ControlPoint[1]*4+i]);
        pCard->setPosition(Vec2(nXScreenPos + nXExcusionV*i + 5, nYScreenPos));
        m_layer->addChild(pCard);
    }
    //第四扑克
    if (cardData[3] == 0) return;
    Sprite* pCard = GetCardSprite(cardData[3], false);
    pCard->setTag(m_MyWeaveTag[m_ControlPoint[1]*4+3]);
    pCard->setPosition(Vec2(nXScreenPos + nXExcusionV+5, nYScreenPos + 25));
    m_layer->addChild(pCard);
}

//设置扑克
bool WZMJWeaveCard::SetCardData(tagWeaveItem tagWeave, BYTE chair)
{
    //设置扑克
    
    //白板转财神
    if (tagWeave.cbCenterCard == CARD_SPACE_DATA_MASK)
        tagWeave.cbCenterCard = m_cbMagicCard;
    
    BYTE cardData[4] = {0, 0, 0, 0};
    for (BYTE i = 0; i < 4; i++)
    {
        if (tagWeave.cbWeaveKind == WIK_GANG)
            cardData[i] = tagWeave.cbCenterCard;
        
         //财神转换回白板
        if (cardData[i] == m_cbMagicCard)
            cardData[i] = CARD_SPACE_DATA_MASK;
    }
    for (BYTE i = 0; i < 3; i++)
    {
        if (tagWeave.cbWeaveKind == WIK_PENG)
            cardData[i] = tagWeave.cbCenterCard;
        
        else if (tagWeave.cbWeaveKind == WIK_LEFT)
           cardData[i] = tagWeave.cbCenterCard + i;
        
        else if (tagWeave.cbWeaveKind == WIK_CENTER)
            cardData[i] = tagWeave.cbCenterCard + i - 1;
        
        else if (tagWeave.cbWeaveKind == WIK_RIGHT)
            cardData[i] = tagWeave.cbCenterCard + i - 2;
        
        //财神转换回白板
        if (cardData[i] == m_cbMagicCard)
            cardData[i] = CARD_SPACE_DATA_MASK;
    }
    
    if (chair == 0)
        SetTopCardData(cardData, tagWeave.cbPublicCard);
    else
        SetMyCardData(cardData, tagWeave.cbPublicCard);
    return true;
}

void WZMJWeaveCard::setControlPoint(const BYTE chair)
{
    m_ControlPoint[chair]++;
}

void WZMJWeaveCard::clearDataAndCard()
{
    for (int i = 0; i < GAME_PLAYER_WZMJ; i++)
    {
        m_ControlPoint[i] = 0;
    }
    m_layer->removeAllChildren();
//    for (int j = 0; j < MAX_INDEX; j++)
//    {
//        auto mchild = m_layer->getChildByTag(m_MyWeaveTag[j]);
//        if(mchild)
//            mchild->removeFromParent();
//        auto ochild = m_layer->getChildByTag(m_OtherWeaveTag[j]);
//        if(ochild)
//            ochild->removeFromParent();
//    }
}

void WZMJWeaveCard::clearMyDataAndCard()
{

    m_ControlPoint[1] = 0;
  
    for (int j = 0; j < MAX_INDEX; j++)
    {
        auto mchild = m_layer->getChildByTag(m_MyWeaveTag[j]);
        if(mchild)
        {
//            log("------->>>>>child tag = %d", m_MyWeaveTag[j]);
            mchild->removeFromParent();
        }
    }
}

void WZMJWeaveCard::clearOtherDataAndCard()
{
    m_ControlPoint[0] = 0;
    
    for (int j = 0; j < MAX_INDEX; j++)
    {
        auto ochild = m_layer->getChildByTag(m_OtherWeaveTag[j]);
        if(ochild)
        {
//            log("------->>>>>child tag = %d", m_MyWeaveTag[j]);
            ochild->removeFromParent();
        }
    }
}
