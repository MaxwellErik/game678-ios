﻿#ifndef _WZMJGAME_VIEW_LAYER_H_
#define _WZMJGAME_VIEW_LAYER_H_

#include "GameScene.h"
#include "FrameGameView.h"
#include "WZMJGameLogic.h"
#include "WZMJCardControl.hPP"
#include "CMD_WZMJ.h"


class CTimeTaskLayer;
class WZMJGameViewLayer : public IGameView
{
public:

	enum enTag
	{
        TAG_REAGY,
        TAG_RETURNBACK,
        TAG_SETTING,
        TAG_BUY,
        TAG_NOBUY,
        TAG_CHISHANG,
        TAG_CHIXIA,
        TAG_CHIZHONG,
        TAG_PENG,
        TAG_GANG,
        TAG_HU,
        TAG_QI,
        TAG_SURE,
		TAG_MAX
	};
    
    enum drawIndex
    {
        OTHER_HAND,
        SELF_HAND,
        MAX
    };
    
	static WZMJGameViewLayer *create(GameScene *pGameScene);
	
	virtual ~WZMJGameViewLayer();

	virtual bool init(); 
	virtual void onEnter();
	virtual void onExit();
    
	// default implements are used to call script callback if exist	
    virtual bool onTouchBegan(Touch *pTouch, Event *pEvent);
    virtual void onTouchMoved(Touch *pTouch, Event *pEvent);
    virtual void onTouchEnded(Touch *pTouch, Event *pEvent);

	// TextField 触发
	virtual bool onTextFieldAttachWithIME(TextFieldTTF*pSender);
	virtual bool onTextFieldDetachWithIME(TextFieldTTF*pSender);
	virtual bool onTextFieldInsertText(TextFieldTTF *pSender, const char *text, int nLen);
	virtual bool onTextFieldDeleteBackward(TextFieldTTF *pSender, const char *delText, int nLen);

	// IME 触发
	virtual void keyboardWillShow(IMEKeyboardNotificationInfo &info);
	virtual void keyboardDidShow(IMEKeyboardNotificationInfo &info);
	virtual void keyboardWillHide(IMEKeyboardNotificationInfo &info);
	virtual void keyboardDidHide(IMEKeyboardNotificationInfo &info);

	// 响应菜单快捷键
	virtual void backLoginView(Ref *pSender);

	string AddCommaToNum(LONG Num);

	//初始化
	void InitGame();
	void AddPlayerInfo();
	void AddButton();
	Menu* CreateButton(std::string szBtName ,const Vec2 &p , int tag);

	// 按钮事件管理
	void DialogConfirm(Ref *pSender);
	void DialogCancel(Ref *pSender);

	void callbackBt( Ref *pSender );

	void hiddenAll();

	virtual void GameEnd();

	//网络接口
public:
	void OnEventUserLeave( tagUserData * pUserData, WORD wChairID, bool bLookonUser );
	//用户进入
	virtual void OnEventUserEnter(tagUserData * pUserData, WORD wChairID, bool bLookonUser);
	//用户积分
	virtual void OnEventUserScore(tagUserData * pUserData, WORD wChairID, bool bLookonUser);
	//用户状态
	virtual void OnEventUserStatus(tagUserData * pUserData, WORD wChairID, bool bLookonUser);
    //用户换桌
    virtual void OnEventUserChangeTable(tagUserData * pUserData, WORD wChairID, bool bLookonUser){};
	//游戏消息
	virtual bool OnGameMessage(WORD wSubCmdID, const void * pBuffer, WORD wDataSize);
	//场景消息
	virtual bool OnGameSceneMessage(BYTE cbGameStatus, const void * pBuffer, WORD wDataSize);

	virtual void OnReconnectAction(){};

	void OnQuit();

protected:	
	WZMJGameViewLayer(GameScene *pGameScene);


public:
	static void reset();

	// 防止直接引用 
	WZMJGameViewLayer(const WZMJGameViewLayer&);
    WZMJGameViewLayer& operator = (const WZMJGameViewLayer&);


//发送网络消息

public:
	void SendGameStart();

	void SendOutCard(BYTE outCardData);

	void SendReqSendRecord();
    
    void caiShen(BYTE MagicCard);

    bool VerdictOutCard(BYTE cbPreCardData, BYTE cbAlreadyCardData[8], BYTE cbFengCardCount);

    int CheckOutCard(BYTE cbPreCardData, BYTE cbAlreadyCardData[8], BYTE cbFengCardCount);
    
    void NocanOUT(int Tag);
    
    //设置状态
    void SetControlInfo(BYTE cbAcitonMask);
    
    void showMenuItem(int iTag, bool bVisible);
    
    void setMenuItemBtnEnable(int iTag, bool bVisible);
private:
    void SetMaidiUI();
    
    int OnChip(BYTE wParam);
    
    void drawView(int tag);
    
    void playAnim(int touziCount);
    
    void AnimEnd();
    
    void PlayCardSound(WORD wChairID, BYTE cbCardData);
    
    void PlayActionSound(WORD wChairID, BYTE cbAction, BYTE cbSoundKind);

    int OnUserAction(BYTE wParam);
    
    BYTE GetSelectCardInfo(WORD wOperateCode, tagSelectCardInfo_WZMJ SelectInfo[MAX_COUNT_WZMJ]);
    
    void SetHandCardControl(BYTE cbCardIndex[MAX_INDEX], BYTE cbAdvanceCard);

    void showRefultView(WZMJ::CMD_S_GameEnd_WZMJ* pGameEnd);
    
    void setDifen();
    
    void setMaiDiInfo();
    
    void liuju(float dt);
protected:
    WZMJGameLogic       m_GameLogic;        //逻辑
    Sprite*             m_BackSpr;          //背景图片
    Menu*				m_BtnReadyPlay;		//开始按钮
    Menu*				m_BtnBuy;           //买底按钮
    Menu*				m_BtnDing;          //买底按钮
    Menu*				m_BtnNoBuy;         //不买按钮
    Menu*				m_BtnBackToLobby;	//返回按钮
    Menu*				m_BtnChiSCard;		//吃牌按钮
    Menu*				m_BtnChiZCard;      //吃牌按钮
    Menu*				m_BtnChiXCard;      //吃牌按钮
    Menu*				m_BtnPengCard;		//碰牌按钮
//    Menu*				m_BtnTingCard;      //听牌按钮
    Menu*				m_BtnHuCard;        //胡牌按钮
    Menu*				m_BtnGangCard;      //杠牌按钮
    Menu*				m_BtnQiCard;        //弃牌按钮
    Menu*				m_BtnSeting;		//设置按钮
    
    int					m_StartTime;
    Sprite*             m_ClockSpr;         //时间背景
    Sprite*             m_curBankSpr;       //庄
    Sprite*             m_pTips;            //提示文字
    Label*              m_lianzhuang;
    Label*              m_difenLabel;
    ui::Scale9Sprite*   m_resultBg;         //结算背景
    
    //位置
    Vec2				m_HeadPos[GAME_PLAYER_WZMJ];
    Vec2				m_ReadyPos[GAME_PLAYER_WZMJ];
    Vec2				m_ClockPos[GAME_PLAYER_WZMJ];
    
    //tag标志
    int					m_ReadyTga[GAME_PLAYER_WZMJ];
    int					m_HeadTga[GAME_PLAYER_WZMJ];
    int					m_GlodTga[GAME_PLAYER_WZMJ];
    int					m_NickNameTga[GAME_PLAYER_WZMJ];
    string              m_nickName[GAME_PLAYER_WZMJ];
    int                 m_AnimTga;
    int                 m_maidiTga[GAME_PLAYER_WZMJ];
    WORD                m_wBankerUser;
    WORD                m_wCurrentUser;
    WORD                m_wCurSendCardUser;
    WORD                m_wReplaceUser;
    UINT32              m_lSiceCount;
    BYTE                m_bMagicCardData;
    WORD                m_wMagicPos;
    BYTE                m_cbHeapMagic;
    int                 m_cbLeftCardCount;
    BYTE                m_cbActionMask;
    BYTE                m_cbActionCard;
    WORD                m_wHeapHead;
    WORD                m_wHeapTail;
    BYTE                m_cbLianZhuangCount;    //连庄数
    LONGLONG            m_lCellScore;           //底分
    bool                m_canOutCard;
    
    BYTE                            m_curCardData[GAME_PLAYER_WZMJ];
    BYTE                            m_bMaiDi[GAME_PLAYER_WZMJ];                 //买底数据
    BYTE                            m_cbHandCardIndex[MAX_INDEX];               //手上牌索引
    BYTE                            m_cbHandCardData[MAX_COUNT_WZMJ];           //手上牌数据
    BYTE                            m_cbHandCardCount[GAME_PLAYER_WZMJ];		//牌数目
    WZMJCardControl					m_HandCardControl[GAME_PLAYER_WZMJ];        //手上牌视图
    WZMJOutCardItem                 m_OutCardItem;
    int								m_SendCardCount;
    
    //跟风变量
    BYTE							m_cbFengCardCount;					//风牌个数
    BYTE							m_cbFengCardData[8];				//8个风牌
    //组合扑克
public:
    BYTE							m_cbWeaveCount[GAME_PLAYER_WZMJ];               //组合数目
    tagWeaveItem                    m_WeaveItemArray[GAME_PLAYER_WZMJ][MAX_WEAVE];	//组合扑克
    WZMJWeaveCard					m_WeaveCard;
    BYTE                            m_WeaveCardData[GAME_PLAYER_WZMJ][MAX_COUNT_WZMJ];
    //接收网络消息
protected:
	//游戏开始
	bool OnSubGameStart(const void * pBuffer, WORD wDataSize);
	//发牌
	bool OnSubSendCard(const void * pBuffer, WORD wDataSize);
    //出牌
    bool OnSubOutCard(const void * pBuffer, WORD wDataSize);
    //操作提示
    bool OnSubOperateNotify(const void * pBuffer, WORD wDataSize);
    //操作结果
    bool OnSubOperateResult(const void * pBuffer, WORD wDataSize);
    //买底消息
    bool OnSubChip(const void* pBuffer, WORD wDataSize);
	//结束
	bool OnSubGameEnd(const void * pBuffer, WORD wDataSize);

	void UpdateTime(float fp);

	void StopTime();

	void StartTime(int _time, int chair);
};

#endif
