//
//  WZMJCardControl.hpp
//  550game
//
//  Created by maxwell on 16/7/7.
//
//

#ifndef WZMJCardControl_hpp
#define WZMJCardControl_hpp

#include <stdio.h>
#include <vector>
#include "GameScene.h"
#include "WZMJGameLogic.h"
#include "math/CCGeometry.h"

#define CARD_MAXCOUNT   34

using namespace std;
//牌结构
struct tagMJCardItem
{
public:
	tagMJCardItem()
	{
		cardspr = NULL;
		bShoot = false;
		cbCardData = 0;
	}
	Sprite*                         cardspr;
	bool							bShoot;								//弹起标志
	BYTE							cbCardData;							//牌数据
};

//选择扑克信息
struct tagSelectCardInfo_WZMJ
{
	WORD							wActionMask;						//操作码
	BYTE							cbActionCard;						//操作牌
	BYTE							cbCardCount;						//选择数目
	BYTE							cbCardData[MAX_COUNT_WZMJ];			//选择牌
};

class WZMJCardControl
{
public:
	WZMJCardControl();
	~WZMJCardControl();
	virtual void SetLayer(GameLayer* _layer, int tga, int chair);

public:
	GameLayer*              m_layer;
	WZMJGameLogic           m_GameLogic;
	vector<tagMJCardItem>	m_CardData;
	tagMJCardItem           m_CurrentCard;
	int                     m_CardTga[20];
	int                     m_TopCardBackSprTga;
	WORD                     m_CardCount;
	int                     m_Chair;
	int                     m_OutCardTga;
	BYTE                    m_cbMagicCard;

	//扑克操作数据
	static BYTE             g_cbCardData[CARD_MAXCOUNT];
	static BYTE             g_cbMaskValue;                  //牌值掩码
	static BYTE             g_cbMaskColor;                  //花色掩码
	static BYTE             g_cbCardBack;                   //空牌值
	static float            g_fCardSpace;                   //扑克间隔

	BYTE							m_cbSelectInfoCount;                    //选择扑克组合数目
	tagSelectCardInfo_WZMJ			m_SelectCardInfo[MAX_COUNT_WZMJ];		//选择扑克组合信息
	BYTE                            m_cbCurSelectIndex;

	//扑克数据
	static BYTE GetCardValue(BYTE cbCardData) { return cbCardData&g_cbMaskValue; }
	static BYTE GetCardColor(BYTE cbCardData) { return cbCardData&g_cbMaskColor; }
public:
	//玩家吃牌
	bool OnEventUserAction(const tagSelectCardInfo_WZMJ SelectInfo[MAX_COUNT_WZMJ], BYTE cbInfoCount);
	//获取玩家操作结果
	void GetUserSelectResult(tagSelectCardInfo_WZMJ &SelectInfo);

	//设置扑克
	void SetCardData(const BYTE cbCardData[], WORD wCardCount, BYTE cbCurrentCard);
	//设置财神
	void SetMagicCard(const BYTE cbMagicCard) { m_cbMagicCard = cbMagicCard; }
	//是否财神
	bool IsMagicCard(const BYTE cbMagicCard);

	void SetTopCardData(WORD dwCardCount, BYTE cbCurrentCard);
	void SetMyCardData(const BYTE bCardData[], WORD dwCardCount, BYTE cbCurrentCard);

	BYTE GetShootCard(BYTE cbCardData);

	void ClearShowOutCard();
	WORD GetCardData(BYTE cbCardData[], WORD wBufferCount);
	void ClearCardData();
	int GetCardCount();
	Sprite* GetCardSprite(BYTE card, bool isStandCard = true);

	BYTE Box2d(Vec2 localPos);
	bool SetShootCard(BYTE cbCardData);
	bool RemoveShootItem();
};


class WZMJOutCardItem : public WZMJCardControl
{
public:
	WZMJOutCardItem();
	~WZMJOutCardItem();
	void SetLayer(Layer* _layer, int tga);

	void SetTopCardData(const BYTE bCardData);
	void SetMyCardData(const BYTE bCardData);
	void removeMyLastCard();
	void removeOtherLastCard();
	void clearAllDataAndCard();
protected:
	vector<tagMJCardItem>	m_MyCardData;
	vector<tagMJCardItem>	m_OhterCardData;

protected:
	int m_MyOutTag[MAX_INDEX];
	int m_OtherOutTag[MAX_INDEX];
    Layer               *m_layer;
};

class WZMJWeaveCard : public WZMJCardControl
{
public:
	WZMJWeaveCard();
	~WZMJWeaveCard();

	void SetLayer(Layer *_layer, int tga);

	void SetTopCardData(BYTE cardData[], bool publicCard);
	void SetMyCardData(BYTE cardData[], bool publicCard);
	void setControlPoint(const BYTE Chair);

	bool SetCardData(tagWeaveItem tagWeave, BYTE chair);

	void clearDataAndCard();
    void clearMyDataAndCard();
    void clearOtherDataAndCard();

protected:
    Layer               *m_layer;
    int                 m_MyWeaveTag[MAX_INDEX];
	int                 m_OtherWeaveTag[MAX_INDEX];
	int                 m_ControlPoint[GAME_PLAYER_WZMJ];						//基准位置
};
#endif /* WZMJCardControl_hpp */
