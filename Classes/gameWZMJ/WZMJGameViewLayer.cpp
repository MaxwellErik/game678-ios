﻿#include "WZMJGameViewLayer.h"
#include "LobbyLayer.h"
#include "ClientSocketSink.h"
#include "HXmlParse.h"
#include "LocalDataUtil.h"
#include "JniSink.h"
#include "VisibleRect.h"
#include "HttpConstant.h"
#include "Screen.h"
#include "LobbySocketSink.h"
#include "LoginLayer.h"
#include "GameLayerMove.h"
#include "CMD_WZMJ.h"
#include "Convert.h"

USING_NS_CC;
using namespace WZMJ;

#define INDEX_NOBUY   1
#define INDEX_BUY     2
#define WIDTH Director::getInstance()->getVisibleSize().width
#define HEIGHT Director::getInstance()->getVisibleSize().height

WZMJGameViewLayer::WZMJGameViewLayer(GameScene *pGameScene)
:IGameView(pGameScene)
{
    m_GameState=enGameNormal;
    m_BtnReadyPlay = nullptr;		//开始按钮
    m_BtnBuy = nullptr;           //买底按钮
    m_BtnNoBuy = nullptr;         //不买按钮
    m_BtnBackToLobby = nullptr;	//返回按钮
    m_BtnChiSCard = nullptr;		//吃牌按钮
    m_BtnChiZCard = nullptr;      //吃牌按钮
    m_BtnChiXCard = nullptr;      //吃牌按钮
    m_BtnPengCard = nullptr;		//碰牌按钮
    m_BtnHuCard = nullptr;        //胡牌按钮
    m_BtnGangCard = nullptr;      //杠牌按钮
    m_BtnQiCard = nullptr;        //弃牌按钮
    m_BtnSeting = nullptr;		//设置按钮
    m_difenLabel = nullptr;
    m_lianzhuang = nullptr;
    
    m_StartTime = 0;
    m_wBankerUser = 0;
    m_wCurrentUser = 0;
    m_wCurSendCardUser = -1;
    m_wReplaceUser = 0;
    m_lSiceCount = 0;
    m_bMagicCardData = 0;
    m_wMagicPos = 0;
    m_cbHeapMagic = 0;
    m_cbLeftCardCount = 0;
    m_cbActionMask = 0;
    m_wHeapHead = 0;
    m_wHeapTail = 0;
    m_cbLianZhuangCount = 0;
    m_lCellScore = 0;
    m_curCardData[0] = 0;
    m_curCardData[1] = 0;
  
    m_ClockSpr = nullptr;
    m_curBankSpr = nullptr;
    m_pTips = nullptr;
    m_resultBg = nullptr;
    SetKindId(WZMJ_KIND_ID);
}

WZMJGameViewLayer::~WZMJGameViewLayer()
{
}

WZMJGameViewLayer *WZMJGameViewLayer::create(GameScene *pGameScene)
{
    WZMJGameViewLayer *pLayer = new WZMJGameViewLayer(pGameScene);
    if(pLayer && pLayer->init())
    {
        pLayer->autorelease();
        return pLayer;
    }
    else
    {
        CC_SAFE_DELETE(pLayer);
        return NULL;
    }
}

bool WZMJGameViewLayer::init()
{
    if ( !Layer::init() )
    {
        return false;
    }
    setLocalZOrder(3);
    
    Tools::addSpriteFrame("WZMJ/WZMJ_img.plist");

    SoundUtil::sharedEngine()->playBackMusic("WZMJ/sound/BACK_GROUND", true);
    SoundUtil::sharedEngine()->setBackSoundVolume(g_GlobalUnits.m_fBackMusicValue);
    SoundUtil::sharedEngine()->setSoundVolume(g_GlobalUnits.m_fSoundValue);
    
    IGameView::onEnter();
    JniSink::share()->setIGameView(this);
    setGameStatus(GS_FREE);
    
    InitGame();
    AddButton();
    
    AddPlayerInfo();
    setTouchEnabled(true);
    
    auto listener = EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(true);
    listener->onTouchBegan = CC_CALLBACK_2(WZMJGameViewLayer::onTouchBegan, this);
    listener->onTouchMoved = CC_CALLBACK_2(WZMJGameViewLayer::onTouchMoved, this);
    listener->onTouchEnded = CC_CALLBACK_2(WZMJGameViewLayer::onTouchEnded, this);
    _eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);
    return true;
}

void WZMJGameViewLayer::onEnter()
{
}

void WZMJGameViewLayer::StartTime(int _time, int chair)
{
    if (chair == GetMeChairID() && _time == TIME_OUT_CARD)
    {
        NocanOUT(0);
    }
    m_StartTime = _time;
    schedule(schedule_selector(WZMJGameViewLayer::UpdateTime), 1);
    m_ClockSpr->setVisible(true);
    m_ClockSpr->setPosition(m_ClockPos[g_GlobalUnits.SwitchViewChairID(chair)]);
    
    UpdateTime(m_StartTime);
}

void WZMJGameViewLayer::StopTime()
{
    unschedule(schedule_selector(WZMJGameViewLayer::UpdateTime));
    m_ClockSpr->removeChildByTag(100);
    m_ClockSpr->removeChildByTag(101);
    m_ClockSpr->setVisible(false);
}

void WZMJGameViewLayer::UpdateTime(float fp)
{
    if (m_StartTime < 0)
    {
        unschedule(schedule_selector(WZMJGameViewLayer::UpdateTime));
        if (m_GameState == enGameCallBank) //买底状态
             OnChip(INDEX_NOBUY);
        else if (m_GameState == enGameNormal || m_GameState == enGameEnd)
        {
            IGameView::backLoginView(nullptr);
        }
        return;
    }
    
    if (m_StartTime < 3)
    {
        SoundUtil::sharedEngine()->playEffect("WZMJ/sound/GAME_WARN");
    }
    
    m_ClockSpr->removeChildByTag(100);
    std::string str = StringUtils::toString(m_StartTime);
    if (m_StartTime < 10)
        str = "0" + str;
    LabelAtlas *time = LabelAtlas::create(str, "Common/time_1.png", 23, 35, '+');
    time->setPosition(Vec2(20, 25));
    m_ClockSpr->addChild(time, 10 ,100);
    
    m_StartTime--;
    
}

void WZMJGameViewLayer::onExit()
{
    Tools::removeSpriteFrameCache("WZMJ/WZMJ_img.plist");
    IGameView::onExit();
}

// Touch 触发
bool WZMJGameViewLayer::onTouchBegan(Touch *pTouch, Event *pEvent)
{
    //操作状态
    if (m_GameState == enGameBet || m_GameState == enGameEnd || m_GameState == enGameNormal)
        return true;
    
    if (!m_canOutCard)
        return true;
    
    BYTE cbOutCardData = m_HandCardControl[1].Box2d(convertToNodeSpace(pTouch->getLocation()));
    if (cbOutCardData != 0)
    {
        if (m_wCurrentUser != GetMeChairID())
            return true;
        
        if (VerdictOutCard(cbOutCardData, m_cbFengCardData, m_cbFengCardCount) == false)
        {
            return true;
        }
        
        BYTE	cbCardIndex[MAX_INDEX];			//手中扑克
        ZeroMemory(cbCardIndex, sizeof(cbCardIndex));
        memcpy(cbCardIndex, m_cbHandCardIndex, sizeof(m_cbHandCardIndex));
        if (!m_GameLogic.RemoveCard(cbCardIndex, cbOutCardData))
        {
            return true;
        }
        
        //发送数据
        CMD_C_OutCard_WZMJ OutCard;
        OutCard.cbCardData = cbOutCardData;
        SendData(SUB_C_OUT_CARD_WZMJ, &OutCard, sizeof(OutCard));
        NocanOUT(-1);
        m_wCurSendCardUser = -1;
        m_canOutCard = false;
        
        SetControlInfo(WIK_NULL);
    }
    return true;
}

void WZMJGameViewLayer::onTouchMoved(Touch *pTouch, Event *pEvent)
{
}

void WZMJGameViewLayer::onTouchEnded(Touch*pTouch, Event*pEvent)
{
}

//播放出牌声音
void WZMJGameViewLayer::PlayCardSound(WORD wChairID, BYTE cbCardData)
{
    if (m_GameLogic.IsValidCard(cbCardData) == false)
    {
        return;
    }
    
    //判断性别
    int wtable = ClientSocketSink::sharedSocketSink()->GetMeUserData()->wTableID;
    if (wtable > 200) return;
    const tagUserData* pUserData = ClientSocketSink::sharedSocketSink()->m_UserManager.GetUserData(wtable,wChairID);
    
    if (pUserData == NULL)
    {
        assert(0 && "得不到玩家信息");
        return;
    }
    bool bBoy = (pUserData->wGender == 1 ? true : false);
    
    int cbType = (cbCardData & MASK_COLOR);
    int cbValue = (cbCardData & MASK_VALUE);
    log("--cbType = %d, cbValue = %d", cbType, cbValue);
    
    string strSoundName = "WZMJ/sound/";
    
    if (bBoy)
    {
        strSoundName.append("man/");
    }
    else
    {
        strSoundName.append("woman/");
    }
    switch (cbType)
    {
        case 0X30:	//风
        {
            switch (cbValue)
            {
                case 1:
                {
                    strSoundName.append("F_1");
                    break;
                }
                case 2:
                {
                    strSoundName.append("F_2");
                    break;
                }
                case 3:
                {
                    strSoundName.append("F_3");
                    break;
                }
                case 4:
                {
                    strSoundName.append("F_4");
                    break;
                }
                case 5:
                {
                    strSoundName.append("F_5");
                    break;
                }
                case 6:
                {
                    strSoundName.append("F_6");
                    break;
                }
                case 7:
                {
                    //如果白板不是财神
                    if (0x37 != m_bMagicCardData)
                    {
                        //BYTE bdata = m_GameLogic.SwitchToCardData(m_bMagicIndex);
                        PlayCardSound(wChairID, m_bMagicCardData);
                        
                    }
                    else
                    {
                        strSoundName.append("F_7");
                    }
                    
                    break;
                }
                default:
                {
                    strSoundName.append("BU_HUA");
                }
                    
            }
            break;
        }
        case 0X20:	//筒
        {
            stringstream ss;
            ss<<cbValue;
            
            strSoundName.append("T_");
            strSoundName.append(ss.str());
            break;
        }
            
        case 0X10:	//索
        {
            stringstream ss;
            ss<<cbValue;
            
            strSoundName.append("S_");
            strSoundName.append(ss.str());
            break;
        }
        case 0X00:	//万
        {
            stringstream ss;
            ss<<cbValue;
            
            strSoundName.append("W_");
            strSoundName.append(ss.str());
            break;
        }
    }
    
    SoundUtil::sharedEngine()->playEffect((char*)strSoundName.c_str());
}

void WZMJGameViewLayer::PlayActionSound(WORD wChairID, BYTE cbAction, BYTE cbSoundKind)
{
    //判断性别
    const tagUserData* pUserData = GetUserData(wChairID);
    if (pUserData == NULL)
    {
        assert(0 && "得不到玩家信息");
        return;
    }
    bool bBoy = (pUserData->wGender == 1 ? true : false);
    switch (cbAction)
    {
        case WIK_LEFT:
        case WIK_CENTER:
        case WIK_RIGHT:
        {
            if (bBoy)
            {
                SoundUtil::sharedEngine()->playEffect("WZMJ/sound/man/CHI");
            }
            else
            {
                SoundUtil::sharedEngine()->playEffect("WZMJ/sound/woman/CHI");
            }
            
            break;
        }
        case WIK_PENG:
        {
            if (bBoy)
            {
                SoundUtil::sharedEngine()->playEffect("WZMJ/sound/man/PENG");
            }
            else
            {
                SoundUtil::sharedEngine()->playEffect("WZMJ/sound/woman/PENG");
            }
            break;
        }
        case WIK_GANG:
        {
            if (bBoy)
            {
                SoundUtil::sharedEngine()->playEffect("WZMJ/sound/man/GANG");
            }
            else
            {
                SoundUtil::sharedEngine()->playEffect("WZMJ/sound/woman/GANG");
            }
            break;
        }
        case WIK_CHI_HU:
        {
            if (cbSoundKind == 1)
            {
                if (bBoy)
                {
                    SoundUtil::sharedEngine()->playEffect("WZMJ/sound/man/CHI_HU_DA");
                }
                else
                {
                    SoundUtil::sharedEngine()->playEffect("WZMJ/sound/woman/CHI_HU_DA");
                }
            }
            else if (cbSoundKind == 2)
            {
                if (bBoy)
                {
                    SoundUtil::sharedEngine()->playEffect("WZMJ/sound/man/CHI_HU");
                }
                else
                {
                    SoundUtil::sharedEngine()->playEffect("WZMJ/sound/woman/CHI_HU");
                }
            }
            else
            {
                if (bBoy)
                {
                    SoundUtil::sharedEngine()->playEffect("WZMJ/sound/man/CHI_HU_RUAN");
                }
                else
                {
                    SoundUtil::sharedEngine()->playEffect("WZMJ/sound/woman/CHI_HU_RUAN");
                }
            }
            break;
        }
    }
    
    return;
}

// TextField 触发
bool WZMJGameViewLayer::onTextFieldAttachWithIME(TextFieldTTF *pSender)
{
    return false;
}

bool WZMJGameViewLayer::onTextFieldDetachWithIME(TextFieldTTF *pSender)
{
    return false;
}

bool WZMJGameViewLayer::onTextFieldInsertText(TextFieldTTF *pSender, const char *text, int nLen)
{
    return true;
}

bool WZMJGameViewLayer::onTextFieldDeleteBackward(TextFieldTTF *pSender, const char *delText, int nLen)
{
    return true;
}

// IME 触发
void WZMJGameViewLayer::keyboardWillShow(IMEKeyboardNotificationInfo& info)
{
}

void WZMJGameViewLayer::keyboardDidShow(IMEKeyboardNotificationInfo& info)
{
}

void WZMJGameViewLayer::keyboardWillHide(IMEKeyboardNotificationInfo& info)
{
    MoveTo *pMovAction = MoveTo::create(0.15f, Vec2::ZERO);
    Action *pActSeq = Sequence::create(pMovAction, NULL);
    runAction(pActSeq);
}

void WZMJGameViewLayer::keyboardDidHide(IMEKeyboardNotificationInfo& info)
{
}

// 按钮事件管理
// Alert Message 确认消息处理
void WZMJGameViewLayer::DialogConfirm(Ref *pSender)
{
    this->RemoveAlertMessageLayer();
    
    // 设定 触发
    this->SetAllTouchEnabled(true);
}

// Alert Message 取消消息处理
void WZMJGameViewLayer::DialogCancel(Ref *pSender)
{
    // 清除 Alert Message Layer
    this->RemoveAlertMessageLayer();
    
    // 设定 触发
    this->SetAllTouchEnabled(true);
}

void WZMJGameViewLayer::OnEventUserScore( tagUserData * pUserData, WORD wChairID, bool bLookonUser )
{
    IGameView::OnEventUserScore(pUserData, wChairID, bLookonUser);
    AddPlayerInfo();
}

void WZMJGameViewLayer::OnEventUserStatus(tagUserData * pUserData, WORD wChairID, bool bLookonUser)
{
    IGameView::OnEventUserStatus(pUserData, wChairID, bLookonUser);
    AddPlayerInfo();
}

void WZMJGameViewLayer::OnEventUserEnter( tagUserData * pUserData, WORD wChairID, bool bLookonUser )
{
    AddPlayerInfo();
}

void WZMJGameViewLayer::OnEventUserLeave( tagUserData * pUserData, WORD wChairID, bool bLookonUser )
{
    AddPlayerInfo();
}

//游戏消息
bool WZMJGameViewLayer::OnGameMessage(WORD wSubCmdID, const void * pBuffer, WORD wDataSize)
{
    CCLOG("RECEIVE MESSAGE CMD[%d]" , wSubCmdID);
    switch (wSubCmdID)
    {
        case WZMJ_SUB_S_GAME_START:         //游戏开始
            return OnSubGameStart(pBuffer, wDataSize);
            
        case WZMJ_SUB_S_OUT_CARD:           //出牌命令
            return OnSubOutCard(pBuffer, wDataSize);
            
        case WZMJ_SUB_S_SEND_CARD:			//发牌命令
            return OnSubSendCard(pBuffer, wDataSize);
            
        case WZMJ_SUB_S_OPERATE_NOTIFY:		//操作提示
            return OnSubOperateNotify(pBuffer, wDataSize);
            
        case WZMJ_SUB_S_OPERATE_RESULT:		//操作命令
            return OnSubOperateResult(pBuffer, wDataSize);
            
        case WZMJ_SUB_S_GAME_END:			//游戏结束
            return OnSubGameEnd(pBuffer, wDataSize);
            
        case WZMJ_SUB_S_CHIP:               //买底
            return OnSubChip(pBuffer, wDataSize);
            
        default:
            log("---->> invalid cmd id: %d", wSubCmdID);
            break;
    }
    return true;
}

//场景消息
bool WZMJGameViewLayer::OnGameSceneMessage(BYTE cbGameStatus, const void * pBuffer, WORD wDataSize)
{
    setGameStatus(cbGameStatus);
    
    switch (getGameStatus())
    {
        case GAME_SCENE_FREE:
        {
            if (wDataSize!=sizeof(CMD_S_StatusFree_WZMJ)) return false;
            CMD_S_StatusFree_WZMJ * pStatusFree=(CMD_S_StatusFree_WZMJ *)pBuffer;
            
            if (m_BtnReadyPlay != NULL)	m_BtnReadyPlay->setVisible(true);
            
            m_BtnReadyPlay->setVisible(true);
            //时间定义
            m_wBankerUser=pStatusFree->wBankerUser;
            m_lCellScore=pStatusFree->lCellScore;
            
            StartTime(15, GetMeChairID());
            return true;
        }
        case GAME_SCENE_CHIP:
        {
            if (wDataSize!=sizeof(CMD_S_StatusChip_WZMJ)) return false;
            CMD_S_StatusChip_WZMJ * pStatusChip=(CMD_S_StatusChip_WZMJ *)pBuffer;
            
            m_wBankerUser = pStatusChip->wBankerUser;
            
            //玩家设置
            if (!pStatusChip->bComplete)
            {
                if (m_wBankerUser == GetMeChairID())
                    m_BtnBuy->setVisible(true);
                else
                    m_BtnDing->setVisible(true);
                
                m_BtnNoBuy->setVisible(true);
            }
            
            //设置状态
            setGameStatus(GAME_SCENE_CHIP);
            m_GameState=enGameCallBank;//买底状态
            
            //设置定时器
            StartTime(TIME_CHIP, GetMeChairID());
            
            //设置界面
            SetMaidiUI();
            return true;
        }
        case GAME_SCENE_PLAY:
        {
            if (wDataSize!=sizeof(CMD_S_StatusPlay_WZMJ)) return false;
            CMD_S_StatusPlay_WZMJ *pStatusPlay = (CMD_S_StatusPlay_WZMJ *)pBuffer;
            
            //清除数据
            for (int i = 0; i < GAME_PLAYER_WZMJ; i++)
            {
                m_HandCardControl[i].ClearShowOutCard();
                m_HandCardControl[i].ClearCardData();
            }
            
            m_OutCardItem.clearAllDataAndCard();
            m_WeaveCard.clearDataAndCard();
            auto caishen = this->getChildByTag(99);
            if (caishen)
                caishen->removeFromParent();
            
            m_cbFengCardCount = 0;
            ZeroMemory(m_cbWeaveCount, sizeof(m_cbWeaveCount));
            ZeroMemory(m_WeaveItemArray, sizeof(m_WeaveItemArray));
            ZeroMemory(m_cbFengCardData, sizeof(m_cbFengCardData));
            
            m_wBankerUser = pStatusPlay->wBankerUser;
            m_wCurrentUser = pStatusPlay->wCurrentUser;
            m_cbActionMask = pStatusPlay->cbActionMask;
            m_bMagicCardData = pStatusPlay->cbKingCardData;
            m_lCellScore = pStatusPlay->lCellScore;
            m_cbLeftCardCount = pStatusPlay->cbLeftCardCount;
            m_cbLianZhuangCount = pStatusPlay->cbBankContinueCount;
            
            m_canOutCard = true;
            setDifen();

            //设置财神数据
            m_GameLogic.SetKingCardData(m_bMagicCardData);
            //设置桌面财神牌
            caiShen(m_bMagicCardData);
            
            //设置手里财神
            m_HandCardControl[1].SetMagicCard(m_bMagicCardData);
            m_WeaveCard.SetMagicCard(m_bMagicCardData);
            m_OutCardItem.SetMagicCard(m_bMagicCardData);
            
            //设置游戏状态
            setGameStatus(GAME_SCENE_PLAY);
            m_GameState = enGameSendCard;
            
            //设置倒计时
            StartTime(TIME_OUT_CARD, m_wBankerUser);
            
            //操作牌
            CopyMemory(m_cbWeaveCount, pStatusPlay->cbWeaveCount, sizeof(m_cbWeaveCount));
            CopyMemory(m_WeaveItemArray, pStatusPlay->WeaveItemArray, sizeof(m_WeaveItemArray));
            for (BYTE i = 0; i < GAME_PLAYER_WZMJ; i++)
            {
                for (BYTE j = 0; j < m_cbWeaveCount[i]; j++)
                {
                    WORD VIEWID = g_GlobalUnits.SwitchViewChairID(i);
                    m_WeaveCard.setControlPoint(VIEWID);
                    m_WeaveCard.SetCardData(m_WeaveItemArray[i][j], VIEWID);
                }
            }
            
            //手里牌
            m_cbHandCardCount[0] = m_wCurrentUser == GetMeChairID() ?  MAX_COUNT_WZMJ - 1: MAX_COUNT_WZMJ;
            m_cbHandCardCount[0] -= m_cbWeaveCount[1-GetMeChairID()]*3;
            m_cbHandCardCount[1] = pStatusPlay->cbCardCount;
            m_GameLogic.SwitchToCardIndex(pStatusPlay->cbCardData, m_cbHandCardCount[1], m_cbHandCardIndex);
            
            if (m_wCurrentUser == GetMeChairID())
            {
                m_curCardData[0] = 0;
                m_curCardData[1] = pStatusPlay->cbSendCardData;
                drawView(SELF_HAND);
                m_HandCardControl[0].SetTopCardData(m_cbHandCardCount[0], 0);
            }
            else
            {
                m_curCardData[0] = 1;
                m_curCardData[1] = 0;
                drawView(OTHER_HAND);
                drawView(SELF_HAND);
            }
            
            //出牌
            for (BYTE i = 0; i < GAME_PLAYER_WZMJ; i++)
            {
                for (int j = 0; j < pStatusPlay->cbDiscardCount[i]; j++)
                {
                    if (i == GetMeChairID())
                        m_OutCardItem.SetMyCardData(pStatusPlay->cbDiscardCard[i][j]);
                    else
                        m_OutCardItem.SetTopCardData(pStatusPlay->cbDiscardCard[i][j]);
                }
            }
            
            for (int i = 0; i < GAME_PLAYER_WZMJ; i++)
                m_bMaiDi[i] = pStatusPlay->cbChip[i];
            
            SetMaidiUI();
            setMaiDiInfo();
            return true;
        }
    }
    return true;
}

//发送用户准备
void WZMJGameViewLayer::SendGameStart()
{
    for (int i = 0; i < GAME_PLAYER_WZMJ; i++)
    {
        m_HandCardControl[i].ClearShowOutCard();
        m_HandCardControl[i].ClearCardData();
        removeAllChildByTag(m_maidiTga[i]);
    }
    m_OutCardItem.clearAllDataAndCard();
    m_WeaveCard.clearDataAndCard();
    auto caishen = this->getChildByTag(99);
    if (caishen)
        caishen->removeFromParent();
    
    ClientSocketSink::sharedSocketSink()->SendData(MDM_GF_FRAME, SUB_GF_USER_READY);
    if (m_BtnReadyPlay != NULL)
    {
        m_BtnReadyPlay->setVisible(false);
    }
    m_cbFengCardCount = 0;
    ZeroMemory(m_cbWeaveCount, sizeof(m_cbWeaveCount));
    ZeroMemory(m_WeaveItemArray, sizeof(m_WeaveItemArray));
    ZeroMemory(m_cbFengCardData, sizeof(m_cbFengCardData));
    StopTime();
    
    if (m_lianzhuang != nullptr)
    {
        m_lianzhuang->removeFromParent();
        m_lianzhuang = nullptr;
    }
    
    if (m_difenLabel != nullptr)
    {
        m_difenLabel->removeFromParent();
        m_difenLabel = nullptr;
    }
    
    SoundUtil::sharedEngine()->playEffect("WZMJ/sound/game_READY");
}

//发送出牌消息
void WZMJGameViewLayer::SendOutCard(BYTE outCardData)
{
    //发送数据
    CMD_C_OutCard_WZMJ OutCard;
    OutCard.cbCardData = outCardData;
    SendData(SUB_C_OUT_CARD_WZMJ, &OutCard, sizeof(OutCard));
}

void WZMJGameViewLayer::AnimEnd()
{
    //删除骰子
    removeAllChildByTag(m_AnimTga);
    
    //设置桌面财神牌
    caiShen(m_bMagicCardData);
    
    //设置手里财神
    m_HandCardControl[1].SetMagicCard(m_bMagicCardData);
    m_WeaveCard.SetMagicCard(m_bMagicCardData);
    m_OutCardItem.SetMagicCard(m_bMagicCardData);
    
    //设置游戏状态
    setGameStatus(GAME_SCENE_PLAY);
    m_GameState = enGameSendCard;
    
    //设置倒计时
    StartTime(TIME_OUT_CARD, m_wBankerUser);
    
    drawView(OTHER_HAND);
    drawView(SELF_HAND);
    
    //变量定义
    tagGangCardResult GangCardResult;
    ZeroMemory(&GangCardResult,sizeof(GangCardResult));

    //杠牌判断
    if (m_cbActionMask != WIK_NULL)
    {
        if ((m_cbActionMask & WIK_GANG) != 0)
        {
            SetControlInfo(WIK_GANG);
        }
        else if ((m_cbActionMask & WIK_CHI_HU) != 0)
        {
             SetControlInfo(WIK_CHI_HU);
        }
    }
    
    else if (m_wBankerUser == GetMeChairID())
    {
          NocanOUT(0);
    }
    SoundUtil::sharedEngine()->playEffect("WZMJ/sound/GET_CARD");
}

void WZMJGameViewLayer::playAnim(int touziCount)
{
    int touzi[] = {touziCount & 0x00FF , (touziCount & 0xFF00) >> 8};
 
    for (int loop = 0; loop < 2; loop++)
    {
        Vector<AnimationFrame*> arrayOfAnimationFrameNames;
        for(int i = 1; i < 7; ++i)
        {
            int point = touzi[loop] + i;
            char buffer[50]= {0};
            sprintf(buffer, "touzi_%d.png", point % 6 + 1);
            
            SpriteFrame *pSpriteFrame = SpriteFrameCache::getInstance()->getSpriteFrameByName(buffer);
            ValueMap userInfo;
            AnimationFrame *pAnimationFrame = AnimationFrame::create(pSpriteFrame, 0.1f, userInfo);
            arrayOfAnimationFrameNames.pushBack(pAnimationFrame);
        }
        //生成动画数据对象
        Animation *pAnimation = Animation::create(arrayOfAnimationFrameNames, 0.1f);
        //生成动画动作对象
        Animate* animate = Animate::create(pAnimation);
        
        Sprite* pSprite = Sprite::create();
        addChild(pSprite,4,m_AnimTga);
        pSprite->setPosition(Vec2(200,0));
        ActionInstant *func = CallFunc::create(CC_CALLBACK_0(WZMJGameViewLayer::AnimEnd, this));
        MoveTo *moveTo = MoveTo::create(0.6f, Vec2(550 + 60 * loop, 300 - 30 * loop));
        JumpTo *jumpTo = JumpTo::create(0.6f, Vec2(550 + 60 * loop, 300 - 30 * loop), 50, 5);
        Spawn *spawn = Spawn::create(moveTo, jumpTo, animate, nullptr);
        DelayTime *delay = DelayTime::create(2.0f);
        pSprite->runAction(Sequence::create(spawn, delay, func, NULL));
    }
}

void WZMJGameViewLayer::setMaiDiInfo()
{
    for (int i = 0; i < GAME_PLAYER_WZMJ; i++)
    {
        if (m_bMaiDi[i] == INDEX_BUY)
        {
            Sprite* maidi;
            if (m_wBankerUser == i)
            {
                maidi = Sprite::createWithSpriteFrameName("maidi.png");
            }
            else
            {
                maidi = Sprite::createWithSpriteFrameName("dingdi.png");
            }
            
            if (i == GetMeChairID())
            {
                maidi->setPosition(Vec2(150, 320));
            }
            else
            {
                maidi->setPosition(Vec2(150, 830));
            }
            maidi->setTag(m_maidiTga[i]);
            addChild(maidi);
        }
    }
}

void WZMJGameViewLayer::setDifen()
{
    char str[32];
    sprintf(str, "连庄: %d", m_cbLianZhuangCount);
    
    m_lianzhuang = Label::createWithSystemFont(str, _GAME_FONT_NAME_2_, 46);
    m_lianzhuang->setAnchorPoint(Vec2(0, 0.5f));
    m_lianzhuang->setPosition(Vec2(700, 1040));
    addChild(m_lianzhuang);
    
    sprintf(str, "底数: %lld", m_lCellScore * m_cbLianZhuangCount);
    m_difenLabel = Label::createWithSystemFont(str, _GAME_FONT_NAME_2_, 46);
    m_difenLabel->setAnchorPoint(Vec2(0, 0.5f));
    m_difenLabel->setPosition(Vec2(1000, 1040));
    addChild(m_difenLabel);
}

bool WZMJGameViewLayer::OnSubGameStart( const void * pBuffer, WORD wDataSize )
{
    if (wDataSize != sizeof(CMD_S_GameStart_WZMJ))
        return false;
    
    //变量定义
    CMD_S_GameStart_WZMJ * pGameStart = (CMD_S_GameStart_WZMJ *)pBuffer;
    m_wBankerUser = pGameStart->wBankerUser;
    m_wCurrentUser = pGameStart->wCurrentUser;
    m_cbActionMask = pGameStart->cbUserAction;
    m_bMagicCardData = pGameStart->cbKingCardData;
    m_lCellScore = pGameStart->basescore;
    m_cbLianZhuangCount = pGameStart->cbBankContinueCount;
    m_cbLeftCardCount = MAX_REPERTORY - GAME_PLAYER_WZMJ*(MAX_COUNT_WZMJ - 1) - 1;
    
    for (int i = 0; i < GAME_PLAYER_WZMJ; i++)
    {
        m_bMaiDi[i] = pGameStart->cbChip[i];
    }
    
    setMaiDiInfo();
    setDifen();
    
    //环境处理
    SoundUtil::sharedEngine()->playEffect("WZMJ/sound/GAME_START");
    SoundUtil::sharedEngine()->playEffect("WZMJ/sound/DRAW_SICE");

    
    //设置扑克(庄家多抓一张牌)
    //设置财神
    m_GameLogic.SetKingCardData(m_bMagicCardData);
    
    if (GetMeChairID() == m_wBankerUser)
    {
        m_cbHandCardCount[0] =  MAX_COUNT_WZMJ - 1;
        m_cbHandCardCount[1] =  MAX_COUNT_WZMJ;
        
        m_curCardData[0] = 0;
        m_curCardData[1] = pGameStart->cbCardData[m_cbHandCardCount[1] - 1];
    }
    else
    {
        m_cbHandCardCount[0] =  MAX_COUNT_WZMJ;
        m_cbHandCardCount[1] =  MAX_COUNT_WZMJ - 1;
        m_curCardData[0] = 1;
        m_curCardData[1] = 0;
    }
    
    ZeroMemory(m_cbHandCardIndex, sizeof(m_cbHandCardIndex));
    m_GameLogic.SwitchToCardIndex(pGameStart->cbCardData, m_cbHandCardCount[1], m_cbHandCardIndex);
    
    //骰子动画
    playAnim(pGameStart->wSiceCount);
    m_canOutCard = true;
    return true;
}

//操作提示
bool WZMJGameViewLayer::OnSubOperateNotify(const void * pBuffer, WORD wDataSize)
{
    //效验数据
    ASSERT(wDataSize == sizeof(CMD_S_OperateNotify_WZMJ));
    if (wDataSize != sizeof(CMD_S_OperateNotify_WZMJ))
        return false;
    
    //变量定义
    CMD_S_OperateNotify_WZMJ * pOperateNotify = (CMD_S_OperateNotify_WZMJ *)pBuffer;
    //用户界面
    if (pOperateNotify->cbActionMask != WIK_NULL)
    {
        //获取变量
        WORD wMeChairID = GetMeChairID();
        if (wMeChairID == pOperateNotify->wResumeUser)
        {
            m_cbActionMask = pOperateNotify->cbActionMask;
            m_cbActionCard = pOperateNotify->cbActionCard;
            
            //设置界面
            SetControlInfo(m_cbActionMask);
        }
        
        //设置时间
        StartTime(TIME_ACTION_CARD, pOperateNotify->wResumeUser);
        m_wCurrentUser = pOperateNotify->wResumeUser;
        m_GameState = enGameBet;
    }
    
    return true;
}

//操作结果
bool WZMJGameViewLayer::OnSubOperateResult(const void * pBuffer, WORD wDataSize)
{
    //效验消息
    ASSERT(wDataSize == sizeof(CMD_S_OperateResult_WZMJ));
    if (wDataSize != sizeof(CMD_S_OperateResult_WZMJ))
        return false;
    
    //消息处理
    CMD_S_OperateResult_WZMJ * pOperateResult = (CMD_S_OperateResult_WZMJ *)pBuffer;
    
    //变量定义
    BYTE cbPublicCard = true;
    WORD wOperateUser = pOperateResult->wOperateUser;
    WORD wOperateViewID = g_GlobalUnits.SwitchViewChairID(wOperateUser);

    m_wCurrentUser = wOperateUser;
    
    BYTE cbOperateCard = pOperateResult->cbOperateCard;
    
    //杠牌
    if ((pOperateResult->cbOperateCode&WIK_GANG) != 0)
    {
        //组合扑克
        BYTE cbWeaveIndex = 0xFF;
        for (BYTE i = 0; i < m_cbWeaveCount[wOperateUser]; i++)
        {
            BYTE cbWeaveKind = m_WeaveItemArray[wOperateUser][i].cbWeaveKind;
            BYTE cbCenterCard = m_WeaveItemArray[wOperateUser][i].cbCenterCard;
            if ((cbCenterCard == cbOperateCard) && (cbWeaveKind == WIK_PENG))
            {
                cbWeaveIndex = i;
                m_WeaveItemArray[wOperateUser][cbWeaveIndex].cbPublicCard = true;
                m_WeaveItemArray[wOperateUser][cbWeaveIndex].cbWeaveKind = pOperateResult->cbOperateCode;
                break;
            }
        }
        
        if (cbWeaveIndex == 0xFF)
        {
            //暗杠判断
            cbPublicCard =(pOperateResult->wProvideUser == wOperateUser) ? false : true;
            
            //设置扑克
            cbWeaveIndex = m_cbWeaveCount[wOperateUser]++;
            m_WeaveItemArray[wOperateUser][cbWeaveIndex].cbPublicCard = cbPublicCard;
            m_WeaveItemArray[wOperateUser][cbWeaveIndex].cbCenterCard = cbOperateCard;
            m_WeaveItemArray[wOperateUser][cbWeaveIndex].cbWeaveKind = pOperateResult->cbOperateCode;
            m_WeaveItemArray[wOperateUser][cbWeaveIndex].wProvideUser = pOperateResult->wProvideUser;
        }
        
        //组合界面
        if (wOperateUser == GetMeChairID())
            m_WeaveCard.clearMyDataAndCard();
        else
            m_WeaveCard.clearOtherDataAndCard();
        
        for (BYTE i = 0; i < m_cbWeaveCount[wOperateUser]; i++)
        {
            m_WeaveCard.setControlPoint(wOperateViewID);
            m_WeaveCard.SetCardData(m_WeaveItemArray[wOperateUser][i], wOperateViewID);
        }
        
        //扑克设置
        if (GetMeChairID() == wOperateUser)
        {
            m_cbHandCardIndex[m_GameLogic.SwitchToCardIndex(cbOperateCard)] = 0;
            
            SetHandCardControl(m_cbHandCardIndex, 0x00);
            
            m_OutCardItem.removeOtherLastCard();
        }
        else
        {
            BYTE cbCardCount = MAX_COUNT_WZMJ - m_cbWeaveCount[wOperateUser] * 3;
            m_HandCardControl[0].SetTopCardData(cbCardCount, 1);
            m_cbHandCardCount[0]= cbCardCount;
            m_OutCardItem.removeMyLastCard();
        }
    }
    else if (pOperateResult->cbOperateCode != WIK_NULL)
    {
        //设置组合
        BYTE cbWeaveIndex = m_cbWeaveCount[wOperateUser]++;
        m_WeaveItemArray[wOperateUser][cbWeaveIndex].cbPublicCard = true;
        m_WeaveItemArray[wOperateUser][cbWeaveIndex].cbCenterCard = cbOperateCard;
        m_WeaveItemArray[wOperateUser][cbWeaveIndex].cbWeaveKind = pOperateResult->cbOperateCode;
        m_WeaveItemArray[wOperateUser][cbWeaveIndex].wProvideUser = pOperateResult->wProvideUser;
        
        //组合界面
        m_WeaveCard.setControlPoint(wOperateViewID);
        m_WeaveCard.SetCardData(m_WeaveItemArray[wOperateUser][cbWeaveIndex], wOperateViewID);
        
        //删除扑克
        if (GetMeChairID() == wOperateUser)
        {
            BYTE cbWeaveCard[2] = {0, 0};
            
            if (cbOperateCard == CARD_SPACE_DATA_MASK)
                cbOperateCard = m_bMagicCardData;
            
            if (pOperateResult->cbOperateCode == WIK_PENG)
            {
                cbWeaveCard[0] = cbOperateCard;
                cbWeaveCard[1] = cbOperateCard;
            }
            else if (pOperateResult->cbOperateCode == WIK_LEFT)
            {
                cbWeaveCard[0] = cbOperateCard + 1;
                cbWeaveCard[1] = cbOperateCard + 2;
            }
            else if (pOperateResult->cbOperateCode == WIK_CENTER)
            {
                cbWeaveCard[0] = cbOperateCard - 1;
                cbWeaveCard[1] = cbOperateCard + 1;
            }
            else if (pOperateResult->cbOperateCode == WIK_RIGHT)
            {
                cbWeaveCard[0] = cbOperateCard - 1;
                cbWeaveCard[1] = cbOperateCard - 2;
            }
            m_GameLogic.RemoveCard(m_cbHandCardIndex, cbWeaveCard, 2);
    
            SetHandCardControl(m_cbHandCardIndex, 0x00);
            
            m_OutCardItem.removeOtherLastCard();
        }
        else
        {
            BYTE cbCardCount = MAX_COUNT_WZMJ - m_cbWeaveCount[wOperateUser] * 3;
            m_HandCardControl[0].SetTopCardData(cbCardCount, 1);
            m_cbHandCardCount[0]=cbCardCount;
            m_OutCardItem.removeMyLastCard();
        }
    }
    
    //环境设置
    PlayActionSound(wOperateUser, pOperateResult->cbOperateCode, 2);
    

    //设置时间
    StartTime(TIME_OUT_CARD, m_wCurrentUser);
    m_GameState = enGameSendCard;
    
    return true;
}

//买底
bool WZMJGameViewLayer::OnSubChip(const void* pBuffer, WORD wDataSize)
{
    //效验数据
    ASSERT(wDataSize == sizeof(CMD_S_Chip_WZMJ));
    if (wDataSize != sizeof(CMD_S_Chip_WZMJ))
        return false;

    //定义变量
    CMD_S_Chip_WZMJ *pChip = (CMD_S_Chip_WZMJ*)pBuffer;
    m_wBankerUser = pChip->wBankerUser;
    
    //玩家设置
    if (m_wBankerUser == GetMeChairID())
        m_BtnBuy->setVisible(true);
    else
        m_BtnDing->setVisible(true);
    
    m_BtnNoBuy->setVisible(true);
    
    //设置状态
    setGameStatus(GAME_SCENE_CHIP);
    m_GameState=enGameCallBank;//买底状态
    
    //设置定时器
    StartTime(TIME_CHIP, GetMeChairID());
    
    //设置界面
    SetMaidiUI();
    return true;
}


//游戏结束
bool WZMJGameViewLayer::OnSubGameEnd(const void * pBuffer, WORD wDataSize)
{
    //效验数据
    ASSERT(wDataSize == sizeof(CMD_S_GameEnd_WZMJ));
    if (wDataSize != sizeof(CMD_S_GameEnd_WZMJ)) return false;
    
    //消息处理
    CMD_S_GameEnd_WZMJ * pGameEnd = (CMD_S_GameEnd_WZMJ *)pBuffer;
    
    //删除定时器
    StopTime();
    NocanOUT(-1);
    if (m_curBankSpr)
    {
        m_curBankSpr->removeFromParent();
        m_curBankSpr = NULL;
    }
    //设置界面
    showRefultView(pGameEnd);
    
    //设置控件
    SoundUtil::sharedEngine()->playEffect("/WZMJ/sound/GAME_END");
    
    GameEnd();
    return true;
}

void WZMJGameViewLayer::liuju(float dt)
{
    auto child = this->getChildByTag(10000);
    if (child)
        child->removeFromParent();
    m_BtnReadyPlay->setVisible(true);
    StartTime(15, GetMeChairID());
    m_GameState = enGameNormal;
    m_wCurrentUser = GetMeChairID();
}

void WZMJGameViewLayer::showRefultView(CMD_S_GameEnd_WZMJ* pGameEnd)
{
    //计算框信息
     WORD wProvideUser = pGameEnd->wProvideUser;                            //供应用户
    
    if (wProvideUser == INVALID_CHAIR && pGameEnd->isRunAway == false)
    {
        Sprite* liujiu = Sprite::createWithSpriteFrameName("liuju.png");
        liujiu->setPosition(Vec2(960,540));
        addChild(liujiu,10,10000);
        this->scheduleOnce(schedule_selector(WZMJGameViewLayer::liuju), 2.0f);
        return;
    }
    
    WORD wChiHuRight = pGameEnd->wChiHuRight;
    WORD wChiHuKind = pGameEnd->wwChiHuKind;
    BYTE cbChiHuCard = pGameEnd->cbChiHuCard;
    
    //输出胡牌信息
    
    string fanXing, paixing;
    if(wChiHuKind != 0)
    {
        //双翻
        if(wChiHuRight & CHR_SHUANG_FAN)
        {
           fanXing = "双 翻";
        }
        else
        {
           fanXing = "普通胡";
        }
    }
    
    if(wChiHuRight & CHR_YING_HU || wChiHuRight & CHR_SHUANG_FAN)
    {
      paixing = "硬  胡";
    }
    else
    {
        if (cbChiHuCard == 255)
        {
            paixing = "玩家逃跑";
        }
        else if(cbChiHuCard == 254){
            paixing = "玩家掉线";
        }
        else
        {
            paixing = "软  胡";
        }
    }
    
    //积分信息
    LONGLONG lGameScore[GAME_PLAYER_WZMJ];
    ZeroMemory(lGameScore, GAME_PLAYER_WZMJ);
    
    //扑克信息
    BYTE cbCardCount = 0;                //扑克数目
    
    BYTE cbCardData[MAX_COUNT_WZMJ];
    ZeroMemory(cbCardData, MAX_COUNT_WZMJ);
    
    unsigned dwChiHuKind[GAME_PLAYER_WZMJ];
    dwChiHuKind[0] =0;
    dwChiHuKind[1] =0;
    
    BYTE winPlayer = -1;
    for (WORD i = 0; i < GAME_PLAYER_WZMJ; i++)
    {
        lGameScore[i] = pGameEnd->lGameScore[i];                    //游戏积分
        if (pGameEnd->wChiHuKind[i] == WIK_NULL)
        {
            winPlayer = 1 - i;
            continue;
        }
        
        cbCardCount = pGameEnd->cbCardCount[i];                     //扑克数目
        
        for (int j = 0; j < cbCardCount; j++)                       //扑克数据
        {
            if (CARD_KING_DATA_MASK == pGameEnd->cbCardData[i][j])
                cbCardData[j] = m_bMagicCardData;
            else if (m_bMagicCardData == pGameEnd->cbCardData[i][j])
                cbCardData[j] = CARD_SPACE_DATA_MASK;
            else
                cbCardData[j] = pGameEnd->cbCardData[i][j];
        }
    }
    
    m_resultBg = ui::Scale9Sprite::createWithSpriteFrameName("tips_back.png");
    m_resultBg->setContentSize(Size(1100, 700));
    m_resultBg->setPosition(Vec2(960, 540));
    addChild(m_resultBg, 100);
    
    Sprite* titleSpr = Sprite::createWithSpriteFrameName("jiesuan.png");
    titleSpr->setPosition(Vec2(550,650));
    m_resultBg->addChild(titleSpr);
    
    Vec2 pos = Vec2(100, 530);
    
    for (BYTE i = 0; i < m_cbWeaveCount[winPlayer]; i++)
    {
        //白板转财神
        if (m_WeaveItemArray[winPlayer][i].cbCenterCard == CARD_SPACE_DATA_MASK)
            m_WeaveItemArray[winPlayer][i].cbCenterCard = m_bMagicCardData;
        
        BYTE cardData[4] = {0, 0, 0, 0};
        int count = 0;
        
        if (m_WeaveItemArray[winPlayer][i].cbWeaveKind == WIK_GANG)
        {
            count = 4;
            for (BYTE j = 0; j < 4; j++)
            {
                cardData[j] = m_WeaveItemArray[winPlayer][i].cbCenterCard;
                
                //财神转换回白板
                if (cardData[j] == m_bMagicCardData)
                    cardData[j] = CARD_SPACE_DATA_MASK;
            }
        }
        else
        {
            for (BYTE j = 0; j < 3; j++)
            {
                count = 3;
                if (m_WeaveItemArray[winPlayer][i].cbWeaveKind == WIK_PENG)
                    cardData[j] = m_WeaveItemArray[winPlayer][i].cbCenterCard;
                
                else if (m_WeaveItemArray[winPlayer][i].cbWeaveKind == WIK_LEFT)
                    cardData[j] = m_WeaveItemArray[winPlayer][i].cbCenterCard + j;
                
                else if (m_WeaveItemArray[winPlayer][i].cbWeaveKind == WIK_CENTER)
                    cardData[j] = m_WeaveItemArray[winPlayer][i].cbCenterCard + j - 1;
                
                else if (m_WeaveItemArray[winPlayer][i].cbWeaveKind == WIK_RIGHT)
                    cardData[j] = m_WeaveItemArray[winPlayer][i].cbCenterCard + j - 2;
                
                //财神转换回白板
                if (cardData[j] == m_bMagicCardData)
                    cardData[j] = CARD_SPACE_DATA_MASK;
            }
        }

        for (int j = 0; j < count; j++)
        {
            int color = WZMJCardControl::GetCardColor(cardData[j]) >> 4;
            int value = WZMJCardControl::GetCardValue(cardData[j]);
            
            Sprite* cardSpr = Sprite::createWithSpriteFrameName("wzmj_44.png");
            string str = "wzmj_" + StringUtils::toString(color*10 + value) + ".png";
            Sprite *c = Sprite::createWithSpriteFrameName(str.c_str());
            c->setPosition(cardSpr->getContentSize().width / 2, cardSpr->getContentSize().height / 2 + 10);
            cardSpr->addChild(c);
            cardSpr->setScale(0.6f);
            cardSpr->setPosition(pos);
            m_resultBg->addChild(cardSpr);
            
            pos.x += 50;
        }
        
        pos.x += 15;
    }
    
    bool hasAdd =false;
    
    for (int i = 0; i < cbCardCount; i++)
    {
        int color = WZMJCardControl::GetCardColor(cbCardData[i]) >> 4;
        int value = WZMJCardControl::GetCardValue(cbCardData[i]);
        
        Sprite* cardSpr = Sprite::createWithSpriteFrameName("wzmj_44.png");
        string str = "wzmj_" + StringUtils::toString(color*10 + value) + ".png";
        Sprite *c = Sprite::createWithSpriteFrameName(str.c_str());
        c->setPosition(cardSpr->getContentSize().width / 2, cardSpr->getContentSize().height / 2 + 10);
        cardSpr->addChild(c);
        cardSpr->setScale(0.6f);
        cardSpr->setPosition(pos);
        m_resultBg->addChild(cardSpr);
        
        if (cbCardData[i] == pGameEnd->cbChiHuCard && !hasAdd && !(pGameEnd->isRunAway))
        {
            hasAdd = true;
            Sprite* tip = nullptr;
            if (pGameEnd->bisZiMo)
            {
                tip = Sprite::createWithSpriteFrameName("zimo.png");
            }
            else
            {
                tip = Sprite::createWithSpriteFrameName("chihu.png");
            }
            tip->setScale(4.0f);
            tip->setPosition(Vec2(45,170));
            cardSpr->addChild(tip);
        }
        pos.x += 50;
    }
    
    Sprite* fanx = Sprite::createWithSpriteFrameName("fanxin.png");
    fanx->setPosition(Vec2(130, 440));
    m_resultBg->addChild(fanx);
    
    Label * fanLabel = Label::createWithSystemFont(fanXing, _GAME_FONT_NAME_1_, 40);
    fanLabel->setAnchorPoint(Vec2(0,0.5f));
    fanLabel->setPosition(Vec2(220,440));
    m_resultBg->addChild(fanLabel);
    
    Sprite* di = Sprite::createWithSpriteFrameName("dishu.png");
    di->setPosition(Vec2(480, 440));
    m_resultBg->addChild(di);
    
    string diScore = StringUtils::toString(m_lCellScore * m_cbLianZhuangCount);
    Label * diLabel = Label::createWithSystemFont(diScore, _GAME_FONT_NAME_1_, 40);
    diLabel->setAnchorPoint(Vec2(0, 0.5f));
    diLabel->setPosition(Vec2(530, 440));
    m_resultBg->addChild(diLabel);
    
    Sprite* paix = Sprite::createWithSpriteFrameName("paixing.png");
    paix->setPosition(Vec2(770, 440));
    m_resultBg->addChild(paix);
    
    Label * paiLabel = Label::createWithSystemFont(paixing, _GAME_FONT_NAME_1_, 40);
    paiLabel->setAnchorPoint(Vec2(0, 0.5f));
    paiLabel->setPosition(Vec2(850, 440));
    m_resultBg->addChild(paiLabel);

    Sprite* player = Sprite::createWithSpriteFrameName("wanjia.png");
    player->setPosition(Vec2(130, 380));
    m_resultBg->addChild(player);
    
    Sprite* buy = Sprite::createWithSpriteFrameName("maidi.png");
    buy->setPosition(Vec2(480, 380));
    m_resultBg->addChild(buy);
    
    Sprite* ding = Sprite::createWithSpriteFrameName("dingdi.png");
    ding->setPosition(Vec2(780, 380));
    m_resultBg->addChild(ding);
    
    for (int i = 0; i < GAME_PLAYER_WZMJ; i++)
    {
        Label *nameLabel = Label::createWithSystemFont(m_nickName[i], "", 40);
        nameLabel->setAnchorPoint(Vec2(0,0.5f));
        nameLabel->setPosition(Vec2(80, 320 - i * 60));
        m_resultBg->addChild(nameLabel);
        
         WORD wViewID = g_GlobalUnits.SwitchViewChairID(i);
        
        if (wViewID == m_wBankerUser && m_bMaiDi[wViewID] == INDEX_BUY)
        {
            Sprite* maidi = Sprite::createWithSpriteFrameName("down2.png");
            maidi->setScale(0.8f);
            maidi->setPosition(Vec2(480, 320 - i * 60));
            m_resultBg->addChild(maidi);
        }
        else if (wViewID != m_wBankerUser && m_bMaiDi[wViewID] == INDEX_BUY)
        {
            Sprite* dingdi = Sprite::createWithSpriteFrameName("down2.png");
            dingdi->setScale(0.8f);
            dingdi->setPosition(Vec2(780, 320 - i * 60));
            m_resultBg->addChild(dingdi);
        }
    }
    
    Sprite* total = Sprite::createWithSpriteFrameName("zongjidefen.png");
    total->setAnchorPoint(Vec2(1, 0.5f));
    total->setPosition(Vec2(550, 200));
    m_resultBg->addChild(total);
    
    Label *score = Label::createWithSystemFont(StringUtils::toString(lGameScore[GetMeChairID()]), "", 40);
    score->setColor(_GAME_FONT_COLOR_4_);
    score->setAnchorPoint(Vec2(0,0.5));
    score->setPosition(Vec2(560, 200));
    m_resultBg->addChild(score);
    
    Menu* sureBtn = CreateButton("btn_sure", Vec2(m_resultBg->getContentSize().width/2, 100), TAG_SURE);
    m_resultBg->addChild(sureBtn, 0, TAG_SURE);
}

//设置扑克
void WZMJGameViewLayer::SetHandCardControl(BYTE cbCardIndex[MAX_INDEX], BYTE cbAdvanceCard)
{
    //组合数目
    WORD wMeChairID = GetMeChairID();
    BYTE cbWeaveCardCount = m_cbWeaveCount[wMeChairID] * 3;
    
    //转换扑克
    BYTE cbHandCardData[MAX_COUNT_WZMJ];
    BYTE cbCardCount = m_GameLogic.SwitchToCardData(cbCardIndex, cbHandCardData, MAX_COUNT_WZMJ);
    
    //调整扑克
    if ((cbWeaveCardCount + cbCardCount) == MAX_COUNT_WZMJ)
    {
        if (cbAdvanceCard != 0x00)
        {
            //删除扑克
            BYTE cbRemoveCard[] = { cbAdvanceCard };
            ASSERT(m_GameLogic.RemoveCard(cbHandCardData, cbCardCount, cbRemoveCard, 1));
            
            //设置扑克
            cbHandCardData[cbCardCount - 1] = cbAdvanceCard;
        }
        m_curCardData[1] = cbHandCardData[cbCardCount - 1];
        m_HandCardControl[1].SetCardData(cbHandCardData, cbCardCount, cbHandCardData[cbCardCount - 1]);
    }
    else {
        m_curCardData[1] = 0;
        m_HandCardControl[1].SetCardData(cbHandCardData, cbCardCount, 0x00);
    }
    
    return;
}

bool WZMJGameViewLayer::VerdictOutCard(BYTE cbPreCardData, BYTE cbAlreadyCardData[8], BYTE cbFengCardCount)
{
    int bRet = CheckOutCard(cbPreCardData, cbAlreadyCardData, cbFengCardCount);
    
    NocanOUT(bRet);
    return (bRet == 0);
}

int WZMJGameViewLayer::CheckOutCard(BYTE cbPreCardData, BYTE cbAlreadyCardData[8], BYTE cbFengCardCount)
{
    if (m_GameLogic.IsKingCardData(cbPreCardData))
    {
        for (int i = 0; i < MAX_INDEX-1; i++)
        {
            if (m_cbHandCardIndex[i] != 0x00)
                return 1;
        }
    }
    
    BYTE bPreIndex = m_GameLogic.SwitchToCardIndex(cbPreCardData);
    bool bFendMagic = m_bMagicCardData >= 0x30;
    
    
    if (cbFengCardCount != 0)
    {
        for (int i = 0; i < (bFendMagic ? 7 : 6); i++)
        {
            if (cbAlreadyCardData[i] == 0 || m_GameLogic.IsKingCardData(cbAlreadyCardData[i])) continue;
            if (cbPreCardData == cbAlreadyCardData[i] && m_cbHandCardIndex[bPreIndex] == 1)
                return 0;
        }
        
        for (int i = 0; i < (bFendMagic ? 7 : 6); i++)
        {
            if (cbAlreadyCardData[i] == 0 || m_GameLogic.IsKingCardData(cbAlreadyCardData[i])) continue;
            if (m_cbHandCardIndex[m_GameLogic.SwitchToCardIndex(cbAlreadyCardData[i])] == 1)
                return 3;
        }
    }
    
    
    if (bPreIndex >= 27 && bPreIndex <= (bFendMagic ? 33 : 32) && m_cbHandCardIndex[bPreIndex] == 1)
        return 0;
    else
    {
        
        for (int Index = 27; Index <= (bFendMagic ? 33 : 32); Index++)
        {
            if (m_GameLogic.IsKingCardIndex(Index))	continue;
            if (m_cbHandCardIndex[Index] == 1)
                return 2;
        }
    }
    
    return 0;
}

void WZMJGameViewLayer::NocanOUT(int Tag)
{
    switch (Tag)
    {
        case 0:
        {
            m_pTips->setSpriteFrame("chosecard.png");
            m_pTips->setVisible(true);
            break;
        }
        case 1:
        {
            m_pTips->setSpriteFrame("cannotcaishen.png");
            m_pTips->setVisible(true);
            break;
        }
        case 2:
        {
            m_pTips->setSpriteFrame("chudanzi.png");
            m_pTips->setVisible(true);
            break;
        }
        case 3:
        {
            m_pTips->setSpriteFrame("outsamecard.png");
            m_pTips->setVisible(true);
            break;
        }
        default:
            m_pTips->setVisible(false);
            break;
    }
    
}

void WZMJGameViewLayer::showMenuItem(int iTag,bool bVisible)
{
    switch (iTag)
    {
        case TAG_PENG:
        {
            m_BtnPengCard->setVisible(bVisible);
            break;
        }
        case TAG_GANG:
        {
            m_BtnGangCard->setVisible(bVisible);
            break;
        }
        case TAG_HU:
        {
            m_BtnHuCard->setVisible(bVisible);
            break;
        }
        case TAG_QI:
        {
            m_BtnQiCard->setVisible(bVisible);
            break;
        }
        case TAG_CHIZHONG:
        {
            m_BtnChiZCard->setVisible(bVisible);
            break;
        }
        case TAG_CHIXIA:
        {
            m_BtnChiXCard->setVisible(bVisible);
            break;
        }
        case TAG_CHISHANG:
        {
            m_BtnChiSCard->setVisible(bVisible);
            break;
        }
        default:
            break;
    }
}

void WZMJGameViewLayer::setMenuItemBtnEnable(int iTag, bool bVisible)
{
    Color3B color = bVisible ?  Color3B(255,255,255) : Color3B(80,80,80);
    switch (iTag)
    {
        case TAG_PENG:
        {
            m_BtnPengCard->setEnabled(bVisible);
            m_BtnPengCard->setColor(color);
            break;
        }
        case TAG_GANG:
        {
            m_BtnGangCard->setEnabled(bVisible);
            m_BtnGangCard->setColor(color);
            break;
        }
        case TAG_HU:
        {
            m_BtnHuCard->setEnabled(bVisible);
            m_BtnHuCard->setColor(color);
            break;
        }
        case TAG_QI:
        {
            m_BtnQiCard->setEnabled(bVisible);
            m_BtnQiCard->setColor(color);
            break;
        }
        case TAG_CHIZHONG:
        {
            m_BtnChiZCard->setEnabled(bVisible);
            m_BtnChiZCard->setColor(color);
            break;
        }
        case TAG_CHIXIA:
        {
            m_BtnChiXCard->setEnabled(bVisible);
            m_BtnChiXCard->setColor(color);
            break;
        }
        case TAG_CHISHANG:
        {
            m_BtnChiSCard->setEnabled(bVisible);
            m_BtnChiSCard->setColor(color);
            break;
        }
        default:
            break;
    }
}

//设置状态
void WZMJGameViewLayer::SetControlInfo(BYTE cbAcitonMask)
{
    //控制按钮
    setMenuItemBtnEnable(TAG_PENG, ((cbAcitonMask&WIK_PENG) != 0) ? TRUE : FALSE);
    setMenuItemBtnEnable(TAG_GANG, (((cbAcitonMask&WIK_GANG)<<4) != 0) ? TRUE : FALSE);
    setMenuItemBtnEnable(TAG_HU, (((cbAcitonMask&WIK_CHI_HU)<<4) != 0) ? TRUE : FALSE);
    setMenuItemBtnEnable(TAG_CHIZHONG, ((cbAcitonMask&WIK_CENTER) != 0) ? TRUE : FALSE);
    setMenuItemBtnEnable(TAG_CHISHANG, ((cbAcitonMask&WIK_LEFT) != 0) ? TRUE : FALSE);
    setMenuItemBtnEnable(TAG_CHIXIA, ((cbAcitonMask&WIK_RIGHT) != 0) ? TRUE : FALSE);
    
    //控制窗口
    showMenuItem(TAG_PENG, (cbAcitonMask != WIK_NULL) ? SW_SHOW : SW_HIDE);
    showMenuItem(TAG_GANG, (cbAcitonMask != WIK_NULL) ? SW_SHOW : SW_HIDE);
    showMenuItem(TAG_CHIZHONG, (cbAcitonMask != WIK_NULL) ? SW_SHOW : SW_HIDE);
    showMenuItem(TAG_QI, (cbAcitonMask != WIK_NULL) ? SW_SHOW : SW_HIDE);
    showMenuItem(TAG_HU, (cbAcitonMask != WIK_NULL) ? SW_SHOW : SW_HIDE);
    showMenuItem(TAG_CHISHANG, (cbAcitonMask != WIK_NULL) ? SW_SHOW : SW_HIDE);
    showMenuItem(TAG_CHIXIA, (cbAcitonMask != WIK_NULL) ? SW_SHOW : SW_HIDE);
    return;
}

//发牌消息
bool WZMJGameViewLayer::OnSubSendCard(const void * pBuffer, WORD wDataSize)
{
    //效验数据
    ASSERT(wDataSize == sizeof(CMD_S_SendCard_WZMJ));
    if (wDataSize != sizeof(CMD_S_SendCard_WZMJ)) return false;
    
    //关闭操作按钮
    SetControlInfo(WIK_NULL);
    
    //消息处理
    CMD_S_SendCard_WZMJ * pSendCard = (CMD_S_SendCard_WZMJ *)pBuffer;
    m_wCurrentUser = pSendCard->wCurrentUser;
    m_wCurSendCardUser = pSendCard->wCurrentUser;
    m_cbActionMask = pSendCard->cbActionMask;
    m_cbActionCard = pSendCard->cbCardData;
    
    if (m_cbActionMask != WIK_NULL)
    {
        if (m_wCurrentUser == GetMeChairID())
        {
            SetControlInfo(m_cbActionMask);
        }
        NocanOUT(-1);
        StartTime(TIME_ACTION_CARD, m_wCurrentUser);
        m_GameState = enGameBet;
    }
    
    
    if (pSendCard->wCurrentUser == GetMeChairID())
    {
        m_cbHandCardIndex[m_GameLogic.SwitchToCardIndex(pSendCard->cbCardData)]++;
        m_cbHandCardCount[1]++;
        m_curCardData[0] = 0;
        m_curCardData[1] = pSendCard->cbCardData;
        drawView(SELF_HAND);
        SoundUtil::sharedEngine()->playEffect("WZMJ/sound/SEND_CARD");
    }
    else
    {
        m_cbHandCardCount[1]++;
        m_curCardData[0] = 1;
        m_curCardData[1] = 0;
        drawView(OTHER_HAND);
    }
    StartTime(TIME_OUT_CARD, m_wCurrentUser);
    m_GameState = enGameSendCard;
    
    return true;
}

//出牌消息
bool WZMJGameViewLayer::OnSubOutCard(const void * pBuffer, WORD wDataSize)
{
    //效验数据
    if (wDataSize!= sizeof(CMD_S_OutCard_WZMJ)) return false;
    CMD_S_OutCard_WZMJ* pOutCard = (CMD_S_OutCard_WZMJ*)pBuffer;
    
    //隐藏提示
    NocanOUT(-1);
    m_canOutCard = true;
    
    if ((pOutCard->cbOutCardData&MASK_COLOR) == 0x30)
    {
        if (m_cbFengCardData[pOutCard->cbOutCardData - 0x31] == 0)
            m_cbFengCardCount++;
        m_cbFengCardData[pOutCard->cbOutCardData - 0x31] = pOutCard->cbOutCardData;
    }
    
    if (pOutCard->wOutCardUser != GetMeChairID())
    {
        m_OutCardItem.SetTopCardData(pOutCard->cbOutCardData);
        PlayCardSound(pOutCard->wOutCardUser, pOutCard->cbOutCardData);
        m_curCardData[0] = 0;
        drawView(OTHER_HAND);
    }
    else if (pOutCard->wOutCardUser == GetMeChairID())
    {
        BYTE cbOutCardData = pOutCard->cbOutCardData;
        //播放声音
        PlayCardSound(GetMeChairID(), cbOutCardData);
        
        m_wCurrentUser = INVALID_CHAIR;
        m_cbActionMask = 0;
        m_curCardData[0] = 0;
        m_curCardData[1] = 0;
        
        if (!m_GameLogic.RemoveCard(m_cbHandCardIndex, cbOutCardData))
        {
            return 0;
        }
        
        BYTE cbCardData[MAX_COUNT_WZMJ];
        ZeroMemory(cbCardData, MAX_COUNT_WZMJ);
        BYTE cbCardCount = m_GameLogic.SwitchToCardData(m_cbHandCardIndex, cbCardData, MAX_COUNT_WZMJ);
        
        m_HandCardControl[1].SetCardData(cbCardData, cbCardCount, 0);
        m_OutCardItem.SetMyCardData(pOutCard->cbOutCardData);
        drawView(SELF_HAND);
        
        SoundUtil::sharedEngine()->playEffect("WZMJ/sound/OUT_CARD");
    }
    
    
    return true;
}

void WZMJGameViewLayer::SetMaidiUI()
{    
    if (m_curBankSpr != NULL)
    {
        m_curBankSpr->removeFromParent();
        m_curBankSpr = NULL;
    }
    WORD viewChairID = g_GlobalUnits.SwitchViewChairID(m_wBankerUser);
    m_curBankSpr = Sprite::createWithSpriteFrameName("label_bank.png");
    
    if (m_wBankerUser == GetMeChairID())
    {
        m_curBankSpr->setPosition(m_HeadPos[viewChairID] - Vec2(120, 0));
    }
    else
    {
        m_curBankSpr->setPosition(m_HeadPos[viewChairID] - Vec2(140, 20));
    }
    
    addChild(m_curBankSpr);
}

void WZMJGameViewLayer::drawView(int tag)
{
    switch (tag) {
        case OTHER_HAND:
        {
            m_HandCardControl[0].ClearCardData();
            m_HandCardControl[0].SetCardData(nullptr, m_cbHandCardCount[0], m_curCardData[0]);
            break;
        }
        case SELF_HAND:
        {
            m_HandCardControl[1].ClearCardData();
            BYTE cbCardData[MAX_COUNT_WZMJ];
            BYTE cbCardCount = m_GameLogic.SwitchToCardData(m_cbHandCardIndex, cbCardData, MAX_COUNT_WZMJ);
            
            m_HandCardControl[1].SetCardData(cbCardData, cbCardCount, m_curCardData[1]);
            break;
        }
        default:
            break;
            
    }
}

void WZMJGameViewLayer::InitGame()
{
    //背景图
    m_BackSpr = Sprite::create("WZMJ/desk_bg.jpg");
    m_BackSpr->setPosition(_STANDARD_SCREEN_CENTER_IN_PIXELS_);
    addChild(m_BackSpr);
    
    m_ClockSpr = Sprite::createWithSpriteFrameName("timeback.png");
    addChild(m_ClockSpr);
    m_ClockSpr->setVisible(false);
    
    for (int i = 0; i < GAME_PLAYER_WZMJ; i++)
    {
        //tag标志
        m_ReadyTga[i] = 1000 + i;
        m_HeadTga[i] = 1010 + i;
        m_GlodTga[i] = 1020 + i;
        m_NickNameTga[i] = 1030 + i;
        m_maidiTga[i] = 1035 + i;
    }
    m_AnimTga = 1040;
    
    //位置
    m_HeadPos[0] = Vec2(310, 920);
    m_HeadPos[1] = Vec2(170, 150);
    m_ReadyPos[0] = Vec2(510, 870);
    m_ReadyPos[1] = Vec2(400, 200);
    m_ClockPos[0] = Vec2(310, 730);
    m_ClockPos[1] = Vec2(380, 280);
    m_canOutCard = false;
    
    m_HandCardControl[0].SetLayer(this,300,0);
    m_HandCardControl[1].SetLayer(this,400,1);
    
    Layer * weaveLayer = Layer::create();
    addChild(weaveLayer);
    Layer *outLayer = Layer::create();
    addChild(outLayer);
    
    m_WeaveCard.SetLayer(weaveLayer,500);
    m_OutCardItem.SetLayer(outLayer, 900);
    
    m_wBankerUser = 0;
    
    m_pTips = Sprite::createWithSpriteFrameName("chosecard.png");
    m_pTips->setPosition(Vec2(960, 210));
    m_pTips->setTag(2331);
    addChild(m_pTips,999);
    m_pTips->setVisible(false);
}

void WZMJGameViewLayer::AddButton()
{
    m_BtnReadyPlay = CreateButton("btn_start" ,Vec2(_STANDARD_SCREEN_CENTER_.x,360),TAG_REAGY);
    addChild(m_BtnReadyPlay , 100 , TAG_REAGY);
    m_BtnReadyPlay->setVisible(false);
    
    m_BtnBackToLobby = CreateButton("Returnback", Vec2(90, 1000), TAG_RETURNBACK);
    addChild(m_BtnBackToLobby,  100, TAG_RETURNBACK);
    m_BtnBackToLobby->setVisible(true);
    
    m_BtnSeting = CreateButton("seting" ,Vec2(1810,1000),TAG_SETTING);
    addChild(m_BtnSeting , 100 , TAG_SETTING);
    m_BtnSeting->setVisible(true);
    
    m_BtnBuy = CreateButton("btn_pay", Vec2(800, 360), TAG_BUY);
    addChild(m_BtnBuy, 100, TAG_BUY);
    m_BtnBuy->setVisible(false);

    m_BtnDing = CreateButton("btn_ding", Vec2(800, 360), TAG_BUY);
    addChild(m_BtnDing, 100, TAG_BUY);
    m_BtnDing->setVisible(false);

    m_BtnNoBuy = CreateButton("btn_nopay", Vec2(1120, 360), TAG_NOBUY);
    addChild(m_BtnNoBuy, 100, TAG_NOBUY);
    m_BtnNoBuy->setVisible(false);
    
    m_BtnGangCard = CreateButton("btn_gang", Vec2(760, 360), TAG_GANG);
    addChild(m_BtnGangCard, 100, TAG_GANG);
    m_BtnGangCard->setVisible(false);
    
    m_BtnPengCard = CreateButton("btn_peng", Vec2(920, 360), TAG_PENG);
    addChild(m_BtnPengCard, 100, TAG_PENG);
    m_BtnPengCard->setVisible(false);
    
    m_BtnChiSCard = CreateButton("btn_eatup", Vec2(1080, 360), TAG_CHISHANG);
    addChild(m_BtnChiSCard, 100, TAG_CHISHANG);
    m_BtnChiSCard->setVisible(false);
    
    m_BtnChiZCard = CreateButton("btn_eatmid", Vec2(1240, 360), TAG_CHIZHONG);
    addChild(m_BtnChiZCard, 100, TAG_CHIZHONG);
    m_BtnChiZCard->setVisible(false);
    
    m_BtnChiXCard = CreateButton("btn_eatdown", Vec2(1400, 360), TAG_CHIXIA);
    addChild(m_BtnChiXCard, 100, TAG_CHIXIA);
    m_BtnChiXCard->setVisible(false);
    
    m_BtnHuCard = CreateButton("btn_hu", Vec2(1560, 360), TAG_HU);
    addChild(m_BtnHuCard, 100, TAG_HU);
    m_BtnHuCard->setVisible(false);
    
    m_BtnQiCard = CreateButton("btn_fangqi", Vec2(1720, 360), TAG_QI);
    addChild(m_BtnQiCard, 100, TAG_QI);
    m_BtnQiCard->setVisible(false);
}

Menu* WZMJGameViewLayer::CreateButton(std::string szBtName ,const Vec2 &p , int tag )
{
    return Tools::Button(StrToChar(szBtName+"_normal.png"), StrToChar(szBtName+"_select.png"), p, this , menu_selector(WZMJGameViewLayer::callbackBt) , tag);
}

void WZMJGameViewLayer::AddPlayerInfo()
{
    for (int i = 0; i < GAME_PLAYER_WZMJ; i++)
    {
        if (ClientSocketSink::sharedSocketSink()->GetMeUserData() == NULL)
        {
            return;
        }
        
        const tagUserData* pUserData = GetUserData(i);
        int chair = g_GlobalUnits.SwitchViewChairID(i);
        if (pUserData != NULL)
        {
            //头像
            removeAllChildByTag(m_HeadTga[chair]);
            
            Sprite* headBg = Sprite::createWithSpriteFrameName("touxiangkuangdi.png");
            headBg->setPosition(m_HeadPos[chair]);
            headBg->setTag(m_HeadTga[chair]);
            addChild(headBg);

            string heads = g_GlobalUnits.getFace(pUserData->wGender, pUserData->lScore);
            Sprite* mHead = Sprite::createWithSpriteFrameName(heads);
            mHead->setScale(0.9f);
            mHead->setPosition(Vec2(88,134));
            headBg->addChild(mHead);
            
            //金币
            removeAllChildByTag(m_GlodTga[chair]);
            char strc[32]="";
            memset(strc , 0 , sizeof(strc));
            LONGLONG lMon = pUserData->lScore;
            if (lMon > 100000000)
                sprintf(strc, "%.02lf亿", lMon / 100000000.0f);
            else if (lMon > 1000000)
                sprintf(strc, "%.02lf万", lMon / 10000.0f);
            else
                sprintf(strc, "%lld", lMon);
            
            Label *mGoldFont = Label::createWithSystemFont(strc, _GAME_FONT_NAME_1_, 30);
            mGoldFont->setTag(m_GlodTga[chair]);
            addChild(mGoldFont);
            mGoldFont->setColor(Color3B::YELLOW);
            mGoldFont->setPosition(m_HeadPos[chair] - Vec2(0,105));
            
            //昵称
            removeAllChildByTag(m_NickNameTga[chair]);
            m_nickName[chair] = gbk_utf8(pUserData->szNickName);
            Label* mNickfont = Label::createWithSystemFont(m_nickName[chair], _GAME_FONT_NAME_1_, 30);
            mNickfont->setTag(m_NickNameTga[chair]);
            mNickfont->setPosition(m_HeadPos[chair] + Vec2(0, 105));
            addChild(mNickfont);
            
            //准备
            removeAllChildByTag(m_ReadyTga[chair]);
            Sprite* mReaderSpr = Sprite::createWithSpriteFrameName("img_ready.png");
            mReaderSpr->setPosition(m_ReadyPos[chair]);
            mReaderSpr->setTag(m_ReadyTga[chair]);
            addChild(mReaderSpr);
            
            if (pUserData->cbUserStatus == US_READY)
            {
                getChildByTag(m_ReadyTga[chair])->setVisible(true);
            }
            else
                getChildByTag(m_ReadyTga[chair])->setVisible(false);
        }
        else
        {
            if (chair == 1 && !g_GlobalUnits.m_bLeaveGameByServer) //如果是自己退出，就关闭游戏
            {
//                AlertMessageLayer::createConfirm(this , "\u60a8\u7684\u9152\u5427\u8c46\u4e0d\u8db3\uff0c\u4e0d\u80fd\u7ee7\u7eed\u6e38\u620f\uff01", menu_selector(IGameView::backLoginView));
                backLoginView(nullptr);
                return;
            }
            removeAllChildByTag(m_HeadTga[chair]);
            removeAllChildByTag(m_GlodTga[chair]);
            removeAllChildByTag(m_NickNameTga[chair]);
            removeAllChildByTag(m_ReadyTga[chair]);
        }
    }
}

string WZMJGameViewLayer::AddCommaToNum(LONG Num)
{
    char _str[256];
    sprintf(_str,"%ld", Num);
    string _string = _str;
    long step = _string.length()/3;
    for (int i = 1; i <= step; i++)
    {
        _string.insert(_string.length()-(i-1)-(i*3), ",");
    }
    return _string;
}



void WZMJGameViewLayer::SendReqSendRecord()
{
    CMD_C_GP_GetSendRecord GetSendRecord;
    ZeroMemory(&GetSendRecord ,sizeof(GetSendRecord));
    GetSendRecord.dwUserID = g_GlobalUnits.GetUserID();
    GetSendRecord.wKindID = Ox2KIND_ID;
    GetSendRecord.wItemCount = SENDRECORD_COUNT;
    GetSendRecord.wPageIndex = 1;
    SendMobileData(SUB_C_GP_SENDRECORD , &GetSendRecord ,sizeof(GetSendRecord));
}

void WZMJGameViewLayer::callbackBt(Ref *pSender )
{
    Node *pNode = (Node *)pSender;
    switch (pNode->getTag())
    {
        case TAG_REAGY:	//准备按钮
        {
            SendGameStart();
            break;
        }
        case TAG_RETURNBACK: // 返回大厅按钮
        {
            SoundUtil::sharedEngine()->playEffect("buttonMusic");
            BackToLobby();
            break;
        }
        case TAG_SETTING: //设置
        {
            SoundUtil::sharedEngine()->playEffect("buttonMusic");
            GameLayerMove::sharedGameLayerMoveSink()->OpenSeting();
            break;
        }
        case TAG_BUY://买底
        {
            OnChip(INDEX_BUY);
            StopTime();
            break;
        }
        case TAG_NOBUY://不买
        {
            OnChip(INDEX_NOBUY);
            StopTime();
            break;
        }
        case TAG_PENG:
        {
            SetControlInfo(WIK_NULL);
            OnUserAction(WIK_PENG);
            break;
        }
        case TAG_GANG:
        {
            SetControlInfo(WIK_NULL);
            OnUserAction(WIK_GANG);
            break;
        }
        case TAG_CHISHANG:
        {
            SetControlInfo(WIK_NULL);
            OnUserAction(WIK_LEFT);
            break;
        }
        case TAG_CHIZHONG:
        {
            SetControlInfo(WIK_NULL);
            OnUserAction(WIK_CENTER);
            break;
        }
        case TAG_CHIXIA:
        {
            SetControlInfo(WIK_NULL);
            OnUserAction(WIK_RIGHT);
            break;
        }
        case TAG_QI:
        {
            SetControlInfo(WIK_NULL);
            OnUserAction(WIK_NULL);
            break;
        }
        case TAG_HU:
        {
            SetControlInfo(WIK_NULL);
            OnUserAction(WIK_CHI_HU);
            break;
        }
        case TAG_SURE:
        {
            if (m_resultBg)
            {
                m_resultBg->removeFromParent();
            }
            m_BtnReadyPlay->setVisible(true);
            StartTime(15, GetMeChairID());
            m_GameState = enGameNormal;
            m_wCurrentUser = GetMeChairID();
            break;
        }
    }
}
//玩家操作控件
int WZMJGameViewLayer::OnUserAction(BYTE cbOperateCode)
{
    //变量定义
    BYTE cbOperateCard[3] = { 0, 0, 0 };
    //隐藏操作控件
    
    //状态判断
    if (cbOperateCode == WIK_NULL)
    {
        BYTE B1 = (m_cbActionMask & WIK_CHI_HU) << 4;
        BYTE B2 = (m_cbActionMask & WIK_GANG) << 4;
        
        if ((B1 == 0) && (B2 == 0) && (m_wCurSendCardUser != m_wCurrentUser))
        {
            //发送消息
            CMD_C_OperateCard_WZMJ OperateCard;
            OperateCard.cbOperateCode = WIK_NULL;
            ZeroMemory(&OperateCard.cbOperateCard, sizeof(OperateCard.cbOperateCard));
            SendData(SUB_C_OPERATE_CARD_WZMJ, &OperateCard, sizeof(OperateCard));
        }

        //删除定时器
        StopTime();
        //设置变量
        m_cbActionMask = WIK_NULL;
        m_cbActionCard = 0;
        return 0;
    }
    
    bool bDone = false;
    //胡牌
    if (cbOperateCode & WIK_CHI_HU)
    {
        bDone = true;
    }
    else
    {
        //获取选择组合
        tagSelectCardInfo_WZMJ sci[MAX_COUNT_WZMJ];
        BYTE cbInfoCount = GetSelectCardInfo(cbOperateCode, sci);
        
        //设置操作事件
        bDone = m_HandCardControl[1].OnEventUserAction(sci, cbInfoCount);
        if(cbInfoCount == 1)
        {
            m_HandCardControl[1].m_cbSelectInfoCount = 1;
            bDone = true;
        }
        else
        {
            m_HandCardControl[1].m_cbSelectInfoCount = cbInfoCount;
            bDone = false;
        }
        //如果完成操作
        if (bDone)
        {
            //设置操作结果
            tagSelectCardInfo_WZMJ si;
            m_HandCardControl[1].GetUserSelectResult(si);
            cbOperateCode = (BYTE)si.wActionMask;
            cbOperateCard[0] = si.cbActionCard;
            CopyMemory(&cbOperateCard[1], si.cbCardData, 2 * sizeof(BYTE));
        }
        //否则，设置等待选择
        else
        {
            StopTime();
            StartTime(TIME_OUT_CARD, GetMeChairID());
        }
    }
    
    //如果操作完成，直接发送操作命令
    if (bDone)
    {
        //删除定时器
        StopTime();
        
        //设置变量
        m_wCurrentUser = INVALID_CHAIR;
        m_cbActionMask = WIK_NULL;
        m_cbActionCard = 0;
        
        CMD_C_OperateCard_WZMJ OperateCard;
        OperateCard.cbOperateCode = cbOperateCode;
        if (cbOperateCard[0] == CARD_SPACE_DATA_MASK)
            OperateCard.cbOperateCard = m_bMagicCardData;
        else
            OperateCard.cbOperateCard = cbOperateCard[0];
        
        //CopyMemory(&OperateCard.cbOperateCard, cbOperateCard, sizeof(cbOperateCard));
        SendData(SUB_C_OPERATE_CARD_WZMJ, &OperateCard, sizeof(OperateCard));
    }
    
    return 0;
}

//获取操作信息
BYTE WZMJGameViewLayer::GetSelectCardInfo(WORD wOperateCode, tagSelectCardInfo_WZMJ SelectInfo[MAX_COUNT_WZMJ])
{
    //初始化
    BYTE cbSelectCount = 0;
    if (wOperateCode == WIK_NULL) return 0;
    
    //吃牌
    if (wOperateCode&(WIK_LEFT | WIK_CENTER | WIK_RIGHT))
    {
        //效验
        ASSERT(m_cbActionCard != 0);
        if (m_cbActionCard == 0) return 0;
        
        //替换白板 (换成财神计算牌值)
        if (m_cbActionCard == 0x37 && m_bMagicCardData != 0x37)
            m_cbActionCard = m_bMagicCardData;
        
        if (m_cbActionMask & WIK_LEFT & wOperateCode)
        {
            SelectInfo[cbSelectCount].cbActionCard = m_cbActionCard;
            SelectInfo[cbSelectCount].wActionMask = WIK_LEFT;
            SelectInfo[cbSelectCount].cbCardCount = 2;
            SelectInfo[cbSelectCount].cbCardData[0] = m_cbActionCard + 1;
            SelectInfo[cbSelectCount++].cbCardData[1] = m_cbActionCard + 2;
        }
        if (m_cbActionMask & WIK_CENTER & wOperateCode)
        {
            SelectInfo[cbSelectCount].cbActionCard = m_cbActionCard;
            SelectInfo[cbSelectCount].wActionMask = WIK_CENTER;
            SelectInfo[cbSelectCount].cbCardCount = 2;
            SelectInfo[cbSelectCount].cbCardData[0] = m_cbActionCard - 1;
            SelectInfo[cbSelectCount++].cbCardData[1] = m_cbActionCard + 1;
        }
        if (m_cbActionMask & WIK_RIGHT & wOperateCode)
        {
            SelectInfo[cbSelectCount].cbActionCard = m_cbActionCard;
            SelectInfo[cbSelectCount].wActionMask = WIK_RIGHT;
            SelectInfo[cbSelectCount].cbCardCount = 2;
            SelectInfo[cbSelectCount].cbCardData[0] = m_cbActionCard - 2;
            SelectInfo[cbSelectCount++].cbCardData[1] = m_cbActionCard - 1;
        }
        
        //替换白板	(换回来)
        if (m_cbActionMask & (WIK_RIGHT | WIK_LEFT | WIK_CENTER))
        {
            for (int i = 0; i < cbSelectCount; i++)
            {
                if (SelectInfo[i].cbActionCard == m_bMagicCardData)//m_GameLogic.SwitchToCardData(m_bMagicIndex))
                    SelectInfo[i].cbActionCard = 0x37;
                
                for (int j = 0; j < 2; j++)
                {
                    if (SelectInfo[i].cbCardData[j] == m_bMagicCardData)//m_GameLogic.SwitchToCardData(m_bMagicIndex))
                        SelectInfo[i].cbCardData[j] = 0x37;
                }
            }
        }
    }
    //碰牌
    else if (wOperateCode & WIK_PENG)
    {
        //效验
        ASSERT(m_cbActionCard != 0);
        if (m_cbActionCard == 0) return 0;
        SelectInfo[cbSelectCount].cbActionCard = m_cbActionCard;
        SelectInfo[cbSelectCount].wActionMask = WIK_PENG;
        SelectInfo[cbSelectCount].cbCardCount = 2;
        SelectInfo[cbSelectCount].cbCardData[0] = m_cbActionCard;
        SelectInfo[cbSelectCount++].cbCardData[1] = m_cbActionCard;
    }
    //杠牌
    else if (wOperateCode & WIK_GANG)
    {
        //寻找是否有多个杠牌
        WORD wMeChairId = GetMeChairID();
        tagGangCardResult gcr;
        BYTE cbCardIndex[MAX_INDEX];
        ZeroMemory(cbCardIndex, sizeof(cbCardIndex));
        CopyMemory(cbCardIndex, m_cbHandCardIndex, sizeof(m_cbHandCardIndex));
        cbCardIndex[m_GameLogic.SwitchToCardIndex(m_cbActionCard)]++;
        m_GameLogic.AnalyseGangCard(cbCardIndex, m_WeaveItemArray[wMeChairId], m_cbWeaveCount[wMeChairId], gcr);
        
        for (BYTE i = 0; i < gcr.cbCardCount; i++)
        {
            SelectInfo[cbSelectCount].cbActionCard = gcr.cbCardData[i];
            SelectInfo[cbSelectCount].wActionMask = WIK_GANG;
            if (m_cbHandCardIndex[m_GameLogic.SwitchToCardIndex(gcr.cbCardData[i])] == 1)
            {
                SelectInfo[cbSelectCount].cbCardCount = 1;
                SelectInfo[cbSelectCount].cbCardData[0] = gcr.cbCardData[i];
            }
            else
            {
                SelectInfo[cbSelectCount].cbCardCount = m_GameLogic.GetWeaveCard(WIK_GANG, gcr.cbCardData[i], SelectInfo[cbSelectCount].cbCardData);
            }
            cbSelectCount++;
        }
    }
    
    return cbSelectCount;
}

//买底消息
int WZMJGameViewLayer::OnChip(BYTE wParam)
{
    if (m_BtnNoBuy)
        m_BtnNoBuy->setVisible(false);
    if (m_BtnBuy)
        m_BtnBuy->setVisible(false);
    if (m_BtnDing)
        m_BtnDing->setVisible(false);
    
    //发送消息
    CMD_C_Chip_WZMJ	chip;
    ZeroMemory(&chip, sizeof(chip));
    chip.cbChipTimes = wParam;
    
    SendData(SUB_C_CHIP_WZMJ, &chip, sizeof(chip));
    
    return 0;
}

//财神 @xiehao
void WZMJGameViewLayer::caiShen(BYTE MagicCard)
{
    auto caishen1 = Sprite::createWithSpriteFrameName("CaiShen_bg.png");
    caishen1->setAnchorPoint(Vec2::ZERO);
    caishen1->setPosition(Vec2(1700, 650));
    addChild(caishen1, 2, 99);
    
    Sprite* caishenSpr = m_HandCardControl->GetCardSprite(MagicCard, false);
    if (caishenSpr != NULL)
    {
        caishenSpr->setPosition(Vec2(caishen1->getBoundingBox().size.width / 2, caishen1->getBoundingBox().size.height / 2 + 20));
        caishen1->addChild(caishenSpr);
    }
    else
    {
        assert("---------------------majiang = NULL---------------------");
    }
}

void WZMJGameViewLayer::backLoginView(Ref *pSender )
{
    IGameView::backLoginView(pSender);	
}

void WZMJGameViewLayer::GameEnd()
{
    //状态设置
    setGameStatus(GS_FREE);
    m_GameState = enGameEnd;
    StartTime(15, GetMeChairID());
    
    for (BYTE i = 0; i < GAME_PLAYER_WZMJ; i++)
    {
		if (ClientSocketSink::sharedSocketSink()->GetMeUserData() == NULL)
		{
			return;
		}
		int wtable = ClientSocketSink::sharedSocketSink()->GetMeUserData()->wTableID;
		if (wtable > 200) return;
		const tagUserData* pUserData = ClientSocketSink::sharedSocketSink()->m_UserManager.GetUserData(wtable, i);
		int chair = g_GlobalUnits.SwitchViewChairID(i);
        Label* gold = (Label*)getChildByTag(m_GlodTga[chair]);
        if (gold)
        {
			gold->setString(StringUtils::toString(pUserData->lScore));
        }
    }
}

void WZMJGameViewLayer::OnQuit()
{
    m_pGameScene->removeChild(this);
}
