#ifndef CMD_WZMJ_GAME_HEAD_FILE
#define CMD_WZMJ_GAME_HEAD_FILE

#include "def.h"

#pragma pack(push)
#pragma pack(1)

//数组维数
#define CountArray(Array) (sizeof(Array)/sizeof(Array[0]))
#define CopyMemory(Destination,Source,Length) memcpy((Destination),(Source),(Length))
#define SW_SHOW         true
#define SW_HIDE         false
//////////////////////////////////////////////////////////////////////////////////
//服务定义
namespace WZMJ
{
	//游戏属性
#define WZMJ_KIND_ID				400									//游戏 I D
#define GAME_NAME					TEXT("温州麻将")						//游戏名字

	//组件属性
#define GAME_PLAYER_WZMJ			2                                   //游戏人数
#define VERSION_SERVER				PROCESS_VERSION(6,0,3)				//程序版本
#define VERSION_CLIENT				PROCESS_VERSION(6,0,3)				//程序版本

	//////////////////////////////////////////////////////////////////////////////////
	//游戏定时器
#define TIME_CHIP					10									//买底定时器
#define TIME_OUT_CARD               18									//出牌定时器
#define TIME_ACTION_CARD			8									//动作定时器

#define MAX_TIME_OUT				3									//最大超时次数

	//按钮标识
#ifndef _UNICODE
#define myprintf	_snprintf
#define mystrcpy	strcpy
#define mystrlen	strlen
#define myscanf		_snscanf
#define	myLPSTR		LPCSTR
#else
#define myprintf	swprintf
#define mystrcpy	wcscpy
#define mystrlen	wcslen
#define myscanf		_snwscanf
#define	myLPSTR		LPWSTR
#endif

	/////////////////////////////////////////////////////////////////////////////////////
	//状态定义

#define GAME_SCENE_FREE				GS_FREE                             //等待开始
#define GAME_SCENE_CHIP				(GS_PLAYING+1)                      //买底状态
#define GAME_SCENE_PLAY				(GS_PLAYING+2)                      //游戏进行

//常量定义
#define MAX_WEAVE					5									//最大组合
#define MAX_INDEX					42									//最大索引
#define MAX_COUNT_WZMJ				17									//最大数目
#define MAX_REPERTORY				136									//最大库存
#define MAX_HUA_CARD				8									//花牌个数

	//扑克定义
#define HEAP_FULL_COUNT				34									//堆立全牌

#define MAX_RIGHT_COUNT				3									//最大权位DWORD个数

#define SERVER_LEN				    32

    //常量定义
#define CARD_KING_INDEX				MAX_INDEX-1                         //王牌索引
#define CARD_KING_DATA_MASK			0x80                                //王牌数据蒙板
#define CARD_SPACE_DATA_MASK		0x37                                //白板数据蒙板

    
#define SCROE_WAN			10000
#define ROOM_LEVEL_JUNIOR   1*SCROE_WAN
#define ROOM_LEVEL_MIDDLE	50*SCROE_WAN
#define ROOM_LEVEL_SENIOR	500*SCROE_WAN
	//////////////////////////////////////////////////////////////////////////

	//组合子项
	struct CMD_WeaveItem_WZMJ
	{
		BYTE							cbWeaveKind;						//组合类型
		BYTE							cbCenterCard;						//中心扑克
		BYTE							cbPublicCard;						//公开标志
		WORD							wProvideUser;						//供应用户
	};

	//////////////////////////////////////////////////////////////////////////
	//服务器命令结构

#define WZMJ_SUB_S_GAME_START			100									//游戏开始
#define WZMJ_SUB_S_OUT_CARD				101									//出牌命令
#define WZMJ_SUB_S_SEND_CARD			102									//发送扑克
#define WZMJ_SUB_S_OPERATE_NOTIFY		104									//操作提示
#define WZMJ_SUB_S_OPERATE_RESULT		105									//操作命令
#define WZMJ_SUB_S_GAME_END				106									//游戏结束
#define WZMJ_SUB_S_CHIP                 107									//买底结果


	struct CMD_S_TopUserCard_WZMJ
	{
		BYTE             cbCard[MAX_COUNT_WZMJ];
		BYTE             cbCount;
	};

	//空闲状态
	struct CMD_S_StatusFree_WZMJ
	{
		LONGLONG						lCellScore;							//基础金币
		WORD							wBankerUser;						//庄家用户
		bool							bTrustee[GAME_PLAYER_WZMJ];			//是否托管
        LONGLONG                        lAllTurnScore;                      //总局得分
        LONGLONG                        lLastTurnScore;                     //上局得分
        bool							bisScore;                           //是否积分房间
	};

	//买底状态
	struct CMD_S_StatusChip_WZMJ
	{
		LONGLONG						lCellScore;							//基础金币
		WORD							wBankerUser;						//庄家用户
        BYTE							bComplete;							//完成标志
		bool							bTrustee[GAME_PLAYER_WZMJ];			//是否托管
        
        //历史积分
        LONGLONG                        lAllTurnScore[GAME_PLAYER_WZMJ];	//总局得分
        LONGLONG                        lLastTurnScore[GAME_PLAYER_WZMJ];	//上局得分
        bool                            bisScore;                           //是否积分房间

	};

	//游戏状态
	struct CMD_S_StatusPlay_WZMJ
	{
        //游戏变量
        LONGLONG						lCellScore;							//单元积分
        WORD							wSiceCount;							//骰子点数
        WORD							wSiceCount2;						//骰子点数
        WORD							wBankerUser;						//庄家用户
        WORD							wCurrentUser;						//当前用户
        BYTE							cbChip[GAME_PLAYER_WZMJ];			//买顶底分数
        bool							bTrustee[GAME_PLAYER_WZMJ];			//是否托管
        BYTE							cbBankContinueCount;
        
        //风牌记录
        BYTE							cbFengCardData[8];					//风牌记录
        BYTE							cbFengCardCount;					//风牌记录
        
        //状态变量
        BYTE							cbActionCard;						//动作扑克
        BYTE							cbActionMask;						//动作掩码
        BYTE							cbLeftCardCount;					//剩余数目
        
        //出牌信息
        WORD							wOutCardUser;						//出牌用户
        BYTE							cbOutCardData;						//出牌扑克
        BYTE							cbDiscardCount[GAME_PLAYER_WZMJ];	//丢弃数目
        BYTE							cbDiscardCard[GAME_PLAYER_WZMJ][60];//丢弃记录
        
        //扑克数据
        BYTE							cbCardCount;						//扑克数目
        BYTE							cbKingCardData;						//王牌扑克
        BYTE							cbCardData[MAX_COUNT_WZMJ];			//扑克列表
        BYTE							cbSendCardData;						//发送扑克
        
        //组合扑克
        BYTE							cbWeaveCount[GAME_PLAYER_WZMJ];		//组合数目
        CMD_WeaveItem_WZMJ				WeaveItemArray[GAME_PLAYER_WZMJ][5];//组合扑克
        LONGLONG                        lAllTurnScore[GAME_PLAYER_WZMJ];	//总局得分
        LONGLONG                        lLastTurnScore[GAME_PLAYER_WZMJ];	//上局得分
        bool                            bisScore;                           //是否积分房间
	};

	//买底消息
	struct CMD_S_Chip_WZMJ
	{
		WORD							wBankerUser;						//庄家位置
	};

	//游戏开始
	struct CMD_S_GameStart_WZMJ
	{
        WORD							wSiceCount;							//骰子点数
        WORD							wSiceCount2;						//骰子点数2

		WORD							wBankerUser;						//庄家用户
		WORD							wCurrentUser;						//当前用户
		
		BYTE							cbUserAction;						//用户动作
        BYTE							cbChip[GAME_PLAYER_WZMJ];			//买顶底分数
        BYTE							cbBankContinueCount;
        BYTE							cbKingCardData;						//王牌扑克
        BYTE							cbCardData[MAX_COUNT_WZMJ];			//扑克列表
        
        BYTE							cbDuijiaCardData[MAX_COUNT_WZMJ];	//扑克列表
        bool							bTrustee[GAME_PLAYER_WZMJ];			//是否托管
        LONGLONG						basescore;
	};

	//出牌命令
	struct CMD_S_OutCard_WZMJ
	{
		WORD							wOutCardUser;						//出牌用户
		BYTE							cbOutCardData;						//出牌扑克
	};

	//发送扑克
	struct CMD_S_SendCard_WZMJ
	{
		BYTE							cbCardData;							//扑克数据
		BYTE							cbActionMask;						//动作掩码
		WORD							wCurrentUser;						//当前用户
		bool							cbIsNotGang;						//是否杠牌
	};

	//操作提示
	struct CMD_S_OperateNotify_WZMJ
	{
		WORD							wResumeUser;						//还原用户
		BYTE							cbActionMask;						//动作掩码
		BYTE							cbActionCard;						//动作扑克
        WORD							wOperateuser;                       //操作用户
	};

	//操作命令
	struct CMD_S_OperateResult_WZMJ
	{
		WORD							wOperateUser;						//操作用户
		WORD							wProvideUser;						//供应用户
		BYTE							cbOperateCode;						//操作代码
		BYTE							cbOperateCard;                      //操作扑克
	};

    //游戏结束
    struct CMD_S_GameEnd_WZMJ
    {
        LONGLONG						lGameTax;							//游戏税收
        BYTE							cbChiHuCard;						//吃胡扑克
        UINT32							bisZiMo;							//是否自摸
        BYTE							BankContinueCount;

        
        WORD							wProvideUser;					    	//点炮用户
        WORD							wwChiHuKind;					    	//吃胡类型
        WORD							wChiHuRight;					    	//胡牌权位
        LONGLONG						lGameScore[GAME_PLAYER_WZMJ];			//游戏积分
        BYTE							cbCardCount[GAME_PLAYER_WZMJ];          //扑克数目
        WORD							wChiHuKind[GAME_PLAYER_WZMJ];			//胡牌类型
        BYTE							cbCardData[GAME_PLAYER_WZMJ][MAX_COUNT_WZMJ];	    //扑克数据
        int								nKing[GAME_PLAYER_WZMJ];				//财神个数
        UINT32							bisKing;								//是否算财神
        LONGLONG						lAllTurnScore[GAME_PLAYER_WZMJ];		//总局得分
        LONGLONG						lLastTurnScore[GAME_PLAYER_WZMJ];		//上局得分
        bool							isRunAway;								//是否逃跑
    };

	//用户托管
	struct CMD_S_Trustee_WZMJ
	{
		bool							bTrustee;							//是否托管
		WORD							wChairID;							//托管用户
	};

	//用户听牌
	struct CMD_S_Listen_WZMJ
	{
		WORD							wChairId;							//听牌用户
	};

	//补牌命令
	struct CMD_S_ReplaceCard_WZMJ
	{
		WORD							wReplaceUser;						//补牌用户
		BYTE							cbReplaceCard;						//补牌扑克
	};

	//////////////////////////////////////////////////////////////////////////
	//客户端命令结构

#define SUB_C_OUT_CARD_WZMJ				1									//出牌命令
#define SUB_C_CHIP_WZMJ					2									//用户买底
#define SUB_C_OPERATE_CARD_WZMJ			3									//操作扑克
#define SUB_C_TRUSTEE_WZMJ				4									//用户托管


	//出牌命令
	struct CMD_C_OutCard_WZMJ
	{
		BYTE							cbCardData;							//扑克数据
	};

	//操作命令
	struct CMD_C_OperateCard_WZMJ
	{
		BYTE							cbOperateCode;						//操作代码
		BYTE							cbOperateCard;                      //操作扑克
	};

	//用户听牌
	struct CMD_C_Listen_WZMJ
	{
		BYTE							cbListen;							//听牌用户
	};

	//用户托管
	struct CMD_C_Trustee_WZMJ
	{
		bool							bTrustee;							//是否托管	
	};

	//买底消息
	struct CMD_C_Chip_WZMJ
	{
        BYTE							cbChipTimes;						//买顶倍数
        WORD							wCurrentUser;						//当前用户
        BYTE							cbCheatNumber;						//牌数
	};

	//////////////////////////////////////////////////////////////////////////

#pragma pack(pop)

}

#endif
