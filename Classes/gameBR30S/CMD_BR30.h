#ifndef CMD_BR30_HEAD_FILE
#define CMD_BR30_HEAD_FILE
#pragma pack(1)

#define BR30S_KIND_ID				122                                 //游戏 I D
#define BR30S_GAME_PLAYER			150                                 //游戏人数
#define BR30S_GAME_NAME				TEXT("")                            //游戏名字
#define BR30S_MAX_COUNT				3                                   //最大数目
#define VERSION_SERVER				PROCESS_VERSION(6,0,3)				//程序版本
#define VERSION_CLIENT				PROCESS_VERSION(6,0,3)				//程序版本

//状态定义
#define GAME_SCENE_FREE				GS_FREE                             //等待开始
#define GAME_SCENE_PLAY				GS_PLAYING                          //游戏进行
#define GAME_SCENE_BET				GS_PLAYING                          //下注状态
#define	GAME_SCENE_END				GS_PLAYING+1                        //结束状态


//玩家索引
#define AREA_INVALE					-1									//无效
#define AREA_XIAN					0									//闲家索引
#define AREA_PING					1									//平家索引
#define AREA_ZHUANG					2									//庄家索引
#define AREA_LONG					3									//龙, 闲天王
#define AREA_HU						4									//虎, 庄天王
#define AREA_TONG_DUI				5									//同点平
#define AREA_XIAN_DUI				6									//闲对子
#define AREA_ZHUANG_DUI				7									//庄对子
#define AREA_MAX					8									//最大区域

//区域倍数multiple
#define MULTIPLE_XIAN				1									//闲家倍数
#define MULTIPLE_PING				8									//平家倍数
#define MULTIPLE_ZHUANG				1									//庄家倍数
#define MULTIPLE_XIAN_TIAN			2									//闲天王倍数
#define MULTIPLE_ZHUANG_TIAN		2									//庄天王倍数
#define MULTIPLE_TONG_DIAN			32									//同点平倍数
#define MULTIPLE_XIAN_PING			11									//闲对子倍数
#define MULTIPLE_ZHUANG_PING		11									//庄对子倍数


//赔率定义
#define RATE_TWO_PAIR				12									//对子赔率
#define SERVER_LEN					32									//房间长度

//记录信息
struct BR30S_tagServerGameRecord
{
    BYTE		cbWinner[2];										//闲家,平家,庄家
    BYTE		cbTwoPair[2];										//对子
    BYTE		cbPoint[2];											//点数
    LONGLONG 	cbGameTimes;										//第几局
};

//////////////////////////////////////////////////////////////////////////
//服务器命令结构

#define BR30S_SUB_S_GAME_FREE				99									//游戏空闲
#define BR30S_SUB_S_GAME_START				100									//游戏开始
#define BR30S_SUB_S_PLACE_JETTON			101									//用户下注
#define BR30S_SUB_S_GAME_END				102									//游戏结束
#define BR30S_SUB_S_APPLY_BANKER			103									//申请庄家
#define BR30S_SUB_S_CHANGE_BANKER			104									//切换庄家
#define BR30S_SUB_S_CHANGE_USER_SCORE		105									//更新积分
#define BR30S_SUB_S_SEND_RECORD				106									//游戏记录
#define BR30S_SUB_S_PLACE_JETTON_FAIL		107									//下注失败
#define BR30S_SUB_S_CANCEL_BANKER			108									//取消申请
#define BR30S_SUB_S_AMDIN_COMMAND			109									//历史记录
#define BR30S_SUB_S_COMPTETE_BANKER			110									//抢庄家
#define BR30S_SUB_S_START_SIDE				111									//抢庄家

//请求回复
struct CMD_S_CommandResult
{
	BYTE cbAckType;					//回复类型
#define ACK_SET_WIN_AREA  1
#define ACK_PRINT_SYN     2
#define ACK_RESET_CONTROL 3
	BYTE cbResult;
#define CR_ACCEPT  2			//接受
#define CR_REFUSAL 3			//拒绝
	BYTE cbExtendData[20];			//附加数据
};

//失败结构
struct CMD_S_PlaceBetFail
{
	BYTE							lBetArea;							//下注区域
	LONGLONG						lPlaceScore;						//当前下注
};

//申请庄家
struct CMD_S_ApplyBanker
{
	WORD							wApplyUser;							//申请玩家
    int								nRobBanker;							//抢庄的人
};

//取消申请
struct CMD_S_CancelBanker
{
    CHAR							szCancelUser[32];					//取消玩家
    int								nRobBanker;							//抢庄的人
    bool							bisRobBanker;
};

//切换庄家
struct CMD_S_ChangeBanker
{
	WORD							wBankerUser;						//当庄玩家
	LONGLONG						lBankerScore;						//庄家分数
    int								nRobBanker;							//抢庄的人
};

//游戏状态
struct BR30S_CMD_S_StatusFree
{
    //全局信息
    BYTE							cbTimeLeave;						//剩余时间
    
    //玩家信息
    LONGLONG						lUserMaxScore;						//玩家金币
    
    //庄家信息
    WORD							wBankerUser;						//当前庄家
    WORD							cbBankerTime;						//庄家局数
    LONGLONG						lBankerWinScore;					//庄家成绩
    LONGLONG						lBankerScore;						//庄家分数
    bool							bEnableSysBanker;					//系统做庄
    
    //控制信息
    LONGLONG						lApplyBankerCondition;				//申请条件
    LONGLONG						lAreaLimitScore[AREA_MAX];          //区域限制
};

//游戏状态
struct BR30S_CMD_S_StatusPlay
{
    //全局下注
    LONGLONG						lTotalAreaScore[AREA_MAX];
    
    //玩家下注
    LONGLONG						lUserAreaScore[AREA_MAX];
    
    //各区域限注
    LONGLONG						lAreaScoreLimit[AREA_MAX];
    
    //玩家积分
    LONGLONG						lUserMaxScore;					//最大下注
    
    //控制信息
    LONGLONG						lApplyBankerCondition;			//申请条件
    
    //扑克信息
    BYTE							cbTableCardArray[2][BR30S_MAX_COUNT];	//桌面扑克
    BYTE							cbCardCount[2];					//扑克数量
    //庄家信息
    WORD							wBankerUser;					//当前庄家
    WORD							cbBankerTime;					//庄家局数
    LONGLONG						lBankerWinScore;				//庄家赢分
    LONGLONG						lBankerScore;					//庄家分数
    bool							bEnableSysBanker;				//系统做庄
    
    //结束信息
    LONGLONG						lEndBankerScore;				//庄家成绩
    LONGLONG						lEndUserScore;					//玩家成绩
    LONGLONG						lEndUserReturnScore;			//返回积分
    LONGLONG						lEndRevenue;					//游戏税收
    
    //全局信息
    BYTE							cbTimeLeave;					//剩余时间
    BYTE							cbGameStatus;					//游戏状态
};

//游戏空闲
struct BR30S_CMD_S_GameFree
{
    //全局信息
    BYTE							cbTimeLeave;						//剩余时间
    LONGLONG						lGameTimes;							//当前是游戏启动以来的第几局
};

//游戏开始
struct BR30S_CMD_S_GameStart
{
    WORD							wBankerUser;							//庄家位置
    LONGLONG						lBankerScore;							//庄家金币
    LONGLONG						lUserMaxScore;							//我的金币
    BYTE							cbTimeLeave;							//剩余时间
    
    LONGLONG						lAreaLimitScore[AREA_MAX];              //各区域可下分
};

struct BR30S_CMD_S_GameSide
{
	WORD							wBetUser;						//庄家位置
	BYTE							cbbetArea;
};

//用户下注
struct BR30S_CMD_S_PlaceBet
{
    WORD							wChairID;							//用户位置
    BYTE							cbJettonArea;						//筹码区域
    LONGLONG						lJettonScore;						//加注数目
};

//游戏结束
struct BR30S_CMD_S_GameEnd
{
	//下局信息
	BYTE							cbTimeLeave;						//剩余时间

	//扑克信息
	BYTE							cbTableCardArray[2][3];				//桌面扑克
	BYTE							cbCardCount[2];						//扑克数目

    //庄家信息
	LONGLONG						lBankerScore;						//庄家成绩
	LONGLONG						lBankerTotallScore;					//庄家成绩
	INT								nBankerTime;						//做庄次数

	//玩家成绩
	LONGLONG						lPlayScore;                         //玩家成绩
	LONGLONG						lPlayAllScore;						//返回积分

	//全局信息
	LONGLONG						lRevenue;							//游戏税收
};

//////////////////////////////////////////////////////////////////////////
//客户端命令结构

#define SUB_C_PLACE_JETTON			1									//用户下注
#define SUB_C_APPLY_BANKER			2									//申请庄家
#define SUB_C_CANCEL_BANKER			3									//取消申请
#define SUB_C_AMDIN_COMMAND			4									//管理员命令
#define SUB_C_COMPTETE_BANKER		5									//抢庄家
#define SUB_C_RECORD               10									//游戏记录

struct CMD_C_AdminReq
{
	BYTE cbReqType;
#define RQ_SET_WIN_AREA             1
#define RQ_RESET_CONTROL            2
#define RQ_PRINT_SYN                3
	BYTE cbExtendData[20];			//附加数据
};


//用户下注
struct CMD_C_PlaceBet
{
	BYTE							cbBetArea;						//筹码区域
	LONGLONG						lBetScore;						//加注数目
};

//////////////////////////////////////////////////////////////////////////
#pragma pack()
#endif
