﻿#include "BR30SGameViewLayer.h"
#include "LobbyLayer.h"
#include "ClientSocketSink.h"
#include "HXmlParse.h"
#include "LocalDataUtil.h"
#include "JniSink.h"
#include "VisibleRect.h"
#include "HttpConstant.h"
#include "Screen.h"
#include "LobbySocketSink.h"
#include "LoginLayer.h"
#include "GameLayerMove.h"
#include "GlobalDef.h"
#include "def.h"
#include "convert.h"

#define		MENU_INIT_POINT			(Vec2Make(-_STANDARD_SCREEN_CENTER_.x+68+30 , _STANDARD_SCREEN_CENTER_.y-68-20))

#define		ADV_SIZE				(CCSizeMake(520,105))
//宝箱坐标
#define		BOX_POINT				(Vec2(1195,70))

//最少筹码数
#define		MIN_PROP_PAY_SCORE			1
#define		ALL_ANIMATION_COUNT			5


#define		MAX_CREDIT					(9999999)
#define		MAX_BET						(9999)
#define		MAX_ADV_COUNT				(10)

#if (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
#define		BIG_AWARD_MULITY			10
#else
#define		BIG_AWARD_MULITY			30
#endif // _DEBUG

#define Btn_Ready						1
#define Btn_BackToLobby					2
#define Btn_Seting						3
#define Btn_CallBank					4
#define Btn_100                         5
#define Btn_1000						6
#define Btn_1w							7
#define Btn_10w							8
#define Btn_100w						9
#define Btn_500w						10
#define Btn_1000w						11
#define Btn_CancleBank					12
#define Btn_Uplist						13
#define Btn_Downlist					14
#define Btn_LZ							15
#define Btn_LzClose						16
#define Btn_LzLeft						17
#define Btn_LzRight						18
#define Btn_BankBtn						20
#define Btn_PX							21
#define Btn_GetScoreBtn					22


BR30SGameViewLayer::BR30SGameViewLayer(GameScene *pGameScene)
	:IGameView(pGameScene)
{
	m_GameState=enGameNormal;
	for (int i = 0; i < 8; i++)
	{
		m_lUserJettonScore[i] = 0;
		m_lAllJettonScore[i]  = 0;
	}
	m_lBankerScore = 0;
	m_BankZhangJI = 0;
	m_BankJuShu = 0;
	m_CurSelectGold = 0;
	m_lPlayAllScore = 0;
	m_lBankerCurGameScore = 0;
	m_lApplyBankerCondition = 0;
	m_LzShow = false;
	m_Index1 = 0;
	m_Index2 = 0;
	m_Index3 = 0;
	//m_nRecordFirst= 0;	
	m_nRecordLast= 0;	
	m_bBetSound = false;
	m_OpenBankList = false;
	m_BankUserListSize = 0;
	m_bLz_PX = false;
	m_IsMeBank = false;
	SetKindId(BR30S_KIND_ID);
}

BR30SGameViewLayer::~BR30SGameViewLayer() 
{

}

BR30SGameViewLayer *BR30SGameViewLayer::create(GameScene *pGameScene)
{
    BR30SGameViewLayer *pLayer = new BR30SGameViewLayer(pGameScene);
    if(pLayer && pLayer->init())
    {
        pLayer->autorelease();
        return pLayer;
    }
    else
    {
        CC_SAFE_DELETE(pLayer);
        return NULL;
    }
}


bool BR30SGameViewLayer::init()
{
	if ( !Layer::init() )
	{
		return false;
	}
	setLocalZOrder(3);

	Tools::addSpriteFrame("BR30S/BR30S.plist");
	Tools::addSpriteFrame("Common/CardSprite2.plist");
	SoundUtil::sharedEngine()->playBackMusic("BR30S/BACK_GROUND", true);
    SoundUtil::sharedEngine()->setBackSoundVolume(g_GlobalUnits.m_fBackMusicValue);
    SoundUtil::sharedEngine()->setSoundVolume(g_GlobalUnits.m_fSoundValue);
    
	IGameView::onEnter();
	JniSink::share()->setIGameView(this);
	setGameStatus(GS_FREE);

	InitGame();
	AddButton();

	AddPlayerInfo();

	setTouchEnabled(true);

	auto listener = EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(true);
	listener->onTouchBegan = CC_CALLBACK_2(BR30SGameViewLayer::onTouchBegan, this);
	listener->onTouchMoved = CC_CALLBACK_2(BR30SGameViewLayer::onTouchMoved, this);
	listener->onTouchEnded = CC_CALLBACK_2(BR30SGameViewLayer::onTouchEnded, this);
	_eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);

	return true;
}

void BR30SGameViewLayer::onEnter ()
{	
	
}

void BR30SGameViewLayer::StartTime(int _time, int _type)
{
	m_StartTime = _time;
	schedule(schedule_selector(BR30SGameViewLayer::UpdateTime), 1);
	m_ClockSpr->setVisible(true);
	UpdateTime(m_StartTime);

	removeAllChildByTag(m_TimeType_Tga);
    std::string szTempTitle;
    if (_type == 0)	szTempTitle = "\u7a7a\u95f2\u65f6\u95f4";
    else if(_type == 1) szTempTitle = "\u4e0b\u6ce8\u65f6\u95f4";
    else if(_type == 2) szTempTitle = "\u5f00\u724c\u65f6\u95f4";
#ifdef _WIN32
    Tools::GBKToUTF8(szTempTitle , "gb2312" , "utf-8");
#endif
    Label* temp = Label::createWithSystemFont(szTempTitle.c_str(),_GAME_FONT_NAME_1_,46);
    temp->setColor(_GAME_FONT_COLOR_5_);
    temp->setPosition(Vec2(850, 1040));
    temp->setTag(m_TimeType_Tga);
    addChild(temp);
}

void BR30SGameViewLayer::StopTime()
{
	unschedule(schedule_selector(BR30SGameViewLayer::UpdateTime));
	m_ClockSpr->removeAllChildren();
	m_ClockSpr->setVisible(false);
	removeAllChildByTag(m_TimeType_Tga);
}

void BR30SGameViewLayer::UpdateTime(float fp)
{
	//下注时间
	if (getGameStatus() == GAME_SCENE_BET)
	{
		if(m_StartTime == 13) SoundUtil::sharedEngine()->playEffect("BR30S/bet1"); 
		else if(m_StartTime == 2) SoundUtil::sharedEngine()->playEffect("BR30S/BetEnd"); 
		
		if(m_StartTime == 13) removeAllChildByTag(m_StartBetText_Tga);
		else if (m_StartTime == 8)
		{
			removeAllChildByTag(m_StartBetText_Tga);
			Sprite* temp = Sprite::createWithSpriteFrameName("Bet_WillEndSoon.png");
			temp->setPosition(Vec2(_STANDARD_SCREEN_CENTER_.x,540));
			temp->setTag(m_StartBetText_Tga);
			addChild(temp,1000);
		}
		else if (m_StartTime == 2)
		{
			removeAllChildByTag(m_StartBetText_Tga);
			Sprite* temp = Sprite::createWithSpriteFrameName("BetEnd.png");
			temp->setPosition(Vec2(_STANDARD_SCREEN_CENTER_.x, 540));
			temp->setTag(m_StartBetText_Tga);
			addChild(temp);
            m_CurSelectGold = 0;
		}

	}
	if (m_StartTime <= 0)
	{
		unschedule(schedule_selector(BR30SGameViewLayer::UpdateTime));
	}
	m_ClockSpr->removeAllChildren();

    std::string str = StringUtils::toString(m_StartTime);
    if (m_StartTime < 10)
        str = "0" + str;
    LabelAtlas *time = LabelAtlas::create(str, "Common/time_1.png", 23, 35, '+');
    time->setPosition(Vec2(20, 25));
    m_ClockSpr->addChild(time);
    
    m_StartTime--;

}

void BR30SGameViewLayer::onExit()
{
	Tools::removeSpriteFrameCache("BR30S/BR30S.plist");
	Tools::removeSpriteFrameCache("Common/CardSprite2.plist");
	IGameView::onExit();
}

// Touch 触发
bool BR30SGameViewLayer::onTouchBegan(Touch *pTouch, Event *pEvent)
{
	if(!isVisible()) return true;
    if (m_OpenBankList)
    {
        CloseBankList();
        return false;
    }
    else if(m_bLz_PX)
    {
        CloseLzList();
        return false;
    }
    Vec2 touchLocation = pTouch->getLocation(); // 返回GL坐标
	Vec2 localPos = convertToNodeSpace(touchLocation);
	if (getGameStatus() != GAME_SCENE_BET) return true;
	for (int i = AREA_XIAN; i < AREA_MAX; i++)
	{
		Node * temp= getChildByTag(m_Click_Tga[i]);
		if (temp == NULL) continue;
		Rect rc = temp->getBoundingBox();
		bool isTouched = rc.containsPoint(localPos);
		if (isTouched)
		{
			if (m_CurSelectGold == 0) return true;
			SoundUtil::sharedEngine()->playEffect("buttonMusic");
			int cbJettonArea = i;
			//变量定义
			CMD_C_PlaceBet PlaceJetton;
			ZeroMemory(&PlaceJetton,sizeof(PlaceJetton));

			//构造变量
			PlaceJetton.cbBetArea=cbJettonArea;
			PlaceJetton.lBetScore=m_CurSelectGold;
		
			if (PlaceJetton.lBetScore >=1000000) SoundUtil::sharedEngine()->playEffect("BR30S/ADD_GOLD_EX");
			else SoundUtil::sharedEngine()->playEffect("BR30S/bet");

			//发送消息
			SendData(SUB_C_PLACE_JETTON,&PlaceJetton,sizeof(CMD_C_PlaceBet));
		}
	}

	return true;
}

//更新控制
void BR30SGameViewLayer::UpdateButtonContron()
{
	//不是下注状态,自己是庄家
	if (getGameStatus() != GAME_SCENE_BET || m_wCurrentBanker == GetMeChairID() || m_bNoBankUser == true)
	{
		removeAllChildByTag(m_BtnAnim_Tga);
		m_Gold1000->setEnabled(false);
		m_Gold1w->setEnabled(false);
		m_Gold10w->setEnabled(false);
		m_Gold100w->setEnabled(false);
		m_Gold500w->setEnabled(false);
		m_Gold1000->setColor(Color3B(100,100,100));
		m_Gold1w->setColor(Color3B(100,100,100));
		m_Gold10w->setColor(Color3B(100,100,100));
		m_Gold100w->setColor(Color3B(100,100,100));
		m_Gold500w->setColor(Color3B(100,100,100));
	}
	else
	{

		//计算积分
		LONGLONG lLeaveScore = m_lMeMaxScore;
		for (int nAreaIndex=1; nAreaIndex<=AREA_MAX; ++nAreaIndex)
            lLeaveScore -= m_lUserJettonScore[nAreaIndex];

		//最大下注
		LONGLONG lUserMaxJetton = 0;

		for (int nAreaIndex=1; nAreaIndex<=AREA_MAX; ++nAreaIndex)
		{
			if(lUserMaxJetton==0&&nAreaIndex == 1)
			{
				lUserMaxJetton = GetUserMaxJetton(nAreaIndex);

			}else
			{
				if(GetUserMaxJetton(nAreaIndex)>lUserMaxJetton)
				{
					lUserMaxJetton = GetUserMaxJetton(nAreaIndex);
				}
			}
		}
		if (lLeaveScore > lUserMaxJetton) lLeaveScore = lUserMaxJetton;

		//控制按钮
		int iTimer = 1;

        
		if(lLeaveScore >= 1000*iTimer && lUserMaxJetton >= 1000*iTimer)
		{
			m_Gold1000->setEnabled(true);
			m_Gold1000->setColor(Color3B(255,255,255));
		}
		else
		{
			m_Gold1000->setEnabled(false);
			m_Gold1000->setColor(Color3B(100,100,100));
		}
		if(lLeaveScore>=10000*iTimer && lUserMaxJetton>=10000*iTimer)
		{
			m_Gold1w->setEnabled(true);
			m_Gold1w->setColor(Color3B(255,255,255));
		}
		else
		{
			m_Gold1w->setEnabled(false);
			m_Gold1w->setColor(Color3B(100,100,100));
		}
		if(lLeaveScore>=100000*iTimer && lUserMaxJetton>=100000*iTimer)
		{
			m_Gold10w->setEnabled(true);
			m_Gold10w->setColor(Color3B(255,255,255));
		}
		else
		{
			m_Gold10w->setEnabled(false);
			m_Gold10w->setColor(Color3B(100,100,100));
		}
		if(lLeaveScore>=1000000*iTimer && lUserMaxJetton>=1000000*iTimer)
		{
			m_Gold100w->setEnabled(true);
			m_Gold100w->setColor(Color3B(255,255,255));
		}
		else
		{
			m_Gold100w->setEnabled(false);
			m_Gold100w->setColor(Color3B(100,100,100));
		}
		if(lLeaveScore>=5000000*iTimer && lUserMaxJetton>=5000000*iTimer)
		{
			m_Gold500w->setEnabled(true);
			m_Gold500w->setColor(Color3B(255,255,255));
		}
		else
		{
			m_Gold500w->setEnabled(false);
			m_Gold500w->setColor(Color3B(100,100,100));
		}
	}

	return;
}

//最大下注
LONGLONG BR30SGameViewLayer::GetUserMaxJetton(BYTE cbBetArea)
{
	if (GetMeChairID() == INVALID_CHAIR) return 0L;
	tagUserData* pMeUserItem = ClientSocketSink::sharedSocketSink()->GetMeUserData();
	if ( pMeUserItem == NULL || cbBetArea >= AREA_MAX )return 0L;

	if( cbBetArea >= AREA_MAX ) return 0L;

	//已下注额
	LONGLONG lNowBet = 0l;
	for (int nAreaIndex = 0; nAreaIndex < AREA_MAX; ++nAreaIndex ) 
		lNowBet += m_lUserJettonScore[nAreaIndex];

	//庄家金币
	LONGLONG lBankerScore = -1;

	//区域倍率
	BYTE cbMultiple[AREA_MAX] = {MULTIPLE_XIAN, MULTIPLE_PING, MULTIPLE_ZHUANG, 
		MULTIPLE_XIAN_TIAN, MULTIPLE_ZHUANG_TIAN, MULTIPLE_TONG_DIAN, 
		MULTIPLE_XIAN_PING, MULTIPLE_ZHUANG_PING};

	//区域输赢
	BYTE cbArae[4][4] = {	{ AREA_XIAN_DUI,	255,			AREA_MAX,			AREA_MAX }, 
	{ AREA_ZHUANG_DUI,	255,			AREA_MAX,			AREA_MAX }, 
	{ AREA_XIAN,		AREA_PING,		AREA_ZHUANG,		AREA_MAX },  
	{ AREA_LONG,	AREA_TONG_DUI,	AREA_HU,	255 }};
	//筹码设定
	for ( int nTopL = 0; nTopL < 4; ++nTopL )
	{
		if( cbArae[0][nTopL] == AREA_MAX )
			continue;

		for ( int nTopR = 0; nTopR < 4; ++nTopR )
		{
			if( cbArae[1][nTopR] == AREA_MAX )
				continue;

			for ( int nCentral = 0; nCentral < 4; ++nCentral )
			{
				if( cbArae[2][nCentral] == AREA_MAX )
					continue;

				for ( int nBottom = 0; nBottom < 4; ++nBottom )
				{
					if( cbArae[3][nBottom] == AREA_MAX )
						continue;

					BYTE cbWinArea[AREA_MAX] = {FALSE};

					//指定获胜区域
					if ( cbArae[0][nTopL] != 255 && cbArae[0][nTopL] != AREA_MAX )
						cbWinArea[cbArae[0][nTopL]] = TRUE;

					if ( cbArae[1][nTopR] != 255 && cbArae[1][nTopR] != AREA_MAX )
						cbWinArea[cbArae[1][nTopR]] = TRUE;

					if ( cbArae[2][nCentral] != 255 && cbArae[2][nCentral] != AREA_MAX )
						cbWinArea[cbArae[2][nCentral]] = TRUE;

					if ( cbArae[3][nBottom] != 255 && cbArae[3][nBottom] != AREA_MAX )
						cbWinArea[cbArae[3][nBottom]] = TRUE;

					//选择区域为玩家胜利，同等级的其他的区域为玩家输。以得出最大下注值
					for ( int i = 0; i < 4; i++ )
					{
						for ( int j = 0; j < 4; j++ )
						{
							if ( cbArae[i][j] == cbBetArea )
							{
								for ( int n = 0; n < 4; ++n )
								{
									if ( cbArae[i][n] != 255 && cbArae[i][n] != AREA_MAX )
									{
										cbWinArea[cbArae[i][n]] = FALSE;
									}
								}
								cbWinArea[cbArae[i][j]] = TRUE;
							}
						}
					}

					LONGLONG lScore = m_lBankerScore;
					for (int nAreaIndex = 0; nAreaIndex < AREA_MAX; ++nAreaIndex ) 
					{
						if ( cbWinArea[nAreaIndex] == TRUE )
						{
							lScore -= m_lUserJettonScore[nAreaIndex]*(cbMultiple[nAreaIndex] - 1);
						}
						else
						{
							lScore += m_lUserJettonScore[nAreaIndex];
						}
					}
					if ( lBankerScore == -1 )
						lBankerScore = lScore;
					else
						lBankerScore = min(lBankerScore, lScore);
				}
			}
		}
	}

	//最大下注
	LONGLONG lMaxBet = 0L;

	//最大下注
	if( m_lMeMaxScore - lNowBet> m_lAreaLimitScore[cbBetArea] - m_lUserJettonScore[cbBetArea])
	{
		lMaxBet = m_lAreaLimitScore[cbBetArea] - m_lUserJettonScore[cbBetArea];
	}
	else
	{
		lMaxBet = m_lMeMaxScore - lNowBet;
	}

	if (lMaxBet > lBankerScore / (cbMultiple[cbBetArea] - 1))
	{
		lMaxBet = lBankerScore / (cbMultiple[cbBetArea] - 1);
	}

	//非零限制
	//ASSERT(lMaxBet >= 0);
	if (lMaxBet < 0) lMaxBet = 0;
	return lMaxBet;
}

void BR30SGameViewLayer::onTouchMoved(Touch *pTouch, Event *pEvent)
{
}

void BR30SGameViewLayer::onTouchEnded(Touch*pTouch, Event*pEvent)
{
}

// Alert Message 确认消息处理
void BR30SGameViewLayer::DialogConfirm(Ref *pSender)
{
	this->RemoveAlertMessageLayer();
	this->SetAllTouchEnabled(true);
}

// Alert Message 取消消息处理
void BR30SGameViewLayer::DialogCancel(Ref *pSender)
{
	this->RemoveAlertMessageLayer();
	this->SetAllTouchEnabled(true);
}

void BR30SGameViewLayer::OnEventUserScore( tagUserData * pUserData, WORD wChairID, bool bLookonUser )
{
	IGameView::OnEventUserScore(pUserData, wChairID, bLookonUser);
    AddPlayerInfo();
}

void BR30SGameViewLayer::OnEventUserStatus(tagUserData * pUserData, WORD wChairID, bool bLookonUser)
{
	IGameView::OnEventUserStatus(pUserData, wChairID, bLookonUser);
	AddPlayerInfo();
}

void BR30SGameViewLayer::OnEventUserEnter( tagUserData * pUserData, WORD wChairID, bool bLookonUser )
{
	AddPlayerInfo();
}

void BR30SGameViewLayer::OnEventUserLeave( tagUserData * pUserData, WORD wChairID, bool bLookonUser )
{
	AddPlayerInfo();
}

//游戏消息
bool BR30SGameViewLayer::OnGameMessage(WORD wSubCmdID, const void * pBuffer, WORD wDataSize)
{
	CCLOG("RECEIVE MESSAGE CMD[%d]" , wSubCmdID);
	switch (wSubCmdID)
	{
	case BR30S_SUB_S_GAME_FREE:	//游戏空闲
		return OnSubGameFree(pBuffer,wDataSize);

	case BR30S_SUB_S_GAME_START:		//游戏开始
		return OnSubGameStart(pBuffer,wDataSize);

	case BR30S_SUB_S_PLACE_JETTON:	//用户加注
		return OnSubPlaceJetton(pBuffer,wDataSize);

	case BR30S_SUB_S_APPLY_BANKER:	//申请做庄
		return OnSubUserApplyBanker(pBuffer, wDataSize);

	case BR30S_SUB_S_CANCEL_BANKER:	//取消做庄
		return OnSubUserCancelBanker(pBuffer, wDataSize);

	case BR30S_SUB_S_CHANGE_BANKER:	//切换庄家
		return OnSubChangeBanker(pBuffer, wDataSize);
		
	case BR30S_SUB_S_GAME_END:		//游戏结束
		return OnSubGameEnd(pBuffer,wDataSize);

	case BR30S_SUB_S_SEND_RECORD:		//游戏记录
        return OnSubGameRecord(pBuffer,wDataSize);

	case BR30S_SUB_S_PLACE_JETTON_FAIL:	//下注失败
		return OnSubPlaceJettonFail(pBuffer,wDataSize);

	case BR30S_SUB_S_COMPTETE_BANKER:
		return OnSubUserCompeteBanker(pBuffer,wDataSize);
	}
	return true;
}

//场景消息
bool BR30SGameViewLayer::OnGameSceneMessage(BYTE cbGameStatus, const void * pBuffer, WORD wDataSize)
{
	switch (cbGameStatus)
	{
	case GAME_SCENE_FREE:			//空闲状态
		{
			//效验数据
			//ASSERT(wDataSize==sizeof(BR30S_CMD_S_StatusFree));
			if (wDataSize!=sizeof(BR30S_CMD_S_StatusFree))
			{
				return false;
			}

			//消息处理
			BR30S_CMD_S_StatusFree * pStatusFree=(BR30S_CMD_S_StatusFree *)pBuffer;

            //庄家信息
			m_BankZhangJI = pStatusFree->lBankerWinScore;
			m_BankJuShu = pStatusFree->cbBankerTime;
            for(int i = 0; i < AREA_MAX; i++)
                m_lAreaLimitScore[i]=pStatusFree->lAreaLimitScore[i];
			m_lApplyBankerCondition=pStatusFree->lApplyBankerCondition;
		//	m_lCompetitionScore=pStatusFree->lCompetitionScore;
			SetBankerInfo(pStatusFree->wBankerUser,pStatusFree->lBankerScore);

			//设置时间
			StartTime(pStatusFree->cbTimeLeave, 0);
			UpdateButtonContron();
			return true;
		}
	case GAME_SCENE_BET:	//游戏状态
	case GAME_SCENE_END:	//结束状态
		{
			//效验数据
			if (wDataSize!=sizeof(BR30S_CMD_S_StatusPlay))
			{
					return false;
			}
			//消息处理
			BR30S_CMD_S_StatusPlay * pStatusPlay=(BR30S_CMD_S_StatusPlay *)pBuffer;

			//设置时间
			if( pStatusPlay->cbGameStatus == GAME_SCENE_BET )
			{
				StartTime(pStatusPlay->cbTimeLeave,1);
 			}
			else
			{
				StartTime(pStatusPlay->cbTimeLeave,2);
			}

			for (BYTE i = 0; i < 8;i++)
			{
				m_lAllJettonScore[i] = pStatusPlay->lTotalAreaScore[i];
				m_lUserJettonScore[i] = pStatusPlay->lUserAreaScore[i];
			}

//			//下注信息
			for (int nAreaIndex = AREA_XIAN; nAreaIndex < AREA_MAX; ++nAreaIndex)
			{
				SetEndScore(nAreaIndex, pStatusPlay->lTotalAreaScore[nAreaIndex]);
				SetMePlaceJetton(nAreaIndex, pStatusPlay->lUserAreaScore[nAreaIndex]);
                m_lAreaLimitScore[nAreaIndex] = pStatusPlay->lAreaScoreLimit[nAreaIndex];
			}
			UpdataAllScore();
			
			//庄家信息
			m_BankZhangJI = pStatusPlay->lBankerWinScore;
			m_BankJuShu = pStatusPlay->cbBankerTime;
			
			m_lApplyBankerCondition = pStatusPlay->lApplyBankerCondition;
			//m_lCompetitionScore = pStatusPlay->lCompetitionScore;
			SetBankerInfo(pStatusPlay->wBankerUser,pStatusPlay->lBankerScore);			

			// 设置结束信息
			if ( pStatusPlay->cbGameStatus == GAME_SCENE_END)
			{
				// 设置界面
				SetCardInfo(pStatusPlay->cbCardCount, pStatusPlay->cbTableCardArray);
				//成绩信息
				m_lPlayAllScore = pStatusPlay->lEndUserScore;
				m_lBankerCurGameScore = pStatusPlay->lBankerScore;
				SendCard();
			}
//			PlayBackGroundSound(AfxGetInstanceHandle(),TEXT("BACK_GROUND"));
			return true;
		}
	}
	return true;
}

void BR30SGameViewLayer::SendGameStart()
{
}

bool BR30SGameViewLayer::IsLookonMode()
{
	return m_LookMode;
}

//游戏空闲
bool BR30SGameViewLayer::OnSubGameFree(const void * pBuffer, WORD wDataSize)
{
	//效验数据
	ASSERT(wDataSize==sizeof(BR30S_CMD_S_GameFree));
	if (wDataSize != sizeof(BR30S_CMD_S_GameFree))
	{
		return false;
	}
	//消息处理
	BR30S_CMD_S_GameFree * pGameFree=(BR30S_CMD_S_GameFree *)pBuffer;

	//m_blUsing = true;
	
	//自己是庄家
	if (m_IsMeBank)
	{
		m_BtnCancelBank->setVisible(true);
		m_BtnCancelBank->setEnabled(true);
		m_BtnCancelBank->setColor(Color3B(255,255,255));
	}

	////更新用户列表
	//UpdateUserList();

	//设置时间
	StartTime(pGameFree->cbTimeLeave,0);

	//清理桌面
	for (int i = 0; i < 8; i++)
	{
		removeAllChildByTag(m_LightArea_Tga[i]); 
	}
	removeAllChildByTag(m_SendCardAnim_Tga);
	removeAllChildByTag(m_XianCard_Tga); 
	removeAllChildByTag(m_ZhuangCard_Tga);
	removeAllChildByTag(m_XianPoint_Tga);
	removeAllChildByTag(m_ZhuangPoint_Tga);
	removeAllChildByTag(m_BtnAnim_Tga);
	for (int i = 0; i < 8; i++)
	{
		removeAllChildByTag(m_BetScore_Tga[i]); 
		removeAllChildByTag(m_BetScore1_Tga[i]); 
		m_lUserJettonScore[i] = 0;
		m_lAllJettonScore[i] = 0;
	}
	m_CurSelectGold = 0;

	//m_GameClientView.SetWinnerSide(blWin, false);

	////更新控件
	UpdateButtonContron();

	DrawEndScore();

	////更新成绩
	//for (WORD wUserIndex = 0; wUserIndex < MAX_CHAIR; ++wUserIndex)
	//{
	//	IClientUserItem *pClientUserItem = GetTableUserItem(wUserIndex);

	//	if ( pClientUserItem == NULL ) continue;
	//	tagApplyUser ApplyUser ;

	//	//更新信息
	//	ApplyUser.lUserScore = pClientUserItem->GetUserScore();
	//	ApplyUser.strUserName = pClientUserItem->GetNickName();
	//	m_GameClientView.m_ApplyUser.UpdateUser(ApplyUser);
	//}

	//AllowBackGroundSound(false);

	return true;
}

bool BR30SGameViewLayer::OnSubGameStart( const void * pBuffer, WORD wDataSize )
{
	//效验数据
	if (wDataSize != sizeof(BR30S_CMD_S_GameStart))
	{
		return false;
	}

	//消息处理
	BR30S_CMD_S_GameStart * pGameStart=(BR30S_CMD_S_GameStart *)pBuffer;

	removeAllChildByTag(m_JsBackBack_Tga);

	if (getChildByTag(m_ChangeBankTga) != NULL)
	{
		removeChildByTag(m_ChangeBankTga);
	}
	for (int i = 0; i < 8; i++)
	{
		removeAllChildByTag(m_LightArea_Tga[i]); 
	}

	setGameStatus(GAME_SCENE_BET);
	//庄家信息
	SetBankerInfo(pGameStart->wBankerUser,pGameStart->lBankerScore);

	m_lMeMaxScore = pGameStart->lUserMaxScore;
    
    for(int i = 0; i < AREA_MAX; i++)
        m_lAreaLimitScore[i]=pGameStart->lAreaLimitScore[i];
    
	//自己是庄家
	if (m_IsMeBank)
	{
		m_BtnCancelBank->setVisible(true);
		m_BtnCancelBank->setEnabled(false);
		m_BtnCancelBank->setColor(Color3B(100,100,100));
	}

	//设置时间
	StartTime(pGameStart->cbTimeLeave,1);

	UpdateButtonContron();

	SoundUtil::sharedEngine()->playEffect("BR30S/BetStart"); 

	Sprite* temp = Sprite::createWithSpriteFrameName("begin_bet.png");
	temp->setPosition(Vec2(_STANDARD_SCREEN_CENTER_.x,540));
	temp->setTag(m_StartBetText_Tga);
	addChild(temp);


	return true;
}

void BR30SGameViewLayer::BetSound(float fp)
{
	unschedule(schedule_selector(BR30SGameViewLayer::BetSound));
	m_bBetSound = false;
}

//用户加注aceBet));
bool BR30SGameViewLayer::OnSubPlaceJetton(const void * pBuffer, WORD wDataSize)
{
	if (wDataSize != sizeof(BR30S_CMD_S_PlaceBet))
	{
		return false;
	}

	BR30S_CMD_S_PlaceBet * pPlaceJetton=(BR30S_CMD_S_PlaceBet *)pBuffer;

	//效验参数
	if (pPlaceJetton->lJettonScore >=1000000)
	{
		SoundUtil::sharedEngine()->playEffect("BR30S/ADD_GOLD_EX"); 		
	}
	else 
	{
		if (m_bBetSound == false)
		{
			m_bBetSound = true;
			SoundUtil::sharedEngine()->playEffect("BR30S/bet"); 
			schedule(schedule_selector(BR30SGameViewLayer::BetSound), 0.2f);
		}
	}
	
	{  
		if(pPlaceJetton->cbJettonArea > AREA_MAX) return true;
		m_lAllJettonScore[pPlaceJetton->cbJettonArea] += pPlaceJetton->lJettonScore;
		SetEndScore(pPlaceJetton->cbJettonArea, m_lAllJettonScore[pPlaceJetton->cbJettonArea]);
	}
	if (GetMeChairID()==pPlaceJetton->wChairID)
	{
		m_lUserJettonScore[pPlaceJetton->cbJettonArea] += pPlaceJetton->lJettonScore;
		SetMePlaceJetton(pPlaceJetton->cbJettonArea, m_lUserJettonScore[pPlaceJetton->cbJettonArea]);
	}

	UpdataAllScore();

	UpdateButtonContron();

	return true;
}

void BR30SGameViewLayer::DeleteBankList(tagApplyUser ApplyUser)
{
    for (int i = 0; i < m_BankUserList.size(); i++)
    {
        if (strcmp(m_BankUserList[i].strUserName, ApplyUser.strUserName) == 0)
        {
            m_BankUserList.erase(m_BankUserList.begin()+i);
            return;
        }
    }
}

void BR30SGameViewLayer::DrawBankList()
{
    m_listView->removeAllItems();
    for (int i = 0; i < m_BankUserList.size(); i++)
    {
        ui::Layout* custom_item = ui::Layout::create();
        custom_item->setContentSize(Size(920, 120));
        
        Sprite* lable = Sprite::createWithSpriteFrameName("label_line.png");
        lable->setPosition(460, 0);
        custom_item->addChild(lable);
        
        //头像
        string headS = g_GlobalUnits.getFace(m_BankUserList[i].gender , m_BankUserList[i].lUserScore);
        Sprite* mHead = Sprite::createWithSpriteFrameName(headS);
        mHead->setPosition(Vec2(90, 60));
        mHead->setScale(0.6f);
        custom_item->addChild(mHead);
        
        //昵称
        std::string szTempTitle = gbk_utf8(m_BankUserList[i].strUserName);
        Label* temp = Label::createWithSystemFont(szTempTitle.c_str(),_GAME_FONT_NAME_1_,40);
        temp->setAnchorPoint(Vec2(0,0.5f));
        
        if (!strcmp(GetMeUserData()->szNickName, m_BankUserList[i].strUserName))
            temp->setColor(Color3B::RED);
        else
            temp->setColor(Color3B::BLACK);
        temp->setPosition(Vec2(170, 60));
        custom_item->addChild(temp);
        
        
        Sprite* goldIcon = Sprite::createWithSpriteFrameName("gold_icon.png");
        goldIcon->setPosition(Vec2(620, 60));
        custom_item->addChild(goldIcon);
        char _score[32];
        if (m_BankUserList[i].lUserScore > 1000000000000)
        {
            sprintf(_score,"%.02lf兆", m_BankUserList[i].lUserScore / 1000000000000.0);
        }
        else if (m_BankUserList[i].lUserScore > 100000000)
        {
            sprintf(_score,"%.02lf亿", m_BankUserList[i].lUserScore / 100000000.0);
        }
        else if (m_BankUserList[i].lUserScore > 1000000)
        {
            sprintf(_score,"%.02lf万", m_BankUserList[i].lUserScore / 10000.0);
        }
        temp = Label::createWithSystemFont(_score,_GAME_FONT_NAME_1_,40);
        temp->setAnchorPoint(Vec2(0,0.5f));
        temp->setColor(Color3B::BLACK);
        temp->setPosition(Vec2(660, 60));
        custom_item->addChild(temp);
        m_listView->pushBackCustomItem(custom_item);
    }
}

//申请做庄
bool BR30SGameViewLayer::OnSubUserApplyBanker(const void * pBuffer, WORD wDataSize)
{
	//效验数据
	ASSERT(wDataSize==sizeof(CMD_S_ApplyBanker));
	if (wDataSize != sizeof(CMD_S_ApplyBanker))
	{
		return false;
	}

	//消息处理
	CMD_S_ApplyBanker * pApplyBanker=(CMD_S_ApplyBanker *)pBuffer;

	const tagUserData * pClientUserItem = GetUserData(pApplyBanker->wApplyUser);
	//插入玩家
    if (m_wCurrentBanker!=pApplyBanker->wApplyUser)
    {
        tagApplyUser ApplyUser;
        sprintf(ApplyUser.strUserName,"%s", pClientUserItem->szNickName);
        ApplyUser.lUserScore=pClientUserItem->lScore;
        ApplyUser.gender =pClientUserItem->wGender;
        ApplyUser.bCompetition = false;
        m_BankUserList.push_back(ApplyUser);
    }
    
    //自己判断
	if (IsLookonMode()==false && GetMeChairID()==pApplyBanker->wApplyUser)
	{
		m_BtnCallBank->setVisible(false);
		//m_BtnQiangBank->setVisible(false);
		m_BtnCancelBank->setVisible(true);
        m_BtnCancelBank->setEnabled(true);

	}

	//更新控件
	DrawBankList();

	UpdateButtonContron();
	return true;
}


//取消做庄
bool BR30SGameViewLayer::OnSubUserCancelBanker(const void * pBuffer, WORD wDataSize)
{
	//效验数据
	ASSERT(wDataSize==sizeof(CMD_S_CancelBanker));
	if (wDataSize != sizeof(CMD_S_CancelBanker))
	{
		return false;
	}

	CMD_S_CancelBanker * pCancelBanker=(CMD_S_CancelBanker *)pBuffer;

    //删除玩家
    tagApplyUser ApplyUser;
    sprintf(ApplyUser.strUserName,"%s",pCancelBanker->szCancelUser);
    ApplyUser.lUserScore=0;
    DeleteBankList(ApplyUser);
    
    //自己判断
    const tagUserData * pMeUserData= GetMeUserData();
    
    if (IsLookonMode() == false && !strcmp(pMeUserData->szNickName, pCancelBanker->szCancelUser))
    {
        m_BtnCancelBank->setVisible(false);
        m_BtnCallBank->setVisible(true);
        m_BtnCallBank->setEnabled(true);
    }
    
    //更新控件
    UpdateButtonContron();
    
    DrawBankList();
    
    return true;
}


//切换庄家
bool BR30SGameViewLayer::OnSubChangeBanker(const void * pBuffer, WORD wDataSize)
{
	ASSERT(wDataSize==sizeof(CMD_S_ChangeBanker));
	if (wDataSize != sizeof(CMD_S_ChangeBanker))
	{
		return false;
	}

	CMD_S_ChangeBanker * pChangeBanker = (CMD_S_ChangeBanker *)pBuffer;

	//庄家信息
	SetBankerInfo(pChangeBanker->wBankerUser,pChangeBanker->lBankerScore);

	//自己是庄家
	if (m_IsMeBank == true)
	{
		m_BtnCancelBank->setVisible(false);
		m_BtnCallBank->setVisible(true);
        m_BtnCallBank->setEnabled(true);

//		m_BtnQiangBank->setVisible(true);
	}

	m_IsMeBank = false;

	//自己判断
	if (IsLookonMode() == false && pChangeBanker->wBankerUser==GetMeChairID())
	{
		//m_BtnCancelBank->setVisible(false);
		//m_BtnCallBank->setVisible(true);
		////m_BtnQiangBank->setVisible(true);
		//m_BankZhangJI = 0;
		//m_BankJuShu = 0;

		m_BtnCancelBank->setVisible(true);
		m_BtnCancelBank->setEnabled(false);
		m_BtnCancelBank->setColor(Color3B(100,100,100));
		m_BtnCallBank->setVisible(false);
//		m_BtnQiangBank->setVisible(false);
		m_BankZhangJI = 0;
		m_BankJuShu = 0;
		m_IsMeBank = true;
	}


	removeChildByTag(m_JsBackBack_Tga);
	if (getChildByTag(m_ChangeBankTga) != NULL)
	{
		removeChildByTag(m_ChangeBankTga);
	}
//	Sprite* ChangeBankSpr = Sprite::createWithSpriteFrameName("CHANGE_BANKER.png");
//	ChangeBankSpr->setTag(m_ChangeBankTga);
//	ChangeBankSpr->setPosition(Vec2(_STANDARD_SCREEN_CENTER_IN_PIXELS_.x, _STANDARD_SCREEN_CENTER_IN_PIXELS_.y-100));
//	addChild(ChangeBankSpr,1000);

	//删除玩家
	if (m_wCurrentBanker!=INVALID_CHAIR)
	{
		const tagUserData* pBankerUserData = GetUserData(m_wCurrentBanker);
		if (pBankerUserData != NULL)
		{
			tagApplyUser ApplyUser;
			sprintf(ApplyUser.strUserName, "%s", pBankerUserData->szNickName);
			DeleteBankList(ApplyUser);
		}
	}

	//更新界面
	UpdateButtonContron();
	DrawBankList();

	return true;
}

void BR30SGameViewLayer::addWinSpr(int index, Size size)
{
    removeAllChildByTag(m_LightArea_Tga[index]);
    ui::Scale9Sprite* temp = ui::Scale9Sprite::createWithSpriteFrameName("win.png");
    temp->setContentSize(size);
    temp->setPosition(m_ClickPos[index]);
    temp->setTag(m_LightArea_Tga[index]);
    addChild(temp);
    ActionInterval *actionBlink = Blink::create(8, 15);
    temp->runAction(Sequence::create(actionBlink,NULL));
}

void BR30SGameViewLayer::SetWinArea(float fp) 	//设置赢得区域
{
	unschedule(schedule_selector(BR30SGameViewLayer::SetWinArea));
	
	BYTE cbPlayerPoint = m_GameLogic.GetCardListPip(m_cbTableCardArray[0],m_cbCardCount[0]);
	BYTE cbBankerPoint = m_GameLogic.GetCardListPip(m_cbTableCardArray[1],m_cbCardCount[1]);
    
	//高亮区域
	if (cbPlayerPoint == cbBankerPoint)  //和
	{
        addWinSpr(AREA_PING, Size(648, 286));
        
        bool tong = true;
        
        if (m_cbCardCount[0] == m_cbCardCount[1])
        {
            for (int i = 0; i < m_cbCardCount[0]; i++)
            {
                BYTE cbBankerValue = m_GameLogic.GetCardValue(m_cbTableCardArray[0][i]);
                BYTE cbPlayerValue = m_GameLogic.GetCardValue(m_cbTableCardArray[1][i]);
                
                if (cbBankerValue != cbPlayerValue)
                {
                    tong = false;
                    break;
                }
            }
        }
        else
        {
            tong = false;
        }
        
        if (tong)
            addWinSpr(AREA_TONG_DUI, Size(648, 186));
	}
	else if (cbPlayerPoint<cbBankerPoint) //庄
	{
        addWinSpr(AREA_ZHUANG, Size(448, 286));

		//天王判断
		if ( cbBankerPoint == 8 || cbBankerPoint == 9 )
		{
            addWinSpr(AREA_HU, Size(448, 186));
		}
	}
	else  //闲
	{
        addWinSpr(AREA_XIAN, Size(448, 286));

		if ( cbPlayerPoint == 8 || cbPlayerPoint == 9 )
		{
            addWinSpr(AREA_LONG, Size(448, 186));
		}
	}
	bool bPlayerTwoPair = false;
	if (m_GameLogic.GetCardValue(m_cbTableCardArray[0][0]) == m_GameLogic.GetCardValue(m_cbTableCardArray[0][1]))//闲对
	{	
		bPlayerTwoPair = true;
        addWinSpr(AREA_XIAN_DUI, Size(448, 186));
	}
	bool bBankerTwoPair = false;
	if (m_GameLogic.GetCardValue(m_cbTableCardArray[1][0]) == m_GameLogic.GetCardValue(m_cbTableCardArray[1][1]))//庄对
	{	
		bBankerTwoPair = true;
        addWinSpr(AREA_ZHUANG_DUI, Size(448, 186));
	}

	if ( m_lPlayAllScore > 0 ) 
		SoundUtil::sharedEngine()->playEffect("BR30S/END_WIN");  
	else if ( m_lPlayAllScore < 0 ) 
		SoundUtil::sharedEngine()->playEffect("BR30S/END_LOST"); 
	else 
		SoundUtil::sharedEngine()->playEffect("BR30S/END_DRAW");
}

void BR30SGameViewLayer::SendCard()
{
	if (m_SendCardIndex > 6)
	{
		return;
	}

	if (m_FirstSend == 0) //先发闲家
	{
		if (m_XianSendCardIndex < m_cbCardCount[0])
		{
            Vector<AnimationFrame*> arrayOfAnimationFrameNames;
            for(int i = 0; i < 7; i++)
			{
				char name[50]= {0};
				sprintf(name, "head%d.png", i);
				SpriteFrame *pSpriteFrame = SpriteFrameCache::getInstance()->getSpriteFrameByName(name);
                ValueMap userInfo;
                AnimationFrame *pAnimationFrame = AnimationFrame::create(pSpriteFrame, 0.6f,userInfo);
                arrayOfAnimationFrameNames.pushBack(pAnimationFrame);
			}
			Animation *pAnimation = Animation::create(arrayOfAnimationFrameNames, 0.5f,1);
			Animate* m_KuangAnimate = Animate::create(pAnimation);
			Sprite* temp = Sprite::create();
			temp->setPosition(m_SendCardAnimPos);
			addChild(temp,4,m_SendCardAnim_Tga);
			long card = m_cbTableCardArray[0][m_XianSendCardIndex];
			ActionInstant *func = __CCCallFuncND::create(this,callfuncND_selector(BR30SGameViewLayer::XianAnimCallback),(void*)card);
			temp->runAction(Sequence::create(m_KuangAnimate,func,NULL));
		}
		else if (m_ZhuangSendCardIndex < m_cbCardCount[1])
		{
            Vector<AnimationFrame*> arrayOfAnimationFrameNames;
            for(int i = 0; i < 7; i++)
			{
				char name[50]= {0};
				sprintf(name, "head%d.png", i);
				SpriteFrame *pSpriteFrame = SpriteFrameCache::getInstance()->getSpriteFrameByName(name);
                ValueMap userInfo;
                AnimationFrame *pAnimationFrame = AnimationFrame::create(pSpriteFrame, 0.6f,userInfo);
                arrayOfAnimationFrameNames.pushBack(pAnimationFrame);
			}
			Animation *pAnimation = Animation::create(arrayOfAnimationFrameNames, 0.5f,1);
			Animate* m_KuangAnimate = Animate::create(pAnimation);
			Sprite* temp = Sprite::create();
			temp->setPosition(m_SendCardAnimPos);
			addChild(temp,4,m_SendCardAnim_Tga);
			long card = m_cbTableCardArray[1][m_ZhuangSendCardIndex];
			ActionInstant *func = __CCCallFuncND::create(this,callfuncND_selector(BR30SGameViewLayer::ZhuangAnimCallback),(void*)card);
			temp->runAction(Sequence::create(m_KuangAnimate,func,NULL));
		}
        else
        {
            schedule(schedule_selector(BR30SGameViewLayer::SetXianSound), 0.1f);
            
            //高亮
            schedule(schedule_selector(BR30SGameViewLayer::SetWinArea),0.1f);
         
            SendData(SUB_C_RECORD, nullptr, 0);
        }
	}
	else if (m_FirstSend == 1) //发庄家
	{
		if (m_ZhuangSendCardIndex < m_cbCardCount[1])
		{
			Vector<AnimationFrame*> arrayOfAnimationFrameNames;
            for(int i = 0; i < 7; i++)
			{
				char name[50]= {0};
				sprintf(name, "head%d.png", i);
				SpriteFrame *pSpriteFrame = SpriteFrameCache::getInstance()->getSpriteFrameByName(name);
                ValueMap userInfo;
                AnimationFrame *pAnimationFrame = AnimationFrame::create(pSpriteFrame, 0.6f,userInfo);
                arrayOfAnimationFrameNames.pushBack(pAnimationFrame);
			}
			Animation *pAnimation = Animation::create(arrayOfAnimationFrameNames, 0.5f,1);
			Animate* m_KuangAnimate = Animate::create(pAnimation);
			Sprite* temp = Sprite::create();
			temp->setPosition(m_SendCardAnimPos);
			addChild(temp,4,m_SendCardAnim_Tga);
			long card = m_cbTableCardArray[1][m_ZhuangSendCardIndex];
			ActionInstant *func = __CCCallFuncND::create(this,callfuncND_selector(BR30SGameViewLayer::ZhuangAnimCallback),(void*)card);
			temp->runAction(Sequence::create(m_KuangAnimate,func,NULL));
		}
		else if (m_XianSendCardIndex < m_cbCardCount[0])
		{
			BYTE cbBankerPoint = m_GameLogic.GetCardListPip(m_cbTableCardArray[1],m_cbCardCount[1]);
			char xplay[32];
			sprintf(xplay,"BR30S/Z/Z_%d", cbBankerPoint);
			SoundUtil::sharedEngine()->playEffect(xplay);
			Vector<AnimationFrame*> arrayOfAnimationFrameNames;
            for(int i = 0; i < 7; i++)
			{
				char name[50]= {0};
				sprintf(name, "head%d.png", i);
				SpriteFrame *pSpriteFrame = SpriteFrameCache::getInstance()->getSpriteFrameByName(name);
                ValueMap userInfo;
                AnimationFrame *pAnimationFrame = AnimationFrame::create(pSpriteFrame, 0.6f,userInfo);
                arrayOfAnimationFrameNames.pushBack(pAnimationFrame);
			}
			Animation *pAnimation = Animation::create(arrayOfAnimationFrameNames, 0.5f,1);
			Animate* m_KuangAnimate = Animate::create(pAnimation);
			Sprite* temp = Sprite::create();
			temp->setPosition(m_SendCardAnimPos);
			addChild(temp,4,m_SendCardAnim_Tga);
			long card = m_cbTableCardArray[0][m_XianSendCardIndex];
			ActionInstant *func = __CCCallFuncND::create(this,callfuncND_selector(BR30SGameViewLayer::XianAnimCallback),(void*)card);
			temp->runAction(Sequence::create(m_KuangAnimate,func,NULL));
		}
        else
        {
            schedule(schedule_selector(BR30SGameViewLayer::SetXianSound), 0.1f);
            
            //高亮
            schedule(schedule_selector(BR30SGameViewLayer::SetWinArea), + 2.0f);
            
            SendData(SUB_C_RECORD, nullptr, 0);
        }
	}
	m_SendCardIndex++;
}


void BR30SGameViewLayer::XianAnimCallback(Node *pSender,void*data)  //两个参数
{
	LONG card = (LONG)data;
	removeAllChildByTag(m_SendCardAnim_Tga);
	string _name = GetCardStringName(card);
	Sprite* temp = Sprite::createWithSpriteFrameName(_name.c_str());
	temp->setPosition(Vec2(350+m_XianSendCardIndex*60,570));
	temp->setTag(m_XianCard_Tga);
	addChild(temp,1000);
	m_XianSendCardIndex++;
	m_FirstSend = 1;

	//显示点数
	removeAllChildByTag(m_XianPoint_Tga);
	char filename[30]={0};
	BYTE cbPlayerPoint = m_GameLogic.GetCardListPip(m_cbTableCardArray[0],m_XianSendCardIndex);
	sprintf(filename,"point%d.png",cbPlayerPoint);
	temp = Sprite::createWithSpriteFrameName(filename);
	temp->setPosition(Vec2(400,500));
	temp->setTag(m_XianPoint_Tga);
	addChild(temp,1000);

	SendCard();
}

void BR30SGameViewLayer::ZhuangAnimCallback(Node *pSender,void*data) //两个参数
{
	LONG card = (LONG)data;
	removeAllChildByTag(m_SendCardAnim_Tga);
	string _name = GetCardStringName(card);
	Sprite* temp = Sprite::createWithSpriteFrameName(_name.c_str());
	temp->setPosition(Vec2(1480+m_ZhuangSendCardIndex*60,570));
	temp->setTag(m_ZhuangCard_Tga);
	addChild(temp, 1000);
	m_ZhuangSendCardIndex++;
	m_FirstSend = 0;


	//庄家点数
	removeAllChildByTag(m_ZhuangPoint_Tga);
	char filename[30]={0};
	BYTE cbBankerPoint = m_GameLogic.GetCardListPip(m_cbTableCardArray[1],m_ZhuangSendCardIndex);
	sprintf(filename,"point%d.png",cbBankerPoint);
	temp = Sprite::createWithSpriteFrameName(filename);
	temp->setPosition(Vec2(1530,500));
	temp->setTag(m_ZhuangPoint_Tga);
	addChild(temp,1000);

	SendCard();
	
}

//游戏记录
bool BR30SGameViewLayer::OnSubGameRecord(const void * pBuffer, WORD wDataSize)
{
	//效验参数
	ASSERT(wDataSize % sizeof(BR30S_tagServerGameRecord)==0);
	if (wDataSize % sizeof(BR30S_tagServerGameRecord) != 0)
	{
		return false;
	}
	//设置记录
    m_GameRecordArrary.clear();
    
	WORD wRecordCount=wDataSize/sizeof(BR30S_tagServerGameRecord);
	
	for (WORD wIndex=0; wIndex<wRecordCount; wIndex++)
	{
		BR30S_tagServerGameRecord * pServerGameRecord=(((BR30S_tagServerGameRecord *)pBuffer)+wIndex);
		
        SetGameHistory(pServerGameRecord);
	}

	UpdataGameHistory();

	return true;
}

void BR30SGameViewLayer::SetGameHistory(BR30S_tagServerGameRecord *pServerGameRecord)
{
	m_GameRecordArrary.push_back(*pServerGameRecord);
    if (m_GameRecordArrary.size() > 10)
    {
        auto font = m_GameRecordArrary.begin();
        m_GameRecordArrary.erase(font);
    }
    
}

//历史记录
void BR30SGameViewLayer::UpdataGameHistory()
{
	int sx = 203, sy = 330;
   
	//清理所有
	for (int i = 0; i < m_GameRecordArrary.size(); i++)
	{
		Node * temp = m_LzBack->getChildByTag(m_LzPxTga+i);
		if (temp != NULL)
            m_LzBack->removeChild(temp,false);
	}

	for (int i = 0; i < m_GameRecordArrary.size(); i++)
	{
		BR30S_tagServerGameRecord& record = m_GameRecordArrary[i];
        
		string name = "";
		if(record.cbWinner[0] == AREA_XIAN)
		{
			name = "lz_xian.png";
		}
		else if(record.cbWinner[0] == AREA_ZHUANG)
		{
			name = "lz_zhuang.png";
		}
		else
		{
			name = "lz_he.png";
		}
        
        Sprite* winSpr = Sprite::createWithSpriteFrameName(name);
        winSpr->setPosition(Vec2(sx + i * 84, sy - record.cbWinner[0] * 120));
        m_LzBack->addChild(winSpr, 0, m_LzPxTga+i);
	}
}

//游戏结束
bool BR30SGameViewLayer::OnSubGameEnd(const void * pBuffer, WORD wDataSize)
{
	//效验数据
	if (wDataSize != sizeof(BR30S_CMD_S_GameEnd))
	{
		return false;
	}

	//消息处理
	BR30S_CMD_S_GameEnd * pGameEnd=(BR30S_CMD_S_GameEnd *)pBuffer;

	m_CurSelectGold = 0;
	removeAllChildByTag(m_StartBetText_Tga);
	//设置时间
	StartTime(pGameEnd->cbTimeLeave,2);

	//扑克信息
	SetCardInfo(pGameEnd->cbCardCount,pGameEnd->cbTableCardArray);

	//发牌
	SendCard();

	m_BankJuShu = pGameEnd->nBankerTime;
	m_BankZhangJI = pGameEnd->lBankerTotallScore;

	//成绩信息
	m_lPlayAllScore = pGameEnd->lPlayScore;
	m_lBankerCurGameScore = pGameEnd->lBankerScore;
	m_lMeStatisticScore += m_lPlayAllScore;
    
	//设置状态
	setGameStatus(GAME_SCENE_END);	   

	UpdateButtonContron();

	return true;
}

void BR30SGameViewLayer::SetXianSound(float fp)
{
	unschedule(schedule_selector(BR30SGameViewLayer::SetXianSound));
	BYTE cbPlayerPoint = m_GameLogic.GetCardListPip(m_cbTableCardArray[0],m_cbCardCount[0]);
	//BYTE cbBankerPoint = m_GameLogic.GetCardListPip(m_cbTableCardArray[1],m_cbCardCount[1]);
	char xplay[32];
	sprintf(xplay,"BR30S/X/X_%d", cbPlayerPoint);
	SoundUtil::sharedEngine()->playEffect(xplay);
	schedule(schedule_selector(BR30SGameViewLayer::SetZhuangSound), 1.5f);
}

void BR30SGameViewLayer::SetZhuangSound(float fp)
{
	unschedule(schedule_selector(BR30SGameViewLayer::SetZhuangSound));
	//BYTE cbPlayerPoint = m_GameLogic.GetCardListPip(m_cbTableCardArray[0],m_cbCardCount[0]);
	BYTE cbBankerPoint = m_GameLogic.GetCardListPip(m_cbTableCardArray[1],m_cbCardCount[1]);
	char xplay[32];
	sprintf(xplay,"BR30S/Z/Z_%d", cbBankerPoint);
	SoundUtil::sharedEngine()->playEffect(xplay);
	schedule(schedule_selector(BR30SGameViewLayer::WinSound), 1.0f);
}

void BR30SGameViewLayer::WinSound(float fp)
{
	unschedule(schedule_selector(BR30SGameViewLayer::WinSound));
	BYTE cbPlayerPoint = m_GameLogic.GetCardListPip(m_cbTableCardArray[0],m_cbCardCount[0]);
	BYTE cbBankerPoint = m_GameLogic.GetCardListPip(m_cbTableCardArray[1],m_cbCardCount[1]);
	char xplay[32];
	if (cbPlayerPoint > cbBankerPoint) 
	{
		strcpy(xplay,"BR30S/X/X_Y");
		SoundUtil::sharedEngine()->playEffect(xplay);
	}
	else if(cbPlayerPoint < cbBankerPoint) 
	{
		strcpy(xplay,"BR30S/Z/Z_Y");
		SoundUtil::sharedEngine()->playEffect(xplay);
	}
	
	
}

void BR30SGameViewLayer::DrawEndScore()
{
	Sprite* TempSpr = Sprite::createWithSpriteFrameName("bg_tongyongkuang3.png");
	TempSpr->setPosition(Vec2(_STANDARD_SCREEN_CENTER_.x,_STANDARD_SCREEN_CENTER_.y));
	TempSpr->setTag(m_JsBackBack_Tga);
	addChild(TempSpr);
    
    Sprite* title = Sprite::createWithSpriteFrameName("gamover.png");
    title->setPosition(Vec2(519, 480));
    TempSpr->addChild(title);
    
    Sprite* line = Sprite::createWithSpriteFrameName("label_line.png");
    line->setPosition(Vec2(519, 220));
    TempSpr->addChild(line);
    
    Label* selfLabel = Label::createWithSystemFont("本家", _GAME_FONT_NAME_1_, 50);
    selfLabel->setColor(Color3B::BLACK);
    selfLabel->setPosition(Vec2(200, 310));
    TempSpr->addChild(selfLabel);
    
    Label* otherLabel = Label::createWithSystemFont("庄家", _GAME_FONT_NAME_1_, 50);
    otherLabel->setColor(Color3B::BLACK);
    otherLabel->setPosition(Vec2(200, 130));
    TempSpr->addChild(otherLabel);
    
    
	removeAllChildByTag(m_JsBank_Tga);
    removeAllChildByTag(m_JsXian_Tga);
    
    char strc[32];
    ZeroMemory(strc, sizeof(strc));
    if (m_lBankerCurGameScore > 0)
        sprintf(strc,"+%lld", m_lBankerCurGameScore);
    else
        sprintf(strc,"%lld", m_lBankerCurGameScore);
    
    LabelAtlas* banktemp = LabelAtlas::create(strc, "Common/gold_result.png", 45, 62, '+');
    banktemp->setTag(m_JsBank_Tga);
    banktemp->setPosition(Vec2(400,100));
    TempSpr->addChild(banktemp);

    ZeroMemory(strc, sizeof(strc));
    if (m_lPlayAllScore > 0)
        sprintf(strc,"+%lld", m_lPlayAllScore);
    else
        sprintf(strc,"%lld", m_lPlayAllScore);

    LabelAtlas* self = LabelAtlas::create(strc, "Common/gold_result.png", 45, 62, '+');
    self->setTag(m_JsXian_Tga);
    self->setPosition(Vec2(400,280));
    TempSpr->addChild(self);
}


//下注失败
bool BR30SGameViewLayer::OnSubPlaceJettonFail(const void * pBuffer, WORD wDataSize)
{
	//效验数据
	ASSERT(wDataSize==sizeof(CMD_S_PlaceBetFail));
	if (wDataSize != sizeof(CMD_S_PlaceBetFail))
	{
		return false;
	}

	//消息处理
	return true;
}


//申请做庄
bool BR30SGameViewLayer::OnSubUserCompeteBanker(const void * pBuffer, WORD wDataSize)
{
	//效验数据
	ASSERT(wDataSize==sizeof(CMD_S_ApplyBanker));
	if (wDataSize != sizeof(CMD_S_ApplyBanker))
	{
		return false;
	}
	CMD_S_ApplyBanker * pApplyBanker=(CMD_S_ApplyBanker *)pBuffer;

	//获取玩家
	const tagUserData*  pClientUserItem=GetUserData(pApplyBanker->wApplyUser);

	//删除玩家
	tagApplyUser ApplyUserFirst;
	sprintf(ApplyUserFirst.strUserName,"%s", pClientUserItem->szNickName);
	ApplyUserFirst.lUserScore=pClientUserItem->lScore;
	ApplyUserFirst.bCompetition = true;
	DeleteBankList(ApplyUserFirst);

	//插入玩家
	if (m_wCurrentBanker!=pApplyBanker->wApplyUser)
	{
		m_BankUserListSize++;
		bool _change = false;
		for (int i = 0; i < m_BankUserListSize; i++)
		{
			tagApplyUser temp;
			if (_change == false)
			{
				if (m_BankUserList[i].bCompetition == false) //当前玩家不是抢庄来的，插入
				{
					temp = m_BankUserList[i];
					m_BankUserList[i] = ApplyUserFirst;
					_change = true;
				}
			}
			else //继续交换
			{
				tagApplyUser temp1;
				temp1 = m_BankUserList[i];
				m_BankUserList[i] = temp;
				temp = temp1;
			}

		}
	}

	//自己判断
	if (IsLookonMode()==false && GetMeChairID()==pApplyBanker->wApplyUser)
	{
		m_BtnCallBank->setVisible(false);
		//m_BtnQiangBank->setVisible(false);
		m_BtnCancelBank->setVisible(true);
	}

	//更新控件
	UpdateButtonContron();

	return true;
}

//设置扑克
void BR30SGameViewLayer::SetCardInfo(BYTE cbCardCount[2], BYTE cbTableCardArray[2][3])
{
	m_FirstSend = 0;
	m_SendCardIndex = 0;
	m_XianSendCardIndex = 0;
	m_ZhuangSendCardIndex = 0;

	if (cbCardCount != NULL)
	{
		CopyMemory(m_cbCardCount,cbCardCount,sizeof(m_cbCardCount));
		CopyMemory(m_cbTableCardArray,cbTableCardArray,sizeof(m_cbTableCardArray));
	}
	else
	{
		ZeroMemory(m_cbCardCount,sizeof(m_cbCardCount));
		ZeroMemory(m_cbTableCardArray,sizeof(m_cbTableCardArray));
	}
}

void BR30SGameViewLayer::SetMePlaceJetton(int chair, LONGLONG score)
{

	removeAllChildByTag(m_BetScore1_Tga[chair]);
    
    char strc[32]="";
    sprintf(strc, "%lld", score);
    LabelAtlas* temp = LabelAtlas::create(strc, "Common/gold_self.png", 19, 24, '+');
    temp->setAnchorPoint(Vec2(0.5f, 0.5f));
    temp->setTag(m_BetScore1_Tga[chair]);
    if (chair < 3)
        temp->setPosition(m_BetNumPos[chair] - Vec2(0, 130));
    else
        temp->setPosition(m_BetNumPos[chair] - Vec2(0, 80));

    addChild(temp);
}

void BR30SGameViewLayer::SetEndScore(int chair, LONGLONG score)
{
	removeAllChildByTag(m_BetScore_Tga[chair]);
	
    char strc[32]="";
    sprintf(strc, "%lld", score);
    LabelAtlas* temp = LabelAtlas::create(strc, "Common/xiazhushuzi.png", 34, 46, '+');
    temp->setAnchorPoint(Vec2(0.5f, 0.5f));
    temp->setTag(m_BetScore_Tga[chair]);
    temp->setPosition(m_BetNumPos[chair]);
    addChild(temp);
}


void BR30SGameViewLayer::UpdataAllScore()
{
	LONGLONG score = 0;
	for (int i = 0; i < 8; i++)
	{
		score += m_lAllJettonScore[i];
	}

	removeAllChildByTag(m_AllScoreTga);
    
    char strc[32]="";
    sprintf(strc, "%lld", score);
    LabelAtlas* temp = LabelAtlas::create(strc, "Common/gold_total.png", 45, 56, '+');
    temp->setAnchorPoint(Vec2(0.5f, 0.5f));
    temp->setTag(m_AllScoreTga);
    temp->setPosition(Vec2(960, 770));
    addChild(temp);
}

string BR30SGameViewLayer::GetCardStringName(BYTE card)
{
	if (card == 0) return "backCard.png";
	char tt[32];
	sprintf(tt,"%0x",card);
	BYTE _value = m_GameLogic.GetCardValue(card);
	BYTE _color = m_GameLogic.GetCardColor(card);
	string temp;
	if (_color == 0) temp = "fangkuai_";
	if (_color == 1) temp = "meihua_";
	if (_color == 2) temp = "hongtao_";
	if (_color == 3) temp = "heitao_";
	char _valstr[32];
	ZeroMemory(_valstr,32);
	if (card == 0x41) //小王
	{
		sprintf(_valstr,"xiaowang.png");
	}
	else if (card == 0x42)
	{
		sprintf(_valstr,"dawang.png");
	}
	else if (_value < 10)
	{
		sprintf(_valstr,"0%d.png",_value);
	}
	else 
	{
		sprintf(_valstr,"%d.png",_value);
	}

	temp+=_valstr;
	return temp;
}

void BR30SGameViewLayer::InitGame()
{
	//背景图
	m_BackSpr = Sprite::create("BR30S/BR30S_Back.jpg");
	m_BackSpr->setPosition(_STANDARD_SCREEN_CENTER_IN_PIXELS_);
	addChild(m_BackSpr);
    
    Sprite *middleBg = Sprite::createWithSpriteFrameName("dakuang.png");
    middleBg->setPosition(Vec2(_STANDARD_SCREEN_CENTER_.x, _STANDARD_SCREEN_CENTER_.y+30));
    addChild(middleBg);
    
    Sprite* bankSp = Sprite::createWithSpriteFrameName("title_total.png");
    bankSp->setPosition(Vec2(_STANDARD_SCREEN_CENTER_.x, 820));
    addChild(bankSp);
    
    Sprite *chipBg = Sprite::createWithSpriteFrameName("chip_bottom.png");
    chipBg->setPosition(Vec2(_STANDARD_SCREEN_CENTER_.x, 0));
    chipBg->setAnchorPoint(Vec2(0.5f, 0));
    addChild(chipBg);
    
    Sprite* chipTop = Sprite::createWithSpriteFrameName("chip_top.png");
    chipTop->setPosition(Vec2(_STANDARD_SCREEN_CENTER_.x, 0));
    chipTop->setAnchorPoint(Vec2(0.5f, 0));
    addChild(chipTop, 100);

	m_ClockSpr = Sprite::createWithSpriteFrameName("TimeBack.png");
	m_ClockSpr->setPosition(Vec2(850, 965));
	m_ClockSpr->setVisible(false);
	addChild(m_ClockSpr);
    
	Sprite* anim = Sprite::createWithSpriteFrameName("head0.png");
	anim->setPosition(Vec2(1150,996));
	addChild(anim);

	m_BankListBackSpr = Sprite::createWithSpriteFrameName("bg_tongyongkuang3.png");
	m_BankListBackSpr->setPosition(Vec2(_STANDARD_SCREEN_CENTER_.x, 1500));
	addChild(m_BankListBackSpr,10000);
	
    Sprite* pBankTitle = Sprite::createWithSpriteFrameName("shangzhuangliebiao.png");
    pBankTitle->setScale(0.8f);
    pBankTitle->setPosition(Vec2(519,480));
    m_BankListBackSpr->addChild(pBankTitle);
    
    m_listView = ui::ListView::create();
    m_listView->setContentSize(Size(920, 380));
    m_listView->setDirection(ui::ScrollView::Direction::VERTICAL);
    m_listView->setPosition(Vec2(50,30));
    m_BankListBackSpr->addChild(m_listView);
   
    m_LzBack = Sprite::createWithSpriteFrameName("bg_tongyongkuang3.png");
    m_LzBack->setPosition(Vec2(_STANDARD_SCREEN_CENTER_.x, 1500));
    addChild(m_LzBack,10000);

    Sprite* lzTitle = Sprite::createWithSpriteFrameName("luzi.png");
    lzTitle->setScale(0.8f);
    lzTitle->setPosition(Vec2(519,480));
    m_LzBack->addChild(lzTitle);
    
     string name[] = {"xian.png", "ping.png", "zhuang.png"};
    for (int i = 0; i < 3; i++)
    {
        Sprite* temp = Sprite::createWithSpriteFrameName("luzi_"+name[i]);
        temp->setPosition(Vec2(100, 330-i*120));
        m_LzBack->addChild(temp);
    }
    
    for (int i = 0; i < 2; i++)
    {
        Sprite* temp = Sprite::createWithSpriteFrameName("label_line.png");
        temp->setPosition(Vec2(519, 270-i*120));
        m_LzBack->addChild(temp);
    }

    //tga
	m_Head_Tga         = 100;
	m_NickName_Tga     = 101;
	m_Glod_Tga	       = 102;
	m_WinGlod_Tga      = 103;
	m_BankNickName_Tga = 104;
	m_BankGold_Tga     = 105;
	m_BankZhangJi_Tga  = 106;
	m_BankJuShu_Tga    = 107;
	m_TimeType_Tga	   = 108;
	m_Click_Tga[0]     = 109;
	m_Click_Tga[1]     = 110;
	m_Click_Tga[2]     = 111;
	m_Click_Tga[3]     = 112;
	m_Click_Tga[4]     = 113;
	m_BetScore_Tga[0]  = 114;
	m_BetScore_Tga[1]  = 115;
	m_BetScore_Tga[2]  = 116;
	m_BetScore_Tga[3]  = 117;
	m_BetScore_Tga[4]  = 118;
	m_BetScore_Tga[5]  = 119;
	m_BetScore_Tga[6]  = 120;
	m_BetScore_Tga[7]  = 121;
	m_BankBet_Tga      = 122;
	m_XianBet_Tga	   = 123;
	m_HeBet_Tga		   = 124;
	m_SendCardAnim_Tga = 125;
	m_LightArea_Tga[0] = 126;
	m_LightArea_Tga[1] = 127;
	m_LightArea_Tga[2] = 128;
	m_LightArea_Tga[3] = 129;
	m_LightArea_Tga[4] = 130;
	m_XianCard_Tga	   = 131;
	m_ZhuangCard_Tga   = 132;
	m_BtnAnim_Tga	   = 133;
	m_ZhuangPoint_Tga  = 134;
	m_XianPoint_Tga    = 135;

	m_BetScore1_Tga[0] = 137;
	m_BetScore1_Tga[1] = 138;
	m_BetScore1_Tga[2] = 139;
	m_BetScore1_Tga[3] = 140;
	m_BetScore1_Tga[4] = 141;
	m_BetScore1_Tga[5] = 142;
	m_BetScore1_Tga[6] = 143;
	m_BetScore1_Tga[7] = 144;
	m_StartBetText_Tga = 145;
	m_BankList_Tga[0]  = 146;
	m_BankList_Tga[1]  = 147;
	m_BankList_Tga[2]  = 148;
	m_BankList_Tga[3]  = 149;
	m_BankList1_Tga[0] = 151;
	m_BankList1_Tga[1] = 152;
	m_BankList1_Tga[2] = 153;
	m_BankList1_Tga[3] = 154;
	m_Click_Tga[5]     = 155;
	m_Click_Tga[6]     = 156;
	m_Click_Tga[7]     = 157;
	m_LightArea_Tga[5] = 158;
	m_LightArea_Tga[6] = 159;
	m_LightArea_Tga[7] = 160;
	m_AllScoreTga	   = 161;
	m_ChangeBankTga    = 162;
    m_bankBg_Tga       = 163;
    m_JsBank_Tga       = 164;
    m_JsBackBack_Tga   = 165;
    m_JsXian_Tga       = 166;
    
	m_LzLeftzDui_Tga   = 300;
	m_LzLeft_Tga	   = 400;
	m_LzLeftxDui_Tga   = 500;
	m_LzRight_Tga	   = 600;
	m_LzPxTga		   = 700;
	m_LzRightzDui_Tga  = 800;
	m_LzRightxDui_Tga  = 1000;

	//pos
	m_HeadPos          = Vec2(250,100);
    m_NickNamePos      = Vec2(150,115);
    m_GlodPos          = Vec2(150,75);
    m_WinGlodPos       = Vec2(150,35);

	m_BankNickNamePos  = Vec2(350,1050);
	m_BankGoldPos      = Vec2(350,1015);
	m_BankZhangJiPos   = Vec2(350,980);
	m_BankJuShuPos	   = Vec2(350,945);

    //点击区域
	m_ClickPos[AREA_XIAN]           = Vec2(400, 570);   //闲家索引
	m_ClickPos[AREA_PING]           = Vec2(960, 570);   //平家索引
	m_ClickPos[AREA_ZHUANG]         = Vec2(1520, 570);  //庄家索引
	m_ClickPos[AREA_LONG]           = Vec2(400, 320);   //龙, 闲天王
	m_ClickPos[AREA_HU]             = Vec2(1520, 320);   //虎, 庄天王
	m_ClickPos[AREA_TONG_DUI]       = Vec2(960, 320);  //同点平
	m_ClickPos[AREA_XIAN_DUI]       = Vec2(400, 820);   //闲对子
	m_ClickPos[AREA_ZHUANG_DUI]     = Vec2(1520, 820);  //庄对子

	m_BetNumPos[AREA_XIAN]          = Vec2(400, 680);
	m_BetNumPos[AREA_PING]          = Vec2(960, 680);
	m_BetNumPos[AREA_ZHUANG]        = Vec2(1520, 680);
	m_BetNumPos[AREA_LONG]          = Vec2(400, 380);
	m_BetNumPos[AREA_HU]            = Vec2(1520, 380);
	m_BetNumPos[AREA_TONG_DUI]      = Vec2(960, 380);
	m_BetNumPos[AREA_XIAN_DUI]      = Vec2(400, 880);
	m_BetNumPos[AREA_ZHUANG_DUI]    = Vec2(1520, 880);

	

	m_BankBetPos	   = Vec2(286,731);
	m_XianBetPos       = Vec2(286,709);
	m_HeBetPos         = Vec2(286,687);
	m_SendCardAnimPos  = Vec2(1150,996);
	m_BankListPos[0]   = Vec2(20,200);
	m_BankListPos[1]   = Vec2(20,150);
	m_BankListPos[2]   = Vec2(20,105);
	m_BankListPos[3]   = Vec2(20,60);

	//变量
	m_lMeStatisticScore = 0;
	m_BankZhangJI = 0;
	m_BankJuShu = 0;
	m_LookMode = false;
	m_bNoBankUser = false;
	m_lMeMaxScore = 0;
	m_BankListBeginIndex = 0;
	m_lMeStatisticScore=0;
    m_IsLayerMove = false;
    
	//点击区域
	for (int i = AREA_XIAN; i < AREA_MAX; i++)
	{
        m_lAreaLimitScore[i]=0;
		char _name[128];
		sprintf(_name,"click%d.png",i);
		Sprite* temp = Sprite::createWithSpriteFrameName(_name);
		temp->setPosition(m_ClickPos[i]);
		temp->setTag(m_Click_Tga[i]);
		addChild(temp);
        
        string str = "";
        if (i == 1 || i == 5)
            str = "gai1.png";
        else
            str = "gai2.png";
        Sprite *gai = Sprite::createWithSpriteFrameName(str);
        if (i < 3)
            gai->setPosition(Vec2(m_ClickPos[i].x , m_ClickPos[i].y + 110));
        else
            gai->setPosition(Vec2(m_ClickPos[i].x , m_ClickPos[i].y + 60));
        
        addChild(gai);
	}
}

void BR30SGameViewLayer::AddButton()
{
	m_BtnBackToLobby = CreateButton("Returnback" ,Vec2(90,1000),Btn_BackToLobby);
	addChild(m_BtnBackToLobby , 0 , Btn_BackToLobby);
	
	m_BtnSeting = CreateButton("seting" ,Vec2(1830,1000),Btn_Seting);
	addChild(m_BtnSeting , 0 , Btn_Seting);
	
	m_BtnCallBank = CreateButton("BtnCallBank" ,Vec2(1500,990),Btn_CallBank);
	addChild(m_BtnCallBank , 0 , Btn_CallBank);

	m_BtnCancelBank = CreateButton("BtnCancleBank" ,Vec2(1500,990),Btn_CancleBank);
	addChild(m_BtnCancelBank , 0 , Btn_CancleBank);
	m_BtnCancelBank->setVisible(false);
	
	m_BtnLZ = CreateButton("lz_btn" ,Vec2(1550,70),Btn_LZ);
	addChild(m_BtnLZ , 0 , Btn_LZ);
    
    m_BankBtn = CreateButton("bankBtn" ,Vec2(1790,70),Btn_BankBtn);
    addChild(m_BankBtn , 0 , Btn_BankBtn);

    m_Gold1000 = CreateButton("chip_1000" ,Vec2(660,80),Btn_1000);
    addChild(m_Gold1000 , 1, Btn_1000);
    
    m_Gold1w = CreateButton("chip_1w" ,Vec2(810,80),Btn_1w);
    addChild(m_Gold1w , 1, Btn_1w);
    
    m_Gold10w = CreateButton("chip_10w" ,Vec2(960,80),Btn_10w);
    addChild(m_Gold10w , 1, Btn_10w);
    
    m_Gold100w = CreateButton("chip_100w" ,Vec2(1110,80),Btn_100w);
    addChild(m_Gold100w , 1, Btn_100w);
    
    m_Gold500w = CreateButton("chip_500w" ,Vec2(1260,80),Btn_500w);
    addChild(m_Gold500w , 1, Btn_500w);
}

//设置庄家
void BR30SGameViewLayer::SetBankerInfo(WORD wBanker,LONGLONG lScore)
{
    removeAllChildByTag(m_BankNickName_Tga);
    removeAllChildByTag(m_BankGold_Tga);
    removeAllChildByTag(m_BankZhangJi_Tga);
    removeAllChildByTag(m_BankJuShu_Tga);
    removeAllChildByTag(m_bankBg_Tga);
 
	m_wCurrentBanker=wBanker;
    
    ui::Scale9Sprite* playerInfoBg = ui::Scale9Sprite::createWithSpriteFrameName("bg_player_info.png");
    playerInfoBg->setContentSize(Size(520, 150));
    playerInfoBg->setPosition(Vec2(450, 1000));
    addChild(playerInfoBg, 0, m_bankBg_Tga);
    
    char name[256];
	int tableid = ClientSocketSink::sharedSocketSink()->GetMeUserData()->wTableID;
	tagUserData* pUserData = ClientSocketSink::sharedSocketSink()->m_UserManager.GetUserData(tableid,m_wCurrentBanker);
	if(m_wCurrentBanker == INVALID_CHAIR || pUserData == NULL) 
    {
        m_BankZhangJI = 0;
        m_lBankerScore = 0;
        m_BankJuShu = 0;
        m_bNoBankUser = true;
        
        Label* temp = Label::createWithSystemFont("无人坐庄", _GAME_FONT_NAME_1_,46);
        temp->setColor(Color3B::RED);
        temp->setTag(m_BankNickName_Tga);
        temp->setPosition(Vec2(450, 1000));
        addChild(temp);
        return;
    }
    else
    {
        m_lBankerScore=lScore;
        m_bNoBankUser = false;
        sprintf(name, "%s", gbk_utf8(pUserData->szNickName).c_str());
    }
  
    string heads = g_GlobalUnits.getFace(pUserData->wGender, pUserData->lScore);
    Sprite* mHead = Sprite::createWithSpriteFrameName(heads);
    mHead->setPosition(Vec2(75, 75));
    mHead->setScale(0.9f);
    playerInfoBg->addChild(mHead);
    
	//昵称
    std::string szTempTitle;
    szTempTitle = "昵称: ";
    szTempTitle.append(name);
    
    Label* temp = Label::createWithSystemFont(szTempTitle.c_str(),_GAME_FONT_NAME_1_,30);
    temp->setAnchorPoint(Vec2(0,0.5f));
    temp->setTag(m_BankNickName_Tga);
    temp->setPosition(m_BankNickNamePos);
    addChild(temp);
    
    //金币
    char strc[32]="";
    Tools::AddComma(m_lBankerScore , strc);
    szTempTitle = "酒吧豆: ";
    szTempTitle.append(strc);
    temp = Label::createWithSystemFont(szTempTitle, _GAME_FONT_NAME_1_, 30);
    temp->setAnchorPoint(Vec2(0,0.5f));
    temp->setTag(m_BankGold_Tga);
    temp->setColor(_GAME_FONT_COLOR_4_);
    temp->setPosition(m_BankGoldPos);
    addChild(temp);
    
    //战绩
    Tools::AddComma(m_BankZhangJI , strc);
    szTempTitle = "战绩: ";
    szTempTitle.append(strc);
    temp = Label::createWithSystemFont(szTempTitle, _GAME_FONT_NAME_1_, 30);
    temp->setAnchorPoint(Vec2(0,0.5f));
    temp->setTag(m_BankZhangJi_Tga);
    temp->setColor(_GAME_FONT_COLOR_4_);
    temp->setPosition(m_BankZhangJiPos);
    addChild(temp);
    
    //局数
    sprintf(strc,"%d",m_BankJuShu);
    szTempTitle = "局数: ";
    szTempTitle.append(strc);
    temp = Label::createWithSystemFont(szTempTitle, _GAME_FONT_NAME_1_, 30);
    temp->setAnchorPoint(Vec2(0,0.5f));
    temp->setTag(m_BankJuShu_Tga);
    temp->setColor(_GAME_FONT_COLOR_4_);
    temp->setPosition(m_BankJuShuPos);
    addChild(temp);
    
    UpdateButtonContron();
}

Menu* BR30SGameViewLayer::CreateButton(std::string szBtName ,const Vec2 &p , int tag )
{
	return Tools::Button(StrToChar(szBtName+"_normal.png") , StrToChar(szBtName+"_click.png") , p, this , menu_selector(BR30SGameViewLayer::callbackBt) , tag);
}

void BR30SGameViewLayer::AddPlayerInfo()
{
	if (ClientSocketSink::sharedSocketSink()->GetMeUserData() == NULL) return;
	int userid = ClientSocketSink::sharedSocketSink()->GetMeUserData()->dwUserID;
	const tagUserData* pUserData = ClientSocketSink::sharedSocketSink()->m_UserManager.SearchUserByUserID(userid);
	if (pUserData == NULL) return;
    
     removeAllChildByTag(m_Head_Tga);
    //头像
    ui::Scale9Sprite* playerInfoBg = ui::Scale9Sprite::createWithSpriteFrameName("bg_player_info.png");
    playerInfoBg->setContentSize(Size(450, 150));
    playerInfoBg->setPosition(Vec2(m_HeadPos.x, m_HeadPos.y+5));
    addChild(playerInfoBg, 0, m_Head_Tga);
    
    Sprite* headBg = Sprite::createWithSpriteFrameName("bg_head_img.png");
    headBg->setPosition(Vec2(70, 75));
    playerInfoBg->addChild(headBg);
    
    string heads = g_GlobalUnits.getFace(pUserData->wGender, pUserData->lScore);
    Sprite* mHead = Sprite::createWithSpriteFrameName(heads);
    mHead->setPosition(Vec2(75, 75));
    mHead->setScale(0.9f);
    playerInfoBg->addChild(mHead);
    
    removeAllChildByTag(m_Glod_Tga);
    char strc[32]="";
    LONGLONG lMon = pUserData->lScore;
    memset(strc , 0 , sizeof(strc));
    Tools::AddComma(lMon , strc);
  
    string goldstr = "酒吧豆: ";
    goldstr.append(strc);
    Label* mGoldFont;
    mGoldFont = Label::createWithSystemFont(goldstr,_GAME_FONT_NAME_1_,30);
    mGoldFont->setTag(m_Glod_Tga);
    playerInfoBg->addChild(mGoldFont);
    mGoldFont->setColor(Color3B::YELLOW);
    mGoldFont->setAnchorPoint(Vec2(0, 0.5f));
    mGoldFont->setPosition(m_GlodPos);
    
    //昵称
    removeAllChildByTag(m_NickName_Tga);
    string namestr = "昵称: ";
    namestr.append(gbk_utf8(pUserData->szNickName));
    Label* mNickfont = Label::createWithSystemFont(namestr, _GAME_FONT_NAME_1_, 30);
    mNickfont->setTag(m_NickName_Tga);
    playerInfoBg->addChild(mNickfont);
    mNickfont->setAnchorPoint(Vec2(0,0.5f));
    mNickfont->setPosition(m_NickNamePos);
    
    //输赢金币
    removeAllChildByTag(m_WinGlod_Tga);
    Tools::AddComma(m_lMeStatisticScore, strc);
    string winStr = "成绩: ";
    winStr.append(strc);
    mGoldFont = Label::createWithSystemFont(winStr,_GAME_FONT_NAME_1_,30);
    mGoldFont->setAnchorPoint(Vec2(0,0.5f));
    mGoldFont->setTag(m_WinGlod_Tga);
    mGoldFont->setPosition(m_WinGlodPos);
    playerInfoBg->addChild(mGoldFont);
}

string BR30SGameViewLayer::AddCommaToNum(LONG Num)
{
	char _str[256];
	sprintf(_str,"%ld", Num);
	string _string = _str;
	int step = _string.length()/3;
	for (int i = 1; i <= step; i++)
	{
		_string.insert(_string.length()-(i-1)-(i*3), ",");
	}
	return _string;
}


//申请消息
void BR30SGameViewLayer::OnApplyBanker(int wParam)
{
    if (m_StartTime < 2)
        return;
    
	//合法判断
	const tagUserData * pClientUserItem = GetMeUserData();
	//旁观判断
	if (IsLookonMode()) return;

	if (wParam == 1)
	{
		if (pClientUserItem->lScore < m_lApplyBankerCondition)
		{
			char TipMessage[128] = {0};
			sprintf(TipMessage, "\u60a8\u7684\u9152\u5427\u8c46\u4e0d\u8db3\uff0c\u65e0\u6cd5\u4e0a\u5e84\uff0c\u4e0a\u5e84\u6761\u4ef6\u4e3a\uff1a%lld\u9152\u5427\u8c46",m_lApplyBankerCondition);
			AlertMessageLayer::createConfirm(TipMessage);
			return;
		}
		//发送消息
		SendData(SUB_C_APPLY_BANKER, NULL, 0);

		//m_bMeApplyBanker=true;
	}
	else
	{
		//发送消息
		SendData(SUB_C_CANCEL_BANKER, NULL, 0);

		//m_bMeApplyBanker=false;
	}
    
    m_BtnCallBank->setEnabled(false);
    m_BtnCancelBank->setEnabled(false);
	
    //设置按钮
	UpdateButtonContron();
}


//申请消息
void BR30SGameViewLayer::OnCompeteBanker()
{
	//合法判断
	const tagUserData* pClientUserItem= GetMeUserData();

	if (pClientUserItem->lScore < m_lApplyBankerCondition+m_lCompetitionScore)
	{
		char TipMessage[128] = {0};
		sprintf(TipMessage, "\u60a8\u7684\u9152\u5427\u8c46\u4e0d\u8db3\uff0c\u65e0\u6cd5\u4e0a\u5e84\uff0c\u4e0a\u5e84\u6761\u4ef6\u4e3a\uff1a%lld\u9152\u5427\u8c46",m_lApplyBankerCondition);
		AlertMessageLayer::createConfirm(TipMessage);
		return;
	}

	//旁观判断
	if (IsLookonMode()) return;

	//当前判断
	if (m_wCurrentBanker == GetMeChairID()) return;

	//发送消息
	SendData(SUB_C_COMPTETE_BANKER, NULL, 0);

	//设置按钮
	UpdateButtonContron();
}


void BR30SGameViewLayer::callbackBt( Ref *pSender )
{
    if (m_OpenBankList)
    {
        CloseBankList();
        return;
    }
    else if (m_bLz_PX)
    {
        CloseLzList();
        return;
    }
    Node *pNode = (Node *)pSender;
	switch (pNode->getTag())
	{
	case Btn_GetScoreBtn:
		{	
			GetScoreForBank();
			break;
		}
	case Btn_LZ:
		{
            if (m_bLz_PX == false)
            {
                OpenLzList();
            }
            else
            {
                CloseLzList();
            }
            break;
            break;
		}
	case Btn_BankBtn:
		{
			if (m_OpenBankList == true)	CloseBankList();
			else OpenBankList();
			break;;
		}
	case Btn_BackToLobby: // 返回大厅按钮
		{
			SoundUtil::sharedEngine()->playEffect("buttonMusic");
			BackToLobby();
			break;
		}
	case Btn_Seting: //设置
		{
			SoundUtil::sharedEngine()->playEffect("buttonMusic");
			GameLayerMove::sharedGameLayerMoveSink()->OpenSeting();
			break;
		}
	case Btn_CallBank: //申请上庄
		{
			SoundUtil::sharedEngine()->playEffect("buttonMusic");
			OnApplyBanker(1);
			break;
		}
	case Btn_CancleBank: //取消上庄
		{
			SoundUtil::sharedEngine()->playEffect("buttonMusic");
			OnApplyBanker(0);
			break;
		}
	case Btn_1000:
		{
			m_CurSelectGold = 1000;
			SoundUtil::sharedEngine()->playEffect("buttonMusic");
			removeAllChildByTag(m_BtnAnim_Tga);
			Sprite *temp = Sprite::createWithSpriteFrameName("agoldselect.png");
			temp->setTag(m_BtnAnim_Tga);
			temp->setPosition(Vec2(660,80));
			addChild(temp);
			break;
		}
	case Btn_1w:
		{
			m_CurSelectGold = 10000;
			SoundUtil::sharedEngine()->playEffect("buttonMusic");
			removeAllChildByTag(m_BtnAnim_Tga);
			Sprite *temp = Sprite::createWithSpriteFrameName("agoldselect.png");
			temp->setTag(m_BtnAnim_Tga);
			temp->setPosition(Vec2(810 ,80));
			addChild(temp);
			break;
		}
	case Btn_10w:
		{
			m_CurSelectGold = 100000;
			SoundUtil::sharedEngine()->playEffect("buttonMusic");
			removeAllChildByTag(m_BtnAnim_Tga);
			Sprite *temp = Sprite::createWithSpriteFrameName("agoldselect.png");
			temp->setTag(m_BtnAnim_Tga);
			temp->setPosition(Vec2(960  ,80));
			addChild(temp);
			break;
		}
	case Btn_100w:
		{
			m_CurSelectGold = 1000000;
			SoundUtil::sharedEngine()->playEffect("buttonMusic");
			removeAllChildByTag(m_BtnAnim_Tga);
			Sprite *temp = Sprite::createWithSpriteFrameName("agoldselect.png");
			temp->setTag(m_BtnAnim_Tga);
			temp->setPosition(Vec2(1110 ,80));
			addChild(temp);
			break;
		}
	case Btn_500w:
		{
			m_CurSelectGold = 5000000;
			SoundUtil::sharedEngine()->playEffect("buttonMusic");
			removeAllChildByTag(m_BtnAnim_Tga);
			Sprite *temp = Sprite::createWithSpriteFrameName("agoldselect.png");
			temp->setTag(m_BtnAnim_Tga);
			temp->setPosition(Vec2(1260 ,80));
			addChild(temp);
			break;
		}
	}
}

void BR30SGameViewLayer::backLoginView( Ref *pSender )
{
	IGameView::backLoginView(pSender);	
}

void BR30SGameViewLayer::OnQuit()
{
	m_pGameScene->removeChild(this);
}

void BR30SGameViewLayer::CloseLzList()
{
    if (m_IsLayerMove)
        return;
    m_IsLayerMove = true;
    m_bLz_PX = false;
    MoveTo *leftMoveBy = MoveTo::create(0.2f, Vec2(_STANDARD_SCREEN_CENTER_.x, 1500));
    ActionInstant *func = CallFunc::create(CC_CALLBACK_0(BR30SGameViewLayer::LayerMoveCallBack,this));
    m_LzBack->runAction(Sequence::create(leftMoveBy, func, NULL));
}

void BR30SGameViewLayer::OpenLzList()
{
    if (m_IsLayerMove)
        return;
    m_IsLayerMove = true;
    m_bLz_PX = true;
    MoveTo *leftMoveBy = MoveTo::create(0.2f, _STANDARD_SCREEN_CENTER_);
    ActionInstant *func = CallFunc::create(CC_CALLBACK_0(BR30SGameViewLayer::LayerMoveCallBack,this));
    m_LzBack->runAction(Sequence::create(leftMoveBy, func, NULL));
}

void BR30SGameViewLayer::CloseBankList()
{
    if (m_IsLayerMove)
        return;
    m_IsLayerMove = true;
    m_OpenBankList = false;
    MoveTo *leftMoveBy = MoveTo::create(0.2f, Vec2(960,1500));
    ActionInstant *func = CallFunc::create(CC_CALLBACK_0(BR30SGameViewLayer::LayerMoveCallBack,this));
    m_BankListBackSpr->runAction(Sequence::create(leftMoveBy, func, NULL));
}
void BR30SGameViewLayer::OpenBankList()
{
    if (m_IsLayerMove)
        return;
    m_IsLayerMove = true;
    m_OpenBankList = true;
    MoveTo *leftMoveBy = MoveTo::create(0.2f, _STANDARD_SCREEN_CENTER_);
    ActionInstant *func = CallFunc::create(CC_CALLBACK_0(BR30SGameViewLayer::LayerMoveCallBack,this));
    m_BankListBackSpr->runAction(Sequence::create(leftMoveBy, func, NULL));
}


void BR30SGameViewLayer::LayerMoveCallBack()
{
    m_IsLayerMove = false;
}
