﻿#ifndef _BR30GAME_VIEW_LAYER_H_
#define _BR30GAME_VIEW_LAYER_H_

#include "GameScene.h"
#include "FrameGameView.h"
#include "BR30SGameLogic.h"
#include "CMD_BR30.h"
#include "BR30SGameUserList.h"
#include "BR30SGameViewLayer.h"
#include "ui/UIListView.h"

#define TOP_TITLE_POINT CCPointMake(_STANDARD_SCREEN_CENTER_.x , _STANDARD_SCREEN_CENTER_.y+230)
#define TOP_BEATDRUMLEFT_POINT CCPointMake(80 , _STANDARD_SCREEN_SIZE_.height-150+52)
#define TOP_BEATDRUMRIGHT_POINT CCPointMake(150+30 , _STANDARD_SCREEN_SIZE_.height-150+60)
#define TOP_BEAM_POINT CCPointMake(185 , _STANDARD_SCREEN_SIZE_.height-93)
#define TOP_TEXTSHZ_POINT CCPointMake(_STANDARD_SCREEN_CENTER_.x-4 , _STANDARD_SCREEN_SIZE_.height-64)
#define TOP_BANNER_POINT CCPointMake(_STANDARD_SCREEN_SIZE_.width-189+12 , _STANDARD_SCREEN_SIZE_.height-20-26-40-3)

enum
{
	BR30S_TAG_URL_ADV=0,
	BR30S_TAG_URL_SM_LOG,		//短信发送log
	BR30S_TAG_VALID
};

class CTimeTaskLayer;
class BR30SGameViewLayer : public IGameView
{
	struct tagApplyUser
	{
		//玩家信息
		char							strUserName[128];						//玩家帐号
        BYTE                            gender;
		LONGLONG						lUserScore;							//玩家金币
		bool							bCompetition;
	};

public:
	static BR30SGameViewLayer *create(GameScene *pGameScene);
	virtual ~BR30SGameViewLayer();

	virtual bool init(); 
	virtual void onEnter();
	virtual void onExit();

    virtual bool onTouchBegan(Touch *pTouch, Event *pEvent);
    virtual void onTouchMoved(Touch *pTouch, Event *pEvent);
    virtual void onTouchEnded(Touch *pTouch, Event *pEvent);

	virtual bool onTextFieldAttachWithIME(TextFieldTTF*pSender){return false;}
	virtual bool onTextFieldDetachWithIME(TextFieldTTF*pSender){return false;}
	virtual bool onTextFieldInsertText(TextFieldTTF *pSender, const char *text, int nLen){return true;}
	virtual bool onTextFieldDeleteBackward(TextFieldTTF *pSender, const char *delText, int nLen){return true;}

	virtual void keyboardWillShow(IMEKeyboardNotificationInfo &info){}
	virtual void keyboardDidShow(IMEKeyboardNotificationInfo &info){}
	virtual void keyboardWillHide(IMEKeyboardNotificationInfo &info){}
	virtual void keyboardDidHide(IMEKeyboardNotificationInfo &info){}
	virtual void DrawUserScore(){}	//绘制玩家分数
	virtual void GameEnd(){};
	virtual void ShowAddScoreBtn(bool bShow=true){};
	virtual void UpdateDrawUserScore(){};		//更新显示的游戏币数量

	void callbackBt( Ref *pSender );

	virtual void backLoginView(Ref *pSender);
	string  AddCommaToNum(LONG Num);

	//初始化
	void InitGame();
	void AddPlayerInfo();
	void AddButton();
	Menu* CreateButton(std::string szBtName ,const Vec2 &p , int tag);

	// 按钮事件管理
	void DialogConfirm(Ref *pSender);
	void DialogCancel(Ref *pSender);
	string GetCardStringName(BYTE card);
    
    void CloseLzList();
    void OpenLzList();
	void CloseBankList();
	void OpenBankList();
	void UpdataAllScore();

	//网络接口
public:
	void OnEventUserLeave( tagUserData * pUserData, WORD wChairID, bool bLookonUser );
	//用户进入
	virtual void OnEventUserEnter(tagUserData * pUserData, WORD wChairID, bool bLookonUser);
	//用户积分
	virtual void OnEventUserScore(tagUserData * pUserData, WORD wChairID, bool bLookonUser);
	//用户状态
	virtual void OnEventUserStatus(tagUserData * pUserData, WORD wChairID, bool bLookonUser);
    //用户换桌
    virtual void OnEventUserChangeTable(tagUserData * pUserData, WORD wChairID, bool bLookonUser){};
	//游戏消息
	virtual bool OnGameMessage(WORD wSubCmdID, const void * pBuffer, WORD wDataSize);
	//场景消息
	virtual bool OnGameSceneMessage(BYTE cbGameStatus, const void * pBuffer, WORD wDataSize);

	virtual void OnReconnectAction(){};

	void OnQuit();

protected:	
	BR30SGameViewLayer(GameScene *pGameScene);

protected:
	BR30SGameLogic		m_GameLogic;

	//图片
	Sprite*			m_BackSpr;
	Sprite*          m_ClockSpr;
	Sprite*			m_LzBack;
	Sprite*			m_BankListBackSpr;
    
    ui::ListView*       m_listView;
	//按钮
	Menu*				m_BtnBackToLobby;
	Menu*				m_BtnSeting;
	Menu*				m_BtnCallBank;
	Menu*				m_BtnQiangBank;
	Menu*				m_BtnCancelBank;
	Menu*				m_Gold100;
	Menu*				m_Gold1000;
	Menu*				m_Gold1w;
	Menu*				m_Gold10w;
	//CCMenu*				m_Gold50w;
	Menu*				m_Gold100w;
	Menu*				m_Gold500w;
    Menu*				m_Gold1000w;
	Menu*				m_BtnLZ;
	Menu*				m_BtnPX;
	Menu*				m_BtnLz_Left;
	Menu*				m_BtnLz_Right;
	

	Menu*				m_BankBtn;
	Menu*				m_BankUp;
	Menu*				m_BankDown;
	Menu*				m_BtnGetScoreBtn;

	//索引
	int					m_Head_Tga;			//自己的信息
	int					m_NickName_Tga;
	int					m_Glod_Tga;
	int					m_WinGlod_Tga;
    int                 m_bankBg_Tga;
	int					m_BankNickName_Tga;	//庄家信息
	int					m_BankGold_Tga;
	int					m_BankZhangJi_Tga;
	int					m_BankJuShu_Tga;
	int					m_TimeType_Tga;
	int					m_Click_Tga[8];
	int					m_BetScore_Tga[8];	//下注额度
	int					m_BankBet_Tga;		//庄可下
	int					m_XianBet_Tga;		//闲可下
	int					m_HeBet_Tga;		//和可下
	int					m_SendCardAnim_Tga; //发牌动画
	int					m_LightArea_Tga[8]; //高亮区域
	int					m_XianCard_Tga;
	int                 m_ZhuangCard_Tga;
	int					m_XianPoint_Tga;
	int					m_ZhuangPoint_Tga;
	int					m_BtnAnim_Tga;
	int					m_JsBackBack_Tga;
	int					m_JsBank_Tga;
	int					m_JsXian_Tga;
	int					m_BetScore1_Tga[8];	//下注额度
	int					m_StartBetText_Tga;
	int					m_BankList_Tga[4];
	int					m_BankList1_Tga[4];
	int					m_LzLeft_Tga;
	int					m_LzLeftzDui_Tga;
	int					m_LzLeftxDui_Tga;
	int					m_LzRight_Tga;
	int					m_LzRightzDui_Tga;
	int					m_LzRightxDui_Tga;
	int					m_LzPxTga;
	int					m_AllScoreTga;
	int					m_ChangeBankTga;

	//位置
	Vec2				m_HeadPos;          //自己的信息
	Vec2				m_NickNamePos;
	Vec2				m_GlodPos;
	Vec2				m_WinGlodPos;
	Vec2				m_BankNickNamePos;	//庄家信息
	Vec2				m_BankGoldPos;
	Vec2				m_BankZhangJiPos;
	Vec2				m_BankJuShuPos;
	Vec2				m_ClickPos[8];
	Vec2				m_BetNumPos[8];
	Vec2				m_BankBetPos;
	Vec2				m_XianBetPos;
	Vec2				m_HeBetPos;
	Vec2				m_SendCardAnimPos;
	Vec2				m_BankListPos[4];

	//变量
	bool				m_bLz_PX;
	int					m_BankUserListSize;
	bool				m_OpenBankList;
	LONGLONG			m_lCompetitionScore;				//抢庄扣除
	LONGLONG			m_lApplyBankerCondition;			//申请条件
	LONGLONG			m_lAreaLimitScore[AREA_MAX];		//区域限制
	LONGLONG			m_lMeMaxScore;
	int					m_CurSelectGold;
	int					m_StartTime;
	LONGLONG            m_lMeStatisticScore;
	int					m_wCurrentBanker;	
	LONGLONG			m_lBankerScore;
	LONGLONG			m_BankZhangJI;
	int					m_BankJuShu;
	bool				m_LookMode;
	LONGLONG			m_lAllJettonScore[AREA_MAX];
	LONGLONG			m_lUserJettonScore[AREA_MAX];	    //个人总注
	LONGLONG			m_lPlayAllScore;
	LONGLONG			m_lBankerCurGameScore;
	bool				m_bNoBankUser;
	bool				m_LzShow;

	int 				m_FirstSend;  // 0 闲， 1庄
	int					m_SendCardIndex;
	int					m_XianSendCardIndex;
	int					m_ZhuangSendCardIndex;
	BYTE				m_cbCardCount[2];					//扑克数目
	BYTE				m_cbTableCardArray[2][3];			//桌面扑克

	int					m_BankListBeginIndex;
	vector<tagApplyUser> m_BankUserList;

	BYTE				m_Index1;							//牌型索引
	BYTE				m_Index2;							//牌型索引
	BYTE				m_Index3;							//牌型索引
	int					m_nRecordLast;						//最后记录
	vector<BR30S_tagServerGameRecord>	 m_GameRecordArrary;		//游戏记录
	bool				m_bBetSound;
	bool				m_IsMeBank;
    bool                m_IsLayerMove;
public:
	static void reset();

	// 防止直接引用 
	BR30SGameViewLayer(const BR30SGameViewLayer&);
    BR30SGameViewLayer& operator = (const BR30SGameViewLayer&);


//发送网络消息

public:
	void SendGameStart();

protected:
	bool OnBet(const void * pBuffer, WORD wDataSize);

	bool OnBetFail();

	//游戏空闲
	bool OnSubGameFree(const void * pBuffer, WORD wDataSize);
	//游戏开始
	bool OnSubGameStart(const void * pBuffer, WORD wDataSize);
	//用户加注
	bool OnSubPlaceJetton(const void * pBuffer, WORD wDataSize);
	//申请做庄
	bool OnSubUserApplyBanker(const void * pBuffer, WORD wDataSize);
	//取消做庄
	bool OnSubUserCancelBanker(const void * pBuffer, WORD wDataSize);
	//切换庄家
	bool OnSubChangeBanker(const void * pBuffer, WORD wDataSize);
	//游戏结束
	bool OnSubGameEnd(const void * pBuffer, WORD wDataSize);
	//下注失败
	bool OnSubPlaceJettonFail(const void * pBuffer, WORD wDataSize);
	//抢庄
	bool OnSubUserCompeteBanker(const void * pBuffer, WORD wDataSize);
	//游戏记录
	bool OnSubGameRecord(const void * pBuffer, WORD wDataSize);

	void SetGameHistory(BR30S_tagServerGameRecord * pServerGameRecord);
	void UpdataGameHistory();
	void DeleteBankList(tagApplyUser ApplyUser);
	void DrawBankList();
	void OnCompeteBanker();
	void OnApplyBanker(int wParam);
	LONGLONG GetUserMaxJetton(BYTE cbBetArea);
	void DrawEndScore();
	void UpdateButtonContron();
	void SetWinArea(float fp);	//设置赢得区域
	void XianAnimCallback(Node *pSender,void*data);  //两个参数
	void ZhuangAnimCallback(Node *pSender,void*data);  //两个参数
    void LayerMoveCallBack();
	void SendCard();
	void SetCardInfo(BYTE cbCardCount[2], BYTE cbTableCardArray[2][3]);
	void SetEndScore(int chair, LONGLONG score);
	void SetMePlaceJetton(int chair, LONGLONG score);
	bool IsLookonMode();
	void SetBankerInfo(WORD wBanker,LONGLONG lScore);
	void UpdateTime(float fp);
	void StopTime();
	void StartTime(int _time, int _type);
	void SetXianSound(float fp);
	void SetZhuangSound(float fp);
	void WinSound(float fp);
	void BetSound(float fp);
    void addWinSpr(int index, Size size);
};

#endif
