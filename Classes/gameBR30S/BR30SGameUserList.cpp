#include "BR30SGameUserList.h"
#include "GameUserInfo.h"
#include "ClientSocketSink.h"

BR30GameSUserList::BR30GameSUserList(GameScene *pGameScene):GameLayer(pGameScene)
{
	m_tableView = NULL;
}

BR30GameSUserList::~BR30GameSUserList()
{

}

void BR30GameSUserList::callbackBt(Ref *pSender)
{
	Node *pNode = (Node *)pSender;
	int tga = pNode->getTag();
	if (tga == 1) 
	{
		SoundUtil::sharedEngine()->playEffect("buttonMusic");
		close();
		return;
	}
}

bool BR30GameSUserList::init()
{
	if ( !Layer::init() )
	{
		return false;
	}

	setLocalZOrder(4);
	setTouchEnabled(true);

	//创建一个数组  
	m_Arr = __Array::createWithCapacity(100);
	m_Arr->retain();  

	m_tableView = TableView::create(this, Size(140,270));
    m_tableView->setDirection(ScrollView::Direction::HORIZONTAL);
    m_tableView->setVerticalFillOrder(TableView::VerticalFillOrder::BOTTOM_UP);
	m_tableView->setAnchorPoint(Vec2(0.5,0.5));
	m_tableView->setPosition(Vec2(0,2));
	m_tableView->setDelegate(this);
	addChild(m_tableView);

    auto listener = EventListenerTouchOneByOne::create();
    listener->onTouchBegan = CC_CALLBACK_2(BR30GameSUserList::onTouchBegan, this);
    listener->onTouchMoved = CC_CALLBACK_2(BR30GameSUserList::onTouchMoved, this);
    listener->onTouchEnded = CC_CALLBACK_2(BR30GameSUserList::onTouchEnded, this);
    Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(listener, this);
    
	return true;
}

void BR30GameSUserList::close()
{
	m_pGameScene->removeChild(this);
}

void BR30GameSUserList::onRemove()
{
	m_pGameScene->removeChild(this);
}

void BR30GameSUserList::reloadData()
{
	if (m_tableView != NULL)
	{
		m_tableView->reloadData();
	}
}

void BR30GameSUserList::UpdataList()
{
	m_LineCount.clear();
	int _t = ClientSocketSink::sharedSocketSink()->m_UserManager.GetOnLineCount();
	for (int i = 0; i < _t; i++)
	{
		tagUserData *  p = ClientSocketSink::sharedSocketSink()->m_UserManager.EnumUserItem(i);
		if (p == NULL ) continue;
		UserList temp;
		sprintf(temp.strUserName, "%s", p->szNickName);
		temp.lUserScore = p->lScore;
		m_LineCount.push_back(temp);
	}

	m_Arr->removeAllObjects();
	for(int i = 0; i < m_LineCount.size(); i++)
	{  
		TableViewCell* cell = new TableViewCell();
		cell->autorelease();  
		m_Arr->addObject(cell);  
		
		//昵称
		char name[128]={0};
		sprintf(name , "%s",m_LineCount[i].strUserName);
		std::string szRank = name;
		Label *pLabel= createLabel(szRank , Vec2(0,270) , 20, Color3B(255,226,120) , true);
		pLabel->setRotation(90);
		//pLabel->setAnchorPoint(ccp(0.5,0));
		cell->addChild(pLabel);

		//数字
		sprintf(name , "%lld",m_LineCount[i].lUserScore);
		szRank = name;
		pLabel= createLabel(szRank , Vec2(0,125) , 20, Color3B(255,226,120) , true);
		//pLabel->setAnchorPoint(ccp(0.5,0));
		pLabel->setRotation(90);
		cell->addChild(pLabel);
	} 
	m_tableView->reloadData();
}

bool BR30GameSUserList::onTouchBegan(Touch *pTouch, Event *pEvent )
{
	m_tableView->onTouchBegan(pTouch , pEvent);
	return isVisible();
}

void BR30GameSUserList::onTouchEnded(Touch *pTouch, Event *pEvent )
{
	m_tableView->onTouchEnded(pTouch , pEvent);
}

void BR30GameSUserList::onTouchMoved(Touch *pTouch, Event *pEvent )
{
	m_tableView->onTouchMoved(pTouch , pEvent);
}

void BR30GameSUserList::onTouchCancelled(Touch *pTouch, Event *pEvent )
{
	m_tableView->onTouchCancelled(pTouch , pEvent);
}

void BR30GameSUserList::tableCellTouched(TableView* table, TableViewCell* cell)
{
}

Size BR30GameSUserList::tableCellSizeForIndex(TableView *table,  ssize_t idx)
{
	return Size(25,270);
}


TableViewCell* BR30GameSUserList::tableCellAtIndex(TableView *table, ssize_t idx)
{
	return (TableViewCell*)m_Arr->getObjectAtIndex(idx);
}

ssize_t BR30GameSUserList::numberOfCellsInTableView(TableView *table)
{
	return m_Arr->count();
}

BR30GameSUserList* BR30GameSUserList::create(GameScene *pGameScene)
{
	BR30GameSUserList* temp = new BR30GameSUserList(pGameScene);
	if(temp && temp->init())
	{
		temp->autorelease();
		return temp;
	}
	else
	{
		CC_SAFE_DELETE(temp);
		return NULL;
	}
}

void BR30GameSUserList::onEnter()
{
	GameLayer::onEnter();
}

void BR30GameSUserList::onExit()
{
	GameLayer::onExit();
}

Menu* BR30GameSUserList::CreateButton( std::string szBtName ,const Vec2 &p , int tag )
{
	Menu *pBT = Tools::Button(StrToChar(szBtName+"_normal.png") , StrToChar(szBtName+"_click.png") , p, this , menu_selector(BR30GameSUserList::callbackBt) , tag);
	return pBT;
}

Label * BR30GameSUserList::createLabel(const char *szText ,const Vec2 &p, int fontSize ,Color3B color, bool bChinese)
{
	std::string szTemp = szText;
	return createLabel(szTemp , p , fontSize , color , bChinese);
}
Label * BR30GameSUserList::createLabel(const std::string szText ,const Vec2 &p, int fontSize ,Color3B color, bool bChinese)
{

	std::string szTempTitle = szText;
	if (bChinese && szTempTitle.length() != 0)
	{
#ifdef _WIN32
		Tools::GBKToUTF8(szTempTitle , "gb2312" , "utf-8");
#endif // _WIN32
	}

	Label *pLable = Label::createWithSystemFont(szTempTitle.c_str(), _GAME_FONT_NAME_1_, fontSize);
    pLable->setHorizontalAlignment(TextHAlignment::LEFT);
	pLable->setColor(color);
	pLable->setAnchorPoint(Vec2(0,0));
	pLable->setPosition(ccppx(p));
	return pLable;
}
