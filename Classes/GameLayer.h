﻿#ifndef _GAME_LAYER_H_
#define _GAME_LAYER_H_

#include "cocos2d.h"

#include "GameParm.h"
#include "GameConstant.h"
#include "Resource.h"

#define StrToChar(str)					(((std::string)(str)).c_str())


class GameScene;
class GameLayer : public Layer, public IMEDelegate, public TextFieldDelegate
{
public:
	
	// static GameScene *create(GameScene *pGameScene);

	virtual ~GameLayer();

	// 进入 Layer 触发
	virtual void onEnter();

	// 离开 Layer 触发
	virtual void onExit();

	// 播放 背景 音乐
	virtual void PlayBackMusic(const char *pMusicName, bool bloop);
	virtual void PlayBackMusic(const char *pMusicName);
	virtual void StopBackMusic(void);
	virtual void SetMusicVolume(float fv);

	// 播放 背景 效
	virtual void PlayEffect(const char *pSoundName);

	// 切换 主 Layer
	void ReplaceMainGameLayer(GameLayer *pNewLayer);

	// 清除 Alert Message Layer
	void RemoveAlertMessageLayer(void);

	void removeAllChildByTag(int tag);

	// default implements are used to call script callback if exist
    virtual bool onTouchBegan(Touch *pTouch, Event *pEvent);
    virtual void onTouchMoved(Touch *pTouch, Event *pEvent);
    virtual void onTouchEnded(Touch *pTouch, Event *pEvent);
    virtual void onTouchCancelled(Touch *pTouch, Event *pEvent);

    // default implements are used to call script callback if exist
    virtual void onTouchesBegan(__Set *pTouches, Event *pEvent);
    virtual void onTouchesMoved(__Set *pTouches, Event *pEvent);
    virtual void onTouchesEnded(__Set *pTouches, Event *pEvent);
    virtual void onTouchesCancelled(__Set *pTouches, Event *pEvent);

//	virtual void didAccelerate(Acceleration*pAccelerationValue);

	/**If isTouchEnabled, this method is called onEnter. Override it to change the
    way CCLayer receives touch events.
    ( Default: CCTouchDispatcher::sharedDispatcher()->addStandardDelegate(this,0); )
    Example:
    void CCLayer::registerWithTouchDispatcher()
    {
    CCTouchDispatcher::sharedDispatcher()->addTargetedDelegate(this,INT_MIN+1,true);
    }
    @since v0.8.0
    */
//    virtual void registerWithTouchDispatcher(void);
    
    /**Register script touch events handler */
    virtual void registerScriptTouchHandler(int nHandler, bool bIsMultiTouches = false, int nPriority = INT_MIN, bool bSwallowsTouches = false);
    /**Unregister script touch events handler */
    virtual void unregisterScriptTouchHandler(void);

    /**whether or not it will receive Touch events.
    You can enable / disable touch events with this property.
    Only the touches of this node will be affected. This "method" is not propagated to it's children.
    @since v0.8.1
    */
    virtual bool isTouchEnabled();
    virtual void setTouchEnabled(bool value);
    
    virtual void setTouchMode(Touch::DispatchMode mode);
    virtual Touch::DispatchMode getTouchMode();
    
    /**priority of the touch events. Default is 0 */
    virtual void setTouchPriority(int priority);
    virtual int getTouchPriority();

	virtual void SetAllTouchEnabled(bool IsTouch);
	virtual void SetAllKeypadDisabled(void);
	virtual void DialogConfirm(Ref *pSender);
	virtual void DialogCancel(Ref *pSender);

protected:	

	GameLayer(GameScene *pGameScene);

public:
	GameScene *m_pGameScene;

private:

	// 防止直接引用 
	GameLayer(const GameLayer&);
    GameLayer& operator = (const GameLayer&);
};

#endif

