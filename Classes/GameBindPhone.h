#ifndef _GameBINDHPONE_H_
#define _GameBINDHPONE_H_

#include "GameScene.h"
#include "cocos-ext.h"

//银行
class GameBindPhone : public GameLayer
{
public:
	virtual bool init();  
	static GameBindPhone *create(GameScene *pGameScene);
	virtual ~GameBindPhone();
	GameBindPhone(GameScene *pGameScene);
	GameBindPhone(const GameBindPhone&);
	GameBindPhone& operator = (const GameBindPhone&);
	enum Tag
	{
		_BtnCancel,
		_BtnDownOk,
		_BtnOk
	};

public:
    cocos2d::ui::EditBox*	m_Tel;
    cocos2d::ui::EditBox*	m_QQMail;
	int                     m_BackTga;

public:
	virtual void onEnter();
	virtual void onExit();
	virtual bool onTouchBegan(Touch *pTouch, Event *pEvent){return true;}
	virtual void onTouchEnded(Touch *pTouch, Event *pEvent){}
	virtual void onTouchMoved(Touch *pTouch, Event *pEvent){}
	virtual void callbackBt(Ref *pSender);

	void PoPLock();	//提示下载手机锁
	void PoPNeedUnLock();  //需要解锁才能登陆

	Menu* CreateButton( std::string szBtName ,const Vec2 &p , int tag );
	void close();
	void onRemove();
	Label * createLabel(const char *szText ,const Vec2 &p, int fontSize ,Color3B color, bool bChinese=false);
	Label * createLabel(const std::string szText ,const Vec2 &p, int fontSize ,Color3B color, bool bChinese=false);
};
#endif