#ifndef _NUMBERUTIL_H_
#define _NUMBERUTIL_H_

#include "GameScene.h"

#define		NUMBER_LABEL_COUNT													20

#define		NUM_BIG																"numberlabel_big.png"
#define		NUM_CANNON															"numberlabel_cannon.png"
#define		NUM_DOUBLE_BIG														"numberlabel_double_big.png"
#define		NUM_DOUBLE_SMALL													"numberlabel_double_small.png"
#define		NUM_LVPOPUP															"numberlabel_lvpopup.png"
#define		NUM_SINGLEWIN0														"numberlabel_singlewin0.png"
#define		NUM_SMALL															"numberlabel_small.png"
#define		NUM_LOCKLEVEL														"numberlabel_locklevel.png"
#define		NUM_LOCKSCORE														"numberlabel_lockscore.png"
#define		NUM_FISHTOP															"numberlabel_fishtop.png"
#define		NUM_PEARLSCORE														"numberlabel_pearlscore.png"
#define		NUM_LOBBYSCORE														"numberlabel_lobbyscore.png"
#define		NUM_LOBBYCANNON														"numberlabel_lobbycannon.png"
#define		NUM_SINGLEWIN1														"numberlabel_singlewin1.png"
#define		NUM_PAYMONEY														"numberlabel_paymoney.png"

#define		NUM_GAMETASKAMOUNT													NUM_LOCKLEVEL
#define		NUM_TASKFINISHPRIZE													NUM_LVPOPUP
#define		NUM_LEVEL															NUM_LVPOPUP
#define		NUM_SCORE															NUM_FISHTOP
#define		NUM_TASKPRIZE														NUM_PEARLSCORE


class NumberUtil
{
public:
	NumberUtil(void);
	~NumberUtil(void);

public:
	static LabelAtlas * createNumberLabel(LONGLONG lNum, const char *szNumLabel);

	static LabelAtlas * createNumberLabel(LONG lNum, const char *szNumLabel);

	static LabelAtlas * createNumberLabel(char *szNum, const char *szNumLabel, int iSize = 10);

	static LabelAtlas * createFishScore(char *szNum, WORD wMulity , bool bDouble);
};

#endif
