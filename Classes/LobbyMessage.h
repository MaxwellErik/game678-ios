﻿#ifndef _LOBBY_MESSAGE_H_
#define _LOBBY_MESSAGE_H_

#include "FactoryMessage.h"

#define WM_UPDATE_HWND	WM_USER+112
#define WM_GAME_HWND	WM_USER+111
#define WM_UPDATE_SUCCESS WM_USER+113 
#define WM_WRITE_MAIL_SUCC  WM_USER+116			//成功写入邮件通知
#define WM_WRITE_MAIL_UNREAD	WM_USER+117		//提示是否有未读邮件
#define WM_OPEN_MAIL	WM_USER+118				//提示已经打开邮箱 大厅停止闪烁
#define WM_CHANGE_VIPTYPE	WM_USER+119			//改变用户VipType
#define WM_LONG_TIME_NOWORK		WM_USER+120		//长时间不操作
#define WM_SHOW_LOTTERY			WM_USER+121		//显示周年庆弹出框
#define WM_CLOSE_LOTTERY		WM_USER+122		//关闭周年庆对话框
#define WM_GAME_ACTMONEY        WM_USER+124     //活动比大于20
//#define WM_SHOW_BSXQ			WM_USER+123		//显示比赛详情

#define MESSAGE_VERSION  3      //消息版本

#define  MAX_KIND_INFO	5

#define ROOM_MAX_ONLINE		300

/*define the message type*/ 

#define GAME_KIND_RES_MSG						0x03			//服务器游戏种类信息回应
#define GAME_INFO_RES_MSG          				0x04			//服务器游戏信息回应
#define GAME_SERVER_INFO_RES_MSG				0x05			//游戏服务器信息回应
#define ROOM_TYPE_INFO_RES_MSG					0x06			//游戏房间种类信息
#define ROOM_INFO_RES_MSG						0x07			//具体游戏房间信息
#define SELF_SERVICE_RES_MSG					0x08			//服务器自助服务信息回应
#define LOBBY_INFO_REQ_MSG						0x09			//客户端请求大厅信息
#define	TIP_RES_MSG								0x25			//大厅提示信息的回应 
#define CLOSE_TCP_NOTICE_MSG									0xff			//通知客户端关闭TCP链接
//内部消息
#define CONNECT_MSG										0x0c			//客户端连接消息
#define DISCONNECT_MSG								0x0d			//客户端断开消息

//1:公共消息
#define   KEEP_ALIVE_MSG_1                   0x10  //保持连接消息

//2:大厅Server发起的Radius消息
#define		USER_AUTHEN_REQ_RADIUS_MSG			 0x20		//玩家登陆大厅验证请求
#define		USER_QUIT_REQ_RADIUS_MSG				 0x21		//玩家离开大厅请求，设置登陆状态为0
#define   USER_PLAY_INFO_RADIUS_MSG				 0x22		//玩家进入游戏信息

#define   AUTHEN_REQ_RADIUS_MSG						 0x51	 //服务器登陆Radius请求
//3:Radius回应大厅Server消息
#define		USER_AUTHEN_RES_RADIUS_MSG			 0x30		//登陆回应
                    
#define   AUTHEN_RES_RADIUS_MSG				     0x61	 //服务器登陆Radius请求回应

 
//
//typedef struct LobbyAuthenReq					//LOBBY_AUTHEN_REQ_MSG  0x01  玩家请求大厅认证
//{
//	MsgHeadDef	msgHeadInfo;
//	char	szUserName[32];	/*用户名*/
//	char	szUserPasswd[50];	/*密码*/
//	char	cType;				//登录类型 /1为手机登陆
//	char  szPCInfo[100]; //获取PC硬件信息
//	char  szMacAddr[20];//MAC地址
//	int  iLobbyVersion;		//大厅版本号
//	int  iClientAreaID;	//大厅客户端地推标记
//}LobbyAuthenReqDef;	


typedef struct LobbyInfoReq	//LOBBY_INFO_REQ_MSG 09	客户端请求大厅信息
{
	MsgHeadDef	msgHeadInfo;
}LobbyInfoReqDef;

typedef struct GameKindInfoRes		//GAME_KIND_RES_MSG	03游戏种类回应
{
	MsgHeadDef  msgHeadInfo;
	char				cFlag;
	char				cKindNum;		//游戏类型总数
	char                szEmpty[2];
}GameKindInfoResDef;

typedef struct GameKindInfoResExtra	//匹配SINOGAME数据库的KINDS表
{
	char	cKindID;					//类型ID。
	char	szKindName[16];		//类型名，英文，请根据约定翻译成中文。
	char    szEmpty[3];
}GameKindInfoResExtraDef;

typedef struct GameInfoResExtra				/*每个游戏信息消息结构*/
{
	char  cKindID;						//类型ID
	unsigned char	cGameID;			//游戏ID
	char	szGameName[16];			    //游戏名，，英文，，请根据约定翻译成中文
	char	cGameState;					//游戏状态,测试状态，热门游戏状态，新游戏状态，推荐游戏状态，比赛游戏状态
	char  cPlayerNum;					//游戏玩家人数
	char	cGameType;					//0为斗地主类游戏，1麻将类，2梭哈类，3为十三支类
	char  szEmpty[3];
	char  cGameStatus;                  //0：正常、1：内测、2：公测、3：维护、4：活动、5：改版 
}GameInfoResExtraDef;

typedef struct GameInfoRes					/*GAME_INFO_RES_MSG      04    			游戏信息消息*/		
{
	MsgHeadDef	msgHeadInfo;
	char				cGameNum;					/*可玩游戏总数目*/
	char  szEmpty[3];
}GameInfoResDef;

typedef struct ServerInfoRes		    /*SERVER_INFO_RES_MSG  05	游戏服务器信息消息*/
{
	MsgHeadDef	msgHeadInfo;
	unsigned short	iGameServerNum;		/*可用游戏服务器总数目*/
	char  szEmpty[2];
}ServerInfoResDef;

typedef struct ServerInfoResExtra		/*每个游戏服务器信息消息结构*/
{
	char	        cKindID;
	unsigned char	cGameID;				/*游戏ID*/
	unsigned short 	iGameServerID;		/*游戏服务器ID*/
	unsigned long	iGameServerIP;			//游戏服务器IP
	unsigned short  iGameServerPort;			//游戏服务器端口
	unsigned long	iGameServerIPBak;		//游戏服务器IP备份（网通）
	unsigned short  iGameServerPortBak;		//游戏服务器端口备份（网通）
	unsigned char   cServerNum;
	char  cEmpty;
}ServerInfoResExtraDef;

typedef struct RoomTypeInfoRes   //ROOM_TYPE_INFO_RES_MSG	06 游戏房间类型信息回应
{
	MsgHeadDef   msgHeadInfo;
	unsigned short	iTypeNum;
	char  szEmpty[2];
}RoomTypeInfoResDef;

typedef struct RoomTypeInfoResExtra	//游戏房间类型信息，，不是具体实际的多少个房间。
{
	char	cKindID;					//类型ID
	unsigned char  cGameID;					//游戏ID
	char  cRoomTypeID;			//房间类型ID
	char  szEmpty;
	int   iMoneyLimitation;	//进入的财富限制
	int   iMaxMoneyLimit;	//进入的最多星币数
	int   iBasePoint;				//梭哈类游戏基础押注，博彩游戏使用
	int   iMinPoint;				//梭哈类游戏最小押注，博彩游戏使用
	int   iMaxPoint;				//梭哈类游戏最大押注，博彩游戏使用
	int   iMinTime;					//休闲类游戏最小基础倍数，休闲游戏
	int   iMaxTime;					//休闲类游戏最大倍数，休闲游戏
	int	  iMaxWin;					//每局封顶
	char  cCheckend;         //可点房间1为正常房间2为即将推出房间0为不可点房间
	char  cMoneyRate;				//筹码兑换比例。
	char  szVipType[20];      //可进入房间的玩家类型,
	char  cIfRecommend;			//1.推荐房间，2.元宝房间，3聚宝盆房间
	char  cIfFreeRoom;      //是否免费房间
	char  cIfActivityStatus;       //是否开启活动状态   
}RoomTypeInfoResExtraDef;

typedef struct RoomInfoRes   //ROOM_INFO_RES_MSG   07	具体游戏房间信息回应
{
	MsgHeadDef   msgHeadInfo;
	unsigned short 	iRoomNum;
	char  szEmpty[2];
}RoomInfoResDef;

typedef struct RoomInfoResExtra
{
	char           cKindID;
	unsigned char  cGameID;  ////游戏ID
	char           cRoomTypeID;  //房间类型ID
	char           cRoomID;
	unsigned short iServerID;
	unsigned short iRoomOnlineNum;
	char           szRoomName[22];
	char	       cBeginTime;				
	char	       cEndTime;
}RoomInfoResExtraDef;

typedef struct SelfService					/*SELF_SERVICE_RES_MSG  08  系统信息消息	*/
{
	MsgHeadDef	msgHeadInfo;
	char			cSystemInfoNum;				/*自助服务类型种数*/
}SelfServiceResDef;	

typedef struct SelfServiceResExtra
{
	char		szName[16];					/*信息名*/
	char		szValue[100];				/*对应的URL*/
}SelfServiceResExtraDef;
 
typedef struct TipRes					//TIP_RES_MSG										0x25			//大厅提示信息的回应
{
	MsgHeadDef	msgHeadInfo;
	char 				szContent[100];//内容
}TipResDef;
 
//************************************************************
//************       大厅客户端 <-> 登录服务器         **********
//************************************************************

enum
{
	LOBBY_AUTHEN_REQ = 0x8000,
	LOBBY_AUTHEN_RES,
	LOBBY_AUTHEN_RES_2,
	LOBBY_HECHENG_REQ,				//合成参赛卷
	LOBBY_HECHEGN_RES,
	LOBBY_HECHENG_RES_2,
	GAME_SERVERINFO_NOTICE
};

//每款游戏开100个房间
static const long SERVER_SIZE = 100;
//登录服务器返回给大厅客户端的玩家数据; 登录服务器 -> 大厅客户端

#pragma pack(push, 4)

typedef struct tagLobbyAuthenRes
{
	MsgHeadDef	msgHeadInfo;
	unsigned long   ulUserID;                       //用户ID
	long long       nScore;                         //用户银子
	unsigned long   ulCoin2Award;                   //银券通
	unsigned long   ulCompeteScore;                 //参赛券
	unsigned long   ulAwardScore;                   //奖品券
	unsigned char   ucGender;                       //用户性别
}LobbyAuthenRes, *PLobbyAuthenRes;
#pragma pack(pop)

#pragma pack(push, 4)

typedef struct tagLobbyHeChengRes
{
	MsgHeadDef	msgHeadInfo;
	long long       nScore;                         //用户银子
	unsigned long   ulCoin2Award;                   //银券通
	unsigned long   ulCompeteScore;                 //参赛券
}LobbyHeChengRes,  *PLobbyHeChengRes;

#pragma pack(pop)

typedef struct LobbyHeCheng
{
	MsgHeadDef	msgHeadInfo;
	int cChoose;
}LobbyHeChengReq,  *PLobbyHeChengReq;

 
//大厅客户端验证请求;  大厅客户端 -> 登录服务器

#pragma pack(push, 4)

typedef struct tagLobbyAuthenReq		//LOBBY_AUTHEN_REQ_MSG  0x01  玩家请求大厅认证

{
	MsgHeadDef	msgHeadInfo;
	int  iGameID;
	char szUserName[NAME_LEN];
	char szPassword[PASSWD_LEN];
}LobbyAuthenReq;

#pragma pack(pop)

typedef enum LOBBY_AUTHEN_ERROR
{
	ERROR_AUTHEN_SERVER_DISCONNECT = -2,  //无法连接 // 服务器连接失败。
	ERROR_AUTHEN_EXECUTE_PROCEDURE_FAILED,// 存储失败
	ERROR_AUTHEN_DBRETURN_USER_PASS_ERROR = 1,//账号密码错误
	ERROR_AUTHEN_DBRETURN_OPERATE_ERROR = 2     //insert if not exist error 数据库操作失败  
}LOBBY_AUTHEN_ERROR;


//登录服务器返回给大厅客户端的验证错误码; 登录服务器 -> 大厅客户端
typedef struct tagLobbyAuthenRes_2
{
	MsgHeadDef	msgHeadInfo;
	LOBBY_AUTHEN_ERROR lError;  //错误码
}LobbyAuthenRes_2, *PLobbyAuthenRes_2;

 

typedef enum LOBBY_HECHENG_ERROR
{   

}LOBBY_HECHENG_ERROR_DEF;


//登录服务器返回给大厅客户端的验证错误码; 登录服务器 -> 大厅客户端
typedef struct tagLobbyHeChengRes_2
{
	MsgHeadDef	msgHeadInfo;
	LOBBY_HECHENG_ERROR lError; //错误码
}LobbyHeCheng_2, *PLobbyHeCheng_2;

//服务器列表信息结构体(要跟大厅客户端的GameServerInfo保持一致)
typedef struct tagServerInfo
{
	unsigned short  usGameID;           //游戏ID
	unsigned short  usServerID;         //房间ID
	char            szServerName[32];   //房间名称
	unsigned long   ulLimitPoint;       //房间限制
	unsigned long   ulBaseTime;         //房间底注
	unsigned short  usOnlineCount;      //在线人数
	unsigned short  usServerPort;       //房间PORT
	unsigned long   ulServerIP;         //房间IP
	unsigned char   ucServerType;       //房间类型(比赛、积分、游戏币）
	unsigned char   ucOpenFlag;         //此房间是否开启, 0: 关闭;
}ServerInfo;

typedef struct tagGameServer
{
	MsgHeadDef	msgHeadInfo;
	unsigned short  usListCount;            //服务器列表数量
	ServerInfo      serverInfo[SERVER_SIZE];//服务器列表信息
}GameServer, *PGameServer;
   
#endif 
