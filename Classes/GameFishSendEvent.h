#ifndef _GAMEFISHSENDEVENT_H_
#define _GAMEFISHSENDEVENT_H_

#include "GameScene.h"
#include "cocos-ext.h"
USING_NS_CC_EXT;

class GameFishSendEvent : public GameLayer
{
public:
	virtual bool init();  
	static GameFishSendEvent *create(GameScene *pGameScene);
	virtual ~GameFishSendEvent();
	GameFishSendEvent(GameScene *pGameScene);
	GameFishSendEvent(const GameFishSendEvent&);
	GameFishSendEvent& operator = (const GameFishSendEvent&);

private:
	virtual void onEnter();
	virtual void onExit();
	virtual bool onTouchBegan(Touch *pTouch, Event *pEvent){return true;}
	virtual void onTouchMoved(Touch *pTouch, Event *pEvent){}
	virtual void onTouchEnded(Touch *pTouch, Event *pEvent){}
	virtual void onTouchCancelled(Touch *pTouch, Event *pEvent){}
	void callbackBt(Ref *pSender );

	Menu* CreateButton(std::string szBtName ,const Vec2 &p , int tag );

public:
	void OnRemove();
	void actionShow();
	void actionMin();
};
#endif
