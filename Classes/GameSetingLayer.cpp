#include "GameSetingLayer.h"
#include "LobbySocketSink.h"
#include "ClientSocketSink.h"
#include "LocalDataUtil.h"
#include "GameLayerMove.h"

#define	Btn_Close			1
#define Back_Music			2
#define Back_Effect			3
#define Back_Shake			4


GameSetingLayer::GameSetingLayer(GameScene *pGameScene):GameLayer(pGameScene)
{
}

GameSetingLayer::~GameSetingLayer()
{

}


Menu* GameSetingLayer::CreateButton(std::string szBtName ,const Vec2 &p , int tag )
{
	Menu *pBT = Tools::Button(StrToChar(szBtName+"_normal.png") , StrToChar(szBtName+"_click.png") , p, this , menu_selector(GameSetingLayer::callbackBt) , tag);
	return pBT;
}

bool GameSetingLayer::init()
{
	if ( !Layer::init() )
	{
		return false;
	}
	GameLayer::onEnter();
	setLocalZOrder(100);
	Tools::addSpriteFrame("SetLayer.plist");


	Sprite *LockBack = Sprite::createWithSpriteFrameName("bg_tongyongkuang3.png");
	LockBack->setPosition(_STANDARD_SCREEN_CENTER_);
	addChild(LockBack);
    
    Sprite *title = Sprite::createWithSpriteFrameName("set_title.png");
    title->setPosition(Vec2(519,480));
    LockBack->addChild(title);

    Menu* btn = CreateButton("SetingClose", Vec2(990,490),Btn_Close);
	LockBack->addChild(btn);

    Sprite *pTitleLable = Sprite::createWithSpriteFrameName("beijing.png");
    pTitleLable->setPosition(Vec2(130, 280));
	LockBack->addChild(pTitleLable);

	Sprite *pStart = Sprite::createWithSpriteFrameName("sound_start.png");
	pStart->setPosition(Vec2(250,280));
	LockBack->addChild(pStart);
	// Add the slider
	ControlSlider *slider = ControlSlider::create(
		Sprite::createWithSpriteFrameName("set_sound_empty.png") ,
		Sprite::createWithSpriteFrameName("set_sound_full1.png") ,
		Sprite::createWithSpriteFrameName("set_sound_point.png")
		);
	slider->setAnchorPoint(Vec2(0.5f, 0.5f));
	slider->setMinimumValue(0.0f);
	slider->setMaximumValue(1.0f);
	slider->setPosition(Vec2(600,280));
	slider->setTag(Back_Music);
	slider->setValue(g_GlobalUnits.m_fBackMusicValue);
    slider->addTargetWithActionForControlEvents(this, cccontrol_selector(GameSetingLayer::valueChanged), Control::EventType::VALUE_CHANGED);
	LockBack->addChild(slider);

	Sprite *pEnd = Sprite::createWithSpriteFrameName("sound_end.png");
	pEnd->setPosition(Vec2(950,280));
	LockBack->addChild(pEnd);


    Sprite* pTitle1 = Sprite::createWithSpriteFrameName("shengyin.png");
    pTitle1->setPosition(Vec2(130, 150));
	LockBack->addChild(pTitle1);
	pStart = Sprite::createWithSpriteFrameName("sound_start.png");
	pStart->setPosition(Vec2(250,150));
	LockBack->addChild(pStart);
	// Add the slider
	slider = ControlSlider::create(
		Sprite::createWithSpriteFrameName("set_sound_empty.png") ,
		Sprite::createWithSpriteFrameName("set_sound_full.png") ,
		Sprite::createWithSpriteFrameName("set_sound_point.png")
		);
	slider->setAnchorPoint(Vec2(0.5f, 0.5f));
	slider->setMinimumValue(0.0f);
	slider->setMaximumValue(1.0f);
	slider->setPosition(Vec2(600,150));
	slider->setTag(Back_Effect);
	slider->setValue(g_GlobalUnits.m_fSoundValue);
    slider->addTargetWithActionForControlEvents(this, cccontrol_selector(GameSetingLayer::valueChanged), Control::EventType::VALUE_CHANGED);
	LockBack->addChild(slider);

	pEnd = Sprite::createWithSpriteFrameName("sound_end.png");
	pEnd->setPosition(Vec2(950,150));
	LockBack->addChild(pEnd);
	
	actionShow();
	setTouchEnabled(true);
    auto listener = EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(true);
    listener->onTouchBegan = CC_CALLBACK_2(GameSetingLayer::onTouchBegan, this);
    listener->onTouchMoved = CC_CALLBACK_2(GameSetingLayer::onTouchMoved, this);
    listener->onTouchEnded = CC_CALLBACK_2(GameSetingLayer::onTouchEnded, this);
    Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(listener, this);
	return true;
}


void GameSetingLayer::valueChanged(Ref *sender, Control::EventType event)
{
	Node *pNode = (Node *)sender;
	switch (pNode->getTag())
	{
	case Back_Music:
		{
			ControlSlider *slider = (ControlSlider *)pNode;
			float v = slider->getValue();
			g_GlobalUnits.m_fBackMusicValue = v;
			LocalDataUtil::SaveBackMusicValue(v);
			SoundUtil::sharedEngine()->setBackSoundVolume(v);
			break;
		}
	case Back_Effect:
		{
			ControlSlider *slider = (ControlSlider *)pNode;
			float v = slider->getValue();
			g_GlobalUnits.m_fSoundValue = v;
			LocalDataUtil::SaveSoundValue(v);
			SoundUtil::sharedEngine()->setSoundVolume(v);
			break;
		}
	}
}
void GameSetingLayer::callbackBt( Ref *pSender )
{
	Node *pNode = (Node *)pSender;
	switch (pNode->getTag())
	{
	case Btn_Close:
		{
			SoundUtil::sharedEngine()->playEffect("buttonMusic");
			actionMin();
			break;
		}
	}
}

void GameSetingLayer::OnRemove()
{
	GameLayerMove::sharedGameLayerMoveSink()->ClearSeting();
	m_pGameScene->removeChild(this);
}

GameSetingLayer* GameSetingLayer::create(GameScene *pGameScene)
{
	GameSetingLayer* temp = new GameSetingLayer(pGameScene);
	if(temp && temp->init())
	{
		temp->autorelease();
		return temp;
	}
	else
	{
		CC_SAFE_DELETE(temp);
		return NULL;
	}
}

void GameSetingLayer::onEnter()
{
	GameLayer::onEnter();
}

void GameSetingLayer::onExit()
{
	GameLayer::onExit();
	Tools::removeSpriteFrameCache("SetLayer.plist");
}


Label * GameSetingLayer::createLabel(const char *szText ,const Vec2 &p, int fontSize ,Color3B color, bool bChinese)
{
	std::string szTemp = szText;
	return createLabel(szTemp , p , fontSize , color , bChinese);
}
Label * GameSetingLayer::createLabel(const std::string szText ,const Vec2 &p, int fontSize ,Color3B color, bool bChinese)
{

	std::string szTempTitle = szText;
	if (bChinese && szTempTitle.length() != 0)
	{
#ifdef _WIN32
		Tools::GBKToUTF8(szTempTitle , "gb2312" , "utf-8");
#endif // _WIN32
	}

	Label *pLable = Label::createWithSystemFont(szTempTitle.c_str(), _GAME_FONT_NAME_1_, fontSize);
    pLable->setHorizontalAlignment(TextHAlignment::LEFT);
	pLable->setColor(color);
	pLable->setAnchorPoint(Vec2(0,0));
	pLable->setPosition(ccppx(p));
	return pLable;
}

void GameSetingLayer::actionShow()
{
	setScale(0.1f);
	this->runAction(Sequence::create(ScaleTo::create(0.2f , 1.f),NULL));
}


void GameSetingLayer::actionMin()
{
   this->runAction(Sequence::create(ScaleTo::create(0.2f , 0.1f),CallFunc::create(CC_CALLBACK_0(GameSetingLayer::OnRemove, this)),NULL));
}
