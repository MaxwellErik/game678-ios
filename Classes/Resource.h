﻿#ifndef __RESOURCE_H__
#define __RESOURCE_H__

#include "GameConstant.h"

#define _GAME_VIEW_ANIM_DIR_							""
#define _GAME_VIEW_FLAT_DIR_							""

//得分数字
#define		WIN_SCORE_POINT							(CCPointMake(814 , 254+100))
//Lose Win
#define		WIN_TEXT_POINT							(CCPointMake(265 , 249+100))



#define ITEM_SPRITE_FRAME_PLIST(i)			(std::string(std::string(_GAME_VIEW_ANIM_DIR_)+ ITEM_RES_NAMES[i]+ "_frame.plist").c_str())	
#define ITEM_SPRITE_SHAKE_PLIST(i)			(std::string(std::string(_GAME_VIEW_ANIM_DIR_)+ ITEM_RES_NAMES[i]+ "_shake.plist").c_str())	
#define ITEM_SPRITE_FLASH_PLIST(i)			(std::string(std::string(_GAME_VIEW_ANIM_DIR_)+ ITEM_RES_NAMES[i]+ "_flash.plist").c_str())	
#define ITEM_SPRITE_ANIM_PLIST(i)			(std::string(std::string(_GAME_VIEW_ANIM_DIR_)+ ITEM_RES_NAMES[i]+ "_anim.plist").c_str())
#define ITEM_SPRITE_ANIMNAME(i,s)				(std::string(ITEM_RES_NAMES[i]+"_"+ITEM_RES_STATE_NAMES[s]).c_str())	
#define ITEM_SPRITE_STATENAME(i,s)			(std::string(ITEM_RES_NAMES[i]+"_"+ITEM_RES_STATE_NAMES[s]+".png").c_str())	


const std::string _GAME_16BIT_TEXTURE_NAME_[] =
{
	"GameBack.plist","ui_view.plist"
};
const int _GAME_16BIT_TEXTURE_LENGTH_ = sizeof(_GAME_16BIT_TEXTURE_NAME_)/sizeof(_GAME_16BIT_TEXTURE_NAME_[0]);


const std::string UI_MENU_BT_NAME[8]=
{
	"bt_menu","bt_help","bt_pay","bt_rank","bt_set","bt_shop","bt_task","bt_person_info"
};

const std::string UI_FUN_CHOOSE_BT_NAME[2]=
{
	"bt_fun_bet","bt_fun"
};

const std::string UI_COMPARE_BT_NAME[3]=
{
	"bt_score","bt_compare","bt_continue"
};

const std::string UI_BET_BT_NAME[6]=
{
	"bt_bet","bt_select_line","bt_max_start","bt_start","bt_bet_view","bt_select_line_view"
};

const std::string UI_TASK_BT_NAME[2]=
{
	"bt_task_close","bt_task_receive"
};

const std::string UI_PROPS_BT_NAME[2]=
{
	"bt_send_record","bt_send"
};

const std::string UI_LOGINNET_BT_NAME[6]=
{
	"bt_single_pay" , "bt_login_net" , "bt_gopay", "bt_SMPay", "bt_other_pay", "bt_free_gold"
};

const std::string UI_FREE_GOLD_NAME[3]=
{
	"bt_daily_task" , "bt_earn_gold","bt_InviteFriends"
};


const std::string UI_BUY_BT_NAME[30]=
{
	"bt_task_close", "bt_buy_record", "bt_gold",
	"bt_coins", "bt_buy", "bt_buy", 
	"bt_buy", "bt_buy", "bt_buy", 
	"bt_restore","bt_Invite", "bt_300_coins",
	"pay_gold_0", "pay_gold_1", "pay_gold_2", 
	"pay_gold_3", "pay_gold_4", "pay_gold_5", 
	"pay_gold_6", "pay_gold_7", "pay_gold_8", 
	"pay_coins_0", "pay_coins_1", "pay_coins_2", 
	"pay_coins_3", "pay_coins_4", "pay_coins_5", 
	"pay_coins_6", "pay_coins_7", "pay_coins_8"
};


const std::string UI_BUYRECORD_BT_NAME[3]=
{
	"bt_back" , "bt_buy_record","bt_buy_again"
};


const std::string UI_SEND_BT_NAME[2]=
{
	"bt_confirm" , "bt_cancel1"
};

const std::string PERSON_UI_BT_NAME[2]=
{
	"bt_person_buy_record","bt_person_send_record"
};

const std::string WINLOGIN_UI_BT_NAME[2]=
{
	"bt_win_login", "bt_continue_single"
};

const std::string CONTACT_UI_BT_NAME[9]=
{
	"bt_uc_invited" , "bt_uc_ainvited" , "bt_uc_uinvited" , "bt_uc_invited_done" , "bt_uc_back" , "bt_uc_confirm" ,"bt_uc_cancel" , "bt_uc_unchecked" , "bt_uc_checked"
};


#ifdef _WIN32

#define		_UNICODE_DEFAULT_FONT_NAME													_GAME_FONT_NAME_1_
#define		_UNICODE_CONNECT_FAILT														"网络连接失败"
#define		_UNICODE_CONNECTING															"网络不稳定，正在重新连接中..."
#define		_UNICODE_CONNECT_AGAIN														"网络连接失败，请点击重试！"
#define		_NUICODE_COW1																"青铜牛"
#define		_NUICODE_COW2																"黄铜牛"
#define		_NUICODE_COW3																"白银牛"
#define		_NUICODE_COW4																"黄金牛"
#define		_NUICODE_COW5																"白金牛"
#define		_NUICODE_COW6																"钻石牛"
#define		_NUICODE_COW7																"水晶牛"
#define		_NUICODE_COW8																"神级牛"
#define     _POPMSG																		"欢迎来到二人牛牛，激情无限！没钱请充值，点击右下角充值图标即可。。。。。。"

const std::string _UNICODE_WIN_TOP_TEXT[3]=
{
	"游客%ld在网络版中押中五连线，获得价值￥%ld的黄金。",
	"游客%ld在网络版中押中小玛丽，获得价值￥%ld的黄金。",
	"游客%ld在网络版中运气爆发，获得%s全盘奖，获得价值￥%ld的黄金。"
};

const std::string _UNICODE_ALL_PRIZE_TEXT[10]=
{
	"武器","人物","斧头","银枪","大刀",
	"林冲","鲁智深","宋江","替天行道","水浒传"
};

#else

#define		_UNICODE_DEFAULT_FONT_NAME													"\u5fae\u8f6f\u96c5\u9ed1"
#define		_UNICODE_CONNECT_FAILT														"\u7f51\u7edc\u8fde\u63a5\u5931\u8d25"
#define		_UNICODE_CONNECTING															"\u7f51\u7edc\u4e0d\u7a33\u5b9a\uff0c\u6b63\u5728\u91cd\u65b0\u8fde\u63a5\u4e2d..."
#define		_UNICODE_CONNECT_AGAIN														"\u7f51\u7edc\u8fde\u63a5\u5931\u8d25\uff0c\u8bf7\u70b9\u51fb\u91cd\u8bd5\uff01"

#define		_NUICODE_COW1																"\u9752\u94dc\u725b"
#define		_NUICODE_COW2																"\u9ec4\u94dc\u725b"
#define		_NUICODE_COW3																"\u767d\u94f6\u725b"
#define		_NUICODE_COW4																"\u9ec4\u91d1\u725b"
#define		_NUICODE_COW5																"\u767d\u91d1\u725b"
#define		_NUICODE_COW6																"\u94bb\u77f3\u725b"
#define		_NUICODE_COW7																"\u6c34\u6676\u725b"
#define		_NUICODE_COW8																"\u795e\u7ea7\u725b"
#define     _POPMSG																		"\u6b22\u8fce\u6765\u5230\u4e8c\u4eba\u725b\u725b\uff0c\u6fc0\u60c5\u65e0\u9650\uff01\u6ca1\u94b1\u8bf7\u5145\u503c\uff0c\u70b9\u51fb\u53f3\u4e0b\u89d2\u5145\u503c\u56fe\u6807\u5373\u53ef\u3002\u3002\u3002\u3002\u3002\u3002"

const std::string _UNICODE_WIN_TOP_TEXT[3]=
{
	"\u6e38\u5ba2%ld\u5728\u7f51\u7edc\u7248\u4e2d\u62bc\u4e2d\u56db\u8fde\u7ebf\uff0c\u83b7\u5f97\u4ef7\u503c\uffe5%ld\u7684\u9ec4\u91d1\u3002",
	"\u6e38\u5ba2%ld\u5728\u7f51\u7edc\u7248\u4e2d\u62bc\u4e2d\u5c0f\u739b\u4e3d\uff0c\u83b7\u5f97\u4ef7\u503c\uffe5%ld\u7684\u9ec4\u91d1\u3002",
	"\u6e38\u5ba2%ld\u5728\u7f51\u7edc\u7248\u4e2d\u8fd0\u6c14\u7206\u53d1\uff0c\u83b7\u5f97%s\u5168\u76d8\u5956\uff0c\u83b7\u5f97\u4ef7\u503c\uffe5%ld\u7684\u9ec4\u91d1\u3002"
};

const std::string _UNICODE_ALL_PRIZE_TEXT[10]=
{
	"\u6b66\u5668","\u4eba\u7269","\u65a7\u5934","\u94f6\u67aa","\u5927\u5200",
	"\u6797\u51b2","\u9c81\u667a\u6df1","\u5b8b\u6c5f","\u66ff\u5929\u884c\u9053","\u6c34\u6d52\u4f20"
};

#endif // _WIN32

#define		UI_BG_COLOR															(Color4B(186,186,186,255))
#define		WHITE_COLOR															(Color3B(255,255,255))
#define		BLACK_COLOR															(Color3B(0,0,0))
#define		RED_COLOR															(Color3B(255,0,0))
#define		PRICE_COLOR															(Color3B(196,255,7))
#define		TITLE_COLOR															(Color3B(95,5,16))
#define		TEXT_COLOR															(Color3B(103,48,0))
#define		LINEBG_COLOR														(Color4B(227,227,227,255))

#define SETUP_COUNT																4		//设置的个数
#define RANK_COUNT																20
#define SENDRECORD_COUNT														10




//充值比例（游戏币,人民币）
#define	GOLD_SCALE		100000			//金币比例
#define	COINS_SCALE		10000			//铜钱比例

//支付
const LONG BUY_GOLD_SETUP[5][2]=
{
    {205000,    10},
    {410000,    20},
    {1025000,    50},
    {2050000,   100},
    {10250000,   500}
};

const LONG BUY_GOLD_IOS[5][3]=
{
    {60000,6, 118},
    {120000,12, 123},
    {180000,18, 128},
    {250000,25, 138},
    {300000,30, 148}
};

#ifdef _DEBUG
#define WEB_SERVER_URL										"http://116.236.169.10:8080/appstore/"
#else
#define WEB_SERVER_URL										"http://tbpay.12wanwan.com/"
#endif // _DEBUG

#endif    // __MAIN_H__
