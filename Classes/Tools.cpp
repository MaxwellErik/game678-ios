﻿#include "Tools.h"
#include <vector>
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
#include "iconv.h"
#endif

Menu*Tools::Button(const char *NormalBitmpaName, const char *SelectBitmpaName, Vec2 Position, Node *Node, SEL_MenuHandler sel, int iTag)
{
	Sprite *tempButtonNormal = Sprite::createWithSpriteFrameName(NormalBitmpaName);
    Sprite *tempButtonSelect = Sprite::createWithSpriteFrameName(NormalBitmpaName);
	tempButtonSelect->setColor(Color3B(150,150,150));
	tempButtonSelect->setScale(0.96f);
	Size size = tempButtonSelect->getContentSize();
	tempButtonSelect->setPosition(ccpx(size.width*0.02, size.height*0.02));
    
    MenuItemSprite *tempButton = MenuItemSprite::create(tempButtonNormal, tempButtonSelect, nullptr, Node, sel);
	Menu *tempMenu = Menu::create(tempButton, NULL);
	tempMenu->setPosition(CC_POINT_PIXELS_TO_POINTS(Position));

    if(iTag != -1)
    {
        tempButton->setTag(iTag);
    }
    
    tempButton->setUserData(tempMenu);

    return tempMenu;
}

MenuItemSprite*Tools::Button(const char *NormalBitmpaName, const char *SelectBitmpaName, const char *DisableBitmpaName,Node *Node, SEL_MenuHandler sel, int iTag)
{
    Sprite *tempButtonNormal = Sprite::createWithSpriteFrameName(NormalBitmpaName);
    Sprite *tempButtonSelect = Sprite::createWithSpriteFrameName(SelectBitmpaName);
    Sprite *tempButtonDisable = Sprite::createWithSpriteFrameName(DisableBitmpaName);
    
    MenuItemSprite *ptempButton = MenuItemSprite::create(tempButtonNormal, tempButtonSelect, tempButtonDisable, Node, sel);
    if(iTag != -1)
    {
        ptempButton->setTag(iTag);
    }
    
    ptempButton->setUserData(ptempButton);
    
    return ptempButton;
}

Menu*Tools::Toggle(char *NormalBitmpaName, char *SelectBitmpaName, Vec2 Position, Node *Node, SEL_MenuHandler sel,  bool isSelected, int iTag)
{
    Sprite *tempButtonNormal1 = Sprite::createWithSpriteFrameName(NormalBitmpaName);
    Sprite *tempButtonNormal2 = Sprite::createWithSpriteFrameName(NormalBitmpaName);
    
    Sprite *tempButtonSelect1 = Sprite::createWithSpriteFrameName(SelectBitmpaName);
    Sprite *tempButtonSelect2 = Sprite::createWithSpriteFrameName(SelectBitmpaName);
    
    
    MenuItemSprite *tempMenuItem1 = MenuItemSprite::create(tempButtonNormal1, tempButtonNormal2);
    MenuItemSprite *tempMenuItem2 = MenuItemSprite::create(tempButtonSelect1, tempButtonSelect2);
    
    MenuItemToggle *tempToggle = MenuItemToggle::createWithTarget(Node, sel, tempMenuItem1, tempMenuItem2, NULL);
    if(isSelected)
	{
        tempToggle->setSelectedIndex(1);
    }
	else
	{
        tempToggle->setSelectedIndex(0);
    }

    Menu *tempMenu = Menu::create(tempToggle);
    tempMenu->setPosition(CC_POINT_PIXELS_TO_POINTS(Position));
    if(iTag != -1)
    {
        tempMenu->setTag(iTag);
    }
    tempToggle->setUserData(tempMenu);
    return tempMenu;
}

MenuItemToggle*Tools::ItemToggle(char *NormalBitmpaName, char *SelectBitmpaName, char *PressBitmapName1, char *PressBitmapName2, Node *Node, SEL_MenuHandler sel)
{
    Sprite *tempButtonNormal1 = Sprite::createWithSpriteFrameName(NormalBitmpaName);
    Sprite *tempButtonNormal2;
    if(PressBitmapName1)
    {
        tempButtonNormal2 = Sprite::createWithSpriteFrameName(PressBitmapName1);
    }else
    {
        tempButtonNormal2 = Sprite::createWithSpriteFrameName(NormalBitmpaName);
    }
    
    Sprite *tempButtonSelect1 = Sprite::createWithSpriteFrameName(SelectBitmpaName);
    Sprite *tempButtonSelect2;
    if(PressBitmapName2)
    {
        tempButtonSelect2 = Sprite::createWithSpriteFrameName(PressBitmapName2);
    }
	else
    {
        tempButtonSelect2 = Sprite::createWithSpriteFrameName(SelectBitmpaName);
    }
    
    
    MenuItemSprite *tempMenuItem1 = MenuItemSprite::create(tempButtonNormal1, tempButtonNormal2);
    MenuItemSprite *tempMenuItem2 = MenuItemSprite::create(tempButtonSelect1, tempButtonSelect2);
    
    MenuItemToggle *itemToggle = MenuItemToggle::createWithTarget(Node, sel, tempMenuItem1, tempMenuItem2, NULL);

    return itemToggle;
}

Menu*Tools::ToggleMenu(MenuItemToggle *ItemToggle, Vec2 Position, bool isSelected, int iTag)
{
    if(isSelected)
	{
        ItemToggle->setSelectedIndex(1);
    }
	else
	{
        ItemToggle->setSelectedIndex(0);
    }

	Menu *tempMenu = Menu::createWithItem(ItemToggle);
    tempMenu->setPosition(CC_POINT_PIXELS_TO_POINTS(Position));
    if(iTag != -1) 
    {
        tempMenu->setTag(iTag);
    }
    ItemToggle->setUserData(tempMenu);

    return tempMenu;
}

Menu*Tools::ToggleMenu(char *NormalBitmpaName1, char *NormalBitmpaName2, char *SelectBitmpaName1, char *SelectBitmpaName2, Vec2 Position, Node *Node, SEL_MenuHandler sel, int iTag)
{
    Sprite *tempButtonNormal1 = Sprite::createWithSpriteFrameName(NormalBitmpaName1);
    Sprite *tempButtonNormal2 = Sprite::createWithSpriteFrameName(NormalBitmpaName2);
    
    Sprite *tempButtonSelect1 = Sprite::createWithSpriteFrameName(SelectBitmpaName1);
    Sprite *tempButtonSelect2 = Sprite::createWithSpriteFrameName(SelectBitmpaName2);
    
    MenuItemSprite *tempMenuItem1 = MenuItemSprite::create(tempButtonNormal1, tempButtonNormal2);
    MenuItemSprite *tempMenuItem2 = MenuItemSprite::create(tempButtonSelect1, tempButtonSelect2);
    
    MenuItemToggle *tempToggle = MenuItemToggle::createWithTarget(Node, sel, tempMenuItem1, tempMenuItem2, NULL);
    Menu *tempMenu = Menu::create(tempToggle, nullptr);
    tempToggle->setUserData(tempMenu);
    tempMenu->setPosition(CC_POINT_PIXELS_TO_POINTS(Position));
    tempMenu->setTag(iTag);

    return tempMenu;
}

TextFieldTTF *Tools::AddTextField(Vec2 Position, const char *pText, const cocos2d::Size &dimensions, TextHAlignment alignment, const char *fontName, float fontSize)
{  
	// 取得 实际大小
    Size dimSize = CC_SIZE_PIXELS_TO_POINTS(dimensions);

	// 优化高度
	dimSize.height = fontSize + 6;
    
    TextFieldTTF *pTextField = TextFieldTTF::textFieldWithPlaceHolder(pText, dimSize, alignment, fontName, fontSize);
    
	// 设定 实际坐标
    pTextField->setPosition(CC_POINT_PIXELS_TO_POINTS(Position));
    pTextField->setColor(Color3B::BLACK);
    
    return pTextField;
}

Label*Tools::SetLabelShowRect(const char *pLabel, const Size &dimensions, TextHAlignment alignment, const char *fontName, float fontSize, Rect rect, Vec2 Position)
{
	Label*ptempLabel = Label::create(pLabel, fontName, fontSize, dimensions, alignment);
//	ptempLabel->setTextureRect(CC_RECT_PIXELS_TO_POINTS(rect));
    ptempLabel->setPosition(CC_POINT_PIXELS_TO_POINTS(Position));

    return ptempLabel;
}

void Tools::GetCursorPosition(Vec2 &CursorPosition, TextFieldTTF *pTextField, Sprite *pSpriteCursor)
{
	float tempStandWidth = pTextField->getContentSize().width;
    float tempStandHeight = pTextField->getContentSize().height;

    Vec2 tempPosition = pTextField->getPosition();

    float distance = tempPosition.x - tempStandWidth / 2;
    tempPosition.x -= tempStandWidth / 2;

	tempPosition.y += (tempStandHeight - pSpriteCursor->getContentSize().height) /2;

	pSpriteCursor->setScaleY(pTextField->getSystemFontSize() / pSpriteCursor->getContentSize().height);
    
    std::string tempName = pTextField->getString();

    Label *tempLabel = Label::createWithSystemFont(tempName.c_str(), pTextField->getSystemFontName(), pTextField->getSystemFontSize());
	if(tempLabel->getContentSize().width <= tempStandWidth)
    {
        tempPosition.x += tempLabel->getContentSize().width + pTextField->getSystemFontSize() / 10.0f;
    }
	// 多行处理
	// TODO:
	else
    {
		tempPosition.x += tempLabel->getContentSize().width + pTextField->getSystemFontSize() / 10.0f;
    }
    
    CursorPosition = tempPosition;
}

void Tools::UTF_8_Delete_Back(std::string *pUTF8String)
{
	unsigned long tempStrLength = pUTF8String->length();
    if(tempStrLength == 0)
    {
        // there is no string
        return;
    }

    // get the delete byte number
	// default, erase 1 byte
    int tempDeleteLength = 1;    

    while((pUTF8String->at(tempStrLength - tempDeleteLength) & 0xC0) == 0x80)
    {
        tempDeleteLength++;
	}

    // set new input text
    std::string tempText(pUTF8String->c_str(), tempStrLength - tempDeleteLength);
    *pUTF8String = tempText;
}

int Tools::UTF_8_CharLength(std::string *pUTF8String)
{
    int tempLength = 0;
    char tempChar = 0;

	char * ptempString = (char *)pUTF8String->c_str();

    while ((tempChar = *ptempString))
    {
        CC_BREAK_IF(! tempChar);
        
        if((tempChar & 0xC0) != 0x80)
        {
            tempLength++;
        }

        ptempString++;
    }
    return tempLength;
}

void Tools::UTF_8_SetPassword(std::string *pUTF8String, std::string *pPassword)
{
	int tempLength = 0;
    char tempChar = 0;

	char *ptempCharBuffer = (char *)pUTF8String->c_str();

    while ((tempChar = *ptempCharBuffer))
    {
        CC_BREAK_IF(! tempChar);
        
        if((tempChar & 0xC0) != 0x80)
        {
            tempLength++;
        }

        ptempCharBuffer++;
    }

	pPassword->clear();

	for(int i = 0; i < tempLength; i++)
	{
		*pPassword += '*';
	}
}

void Tools::GetRect(Node *pNode, Rect & SrcRect)
{
    SrcRect.origin = pNode->getPosition(); 
    SrcRect.size = pNode->getContentSize();
    SrcRect.origin.x -= SrcRect.size.width/2;
    SrcRect.origin.y -= SrcRect.size.height/2;

    // SrcRect.size.height += 40;
    // SrcRect.origin.y -= 20;
}

void Tools::LoadDataByKey(const char * const Key, std::string &Value)
{
	string tempValue = UserDefault::getInstance()->getStringForKey(Key, std::string(""));
	ParseData(tempValue);

	Value = tempValue;
}

void Tools::SaveDataByKey(const char * const Key, std::string &Value)
{
	// 加密
	std::string tempValue = Value;
	SaveData(tempValue);

	UserDefault::getInstance()->setStringForKey(Key, tempValue);
	UserDefault::getInstance()->flush();
}

bool Tools::SaveFile(const char *path, void *buffer, int len) //path 文件路径 T要存储的数据对象
{
    remove(path);
    FILE*file = fopen(path, "wb+");
    if(file != NULL)
    {
        fwrite(buffer, 1, len, file);
        fclose(file);
        return true;
    }
    return false;
}

bool Tools::LoadFile(const char *path, void *buffer, int len)//path 文件路径 T要从文件读取的数据对象 
{
    FILE*file = fopen(path, "rb+");
    if(file != NULL)
    {
        fread(buffer, 1, len, file);
        fclose(file);
        return true;
    }
    return false;
}


int Tools::StringTransform( std::string &gbkStr )
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
	return GBKToUTF8( gbkStr, "gb2312", "utf-8" );
#else 
	return 0;
#endif

}


#if (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
//字符转换，使cocos2d-x在win32平台支持中文显示
int Tools::GBKToUTF8( std::string &gbkStr,const char* toCode,const char* formCode )
{
	iconv_t iconvH;
	iconvH = iconv_open(formCode,toCode);
	if(iconvH == 0)
	{
		return -1;
	}

	const char* strChar = gbkStr.c_str();
	const char** pin = &strChar;

	size_t strLength = gbkStr.length();
	char* outbuf = (char*)malloc(strLength*4);
	char* pBuff = outbuf;
	memset(outbuf,0,strLength*4);
	size_t outLength = strLength*4;
	if(-1 == iconv(iconvH,pin,&strLength,&outbuf,&outLength))
	{
		iconv_close(iconvH);
		return -1;
	}

	gbkStr = pBuff;
	iconv_close(iconvH);
	return 0;
}

#endif

void Tools::AddComma( LONGLONG lNum ,char *szOut)
{
#ifdef _WIN32
	sprintf(szOut , "%I64d" , lNum);
#else
	sprintf(szOut , "%lld" , lNum);
#endif // _DEBUG
	std::string szTemp(szOut);
	size_t len = szTemp.length();
	for(int index =(int) len-3; index > 0; index -= 3)
	{
		szTemp.insert(index, ",");
	}
	strcpy(szOut , szTemp.c_str()) ;
}

void Tools::GetSystemTime(char *szTime)
{
    struct timeval now;  // 秒，毫秒
	gettimeofday(&now, NULL);
	struct tm * ptm;
	time_t t;
#if(CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
	t = time(NULL);
#else
	t = now.tv_sec;
#endif

	ptm = localtime(&t);
	int year = ptm->tm_year + 1900;
	int month = ptm->tm_mon + 1;
	int day = ptm->tm_mday;
	int hour = ptm->tm_hour;
	int min = ptm->tm_min;
	sprintf(szTime , "%d-%d-%d %02d:%02d" , year, month , day, hour, min);
}

void Tools::removeSpriteFrameCache( const char *szName )
{
	char szTemp[128] = {0};
	strcpy(szTemp , szName);
	char *p=NULL;
	p = strtok(szTemp,".");
	char szOut[128] = {0};
	sprintf(szOut , "%s.plist" , p);
	SpriteFrameCache::getInstance()->removeSpriteFramesFromFile(szOut);
	sprintf(szOut , "%s.png" , p);
	Director::getInstance()->getTextureCache()->removeTextureForKey(szOut);
	sprintf(szOut , "%s.png.uzqp" , p);
	Director::getInstance()->getTextureCache()->removeTextureForKey(szOut);
}

void Tools::addSpriteFrame( const char *szName , Texture2D::PixelFormat format)
{
	Texture2D::setDefaultAlphaPixelFormat(format);
	SpriteFrameCache::getInstance()->addSpriteFramesWithFile(szName);
    Texture2D::setDefaultAlphaPixelFormat(Texture2D::PixelFormat::RGBA8888);
}

void Tools::NumberToChar( char *szOut , LONGLONG lNumber )
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
	sprintf(szOut , "%I64d", lNumber);
#else
	sprintf(szOut , "%lld", lNumber);
#endif
}

void Tools::GetValueStr( char* value, char * name, char *filename )
{
	ssize_t uSize = 0;

	unsigned char *szTempOut = FileUtils::getInstance()->getFileDataFromZip(filename , "rt" , &uSize);
	if (szTempOut == NULL)
	{
		return;
	}
	char szLine[1280]={0};
	int start=0;
	int end=0;
	char *tokenPtr=NULL;
	for (int i=0 ; i<uSize ; i++)
	{
		if (szTempOut[i] == '\n')
		{
			end=i;
			memset(szLine , 0 , sizeof(szLine));
			memcpy(szLine , szTempOut+start , end-start);
			start=end+1;
			tokenPtr=strtok(szLine,"=");
			if (strcmp(tokenPtr, name) == 0)
			{
				tokenPtr = strtok(NULL,"=");
				strcpy(value , tokenPtr);
				break;
			}
			else continue;
		}		
	}	
}

int Tools::GetValueInt( char * name, char *filename )
{
	char value[32]={0};
	GetValueStr(value,name, filename );
	return atoi(value);
}

std::string Tools::urlencode( const char * str )
{
	std::string strDes;
	size_t len = strlen(str);
	for (size_t i=0;i<len;i++)
	{
		if(isalnum((BYTE)str[i]))
		{
			char tempbuff[2];
			sprintf(tempbuff,"%c",str[i]);
			strDes.append(tempbuff);
		}
		else if (isspace((BYTE)str[i]))
		{
			strDes.append("+");
		}
		else
		{
			char tempbuff[4];
			sprintf(tempbuff,"%%%X%X",((BYTE*)str)[i] >>4,((BYTE*)str)[i] %16);
			strDes.append(tempbuff);
		}
	}
	return strDes;
}


void Tools::ltrim(char *s)
{// 去掉前部的
	unsigned long l=0,p=0,k=0;
	l = strlen(s);
	if( l == 0 ) return;
	p = 0;
	while( s[p] == ' ' || s[p] == '\t' || s[p] == '\n' || s[p] == '\r' )  p++;
	if( p == 0 ) return;
	while( s[k] != '\0') s[k++] = s[p++];
	return;
}

void Tools::rtrim(char *s)
{// 去掉尾部的
	unsigned long l=0,p=0;
	l = strlen(s);
	if( l == 0 ) return;
	p = l -1;
	while( s[p] == ' ' || s[p] == '\t' || s[p] == '\n' || s[p] == '\r' ) {
		s[p--] = '\0';
		if( p < 0 ) break;
	}
	return;
}

time_t Tools::loadLoginTime()
{
	time_t t;
	std::string strValue;
	LoadDataByKey("LoginTime", strValue);
	__String *str = __String::create(strValue);
	t = (time_t)str->intValue();
	return t;
}

void Tools::saveLoginTime(time_t t)
{
	__String *str = __String::createWithFormat("%ld", t);
	SaveDataByKey("LoginTime", str->_string);
}

int Tools::LoadLoginTaskDay()
{
	int iDay;
	std::string strValue;
	LoadDataByKey("LoginTaskDay", strValue);
	__String *str = __String::create(strValue);
	iDay = str->intValue();
	return iDay;
}

void Tools::SaveLoginTaskDay(int iDay)
{
	__String *str = __String::createWithFormat("%d", iDay);
	SaveDataByKey("LoginTaskDay", str->_string);
}

time_t Tools::LoadNextTaskTime()
{
	time_t t;
	std::string strValue;
	LoadDataByKey("TaskTime", strValue);
	__String *str = __String::create(strValue);
	t = (time_t)str->intValue();
	return t;
}

void Tools::SaveNextTaskTime( time_t t )
{
	__String *str = __String::createWithFormat("%ld", t);
	SaveDataByKey("TaskTime", str->_string);
}

