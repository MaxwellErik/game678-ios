﻿#include "ServerListManager.h"

CServerListManager::CServerListManager()
{

}

CServerListManager::~CServerListManager()
{

}


bool CServerListManager::InsertTypeItem( tagGameType GameType[], WORD wCount )
{
	for (WORD i=0;i<wCount;i++)
	{
		m_vcType.push_back(GameType[i]);
	}
	//排序
	tagGameType iTemp; 
	for(int i=1; i<m_vcType.size(); i++) 
	{ 
		for(int j=m_vcType.size()-1;j>=i;j--) 
		{ 
			if(m_vcType[j].wSortID < m_vcType[j-1].wSortID) 
			{ 
				iTemp = m_vcType[j-1]; 
				m_vcType[j-1] = m_vcType[j]; 
				m_vcType[j] = iTemp; 
			} 
		} 
	} 
	return true;
}

bool CServerListManager::InsertKindItem( tagGameKind GameKind[], WORD wCount )
{
	for (WORD i=0;i<wCount;i++)
	{
		m_vcKind.push_back(GameKind[i]);
	}
	return true;
}

bool CServerListManager::InsertServerItem( tagGameServer GameServer[], WORD wCount )
{
	m_vcServer.clear();
	for (WORD i=0;i<wCount;i++)
	{
		m_vcServer.push_back(GameServer[i]);
	}

	//排序
	tagGameServer iTemp; 
	for(int i=1; i<m_vcServer.size(); i++) 
	{ 
		for(ssize_t j=m_vcServer.size()-1;j>=i;j--)
		{ 
			if(m_vcServer[j].wSortID < m_vcServer[j-1].wSortID)
			{ 
				iTemp = m_vcServer[j-1]; 
				m_vcServer[j-1] = m_vcServer[j]; 
				m_vcServer[j] = iTemp;
			} 
		} 
	} 

	return true;
}

tagGameType * CServerListManager::SearchTypeItem( WORD wTypeID )
{
	for (int i=0; i<(int)m_vcType.size(); i++)
	{
		if (m_vcType[i].wTypeID == wTypeID)
		{
			return &m_vcType[i];
		}
	}

	return NULL;
		
}

tagGameKind * CServerListManager::SearchKindItem( WORD wKindID )
{
	for (int i=0; i<(int)m_vcKind.size(); i++)
	{
		if (m_vcKind[i].wKindID == wKindID)
		{
			return &m_vcKind[i];
		}
	}

	return NULL;
}

tagGameServer * CServerListManager::SearchServerItem( WORD wKindID, WORD wServerID )
{
	for (int i=0; i<(int)m_vcServer.size(); i++)
	{
		if (m_vcServer[i].wKindID == wKindID && m_vcServer[i].wServerID == wServerID)
		{
			return &m_vcServer[i];
		}
	}
	return NULL;
}


tagGameType * CServerListManager::EnumTypeItem( ULONGLONG nIndex )
{
	if (m_vcType.size() > nIndex && nIndex >= 0)
	{
		return &m_vcType[nIndex];
	}
	else
	{
		return NULL;
	}
}


tagGameKind * CServerListManager::EnumKindItem( ULONGLONG nIndex )
{
	if (m_vcKind.size() > nIndex && nIndex >= 0)
	{
		return &m_vcKind[nIndex];
	}
	else
	{
		return NULL;
	}
}

tagGameServer * CServerListManager::EnumServerItem( ULONGLONG nIndex )
{
	if (m_vcServer.size() > nIndex && nIndex >= 0)
	{
		return &m_vcServer[nIndex];
	}
	else
	{
		return NULL;
	}
}

void CServerListManager::SortServerItem()
{
	if (m_vcServer.size() <= 0) return;
	int min = 0;
	for (int i = 0; i < m_vcServer.size()-1; ++i)
	{
		min = i;
		for (int j = i+1; j <m_vcServer.size(); ++j)
		{
			if(m_vcServer[j].wSortID < m_vcServer[min].wSortID)
			{
				min = j;
			}
		}
		if(min != i)
		{
			tagGameServer temp = m_vcServer[min];
			m_vcServer[min] = m_vcServer[i];
			m_vcServer[i] = temp;
		}
	}
}

void CServerListManager::ClearAllData()
{
	m_vcType.clear();
	m_vcKind.clear();
	m_vcServer.clear();
}

