﻿#ifndef _LOBBYSOCKETSINK_H_
#define _LOBBYSOCKETSINK_H_

#include "cocos2d.h"
#include "UserManager.h"
#include "TcpClientThread.h"
#include "LobbyView.h"
#include "LobbyLayer.h"
#include "GameBank.h"
#include "LoginLayer.h"

class LobbySocketSink : public TcpCallback , public Node
{
public:
	~LobbySocketSink(void);

	static LobbySocketSink* sharedSocketSink();		//自己的单例接口
	static TcpClientSocket *sharedLoginSocket(void);

	//读取事件
	virtual bool OnEventTCPSocketRead(CMD_Command Command, VOID * pData, WORD wDataSize);
	//连接事件
    virtual bool OnEventTCPSocketLink();
	//关闭事件
	virtual bool OnEventTCPSocketShut();//{return true;}

	virtual void update(float delta);
    
    bool OnEventTCPConnectError();
    
    virtual void OnError(float delta);
    
    void OnConnectSuccess(float ft);

	//登录消息
	bool OnSocketMainLogon(CMD_Command Command, void * pBuffer, WORD wDataSize);
	//列表消息
	bool OnSocketMainServerList(CMD_Command Command, void * pBuffer, WORD wDataSize);
	//系统命令码
	bool OnSocketMainSystem(CMD_Command Command, void * pBuffer, WORD wDataSize);

	bool OnSocketUserServer( CMD_Command Command, void * pBuffer, WORD wDataSize );

	void StartReceive();
	void StopReceive();
	void setRcvLayer(GameLayer *pGameLayer);
	void closeSocket();

	int safeconnect(char* ip,int port,time_t Unixdate);
	bool TestConnectServer(string _ip);
	bool ConnectServer();
    
	bool ConnectServer(char* ip, DWORD port);
	bool Jionroom(int iPort,WORD KindID);

	bool SendMobileData(WORD wSubCmdID , void * pData , WORD wDataSize);

	bool SendMobileData(WORD wSubCmdID);

	void SetLobbyLayer(LobbyLayer* _layer);

	void SetBankLayer(GameBank* _layer);

	void SetLoginLayer(LoginLayer* _layer);
    
    int getRand(int start, int end);
protected:
	LobbySocketSink(void);
    void ConnectToServer();
private:

	static LobbySocketSink* m_pLobbySocketSink;
	static TcpClientSocket *m_pLobbySocket;

public:
	GameLayer*				 m_pRcvLayer;
	LobbyLayer*				 m_LobbyLayer;
	
	GameBank*				 m_BankLayer;
	LoginLayer*				 m_LoginLayer;
};

#endif
