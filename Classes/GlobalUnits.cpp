﻿#include "GlobalUnits.h"
#include "Resource.h"
#include "Tools.h"
#include "LocalDataUtil.h"
#include <algorithm>
//////////////////////////////////////////////////////////////////////////

//全局变量
CGlobalUnits					g_GlobalUnits;						//信息组件			

CGlobalUnits::CGlobalUnits( void )
{
	m_IsLogin = false;
	m_bSingleGame=true;
	m_bIsMePlaying=false;
	m_wStep=0;
    m_HeartCount=0;
	m_bNoviceTeaching=false;
	m_iFreeReceiveCount = 0;
	ZeroMemory(&m_GlobalUserData, sizeof(m_GlobalUserData));
	ZeroMemory(m_BankPassWord,33);
	m_LogonBank_IsOk = false;
	m_bLink = false;
	ZeroMemory(m_pUserItem,sizeof(m_pUserItem));
	m_wMeChair = 1;
	m_bCheckPhone = false;
    m_isLoadLayerShow = false;
	ZeroMemory(m_SignName,sizeof(m_SignName));
										//windows  android
    m_IP = "";
    m_ChannelID = 0;
    m_IPIndex = 0;
	m_ServerPort = 0;
    m_boradcastTime = 0;
    m_FreeAccount = 0;
    m_FreeWeixin = 0;
}

int CGlobalUnits::GetFaceID(LONGLONG lScore)
{
    int nImageIndex = 0;
    if (lScore < 10000)
    {
        nImageIndex = 0;
    }
    else if (lScore < 100000)
    {
        nImageIndex = 1;
    }
    else if (lScore < 200000)
    {
        nImageIndex = 2;
    }
    else if (lScore < 400000)
    {
        nImageIndex = 3;
    }
    else if (lScore < 800000)
    {
        nImageIndex = 4;
    }
    else if (lScore < 1600000)
    {
        nImageIndex = 5;
    }
    else if (lScore < 3200000)
    {
        nImageIndex = 6;
    }
    else if (lScore < 6400000)
    {
        nImageIndex = 7;
    }
    else if (lScore < 12800000)
    {
        nImageIndex = 8;
    }
    else if (lScore <25600000)
    {
        nImageIndex = 9;
    }
    else if (lScore < 50000000)
    {
        nImageIndex = 10;
    }
    else
    {
        nImageIndex = 11;
    }
    return nImageIndex;
}

string CGlobalUnits::getFace(BYTE gender, LONGLONG lScore)
{
    char buf[30];
    ZeroMemory(buf, sizeof(buf));
    sprintf(buf, "%s_head%d.png", gender == GENDER_BOY ? "man" : "woman", GetFaceID(lScore));
    string ret = buf;
    return ret;
}

void CGlobalUnits::InsertUserData(UserData data)
{
    sprintf(m_UserDataVec[0].szAccounts,"%s",data.szAccounts);
    sprintf(m_UserDataVec[0].pszPassword,"%s",data.pszPassword);
}

void CGlobalUnits::SaveUserData()
{
    LocalDataUtil::SaveUserName(m_UserDataVec[0].szAccounts, DATA_KEY_UZ_USERNAME);
    LocalDataUtil::SavePassword(m_UserDataVec[0].pszPassword, DATA_KEY_UZ_PASSWORD);
}

void CGlobalUnits::AddBuyRecordJson( const char *szOrderNum, const char *szJson )
{
	for(int i=0 ; i<m_vcGoldRecord.size() ; i++)
	{
		BuyRecordInfo &pInfo = m_vcGoldRecord[i];
		if (strcmp(pInfo.szNum , szOrderNum) == 0)
		{
			pInfo.cbStatus=0;
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
			strcpy(pInfo.szJson, szJson);
#endif			
			SaveBuyRecord();
		}
	}
}


bool CGlobalUnits::OrderSuccess(const char *szOrderNum , bool bCheck)
{
	for(int i=0 ; i<m_vcGoldRecord.size() ; i++)
	{
		BuyRecordInfo &pInfo = m_vcGoldRecord[i];
		if (strcmp(pInfo.szNum , szOrderNum) == 0)
		{
			if (pInfo.cbStatus == 0)
			{
				if(bCheck) return true;
				pInfo.cbStatus=1;
				SaveBuyRecord();
				return true;
			}
		}
	}


	return false;
}

void CGlobalUnits::SaveBuyRecord()
{
	loadBuyRecord();
	char szBuf[100];
	sprintf(szBuf, "%d_%s", GetUserID(), DATA_KEY_BUYRECORD);
	LocalDataUtil::writeFile(szBuf , NULL, 0, true);
	for(int i=0 ; i<m_vcGoldRecord.size() ; i++)
	{
		LocalDataUtil::writeFile(szBuf, &m_vcGoldRecord[i], sizeof(BuyRecordInfo));
	}
}

void CGlobalUnits::AddBuyRecord(IosDword dwUserID, WORD wID , BYTE cbType , TCHAR *szNum, int lScore, int lMoney)
{
	loadBuyRecord();
	LONG lPrice[2] = {0};
	memcpy(lPrice , BUY_GOLD_SETUP[wID-1] , sizeof(lPrice));
	BuyRecordInfo record;
	ZeroMemory(&record , sizeof(record));
	record.wBuyID = wID;
	record.cbType = cbType;
	record.lScore = lScore;
	record.lMoney = lMoney;
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
	record.cbStatus = 2;	//苹果的多一个未付款状态
#endif
	strcpy(record.szNum, szNum);
	Tools::GetSystemTime(record.szTime);
	m_vcGoldRecord.push_back(record);
	//dwUserID
	LocalDataUtil::writeBuyRecord(dwUserID, &record);
}

void CGlobalUnits::loadBuyRecord()
{
	if (m_vcGoldRecord.empty())
	{
		VectorBuyRecordInfo vcRecordInfo;
		LocalDataUtil::readFileBuyRecord(GetUserID(), vcRecordInfo);
		for (int i=0 ; i<(int)vcRecordInfo.size() ; i++)
		{
			const BuyRecordInfo &info = vcRecordInfo[i];
			m_vcGoldRecord.push_back(info);		
		}
	}
}

void CGlobalUnits::cleanAllBuyRecord()
{
	m_vcGoldRecord.clear();
}

int getLengthW(const char * psz)
{
	if(!psz)
		return 0;
	int nLen = 0;
	for(int i = 0;;i++)
	{
		if(psz[i] == '\0')
			return nLen;
		nLen++;
		if(psz[i] > 127 || psz[i] < 0)
		{
			i++;
		}
	}
	return nLen;
}

//切换椅子
WORD CGlobalUnits::SwitchViewChairID(WORD wChairID)
{
	//转换椅子
	WORD wViewChairID=(m_wChairCount + wChairID-m_wMeChair);
	switch (m_wChairCount)
	{
	case 2: { wViewChairID+=1; break; }
	case 3: { wViewChairID+=1; break; }
	case 4: { wViewChairID+=2; break; }
	case 5: { wViewChairID+=2; break; }
	case 6: { wViewChairID+=3; break; }
	case 7: { wViewChairID+=3; break; }
	case 8: { wViewChairID+=4; break; }
	}
	return wViewChairID%m_wChairCount;
}

//设置用户
bool CGlobalUnits::SetUserInfo(WORD wChairID, tagUserData * pUserItem)
{
	if(wChairID==INVALID_CHAIR)
	{
		ZeroMemory(m_pUserItem,sizeof(m_pUserItem));
		return true;
	}
	ASSERT(wChairID<MAX_CHAIR);
	if (wChairID>=MAX_CHAIR) return false;
	m_pUserItem[wChairID]=pUserItem;
	return true;
}

const tagUserData * CGlobalUnits::GetUserInfo(WORD wChairID)
{
	ASSERT(wChairID<MAX_CHAIR);
	if (wChairID>=MAX_CHAIR) return NULL;
	return m_pUserItem[wChairID];
}
