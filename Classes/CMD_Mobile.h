﻿#ifndef CMD_MOBILE_HEAD_FILE
#define CMD_MOBILE_HEAD_FILE

#include <vector>
#include "GlobalDef.h"
//////////////////////////////////////////////////////////////////////////
#pragma pack(1)

#define MDM_GP_MOBILE						5								//新增手机消息
#define LEN_NICKNAME						32
#define LEN_PASSWORD						33 
#define LEN_MACHINE_ID						33
#define LEN_MD5								33
#define LEN_ACCOUNTS						32									//帐号长度
#define LEN_UNDER_WRITE						32									//个性签名
//////////////////////////////////////////////////////////////////////////
//服务端消息
//////////////////////////////////////////////////////////////////////////

#define SUB_MB_SEND_USERINFO				205									//完善资料
#define SUB_MB_RECV_USERINFO_SUCC			206									//完善资料

#define SUB_S_GP_TRANSPROP					300								//返回道具
#define SUB_S_GP_SENDPROPSUCCESS			301								//赠送道具成功
#define SUB_S_GP_SENDPROPERROR				302								//赠送道具失败
#define SUB_S_GP_SENDRECORD					303								//返回赠送记录

//转账
#define SUB_S_GP_TRANSFER_GOLD_END			500	
#define SUB_S_GP_CHAXUN_USER_END			501								//转账用户查询
#define SUB_S_GP_REFRESH_SCORE				502								//刷新积分
#define SUB_S_GP_TRANSFER_REC				503								//转账记录

#define SUB_S_GP_STOREGETEND				504								//存取结束
#define SUB_S_GP_BANKGETREC					506								//存取结束
#define SUB_S_GP_BANKOPEN				    507								//银行开关
#define SUB_S_GP_USERINFOBIND				508								//用户信息
#define SUB_S_GP_BINDUSERINFO			    509								//服务端发送用户信息

#define MDM_GR_INSURE				5										//用户信息


#define SUB_GP_MODIFY_TUIJIANREN			201                         //设置推荐人ID子标识
#define SUB_GP_SETSPREADERIDRESULE_MB		499							//推荐人服务端返回值



/////////////////////////////////////////
//银行服务
#define MDM_GF_BANK                 103                 //银行消息

//操作结果
#define SUB_GP_OPERATE_SUCCESS      2000				//操作成功
#define SUB_MB_OPERATOR_ERROR		2001				//操作失败

#define SUB_MB_BANK_STORAGE         2450                 //银行存储
#define SUB_MB_BANK_GET             2451                 //银行提取
#define SUB_MB_BANK_PRESENT         2452                 //赠送酒吧豆
#define SUB_MB_BANK_QUERY           2459                 //查询金币
#define SUB_MB_GET_NICKNAME         2460                 //查询用户信息
#define SUB_MB_BANK_RECORD          2461                 //查询银行记录

#define SUB_MB_BANK_STORAGE_SUCCESS     2550              //银行存储
#define SUB_MB_BANK_GET_SUCCESS         2551              //银行提取
#define SUB_MB_BANK_PRESENT_SUCCESS     2552              //赠送酒吧豆
#define SUB_MB_BANK_QUERY_SUCCESS       2559              //查询金币
#define SUB_MB_GET_NICKNAME_SUCCESS     2560              //用户信息
#define SUB_MB_BANK_RECORD_SUCCESS      2561              //查询银行记录结果

#define SUB_GF_MODIFY_PASSWORD          454               //修改密码
#define SUB_GF_MODIFYPWD_RESULT         602               //密码修改结果
#define MAX_BANK_RECORD_COUNT           20                  //一次发送银行记录条数的上限
//银行资料
struct CMD_GR_S_UserInsureInfo
{
	BYTE                  cbActivityGame;                   //游戏动作
	WORD				  wRevenueTake;                     //税收比例
	WORD				  wRevenueTransfer;                 //税收比例
	WORD				  wServerID;                        //房间标识
	LONGLONG              lUserScore;                       //用户金币
	LONGLONG              lUserInsure;                      //银行金币
	LONGLONG              lTransferPrerequisite;            //转账条件
};

//银行记录
//struct tagBankRecordInfo
//{
//    UINT32                dwTradeType;          //记录类型(1、存入 2、取出 3、赠送)
//    UINT32                dwSwapScore;          //交换值
//    UINT32                dwInsureScore;          //主动玩家最终金币数量
//    UINT32                dwTargetInsureScore;      //被动玩家最终金币数量
//    UINT32                dwSourceUserID;          //主动玩家ID
//    UINT32                dwTargetUserID;          //被动玩家ID
//    CHAR                  szSourceAccounts[NAME_LEN];    //主动玩家名称
//    CHAR                  szTargetAccounts[NAME_LEN];    //被动玩家名称
//    CHAR                  szCollectDate[TIME_LEN];    //记录时间
//};


#define DBR_MB_BANK_RECORD        2018              //查询银行记录
#define SUB_MB_BANK_RECORD_SUCCESS    2561                //查询银行记录结果

//日志查询请求
struct tagMBLogRequest
{
    CHAR                szCollectDate[12];    //记录时间
    UINT32              dwUserID;      //玩家ID
    UINT32              dwPage;        //页数
};
//返回的日志头
struct tagMBLogHead
{
    CHAR                szCollectDate[12];      //记录时间
    UINT32              dwCurPage;              //当前页数
    UINT32              dwTotalNum;             //总计录数
    UINT32              dwNum;                  //当前有效计录数
};

//转账日志
struct tagMBLogTransfer
{
    CHAR                szCollectDate[TIME_LEN];        //记录时间
    CHAR                szTargetAccounts[NAME_LEN];      //转入者名称
    UINT32              dwTargetGameID;                 //转入者ID
    LONGLONG            llSwapScore;                    //交易数量
    CHAR                szClientIP[16];                 //操作者IP
};

//查询银行记录
struct CMD_MB_BankRecord
{
    tagMBLogRequest          request;
    UINT32                   dwTransferType;    //转帐类型(0:全部。1：转入。 2：转出)
    UINT32                   dwTargetID;        //指定查询账户
};

struct CMD_MB_BankRecordResult
{
    tagMBLogHead             head;
    tagMBLogTransfer         record[13];
};

//银行赠送成功
struct CMD_MB_BankPresentSuccess
{
    LONGLONG              lNumber;                  //交易编号
    LONGLONG              lSwapScore;               //交易金额
    LONGLONG              lRevenue;                 //税收
    UINT32                dwTargetGameID;           //目标游戏ID
    CHAR                  szSrcNickname[NAME_LEN];  //赠送者昵称
    CHAR                  szTime[TIME_LEN];         //交易时间
};

//银行成功
struct CMD_GR_S_UserInsureSuccess
{
	BYTE                  cbActivityGame;                   //游戏动作
	LONGLONG              lUserScore;                       //身上金币
	LONGLONG              lUserInsure;                      //银行金币
	char				  szDescribeString[128];            //描述消息
};

//银行失败
struct CMD_GR_S_UserInsureFailure
{
	BYTE                  cbActivityGame;                       //游戏动作
	int		              lErrorCode;                           //错误代码
	char                  szDescribeString[128];                //描述消息
};

//消息返回
struct CMD_GP_SetSpreaderID_Rst
{
	LONGLONG            lResultCode;    
	CHAR              szDescribeString[128];                    //描述消息
};

//消息结构
struct CMD_GP_UserSetSpreaderID
{
	UINT32      dwMyUserID;
	UINT32      dwSpreaderID;
    CHAR        szPassword[33];            //玩家密码

};

//////////////新银行////////////////
//银行资料
struct CMD_SC_QueryScore
{
	LONGLONG						lUserScore;							//用户金币
	LONGLONG						lUserInsure;						//银行金币
};

//查询银行
struct CMD_GP_QueryInsureInfo
{
	IosDword							dwUserID;							//用户 I D
};

struct CMD_GP_UserInsureLogon_MB
{
	IosDword							dwUserID;						//用户 I D
	char							szPassword[LEN_PASSWORD];		    //用户密码
	char							szMachineID[LEN_MACHINE_ID];		//机器序列
};

//手机银行失败
struct CMD_GP_UserInsureFailure_MB
{
	int							lResultCode;						//错误代码
	char						szDescribeString[128];				//描述消息
};

//游戏银行数据查询
struct CMD_GP_QueryGameInsureInfo
{
    UINT32              dwUserID;                 //用户ID
    CHAR                szPassword[PASS_LEN];     //用户密码
    WORD                wKindID;                   //游戏类型ID
    LONGLONG            lToken;                     //验证码
};
struct CMD_GP_UserGameInsureInfo
{
	LONGLONG							lUserScore;							//用户金币
	LONGLONG							lUserInsure;						//银行金币
};

//手机银行成功
struct CMD_GP_UserInsureSuccess_MB
{
	IosDword							dwUserID;							//用户 I D
	LONGLONG						lUserScore;							//用户金币
	LONGLONG						lUserInsure;						//银行金币
	CHAR							szDescribeString[128];				//描述消息
};

//手机用户查询
struct CMD_GP_QueryUserInfoRequest_MB
{
    UINT32            dwUserID;           //自己的UID
    UINT32            dwTargetGameID;     //对方的游戏ID
    LONGLONG          lInsureScore;       //转账金额

};

//手机用户查询
struct CMD_GP_UserTransferUserInfo_MB
{
    
    UINT32          nType;          //0：无需密码 4：需微信验证码
    CHAR            szNickname[NAME_LEN];  //玩家昵称
};

//赠送酒吧豆
struct CMD_MB_BankPresent
{
    UINT32                  dwUserID;                   //用户ID
    LONGLONG                lPresentValue;              //赠送数目
    CHAR                    szPassword[PASS_LEN];       //用户密码
    CHAR                    szValidateCode[12];        //微信验证码
    UINT32                  dwGameID;                   //接收者游戏ID
    LONGLONG                lToken;                     //验证码

};

//转账记录
//手机
struct CMD_GP_UserTransferRecordResult_MB
{
	IosDword                           dwRecordID;                             //记录ID
	IosDword                           dwSourceUserID;                         //赠送用户ID
	IosDword                           dwTargetUserID;                           //获赠用户ID
	CHAR                           szSourceNickName[64];         //赠送用户昵称
	CHAR                           szTargetNickName[64];           //赠送
	LONGLONG						lScore;								    //用户游戏币
	WORD							wYear;
	WORD							wMonth;
	WORD							wDay;
	WORD							wHour;
	WORD							wMinu;
	WORD							wSecd;

};

//修改密码
struct CMD_GP_ModifyInsurePass_MB
{
	IosDword							dwUserID;							//用户 I D
	CHAR							szDesPassword[LEN_PASSWORD];		//用户密码
	CHAR							szScrPassword[LEN_PASSWORD];		//用户密码
};

//银行操作结果
//操作失败
struct CMD_SC_OperatorError
{
    UINT32              dwErrorCode;    // 错误码
    CHAR                szErrorDescribe[128]; // 错误信息
};

//操作成功
struct CMD_GP_OperateSuccess_MB
{
	int							lResultCode;						//操作代码
	CHAR						szDescribeString[128];				//成功消息
};

///////////////新银行结尾////////////////////////////

//完善手机锁的信息
struct CMD_MB_PostUserInfo 
{
	LONGLONG						dwUserID;
	CHAR							szPassword[LEN_PASSWORD];			//用户密码
	CHAR							szEmail[LEN_PASSWORD];			//邮箱
	CHAR							szMobilePhone[12];	//电话号码
};

//完善手机锁的信息
struct CMD_MB_PostUserInfo_Rst 
{
	LONGLONG						lResultCode;		
	CHAR							szDescribeString[128];				//描述消息
};

///房间滚动消息
struct CMD_S_GP_TopMsgInfo
{
	int m_MsgCount;
	CHAR  szTopMsg[5][250];
};

struct CMD_S_GP_BindUserInfo
{
	bool							bBindResult;								//成功标识
};
///完善用户信息
struct CMD_S_GP_UserInfoBind
{
	IosDword							dwUserID;								//用户id
	CHAR							szIdCard[20];							//IDCARD
	CHAR							szPhone[20];							//电话号码
};

struct CMD_S_GP_BankOpen
{
	bool							bBankOpen;								//成功标识
};

//金币赠送
struct CMD_GP_TransferUser_Result
{
	bool								bOperate;						//成功标识
	LONGLONG							lHappyBear;                     //积分数量
};
//玩家查询
struct CMD_GP_Chaxun_User_Result
{
	bool								bReturn;						//玩家ID
	TCHAR								szNickName[32];			//登录密码
};



//////////////////////////////////////////////////////////////////////////
//客户端消息
//////////////////////////////////////////////////////////////////////////
#define SUB_C_GP_GETTASK					100								//获取任务
#define SUB_C_GP_TASK_FINISH				101								//任务完成
#define SUB_C_GP_RECEIVETASKPRIZE			102								//领取任务奖励
#define SUB_C_GP_TAST_STATUS				103								//获取任务状态

#define SUB_C_GP_GETTRANSPROP				300								//获取交易道具
#define SUB_C_GP_SENDPROP					301								//赠送道具
#define SUB_C_GP_SENDRECORD					302								//获取赠送记录

#define SUB_C_GP_TRANSFER_GOLD				500								//金币赠送
#define SUB_C_GP_CHAXUN_USER				501								//转账用户查询
#define SUB_C_GP_REFRESH_SCORE				502								//刷新积分
#define SUB_C_GP_TRANSFER_REC				503								//转账记录

#define SUB_C_GP_BANKSTORE					504								//存
#define SUB_C_GP_BANKGET				    505								//取
#define SUB_C_GP_BANKGETREC				    506								//存取记录
#define SUB_C_GP_BANKOPEN				    507								//银行开关
#define SUB_C_GP_USERINFOBIND			    508								//用户信息
#define SUB_C_GP_BINDUSERINFO			    509								//客户端发送用户信息
#define SUB_C_GP_TOPRUNMSG					510								//大厅上方滚动信息

//获取审核状态
struct CMD_MB_GetAuditState
{
    WORD        auditState;
    UINT32      pass;
};

struct CMD_MB_Android_Update
{
    UINT32      needUpdate;
};

///房间滚动消息
struct CMD_C_GP_TopMsgInfo
{
	WORD								wKindID;						//游戏ID
};

struct CMD_C_GP_BindUserInfo
{
	IosDword							dwUserID;								//用户id
	CHAR							szLogonPass[33];
	CHAR							szmobile[15];
	CHAR							szemail[30];
	int								wquestion;
	CHAR							szanswer[250];
};

///完善用户信息
struct CMD_C_GP_UserInfoBind
{
	IosDword							dwUserID;								//用户id
};

//取
struct CMD_GP_BankGetScore
{
    UINT32                  dwUserID;               //用户ID
    LONGLONG                lGetValue;              //提取酒吧豆
    CHAR                    szPassword[PASS_LEN];   //用户密码
    WORD                    wKindID;                //游戏类型ID
    LONGLONG                lToken;                 //验证码
};
//存
struct CMD_GP_BankStoreScore
{
    UINT32                  dwUserID;               //用户ID
    LONGLONG                lStorageValue;          //存储酒吧豆
    CHAR                    szPassword[PASS_LEN];   //用户密码
    WORD                    wKindID;                //游戏类型ID
    LONGLONG                lToken;                 //验证码
};

//银行结果
struct CMD_S_GP_StoreGetBank
{
	bool							bOperate;								//成功标识
	LONGLONG                        lScore;                           //
	LONGLONG                        lInsureScore;                     //
	CHAR							szErrorDescribe[128];					//提示消息
};

//修改密码
struct CMD_GF_ModifyPassword
{
    UINT32               dwUserID;//用户ID
    UINT32               nType;//1:登录 2:银行
    CHAR                 szOldPwd[PASS_LEN];//旧密码
    CHAR                 szNewPwd[PASS_LEN];//新密码
    LONGLONG             lToken;
};

//密码修改结果
struct CMD_GF_ModifyPwdResult
{
    UINT32              nType;//1:登录 2:银行
    UINT32              lErrorCode;            //错误代码
    CHAR                szNewPwd[PASS_LEN];//新密码
    CHAR                szErrorDescribe[128];      //错误消息
};

struct CMD_C_GP_TransRecd
{
	IosDword								dwUserID;						//玩家ID
	CHAR								szPassWord[33];			//登录密码
};

struct TransRecdinfo
{
	IosDword								dwSendUserID;					//客户端玩家ID
	TCHAR								szSendNickName[32];		//对方(操作玩家)昵称
	LONGLONG							lBeforeScore;					
	LONGLONG							lAfterScore;	
	LONGLONG							lMyScore;						//积分数量
	TCHAR								szDateTime1[32];					//时间
	TCHAR								szDateTime2[32];					//时间
	WORD								wChangType;						//类型   101,102为存取;3为转账
};

struct CMD_S_GP_TransRecd
{
	WORD								wRecordCount;					//记录数
	TransRecdinfo						Recdinfo[20];					//记录信息
};

//刷新积分
struct CMD_C_GP_RefreshScore
{
	IosDword								dwUserID;						//玩家ID
	CHAR								szPassWord[33];			//登录密码
};

//刷新积分
struct CMD_S_GP_RefreshScore
{
	IosDword								dwUserID;						//玩家ID
	LONGLONG							lMyScore;						//积分数量
	LONGLONG							lMyBankScore;						//积分数量
};

struct CMD_GP_TransferUser
{
	IosDword								dwUserID;						//玩家ID
	IosDword								dwReceiveGameid;
	LONGLONG                            lHappyBear;                     //积分数量
	CHAR								szPassWord[33];			//登录密码
	WORD								wKindID;						//游戏ID
};
//玩家查询
struct CMD_GP_Chaxun_User
{
	IosDword								dwGameID;						//玩家ID
	WORD								wKindID;						//玩家ID
};


//////////////////////////////////////////////////////////////////////////

//任务结构
struct Task
{
	WORD	wTaskID;						//任务ID
	CHAR	szTitle[32];					//任务标题
	CHAR	szContent[64];					//任务内容
	BYTE	cbType;							//暂未使用
	BYTE	cbConditionType;				//任务类型（1--登录任务 2--游戏局数 3--获胜局数 4--获胜分数）
	int	lCondition;						//完成条件
	int	lPrize;							//任务奖励
};

struct TaskStatus
{
	WORD	wTaskID;						//任务ID
	BYTE	cbStatus;						//任务状态
	int	lProgress;						//任务条件
};

struct UserRank
{
	WORD		wFaceID;
	WORD		wGender;
	WORD		UserType;
	CHAR		szNickName[32];
	LONGLONG	lData;
	LONGLONG	GameID;
	CHAR		Tel[12];
	CHAR		UnWrite[128];
};

struct TransProp
{
	WORD		wID;
	CHAR		szName[32];
	int		lScore;
	int		lMoney;
	BYTE		cbUseMemberOrder;
};

struct TransPropRecord
{
	CHAR		szNickName[32];
	CHAR		szReceiveNickName[32];
	WORD		wPropID;
	CHAR		szPropName[32];
	WORD		wAmount;
	int		lPayScore;
	CHAR		szRecordTime[32];
};

//////////////////////////////////////////////////////////////////////////
//房间状态数据包

#define MDM_GR_STATUS				4									//状态信息

#define SUB_GR_TABLE_INFO			100									//桌子信息
#define SUB_GR_TABLE_STATUS			101									//桌子状态

//桌子状态结构
struct tagTableStatus
{
    BYTE							bTableLock;							//锁定状态
    BYTE							bPlayStatus;						//游戏状态
};

//桌子状态数组
struct CMD_GR_TableInfo
{
    WORD							wTableCount;						//桌子数目
    tagTableStatus					TableStatus[512];					//状态数组
};

//桌子状态信息
struct CMD_GR_TableStatus
{
    WORD							wTableID;							//桌子号码
    BYTE							bTableLock;							//锁定状态
    BYTE							bPlayStatus;						//游戏状态
};

struct CMD_S_GP_TaskConfig
{
	WORD	wTaskCount;
	Task	task[20];
};

struct CMD_S_GP_TaskStatus
{
	WORD		wTaskCount;
	TaskStatus	taskStatus[20];
};

struct CMD_S_GP_Rank
{
	WORD		wRankCount;
	UserRank	userRank[50];
};

struct CMD_S_GP_PropConfig
{
	WORD		wPropCount;
	TransProp	transProp[20];
};

struct CMD_S_GP_PropRecord
{
	WORD				wRecordCount;
	WORD				wTotalCount;
	TransPropRecord		transPropRecord[20];
};

struct CMD_S_GP_Error
{
	int				lErrorCode;
	CHAR				szErrorDescribe[128];
};

struct CMD_S_GP_RcvTaskSuccess
{
	WORD				wTaskID;
};

struct CMD_S_GP_RcvTaskError
{
	WORD				wTaskID;
	CHAR				szErrorDescribe[128];
};

//////////////////////////////////////////////////////////////////////////


struct CMD_C_GP_GetTask
{
	IosDword	dwUserID;
	WORD	wKindID;
};

struct CMD_C_GP_GetTaskStatus
{
	IosDword	dwUserID;
	WORD	wKindID;
};


struct CMD_C_GP_GetTransProps
{
	WORD	wKindID;
};

struct CMD_C_GP_RcvTaskPrize
{
	DWORD				dwUserID;
	WORD				wKindID;
	WORD				wTaskID;
};

struct CMD_C_GP_SendProp
{
	DWORD				dwUserID;
	DWORD				dwRcvUserID;
	WORD				wKindID;
	WORD				wPropID;
	int				lAmount;
	int				lPayScore;
	BYTE				cbPlatform;
};

struct CMD_C_GP_GetSendRecord
{
	WORD	wKindID;
	IosDword	dwUserID;
	WORD	wPageIndex;
	WORD	wItemCount;
};


typedef	std::vector<TaskStatus>			VectorTaskStatus;

#pragma pack()


////////////////////////////////////////////////////////////////////////////////////////////////

#endif
