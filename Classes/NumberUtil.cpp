#include "NumberUtil.h"
#include "Tools.h"

NumberUtil::NumberUtil(void)
{
}


NumberUtil::~NumberUtil(void)
{
}

LabelAtlas * NumberUtil::createNumberLabel( char *szNum, const char *szNumLabel, int iSize)
{
    LabelAtlas *pRet = LabelAtlas::create();
	Texture2D *texture = Director::getInstance()->getTextureCache()->addImage(szNumLabel);
	ASSERT(texture->getPixelsWide()%iSize == 0);
	if(pRet && pRet->initWithString(szNum, texture, texture->getPixelsWide()/iSize, texture->getPixelsHigh(), '0'))
	{
		pRet->autorelease();
		return pRet;
	}
//	CC_SAFE_DELETE(pRet);
	return NULL;
}

LabelAtlas * NumberUtil::createNumberLabel( LONG lNum, const char *szNumLabel)
{
	char szNum[32]={0};
	sprintf(szNum , "%ld" , lNum);
	return createNumberLabel(szNum , szNumLabel);
}

LabelAtlas * NumberUtil::createNumberLabel( LONGLONG lNum, const char *szNumLabel)
{
	char szNum[32]={0};
	Tools::NumberToChar(szNum , lNum);
	return createNumberLabel(szNum , szNumLabel);
}

LabelAtlas * NumberUtil::createFishScore( char *szNum, WORD wMulity , bool bDouble )
{
	char szLable[32] = {0};
	if (wMulity >= 10)
	{
		if (bDouble)
		{
			strcpy(szLable , NUM_DOUBLE_BIG);
		}
		else
		{
			strcpy(szLable , NUM_BIG);
		}	
	}
	else
	{
		if (bDouble)
		{
			strcpy(szLable , NUM_DOUBLE_SMALL);
		}
		else
		{
			strcpy(szLable , NUM_SMALL);
		}
	}
	return createNumberLabel(szNum , szLable);
}
