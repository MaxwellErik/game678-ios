﻿#ifndef _GAME_PARM_H_
#define _GAME_PARM_H_

#include "cocos2d.h"

#include "AppMacros.h"
#include "TcpClientSocket.h"
#include "Thread.h"


USING_NS_CC;

using namespace std;

// 主要 参数设定 ========================================================================================

#define VERSIONNUM "V 1.0.0"

// 定义手机型态
#define _ANDROID_

#ifdef _IPHONE_
	#define PHONETYPE "ipone"
#endif
#ifdef _IPAD_
	#define PHONETYPE "ipad"
#endif
#ifdef _ANDROID_
	#define PHONETYPE "android"
#endif

#define UZ_CCP(__X__,__Y__) CC_POINT_PIXELS_TO_POINTS(ccp(__X__, __Y__))

// 设定 游戏 桌面大小
#define _STANDARD_SCREEN_SIZE_ (Size(1920, 1080))
#define	_STANDARD_SCREEN_CENTER_ (Vec2(960, 540))

#define _CELL_BASE_POINT_(i,k) (Vec2(_STANDARD_SCREEN_CENTER_.x-4+(i-2)*(199+22), _STANDARD_SCREEN_CENTER_.y-27-(k-1)*(156+5)))
#define _CELL_FRAME_POINT_(i,k) (Vec2(_CELL_BASE_POINT_(i,k).x , _CELL_BASE_POINT_(i,k).y-3))
#define _MARY_CELL_POINT_(i) (Vec2(323+i*213 , 249))

#define _LEFT_LIGHT_POINT_(i) (Vec2(9+60/2,591-49/2-i*50-i/3*12))
#define _RIGHT_LIGHT_POINT_(i) (Vec2(_STANDARD_SCREEN_SIZE_.width-10-60/2,591-49/2-i*50-i/3*12))

// 设定 游戏 大小 In Pixels
#define _STANDARD_SCREEN_SIZE_IN_PIXELS_ (CCEGLView::sharedOpenGLView()->getFrameSize())
#define	_STANDARD_SCREEN_CENTER_IN_PIXELS_ (CC_POINT_PIXELS_TO_POINTS(_STANDARD_SCREEN_CENTER_))

// 设定 拉伸 大小
#define _SCALE_FACTOR_X_ (_STANDARD_SCREEN_SIZE_IN_PIXELS_.width / _STANDARD_SCREEN_SIZE_.width)
#define _SCALE_FACTOR_Y_ (_STANDARD_SCREEN_SIZE_IN_PIXELS_.height / _STANDARD_SCREEN_SIZE_.height)

// 设定 字型 参数
#define _GAME_FONT_SIZE_20_ (20 / CC_CONTENT_SCALE_FACTOR())
#define _GAME_FONT_SIZE_24_ (24 / CC_CONTENT_SCALE_FACTOR())
#define _GAME_FONT_SIZE_26_ (26 / CC_CONTENT_SCALE_FACTOR())
#define _GAME_FONT_SIZE_28_ (28 / CC_CONTENT_SCALE_FACTOR())
#define _GAME_FONT_SIZE_30_ (30 / CC_CONTENT_SCALE_FACTOR())
#define _GAME_FONT_SIZE_32_ (32 / CC_CONTENT_SCALE_FACTOR())

// 设定 使用 字型
#define _GAME_FONT_NAME_1_ "Arial"
#define _GAME_FONT_NAME_2_ "AppleGothic"

//字体颜色
#define _GAME_FONT_COLOR_1_ Color3B(201, 190, 220)
#define _GAME_FONT_COLOR_2_ Color3B(168, 153, 192)
#define _GAME_FONT_COLOR_3_ Color3B(85,  75,  134)
#define _GAME_FONT_COLOR_4_ Color3B(236, 191, 89)
#define _GAME_FONT_COLOR_5_ Color3B(243, 216, 72)
#define _GAME_FONT_COLOR_6_ Color3B(245, 239, 179)

//输入框背景
#define INPUT_BACKGROUND        "Common/bg_touming.png"

#define _GAME_LOOP_TICK_ (1.0f/33.0f)



// 网路 参数设定 ========================================================================================

#define _DEBUG_TEST_

#ifdef _DEBUG_TEST_

	//内网 测试手游服务器
	#define _GAME_SERVER_COMPETE_ADDRSS_IP_ "10.0.90.221"  
	#define _GAME_SERVER_COMPETE_ADDRSS_PORT_ (18192)  

	#define _GAME_SERVER_EASY_ROOM_ADDRSS_IP_ "10.0.90.221"  
	#define _GAME_SERVER_EASY_ROOM_ADDRSS_PORT_ (16055)  

	#define _GAME_SERVER_NORMAL_ROOM_ADDRSS_IP_ "10.0.90.221"  
	#define _GAME_SERVER_NORMAL_ROOM_ADDRSS_PORT_ (18193)  

	#define _GAME_SERVER_VIP_ROOM_ADDRSS_IP_ "10.0.90.221"  
	#define _GAME_SERVER_VIP_ROOM_ADDRSS_PORT_ (18194)  

	#define _ROOM_SERVER_ADDRSS_IP_ "10.0.90.221"  
	#define _ROOM_SERVER_ADDRSS_PORT_ (26000) 

#else

	//外网 测试手游服务器

	#define _GAME_SERVER_COMPETE_ADDRSS_IP_ "61.152.126.133"  
	#define _GAME_SERVER_COMPETE_ADDRSS_PORT_ (18192)  

	#define _GAME_SERVER_EASY_ROOM_ADDRSS_IP_ "61.152.126.133"  
	#define _GAME_SERVER_EASY_ROOM_ADDRSS_PORT_ (16055)  

	#define _GAME_SERVER_NORMAL_ROOM_ADDRSS_IP_ "61.152.126.133"  
	#define _GAME_SERVER_NORMAL_ROOM_ADDRSS_PORT_ (18193)  

	#define _GAME_SERVER_VIP_ROOM_ADDRSS_IP_ "61.152.126.133"  
	#define _GAME_SERVER_VIP_ROOM_ADDRSS_PORT_ (18194)  

	#define _ROOM_SERVER_ADDRSS_IP_ "61.152.126.133"  
	#define _ROOM_SERVER_ADDRSS_PORT_ (26000) 

#endif

#define _TCP_CLIENT_SOCKET_LOOP_TICKET_ (1.0f / 33.0f)

// Game 参数设定 ========================================================================================

#define _GAME_ID_ (605)

// 设定 牌大小
#define	_CARD_WIDTH_ (141)
#define	_CARD_HEIGHT_ (173)


// Logo Layer 参数设定 ========================================================================================

// 定义 Logo Back 最小显示时间
#ifdef DEBUG

	#define _LOGO_LAYER_DISPLAY_TIME_ (1)

#else

	#define _LOGO_LAYER_DISPLAY_TIME_ (3)

#endif


#define ccpx(_x, _y)					(CC_POINT_PIXELS_TO_POINTS(Vec2(_x,_y)))
#define ccppx(_p)					(CC_POINT_PIXELS_TO_POINTS(_p))

#endif
