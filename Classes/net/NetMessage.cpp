#include "NetMessage.h"
#include "cocos2d.h"
USING_NS_CC;

namespace custom{
	NetMessage::NetMessage()
	{
		clear();
	}

	NetMessage::NetMessage(UINT mainId, UINT subId, UINT msgLen, char* pData, UINT clientId)
		:_mainId(mainId)
		, _subId(subId)
		, _msgLen(msgLen)
		, _clientId(clientId)
	{
		memset(&_data, 0, SOCKET_TCP_PACKET);
		memcpy(_data, pData, msgLen);
	}

	NetMessage::~NetMessage()
	{
	}

	void NetMessage::setMainID(UINT mainId)
	{
		_mainId = mainId;
	}

	void NetMessage::setSubId(UINT subId)
	{
		_subId = subId;
	}

	void NetMessage::setMsgLen(UINT msgLen)
	{
		_msgLen = msgLen;
	}

	UINT NetMessage::getMainID()
	{
		return _mainId;
	}

	UINT NetMessage::getSubId()
	{
		return _subId;
	}

	UINT NetMessage::getMsgLen()
	{
		return _msgLen;
	}

	BYTE NetMessage::getDataAtIndex(UINT index)
	{
		if (index < _msgLen)
		{
			return _data[index];
		}

		return '\0';
	}

	void NetMessage::setDataAtIndex(UINT index, BYTE chData)
	{
		_data[index] = chData;
	}

	void NetMessage::setClientId(UINT clientId)
	{
		_clientId = clientId;
	}

	UINT NetMessage::getClientId()
	{
		return _clientId;
	}

	void NetMessage::clear()
	{
		_mainId = 0;
		_subId = 0;
		_msgLen = 0;
		_clientId = -1;
		memset(&_data, 0, SOCKET_TCP_PACKET);
	}
}