#ifndef __MsgManager_h__
#define __MsgManager_h__

#include "../utils/GameBase.h"
#include "Packet.h"
#include "NetBase.h"
#include "cocos2d.h"
#include <iostream>
#include <queue>
#include <mutex>
using namespace std;
USING_NS_CC;

//负责协议加解密
class MsgManager : public Ref,  public ISvrNetEvent
{
private:
	MsgManager();
	virtual ~MsgManager();
    unsigned short SeedRandMap(unsigned short wSeed);
    unsigned char MapSendByte(unsigned char const cbData);
    BYTE MapRecvByte(unsigned char const cbData);
    BYTE m_cbSendRound;
    BYTE m_cbRecvRound;
    ULONG m_dwSendXorKey;
    ULONG m_dwRecvXorKey;
    unsigned m_dwSendPacketCount;
	void update(float ft);
	WORD EncryptBuffer(BYTE pcbDataBuffer[], WORD wDataSize, WORD wBufferSize);  //加密
	WORD CrevasseBuffer(BYTE pcbDataBuffer[], WORD wDataSize);  //解密
	bool MapEncrypt(CLPTSTR pszSourceData, CLPTSTR pszEncrypData, WORD wMaxCount);  //生成密文
	bool MapCrevasse(CLPTSTR pszEncrypData, CLPTSTR pszSourceData, WORD wMaxCount); //生成明文
	BYTE GetCheckCode(BYTE *PData, WORD Len);  //生成校验字段
	ULONG CompressData(LPBYTE pcbSourceData, ULONG lSourceSize, BYTE cbResultData[], ULONG lResultSize); //压缩数据
	ULONG UnCompressData(LPBYTE pcbSourceData, ULONG lSourceSize, BYTE cbResultData[], ULONG lResultSize); //解压数据
	void parseMessage(ClientTCP_Buffer *msgBuf);
public:
	static MsgManager *instance();
	void setParseCountPerFrame(UINT count);
	void sendMessage(UINT clientId, UINT mainCmdId, UINT subCmdId, const char* msg, UINT msgLen);
	void sendMessage(UINT clientId, UINT mainCmdId, UINT subCmdId);

	//ISvrNetEvent
	virtual void onGameMessage(unsigned int clientId, TCP_Buffer* msg);  //消息协议
	virtual void onNetClose(UINT clientId);               //网络断开
	virtual void onConnected(UINT clientId, bool bSuccess);     //网络连接结果

private:
	static MsgManager *_pInstance;
	queue<ClientTCP_Buffer*> _msgQueue;    //消息队列
	mutex _mutex;
	UINT _parsePerFrameNum;
};

#endif