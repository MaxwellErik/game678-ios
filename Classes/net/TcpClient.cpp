#include "TcpClient.h"
#include "Packet.h"
#include "base/ccUtils.h"
#include "cocos2d.h"
#include <iostream>
#include "MsgManager.h"
USING_NS_CC;

#ifdef PLATFORM_IOS
#include "IosTcpManager.h"
#endif

TcpClient::TcpClient(UINT clientId)
	:_recvBufLen(0)
	, _socketState(eSocketClosed)
	, _ip("")
	, _port(0)
	, _clientId(clientId)
	, _pNetEvent(nullptr)
	, _pthread(nullptr)
	, _bRecvMsg(false)
	, _pOdsocket(nullptr)
{
}

void TcpClient::connect(std::string ip, UINT port)
{
	_ip = ip;
	_port = port;
    _bRecvMsg = false;

#ifdef PLATFORM_IOS
    IosTcpManager::instance()->connectHost(ip, port, _clientId);
#else
	CC_SAFE_DELETE(_pthread);
	_pthread = new std::thread(&TcpClient::connectRun, this, _clientId);

	_pthread->detach();
#endif
}

TcpClient::~TcpClient()
{
	CC_SAFE_DELETE(_pOdsocket);
	CC_SAFE_DELETE(_pthread);
}

void TcpClient::setGameServer(ISvrNetEvent *pNetEvent)
{
	_pNetEvent = pNetEvent;
}

void TcpClient::sendMessage(TCP_Buffer *ptr)
{
#ifdef PLATFORM_IOS
	IosTcpManager::instance()->sendMessage((const char*)ptr, ptr->Head.TCPInfo.wPacketSize, _clientId);
#else
	if (_pOdsocket && _socketState == eSocketConnected)
	{
		int sendSize = _pOdsocket->Send((const char*)ptr, ptr->Head.TCPInfo.wPacketSize);
		if (sendSize < 0)
		{
			log("send message error sendSize:%d", sendSize);
			close();
		}
	}
#endif
}

void TcpClient::setHeartDelayTime(UINT delaytime)
{
}

void TcpClient::update()
{
#ifndef PLATFORM_IOS
	recvMessage();
#endif
    
    parseMessage();
}

void TcpClient::connectRun(UINT clientId)
{
	_pOdsocket = new ODSocket();

	_pOdsocket->Init();
	_pOdsocket->Create(AF_INET, SOCK_STREAM);
    
	_socketState = eSocketConnecting;
	if (_pOdsocket->Connect(_ip.c_str(), _port))
	{
		_bRecvMsg = true;
		_socketState = eSocketConnected;
    }
	else
	{
		_bRecvMsg = false;
		_socketState = eSocketClosed;
	}

	if (_pNetEvent)
	{
		_pNetEvent->onConnected(_clientId, _bRecvMsg);
	}
}

void TcpClient::close(bool bActive)
{
	log("active:%d", bActive);

	memset(_msgRecvBuf, 0, MAX_RECV_BUF_SIZE);
	_recvBufLen = 0;

	if (!_bRecvMsg)
	{
		return;
	}
	_bRecvMsg = false;

	if (!bActive && _pNetEvent)
	{
		_pNetEvent->onNetClose(_clientId);
	}
	if (_pOdsocket)
	{
		_pOdsocket->Close();
	}
	_pNetEvent = NULL;
    MsgManager::instance()->onNetClose(_clientId);
#ifdef PLATFORM_IOS
    IosTcpManager::instance()->closeConnect(_clientId);
#endif
}

void TcpClient::onNetClose()
{
	log("TcpClient::onNetClose");
    if (_pNetEvent) {
        _pNetEvent->onNetClose(_clientId);
    }
	_pNetEvent = nullptr;
}

void TcpClient::onConnect(bool bSuccess)
{
    _bRecvMsg = true;
    _socketState = eSocketConnected;
	    
    if (_pNetEvent)
    {
        _pNetEvent->onConnected(_clientId, bSuccess);
    }
}

void TcpClient::onRecvData(const char* pdata, int dataLen)
{
    _mutex.lock();
    memcpy(&_msgRecvBuf[_recvBufLen], pdata, dataLen);
    _recvBufLen += dataLen;
    _mutex.unlock();
}

void TcpClient::recvMessage()
{
	if (_pOdsocket && _bRecvMsg)
	{
        char buf[MAX_RECV_BUF_SIZE];
		int ret = _pOdsocket->Recv(buf, MAX_RECV_BUF_SIZE);
        if (ret <= 0)
        {
			log("recv message error size:%d", ret);
            close();
            _socketState = eSocketClosed;
            return;
        }
		if (ret != INT_MAX)
		{
			_mutex.lock();
			memmove(_msgRecvBuf + _recvBufLen, buf, ret);
			_recvBufLen += ret;
			_mutex.unlock();
		}
	}
}

void TcpClient::parseMessage()   
{
    _mutex.lock();
    int offset = 0;
    while (offset + sizeof(TCP_Head) <= _recvBufLen) {
        auto packet = (TCP_Buffer*)((char*)_msgRecvBuf + offset);
        auto packetSize = packet->Head.TCPInfo.wPacketSize;
        if (offset + packetSize > _recvBufLen) {
            break;
        }
        
        if (_pNetEvent) {
            _pNetEvent->onGameMessage(_clientId, packet);
        }
        offset += packetSize;
    }
	
	memmove(_msgRecvBuf, &_msgRecvBuf[offset], _recvBufLen - offset);
	_recvBufLen -= offset;
	
	_mutex.unlock();
}
