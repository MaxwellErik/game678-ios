#include "TcpManager.h"
#include "MsgManager.h"
USING_NS_CC;

TcpManager* TcpManager::_pInstance = nullptr;

TcpManager::TcpManager()
{
	running = true;
	_pthread = new std::thread(&TcpManager::run, this, 1001);
	_pthread->detach();
}

TcpManager::~TcpManager()
{
	running = false;
}

void TcpManager::run(UINT clientId)
{
	while (running)
	{
		_mutex.lock();
		map<int, TcpClient*>::iterator it = _clients.begin();
		while (it != _clients.end())
		{
			if (it->second)
			{
				it->second->update();
			}
			it++;
		}

		_mutex.unlock();
		std::this_thread::sleep_for(std::chrono::milliseconds(1));
	}	
}

TcpManager *TcpManager::instance()
{
	if (!_pInstance)
	{
		_pInstance = new TcpManager();
	}

	return _pInstance;
}

TcpClient* TcpManager::getClient(int clientId)
{
	return _clients[clientId];
}

TcpClient* TcpManager::addClient(int clientId)
{
	//auto pClient = getClient(clientId);
	TcpClient* pClient = nullptr;
	if (!pClient)
	{
		pClient = new TcpClient(clientId);
		pClient->setGameServer(MsgManager::instance());
		//_mutex.lock();
		_clients[clientId] = pClient;
		//_mutex.unlock();
	}
	return pClient;
}

void TcpManager::delClient(int clientId)
{
//_delClients.push(clientId);
	log("TcpManager::delClient :%d", clientId);
	if (_clients[clientId])
	{
		_clients[clientId]->close(true);
	}
}

void TcpManager::closeAllNet()
{
	map<int, TcpClient*>::iterator it = _clients.begin();
	while (it != _clients.end())
	{
		if (it->second)
		{
			it->second->close(false);
		}
		it++;
	}
}
