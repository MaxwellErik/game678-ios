#ifndef __TcpManager_h__
#define __TcpManager_h__


#include "TcpClient.h"
#include "cocos2d.h"
#include "../utils/GameBase.h"
#include <iostream>
#include <thread>
#include <mutex>

class TcpManager
{
private:
	TcpManager();
	virtual ~TcpManager();

	void run(UINT clientId);
public:
	static TcpManager *instance();
	TcpClient* getClient(int clientId);
	TcpClient* addClient(int clientId);
	void delClient(int clientId);
	void closeAllNet();
private:
	static TcpManager *_pInstance;

	map<int, TcpClient*> _clients;
	queue<int> _delClients;
	std::thread *_pthread;
	std::mutex _mutex;
	bool running;
};

#endif