#ifndef __NetBase_h__
#define __NetBase_h__

#include "Packet.h"
#include <iostream>
using namespace std;

class ISvrNetEvent
{
public:
	virtual void onGameMessage(unsigned int clientId, TCP_Buffer* msg) = 0;
	virtual void onNetClose(UINT clientId) = 0;
	virtual void onConnected(UINT clientId, bool bSuccess) = 0;
};

#endif