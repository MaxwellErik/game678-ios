﻿#include "MsgManager.h"
#include "TcpManager.h"
#include "PlatUtil.h"
#include "Packet.h"
#include "NetMessage.h"
#include "zlib.h"
#include "cocos2d.h"

using namespace cocos2d;


#define SEND_DATA_KIND			DK_MAPPED | DK_COMPRESS			//发送数据类型

MsgManager* MsgManager::_pInstance = nullptr;
MsgManager::MsgManager()
{
	_parsePerFrameNum = 10;
	Director::getInstance()->getScheduler()->schedule(schedule_selector(MsgManager::update), this, 0, false);
    m_cbSendRound = 0;
    m_cbRecvRound = 0;
    m_dwSendPacketCount = 0;
    m_dwRecvXorKey = 0;
    m_dwSendXorKey = 0;
}

MsgManager::~MsgManager()
{
	Director::getInstance()->getScheduler()->unscheduleAllForTarget(this);
}

void MsgManager::update(float ft)
{
	_mutex.lock();
	int index = 0;
	while (index < _parsePerFrameNum)
	{
		index++;
		if (!_msgQueue.empty())
		{
			ClientTCP_Buffer *msg = _msgQueue.front();
			_msgQueue.pop();
			_mutex.unlock();

			parseMessage(msg);
			delete msg;
			_mutex.lock();
		}
		else
			break;
	}
	
	_mutex.unlock();
}

void MsgManager::parseMessage(ClientTCP_Buffer *msgBuf)
{
	if (msgBuf->type == BUFFER_TYPE_NET_CLOSED)
	{
		PlatUtil::onNetClosed(msgBuf->clienId);
	}
	else if (msgBuf->type == BUFFER_TYPE_CONNECTED_OK)
	{
		PlatUtil::onNetConnected(msgBuf->clienId, true);
	}
	else if (msgBuf->type == BUFFER_TYPE_CONNECTED_FAILED)
	{
		PlatUtil::onNetConnected(msgBuf->clienId, false);
	}
	else
	{
		UINT crevasseLen = CrevasseBuffer((BYTE*)&msgBuf->tcpBuffer, msgBuf->tcpBuffer.Head.TCPInfo.wPacketSize);
		if (crevasseLen < 0)
		{
			return;
		}

		if (crevasseLen - sizeof(TCP_Head) >= 0 && crevasseLen - sizeof(TCP_Head) <= SOCKET_TCP_PACKET)
		{
			NetMessage netMsg(msgBuf->tcpBuffer.Head.CommandInfo.wMainCmdID,
				msgBuf->tcpBuffer.Head.CommandInfo.wSubCmdID,
				crevasseLen - sizeof(TCP_Head),
				(char*)&msgBuf->tcpBuffer.cbBuffer,
				msgBuf->clienId);

			PlatUtil::onMessage(msgBuf->tcpBuffer.Head.CommandInfo.wMainCmdID, msgBuf->tcpBuffer.Head.CommandInfo.wSubCmdID, netMsg);
		}		
	}	
}

MsgManager *MsgManager::instance()
{
	if (!_pInstance)
	{
		_pInstance = new MsgManager();
	}
	return _pInstance;
}

void MsgManager::setParseCountPerFrame(UINT count)
{
	_parsePerFrameNum = count;
}

void MsgManager::sendMessage(UINT clientId, UINT mainCmdId, UINT subCmdId)
{
	//构造数据
	TCP_Buffer *ptr = new TCP_Buffer();
	ptr->Head.CommandInfo.wMainCmdID = mainCmdId;
    ptr->Head.CommandInfo.wSubCmdID = subCmdId;
    log("wMainID:%d wSubID:%d", mainCmdId, subCmdId);

	//加密数据
	WORD wSendSize = EncryptBuffer((BYTE*)ptr, sizeof(TCP_Head), sizeof(TCP_Buffer));
	if (!wSendSize)
		return;

	auto pClient = TcpManager::instance()->getClient(clientId);
	if (pClient)
	{
		pClient->sendMessage(ptr);
	}
	delete ptr;
}

void MsgManager::sendMessage(UINT clientId, UINT mainCmdId, UINT subCmdId, const char* msg, UINT msgLen)
{
	//效验数据
	if (!msg || msgLen > SOCKET_TCP_PACKET)
		return;

	//构造数据
	TCP_Buffer *ptr = new TCP_Buffer();
	ptr->Head.CommandInfo.wMainCmdID = mainCmdId;
	ptr->Head.CommandInfo.wSubCmdID = subCmdId;
	memcpy(ptr->cbBuffer, msg, msgLen);
    
    WORD wSendSize = EncryptBuffer((BYTE*)ptr, sizeof(TCP_Head) + msgLen, sizeof(TCP_Buffer));
    if (!wSendSize)
        return;
    
	auto pClient = TcpManager::instance()->getClient(clientId);
	if (pClient)
	{
		pClient->sendMessage(ptr);
	}
	delete ptr;
}

void MsgManager::onGameMessage(unsigned int clientId, TCP_Buffer* msg)  //消息协议
{

	ClientTCP_Buffer *recvMsg = new ClientTCP_Buffer();
	memcpy(&recvMsg->tcpBuffer, msg, sizeof(ClientTCP_Buffer));
	recvMsg->type = BUFFER_TYPE_SOCKET;
	recvMsg->clienId = clientId;
    
	_mutex.lock();
	_msgQueue.push(recvMsg);
	_mutex.unlock();
}

void MsgManager::onNetClose(UINT clientId)               //网络断开
{
	ClientTCP_Buffer *recvMsg = new ClientTCP_Buffer();
	recvMsg->type = BUFFER_TYPE_NET_CLOSED;
	recvMsg->clienId = clientId;

	_mutex.lock();
	//断开网络，清除之前收到的协议，不用处理，没有意义了
	queue<ClientTCP_Buffer*> msgqueue;    //消息队列

	while (!_msgQueue.empty())
	{
		ClientTCP_Buffer *msg = _msgQueue.front();
		_msgQueue.pop();
		if (msg->clienId != clientId)
		{
			msgqueue.push(msg);
		}
		else
		{
			delete msg;
		}
	}
	_msgQueue = msgqueue;
	_msgQueue.push(recvMsg);
	_mutex.unlock();
    
    m_cbSendRound = 0;
    m_cbRecvRound = 0;
    m_dwSendPacketCount = 0;
    m_dwRecvXorKey = 0;
    m_dwSendXorKey = 0;
}

void MsgManager::onConnected(UINT clientId, bool bSuccess)     //网络连接结果
{
	ClientTCP_Buffer *recvMsg = new ClientTCP_Buffer();
	recvMsg->type = bSuccess ? BUFFER_TYPE_CONNECTED_OK : BUFFER_TYPE_CONNECTED_FAILED;
	recvMsg->clienId = clientId;

	_mutex.lock();
	_msgQueue.push(recvMsg);
	_mutex.unlock();
}



//随机映射
unsigned short MsgManager::SeedRandMap(unsigned short wSeed)
{
    DWORD dwHold = wSeed;
    return (unsigned short)((dwHold = dwHold * 241103L + 2533101L) >> 16);
}

//映射发送数据
unsigned char MsgManager::MapSendByte(unsigned char const cbData)
{
    unsigned char cbMap = g_SendByteMap[(unsigned char)(cbData + m_cbSendRound)];
    m_cbSendRound += 3;
    return cbMap;
}

//映射接收数据
BYTE MsgManager::MapRecvByte(unsigned char const cbData)
{
    unsigned char cbMap = g_RecvByteMap[cbData] - m_cbRecvRound;
    m_cbRecvRound += 3;
    return cbMap;
}

//加密数据
WORD MsgManager::EncryptBuffer(BYTE pcbDataBuffer[], WORD wDataSize, WORD wBufferSize)
{
    
    WORD wEncryptSize=wDataSize-sizeof(CMD_Command),wSnapCount=0;
    if ((wEncryptSize%sizeof(unsigned int))!=0)
    {
        wSnapCount=sizeof(uint32_t)-wEncryptSize%sizeof(uint32_t);
        memset(pcbDataBuffer+sizeof(CMD_Info)+wEncryptSize,0,wSnapCount);
    }
    
    
    BYTE cbCheckCode=0;
    for (WORD i=sizeof(CMD_Info);i<wDataSize;i++)
    {
        cbCheckCode+=pcbDataBuffer[i];
        pcbDataBuffer[i]=MapSendByte(pcbDataBuffer[i]);
    }
    
    CMD_Head * pHead=(CMD_Head *)pcbDataBuffer;
    pHead->CmdInfo.cbCheckCode=~cbCheckCode+1;
    pHead->CmdInfo.wPacketSize=wDataSize;
    pHead->CmdInfo.cbVersion=SOCKET_VER;
    
    uint32_t dwXorKey=m_dwSendXorKey;
    if (m_dwSendPacketCount==0)
    {
        dwXorKey = 0xaa9d3159;
        m_dwSendXorKey=dwXorKey;
        m_dwRecvXorKey=dwXorKey;
    }
    
    
    WORD * pwSeed=(WORD *)(pcbDataBuffer+sizeof(CMD_Info));
    uint32_t * pdwXor=(uint32_t *)(pcbDataBuffer+sizeof(CMD_Info));
    WORD wEncrypCount=(wEncryptSize+wSnapCount)/sizeof(uint32_t);
    for (int i=0;i<wEncrypCount;i++)
    {
        *pdwXor++^=dwXorKey;
        dwXorKey=SeedRandMap(*pwSeed++);
        dwXorKey|=((uint32_t)SeedRandMap(*pwSeed++))<<16;
        dwXorKey^=g_dwPacketKey;
    }
    
    
    m_dwSendPacketCount++;
    m_dwSendXorKey=dwXorKey;
    
    return wDataSize;}

//解密数据
WORD MsgManager::CrevasseBuffer(BYTE pcbDataBuffer[], WORD wDataSize)
{
    WORD wSnapCount=0;
    if ((wDataSize%sizeof(uint32_t))!=0)
    {
        wSnapCount=sizeof(uint32_t)-wDataSize%sizeof(uint32_t);
        memset(pcbDataBuffer+wDataSize,0,wSnapCount);
    }
    
    
    uint32_t dwXorKey=m_dwRecvXorKey;
    uint32_t * pdwXor=(uint32_t *)(pcbDataBuffer+sizeof(CMD_Info));
    WORD  * pwSeed=(WORD *)(pcbDataBuffer+sizeof(CMD_Info));
    WORD wEncrypCount=(wDataSize+wSnapCount-sizeof(CMD_Info))/4;
    for (WORD i=0;i<wEncrypCount;i++)
    {
        if ((i==(wEncrypCount-1))&&(wSnapCount>0))
        {
            BYTE * pcbKey=((BYTE *)&m_dwRecvXorKey)+sizeof(uint32_t)-wSnapCount;
            copyMemory(pcbDataBuffer+wDataSize,pcbKey,wSnapCount);
        }
        dwXorKey=SeedRandMap(*pwSeed++);
        dwXorKey|=((uint32_t)SeedRandMap(*pwSeed++))<<16;
        dwXorKey^=g_dwPacketKey;
        *pdwXor++^=m_dwRecvXorKey;
        m_dwRecvXorKey=dwXorKey;
    }
    
    
    CMD_Head * pHead=(CMD_Head *)pcbDataBuffer;
    BYTE cbCheckCode=pHead->CmdInfo.cbCheckCode;
    for (int i=sizeof(CMD_Info);i<wDataSize;i++)
    {
        pcbDataBuffer[i]=MapRecvByte(pcbDataBuffer[i]);
        cbCheckCode+=pcbDataBuffer[i];
    }
    
    return wDataSize;
}

//生成密文
bool MsgManager::MapEncrypt(CLPTSTR pszSourceData, CLPTSTR pszEncrypData, WORD wMaxCount)
{
	//效验参数
	//ASSERT(wMaxCount>lstrlen(pszEncrypData));
	//ASSERT((pszEncrypData != NULL) && (pszSourceData != NULL));

	//变量定义
	UINT nLength = wMaxCount;
	BYTE * pcbEncrypData = (BYTE *)pszEncrypData;
	BYTE * pcbSourceData = (BYTE *)pszSourceData;

	//解密数据
	for (UINT i = 0; i < nLength; i++)
	{
		BYTE cbIndex = pcbSourceData[i];
		pcbEncrypData[i] = g_SendByteMap[cbIndex];
	}

	//设置结果
	//pszEncrypData[nLength]=0;

	return true;
}


//解开密文
bool MsgManager::MapCrevasse(CLPTSTR pszEncrypData, CLPTSTR pszSourceData, WORD wMaxCount)
{
	//效验参数
	//ASSERT(wMaxCount>lstrlen(pszEncrypData));
	CCAssert((pszEncrypData != NULL) && (pszSourceData != NULL), "MsgManager::MapCrevasse (pszEncrypData != NULL) && (pszSourceData != NULL)");

	//变量定义
	UINT nLength = wMaxCount;
	BYTE * pcbEncrypData = (BYTE *)pszEncrypData;
	BYTE * pcbSourceData = (BYTE *)pszSourceData;

	//解密数据
	for (UINT i = 0; i < nLength; i++)
	{
		BYTE cbIndex = pcbEncrypData[i];
		pcbSourceData[i] = g_RecvByteMap[cbIndex];
	}
	
	return true;
}

//生成效验字段
BYTE MsgManager::GetCheckCode(BYTE *PData, WORD Len)
{
	return 0;
}

ULONG MsgManager::CompressData(LPBYTE pcbSourceData, ULONG lSourceSize, BYTE cbResultData[], ULONG lResultSize) //压缩数据
{
	//压缩数据
	if (compress(cbResultData, &lResultSize, pcbSourceData, lSourceSize) == 0L)
	{
		return lResultSize;
	}

	return 0L;
}

ULONG MsgManager::UnCompressData(LPBYTE pcbSourceData, ULONG lSourceSize, BYTE cbResultData[], ULONG lResultSize) //解压数据
{
	//解压数据
	if (uncompress(cbResultData, &lResultSize, pcbSourceData, lSourceSize) == 0L)
	{
		return lResultSize;
	}

	return 0L;
}
