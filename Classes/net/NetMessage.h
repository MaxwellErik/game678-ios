#ifndef __NetMessage_h__
#define __NetMessage_h__

#include "../utils/GameBase.h"
#include "Packet.h"

namespace custom{

	class NetMessage
	{
	public:
		UINT _mainId;
		UINT _subId;
		UINT _msgLen;
		UINT _clientId;

		BYTE _data[SOCKET_TCP_PACKET];

		NetMessage();
		NetMessage(UINT mainId, UINT subId, UINT msgLen, char* pData, UINT clientId);
		virtual ~NetMessage();

		void setMainID(UINT mainId);
		void setSubId(UINT subId);
		void setMsgLen(UINT msgLen);

		UINT getMainID();
		UINT getSubId();
		UINT getMsgLen();
		BYTE getDataAtIndex(UINT index);
		void setDataAtIndex(UINT index, BYTE chData);
		void setClientId(UINT clientId);
		UINT getClientId();
		void clear();
	};
}

#endif