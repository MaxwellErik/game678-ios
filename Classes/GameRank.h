#ifndef _GAMERANK_H_
#define _GAMERANK_H_

#include "GameScene.h"
#include "cocos-ext.h"
#include "ui/UIListview.h"
USING_NS_CC_EXT;

class GameRank : public GameLayer
{
public:
	virtual bool init();  
	static GameRank *create(GameScene *pGameScene);
	virtual ~GameRank();
	GameRank(GameScene *pGameScene);

    Menu* CreateButton(std::string szBtName ,const Vec2 &p , int tag);
    void callbackBt( Ref *pSender);
    void setRankData(CMD_RankingQuery_Result* rankData);
    void menuCallback(Ref* pSender, ui::Widget::TouchEventType type);
    void showUserInfo(int rank);
private:
    ui::ListView*       m_listView;
    Sprite*             m_UserInfoBg;
    Menu*               m_ColorBg;
    map<int, tagRankingInfo> m_rankInfo;
private:
	virtual void onEnter();
	virtual void onExit();
	virtual bool onTouchBegan(Touch *pTouch, Event *pEvent){return true;}
	virtual void onTouchMoved(Touch *pTouch, Event *pEvent){}
	virtual void onTouchEnded(Touch *pTouch, Event *pEvent){}
	virtual void onTouchCancelled(Touch *pTouch, Event *pEvent){}
};
#endif
