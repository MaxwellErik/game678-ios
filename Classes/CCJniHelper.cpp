﻿#include "CCJniHelper.h"

#if(CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
#include <jni.h>
#include "platform/android/jni/JniHelper.h"
#elif(CC_TARGET_PLATFORM == CC_PLATFORM_IOS)

#endif

#include "JniSink.h"

#define CLASS_NAME "com/game/jni/JniHelper"

using namespace cocos2d;

extern "C"
{
	//C++访问JAVA的接口
	void callJavaCommand(const int command, const char *szParam)
	{
#if(CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
		JniMethodInfo t;
		if(JniHelper::getStaticMethodInfo(t, CLASS_NAME, "callJavaCommand", "(ILjava/lang/String;)V"))
		{
			jstring jParam = t.env->NewStringUTF(szParam);
			jint jCommond = (jint)command;
			t.env->CallStaticVoidMethod(t.classID, t.methodID, jCommond, jParam);
			t.env->DeleteLocalRef(jParam);
		}
#endif
	}

#if(CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)

	//JAVA访问C++的接口
	void Java_com_game_jni_JniHelper_callCCommand(JNIEnv *env, jobject thiz , jint jCommand, jstring jParam)
	{
		const char *szParam = env->GetStringUTFChars(jParam, NULL);

		JniSink::share()->javaCallback(jCommand , szParam);

		env->ReleaseStringUTFChars(jParam, szParam);
	}

#endif

}
