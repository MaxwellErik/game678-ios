#include "GameKind.h"
#include "GameLayerMove.h"
#include "LobbySocketSink.h"
#include "ClientSocketSink.h"
#include "CCLuaEngine.h"
#include "CCLuaStack.h"
#include "Encrypt.h"
#include "Convert.h"
#include "DownloadGameResource.hpp"

#define GameOx2		100
#define GameOx6		166
#define GameOx4		102
#define GameSH		103
#define GameSG		104
#define GameDDZ     105
#define GameBRNN	106
#define Game30S		107
#define GameWZLZ	108
#define GameWKNH	109
#define GameSDB		110
#define GameHHSW	111
#define GameQBSQ    112
#define GameWZMJ    113

static bool firstEnterFish = true;

GameKind::GameKind(GameScene *pGameScene):GameLayer(pGameScene)
{
    m_MoveText          = nullptr;
    m_runbg             = nullptr;
    m_cNode             = NULL;
    m_RoomCount         = 0;
    m_GameTag           = 0;
    m_isBoardcasting    = false;
}

GameKind::~GameKind()
{
    unscheduleUpdate();
}

void GameKind::OnCallMove()
{
    GameLayerMove::sharedGameLayerMoveSink()->m_BackBtnEnable = true;
    setVisible(false);
}

void GameKind::callbackBt(Ref *pSender)
{
      if (g_GlobalUnits.m_bIsMePlaying)
          return;
    
	Node *pNode = (Node *)pSender;
	int _index = pNode->getTag();
  
	//进入房间
	if (_index >= m_EnterRoomBtnTga)
	{
		LoadLayer::create(m_pGameScene);
		GameLayerMove::sharedGameLayerMoveSink()->AddEnterRoomTime();
		SoundUtil::sharedEngine()->playEffect("buttonMusic");
		int _id = _index - m_EnterRoomBtnTga;
		LobbySocketSink::sharedSocketSink()->Jionroom(m_GameRoomList[_id].wServerPort, m_GameRoomList[_id].wKindID);

		GameLayerMove::sharedGameLayerMoveSink()->SetGameID(m_GameRoomList[_id].wKindID);

		char gamename[256];
		sprintf(gamename,"%s", gbk_utf8(m_GameRoomList[_id].szServerName).c_str());
		GameLayerMove::sharedGameLayerMoveSink()->SetGameName(gamename,GetGameNameForKindId(m_GameRoomList[_id].wKindID));
		return;
	}

	if (m_BackBtnTga == _index) //返回按钮
	{
        m_tableView->setVisible(false);
        auto child = getChildByTag(m_RoomTitleBackTga);
        if (child) child->removeFromParent();
		SoundUtil::sharedEngine()->playEffect((char*)("buttonMusic"));
	}
}

void GameKind::AddText()
{
   if (g_GlobalUnits.m_SysMsg.size() <= 0 || m_isBoardcasting)
        return;
    
    scheduleUpdate();
    m_runbg->setVisible(true);
    m_isBoardcasting = true;
    
    std::string szTempTitle = g_GlobalUnits.m_SysMsg[0].szText;
    m_MoveText = Label::createWithSystemFont(szTempTitle.c_str(),_GAME_FONT_NAME_1_,46);
    m_MoveText->setColor(_GAME_FONT_COLOR_5_);
    m_MoveText->setPosition(Vec2(m_clippingSize.width+m_MoveText->getContentSize().width/2, 32));
    m_cNode->addChild(m_MoveText);
}

void GameKind::enterRoomList()
{
    switch (m_GameTag)
    {
        case GameDDZ:
        {
            SoundUtil::sharedEngine()->playEffect("buttonMusic");
            OpenGameList(Dating_GameKind_DDZ);
            break;
        }
        case GameOx2:
        {
            SoundUtil::sharedEngine()->playEffect("buttonMusic");
            OpenGameList(Dating_GameKind_Ox2);
            break;
        }
        case GameOx4:
        {
            SoundUtil::sharedEngine()->playEffect("buttonMusic");
            OpenGameList(Dating_GameKind_Ox4);
            break;
        }
        case GameOx6:
        {
            SoundUtil::sharedEngine()->playEffect("buttonMusic");
            OpenGameList(Dating_GameKind_Ox6);
            break;
        }
        case GameSH:
        {
            SoundUtil::sharedEngine()->playEffect("buttonMusic");
            OpenGameList(Dating_GameKind_SH);
            break;
        }
        case GameSG:
        {
            SoundUtil::sharedEngine()->playEffect("buttonMusic");
            OpenGameList(9);
            break;;
        }
        case GameBRNN:
        {
            SoundUtil::sharedEngine()->playEffect("buttonMusic");
            OpenGameList(Dating_GameKind_BRNN);
            break;
        }
        case Game30S:
        {
            SoundUtil::sharedEngine()->playEffect("buttonMusic");
            OpenGameList(Dating_GameKind_30S);
            break;
        }
        case GameWKNH:
        {
            SoundUtil::sharedEngine()->playEffect("buttonMusic");
            auto stack = LuaEngine::getInstance()->getLuaStack();
            if (firstEnterFish)
            {
                stack->executeString("require 'appentry'");
                firstEnterFish = false;
            }
            else
            {
                stack->executeString("require 'fishEnter'");
                lua_getglobal(stack->getLuaState(), "enterFish");
                lua_pcall(stack->getLuaState(), 0, 0, 0);
                lua_pop(stack->getLuaState(), 1);
            }
            
            break;
        }
        case GameSDB:
        {
            SoundUtil::sharedEngine()->playEffect("buttonMusic");
            OpenGameList(Dating_GameKind_SDB);
            break;
        }
        case GameHHSW:
        {
            SoundUtil::sharedEngine()->playEffect("buttonMusic");
            OpenGameList(Dating_GameKind_HHSW);
            break;
        }
        case GameQBSQ:
        {
            SoundUtil::sharedEngine()->playEffect("buttonMusic");
            OpenGameList(Dating_GameKind_QBSQ);
            break;
        }
        case GameWZMJ:
        {
            SoundUtil::sharedEngine()->playEffect("buttonMusic");
            OpenGameList(Dating_GameKind_WZMJ);
            break;
        }
        case GameWZLZ:
        {
            SoundUtil::sharedEngine()->playEffect("buttonMusic");
            OpenGameList(Dating_GameKind_WZLZ);
            break;
        }
    }
}

void GameKind::menuCallback(Ref* pSender, ui::Widget::TouchEventType type)
{
    updatePoint();
    if (m_tableView->isVisible())
    {
        return;
    }
    
    bool flag = false;
    ui::Button *btn = (ui::Button *)pSender;
    if (type == ui::Widget::TouchEventType::BEGAN)
    {
        btn->setScale(1.03f);
    }
    else if (type == ui::Widget::TouchEventType::MOVED)
    {
        btn->setScale(1.0f);
        flag = true;
    }
    else if (type == ui::Widget::TouchEventType::ENDED)
    {
        btn->setScale(1.0f);
        if (!flag)
        {
            m_GameTag = btn->getTag();

            auto loadResouce = DownLoadGameResource::create();
            addChild(loadResouce);
            loadResouce->setFileName(GetGameResourceFileName());
        }
    }
    else if (type == ui::Widget::TouchEventType::CANCELED)
    {
        btn->setScale(1.0f);
    }
}

//添加游戏类型
int GameKind::AddGame(Vec2 pos, tagGameKind GameKind, ui::Layout* page)
{
    string gameBg = "";
    int gameKind = 0;
    switch (GameKind.wKindID) {
        case Dating_GameKind_DDZ: //斗地主
        {
            Node* temp = getChildByTag(GameDDZ);
            if(temp != NULL) return 0;
            
            gameBg = "bg_ddz.png";
            gameKind = GameDDZ;
            break;
        }
        case Dating_GameKind_SH://梭哈
        {
            Node* temp = getChildByTag(GameSH);
            if(temp != NULL) return 0;
            
            gameBg = "bg_sh.png";
            gameKind = GameSH;
            break;
        }
        case Dating_GameKind_Ox2://二人牛牛
        {
            Node* temp = getChildByTag(GameOx2);
            if(temp != NULL) return 0;
            
            gameBg = "bg_ernn.png";
            gameKind = GameOx2;
            break;
        }
        case Dating_GameKind_Ox4: //四人牛牛
        {
            Node* temp = getChildByTag(GameOx4);
            if(temp != NULL) return 0;
            
            gameBg = "bg_srnn.png";
            gameKind = GameOx4;
            break;
        }
        case Dating_GameKind_Ox6:  //通比牛牛
        {
            Node* temp = getChildByTag(GameOx6);
            if(temp != NULL) return 0;
            
            gameBg = "bg_tbnn.png";
            gameKind = GameOx6;
            break;
        }
        case Dating_GameKind_BRNN://多人牛牛
        {
            Node* temp = getChildByTag(GameBRNN);
            if(temp != NULL) return 0;
            
            gameBg = "bg_brnn.png";
            gameKind = GameBRNN;
            break;
        }
        case Dating_GameKind_30S://30秒
        {
            Node* temp = getChildByTag(Game30S);
            if(temp != NULL) return 0;
            
            gameBg = "bg_30s.png";
            gameKind = Game30S;
            break;
        }
        case Dating_GameKind_WKNH: //悟空
        {
            Node* temp = getChildByTag(GameWKNH);
            if(temp != NULL) return 0;
            
            gameBg = "bg_wknh.png";
            gameKind = GameWKNH;
            break;
        }
        case Dating_GameKind_SDB://十点半
        {
            Node* temp = getChildByTag(GameSDB);
            if(temp != NULL) return 0;
            
            gameBg = "bg_brsdb.png";
            gameKind = GameSDB;
            break;
        }
        case Dating_GameKind_HHSW: //虎虎生威
        {
            Node* temp = getChildByTag(GameHHSW);
            if(temp != NULL) return 0;
            
            gameBg = "bg_hhsw.png";
            gameKind = GameHHSW;
            break;
        }
        case Dating_GameKind_QBSQ: //千变双扣
        {
            Node* temp = getChildByTag(GameQBSQ);
            if(temp != NULL) return 0;
            
            gameBg = "bg_qbsk.png";
            gameKind = GameQBSQ;
            break;
        }
        case Dating_GameKind_WZMJ: //温州麻将
        {
            Node* temp = getChildByTag(GameWZMJ);
            if(temp != NULL) return 0;
            
            gameBg = "bg_wzmj.png";
            gameKind = GameWZMJ;
            break;
        }
        case Dating_GameKind_WZLZ: //温州两张
        {
            Node* temp = getChildByTag(GameWZLZ);
            if(temp != NULL) return 0;
    
            gameBg = "bg_wzlz.png";
            gameKind = GameWZLZ;
            break;
        }
        default:
            log("not found game with kindId:%d", GameKind.wKindID);
            return 0;
    }

    
    if ("" != gameBg)
    {
        gameBg = "DaTing/" + gameBg;
        ui::Button * pNode = ui::Button::create(gameBg,gameBg);
        pNode->addTouchEventListener(CC_CALLBACK_2(GameKind::menuCallback,this));
        pNode->setTag(gameKind);
        pNode->setPosition(pos);
        page->addChild(pNode);
        return 1;
    }

    return 0;
}

void GameKind::addGameToPage()
{
    m_pageView->removeAllPages();
    for (int i = 0; i < 10; i++)
    {
        auto child = getChildByTag(m_pointTag[i]);
        if (child) child->removeFromParent();
    }
    
	int count = 0;
    int pageCount = 0;
	bool pageFull = true;
    ui::Layout *page = nullptr;
	for(int i = 0; i < g_GlobalUnits.m_ServerListManager.m_vcKind.size(); i++)
	{
        if (pageFull)
        {
            page = ui::Layout::create();
            m_pageView->addPage(page);
            pageFull = false;
            pageCount++;
        }
        
		Vec2 pt = Vec2(280+(count%4)*450, 340);
		int _value = AddGame(pt, g_GlobalUnits.m_ServerListManager.m_vcKind[i], page);
		if (_value == 1)
		{
			count++;
			if(count == 4)
			{
				count = 0;
                pageFull = true;
			}
		}
	}

    auto lastPage = m_pageView->getPage(pageCount-1);
    auto itemCnt = lastPage->getChildrenCount();
    if ( 0 == itemCnt )
    {
        m_pageView->removePageAtIndex(--pageCount);
    }
    
    for (int i = 0; i < pageCount; i++)
    {
        Vec2 pos = Vec2(_STANDARD_SCREEN_CENTER_.x + (i - pageCount/2) * 80, 240);
        Sprite* point = Sprite::createWithSpriteFrameName("point_normal.png");
        point->setTag(m_pointTag[i]);
        point->setPosition(pos);
        addChild(point);
    }
    
    m_curPage = m_pageView->getCurPageIndex();
    Sprite* point = (Sprite*)getChildByTag(m_pointTag[m_curPage]);
    if (point)
    {
        point->setSpriteFrame("point_cur.png");
    }
}

bool GameKind::init()
{
	if ( !Layer::init() )
	{
		return false;
	}
    setLocalZOrder(1);
	m_RoomTitleBackTga = 2;
	m_EnterRoomBtnTga = 1000;
	m_BackBtnTga = 3;
	m_MsgTga = 4;
    
    for (int i = 0; i < 10; i++)
        m_pointTag[i] = 3000+i;
    
	setTouchEnabled(true);

    //滚动字幕
    m_runbg = Sprite::createWithSpriteFrameName("gonggaodi.png");
    m_runbg->setPosition(Vec2(960, 880));
    addChild(m_runbg);
    m_runbg->setVisible(false);
    
    Sprite* laba = Sprite::createWithSpriteFrameName("laba.png");
    laba->setPosition(Vec2(200, 31));
    m_runbg->addChild(laba);
    
    m_clippingSize = Size(820, 64);
    auto draw = DrawNode::create();
    draw->drawRect(Vec2::ZERO, Vec2(0,m_clippingSize.height),Vec2(m_clippingSize.width,m_clippingSize.height), Vec2(m_clippingSize.width,0), Color4F(255,255,255,255));
    m_cNode = ClippingNode::create();
    m_cNode->setInverted(false);
    m_cNode->setStencil(draw);
    m_cNode->setPosition(Vec2(240,0));
    m_runbg->addChild(m_cNode);
    
    m_pageView = ui::PageView::create();
    m_pageView->setContentSize(Size(1920, 670));
    m_pageView->setPosition(Vec2(0 , 160));
    m_pageView->setSwallowTouches(false);
    m_pageView->setCustomScrollThreshold(100.0);
    addChild(m_pageView);
    
	//创建一个数组
	m_Arr = __Array::create();
	m_Arr->retain();  
    Size winSize = Size(1735, 760);
	m_tableView = TableView::create(this, winSize);
    m_tableView->setDirection(ScrollView::Direction::VERTICAL);
    m_tableView->setVerticalFillOrder(TableView::VerticalFillOrder::TOP_DOWN);
	m_tableView->setPosition(Vec2(95,100));
	m_tableView->setDelegate(this);
	addChild(m_tableView,100);
	m_tableView->setVisible(false);
 
    auto listener = EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(true);
    listener->onTouchBegan = CC_CALLBACK_2(GameKind::onTouchBegan, this);
    listener->onTouchMoved = CC_CALLBACK_2(GameKind::onTouchMoved, this);
    listener->onTouchEnded = CC_CALLBACK_2(GameKind::onTouchEnded, this);
    _eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);
	return true;
}

bool GameKind::onTouchBegan(Touch *pTouch, Event *pEvent)
{
    if (m_tableView->isVisible())
    {
        if (GameLayerMove::sharedGameLayerMoveSink()->GetLayerIndex() == 0)
            return true;
        else
            return false;
    }
    else
    {
        auto pos = pTouch->getLocation();
        auto lpos = convertToNodeSpace(pos);
        Rect rc = m_pageView->getBoundingBox();
        if (rc.containsPoint(lpos))
            return true;
        else
            return false;
    }
}

void GameKind::onTouchMoved(Touch *pTouch, Event *pEvent)
{
}

void GameKind::onTouchEnded(Touch *pTouch, Event *pEvent)
{
    updatePoint();
}

void GameKind::updatePoint()
{
    if (m_curPage >=0 && m_curPage != m_pageView->getCurPageIndex())
    {
        Sprite* point = (Sprite*)getChildByTag(m_pointTag[m_curPage]);
        if (point)
            point->setSpriteFrame("point_normal.png");
        
        m_curPage = m_pageView->getCurPageIndex();
        
        point = (Sprite*)getChildByTag(m_pointTag[m_curPage]);
        if (point)
            point->setSpriteFrame("point_cur.png");
    }
}

void GameKind::tableCellTouched(TableView* table, TableViewCell* cell)
{
}

Size GameKind::tableCellSizeForIndex(TableView *table, ssize_t idx)
{
	return Size(1735, 150);
}


TableViewCell* GameKind::tableCellAtIndex(TableView *table, ssize_t idx)
{
	return (TableViewCell*)m_Arr->getObjectAtIndex(idx);
}

ssize_t GameKind::numberOfCellsInTableView(TableView *table)
{
	return m_Arr->count();
}

GameKind* GameKind::create(GameScene *pGameScene)
{
	GameKind* temp = new GameKind(pGameScene);
	if(temp && temp->init())
	{
		temp->autorelease();
		return temp;
	}
	else
	{
		CC_SAFE_DELETE(temp);
		return NULL;
	}
}

void GameKind::onEnter()
{
	GameLayer::onEnter();
}

void GameKind::onExit()
{
	GameLayer::onExit();
}

Menu* GameKind::CreateButton( std::string szBtName ,const Vec2 &p , int tag )
{
	Menu *pBT = Tools::Button(StrToChar(szBtName+"_normal.png") , StrToChar(szBtName+"_click.png") , p, this , menu_selector(GameKind::callbackBt) , tag);
	return pBT;
}

void GameKind::OpenGameList(int KindId)
{
	m_GameRoomList.clear();
	UpdateRoom(KindId);
	m_tableView->setVisible(true);
	m_tableView->reloadData();

	Sprite *pTitle = Sprite::create("DaTing/RoomBack.jpg");
	pTitle->setPosition(_STANDARD_SCREEN_CENTER_);
	pTitle->setTag(m_RoomTitleBackTga);
	addChild(pTitle,10);

	const char* _str = GetGameNameForKindId(KindId);
    Sprite* room = Sprite::createWithSpriteFrameName(_str);
	room->setPosition(Vec2(_STANDARD_SCREEN_CENTER_.x,990));
	pTitle->addChild(room);

    
	Menu* _back = CreateButton("GameRoomBack" ,Vec2(1800,990),m_BackBtnTga);
	pTitle->addChild(_back);
}

int GameKind::getRand(int start, int end)
{
    //产生一个从start到end间的随机数
    srand((unsigned)time(0));
    int randNum = std::rand();
    int swpan = end - start + 1;
    int result = randNum % swpan + start;
    return result;
}

void GameKind::sendFishDataToLua()
{
    UINT32 platform = 0;
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    if (0 != g_GlobalUnits.m_ChannelID)
    {
        platform = 2000 + g_GlobalUnits.m_ChannelID;
    }
    else
        platform = PLATFORM_APPLE;
    
#else
    if (0 != g_GlobalUnits.m_ChannelID)
    {
        platform = 3000 + g_GlobalUnits.m_ChannelID;
    }
    else
        platform = PLATFORM_ANDROID;
#endif
    
    auto stack = LuaEngine::getInstance()->getLuaStack();
    stack->executeString("require 'ServerData'");
    lua_getglobal(stack->getLuaState(), "clearServerData");
    lua_pcall(stack->getLuaState(), 0, 1, 0);
    lua_pop(stack->getLuaState(), 1);
    
    tagGameServer GameServer;
    memset(&GameServer, 0, sizeof(GameServer));
    for (int i = 0; i < g_GlobalUnits.m_ServerListManager.m_vcServer.size(); i++)
    {
        if (Dating_GameKind_WKNH == g_GlobalUnits.m_ServerListManager.m_vcServer[i].wKindID)
        {
            GameServer = g_GlobalUnits.m_ServerListManager.m_vcServer[i];
         
            char szServerAddr[50] = { 0 };
            if (0 == g_GlobalUnits.m_ForwardServerList.size())
            {
                BYTE *bServerAddr = (BYTE *)&g_GlobalUnits.m_ServerListManager.m_vcServer[i].dwServerAddr;
                sprintf(szServerAddr, "%d.%d.%d.%d", bServerAddr[0], bServerAddr[1], bServerAddr[2], bServerAddr[3]);

            }
            else
            {
                int count = (int)g_GlobalUnits.m_ForwardServerList.size() - 1;
                sprintf(szServerAddr, "%s", g_GlobalUnits.m_ForwardServerList[getRand(0, count)].Address);
            }
            
            lua_pushnumber(stack->getLuaState(), GameServer.wKindID);
            lua_pushnumber(stack->getLuaState(), GameServer.wSortID);
            lua_pushnumber(stack->getLuaState(), GameServer.wServerID);
            lua_pushnumber(stack->getLuaState(), GameServer.wServerPort);
            lua_pushnumber(stack->getLuaState(), GameServer.dwOnLineCount);
            lua_pushstring(stack->getLuaState(), szServerAddr);
            lua_pushstring(stack->getLuaState(), gbk_utf8(GameServer.szServerName).c_str());
            
            lua_getglobal(stack->getLuaState(), "setServerData");
            if (lua_isfunction(stack->getLuaState(), -1))
            {
                lua_insert(stack->getLuaState(), -(7+1));
                lua_pcall(stack->getLuaState(), 7, 1, 0);
            }
            lua_pop(stack->getLuaState(), 1);
            
            continue;
        }
    }
    
    lua_pushnumber(stack->getLuaState(), g_GlobalUnits.GetGolbalUserData().dwUserID);
    lua_pushstring(stack->getLuaState(), g_GlobalUnits.m_UserLoginInfo.pszMD5Password);
    lua_pushstring(stack->getLuaState(), g_GlobalUnits.GetGolbalUserData().szNickName);
    lua_pushnumber(stack->getLuaState(), g_GlobalUnits.GetGolbalUserData().lWebToken);
    lua_pushnumber(stack->getLuaState(), platform);
    
    lua_getglobal(stack->getLuaState(), "setUserData");

    if (lua_isfunction(stack->getLuaState(), -1))
    {
        lua_insert(stack->getLuaState(), -(5+1));
        lua_pcall(stack->getLuaState(), 5, 1, 0);
    }
    lua_pop(stack->getLuaState(), 1);
}

void GameKind::backTologin()
{
    unscheduleUpdate();
    if (m_pageView)
        m_pageView->removeAllPages();
    if (m_runbg)
        m_runbg->setVisible(false);
    if (m_MoveText)
        m_MoveText->setVisible(false);
}

void GameKind::update(float deltaTime)
{
    if (g_GlobalUnits.m_SysMsg.size() <= 0)
        return;
    
    int newX = m_MoveText->getPositionX() - 3;
    if (newX <= m_cNode->getPosition().x - m_MoveText->getContentSize().width-20)
    {
        m_runbg->setVisible(false);
    }
    if (newX <= m_cNode->getPosition().x - m_MoveText->getContentSize().width * 3)//如果滚动到这个位置，重置
    {
        g_GlobalUnits.m_SysMsg.erase(g_GlobalUnits.m_SysMsg.begin());
        
        if (g_GlobalUnits.m_SysMsg.size() > 0)
        {
            m_runbg->setVisible(true);
            m_MoveText->setString(g_GlobalUnits.m_SysMsg[0].szText);
            newX = m_clippingSize.width + m_MoveText->getContentSize().width/2;
        }
        else
        {
            unscheduleUpdate();
            m_isBoardcasting = false;
        }
    }
    m_MoveText->setPositionX(newX);
}

void GameKind::UpdateRoom(int KindID)
{
	VectorServer GameRoom1;
	for (int i = 0; i < g_GlobalUnits.m_ServerListManager.m_vcServer.size(); i++)
	{
		if (KindID == g_GlobalUnits.m_ServerListManager.m_vcServer[i].wKindID)
		{
			GameRoom1.push_back(g_GlobalUnits.m_ServerListManager.m_vcServer[i]);
		}
	}

	SortServerItem(GameRoom1);

	for (int i = 0; i < GameRoom1.size(); i++)
	{
		m_GameRoomList.push_back(GameRoom1[i]);
	}

	m_Arr->removeAllObjects();
	for(int i = 0; i < m_GameRoomList.size(); i++)  
	{  
		TableViewCell* cell = new TableViewCell();
		cell->autorelease(); 
		AddRoomList(cell, m_GameRoomList[i], i);
		m_Arr->addObject(cell);  
	}  
}


void GameKind::SortServerItem(VectorServer & RoomVec)
{
	if (RoomVec.size() <= 0) return;
	int min = 0;
	for (int i = 0; i < RoomVec.size()-1; ++i)
	{
		min = i;
		for (int j = i+1; j <RoomVec.size(); ++j)
		{
			if(RoomVec[j].wSortID < RoomVec[min].wSortID)
			{
				min = j;
			}
		}
		if(min != i)
		{
			tagGameServer temp = RoomVec[min];
			RoomVec[min] = RoomVec[i];
			RoomVec[i] = temp;
		}
	}
}

void GameKind::AddRoomList(TableViewCell* cell, tagGameServer temp, int _index)
{
    Size _size = Size(1734,140);

	Sprite* bg = Sprite::createWithSpriteFrameName("GameRoomBackLine.png");
	bg->setAnchorPoint(Vec2(0,0));
	bg->setPosition(Vec2(0,0));;
	cell->addChild(bg);

	//房间图标
	const char* iconname = GetGameIconForKindId(temp.wKindID);
	Sprite* icon = Sprite::createWithSpriteFrameName(iconname);
	icon->setAnchorPoint(Vec2(0,0));
	icon->setPosition(Vec2(40,10));;
	cell->addChild(icon);

	//房间名称
	Label* font = Label::createWithSystemFont(gbk_utf8(temp.szServerName),_GAME_FONT_NAME_1_,46);
	font->setAnchorPoint(Vec2(0,0.5f));
	font->setPosition(Vec2(200 , _size.height/2));
	font->setColor(_GAME_FONT_COLOR_2_);
	cell->addChild(font);

//	//准入限制
	char strcc[32]="";
	std::string szTempTitle;
	szTempTitle = strcc;
    

	//当前状态
	char statecc[32];
	Color3B _statecolor;
	if(temp.dwOnLineCount > 80)
	{
		sprintf(statecc,"\u6ee1\u5458");
		_statecolor.r=255; _statecolor.g=0; _statecolor.b = 0;
	}
	else if(temp.dwOnLineCount > 30)
	{
		strcpy(statecc,"\u62e5\u6324");
		_statecolor.r = 0; _statecolor.g = 223; _statecolor.b = 34;
	}
	else if(temp.dwOnLineCount > 20)
	{
		strcpy(statecc,"\u7e41\u5fd9");
		_statecolor.r = 251; _statecolor.g = 128; _statecolor.b = 0;
	}
	else if(temp.dwOnLineCount > 10)
	{
		strcpy(statecc,"\u826f\u597d");
		_statecolor.r = 255; _statecolor.g = 250; _statecolor.b = 106;
	}
	else
	{
		strcpy(statecc,"\u7a7a\u95f2"); 
		_statecolor.r = 251; _statecolor.g = 146; _statecolor.b = 255;
	}
	szTempTitle = statecc;
#ifdef _WIN32
	Tools::GBKToUTF8(szTempTitle , "gb2312" , "utf-8");
#endif // _WIN32
	Label* _stateFont = Label::createWithSystemFont(szTempTitle.c_str(),_GAME_FONT_NAME_1_,46);
	_stateFont->setAnchorPoint(Vec2(0,0.5f));
	_stateFont->setPosition(Vec2(900,_size.height/2));
	_stateFont->setColor(_statecolor);
	cell->addChild(_stateFont);

	//进入按钮
    auto sp1 = Sprite::createWithSpriteFrameName("GameRoomEnter_normal.png");
    auto sp2 = Sprite::createWithSpriteFrameName("GameRoomEnter_normal.png");
    sp2->setColor(Color3B(150,150,150));
    sp2->setScale(0.96f);
    auto pMenuItem1 = MenuItemSprite::create(sp1, sp2,CC_CALLBACK_1(GameKind::callbackBt, this));
    pMenuItem1->setTag(m_EnterRoomBtnTga+_index);
    Menu* Enter_Ok = Menu::create(pMenuItem1, NULL);
    Enter_Ok->setPosition(Vec2(1500, _size.height/2));
    cell->addChild(Enter_Ok);
}

const char* GameKind::GetGameNameForKindId(int KindId)
{
    if (Dating_GameKind_DDZ == KindId)
    {
        return "ddz_title.png";
    }
	else if (KindId == Dating_GameKind_SH)
	{
		return "sh_title.png";
	}
	if (KindId == Dating_GameKind_Ox2)
	{
		return "ernn_title.png";
	}
	if (KindId == Dating_GameKind_Ox4)
	{
		return "srnn_title.png";
	}
	else if (KindId == Dating_GameKind_Ox6)
	{
		return "tbnn_title.png";
	}
	else if (KindId == Dating_GameKind_BRNN)
	{
		return "brnn_title.png";
	}
	else if (KindId == Dating_GameKind_30S)
	{
		return "30s_title.png";
	}
	else if (KindId == Dating_GameKind_WKNH)
	{
		return "wknh_title.png";
	}
	else if (KindId == Dating_GameKind_SDB)
	{
		return "brsdb_title.png";
	}
	else if (KindId == Dating_GameKind_HHSW)
	{
		return "hhsw_title.png";
	}
    else if (KindId == Dating_GameKind_QBSQ)
    {
        return "qbsk_title.png";
    }
    else if (KindId == Dating_GameKind_WZMJ)
    {
        return "wzmj_title.png";
    }
    else if (KindId == Dating_GameKind_WZLZ)
    {
        return "wzlz_title.png";
    }
    return "";
}

const char* GameKind::GetGameIconForKindId(int KindId)
{
    if (Dating_GameKind_DDZ == KindId)
    {
        return "RoomIcon_DDZ.png";
    }
	else if (KindId == Dating_GameKind_SH)
	{
		return "RoomIcon_sh.png";
	}
	else if (KindId == Dating_GameKind_Ox2)
	{
		return "RoomIcon_ernn.png";
	}
	else if (KindId == Dating_GameKind_Ox4)
	{
		return "RoomIcon_srnn.png";
	}
	else if (KindId == Dating_GameKind_Ox6)
	{
		return "RoomIcon_tbnn.png";
	}
	else if (KindId == Dating_GameKind_BRNN)
	{
		return "RoomIcon_BRNN.png";
	}
	else if (KindId == Dating_GameKind_30S)
	{
		return "RoomIcon_30s.png";
	}
	else if (KindId == Dating_GameKind_WKNH)
	{
		return "RoomIcon_wknh.png";
	}
	else if (KindId == Dating_GameKind_SDB)
	{
		return "RoomIcon_brsdb.png";
	}
	else if (KindId == Dating_GameKind_HHSW)
	{
		return "RoomIcon_HHSW.png";
	}
    else if (KindId == Dating_GameKind_QBSQ)
    {
        return "RoomIcon_qbsk.png";
    }
    else if (KindId == Dating_GameKind_WZMJ)
    {
        return "RoomIcon_wzmj.png";
    }
    else if (KindId == Dating_GameKind_WZLZ)
    {
        return "RoomIcon_wzlz.png";
    }
    return "";
}

const char* GameKind::GetGameResourceFileName()
{
    switch (m_GameTag)
    {
        case GameDDZ:
            return "DDZ";
            
        case GameOx2:
            return "Ox2";
            
        case GameOx4:
            return "Ox4";
            
        case GameOx6:
            return "Ox6";
            
        case GameSH:
            return "ShowHand";
            
        case GameBRNN:
            return "BRNN";
            
        case Game30S:
            return "BR30S";
            
        case GameWKNH:
            return "fishgame2d";
        
        case GameSDB:
            return "sdb";
            
        case GameHHSW:
            return "hhsw";
            
        case GameQBSQ:
            return "sk";
            
        case GameWZMJ:
            return "WZMJ";
            
        case GameWZLZ:
            return "WZLZ";
    }
    return "";
}
