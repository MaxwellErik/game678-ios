﻿#ifndef _SOCKETENGINE_H_
#define _SOCKETENGINE_H_

#include "GameLayer.h"
#include "FrameGameView.h"

//class CTableFrameSink;
class FrameGameView;

class SocketEngine
{
public:
	SocketEngine(void);
	~SocketEngine(void);

protected:
//	CTableFrameSink				*m_pTableFrameSink;
	FrameGameView				*m_pFrameGameView;

public:
//	virtual void setTableFrameSink(CTableFrameSink *pTableFrameSink);
	virtual void setFrameGameView(FrameGameView *pFrameGameView);


public:
	//发送函数
	virtual bool SendData(WORD wSubCmdID);
	//发送函数
	virtual bool SendData(WORD wSubCmdID, const void * pData, WORD wDataSize);
	//发送函数
	virtual bool SendMobileData(WORD wSubCmdID);
	//发送函数
	virtual bool SendMobileData(WORD wSubCmdID, const void * pData, WORD wDataSize);
	

	virtual bool SendGameScene(const void * pData , WORD wDataSize);

public:
	virtual bool ReceiveData(WORD wMainCmdID , WORD wSubCmdID);

	virtual bool ReceiveData(WORD wMainCmdID , WORD wSubCmdID,const void * pData, WORD wDataSize);

protected:
	static SocketEngine * m_pSocketEngine;

public:
	static SocketEngine * sharedSocketEngine();

};

#endif
