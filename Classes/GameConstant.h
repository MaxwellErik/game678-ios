﻿#ifndef __GAMECONSTANT_H__
#define __GAMECONSTANT_H__

#include <string>
#include "def.h"
//全局头文件
#include "Constant.h"
#include "GlobalDef.h"
#include "GlobalField.h"
#include "GlobalFrame.h"
#include "GlobalRight.h"
#include "GlobalProperty.h"
#include "GlobalUnits.h"

//命令头文件
#include "CMD_Mobile.h"
#include "CMD_Game.h"
#include "CMD_Plaza.h"


#define		RUN_ITEM_FT								0
#define		RUN_ITEM_YQ								1
#define		RUN_ITEM_DD								2
#define		RUN_ITEM_LZS							3
#define		RUN_ITEM_LC								4
#define		RUN_ITEM_SJ								5
#define		RUN_ITEM_TTXD							6
#define		RUN_ITEM_ZYT							7
#define		RUN_ITEM_EXIT							8


#define		RUN_CELL_COUNT							9
#define		MARY_PRIZE_ITEM_COUNT					8

#define		ITEM_INDEX_FT											0	//斧头
#define		ITEM_INDEX_YQ											1	
#define		ITEM_INDEX_DD											2
#define		ITEM_INDEX_LZS											3
#define		ITEM_INDEX_LC											4	//林冲
#define		ITEM_INDEX_SJ											5
#define		ITEM_INDEX_TTXD											6
#define		ITEM_INDEX_ZYT											7	
#define		ITEM_INDEX_SHZ											8	//水浒传

#define		ITEM_COUNT												9

#define		ITEM_STATE_NORMAL										0
#define		ITEM_STATE_GRAY											1
#define		ITEM_STATE_SHARK										2
#define		ITEM_STATE_FLASH										3
#define		ITEM_STATE_ANIMATION									4

#define		ITEM_STATE_COUNT										5

#define		CELL_START_TAG											100
#define		CELL_COUNT												15

#define		ROW_COUNT												3
#define		LINE_COUNT												5
#define		LIGHT_COUNT												9

#define		LINE_WIDTH												1104

#define		BET_NUM_SIZE	CCSizeMake(30,30)
#define		WIN_NUM_SIZE	CCSizeMake(97,134)

#define		GAME_TYPE_MAIN											0
#define		GAME_TYPE_MARY											1
#define		GAME_TYPE_COMPARE										2

#define		RUN_ITEM_COUNT							24
#define		MARY_CELL_COUNT							4

#define		COMPARE_AREA_SMALL										0
#define		COMPARE_AREA_TIE										1
#define		COMPARE_AREA_BIG										2

#define		COMPARE_AREA_COUNT										3


#define		COMPARE_BIG								0
#define		COMPARE_BIG_PAIR						1
#define		COMPARE_SMALL							2
#define		COMPARE_SMALL_PAIR						3
#define		COMPARE_TIE								4


#define		COMPARE_ODD_COUNT						5
#define		DICE_COUNT								2

#define		DEFAULT_SCALE							4

#define		CONNECT_TIMEOUT							(2)
#define		REMOVE_CONNECT_TIMEOUT					(15)

#define		MIN_BET_SCORE											(1)

#ifdef _DEBUG
#define		MAX_BET_SCORE											(100000)
#else
#define		MAX_BET_SCORE											(100000)
#endif // _DEBUG

const std::string ITEM_RES_NAMES[ITEM_COUNT] = 
{
	"ft","yq","dd","lzs","lc","sj","ttxd","zyt","shz"
};

const std::string ITEM_RES_STATE_NAMES[ITEM_STATE_COUNT] = 
{
	"normal" , "gray" , "shake" ,"flash" , "anim"
};

const std::string WIN_TEXT[3] = {
	"compare.png" , "winscore.png" , "congratulation.png"
};

const int LINE_MATRIX[LIGHT_COUNT][LINE_COUNT][2]=
{
	{{1,0},{1,1},{1,2},{1,3},{1,4}},
	{{0,0},{0,1},{0,2},{0,3},{0,4}},
	{{2,0},{2,1},{2,2},{2,3},{2,4}},
	{{0,0},{1,1},{2,2},{1,3},{0,4}},
	{{2,0},{1,1},{0,2},{1,3},{2,4}},
	{{0,0},{0,1},{1,2},{0,3},{0,4}},
	{{2,0},{2,1},{1,2},{2,3},{2,4}},
	{{1,0},{2,1},{2,2},{2,3},{1,4}},
	{{1,0},{0,1},{0,2},{0,3},{1,4}}
};//按行初始化

const int LINE_ODDS[LIGHT_COUNT][3]=
{
	{2,5,20},{3,10,40},{5,15,60},
	{7,20,100},{10,30,160},{15,40,200},
	{20,80,400},{50,200,1000},{0,0,2000}
};

const int COMPARE_ODDS[COMPARE_ODD_COUNT]=
{
	2,4,2,4,6
};

const int ALL_ODDS[ITEM_COUNT] = 
{
	50,100,150,250,400,500,1000,2000,5000
};

const int ALLANY_ODDS[2] = {15,50};

const int ITEM_BASE_RATE[ITEM_COUNT]=
{
	2210,1660,1390,1200,1015,810,645,350,720
};

const int MARY_LINE_ODDS[MARY_CELL_COUNT]=
{
	0,0,20 , 500
};


const int MARY_ITEM_ODDS[MARY_PRIZE_ITEM_COUNT]=
{
	2,5,10,20,50,70,100,200
};

const int RUN_CELL_ITEM[RUN_ITEM_COUNT] = 
{
	RUN_ITEM_EXIT,RUN_ITEM_SJ,RUN_ITEM_LZS,RUN_ITEM_FT,RUN_ITEM_DD,RUN_ITEM_LC,RUN_ITEM_EXIT,
	RUN_ITEM_TTXD,RUN_ITEM_FT,RUN_ITEM_YQ,RUN_ITEM_LZS,RUN_ITEM_SJ,
	RUN_ITEM_EXIT,RUN_ITEM_DD,RUN_ITEM_FT,RUN_ITEM_ZYT,RUN_ITEM_YQ,RUN_ITEM_LZS,RUN_ITEM_EXIT,
	RUN_ITEM_TTXD,RUN_ITEM_DD,RUN_ITEM_FT,RUN_ITEM_YQ,RUN_ITEM_LC
// 	RUN_ITEM_EXIT,RUN_ITEM_LZS,RUN_ITEM_YQ,RUN_ITEM_ZYT,RUN_ITEM_FT,RUN_ITEM_DD,
// 	RUN_ITEM_EXIT,RUN_ITEM_SJ,RUN_ITEM_LZS,RUN_ITEM_YQ,RUN_ITEM_FT,RUN_ITEM_TTXD,
// 	RUN_ITEM_EXIT,RUN_ITEM_LC,RUN_ITEM_DD,RUN_ITEM_FT,RUN_ITEM_LZS,RUN_ITEM_SJ,
// 	RUN_ITEM_EXIT,RUN_ITEM_LC,RUN_ITEM_YQ,RUN_ITEM_FT,RUN_ITEM_DD,RUN_ITEM_TTXD
};

struct Grid
{
	int row;
	int line;
};

struct LinePrize 
{
	BYTE		lineID;
	BYTE		lineCount;
	BYTE		prizeNum;
	BYTE		sendMary;
	Grid		lineGrid[LINE_COUNT];
};


struct UserGameInfo
{
	LONGLONG							lScore;			//金子或铜钱
	LONGLONG							lCredit;		//左下角显示的钱
	LONGLONG							lTotalWin;
	LONG								lLine;			//线数
	LONG								lBet;			//单注
	LONGLONG							lTotalBet;		//总下注
	LONG								lTopScore;
	bool								bWin;
	BYTE								cbBetLevel;		//下注等级
};

typedef std::vector<LinePrize> VCLinePrize;


////////////////////////////////////////////////////////////////////////////////////////
#define USER_LOCAL_SCORE										500
#define USER_LESS_SCORE											2000		//单机游戏补充到的铜钱数
#define USER_LOCAL_MIN_SCORE									1			//最少游戏币数量
#define USER_GUIDE_SCORE										100			//新手引导初值
////////////////////////////////////////////////////////////////////////////////////////

#endif    // __MAIN_H__
