#include "Screen.h"
#include "cocos2d.h"

USING_NS_CC;

CScreen *CScreen::m_pScreen = NULL;

CScreen::CScreen(void)
{

}


CScreen::~CScreen(void)
{

}

CScreen * CScreen::share()
{
	if (m_pScreen == NULL)
	{
		m_pScreen = new CScreen;
	}
	return m_pScreen;
}

void CScreen::Shoot( const char *fileName )
{
	Size size = Director::getInstance()->getWinSize();
	//定义一个屏幕大小的渲染纹理  
	RenderTexture* pScreen = RenderTexture::create(size.width,size.height, Texture2D::PixelFormat::RGBA8888);
	//获得当前的场景指针  
	Scene* pCurScene = Director::getInstance()->getRunningScene();
	//渲染纹理开始捕捉  
	pScreen->begin();  
	//当前场景参与绘制  
	pCurScene->visit();  
	//结束捕捉  
	pScreen->end();  
	//保存为png
	pScreen->saveToFile(fileName, Image::Format::PNG);
	CC_SAFE_DELETE(pScreen);
}

std::string CScreen::GetShootPath( const char *fileName )
{
	std::string fullpath = FileUtils::getInstance()->getWritablePath() + fileName;
	return fullpath;
}
