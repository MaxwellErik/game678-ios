﻿#include "LobbySocketSink.h"
#include "ClientSocketSink.h"
#include "AlertMessageLayer.h"
#include "LocalDataUtil.h"
#include "GlobalUnits.h"
#include "Convert.h"
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
#include "sendInfoToUmeng.h"
#endif

LobbySocketSink	*	LobbySocketSink::m_pLobbySocketSink=NULL;
TcpClientSocket *	LobbySocketSink::m_pLobbySocket=NULL;
//////////////////////////////////////////////////////////////////////////////////
LobbySocketSink::LobbySocketSink(void)
{
	m_LobbyLayer = NULL;
	m_BankLayer = NULL;
	m_LoginLayer = NULL;
}

LobbySocketSink::~LobbySocketSink(void)
{
}

LobbySocketSink* LobbySocketSink::sharedSocketSink()
{
	if (m_pLobbySocketSink == NULL)
	{
		m_pLobbySocketSink = new LobbySocketSink();
	}
	return m_pLobbySocketSink;
}

TcpClientSocket * LobbySocketSink::sharedLoginSocket( void )
{
	if (m_pLobbySocket == NULL)
	{
		m_pLobbySocket = new TcpClientSocket();
	}
	return m_pLobbySocket;
}

bool LobbySocketSink::OnEventTCPSocketRead( CMD_Command Command, VOID * pData, WORD wDataSize )
{
    cocos2d::log("----LobbySocketSink------------->>>>>main ID: %d, sub ID: %d",Command.wMainCmdID, Command.wSubCmdID);
	switch (Command.wMainCmdID)
	{
    case MDM_GP_LOGON:
	case MDM_MB_LOGON:				//登录消息
		{
			return OnSocketMainLogon(Command,pData,wDataSize);
		}
	case MDM_GP_SERVER_LIST:		//列表消息
		{
			return OnSocketMainServerList(Command,pData,wDataSize);
		}
	case MDM_GF_BANK: //银行相关....
		{
			return OnSocketMainSystem(Command,pData,wDataSize);
		}
    case MDM_GP_USER:
        {
            return OnSocketUserServer(Command,pData,wDataSize);
        }
	}
	return true;
}

bool LobbySocketSink::OnSocketUserServer( CMD_Command Command, void * pBuffer, WORD wDataSize )
{
    switch (Command.wSubCmdID)
    {
        case SUB_GP_HEARBEAT_RESULT:
        {
            CMD_GP_Hearbeat_Result* heart = (CMD_GP_Hearbeat_Result*)pBuffer;
            
            cocos2d::log("--------dwResult = %d", heart->dwResult);
            
            if (0 == heart->dwResult)
            {
                g_GlobalUnits.m_HeartCount++;
            }
            else if (1 == heart->dwResult)
            {
                g_GlobalUnits.m_HeartCount = 0;
                AlertMessageLayer::createConfirm(m_LobbyLayer, "你的账号在其他地方登录！", menu_selector(LobbyLayer::GoToLogin));
            }
            else if (3 == heart->dwResult)
            {
                g_GlobalUnits.m_HeartCount = 0;
                AlertMessageLayer::createConfirm(m_LobbyLayer, "", menu_selector(LobbyLayer::GoToLogin));
            }
            break;
        }
        case SUB_RANKING_RESPONSE:
        {
            LoadLayer::removeLoadLayer(Director::getInstance()->getRunningScene());
            CMD_RankingQuery_Result* rank = (CMD_RankingQuery_Result*)pBuffer;
            if (0 != wDataSize % sizeof(tagRankingInfo))
                return true;
            
            m_LobbyLayer->OnRecvRankData(rank);
            break;
        }
        case SUB_GP_FORWARDSERVER_RESULT:
        {
            int addressCount = wDataSize/sizeof(CMD_GP_ForwardServerResult);
            g_GlobalUnits.m_ForwardServerList.clear();
            for (int i = 0; i < addressCount; i++)
            {
                CMD_GP_ForwardServerResult *address = ((CMD_GP_ForwardServerResult*)pBuffer + i);
                g_GlobalUnits.m_ForwardServerList.push_back(*address);
            }
            
            //关闭连接
            closeSocket();
            //进入大厅，隐藏登陆
            if(m_LoginLayer != NULL)
            {
                m_LoginLayer->close();
                m_LoginLayer = NULL;
            }
            g_GlobalUnits.m_IsLogin = true;
            setRcvLayer(m_LobbyLayer);
            m_LobbyLayer->UpdateUserData();
            m_LobbyLayer->SendBoardCastReq();
            m_LobbyLayer->SendHeart();
            break;
        }
        case SUB_GM_BROADCAST_RES:
        {
            CMD_GP_Broadcast_Result* broadcast = (CMD_GP_Broadcast_Result*)pBuffer;
            if (strcmp(broadcast->Scope, "0") == 0)
            {
                SysMsg _sysmsg;
                sprintf(_sysmsg.szText,"%s", gbk_utf8(broadcast->Content).c_str());
                g_GlobalUnits.m_SysMsg.push_back(_sysmsg);

                if(m_LobbyLayer != NULL) m_LobbyLayer->AddText();
            }
            break;
        }
        case SUB_GM_BROADCAST_END:
        {
            CMD_GP_Broadcast* broadcast = (CMD_GP_Broadcast*)pBuffer;
            g_GlobalUnits.m_boradcastTime = broadcast->LastTime;
            break;
        }
        case SUB_FISH_FREECOIN_RES:
        {
            CMD_Fish_Freecoin_Result* result = (CMD_Fish_Freecoin_Result*)pBuffer;
            g_GlobalUnits.m_FreeAccount = result->freeAccount;
            g_GlobalUnits.m_FreeWeixin = result->freeWeixin;
        }
    }
    return true;
}

bool LobbySocketSink::OnSocketMainLogon( CMD_Command Command, void * pBuffer, WORD wDataSize )
{
	if (Command.wSubCmdID == SUB_MB_LOGON_SUCCESS) //登录成功
	{
		//效验参数
		CC_ASSERT(wDataSize>=sizeof(CMD_MB_LogonSuccess));
		if (wDataSize<sizeof(CMD_MB_LogonSuccess)) return false;

		//保存信息
		tagGlobalUserData &UserData = g_GlobalUnits.GetGolbalUserData();
		CMD_MB_LogonSuccess * pLogonSuccess=(CMD_MB_LogonSuccess *)pBuffer;
		UserData.wFaceID = pLogonSuccess->wFaceID;
		UserData.wGender = pLogonSuccess->cbGender;
		UserData.cbMember =0;
		UserData.dwUserID = pLogonSuccess->dwUserID;
		UserData.dwGameID = pLogonSuccess->dwGameID;
		UserData.dwExperience = pLogonSuccess->dwExperience;
		UserData.lScore = pLogonSuccess->lScore;
		UserData.lInsureScore = pLogonSuccess->lInsureScore;
        UserData.fLastLogonTime = pLogonSuccess->fLastLogonTime;
        UserData.lWebToken = pLogonSuccess->lWebToken;
        UserData.dwLoveliness = pLogonSuccess->dwLoveLiness;
		strcpy( UserData.szNickName, gbk_utf8(pLogonSuccess->szNickName).c_str());
        UserData.bBindWeChat = pLogonSuccess->cbBindWeixin;
        UserData.wLogonType = pLogonSuccess->wLogonType;
        if (UserData.wLogonType == 1)//游客
        {
            sprintf(g_GlobalUnits.m_UserLoginInfo.pszMD5Password,"%s", pLogonSuccess->szLogonPass);
        }
        else if(UserData.wLogonType == 2) //微信登录
        {
            sprintf(g_GlobalUnits.m_UserLoginInfo.pszMD5Password,"%s", pLogonSuccess->szLogonPass);
        }
        else
        {
            g_GlobalUnits.InsertUserData(g_GlobalUnits.m_UserLoginInfo);
            g_GlobalUnits.SaveUserData();
        }
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
        if (1 == pLogonSuccess->registerOrLogin)
            SendInfoToUmeng::sendRegisterInfoToUmeng(pLogonSuccess->dwUserID);
        else
            SendInfoToUmeng::sendLoginInfoToUmeng(pLogonSuccess->dwUserID);
#else
        char userid[32];
        memset(userid, 0, sizeof(userid));
        sprintf(userid, "%d", pLogonSuccess->dwUserID);
        if (1 == pLogonSuccess->registerOrLogin)
        	JniSink::share()->SendRegisterSuccess(userid);
        else
        	JniSink::share()->SendLoginSuccess(userid);
#endif
    }
    else if (Command.wSubCmdID == SUB_GP_VALIDATE_PASSPORT_ID) //验证身份证
	{
        if(m_LoginLayer != NULL)
        {
            m_LoginLayer->OnExitLoginLayerToBind();
        }
	}
	else if (Command.wSubCmdID == SUB_MB_LOGON_FAILURE)		//登录失败
	{
        LoadLayer::removeLoadLayer(Director::getInstance()->getRunningScene());
		//效验参数
		CMD_GP_LogonError *pLogonError = (CMD_GP_LogonError *)pBuffer;
        
        if (4 == pLogonError->cbValidateType && nullptr != m_LoginLayer)//微信验证码
        {
            m_LoginLayer->OnExitLoginLayerToBind();
            return true;
        }
        
        //显示消息
        AlertMessageLayer::createConfirm(m_LobbyLayer, gbk_utf8(pLogonError->szErrorDescribe).c_str());
        return true;
	}
	else if (Command.wSubCmdID == SUB_GP_LOGON_FINISH)		//登录完成
	{
        //请求转发服务器列表
        m_LobbyLayer->GetServerAddress();
        return true;
    }
	else if (Command.wSubCmdID == SUB_MB_BANK_OPEN) //银行开
	{
		if(m_LobbyLayer != NULL) m_LobbyLayer->SetBankDisable(true);
	}
	else if (Command.wSubCmdID == SUB_MB_BANK_CLOSE) //银行关
	{
		if(m_LobbyLayer != NULL) m_LobbyLayer->SetBankDisable(true);
	} 
	else if(Command.wSubCmdID == SUB_GP_LINK_SERVER_FINISH)
	{
		closeSocket();
		CMD_GP_LinkServerRes *pLinkServer = (CMD_GP_LinkServerRes *)pBuffer;
		int ret = pLinkServer->wRt;
		if (ret != 1)
		{
			LoadLayer::removeLoadLayer(Director::getInstance()->getRunningScene());
			return true;
		}
		
        BYTE *bServerAddr = (BYTE *)&pLinkServer->gameServer.dwServerAddr;
        char szServerAddr[50] = { 0 };
        sprintf(szServerAddr, "%d.%d.%d.%d", bServerAddr[0], bServerAddr[1], bServerAddr[2], bServerAddr[3]);
        
		((LobbyView *)m_pRcvLayer)->selectServer(szServerAddr, pLinkServer->gameServer.wServerPort);
		ClientSocketSink::sharedSocketSink()->AccountCheckFinish(pLinkServer->wKindID);
		return true;
	}
	else if (Command.wSubCmdID == SUB_GP_REGISTER_SUCCESS)
	{
		ASSERT_RETURN(wDataSize == sizeof(CMD_S_GP_RegisterSuccess) , false);
		CMD_S_GP_RegisterSuccess *pRegisterSuccess = (CMD_S_GP_RegisterSuccess *)pBuffer;

		tagGlobalUserData & GlobalUserInfo=g_GlobalUnits.GetGolbalUserData();
		GlobalUserInfo.cbOpenPlat = pRegisterSuccess->cbOpenPlat;
		if (pRegisterSuccess->cbOpenPlat == OPENFLAT_GUEST)
		{
			LocalDataUtil::SaveUserId(pRegisterSuccess->dwUserID);
			LocalDataUtil::SavePassword(pRegisterSuccess->szPassWord, DATA_KEY_GUEST_PASS);
		}
	}
    else if (Command.wSubCmdID == SUB_MB_MODIFICA_VISITOR_SUCCESS)
    {
        CMD_GP_ModificaVisitorSuccess *pRegisterSuccess = (CMD_GP_ModificaVisitorSuccess *)pBuffer;
        sprintf(g_GlobalUnits.m_BankPassWord,"%s", pRegisterSuccess->szPassword);

        strcpy(g_GlobalUnits.GetGolbalUserData().szNickName, gbk_utf8(pRegisterSuccess->szNickname).c_str());
        g_GlobalUnits.InsertUserData(g_GlobalUnits.m_UserLoginInfo);
        g_GlobalUnits.SaveUserData();
        sprintf(g_GlobalUnits.m_UserLoginInfo.pszMD5Password,"%s",pRegisterSuccess->szPassword);
        g_GlobalUnits.GetGolbalUserData().lInsureScore += pRegisterSuccess->dwAddInsureScore;
        
        LoadLayer::removeLoadLayer(Director::getInstance()->getRunningScene());
        m_LobbyLayer->modefiySuccess();
        m_LobbyLayer->UpdataLobbyScore();
    }
    else if (Command.wSubCmdID == SUB_MB_RES_AUDIT_STATE)
    {
        closeSocket();
        LoadLayer::removeLoadLayer(Director::getInstance()->getRunningScene());
        CMD_MB_GetAuditState *auditState = (CMD_MB_GetAuditState *)pBuffer;
        g_GlobalUnits.m_SwitchYZ = auditState->auditState;
        g_GlobalUnits.m_YZPASS = auditState->pass;
        if (nullptr != m_LoginLayer)
            m_LoginLayer->showWechatLoginBtn(g_GlobalUnits.m_SwitchYZ);
    }
    else if (Command.wSubCmdID == SUB_MB_APPLE_PAY_RESPONSE)
    {
        LoadLayer::removeLoadLayer(Director::getInstance()->getRunningScene());
        CMD_MB_ApplePayResponse *result = (CMD_MB_ApplePayResponse*)pBuffer;
        if (0 == result->result)
        {
            AlertMessageLayer::createConfirm(m_LobbyLayer, "您已充值成功！");
            g_GlobalUnits.GetGolbalUserData().lInsureScore += result->score;
            m_LobbyLayer->UpdataLobbyScore();
        }
        else
        {
            AlertMessageLayer::createConfirm(m_LobbyLayer, "充值失败，请联系客服！");
        }
    }
    else if (Command.wSubCmdID == SUB_MB_ANDROID_VERSION_RESPONSE)
    {
        LoadLayer::removeLoadLayer(Director::getInstance()->getRunningScene());
        CMD_MB_Android_Update *androidUpdate = (CMD_MB_Android_Update *)pBuffer;
        if (androidUpdate->needUpdate == 1)
        {
            if(m_LobbyLayer != NULL) m_LobbyLayer->UpdataGame();
        }
    }
    return true;
}

bool LobbySocketSink::OnSocketMainServerList( CMD_Command Command, void * pBuffer, WORD wDataSize )
{
	if (SUB_GP_LIST_KIND == Command.wSubCmdID)//种类信息
	{
		//效验参数
		CC_ASSERT(wDataSize%sizeof(tagGameKind)==0);
		if (wDataSize%sizeof(tagGameKind)!=0) return false;

		//处理消息
		tagGameKind * pGameKind=(tagGameKind *)pBuffer;
		WORD wItemCount=wDataSize/sizeof(tagGameKind);
		g_GlobalUnits.m_ServerListManager.InsertKindItem(pGameKind,wItemCount);
		return true;
	}
	else if (SUB_GP_LIST_SERVER_TYPE == Command.wSubCmdID)//房间类型列表
	{
		//效验参数
		CC_ASSERT(wDataSize%sizeof(tagGameType)==0);
		if (wDataSize%sizeof(tagGameType)!=0) return false;

		//处理消息
		tagGameType * pGameServerType=(tagGameType *)pBuffer;
		WORD wItemCount=wDataSize/sizeof(tagGameType);
		g_GlobalUnits.m_ServerListManager.InsertTypeItem(pGameServerType,wItemCount);

		return true;
	}
	else if (SUB_GP_LIST_SERVER == Command.wSubCmdID)//房间列表
	{
		//效验参数
		CC_ASSERT(wDataSize%sizeof(tagGameServer)==0);
		if (wDataSize%sizeof(tagGameServer)!=0) return false;

		//处理消息
		tagGameServer * pGameServer=(tagGameServer *)pBuffer;
		WORD wItemCount=wDataSize/sizeof(tagGameServer);
		g_GlobalUnits.m_ServerListManager.InsertServerItem(pGameServer,wItemCount);
		return true;
	}
	return true;
}


bool LobbySocketSink::OnEventTCPConnectError()
{
    if(_parent == NULL)
    {
        Director::getInstance()->getRunningScene()->addChild(this);
    }
  
    scheduleOnce(schedule_selector(LobbySocketSink::OnError),_TCP_CLIENT_SOCKET_LOOP_TICKET_);
    return true;
}

void LobbySocketSink::OnError(float ft)
{
    //网络连接失败
    m_LoginLayer->ReconnectServer();
//    LoadLayer::removeLoadLayer(Director::getInstance()->getRunningScene());
//    AlertMessageLayer::createConfirm(m_LobbyLayer, "\u7f51\u7edc\u8fde\u63a5\u5931\u8d25\uff01");
    log("网络连接失败");
}

void LobbySocketSink::StartReceive()
{
	if(_parent == NULL)
	{
        auto scene = Director::getInstance()->getRunningScene();
        if (nullptr != scene)
            scene->addChild(this);
	}
    
	schedule(schedule_selector(LobbySocketSink::update),_TCP_CLIENT_SOCKET_LOOP_TICKET_);
}

void LobbySocketSink::StopReceive()
{
	unschedule(schedule_selector(LobbySocketSink::update));
}

void LobbySocketSink::update( float delta )
{
	sharedLoginSocket()->RecvPack();
}

void LobbySocketSink::setRcvLayer( GameLayer *pGameLayer)
{
	if (pGameLayer != m_pRcvLayer)
	{
		m_pRcvLayer = pGameLayer;
		if(_parent != NULL)
		{
			_parent->removeChild(this);
		}
		m_pRcvLayer->removeChild(this);
		m_pRcvLayer->addChild(this);
	}
}

void LobbySocketSink::closeSocket()
{
	StopReceive();
	if(_parent != NULL)
	{
		_parent->removeChild(this);
	}
    
	sharedLoginSocket()->CloseSocket();
}

bool LobbySocketSink::OnEventTCPSocketShut()
{
//	m_pRcvLayer->ShowMessageBox("与服务器断开连接！" , SMT_EJECT);
	return true;
}

int LobbySocketSink::safeconnect(char* ip,int port,time_t Unixdate)
{
	return sharedLoginSocket()->safeconnect(ip, port,Unixdate);
}

bool LobbySocketSink::TestConnectServer(string _ip)
{
    return true;
}

void LobbySocketSink::ConnectToServer()
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    sharedLoginSocket()->CloseSocket();
#endif
    
    if (sharedLoginSocket()->IsConnect())
    {
        StartReceive();
    }
    
    const char *sLOGINIP = const_cast<char*>(g_GlobalUnits.m_IP.c_str());
    struct hostent *remoteHostEnt = gethostbyname(g_GlobalUnits.m_IP.c_str());
    if (nullptr != remoteHostEnt)
    {
        struct in_addr *remoteInAddr = (struct in_addr *) remoteHostEnt->h_addr_list[0];
        sLOGINIP = inet_ntoa(*remoteInAddr);
    }
    else
    {
        sLOGINIP = "183.60.201.29";
    }
    
    sharedLoginSocket()->ConnectServer(sLOGINIP, g_GlobalUnits.m_ServerPort, this);
}

bool LobbySocketSink::OnEventTCPSocketLink()
{
    StartReceive();
    return true;
}



bool LobbySocketSink::ConnectServer()
{
    if (sharedLoginSocket()->IsConnect())
        sharedLoginSocket()->CloseSocket();
    
    sharedLoginSocket()->SetSocketLink(this);
    return true;
}


bool LobbySocketSink::ConnectServer(char* ip, DWORD port)
{
    sharedLoginSocket()->SetSocketLink(this);
    return true;
}

bool LobbySocketSink::SendMobileData(WORD wSubCmdID , void * pData , WORD wDataSize)
{
	if(_parent == NULL)
	{
		m_pRcvLayer->addChild(this);
	}
	if (ConnectServer())
	{
		return sharedLoginSocket()->SendData(MDM_GP_MOBILE , wSubCmdID , pData , wDataSize);
	}
	return true;
}

bool LobbySocketSink::SendMobileData( WORD wSubCmdID )
{
	if(_parent == NULL)
	{
		m_pRcvLayer->addChild(this);
	}
	if (ConnectServer())
	{
		return sharedLoginSocket()->SendData(MDM_GP_MOBILE , wSubCmdID);
	}
	return true;
}

void LobbySocketSink::SetLobbyLayer(LobbyLayer* _layer)
{
	m_LobbyLayer = _layer;
}

void LobbySocketSink::SetBankLayer(GameBank* _layer)
{
	m_BankLayer = _layer;
}

void LobbySocketSink::SetLoginLayer(LoginLayer* _layer)
{
	m_LoginLayer = _layer;
}

bool LobbySocketSink::OnSocketMainSystem( CMD_Command Command, void * pBuffer, WORD wDataSize )
{
	switch (Command.wSubCmdID)
	{
	case SUB_MB_RECV_USERINFO_SUCC: //完善用户资源
		{
			CMD_MB_PostUserInfo_Rst *pRank = (CMD_MB_PostUserInfo_Rst *)pBuffer;
			AlertMessageLayer::createConfirm(m_LobbyLayer, pRank->szDescribeString);
			break;
		}
	case SUB_MB_BANK_QUERY_SUCCESS:	//查询金币
    case SUB_MB_BANK_STORAGE_SUCCESS: //存成功
    case SUB_MB_BANK_GET_SUCCESS://取成功
		{
            LoadLayer::removeLoadLayer(Director::getInstance()->getRunningScene());
           
			CMD_SC_QueryScore* temp = (CMD_SC_QueryScore*)pBuffer;
			g_GlobalUnits.GetGolbalUserData().lScore=temp->lUserScore;
            g_GlobalUnits.GetGolbalUserData().lInsureScore = temp->lUserInsure;
            
            if(m_LobbyLayer != NULL)
            {
                m_LobbyLayer->OpenBank();
                m_LobbyLayer->UpdataLobbyScore();
            }
            if(m_BankLayer != NULL)
                m_BankLayer->FlashSave(true,0);
			break;
		}
	case SUB_MB_GET_NICKNAME_SUCCESS: //检查用户昵称
		{
            LoadLayer::removeLoadLayer(Director::getInstance()->getRunningScene());
			CMD_GP_UserTransferUserInfo_MB* temp = (CMD_GP_UserTransferUserInfo_MB*)pBuffer;
			if (m_BankLayer != NULL)
			{
                m_BankLayer->SetNickName(temp->szNickname, Color3B::RED, temp->nType);
			}
			break;
		}
	case SUB_MB_BANK_PRESENT_SUCCESS: //转账成功
		{
            LoadLayer::removeLoadLayer(Director::getInstance()->getRunningScene());
			CMD_MB_BankPresentSuccess* temp = (CMD_MB_BankPresentSuccess*)pBuffer;
			g_GlobalUnits.GetGolbalUserData().lInsureScore -= temp->lSwapScore;
			if (m_BankLayer != NULL)
			{
				if(m_LobbyLayer != NULL) m_LobbyLayer->UpdataLobbyScore();
				m_BankLayer->OpenSend();
				m_BankLayer->SendSuc(temp->lSwapScore, temp->lRevenue, temp->szTime);
			}
			break;
		}
	case SUB_MB_BANK_RECORD_SUCCESS:  //转账记录
		{
            LoadLayer::removeLoadLayer(Director::getInstance()->getRunningScene());
            CMD_MB_BankRecordResult* temp = (CMD_MB_BankRecordResult*)pBuffer;
            g_GlobalUnits.m_SendRecordVec.clear();
            for (int i = 0; i < temp->head.dwNum; i++)
            {
                g_GlobalUnits.m_SendRecordVec.push_back(temp->record[i]);
            }
           if(m_BankLayer!=NULL) m_BankLayer->reloadData();
			break;
		}
	case SUB_GP_OPERATE_SUCCESS://银行操作成功
		{
            LoadLayer::removeLoadLayer(Director::getInstance()->getRunningScene());
			CMD_GP_OperateSuccess_MB* temp = (CMD_GP_OperateSuccess_MB*)pBuffer;
			AlertMessageLayer::createConfirm(m_LobbyLayer, temp->szDescribeString);
			break;
		}
	case SUB_MB_OPERATOR_ERROR: //银行操作失败
		{
            LoadLayer::removeLoadLayer(Director::getInstance()->getRunningScene());
			CMD_SC_OperatorError* temp = (CMD_SC_OperatorError*)pBuffer;
            
			AlertMessageLayer::createConfirm(m_LobbyLayer, gbk_utf8(temp->szErrorDescribe).c_str());
            break;
		}
    case SUB_GF_MODIFYPWD_RESULT:
        {
            LoadLayer::removeLoadLayer(Director::getInstance()->getRunningScene());
            CMD_GF_ModifyPwdResult* temp = (CMD_GF_ModifyPwdResult*)pBuffer;
            
            if (temp->nType == 1)
            {
                sprintf(g_GlobalUnits.m_UserLoginInfo.pszMD5Password,"%s",temp->szNewPwd);
            }
            else if (temp->nType == 2)
            {
                sprintf(g_GlobalUnits.m_BankPassWord,"%s",temp->szNewPwd);
            }
            AlertMessageLayer::createConfirm(m_LobbyLayer, gbk_utf8(temp->szErrorDescribe).c_str());
            break;
        }
	}
	return true;
}

bool LobbySocketSink::Jionroom(int iPort,WORD KindID)
{
    if (0 == g_GlobalUnits.m_ForwardServerList.size())
    {
        for (int i = 0; i < g_GlobalUnits.m_ServerListManager.m_vcServer.size(); i++)
        {
            if (KindID == g_GlobalUnits.m_ServerListManager.m_vcServer[i].wKindID)
            {
                BYTE *bServerAddr = (BYTE *)&g_GlobalUnits.m_ServerListManager.m_vcServer[i].dwServerAddr;
                char szServerAddr[50] = { 0 };
                sprintf(szServerAddr, "%d.%d.%d.%d", bServerAddr[0], bServerAddr[1], bServerAddr[2], bServerAddr[3]);
                
                ((LobbyView *)m_pRcvLayer)->selectServer(szServerAddr, iPort);
                ClientSocketSink::sharedSocketSink()->AccountCheckFinish(KindID);
                return true;
            }
        }
    }
    int count = (int)g_GlobalUnits.m_ForwardServerList.size() - 1;
    char* szServerAddr = g_GlobalUnits.m_ForwardServerList[getRand(0, count)].Address;
    
	((LobbyView *)m_pRcvLayer)->selectServer(szServerAddr, iPort);
	ClientSocketSink::sharedSocketSink()->AccountCheckFinish(KindID);
	return true;
}

int LobbySocketSink::getRand(int start, int end)
{
    //产生一个从start到end间的随机数
    srand((unsigned)time(0));
    int randNum = std::rand();
    int swpan = end - start + 1;
    int result = randNum % swpan + start;
    return result;
}
