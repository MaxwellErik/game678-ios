#ifndef _GameBuyLayer_H_
#define _GameBuyLayer_H_

#include "GameScene.h"
#include "cocos-ext.h"
#include "JniSink.h"
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
#include "NSBuy.h"
#endif

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
class GamePay : public GameLayer, public JniCallback
#else
class GamePay : public GameLayer,JniCallback
#endif
{
public:
	virtual bool init();  
	static GamePay *create(GameScene *pGameScene);
	virtual ~GamePay();
	GamePay(GameScene *pGameScene);
	GamePay(const GamePay&);
	GamePay& operator = (const GamePay&);

public:
	virtual void onEnter();
	virtual void onExit();
	virtual bool onTouchBegan(Touch *pTouch, Event *pEvent){return true;}
	virtual void onTouchMoved(Touch *pTouch, Event *pEvent){}
	virtual void onTouchEnded(Touch *pTouch, Event *pEvent){}
	virtual void onTouchCancelled(Touch *pTouch, Event *pEvent){}
	virtual bool onTextFieldAttachWithIME(TextFieldTTF*pSender){return false;}
	virtual bool onTextFieldDetachWithIME(TextFieldTTF*pSender){return false;}
	virtual bool onTextFieldInsertText(TextFieldTTF *pSender, const char *text, int nLen){return true;}
	virtual bool onTextFieldDeleteBackward(TextFieldTTF *pSender, const char *delText, int nLen){return true;}
	virtual void keyboardWillShow(IMEKeyboardNotificationInfo &info){}
	virtual void keyboardDidShow(IMEKeyboardNotificationInfo &info){}
	virtual void keyboardWillHide(IMEKeyboardNotificationInfo &info){}
	virtual void keyboardDidHide(IMEKeyboardNotificationInfo &info){}

	Menu* CreateButton(std::string szBtName ,const Vec2 &p , int tag );
	virtual void callbackBt(Ref *pSender);
	void onRemove();
    void initGoodsLayer();
    void initWayLayer();
    void update(float dt);
    
    virtual void paycallback(const char *szOrderNum);
	void UpdateButtonWay(float deltaTime);
    
public:
    Layer*          m_GoodSLayer;
    Layer*          m_WayLayer;
	Sprite*         m_payback;
    int             m_GoodIndex;
	int             m_cbType;
    std::string     m_strPayCallBack;
    bool            m_loadres;
};
#endif
