﻿#pragma once
#include "thread.h"
#include "cocos2d.h"
#include "TcpClientSocket.h"
#include <vector>

USING_NS_CC;

struct MsgInfo
{
	CMD_Command Command;
	char		szBuffer[SOCKET_BUFFER];
	WORD		wDataSize;
};

//Tcp网络消息线程
class CTcpClientThread :
	public TcpClientSocket, public CThread, public Node
{

public:
	virtual void Run();

	virtual bool RecvPack();

private:
	std::vector<MsgInfo>		m_vecMsgInfo;		//消息队列
	pthread_mutex_t				m_MsgLock;			//收到消息队列锁

	bool						m_bClose;			//网络断开
    size_t                      m_lostCount = 0;
    
public:
	CTcpClientThread(void);
	~CTcpClientThread(void);

 	//开始接收消息
 	void StartReceive();
 	//停止接收消息
 	void StopReceive();

	virtual void CloseSocket();

	virtual void update(float delta);

};

