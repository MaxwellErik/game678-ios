﻿#ifndef _DEF_H_
#define _DEF_H_

// #define GAME_PLATFORM PLATFORM_YDMM
// #define GAME_OPENFLAT OPENFLAT_UZ
// #define GAME_PLATFORM PLATFORM_XM
// #define GAME_OPENFLAT OPENFLAT_XM
#define GAME_PLATFORM OPENFLAT_UZ				//TODO代理商（宏）
#define GAME_OPENFLAT OPENFLAT_UZ

typedef char                CHAR;
typedef signed char         INT8;
typedef unsigned char       UCHAR;
typedef unsigned char       UINT8;
typedef unsigned char       BYTE;
typedef short               SHORT;
typedef signed short        INT16;
typedef unsigned short      USHORT;
typedef unsigned short      UINT16;
typedef unsigned short      WORD;
typedef int                 INT;
typedef signed int          INT32;
typedef unsigned int        UINT;
typedef unsigned int        UINT32;
typedef long                LONG;

#ifdef __LP64__
typedef int                 IosDword;
#else
typedef unsigned long       IosDword;
#endif
typedef unsigned long       ULONG;
typedef unsigned long       DWORD;
typedef long long           LONGLONG;
typedef long long           LONG64;
typedef signed long long    INT64;
typedef unsigned long long  ULONGLONG;
typedef unsigned long long  DWORDLONG;
typedef unsigned long long  ULONG64;
typedef unsigned long long  DWORD64;
typedef unsigned long long  UINT64;

typedef DWORD				COLORREF;

#ifndef TEXT
#define TEXT(quote) quote         // r_winnt
#endif

#ifndef _TCHAR_DEFINED
typedef char TCHAR, *PTCHAR;
typedef unsigned char TBYTE , *PTBYTE ;
#define _TCHAR_DEFINED
#endif /* !_TCHAR_DEFINED */

#ifndef FALSE
#define FALSE               0
#endif

#ifndef TRUE
#define TRUE                1
#endif



#ifndef MAKEWORD
#define MAKEWORD(a, b)      ((WORD)(((BYTE)(((DWORD_PTR)(a)) & 0xff)) | ((WORD)((BYTE)(((DWORD_PTR)(b)) & 0xff))) << 8))
#define MAKELONG(a, b)      ((LONG)(((WORD)(((DWORD_PTR)(a)) & 0xffff)) | ((DWORD)((WORD)(((DWORD_PTR)(b)) & 0xffff))) << 16))
#define LOWORD(l)           ((WORD)(((DWORD_PTR)(l)) & 0xffff))
#define HIWORD(l)           ((WORD)((((DWORD_PTR)(l)) >> 16) & 0xffff))
#define LOBYTE(w)           ((BYTE)(((DWORD_PTR)(w)) & 0xff))
#define HIBYTE(w)           ((BYTE)((((DWORD_PTR)(w)) >> 8) & 0xff))
#endif

#ifndef GUID_DEFINED
#define GUID_DEFINED
#if defined(__midl)
typedef struct {
	unsigned long  Data1;
	unsigned short Data2;
	unsigned short Data3;
	byte           Data4[ 8 ];
} GUID;
#else
typedef struct _GUID {
	unsigned long  Data1;
	unsigned short Data2;
	unsigned short Data3;
	unsigned char  Data4[ 8 ];
} GUID;
#endif
#endif


#if(CC_TARGET_PLATFORM != CC_PLATFORM_WIN32)
#define MoveMemory(Destination,Source,Length) memmove((Destination),(Source),(Length))
#define CopyMemory(Destination,Source,Length) memcpy((Destination),(Source),(Length))
#define FillMemory(Destination,Length,Fill) memset((Destination),(Fill),(Length))
#define ZeroMemory(Destination,Length) memset((Destination),0,(Length))

#define lstrcpyn(Destination,Source,Length) strcpy(Destination, Source)
#endif


//数组维数
#ifndef CountArray
#define CountArray(Array) (sizeof(Array)/sizeof(Array[0]))
#endif

#define LAYER_CREATE_FUNC(__TYPE__) \
	static __TYPE__* create(GameScene *pGameScene) \
{ \
	__TYPE__ *pRet = new __TYPE__(pGameScene); \
	if (pRet && pRet->init()) \
{ \
	pRet->autorelease(); \
	return pRet; \
} \
	else \
{ \
	delete pRet; \
	pRet = NULL; \
	return NULL; \
} \
}

#endif

