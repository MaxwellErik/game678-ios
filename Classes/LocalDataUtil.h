﻿#ifndef __LOCALDATAUTIL_H__
#define __LOCALDATAUTIL_H__

#include <string>
#include "GlobalUnits.h"

//游客登录的账号
#define	DATA_KEY_USERNAME							"__DATA_NewGame_KEY_USERNAME__"

//悠哉游戏的账号
#define DATA_KEY_UZ_USERCount						"__DATA_NewGame_KEY_USERCOUNT__"	//用户数量

#define	DATA_KEY_UZ_USERNAME						"__DATA_NewGame_KEY_UZ_USERNAME__"
#define	DATA_KEY_UZ_PASSWORD						"__DATA_NewGame_KEY_UZ_PASSWORD__"
#define	DATA_KEY_SAVE_PASSWORD						"__DATA_NewGame_KEY_SAVE_PASSWORD__"

#define	DATA_KEY_UZ_USERNAME1						"__DATA_NewGame_KEY_UZ_USERNAME1__"
#define	DATA_KEY_UZ_PASSWORD1						"__DATA_NewGame_KEY_UZ_PASSWORD1__"
#define	DATA_KEY_SAVE_PASSWORD1						"__DATA_NewGame_KEY_SAVE_PASSWORD1__"

#define	DATA_KEY_UZ_USERNAME2						"__DATA_NewGame_KEY_UZ_USERNAME2__"
#define	DATA_KEY_UZ_PASSWORD2						"__DATA_NewGame_KEY_UZ_PASSWORD2__"
#define	DATA_KEY_SAVE_PASSWORD2						"__DATA_NewGame_KEY_SAVE_PASSWORD2__"

#define	DATA_KEY_UZ_USERNAME3						"__DATA_NewGame_KEY_UZ_USERNAME3__"
#define	DATA_KEY_UZ_PASSWORD3						"__DATA_NewGame_KEY_UZ_PASSWORD3__"
#define	DATA_KEY_SAVE_PASSWORD3						"__DATA_NewGame_KEY_SAVE_PASSWORD3__"

#define	DATA_KEY_UZ_USERNAME4						"__DATA_NewGame_KEY_UZ_USERNAME4__"
#define	DATA_KEY_UZ_PASSWORD4						"__DATA_NewGame_KEY_UZ_PASSWORD4__"
#define	DATA_KEY_SAVE_PASSWORD4						"__DATA_NewGame_KEY_SAVE_PASSWORD4__"

#define DATA_KEY_SETUP								"__DATA_NewGame_KEY_SETUP__"
#define DATA_KEY_GUEST_USERID						"__DATA_NewGame_GUEST_USERID__"
#define DATA_KEY_GUEST_PASS							"__DATA_NewGame_KEY_GUEST_PASS__"

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
#define DATA_KEY_BUYRECORD							"IOSBuyRecordInfo.dat"
#else
#define DATA_KEY_BUYRECORD							"BuyRecordInfo.dat"
#endif

#define WknhFishDataFile								"wknh/FishData.xml"

class LocalDataUtil
{
public:
	LocalDataUtil(void);
	~LocalDataUtil(void);

public:
	//保存账号
	static void SaveUserName(const char * szUserName, const char *szUserNameType = DATA_KEY_USERNAME);
	static const char * GetUsername(char * szUserName, const char *szUserNameType = DATA_KEY_USERNAME);
	//保存密码
	static void SavePassword(const char * szPassword, const char *szPasswordType = DATA_KEY_GUEST_PASS);
	static void GetPassword(char * szPassword, const char *szPasswordType = DATA_KEY_GUEST_PASS);

	//是否保存密码
	static void SavePasswordIsOk(bool IsSave, char *szIsSavePsdType);
	static bool GetPasswordIsOk(char *szIsSavePsdType);

	static void SaveUserId(DWORD dwUserID);
	static DWORD GetUserID();

	static void SaveSetup(int iIndex , bool bCheck);
	static bool GetSetup( int iIndex );

	static void SaveSoundValue(float fd);
	static void SaveBackMusicValue( float fd );

	static float GetSoundValue();
	static float GetBackMusicValue();
public:
	static void setObject(char *szKey ,const void *pData , unsigned short wDataSize);

	static void getObject(char *szKey , void *pData);

	static void writeFile(char *szFileName , void *pData , WORD wDataSize, bool bNew = false);	//写文件
	static bool readFile(char *szFileName , void *pData , ssize_t &size);	//读文件


	static void readFileBuyRecord(DWORD dwUserID, std::vector<BuyRecordInfo> &vcRecord);	//读取购买记录
	static void writeBuyRecord(DWORD dwUserID, BuyRecordInfo *info, bool bNew = false);						//写入购买记录

	//static void Load_wknh_FishData();

};

#endif
