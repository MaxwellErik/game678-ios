﻿#include "LobbyLayer.h"
#include "LoginLayer.h"
#include "LobbySocketSink.h"
#include "ClientSocketSink.h"
#include "GameLayerMove.h"
#include "JniSink.h"
#include "GameUserInfo.h"
#include "GameBindPhone.h"
#include "GameBuyLayer.h"
#include "GameSetingLayer.h"
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
#include "GoodsByWeChatPay.h"
#include "systemInfo.h"
#include "CPasteboard.h"
#endif
#include "Encrypt.h"
#include "Convert.h"
#include "localInfo.h"
#include "MainSetting.h"
#include "GameUpgrade.h"
#include "GameRank.h"
#include "SimpleAudioEngine.h"
#include "GameFishSendEvent.h"

#define ANDROID_VERIFY false

LobbyLayer::LobbyLayer(GameScene *pGameScene):GameLayer(pGameScene)
{
	m_GameKind = NULL;
	m_BankPass = NULL;
    m_Mainsetting = NULL;
    g_GlobalUnits.m_IsBankOpen = false;
}

LobbyLayer::~LobbyLayer()
{
     this->unschedule(schedule_selector(LobbyLayer::SendHeartData));
    _eventDispatcher->removeAllEventListeners();
}

void LobbyLayer::AddEnterRoomTime()
{
	schedule(schedule_selector(LobbyLayer::KillEnterRoomTime),10.0f);
}

void LobbyLayer::KillEnterRoomTime(float deltaTime)
{
	unschedule(schedule_selector(LobbyLayer::KillEnterRoomTime));
	ClientSocketSink::sharedClientSocket()->CloseSocket();
	LoadLayer::removeLoadLayer(Director::getInstance()->getRunningScene());
	char info[] = "\u8fde\u63a5\u623f\u95f4\u5931\u8d25\uff0c\u8bf7\u68c0\u67e5\u60a8\u7684\u7f51\u7edc\u6216\u91cd\u65b0\u8fde\u63a5\u3002";
	AlertMessageLayer::createConfirm(this, info);
}

void LobbyLayer::KillEnterRoomTime(bool removeLoad)
{
	unschedule(schedule_selector(LobbyLayer::KillEnterRoomTime));
    if (removeLoad)
        LoadLayer::removeLoadLayer(Director::getInstance()->getRunningScene());
}

void LobbyLayer::AddEnterGameTime()
{
	schedule(schedule_selector(LobbyLayer::KillEnterGameTime),10.0f);
}

void LobbyLayer::KillEnterGameTime(float deltaTime)
{
	unschedule(schedule_selector(LobbyLayer::KillEnterGameTime));
	LoadLayer::removeLoadLayer(Director::getInstance()->getRunningScene());
	ClientSocketSink::sharedClientSocket()->CloseSocket();
    char info[] = "\u8fdb\u5165\u6e38\u620f\u5931\u8d25\uff0c\u8bf7\u68c0\u67e5\u60a8\u7684\u7f51\u7edc\uff0c\u70b9\u51fb\u201c\u786e\u5b9a\u201d\u8fd4\u56de\u623f\u95f4\u5217\u8868\u3002";
	AlertMessageLayer::createConfirm(this, info,
		menu_selector(LobbyLayer::GoToRoom));
}

void LobbyLayer::GoToRoom(Ref *pSender)
{
	this->RemoveAlertMessageLayer();
	this->SetAllTouchEnabled(true);
	GameLayerMove::sharedGameLayerMoveSink()->GoTableToKind();
}

void LobbyLayer::KillEnterGameTime()
{
	unschedule(schedule_selector(LobbyLayer::KillEnterGameTime));
	LoadLayer::removeLoadLayer(Director::getInstance()->getRunningScene());
}

void LobbyLayer::playBackMusic(float dt)
{
    SoundUtil::sharedEngine()->playBackMusic("dating", true);
    SoundUtil::sharedEngine()->setBackSoundVolume(g_GlobalUnits.m_fBackMusicValue);
}

void LobbyLayer::UpdateUserData()
{
	if(m_GameKind != NULL)
    {
        m_GameKind->addGameToPage();
        m_GameKind->sendFishDataToLua();
    }
    SoundUtil::sharedEngine()->stopBackMusic();
    CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("huanyingyu.wav" , false);
    SoundUtil::sharedEngine()->setSoundVolume(g_GlobalUnits.m_fSoundValue);
    
    scheduleOnce(schedule_selector(LobbyLayer::playBackMusic), 8.0f);
	Node* pView = getChildByTag(1);
	AddUserInfo(pView);
    AddButton();
    
    if (g_GlobalUnits.m_UserLoginInfo.isTourist)
    {
        GameUpgrade::create(m_pGameScene);
    }
    else if (g_GlobalUnits.m_FreeAccount/* && 1 == g_GlobalUnits.m_FreeWeixin*/)
    {
        //显示捕鱼活动引导
        GameFishSendEvent::create(m_pGameScene);
    }
    
    showUserInfo();
}

void LobbyLayer::StartGameHeart()
{
    schedule(schedule_selector(LobbyLayer::SendGameHeart), 3.0f);
}

void LobbyLayer::StopGameHeart()
{
    unschedule(schedule_selector(LobbyLayer::SendGameHeart));
}

void LobbyLayer::SendGameHeart(float dt)
{
     ClientSocketSink::sharedSocketSink()->SendData(MDM_GR_USER, SUB_GF_HEARTBEAT);
}

void LobbyLayer::SendHeartData(float dt)
{
    if (g_GlobalUnits.m_isLoadLayerShow)
        return;
    
    CMD_GP_Hearbeat heart;
    heart.dwHearbeat = g_GlobalUnits.m_HeartCount;
    heart.dwUserID = g_GlobalUnits.GetUserID();
    heart.llToken = g_GlobalUnits.GetGolbalUserData().lWebToken;
    LobbySocketSink::sharedSocketSink()->ConnectServer();
    LobbySocketSink::sharedLoginSocket()->SendData(MDM_GP_USER, SUB_GP_HEARBEAT,&heart, sizeof(CMD_GP_Hearbeat));
}

void LobbyLayer::SendBoardCastData(float dt)
{
    if (g_GlobalUnits.m_isLoadLayerShow)
        return;
    
    CMD_GP_Broadcast baordcast;
    baordcast.LastTime = g_GlobalUnits.m_boradcastTime;
    LobbySocketSink::sharedSocketSink()->ConnectServer();
    LobbySocketSink::sharedLoginSocket()->SendData(MDM_GP_USER, SUB_GM_BROADCAST_REQ, &baordcast, sizeof(CMD_GP_Broadcast));
}

void LobbyLayer::OnSendFishFreecoin()
{
    CMD_Fish_Freecoin Freecoin;
    Freecoin.dwUserID = g_GlobalUnits.GetUserID();
    LobbySocketSink::sharedSocketSink()->ConnectServer();
    LobbySocketSink::sharedLoginSocket()->SendData(MDM_GP_USER, SUB_FISH_FREECOIN_REQ, &Freecoin, sizeof(CMD_Fish_Freecoin));
}

void LobbyLayer::AddGameBoardCast()
{
    Layer* bg = Layer::create();
    addChild(bg, 0, 999);
    
    Sprite *sp = Sprite::create("DaTing/DaTingBack.jpg");
    auto pMenuItem = MenuItemSprite::create(sp, sp, sp);
    Menu* pTouchDisableMenu = Menu::create(pMenuItem, nullptr);
    pTouchDisableMenu->setPosition(Vec2(960, 540));
    bg->addChild(pTouchDisableMenu);
    
    Label* title = Label::createWithSystemFont("健康游戏忠告", _GAME_FONT_NAME_1_, 80);
    title->setPosition(Vec2(960, 600));
    bg->addChild(title);
    
    Label* firstLine =Label::createWithSystemFont("抵制不良游戏，拒绝盗版游戏。", _GAME_FONT_NAME_1_, 46);
    firstLine->setPosition(Vec2(960, 500));
    bg->addChild(firstLine);
    
    Label* secondLine =Label::createWithSystemFont("注意自我保护，谨防受骗上当。", _GAME_FONT_NAME_1_, 46);
    secondLine->setPosition(Vec2(960, 440));
    bg->addChild(secondLine);
    
    Label* thirdLine =Label::createWithSystemFont("适度游戏益脑，沉迷游戏伤身。", _GAME_FONT_NAME_1_, 46);
    thirdLine->setPosition(Vec2(960, 380));
    bg->addChild(thirdLine);
    
    Label* forthLine =Label::createWithSystemFont("合理安排时间，享受健康生活。", _GAME_FONT_NAME_1_, 46);
    forthLine->setPosition(Vec2(960, 320));
    bg->addChild(forthLine);
    
    Label* owner =Label::createWithSystemFont("游戏著作权人：广州凤栖互联网科技有限公司    批准文号：新广出审[2016]4745号", _GAME_FONT_NAME_1_, 46);
    owner->setPosition(Vec2(960, 180));
    bg->addChild(owner);

    Label* company =Label::createWithSystemFont("出版服务单位：广州凤栖互联网科技有限公司   出版物号：ISBN 978-7-7979-3196-0", _GAME_FONT_NAME_1_, 46);
    company->setPosition(Vec2(960, 100));
    bg->addChild(company);

}

void LobbyLayer::LoadLoginLayer(float dt)
{
    m_LoginLayer = LoginLayer::create(m_pGameScene);
    auto child = getChildByTag(999);
    if (child)
        child->removeFromParent();
}

bool LobbyLayer::init()
{
	if ( !Layer::init() )	return false;
	setLocalZOrder(0);
	StopBackMusic();
    Tools::addSpriteFrame("DaTing/datingui.plist");
	Tools::addSpriteFrame("LoadingBar.plist");
    
    Sprite *pView = Sprite::create("DaTing/DaTingBack.jpg");
    pView->setAnchorPoint(Vec2(0,0));
    pView->setPosition(Vec2(0,0));
    addChild(pView);
    
    Sprite* titleBg = Sprite::createWithSpriteFrameName("bg_titlemain.png");
    titleBg->setPosition(Vec2(_STANDARD_SCREEN_CENTER_.x, 1030));
    addChild(titleBg);
    
    Sprite* title = Sprite::createWithSpriteFrameName("bg_title.png");
    title->setPosition(titleBg->getContentSize().width/2, 82);
    titleBg->addChild(title);
    
	//开启触摸
    auto listener = EventListenerTouchOneByOne::create();
    listener->onTouchBegan = CC_CALLBACK_2(LobbyLayer::onTouchBegan, this);
    listener->onTouchMoved = CC_CALLBACK_2(LobbyLayer::onTouchMoved, this);
    listener->onTouchEnded = CC_CALLBACK_2(LobbyLayer::onTouchEnded, this);
    _eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);
	setTouchEnabled(true);
    
    auto _listener = EventListenerCustom::create("CHANGE_ACCOUNT", CC_CALLBACK_1(LobbyLayer::changeAccount, this));
    _eventDispatcher->addEventListenerWithFixedPriority(_listener, 1);
    
    auto _listener1 = EventListenerCustom::create("APP_ENTER_FOREGROUND_EVENT", CC_CALLBACK_1(LobbyLayer::SendFishFreecoin, this));
    _eventDispatcher->addEventListenerWithFixedPriority(_listener1,1);
    
    GameLayerMove::sharedGameLayerMoveSink()->SetLobbyLayer(this);
    
    m_GameKind = GameKind::create(m_pGameScene);
    m_GameTable = GameTable::create(m_pGameScene);
    
    GameLayerMove::sharedGameLayerMoveSink()->SetGameKind(m_GameKind);
    GameLayerMove::sharedGameLayerMoveSink()->SetGameTable(m_GameTable);
    
    ClientSocketSink::sharedSocketSink()->SetLobbyLayer(this);
    LobbySocketSink::sharedSocketSink()->SetLobbyLayer(this);
    
    if (static_isFirstEnter)
    {
        if (ANDROID_VERIFY)
        {
            AddGameBoardCast();

            scheduleOnce(schedule_selector(LobbyLayer::LoadLoginLayer), 5.0f);
        }
        else
        {
            m_LoginLayer = LoginLayer::create(m_pGameScene);
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
            GoodsByWeChatPay_cpp::shared()->GetIOSAuditState();
#endif
        }
        
        static_isFirstEnter = false;
    }
    else
    {
        if(m_GameKind != NULL)
        {
            m_GameKind->addGameToPage();
        }
        Node* pView = getChildByTag(1);
        AddUserInfo(pView);
        
        AddButton();
    }
    
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
	Application::getInstance()->LockAutoScreen();
#endif
    return true;
}

void LobbyLayer::OnRecvRankData(CMD_RankingQuery_Result* rank)
{
    GameRank* rankLayer = GameRank::create(m_pGameScene);
    rankLayer->setRankData(rank);
}

void LobbyLayer::SendHeart()
{
    this->schedule(schedule_selector(LobbyLayer::SendHeartData), 40);
}

void LobbyLayer::SendBoardCastReq()
{
    this->schedule(schedule_selector(LobbyLayer::SendBoardCastData), 30);
}

void LobbyLayer::GetServerAddress()
{
    LobbySocketSink::sharedLoginSocket()->SendData(MDM_GP_USER, SUB_GP_FORWARDSERVER);
}

LobbyLayer *LobbyLayer::create(GameScene *pGameScene)
{
    LobbyLayer *ptempLobbyLayer = new LobbyLayer(pGameScene);
    if(ptempLobbyLayer && ptempLobbyLayer->init())
    {
        ptempLobbyLayer->autorelease();
        return ptempLobbyLayer;
    }
    else
    {
        CC_SAFE_DELETE(ptempLobbyLayer);
        return NULL;
    }
}

void LobbyLayer::onEnter()
{
	GameLayer::onEnter();
}

void LobbyLayer::onExit()
{
	GameLayer::onExit();
	Tools::removeSpriteFrameCache("LoadingBar.plist");
	Tools::removeSpriteFrameCache("DaTing/datingui.plist");
}

void LobbyLayer::UpdateGameTableUserStatus(tagUserData *pUserData)
{
    m_GameTable->OnEventUserStatus(pUserData);
}

void LobbyLayer::updateUserInfo(float deltaTime)
{
    int state = LocalInfo::getCurNetInfo();

    if(state == NetWorkTypeNone)
    {
        m_netInfo->setSpriteFrame("wifi1.png");
    }
    else if(state == NetWorkType2G)
    {
        m_netInfo->setSpriteFrame("2G.png");
    }
    else if(state == NetWorkType3G)
    {
        m_netInfo->setSpriteFrame("3G.png");
    }
    else if(state == NetWorkType4G)
    {
        m_netInfo->setSpriteFrame("4G.png");
    }
    else if(state == NetWorkTypeWiFI)
    {
        m_netInfo->setSpriteFrame("wifi3.png");
    }

    float p = LocalInfo::getCurPowerInfo();
    m_power->setScaleX(p);

    struct timeval tv;
    gettimeofday(&tv, 0);
    struct tm *lTime = localtime(&tv.tv_sec);
    
    char timeArr[20];
    memset(timeArr, 0, sizeof(timeArr));
    if (lTime->tm_hour > 12)
    {
        sprintf(timeArr, "%d:%02d:%02d PM", lTime->tm_hour-12, lTime->tm_min, lTime->tm_sec);
    }
    else
    {
        sprintf(timeArr, "%d:%02d:%02d PM", lTime->tm_hour, lTime->tm_min, lTime->tm_sec);
    }
   
    m_timeLabel->setString(timeArr);
}

void LobbyLayer::initGameUserInfo()
{
    m_userInfo = GameUserInfo::create(m_pGameScene);
    m_userInfo->SetUserInfo(g_GlobalUnits.GetGolbalUserData().szNickName, g_GlobalUnits.GetGolbalUserData().dwGameID, g_GlobalUnits.GetGolbalUserData().wFaceID);
}

bool LobbyLayer::onTouchBegan(Touch *pTouch, Event *pEvent)
{
    Vec2 localPos = convertTouchToNodeSpace(pTouch);
    cocos2d::Rect rc =cocos2d::Rect(0, 920, 160, 160);
    if (rc.containsPoint(localPos))
    {
        initGameUserInfo();
    }
	return true;
}

void LobbyLayer::AddUserInfo(Node *pView)
{
	removeChildByTag(2000);
	removeChildByTag(2001);
	removeChildByTag(2002);
	
    Sprite* headBg = Sprite::createWithSpriteFrameName("touxiangkuang.png");
    headBg->setPosition(Vec2(80, _STANDARD_SCREEN_SIZE_.height - 80));
    addChild(headBg, 2, 2003);
    
    ClippingNode *node = ClippingNode::create(Sprite::createWithSpriteFrameName("touxiang.png"));
    node->setInverted(false);
    node->setAlphaThreshold(0.5f);
    node->setPosition(Vec2(80, _STANDARD_SCREEN_SIZE_.height - 80));
    addChild(node,1, 2006);
    
    string heads = g_GlobalUnits.getFace(g_GlobalUnits.GetGolbalUserData().wGender, g_GlobalUnits.GetGolbalUserData().lInsureScore);
    Sprite* mHead = Sprite::createWithSpriteFrameName(heads);
    mHead->setPosition(Vec2::ZERO);
	mHead->setTag(2000);
	node->addChild(mHead);

	//昵称id
    Sprite* nameBg = Sprite::createWithSpriteFrameName("nichendi.png");
    nameBg->setAnchorPoint(Vec2(0, 0.5f));
    nameBg->setPosition(Vec2(110, _STANDARD_SCREEN_SIZE_.height - 45));
    addChild(nameBg,0,2004);

	Label *fontl = Label::createWithSystemFont(g_GlobalUnits.GetGolbalUserData().szNickName ,_GAME_FONT_NAME_1_,46);
	fontl->setAnchorPoint(Vec2(0,0.5f));
	fontl->setPosition(Vec2(155,_STANDARD_SCREEN_SIZE_.height - 45));
	fontl->setTag(2001);
	addChild(fontl);

	//id
    Sprite* idBg = Sprite::createWithSpriteFrameName("nichendi.png");
    idBg->setAnchorPoint(Vec2(0, 0.5f));
    idBg->setPosition(Vec2(110, _STANDARD_SCREEN_SIZE_.height - 115));
    addChild(idBg, 0, 2005);
    
	char strc[32]="";
	sprintf(strc,"ID:%d", g_GlobalUnits.GetGolbalUserData().dwGameID);
	Label* useridfont=Label::createWithSystemFont(strc, _GAME_FONT_NAME_1_,46);
	useridfont->setAnchorPoint(Vec2(0,0.5f));
	useridfont->setPosition(Vec2(150, _STANDARD_SCREEN_SIZE_.height - 115));
	useridfont->setTag(2002);
	addChild(useridfont);
    
    //网络状况
    m_netInfo = Sprite::createWithSpriteFrameName("2G.png");
    m_netInfo->setPosition(Vec2(520, _STANDARD_SCREEN_SIZE_.height - 45));
    addChild(m_netInfo);
    m_netInfo->setVisible(false);
    
    //电池电量
    m_powerBg = Sprite::createWithSpriteFrameName("power_bg.png");
    m_powerBg->setPosition(Vec2(650, _STANDARD_SCREEN_SIZE_.height - 45));
    addChild(m_powerBg);
    m_powerBg->setVisible(false);
    
    m_power = Sprite::createWithSpriteFrameName("power_full.png");
    m_power->setScaleX(LocalInfo::getCurPowerInfo());
    m_power->setAnchorPoint(Vec2(0,0.5));
    m_power->setPosition(Vec2(605, _STANDARD_SCREEN_SIZE_.height - 45));
    addChild(m_power);
    m_power->setVisible(false);
    
    //当前时间
    m_timeLabel = Label::createWithSystemFont("", _GAME_FONT_NAME_1_, 46);
    m_timeLabel->setPosition(Vec2(600, _STANDARD_SCREEN_SIZE_.height - 115));
    addChild(m_timeLabel);
    m_timeLabel->setVisible(false);
    
    Sprite* goldBg = Sprite::createWithSpriteFrameName("label_money.png");
    goldBg->setPosition(Vec2(1480, _STANDARD_SCREEN_SIZE_.height - 80));
    addChild(goldBg, 0, goldBgTag);
    
    auto str = StringUtils::toString(g_GlobalUnits.GetGolbalUserData().lInsureScore);
    Label* goldLabel = Label::createWithSystemFont(str, _GAME_FONT_NAME_1_, 46);
    goldLabel->setAnchorPoint(Vec2(0, 0.5f));
    goldLabel->setPosition(Vec2(1330, _STANDARD_SCREEN_SIZE_.height - 80));
    addChild(goldLabel, 0, goldLabelTag);
    
    schedule(schedule_selector(LobbyLayer::updateUserInfo),0.5f);
}

void LobbyLayer::showUserInfo()
{
    m_netInfo->setVisible(true);
    m_power->setVisible(true);
    m_powerBg->setVisible(true);
    m_timeLabel->setVisible(true);
}

void LobbyLayer::OnIosRechargeSuccess(int payType)
{
    CMD_MB_ApplePayRequest request;
    request.userid = g_GlobalUnits.GetUserID();
    request.score = (int)BUY_GOLD_IOS[payType][0];
    request.pass = g_GlobalUnits.m_YZPASS;
    LobbySocketSink::sharedLoginSocket()->SendData(MDM_MB_LOGON, SUB_MB_APPLE_PAY_REQUEST, &request, sizeof(request));
}

void LobbyLayer::UpdataLobbyScore()
{
    auto str = StringUtils::toString(g_GlobalUnits.GetGolbalUserData().lInsureScore);
    Label* l = (Label*)getChildByTag(goldLabelTag);
    if (l)
        l->setString(str);
}

string LobbyLayer::AddCommaToNum(LONG Num)
{
	char _str[256];
	sprintf(_str,"%ld", Num);
	string _string = _str;
	auto step = _string.length()/3;
	for (int i = 1; i <= step; i++)
	{
		_string.insert(_string.length()-(i-1)-(i*3), ",");
	}
	return _string;
}

void LobbyLayer::AddButton()
{
    Sprite* btnBg = Sprite::createWithSpriteFrameName("icondi.png");
    btnBg->setPosition(Vec2(_STANDARD_SCREEN_CENTER_.x, 55));
    addChild(btnBg, 0, 2007);
 
    Menu *rank = CreateButton("rank" ,Vec2(300,100),RankBtn);
    addChild(rank , 0 , RankBtn);
    
    Menu *bind = CreateButton("bind" ,Vec2(630,100),BindBtn);
    addChild(bind , 0 , BindBtn);

    Menu *addSend = CreateButton("addSend" ,Vec2(960,100),AddSendBtn);
    addChild(addSend , 0 , AddSendBtn);

    Menu *kefu = CreateButton("kefu" ,Vec2(1290,100),KeFuBtn);
    addChild(kefu , 0 , KeFuBtn);

	Menu *addmoeny = CreateButton("addMoney" ,Vec2(1620,100),AddMoneyBtn);
	addChild(addmoeny , 0 , AddMoneyBtn);

    Menu *btn_back = CreateButton("settingmain" ,Vec2(1800,1000),SettingBtn);
    addChild(btn_back , 0 , SettingBtn);
}

void LobbyLayer::SetBankDisable(bool ebale)
{
	Node * temp = getChildByTag(AddSendBtn);
	if (temp != NULL)
	{
		temp->setVisible(ebale);
	}
}

void LobbyLayer::SetCheckPhone(bool _check)
{
	g_GlobalUnits.m_bCheckPhone = _check;
}

Menu* LobbyLayer::CreateButton(std::string szBtName ,const Vec2 &p , int tag )
{
	return Tools::Button(StrToChar(szBtName+"_normal.png") , StrToChar(szBtName+"_click.png") , p, this , menu_selector(LobbyLayer::callbackBt) , tag);
}

void LobbyLayer::FlashBtn()
{
	LobbySocketSink::sharedSocketSink()->SetLobbyLayer(this);
	CMD_C_GP_RefreshScore temp;
	ZeroMemory(&temp ,sizeof(temp));
	temp.dwUserID = g_GlobalUnits.GetUserID();
	sprintf(temp.szPassWord,"%s",g_GlobalUnits.GetGolbalUserData().szPassWord);
	LobbySocketSink::sharedSocketSink()->ConnectServer();
	LobbySocketSink::sharedLoginSocket()->SendData(MDM_GP_MOBILE, SUB_C_GP_REFRESH_SCORE,&temp, sizeof(CMD_C_GP_RefreshScore));
}

void LobbyLayer::GoToLogin(Ref *pSender)
{
    if (isScheduled(schedule_selector(LobbyLayer::updateUserInfo)))
        unschedule(schedule_selector(LobbyLayer::updateUserInfo));
    
    this->unschedule(schedule_selector(LobbyLayer::SendHeartData));
    g_GlobalUnits.m_HeartCount = 0;
	g_GlobalUnits.m_ServerListManager.ClearAllData();
	g_GlobalUnits.m_UserDataVec.clear();
	g_GlobalUnits.m_UserLoginInfo.isWxUser = false;
    g_GlobalUnits.m_LogonBank_IsOk = false;
    m_GameKind->backTologin();
    
    for (int i = 2000; i <= 2007; i++)
        removeChildByTag(i);
    
    
    m_netInfo->removeFromParent();
    m_power->removeFromParent();
    m_powerBg->removeFromParent();
    m_timeLabel->removeFromParent();
    
    getChildByTag(goldBgTag)->removeFromParent();
    getChildByTag(SettingBtn)->removeFromParent();
    getChildByTag(AddMoneyBtn)->removeFromParent();
    getChildByTag(AddSendBtn)->removeFromParent();
    getChildByTag(goldLabelTag)->removeFromParent();
    getChildByTag(BindBtn)->removeFromParent();
    
    if (m_Mainsetting)
    {
        m_Mainsetting->OnRemove();
        m_Mainsetting = nullptr;
    }
    
    ClientSocketSink::sharedSocketSink()->CleaAllUser();
    ClientSocketSink::sharedSocketSink()->closeSocket();
	LobbySocketSink::sharedSocketSink()->closeSocket();
	m_LoginLayer = LoginLayer::create(m_pGameScene);
    m_LoginLayer->showWechatLoginBtn(g_GlobalUnits.m_SwitchYZ);
    
	this->RemoveAlertMessageLayer();
	this->SetAllTouchEnabled(true);
}

void LobbyLayer::SendFishFreecoin(EventCustom* event)
{
    OnSendFishFreecoin();
}

void LobbyLayer::changeAccount(EventCustom* event)
{
    GoToLogin(nullptr);
}

void LobbyLayer::callbackBt( Ref* pObject )
{
	Menu * p = (Menu *)pObject;
	if (!p->isVisible())
	{
		return;
	}
    
	switch(p->getTag())
	{
	case SettingBtn:  //设置按钮
		{
            m_Mainsetting = MainSetting::create(m_pGameScene);
			break;
		}
    case RankBtn: //排行榜
        {
            SoundUtil::sharedEngine()->playEffect("buttonMusic");
            LoadLayer::create(m_pGameScene);
            LobbySocketSink::sharedLoginSocket()->SendData(MDM_GP_USER, SUB_RANKING_QUERY);
            break;
        }
	case AddFlashBtn: //刷新按钮
		{
			SoundUtil::sharedEngine()->playEffect("buttonMusic");
			FlashBtn();
			break;
		}
    case KeFuBtn:
        {
            SoundUtil::sharedEngine()->playEffect("buttonMusic");
            JniSink::share()->OpenSystemWeb("http://crm2.qq.com/page/portalpage/wpa.php?uin=800181678&aty=0&a=0&curl=&ty=1");
            break;
        }
    case BindBtn:
        {
            
            if (1 == g_GlobalUnits.GetGolbalUserData().bBindWeChat)
            {
                AlertMessageLayer::createConfirm(this, "你已绑定微信");
                return;
            }
            char url[255] = {0};
            char _str[128] = {0};
            char _md5[33] = {0};
            
            LONGLONG sum = g_GlobalUnits.GetGolbalUserData().dwUserID +
                            g_GlobalUnits.GetGolbalUserData().dwGameID +
                            g_GlobalUnits.GetGolbalUserData().lWebToken;
            
            sprintf(_str,"hao22---!%lld---22yy", sum);
            
            CMD5Encrypt::EncryptData(_str, _md5);

            sprintf(url, "https://weixin.game678.net.cn/WeixinBind.aspx?u=%u&g=%u&t=%lld&m=%s",
                    g_GlobalUnits.GetGolbalUserData().dwUserID,
                    g_GlobalUnits.GetGolbalUserData().dwGameID,
                    g_GlobalUnits.GetGolbalUserData().lWebToken,_md5);
            
            #if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
                GoodsByWeChatPay_cpp::shared()->WxBind(url);
            #else
                JniSink::share()->OpenWebView(url);
            #endif
            break;
        }
	case AddSendBtn: //银行
		{
			SoundUtil::sharedEngine()->playEffect("buttonMusic");
			if (g_GlobalUnits.m_LogonBank_IsOk == false)
			{
				m_BankPass = GameBankPass::create(m_pGameScene);
			}
			else
			{
                sendRefreshBankScore();
			}
			
			break;
		}
	case AddMoneyBtn:
		{
			SoundUtil::sharedEngine()->playEffect("buttonMusic");
            GamePay::create(m_pGameScene);
            break;

		}
	}
}

void LobbyLayer::sendRefreshBankScore()
{
    CMD_GP_QueryGameInsureInfo temp;
    temp.dwUserID = g_GlobalUnits.GetGolbalUserData().dwUserID;
    sprintf(temp.szPassword,"%s",g_GlobalUnits.m_BankPassWord);
    temp.wKindID = Dating_GameKind_WZMJ;
    temp.lToken = g_GlobalUnits.GetGolbalUserData().lWebToken;
    
    if(LobbySocketSink::sharedSocketSink()->ConnectServer())
    {
        LobbySocketSink::sharedLoginSocket()->SendData(MDM_GF_BANK, SUB_MB_BANK_QUERY,&temp, sizeof(CMD_GP_QueryGameInsureInfo));
        LoadLayer::create(m_pGameScene);
    }
}

void LobbyLayer::removeTips(Ref *sender)
{
    Node* node = (Node*)sender;
    node->removeFromParent();
}

void LobbyLayer::modefiySuccess()
{
    if (m_userInfo)
        m_userInfo->removeFromParent();
    
    g_GlobalUnits.m_UserLoginInfo.isTourist = false;
    
    Label* name = (Label*)getChildByTag(2001);
    name->setString(g_GlobalUnits.GetGolbalUserData().szNickName);
    
    Sprite* tip = Sprite::createWithSpriteFrameName("tip_upgradeSuccess.png");
    tip->setGlobalZOrder(10000);
    tip->setPosition(Vec2(960, 540));
    addChild(tip, 10000);
    
    DelayTime *del = DelayTime::create(2.0f);
    CallFuncN  *fun = CallFuncN::create(CC_CALLBACK_1(LobbyLayer::removeTips, this));
    Sequence *s = Sequence::create(del, fun, NULL);
    tip->runAction(s);
}

void LobbyLayer::OpenBank()
{
    if (g_GlobalUnits.m_IsBankOpen)
        return;
    
    g_GlobalUnits.m_IsBankOpen = true;
	g_GlobalUnits.m_LogonBank_IsOk = true;
    
	m_pBankUILayer = GameBank::create(m_pGameScene);
	LobbySocketSink::sharedSocketSink()->SetBankLayer(m_pBankUILayer);
	LobbySocketSink::sharedSocketSink()->SetLobbyLayer(this);
}

void LobbyLayer::AddText()
{
	m_GameKind->AddText();
}

void LobbyLayer::AddGameBroadcast(char *msg)
{
    m_GameTable->AddGameBroadcast(msg);
}

void LobbyLayer::setRcvLayer(GameLayer *pGameLayer)
{
	m_pRcvLayer = pGameLayer;
}

// Alert Message 确认消息处理
void LobbyLayer::DialogConfirm(Ref *pSender)
{
	this->RemoveAlertMessageLayer();
	this->SetAllTouchEnabled(true);
}

// Alert Message 取消消息处理
void LobbyLayer::DialogCancel(Ref *pSender)
{
	this->RemoveAlertMessageLayer();
	this->SetAllTouchEnabled(true);
}

void LobbyLayer::UpdataGame()
{
    JniSink::share()->UpdataGame();
}
