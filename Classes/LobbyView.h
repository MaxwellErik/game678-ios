﻿#ifndef _LOBBYVIEW_H_
#define _LOBBYVIEW_H_

#include "GameLayer.h"
#include "AlertMessageLayer.h"

class LobbyView : public GameLayer
{
public:
	~LobbyView(void);

protected:
	LobbyView(GameScene *pGameScene);
//	void

public:
	void SendEnterRoomData(WORD wRoomType, WORD wKindID);

	void sendLoginData(const char * szUserName , const char * szPassword);	//悠哉账号登录

	void sendGuestLogin(const DWORD dwUserID , const char * szPassword);	//游客登录

	void sendXiaoMiLogin(const char * szOpenID , const char * szPassword, const char * szUserToken);	//小米账号登录

	void sendXiaoMiRegister(const char * szOpenID , const char * szNickName);	//小米账号注册

	bool ConnectServer();

	void selectServer(char * szServerAddr , int iPort);

	void showMessage(char *szMessage);

	//参数2,是否能登录
	void showVersionMessage(char *szMessage, bool bAllowConnect);

	virtual void DialogConfirmDownload(Ref *pSender);	//确认下载新版本

	virtual void DialogCancelDownload(Ref *pSender);	//取消下载新版本

	virtual void alone();			//进入单机版

	virtual void DialogConfirmAlong(Ref *pSender);
};

#endif
