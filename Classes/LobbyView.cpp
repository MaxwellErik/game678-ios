﻿#include "LobbyView.h"
#include "LobbySocketSink.h"
#include "Encrypt.h"
#include "ClientSocketSink.h"
#include "AlertMessageLayer.h"
#include "LocalDataUtil.h"
#include "JniSink.h"

#if (GAME_PLATFORM == PLATFORM_IOS)
#define DOWNLOAD_URL	"https://itunes.apple.com/us/app/lao-hu-ji-shui-hu-chuan-xia/id788482028?ls=1&mt=8"
#else
#define DOWNLOAD_URL	"http://shz.888qipai.com"
#endif

//#define  _UNICODE_CONNECT_FAILT 
#define  _UNICODE_CONNECT_FAILT_ALONG "\u7f51\u7edc\u8fde\u63a5\u5931\u8d25\uff0c\u662f\u5426\u8981\u8fdb\u5165\u5355\u673a\u7248\u6e38\u620f\uff1f"

LobbyView::LobbyView(GameScene *pGameScene):GameLayer(pGameScene)
{
	LobbySocketSink::sharedSocketSink()->setRcvLayer(this);
}


LobbyView::~LobbyView(void)
{
}

void LobbyView::SendEnterRoomData(WORD wRoomType, WORD wKindID)
{
	//CMD_GP_LinkServer temp;
	//temp.dwUserID = g_GlobalUnits.GetGolbalUserData().dwUserID;
	//temp.wKindID = wKindID;
	//temp.wRoomID = wRoomType;
	//temp.wLine = 0;
	//LobbySocketSink::sharedLoginSocket()->SendData(MDM_GP_LOGON,SUB_GP_LINK_SERVER,&temp,sizeof(temp));
}

void LobbyView::sendLoginData( const char * szUserName , const char * szPassword )
{
	if(ConnectServer())
	{
		LoadLayer::create(m_pGameScene);
		//连接成功
		TCHAR szEnPassword[33];
        
		CMD5Encrypt::EncryptData(szPassword,szEnPassword);


		CMD_MB_LogonAccounts LogonByAccounts;
		memset(&LogonByAccounts, 0x0, sizeof(LogonByAccounts));


		
	}
	else
	{
		showMessage(_UNICODE_CONNECT_FAILT);
		//AlertMessageLayer::createConfirmAndCancel(this , _UNICODE_CONNECT_FAILT_ALONG, menu_selector(LobbyView::DialogConfirmAlong));
	}
}

bool LobbyView::ConnectServer()
{
	return LobbySocketSink::sharedSocketSink()->ConnectServer();
}

void LobbyView::selectServer( char * szServerAddr , int iPort )
{
    ClientSocketSink::sharedSocketSink()->closeSocket();
	ClientSocketSink::sharedSocketSink()->ConnectServer(szServerAddr , iPort);
}

struct CMD_GP_LoginByOpenId
{
	TCHAR								szOpenId[64];			//腾讯OpenID
	TCHAR								szOpenKey[64];			//腾讯OpenKey
};

void LobbyView::sendGuestLogin( const DWORD dwUserID , const char * szPassword )
{
// 	if (g_GlobalUnits.GetUserID() != 0)
// 	{
// 		ClientSocketSink::sharedSocketSink()->AccountCheckFinish();
// 	}
// 	else 
	if(ConnectServer())
	{


		LoadLayer::create(m_pGameScene);
		//连接成功
		CMD_GP_LogonByUserID LogonByUserID;
		memset(&LogonByUserID, 0x00, sizeof(LogonByUserID));
		//LogonByUserID.dwPlazaVersion = MAKELONG(MAKEWORD(2,0),MAKEWORD(1,0));
		LogonByUserID.dwPlazaVersion = VER_PLAZA_FRAME;
		LogonByUserID.wKindID = 0;
		LogonByUserID.dwUserID = dwUserID;
		LogonByUserID.cbOpenPlat = OPENFLAT_GUEST;
		LogonByUserID.cbPlatform = GAME_PLATFORM;
		strcpy(LogonByUserID.szPassWord, szPassword);

		LobbySocketSink::sharedLoginSocket()->SendData(MDM_MB_LOGON,SUB_MB_LOGON_GAMEID,&LogonByUserID,sizeof(LogonByUserID));

	}
	else
	{
		showMessage(_UNICODE_CONNECT_FAILT);
		//AlertMessageLayer::createConfirmAndCancel(this , _UNICODE_CONNECT_FAILT_ALONG, menu_selector(LobbyView::DialogConfirmAlong));
	}
}

void LobbyView::showMessage( char *szMessage)
{
	AlertMessageLayer::createConfirm(this , szMessage);
}

void LobbyView::DialogConfirmDownload( Ref *pSender )
{
	LoadLayer::removeLoadLayer(Director::getInstance()->getRunningScene());
	GameLayer::DialogConfirm(pSender);

	JniSink::share()->OpenSystemWeb(DOWNLOAD_URL);
	
}

void LobbyView::DialogCancelDownload( Ref *pSender )
{
	GameLayer::DialogCancel(pSender);
	ClientSocketSink::sharedSocketSink()->setIsLogin(true);
	ClientSocketSink::sharedSocketSink()->LoginServerSuccess();
}


void LobbyView::showVersionMessage( char *szMessage, bool bAllowConnect)
{
	if (bAllowConnect)
	{
		AlertMessageLayer::createConfirmAndCancel(this , szMessage, menu_selector(LobbyView::DialogConfirmDownload), menu_selector(LobbyView::DialogCancelDownload));
	}
	else
	{
		AlertMessageLayer::createConfirm(this , szMessage, menu_selector(LobbyView::DialogConfirmDownload));
	}
	
}

void LobbyView::alone()
{

}

void LobbyView::DialogConfirmAlong(Ref *pSender )
{
	alone();
}


void LobbyView::sendXiaoMiLogin( const char * szOpenID , const char * szPassword, const char * szUserToken )
{
	if(ConnectServer())
	{
		CMD_GP_LogonByOpenID LogonByOpenID;
		memset(&LogonByOpenID, 0x00, sizeof(LogonByOpenID));
		LogonByOpenID.cbOpenPlat = OPENFLAT_XM;
		LogonByOpenID.cbPlatform = PLATFORM_XM;
		LogonByOpenID.dwPlazaVersion = VER_PLAZA_FRAME;
		strcpy(LogonByOpenID.szOpenID, szOpenID);
		strcpy(LogonByOpenID.szUserToken, szUserToken);
		strcpy(LogonByOpenID.szPassword, szPassword);
		LogonByOpenID.wKindID = 0;
		LobbySocketSink::sharedLoginSocket()->SendData(MDM_GP_LOGON,SUB_GP_LOGON_OPENID,&LogonByOpenID,sizeof(LogonByOpenID));
	}
	else
	{
		showMessage(_UNICODE_CONNECT_FAILT);
	}
}

void LobbyView::sendXiaoMiRegister( const char * szOpenID , const char * szNickName )
{
	if(ConnectServer())
	{
		CMD_GP_RegisterOpenID RegisterOpenID;
		memset(&RegisterOpenID, 0x00, sizeof(RegisterOpenID));
		RegisterOpenID.dwPlazaVersion = VER_PLAZA_FRAME;
		RegisterOpenID.wKindID = 0;
		strcpy(RegisterOpenID.szOpenID, szOpenID);
		strcpy(RegisterOpenID.szNickName, szNickName);
		RegisterOpenID.cbOpenPlat = OPENFLAT_XM;
		RegisterOpenID.cbPlatform = PLATFORM_XM;

		char szBuf[1000];
		sprintf(szBuf, "%u%d%s%s%d%d%s",RegisterOpenID.dwPlazaVersion, RegisterOpenID.wKindID, 
			RegisterOpenID.szOpenID, RegisterOpenID.szNickName, RegisterOpenID.cbOpenPlat, RegisterOpenID.cbPlatform, REGISTER_KEY);
		CMD5Encrypt::EncryptData(szBuf, RegisterOpenID.szMD5Result);

		LobbySocketSink::sharedLoginSocket()->SendData(MDM_GP_LOGON,SUB_GP_REGISTER_OPENID,&RegisterOpenID,sizeof(RegisterOpenID));
	}
	else
	{
		showMessage(_UNICODE_CONNECT_FAILT);
	}
}

