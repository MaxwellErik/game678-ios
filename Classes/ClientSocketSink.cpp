﻿#include "ClientSocketSink.h"
#include "LobbyLayer.h"
#include "GameConstant.h"
#include "JniSink.h"
#include "gameShowHand/ShGameViewLayer.h"
#include "SocketEngine.h"
#include "GameLayerMove.h"
#include "Encrypt.h"
#include "GlobalField.h"
#include "Convert.h"

ClientSocketSink* ClientSocketSink::m_pClientSocketSink = NULL;
CTcpClientThread * ClientSocketSink::m_pConnectRoomSocket = NULL;
/////////////////////////////////////////////////////////////////////////////////////////////////////
ClientSocketSink::ClientSocketSink()
{
    m_pMeUserData = NULL;
    //m_pTableFrameSink = NULL;
    m_pFrameGameView = NULL;
    m_bClientReady = false;
    setIsLogin(true);
}


ClientSocketSink::~ClientSocketSink(void)
{

}


ClientSocketSink* ClientSocketSink::sharedSocketSink(void)
{
    if (m_pClientSocketSink == NULL)
    {
        m_pClientSocketSink = new ClientSocketSink();
        m_pClientSocketSink->retain();
    }
    return m_pClientSocketSink;
}

CTcpClientThread * ClientSocketSink::sharedClientSocket( void )
{
    if(m_pConnectRoomSocket == NULL)
    {
        m_pConnectRoomSocket = new CTcpClientThread();
    }
    return m_pConnectRoomSocket;
}

bool ClientSocketSink::OnEventTCPSocketRead( CMD_Command Command, VOID * pData, WORD wDataSize )
{
#ifdef _DEBUG
    cocos2d::log("----------------->>>>>main ID: %d, sub ID: %d",Command.wMainCmdID, Command.wSubCmdID);
#endif // _DEBUG
    
    switch (Command.wMainCmdID)
    {
        case MDM_GR_LOGON:			//登录消息
        {
            return OnSocketMainLogon(Command,pData,wDataSize);
        }
        case MDM_GR_USER:			//用户消息
        {
            return OnSocketMainUser(Command,pData,wDataSize);
        }
        case MDM_GR_INFO:			//配置信息
        {
            return OnSocketMainInfo(Command,pData,wDataSize);
        }
        case MDM_GR_STATUS:			//状态信息
        {
            return OnSocketMainStatus(Command,pData,wDataSize);
        }
        case MDM_GR_SYSTEM:			//系统消息
        {
            return OnSocketMainSystem(Command,pData,wDataSize);
        }
        case MDM_GR_SERVER_INFO:	//房间信息
        {
            return OnSocketMainServerInfo(Command,pData,wDataSize);
        }
        case MDM_GF_GAME:			//游戏消息
        case MDM_GF_FRAME:			//框架消息
        case MDM_GF_PRESENT:		//礼物消息
        {
            return OnSocketMainGameFrame(Command,pData,wDataSize);
        }
        case MDM_GP_MOBILE:
        {
            return OnSocketMainMobile(Command,pData,wDataSize);
        }
        case MDM_GF_LOTTERY:
        {
            return OnSocketCaijin(Command,pData,wDataSize);
        }
    }
    
    return true;
    
}

bool ClientSocketSink::OnSocketMainLogon( CMD_Command Command, void * pBuffer, WORD wDataSize )
{
    CC_ASSERT(Command.wMainCmdID==MDM_GR_LOGON);
    switch (Command.wSubCmdID)
    {
        case SUB_GR_LOGON_SUCCESS:		//登录成功
        {
            //设置变量
            g_GlobalUnits.m_bSingleGame = false;
            LoginServerSuccess();
            return true;
        }
        case SUB_GR_LOGON_ERROR:		//登录失败
        {
            //效验参数
            CMD_GR_LogonError * pLogonError=(CMD_GR_LogonError *)pBuffer;
            CC_ASSERT(wDataSize>=(sizeof(CMD_GR_LogonError)-sizeof(pLogonError->szErrorDescribe)));
            if (wDataSize<(sizeof(CMD_GR_LogonError)-sizeof(pLogonError->szErrorDescribe))) return false;
            
            //关闭连接
            sharedClientSocket()->CloseSocket();
            
            AlertMessageLayer::createConfirm(gbk_utf8(pLogonError->szErrorDescribe).c_str());
            LoadLayer::removeLoadLayer(Director::getInstance()->getRunningScene());
            GameLayerMove::sharedGameLayerMoveSink()->KillEnterRoomTime();
            return true;
        }
        case SUB_GR_LOGON_FINISH:		//登录完成
        {
            LoginServerSuccess();
            GameLayerMove::sharedGameLayerMoveSink()->KillEnterRoomTime();
            GameLayerMove::sharedGameLayerMoveSink()->loginGameServerSuccess();
            //在游戏中直接进入游戏
            if (m_pMeUserData != NULL && m_pMeUserData->wTableID != INVALID_CHAIR)
                StartGameClient();
            
            return true;
        }
    }
    return true;
}

bool ClientSocketSink::OnSocketMainUser( CMD_Command Command, void * pBuffer, WORD wDataSize )
{
    CC_ASSERT(Command.wMainCmdID==MDM_GR_USER);
    switch (Command.wSubCmdID)
    {
        case SUB_GR_USER_COME:			//用户进入
        {
            return OnSocketSubUserCome(Command,pBuffer,wDataSize);
        }
        case SUB_GR_USER_STATUS:		//用户状态
        {
            return OnSocketSubStatus(Command,pBuffer,wDataSize);
        }
        case SUB_GR_USER_SCORE:			//用户分数
        {
            return OnSocketSubScore(Command,pBuffer,wDataSize);
        }
        case SUB_GR_SIT_FAILED:			//坐下失败
        {
            return OnSocketSubSitFailed(Command,pBuffer,wDataSize);
        }
        case SUB_GR_SCORE_SIT_FAILED:	//因分数不足而坐下失败
        {
            return OnSocketSubScoreSitFailed(Command,pBuffer,wDataSize);
        }
        case SUB_GR_USER_AUTO_MATCH_ADMIN:
        {
            if (m_pFrameGameView != nullptr)
            {
                m_pFrameGameView->OnEventUserChangeTable(nullptr, 1, false);
            }
        }
    }
    return true;
}

void ClientSocketSink::StartGameClient(bool _move)
{
    GetScene();
    LoadLayer::removeLoadLayer(Director::getInstance()->getRunningScene());
    GameLayerMove::sharedGameLayerMoveSink()->KillEnterGameTime();
    GameLayerMove::sharedGameLayerMoveSink()->StartGame(_move);
    m_bClientReady = true;
    g_GlobalUnits.m_bLeaveGameByServer = false;
    g_GlobalUnits.m_bIsMePlaying = true;
}

bool ClientSocketSink::OnSocketMainInfo( CMD_Command Command, void * pBuffer, WORD wDataSize )
{
    CC_ASSERT(Command.wMainCmdID==MDM_GR_INFO);
    switch (Command.wSubCmdID)
    {
        case SUB_GR_SERVER_INFO:	//房间信息
        {
            CC_ASSERT(wDataSize>=sizeof(CMD_GR_ServerInfo));
            if (wDataSize<sizeof(CMD_GR_ServerInfo)) return false;
            //消息处理
            CMD_GR_ServerInfo * pServerInfo=(CMD_GR_ServerInfo *)pBuffer;
            
            g_GlobalUnits.m_wChairCount=pServerInfo->wChairCount;
            g_GlobalUnits.m_wTableCount=pServerInfo->wTableCount;
            break;
        }
    }
    return true;
}

bool ClientSocketSink::OnSocketMainStatus( CMD_Command Command, void * pBuffer, WORD wDataSize )
{
    //效验参数
    if (Command.wSubCmdID == SUB_GR_TABLE_INFO)
    {
        CMD_GR_TableInfo * pMessage=(CMD_GR_TableInfo *)pBuffer;
        GameLayerMove::sharedGameLayerMoveSink()->InitTableStatus(pMessage);
    }
    else if (Command.wSubCmdID == SUB_GR_TABLE_STATUS)
    {
        CMD_GR_TableStatus * pMessage=(CMD_GR_TableStatus *)pBuffer;
        GameLayerMove::sharedGameLayerMoveSink()->ReloadTable(pMessage);
    }
    
    
    return true;
}

bool ClientSocketSink::OnSocketMainSystem( CMD_Command Command, void * pBuffer, WORD wDataSize )
{
    CC_ASSERT(Command.wMainCmdID==MDM_GR_SYSTEM);
    switch (Command.wSubCmdID)
    {
        case SUB_GR_MESSAGE:		//系统消息
        {
            //效验参数
            CMD_GR_Message * pMessage=(CMD_GR_Message *)pBuffer;
            if (pMessage->wMessageType & SMT_CLOSE_ROOM)
            {
                LoadLayer::removeLoadLayer(Director::getInstance()->getRunningScene());
                if (m_pFrameGameView != nullptr)
                {
                    g_GlobalUnits.m_bLeaveGameByServer = true;
                    AlertMessageLayer::createConfirm(m_pFrameGameView, gbk_utf8(pMessage->szContent).c_str(), menu_selector(IGameView::backLoginView));
                    GameLayerMove::sharedGameLayerMoveSink()->GoTableToKind();
                    closeSocket();
                    m_UserManager.DeleteAllUser();
                }
                else
                {
                    g_GlobalUnits.m_bLeaveGameByServer = true;
                    AlertMessageLayer::createConfirm(gbk_utf8(pMessage->szContent).c_str());
                    GameLayerMove::sharedGameLayerMoveSink()->GoTableToKind();
                    closeSocket();
                    m_UserManager.DeleteAllUser();
                }
                GameLayerMove::sharedGameLayerMoveSink()->KillEnterRoomTime();
                return true;
            }
            else if (pMessage->wMessageType & SMT_EJECT)
            {
                LoadLayer::removeLoadLayer(Director::getInstance()->getRunningScene());
                AlertMessageLayer::createConfirm(gbk_utf8(pMessage->szContent).c_str());
                GameLayerMove::sharedGameLayerMoveSink()->KillEnterRoomTime();
            }
            
            return true;
        }
    }
    
    return true;
}

bool ClientSocketSink::OnSocketMainServerInfo( CMD_Command Command, void * pBuffer, WORD wDataSize )
{
    return true;
}

bool ClientSocketSink::OnSocketMainGameFrame( CMD_Command Command, void * pBuffer, WORD wDataSize )
{
	log("m_pFrameGameView === Command");
    if (m_pFrameGameView == NULL)
    {
    	log("m_pFrameGameView === null");
        if (Command.wMainCmdID == MDM_GF_FRAME)
        {
            if (Command.wSubCmdID == SUB_CM_SYSTEM_MESSAGE)
            {
                CMD_CM_SystemMessage_MB* temp = (CMD_CM_SystemMessage_MB*)pBuffer;
                if (temp->wType&SMT_CLOSE_ROOM)
                {
                    GameLayerMove::sharedGameLayerMoveSink()->CallCloseRoom(temp->szString);
                }
            }
        }
        return true;
    }
    m_pFrameGameView->removeConnect();
    switch (Command.wMainCmdID)
    {
        case MDM_GF_FRAME:	//框架消息
        {
            if (Command.wSubCmdID == SUB_GF_SCENE)
            {
                m_bClientReady=true;
            }
            return m_pFrameGameView->OnFrameMessage(Command.wSubCmdID,pBuffer,wDataSize);

            break;
        }
        case MDM_GF_GAME:	//游戏消息
        {
            return m_pFrameGameView->OnGameMessage(Command.wSubCmdID,pBuffer,wDataSize);
        }
    }
    
    return true;
}

bool ClientSocketSink::OnSocketSubUserCome( CMD_Command Command, void * pBuffer, WORD wDataSize )
{
    //效验参数
    CC_ASSERT(Command.wMainCmdID==MDM_GR_USER);
    CC_ASSERT(Command.wSubCmdID==SUB_GR_USER_COME);
    CC_ASSERT(wDataSize>=sizeof(tagUserInfoHead));
    if (wDataSize<sizeof(tagUserInfoHead)) return false;

    //读取基本信息
    tagUserData UserData;
    memset(&UserData,0,sizeof(UserData));
    tagUserInfoHead * pUserInfoHead=(tagUserInfoHead *)pBuffer;
    
    //读取信息
    UserData.dwUserID=pUserInfoHead->dwUserID;
    UserData.dwGameID=pUserInfoHead->dwGameID;
    UserData.wTableID=pUserInfoHead->wTableID;
    UserData.wChairID=pUserInfoHead->wChairID;
    UserData.cbUserStatus=pUserInfoHead->cbUserStatus;
    
    UserData.wFaceID=pUserInfoHead->wFaceID;
    UserData.wGender=pUserInfoHead->cbGender;
    UserData.cbMemberOrder=pUserInfoHead->cbMemberOrder;
    UserData.cbMasterOrder=pUserInfoHead->cbMasterOrder;
    
    UserData.lScore=pUserInfoHead->UserScoreInfo.lScore;
    UserData.lGameGold=pUserInfoHead->UserScoreInfo.lGameGold;
    UserData.lInsureScore=pUserInfoHead->UserScoreInfo.lInsureScore;
    UserData.lInsureSilverScore = pUserInfoHead->UserScoreInfo.lInsureScore;
    
    UserData.lWinCount = pUserInfoHead->UserScoreInfo.lWinCount;
    UserData.lLostCount = pUserInfoHead->UserScoreInfo.lLostCount;
    UserData.lDrawCount = pUserInfoHead->UserScoreInfo.lDrawCount;
    UserData.lFleeCount = pUserInfoHead->UserScoreInfo.lFleeCount;
    UserData.lExperience = pUserInfoHead->UserScoreInfo.lExperience;
    UserData.dwMasterRight=pUserInfoHead->dwMasterRight;
    UserData.cbBanker=pUserInfoHead->cbBanker;
    //变量定义
    
    void * pDataBuffer=NULL;
    tagDataDescribe DataDescribe;
    CRecvPacketHelper RecvPacket(pUserInfoHead+1,wDataSize-sizeof(tagUserInfoHead));
    while (true)
    {
        pDataBuffer=RecvPacket.GetData(DataDescribe);
        if (DataDescribe.wDataDescribe==DTP_NULL) break;
        switch (DataDescribe.wDataDescribe)
        {
            case DTP_USER_ACCOUNTS:    //昵称
            {
                ASSERT(pDataBuffer!=NULL);
                ASSERT(DataDescribe.wDataSize<=sizeof(UserData.szNickName));
                
                if (DataDescribe.wDataSize<=sizeof(UserData.szNickName))
                {
                    CopyMemory(&UserData.szNickName, pDataBuffer,NAME_LEN);
                    UserData.szNickName[CountArray(UserData.szNickName)-1]=0;
                }
                break;
            }
        }
    }
    
    tagUserData *pUserData = m_UserManager.SearchUserByUserID(UserData.dwUserID);
    
    if (pUserData == NULL)
    {
        pUserData = m_UserManager.AddUserItem(UserData);
        if (m_pMeUserData != NULL)
        {
            if (m_pMeUserData->dwUserID==pUserData->dwUserID)
            {
                g_GlobalUnits.m_wMeChair = pUserData->wChairID;
            }
            
            WORD wViewChairID=g_GlobalUnits.SwitchViewChairID(pUserData->wChairID);
            if (pUserData->wTableID!=INVALID_TABLE)
            {
                g_GlobalUnits.SetUserInfo(wViewChairID,pUserData);
                if(m_pFrameGameView != NULL) m_pFrameGameView->OnEventUserEnter(pUserData, pUserData->wChairID, pUserData->cbUserStatus == US_LOOKON);
            }
        }
    }
    
    if (m_pMeUserData == NULL && g_GlobalUnits.GetGolbalUserData().dwUserID == pUserData->dwUserID)
    {
        m_pMeUserData = pUserData;
        g_GlobalUnits.GetGolbalUserData().lScore = m_pMeUserData->lScore;
        g_GlobalUnits.GetGolbalUserData().lInsureScore = m_pMeUserData->lInsureScore;
        g_GlobalUnits.GetGolbalUserData().lSilverScore = m_pMeUserData->lSilverScore;
        g_GlobalUnits.GetGolbalUserData().lInsureSilverScore = m_pMeUserData->lInsureSilverScore;
        g_GlobalUnits.GetGolbalUserData().lIngot = m_pMeUserData->lIngot;
        g_GlobalUnits.GetGolbalUserData().lLoveliness = m_pMeUserData->lLoveliness;
        g_GlobalUnits.m_wMeChair = m_pMeUserData->wChairID;
    }
    
    
    return true;
}

void ClientSocketSink::CleaAllUser()
{
    m_UserManager.DeleteAllUser();
}

bool ClientSocketSink::OnSocketSubStatus( CMD_Command Command, void * pBuffer, WORD wDataSize )
{
    //效验参数
    CC_ASSERT(wDataSize>=sizeof(CMD_GR_UserStatus));
    if (wDataSize<sizeof(CMD_GR_UserStatus)) return false;
    
    //处理数据
    CMD_GR_UserStatus * pUserStatus=(CMD_GR_UserStatus *)pBuffer;
    tagUserData * pUserData = m_UserManager.SearchUserByUserID(pUserStatus->dwUserID);

    if (nullptr == pUserData) return true;
    
    //定义变量
    WORD wNowTableID = pUserStatus->wTableID;
    WORD wNowChairID = pUserStatus->wChairID;
    BYTE cbNowStatus = pUserStatus->cbUserStatus;

    WORD wLastTableID = pUserData->wTableID;
    WORD wLastChairID = pUserData->wChairID;
    BYTE cbLastStatus = pUserData->cbUserStatus;
    
    //用户离开
    if (cbNowStatus == US_NULL)
    {
        //删除用户
        if(m_pMeUserData != NULL && m_pMeUserData->dwUserID == pUserData->dwUserID)
            m_pMeUserData = NULL;

        m_UserManager.DeleteUserItem(pUserData);
        WORD wViewChairID=g_GlobalUnits.SwitchViewChairID(pUserData->wChairID);
        g_GlobalUnits.SetUserInfo(wViewChairID, NULL);
        return true;
    }
    
    //更新状态
    tagUserStatus UserStatus;
    UserStatus.wTableID = wNowTableID;
    UserStatus.wChairID = wNowChairID;
    UserStatus.cbUserStatus = cbNowStatus;
    
    m_UserManager.UpdateUserItemStatus(pUserData, &UserStatus);
    
    if (m_pMeUserData != NULL && m_pMeUserData->dwUserID == pUserData->dwUserID)
    {
        g_GlobalUnits.m_wMeChair = pUserData->wChairID;
    }
    WORD wViewChairID = g_GlobalUnits.SwitchViewChairID(UserStatus.wChairID);
    if (pUserData->wTableID != INVALID_TABLE)
        g_GlobalUnits.SetUserInfo(wViewChairID,pUserData);
    
    //设置新状态
    if ((wNowTableID != INVALID_TABLE) && ((wNowTableID!=wLastTableID) || (wNowChairID != wLastChairID)))
    {
        if (m_pMeUserData != NULL && (m_pMeUserData->dwUserID != pUserData->dwUserID) && (m_pMeUserData->wTableID==wNowTableID))
        {
            if(m_pFrameGameView != NULL)
                m_pFrameGameView->OnEventUserEnter(pUserData, wNowChairID, pUserData->cbUserStatus == US_LOOKON);
        }
    }
    
    //判断发送(是否给游戏发送状态)
    bool bNotifyGame = false;
    
    if (m_pMeUserData != nullptr)
    {
        if (pUserData->dwUserID == m_pMeUserData->dwUserID)
            bNotifyGame = true;
        else if ((m_pMeUserData->wTableID != INVALID_TABLE) && (m_pMeUserData->wTableID == wNowTableID || m_pMeUserData->wTableID==wLastTableID))
            bNotifyGame=true;
    }
    
    //发送状态
    if (bNotifyGame==true)
    {
        if (m_pFrameGameView != NULL)
        {
            if (pUserData->cbUserStatus < US_SIT)
            {
                m_pFrameGameView->OnEventUserLeave(pUserData, wLastChairID, pUserData->cbUserStatus == US_LOOKON);
            }
            else
            {
                m_pFrameGameView->OnEventUserStatus(pUserData, wLastChairID, pUserData->cbUserStatus == US_LOOKON);
            }
        }
    }
    
    m_lobbylayer->UpdateGameTableUserStatus(pUserData);
    
    //判断自己
    if (m_pMeUserData != NULL && pUserData->dwUserID == m_pMeUserData->dwUserID)
    {
        //启动游戏
        if ((wNowTableID != INVALID_TABLE)&&((wNowTableID != wLastTableID)||(wNowChairID != wLastChairID)))
        {
            //启动游戏
            StartGameClient();
        }
        else if ((wNowTableID != INVALID_TABLE) && ((wNowTableID == wLastTableID) &&( wNowChairID == wLastChairID)) && !m_bClientReady)
        {
        	StartGameClient(false);
        }
    }
    return true;
}


bool ClientSocketSink::OnSocketSubScore( CMD_Command Command, void * pBuffer, WORD wDataSize )
{
    //效验参数
    CC_ASSERT(Command.wMainCmdID==MDM_GR_USER);
    CC_ASSERT(Command.wSubCmdID==SUB_GR_USER_SCORE);
    CC_ASSERT(wDataSize>=sizeof(CMD_GR_UserScore));
    if (wDataSize<sizeof(CMD_GR_UserScore)) return false;
    
    //处理数据
    CMD_GR_UserScore * pUserScore=(CMD_GR_UserScore *)pBuffer;
    tagUserData * pUserData=m_UserManager.SearchUserByUserID(pUserScore->dwUserID);
    if (pUserData==NULL) return true;

    pUserData->lScore = pUserScore->UserScore.lScore;
    pUserData->lGameGold = pUserScore->UserScore.lScore;
    
    m_UserManager.UpdateUserItemScore(pUserData,&pUserScore->UserScore);
    if (g_GlobalUnits.GetGolbalUserData().dwUserID == pUserData->dwUserID)
    {
        g_GlobalUnits.GetGolbalUserData().lScore = m_pMeUserData->lScore;
        g_GlobalUnits.GetGolbalUserData().lInsureScore = m_pMeUserData->lInsureScore;
        g_GlobalUnits.GetGolbalUserData().lSilverScore = m_pMeUserData->lSilverScore;
        g_GlobalUnits.GetGolbalUserData().lInsureSilverScore = m_pMeUserData->lInsureSilverScore;
        g_GlobalUnits.GetGolbalUserData().lIngot = m_pMeUserData->lIngot;
        g_GlobalUnits.GetGolbalUserData().lLoveliness = m_pMeUserData->lLoveliness;
    }
    //更新游戏
    if (m_pMeUserData != NULL && (m_pMeUserData->wTableID!=INVALID_TABLE)&&(m_pMeUserData->wTableID==pUserData->wTableID))
    {
        if(m_pFrameGameView != NULL) m_pFrameGameView->OnEventUserScore(pUserData, pUserData->wChairID, pUserData->cbUserStatus == US_LOOKON);
    }
    
    return true;
}

bool ClientSocketSink::OnSocketSubSitFailed( CMD_Command Command, void * pBuffer, WORD wDataSize )
{
    //效验参数
    CC_ASSERT(Command.wMainCmdID==MDM_GR_USER);
    CC_ASSERT(Command.wSubCmdID==SUB_GR_SIT_FAILED || Command.wSubCmdID==SUB_GR_SCORE_SIT_FAILED);
    
    LoadLayer::removeLoadLayer(Director::getInstance()->getRunningScene());
    GameLayerMove::sharedGameLayerMoveSink()->KillEnterGameTime();
    //消息处理
    CMD_GR_RequestFailure_MB * pSitFailed=(CMD_GR_RequestFailure_MB *)pBuffer;
    
    AlertMessageLayer::createConfirm(gbk_utf8(pSitFailed->szDescribeString).c_str());
    return true;
}

bool ClientSocketSink::OnSocketSubScoreSitFailed( CMD_Command Command, void * pBuffer, WORD wDataSize )
{
    LoadLayer::removeLoadLayer(Director::getInstance()->getRunningScene());
    return OnSocketSubSitFailed(Command, pBuffer, wDataSize);
}

bool ClientSocketSink::OnEventTCPSocketShut()
{
    log("与服务器断开连接");
    m_bClientReady=false;
    if (m_pFrameGameView && !m_pFrameGameView->isConnecting())
    {
        m_pFrameGameView->beamreconnect();
    }
    return true;
}

void ClientSocketSink::StartReceive()
{
//    schedule(schedule_selector(ClientSocketSink::update),_TCP_CLIENT_SOCKET_LOOP_TICKET_);
    sharedClientSocket()->StartReceive();
}

void ClientSocketSink::StopReceive()
{
//    unschedule(schedule_selector(ClientSocketSink::update));
    sharedClientSocket()->StopReceive();
}

void ClientSocketSink::update( float delta )
{
    StopReceive();
    if (sharedClientSocket()->RecvPack())
    {
        StartReceive();
    }
}

tagUserData* ClientSocketSink::GetMeUserData()
{
    tagUserData* ret = m_pMeUserData;
    return ret;
}

void ClientSocketSink::setFrameGameView( FrameGameView * pFrameGameView )
{
    m_pFrameGameView = pFrameGameView;
    SocketEngine::sharedSocketEngine()->setFrameGameView(m_pFrameGameView);
    
}

void ClientSocketSink::closeSocket()
{
    StopReceive();
    if(_parent != NULL)
    {
        _parent->removeChild(this);
        _parent=NULL;
    }
    if (m_pMeUserData!=NULL)
    {
        m_UserManager.DeleteUserItem(m_pMeUserData->dwUserID);
        WORD wViewChairID=g_GlobalUnits.SwitchViewChairID(m_pMeUserData->wChairID);
        g_GlobalUnits.SetUserInfo(wViewChairID,NULL);
    }
    
    m_pFrameGameView = NULL;
    m_pMeUserData = NULL;
    sharedClientSocket()->CloseSocket();
}

bool ClientSocketSink::OnEventTCPSocketLink()
{
    StartReceive();
    return true;
}

bool ClientSocketSink::OnEventTCPConnectError()
{
    // .....
    if (_parent == NULL)
    {
        Director::getInstance()->getRunningScene()->addChild(this);
    }
    scheduleOnce(schedule_selector(ClientSocketSink::OnError), 0.1);
    return true;
}

void ClientSocketSink::OnError(float ft)
{
    //网络连接失败
//    LoadLayer::removeLoadLayer(Director::getInstance()->getRunningScene());
//    AlertMessageLayer::createConfirm("\u4f60\u7684\u8d26\u53f7\u5728\u5176\u4ed6\u5730\u65b9\u767b\u9646\uff01");
    log("222222");
}

bool ClientSocketSink::ConnectServer(char *server, int port)
{
    if (server && port) {
        m_wPort = port;
        strcpy(m_szServerAddr , server);
    }
    sharedClientSocket()->SetSocketLink(this);
    return true;
}

void ClientSocketSink::ConnectToServer()
{
    if(sharedClientSocket()->IsConnect()) return;
    const char *sLOGINIP = m_szServerAddr;
    
    struct hostent *remoteHostEnt = gethostbyname(sLOGINIP);
    if (remoteHostEnt == NULL)
    {
        log("origin IP = %s", sLOGINIP);
        sLOGINIP = "183.60.201.29";
    }
    else
    {
        // Get address info from host entry
        struct in_addr *remoteInAddr = (struct in_addr *) remoteHostEnt->h_addr_list[0];
        
        // Convert numeric addr to ASCII string
        sLOGINIP = inet_ntoa(*remoteInAddr);
    }
    
    sharedClientSocket()->ConnectServer(sLOGINIP , m_wPort , this);
    return;
}

void ClientSocketSink::LoginServer(WORD wKindID)
{
    if(ConnectServer())
    {
        tagGlobalUserData & GlobalUserInfo=g_GlobalUnits.GetGolbalUserData();
        BYTE sendBuffer[1000] = { 0 };
        CMD_GR_LogonByUserID *LogonByUserID = (CMD_GR_LogonByUserID *)sendBuffer;
       
        LogonByUserID->dwPlazaVersion = 0;
        LogonByUserID->dwProcessVersion = 0;
        LogonByUserID->dwUserID = GlobalUserInfo.dwUserID;
        strcpy(LogonByUserID->szPassWord, g_GlobalUnits.m_UserLoginInfo.pszMD5Password);
        LogonByUserID->lWebToken = GlobalUserInfo.lWebToken;
        
        char szMachineID[33];
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
        const char* p = const_cast<char*>(Application::getInstance()->GetUUID());
        if(p == NULL)  sprintf(szMachineID,"%s","test");
        else  CMD5Encrypt::EncryptData(p, szMachineID);
#elif(CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
        const char* p = JniSink::share()->GetDeviceID();
        if(p == NULL)  sprintf(szMachineID,"%s","test");
        else  CMD5Encrypt::EncryptData(p, szMachineID);
#endif
        
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
        if (0 != g_GlobalUnits.m_ChannelID)
        {
            LogonByUserID->dwPlatform = 2000 + g_GlobalUnits.m_ChannelID;
        }
        else
            LogonByUserID->dwPlatform = PLATFORM_APPLE;
        
#else
        if (0 != g_GlobalUnits.m_ChannelID)
        {
            LogonByUserID->dwPlatform = 3000 + g_GlobalUnits.m_ChannelID;
        }
        else
            LogonByUserID->dwPlatform = PLATFORM_ANDROID;
#endif
        
        CSendPacketHelper SendPacket(LogonByUserID + 1, 1000 - sizeof(CMD_GR_LogonByUserID));
        SendPacket.AddPacket(szMachineID, strlen(szMachineID)+1, 1000);
        
        sharedClientSocket()->SendData(MDM_GR_LOGON, SUB_MB_LOGON_USERID,sendBuffer, sizeof(CMD_GR_LogonByUserID) + SendPacket.GetDataSize());
    }
    else
    {
        LoadLayer::removeLoadLayer(Director::getInstance()->getRunningScene());
        AlertMessageLayer::createConfirm(_UNICODE_CONNECT_FAILT);
    }
}

void ClientSocketSink::AccountCheckFinish(WORD wKindID)
{
    LoginServer(wKindID);
    if (_parent != NULL)
    {
        _parent->removeChild(this);
        _parent = NULL;
    }
    Director::getInstance()->getRunningScene()->addChild(this);
    ClientSocketSink::sharedSocketSink()->StartReceive();
    
}

void ClientSocketSink::LoginServerSuccess()
{
    if (!getIsLogin() && !g_GlobalUnits.m_bSingleGame)
    {
        return;
    }
    if(_parent != NULL)
    {
        StopReceive();
        _parent->removeChild(this);
        _parent= NULL;
    }
    if (CheckClientStart())
    {
        if (m_pFrameGameView)
            m_pFrameGameView->addChild(this);
    }
    ClientSocketSink::sharedSocketSink()->StartReceive();
}

void ClientSocketSink::SitdownReq(WORD wTableID , WORD wChairID)
{
    CMD_GR_UserSitReq UserSitReq;
    memset(&UserSitReq,0,sizeof(UserSitReq));
    UserSitReq.dwProcessVersion = 0;
    UserSitReq.wTableID=wTableID;
    UserSitReq.wChairID=wChairID;
    sprintf(UserSitReq.szVersion,"%s", "!@#$%^123");
    UserSitReq.cbPassLen = PASS_LEN;
    sprintf(UserSitReq.szTablePass,"%s", "");
    //发送数据包
    sharedClientSocket()->SendData(MDM_GR_USER, SUB_GR_USER_SIT_REQ, &UserSitReq, sizeof(CMD_GR_UserSitReq));
}

void ClientSocketSink::SitdownReq(const char* password, WORD wTableID , WORD wChairID)
{
    CMD_GR_UserSitReq UserSitReq;
    memset(&UserSitReq,0,sizeof(UserSitReq));
     UserSitReq.dwProcessVersion = 0;
    UserSitReq.wTableID=wTableID;
    UserSitReq.wChairID=wChairID;
    sprintf(UserSitReq.szVersion,"%s", "!@#$%^123");
    UserSitReq.cbPassLen = PASS_LEN;
    sprintf(UserSitReq.szTablePass,"%s", password);
    //发送数据包
    sharedClientSocket()->SendData(MDM_GR_USER,SUB_GR_USER_SIT_REQ,&UserSitReq,sizeof(CMD_GR_UserSitReq));
}

void ClientSocketSink::GetScene()
{
    CMD_GF_Info GameOption;
    ZeroMemory(&GameOption,sizeof(CMD_GF_Info));
    
    //构造数据
    GameOption.bAllowLookon=0;
    sprintf(GameOption.szVersion,"%s", "!@#$%^123");
    SendData(MDM_GF_FRAME,SUB_GF_GAME_OPTION,&GameOption,sizeof(CMD_GF_Info));
}

bool ClientSocketSink::SendData(WORD wMainCmdID, WORD wSubCmdID )
{
    if (g_GlobalUnits.m_bSingleGame)
    {
        return SocketEngine::sharedSocketEngine()->ReceiveData(wMainCmdID , wSubCmdID);
    }
    return sharedClientSocket()->SendData(wMainCmdID , wSubCmdID);
}

bool ClientSocketSink::SendData(WORD wMainCmdID, WORD wSubCmdID, void * pData, WORD wDataSize )
{
    if (g_GlobalUnits.m_bSingleGame)
    {
        return SocketEngine::sharedSocketEngine()->ReceiveData(wMainCmdID , wSubCmdID, pData , wDataSize);
    }
    return sharedClientSocket()->SendData(wMainCmdID , wSubCmdID , pData , wDataSize);
}

bool ClientSocketSink::OnSocketMainMobile( CMD_Command Command, void * pBuffer, WORD wDataSize )
{
    return true;
}
bool ClientSocketSink::OnSocketCaijin( CMD_Command Command, void * pBuffer, WORD wDataSize )
{
    switch (Command.wSubCmdID) {
        case SUB_GF_LOTTERY:
        case SUB_GF_LOTTERY_WEEK:
        {
            if (m_pFrameGameView)
                m_pFrameGameView->OnGameMessage(Command.wSubCmdID, pBuffer, wDataSize);
            break;
        }
        case SUB_GF_BROADCAST_TEXT:
        {
            CMD_GF_Broadcast* broadcast = (CMD_GF_Broadcast*)pBuffer;
            char msg[256];
            memset(msg, 0, sizeof(msg));
            sprintf(msg, "%s", gbk_utf8(broadcast->Content).c_str());
            m_lobbylayer->AddGameBroadcast(msg);
            break;
        }
        default:
            break;
    }
    return true;
}

void ClientSocketSink::LeftGameReq()
{
    sharedClientSocket()->SendData(MDM_GR_USER,SUB_GR_USER_LEFT_GAME_REQ);
}

bool ClientSocketSink::CheckClientStart()
{

    Scene *scene = Director::getInstance()->getRunningScene();
    auto children = scene->getChildren();
    if((!children.empty()) && (children.size() > 0))
    {
        Ref* pObject = NULL;
        for (Vector<Node*>::iterator it = children.begin(); it != children.end(); it++)
        {
            pObject = *it;
            if(pObject != NULL)
            {
                if (dynamic_cast<FrameGameView *>(pObject) != NULL)
                {
                    return true;
                }
            }
        }
    }
    return false;
}

bool ClientSocketSink::CheckClientLoad()
{
    return true;
}
