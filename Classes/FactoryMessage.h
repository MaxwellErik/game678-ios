﻿#ifndef __FACTORY_MESSAGE_H__
#define __FACTORY_MESSAGE_H__

static const unsigned char IDENTIFY_VER = 0x05;
static const unsigned char MESSAGE_VER  = 0x03;

static const unsigned char ENCODE_NONE  = 0x00;
static const unsigned char ENCODE_AES   = 0x01;

static const unsigned short NM_KEEP_ALIVE = 0xF0F1;

static const unsigned long NAME_LEN     = 32;     //帐号长度
static const unsigned long PASSWD_LEN   = 33;     //密码长度

//公有状态定义(所有可能的状态)
#define MAX_ROOM_NUM		4
#define MESSAGE_VERSION		3	//消息版本
#define MAX_PLAYER_NUM		10
#define MAX_CHAIR			10
  
#pragma pack(push, 4) 
typedef struct MsgHeadDef
{
	unsigned char  identity;
	unsigned char  encode;
	unsigned short length;
	unsigned char  version;
	unsigned char  reserve;
	unsigned short cMsgType;
}MsgHeadDef, *PMsgHeader; 
#pragma pack(pop)

typedef struct RobotLoginReq //请求机器人进入
{
	MsgHeadDef      msgHeadInfo;

}RobotLoginReqDef;

typedef struct TestNetMsg
{
	MsgHeadDef		msgHeadInfo;
	char					cType;//0为客户端请求包，2为客户端确认,
	int						iDelayTime;
	char					cTableNumExtra;
}TestNetMsgDef;

//特殊消息
#define		CLIENT_GET_CSERVER_LIST_MSG					0xF0		//客户端获得中心服务器的服务器列表消息（仅客户端和中心服务器使用）
#define		TEST_NET_MSG								0xF1		//网络测速消息
#define		GM_PLAYER_INFO_MSG							0xF2		//发送给GM所有玩家信息
#define		GM_PLAYER_JOIN_MSG							0xF3		//通知GM玩家进入服务器
#define		GM_PLAYER_LEAVE_MSG							0xF4		//通知GM玩家离开服务器
#define		GM_PLAYER_KICK_MSG							0xF5		//GM踢人消息
#define		GM_PLAYER_CHAT_MSG							0xF6		//GM发送消息信息
#define		PLAYER_KICK_MSG								0xF7		//玩家发送踢人消息 
#define		IMPEACH_REQ_MSG								0xF9		//玩家检举消息
#define		IMPEACH_RES_MSG								0xFA		//检举回应消息
#define		APPEAL_REQ_MSG								0xFB		//申诉请求消息
#define	  	REQUEST_ROBOT_REQ_MSG						0xFC		//请求机器人进入

//1: 客户端请求消息
#define   KEEP_ALIVE_MSG                   				0xE0  //保持连接消息
#define   AUTHEN_REQ_MSG                   				0xA0  //玩家登陆验证请求
#define   SITDOWN_REQ_MSG								0xA4	 //玩家“坐下”请求
#define   READY_REQ_MSG              			 		0xA9  //玩家按下“开始”请求，超时自动发
#define   ESCAPE_REQ_MSG           				 		0xE4  //玩家“逃跑”请求
#define   CHAT_REQ_MSG									0xE5  //玩家“聊天”请求
#define	  LEAVE_REQ_MSG									0xE6 //玩离开游戏请求回应
#define	  SHOUT_REQ_MSG									0xE7 //玩家全服务器喊话请求

//2: 服务器回应请求消息
#define   AUTHEN_RES_MSG                   				0xA1  //登陆验证响应
#define   SITDOWN_RES_MSG								0xA6	 //玩家“坐下”请求回应
#define   CHAT_RES_MSG									0xEA  //玩家“聊天”请求回应
#define	  AGAIN_LOGIN_RES_MSG					   	    0xA2	 //发送断线重入响应

#define   PLAYER_CHOICE_MULTIPLE_REQ_MSG				0xEF  //玩家请求底分

//4: 服务器主动通知消息 
#define   GAME_RESULT_SERVER_MSG           				0x67  //游戏结果
#define	 GAME_BULL_NOTICE_MSG							0xEE  //游戏公告通知


//3: 服务器通知客户端消息
#define   TABLE_PLAYER_JOIN_MSG						    0xA7  //桌上玩家进入消息

#define   READY_NOTICE_MSG              	 			0xAA  //玩家“开始”通知
#define   ESCAPE_NOTICE_MSG           		 			0xD2  //玩家“逃跑”通知
#define	  WAIT_LOGIN_AGAIN_NOTICE_MSG			 		0xD3	 //玩家掉线等待通知
#define	  LOGIN_AGAIN_NOTICE_MSG			 		 	0xD4	 //玩家掉线再次进入通知 add skyhawk 20080315
#define	  JACKPOT_NOTICE_MSG							0xD5 //通知玩家获得彩金 add skyhawk 20080721

#define   HONGBAO_NOTICE_MSG            				0xD6  //玩家获得红包通知20081030
#define   HONGBAO_SERVER_MSG            				0xD7  //玩家确认获得红包20081030

//比赛消息
#define		GET_ORDER_REQ_MSG							0x90 //客户端、服务请求Radius获得比赛排名
#define		GET_ORDER_RES_MSG							0x91 //回应比赛排名
#define     BUY_COMPETE_REQ_MSG							0x92 //客户端、服务请求Radius购买比赛资格
#define     BUY_COMPETE_RES_MSG							0x93 //回应购买比赛资格
#define		BONUS_NOTICE_MSG		                    0x95 //奖金服务器群发通知当前奖金数
#define     SHOUT_INFO_RES_MSG				0x97 //聊天信息消息回应
#define	    MAIL_NOTICE_MSG	                0x9B //邮件通知
#define	    USER_PROPINFO_RES_MSG	        0x9E //玩家道具信息回应

//#define  UPDATE_USER_PROP_REQ_MSQ			0xB1	//玩家获得道具请求（或者使用）

#define  SPECIAL_COMPETE_START_NOTICE       0xAE //特殊的网吧比赛开始通知
#define  SPECIAL_COMPETE_ORDER              0xAF //特殊的网吧比赛排名

#define   GUESS_CARD_REQ_MSG				0xB4    //猜牌请求 
#define   QUIT_GUESS_REQ_MSG				0xB6	//退出猜牌请求
#define   QUIT_GUESS_RES_MSG				0xB7	//退出猜牌回应
 
         
typedef struct SpecialUserCompeteData
{
	int iUserID;
	char szNickName[16]; 
	int iMoney;
	int iGameNum;
}SpecialUserCompeteDataDef;

typedef struct SpecialCompeteOrder
{
	MsgHeadDef msgHeadInfo;
	char cType;//定时排名是0，结束排名是1
	char cNum;//参加排名人数
	SpecialUserCompeteDataDef data[27];
}SpecialCompeteOrderDef;
 

typedef struct ShoutInfoRes//SHOUT_INFO_RES_MSG 喊话信息回应
{
	MsgHeadDef	 msgHeadInfo;
	int			iUserID;
	int			iLimitTime;//0表示成功，大于0表示封停时间
}ShoutInfoResDef;

typedef struct BonusNotice				//BONUS_NOTICE_MSG	奖金服务器群发通知当前奖金数
{
	MsgHeadDef	 msgHeadInfo;
	int			iNowTotalBonus;		//当前正在累积的比赛总奖金元宝数
	int			iFinalTotalBonus;	//当日最终奖金数，决赛房用	
	int			iCompeteState;			//比赛状态，0正常比赛，1决赛开始iNowTotalBonus清0，2本周比赛已经结束，通知游戏清掉玩家客户端资格赛积分
}BonusNoticeDef;


typedef struct GetOrderReq  //0x90 客户端、服务请求Radius获得比赛排名
{
	MsgHeadDef		msgHeadInfo;
	int  iUserID; //用户ID
	int  iCompeteAmount;//当前的比赛积分
}GetOrderReqDef;

typedef struct KeepAlive		//KEEP_ALIVE_MSG  保持连接消息
{
	MsgHeadDef		msgHeadInfo;
	char					szEmpty[4];
}KeepAliveDef;


typedef struct EscapeReq   //ESCAPE_REQ_MSG  玩家“逃跑”请求
{
	MsgHeadDef			msgHeadInfo;
	char						cType;//0为手动推出，1为换位
}EscapeReqDef;


typedef struct KickOutServer 
{
	MsgHeadDef       msgHeadInfo;
	unsigned char   ucType;
	int             iKickUserID;
}KickOutServerDef;

typedef struct AgainLoginRes                        //AGAIN_LOGIN_RES_MSG 发送断线重入响应
{
	MsgHeadDef   msgHeadInfo;
	int             iUserID;
	int             iServerTime;
	unsigned short  iTableNum;
	unsigned short  usTableNumExtra;
	char            cPlayerNum;
}AgainLoginResDef;

typedef struct EscapeNotice		//ESCAPE_NOTICE_MSG 玩家“逃跑”通知,玩家收到判断如果是自己，那么就返回大厅,显示等待用户列表，收到用户列表后才刷新进入
{
	MsgHeadDef   msgHeadInfo;
	char				 cTableNumExtra;
	int					 iUserID;//for see
	char				 cType;	//modify by crystal 11/20 新增标记为，特殊为10的话就是WAIT_DESK状态发来的，客户端收到就直接关掉了
	int			 iExpTime;
}EscapeNoticeDef;


typedef struct WaitLoginAgainNotice	//WAIT_LOGIN_AGAIN_NOTICE_MSG	玩家掉线等待通知
{
	MsgHeadDef		msgHeadInfo;
	char			cTableNumExtra;
	char			cDisconnectType;//掉线类型 20080716 0没有掉线，1在游戏中主动离开，2多次不出牌自己踢自己，3超时被别人踢掉，4 Socket断开（例如拔掉网线）
}WaitLoginAgianNoticeDef;

typedef struct GameResultServerBase		//GAME_RESULT_SERVER_MSG 当局结果...基础结构类型.用于FACTORY里异常逃跑结果通知,msg里面有扩展定义
{
	MsgHeadDef   msgHeadInfo;
	char				 cResultType;				//结构体类型,1为BASE
	int				 	 iMoneyResult[MAX_PLAYER_NUM];		
	int				 	 iAmountResult[MAX_PLAYER_NUM];
}GameResultServerBaseDef;

typedef struct PlayerKickReq    //PLAYER_KICK_MSG		玩家发送踢人消息请求 某个玩家超时
{
	MsgHeadDef   msgHeadInfo;
	int 		 iUserID;
	int			 iStatus;//被踢人的状态
}PlayerKickReqDef;

typedef struct LoginAgainNotice  //LOGIN_AGAIN_NOTICE_MSG 玩家掉线再次进入通知 add skyhawk 20080315
{
	MsgHeadDef   msgHeadInfo;
	char		 cTableNumExtra;
}LoginAgainNoticeDef;

typedef struct UrgeCard  //URGR_CARD_MSG 玩家催牌消息 add skyhawk 20080322
{
	MsgHeadDef   msgHeadInfo;
	char		 cWordID;//催牌话语的ID
	char		 cTableNumExtra;//催牌人的绝对位置
}UrgeCardDef;
 

typedef struct AppealReq  //APPEAL_REQ_MSG  申诉请求消息
{
	MsgHeadDef   msgHeadInfo;
	char		 cTableNumExtra;//申诉请求人的绝对位置
	char		 cGameNum[10];//牌局编号
	char		 m_cUserName[100];//游戏开始后保存玩家昵称
}AppealReqDef;

typedef struct ShoutInfoReq //SHOUT_REQ_MSG  玩家全服务器喊话请求
{
	MsgHeadDef   msgHeadInfo;
	char		szChatInfo[100];//喊话内容
}ShoutInfoReqDef;

typedef struct ChatInfoReq //CHAT_REQ_MSG  聊天请求消息
{
	MsgHeadDef   msgHeadInfo;
	char		 cTableNumExtra;//发送聊天消息的人的绝对位置
	char 		 cRecvTableNumExtra;//接收消息人的位置，群发则此处为0
	char		 szChatInfo[51];//聊天内容
	char		 szGameNum[15];		//每局的游戏编号
	char        szChatInfoUnicode[150];//Unicode模式的聊天内容，方便服务器端转成utf-8的存入数据库
}ChatInfoReqDef;

typedef struct ChatInfoRes //CHAT_RES_MSG  聊天回应消息
{
	MsgHeadDef   msgHeadInfo;
	char		 cTableNumExtra;//发送聊天消息的人的绝对位置
	char 		 cRecvTableNumExtra;//接收消息人的位置，群发则此处为0
	char		 szChatInfo[51];//聊天内容
}ChatInfoResDef;

typedef struct LeaveGame 
{
	MsgHeadDef   msgHeadInfo;
	char		 cType;//0在游戏中主动退出的，1大厅关闭后退出游戏的。
}LeaveGameDef;

typedef struct JackPotNotice //JACKPOT_NOTICE_MSG 通知玩家
{
	MsgHeadDef	 	msgHeadInfo;
	char			cTableNumExtra;
	char			cType;//彩金类型:1任务星豆，4任务金元宝，2大累积星豆，3小累积星豆，5小累计金元宝，6大累积元宝,98大类集彩金获得通知所有玩家99小累计彩金得通知所有玩家
	int				iJackPotNum;
	int				iUserID;//获得彩金玩家ID
	char			cIfFull;//标记当天任务元宝获得是否已经满了，1表示满了
	char			cIfFirst;//是否第一次
}JackPotNoticeDef;

typedef struct BullNotice//通知所有玩家的结构体
{
	MsgHeadDef msgHeadInfo;
	char szBullContent[200];
	char cType;	//公告类型0是后台添加的重要公告1是彩金类的公告2是喊话公告
} BullNoticeDef;
 

typedef struct HongBaoServer//玩家获得红包的确认  HONGBAO_SERVER_MSG            0x3A  //玩家确认获得红包20081030
{
	MsgHeadDef   msgHeadInfo;
	int		iNum;//红包的金额
	int		iType;//红包的类型
}HongBaoServerDef;

typedef struct CenterServerRes	//CLIENT_GET_CSERVER_LIST_MSG
{
	MsgHeadDef	 msgHeadInfo;
	int iNum; //后跟多少个ServerInfoDef
}CenterServerResDef;

typedef struct ServerInfoFa
{
	unsigned int iIP;
	short sPort;
}ServerInfoDef;

typedef struct PlayerChoiceMultipleReq //PLAYER_CHOICE_MULTIPLE_REQ_MSG				0xEF  //玩家请求底分
{
	MsgHeadDef	 msgHeadInfo;
	int iChoiceMultiple;//玩家请求的底分
}PlayerChoiceMultipleReqDef;

 
typedef struct PropInfoM		//道具信息
{
	int iPropID;
	int iPropNum;
	int iLastTime;
	int iPropUsed;//道具可使用状态
}PropInfoMDef;

typedef struct UserPropInfoResRadius	//道具信息回应
{
	MsgHeadDef msgHeadInfo;
	int iNum;
	int iUserID;
}UserPropInfoResRadiusDef;

typedef struct ClientUpdateUserProp  //通知客户端玩家获得的道具 UPDATE_USER_PROP_REQ_MSQ
{
	MsgHeadDef	 msgHeadInfo;
	int iType;//1是获得，2是使用
	PropInfoMDef propInfo;//玩家道具属性
}ClientUpdateUserPropDef;


//SitDownResDef 玩家坐下出错类型
#define SRD_USER_STATE_ERROR            131          //玩家状态错
#define SDR_NO_FIND_TABLE               132          //无法找到合适的座位
#define SDR_TABLE_ALREADY_USING         133          //指定的位置已经有玩家了
#define SDR_TABLENUM_ERROR              134          //请求桌号不在房间桌范围
#define SDR_TABLENUMEXTRA_ERROR         135          //请求的桌上位置不在游戏桌座位
#define SDR_TABLE_LOCK_STATUS           136          //桌子锁定状态
#define SDR_TABLE_PASSWD_ERROR          137          //桌密码错误
#define SDR_TABLE_NEED_SET_BASEPOINT    138          //桌需要设置底柱
#define SDR_TABLE_SETTING_BASEPOINT     139          //表示这个桌子正在设置底柱


//AuthenResDef认证出错类型
#define AUTHEN_SERVER_CLOSE_ERROR          101        //房间被关闭
#define AUTHEN_SERVER_FULL_ERROR           102        //房间满员
#define AUTHEN_RADIUS_NOFIND_USERID        103        //用户名不存在
#define AUTHEN_RADIUS_PASSWORD_ERROR       104        //密码错误
#define AUTHEN_RADIUS_ACCOUNT_DISABLE      105        //账号被禁用
#define AUTHEN_RADIUS_ALREADY_INGAME       106        //账号已经在游戏中
#define AUTHEN_RADIUS_DB_ERROR             107        //数据库存在异常
#define AUTHEN_NO_ENOUGH_MONEY             108        //帐号银子不够该房间限制
#define AUTHEN_ANGIN_NOFIND_OLDNODE        109  
 


typedef struct TablePlayerJoin                       //TABLE_PLAYER_JOIN_MSG 桌上玩家进入消息
{
	MsgHeadDef      msgHeadInfo;
	unsigned short         usTableNumExtra;          //玩家加入时候在服务器的就绝对问题
}TablePlayerJoinDef;


//typedef __int64 int64;

/*****************************************************选座位房间 一些函数********************************************/

typedef struct PlayerInfoExtra
{
	long long                   lFirstMoney;
	unsigned long               ulCurLevelExperience;   //当前等级经验值   
	unsigned long               ulNextLevelExperience;  //下一个等价经验值
	int                         iSingalGameExperience; //单个游戏的经验值 ****************

	int                         iUserID;
	int                         iSecondMony;            //银券通
	int                         iYBNum;                 //金元宝数
	int                         iPrizeNum;              //奖品券

	int                         iAreaID;
	int                         iCompeteAmount;         //比赛积分
	int                         iWinNum;                //胜利次数
	int                         iLostNum;               //输的次数
	int                         iDrawNum;               //和局次数
	int                         iDisNum;                //掉线
	int                         iLastResult;            //上把结果,只客户端有
	int                         iPhotoKey;              //自定义图像ID

	unsigned  short             iTableNum;
	unsigned  short             usIconNum;              //人物图像图标号
	unsigned  short             usTableNumExtra;
	unsigned  short             usExpLevel;             //用户经验等级
	char                        cSexType;
	char                        cVipType;               //VIP类型
	char                        cPhotoVerify;

	char                        cMasterType;            //管理登记
	unsigned char               ucSpecialIdentify;      //管理员特殊身份标识 
	char                        cUserStatus;            //用户状态 

	char                        cIfReady;
	char                        cDisconnectType;
	char                        szNickName[NAME_LEN];
	char                        szAreaName[20];
}PlayerInfoExtraDef;       

typedef struct ReadyNotice		//READY_NOTICE_MSG 玩家“开始”通知
{
	MsgHeadDef      msgHeadInfo;
	unsigned short         usTableNumExtra;
}ReadyNoticeDef;

typedef struct PlayerNode	    
{
	int                         iUserID;				//用户ID
	char                        szNickName[NAME_LEN];	//昵称
	long long                   iMoney;		            //玩家银子   lFirstMoney
	int                         iAmount;		        //玩家积分  
	unsigned  short             cTableNum;	            //玩家所在桌号,没在桌上为0
	unsigned  short             cIconNum;
	short                       usTableNumExtra;	    //玩家在桌上的位置，从最左边开始逆时针为0-3，short 保存-1
	int                         iWinNum;			    //胜利次数
	int                         iLostNum;				//输的次数
	int                         iDrawNum;               //和局次数
	int                         iDisNum;			    //掉线

	int                         iGameLevel;             //游戏中等级        
	int                         iGameCurExp;            //游戏中当前经验
	int                         iGameNextExp;           //游戏中下一个经验
	int                         iPhotoKey;              //自定义图像ID

	unsigned long               ulCurLevelExperience;   //当前等级经验值   
	unsigned long               ulNextLevelExperience;  //下一个等价经验值
	int                         iSingalGameExperience; //单个游戏的中的经验值 ****************

	char                        cPhotoVerify;
	char                        cSexType;               //玩家性别
	char                        cVipType;               //超级玩家标记,100为GM
	char                        cSeeTableNumExtra;      //旁观新增标记 -1为非旁观
	char                        cIfReady;
	char                        cDisconnectType;
	int                         iLastResult;            //上把结果,只客户端有
	int                         iYBNum;                 //玩家的金元宝数 
	int                         iSencondMoney;          //银拳通
	int                         iPrizeNum;              //玩家奖品全
	int	                        iAreaID;			    //地区ID
	int                         iExpTime;             	//经验值
	int                         iCompeteAmount;			//比赛积分
	char                        szAreaName[20];	        //地区名
	char                        szMoneyLevel[20];       //游戏中财富等级
	char                        cMoneylevelNum;         //游戏中的财富等级值
	char                        cLoginType;			    //登录类型,电脑0 手机1
	char                        cMasterType;            //管理登记   ？？？？
	unsigned char               ucSpecialIdentify;      //管理员特殊身份标识  ？？？？
	char                        cUserStatus;            //用户状态 
} PlayerNodeDef;

typedef struct AuthenReq			//AUTHEN_REQ_MSG  玩家登陆验证请求
{
	MsgHeadDef  msgHeadInfo;                 
	int            iUserID;                     //用户ID
	int            iRoomID;                     //房间RoomID  这里是考虑针对房间登录 房间编号为
	char           szPasswd[PASSWD_LEN];        //用户密码
	char           cLoginType;                  //登录类型
}AuthenReqDef;

typedef struct UserPointLimit                          
{
	int iMinPoint;                                   //最小底注
	int iMaxPoint;                                   //最大底注
}UserPointLimitDef; 

typedef struct SitDownReq			//SITDOWN_REQ_MSG		玩家“坐下”请求
{
	MsgHeadDef          msgHeadInfo;
	int                iBindUserID;                  //绑定一起入座的USERID,比如升级游戏
	unsigned short     iTableNum;                    //桌号 为0表示请求服务器分配座位
	unsigned short     usTableNumExtra;              //桌位置
	UserPointLimitDef  userPointLimit;               //用户入座限制

}SitDownReqDef;

typedef struct ReadyReq				//READY_REQ_MSG 玩家按下“开始”请求，超时自动发
{
	MsgHeadDef      msgHeadInfo;
	char           cReady;
}ReadyReqDef;

typedef struct AuthenRes
{
	MsgHeadDef      msgHeadInfo; 
	char           cError;                      //拥有一个登录出错列表，对应返回客户端出了那些问题
	int            iPlayServerID;
	int            iPlayGameID;                 //如果iPla
}AuthenResDef;

typedef struct SitDownRes						//SITDOWN_RES_MSG	玩家“坐下”请求回应
{	
	MsgHeadDef      msgHeadInfo;
	unsigned short  iTableNum;                       //桌号
	unsigned char   cError;                          //等于0表示入座正常   大于0出错类型（特殊类型138 表示桌子需要设置底注的情况）
	unsigned short  usTableNumExtra;                 //位置 初始值是－1  0'1'2'3'4'5....对应为桌子上的作为号
	char            cPlayerNum;                      //当前桌已经有多少玩家坐下
}SitDownResDef;

typedef struct PlayerInfoNotice		//PLAYER_INFO_NOTICE_MSG	玩家信息通知,玩家在桌上游戏时每局结束后发送，有玩家加入游戏或者离开时发送
{
	MsgHeadDef    msgHeadInfo;
	char					cPlayerNum;
	char					cIfSee;//for see
}PlayerInfoNoticeDef;

#endif //__FACTORYMESSAGE_H__
