﻿#include "BRNNGameViewLayer.h"
#include "LobbyLayer.h"
#include "ClientSocketSink.h"
#include "HXmlParse.h"
#include "LocalDataUtil.h"
#include "JniSink.h"
#include "VisibleRect.h"
#include "HttpConstant.h"
#include "Screen.h"
#include "LobbySocketSink.h"
#include "LoginLayer.h"
#include "GameLayerMove.h"
#include "GameGetScore.h"
#include "Convert.h"

#define		MENU_INIT_POINT			(Vec2Make(-_STANDARD_SCREEN_CENTER_.x+68+30 , _STANDARD_SCREEN_CENTER_.y-68-20))

#define		ADV_SIZE				(CCSizeMake(520,105))
//宝箱坐标
#define		BOX_POINT				(Vec2(1195,70))

//最少筹码数
#define		MIN_PROP_PAY_SCORE			1
#define		ALL_ANIMATION_COUNT			5


#define		MAX_CREDIT					(9999999)
#define		MAX_BET						(9999)
#define		MAX_ADV_COUNT				(10)

#if (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
#define		BIG_AWARD_MULITY			10
#else
#define		BIG_AWARD_MULITY			30
#endif // _DEBUG

#define JETTON_COUNT				7

#define Btn_Ready						1
#define Btn_BackToLobby					2
#define Btn_Seting						3
#define Btn_CallBank					4
#define Btn_QiangBank					5
#define Btn_100							6
#define Btn_1000						7
#define Btn_1w							8
#define Btn_10w							9
#define Btn_100w						10
#define Btn_500w						11
#define Btn_1000w						12
#define Btn_CancleBank					13
#define Btn_Uplist						14
#define Btn_Downlist					15
#define Btn_LZ							16
#define Btn_LzClose						17
#define Btn_LzLeft						18
#define Btn_LzRight						19
#define Btn_BankBtn						20
#define Btn_GetScoreBtn					21


BRNNGameViewLayer::BRNNGameViewLayer(GameScene *pGameScene)
	:IGameView(pGameScene)
{
	m_BankZhangJI = 0;
	m_BankJuShu = 0;
	m_wCurrentBanker = INVALID_CHAIR;
	m_LookMode = false;
	m_lAllJettonScore[0] = 0;
	m_lAllJettonScore[1] = 0;
	m_lAllJettonScore[2] = 0;
	m_lAllJettonScore[3] = 0;
	m_BankListBeginIndex = 0;
	m_CurSelectGold = 0;
	m_OpenBankList = false;
	m_lPlayAllScore = 0;
	m_lBankerCurGameScore = 0;
	m_lPlayReturnScore = 0;
	m_lBankerRevenue = 0;
	m_lMeStatisticScore = 0;
	m_lCompetitionScore = 3000000;
	m_IsMeBank = false;
	for (int i = 0; i < 4; i++)
	{
		m_lUserJettonScore[i] = 0;
		m_lAllJettonScore[i]  = 0;
	}
	m_LzShow = false;
	m_GameState=enGameNormal;
	SetKindId(BRNN_KIND_ID);
}

BRNNGameViewLayer::~BRNNGameViewLayer() 
{

}

BRNNGameViewLayer *BRNNGameViewLayer::create(GameScene *pGameScene)
{
    BRNNGameViewLayer *pLayer = new BRNNGameViewLayer(pGameScene);
    if(pLayer && pLayer->init())
    {
        pLayer->autorelease();
        return pLayer;
    }
    else
    {
        CC_SAFE_DELETE(pLayer);
        return NULL;
    }
}


bool BRNNGameViewLayer::init()
{
	if ( !Layer::init() )
	{
		return false;
	}

	setLocalZOrder(3);

	Tools::addSpriteFrame("BRNN/BRNN.plist");
	Tools::addSpriteFrame("Common/CardSprite2.plist");
	SoundUtil::sharedEngine()->playBackMusic("BRNN/sound/BACK_GROUND", true);
    SoundUtil::sharedEngine()->setBackSoundVolume(g_GlobalUnits.m_fBackMusicValue);
    SoundUtil::sharedEngine()->setSoundVolume(g_GlobalUnits.m_fSoundValue);
	IGameView::onEnter();
	JniSink::share()->setIGameView(this);
	setGameStatus(GS_FREE);

	InitGame();
	AddButton();

	AddPlayerInfo();

	setTouchEnabled(true);
	auto listener = EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(true);
	listener->onTouchBegan = CC_CALLBACK_2(BRNNGameViewLayer::onTouchBegan, this);
	listener->onTouchMoved = CC_CALLBACK_2(BRNNGameViewLayer::onTouchMoved, this);
	listener->onTouchEnded = CC_CALLBACK_2(BRNNGameViewLayer::onTouchEnded, this);
	_eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);

	return true;
}


void BRNNGameViewLayer::InitGame()
{
	//背景图
	m_BackSpr = Sprite::create("BRNN/BRNN_back.jpg");
	m_BackSpr->setPosition(_STANDARD_SCREEN_CENTER_IN_PIXELS_);
	addChild(m_BackSpr);

    Sprite* choseSpr = Sprite::createWithSpriteFrameName("choumakuang.png");
    choseSpr->setAnchorPoint(Vec2(0.5f,0));
    choseSpr->setPosition(Vec2(_STANDARD_SCREEN_CENTER_.x,0));
    addChild(choseSpr);
    
    ui::Scale9Sprite* bankBg = ui::Scale9Sprite::createWithSpriteFrameName("bg_bankInfo.png");
    bankBg->setContentSize(Size(340, 160));
    bankBg->setPosition(Vec2(680, 915));
    addChild(bankBg);
    
    string name[] = {"tian.png", "di.png", "xuan.png", "huang.png"};
    for (int i =0; i < 4; i ++)
    {
        Sprite* men = Sprite::createWithSpriteFrameName("xiazhudi.png");
        men->setPosition(Vec2(435 + i * 350, 590));
        addChild(men);
        
        Sprite* temp = Sprite::createWithSpriteFrameName(name[i]);
        temp->setPosition(Vec2(435 + i * 350, 600));
        addChild(temp);
    }
    //区域位置
    m_rcTianMen =   Rect(315, 475, 240, 230);
    m_rcDimen =     Rect(665, 475, 240, 230);
    m_rcXuanMen =   Rect(1015, 475, 240, 230);
    m_rcHuangMen =  Rect(1365, 475, 240, 230);
    
	m_ClockSpr = Sprite::createWithSpriteFrameName("BRNN_TimeBack.png");
	m_ClockSpr->setPosition(Vec2(340, 900));
	m_ClockSpr->setVisible(false);
	addChild(m_ClockSpr);

	m_BankListBackSpr = Sprite::createWithSpriteFrameName("tips_back.png");
	m_BankListBackSpr->setPosition(Vec2(_STANDARD_SCREEN_CENTER_.x, 1500));
	addChild(m_BankListBackSpr,10000);
    
    Sprite* bankTitle = Sprite::createWithSpriteFrameName("shangzhuangliebiao.png");
    bankTitle->setPosition(Vec2(468,550));
    bankTitle->setScale(0.8f);
    m_BankListBackSpr->addChild(bankTitle);
    
    m_listView = ui::ListView::create();
    m_listView->setContentSize(Size(830, 400));
    m_listView->setDirection(ui::ScrollView::Direction::VERTICAL);
    m_listView->setPosition(Vec2(50,80));
    m_BankListBackSpr->addChild(m_listView);
    
    m_LzBack = Sprite::createWithSpriteFrameName("tips_back.png");
	m_LzBack->setPosition(Vec2(_STANDARD_SCREEN_CENTER_.x, 1500));
	addChild(m_LzBack,100);
    
    Sprite* lzTitle = Sprite::createWithSpriteFrameName("luzi.png");
    lzTitle->setScale(0.8f);
    lzTitle->setPosition(Vec2(468,550));
    m_LzBack->addChild(lzTitle);
    
    for (int i = 0; i < 4; i++)
    {
        Sprite* temp = Sprite::createWithSpriteFrameName("lu_"+name[i]);
        temp->setPosition(Vec2(100, 420-i*100));
        m_LzBack->addChild(temp);
    }
    
    m_bCanRobBanker    = false;
	m_Head_Tga         = 100;
	m_NickName_Tga     = 101;
	m_Glod_Tga	       = 102;
	m_WinGlod_Tga      = 103;
	m_BankNickName_Tga = 104;
	m_BankGold_Tga     = 105;
	m_BankZhangJi_Tga  = 106;
	m_BankJuShu_Tga    = 107;
	m_TalbeGold_Tga[0] = 108;
	m_TalbeGold_Tga[1] = 109;
	m_TalbeGold_Tga[2] = 110;
	m_TalbeGold_Tga[3] = 111;
	m_BetScore_Tga[0]  = 112;
	m_BetScore_Tga[1]  = 113;
	m_BetScore_Tga[2]  = 114;
	m_BetScore_Tga[3]  = 115;
	m_TimeType_Tga     = 116;
    m_LightArea_Tga[0] = 117;
    m_LightArea_Tga[1] = 118;
    m_LightArea_Tga[2] = 119;
    m_LightArea_Tga[3] = 120;
	m_BtnAnim_Tga	   = 121;

	m_BetMe_Tga[0]     = 126;
	m_BetMe_Tga[1]     = 127;
	m_BetMe_Tga[2]     = 128;
	m_BetMe_Tga[3]     = 129;
	m_TableCard_Tga[0] = 130;
	m_TableCard_Tga[1] = 131;
	m_TableCard_Tga[2] = 132;
	m_TableCard_Tga[3] = 133;
	m_TableCard_Tga[4] = 134;
    m_CardType_Tga     = 135;
    m_FirstCardTga        = 140;
	m_LzCell_Tga = 1000;

	//pos
	m_HeadPos          = Vec2(350,80);
	m_NickNamePos      = Vec2(140,115);
	m_GlodPos          = Vec2(140,75);
	m_WinGlodPos       = Vec2(140,35);
    
	m_BankNickNamePos  = Vec2(540,975);
	m_BankGoldPos      = Vec2(540,935);
	m_BankZhangJiPos   = Vec2(540,895);
	m_BankJuShuPos	   = Vec2(540,855);
	
    m_CardPos[0]	   = Vec2(910,840);
	m_CardPos[1]	   = Vec2(300,230);
	m_CardPos[2]	   = Vec2(650,230);
	m_CardPos[3]	   = Vec2(1000,230);
	m_CardPos[4]	   = Vec2(1350,230);
    
	m_BetNumPos[0]     = Vec2(435,470);
	m_BetNumPos[1]     = Vec2(785,470);
	m_BetNumPos[2]     = Vec2(1135,470);
	m_BetNumPos[3]     = Vec2(1485,470);
    
	m_CardTypePos[0]   = Vec2(1060,900);
	m_CardTypePos[1]   = Vec2(440,290);
	m_CardTypePos[2]   = Vec2(790,290);
	m_CardTypePos[3]   = Vec2(1140,290);
	m_CardTypePos[4]   = Vec2(1490,290);
	
    m_BankListPos[0]   = Vec2(80,267);
	m_BankListPos[1]   = Vec2(80,208);
	m_BankListPos[2]   = Vec2(80,149);
	m_BankListPos[3]   = Vec2(80,90);
    m_IsLayerMove = false;
    m_GameRecordArrary.clear();
}

void BRNNGameViewLayer::AddButton()
{
	m_BtnBackToLobby = CreateButton("Returnback" ,Vec2(90,1000),Btn_BackToLobby);
	addChild(m_BtnBackToLobby , 0 , Btn_BackToLobby);

	m_BtnSeting = CreateButton("seting" ,Vec2(1830,1000),Btn_Seting);
	addChild(m_BtnSeting , 0 , Btn_Seting);

	m_Gold100 = CreateButton("chip_100" ,Vec2(700,80),Btn_100);
	addChild(m_Gold100 , 0 , Btn_100);

	m_Gold1000 = CreateButton("chip_1000" ,Vec2(870,80),Btn_1000);
	addChild(m_Gold1000 , 0 , Btn_1000);

	m_Gold1w = CreateButton("chip_1w" ,Vec2(1040,80),Btn_1w);
	addChild(m_Gold1w , 0 , Btn_1w);

	m_Gold10w = CreateButton("chip_10w" ,Vec2(1210,80),Btn_10w);
	addChild(m_Gold10w , 0 , Btn_10w);

	m_Gold100w = CreateButton("chip_100w" ,Vec2(1380,80),Btn_100w);
	addChild(m_Gold100w , 0 , Btn_100w);

	m_Gold500w = CreateButton("chip_500w" ,Vec2(1550,80),Btn_500w);
	addChild(m_Gold500w , 0 , Btn_500w);

	m_Gold1000w = CreateButton("chip_1000w" ,Vec2(1720,80),Btn_1000w);
	addChild(m_Gold1000w , 0 , Btn_1000w);

	m_BankBtn = CreateButton("bankBtn" ,Vec2(1800, 280),Btn_BankBtn);
	addChild(m_BankBtn , 0 , Btn_BankBtn);

	m_BtnCallBank = CreateButton("BtnCallBank" ,Vec2(1600,930),Btn_CallBank);
	addChild(m_BtnCallBank , 0 , Btn_CallBank);

	m_BtnQiangBank = CreateButton("BtnQiangBank" ,Vec2(1340,930),Btn_QiangBank);
	addChild(m_BtnQiangBank , 0 , Btn_QiangBank);
    m_BtnQiangBank->setVisible(false);

	m_BtnCancelBank = CreateButton("BtnCancleBank" ,Vec2(1600,930),Btn_CancleBank);
	addChild(m_BtnCancelBank , 0 , Btn_CancleBank);
	m_BtnCancelBank->setVisible(false);

	m_BtnLZBtn = CreateButton("lzBtn" ,Vec2(120,280),Btn_LZ);
	addChild(m_BtnLZBtn , 0 , Btn_LZ);
}

void BRNNGameViewLayer::DrawBankList()
{
    m_listView->removeAllItems();
    for (int i = 0; i < m_BankList.size(); i++)
    {
        ui::Layout* custom_item = ui::Layout::create();
        custom_item->setContentSize(Size(830, 95));
        
        Sprite* lable = Sprite::createWithSpriteFrameName("lable.png");
        lable->setPosition(415, 47);
        custom_item->addChild(lable);
        
        //头像
        string heads = g_GlobalUnits.getFace(m_BankList[i].wGender, m_BankList[i].lUserScore);
        Sprite* mHead = Sprite::createWithSpriteFrameName(heads);
        mHead->setPosition(Vec2(90, 47));
        mHead->setScale(0.5f);
        custom_item->addChild(mHead);
  
        //昵称
        std::string szTempTitle = gbk_utf8(m_BankList[i].strUserName);
        Label* temp = Label::createWithSystemFont(szTempTitle.c_str(),_GAME_FONT_NAME_1_,40);
        temp->setAnchorPoint(Vec2(0,0.5f));
        
        if (!strcmp(GetMeUserData()->szNickName, m_BankList[i].strUserName))
            temp->setColor(Color3B::RED);
        else
            temp->setColor(Color3B::WHITE);
        temp->setPosition(Vec2(150, 47));
        custom_item->addChild(temp);

        
        Sprite* goldIcon = Sprite::createWithSpriteFrameName("gold_icon.png");
        goldIcon->setPosition(Vec2(560, 47));
        custom_item->addChild(goldIcon);
        char _score[32];
        if (m_BankList[i].lUserScore > 1000000000000)
        {
            sprintf(_score,"%.02lf兆", m_BankList[i].lUserScore / 1000000000000.0);
        }
        else if (m_BankList[i].lUserScore > 100000000)
        {
            sprintf(_score,"%.02lf亿", m_BankList[i].lUserScore / 100000000.0);
        }
        else if (m_BankList[i].lUserScore > 1000000)
        {
            sprintf(_score,"%.02lf万", m_BankList[i].lUserScore / 10000.0);
        }
        temp = Label::createWithSystemFont(_score,_GAME_FONT_NAME_1_,40);
        temp->setAnchorPoint(Vec2(0,0.5f));
        temp->setColor(Color3B::WHITE);
        temp->setPosition(Vec2(600, 47));
        custom_item->addChild(temp);
        m_listView->pushBackCustomItem(custom_item);
    }
}

void BRNNGameViewLayer::onEnter ()
{	
	
}

void BRNNGameViewLayer::onExit()
{
	Tools::removeSpriteFrameCache("BRNN/BRNN.plist");
	Tools::removeSpriteFrameCache("Common/CardSprite2.plist");
	IGameView::onExit();
}

//更新控制
void BRNNGameViewLayer::UpdateButtonContron()
{
	//不是下注状态,自己是庄家
	if (getGameStatus() != BRNN_GAME_SCENE_PLACE_JETTON || m_wCurrentBanker == GetMeChairID() || m_bNoBankUser == true)
	{
		removeAllChildByTag(m_BtnAnim_Tga);
		m_Gold100->setEnabled(false);
		m_Gold1000->setEnabled(false);
		m_Gold1w->setEnabled(false);
		m_Gold10w->setEnabled(false);
		m_Gold100w->setEnabled(false);
		m_Gold500w->setEnabled(false);
		m_Gold1000w->setEnabled(false);
		m_Gold100->setColor(Color3B(100,100,100));
		m_Gold1000->setColor(Color3B(100,100,100));
		m_Gold1w->setColor(Color3B(100,100,100));
		m_Gold10w->setColor(Color3B(100,100,100));
		m_Gold100w->setColor(Color3B(100,100,100));
		m_Gold500w->setColor(Color3B(100,100,100));
		m_Gold1000w->setColor(Color3B(100,100,100));
	}
	else
	{

		//计算积分
		LONGLONG lCurrentJetton=m_CurSelectGold;
		LONGLONG lLeaveScore=m_lMeMaxScore;
		for (int nAreaIndex=0; nAreaIndex < AREA_COUNT; ++nAreaIndex) lLeaveScore -= m_lUserJettonScore[nAreaIndex];

		//最大下注
		LONGLONG lUserMaxJetton=GetUserMaxJetton();

		//设置光标
		lLeaveScore = lLeaveScore < lUserMaxJetton ? lLeaveScore : lUserMaxJetton;
		if (lCurrentJetton>lLeaveScore)
		{
			if (lLeaveScore>=10000000L) Select1000w();
			else if (lLeaveScore>=5000000L) Select500w();
			else if (lLeaveScore>=1000000L) Select100w();
			else if (lLeaveScore>=100000L) Select10w();
			else if (lLeaveScore>=10000L) Select1w();
			else if (lLeaveScore>=1000L) Select1000();
			else if (lLeaveScore>=100L) Select100();
			else Select0();
		}

		//控制按钮
		if (lLeaveScore>=100)
		{
			m_Gold100->setEnabled(true);
			m_Gold100->setColor(Color3B(255,255,255));
		}
		else
		{
			m_Gold100->setEnabled(false);
			m_Gold100->setColor(Color3B(100,100,100));
		}

		if (lLeaveScore>=1000)
		{
			m_Gold1000->setEnabled(true);
			m_Gold1000->setColor(Color3B(255,255,255));
		}
		else
		{
			m_Gold1000->setEnabled(false);
			m_Gold1000->setColor(Color3B(100,100,100));
		}

		if (lLeaveScore>=10000)
		{
			m_Gold1w->setEnabled(true);
			m_Gold1w->setColor(Color3B(255,255,255));
		}
		else
		{
			m_Gold1w->setEnabled(false);
			m_Gold1w->setColor(Color3B(100,100,100));
		}

		if (lLeaveScore>=100000)
		{
			m_Gold10w->setEnabled(true);
			m_Gold10w->setColor(Color3B(255,255,255));
		}
		else
		{
			m_Gold10w->setEnabled(false);
			m_Gold10w->setColor(Color3B(100,100,100));
		}

		if (lLeaveScore>=1000000)
		{
			m_Gold100w->setEnabled(true);
			m_Gold100w->setColor(Color3B(255,255,255));
		}
		else
		{
			m_Gold100w->setEnabled(false);
			m_Gold100w->setColor(Color3B(100,100,100));
		}

		if (lLeaveScore>=5000000)
		{
			m_Gold500w->setEnabled(true);
			m_Gold500w->setColor(Color3B(255,255,255));
		}
		else
		{
			m_Gold500w->setEnabled(false);
			m_Gold500w->setColor(Color3B(100,100,100));
		}

		if (lLeaveScore>=10000000)
		{
			m_Gold1000w->setEnabled(true);
			m_Gold1000w->setColor(Color3B(255,255,255));
		}
		else
		{
			m_Gold1000w->setEnabled(false);
			m_Gold1000w->setColor(Color3B(100,100,100));
		}
	}

	return;
}

//最大下注
LONGLONG BRNNGameViewLayer::GetUserMaxJetton()
{
	//已下注额, 限制总额
	LONGLONG lNowJetton = 0, lLimitTotalScore = 0;
	ASSERT(AREA_COUNT <= CountArray(m_lUserJettonScore));
	for (int nAreaIndex = 0; nAreaIndex < AREA_COUNT; ++nAreaIndex)
    {
        lNowJetton += m_lUserJettonScore[nAreaIndex];
        lLimitTotalScore += m_lAreaLimitScore[nAreaIndex];
    }
    
	//庄家游戏币
	LONGLONG lBankerScore = 2147483647;
	if (m_wCurrentBanker != INVALID_CHAIR)
        lBankerScore = m_lBankerScore;
    
	for (int nAreaIndex = 0; nAreaIndex < AREA_COUNT; ++nAreaIndex)
        lBankerScore -= m_lAllJettonScore[nAreaIndex];

	//区域限制
	LONGLONG lMeMaxScore = 0;
    
	if((m_lMeMaxScore-lNowJetton) > lLimitTotalScore)
	{
		lMeMaxScore= lLimitTotalScore;
	}
    else
	{
		lMeMaxScore = m_lMeMaxScore - lNowJetton;
	}

	//庄家限制
	lMeMaxScore= lMeMaxScore < lBankerScore ? lMeMaxScore : lBankerScore;

	//非零限制
	lMeMaxScore = lMeMaxScore > 0? lMeMaxScore: 0; 

	return lMeMaxScore;
}

// Touch 触发
bool BRNNGameViewLayer::onTouchBegan(Touch *pTouch, Event *pEvent)
{
    if (m_OpenBankList)
    {
        CloseBankList();
        return false;
    }
    else if(m_LzShow)
    {
        CloseLzList();
        return false;
    }
	
    Vec2 touchLocation = pTouch->getLocation(); // 返回GL坐标
	Vec2 localPos = convertToNodeSpace(touchLocation);
	if (getGameStatus() != BRNN_GAME_SCENE_PLACE_JETTON) return true;
	if (m_CurSelectGold == 0) return true;
	if ( GetMeChairID() == m_wCurrentBanker ) return true;

	//天
	int cbJettonArea = -1;
	LONGLONG lJettonScore = m_CurSelectGold;
	if (m_rcTianMen.containsPoint(localPos)) 
	{
		SoundUtil::sharedEngine()->playEffect("buttonMusic");
		cbJettonArea = 0;
	}
	else if (m_rcDimen.containsPoint(localPos)) 
	{
		SoundUtil::sharedEngine()->playEffect("buttonMusic");
		cbJettonArea = 1;
	}
	else if (m_rcXuanMen.containsPoint(localPos)) 
	{
		SoundUtil::sharedEngine()->playEffect("buttonMusic");
		cbJettonArea = 2;
	}
	else if (m_rcHuangMen.containsPoint(localPos)) 
	{
		SoundUtil::sharedEngine()->playEffect("buttonMusic");
		cbJettonArea = 3;
	}
	if(cbJettonArea == -1) return true;
    
	BRNN_CMD_C_PlaceJetton PlaceJetton;
	ZeroMemory(&PlaceJetton,sizeof(PlaceJetton));
	PlaceJetton.cbJettonArea=cbJettonArea;
	PlaceJetton.lJettonScore=lJettonScore;
	SendData(SUB_C_PLACE_JETTON,&PlaceJetton,sizeof(PlaceJetton));

	return true;
}

void BRNNGameViewLayer::onTouchMoved(Touch *pTouch, Event *pEvent)
{
}

void BRNNGameViewLayer::onTouchEnded(Touch*pTouch, Event*pEvent)
{
}

// Alert Message 确认消息处理
void BRNNGameViewLayer::DialogConfirm(Ref *pSender)
{
	this->RemoveAlertMessageLayer();
	this->SetAllTouchEnabled(true);
}

// Alert Message 取消消息处理
void BRNNGameViewLayer::DialogCancel(Ref *pSender)
{
	this->RemoveAlertMessageLayer();
	this->SetAllTouchEnabled(true);
}

void BRNNGameViewLayer::OnEventUserScore( tagUserData * pUserData, WORD wChairID, bool bLookonUser )
{
	IGameView::OnEventUserScore(pUserData, wChairID, bLookonUser);
    AddPlayerInfo();
}

void BRNNGameViewLayer::OnEventUserStatus(tagUserData * pUserData, WORD wChairID, bool bLookonUser)
{
	IGameView::OnEventUserStatus(pUserData, wChairID, bLookonUser);
	AddPlayerInfo();
}

void BRNNGameViewLayer::OnEventUserEnter( tagUserData * pUserData, WORD wChairID, bool bLookonUser )
{
	AddPlayerInfo();
}

void BRNNGameViewLayer::OnEventUserLeave( tagUserData * pUserData, WORD wChairID, bool bLookonUser )
{
	AddPlayerInfo();
}


void BRNNGameViewLayer::AddPlayerInfo()
{
	if (ClientSocketSink::sharedSocketSink()->GetMeUserData() == NULL) return;
	int userid = ClientSocketSink::sharedSocketSink()->GetMeUserData()->dwUserID;
	const tagUserData* pUserData = ClientSocketSink::sharedSocketSink()->m_UserManager.SearchUserByUserID(userid);
	if (pUserData == NULL) return;
    removeAllChildByTag(m_Head_Tga);
    
	//头像
    ui::Scale9Sprite* playerInfoBg = ui::Scale9Sprite::createWithSpriteFrameName("bg_player_info.png");
    playerInfoBg->setContentSize(Size(450, 150));
    playerInfoBg->setPosition(Vec2(m_HeadPos.x, m_HeadPos.y+5));
    addChild(playerInfoBg, 0, m_Head_Tga);
    
    Sprite* headBg = Sprite::createWithSpriteFrameName("bg_head_img.png");
    headBg->setPosition(Vec2(70, 75));
    playerInfoBg->addChild(headBg);
    
    string heads = g_GlobalUnits.getFace(pUserData->wGender, pUserData->lScore);
    Sprite* mHead = Sprite::createWithSpriteFrameName(heads);
    mHead->setPosition(Vec2(70, 75));
    mHead->setScale(0.9f);
    playerInfoBg->addChild(mHead);

    removeAllChildByTag(m_Glod_Tga);
    char strc[32]="";
    LONGLONG lMon = pUserData->lScore;
    memset(strc , 0 , sizeof(strc));
    Tools::AddComma(lMon , strc);
    
    string goldstr = "酒吧豆: ";
    goldstr.append(strc);
    Label* mGoldFont;
    mGoldFont = Label::createWithSystemFont(goldstr,_GAME_FONT_NAME_1_,30);
    mGoldFont->setTag(m_Glod_Tga);
    playerInfoBg->addChild(mGoldFont);
    mGoldFont->setColor(Color3B::YELLOW);
    mGoldFont->setAnchorPoint(Vec2(0, 0.5f));
    mGoldFont->setPosition(m_GlodPos);
    
    //昵称
    removeAllChildByTag(m_NickName_Tga);
    string namestr = "昵称: ";
    namestr.append(gbk_utf8(pUserData->szNickName));
    Label* mNickfont = Label::createWithSystemFont(namestr, _GAME_FONT_NAME_1_, 30);
    mNickfont->setTag(m_NickName_Tga);
    playerInfoBg->addChild(mNickfont);
    mNickfont->setAnchorPoint(Vec2(0,0.5f));
    mNickfont->setPosition(m_NickNamePos);
    
	//输赢金币
	removeAllChildByTag(m_WinGlod_Tga);
	Tools::AddComma(m_lMeStatisticScore, strc);
    string winStr = "成绩: ";
    winStr.append(strc);
	mGoldFont = Label::createWithSystemFont(winStr,_GAME_FONT_NAME_1_,30);
	mGoldFont->setAnchorPoint(Vec2(0,0.5f));
	mGoldFont->setTag(m_WinGlod_Tga);
	mGoldFont->setPosition(m_WinGlodPos);
	playerInfoBg->addChild(mGoldFont);
}

Menu* BRNNGameViewLayer::CreateButton(std::string szBtName ,const Vec2 &p , int tag )
{
	return Tools::Button(StrToChar(szBtName+"_normal.png") , StrToChar(szBtName+"_click.png") , p, this , menu_selector(BRNNGameViewLayer::callbackBt) , tag);
}


void BRNNGameViewLayer::StartTime(int _time, int _type)
{
	m_StartTime = _time;
	schedule(schedule_selector(BRNNGameViewLayer::UpdateTime), 1);
	m_ClockSpr->setVisible(true);
	UpdateTime(m_StartTime);

	removeAllChildByTag(m_TimeType_Tga);
	std::string szTempTitle;
	if (_type == 0)
        szTempTitle = "\u7a7a\u95f2\u65f6\u95f4";//空闲时间
	else if(_type == 1)
        szTempTitle = "\u4e0b\u6ce8\u65f6\u95f4";//下注时间
	else if(_type == 2)
        szTempTitle = "\u5f00\u724c\u65f6\u95f4";//开牌时间

	Label* temp = Label::createWithSystemFont(szTempTitle.c_str(),_GAME_FONT_NAME_1_,46);
	temp->setColor(_GAME_FONT_COLOR_5_);
	temp->setPosition(Vec2(340, 820));
	temp->setTag(m_TimeType_Tga);
	addChild(temp);
}

void BRNNGameViewLayer::StopTime()
{
	unschedule(schedule_selector(BRNNGameViewLayer::UpdateTime));
	m_ClockSpr->removeAllChildren();
	m_ClockSpr->setVisible(false);
}

void BRNNGameViewLayer::UpdateTime(float fp)
{
	if (m_StartTime < 0)
	{
		return;
	}
	m_ClockSpr->removeAllChildren();
    std::string str = StringUtils::toString(m_StartTime);
    if (m_StartTime < 10)
        str = "0" + str;
    LabelAtlas *time = LabelAtlas::create(str, "Common/time_1.png", 23, 35, '+');
    time->setPosition(Vec2(20, 25));
    m_ClockSpr->addChild(time);
    
    m_StartTime--;
}

//游戏空闲
bool BRNNGameViewLayer::OnSubGameFree(const void * pBuffer, WORD wDataSize)
{
	//效验数据
	if (wDataSize!=sizeof(BRNN_CMD_S_GameFree)) return false;
	//消息处理
	BRNN_CMD_S_GameFree * pGameFree=(BRNN_CMD_S_GameFree *)pBuffer;

	//设置时间
	StartTime(pGameFree->cbTimeLeave,0);
    m_GameState = enGameNormal;
    
    //自己是庄家
    if (m_IsMeBank)
    {
        m_BtnCancelBank->setVisible(true);
        m_BtnCancelBank->setEnabled(true);
        m_BtnCancelBank->setColor(Color3B(255,255,255));
    }
    
    if (m_nRobBanker < 3 && m_bCanRobBanker && false == m_IsMeBank)
    {
        m_BtnQiangBank->setEnabled(true);
        m_BtnQiangBank->setColor(Color3B::WHITE);
    }
    else
    {
        m_BtnQiangBank->setEnabled(false);
        m_BtnQiangBank->setColor(Color3B(100, 100, 100));
    }
    
    //清理桌面
	for (int i = 0; i < 4; i++)
	{
		removeAllChildByTag(m_TalbeGold_Tga[i]); 
		removeAllChildByTag(m_BetScore_Tga[i]); 
		removeAllChildByTag(m_BetMe_Tga[i]); 
		m_lUserJettonScore[i] = 0;
		m_lAllJettonScore[i] = 0;

	}
	removeAllChildByTag(m_TableCard_Tga[0]);
	removeAllChildByTag(m_TableCard_Tga[1]);
	removeAllChildByTag(m_TableCard_Tga[2]);
	removeAllChildByTag(m_TableCard_Tga[3]);
	removeAllChildByTag(m_TableCard_Tga[4]);
	removeAllChildByTag(m_CardType_Tga);

	m_CurSelectGold = 0;

	////更新控件
	UpdateButtonContron();

	DrawEndScore();
    for (int i = 0; i < 4; i++)
        removeAllChildByTag(m_LightArea_Tga[i]);
    
    return true;
}


//设置庄家
void BRNNGameViewLayer::SetBankerInfo(WORD wBanker,LONGLONG lScore)
{
	m_wCurrentBanker=wBanker;
	char name[32];
	int tableid = ClientSocketSink::sharedSocketSink()->GetMeUserData()->wTableID;
	tagUserData* pUserData = ClientSocketSink::sharedSocketSink()->m_UserManager.GetUserData(tableid,m_wCurrentBanker);
	
    //昵称
    removeAllChildByTag(m_BankNickName_Tga);
    removeAllChildByTag(m_BankGold_Tga);
    removeAllChildByTag(m_BankZhangJi_Tga);
    removeAllChildByTag(m_BankJuShu_Tga);
   
    if(m_wCurrentBanker == INVALID_CHAIR || pUserData == NULL)
	{
		m_BankZhangJI = 0;
		m_lBankerScore = 0;
		m_BankJuShu = 0;
		m_bNoBankUser = true;
        
        Label* temp = Label::createWithSystemFont("无人坐庄", _GAME_FONT_NAME_1_,46);
        temp->setColor(Color3B::RED);
        temp->setTag(m_BankNickName_Tga);
        temp->setPosition(Vec2(680, 915));
        addChild(temp);
        return;
    }
	else 
	{
		m_lBankerScore=lScore;
		m_bNoBankUser = false;
		sprintf(name, "%s", gbk_utf8(pUserData->szNickName).c_str());
	}
    
    std::string szTempTitle;
    szTempTitle = "昵称: ";
    szTempTitle.append(name);
    
	Label* temp = Label::createWithSystemFont(szTempTitle.c_str(),_GAME_FONT_NAME_1_,30);
	temp->setAnchorPoint(Vec2(0,0.5f));
	temp->setTag(m_BankNickName_Tga);
	temp->setPosition(m_BankNickNamePos);
	addChild(temp);

	//金币
	char strc[32]="";
	Tools::AddComma(m_lBankerScore , strc);
    szTempTitle = "酒吧豆: ";
    szTempTitle.append(strc);
	temp = Label::createWithSystemFont(szTempTitle, _GAME_FONT_NAME_1_, 30);
	temp->setAnchorPoint(Vec2(0,0.5f));
	temp->setTag(m_BankGold_Tga);
	temp->setColor(_GAME_FONT_COLOR_4_);
	temp->setPosition(m_BankGoldPos);
	addChild(temp);

	//战绩
	Tools::AddComma(m_BankZhangJI , strc);
    szTempTitle = "成绩: ";
    szTempTitle.append(strc);
	temp = Label::createWithSystemFont(szTempTitle, _GAME_FONT_NAME_1_, 30);
	temp->setAnchorPoint(Vec2(0,0.5f));
	temp->setTag(m_BankZhangJi_Tga);
	temp->setColor(_GAME_FONT_COLOR_4_);
	temp->setPosition(m_BankZhangJiPos);
	addChild(temp);

	//局数
	sprintf(strc,"%d",m_BankJuShu);
    szTempTitle = "局数: ";
    szTempTitle.append(strc);
	temp = Label::createWithSystemFont(szTempTitle, _GAME_FONT_NAME_1_, 30);
	temp->setAnchorPoint(Vec2(0,0.5f));
	temp->setTag(m_BankJuShu_Tga);
	temp->setColor(_GAME_FONT_COLOR_4_);
	temp->setPosition(m_BankJuShuPos);
	addChild(temp);

	UpdateButtonContron();
}

bool BRNNGameViewLayer::OnSubGameStart( const void * pBuffer, WORD wDataSize )
{
	//效验数据
	if (wDataSize!=sizeof(BRNN_CMD_S_GameStart)) return false;

	//消息处理
	BRNN_CMD_S_GameStart *pGameStart = (BRNN_CMD_S_GameStart *)pBuffer;
    CopyMemory(m_lAreaLimitScore, pGameStart->lAreaLimitScore, sizeof(m_lAreaLimitScore));
	setGameStatus(BRNN_GAME_SCENE_PLACE_JETTON);
    m_GameState = enGameBet;
    
    if (m_GameEndBack_Tga != Node::INVALID_TAG)
    {
        auto child = getChildByTag(m_GameEndBack_Tga);
        if (child)
        {
            child->removeFromParent();
        }
    }
	//庄家信息
	SetBankerInfo(pGameStart->wBankerUser,pGameStart->lBankerScore);
	m_lMeMaxScore=pGameStart->lUserMaxScore;

	//设置时间
	StartTime(pGameStart->cbTimeLeave,1);

    //自己是庄家
    if (m_IsMeBank)
    {
        m_BtnCancelBank->setVisible(true);
        m_BtnCancelBank->setEnabled(false);
        m_BtnCancelBank->setColor(Color3B(100,100,100));
    }
    m_BtnQiangBank->setEnabled(false);
    m_BtnQiangBank->setColor(Color3B(100,100,100));
    
	UpdateButtonContron();
	SoundUtil::sharedEngine()->playEffect("BRNN/sound/GAME_START"); 
	return true;
}

bool BRNNGameViewLayer::IsLookonMode()
{
	return m_LookMode;
}

const char* BRNNGameViewLayer::GetGoldName(LONGLONG lScoreCount)
{
	if(lScoreCount >= 10000000) return "chip_1000w_normal.png";
	else if(lScoreCount >= 5000000) return "chip_500w_normal.png";
	else if(lScoreCount >= 1000000) return "chip_100w_normal.png";
	else if(lScoreCount >= 100000) return "chip_10w_normal.png";
	else if(lScoreCount >= 10000) return "chip_1w_normal.png";
	else if(lScoreCount >= 1000) return "chip_1000_normal.png";
	else  return "chip_100_normal.png";
}

//设置筹码
void BRNNGameViewLayer::PlaceUserJetton(BYTE cbViewIndex, LONGLONG lScoreCount)
{
	//效验参数
	ASSERT(cbViewIndex<=ID_HUANG_MEN);
	if (cbViewIndex>ID_HUANG_MEN) return;

	LONGLONG score = 0;
	switch (cbViewIndex)
	{
	case ID_TIAN_MEN:
		{ 
			m_lAllJettonScore[ID_TIAN_MEN] += lScoreCount;
			score = m_lAllJettonScore[ID_TIAN_MEN];
			if(lScoreCount == 0) return;
			const char* name = GetGoldName(lScoreCount);
			Sprite* temp = Sprite::createWithSpriteFrameName(name);
            temp->setScale(0.3f);
			temp->setTag(m_TalbeGold_Tga[0]);
			float cx = m_rcTianMen.getMidX();
			float cy = m_rcTianMen.getMidY();
			int c = CCRANDOM_0_1()*100;
			int c1 = CCRANDOM_0_1()*100;
			int nXPos,nYPos;
			if(c %2 != 0) nXPos= cx+(CCRANDOM_0_1() * 70);
			else nXPos= cx-(CCRANDOM_0_1() * 70);
			if(c1 %2 == 0)nYPos= cy+(CCRANDOM_0_1() * 70);
			else nYPos= cy-(CCRANDOM_0_1() * 70);
			temp->setPosition(Vec2(nXPos,nYPos));
			addChild(temp);
			break;
		}
	case ID_DI_MEN:
		{
			m_lAllJettonScore[ID_DI_MEN] += lScoreCount;
			score = m_lAllJettonScore[ID_DI_MEN];
			if(lScoreCount == 0) return;
			const char* name = GetGoldName(lScoreCount);
			Sprite* temp = Sprite::createWithSpriteFrameName(name);
            temp->setScale(0.3f);
			temp->setTag(m_TalbeGold_Tga[1]);
			float cx = m_rcDimen.getMidX();
			float cy = m_rcDimen.getMidY();
			int c = CCRANDOM_0_1()*100;
			int c1 = CCRANDOM_0_1()*100;
			int nXPos,nYPos;
			if(c %2 != 0) nXPos= cx+(CCRANDOM_0_1() * 70);
			else nXPos= cx-(CCRANDOM_0_1() * 70);
			if(c1 %2 == 0)nYPos= cy+(CCRANDOM_0_1() * 70);
			else nYPos= cy-(CCRANDOM_0_1() * 70);
			temp->setPosition(Vec2(nXPos,nYPos));
			addChild(temp);
			break;
		}
	case ID_XUAN_MEN:
		{ 
			m_lAllJettonScore[ID_XUAN_MEN] += lScoreCount;
			score = m_lAllJettonScore[ID_XUAN_MEN];
			if(lScoreCount == 0) return;
			const char* name = GetGoldName(lScoreCount);
			Sprite* temp = Sprite::createWithSpriteFrameName(name);
            temp->setScale(0.3f);
			temp->setTag(m_TalbeGold_Tga[2]);
			float cx = m_rcXuanMen.getMidX();
			float cy = m_rcXuanMen.getMidY();
			int c = CCRANDOM_0_1()*100;
			int c1 = CCRANDOM_0_1()*100;
			int nXPos,nYPos;
			if(c %2 != 0) nXPos= cx+(CCRANDOM_0_1() * 70);
			else nXPos= cx-(CCRANDOM_0_1() * 70);
			if(c1 %2 == 0)nYPos= cy+(CCRANDOM_0_1() * 70);
			else nYPos= cy-(CCRANDOM_0_1() * 70);
			temp->setPosition(Vec2(nXPos,nYPos));
			addChild(temp);
			break;
		}
	case ID_HUANG_MEN :
		{ 
			m_lAllJettonScore[ID_HUANG_MEN] += lScoreCount;
			score = m_lAllJettonScore[ID_HUANG_MEN];
			if(lScoreCount == 0) return;
			const char* name = GetGoldName(lScoreCount);
			Sprite* temp = Sprite::createWithSpriteFrameName(name);
            temp->setScale(0.3f);
			temp->setTag(m_TalbeGold_Tga[3]);
			float cx = m_rcHuangMen.getMidX();
			float cy = m_rcHuangMen.getMidY();
			int c = CCRANDOM_0_1()*100;
			int c1 = CCRANDOM_0_1()*100;
			int nXPos,nYPos;
			if(c %2 != 0) nXPos= cx+(CCRANDOM_0_1() * 70);
			else nXPos= cx-(CCRANDOM_0_1() * 70);
			if(c1 %2 == 0)nYPos= cy+(CCRANDOM_0_1() * 70);
			else nYPos= cy-(CCRANDOM_0_1() * 70);
			temp->setPosition(Vec2(nXPos,nYPos));
			addChild(temp);
			break;
		}
	}

	//区域总注显示
	removeAllChildByTag(m_BetScore_Tga[cbViewIndex]);
    string scoreStr = StringUtils::toString(score);
    LabelAtlas *totalScore = LabelAtlas::create(scoreStr,"Common/xiazhushuzi.png", 34, 46, '+');
    totalScore->setPosition(m_BetNumPos[cbViewIndex]);
    totalScore->setAnchorPoint(Vec2(0.5f, 0.5f));
    totalScore->setTag(m_BetScore_Tga[cbViewIndex]);
    addChild(totalScore);

	return;
}


void BRNNGameViewLayer::SetMePlaceJetton(int chair, LONGLONG score)
{
	//区域总注显示
	removeAllChildByTag(m_BetMe_Tga[chair]);

    string scoreStr = StringUtils::toString(score);
    LabelAtlas *selfScore = LabelAtlas::create(scoreStr, "Common/gold_self.png", 19, 24, '+');
    selfScore->setPosition(m_BetNumPos[chair]+ Vec2(0, 240));
    selfScore->setAnchorPoint(Vec2(0.5f, 0.5f));
    selfScore->setTag(m_BetMe_Tga[chair]);
    addChild(selfScore, 100);
}


//用户加注
bool BRNNGameViewLayer::OnSubPlaceJetton(const void * pBuffer, WORD wDataSize)
{
	//效验数据
	ASSERT(wDataSize==sizeof(BRNN_CMD_S_PlaceJetton));
	if (wDataSize!=sizeof(BRNN_CMD_S_PlaceJetton)) return false;

	//消息处理
    m_GameState = enGameBet;
    
	BRNN_CMD_S_PlaceJetton * pPlaceJetton=(BRNN_CMD_S_PlaceJetton *)pBuffer;
	
	PlaceUserJetton(pPlaceJetton->cbJettonArea,pPlaceJetton->lJettonScore);			

	if (pPlaceJetton->lJettonScore>=10000000) SoundUtil::sharedEngine()->playEffect("BRNN/sound/ADD_GOLD_EX"); 
	else if (pPlaceJetton->lJettonScore>=5000000) SoundUtil::sharedEngine()->playEffect("BRNN/sound/CHEER1");
	else if (pPlaceJetton->lJettonScore>=1000000) SoundUtil::sharedEngine()->playEffect("BRNN/sound/CHEER2"); 
	else SoundUtil::sharedEngine()->playEffect("BRNN/sound/ADD_GOLD"); 
			

		if (GetMeChairID()==pPlaceJetton->wChairID)
		{
			m_lUserJettonScore[pPlaceJetton->cbJettonArea] += pPlaceJetton->lJettonScore;
			SetMePlaceJetton(pPlaceJetton->cbJettonArea, m_lUserJettonScore[pPlaceJetton->cbJettonArea]);
		}
	UpdateButtonContron();

	return true;
}

//申请做庄
bool BRNNGameViewLayer::OnSubUserApplyBanker(const void * pBuffer, WORD wDataSize)
{
	//效验数据
	ASSERT(wDataSize==sizeof(BRNN_CMD_S_ApplyBanker));
	if (wDataSize!=sizeof(BRNN_CMD_S_ApplyBanker)) return false;
	//消息处理
	BRNN_CMD_S_ApplyBanker * pApplyBanker=(BRNN_CMD_S_ApplyBanker *)pBuffer;
    m_nRobBanker = pApplyBanker->nRobBanker;
    
    const tagUserData * pClientUserItem = GetUserData(pApplyBanker->wApplyUser);
    
    //插入玩家
    if (m_wCurrentBanker != pApplyBanker->wApplyUser)
    {
        tagApplyUser ApplyUser;
        sprintf(ApplyUser.strUserName,"%s", pClientUserItem->szNickName);
        ApplyUser.lUserScore=pClientUserItem->lScore;
        ApplyUser.wGender = pClientUserItem->wGender;
        ApplyUser.wUserID = pClientUserItem->dwUserID;
        if (-1 == pApplyBanker->mInsert)
            m_BankList.push_back(ApplyUser);
        else
            m_BankList.insert(m_BankList.begin() + pApplyBanker->mInsert, ApplyUser);

    }
    
    tagUserData *pUserData = ClientSocketSink::sharedSocketSink()->GetMeUserData();
    if (nullptr != pUserData && m_BankList.size() > 2)
    {
        if (m_BankList[0].wUserID == pUserData->dwUserID || m_BankList[1].wUserID == pUserData->dwUserID)
        {
            m_bCanRobBanker = false;
        }
        else
        {
            m_bCanRobBanker = true;
        }
    }
    else
        m_bCanRobBanker = false;
    
    
    //自己判断
    if (IsLookonMode()==false && GetMeChairID() == pApplyBanker->wApplyUser)
    {
        m_BtnCallBank->setVisible(false);
        m_BtnQiangBank->setVisible(true);
        m_BtnCancelBank->setVisible(true);
        
        if (enGameNormal == m_GameState && m_nRobBanker < 3 && m_bCanRobBanker)
        {
            m_BtnQiangBank->setEnabled(true);
            m_BtnQiangBank->setColor(Color3B::WHITE);
        }
        else
        {
            m_BtnQiangBank->setEnabled(false);
            m_BtnQiangBank->setColor(Color3B(100, 100, 100));
        }
    }
    
    ////更新控件
    DrawBankList();

	UpdateButtonContron();

	return true;
}

void BRNNGameViewLayer::DeleteBankList(tagApplyUser ApplyUser)
{
	for (int i = 0; i < m_BankList.size(); i++)
	{
		if (strcmp(m_BankList[i].strUserName, ApplyUser.strUserName) == 0)
		{
			m_BankList.erase(m_BankList.begin()+i);
			return;
		}
	}
}

//取消做庄
bool BRNNGameViewLayer::OnSubUserCancelBanker(const void * pBuffer, WORD wDataSize)
{
	//效验数据
	ASSERT(wDataSize==sizeof(BRNN_CMD_S_CancelBanker));
	if (wDataSize!=sizeof(BRNN_CMD_S_CancelBanker)) return false;
	BRNN_CMD_S_CancelBanker * pCancelBanker=(BRNN_CMD_S_CancelBanker *)pBuffer;
    m_nRobBanker = pCancelBanker->nRobBanker;
    
	//删除玩家
	tagApplyUser ApplyUser;
	sprintf(ApplyUser.strUserName,"%s",pCancelBanker->szCancelUser);
	ApplyUser.lUserScore=0;
	DeleteBankList(ApplyUser);

    //自己判断
    const tagUserData * pMeUserData= GetMeUserData();
    if (IsLookonMode() == false && strcmp(pMeUserData->szNickName, pCancelBanker->szCancelUser) == 0)
    {
        m_BtnQiangBank->setVisible(false);
        m_BtnCancelBank->setVisible(false);
        m_BtnCallBank->setVisible(true);
        m_bCanRobBanker = false;
    }
    
    if (nullptr != pMeUserData && m_BankList.size() > 2)
    {
        if (m_BankList[0].wUserID == pMeUserData->dwUserID || m_BankList[1].wUserID == pMeUserData->dwUserID)
        {
            m_bCanRobBanker = false;
        }
        else
        {
            m_bCanRobBanker = true;
        }
    }
    else
        m_bCanRobBanker = false;
    
    
    if (enGameNormal == m_GameState && m_nRobBanker < 3 && m_bCanRobBanker)
    {
        m_BtnQiangBank->setEnabled(true);
        m_BtnQiangBank->setColor(Color3B::WHITE);
    }
    else
    {
        m_BtnQiangBank->setEnabled(false);
        m_BtnQiangBank->setColor(Color3B(100, 100, 100));
    }
    
    //更新控件
	UpdateButtonContron();

	DrawBankList();

	return true;
}

bool BRNNGameViewLayer::OnSubUserGrabBanker(const void * pBuffer, WORD wDataSize)
{
	//效验数据
    ASSERT(wDataSize==sizeof(CMD_S_RobBanker));
    if (wDataSize!=sizeof(CMD_S_RobBanker)) return false;
    CMD_S_RobBanker * pRobBanker = (CMD_S_RobBanker *)pBuffer;
    m_nRobBanker = pRobBanker->nRobBanker;
    
    //插入玩家
    int tableid = ClientSocketSink::sharedSocketSink()->GetMeUserData()->wTableID;
    tagUserData* pUserData = ClientSocketSink::sharedSocketSink()->m_UserManager.GetUserData(tableid, pRobBanker->wApplyUser);
    
    int i = 0;
    for (i = 0; i < m_BankList.size(); i++)
    {
        if (pUserData->dwUserID == m_BankList[i].wUserID)
            break;
    }
    
    swap(m_BankList[m_nRobBanker], m_BankList[i]);
    
    tagUserData *pMyData = ClientSocketSink::sharedSocketSink()->GetMeUserData();
    if (nullptr != pMyData && m_BankList.size() > 2)
    {
        if (m_BankList[0].wUserID == pMyData->dwUserID || m_BankList[1].wUserID == pMyData->dwUserID)
        {
            m_bCanRobBanker = false;
        }
        else
        {
            m_bCanRobBanker = true;
        }
    }
    else
        m_bCanRobBanker = false;
    
    //自己判断
    if (IsLookonMode()==false && GetMeChairID() == pRobBanker->wApplyUser)
    {
        m_BtnCallBank->setVisible(false);
        m_BtnQiangBank->setVisible(false);
        m_BtnCancelBank->setVisible(true);
        m_bCanRobBanker = false;
    }
    
    DrawBankList();
	return true;
}

//切换庄家
bool BRNNGameViewLayer::OnSubChangeBanker(const void * pBuffer, WORD wDataSize)
{
	ASSERT(wDataSize==sizeof(BRNN_CMD_S_ChangeBanker));
	if (wDataSize!=sizeof(BRNN_CMD_S_ChangeBanker)) return false;
	BRNN_CMD_S_ChangeBanker * pChangeBanker = (BRNN_CMD_S_ChangeBanker *)pBuffer;
    m_nRobBanker = pChangeBanker->nRobBanker;
	m_BankZhangJI = 0;
	m_BankJuShu = 0;

    //自己是庄家
    if (m_IsMeBank == true)
    {
        m_BtnCancelBank->setVisible(false);
        m_BtnCallBank->setVisible(true);
        m_BtnQiangBank->setVisible(false);
    }
    
    m_IsMeBank = false;
    
    //自己判断
    if (IsLookonMode() == false && pChangeBanker->wBankerUser==GetMeChairID())
    {
        m_BtnCancelBank->setVisible(true);
        m_BtnCancelBank->setEnabled(false);
        m_BtnCancelBank->setColor(Color3B(100,100,100));
        m_BtnCallBank->setVisible(false);
        m_BtnQiangBank->setVisible(false);
        m_BankZhangJI = 0;
        m_BankJuShu = 0;
        m_IsMeBank = true;
    }
    
    //删除玩家
    if (m_wCurrentBanker!=INVALID_CHAIR)
    {
        tagApplyUser ApplyUser;
        sprintf(ApplyUser.strUserName,"%s", GetUserData(m_wCurrentBanker)->szNickName);
        ApplyUser.lUserScore=0;
        DeleteBankList(ApplyUser);
    }
    
    tagUserData *pUserData = ClientSocketSink::sharedSocketSink()->GetMeUserData();
    if (nullptr != pUserData && m_BankList.size() > 2)
    {
        if (m_BankList[0].wUserID == pUserData->dwUserID || m_BankList[1].wUserID == pUserData->dwUserID)
        {
            m_bCanRobBanker = false;
        }
        else
        {
            m_bCanRobBanker = true;
        }
    }
    else
        m_bCanRobBanker = false;
    
    if (enGameNormal == m_GameState && m_nRobBanker < 3 && m_bCanRobBanker)
    {
        m_BtnQiangBank->setEnabled(true);
        m_BtnQiangBank->setColor(Color3B::WHITE);
    }
    else
    {
        m_BtnQiangBank->setEnabled(false);
        m_BtnQiangBank->setColor(Color3B(100, 100, 100));
    }
    
    //庄家信息
    SetBankerInfo(pChangeBanker->wBankerUser, pChangeBanker->lBankerScore);

	//更新界面
	UpdateButtonContron();
	DrawBankList();
	return true;
}


//设置扑克
void BRNNGameViewLayer::SetCardInfo(BYTE cbTableCardArray[5][5], bool bsence)
{
	if (cbTableCardArray!=NULL)
	{
		CopyMemory(m_cbTableCardArray,cbTableCardArray,sizeof(m_cbTableCardArray));
		//开始发牌
		if (bsence == true)
		{
			SendCardAll();
		}
		else
		{
			 SendCard();
		}
	}
	else
	{
		ZeroMemory(m_cbTableCardArray,sizeof(m_cbTableCardArray));
	}
}

void BRNNGameViewLayer::SendCardAll()
{
	for (int n = 0; n < 5; n++)
	{
		for (int i = 0; i < 5; i++)
		{
			string _name = GetCardStringName(m_cbTableCardArray[n][i]);
			Sprite* temp = Sprite::createWithSpriteFrameName(_name.c_str());
			temp->setPosition(Vec2(m_CardPos[n].x + m_SendCardCount*40 + i * 42, m_CardPos[n].y));
			temp->setAnchorPoint(Vec2(0,0));
			temp->setTag(m_TableCard_Tga[n]);
			temp->setScale(0.7f);
			addChild(temp);
		}
	}

}

void BRNNGameViewLayer::SendCard()
{
	m_SendCardCount = 0;
	m_SendCardOver = true;
	schedule(schedule_selector(BRNNGameViewLayer::UpdataSendCard), 1.2f);
}

void BRNNGameViewLayer::UpdataSendCard(float fp)
{
	//第一个发牌玩家
	if (m_SendCardOver == true)
	{
		SoundUtil::sharedEngine()->playEffect("BRNN/sound/DISPATCH_CARD"); 
		m_SendCardOver = false;
		MoveTo *leftMoveBy = MoveTo::create(0.25f, Vec2(m_CardPos[m_SendCardIndex].x+m_SendCardCount*42,m_CardPos[m_SendCardIndex].y));
		ActionInstant *func = CallFuncN::create(CC_CALLBACK_1(BRNNGameViewLayer::CardMoveCallback1, this));
		Sprite* temp = Sprite::createWithSpriteFrameName("BRNNbackCard.png");
		temp->setPosition(_STANDARD_SCREEN_CENTER_);
		temp->setAnchorPoint(Vec2(0,0));
		temp->setScale(0.7f);
		addChild(temp);
		temp->runAction(Sequence::create(leftMoveBy,func,NULL));
	}
		
	if (m_SendCardCount == 5)
	{
		unschedule(schedule_selector(BRNNGameViewLayer::UpdataSendCard));
	}
}

void BRNNGameViewLayer::CardMoveCallback1(Node *pSender)
{
	removeChild(pSender);
	string _name = GetCardStringName(m_cbTableCardArray[m_SendCardIndex][m_SendCardCount]);
	Sprite* temp = Sprite::createWithSpriteFrameName(_name.c_str());
	temp->setPosition(Vec2(m_CardPos[m_SendCardIndex].x+(m_SendCardCount)*42,m_CardPos[m_SendCardIndex].y));
	temp->setAnchorPoint(Vec2(0,0));
	temp->setTag(m_TableCard_Tga[m_SendCardIndex]);
	temp->setScale(0.7f);
	addChild(temp);
	if (m_SendCardCount == 4)
	{
		BYTE bcTmp[5];
		BYTE bCardType=m_GameLogic.GetCardType(m_cbTableCardArray[m_SendCardIndex],5,bcTmp);
		//排序
		BYTE sortcard[5];
		if(bCardType!= CT_POINT)
		{
			CopyMemory(sortcard,bcTmp,3);
			CopyMemory(sortcard+3,bcTmp+3,2);
			removeAllChildByTag(m_TableCard_Tga[m_SendCardIndex]);
			for (int i = 0; i < 5; i++)
			{
				string _name = GetCardStringName(sortcard[i]);
				Sprite* temp = Sprite::createWithSpriteFrameName(_name.c_str());
				if(i >=3)
                    temp->setPosition(Vec2(m_CardPos[m_SendCardIndex].x+(i)*42+20,m_CardPos[m_SendCardIndex].y));
				else
                    temp->setPosition(Vec2(m_CardPos[m_SendCardIndex].x+(i)*42,m_CardPos[m_SendCardIndex].y));
				temp->setAnchorPoint(Vec2(0,0));
				temp->setTag(m_TableCard_Tga[m_SendCardIndex]);
				temp->setScale(0.7f);
				addChild(temp);
			}
		}

		//点数
		int iIndex = 0;
		if(bCardType!= CT_POINT)
		{
			iIndex = bCardType-2;
			if(iIndex>=13)
			{
				iIndex = 10;
			}
			else
			{
				if(iIndex>10) iIndex = 10;
				if(iIndex<0) iIndex = 0;
			}
		}
		char _name[32];
		if (iIndex >= 10) strcpy(_name,"niuniu.png");
		else if(iIndex > 0) sprintf(_name,"niu_%d.png", iIndex);
		else strcpy(_name,"wuniu.png");
		Sprite* temp = Sprite::createWithSpriteFrameName(_name);
		temp->setPosition(m_CardTypePos[m_SendCardIndex]);
		temp->setTag(m_CardType_Tga);
		addChild(temp);
	}
    
    //第二个发牌玩家
    int sendCardIndex = (m_SendCardIndex + 1) % 5;
	MoveTo *leftMoveBy = MoveTo::create(0.25f, Vec2(m_CardPos[sendCardIndex].x+m_SendCardCount*42,m_CardPos[sendCardIndex].y));
	ActionInstant *func = CallFuncN::create(CC_CALLBACK_1(BRNNGameViewLayer::CardMoveCallback2, this));
	temp = Sprite::createWithSpriteFrameName("BRNNbackCard.png");
	temp->setPosition(_STANDARD_SCREEN_CENTER_);
	temp->setAnchorPoint(Vec2(0,0));
	temp->setScale(0.7f);
	addChild(temp);
	temp->runAction(Sequence::create(leftMoveBy, func, NULL));
	SoundUtil::sharedEngine()->playEffect("BRNN/sound/DISPATCH_CARD"); 
}

void BRNNGameViewLayer::CardMoveCallback2(Node *pSender)  //两个参数
{
	removeChild(pSender);
    int sendCardIndex = (m_SendCardIndex + 1) % 5;
	string _name = GetCardStringName(m_cbTableCardArray[sendCardIndex][m_SendCardCount]);
	Sprite* temp = Sprite::createWithSpriteFrameName(_name.c_str());
	temp->setPosition(Vec2(m_CardPos[sendCardIndex].x+(m_SendCardCount)*42,m_CardPos[sendCardIndex].y));
	temp->setAnchorPoint(Vec2(0,0));
	temp->setTag(m_TableCard_Tga[sendCardIndex]);
    temp->setScale(0.7f);
	addChild(temp);
	if (m_SendCardCount == 4)
	{
		BYTE bcTmp[5];
		BYTE bCardType=m_GameLogic.GetCardType(m_cbTableCardArray[sendCardIndex],5,bcTmp);
		//排序
		BYTE sortcard[5];
		if(bCardType!=CT_POINT)
		{
			CopyMemory(sortcard,bcTmp,3);
			CopyMemory(sortcard+3,bcTmp+3,2);
			removeAllChildByTag(m_TableCard_Tga[sendCardIndex]);
			for (int i = 0; i < 5; i++)
			{
				string _name = GetCardStringName(sortcard[i]);
				Sprite* temp = Sprite::createWithSpriteFrameName(_name.c_str());
				if(i >=3) temp->setPosition(Vec2(m_CardPos[sendCardIndex].x+(i)*42+10,m_CardPos[sendCardIndex].y));
				else temp->setPosition(Vec2(m_CardPos[sendCardIndex].x+(i)*42-10,m_CardPos[sendCardIndex].y));
				temp->setAnchorPoint(Vec2(0,0));
				temp->setTag(m_TableCard_Tga[sendCardIndex]);
				temp->setScale(0.7f);
				addChild(temp);
			}
		}

		int iIndex = 0;
		if(bCardType!= CT_POINT)
		{
			iIndex = bCardType-2;
			if(iIndex>=13)
			{
				iIndex = 10;
			}
			else
			{
				if(iIndex>10) iIndex = 10;
				if(iIndex<0) iIndex = 0;
			}
		}
		char _name[32];
		if (iIndex >= 10) strcpy(_name,"niuniu.png");
		else if(iIndex > 0) sprintf(_name,"niu_%d.png", iIndex);
		else strcpy(_name,"wuniu.png");
		Sprite* temp = Sprite::createWithSpriteFrameName(_name);
		temp->setPosition(m_CardTypePos[sendCardIndex]);
		temp->setTag(m_CardType_Tga);
		addChild(temp);
	}
    
	//第三个发牌玩家
    sendCardIndex = (m_SendCardIndex + 2) % 5;
	MoveTo *leftMoveBy = MoveTo::create(0.25f, Vec2(m_CardPos[sendCardIndex].x+m_SendCardCount*42,m_CardPos[sendCardIndex].y));
	ActionInstant *func = CallFuncN::create(CC_CALLBACK_1(BRNNGameViewLayer::CardMoveCallback3, this));
	temp = Sprite::createWithSpriteFrameName("BRNNbackCard.png");
	temp->setPosition(_STANDARD_SCREEN_CENTER_);
	temp->setAnchorPoint(Vec2(0,0));
    temp->setScale(0.7f);
	addChild(temp);
	temp->runAction(Sequence::create(leftMoveBy,func,NULL));
	SoundUtil::sharedEngine()->playEffect("BRNN/sound/DISPATCH_CARD"); 
}

void BRNNGameViewLayer::CardMoveCallback3(Node *pSender)
{
	removeChild(pSender);
    int sendCardIndex = (m_SendCardIndex + 2) % 5;
	string _name = GetCardStringName(m_cbTableCardArray[sendCardIndex][m_SendCardCount]);
	Sprite* temp = Sprite::createWithSpriteFrameName(_name.c_str());
	temp->setPosition(Vec2(m_CardPos[sendCardIndex].x+(m_SendCardCount)*42,m_CardPos[sendCardIndex].y));
	temp->setAnchorPoint(Vec2(0,0));
	temp->setTag(m_TableCard_Tga[sendCardIndex]);
    temp->setScale(0.7f);
	addChild(temp);
	if (m_SendCardCount == 4)
	{
		BYTE bcTmp[5];
		BYTE bCardType=m_GameLogic.GetCardType(m_cbTableCardArray[sendCardIndex],5,bcTmp);
		//排序
		BYTE sortcard[5];
		if(bCardType!=CT_POINT)
		{
			CopyMemory(sortcard,bcTmp,3);
			CopyMemory(sortcard+3,bcTmp+3,2);
			removeAllChildByTag(m_TableCard_Tga[sendCardIndex]);
			for (int i = 0; i < 5; i++)
			{
				string _name = GetCardStringName(sortcard[i]);
				Sprite* temp = Sprite::createWithSpriteFrameName(_name.c_str());
				if(i >=3) temp->setPosition(Vec2(m_CardPos[sendCardIndex].x+(i)*42+10,m_CardPos[sendCardIndex].y));
				else temp->setPosition(Vec2(m_CardPos[sendCardIndex].x+(i)*40-12,m_CardPos[sendCardIndex].y));
				temp->setAnchorPoint(Vec2(0,0));
				temp->setTag(m_TableCard_Tga[sendCardIndex]);
                temp->setScale(0.7f);
				addChild(temp);
			}
		}
		int iIndex = 0;
		if(bCardType!= CT_POINT)
		{
			iIndex = bCardType-2;
			if(iIndex>=13)
			{
				iIndex = 10;
			}
			else
			{
				if(iIndex>10) iIndex = 10;
				if(iIndex<0) iIndex = 0;
			}
		}
		char _name[32];
		if (iIndex >= 10) strcpy(_name,"niuniu.png");
		else if(iIndex > 0) sprintf(_name,"niu_%d.png", iIndex);
		else strcpy(_name,"wuniu.png");
		Sprite* temp = Sprite::createWithSpriteFrameName(_name);
		temp->setPosition(m_CardTypePos[sendCardIndex]);
		temp->setTag(m_CardType_Tga);
		addChild(temp);
	}
	//第四个发牌玩家
    sendCardIndex = (m_SendCardIndex + 3) % 5;
	MoveTo *leftMoveBy = MoveTo::create(0.25f, Vec2(m_CardPos[sendCardIndex].x+m_SendCardCount*42,m_CardPos[sendCardIndex].y));
	ActionInstant *func = CallFuncN::create(CC_CALLBACK_1(BRNNGameViewLayer::CardMoveCallback4, this));
	temp = Sprite::createWithSpriteFrameName("BRNNbackCard.png");
	temp->setPosition(_STANDARD_SCREEN_CENTER_);
	temp->setAnchorPoint(Vec2(0,0));
    temp->setScale(0.7f);
	addChild(temp);
	temp->runAction(Sequence::create(leftMoveBy,func,NULL));
	SoundUtil::sharedEngine()->playEffect("BRNN/sound/DISPATCH_CARD"); 
}

void BRNNGameViewLayer::CardMoveCallback4(Node *pSender)
{
	removeChild(pSender);
    int sendCardIndex = (m_SendCardIndex + 3) % 5;
	string _name = GetCardStringName(m_cbTableCardArray[sendCardIndex][m_SendCardCount]);
	Sprite* temp = Sprite::createWithSpriteFrameName(_name.c_str());
	temp->setPosition(Vec2(m_CardPos[sendCardIndex].x+(m_SendCardCount)*42,m_CardPos[sendCardIndex].y));
	temp->setAnchorPoint(Vec2(0,0));
	temp->setTag(m_TableCard_Tga[sendCardIndex]);
	temp->setScale(0.7f);
	addChild(temp);
	if (m_SendCardCount == 4)
	{
		BYTE bcTmp[5];
		BYTE bCardType=m_GameLogic.GetCardType(m_cbTableCardArray[sendCardIndex],5,bcTmp);
		//排序
		BYTE sortcard[5];
		if(bCardType!=CT_POINT)
		{
			CopyMemory(sortcard,bcTmp,3);
			CopyMemory(sortcard+3,bcTmp+3,2);
			removeAllChildByTag(m_TableCard_Tga[sendCardIndex]);
			for (int i = 0; i < 5; i++)
			{
				string _name = GetCardStringName(sortcard[i]);
				Sprite* temp = Sprite::createWithSpriteFrameName(_name.c_str());
				if(i >=3) temp->setPosition(Vec2(m_CardPos[sendCardIndex].x+(i)*42+10,m_CardPos[sendCardIndex].y));
				else temp->setPosition(Vec2(m_CardPos[sendCardIndex].x+(i)*42-10,m_CardPos[sendCardIndex].y));
				temp->setAnchorPoint(Vec2(0,0));
				temp->setTag(m_TableCard_Tga[sendCardIndex]);
                temp->setScale(0.7f);
				addChild(temp);
			}
		}
		int iIndex = 0;
		if(bCardType!= CT_POINT)
		{
			iIndex = bCardType-2;
			if(iIndex>=13)
			{
				iIndex = 10;
			}
			else
			{
				if(iIndex>10) iIndex = 10;
				if(iIndex<0) iIndex = 0;
			}
		}
		char _name[32];
		if (iIndex >= 10) strcpy(_name,"niuniu.png");
		else if(iIndex > 0) sprintf(_name,"niu_%d.png", iIndex);
		else strcpy(_name,"wuniu.png");
		Sprite* temp = Sprite::createWithSpriteFrameName(_name);
		temp->setPosition(m_CardTypePos[sendCardIndex]);
		temp->setTag(m_CardType_Tga);
		addChild(temp);
	}
	//第五个发牌玩家
    sendCardIndex = (m_SendCardIndex + 4) % 5;
	MoveTo *leftMoveBy = MoveTo::create(0.25f, Vec2(m_CardPos[sendCardIndex].x+m_SendCardCount*42,m_CardPos[sendCardIndex].y));
	ActionInstant *func = CallFuncN::create(CC_CALLBACK_1(BRNNGameViewLayer::CardMoveCallback5, this));
	temp = Sprite::createWithSpriteFrameName("BRNNbackCard.png");
	temp->setPosition(_STANDARD_SCREEN_CENTER_);
	temp->setAnchorPoint(Vec2(0,0));
	temp->setScale(0.7f);
	addChild(temp);
	temp->runAction(Sequence::create(leftMoveBy,func,NULL));
	SoundUtil::sharedEngine()->playEffect("BRNN/sound/DISPATCH_CARD");  
}

void BRNNGameViewLayer::CardMoveCallback5(Node *pSender)
{
	removeChild(pSender);
    int sendCardIndex = (m_SendCardIndex + 4) % 5;
	string _name = GetCardStringName(m_cbTableCardArray[sendCardIndex][m_SendCardCount]);
	Sprite* temp = Sprite::createWithSpriteFrameName(_name.c_str());
	temp->setPosition(Vec2(m_CardPos[sendCardIndex].x+(m_SendCardCount)*42,m_CardPos[sendCardIndex].y));
	temp->setAnchorPoint(Vec2(0,0));
	temp->setTag(m_TableCard_Tga[sendCardIndex]);
	temp->setScale(0.7f);
	addChild(temp);
	if (m_SendCardCount == 4)
	{
		BYTE bcTmp[5];
		BYTE bCardType=m_GameLogic.GetCardType(m_cbTableCardArray[sendCardIndex],5,bcTmp);
		//排序
		BYTE sortcard[5];
		if(bCardType!=CT_POINT)
		{
			CopyMemory(sortcard,bcTmp,3);
			CopyMemory(sortcard+3,bcTmp+3,2);
			removeAllChildByTag(m_TableCard_Tga[sendCardIndex]);
			for (int i = 0; i < 5; i++)
			{
				string _name = GetCardStringName(sortcard[i]);
				Sprite* temp = Sprite::createWithSpriteFrameName(_name.c_str());
				if(i >=3) temp->setPosition(Vec2(m_CardPos[sendCardIndex].x+(i)*42+10,m_CardPos[sendCardIndex].y));
				else temp->setPosition(Vec2(m_CardPos[sendCardIndex].x+(i)*42-10,m_CardPos[sendCardIndex].y));
				temp->setAnchorPoint(Vec2(0,0));
				temp->setTag(m_TableCard_Tga[sendCardIndex]);
				temp->setScale(0.7f);
				addChild(temp);
			}
		}
		int iIndex = 0;
		if(bCardType!= CT_POINT)
		{
			iIndex = bCardType-2;
			if(iIndex>=13)
			{
				iIndex = 10;
			}
			else
			{
				if(iIndex>10) iIndex = 10;
				if(iIndex<0) iIndex = 0;
			}
		}
		char _name[32];
		if (iIndex >= 10) strcpy(_name,"niuniu.png");
		else if(iIndex > 0) sprintf(_name,"niu_%d.png", iIndex);
		else strcpy(_name,"wuniu.png");
		Sprite* temp = Sprite::createWithSpriteFrameName(_name);
		temp->setPosition(m_CardTypePos[sendCardIndex]);
		temp->setTag(m_CardType_Tga);
		addChild(temp);
	}
	m_SendCardOver = true;
	m_SendCardCount++;
	if (m_SendCardCount == 5)
	{
		unschedule(schedule_selector(BRNNGameViewLayer::UpdataSendCard));
		//推断赢家
		m_CurRecord = 0;
		DeduceWinner(m_CurRecord);
        
		if (m_lPlayAllScore > 0)
            SoundUtil::sharedEngine()->playEffect("BRNN/sound/END_WIN");
		else if(m_lPlayAllScore < 0)
            SoundUtil::sharedEngine()->playEffect("BRNN/sound/END_LOST");
		else
            SoundUtil::sharedEngine()->playEffect("BRNN/sound/END_DRAW");
 
        //高亮
        schedule(schedule_selector(BRNNGameViewLayer::SetWinArea), 1);
        
        SendData(SUB_C_RECORD, nullptr, 0);
    }
}

void BRNNGameViewLayer::SetWinArea(float fp)
{
    unschedule(schedule_selector(BRNNGameViewLayer::SetWinArea));
    
    if (m_CurRecord & 0x01)
    {
        addWinSpr(0);
    }
    if (m_CurRecord & 0x02)
    {
        addWinSpr(1);
    }
    if (m_CurRecord & 0x04)
    {
        addWinSpr(2);
    }
    if (m_CurRecord & 0x08)
    {
        addWinSpr(3);
    }
}

void BRNNGameViewLayer::addWinSpr(int index)
{
    removeAllChildByTag(m_LightArea_Tga[index]);
    ui::Scale9Sprite* temp = ui::Scale9Sprite::createWithSpriteFrameName("win.png");
    temp->setContentSize(Size(265, 310));
    temp->setPosition(Vec2(435 + index * 350, 590));
    temp->setTag(m_LightArea_Tga[index]);
    addChild(temp);
    ActionInterval *actionBlink = Blink::create(10, 25);
    temp->runAction(Sequence::create(actionBlink,NULL));
}

void BRNNGameViewLayer::DeduceWinner(BYTE &record)
{
	//大小比较
	if (m_GameLogic.CompareCard(m_cbTableCardArray[BANKER_INDEX],5,m_cbTableCardArray[SHUN_MEN_INDEX],5) == 1)
    {
        record |= 0x01;
    }
	if (m_GameLogic.CompareCard(m_cbTableCardArray[BANKER_INDEX],5,m_cbTableCardArray[DUI_MEN_INDEX],5) == 1)
    {
        record |= 0x02;
    }
	if (m_GameLogic.CompareCard(m_cbTableCardArray[BANKER_INDEX],5,m_cbTableCardArray[DAO_MEN_INDEX],5) == 1)
    {
        record |= 0x04;
    }
	if (m_GameLogic.CompareCard(m_cbTableCardArray[BANKER_INDEX],5,m_cbTableCardArray[HUAN_MEN_INDEX],5) == 1)
    {
        record |= 0x08;
    }
}

void BRNNGameViewLayer::removeFirstCard()
{
    auto child = getChildByTag(m_FirstCardTga);
    if (child)
        child->removeFromParent();
    
    SetCardInfo(m_cbTableCardArray);
}

//游戏结束
bool BRNNGameViewLayer::OnSubGameEnd(const void * pBuffer, WORD wDataSize)
{
	//效验数据
	ASSERT(wDataSize==sizeof(BRNN_CMD_S_GameEnd));
	if (wDataSize!=sizeof(BRNN_CMD_S_GameEnd)) return false;

	//消息处理
	BRNN_CMD_S_GameEnd * pGameEnd=(BRNN_CMD_S_GameEnd *)pBuffer;
    
    m_GameState = enGameEnd;
	//设置时间
	StartTime(pGameEnd->cbTimeLeave,2);

	//扑克信息
    int cardValue = m_GameLogic.GetCardValue(pGameEnd->bcFirstCard);
    m_SendCardIndex = (cardValue - 1) % 5;
    
    string _name = GetCardStringName(pGameEnd->bcFirstCard);
    Sprite* temp = Sprite::createWithSpriteFrameName(_name.c_str());
    temp->setPosition(Vec2(960, 600));
    addChild(temp, 100, m_FirstCardTga);
   
    CopyMemory(m_cbTableCardArray, pGameEnd->cbTableCardArray, sizeof(m_cbTableCardArray));
    
    ActionInstant *func = CallFunc::create(CC_CALLBACK_0(BRNNGameViewLayer::removeFirstCard,this));
    temp->runAction(Sequence::create(DelayTime::create(2.0f), func, NULL));
    
	//设置状态
	setGameStatus(BRNN_GAME_SCENE_GAME_END);

	m_cbLeftCardCount=pGameEnd->cbLeftCardCount;

	////庄家信息
	m_BankJuShu = pGameEnd->nBankerTime;
	m_BankZhangJI = pGameEnd->lBankerTotallScore;

	//成绩信息
	m_lPlayAllScore = pGameEnd->lUserScore;
	m_lBankerCurGameScore = pGameEnd->lBankerScore;
	m_lMeStatisticScore += m_lPlayAllScore;
	m_lPlayReturnScore = pGameEnd->lUserReturnScore;
	m_lBankerRevenue = pGameEnd->lRevenue;

	////更新控件
	UpdateButtonContron();

	return true;
}


void BRNNGameViewLayer::DrawEndScore()
{
	Sprite* temp = Sprite::createWithSpriteFrameName("tips_back.png");
	temp->setPosition(_STANDARD_SCREEN_CENTER_IN_PIXELS_);
	temp->setTag(m_GameEndBack_Tga);
	addChild(temp);	

    Sprite* title = Sprite::createWithSpriteFrameName("gamover.png");
    title->setPosition(Vec2(468,550));
    title->setScale(0.8f);
    temp->addChild(title);
    
    Sprite* lable = Sprite::createWithSpriteFrameName("lable.png");
    lable->setPosition(Vec2(468,420));
    temp->addChild(lable);
    
    Label* self  = Label::createWithSystemFont("结算", _GAME_FONT_NAME_1_, 46);
    self->setPosition(Vec2(200, 420));
    temp->addChild(self);

    Label* scoreL  = Label::createWithSystemFont("得分", _GAME_FONT_NAME_1_, 46);
    scoreL->setPosition(Vec2(468, 420));
    temp->addChild(scoreL);

    Label* scoreR  = Label::createWithSystemFont("返还所有下分", _GAME_FONT_NAME_1_, 46);
    scoreR->setPosition(Vec2(736, 420));
    temp->addChild(scoreR);
    
    Label* selfT = Label::createWithSystemFont("本家", _GAME_FONT_NAME_1_, 46);
    selfT->setPosition(Vec2(200, 280));
    selfT->setColor(_GAME_FONT_COLOR_5_);
    temp->addChild(selfT);

    Label* bankT = Label::createWithSystemFont("庄家", _GAME_FONT_NAME_1_, 46);
    bankT->setPosition(Vec2(200, 130));
    bankT->setColor(_GAME_FONT_COLOR_4_);
    temp->addChild(bankT);
    
    //自己
	char _player[32];
	sprintf(_player,"%lld",m_lPlayAllScore);
	Label* mNickfont = Label::createWithSystemFont(_player,_GAME_FONT_NAME_1_,46);
	mNickfont->setPosition(Vec2(468, 280));
	mNickfont->setColor(_GAME_FONT_COLOR_5_);
	temp->addChild(mNickfont);

	sprintf(_player,"%lld",m_lPlayReturnScore);
	mNickfont = Label::createWithSystemFont(_player,_GAME_FONT_NAME_1_,46);
	mNickfont->setPosition(Vec2(736,280));
	mNickfont->setColor(_GAME_FONT_COLOR_5_);
	temp->addChild(mNickfont);

	//庄家
	sprintf(_player,"%lld",m_lBankerCurGameScore);
	mNickfont = Label::createWithSystemFont(_player,_GAME_FONT_NAME_1_,46);
	mNickfont->setPosition(Vec2(468,130));
	mNickfont->setColor(_GAME_FONT_COLOR_4_);
	temp->addChild(mNickfont);

	sprintf(_player,"%lld",m_lBankerRevenue);
	mNickfont = Label::createWithSystemFont(_player,_GAME_FONT_NAME_1_,46);
	mNickfont->setPosition(Vec2(736,130));
	mNickfont->setColor(_GAME_FONT_COLOR_4_);
	temp->addChild(mNickfont);


}

//游戏记录
bool BRNNGameViewLayer::OnSubGameRecord(const void * pBuffer, WORD wDataSize)
{
	//效验参数
	ASSERT(wDataSize%sizeof(tagServerGameRecord)==0);
	if (wDataSize%sizeof(tagServerGameRecord)!=0) return false;
    
	//设置记录
    m_GameRecordArrary.clear();
    
	WORD wRecordCount = wDataSize/sizeof(tagServerGameRecord);
	for (WORD wIndex = 0; wIndex < wRecordCount; wIndex++)
	{
		tagServerGameRecord * pServerGameRecord = (((tagServerGameRecord *)pBuffer)+wIndex);
		SetGameHistory(pServerGameRecord->cbGameRecord, pServerGameRecord->cbGameTimes);
	}
	
	UpdataGameHistory();
	return true;
}


//历史记录
void BRNNGameViewLayer::UpdataGameHistory()
{
	int sx = 181, sy = 420;
	int tga = 0;
    
	//清理所有
	for (int i = 0; i < 16; i++)
	{
		Node * temp = m_LzBack->getChildByTag(m_LzCell_Tga+tga);
		if (temp != NULL)
            m_LzBack->removeChild(temp,false);
		temp = m_LzBack->getChildByTag(m_LzCell_Tga+tga+1);
		if (temp != NULL)
            m_LzBack->removeChild(temp,false);
		temp = m_LzBack->getChildByTag(m_LzCell_Tga+tga+2);
		if (temp != NULL)
            m_LzBack->removeChild(temp,false);
		temp = m_LzBack->getChildByTag(m_LzCell_Tga+tga+3);
		if (temp != NULL)
            m_LzBack->removeChild(temp,false);
		tga+=4;
	}

	int _count = 0;
	tga = 0;
    
	for (int i = 0; i < m_GameRecordArrary.size(); i++)
	{
		string name = "";
		if(m_GameRecordArrary[i].cbGameRecord & 0x01) name = "gou.png";
		else name = "cha.png";
		Sprite* temp = Sprite::createWithSpriteFrameName(name.c_str());
		temp->setPosition(Vec2(sx+_count*60,sy));
		temp->setTag(m_LzCell_Tga+tga);
		m_LzBack->addChild(temp);

		if(m_GameRecordArrary[i].cbGameRecord & 0x02) name = "gou.png";
		else name = "cha.png";
		temp = Sprite::createWithSpriteFrameName(name.c_str());
		temp->setPosition(Vec2(sx+_count*60,sy-100));
		temp->setTag(m_LzCell_Tga+tga+1);
		m_LzBack->addChild(temp);

		if(m_GameRecordArrary[i].cbGameRecord & 0x04) name = "gou.png";
		else name = "cha.png";
		temp = Sprite::createWithSpriteFrameName(name.c_str());
		temp->setPosition(Vec2(sx+_count*60,sy-(2*100)));
		temp->setTag(m_LzCell_Tga+tga+2);
		m_LzBack->addChild(temp);

		if(m_GameRecordArrary[i].cbGameRecord & 0x08) name = "gou.png";
		else name = "cha.png";
		temp = Sprite::createWithSpriteFrameName(name.c_str());
		temp->setPosition(Vec2(sx+_count*60,sy-(3*100)));
		temp->setTag(m_LzCell_Tga+tga+3);
		m_LzBack->addChild(temp);
		_count++;
		tga+=4;
	}
}

void BRNNGameViewLayer::SetGameHistory(BYTE cbGameRecord, LONGLONG cbGameTimes)
{
	tagClientGameRecord GameRecord;
	GameRecord.cbGameRecord = cbGameRecord;
	GameRecord.cbGameTimes = cbGameTimes;
	m_GameRecordArrary.push_back(GameRecord);
    if (m_GameRecordArrary.size() > 10)
    {
        auto font = m_GameRecordArrary.begin();
        m_GameRecordArrary.erase(font);
    }
}

bool BRNNGameViewLayer::OnSubBetFull(const void * pBuffer, WORD wDataSize)
{
    m_GameState = enGameBet;
	return true;
}

//游戏消息
bool BRNNGameViewLayer::OnGameMessage(WORD wSubCmdID, const void * pBuffer, WORD wDataSize)
{
	CCLOG("RECEIVE MESSAGE CMD[%d]" , wSubCmdID);
	switch (wSubCmdID)
	{
	case SUB_S_BRNN_GAME_FREE:		//游戏空闲
		{
			return OnSubGameFree(pBuffer,wDataSize);
		}
	case SUB_S_BRNN_GAME_START:		//游戏开始
		{
			return OnSubGameStart(pBuffer,wDataSize);
		}
	case SUB_S_BRNN_PLACE_JETTON:	//用户加注
		{
			return OnSubPlaceJetton(pBuffer,wDataSize);
		}
	case SUB_S_BRNN_JETTONFULL:
		{
			return OnSubBetFull(pBuffer,wDataSize);
		}
	case SUB_S_BRNN_APPLY_BANKER:	//申请做庄
		{
			return OnSubUserApplyBanker(pBuffer, wDataSize);
		}
	case SUB_S_BRNN_CANCEL_BANKER:	//取消做庄
		{
			return OnSubUserCancelBanker(pBuffer, wDataSize);
		}
	case SUB_S_BRNN_GRAB_BANKER://抢庄
		{
			return OnSubUserGrabBanker(pBuffer, wDataSize);
		}
	case SUB_S_BRNN_CHANGE_BANKER:	//切换庄家
		{
			return OnSubChangeBanker(pBuffer, wDataSize);
		}
	case SUB_S_BRNN_GAME_END:		//游戏结束
		{
			return OnSubGameEnd(pBuffer,wDataSize);
		}
	case SUB_S_BRNN_SEND_RECORD:		//游戏记录
		{
			return OnSubGameRecord(pBuffer,wDataSize);
		}
	}
	return true;
}

//场景消息
bool BRNNGameViewLayer::OnGameSceneMessage(BYTE cbGameStatus, const void * pBuffer, WORD wDataSize)
{
	switch (cbGameStatus)
	{
	case BRNN_GAME_SCENE_FREE:			//空闲状态
		{		
			//效验数据
			ASSERT(wDataSize==sizeof(BRNN_CMD_S_StatusFree));
			if (wDataSize!=sizeof(BRNN_CMD_S_StatusFree)) return false;
            
            m_GameState = enGameNormal;
			//消息处理
			BRNN_CMD_S_StatusFree * pStatusFree=(BRNN_CMD_S_StatusFree *)pBuffer;
            
            m_nRobBanker = 0;
			//设置时间
			StartTime(pStatusFree->cbTimeLeave,0);

			//玩家信息
			m_lMeMaxScore=pStatusFree->iUserMaxScore;

			//庄家信息
			SetBankerInfo(pStatusFree->wBankerUser,pStatusFree->iBankerScore);
			
			m_bEnableSysBanker=pStatusFree->bEnableSysBanker;
			
			m_BankZhangJI = pStatusFree->iBankerWinScore;
			m_BankJuShu = pStatusFree->cbBankerTime;
			//控制信息
			m_lApplyBankerCondition=pStatusFree->iApplyBankerCondition;
			CopyMemory(m_lAreaLimitScore, pStatusFree->iAreaLimitScore, sizeof(m_lAreaLimitScore));
			//更新控制
			UpdateButtonContron();
			return true;
		}
	case BRNN_GAME_SCENE_PLACE_JETTON:	//游戏状态
	case BRNN_GAME_SCENE_GAME_END:	//结束状态
		{
			//效验数据
			ASSERT(wDataSize>=sizeof(BRNN_CMD_S_StatusPlay));
			if (wDataSize!=sizeof(BRNN_CMD_S_StatusPlay)) return false;
			BRNN_CMD_S_StatusPlay * pStatusPlay=(BRNN_CMD_S_StatusPlay *)pBuffer;
            m_GameState = enGameOpenCard;
            m_nRobBanker = 0;
			//下注信息
			for (int nAreaIndex=0; nAreaIndex<AREA_COUNT; ++nAreaIndex)
			{
				PlaceUserJetton(nAreaIndex,pStatusPlay->iTotalAreaScore[nAreaIndex]);
			}

			//玩家积分
			m_lMeMaxScore=pStatusPlay->iUserMaxScore;			

			//控制信息
			m_lApplyBankerCondition=pStatusPlay->iApplyBankerCondition;
            CopyMemory(m_lAreaLimitScore, pStatusPlay->iAreaScoreLimit, sizeof(m_lAreaLimitScore));
			m_BankZhangJI = pStatusPlay->iBankerWinScore;
			m_BankJuShu = pStatusPlay->cbBankerTime;


			if (pStatusPlay->cbGameStatus == BRNN_GAME_SCENE_GAME_END)
			{
				StartTime(pStatusPlay->cbTimeLeave,2);
				//扑克信息
				SetCardInfo(pStatusPlay->cbTableCardArray, true);
			}
			else
			{
				StartTime(pStatusPlay->cbTimeLeave,1);
				SetCardInfo(NULL);
			}

			//庄家信息
			SetBankerInfo(pStatusPlay->wBankerUser,pStatusPlay->iBankerScore);
			m_bEnableSysBanker=pStatusPlay->bEnableSysBanker;

			//更新按钮
			UpdateButtonContron();

			//设置状态
			setGameStatus(pStatusPlay->cbGameStatus);

			//设置时间
			
			return true;
		}
	}
	return true;
}

string BRNNGameViewLayer::GetCardStringName(BYTE card)
{
	if (card == 0) return "BRNNbackCard.png";
	char tt[32];
	sprintf(tt,"%0x",card);
	BYTE _value = m_GameLogic.GetCardValue(card);
	BYTE _color = m_GameLogic.GetCardColor(card);
	string temp;
	if (_color == 0) temp = "fangkuai_";
	if (_color == 1) temp = "meihua_";
	if (_color == 2) temp = "hongtao_";
	if (_color == 3) temp = "heitao_";
	char _valstr[32];
	ZeroMemory(_valstr,32);
	if (card == 0x41) //小王
	{
		sprintf(_valstr,"xiaowang.png");
	}
	else if (card == 0x42)
	{
		sprintf(_valstr,"dawang.png");
	}
	else if (_value < 10)
	{
		sprintf(_valstr,"0%d.png",_value);
	}
	else 
	{
		sprintf(_valstr,"%d.png",_value);
	}

	temp+=_valstr;
	return temp;
}

string BRNNGameViewLayer::AddCommaToNum(LONG Num)
{
	char _str[256];
	sprintf(_str,"%ld", Num);
	string _string = _str;
	int step = _string.length()/3;
	for (int i = 1; i <= step; i++)
	{
		_string.insert(_string.length()-(i-1)-(i*3), ",");
	}
	return _string;
}

void BRNNGameViewLayer::callbackBt( Ref *pSender )
{
	Node *pNode = (Node *)pSender;
    if (m_OpenBankList)
    {
        CloseBankList();
        return;
    }
    else if (m_LzShow)
    {
        CloseLzList();
        return;
    }
    
	switch (pNode->getTag())
	{
	case Btn_GetScoreBtn:
		{
			GetScoreForBank();
			break;
		}
	case Btn_LZ:
		{
			if (m_LzShow == false)
			{
                OpenLzList();
			}
			else
			{
                CloseLzList();
			}
			break;
		}
	case Btn_CallBank: //申请上庄
		{
			SoundUtil::sharedEngine()->playEffect("buttonMusic");
			OnApplyBanker(1);
			break;
		}
	case Btn_QiangBank://抢庄
		{
			SoundUtil::sharedEngine()->playEffect("buttonMusic");
			OnCompeteBanker();
			break;
		}
	case Btn_CancleBank: //取消上庄
		{
			SoundUtil::sharedEngine()->playEffect("buttonMusic");
			OnApplyBanker(0);
			break;
		}
	case Btn_BankBtn:
		{
			if (m_OpenBankList == true)
                CloseBankList();
			else
                OpenBankList();
			break;;
		}
	case Btn_BackToLobby: // 返回大厅按钮
		{
			SoundUtil::sharedEngine()->playEffect("buttonMusic");
			BackToLobby();
			break;
		}
	case Btn_Seting: //设置
		{
			SoundUtil::sharedEngine()->playEffect("buttonMusic");
			GameLayerMove::sharedGameLayerMoveSink()->OpenSeting();
			break;
		}
	case Btn_100:
		{
			Select100();
			break;
		}
	case Btn_1000:
		{
			Select1000();
			break;
		}
	case Btn_1w:
		{
			Select1w();
			break;
		}
	case Btn_10w:
		{
			Select10w();
			break;
		}
	case Btn_100w:
		{
			Select100w();
			break;
		}
	case Btn_500w:
		{
			Select500w();
			break;
		}
	case Btn_1000w:
		{
			Select1000w();
			break;
		}
	}
}

void BRNNGameViewLayer::Select0()
{
	m_CurSelectGold = 0;
	removeAllChildByTag(m_BtnAnim_Tga);
}

void BRNNGameViewLayer::Select100()
{
    m_CurSelectGold = 100;
    SoundUtil::sharedEngine()->playEffect("buttonMusic");
    removeAllChildByTag(m_BtnAnim_Tga);
    Sprite *temp = Sprite::createWithSpriteFrameName("agoldselect.png");
    temp->setTag(m_BtnAnim_Tga);
    temp->setPosition(Vec2(700,80));
    addChild(temp);
}

void BRNNGameViewLayer::Select1000()
{
	m_CurSelectGold = 1000;
	SoundUtil::sharedEngine()->playEffect("buttonMusic");
	removeAllChildByTag(m_BtnAnim_Tga);
	Sprite *temp = Sprite::createWithSpriteFrameName("agoldselect.png");
	temp->setTag(m_BtnAnim_Tga);
	temp->setPosition(Vec2(870,80));
	addChild(temp);
}
void BRNNGameViewLayer::Select1w()
{
	m_CurSelectGold = 10000;
	SoundUtil::sharedEngine()->playEffect("buttonMusic");
	removeAllChildByTag(m_BtnAnim_Tga);
	Sprite *temp = Sprite::createWithSpriteFrameName("agoldselect.png");
	temp->setTag(m_BtnAnim_Tga);
	temp->setPosition(Vec2(1040,80));
	addChild(temp);
}
void BRNNGameViewLayer::Select10w()
{
	m_CurSelectGold = 100000;
	SoundUtil::sharedEngine()->playEffect("buttonMusic");
	removeAllChildByTag(m_BtnAnim_Tga);
	Sprite *temp = Sprite::createWithSpriteFrameName("agoldselect.png");
	temp->setTag(m_BtnAnim_Tga);
	temp->setPosition(Vec2(1210,80));
	addChild(temp);
}
void BRNNGameViewLayer::Select100w()
{
	m_CurSelectGold = 1000000;
	SoundUtil::sharedEngine()->playEffect("buttonMusic");
	removeAllChildByTag(m_BtnAnim_Tga);
	Sprite *temp = Sprite::createWithSpriteFrameName("agoldselect.png");
	temp->setTag(m_BtnAnim_Tga);
	temp->setPosition(Vec2(1380,80));
	addChild(temp);
}
void BRNNGameViewLayer::Select500w()
{
	m_CurSelectGold = 5000000;
	SoundUtil::sharedEngine()->playEffect("buttonMusic");
	removeAllChildByTag(m_BtnAnim_Tga);
	Sprite *temp = Sprite::createWithSpriteFrameName("agoldselect.png");
	temp->setTag(m_BtnAnim_Tga);
	temp->setPosition(Vec2(1550,80));
	addChild(temp);
}
void BRNNGameViewLayer::Select1000w()
{
	m_CurSelectGold = 10000000;
	SoundUtil::sharedEngine()->playEffect("buttonMusic");
	removeAllChildByTag(m_BtnAnim_Tga);
	Sprite *temp = Sprite::createWithSpriteFrameName("agoldselect.png");
	temp->setTag(m_BtnAnim_Tga);
	temp->setPosition(Vec2(1720,80));
	addChild(temp);
}

//申请消息
void BRNNGameViewLayer::OnCompeteBanker()
{
	//合法判断
	const tagUserData* pClientUserItem= GetMeUserData();

	if (pClientUserItem->lScore < m_lApplyBankerCondition+m_lCompetitionScore)
	{
		char TipMessage[128] = {0};
		sprintf(TipMessage, "\u60a8\u7684\u9152\u5427\u8c46\u4e0d\u8db3\uff0c\u65e0\u6cd5\u62a2\u5e84\uff0c\u62a2\u5e84\u6761\u4ef6\u4e3a\uff1a%lld\u9152\u5427\u8c46",m_lApplyBankerCondition+m_lCompetitionScore);
		AlertMessageLayer::createConfirm(TipMessage);
		return;
	}

	//旁观判断
	if (IsLookonMode()) return;

	//当前判断
	if (m_wCurrentBanker == GetMeChairID()) return;

    CMD_S_RobBanker bank;
    ZeroMemory(&bank, sizeof(bank));
    bank.wApplyUser = GetMeChairID();
    bank.nRobBanker = GetMeChairID();
	//发送消息
	SendData(SUB_C_COMPTETE_BANKER, &bank, sizeof(bank));
    
    m_BtnQiangBank->setVisible(false);
	
    //设置按钮
	UpdateButtonContron();
}

//申请消息
void BRNNGameViewLayer::OnApplyBanker(int wParam)
{
	//合法判断
	const tagUserData * pClientUserItem = GetMeUserData();
	//旁观判断
	if (IsLookonMode()) return;
    
    if (m_StartTime < 2)
        return;
    
	if (wParam == 1)
	{
		if (pClientUserItem->lScore < m_lApplyBankerCondition)
		{
			char TipMessage[128] = {0};
			sprintf(TipMessage, "\u60a8\u7684\u9152\u5427\u8c46\u4e0d\u8db3\uff0c\u65e0\u6cd5\u4e0a\u5e84\uff0c\u4e0a\u5e84\u6761\u4ef6\u4e3a\uff1a%lld\u9152\u5427\u8c46",m_lApplyBankerCondition);
			AlertMessageLayer::createConfirm(TipMessage);
			return;
		}
		//发送消息
		SendData(SUB_C_APPLY_BANKER, NULL, 0);
	}
	else
	{
		//发送消息
		SendData(SUB_C_CANCEL_BANKER, NULL, 0);
	}
    
	//设置按钮
	UpdateButtonContron();
}

void BRNNGameViewLayer::CloseLzList()
{
    if (m_IsLayerMove)
        return;
    m_IsLayerMove = true;
    m_LzShow = false;
    MoveTo *leftMoveBy = MoveTo::create(0.2f, Vec2(_STANDARD_SCREEN_CENTER_.x, 1500));
    ActionInstant *func = CallFunc::create(CC_CALLBACK_0(BRNNGameViewLayer::LayerMoveCallBack,this));
    m_LzBack->runAction(Sequence::create(leftMoveBy, func, NULL));
}

void BRNNGameViewLayer::OpenLzList()
{
    if (m_IsLayerMove)
        return;
    m_IsLayerMove = true;
    m_LzShow = true;
    MoveTo *leftMoveBy = MoveTo::create(0.2f, _STANDARD_SCREEN_CENTER_);
    ActionInstant *func = CallFunc::create(CC_CALLBACK_0(BRNNGameViewLayer::LayerMoveCallBack,this));
    m_LzBack->runAction(Sequence::create(leftMoveBy, func, NULL));
}

void BRNNGameViewLayer::CloseBankList()
{
    if (m_IsLayerMove)
        return;
    m_IsLayerMove = true;
    m_OpenBankList = false;
    MoveTo *leftMoveBy = MoveTo::create(0.2f, Vec2(960,1500));
    ActionInstant *func = CallFunc::create(CC_CALLBACK_0(BRNNGameViewLayer::LayerMoveCallBack,this));
    m_BankListBackSpr->runAction(Sequence::create(leftMoveBy, func, NULL));
}
void BRNNGameViewLayer::OpenBankList()
{
    if (m_IsLayerMove)
        return;
    m_IsLayerMove = true;
    m_OpenBankList = true;
    MoveTo *leftMoveBy = MoveTo::create(0.2f, _STANDARD_SCREEN_CENTER_);
    ActionInstant *func = CallFunc::create(CC_CALLBACK_0(BRNNGameViewLayer::LayerMoveCallBack,this));
    m_BankListBackSpr->runAction(Sequence::create(leftMoveBy, func, NULL));
}

void BRNNGameViewLayer::LayerMoveCallBack()
{
    m_IsLayerMove = false;
}

void BRNNGameViewLayer::backLoginView( Ref *pSender )
{
	IGameView::backLoginView(pSender);	
}

void BRNNGameViewLayer::OnQuit()
{
    this->removeFromParent();
}
