﻿#include "BRNNGameLogic.h"

//////////////////////////////////////////////////////////////////////////

//扑克数据
const BYTE BRNNGameLogic::m_cbCardListData[CARD_COUNT] =
{
    0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08,0x09,0x0A,0x0B,0x0C,0x0D,	//方块 A - K
    0x11,0x12,0x13,0x14,0x15,0x16,0x17,0x18,0x19,0x1A,0x1B,0x1C,0x1D,	//梅花 A - K
    0x21,0x22,0x23,0x24,0x25,0x26,0x27,0x28,0x29,0x2A,0x2B,0x2C,0x2D,	//红桃 A - K
    0x31,0x32,0x33,0x34,0x35,0x36,0x37,0x38,0x39,0x3A,0x3B,0x3C,0x3D,	//黑桃 A - K
    0x41,0x42															//小王、大王
};

//////////////////////////////////////////////////////////////////////////

//构造函数
BRNNGameLogic::BRNNGameLogic()
{
}

//析构函数
BRNNGameLogic::~BRNNGameLogic()
{
}

//混乱扑克
void BRNNGameLogic::RandCardList(BYTE cbCardBuffer[], BYTE cbBufferCount)
{
    ////混乱准备
    return;
}

//获取牌点
BYTE BRNNGameLogic::GetCardPip(BYTE cbCardData)
{
    //计算牌点
    BYTE cbCardValue	= GetCardValue(cbCardData);
    BYTE cbPipCount		= (cbCardValue >= 10) ? 0 : cbCardValue;
    
    return cbPipCount;
}
//逻辑数值
BYTE BRNNGameLogic::GetCardNewValue(BYTE cbCardData)
{
    //扑克属性
    BYTE cbCardColor=GetCardColor(cbCardData);
    BYTE cbCardValue=GetCardValue(cbCardData);
    
    //转换数值
    if (cbCardColor == 0x04) return cbCardValue + 13 + 2;
    return cbCardValue;
    
}
//逻辑大小
BYTE BRNNGameLogic::GetCardLogicValue(BYTE cbCardData)
{
    BYTE cbValue=GetCardValue(cbCardData);
    
    //获取花色
    BYTE cbColor=GetCardColor(cbCardData);
    
    if(cbValue > 10)
    {
        cbValue = 10;
    }
    if(cbColor == 0x4)
    {
        return 11;
    }
    return cbValue;
}
//获取牌点
BYTE BRNNGameLogic::GetCardListPip(const BYTE cbCardData[], BYTE cbCardCount)
{
    //变量定义
    BYTE cbPipCount=0;
    
    //获取牌点
    for (BYTE i = 0; i < cbCardCount; i++)
    {
        cbPipCount = (GetCardPip(cbCardData[i]) + cbPipCount) % 10;
    }
    
    return cbPipCount;
}

//排序
void BRNNGameLogic::SortCardList(BYTE cbCardBuffer[], BYTE cbBufferCount)
{
    if(cbBufferCount == 0)
        return;
    
    //转换数值
    BYTE cbSortValue[CARD_COUNT] = {0};
    
    for (BYTE i = 0;i < cbBufferCount; i++)
    {
        cbSortValue[i] = GetCardNewValue(cbCardBuffer[i]);
    }
    
    //排序操作
    bool bSorted = true;
    BYTE cbThreeCount, cbLast = cbBufferCount - 1;
    do
    {
        bSorted = true;
        for (BYTE i = 0;i < cbLast; i++)
        {
            if ((cbSortValue[i] < cbSortValue[i + 1]) ||
                ((cbSortValue[i] == cbSortValue[i + 1]) && (cbCardBuffer[i] < cbCardBuffer[i + 1])))
            {
                //交换位置
                cbThreeCount		= cbCardBuffer[i];
                cbCardBuffer[i]		= cbCardBuffer[i+1];
                cbCardBuffer[i + 1]	= cbThreeCount;
                cbThreeCount		= cbSortValue[i];
                cbSortValue[i]		= cbSortValue[i+1];
                cbSortValue[i + 1]	= cbThreeCount;
                bSorted				= false;
            }
        }
        cbLast--;
    } while(bSorted==false);
}

//是否变化牌型
bool BRNNGameLogic::IsVary(const BYTE cbCardData[], BYTE cbCardCount)
{
    for (int i = 0; i < cbCardCount; i++)
    {
        if(cbCardData[i] == 0x41 || cbCardData[i] == 0x42)
        {
            return true;
        }
    }
    return false;
}
//
int BRNNGameLogic::RetType(int itype)
{
    itype = itype % 10;
    switch(itype)
    {
        case 0:
            return CT_SPECIAL_NIUNIU;
        case 1:
            return CT_SPECIAL_NIU1;
            break;
        case 2:
            return CT_SPECIAL_NIU2;
            break;
        case 3:
            return CT_SPECIAL_NIU3;
            break;
        case 4:
            return CT_SPECIAL_NIU4;
            break;
        case 5:
            return CT_SPECIAL_NIU5;
            break;
        case 6:
            return CT_SPECIAL_NIU6;
            break;
        case 7:
            return CT_SPECIAL_NIU7;
            break;
        case 8:
            return CT_SPECIAL_NIU8;
            break;
        case 9:
            return CT_SPECIAL_NIU9;
            break;
        default :
            return CT_POINT;
            break;
    }
}
//获取牌的类型
BYTE BRNNGameLogic::GetCardType(const BYTE cbCardData[], BYTE cbCardCount, BYTE *bcOutCadData)
{
    //合法判断
    ASSERT(5==cbCardCount);
    if (5!=cbCardCount) return CT_ERROR;
    
    //排序扑克
    BYTE cbCardDataSort[CARD_COUNT];
    CopyMemory(cbCardDataSort,cbCardData,sizeof(BYTE)*cbCardCount);
    SortCardList(cbCardDataSort,cbCardCount);
    
    if(bcOutCadData != NULL)
    {
        CopyMemory(bcOutCadData,cbCardDataSort,cbCardCount);
    }
    
    int n = 0;
    
    BYTE bcMakeMax[5];
    memset(bcMakeMax,0,5);
    int iBigValue = 0;
    BYTE iSingleA[2];
   
    bcMakeMax[0]= cbCardDataSort[n];
    
    int iGetTenCount = 0;
    
    for (int iten = 0;iten<cbCardCount;iten++)
    {
        if(GetCardLogicValue(cbCardDataSort[iten])==10||GetCardLogicValue(cbCardDataSort[iten])==11)
        {
            iGetTenCount++;
            
        }
    }
    if( iGetTenCount>=3)
    {
        if ((GetCardLogicValue(cbCardDataSort[3])+GetCardLogicValue(cbCardDataSort[4]))%10==0)
        {
            if(bcOutCadData != NULL)
            {
                bcOutCadData[0] = cbCardDataSort[2];
                bcOutCadData[1] = cbCardDataSort[3];
                bcOutCadData[2] = cbCardDataSort[4];
                bcOutCadData[3] = cbCardDataSort[0];
                bcOutCadData[4] = cbCardDataSort[1];
            }
            return CT_SPECIAL_NIUNIU;
        }
        if((GetCardColor(cbCardDataSort[0])==0x04)&&(GetCardColor(cbCardDataSort[1])==0x04))
        {
            if(bcOutCadData != NULL)
            {
                bcOutCadData[0] = cbCardDataSort[0];
                bcOutCadData[1] = cbCardDataSort[3];
                bcOutCadData[2] = cbCardDataSort[4];
                bcOutCadData[3] = cbCardDataSort[1];
                bcOutCadData[4] = cbCardDataSort[2];
                
            }
            return CT_SPECIAL_NIUNIUDW;
            
        }
        if(GetCardColor(cbCardDataSort[0])==0x04)
        {
            //大小王与最小的组合成牛
            if(bcOutCadData != NULL)
            {
                bcOutCadData[0] = cbCardDataSort[0];
                bcOutCadData[1] = cbCardDataSort[3];
                bcOutCadData[2] = cbCardDataSort[4];
                bcOutCadData[3] = cbCardDataSort[1];
                bcOutCadData[4] = cbCardDataSort[2];
            }
            if(cbCardDataSort[0]==0x42)
                return CT_SPECIAL_NIUNIUDW;
            else
                return CT_SPECIAL_NIUNIUXW;
        }else
        {
            return RetType(GetCardLogicValue(cbCardDataSort[3])+GetCardLogicValue(cbCardDataSort[4]));
        }
        
    }
    if (iGetTenCount == 2)
    {
        if ((GetCardLogicValue(cbCardDataSort[2])+GetCardLogicValue(cbCardDataSort[3])+GetCardLogicValue(cbCardDataSort[4]))%10==0)
        {
            if(bcOutCadData != NULL)
            {
                bcOutCadData[0] = cbCardDataSort[2];
                bcOutCadData[1] = cbCardDataSort[3];
                bcOutCadData[2] = cbCardDataSort[4];
                bcOutCadData[3] = cbCardDataSort[0];
                bcOutCadData[4] = cbCardDataSort[1];
            }
            return CT_SPECIAL_NIUNIU;
        }
    }
    if((iGetTenCount==2)||( (iGetTenCount==1) && GetCardColor(cbCardDataSort[0])==0x04))
    {
        
        if( (GetCardColor(cbCardDataSort[0])==0x04) && (GetCardColor(cbCardDataSort[1])==0x04))
        {
            if(bcOutCadData != NULL)
            {
                bcOutCadData[0] = cbCardDataSort[0];
                bcOutCadData[1] = cbCardDataSort[3];
                bcOutCadData[2] = cbCardDataSort[4];
                bcOutCadData[3] = cbCardDataSort[1];
                bcOutCadData[4] = cbCardDataSort[2];
            }
            return CT_SPECIAL_NIUNIUDW;
        }else
        {
            //如果有一张王 其他任意三张组合为10则是牛牛
            if(GetCardColor(cbCardDataSort[0])==0x04)
            {
                
                for ( n=1;n<cbCardCount;n++)
                {
                    for (int j = 1;j<cbCardCount;j++)
                    {
                        if(j != n)
                        {
                            for (int w = 1;w<cbCardCount;w++)
                            {
                                if(w != n&&w!=j)
                                {
                                    //如果剩余的四张中任意三张能组合位10的整数倍
                                    if((GetCardLogicValue(cbCardDataSort[n])+GetCardLogicValue(cbCardDataSort[j])+GetCardLogicValue(cbCardDataSort[w]))%10==0)
                                    {
                                        
                                        int add = 0;
                                        for (int y = 1;y<cbCardCount;y++)
                                        {
                                            if(y != n&&y!=j&&y!=w)
                                            {
                                                iSingleA[add] =cbCardDataSort[y];
                                                add++;
                                                
                                            }
                                            
                                        }
                                        if(bcOutCadData != NULL)
                                        {
                                            bcOutCadData[0] = cbCardDataSort[n];
                                            bcOutCadData[1] = cbCardDataSort[j];
                                            bcOutCadData[2] = cbCardDataSort[w];
                                            bcOutCadData[3] = cbCardDataSort[0];
                                            bcOutCadData[4] = iSingleA[0];
                                        }
                                        if(cbCardDataSort[0]==0x42)
                                            return CT_SPECIAL_NIUNIUDW;
                                        else
                                            return CT_SPECIAL_NIUNIUXW;
                                        
                                        
                                    }
                                }
                            }
                        }
                    }
                }
                //如果有一张王 其他任意三张组合不为10则 取两张点数最大的组合
                BYTE bcTmp[4];
                int iBig = 0;
                int in = 0;
                for ( in = 1;in<cbCardCount;in++)
                {
                    for (int j = 1;j<cbCardCount;j++)
                    {
                        if(in != j)
                        {
                            BYTE bclogic = (GetCardLogicValue(cbCardDataSort[in])+GetCardLogicValue(cbCardDataSort[j]))%10;
                            if(bclogic>iBig)
                            {
                                iBig = bclogic;
                                int add = 0;
                                bcTmp[0]=cbCardDataSort[in];
                                bcTmp[1]=cbCardDataSort[j];
                                for (int y = 1;y<cbCardCount;y++)
                                {
                                    if(y != in&&y!=j)
                                    {
                                        iSingleA[add] =cbCardDataSort[y];
                                        add++;
                                    }
                                    
                                }
                                bcTmp[2]=iSingleA[0];
                                bcTmp[3]=iSingleA[1];
                                
                            }
                            
                        }
                    }
                }
                
                if(bcOutCadData != NULL)
                {
                    bcOutCadData[0] = cbCardDataSort[0];
                    bcOutCadData[1] = bcTmp[2];
                    bcOutCadData[2] = bcTmp[3];
                    bcOutCadData[3] = bcTmp[0];
                    bcOutCadData[4] = bcTmp[1];
                }
                if(iGetTenCount==1&&GetCardColor(cbCardDataSort[0])==0x04)
                {
                    //下面还能组合 有两张为 10 也可以组合成牛牛
                    
                } else {
                    //如果没有则比较 完与最小组合最大点数和组合
                    return RetType(GetCardLogicValue(bcTmp[0])+GetCardLogicValue(bcTmp[1]));
                    
                }
                
                
            } else {
                if((GetCardLogicValue(cbCardDataSort[2])+GetCardLogicValue(cbCardDataSort[3])+GetCardLogicValue(cbCardDataSort[4]))%10==0)
                {
                    if(bcOutCadData != NULL)
                    {
                        bcOutCadData[0] = cbCardDataSort[2];
                        bcOutCadData[1] = cbCardDataSort[3];
                        bcOutCadData[2] = cbCardDataSort[4];
                        bcOutCadData[3] = cbCardDataSort[0];
                        bcOutCadData[4] = cbCardDataSort[1];
                    }
                    return CT_SPECIAL_NIUNIU;
                } else {
                    for ( n= 2;n<cbCardCount;n++)
                    {
                        for (int j = 2;j<cbCardCount;j++)
                        {
                            if(j != n)
                            {
                                if((GetCardLogicValue(cbCardDataSort[n])+GetCardLogicValue(cbCardDataSort[j]))%10==0)
                                {
                                    int add = 0;
                                    for (int y = 2;y<cbCardCount;y++)
                                    {
                                        if(y != n&&y!=j)
                                        {
                                            iSingleA[add] =cbCardDataSort[y];
                                            add++;
                                            
                                        }
                                    }
                                    if(iBigValue<=iSingleA[0]%10)
                                    {
                                        iBigValue = GetCardLogicValue(iSingleA[0])%10;
                                        if(bcOutCadData != NULL)
                                        {
                                            bcOutCadData[0]= cbCardDataSort[0];
                                            bcOutCadData[1]= cbCardDataSort[n];
                                            bcOutCadData[2]= cbCardDataSort[j];
                                            bcOutCadData[3]= cbCardDataSort[1];
                                            bcOutCadData[4]= iSingleA[0];
                                            
                                        }
                                        
                                        if(iBigValue==0)
                                        {
                                            
                                            return CT_SPECIAL_NIUNIU;
                                        }
                                    }
                                    
                                }
                            }
                        }
                    }
                    if(iBigValue != 0)
                    {
                        return RetType(iBigValue);
                    }
                }
            }
            
        }
        
        iGetTenCount = 1;
        
    }
    //4个组合
    if(iGetTenCount==1)
    {
        if(GetCardColor(cbCardDataSort[0])==0x04)
        {
            for ( n= 1;n<cbCardCount;n++)
            {
                for (int j = 1;j<cbCardCount;j++)
                {
                    if(j != n)
                    {
                        //任意两张组合成牛
                        if((GetCardLogicValue(cbCardDataSort[n])+GetCardLogicValue(cbCardDataSort[j]))%10==0)
                        {
                            int add = 0;
                            for (int y = 1;y < cbCardCount;y++)
                            {
                                if(y != n&&y!=j)
                                {
                                    iSingleA[add] =cbCardDataSort[y];
                                    add++;
                                    
                                }
                                
                            }
                            
                            if(bcOutCadData != NULL)
                            {
                                bcOutCadData[0] = cbCardDataSort[0];
                                bcOutCadData[1] = iSingleA[0];
                                bcOutCadData[2] = iSingleA[1];
                                bcOutCadData[3] = cbCardDataSort[n];
                                bcOutCadData[4] = cbCardDataSort[j];
                            }
                            if ((GetCardLogicValue(iSingleA[0])+GetCardLogicValue(iSingleA[1]))%10==0)
                                return CT_SPECIAL_NIUNIU;
                            else if(cbCardDataSort[0]==0x42)
                                return CT_SPECIAL_NIUNIUDW;
                            else
                                return CT_SPECIAL_NIUNIUXW;
                            
                        }
                    }
                    
                }
            }
            
            //取4张中组合最大的点数
            
            BYTE bcTmp[4];
            int iBig = 0;
            int in = 0;
            for ( in = 1;in<cbCardCount;in++)
            {
                for (int j = 1;j<cbCardCount;j++)
                {
                    if(in != j)
                    {
                        BYTE bclogic = (GetCardLogicValue(cbCardDataSort[in])+GetCardLogicValue(cbCardDataSort[j]))%10;
                        if(bclogic>iBig)
                        {
                            iBig = bclogic;
                            int add = 0;
                            bcTmp[0]=cbCardDataSort[in];
                            bcTmp[1]=cbCardDataSort[j];
                            for (int y = 1;y<cbCardCount;y++)
                            {
                                if(y != in && y!=j)
                                {
                                    iSingleA[add] =cbCardDataSort[y]; 
                                    add++;
                                }
                                
                            }
                            bcTmp[2]=iSingleA[0];
                            bcTmp[3]=iSingleA[1];
                            
                        }
                        
                    }
                }   
            }
            
            if(bcOutCadData != NULL)
            {
                bcOutCadData[0] = cbCardDataSort[0];
                bcOutCadData[1] = bcTmp[2];
                bcOutCadData[2] = bcTmp[3];
                bcOutCadData[3] = bcTmp[0];
                bcOutCadData[4] = bcTmp[1];
            }
            return RetType(GetCardLogicValue(bcTmp[0])+GetCardLogicValue(bcTmp[1]));
            
        }
        //取4张中任两张组合为10 然后求另外两张的组合看是否是组合中最大
        for ( n= 1;n<cbCardCount;n++)
        {
            for (int j = 1;j<cbCardCount;j++)
            {
                if(j != n)
                {
                    if((GetCardLogicValue(cbCardDataSort[n])+GetCardLogicValue(cbCardDataSort[j]))%10==0)
                    {
                        int add = 0;
                        for (int y = 1;y<cbCardCount;y++)
                        {
                            if(y != n&&y!=j)
                            {
                                iSingleA[add] =cbCardDataSort[y]; 
                                add++;
                                
                            }
                            
                        }
                        if(iBigValue<=(GetCardLogicValue(iSingleA[0])+GetCardLogicValue(iSingleA[1]))%10)
                        {
                            iBigValue = GetCardLogicValue(iSingleA[0])+GetCardLogicValue(iSingleA[1])%10;
                            bcMakeMax[0]= cbCardDataSort[0];
                            bcMakeMax[1]= cbCardDataSort[j];
                            bcMakeMax[2]= cbCardDataSort[n]; 
                            bcMakeMax[3]= iSingleA[0]; 
                            bcMakeMax[4]= iSingleA[1]; 
                            
                            if(bcOutCadData != NULL)
                            {
                                CopyMemory(bcOutCadData,bcMakeMax,cbCardCount);
                            }
                            if(iBigValue==0)
                            {
                                
                                return CT_SPECIAL_NIUNIU;
                            }
                        }
                        
                    }
                }
            }
        }
        if(iBigValue!= 0)
        {
            return RetType(iBigValue);
        }else {
            //如果组合不成功
            iGetTenCount = 0;
        }
        
    }
    if(iGetTenCount==0)
    {
        //5个组合
        for ( n= 0;n<cbCardCount;n++)
        {
            for (int j = 0;j<cbCardCount;j++)
            {
                if(j != n)
                {
                    for (int w = 0;w<cbCardCount;w++)
                    {
                        if(w != n&&w!=j)
                        {
                            int valueAdd = GetCardLogicValue(cbCardDataSort[n]);
                            valueAdd += GetCardLogicValue(cbCardDataSort[j]);
                            valueAdd += GetCardLogicValue(cbCardDataSort[w]);
                            
                            if(valueAdd%10==0)
                            {
                                int add = 0;
                                for (int y = 0;y<cbCardCount;y++)
                                {
                                    if(y != n&&y!=j&&y!=w)
                                    {
                                        iSingleA[add] =cbCardDataSort[y]; 
                                        add++;
                                        
                                    }
                                    
                                }
                                if(iBigValue<=(GetCardLogicValue(iSingleA[0])+GetCardLogicValue(iSingleA[1]))%10)
                                {
                                    iBigValue = GetCardLogicValue(iSingleA[0])+GetCardLogicValue(iSingleA[1])%10;
                                    bcMakeMax[0]= cbCardDataSort[n];
                                    bcMakeMax[1]= cbCardDataSort[j];
                                    bcMakeMax[2]= cbCardDataSort[w]; 
                                    bcMakeMax[3]= iSingleA[0]; 
                                    bcMakeMax[4]= iSingleA[1]; 
                                    
                                    if(bcOutCadData != NULL)
                                    {
                                        CopyMemory(bcOutCadData,bcMakeMax,cbCardCount);
                                    }
                                    if(iBigValue==0)
                                    {
                                        
                                        return CT_SPECIAL_NIUNIU;
                                    }
                                }
                                
                            }
                            
                        }
                    }
                }
            }		
        }
        
        if(iBigValue!=0)
        {
            return RetType(iBigValue);
        }	 else {
            return CT_POINT;
        }
        
    }
    
    return CT_POINT;
}
//比较牌大小
int BRNNGameLogic::CompareCard(const BYTE cbFirstCardData[], BYTE cbFirstCardCount, const BYTE cbNextCardData[], BYTE cbNextCardCount)
{
    //牌错误
    if(cbFirstCardCount != MAX_COUNT || cbNextCardCount != MAX_COUNT)
    {
        return 0;
    }
    BYTE cbFirstCard[MAX_COUNT];
    BYTE cbNextCard[MAX_COUNT];
    CopyMemory(cbFirstCard, cbFirstCardData, cbFirstCardCount);
    CopyMemory(cbNextCard, cbNextCardData, cbNextCardCount);
    
    SortCardList(cbFirstCard, cbFirstCardCount);
    SortCardList(cbNextCard, cbNextCardCount);
    
    BYTE cbFirstType = GetCardType(cbFirstCard, cbFirstCardCount);
    BYTE cbNextType = GetCardType(cbNextCard, cbNextCardCount);
    
    //类型相同
    if(cbFirstType == cbNextType)
    {
        bool bFirstVary = false;
        bool bNextVary  = false;
        if ((cbFirstType == CT_SPECIAL_NIUNIUXW) || (cbFirstType == CT_SPECIAL_NIUNIUDW))
        {
            bFirstVary = IsVary(cbFirstCard,cbFirstCardCount);
        }
        if ((cbNextType == CT_SPECIAL_NIUNIUXW) || (cbNextType == CT_SPECIAL_NIUNIUDW))
        {
            bNextVary = IsVary(cbNextCard,cbNextCardCount);
        }
        //百变牌
        if(bFirstVary == true)
        {
            //不是百变
            if(bNextVary == false)
            {
                return 1;
            }
        }
        else
        {
            if(bNextVary == true)
            {
                return 0;
            }
        }
        //花色
        BYTE cbFirstColor	= GetCardColor(cbFirstCard[0]);
        BYTE cbNextColor	= GetCardColor(cbNextCard[0]);
        //面值
        BYTE cbFirstValue	= GetCardNewValue(cbFirstCard[0]);
        BYTE cbNextValue	= GetCardNewValue(cbNextCard[0]);
        //相同牌值
        if(cbFirstValue == cbNextValue)
        {
            //比花色
            return (cbFirstColor < cbNextColor);
        }
        //比大小
        return (cbFirstValue < cbNextValue);
    }
    //不相等的牌
    if(cbFirstType < cbNextType)
    {
         return 1;
    }else{

        return 0;
    }
}
//////////////////////////////////////////////////////////////////////////
