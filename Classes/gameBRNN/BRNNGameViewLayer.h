﻿#ifndef _BRNNGAME_VIEW_LAYER_H_
#define _BRNNGAME_VIEW_LAYER_H_

#include "GameScene.h"
#include "FrameGameView.h"
#include "BRNNGameLogic.h"
#include "CMD_BRNN.h"
#include "BRNNGameViewLayer.h"
#include "ui/UIListView.h"

class CTimeTaskLayer;
class BRNNGameViewLayer : public IGameView
{
	struct tagApplyUser
	{
		//玩家信息
		char							strUserName[128];						//玩家帐号
        BYTE                            wGender;
        DWORD                           wUserID;
		LONGLONG						lUserScore;							//玩家金币
		bool							bCompetition;
	};
	//记录信息
	struct tagClientGameRecord
	{
        BYTE		cbGameRecord;
        LONGLONG    cbGameTimes;
	};
public:
	static BRNNGameViewLayer *create(GameScene *pGameScene);
	virtual ~BRNNGameViewLayer();

	virtual bool init(); 
	virtual void onEnter();
	virtual void onExit();

    virtual bool onTouchBegan(Touch *pTouch, Event *pEvent);
    virtual void onTouchMoved(Touch *pTouch, Event *pEvent);
    virtual void onTouchEnded(Touch *pTouch, Event *pEvent);

	virtual bool onTextFieldAttachWithIME(TextFieldTTF*pSender){return false;}
	virtual bool onTextFieldDetachWithIME(TextFieldTTF*pSender){return false;}
	virtual bool onTextFieldInsertText(TextFieldTTF *pSender, const char *text, int nLen){return true;}
	virtual bool onTextFieldDeleteBackward(TextFieldTTF *pSender, const char *delText, int nLen){return true;}

	virtual void keyboardWillShow(IMEKeyboardNotificationInfo &info){}
	virtual void keyboardDidShow(IMEKeyboardNotificationInfo &info){}
	virtual void keyboardWillHide(IMEKeyboardNotificationInfo &info){}
	virtual void keyboardDidHide(IMEKeyboardNotificationInfo &info){}
	virtual void DrawUserScore(){}	//绘制玩家分数
	virtual void GameEnd(){};
	virtual void ShowAddScoreBtn(bool bShow=true){};
	virtual void UpdateDrawUserScore(){};		//更新显示的游戏币数量

	void callbackBt( Ref *pSender );

	virtual void backLoginView(Ref *pSender);

	string  AddCommaToNum(LONG Num);

	//初始化
	void InitGame();
	void AddPlayerInfo();
	void AddButton();
	Menu* CreateButton(std::string szBtName ,const Vec2 &p , int tag);

	// 按钮事件管理
	void DialogConfirm(Ref *pSender);
	void DialogCancel(Ref *pSender);
	string GetCardStringName(BYTE card);
	//网络接口
public:
	void OnEventUserLeave( tagUserData * pUserData, WORD wChairID, bool bLookonUser );
	//用户进入
	virtual void OnEventUserEnter(tagUserData * pUserData, WORD wChairID, bool bLookonUser);
	//用户积分
	virtual void OnEventUserScore(tagUserData * pUserData, WORD wChairID, bool bLookonUser);
	//用户状态
	virtual void OnEventUserStatus(tagUserData * pUserData, WORD wChairID, bool bLookonUser);
    //用户换桌
    virtual void OnEventUserChangeTable(tagUserData * pUserData, WORD wChairID, bool bLookonUser){};
	//游戏消息
	virtual bool OnGameMessage(WORD wSubCmdID, const void * pBuffer, WORD wDataSize);
	//场景消息
	virtual bool OnGameSceneMessage(BYTE cbGameStatus, const void * pBuffer, WORD wDataSize);

	virtual void OnReconnectAction(){};

	void OnQuit();

protected:	
	BRNNGameViewLayer(GameScene *pGameScene);
	static void reset();
	BRNNGameViewLayer(const BRNNGameViewLayer&);
	BRNNGameViewLayer& operator = (const BRNNGameViewLayer&);
	BRNNGameLogic		m_GameLogic;

public:
	//图片
	Sprite*			m_BackSpr;
	Sprite*         m_ClockSpr;
	Sprite*			m_BankListBackSpr;
	Sprite*			m_LzBack;

    ui::ListView*       m_listView;
	//按钮
	Menu*				m_BtnBackToLobby;
	Menu*				m_BtnSeting;
	Menu*				m_Gold100;
	Menu*				m_Gold1000;
	Menu*				m_Gold1w;
	Menu*				m_Gold10w;
	Menu*				m_Gold100w;
	Menu*				m_Gold500w;
	Menu*				m_Gold1000w;
	Menu*				m_BankBtn;
	Menu*				m_BankUp;
	Menu*				m_BankDown;
	Menu*				m_BtnCallBank;
	Menu*				m_BtnQiangBank;
	Menu*				m_BtnCancelBank;
	Menu*				m_BtnLZBtn;
	Menu*				m_BtnLZ_LeftBtn;
	Menu*				m_BtnLZ_RightBtn;
	Menu*				m_BtnGetScoreBtn;

	//索引
	int					m_Head_Tga;			//自己的信息
	int					m_NickName_Tga;
	int					m_Glod_Tga;
	int					m_WinGlod_Tga;
	int					m_BankNickName_Tga;	//庄家信息
	int					m_BankGold_Tga;
	int					m_BankZhangJi_Tga;
	int					m_BankJuShu_Tga;
	int					m_TalbeGold_Tga[4];
	int					m_BetScore_Tga[4];
	int					m_TimeType_Tga;
	int					m_TableCard_Tga[5];
	int					m_CardType_Tga;
	int					m_GameEndBack_Tga;
	int					m_BtnAnim_Tga;
	int					m_BetMe_Tga[4];
	int					m_LzCell_Tga;
    int                 m_FirstCardTga;
    int                 m_LightArea_Tga[4];
	//位置
	Vec2				m_HeadPos;          //自己的信息
	Vec2				m_NickNamePos;
	Vec2				m_GlodPos;
	Vec2				m_WinGlodPos;
	Vec2				m_BankNickNamePos;	//庄家信息
	Vec2				m_BankGoldPos;
	Vec2				m_BankZhangJiPos;
	Vec2				m_BankJuShuPos;
	Vec2				m_CardPos[5];
	Vec2				m_BetNumPos[4];
	Vec2				m_CardTypePos[5];
	Vec2				m_BankListPos[4];

	//变量
	int					m_StartTime;
	LONGLONG			m_lMeMaxScore;						//最大下注
	LONGLONG			m_lAreaLimitScore[AREA_COUNT];		//区域限制
	LONGLONG			m_lApplyBankerCondition;			//申请条件
	LONGLONG			m_lGrabScore;
	LONGLONG			m_lAllJettonScore[AREA_COUNT];
	LONGLONG			m_lUserJettonScore[AREA_COUNT];     //个人总注
	LONGLONG			m_lBankerScore;						//庄家积分
	WORD				m_wCurrentBanker;					//当前庄家
	BYTE				m_cbLeftCardCount;					//扑克数目
	bool				m_bEnableSysBanker;					//系统做庄
	LONGLONG			m_lCompetitionScore;
	LONGLONG			m_BankZhangJI;
	int					m_BankJuShu;
	bool				m_bNoBankUser;
	bool				m_LookMode;
	Rect				m_rcTianMen;						//闲家区域
	Rect				m_rcDimen;
	Rect				m_rcXuanMen;
	Rect				m_rcHuangMen;
	vector<tagApplyUser>m_BankList;
	BYTE				m_cbTableCardArray[5][5];
	int					m_SendCardCount;
	vector<tagClientGameRecord> m_GameRecordArrary;
	bool				m_SendCardOver;
	LONGLONG			m_lPlayAllScore;
	LONGLONG			m_lBankerCurGameScore;
	LONGLONG			m_lPlayReturnScore;
	LONGLONG            m_lBankerRevenue;
	LONGLONG			m_lMeStatisticScore;
	int					m_BankListBeginIndex;
	bool				m_OpenBankList;
	int					m_CurSelectGold;
	int					m_SIndex;
    int                 m_nRobBanker;
	bool				m_LzShow;
	bool				m_IsMeBank;
    bool                m_bCanRobBanker;
    bool                m_IsLayerMove;
    int                 m_SendCardIndex;
    BYTE                m_CurRecord;
public:
	//游戏空闲
	bool OnSubGameFree(const void * pBuffer, WORD wDataSize);
	bool OnSubGameStart( const void * pBuffer, WORD wDataSize);
	bool OnSubPlaceJetton(const void * pBuffer, WORD wDataSize);
	bool OnSubUserApplyBanker(const void * pBuffer, WORD wDataSize);
	bool OnSubUserCancelBanker(const void * pBuffer, WORD wDataSize);
	bool OnSubUserGrabBanker(const void * pBuffer, WORD wDataSize);
	bool OnSubChangeBanker(const void * pBuffer, WORD wDataSize);
	bool OnSubGameEnd(const void * pBuffer, WORD wDataSize);
	bool OnSubGameRecord(const void * pBuffer, WORD wDataSize);
	bool OnSubBetFull(const void * pBuffer, WORD wDataSize);

public:
	void Select0();
	void Select100();
	void Select1000();
	void Select1w();
	void Select10w();
	void Select100w();
	void Select500w();
	void Select1000w();
	void DeduceWinner(BYTE &record);
	void SetMePlaceJetton(int chair, LONGLONG score);
	LONGLONG GetUserMaxJetton();
	void UpdateButtonContron();
	void OnCompeteBanker();
	void OnApplyBanker(int wParam);
	void CloseBankList();
	void OpenBankList();
	void DrawBankList();
    void CloseLzList();
    void OpenLzList();
	void DrawEndScore();
    void LayerMoveCallBack();
    void removeFirstCard();
	void CardMoveCallback1(Node *pSender);
	void CardMoveCallback2(Node *pSender);
	void CardMoveCallback3(Node *pSender);
	void CardMoveCallback4(Node *pSender);
	void CardMoveCallback5(Node *pSender);
	void UpdataGameHistory();
	void SetGameHistory(BYTE cbGameRecord, LONGLONG cbGameTimes);
	void SendCard();
	void UpdataSendCard(float fp);
	void SendCardAll();
	void SetCardInfo(BYTE cbTableCardArray[5][5], bool bsence = false);
	void DeleteBankList(tagApplyUser ApplyUser);
	const char* GetGoldName(LONGLONG lScoreCount);
	void PlaceUserJetton(BYTE cbViewIndex, LONGLONG lScoreCount);
	bool IsLookonMode();
	void SetBankerInfo(WORD wBanker,LONGLONG lScore);
	void UpdateTime(float fp);
	void StopTime();
	void StartTime(int _time, int _type);
    void SetWinArea(float fp);
    void addWinSpr(int index);
};

#endif
