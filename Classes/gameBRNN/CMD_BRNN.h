#ifndef CMD_BRNNGAME_HEAD_FILE
#define CMD_BRNNGAME_HEAD_FILE
#pragma pack(1)


#define UR_GAME_CONTROL					0x20000000L				//游戏特殊控制

//////////////////////////////////////////////////////////////////////////////////
//服务定义

//游戏属性
#define BRNN_KIND_ID						165									//游戏 I D
#define GAME_NAME_BRNN                     TEXT("百人牛牛")					//游戏名字

//组件属性
#define GAME_PLAYER                         MAX_CHAIR									//游戏人数
#define VERSION_SERVER                      PROCESS_VERSION(6,0,3)				//程序版本
#define VERSION_CLIENT                      PROCESS_VERSION(6,0,3)				//程序版本

//////////////////////////////////////////////////////////////////////////////////
//状态定义

#define BRNN_GAME_SCENE_FREE				GS_FREE					//等待开始
#define BRNN_GAME_SCENE_PLACE_JETTON		GS_PLAYING				//下注状态
#define BRNN_GAME_SCENE_GAME_END			GS_PLAYING+1			//结束状态
#define BRNN_GAME_SCENE_MOVECARD_END		GS_PLAYING+2			//结束状态


//区域索引
#define ID_TIAN_MEN					0									//天
#define ID_DI_MEN					1									//地
#define ID_XUAN_MEN					2									//玄
#define ID_HUANG_MEN				3									//黄

//玩家索引
#define BANKER_INDEX				0									//庄家索引
#define SHUN_MEN_INDEX				1									//顺门索引
#define DUI_MEN_INDEX				2									//对门索引
#define DAO_MEN_INDEX				3									//倒门索引
#define HUAN_MEN_INDEX				4									//倒门索引
#define MAX_INDEX					3									//最大索引

#define AREA_COUNT					4									//区域数目

//赔率定义
#define RATE_TWO_PAIR				12									//对子赔率
#define SERVER_LEN					32									//房间长度

////////////////////////////////////////////////////////////////////////////////////
#ifndef _UNICODE
#define mystrcpy	strcpy
#define mystrlen	strlen
#define myscanf		_snscanf
#define	myLPSTR		LPCSTR
#else
#define mystrcpy	wcscpy
#define mystrlen	wcslen
#define myscanf		_snwscanf
#define	myLPSTR		LPWSTR
#endif

//机器人信息
struct tagRobotInfo
{
	int nChip[8];														//筹码定义
	int nAreaChance[AREA_COUNT];										//区域几率
	CHAR szCfgFileName[260];										//配置文件
	int	nMaxTime;														//最大赔率

	tagRobotInfo()
	{
		int nTmpChip[7] = {100, 1000, 10000, 100000, 1000000, 5000000,10000000};
		int nTmpAreaChance[AREA_COUNT] = {1, 1, 1, 1};
		char szTmpCfgFileName[260] = "OxBattleConfig.ini";

		nMaxTime = 10;
		memcpy(nChip, nTmpChip, sizeof(nChip));
		memcpy(nAreaChance, nTmpAreaChance, sizeof(nAreaChance));
		memcpy(szCfgFileName, szTmpCfgFileName, sizeof(szCfgFileName));
	}
};

//记录信息
struct tagServerGameRecord
{
    BYTE		cbGameRecord;										//天门,地门,玄门,黄门,胜利
    LONGLONG 	cbGameTimes;										//第几局
};

//////////////////////////////////////////////////////////////////////////
//服务器命令结构

#define SUB_S_BRNN_GAME_FREE			99									//游戏空闲
#define SUB_S_BRNN_GAME_START			100									//游戏开始
#define SUB_S_BRNN_PLACE_JETTON			101									//用户下注
#define SUB_S_BRNN_GAME_END				102									//游戏结束
#define SUB_S_BRNN_APPLY_BANKER			103									//申请庄家
#define SUB_S_BRNN_CHANGE_BANKER		104									//切换庄家
#define SUB_S_BRNN_CHANGE_USER_SCORE	105									//更新积分
#define SUB_S_BRNN_SEND_RECORD			106									//游戏记录
#define SUB_S_BRNN_PLACE_JETTON_FAIL	107									//下注失败
#define SUB_S_BRNN_CANCEL_BANKER		108									//取消申请
#define SUB_S_BRNN_JETTONFULL			109                                 //下注已满


#define SUB_S_BRNN_AMDIN_COMMAND		110									//管理员命令
#define SUB_S_BRNN_UPDATE_STORAGE       111									//更新库存
#define SUB_S_BRNN_GRAB_BANKER          112

//更新库存
struct CMD_S_UpdateStorage
{
	LONGLONG						lStorage;							//新库存值
	LONGLONG						lStorageDeduct;						//库存衰减
};

//请求回复
struct BRNN_CMD_S_CommandResult
{
	BYTE cbAckType;					//回复类型
#define ACK_SET_WIN_AREA  1
#define ACK_PRINT_SYN     2
#define ACK_RESET_CONTROL 3
	BYTE cbResult;
#define CR_ACCEPT  2			//接受
#define CR_REFUSAL 3			//拒绝
	BYTE cbExtendData[20];			//附加数据
};

//失败结构
struct BRNN_CMD_S_PlaceJettonFail
{
	WORD							wPlaceUser;							//下注玩家
	BYTE							lJettonArea;						//下注区域
	LONGLONG						lPlaceScore;						//当前下注
};

//更新积分
struct BRNN_CMD_S_ChangeUserScore
{
	WORD							wChairID;							//椅子号码
	LONGLONG							lScore;								//玩家积分

	//庄家信息
	WORD							wCurrentBankerChairID;				//当前庄家
	BYTE							cbBankerTime;						//庄家局数
	LONGLONG							lCurrentBankerScore;				//庄家分数
};

//申请庄家
struct BRNN_CMD_S_ApplyBanker
{
	WORD							wApplyUser;							//申请玩家
    int								nRobBanker;							//抢庄的人
    int                             mInsert;                            //插入索引
};

//取消申请
struct BRNN_CMD_S_CancelBanker
{
    CHAR							szCancelUser[32];					//取消玩家
    bool							bisRobBanker;
    int								nRobBanker;							//抢庄的人
};

//玩家抢庄
struct CMD_S_RobBanker
{
    WORD							wApplyUser;							//玩家抢庄
    int								nRobBanker;							//抢庄的人
};

//切换庄家
struct BRNN_CMD_S_ChangeBanker
{
	WORD							wBankerUser;						//当庄玩家
	LONGLONG						lBankerScore;						//庄家游戏币
    int								nRobBanker;							//抢庄的人
};

//游戏状态
struct BRNN_CMD_S_StatusFree
{
    //全局信息
    BYTE							cbTimeLeave;						//剩余时间
    
    //玩家信息
    LONGLONG						iUserMaxScore;						//玩家金币
    
    //庄家信息
    WORD							wBankerUser;						//当前庄家
    WORD							cbBankerTime;						//庄家局数
    LONGLONG						iBankerWinScore;					//庄家成绩
    LONGLONG						iBankerScore;						//庄家分数
    bool							bEnableSysBanker;					//系统做庄
    
    //控制信息
    LONGLONG						iApplyBankerCondition;				//申请条件
    LONGLONG						iAreaLimitScore[AREA_COUNT];        //区域限制
};

//游戏状态
struct BRNN_CMD_S_StatusPlay
{
    //全局下注
    LONGLONG						iTotalAreaScore[AREA_COUNT];
    
    //玩家下注
    LONGLONG						iUserAreaScore[AREA_COUNT];
    
    //各区域限注
    LONGLONG						iAreaScoreLimit[AREA_COUNT];
    
    //玩家积分
    LONGLONG						iUserMaxScore;					//最大下注
    
    //控制信息
    LONGLONG						iApplyBankerCondition;			//申请条件
    
    //扑克信息
    BYTE							cbTableCardArray[AREA_COUNT+1][5];	//桌面扑克
    BYTE							cbLeftCardCount;				//扑克数目
    
    //庄家信息
    WORD							wBankerUser;					//当前庄家
    WORD							cbBankerTime;					//庄家局数
    LONGLONG						iBankerWinScore;				//庄家赢分
    LONGLONG						iBankerScore;					//庄家分数
    bool							bEnableSysBanker;				//系统做庄
    
    //结束信息
    LONGLONG						iEndBankerScore;				//庄家成绩
    LONGLONG						iEndUserScore;					//玩家成绩
    LONGLONG						iEndUserReturnScore;			//返回积分
    LONGLONG						iEndRevenue;					//游戏税收
    
    //全局信息
    BYTE							cbTimeLeave;					//剩余时间
    BYTE							cbGameStatus;					//游戏状态
};

//游戏空闲
struct BRNN_CMD_S_GameFree
{
	BYTE							cbTimeLeave;						//剩余时间
	LONGLONG						lStorageStart;						//
};

//游戏开始
struct BRNN_CMD_S_GameStart
{
	WORD							wBankerUser;						//庄家位置
	LONGLONG						lBankerScore;						//庄家游戏币
	LONGLONG						lUserMaxScore;						//我的游戏币
	BYTE							cbTimeLeave;						//剩余时间	
	bool							bContiueCard;						//继续发牌
    LONGLONG						lAreaLimitScore[AREA_COUNT];		//各区域可下分
};

//用户下注
struct BRNN_CMD_S_PlaceJetton
{
	WORD							wChairID;							//用户位置
	BYTE							cbJettonArea;						//筹码区域
	LONGLONG						lJettonScore;						//加注数目
};

//游戏结束
struct BRNN_CMD_S_GameEnd
{
	//下局信息
	BYTE							cbTimeLeave;						//剩余时间

	//扑克信息
	BYTE							cbTableCardArray[5][5];				//桌面扑克
	BYTE							cbLeftCardCount;					//扑克数目

	BYTE							bcFirstCard;

	//庄家信息
	LONGLONG						lBankerScore;						//庄家成绩
	LONGLONG						lBankerTotallScore;					//庄家成绩
	INT								nBankerTime;						//做庄次数

	//玩家成绩
	LONGLONG						lUserScore;							//玩家成绩
	LONGLONG						lUserReturnScore;					//返回积分

	//全局信息
	LONGLONG						lRevenue;							//游戏税收
};

//////////////////////////////////////////////////////////////////////////
//客户端命令结构

#define SUB_C_PLACE_JETTON			1									//用户下注
#define SUB_C_APPLY_BANKER			2									//申请庄家
#define SUB_C_CANCEL_BANKER			3									//取消申请
#define SUB_C_CONTINUE_CARD			4									//继续发牌
#define SUB_C_COMPTETE_BANKER		5									//抢庄家

#define SUB_C_AMDIN_COMMAND			5									//管理员命令
#define SUB_C_UPDATE_STORAGE        6									//更新库存
#define SUB_C_GRAB_BANKER			7 
#define SUB_C_RECORD                10                                 //游戏记录



//客户端消息
#define IDM_ADMIN_COMMDN			WM_USER+1000
#define IDM_UPDATE_STORAGE			WM_USER+1001

//控制区域信息
struct tagControlInfo
{
	BYTE cbControlArea[MAX_INDEX];			//控制区域
};

struct tagAdminReq
{
	BYTE							m_cbExcuteTimes;					//执行次数
	BYTE							m_cbControlStyle;					//控制方式
#define		CS_BANKER_LOSE    1
#define		CS_BANKER_WIN	  2
#define		CS_BET_AREA		  3
	bool							m_bWinArea[3];						//赢家区域
};


struct BRNN_CMD_C_AdminReq
{
	BYTE cbReqType;
#define RQ_SET_WIN_AREA		1
#define RQ_RESET_CONTROL	2
#define RQ_PRINT_SYN		3
	BYTE cbExtendData[20];			//附加数据
};
//用户下注
struct BRNN_CMD_C_PlaceJetton
{
	BYTE							cbJettonArea;						//筹码区域
	LONGLONG						lJettonScore;						//加注数目
};

#define RQ_REFRESH_STORAGE		1
#define RQ_SET_STORAGE			2

//更新库存
struct BRNN_CMD_C_UpdateStorage
{
	BYTE                            cbReqType;						//请求类型
	LONGLONG						lStorage;						//新库存值
	LONGLONG						lStorageDeduct;					//库存衰减
};

//////////////////////////////////////////////////////////////////////////
#pragma pack()
#endif
