#include "GameFishSendEvent.h"
#include "LobbySocketSink.h"
#include "ClientSocketSink.h"
#include "LocalDataUtil.h"
#include "GameLayerMove.h"

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
#include "GoodsByWeChatPay.h"
#endif

#include "Encrypt.h"

#define	Btn_Close			1
#define Btn_Bind			2


GameFishSendEvent::GameFishSendEvent(GameScene *pGameScene):GameLayer(pGameScene)
{
    
}

GameFishSendEvent::~GameFishSendEvent()
{

}


Menu* GameFishSendEvent::CreateButton(std::string szBtName ,const Vec2 &p , int tag )
{
	Menu *pBT = Tools::Button(StrToChar(szBtName+".png") , StrToChar(szBtName+".png") , p, this , menu_selector(GameFishSendEvent::callbackBt) , tag);
	return pBT;
}

bool GameFishSendEvent::init()
{
	if ( !Layer::init() )
	{
		return false;
	}
	GameLayer::onEnter();
	setLocalZOrder(100);

    ui::Scale9Sprite* colorBg = ui::Scale9Sprite::create("Common/bg_gray.png");
    colorBg->setContentSize(Size(1920, 1080));
    colorBg->setPosition(_STANDARD_SCREEN_CENTER_);
    addChild(colorBg);
    
    Sprite* bg = Sprite::createWithSpriteFrameName("bg_fishevent.png");
    bg->setPosition(_STANDARD_SCREEN_CENTER_);
    addChild(bg);

    Menu * btnCancel = CreateButton("btn_fishevent_close", Vec2(1430, 830), Btn_Close);
    addChild(btnCancel);

    Menu * btnConfirm = CreateButton("btn_fishBind", Vec2(950, 300), Btn_Bind);
    addChild(btnConfirm);

	actionShow();
	setTouchEnabled(true);
    auto listener = EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(true);
    listener->onTouchBegan = CC_CALLBACK_2(GameFishSendEvent::onTouchBegan, this);
    listener->onTouchMoved = CC_CALLBACK_2(GameFishSendEvent::onTouchMoved, this);
    listener->onTouchEnded = CC_CALLBACK_2(GameFishSendEvent::onTouchEnded, this);
    Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(listener, this);
	return true;
}

void GameFishSendEvent::callbackBt( Ref *pSender )
{
	Node *pNode = (Node *)pSender;
	switch (pNode->getTag())
	{
	case Btn_Close:
		{
			SoundUtil::sharedEngine()->playEffect("buttonMusic");
			actionMin();
			break;
		}
    case Btn_Bind:
        {
            char url[255] = {0};
            char _str[128] = {0};
            char _md5[33] = {0};
            
            LONGLONG sum = g_GlobalUnits.GetGolbalUserData().dwUserID +
            g_GlobalUnits.GetGolbalUserData().dwGameID +
            g_GlobalUnits.GetGolbalUserData().lWebToken;
            
            sprintf(_str,"hao22---!%lld---22yy", sum);
            
            CMD5Encrypt::EncryptData(_str, _md5);
            
            sprintf(url, "https://weixin.game678.net.cn/WeixinBind.aspx?u=%u&g=%u&t=%lld&m=%s",
                    g_GlobalUnits.GetGolbalUserData().dwUserID,
                    g_GlobalUnits.GetGolbalUserData().dwGameID,
                    g_GlobalUnits.GetGolbalUserData().lWebToken,_md5);
            
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
            GoodsByWeChatPay_cpp::shared()->WxBind(url);
#else
            JniSink::share()->OpenWebView(url);
#endif

            SoundUtil::sharedEngine()->playEffect("buttonMusic");
            actionMin();
            break;
        }
	}
}

void GameFishSendEvent::OnRemove()
{
	GameLayerMove::sharedGameLayerMoveSink()->ClearSeting();
	m_pGameScene->removeChild(this);
}

GameFishSendEvent* GameFishSendEvent::create(GameScene *pGameScene)
{
	GameFishSendEvent* temp = new GameFishSendEvent(pGameScene);
	if(temp && temp->init())
	{
		temp->autorelease();
		return temp;
	}
	else
	{
		CC_SAFE_DELETE(temp);
		return NULL;
	}
}

void GameFishSendEvent::onEnter()
{
	GameLayer::onEnter();
}

void GameFishSendEvent::onExit()
{
	GameLayer::onExit();
}

void GameFishSendEvent::actionShow()
{
	setScale(0.1f);
	this->runAction(Sequence::create(ScaleTo::create(0.2f , 1.f),NULL));
}


void GameFishSendEvent::actionMin()
{
   this->runAction(Sequence::create(ScaleTo::create(0.2f , 0.1f),CallFunc::create(CC_CALLBACK_0(GameFishSendEvent::OnRemove, this)),NULL));
}
