#pragma once
#include <string>

#define SHARE_CACHED_IMG					"share_screen.png"

class CScreen
{
public:
	~CScreen(void);
	static CScreen *share();

	//拍摄图片
	void Shoot(const char *fileName);

	std::string GetShootPath(const char *fileName);

private:
	CScreen(void);
	static CScreen *m_pScreen;
};

