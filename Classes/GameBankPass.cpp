#include "GameBankPass.h"
#include "LobbySocketSink.h"
#include "Encrypt.h"
#include "SoundUtil.h"
GameBankPass::GameBankPass( GameScene *pGameScene ):GameLayer(pGameScene)
{

}
GameBankPass::~GameBankPass()
{

}

GameBankPass* GameBankPass::create(GameScene *pGameScene)
{
	GameBankPass* temp = new GameBankPass(pGameScene);
	if(temp && temp->init())
	{
		temp->autorelease();
		return temp;
	}
	else
	{
		CC_SAFE_DELETE(temp);
		return NULL;
	}
}

bool GameBankPass::init()
{
	if ( !Layer::init() )	return false;
	setLocalZOrder(10);

    ui::Scale9Sprite* colorBg = ui::Scale9Sprite::create("Common/bg_gray.png");
    colorBg->setContentSize(Size(1920, 1080));
    colorBg->setPosition(_STANDARD_SCREEN_CENTER_);
    addChild(colorBg);

    ui::Scale9Sprite *pBG = ui::Scale9Sprite::createWithSpriteFrameName("bg_tongyongkuang2.png");
    pBG->setContentSize(Size(1050,600));
    pBG->setPosition(_STANDARD_SCREEN_CENTER_);
    addChild(pBG);
    
    Label *label = Label::createWithSystemFont("初次登录: 保险柜密码默认与登录密码相同", _GAME_FONT_NAME_1_, 40);
    label->setPosition(150, 300);
    label->setAnchorPoint(Vec2::ZERO);
    label->setColor(_GAME_FONT_COLOR_6_);
    pBG->addChild(label);
    
    //YUNG
    Label *label2 = Label::createWithSystemFont("游客、微信登录: 保险柜密码为 123456", _GAME_FONT_NAME_1_, 40);
    label2->setPosition(150, 240);
    label2->setColor(_GAME_FONT_COLOR_6_);
    label2->setAnchorPoint(Vec2::ZERO);
    pBG->addChild(label2);
    
    ui::Scale9Sprite* passBg = ui::Scale9Sprite::createWithSpriteFrameName("bg_shuru.png");
    passBg->setContentSize(Size(890, 100));
    passBg->setPosition(Vec2(525, 450));
    pBG->addChild(passBg);
    
	//密码输入框
	std::string strTrade = "\u8bf7\u8f93\u5165\u4fdd\u9669\u67dc\u5bc6\u7801";
	Tools::StringTransform(strTrade);

    const Size inputSize = Size(810,100);
    m_Pass = cocos2d::ui::EditBox::create(inputSize, INPUT_BACKGROUND);
    m_Pass->setFontName(_GAME_FONT_NAME_1_);
    m_Pass->setFontSize(46);
    m_Pass->setFontColor(_GAME_FONT_COLOR_2_);
    m_Pass->setPlaceholderFontName(_GAME_FONT_NAME_1_);
    m_Pass->setPlaceHolder(strTrade.c_str());
    m_Pass->setPlaceholderFontSize(46);
    m_Pass->setPlaceholderFontColor(_GAME_FONT_COLOR_2_);
    m_Pass->setPosition(Vec2(525,442));
    m_Pass->setMaxLength(16);
    m_Pass->setInputMode(cocos2d::ui::EditBox::InputMode::ANY);
    m_Pass->setInputFlag(cocos2d::ui::EditBox::InputFlag::PASSWORD);
    pBG->addChild(m_Pass);

    Menu* tempbtn = CreateButton("btn_queding" ,Vec2(525,130),_BtnOk);
    pBG->addChild(tempbtn);
    
    tempbtn = CreateButton("btn_close" ,Vec2(1050,600),_BtnCancel);
    pBG->addChild(tempbtn);
  
    auto listener = EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(true);
    listener->onTouchBegan = CC_CALLBACK_2(GameBankPass::onTouchBegan, this);
    listener->onTouchMoved = CC_CALLBACK_2(GameBankPass::onTouchMoved, this);
    listener->onTouchEnded = CC_CALLBACK_2(GameBankPass::onTouchEnded, this);
    _eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);
    
	return true;
}

void GameBankPass::close()
{
	m_pGameScene->removeChild(this);
}

void GameBankPass::onRemove()
{
	m_pGameScene->removeChild(this);
}

void GameBankPass::onEnter()
{
	GameLayer::onEnter();
}

void GameBankPass::onExit()
{
	GameLayer::onExit();
}

void GameBankPass::callbackBt(Ref *pSender )
{
	Node *pNode = (Node *)pSender;
	switch (pNode->getTag())
	{
	case _BtnOk:
		{
			SoundUtil::sharedEngine()->playEffect((char*)"buttonMusic");
			CMD_GP_QueryGameInsureInfo temp;
			temp.dwUserID = g_GlobalUnits.GetGolbalUserData().dwUserID;
            
            char buff[50];
            memset(buff, 0, sizeof(buff));
            sprintf(buff, PWDHEAD, m_Pass->getText());
            
			CMD5Encrypt::EncryptData(buff, temp.szPassword);
			sprintf(g_GlobalUnits.m_BankPassWord,"%s",temp.szPassword);
            temp.wKindID = Dating_GameKind_WZMJ;
            temp.lToken = g_GlobalUnits.GetGolbalUserData().lWebToken;
			if(LobbySocketSink::sharedSocketSink()->ConnectServer())
			{
				LobbySocketSink::sharedLoginSocket()->SendData(MDM_GF_BANK,SUB_MB_BANK_QUERY,&temp, sizeof(CMD_GP_QueryGameInsureInfo));
			}
            LoadLayer::create(m_pGameScene);
			onRemove();
			break;
		}
	case _BtnCancel:
		{
			SoundUtil::sharedEngine()->playEffect((char*)"buttonMusic");
			onRemove();
			break;
		}
	}
}

Menu* GameBankPass::CreateButton( std::string szBtName ,const Vec2 &p , int tag )
{
	Menu *pBT = Tools::Button(StrToChar(szBtName+"_normal.png") , StrToChar(szBtName+"_click.png") , p, this , menu_selector(GameBankPass::callbackBt) , tag);
	return pBT;
}
