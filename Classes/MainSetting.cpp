#include "MainSetting.h"
#include "LobbySocketSink.h"
#include "ClientSocketSink.h"
#include "LocalDataUtil.h"
#include "LobbyLayer.h"

#define	Btn_Close			1
#define Back_Music			2
#define Back_Effect			3
#define Change_Account		4


MainSetting::MainSetting(GameScene *pGameScene):GameLayer(pGameScene)
{
}

MainSetting::~MainSetting()
{

}


Menu* MainSetting::CreateButton(std::string szBtName ,const Vec2 &p , int tag)
{
	Menu *pBT = Tools::Button(StrToChar(szBtName+"_normal.png") , StrToChar(szBtName+"_click.png") , p, this , menu_selector(MainSetting::callbackBt) , tag);
	return pBT;
}

bool MainSetting::init()
{
	if ( !Layer::init())
	{
		return false;
	}
	GameLayer::onEnter();
	setLocalZOrder(100);
	Tools::addSpriteFrame("SetLayer.plist");

    ui::Scale9Sprite* colorBg = ui::Scale9Sprite::create("Common/bg_gray.png");
    colorBg->setContentSize(Size(1920, 1080));
    colorBg->setPosition(_STANDARD_SCREEN_CENTER_);
    addChild(colorBg);

	Sprite *LockBack = Sprite::createWithSpriteFrameName("bg_tongyongkuang1.png");
	LockBack->setPosition(_STANDARD_SCREEN_CENTER_);
	addChild(LockBack);
    
    Sprite *title = Sprite::createWithSpriteFrameName("set_title.png");
    title->setPosition(Vec2(535,630));
    LockBack->addChild(title);

    Menu* btn = CreateButton("btn_close", Vec2(1030,640),Btn_Close);
	LockBack->addChild(btn);
    
    Label *music = Label::createWithSystemFont("音乐", _GAME_FONT_NAME_1_, 46);
    music->setPosition(Vec2(130, 480));
    LockBack->addChild(music);

    Label *sound = Label::createWithSystemFont("音效", _GAME_FONT_NAME_1_, 46);
    sound->setPosition(Vec2(130, 320));
    LockBack->addChild(sound);
    
	// Add the slider
	ControlSlider *slider = ControlSlider::create(
      Sprite::createWithSpriteFrameName("mainSettingSlideBg.png") ,
      Sprite::createWithSpriteFrameName("mainSettingSlide.png") ,
      Sprite::createWithSpriteFrameName("mainSetting_tag.png")
    );
	slider->setAnchorPoint(Vec2(0.5f, 0.5f));
	slider->setMinimumValue(0.0f);
	slider->setMaximumValue(1.0f);
	slider->setPosition(Vec2(600,480));
	slider->setTag(Back_Music);
	slider->setValue(g_GlobalUnits.m_fBackMusicValue);
    slider->addTargetWithActionForControlEvents(this, cccontrol_selector(MainSetting::valueChanged), Control::EventType::VALUE_CHANGED);
	LockBack->addChild(slider);

	// Add the slider
	slider = ControlSlider::create(
		Sprite::createWithSpriteFrameName("mainSettingSlideBg.png") ,
		Sprite::createWithSpriteFrameName("mainSettingSlide.png") ,
		Sprite::createWithSpriteFrameName("mainSetting_tag.png")
		);
	slider->setAnchorPoint(Vec2(0.5f, 0.5f));
	slider->setMinimumValue(0.0f);
	slider->setMaximumValue(1.0f);
	slider->setPosition(Vec2(600,320));
	slider->setTag(Back_Effect);
	slider->setValue(g_GlobalUnits.m_fSoundValue);
    slider->addTargetWithActionForControlEvents(this, cccontrol_selector(MainSetting::valueChanged), Control::EventType::VALUE_CHANGED);
	LockBack->addChild(slider);
	
    Menu * changeAccount = CreateButton("btn_changAccount", Vec2(960, 300), Change_Account);
    addChild(changeAccount);
	
	setTouchEnabled(true);
    auto listener = EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(true);
    listener->onTouchBegan = CC_CALLBACK_2(MainSetting::onTouchBegan, this);
    listener->onTouchMoved = CC_CALLBACK_2(MainSetting::onTouchMoved, this);
    listener->onTouchEnded = CC_CALLBACK_2(MainSetting::onTouchEnded, this);
    Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(listener, this);
	return true;
}


void MainSetting::valueChanged(Ref *sender, Control::EventType event)
{
	Node *pNode = (Node *)sender;
	switch (pNode->getTag())
	{
	case Back_Music:
		{
			ControlSlider *slider = (ControlSlider *)pNode;
			float v = slider->getValue();
			g_GlobalUnits.m_fBackMusicValue = v;
			LocalDataUtil::SaveBackMusicValue(v);
			SoundUtil::sharedEngine()->setBackSoundVolume(v);
			break;
		}
	case Back_Effect:
		{
			ControlSlider *slider = (ControlSlider *)pNode;
			float v = slider->getValue();
			g_GlobalUnits.m_fSoundValue = v;
			LocalDataUtil::SaveSoundValue(v);
			SoundUtil::sharedEngine()->setSoundVolume(v);
			break;
		}
	}
}
void MainSetting::callbackBt( Ref *pSender )
{
	Node *pNode = (Node *)pSender;
	switch (pNode->getTag())
	{
	case Btn_Close:
		{
			SoundUtil::sharedEngine()->playEffect("buttonMusic");
			OnRemove();
			break;
		}
    case Change_Account:
        {
            Director::getInstance()->getEventDispatcher()->dispatchCustomEvent("CHANGE_ACCOUNT");
            SoundUtil::sharedEngine()->playEffect("buttonMusic");
            break;
        }
	}
}

void MainSetting::OnRemove()
{
	m_pGameScene->removeChild(this);
}

MainSetting* MainSetting::create(GameScene *pGameScene)
{
	MainSetting* temp = new MainSetting(pGameScene);
	if(temp && temp->init())
	{
		temp->autorelease();
		return temp;
	}
	else
	{
		CC_SAFE_DELETE(temp);
		return NULL;
	}
}

void MainSetting::onEnter()
{
	GameLayer::onEnter();
}

void MainSetting::onExit()
{
	GameLayer::onExit();
	Tools::removeSpriteFrameCache("SetLayer.plist");
}
