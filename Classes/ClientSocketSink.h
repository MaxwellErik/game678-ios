﻿#ifndef _CLIENT_SOCKETSINK_H_
#define _CLIENT_SOCKETSINK_H_

#include "cocos2d.h"
#include "UserManager.h"

#include "TcpClientSocket.h"
#include "FrameGameView.h"
#include "TcpClientThread.h"
#include "LobbyLayer.h"

class CTableFrameSink;

class ClientSocketSink : public TcpCallback , public Node
{
public:
	
	~ClientSocketSink(void);

	static ClientSocketSink* sharedSocketSink();

	static CTcpClientThread *sharedClientSocket(void);
	//static TcpClientSocket *sharedClientSocket(void);

	//读取事件
	virtual bool OnEventTCPSocketRead(CMD_Command Command, VOID * pData, WORD wDataSize);
	//连接事件
    virtual bool OnEventTCPSocketLink();
    bool OnEventTCPConnectError();
    void OnError(float ft);
	//关闭事件
	virtual bool OnEventTCPSocketShut();

	virtual void update(float delta);
    
public:
	bool SendData(WORD wMainCmdID , WORD wSubCmdID );

	bool SendData(WORD wMainCmdID , WORD wSubCmdID, void * pData, WORD wDataSize );

	void SetLobbyLayer(LobbyLayer* lobbylayer){m_lobbylayer = lobbylayer;}

public:
	void StartReceive();
	void StopReceive();
    tagUserData * GetMeUserData();
   
	void setFrameGameView(FrameGameView * pFrameGameView);
	void closeSocket();

	void ConnectToServer();
    bool ConnectServer(char *server = nullptr, int port = 0);
    
	void LoginServer(WORD wKindID);

	void AccountCheckFinish(WORD wKindID);

	void LoginServerSuccess();

	void SitdownReq(WORD wTableID=INVALID_TABLE , WORD wChairID=INVALID_CHAIR);
	void SitdownReq(const char* password, WORD wTableID=INVALID_TABLE , WORD wChairID=INVALID_CHAIR);

	void GetScene();

	void LeftGameReq();

	bool CheckClientStart();

	bool CheckClientLoad();

	void CleaAllUser();

	CC_SYNTHESIZE(bool, m_bIsLogin, IsLogin)

private:
	tagUserData							*m_pMeUserData;
	FrameGameView						*m_pFrameGameView;
	char								m_szServerAddr[SERVER_LEN];
	WORD								m_wPort;

public:
	bool								m_bClientReady;

private:

	static ClientSocketSink* m_pClientSocketSink;
	static CTcpClientThread *m_pConnectRoomSocket;

	//网络主命令
protected:
	//登录消息
	bool OnSocketMainLogon(CMD_Command Command, void * pBuffer, WORD wDataSize);
	//用户消息
	bool OnSocketMainUser(CMD_Command Command, void * pBuffer, WORD wDataSize);
	//配置消息
	bool OnSocketMainInfo(CMD_Command Command, void * pBuffer, WORD wDataSize);
	//状态消息
	bool OnSocketMainStatus(CMD_Command Command, void * pBuffer, WORD wDataSize);
	//系统消息
	bool OnSocketMainSystem(CMD_Command Command, void * pBuffer, WORD wDataSize);
	//房间消息
	bool OnSocketMainServerInfo(CMD_Command Command, void * pBuffer, WORD wDataSize);
	//游戏消息
	bool OnSocketMainGameFrame(CMD_Command Command, void * pBuffer, WORD wDataSize);
	//手机消息
	bool OnSocketMainMobile(CMD_Command Command, void * pBuffer, WORD wDataSize);
    //彩金消息
    bool OnSocketCaijin(CMD_Command Command, void * pBuffer, WORD wDataSize);
    
	//网络子命令
	//用户进入
	bool OnSocketSubUserCome(CMD_Command Command, void * pBuffer, WORD wDataSize);
	//用户状态
	bool OnSocketSubStatus(CMD_Command Command, void * pBuffer, WORD wDataSize);
	//用户分数
	bool OnSocketSubScore(CMD_Command Command, void * pBuffer, WORD wDataSize);
	//坐下失败
	bool OnSocketSubSitFailed(CMD_Command Command, void * pBuffer, WORD wDataSize);
	//因分数不足而坐下失败
	bool OnSocketSubScoreSitFailed(CMD_Command Command, void * pBuffer, WORD wDataSize); 

protected:
	ClientSocketSink(void);
	void StartGameClient(bool _move = true);

public:
	CUserManager					m_UserManager;						//用户管理类

	WORD							m_wGameGenre;						//游戏类型
	DWORD							m_dwVideoAddr;						//视频地址
	BYTE							m_cbHideUserInfo;					//隐藏信息
	WORD							m_wChairCount;						//椅子数量
	WORD							m_wTableCount;						//桌子数量

	LobbyLayer*						m_lobbylayer;
};

#endif
