﻿#ifndef _TOOLS_H_
#define _TOOLS_H_

#include "GameScene.h"

class Tools 
{
public:

    // 常用普通按钮
    static Menu* Button(const char *NormalBitmpaName, const char *SelectBitmpaName, Vec2 Position, Node *Node, SEL_MenuHandler sel, int iTag = -1);

    static MenuItemSprite *Button(const char *NormalBitmpaName, const char *SelectBitmpaName, const char *DisableBitmpaName, Node *Node, SEL_MenuHandler sel, int iTag = -1);
    
    static Menu *Toggle(char *NormalBitmpaName, char *SelectBitmpaName, Vec2 Position, Node *Node, SEL_MenuHandler sel,  bool isSelected, int iTag = -1);
    
    static MenuItemToggle *ItemToggle(char *NormalBitmpaName, char *SelectBitmpaName, char *pressBitmapName1, char *pressBitmapName2, Node *Node, SEL_MenuHandler sel);
    
    static Menu *ToggleMenu(MenuItemToggle *ItemToggle, Vec2 Position, bool isSelected, int iTag = -1);
    
    static Menu *ToggleMenu(char *NormalBitmpaName1, char *NormalBitmpaName2, char *SelectBitmpaName1, char *SelectBitmpaName2, Vec2 Position, Node *Node, SEL_MenuHandler sel, int iTag = -1);
    
    // 输入框
    static TextFieldTTF *AddTextField(Vec2 Position, const char *pText, const cocos2d::Size& dimensions, TextHAlignment alignment, const char *fontName, float fontSize);

	// 得到光标的位置
    static void GetCursorPosition(Vec2 &CursorPosition, TextFieldTTF *pTextField, Sprite *pSpriteCursor);

	static void UTF_8_Delete_Back(std::string *pUTF8String);
	static int UTF_8_CharLength(std::string *pUTF8String);

	static void UTF_8_SetPassword(std::string *pUTF8String, std::string *pPassword);

    // 设置label显示区域
    static Label*SetLabelShowRect(const char *pLabel, const cocos2d::Size &dimensions, TextHAlignment alignment, const char *fontName, float fontSize, cocos2d::Rect rect, Vec2 position);
    
    // 得到节点的Rect大小
    static void GetRect(Node* pNode, cocos2d::Rect & SrcRect);

	static void LoadDataByKey(const char * const Key, std::string &Value);
	static void SaveDataByKey(const char * const key, std::string &Value);

    static bool SaveFile(const char *path, void *buffer, int len);
    static bool LoadFile(const char *path, void *buffer, int len);

#if (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
	//字符转换，使cocos2d-x在win32平台支持中文显示
	static int GBKToUTF8(std::string &gbkStr,const char* toCode,const char* formCode);
#endif
	static time_t loadLoginTime();				//取得上次登录时间
	static void saveLoginTime(time_t t);		//保存登录时间

	static int LoadLoginTaskDay();				//取得上一次连续登录的天数
	static void SaveLoginTaskDay(int iDay);		//保存连续登录的天数

	static time_t LoadNextTaskTime();			//取得下次领取的时间
	static void SaveNextTaskTime(time_t t);		//保存下次领取的时间

	static int StringTransform(std::string &gbkStr);

	static void AddComma(LONGLONG lNum , char *szOut);

	static void GetSystemTime(char *szTime);

	static void removeSpriteFrameCache(const char *szName);

    static void addSpriteFrame(const char *szName , Texture2D::PixelFormat format = Texture2D::PixelFormat::RGBA8888);	//加载plist CCTexture2DPixelFormat format

	static void NumberToChar(char *szOut , LONGLONG lNumber);

	static void GetValueStr(char* value,  char * name, char *filename);

	static int GetValueInt(char * name, char *filename);

	static std::string urlencode(const char * str);//特殊字符转换(传给web服务器时用)

	static void ltrim(char *s);

	static void rtrim(char *s);

};

#endif
