﻿#ifndef GLOBAL_FRAME_HEAD_FILE
#define GLOBAL_FRAME_HEAD_FILE

#pragma once

#include "GlobalDef.h"
#include "GlobalProperty.h"

//////////////////////////////////////////////////////////////////////////
//宏定义
#pragma pack(1)

//游戏状态
#define GS_FREE                         0                               //空闲状态
#define GS_PLAYING                      100                             //买顶底标志

//////////////////////////////////////////////////////////////////////////
//IPC 网络事件

#define IPC_MAIN_SOCKET					1								//网络消息

#define IPC_SUB_SOCKET_SEND				1								//网络发送
#define IPC_SUB_SOCKET_RECV				2								//网络接收

//IPC 网络包结构
struct IPC_SocketPackage
{
	CMD_Command							Command;						//命令信息
	BYTE								cbBuffer[SOCKET_PACKET];		//数据缓冲
};

//////////////////////////////////////////////////////////////////////////
//IPC 配置信息

#define IPC_MAIN_CONFIG					2								//配置信息

#define IPC_SUB_SERVER_INFO				1								//房间信息
#define IPC_SUB_COLUMN_INFO				2								//列表信息

//游戏信息
struct IPC_GF_ServerInfo
{
	IosDword								dwUserID;						//用户 I D
	WORD								wTableID;						//桌子号码
	WORD								wChairID;						//椅子号码
	WORD								wKindID;						//类型标识
	WORD								wServerID;						//房间标识
	WORD								wGameGenre;						//游戏类型
	WORD								wChairCount;					//椅子数目
	BYTE								cbHideUserInfo;					//隐藏信息
	IosDword								dwVideoAddr;					//视频地址
	TCHAR								szKindName[KIND_LEN];			//类型名字
	TCHAR								szServerName[SERVER_LEN];		//房间名称
};

//////////////////////////////////////////////////////////////////////////
//IPC 用户信息

#define IPC_MAIN_USER					3								//用户信息

#define IPC_SUB_USER_COME				1								//用户信息
#define IPC_SUB_USER_STATUS				2								//用户状态
#define IPC_SUB_USER_SCORE				3								//用户积分
#define IPC_SUB_GAME_START				4								//游戏开始
#define IPC_SUB_GAME_FINISH				5								//游戏结束
#define IPC_SUB_UPDATE_FACE				6								//更新头像
#define IPC_SUB_MEMBERORDER				7								//更新头像

//用户状态
struct IPC_UserStatus
{
	IosDword								dwUserID;						//用户 I D
	WORD								wNetDelay;						//网络延时
	BYTE								cbUserStatus;					//用户状态
};

//用户分数
struct IPC_UserScore
{
	int								lLoveliness;					//用户魅力
	IosDword								dwUserID;						//用户 I D
	tagUserScore						UserScore;						//用户积分
};

//会员等级
struct IPC_MemberOrder
{
	BYTE								cbMember;					//会员等级
	IosDword								dwUserID;						//用户 I D
};

//用户分数
struct IPC_UpdateFace
{
	IosDword								dwCustomFace;					//用户 I D
};

//////////////////////////////////////////////////////////////////////////
//IPC 控制信息

#define IPC_MAIN_CONCTROL				4								//控制信息

#define IPC_SUB_START_FINISH			1								//启动完成
#define IPC_SUB_CLOSE_FRAME				2								//关闭框架
#define IPC_SUB_JOIN_IN_GAME			3								//加入游戏
#define IPC_SUB_CLOSE_GAME				4								//退出游戏（断线处理）
#define IPC_SUB_CHANGE_TABLE			5								//换桌
#define IPC_SUB_TIME_OUT    			6								//长时间不操作

//加入游戏
struct IPC_JoinInGame
{
	WORD								wTableID;						//桌子号码
	WORD								wChairID;						//椅子号码
};

//////////////////////////////////////////////////////////////////////////
//网络命令码
#define MDM_GF_GAME						100								//游戏消息
#define MDM_GF_FRAME					101								//框架消息
#define	MDM_GF_PRESENT					102								//礼物消息
#define	MDM_GF_BANK						103								//银行消息
#define MDM_GF_LOTTERY                 104                            //彩金消息


#define SUB_GF_INFO						1								//游戏信息
#define SUB_GF_USER_READY				2								//用户同意
#define SUB_GF_LOOKON_CONTROL			3								//旁观控制
#define SUB_GF_KICK_TABLE_USER			4								//踢走用户
#define SUB_GF_EXCHANGE_CHIP			5								//兑换筹码
#define SUB_GF_UPDATE_SCORE				6								//更新金币


#define SUB_GF_OPTION					100								//游戏配置
#define SUB_GF_SCENE					101								//场景信息
#define SUB_GR_FIRST_INGOT				102								//第一次获得元宝（第一次哦~~）
#define SUB_GR_ROOM_OPTION				103								//房间配置

#define SUB_GF_SYSTEM_MESSAGE			200								//用户聊天

#define SUB_GF_MESSAGE					300								//系统消息
#define SUB_CM_SYSTEM_MESSAGE			1									//系统消息
#define SUB_GF_NOTICE					400								//系统通告


//#define SUB_GF_GIFT					400								//赠送消息

#define SUB_GF_BANK_STORAGE				450								//银行存储
#define SUB_GF_BANK_GET					451								//银行提取
#define SUB_GF_CHANGE_PASSWORD			452								//银行提取
#define SUB_GF_TRANSFER					453								//银行提取

#define SUB_GF_FLOWER_ATTRIBUTE			500								//鲜花属性
#define SUB_GF_FLOWER					501								//鲜花消息
#define SUB_GF_EXCHANGE_CHARM			502								//兑换魅力

#define SUB_GF_PROPERTY					550								//道具消息
#define SUB_GF_PROPERTY_RESULT			551								//道具结果
#define SUB_GF_RESIDUAL_PROPERTY		552								//剩余道具
#define SUB_GF_PROP_ATTRIBUTE			553								//道具属性
#define SUB_GF_PROP_BUGLE				554								//喇叭道具


#define SUB_GF_LOTTERY                  701                             //彩金信息
#define SUB_GF_LOTTERY_WEEK             700                             //个人彩金
#define SUB_GF_REQUEST_LOTTEWEEK        702                             //客户端请求个人彩金
#define SUB_GF_REQUEST_LOTTERY          703                             //客户端请求彩金信息
#define SUB_GF_BROADCAST_TEXT           704                             //自动广播(游戏内广播)

//框架命令
#define SUB_GF_GAME_OPTION			1									//游戏配置
#define SUB_GF_USER_READY			2									//用户准备
#define SUB_GF_LOOKON_CONFIG		3									//旁观配置
#define SUB_GF_USER_LEAVE			4									//用户离开
	//聊天命令
#define SUB_GF_USER_CHAT			10									//用户聊天
#define SUB_GF_USER_EXPRESSION		11									//用户表情

	//游戏信息
#define SUB_GF_GAME_STATUS			100									//游戏状态
#define SUB_GF_GAME_SCENE			101									//游戏场景
#define SUB_GF_LOOKON_STATUS		102									//旁观状态


//游戏中取款
#define MDM_GR_INSURE					5              //主命令
#define SUB_GR_QUERY_INSURE_INFO		1             //银行命令   查询银行
#define SUB_GR_TAKE_GAME_SCORE_REQUEST  6


//游戏广播
struct CMD_GF_Broadcast
{
    CHAR                Content[256];  //消息内容
};

//查询银行
struct CMD_GR_C_QueryInsureInfoRequest
{
	BYTE               cbActivityGame;                     //游戏动作
	TCHAR              szInsurePass[66];      //银行密码
};

struct CMD_GR_C_TakeGameScoreRequest
{
	BYTE                cbActivityGame;                     //游戏动作
	LONGLONG            lTakeScore;              //取款数目
	char               szInsurePass[33];      //银行密码
};

//彩金玩家
struct CMD_GF_LotteryPlayer
{
    CHAR                 PlayerName[NAME_LEN];       //玩家名称
    int                  Type;                       //彩金类型
    CHAR                 RewardData[5];              //牌型数据
    LONGLONG             RewardScore;                //奖励数目
};

//彩金信息
struct CMD_GF_Lottery
{
    LONGLONG                    Score;              //彩金数目
    int                         Count;              //彩金玩家总量
    int                         PlayerCount;        //彩金玩家数量
    CMD_GF_LotteryPlayer        Player[];           //彩金玩家
};


//个人本周彩金
struct CMD_GF_LotteryWeek
{
    LONGLONG              Score;              //彩金数目
};

//请求个人彩金信息
struct CMD_GF_RequestLotteWeek
{
    UINT32                dwUserID;            //用户ID
};
//请求彩金信息
struct CMD_GF_RequestLottery
{
    UINT32                dwUserID;            //用户ID
};

//版本信息
struct CMD_GF_Info
{
    BYTE                   bAllowLookon;
    CHAR                   szVersion[33];
};

//游戏配置
struct CMD_GF_Option
{
	BYTE								bGameStatus;					//游戏状态
	BYTE								bAllowLookon;					//允许旁观
};

//游戏配置
struct CMD_GF_GameOption
{
	BYTE                                cbAllowLookon;                      //旁观标志
	IosDword							dwFrameVersion;						//框架版本
	IosDword							dwClientVersion;					//游戏版本
};

//旁观控制
struct CMD_GF_LookonControl
{
	IosDword								dwUserID;						//用户标识
	BYTE								bAllowLookon;					//允许旁观
};

//踢走用户
struct CMD_GF_KickTableUser
{
	IosDword								dwUserID;						//用户 I D
};

//兑换筹码   SUB_GF_EXCHANGE_CHIP
struct CMD_GF_ExchangeChip
{
	LONGLONG							lBodyChip;						//兑换筹码数量
};

//SUB_GR_FIRST_INGOT				102								//第一次获得元宝（第一次哦~~）
struct CMD_CF_FirstInfot
{
	LONGLONG							lNum;							//元宝数
};

//SUB_GR_ROOM_OPTION				103								//房间配置
struct CMD_CF_RoomOption
{
	LONGLONG							lCellScore;						//单元积分
	WORD								wServerType;					//服务器类型
};

//聊天结构
struct CMD_GF_UserChat
{
	BYTE								cbChatType;						//聊天类型(0为普通喊话,1为小喇叭喊话)
	TCHAR								szNickName[NAME_LEN];			//用户昵称
	WORD								wChatLength;					//信息长度
	COLORREF							crFontColor;					//信息颜色
	IosDword								dwSendUserID;					//发送用户
	IosDword								dwTargetUserID;					//目标用户
	TCHAR								szChatMessage[MAX_CHAT_LEN];	//聊天信息
};

//系统消息
struct CMD_CM_SystemMessage_MB
{
	WORD							wType;								//消息类型
	WORD							wLength;							//消息长度
	CHAR							szString[1024];						//消息内容
};

//////////////////////////////////////////////////////////////////////////

//消息类型
#define SMT_INFO						0x0001							//信息消息
#define SMT_EJECT						0x0002							//弹出消息
#define SMT_GLOBAL						0x0004							//全局消息

//消息数据包
struct CMD_GF_Message
{
	WORD								wMessageType;					//消息类型
	WORD								wMessageLength;					//消息长度
	TCHAR								szContent[1024];				//消息内容
};


//////////////////////////////////////////////////////////////////////////

//共享内存定义
struct tagShareMemory
{
	WORD								wDataSize;						//数据大小
	//HWND								hWndGameFrame;					//框架句柄
	//HWND								hWndGamePlaza;					//广场句柄
	//HWND								hWndGameServer;					//房间句柄
};

//////////////////////////////////////////////////////////////////////////

//发送场所
#define	LOCATION_GAME_ROOM				1								//游戏房间
#define	LOCATION_PLAZA_ROOM				2								//大厅房间

//////////////////////////////////////////////////////////////////////////

//赠送结构
struct CMD_GF_Gift
{
	BYTE								cbSendLocation;					//发送场所
	IosDword								dwSendUserID;					//赠送者ID
	IosDword								dwRcvUserID;					//接受者ID
	WORD								wGiftID;						//礼物	ID
	WORD								wFlowerCount;					//鲜花数目
};

//道具结构
struct CMD_GF_Property
{
	BYTE								cbSendLocation;					//发送场所
	int									nPropertyID;					//道具ID
	IosDword								dwPachurseCount;				//购买数目
	IosDword								dwOnceCount;					//单次数目
	IosDword								dwSourceUserID;					//赠送玩家
	IosDword								dwTargetUserID;					//目标玩家
	TCHAR								szRcvUserName[32];				//用户名称
};

//喇叭结构
struct CMD_GF_BugleProperty
{
	BYTE								cbSendLocation;					//发送场所
	TCHAR								szUserName[32];					//玩家帐号
	COLORREF							crText;							//文字颜色
	TCHAR								szContext[BUGLE_MAX_CHAR];		//喇叭内容
};

//兑换结构
struct CMD_GF_ExchangeCharm
{
	BYTE								cbSendLocation;					//发送场所
	int								lLoveliness;					//魅力数值
	IosDword								lHappyBearValue;						//欢乐豆数目--》金币-->银币
};

//赠送通知
struct CMD_GF_GiftNotify
{
	BYTE								cbSendLocation;					//发送场所
	IosDword								dwSendUserID;					//赠送者ID
	IosDword								dwRcvUserID;					//接受者ID
	WORD								wGiftID;						//礼物	ID
	WORD								wFlowerCount;					//鲜花数目
};

//存储金币
struct CMD_GF_BankStorage
{
	LONGLONG							lStorageValue;					//存储金币
	LONGLONG							lStorageSilverValue;					//存储银币
	BYTE								cbGameAction;					//游戏操作 0 大厅 1 游戏
};

//提取金币
struct CMD_GF_BankGet
{
	LONGLONG							lGetValue;						//提取金币
	LONGLONG							lGetSilverValue;						//提取银币
	TCHAR								szPassword[PASS_LEN];			//用户密码
	BYTE								cbGameAction;					//游戏操作
};

//提取金币
struct CMD_GF_ChangePassword
{
	TCHAR								szOriginPassword[PASS_LEN];		//用户密码
	TCHAR								szNewPassword[PASS_LEN];		//用户密码
	BYTE								cbGameAction;					//游戏操作 0 大厅 1 游戏
};

//转账记录
struct CMD_GF_Transfer
{
	LONGLONG							lInputCount;					//转账金币
	IosDword								dwUserID;						
	TCHAR								szPassword[PASS_LEN];			//用户密码
	TCHAR								szNickname[NAME_LEN];			//用户昵称
	BYTE								cbGameAction;					//游戏操作 0 大厅 1 游戏
	BYTE								cbTransferType;					//转账类型 0 金币 1 银币
};

//剩余道具
struct CMD_GF_ResidualProperty
{
	IosDword								dwResidualTime[PROPERTY_COUNT];	//剩余时间
};

#pragma pack()


//////////////////////////////////////////////////////////////////////////
#endif
