﻿#ifndef _JNI_SINK_H_
#define _JNI_SINK_H_

#define PAY_TYPE_BANKONLINE											1		//银行卡
#define PAY_TYPE_ALIPAY												2		//支付宝
#define PAY_TYPE_SHORTMESSAGE										3		//移动MM
#define PAY_TYPE_MI													4		//小米
#define PAY_TYPE_IOS												5		//苹果
#define PAY_TYPE_MOBILE_CARD										6		//移动
#define PAY_TYPE_TELE												7		//电信
#define PAY_TYPE_UNICOM												8		//联通
#define PAY_TYPE_SD													9		//盛大
#define PAY_TYPE_WY													10		//网易
#define PAY_TYPE_JW													11		//骏网
#define PAY_TYPE_UP													12		//优贝
#define PAY_TYPE_LDYS												13		//联动优势

//c++调用java
/////////////////////////////////////////////////////////////////////////
#define CMD_CTJ_QQAUTHLOGIN			1				//qq登录授权
#define CMD_CTJ_QQUSERINFO			2				//qq用户信息

#define CMD_CTJ_MILOGIN				10				//小米登录

#define CMD_CTJ_PAY					100				//调用java支付接口
#define CMD_CTJ_PAY_PATCH			101				//补单

#define CMD_CTJ_BROWSE				200				//打开内嵌
#define CMD_CTJ_BROWSE_CLOSE		201				//关闭内嵌
#define CMD_CTJ_SYSTEM_BROWSE		202				//打开系统浏览器

#define CMD_CTJ_VIBRATE				210				//调用java震动
#define CMD_CTJ_READCONTACTS		220				//读取联系人
#define CMD_CTJ_SENDMSG				230				//发送短信
#define CMD_CTJ_WX					280				//分享微信
#define CMD_CTJ_WX_PAY				281				//微信支付
#define CMD_CTJ_WB					290				//分享微博
#define CMD_COPY_ID 				300				//复制ID
#define CMD_CloseGame 				301				//退出游戏
#define CMD_CTJ_GetDeviceID			302
#define CMD_CTJ_LockPhone			303				//手机锁
#define CMD_CTJ_UpdataGame			304
#define CMD_CTJ_ShareWX				305				//分享
#define CMD_CTJ_GooglePay			306				//google支付
#define CMD_CTJ_OpenWX				307				//打开微信
#define CMD_CTJ_WXLogin				308				//微信登陆
#define CMD_CTJ_GetVersion			309             //获取安卓版本
#define CMD_CTJ_REGISTER			310             //注册
#define CMD_CTJ_LOGIN               311             //登录

//Java调用c++
////////////////////////////////////////////////////////////////////////
#define CMD_JTC_QQAUTHLOGIN			1				//qq登录授权
#define CMD_JTC_QQUSERINFO			2				//qq用户信息
#define CMD_JTC_MILOGIN				10				//小米登录回调


#define CMD_JTC_PAY					100				//返回支付结果
#define CMD_JTC_PAY_GETORDERNUM		101				//返回订单编号
#define CMD_JTC_PAY_ERROR			102				//返回支付失败
#define CMD_JTC_WX_PAY				103				//返回微信支付结果

#define	CMD_JTC_READCONTACTS		220				//读取联系人
#define	CMD_JTC_SENDMSG				230				//发送短信
#define CMD_JTC_WIFI				240				//判断WIFI
#define CMD_JTC_SENDHTTPREQUEST		250				//发送HTTP请求
#define CMD_JTC_DOWNLOADAPKCANCEL	260				//下载APK
#define CMD_JTC_WX					280				//分享微信成功
#define CMD_JTC_WB					290				//分享微博成功
#define CMD_JTC_ChannelId          300             //渠道号
#define CMD_JTC_GetDeviceID			302
#define CMD_JTC_WXLogin				303				//微信登陆成功
#define CMD_JTC_GetVersion			304				//获取安卓版本
#define CMD_JTC_GetMobileInfo		305				//获取手机信息

#include "cocos2d.h"
#include "def.h"


using namespace cocos2d;

class IGameView;

class JniCallback
{
public:
	//充值成功
	virtual void paycallback(const char *szOrderNum){}
	//订单号获取成功
	virtual void ordernumcallback(const char *szOrderNum){}
	//分享完成
	virtual void shareCallback(int type , bool bSuccess){}		
	//微信登陆成功
	virtual void WXLoginSuc(const char* szMessage){}
    //获取安卓版本成功
    virtual void GetAndroidVersionSuc(const char* szMessage){}
};

class JniSink
{
protected:
	JniSink(void);
public:
	~JniSink(void);

	static JniSink *share(void);
public:
	//QQ登录认证
	void qqLoginAuth();
	//QQ用户详情
	void qqUserInfo();
#if (GAME_PLATFORM == PLATFORM_XM)
	//小米登录认证
	void XiaoMiLoginAuth();
#endif

	/*支付接口
	参数 userId ：用户ID（单机版传0）
	参数 money ：支付金额（元）
	参数 score ：货币数量
	参数 szVersion : 游戏版本
	参数 payType: 支付类型（1网银 2支付宝 3短信 4小米充值）
	参数 platform：支付平台（先用1）
	*/
	void sendPayOrder(int userId , int gameId, int money , int score , const char* szVersion ,int payType);

	//充值卡支付
	void sendPayOrder(int userId , int money , int score , const char* szVersion ,int payType, int platform , const char * szCardNum , const char * szCardPwd);

	//水浒传支付
	void sendPayOrder(int userId, WORD id, int money , int score , const char* szVersion ,int payType, int platform);

	//微信支付
	void sendWXPayOrder(int userId, int gameId, int money);

	//google支付
	void SendGooglePay(int userid, int money, int score, char* password);

	//震动
	void vibrate(int iTime);

	//打开内嵌(不传参为全屏内嵌)，暂时只能打开一个
	void OpenWebView(char *szURL);
	void OpenWebView(char *szURL, int x, int y, int iWidth, int iHeight);
	//关闭内嵌
	void CloseWebView();

	//打开浏览器
	void OpenSystemWeb(const char *szURL);

	//读取联系人
	void LoadContacts();

	//发送短信(strNum：手机号    strContent：短信内容)
	void SendShortMessage(std::string strNum, std::string strContent);

	//补单
	void sendPatchOrder( const char *szOrderNum, int score, int money);

    void SendLoginSuccess(const char *userd);
    
    void SendRegisterSuccess(const char *userd);
    
	void CopyID(int _ID);

	void SendToDeviceID();

    void GetAndroidVersion();
    
	void CloseGameWin();

	void SendLockPhone(int userId);

	void UpdataGame();

	void OpenWX();

	void CTJ_WXLogin();

public:
	//微信支付回调
	void WXPayCallBack(const char * szParams);

	void javaCallback(int cmd , const char * szParams);
	//分享回调
	void ShareCallback(const char *szParam , int type);
	//QQ登录认证回调
	void qqLoginAuthCallback(const char *szParam);
	//QQ用户详情回调
	void qqUserInfoCallback(const char *szParam);
	//取得订单编号成功
	void GetOrderNumcallback(const char *szParam);
	//易联支付XML报文设置
	void SetXMLcallback(std::string str);
	//安卓支付成功
	void androidPaySuccess(const char *szParam);
	//安卓支付失败(暂时只有IOS支付会调用)
	void androidPayError(const char *szParam);
	//发送短信成功
	void SendMsgSuccess(const char * szTelephone);
	//微信分享
	void shareWeixin(const char *szUrl , const char *path , const char *title , const char *desc);
	void shareWeixin(const char *path);
	//微博分享
	void shareWeibo(const char *szUrl , const char *path , const char *title , const char *desc);
	void shareWeibo(const char *path);

	//新的分享  用户id, 网页或图片1，0  朋友圈或好友1，0 
	void ShareNewWX(int userid, int ShareType, int ShareFriend, char* Title);

    //获取渠道ID
    void SetChannelID(const char *szParam);
	void SetDeviceID(const char *szParam);
	char* GetDeviceID();
    void SetMobileInfo(const char *szParam);
    std::string GetMobileBrand();
    std::string GetMobileKind();
    std::string GetMobileSystemVersion();

#if (GAME_PLATFORM == PLATFORM_XM)
	//小米登录回调
	void XiaoMiLoginAuthCallback(const char *szParam);
#endif

public:
	void	setJniCallback(JniCallback *pJniCallback){m_pJniCallback = pJniCallback;}
	void	setIGameView(IGameView *pIGameView){m_pIGameView = pIGameView;}
	IGameView *m_pIGameView;

	DWORD		m_dwUserID;
	WORD		m_wBuyID;
	BYTE		m_cbType;				//充值类型
	LONG		m_lScore;				//游戏币
	LONG		m_lMoney;				//人民币
	 int		m_iPayType;				//充值方式
	 int		m_iPlatform;			//运营商
	std::string m_strXML;				//易联支付报文
	char		m_DeviceID[128];
    std::string m_MobileBrand;
    std::string m_MobileKind;
    std::string m_SystemVersion;
    
private:
	JniCallback							*m_pJniCallback;
	
	static JniSink *m_pJniSink;

};

#endif
