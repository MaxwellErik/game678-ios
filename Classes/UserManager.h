﻿#ifndef USER_MANAGER_HEAD_FILE
#define USER_MANAGER_HEAD_FILE
#include "GameParm.h"


//用户管理类
class CUserManager
{
public:
	CUserManager(void);
	~CUserManager(void);
private:
	std::vector<tagUserData *>		m_vecUserData;//用户容器


public:
	//添加用户
	tagUserData *AddUserItem(tagUserData &UserData);
	//删除所有用户
	bool DeleteAllUser();
	//删除用户
	bool DeleteUserItem(tagUserData *pUserData);
	//删除用户
	bool DeleteUserItem(DWORD dwUserID);
	//更新积分
	bool UpdateUserItemScore(tagUserData *pUserData, const tagUserScore * pUserScore);
	//更新状态
	bool UpdateUserItemStatus(tagUserData * pUserData, const tagUserStatus * pUserStatus);
	//枚举用户
	tagUserData * EnumUserItem(WORD wEnumIndex);
	//查找用户
	tagUserData * SearchUserByUserID(DWORD dwUserID);
	//获取相应桌号的人数
	WORD GetTableCount(WORD wTableID);
	//获取相应桌号相应座位的玩家节点
	tagUserData *GetUserData(WORD wTableID, WORD wChairID);
	//获取人数
	DWORD GetOnLineCount() { return (DWORD)m_vecUserData.size(); }
};

#endif
