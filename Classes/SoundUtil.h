﻿#ifndef _SOUNDUTIL_H_
#define _SOUNDUTIL_H_

#include "GameConstant.h"
#include <string>

class SoundUtil
{
public:
	SoundUtil(void);
	~SoundUtil(void);

public:

	void preloadEffect(const char* MusicName);

	void playEffect(const char* MusicName , bool bLoop=false);

	void stopAllEffects();

	void playMp3(const char* MusicName , bool bLoop);
	void playWav(const char* MusicName , bool bLoop);
	void playOgg(const char* MusicName , bool bLoop);

	void playBackMusic(const char *pMusicName , bool bLoop=true);

	void setSoundVolume(float fv);

	void setBackSoundVolume(float fv);

	void stopBackMusic(void);

	//手机震动接口IOS
	void vibrate(int iTime = 10);

public:
	static SoundUtil *sharedEngine(void);
	static SoundUtil *m_pSoundUtil;


};


#endif
