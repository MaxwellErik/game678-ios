﻿#ifndef _SHGAME_VIEW_LAYER_H_
#define _SHGAME_VIEW_LAYER_H_

#include "GameScene.h"
#include "ShGameLogic.h"
#include "FrameGameView.h"
#include "ShGameLogic.h"
#include "ShGoldControl.h"
#include "ShCardControl.h"
#include "CMD_ShowHand.h"

class CTimeTaskLayer;
class ShGameViewLayer : public IGameView
{
public:
	static ShGameViewLayer *create(GameScene *pGameScene);
	virtual ~ShGameViewLayer();

	static void reset();
	ShGameViewLayer(const ShGameViewLayer&);
	ShGameViewLayer(GameScene *pGameScene);
	ShGameViewLayer& operator = (const ShGameViewLayer&);
	virtual bool init(); 
	virtual void onEnter();
	virtual void onExit();
    virtual bool onTouchBegan(Touch *pTouch, Event *pEvent);
    virtual void onTouchMoved(Touch *pTouch, Event *pEvent);
    virtual void onTouchEnded(Touch *pTouch, Event *pEvent);
	virtual bool onTextFieldAttachWithIME(TextFieldTTF*pSender){return false;}
	virtual bool onTextFieldDetachWithIME(TextFieldTTF*pSender){return false;}
	virtual bool onTextFieldInsertText(TextFieldTTF *pSender, const char *text, int nLen){return true;}
	virtual bool onTextFieldDeleteBackward(TextFieldTTF *pSender, const char *delText, int nLen){return true;}
	virtual void keyboardWillShow(IMEKeyboardNotificationInfo &info){};
	virtual void keyboardDidShow(IMEKeyboardNotificationInfo &info){};
	virtual void keyboardWillHide(IMEKeyboardNotificationInfo &info);
	virtual void keyboardDidHide(IMEKeyboardNotificationInfo &info);
	virtual void backLoginView(Ref *pSender);
	virtual void DrawUserScore(){}	//绘制玩家分数
	virtual void GameEnd(){};
	virtual void ShowAddScoreBtn(bool bShow=true){};
	virtual void UpdateDrawUserScore(){};		//更新显示的游戏币数量

	//初始化
	void InitGame();
	void AddPlayerInfo();
	void AddButton();
	Menu* CreateButton(std::string szBtName ,const Vec2 &p , int tag);
	string AddCommaToNum(LONG Num);

	// 按钮事件管理
	void DialogConfirm(Ref *pSender);
	void DialogCancel(Ref *pSender);

	void callbackBt( Ref *pSender );
	void hiddenAll();

	void CardMoveEnd(Node *pSender,void*data);
	void CardMoveEnd1(Node *pSender,void*data);
	void GoldMoveCallback(Node *pSender,void*data);
	void OnQuit();

	//网络接口
public:
	void OnEventUserLeave( tagUserData * pUserData, WORD wChairID, bool bLookonUser );
	//用户进入
	virtual void OnEventUserEnter(tagUserData * pUserData, WORD wChairID, bool bLookonUser);
	//用户积分
	virtual void OnEventUserScore(tagUserData * pUserData, WORD wChairID, bool bLookonUser);
	//用户状态
	virtual void OnEventUserStatus(tagUserData * pUserData, WORD wChairID, bool bLookonUser);
    //用户换桌
    virtual void OnEventUserChangeTable(tagUserData * pUserData, WORD wChairID, bool bLookonUser){};
	//游戏消息
	virtual bool OnGameMessage(WORD wSubCmdID, const void * pBuffer, WORD wDataSize);
	//场景消息
	virtual bool OnGameSceneMessage(BYTE cbGameStatus, const void * pBuffer, WORD wDataSize);

	virtual void OnReconnectAction(){};

//发送网络消息
public:
	void SendGameStart();

	void SendReqTransProp();

	void SendSendProp(DWORD dwRcvUserID ,WORD wPropID , WORD wAmount , int lPayscore);

	void SendReqSendRecord();

//接收网络消息
protected:
	//游戏开始
	bool OnSubGameStart(const void * pBuffer, WORD wDataSize);
	//用户加注
	bool OnSubAddScore(const void * pBuffer, WORD wDataSize);
	//用户放弃
	bool OnSubGiveUp(const void * pBuffer, WORD wDataSize);
	//发牌
	bool OnSubSendCard(const void * pBuffer, WORD wDataSize);
	//看牌
	bool OnSubLookCard(const void * pBuffer, WORD wDataSize);
	//结束
	bool OnSubGameEnd(const void * pBuffer, WORD wDataSize);

	string GetCardStringName(BYTE card);
	void UpdateTime(float fp);
	void StopTime();
	void StartTime(int _time, int chair = 1);

	void SetGoldTitleInfo(LONGLONG lMaxGold, LONGLONG lBasicGold);
	void SetUserGold( WORD wChairId, LONGLONG lGold );
	void SetUserGoldInfoNew(WORD wViewChairID, bool bTableGold, LONGLONG dwGold);
	void dispatchCards(WORD chair, BYTE card);
	void CardMoveCallback();
	void CardMoveCallbackEx();
	void SendCard(float dt);
	void ShowScoreControl( bool bShow );
	void UpdateScoreControl(bool IsMe = false);
	void SetUserShowHand( bool bShowHand , WORD chair);
	void OnFollow();
	void OnNotAdd();
	void OnGiveUp();
	void OnAddGold(int wParam);
	void OnShowHand();
	void UpdataSendCard();
	void PerformGameEnd();
	void ShowTips(WORD chair, int _kind); //操作类型 0 过, 1 加, 2 跟, 3 梭哈
	void LookCard();
    void UpdateLookCard(float dt);
	void UpdataAllScore(); //更新总注

protected:
	ShGameLogic			m_GameLogic;

	//精灵
	Sprite*			m_BackSpr;
	Sprite*			m_ClockSpr;
    Sprite*			m_SelfText;
    Sprite*			m_OtherText;
    Sprite*			m_middleText;
    
	//按钮
	Menu*				m_BtnReadyPlay;		//开始按钮
	Menu*				m_BtnBackToLobby;	//返回按钮
	Menu*				m_BtnSeting;		//设置按钮
	Menu*				m_BtnShowHand;		//梭哈
	Menu*				m_BtnFollow;		//跟注
	Menu*				m_BtnGiveUp;		//放弃
	Menu*				m_BtnBack1;			//加注1倍
	Menu*				m_BtnBack2;			//加注2倍
	Menu*				m_BtnBack3;			//加注3倍
	Menu*				m_BtnGetScoreBtn;


	//位置
	Vec2				m_HeadPos[GAME_PLAYER];
	Vec2				m_ReadyPos[GAME_PLAYER];
	Vec2				m_AllBetPos[GAME_PLAYER];
	Vec2				m_TableBetPos[GAME_PLAYER];
	Vec2				m_TimePos[GAME_PLAYER];
	Vec2				m_TipsPos[GAME_PLAYER];
	Vec2				m_GoldStartPos[GAME_PLAYER];

	//tag标志
	int					m_ReadyTga[GAME_PLAYER];
	int					m_HeadTga[GAME_PLAYER];
	int					m_HeadGlodTga[GAME_PLAYER];
	int					m_NickNameTga[GAME_PLAYER];
	int					m_AllGoldTga[GAME_PLAYER];
	int					m_TableGoldTga[GAME_PLAYER];
	int					m_BtnBackTga;                       //按钮背景
	int					m_TempSendCardTga[GAME_PLAYER];     //临时发牌
	int					m_HandCardTga[GAME_PLAYER];
	int					m_CardTypeTga[GAME_PLAYER];         //牌类型
	int					m_TipsKindTga;                      //操作提示
	int					m_FontAllScoreTga;
	int					m_FontScoreBtn[3];                  //筹码按钮
	int					m_EndScore[2];

	//变量
	int					m_GameTime;                         //游戏时间
	LONGLONG			m_lTurnMaxGold;						//最大下注
	LONGLONG			m_lFollowGold;                      //跟注数目
	LONGLONG			m_lBasicGold;						//单元数目
	BYTE				m_bPlayStatus[GAME_PLAYER];			//游戏状态
	WORD				m_wCurrentUser;						//当前玩家
	LONGLONG			m_lShowHandScore;					//限制最高分
	LONGLONG			m_lUserScore[GAME_PLAYER];          //玩家金币
    LONGLONG            m_lTotalGold;                       //当局总下注
	bool				m_bEnableSound;						//启用声音
	bool				m_bShowHand;						//是否梭哈
	LONGLONG			m_lGoldShow;						//加注筹码
	TCHAR				m_szName[GAME_PLAYER][NAME_LEN];	//玩家名字
	CMD_S_GameEnd		m_GameEnd;							//
	CMD_R_SendCard		m_SendCard;
	bool				m_GiveUp;
	WORD				m_wCardCount;
	
	bool				m_bUserShowHand[2];
	bool				m_Win;
	bool				m_bFirstBet;                        //先下注
	LONGLONG			m_lCurTurnScore[GAME_PLAYER];       //当前局分数
    LONGLONG			m_CurBet;                           //当前下注
	bool                m_bAddScore;
	int					m_MyCardCount;
	int					m_OtherCardCount;

	//发牌变量
	BYTE				m_TempCardData[GAME_PLAYER][3];
    BYTE                m_FirstCardData;
	int					m_TempCardCount;
	int					m_SendCardIndex;
    int                 m_SendCardCount;
	int					m_UpCardIndex;
	int					m_DownCardIndex;
	int					m_PlayerHandIndex[GAME_PLAYER];
	BYTE				m_PlayerHandCardData[GAME_PLAYER][3];

    LONGLONG            m_curShowHandScore;
    bool                m_isLookCard;
    
	//对象
	ShGoldControl*		m_AllGoldControl[2];                //个人筹码汇总
	ShGoldControl*		m_TableGoldControl[2];              //个人筹码
	ShCardControl*		m_CardControl[GAME_PLAYER];			//扑克控件
};

#endif
