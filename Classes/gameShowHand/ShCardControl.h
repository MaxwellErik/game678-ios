#ifndef _SHCARDCONTROL_H_
#define _SHCARDCONTROL_H_
#include <vector>
using namespace std;
#include "GameScene.h"
#include "ShGameLogic.h"

class ShCardControl
{
public:
	ShCardControl(GameLayer* _layer, int tga, bool IsMy);
	~ShCardControl();

    virtual bool onTouchBegan(Vec2 pos);
public:
	vector<BYTE> m_CardData;
	bool m_ShowBackCard; //是否显示底牌
	Vec2 m_CardCenterPos;
	Vec2 m_CardSize;
    cocos2d::Rect m_rect;
	int m_dx;
	int m_dy;
	int m_odx;
	int m_ody;
	int m_lookState;
	int m_OtherlookState;
	BYTE m_FirstCard;
	vector<BYTE> m_LookCardVec;
	
	ShGameLogic			m_GameLogic;
	int					m_CardTga[6];
	bool				m_IsMy;
	GameLayer*			m_layer;
    
public:
	void SetCardData(BYTE bCardData[], DWORD dwCardCount);
	void SetFirstCard(BYTE _card);
	void ShowFirstCard(bool _show);
	void SetPos(int x, int y);
	WORD GetCardData(BYTE cbCardData[], WORD wBufferCount);
	void ClearCardData();
	void GiveUp();
	int GetCardCount();
	string GetCardStringName(BYTE card);
	void SetCardType(BYTE cardtype0);

	void LookCard(bool _ShowFirstCard);
	void CardMoveEnd(Node *pSender,void*data);
};
#endif
