#include "ShGameLogic.h"


//扑克数据
BYTE ShGameLogic::m_cbCardListData[54]=
{
	0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08,0x09,0x0A,0x0B,0x0C,0x0D,	//方块 A - K
	0x11,0x12,0x13,0x14,0x15,0x16,0x17,0x18,0x19,0x1A,0x1B,0x1C,0x1D,	//梅花 A - K
	0x21,0x22,0x23,0x24,0x25,0x26,0x27,0x28,0x29,0x2A,0x2B,0x2C,0x2D,	//红桃 A - K
	0x31,0x32,0x33,0x34,0x35,0x36,0x37,0x38,0x39,0x3A,0x3B,0x3C,0x3D,	//黑桃 A - K
	0x4E,0x4F//14 15
};

//////////////////////////////////////////////////////////////////////////

//构造函数
ShGameLogic::ShGameLogic()
{
}

//析构函数
ShGameLogic::~ShGameLogic()
{
}

//获取当前排列的牌类型
BYTE ShGameLogic::GetMyType(BYTE cbCardData[])
{
	BYTE cbHandCard[3];//手牌3张
	BYTE cbOxCard[2];//牛牌2张
	CopyMemory(cbHandCard,cbCardData,3);
	CopyMemory(cbOxCard,&cbCardData[3],2);

	BYTE cbSum=0;
	for(int i=0;i<3;i++)
	{
		cbSum+=GetCardLogicValue(cbHandCard[i]);
	}
	//手牌和为10，证明有牛
	if(cbSum%10==0)
	{
		//牛牌
		for (int i=0;i<2;i++)
		{
			cbSum+=GetCardLogicValue(cbOxCard[i]);
		}
		BYTE cbResult = cbSum%10;
		return (cbResult==0)?10:cbResult;
	}

	return OX_VALUE0;
}

//获取类型
BYTE ShGameLogic::GetCardTypeMo(BYTE cbCardData[], BYTE cbCardCount)
{
	BYTE cbMaxCard[MAX_COUNT]={0};
	BYTE cbMaxType=OX_VALUE0;

	//排序，并找出最终牌型GetMyType()寻找最大牌型
	for(int first=0;first<3;first++)
	{
		for(int second=first+1;second<4;second++)
		{
			for(int three=second+1;three<5;three++)
			{
				BYTE cbTemp[MAX_COUNT]={0};
				cbTemp[0]=cbCardData[first];
				cbTemp[1]=cbCardData[second];
				cbTemp[2]=cbCardData[three];
				int index=3;
				for(int i=0;i<MAX_COUNT;i++)
				{
					if(i!=first && i!=second && i!=three)
					{
						cbTemp[index++]=cbCardData[i];
					}
				}
				BYTE cbType=GetMyType(cbTemp);
				//如果找到更大牌型，取新排列
				if(cbMaxType==OX_VALUE0 || cbMaxType<cbType)
				{
					cbMaxType=cbType;
					CopyMemory(cbMaxCard,cbTemp,MAX_COUNT*sizeof(BYTE));
				}
			}
		}
	}

	return cbMaxType;
}
//获取类型
BYTE ShGameLogic::GetCardType(BYTE cbCardData[], BYTE cbCardCount)
{
	ASSERT(cbCardCount==MAX_COUNT);

	//设置变量
	BYTE cbTempData[MAX_COUNT];
	CopyMemory(cbTempData,cbCardData,sizeof(cbTempData));

	//炸弹牌型
	BYTE bSameCount = 0;
	SortCardList(cbTempData,cbCardCount);
	BYTE bSecondValue = GetCardValue(cbTempData[MAX_COUNT/2]);
	for(BYTE i=0;i<cbCardCount;i++)
	{
		if(bSecondValue == GetCardValue(cbTempData[i]))
		{
			bSameCount++;
		}
	}

	//炸弹
	if(bSameCount==4)return OX_FOURSAME;

	BYTE bTemp[MAX_COUNT];
	BYTE bSum=0;
	for (BYTE i=0;i<cbCardCount;i++)
	{
		bTemp[i]=GetCardLogicValue(cbCardData[i]);
		bSum+=bTemp[i];
	}
	//五小牌型
	if ((bSum>=5)&&(bSum<=10)) { return OX_FIVESMALL; }

	//花牌搜索
	BYTE bKingCount=0,bTenCount=0;
	for(BYTE i=0;i<cbCardCount;i++)
	{
		if(GetCardValue(cbCardData[i])>10)
		{
			bKingCount++;
		}
		else if(GetCardValue(cbCardData[i])==10)
		{
			bTenCount++;
		}
	}

	//五花牌型
	if(bKingCount==MAX_COUNT) { return OX_FIVEKING; }
	if((bKingCount==MAX_COUNT-1)&&(bTenCount==1)) { return OX_FOURKING; }

	for (BYTE i=0;i<cbCardCount-1;i++)
	{
		for (BYTE j=i+1;j<cbCardCount;j++)
		{
			if((bSum-bTemp[i]-bTemp[j])%10==0)
			{
				return ((bTemp[i]+bTemp[j])>10)?(bTemp[i]+bTemp[j]-10):(bTemp[i]+bTemp[j]);
			}
		}
	}

	return OX_VALUE0;
}
//获取倍数
BYTE ShGameLogic::GetTimes(BYTE cbCardData[], BYTE cbCardCount)
{
	if(cbCardCount!=MAX_COUNT)return 0;

	//BYTE bTimes=GetCardType(cbCardData,MAX_COUNT);

	/*
	牛牛——5倍与压注筹码 
	牛9——4倍与压注筹码 
	牛8——3倍与压注筹码 
	牛7——2倍与压注筹码 
	没牛—牛6——1倍与压注筹码
	*/
	BYTE bTimes=GetCardType(cbCardData,MAX_COUNT);
	if(bTimes==OX_FOURSAME) return 8;
	else if(bTimes==OX_FIVESMALL) return 8;
	else if(bTimes==OX_FIVEKING) return 7;
	else if(bTimes==OX_FOURKING) return 6;
	else if(bTimes==10)
	{
		return 5;
	}
	else if(bTimes==9)
	{
		return 4;
	}
	else if(bTimes==8)
	{
		return 3;
	}
	else if(bTimes==7)
	{
		return 2;
	}
	else
	{
		return 1;
	}

	return 0;
}

//逻辑大小
BYTE ShGameLogic::GetCardLogicNewValue(BYTE cbCardData)
{
	BYTE cbValue=GetCardValue(cbCardData);

	//获取花色
	BYTE cbColor=GetCardColor(cbCardData);

	if(cbValue>10)
	{
		cbValue = 10;
	}
	if(cbCardData == 0x41)
	{
		return 15;
	}
	else if (cbCardData == 0x42)
	{
		return 18;
	}
	return cbValue;
}

bool ShGameLogic::GetOxCardEx(BYTE cbCardData[], BYTE cbCardCount)
{
	ASSERT(cbCardCount==MAX_COUNT);

	BYTE bTemp[MAX_COUNT],bTempData[MAX_COUNT];
	CopyMemory(bTempData,cbCardData,sizeof(bTempData));

	//王的数量, 10的数量
	BYTE bTenCount=0;
	for(BYTE i=0;i<cbCardCount;i++)
	{
		if (cbCardData[i]==0x41||cbCardData[i]==0x42 || GetCardValue(cbCardData[i]) >=10) bTenCount++;
	}

	//排序
	BYTE tcard;
	BYTE maxc = 0;
	for(BYTE i=0;i<cbCardCount;i++)
	{
		maxc = i;
		//if(cbCardData[i]==0x41||cbCardData[i]==0x42 || GetCardValue(cbCardData[i])>=10) continue;
		for (int j = i+1; j < cbCardCount; j++)
		{
			//CString tt;
			//tt.Format("%d,%d", GetCardLogicNewValue(cbCardData[i]),GetCardLogicNewValue(cbCardData[j]));
			//MessageBox(0,tt,0,0);
			if (GetCardLogicNewValue(cbCardData[j]) > GetCardLogicNewValue(cbCardData[maxc]))
			{
				maxc = j;
			}
		}

		if (maxc != i)
		{
			tcard = cbCardData[maxc];
			cbCardData[maxc] = cbCardData[i];
			cbCardData[i] = tcard;
		}
	}

	//for(int i=0; i<9; ++i) 
	//{// 选择第 i 小的记录，并交换到位
	//	int j = i;
	//	for(int k=i+1; k<=9; k++ )
	//	{// 在L.r[i..L.length]中选择关键字最小的记录
	//		if( L[k]< L[j] ) 
	//			j = k;
	//	}
	//	if( i != j ){// 与第 i 个记录互换
	//		int temp = L[j];
	//		L[j] = L[i];
	//		L[i] = temp;
	//	}
	//}


	//CString tt;
	//tt.Format("%0x,%0x,%0x,%0x,%0x", cbCardData[0],cbCardData[1],cbCardData[2],cbCardData[3],cbCardData[4]);
	//MessageBox(0,tt,0,0);
	//
	if (bTenCount >= 3)   //543张牛牌
	{
		return true;
	}
	else if (bTenCount == 2) //2张牛牌
	{
		//先计算
		BYTE t1,t2;    //k,k,2,7,8
		if(GetCardValue(cbCardData[2]) + GetCardValue(cbCardData[3]) == 10) // 2,3
		{
			t1 = cbCardData[0]; cbCardData[0] = cbCardData[2]; cbCardData[2] = t1;
			t2 = cbCardData[1]; cbCardData[1] = cbCardData[3]; cbCardData[3] = t2; return true;
		}
		else if (GetCardValue(cbCardData[2]) + GetCardValue(cbCardData[4]) == 10) // 2,4
		{
			t1 = cbCardData[0]; cbCardData[0] = cbCardData[2]; cbCardData[2] = t1;
			t2 = cbCardData[1]; cbCardData[1] = cbCardData[4]; cbCardData[4] = t2; return true;
		}
		else if (GetCardValue(cbCardData[3]) + GetCardValue(cbCardData[4]) == 10) //3,4
		{
			t1 = cbCardData[1]; cbCardData[1] = cbCardData[3]; cbCardData[3] = t1;
			t2 = cbCardData[2]; cbCardData[2] = cbCardData[4]; cbCardData[4] = t2; return true;
		}
		//3张和=10
		else if (GetCardValue(cbCardData[2]) + GetCardValue(cbCardData[3]) + GetCardValue(cbCardData[4]) == 10 || 
			GetCardValue(cbCardData[2]) + GetCardValue(cbCardData[3]) + GetCardValue(cbCardData[4]) == 20)		//k,k,9,6,5
		{
			t1 = cbCardData[0]; cbCardData[0] = cbCardData[3]; cbCardData[3] = t1; 
			t2 = cbCardData[1]; cbCardData[1] = cbCardData[4]; cbCardData[4] = t2;  return true;
		}
	}
	else if (bTenCount == 1)//1张牛牌
	{
		BYTE t1,t2;
		//3张等于10	,20		k,2,5,3,7
		if (GetCardValue(cbCardData[1]) + GetCardValue(cbCardData[2]) + GetCardValue(cbCardData[3]) == 10 ||
			GetCardValue(cbCardData[1]) + GetCardValue(cbCardData[2]) + GetCardValue(cbCardData[3]) == 20)		//k,2,5,3,7
		{
			t1 = cbCardData[0]; cbCardData[0] = cbCardData[3]; cbCardData[3] = t1; return true;
		}
		else if (GetCardValue(cbCardData[1]) + GetCardValue(cbCardData[2]) + GetCardValue(cbCardData[4]) == 10 ||
			GetCardValue(cbCardData[1]) + GetCardValue(cbCardData[2]) + GetCardValue(cbCardData[4]) == 20)		//k,2,5,4,3
		{
			t1 = cbCardData[0]; cbCardData[0] = cbCardData[4]; cbCardData[4] = t1; return true;
		}
		else if (GetCardValue(cbCardData[1]) + GetCardValue(cbCardData[3]) + GetCardValue(cbCardData[4]) == 10 ||
			GetCardValue(cbCardData[1]) + GetCardValue(cbCardData[3]) + GetCardValue(cbCardData[4]) == 20)		//k,2,5,4,3
		{
			t1 = cbCardData[0]; cbCardData[0] = cbCardData[3]; cbCardData[3] = t1;
			t2 = cbCardData[2]; cbCardData[2] = cbCardData[4]; cbCardData[4] = t2; return true;
		}
		else if (GetCardValue(cbCardData[2]) + GetCardValue(cbCardData[3]) + GetCardValue(cbCardData[4]) == 10 || 
			GetCardValue(cbCardData[2]) + GetCardValue(cbCardData[3]) + GetCardValue(cbCardData[4]) == 20)		//k,2,5,4,1
		{
			t1 = cbCardData[0]; cbCardData[0] = cbCardData[3]; cbCardData[3] = t1; 
			t2 = cbCardData[1]; cbCardData[1] = cbCardData[4]; cbCardData[4] = t2; 
			return true;
		}
		//2张等于10			k,2,5,8,7
		else if (GetCardValue(cbCardData[1]) + GetCardValue(cbCardData[2]) == 10)
		{
			return true;
		}
		else if(GetCardValue(cbCardData[1]) + GetCardValue(cbCardData[3]) == 10) //k,2,5,8,7
		{
			t1 = cbCardData[2]; cbCardData[2] = cbCardData[3]; cbCardData[3] = t1; return true;
		}
		else if(GetCardValue(cbCardData[1]) + GetCardValue(cbCardData[4]) == 10) //k,2,5,3,8
		{
			t1 = cbCardData[2]; cbCardData[2] = cbCardData[4]; cbCardData[4] = t1; return true;
		}
		else if(GetCardValue(cbCardData[2]) + GetCardValue(cbCardData[3]) == 10) //k,2,5,5,8
		{
			t1 = cbCardData[1]; cbCardData[1] = cbCardData[3]; cbCardData[3] = t1; return true;
		}
		else if(GetCardValue(cbCardData[2]) + GetCardValue(cbCardData[4]) == 10) //k,2,5,5,8
		{
			t1 = cbCardData[1]; cbCardData[1] = cbCardData[4]; cbCardData[4] = t1; return true;
		}
		else if(GetCardValue(cbCardData[3]) + GetCardValue(cbCardData[4]) == 10) //k,2,4,5,5
		{
			t1 = cbCardData[1]; cbCardData[1] = cbCardData[3]; cbCardData[3] = t1; 
			t2 = cbCardData[2]; cbCardData[2] = cbCardData[4]; cbCardData[4] = t2;
			return true;
		}
	}
	//没有牛牌
	else 
	{
		//CString tt;
		//tt.Format("%0x,%0x,%0x,%0x,%0x", cbCardData[0],cbCardData[1],cbCardData[2],cbCardData[3],cbCardData[4]);
		//MessageBox(0,tt,0,0);

		BYTE t1,t2; //1,4,5,6,7
		if(GetCardValue(cbCardData[0]) + GetCardValue(cbCardData[1]) + GetCardValue(cbCardData[2]) == 10 ||
			GetCardValue(cbCardData[0]) + GetCardValue(cbCardData[1]) + GetCardValue(cbCardData[2]) == 20) 
		{
			return true;
		}
		else if (GetCardValue(cbCardData[0]) + GetCardValue(cbCardData[1]) + GetCardValue(cbCardData[3]) == 10 ||
			GetCardValue(cbCardData[0]) + GetCardValue(cbCardData[1]) + GetCardValue(cbCardData[3]) == 20)	 //1,4,2,5,7
		{
			t1 = cbCardData[2]; cbCardData[2] = cbCardData[3]; cbCardData[3] = t1;  return true;
		}
		else if (GetCardValue(cbCardData[0]) + GetCardValue(cbCardData[1]) + GetCardValue(cbCardData[4]) == 10 ||
			GetCardValue(cbCardData[0]) + GetCardValue(cbCardData[1]) + GetCardValue(cbCardData[4]) == 20)	 //1,4,2,2,5
		{
			t1 = cbCardData[2]; cbCardData[2] = cbCardData[4]; cbCardData[4] = t1;  return true;
		}
		else if (GetCardValue(cbCardData[0]) + GetCardValue(cbCardData[2]) + GetCardValue(cbCardData[3]) == 10 ||
			GetCardValue(cbCardData[0]) + GetCardValue(cbCardData[2]) + GetCardValue(cbCardData[3]) == 20)	 //0,2,3
		{
			t1 = cbCardData[1]; cbCardData[1] = cbCardData[3]; cbCardData[3] = t1;  return true;
		}
		else if (GetCardValue(cbCardData[0]) + GetCardValue(cbCardData[2]) + GetCardValue(cbCardData[4]) == 10 || //0,2,4
			GetCardValue(cbCardData[0]) + GetCardValue(cbCardData[2]) + GetCardValue(cbCardData[4]) == 20)	 
		{
			t1 = cbCardData[1]; cbCardData[1] = cbCardData[4]; cbCardData[4] = t1;  return true;
		}
		else if (GetCardValue(cbCardData[0]) + GetCardValue(cbCardData[3]) + GetCardValue(cbCardData[4]) == 10 || //0,3,4
			GetCardValue(cbCardData[0]) + GetCardValue(cbCardData[3]) + GetCardValue(cbCardData[4]) == 20)	 
		{
			t1 = cbCardData[1]; cbCardData[1] = cbCardData[3]; cbCardData[3] = t1;
			t2 = cbCardData[2]; cbCardData[2] = cbCardData[4]; cbCardData[4] = t2;  return true;
		}
		else if (GetCardValue(cbCardData[1]) + GetCardValue(cbCardData[2]) + GetCardValue(cbCardData[3]) == 10 ||
			GetCardValue(cbCardData[1]) + GetCardValue(cbCardData[2]) + GetCardValue(cbCardData[3]) == 20)	 //1,4,2,4,5
		{
			t1 = cbCardData[0]; cbCardData[0] = cbCardData[3]; cbCardData[3] = t1;  return true;
		}
		else if (GetCardValue(cbCardData[1]) + GetCardValue(cbCardData[2]) + GetCardValue(cbCardData[4]) == 10 ||
			GetCardValue(cbCardData[1]) + GetCardValue(cbCardData[2]) + GetCardValue(cbCardData[4]) == 20)	 //1,4,2,4,5
		{
			t1 = cbCardData[0]; cbCardData[0] = cbCardData[4]; cbCardData[4] = t1;  return true;
		}
		else if (GetCardValue(cbCardData[1]) + GetCardValue(cbCardData[3]) + GetCardValue(cbCardData[4]) == 10 ||
			GetCardValue(cbCardData[1]) + GetCardValue(cbCardData[3]) + GetCardValue(cbCardData[4]) == 20)	 //1,4,2,4,5
		{
			t1 = cbCardData[0]; cbCardData[0] = cbCardData[3]; cbCardData[3] = t1;  
			t2 = cbCardData[2]; cbCardData[2] = cbCardData[4]; cbCardData[4] = t2;return true;
		}
		else if (GetCardValue(cbCardData[2]) + GetCardValue(cbCardData[3]) + GetCardValue(cbCardData[4]) == 10 ||
			GetCardValue(cbCardData[2]) + GetCardValue(cbCardData[3]) + GetCardValue(cbCardData[4]) == 20)	  //1,5,2,4,4
		{
			t1 = cbCardData[0]; cbCardData[0] = cbCardData[3]; cbCardData[3] = t1; 
			t2 = cbCardData[1]; cbCardData[1] = cbCardData[4]; cbCardData[4] = t2;  return true;
		}
		return false;
	}
	return false;
}

//获取牛牛：并且进行排序
bool ShGameLogic::GetOxCard(BYTE cbCardData[], BYTE cbCardCount)
{
	ASSERT(cbCardCount==MAX_COUNT);

	//设置变量
	BYTE bTemp[MAX_COUNT],bTempData[MAX_COUNT];
	CopyMemory(bTempData,cbCardData,sizeof(bTempData));
	BYTE bSum=0;
	for (BYTE i=0;i<cbCardCount;i++)
	{
		bTemp[i]=GetCardLogicValue(cbCardData[i]);
		bSum+=bTemp[i];
	}
	//10的数量
	BYTE bTenCount=0;
	for(BYTE i=0;i<cbCardCount;i++)
	{
		if (GetCardValue(cbCardData[i])==10)
		{
			bTenCount++;
		}
	}
	BYTE maxNiuZhi=0;//最大牛值
	BYTE NiuWeiZhi=0;//记录最大牛牌的位置
	BYTE bNiuTemp[30][MAX_COUNT];

	ZeroMemory(bNiuTemp,sizeof(bNiuTemp));

	BYTE NiuShu=0;
	//查找牛牛
	for (BYTE i=0;i<cbCardCount-1;i++)
	{
		for (BYTE j=i+1;j<cbCardCount;j++)
		{
			//如果减去2张牌的值剩下3张是10的倍数：有牛的组合
			if( (bSum-bTemp[i]-bTemp[j])%10==0 ) 
			{
				BYTE bCount=0;
				//存储3张牌
				for (BYTE k=0;k<cbCardCount;k++)
				{
					if(k!=i && k!=j)
					{
						bNiuTemp[NiuShu][bCount++] = bTempData[k];
					}
				}ASSERT(bCount==3);
				//存储另两张牌
				bNiuTemp[NiuShu][bCount++] = bTempData[i];
				bNiuTemp[NiuShu][bCount++] = bTempData[j];
				

				BYTE cbValue=bTemp[i];
				cbValue+=bTemp[j];
				if(cbValue>10)
				{
					cbValue-=10;
				}

				//如果本次的牛值是最大的
				if (cbValue>maxNiuZhi)
				{
					maxNiuZhi = cbValue;//最大牛值
					NiuWeiZhi = NiuShu;//记录最大牛牌的位置
				}
				NiuShu++;
				continue;
			}
		}
	}

	if (NiuShu>0)
	{
		for(BYTE i=0;i<cbCardCount;i++)
		{
			cbCardData[i]=bNiuTemp[NiuWeiZhi][i];
		}

		return true;
	}

	return false;
}

//获取整数
bool ShGameLogic::IsIntValue(BYTE cbCardData[], BYTE cbCardCount)
{
	BYTE sum=0;
	for(BYTE i=0;i<cbCardCount;i++)
	{
		sum+=GetCardLogicValue(cbCardData[i]);
	}
	ASSERT(sum>0);
	return (sum%10==0);
}

//排列扑克：从大到小排列，大王最大
void ShGameLogic::SortCardList(BYTE bCardData[], BYTE bCardCount)
{
	//转换数值
	BYTE bCardValue[5];
	for (BYTE i=0;i<bCardCount;i++)
	{
		bCardValue[i]=GetCardValue(bCardData[i]);
	}

	BYTE bTempData;
	for (BYTE i=0;i<bCardCount;i++)
	{
		for (BYTE j=i;j<bCardCount;j++)
		{
			if ( (bCardValue[i]<bCardValue[j])||
				( (bCardValue[i]==bCardValue[j]) && (bCardData[i]<bCardData[j]) ) )
			{
				//交换位置
				bTempData=bCardData[i];
				bCardData[i]=bCardData[j];
				bCardData[j]=bTempData;
				bTempData=bCardValue[i];
				bCardValue[i]=bCardValue[j];
				bCardValue[j]=bTempData;
			}
		}
	}

	return;
}

//混乱扑克
void ShGameLogic::RandCardList(BYTE cbCardBuffer[], BYTE cbBufferCount)
{
	//CopyMemory(cbCardBuffer,m_cbCardListData,cbBufferCount);
	//混乱准备
	BYTE cbCardData[CountArray(m_cbCardListData)];
	CopyMemory(cbCardData,m_cbCardListData,sizeof(m_cbCardListData));

	//混乱扑克
	BYTE bRandCount=0,bPosition=0;
	do
	{
		bPosition=(BYTE)(rand()%(CountArray(m_cbCardListData)-bRandCount));
		cbCardBuffer[bRandCount++]=cbCardData[bPosition];
		cbCardData[bPosition]=cbCardData[CountArray(m_cbCardListData)-bRandCount];
	} while (bRandCount<cbBufferCount);

	return;
}

//逻辑数值
BYTE ShGameLogic::GetCardLogicValue(BYTE cbCardData)
{
	//扑克属性
	BYTE bCardColor=GetCardColor(cbCardData);
	BYTE bCardValue=GetCardValue(cbCardData);

	//转换数值
	return (bCardValue>10)?(10):bCardValue;
}

//对比扑克
bool ShGameLogic::CompareCard(BYTE cbFirstData[], BYTE cbNextData[], BYTE cbCardCount,bool FirstOX,bool NextOX)
{
	if(FirstOX!=NextOX) return (FirstOX>NextOX);

	//比较牛大小
	if(FirstOX==TRUE)
	{
		//获取点数
		BYTE cbNextType=GetCardType(cbNextData,cbCardCount);
		BYTE cbFirstType=GetCardType(cbFirstData,cbCardCount);

		//点数判断
		if (cbFirstType!=cbNextType)
			return (cbFirstType>cbNextType);
	}

	//排序大小
	BYTE bFirstTemp[MAX_COUNT],bNextTemp[MAX_COUNT];
	CopyMemory(bFirstTemp,cbFirstData,cbCardCount);
	CopyMemory(bNextTemp,cbNextData,cbCardCount);
	SortCardList(bFirstTemp,cbCardCount);
	SortCardList(bNextTemp,cbCardCount);

	//比较数值：排序后第1个最大
	BYTE cbNextMaxValue=GetCardValue(bNextTemp[0]);
	BYTE cbFirstMaxValue=GetCardValue(bFirstTemp[0]);
	if(cbNextMaxValue!=cbFirstMaxValue)
		return cbFirstMaxValue>cbNextMaxValue;

	//比较颜色
	return GetCardColor(bFirstTemp[0]) > GetCardColor(bNextTemp[0]);

	return false;
}

//对比扑克
bool ShGameLogic::CompareCard(BYTE cbFirstData[], BYTE cbNextData[])
{
	//获取点数
	BYTE cbNextType=GetCardType(cbNextData,MAX_COUNT);
	BYTE cbFirstType=GetCardType(cbFirstData,MAX_COUNT);

	//点数判断
	if (cbFirstType!=cbNextType)
		return (cbFirstType>cbNextType);

	//排序大小
	BYTE bFirstTemp[MAX_COUNT],bNextTemp[MAX_COUNT];
	CopyMemory(bFirstTemp,cbFirstData,MAX_COUNT);
	CopyMemory(bNextTemp,cbNextData,MAX_COUNT);
	SortCardList(bFirstTemp,MAX_COUNT);
	SortCardList(bNextTemp,MAX_COUNT);

	//比较数值
	BYTE cbNextMaxValue=GetCardValue(bNextTemp[0]);
	BYTE cbFirstMaxValue=GetCardValue(bFirstTemp[0]);
	if(cbNextMaxValue!=cbFirstMaxValue)
		return cbFirstMaxValue>cbNextMaxValue;

	//比较颜色
	return GetCardColor(bFirstTemp[0]) > GetCardColor(bNextTemp[0]);

	return false;
}