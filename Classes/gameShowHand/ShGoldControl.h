#ifndef _SHGOLDCONTROL_H_
#define _SHGOLDCONTROL_H_

#include "GameScene.h"

class ShGoldControl
{
public:
	ShGoldControl(GameLayer* _layer, int Tga);
	~ShGoldControl(void);

public:
	LONGLONG m_lGold;
	bool m_bmove;
	bool m_bdraw;
	bool m_overHide;
	Vec2 m_Spos;
	Vec2 m_Epos;
	int m_BetTga;
	GameLayer* m_layer;

public:
	void SetGold(LONGLONG lGold);
	void StartMove(Vec2 spos, Vec2 epos, int _kind, bool overHide = false);
	void SetPos(Vec2 spos);
	void SetShow(bool _show);
	LONGLONG GetGold() { return m_lGold; };
	void GoldMoveCallback(Node *pSender,void*data);  //��������
	void moveWithParabola(Sprite*mSprite, Vec2 startPoint ,Vec2 endPoint, float time);
};
#endif
