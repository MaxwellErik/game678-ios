#ifndef CMD_ShowHand_FILE
#define CMD_ShowHand_FILE
#pragma pack(1)

//////////////////////////////////////////////////////////////////////////
//公共宏定义

#define KIND_ID							110									//游戏标识
#define GAME_PLAYER						2									//游戏人数
#define MAX_CARD_COUNT					5									//最大牌数
#define GAME_NAME						TEXT("二人梭哈")						//游戏名字
#define MAX_COUNT						5									//扑克数目

//操作命令
#define		OP_PASS                     0x0001			//不加
#define 	OP_ADD                      0x0010			//加注
#define 	OP_FOLLOW                   0x0100			//跟注
#define 	OP_SUOHA                    0x1000			//梭哈
//加注次数
#define TIMES_COUNT                     1									//次数

//////////////////////////////////////////////////////////////////////////
//命令码定义

#define SUB_C_GIVE_UP					1234								//放弃跟注
#define SUB_C_LOOK_CARD                322									//用户看牌
#define SUB_C_ADD_SCORE					123                                //加注

#define SUB_S_GAME_START				120									//游戏开始
#define SUB_S_ADD_GOLD					121									//加注结果
#define SUB_S_GIVE_UP					122									//放弃跟注
#define SUB_S_SEND_CARD					123									//发牌消息
#define SUB_S_GAME_END					124									//游戏结束
#define SUB_S_SHOW_CARD					125									//玩家看牌
#define SUB_S_GET_RESULT				126									//玩家结果

#define SUB_S_AMDIN_COMMAND				110									//管理员命令

//操作命令
#define		OP_PASS					0x0001			//不加
#define 	OP_ADD					0x0010			//加注
#define 	OP_FOLLOW				0x0100			//跟注
#define 	OP_SUOHA				0x1000			//梭哈


//游戏状态
struct CMD_S_StatusFree
{
    LONGLONG						dwBasicGold;						//基础金币
    
    CHAR							szGameRoomName[32];			//房间名称
};

//游戏状态
struct CMD_S_StatusPlay
{
    BYTE								bShowHand;							//是否梭哈
    BYTE								bAddScore;							//加注标志
  
    LONGLONG							lShowHandScore;						//最大下注
    LONGLONG							lBasicGold;							//基础金币
    LONGLONG							lTurnMaxGold;						//最大下注
    LONGLONG							lTurnBasicGold;						//最少下注
  
    WORD				 				wCurrentUser;						//当前玩家
    BYTE								bPlayStatus[GAME_PLAYER];			//游戏状态
    LONGLONG							lTableGold[2*GAME_PLAYER];			//桌面金币
    
    BYTE								bTableCardCount[GAME_PLAYER];		//扑克数目
    BYTE								bTableCardArray[GAME_PLAYER][5];	//扑克数组
    BYTE								lHandCard;							//几张能梭
    BYTE								lGameType;							//游戏类型
    BYTE								cbUsrCount;
    
    //历史成绩
    LONGLONG							lTurnScore[GAME_PLAYER];			//积分信息
    LONGLONG							lCollectScore[GAME_PLAYER];			//积分信息
    bool                                bCanShowHand;
};

//游戏开始
struct CMD_S_GameStart
{
    LONGLONG							lMaxScore;                          //最大下注
    LONGLONG							lBasicGold;							//单元下注
    LONGLONG							lTurnMaxScore;						//最大下注
    LONGLONG							lTurnBasicGold;						//最少下注
    
    BYTE								lHandCard;							//几张能梭
    BYTE								lGameType;							//游戏类型
    BYTE                                cbUsrCount;
   
    WORD				 				wCurrentUser;						//当前玩家
    BYTE								bFundusCard;						//底牌扑克
    
    BYTE								bCardData[GAME_PLAYER];				//用户扑克
    BYTE								cbHandCardData[GAME_PLAYER][MAX_CARD_COUNT];//玩家牌数据
    bool                                bProSet;
};

//加注结果
struct CMD_S_AddGold
{
    WORD								wCurrentUser;						//当前用户
    WORD								wLastChairID;						//上一用户
    LONGLONG							lLastAddGold;						//加注数目
    LONGLONG							lCurrentLessGold;					//最少加注
    BYTE								cbTimes;
};

//用户放弃
struct CMD_S_GiveUp
{
    WORD								wUserChairID;						//放弃用户
};

//发牌数据包
struct CMD_R_SendCard
{
    WORD								wCurrentUser;						//当前用户
    WORD								wLastMostUser;						//上次最大用户
    LONGLONG							lTurnMaxScore;						//最大下注
    LONGLONG							lTurnLessScore;						//最小下注
    BYTE								cbSendCardCount;					//发牌数目
    BYTE								bUserCard[GAME_PLAYER][2];			//用户扑克
};

//游戏结束
struct CMD_S_GameEnd
{
    LONGLONG							lTax[GAME_PLAYER];					//游戏税收
    LONGLONG							lGameGold[GAME_PLAYER];				//游戏得分
    BYTE								bUserCard[GAME_PLAYER];				//用户扑克
};



//积分配置
struct CMD_S_ScoreOption
{
    //状态信息
    LONGLONG								lUserCount;							//用户数目
    LONGLONG								lBlackCount;						//用户数目
    
    //积分信息
    LONGLONG								lMaxScore;							//最大变化
    LONGLONG								lMaxWinScore;						//最大赢分
    LONGLONG								lMaxLoseScore;						//最大输分
};
//看牌
struct CMD_S_LookCard
{
    UINT32                                    dwUserID;
};

struct CMD_S_KongZhi
{
    BYTE cbCard;
    BYTE cbIndex;
};

//////////////////////////////////////////////////////////////////////////
//客户端命令结构

#define SUB_C_GET_OPTION				650									//获取配置
#define SUB_C_SET_MAX_SCORE				651									//设置积分
#define SUB_C_SET_MAX_WIN_SCORE			652									//设置积分
#define SUB_C_SET_MAX_LOSE_SCORE		653									//设置积分

#define SUB_C_SET_BLACKLIST				700									//设置用户
#define SUB_C_REMOVE_BLACKLIST			701									//删除用户

#define SUB_C_KONGZHI_1                 661
#define SUB_C_KONGZHI_2                 662


struct CMD_S_ShowHand
{
    WORD                wAddUser;                   //加注用户
    LONGLONG             lScore;                    //加注数目
    WORD                 wCurrentUser;              //当前玩家
    UINT32                dwOperator;               //操作类型
};

//用户加注
struct SH_CMD_C_AddScore
{
    LONGLONG                                nServriValue;
    LONGLONG								lScore;								//加注数目
    BYTE								    cbTimes;							//倍数
};

//获取胜者
struct CMD_C_GetWinner
{
    int								dwUserID;							//用户标识
};

//积分信息
struct CMD_C_ScoreInfo
{
    LONGLONG                        lScore;								//积分信息
};

//用户信息
struct CMD_C_UserIDInfo
{
    int								dwUserID;							//用户标识
};

//看牌
struct CMD_C_LookCard
{
    DWORD							dwUserID;
};
struct CMD_C_KongZhi
{
    BYTE cbCard; 
    LONGLONG nServalValue;
};

#pragma pack()
#endif
