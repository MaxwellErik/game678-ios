#include "ShCardControl.h"
#include "ShGameLogic.h"
#include "ShGameViewLayer.h"

//构造函数
ShCardControl::ShCardControl(GameLayer* _layer,int tga, bool IsMy)
{
	m_lookState = 0;
	m_OtherlookState = 0;
	m_FirstCard = 0;
	
	m_ShowBackCard = false;
	m_CardTga[0] = tga+1;
	m_CardTga[1] = tga+2;
	m_CardTga[2] = tga+3;
	m_CardTga[3] = tga+4;
	m_CardTga[4] = tga+5;
	m_CardTga[5] = tga+6;
	m_IsMy = IsMy;
	m_layer = _layer;
}

//析构函数
ShCardControl::~ShCardControl()
{
}

void ShCardControl::ClearCardData()
{
	m_CardData.clear();
	for (int i = 0; i < 6; i++)
	{
		m_layer->removeAllChildByTag(m_CardTga[i]);
	}
	
}

void ShCardControl::GiveUp()
{
	for (int i = 0; i < m_CardData.size(); i++)
	{
		m_CardData[i] = 0;
	}
	//清理数据
	for (int i = 0; i < m_CardData.size(); i++)
	{
		m_layer->removeAllChildByTag(m_CardTga[i]);
	}

	//根据数据创建对象
	for (int i = 0; i < m_CardData.size(); i++)
	{
		string _name = GetCardStringName(0);
		Sprite* temp = Sprite::createWithSpriteFrameName(_name.c_str());
		if(m_IsMy)
        {
            temp->setPosition(Vec2(_STANDARD_SCREEN_CENTER_.x-100+i*60,150));
        }
        else
            temp->setPosition(Vec2(_STANDARD_SCREEN_CENTER_.x-100+i*60,900));
		
		temp->setTag(m_CardTga[i]);		
		m_layer->addChild(temp);
	}
}

void ShCardControl::LookCard(bool _ShowFirstCard)
{
	for (int i = 0; i < m_CardData.size(); i++)
	{
		m_layer->removeAllChildByTag(m_CardTga[i]);
	}
	//根据数据创建对象
	for (int i = 0; i < m_CardData.size(); i++)
	{
		string _name;
		if (i == 0)
		{
			if (_ShowFirstCard)
                _name = GetCardStringName(m_CardData[i]); //显示第一张
			else
                _name = GetCardStringName(0); //不显示
		}
		else 
		{
			_name = GetCardStringName(m_CardData[i]); //显示第一张
		}
        
		Sprite* temp = Sprite::createWithSpriteFrameName(_name.c_str());
		if(m_IsMy)
            temp->setPosition(Vec2(_STANDARD_SCREEN_CENTER_.x- 100 + i*60,150));
		else
            temp->setPosition(Vec2(_STANDARD_SCREEN_CENTER_.x - 100 + i*60,900));
		temp->setTag(m_CardTga[i]);		
		m_layer->addChild(temp);
	}

	Node * cc = m_layer->getChildByTag(m_CardTga[0]);
	if (cc == NULL) return;
	if (m_IsMy)
	{
//		ActionInterval *action = MoveTo::create(0.5f, Vec2(_STANDARD_SCREEN_CENTER_.x-200,180));
//		ActionInterval *action_back = MoveTo::create(0.5f, Vec2(_STANDARD_SCREEN_CENTER_.x-100,150));
        DelayTime *delaytime = DelayTime::create(2.0f);
		ActionInstant *func = CCCallFuncND::create(m_layer,callfuncND_selector(ShGameViewLayer::CardMoveEnd1),(void*)0);
		ActionInterval *act = Sequence::create(delaytime, func, NULL);
		cc->runAction(act);
	}
	else
	{
		ActionInterval *action = MoveTo::create(0.5f, Vec2(_STANDARD_SCREEN_CENTER_.x-200,930));
		ActionInterval *action_back = MoveTo::create(0.5f, Vec2(_STANDARD_SCREEN_CENTER_.x-100,900));
		ActionInstant *func = CCCallFuncND::create(m_layer,callfuncND_selector(ShGameViewLayer::CardMoveEnd),(void*)0);
		ActionInterval *act = Sequence::create(action, action_back, func, NULL);
		cc->runAction(act);
	}

}

void ShCardControl::CardMoveEnd(Node *pSender,void*data)
{
	for (int i = 0; i < m_CardData.size(); i++)
	{
		m_layer->removeAllChildByTag(m_CardTga[i]);
	}
	//根据数据创建对象
	for (int i = 0; i < m_CardData.size(); i++)
	{
		string _name;
		if (i == 0)
		{
			_name = GetCardStringName(0); //不显示
		}
		else 
		{
			_name = GetCardStringName(m_CardData[i]); //显示第一张
		}
		Sprite* temp = Sprite::createWithSpriteFrameName(_name.c_str());
		if(m_IsMy)
        {
            temp->setPosition(Vec2(_STANDARD_SCREEN_CENTER_.x - 100 + i*60, 150));
        }
		else
            temp->setPosition(Vec2(_STANDARD_SCREEN_CENTER_.x - 100 + i*60,900));

		temp->setTag(m_CardTga[i]);		
		m_layer->addChild(temp);
	}
}

int ShCardControl::GetCardCount()
{
	int r = 0;
	for (int i = 0; i < m_CardData.size(); i++)
	{
		if(m_CardData[i] != 0) r++;
	}
	return r;
}

void ShCardControl::SetCardData(BYTE bCardData[], DWORD dwCardCount)
{
	//设置数据
	for (int i = 0; i < dwCardCount; i++)
	{
		m_CardData.push_back(bCardData[i]);
	}

	//清理数据
	for (int i = 0; i < m_CardData.size(); i++)
	{
		m_layer->removeAllChildByTag(m_CardTga[i]);
	}

	//根据数据创建对象
	for (int i = 0; i < m_CardData.size(); i++)
	{
		string _name;
		if (i == 0 && m_ShowBackCard == false) _name = GetCardStringName(0);
		else _name = GetCardStringName(m_CardData[i]);
		Sprite* temp = Sprite::createWithSpriteFrameName(_name.c_str());
		if(m_IsMy)
        {
            temp->setPosition(Vec2(_STANDARD_SCREEN_CENTER_.x - 100 + i*60,150));
        }
		else
        {
            temp->setPosition(Vec2(_STANDARD_SCREEN_CENTER_.x - 100 + i*60,900));
//            temp->setScale(0.7f);
        }
        temp->setTag(m_CardTga[i]);
		m_layer->addChild(temp);
	}
}

void ShCardControl::SetCardType(BYTE cardtype0)
{
	if (cardtype0 <= 0 || cardtype0 >= 9) return;
	char _valstr[32];
	ZeroMemory(_valstr,32);
	sprintf(_valstr,"Type%d.png",cardtype0);
	Sprite* temp = Sprite::createWithSpriteFrameName(_valstr);
	if(m_IsMy)
        temp->setPosition(Vec2(660,235));
	else
        temp->setPosition(Vec2(660,640));
	temp->setTag(m_CardTga[5]);
	m_layer->addChild(temp,100);
}

void ShCardControl::SetFirstCard(BYTE _card)
{
	if(m_CardData.size() > 0) m_CardData[0] = _card;
	else m_CardData.push_back(_card);
}

void ShCardControl::ShowFirstCard(bool _show)
{
	m_ShowBackCard = _show;
	//清理数据
	for (int i = 0; i < m_CardData.size(); i++)
	{
		m_layer->removeAllChildByTag(m_CardTga[i]);
	}
	//根据数据创建对象
	for (int i = 0; i < m_CardData.size(); i++)
	{
		string _name;
		if (i == 0 && m_ShowBackCard == false) _name = GetCardStringName(0);
		else _name = GetCardStringName(m_CardData[i]);
		Sprite* temp = Sprite::createWithSpriteFrameName(_name.c_str());
		if(m_IsMy)
            temp->setPosition(Vec2(_STANDARD_SCREEN_CENTER_.x - 100 + i*60,150));
		else
            temp->setPosition(Vec2(_STANDARD_SCREEN_CENTER_.x - 100 + i*60,900));
	
		temp->setTag(m_CardTga[i]);		
		m_layer->addChild(temp);
	}
}

void ShCardControl::SetPos(int x, int y)
{
	m_CardCenterPos = Vec2(x, y);
}

//获取扑克
WORD ShCardControl::GetCardData(BYTE cbCardData[], WORD wBufferCount)
{
	if (wBufferCount == 0)	return m_CardData.size();

	//效验参数
	WORD wCardCount = wBufferCount < m_CardData.size()? wBufferCount:m_CardData.size();

	//拷贝扑克
	for( WORD i = 0; i < wCardCount; i++ )
		cbCardData[i] = m_CardData[i];
	return wCardCount;
}

string ShCardControl::GetCardStringName(BYTE card)
{
	if (card == 0)
	{
		return "ShowhandCardBack.png";
	}
	char tt[32];
	sprintf(tt,"%0x",card);
	BYTE _value = m_GameLogic.GetCardValue(card);
	BYTE _color = m_GameLogic.GetCardColor(card);
	string temp;
	if (_color == 0) temp = "fangkuai_";
	if (_color == 1) temp = "meihua_";
	if (_color == 2) temp = "hongtao_";
	if (_color == 3) temp = "heitao_";
	char _valstr[32];
	ZeroMemory(_valstr,32);
	if (_value == 14) //小王
	{
		sprintf(_valstr,"xiaowang.png");
	}
	else if (_value == 15)
	{
		sprintf(_valstr,"dawang.png");
	}
	else if (_value < 10)
	{
		sprintf(_valstr,"0%d.png",_value);
	}
	else 
	{
		sprintf(_valstr,"%d.png",_value);
	}

	temp+=_valstr;
	return temp;
}


bool ShCardControl::onTouchBegan(Vec2 pos)
{
    Node* temp = m_layer->getChildByTag(m_CardTga[0]);
    if (temp != NULL)
    {
        if (temp->getBoundingBox().containsPoint(pos))
        {
            LookCard(true);
        }
    }
    return true;
}
