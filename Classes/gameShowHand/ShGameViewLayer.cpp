﻿#include "ShGameViewLayer.h"
#include "LobbyLayer.h"
#include "ClientSocketSink.h"
#include "HXmlParse.h"
#include "LocalDataUtil.h"
#include "JniSink.h"
#include "VisibleRect.h"
#include "HttpConstant.h"
#include "Screen.h"
#include "LobbySocketSink.h"
#include "LoginLayer.h"
#include "ShGoldControl.h"
#include "ShCardControl.h"
#include "CMD_ShowHand.h"
#include "GameLayerMove.h"
#include "GameSetingLayer.h"
#include "Convert.h"

#define		MENU_INIT_POINT			(Vec2Make(-_STANDARD_SCREEN_CENTER_.x+68+30 , _STANDARD_SCREEN_CENTER_.y-68-20))
#define		ADV_SIZE				(CCSizeMake(520,105))
#define		BOX_POINT				(ccpx(1195,70))
#define		MIN_PROP_PAY_SCORE			1
#define		ALL_ANIMATION_COUNT			5
#define		MAX_CREDIT					(9999999)
#define		MAX_BET						(9999)
#define		MAX_ADV_COUNT				(10)
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
#define		BIG_AWARD_MULITY			10
#else
#define		BIG_AWARD_MULITY			30
#endif // _DEBUG
#define MYSELF_VIEW_ID 1 

#define Btn_Ready						1
#define Btn_BackToLobby					2
#define Btn_Seting						4
#define Btn_ShowHand					500
#define Btn_Follow						501
#define Btn_NotAdd						502
#define Btn_GiveUp						503
#define Btn_BtnBack1					504
#define Btn_BtnBack2					505
#define Btn_BtnBack3					506
#define Btn_BtnLook						507
#define Btn_GetScoreBtn					508


ShGameViewLayer::ShGameViewLayer(GameScene *pGameScene)
	:IGameView(pGameScene)
{
	m_GameState=enGameNormal;
	m_BtnReadyPlay = NULL;
	m_GameTime = 0;

	m_lTurnMaxGold=0L;
	m_lFollowGold=0L;
	m_lBasicGold = 0L;
	memset(m_bPlayStatus,0,sizeof(m_bPlayStatus));
	m_lShowHandScore = 0L;
	ZeroMemory( m_lUserScore,sizeof(m_lUserScore));

	m_lGoldShow=0L;
	m_bShowHand=false;
	memset(m_szName,0,sizeof(m_szName));
	ZeroMemory(&m_GameEnd,sizeof(m_GameEnd));
	m_GiveUp = false;
	m_wCardCount = 0;
	m_bUserShowHand[0] = false;
	m_bUserShowHand[1] = false;
    m_isLookCard = false;
	m_bFirstBet = false;
	m_bAddScore=false;
	m_CurBet = 0;
	SetKindId(KIND_ID);
}

ShGameViewLayer::~ShGameViewLayer() 
{
}

ShGameViewLayer *ShGameViewLayer::create(GameScene *pGameScene)
{
    ShGameViewLayer *pLayer = new ShGameViewLayer(pGameScene);
    if(pLayer && pLayer->init())
    {
        pLayer->autorelease();
        return pLayer;
    }
    else
    {
        CC_SAFE_DELETE(pLayer);
        return NULL;
    }
}

bool ShGameViewLayer::init()
{
	if ( !Layer::init() )
	{
		return false;
	}

	setLocalZOrder(3);
	Tools::addSpriteFrame("ShowHand/GameBack.plist");
	Tools::addSpriteFrame("Common/CardSprite2.plist");
	SoundUtil::sharedEngine()->playBackMusic("ShowHand/back_showhand", true);
    SoundUtil::sharedEngine()->setBackSoundVolume(g_GlobalUnits.m_fBackMusicValue);
    SoundUtil::sharedEngine()->setSoundVolume(g_GlobalUnits.m_fSoundValue);
	IGameView::onEnter();
	JniSink::share()->setIGameView(this);
	setGameStatus(GS_FREE);

	InitGame();
	AddButton();

	AddPlayerInfo();

	setTouchEnabled(true);
	auto listener = EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(true);
	listener->onTouchBegan = CC_CALLBACK_2(ShGameViewLayer::onTouchBegan, this);
	listener->onTouchMoved = CC_CALLBACK_2(ShGameViewLayer::onTouchMoved, this);
	listener->onTouchEnded = CC_CALLBACK_2(ShGameViewLayer::onTouchEnded, this);
	_eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);

	return true;
}

void ShGameViewLayer::onEnter()
{

}

void ShGameViewLayer::StartTime(int _time, int chair)
{
	m_GameTime = _time;
	schedule(schedule_selector(ShGameViewLayer::UpdateTime), 1);
	m_ClockSpr->setVisible(true);
	m_ClockSpr->setPosition(m_TimePos[chair]);
	UpdateTime(m_GameTime);
}

void ShGameViewLayer::StopTime()
{
	unschedule(schedule_selector(ShGameViewLayer::UpdateTime));
	m_ClockSpr->removeAllChildren();
	m_ClockSpr->setVisible(false);
}

void ShGameViewLayer::UpdateTime(float fp)
{
	if (m_GameTime <= 0)
	{
		unschedule(schedule_selector(ShGameViewLayer::UpdateTime));
		if (m_GameState == enGameNormal || m_GameState == enGameEnd)
		{
			GameLayerMove::sharedGameLayerMoveSink()->CloseSeting();
			RemoveAlertMessageLayer();
			SoundUtil::sharedEngine()->stopAllEffects();
			SoundUtil::sharedEngine()->stopBackMusic();
			ClientSocketSink::sharedSocketSink()->LeftGameReq();
			GameLayerMove::sharedGameLayerMoveSink()->CloseGetScore();
			ClientSocketSink::sharedSocketSink()->setFrameGameView(NULL);
			GameLayerMove::sharedGameLayerMoveSink()->GoGameToTable();
		}
		else if (m_GameState)
		{
			OnGiveUp();
			m_bFirstBet = false;
		}
		return;
	}
	m_ClockSpr->removeAllChildren();
    
    std::string str = StringUtils::toString(m_GameTime);
    if (m_GameTime < 10)
        str = "0" + str;
    LabelAtlas *time = LabelAtlas::create(str, "Common/time.png", 33, 50, '+');
    time->setPosition(Vec2(22, 15));
    m_ClockSpr->addChild(time);
	m_GameTime--;
}

void ShGameViewLayer::onExit()
{
	Tools::removeSpriteFrameCache("ShowHand/GameBack.plist");
	Tools::removeSpriteFrameCache("Common/CardSprite2.plist");
	IGameView::onExit();
}

void ShGameViewLayer::UpdateLookCard(float dt)
{
    m_isLookCard = false;
}

// Touch 触发
bool ShGameViewLayer::onTouchBegan(Touch *pTouch, Event *pEvent)
{
    if (m_isLookCard || m_GameState == enGameNormal || m_GameState == enGameEnd)
        return true;
    
    Vec2 touchLocation = pTouch->getLocation(); // 返回GL坐标
    Vec2 localPos = convertToNodeSpace(touchLocation);
    if(m_CardControl[1]->onTouchBegan(localPos))
    {
        m_isLookCard = true;
        scheduleOnce(schedule_selector(ShGameViewLayer::UpdateLookCard), 2.0f);
        LookCard();
    }
    return true;
}

void ShGameViewLayer::onTouchMoved(Touch *pTouch, Event *pEvent)
{
}

void ShGameViewLayer::onTouchEnded(Touch*pTouch, Event*pEvent)
{
}

void ShGameViewLayer::keyboardWillHide(IMEKeyboardNotificationInfo& info)
{
	MoveTo *pMovAction = MoveTo::create(0.15f, Vec2(0,0));
	Action *pActSeq = Sequence::create(pMovAction, NULL);
	runAction(pActSeq);
}

void ShGameViewLayer::keyboardDidHide(IMEKeyboardNotificationInfo& info)
{
}

// Alert Message 确认消息处理
void ShGameViewLayer::DialogConfirm(Ref *pSender)
{
	this->RemoveAlertMessageLayer();
	this->SetAllTouchEnabled(true);
}

// Alert Message 取消消息处理
void ShGameViewLayer::DialogCancel(Ref *pSender)
{
	this->RemoveAlertMessageLayer();
	this->SetAllTouchEnabled(true);
}

void ShGameViewLayer::OnEventUserScore( tagUserData * pUserData, WORD wChairID, bool bLookonUser )
{
	IGameView::OnEventUserScore(pUserData, wChairID, bLookonUser);
    AddPlayerInfo();
}

void ShGameViewLayer::OnEventUserStatus(tagUserData * pUserData, WORD wChairID, bool bLookonUser)
{
	IGameView::OnEventUserStatus(pUserData, wChairID, bLookonUser);
	AddPlayerInfo();
}

void ShGameViewLayer::OnEventUserEnter( tagUserData * pUserData, WORD wChairID, bool bLookonUser )
{
	AddPlayerInfo();
}

void ShGameViewLayer::OnEventUserLeave( tagUserData * pUserData, WORD wChairID, bool bLookonUser )
{
	AddPlayerInfo();
}

//游戏消息
bool ShGameViewLayer::OnGameMessage(WORD wSubCmdID, const void * pBuffer, WORD wDataSize)
{
	CCLOG("RECEIVE MESSAGE CMD[%d]" , wSubCmdID);
	switch (wSubCmdID)
	{
	case SUB_S_GAME_START:	//游戏开始
		return OnSubGameStart(pBuffer, wDataSize);

	case SUB_S_ADD_GOLD:	//用户下注
			return OnSubAddScore(pBuffer,wDataSize);

	case SUB_S_GIVE_UP:		//用户放弃
			return OnSubGiveUp(pBuffer,wDataSize);

	case SUB_S_SEND_CARD:	//发牌消息
			return OnSubSendCard(pBuffer, wDataSize);

	case SUB_S_SHOW_CARD:    //看牌
			return OnSubLookCard(pBuffer,wDataSize);

	case SUB_S_GAME_END:	//游戏结束
			return OnSubGameEnd(pBuffer, wDataSize);
	}
	return true;
}

//场景消息
bool ShGameViewLayer::OnGameSceneMessage(BYTE cbGameStatus, const void * pBuffer, WORD wDataSize)
{
	setGameStatus(cbGameStatus);
	
	switch (getGameStatus())
	{
	case GS_FREE:
		{
			if (m_BtnReadyPlay != NULL)	m_BtnReadyPlay->setVisible(true);
			return true;
		}
	case GS_PLAYING:
		{
			if (m_BtnReadyPlay != NULL)	m_BtnReadyPlay->setVisible(false);
			//效验数据
			if (wDataSize!=sizeof(CMD_S_StatusPlay)) return false;
			CMD_S_StatusPlay * pStatusPlay=(CMD_S_StatusPlay *)pBuffer;

			//下注信息
			m_lTurnMaxGold=pStatusPlay->lTurnMaxGold;
			m_lFollowGold=pStatusPlay->lTurnBasicGold - pStatusPlay->lTableGold[((GetMeChairID()+1)%2)*2+1];;
			m_lBasicGold = pStatusPlay->lBasicGold;
			m_lShowHandScore=pStatusPlay->lShowHandScore;

            //状态信息
			m_wCurrentUser=pStatusPlay->wCurrentUser;
			m_bShowHand=(pStatusPlay->bShowHand==TRUE)?true:false;
			CopyMemory(m_bPlayStatus,pStatusPlay->bPlayStatus,sizeof(m_bPlayStatus));

			m_bAddScore=pStatusPlay->bAddScore;
			SetGoldTitleInfo(m_lTurnMaxGold,m_lBasicGold);

			m_wCardCount=pStatusPlay->bTableCardCount[GetMeChairID()];

			//设置界面
			LONGLONG lTableScore=0L;
			for (WORD i=0;i<GAME_PLAYER;i++)
			{
				//设置位置
				WORD wViewChairID= g_GlobalUnits.SwitchViewChairID(i);

				//设置扑克
				if (m_bPlayStatus[i]==TRUE) 
				{
					BYTE cbCardCount=pStatusPlay->bTableCardCount[i];
					m_CardControl[wViewChairID]->SetCardData(pStatusPlay->bTableCardArray[i],cbCardCount);
				}
				lTableScore += pStatusPlay->lTableGold[2*i+1];
				//设置下注
				SetUserGoldInfoNew(wViewChairID,true,pStatusPlay->lTableGold[i]);
			}
			m_CardControl[0]->ClearCardData();
			m_CardControl[1]->ClearCardData();

			//设置界面
			for (WORD i=0;i<GAME_PLAYER;i++)
			{
				//变量定义
				WORD wViewChairID= g_GlobalUnits.SwitchViewChairID(i);
				//设置扑克
				if (m_bPlayStatus[i]==FALSE)
				{
					BYTE cbCardData[MAX_COUNT];
					ZeroMemory(cbCardData,sizeof(cbCardData));
					m_CardControl[wViewChairID]->SetCardData(cbCardData,pStatusPlay->bTableCardCount[i]);
				}
				else
				{
					BYTE cbCardCount=pStatusPlay->bTableCardCount[i];
					m_CardControl[wViewChairID]->SetCardData(pStatusPlay->bTableCardArray[i],cbCardCount);
				}
			}

			//当前用户
			if (m_wCurrentUser==GetMeChairID()) UpdateScoreControl();

			//设置时间
			StartTime(20);
			return true;
		}
	}	
	return true;
}

void ShGameViewLayer::SendGameStart()
{
	ClientSocketSink::sharedSocketSink()->SendData(MDM_GF_FRAME, SUB_GF_USER_READY);
	if (m_BtnReadyPlay != NULL)
	{
		m_BtnReadyPlay->setVisible(false);
	}
    m_lTotalGold = 0;
	StopTime();
}

//财富标题
void ShGameViewLayer::SetGoldTitleInfo(LONGLONG lMaxGold, LONGLONG lBasicGold)
{
	//设置变量
	m_lBasicGold=lBasicGold;
	Node* Btnback = getChildByTag(m_BtnBackTga);
	Btnback->removeChildByTag(m_FontScoreBtn[0]);
	Btnback->removeChildByTag(m_FontScoreBtn[1]);
	Btnback->removeChildByTag(m_FontScoreBtn[2]);

	//筹码1
	char strc[32]="";
	sprintf(strc,"%lld", m_lBasicGold);
	LabelAtlas* mGoldFont = LabelAtlas::create(strc, "Common/gold.png", 23, 32, '+');
    mGoldFont->setAnchorPoint(Vec2(0.5f, 0.5f));
	mGoldFont->setPosition(Vec2(150,260));
	mGoldFont->setTag(m_FontScoreBtn[0]);
	Btnback->addChild(mGoldFont);

	//筹码2
	sprintf(strc,"%lld", m_lBasicGold*2);
	mGoldFont = LabelAtlas::create(strc, "Common/gold.png", 23, 32, '+');
    mGoldFont->setAnchorPoint(Vec2(0.5f, 0.5f));
    mGoldFont->setPosition(Vec2(150,160));
	mGoldFont->setTag(m_FontScoreBtn[1]);
	Btnback->addChild(mGoldFont);

	//筹码3
	sprintf(strc,"%lld", m_lBasicGold*4);
	mGoldFont = LabelAtlas::create(strc, "Common/gold.png", 23, 32, '+');
    mGoldFont->setAnchorPoint(Vec2(0.5f, 0.5f));
    mGoldFont->setPosition(Vec2(150,60));
	mGoldFont->setTag(m_FontScoreBtn[2]);
	Btnback->addChild(mGoldFont);

	return;
}

void ShGameViewLayer::SetUserGold( WORD wChairId, LONGLONG lGold)
{
    Node * bg = getChildByTag(m_HeadTga[wChairId]);
    if (bg == nullptr)
        return;
    
    Label *goldLabel = (Label*)bg->getChildByTag(m_HeadGlodTga[wChairId]);
    if (goldLabel)
    {
        char strc[32]="";
        memset(strc , 0 , sizeof(strc));
        Tools::AddComma(lGold , strc);
        goldLabel->setString(strc);
    }
}


void ShGameViewLayer::SetUserGoldInfoNew(WORD wViewChairID, bool bTableGold, LONGLONG dwGold)
{
	if (bTableGold) //设置个人总筹码
	{
		m_AllGoldControl[wViewChairID]->SetGold(dwGold);
		m_AllGoldControl[wViewChairID]->SetShow(true);
		if(wViewChairID == 0)
        {
            m_AllGoldControl[0]->SetPos(m_AllBetPos[0]);
        }
		else if(wViewChairID == 1)
        {
            m_AllGoldControl[1]->SetPos(m_AllBetPos[1]);
        }
         m_lTotalGold = m_AllGoldControl[0]->GetGold() + m_AllGoldControl[1]->GetGold();
	}
	else
	{
		if(wViewChairID == 0)
		{
			m_TableGoldControl[wViewChairID]->SetGold(dwGold);
			m_TableGoldControl[wViewChairID]->StartMove(m_GoldStartPos[0],m_TableBetPos[wViewChairID],0);
		}
		else if (wViewChairID == 1)
		{
			m_TableGoldControl[wViewChairID]->SetGold(dwGold);
			m_TableGoldControl[wViewChairID]->StartMove(m_GoldStartPos[1],m_TableBetPos[wViewChairID],1);
		}
	}
	UpdataAllScore();
}

void ShGameViewLayer::SendCard(float dt)
{
	//派发扑克
	if(m_SendCardIndex >= m_TempCardCount)
	{
		unschedule(schedule_selector(ShGameViewLayer::SendCard));
		return;
	}
	SoundUtil::sharedEngine()->playEffect("SEND_CARD");
	for (int i = 0; i< 2; i++)
	{
		dispatchCards(i,m_TempCardData[i][m_SendCardIndex]); 
	}
	m_SendCardIndex++;
}

void ShGameViewLayer::dispatchCards(WORD chair, BYTE card) 
{ 
	switch (chair) 
	{ 
	case 0:  //正上方
		{ 
			m_PlayerHandCardData[0][m_UpCardIndex++] = card;
			MoveTo *leftMoveBy = MoveTo::create(0.2f, Vec2(_STANDARD_SCREEN_CENTER_.x - 100 + m_OtherCardCount++*60, 900));
			ActionInstant *func = CallFunc::create(CC_CALLBACK_0(ShGameViewLayer::CardMoveCallbackEx, this));
			Sprite* temp = Sprite::createWithSpriteFrameName("ShowhandCardBack.png");
			temp->setTag(m_TempSendCardTga[0]);
			addChild(temp);
			temp->setPosition(Vec2(_STANDARD_SCREEN_CENTER_.x,_STANDARD_SCREEN_CENTER_.y));
			temp->runAction(Sequence::create(leftMoveBy,func,NULL));

			break; 
		} 
	case 1: 
		{
			m_PlayerHandCardData[1][m_DownCardIndex++] = card;
            MoveTo *leftMoveBy = MoveTo::create(0.2f, Vec2(_STANDARD_SCREEN_CENTER_.x - 100 + m_MyCardCount++*60, 150));
			ActionInstant *func = CallFunc::create(CC_CALLBACK_0(ShGameViewLayer::CardMoveCallback, this));
			Sprite* temp = Sprite::createWithSpriteFrameName("ShowhandCardBack.png");
			temp->setTag(m_TempSendCardTga[1]);
			addChild(temp);
			temp->setPosition(Vec2(_STANDARD_SCREEN_CENTER_.x,_STANDARD_SCREEN_CENTER_.y));
			temp->runAction(Sequence::create(leftMoveBy,func,NULL));
			break;
		} 
	default: 
		break; 
	} 
} 

void ShGameViewLayer::CardMoveCallback()
{
	removeChildByTag(m_TempSendCardTga[1]);
	m_CardControl[1]->SetCardData(&m_PlayerHandCardData[1][m_PlayerHandIndex[1]],1);
	m_PlayerHandIndex[1]++;
}

void ShGameViewLayer::CardMoveCallbackEx()
{
	removeChildByTag(m_TempSendCardTga[0]);
	m_CardControl[0]->SetCardData(&m_PlayerHandCardData[0][m_PlayerHandIndex[0]],1);
	m_PlayerHandIndex[0]++;
}

bool ShGameViewLayer::OnSubGameStart( const void * pBuffer, WORD wDataSize )
{
	//效验数据
	if (wDataSize!=sizeof(CMD_S_GameStart)) return false;
	CMD_S_GameStart * pGameStart=(CMD_S_GameStart *)pBuffer;

	SoundUtil::sharedEngine()->playEffect("GAME_START");
    
	m_GameState = enGameCallBank;
	//游戏变量
    m_SendCardCount = 2;
	m_lTurnMaxGold = pGameStart->lTurnMaxScore;
	m_lFollowGold = pGameStart->lBasicGold*2;
	m_lBasicGold = pGameStart->lBasicGold;
    
	removeAllChildByTag(m_EndScore[0]);
	removeAllChildByTag(m_EndScore[1]);
	memset(m_szName,0,sizeof(m_szName));
	memset(m_bPlayStatus,0,sizeof(m_bPlayStatus));
	m_wCurrentUser = pGameStart->wCurrentUser;
	m_lShowHandScore = pGameStart->lMaxScore;
	m_GiveUp = false;
    m_lTotalGold = m_lFollowGold * 2;
	
    //辅助变量
	m_lGoldShow=0L;
	m_bShowHand=false;
	m_MyCardCount = 0;
	m_OtherCardCount = 0;
    m_CurBet = 0;
	m_bAddScore=false;

	//变量定义
	SetGoldTitleInfo(pGameStart->lTurnMaxScore, m_lBasicGold);
	
    //设置状态
	setGameStatus(GS_PLAYING);

	for (WORD i=0;i<GAME_PLAYER;i++)
	{
		//变量定义
		WORD wViewChairID=g_GlobalUnits.SwitchViewChairID(i);
		int wtable = ClientSocketSink::sharedSocketSink()->GetMeUserData()->wTableID;
		const tagUserData* pUserData = ClientSocketSink::sharedSocketSink()->m_UserManager.GetUserData(wtable,i);
		//设置界面
		if (pUserData!=NULL)
		{
			m_bPlayStatus[i]=TRUE;
            m_lUserScore[i] = pUserData->lScore;

			lstrcpyn(m_szName[i],pUserData->szName,CountArray(m_szName[i]));
            
//			SetUserGold( wViewChairID, m_lUserScore[i]- pGameStart->lBasicGold*2);
			SetUserGoldInfoNew(wViewChairID,false,0L);
            SetUserGoldInfoNew(wViewChairID,true,pGameStart->lBasicGold*2);
		}
		else
		{
			m_bPlayStatus[i]=FALSE;
			m_lUserScore[i] = 0L;
		}

		//设置控件
		m_CardControl[wViewChairID]->ClearCardData();
		m_CardControl[wViewChairID]->ShowFirstCard(false);
        m_lCurTurnScore[i]=0;
	}
    
	//派发扑克
	m_CardControl[0]->ShowFirstCard(false);
	m_CardControl[1]->ShowFirstCard(false);
	m_CardControl[0]->m_CardData.clear();
	m_CardControl[1]->m_CardData.clear();

	for (BYTE cbIndex=0;cbIndex<2;cbIndex++)
	{
		for (WORD i=0;i<GAME_PLAYER;i++)
		{
			if (m_bPlayStatus[i]==TRUE)
			{
				WORD wViewChairID=g_GlobalUnits.SwitchViewChairID(i);
				BYTE cbCardData[2]={0,pGameStart->bCardData[i]};
				cbCardData[0]=(GetMeChairID()==i)?pGameStart->bFundusCard:0;
				m_TempCardData[wViewChairID][cbIndex] = cbCardData[cbIndex];
			}
		}
	}
    m_FirstCardData = m_TempCardData[1][0];
	m_TempCardCount = 2;
	m_wCardCount = 2;
	m_SendCardIndex = 0;
	m_UpCardIndex = 0;
	m_DownCardIndex = 0;
	m_PlayerHandIndex[0]= 0;
	m_PlayerHandIndex[1] = 0;
	schedule(schedule_selector(ShGameViewLayer::SendCard), 0.2f);

	//下注判断
	if (m_wCurrentUser==GetMeChairID())
	{
		m_bFirstBet = true;
		UpdateScoreControl(true);
		StartTime(15,1);
	}
	else
	{
		StartTime(15,0);
	}

	return true;
}

//用户看牌
bool ShGameViewLayer::OnSubLookCard(const void * pBuffer, WORD wDataSize)
{
	//效验参数
	if (wDataSize!=sizeof(CMD_S_LookCard)) return false;
	CMD_S_LookCard * pLook=(CMD_S_LookCard *)pBuffer;
    
	if (g_GlobalUnits.SwitchViewChairID(pLook->dwUserID) == 0)
	{
		m_CardControl[0]->LookCard(true);
	}
	return true;
}

void ShGameViewLayer::SetUserShowHand( bool bShowHand , WORD chair)
{
	m_bUserShowHand[chair] = bShowHand;
}

//用户下注
bool ShGameViewLayer::OnSubAddScore(const void * pBuffer, WORD wDataSize)
{
    //效验数据
    if (wDataSize!=sizeof(CMD_S_AddGold)) return false;
    CMD_S_AddGold * pAddGold=(CMD_S_AddGold *)pBuffer;
    m_GameState = enGameBet;
    
    //变量定义
    WORD wMeChairID=GetMeChairID();
    WORD wViewChairID=g_GlobalUnits.SwitchViewChairID(pAddGold->wLastChairID);
    m_wCurrentUser = pAddGold->wCurrentUser;
    
    //处理数据
    m_lFollowGold = pAddGold->lLastAddGold;
//    SetUserGold(wViewChairID, m_lUserScore[wViewChairID] - pAddGold->lCurrentLessGold);
    
    if (pAddGold->cbTimes == 5)//梭哈
        m_bShowHand = true;
  
    if( m_bShowHand)
    {
        SoundUtil::sharedEngine()->playEffect("ShowHand/SHOW_HAND");
        SetUserShowHand( true, wViewChairID );
    }
    
    
    if( pAddGold->lLastAddGold > 0L )
    {
        SetUserGoldInfoNew(wViewChairID, false, pAddGold->lLastAddGold);
    }
    
    SetUserGoldInfoNew(wViewChairID, true, pAddGold->lCurrentLessGold);
   
    if (pAddGold->wCurrentUser == wMeChairID)
    {
        UpdateScoreControl();
        StartTime(15,1);
    }
    else
        StartTime(15,0);
    
    //类型
    if ( m_bShowHand)
    {
        ShowTips(wViewChairID,3);
    }
    else if (m_bPlayStatus[pAddGold->wLastChairID]==TRUE)
    {
        if (pAddGold->lLastAddGold == m_CurBet || pAddGold->lLastAddGold == 0) //跟注
        {
            SoundUtil::sharedEngine()->playEffect("follow");
            ShowTips(wViewChairID,2);
        }
        else if (pAddGold->lLastAddGold > 0)
        {
            ShowTips(wViewChairID,1);
            SoundUtil::sharedEngine()->playEffect("ADD_SCORE");
        }
    }
    m_CurBet = pAddGold->lLastAddGold;

	return true;
}

//用户放弃
bool ShGameViewLayer::OnSubGiveUp(const void * pBuffer, WORD wDataSize)
{
	SoundUtil::sharedEngine()->playEffect("ShowHand/giveUp");
	//效验数据
	if (wDataSize!=sizeof(CMD_S_GiveUp)) return false;
	CMD_S_GiveUp * pGiveUp=(CMD_S_GiveUp *)pBuffer;

	//设置变量
	m_bPlayStatus[pGiveUp->wUserChairID]=false;
	if (pGiveUp->wUserChairID==GetMeChairID()) setGameStatus(GS_FREE);
	m_GiveUp = true;
	//设置界面
	WORD wViewStation = g_GlobalUnits.SwitchViewChairID(pGiveUp->wUserChairID);
	m_CardControl[wViewStation]->ShowFirstCard(false);
	m_CardControl[wViewStation]->GiveUp();

	return true;
}
//游戏结束
bool ShGameViewLayer::OnSubGameEnd(const void * pBuffer, WORD wDataSize)
{
	//效验参数
	if (wDataSize!=sizeof(CMD_S_GameEnd)) return false;
	CMD_S_GameEnd * pGameEnd=(CMD_S_GameEnd *)pBuffer;
	setGameStatus(GS_FREE);
	m_GameState = enGameEnd;
    
	CopyMemory( &m_GameEnd,pGameEnd,sizeof(m_GameEnd) );
	PerformGameEnd();
	return true;
}

void ShGameViewLayer::PerformGameEnd()
{
	CMD_S_GameEnd *pGameEnd = &m_GameEnd;

	removeAllChildByTag(m_FontAllScoreTga);
	LONGLONG lTotalGold= m_AllGoldControl[0]->GetGold()+
						 m_AllGoldControl[1]->GetGold()+
						 m_TableGoldControl[0]->GetGold()+
						 m_TableGoldControl[1]->GetGold();
    char strc[32];
    sprintf(strc,"%lld",lTotalGold);
    LabelAtlas* temp = LabelAtlas::create(strc,"Common/gold.png" , 23, 32, '+');
    temp->setTag(m_FontAllScoreTga);
    addChild(temp);
    temp->setAnchorPoint(Vec2(0.5f,0.5f));
    temp->setPosition(Vec2(960, 500));
    
    
	//先移动筹码
	if (pGameEnd->lGameGold[GetMeChairID()]>0L)
	{
		SoundUtil::sharedEngine()->playEffect("GAME_WIN");
	}
	else 
	{
		SoundUtil::sharedEngine()->playEffect("GAME_LOST");
	}

	//游戏正常结束
	for (WORD i=0; i<GAME_PLAYER ;i++)
	{
		//设置信息
		if (pGameEnd->lGameGold[i]!=0L)
		{
			if (pGameEnd->bUserCard[i]!=0)
			{
				WORD wViewStation=g_GlobalUnits.SwitchViewChairID(i);
				m_CardControl[wViewStation]->SetFirstCard(pGameEnd->bUserCard[i]);
				m_CardControl[wViewStation]->ShowFirstCard(true);
			}
		}
	}

	//成绩显示在即时聊天对话框
	//自己
    LONGLONG myWinScore = 0;

	for (WORD i=0;i<GAME_PLAYER;i++)
	{
		WORD wViewChairID= g_GlobalUnits.SwitchViewChairID(i);
		if (pGameEnd->lGameGold[i] >= 0)
		{
			sprintf(strc,"+%lld",pGameEnd->lGameGold[i]);
		}
		else
		{
			sprintf(strc,"%lld",pGameEnd->lGameGold[i]);
		}
        LabelAtlas* endScore = LabelAtlas::create(strc, "Common/gold_self.png", 19, 24, '+');
		endScore->setTag(m_EndScore[wViewChairID]);
        endScore->setAnchorPoint(Vec2(0.5f, 0.5f));
		if (wViewChairID == 1)
		{
            myWinScore = pGameEnd->lGameGold[i];
			endScore->setPosition(Vec2(m_HeadPos[wViewChairID].x, m_HeadPos[wViewChairID].y+100));
		}
		else
		{
			endScore->setPosition(Vec2(m_HeadPos[wViewChairID].x, m_HeadPos[wViewChairID].y-100));
		}
		addChild(endScore);
	}

	////设置控件
	SetUserShowHand( false , 0);
	SetUserShowHand( false , 1);
	ShowScoreControl(false);

	////游戏变量
	m_lTurnMaxGold=0L;
	m_lFollowGold=0L;
	memset(m_bPlayStatus,0,sizeof(m_bPlayStatus));
	m_wCurrentUser = INVALID_CHAIR;
	m_lShowHandScore = 0L;

	////辅助变量
	m_lGoldShow=0L;
	m_bShowHand=false;
	memset(m_szName,0,sizeof(m_szName));
	removeAllChildByTag(m_TipsKindTga);

	StopTime();
    auto userData = ClientSocketSink::sharedSocketSink()->GetMeUserData();
    auto myScore = userData->lScore + myWinScore;
	if (myScore > 0)
	{
		StartTime(20,1);
	}
	if (m_BtnReadyPlay != NULL)	m_BtnReadyPlay->setVisible(true);
}

void ShGameViewLayer::UpdataAllScore()  //更新总注
{
	removeAllChildByTag(m_FontAllScoreTga);
//	LONGLONG lTotalGold= m_AllGoldControl[0]->GetGold() + m_AllGoldControl[1]->GetGold();
	char strc[32];
	sprintf(strc,"%lld", m_lTotalGold);
	LabelAtlas* temp = LabelAtlas::create(strc,"Common/gold.png" , 23, 32, '+');
	temp->setTag(m_FontAllScoreTga);
	addChild(temp);
    temp->setAnchorPoint(Vec2(0.5f,0.5f));
	temp->setPosition(Vec2(960, 500));
}


//发牌消息
bool ShGameViewLayer::OnSubSendCard(const void * pBuffer, WORD wDataSize)
{
	//效验数据
	if (wDataSize!=sizeof(CMD_R_SendCard)) return false;
	CMD_R_SendCard * pSendCard=(CMD_R_SendCard *)pBuffer;
	m_GameState = enGameSendCard;
	CopyMemory( &m_SendCard,pSendCard,sizeof(m_SendCard) );

	//设置变量
	m_lGoldShow=0L;
	m_lFollowGold=0L;
	m_lTurnMaxGold = pSendCard->lTurnMaxScore;
	m_wCurrentUser = pSendCard->wCurrentUser;
    m_SendCardCount += pSendCard->cbSendCardCount;
    m_CurBet = 0;
    m_lCurTurnScore[0] = 0;
    m_lCurTurnScore[1] = 0;
    m_bAddScore=false;
    
    if (m_lUserScore[0] > m_lUserScore[1])
    {
        m_curShowHandScore = m_lUserScore[1] - pSendCard->lTurnLessScore;
    }
    else
    {
        m_curShowHandScore = m_lUserScore[0] - pSendCard->lTurnLessScore;
    }
    
    //更新界面
    for (WORD i=0;i<GAME_PLAYER;i++)
    {
        //获取位置
        WORD wViewChairID=g_GlobalUnits.SwitchViewChairID(i);
        
        //设置金币
        SetUserGoldInfoNew(wViewChairID,false,0L);
    }
	
	UpdataSendCard();

	return true;
}


void ShGameViewLayer::UpdataSendCard()
{
	//派发扑克,从上次最大玩家开始发起
	WORD wLastMostUser = m_SendCard.wLastMostUser;
	ASSERT( wLastMostUser != INVALID_CHAIR );

	m_wCardCount += m_SendCard.cbSendCardCount;
	for (BYTE i=0;i<m_SendCard.cbSendCardCount;i++)
	{
		for (WORD j=0;j<GAME_PLAYER;j++)
		{
			WORD wChairId = (wLastMostUser+j)%GAME_PLAYER;
			if (m_bPlayStatus[wChairId]==TRUE&&m_SendCard.bUserCard[wChairId][i]!=0)
			{
				WORD wViewChairID=g_GlobalUnits.SwitchViewChairID(wChairId);
				m_TempCardData[wViewChairID][i] = m_SendCard.bUserCard[wChairId][i];
			}
		}
	}

	
	m_TempCardCount = m_SendCard.cbSendCardCount;
	m_SendCardIndex = 0;
	m_UpCardIndex = 0;
	m_DownCardIndex = 0;
	m_PlayerHandIndex[0]= 0;
	m_PlayerHandIndex[1] = 0;
	schedule(schedule_selector(ShGameViewLayer::SendCard), 0.1f);

	if (m_wCurrentUser==GetMeChairID())
	{
		m_bFirstBet = true;
		UpdateScoreControl(true);
	}
}

string ShGameViewLayer::GetCardStringName(BYTE card)
{
	char tt[32];
	sprintf(tt,"%0x",card);
	BYTE _value = m_GameLogic.GetCardValue(card);
	BYTE _color = m_GameLogic.GetCardColor(card);
	string temp;
	if (_color == 0) temp = "fangkuai_";
	if (_color == 1) temp = "meihua_";
	if (_color == 2) temp = "hongtao_";
	if (_color == 3) temp = "heitao_";
	char _valstr[32];
	ZeroMemory(_valstr,32);
	if (_value == 14) //小王
	{
		sprintf(_valstr,"xiaowang.png");
	}
	else if (_value == 15)
	{
		sprintf(_valstr,"dawang.png");
	}
	else if (_value < 10)
	{
		sprintf(_valstr,"0%d.png",_value);
	}
	else 
	{
		sprintf(_valstr,"%d.png",_value);
	}
	
	temp+=_valstr;
	return temp;
}

void ShGameViewLayer::InitGame()
{
	//背景图
	m_BackSpr = Sprite::create("ShowHand/ShowHandback.jpg");
	m_BackSpr->setPosition(_STANDARD_SCREEN_CENTER_IN_PIXELS_);
	addChild(m_BackSpr);

    m_SelfText = Sprite::createWithSpriteFrameName("text_self.png");
    m_SelfText->setPosition(Vec2(330,540));
    addChild(m_SelfText);
    
    m_OtherText = Sprite::createWithSpriteFrameName("test_other.png");
    m_OtherText->setPosition(Vec2(1590,540));
    addChild(m_OtherText);
    
    m_middleText =Sprite::createWithSpriteFrameName("label_game.png");
    m_middleText->setPosition(_STANDARD_SCREEN_CENTER_);
    addChild(m_middleText);
    
	m_TimePos[0] = Vec2(1440,680);
	m_TimePos[1] = Vec2(1440,680);
	m_ClockSpr = Sprite::createWithSpriteFrameName("timeback.png");
	m_ClockSpr->setPosition(m_TimePos[0]);
	addChild(m_ClockSpr);
	StartTime(15,1);

	//tga
	for (int i = 0; i < GAME_PLAYER; i++)
	{
		m_ReadyTga[i]		 = 1000+i;
		m_HeadTga[i]		 = 1010+i;
		m_HeadGlodTga[i]	 = 1020+i;
		m_NickNameTga[i]	 = 1030+i;
		m_AllGoldTga[i]		 = 1040+i;
		m_TableGoldTga[i]	 = 1050+i;
		m_TempSendCardTga[i] = 1060+i;
		m_CardTypeTga[i]	 = 1070+i;
	}
    
	m_BtnBackTga = 1003;
	m_HandCardTga[0] = 1013;
	m_HandCardTga[1] = 1023;
	m_TipsKindTga = 1033;
	m_FontAllScoreTga = 1034;
	m_FontScoreBtn[0] = 1035;
	m_FontScoreBtn[1] = 1036;
	m_FontScoreBtn[2] = 1037;
	m_EndScore[0] = 1038;
	m_EndScore[1] = 1039;

	m_HeadPos[0] = Vec2(480,960);
	m_HeadPos[1] = Vec2(480,120);

	m_ReadyPos[0] = Vec2(750,960);
	m_ReadyPos[1] = Vec2(750,120);

	m_AllBetPos[0] = Vec2(1200,578);
	m_AllBetPos[1] = Vec2(720,578);

	m_TableBetPos[0] = Vec2(1200,500);
	m_TableBetPos[1] = Vec2(720,500);

	m_TipsPos[0] = Vec2(280,960);
	m_TipsPos[1] = Vec2(280,120);

	m_GoldStartPos[0] = Vec2(480,960);
	m_GoldStartPos[1] = Vec2(480,120);

	for (int i = 0; i < 2; i++)
	{
		m_AllGoldControl[i] = new ShGoldControl(this,m_AllGoldTga[i]);
		m_TableGoldControl[i] = new ShGoldControl(this,m_TableGoldTga[i]);

		if(i == 0) m_CardControl[i] = new ShCardControl(this,m_HandCardTga[i], false);
		else m_CardControl[i] = new ShCardControl(this,m_HandCardTga[i], true);
	}
}

void ShGameViewLayer::ShowTips(WORD chair, int _kind) //操作类型
{
	removeAllChildByTag(m_TipsKindTga);
	string strc;
    if(_kind == 1) strc = "Tips_Add.png";
	else if(_kind == 2) strc = "Tips_Follow.png";
	else if(_kind == 3) strc = "Tips_ShowHand.png";
	Sprite* temp = Sprite::createWithSpriteFrameName(strc.c_str());
	temp->setPosition(m_TipsPos[chair]);
	temp->setTag(m_TipsKindTga);
	temp->setScale(0.1f);
	addChild(temp);
	ScaleTo *leftMoveBy = ScaleTo::create(0.1f , 1.0f);
	temp->runAction(leftMoveBy);	
}

void ShGameViewLayer::AddButton()
{
	m_BtnReadyPlay = CreateButton("game_Start" ,Vec2(960,380),Btn_Ready);
	addChild(m_BtnReadyPlay , 0 , Btn_Ready);
	m_BtnReadyPlay->setVisible(false);

	m_BtnBackToLobby = CreateButton("Returnback" ,Vec2(90,1000),Btn_BackToLobby);
	addChild(m_BtnBackToLobby , 0 , Btn_BackToLobby);
	m_BtnBackToLobby->setVisible(true);
	
	m_BtnSeting = CreateButton("seting" ,Vec2(1810,1000),Btn_Seting);
	addChild(m_BtnSeting , 0 , Btn_Seting);
	m_BtnSeting->setVisible(true);

	//按钮界面
	Node* temp = Node::create();
	temp->setPosition(Vec2(1320,0));
	temp->setTag(m_BtnBackTga);
	addChild(temp);
    
	m_BtnFollow = CreateButton("Btn_Follow" ,Vec2(450,260),Btn_Follow);
	temp->addChild(m_BtnFollow , 0 , Btn_Follow);
	m_BtnShowHand = CreateButton("Btn_ShowHand" ,Vec2(450,160),Btn_ShowHand);
	temp->addChild(m_BtnShowHand , 0 , Btn_ShowHand);
    m_BtnGiveUp = CreateButton("Btn_GiveUp" ,Vec2(450,60),Btn_GiveUp);
    temp->addChild(m_BtnGiveUp , 0 , Btn_GiveUp);
	temp->setVisible(false);

	m_BtnBack1 = CreateButton("Btn_BtnBack" ,Vec2(150,260),Btn_BtnBack1);
	temp->addChild(m_BtnBack1 , 0 , Btn_BtnBack1);
	m_BtnBack2 = CreateButton("Btn_BtnBack" ,Vec2(150,160),Btn_BtnBack2);
	temp->addChild(m_BtnBack2 , 0 , Btn_BtnBack2);
	m_BtnBack3 = CreateButton("Btn_BtnBack" ,Vec2(150,60),Btn_BtnBack3);
	temp->addChild(m_BtnBack3 , 0 , Btn_BtnBack3);
}

void ShGameViewLayer::ShowScoreControl( bool bShow )
{
	int nCmdShow = bShow?true:false;
	Node* _back = getChildByTag(m_BtnBackTga);
	if(_back != NULL) _back->setVisible(nCmdShow);
}

//
void ShGameViewLayer::UpdateScoreControl(bool IsMe)
{
	Color3B disablecolor;
	disablecolor.r = 20;disablecolor.g = 70; disablecolor.b = 70;
	Color3B colorHight;
	colorHight.r = 255;colorHight.g = 255; colorHight.b = 255;
	//显示按钮
	ShowScoreControl( true );

	WORD wMeChairID = GetMeChairID();
	m_lGoldShow = m_TableGoldControl[1]->GetGold();
	//变量定义
	LONGLONG lLeaveScore=m_lTurnMaxGold-m_AllGoldControl[1]->GetGold();

	//禁止按钮
	bool shouhandable = (m_wCardCount>=3 && (lLeaveScore>0 || m_bShowHand)) ? true : false;
	m_BtnShowHand->setEnabled(shouhandable);
	if(shouhandable)
        m_BtnShowHand->setColor(colorHight);
	else
        m_BtnShowHand->setColor(disablecolor);

    LONGLONG lShowHandScore = m_lUserScore[wMeChairID] > m_lShowHandScore?m_lShowHandScore:m_lUserScore[wMeChairID];
    if(m_lFollowGold==0 || (lLeaveScore > 0 && lShowHandScore > m_lFollowGold + m_AllGoldControl[1]->GetGold()))
    {
        m_BtnFollow->setEnabled(true);
        m_BtnFollow->setColor(colorHight);
    }
    else 
    {
        m_BtnFollow->setEnabled(false);
        m_BtnFollow->setColor(disablecolor);
    }

	if(m_lFollowGold > 0)
	{
		m_BtnFollow->setEnabled(true);
		m_BtnFollow->setColor(colorHight);
	}


	//加注按钮
	lLeaveScore=m_lTurnMaxGold-m_lFollowGold-m_AllGoldControl[1]->GetGold();

	bool b1 = (lLeaveScore>=m_lBasicGold) ? true : false ;
	bool b2 = (lLeaveScore>=2*m_lBasicGold) ? true : false;
	bool b3 = (lLeaveScore>=4*m_lBasicGold) ? true : false;

	if(m_bAddScore) 
	{
		 b1=b2=b3=false;
	}
	else
	{
		if(m_bShowHand==false)
		{
			b1=b2=b3=true;
		}
	}
	m_BtnBack1->setEnabled(b1);
	m_BtnBack2->setEnabled(b2);
	m_BtnBack3->setEnabled(b3);

	if(b1)
    {
        m_BtnBack1->setColor(colorHight);
    }
	else
    {
        m_BtnBack1->setColor(disablecolor);
    }
	if(b2)
    {
        m_BtnBack2->setColor(colorHight);
    }
	else
    {
        m_BtnBack2->setColor(disablecolor);
    }
	if(b3)
    {
        m_BtnBack3->setColor(colorHight);
    }
	else
    {
        m_BtnBack3->setColor(disablecolor);
    }
    
	if (m_bShowHand)
	{
        m_BtnShowHand->setEnabled(false);
		m_BtnBack1->setEnabled(false);
		m_BtnBack2->setEnabled(false);
		m_BtnBack3->setEnabled(false);
		m_BtnShowHand->setColor(disablecolor);
		m_BtnBack1->setColor(disablecolor);
		m_BtnBack2->setColor(disablecolor);
		m_BtnBack3->setColor(disablecolor);
	}

	return;
}

Menu* ShGameViewLayer::CreateButton(std::string szBtName ,const Vec2 &p , int tag )
{
	return Tools::Button(StrToChar(szBtName+"_normal.png") , StrToChar(szBtName+"_click.png") , p, this , menu_selector(ShGameViewLayer::callbackBt) , tag);
}

void ShGameViewLayer::AddPlayerInfo()
{
	for (int i = 0; i < GAME_PLAYER; i++)
	{
		if (ClientSocketSink::sharedSocketSink()->GetMeUserData() == NULL)
		{
			return;
		}
		int wtable = ClientSocketSink::sharedSocketSink()->GetMeUserData()->wTableID;
		if (wtable > 200) return;
		const tagUserData* pUserData = ClientSocketSink::sharedSocketSink()->m_UserManager.GetUserData(wtable,i);
		int chair = g_GlobalUnits.SwitchViewChairID(i);
		if (pUserData != NULL)
		{
			//头像
			removeAllChildByTag(m_HeadTga[chair]);
            Sprite* playerInfoBg = Sprite::createWithSpriteFrameName("bg_player_info.png");
            playerInfoBg->setPosition(m_HeadPos[chair]);
            addChild(playerInfoBg, 0, m_HeadTga[chair]);

            Sprite* headBg = Sprite::createWithSpriteFrameName("bg_head_img.png");
            headBg->setPosition(Vec2(70, 73));
            playerInfoBg->addChild(headBg);
            
            string heads = g_GlobalUnits.getFace(pUserData->wGender, pUserData->lScore);
            Sprite* mHead = Sprite::createWithSpriteFrameName(heads);
			mHead->setPosition(Vec2(70, 73));
            mHead->setScale(0.9f);
			playerInfoBg->addChild(mHead);

			//金币
			removeAllChildByTag(m_HeadGlodTga[chair]);
			char strc[32]="";
			LONGLONG lMon = pUserData->lScore;
			memset(strc , 0 , sizeof(strc));
			Tools::AddComma(lMon , strc);
			Label* mGoldFont;
			mGoldFont = Label::createWithSystemFont(strc,_GAME_FONT_NAME_1_,30);
			mGoldFont->setTag(m_HeadGlodTga[chair]);
			playerInfoBg->addChild(mGoldFont);
            mGoldFont->setColor(Color3B::YELLOW);
            mGoldFont->setAnchorPoint(Vec2(0, 0.5f));
			mGoldFont->setPosition(Vec2(140, 90));

			//昵称
			removeAllChildByTag(m_NickNameTga[chair]);
			Label* mNickfont;
			mNickfont = Label::createWithSystemFont(gbk_utf8(pUserData->szNickName),_GAME_FONT_NAME_1_,30);
			mNickfont->setTag(m_NickNameTga[chair]);
			playerInfoBg->addChild(mNickfont);
            mNickfont->setAnchorPoint(Vec2(0,0.5f));
			mNickfont->setPosition(Vec2(140, 40));

			//准备
			removeAllChildByTag(m_ReadyTga[chair]);
			Sprite* mReaderSpr = Sprite::createWithSpriteFrameName("game_ready.png");
			mReaderSpr->setPosition(m_ReadyPos[chair]);
			mReaderSpr->setTag(m_ReadyTga[chair]);
			addChild(mReaderSpr);

			if (pUserData->cbUserStatus == US_READY)
			{
//                m_CardControl[chair]->ClearCardData();
				getChildByTag(m_ReadyTga[chair])->setVisible(true);
			}
			else getChildByTag(m_ReadyTga[chair])->setVisible(false);

		}
		else
		{
			if (chair == 1 && !g_GlobalUnits.m_bLeaveGameByServer) //如果是自己退出，就关闭游戏
			{
//                AlertMessageLayer::createConfirm(this , "\u60a8\u7684\u9152\u5427\u8c46\u4e0d\u8db3\uff0c\u4e0d\u80fd\u7ee7\u7eed\u6e38\u620f\uff01", menu_selector(IGameView::backLoginView));
                 backLoginView(nullptr);
				return;
			}
			removeAllChildByTag(m_HeadTga[chair]);
			removeAllChildByTag(m_HeadGlodTga[chair]);
			removeAllChildByTag(m_NickNameTga[chair]);
			removeAllChildByTag(m_ReadyTga[chair]);

			
		}
	}
}

string ShGameViewLayer::AddCommaToNum(LONG Num)
{
	char _str[256];
	sprintf(_str,"%ld", Num);
	string _string = _str;
	ssize_t step = _string.length()/3;
	for (ssize_t i = 1; i <= step; i++)
	{
		_string.insert(_string.length()-(i-1)-(i*3), ",");
	}
	return _string;
}

void ShGameViewLayer::SendReqTransProp()
{
	CMD_C_GP_GetTransProps GetTransProps;
	ZeroMemory(&GetTransProps ,sizeof(GetTransProps));
	GetTransProps.wKindID = KIND_ID;
	SendMobileData(SUB_C_GP_GETTRANSPROP , &GetTransProps , sizeof(GetTransProps));
}

void ShGameViewLayer::SendSendProp( DWORD dwRcvUserID ,WORD wPropID , WORD wAmount , int lPayscore )
{
	CMD_C_GP_SendProp oSendProp;
	ZeroMemory(&oSendProp ,sizeof(oSendProp));
	oSendProp.dwUserID = g_GlobalUnits.GetUserID();
	oSendProp.dwRcvUserID = dwRcvUserID;
	oSendProp.wKindID = KIND_ID;
	oSendProp.wPropID = wPropID;
	oSendProp.lAmount = wAmount;
	oSendProp.lPayScore = lPayscore;
	oSendProp.cbPlatform = GAME_PLATFORM;
	SendMobileData(SUB_C_GP_SENDPROP , &oSendProp , sizeof(oSendProp));
}

void ShGameViewLayer::SendReqSendRecord()
{
	CMD_C_GP_GetSendRecord GetSendRecord;
	ZeroMemory(&GetSendRecord ,sizeof(GetSendRecord));
	GetSendRecord.dwUserID = g_GlobalUnits.GetUserID();
	GetSendRecord.wKindID = KIND_ID;
	GetSendRecord.wItemCount = SENDRECORD_COUNT;
	GetSendRecord.wPageIndex = 1;
	SendMobileData(SUB_C_GP_SENDRECORD , &GetSendRecord ,sizeof(GetSendRecord));
}

void ShGameViewLayer::callbackBt( Ref *pSender )
{
	Node *pNode = (Node *)pSender;
	switch (pNode->getTag())
	{
	case Btn_GetScoreBtn:
		{	
			GetScoreForBank();
			break;
		}
	case Btn_Ready:	//准备按钮
		{
			SoundUtil::sharedEngine()->playEffect("buttonMusic");
			SendGameStart();
			break;
		}
	case Btn_BackToLobby: // 返回大厅按钮
		{
			SoundUtil::sharedEngine()->playEffect("buttonMusic");
			BackToLobby();
			break;
		}
	case Btn_Seting: //设置
		{
			GameLayerMove::sharedGameLayerMoveSink()->OpenSeting();
			break;
		}
	case Btn_Follow: //跟注
		{
			OnFollow();
			m_bFirstBet = false;
			m_bAddScore=true;
			break;
		}
	case Btn_GiveUp: //放弃
		{
			OnGiveUp();
			m_bFirstBet = false;
			m_bAddScore=true;
			break;
		}
	case Btn_BtnBack1: //加注1倍
		{
			OnAddGold(1);
			m_bFirstBet = false;
			m_bAddScore=true;
			break;
		}
	case Btn_BtnBack2: //加注2倍
		{
			OnAddGold(2);
			m_bFirstBet = false;
			m_bAddScore=true;
			break;
		}
	case Btn_BtnBack3: //加注3倍
		{
			OnAddGold(4);
			m_bFirstBet = false;
			m_bAddScore=true;
			break;
		}
	case Btn_ShowHand: //梭哈
		{
			OnShowHand();
			m_bFirstBet = false;
			m_bAddScore=true;
			break;
		}
	case Btn_BtnLook: //看牌
		{
			SoundUtil::sharedEngine()->playEffect("buttonMusic");
			LookCard();
			break;
		}
	}
}

void ShGameViewLayer::LookCard()
{
	CMD_C_LookCard temp;
	temp.dwUserID = GetMeUserData()->dwUserID;
	SendData(SUB_C_LOOK_CARD, &temp ,sizeof(temp));
}

//梭哈按钮
void ShGameViewLayer::OnShowHand()
{
	m_TableGoldControl[MYSELF_VIEW_ID]->SetGold(m_curShowHandScore);
	m_bShowHand=true;
    
	//发送消息
    auto gameID = g_GlobalUnits.GetGolbalUserData().dwGameID;
    auto tableID =  ClientSocketSink::sharedSocketSink()->GetMeUserData()->wTableID;
    auto chairID = GetMeChairID();
    
    SH_CMD_C_AddScore AddScore;
    AddScore.nServriValue = gameID + tableID + chairID * 135 + m_lBasicGold * (m_SendCardCount)+123 * m_FirstCardData + m_lShowHandScore;
    AddScore.lScore = m_curShowHandScore;
    log("--------->>>>>>%lld", m_curShowHandScore);
    AddScore.cbTimes = 5;
    SendData(SUB_C_ADD_SCORE,&AddScore,sizeof(AddScore));

	SoundUtil::sharedEngine()->playEffect("SHOW_HAND");  
	SetUserShowHand( true, MYSELF_VIEW_ID );

	ShowScoreControl(false);
}

//加注
void ShGameViewLayer::OnAddGold(int wParam)
{
	LONGLONG lGold = m_lFollowGold+m_lBasicGold*wParam;
	LONGLONG lShowHandScore = m_lUserScore[GetMeChairID()] > m_lShowHandScore ? m_lShowHandScore : m_lUserScore[GetMeChairID()];

	//设置加注
	m_TableGoldControl[MYSELF_VIEW_ID]->SetGold(lGold);

    if(lGold+m_AllGoldControl[MYSELF_VIEW_ID]->GetGold() < lShowHandScore)
        lGold= lGold < m_lFollowGold ? m_lFollowGold:lGold;
    
    //设置变量
    if (lShowHandScore == lGold + m_AllGoldControl[MYSELF_VIEW_ID]->GetGold())
    {
        if(!m_bShowHand)
        {
            SetUserShowHand(true, 1);
        }
        
        m_bShowHand = true;
    }
    
    //显示按钮
    ShowScoreControl(false);
    
    //发送消息
    auto gameID = g_GlobalUnits.GetGolbalUserData().dwGameID;
    auto tableID =  ClientSocketSink::sharedSocketSink()->GetMeUserData()->wTableID;
    auto chairID = GetMeChairID();
    
    SH_CMD_C_AddScore AddScore;
    AddScore.nServriValue = gameID + tableID + chairID * 135 + m_lBasicGold * (m_SendCardCount)+123 * m_FirstCardData + m_lShowHandScore;
    AddScore.lScore = m_CurBet + m_lBasicGold*wParam;
    AddScore.cbTimes = wParam;
    SendData(SUB_C_ADD_SCORE,&AddScore,sizeof(AddScore));
}

//放弃按钮
void ShGameViewLayer::OnGiveUp()
{
	//显示按钮
	ShowScoreControl(false);
	SendData(SUB_C_GIVE_UP);
}

//不加按钮
void ShGameViewLayer::OnNotAdd()
{
	//显示按钮
	ShowScoreControl(false);

	//发送消息
	SH_CMD_C_AddScore AddScore;
	AddScore.lScore=0;
	SendData(SUB_C_ADD_SCORE, &AddScore, sizeof(AddScore));
}

//跟注按钮
void ShGameViewLayer::OnFollow()
{
	//显示按钮
	ShowScoreControl(false);

	//发送消息
    auto gameID = g_GlobalUnits.GetGolbalUserData().dwGameID;
    auto tableID =  ClientSocketSink::sharedSocketSink()->GetMeUserData()->wTableID;
    auto chairID = GetMeChairID();
    
    SH_CMD_C_AddScore AddScore;
    
    AddScore.lScore = m_CurBet;
    AddScore.nServriValue = gameID + tableID + chairID * 135 + m_lBasicGold * (m_SendCardCount)+123 * m_FirstCardData + m_lShowHandScore;
    AddScore.cbTimes = 0;
    SendData(SUB_C_ADD_SCORE, &AddScore, sizeof(AddScore));
}

void ShGameViewLayer::backLoginView( Ref *pSender )
{
	IGameView::backLoginView(pSender);	
}

void ShGameViewLayer::CardMoveEnd(Node *pSender,void*data)
{
	m_CardControl[0]->CardMoveEnd(pSender, data);
}

void ShGameViewLayer::CardMoveEnd1(Node *pSender,void*data)
{
    m_isLookCard = false;
	m_CardControl[1]->CardMoveEnd(pSender, data);
}

void ShGameViewLayer::GoldMoveCallback(Node *pSender,void*data) //两个参数
{
	long _kind = (long)data;
	if (_kind == 0)
	{
		m_TableGoldControl[0]->GoldMoveCallback(pSender,data);
	}
	else if (_kind == 1)
	{
		m_TableGoldControl[1]->GoldMoveCallback(pSender,data);
	}
	else if (_kind == 2)
	{
		m_AllGoldControl[0]->GoldMoveCallback(pSender,data);
	}
	else if (_kind == 3)
	{
		m_AllGoldControl[1]->GoldMoveCallback(pSender,data);
	}
}

void ShGameViewLayer::OnQuit()
{
	m_pGameScene->removeChild(this);
	GameLayerMove::sharedGameLayerMoveSink()->m_CurGameLayer = NULL;
}
