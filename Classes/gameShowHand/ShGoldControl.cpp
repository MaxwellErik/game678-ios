#include "ShGoldControl.h"

#include "GameScene.h"
#include "GameLayer.h"
#include "ShGameViewLayer.h"

ShGoldControl::ShGoldControl(GameLayer* _layer, int Tga)
{
	m_lGold = 0;
	m_bmove = false;
	m_bdraw = false;
	m_BetTga = Tga;
	m_layer = _layer;
}

ShGoldControl::~ShGoldControl(void)
{
}

void ShGoldControl::SetGold(LONGLONG lGold)
{
	if (m_lGold!=lGold)
	{
		m_lGold=lGold;
	}
	return;
}

void ShGoldControl::StartMove(Vec2 spos, Vec2 epos, int _kind,bool overHide)
{
	m_layer->removeAllChildByTag(m_BetTga);
	char strc[32]="";
	sprintf(strc,"%lld",m_lGold);
	LabelAtlas *mGoldFont = LabelAtlas::create(strc, "Common/gold.png", 23, 32, '+');
	m_layer->addChild(mGoldFont,0,m_BetTga);
    mGoldFont->setAnchorPoint(Vec2(0.5f,0.5f));
    mGoldFont->setPosition(spos);
    
	int _kindType = _kind;
	MoveTo *leftMoveBy = MoveTo::create(0.35f, epos);
	ActionInstant *func = CCCallFuncND::create(m_layer,callfuncND_selector(ShGameViewLayer::GoldMoveCallback),(void*)_kindType);
	mGoldFont->runAction(Sequence::create(leftMoveBy,func,NULL));

	m_Spos = spos;
	m_Epos = epos;
	m_bmove = true;
	m_bdraw = true;
	m_overHide = overHide;
}

void ShGoldControl::GoldMoveCallback(Node *pSender,void*data)
{
	if(m_overHide)
	{
		m_layer->removeAllChildByTag(m_BetTga);
		m_bdraw = false;
		SetGold(0);
	}
}

void ShGoldControl::SetPos(Vec2 spos)
{
	m_Spos = spos;
	m_bmove = false;

	m_layer->removeAllChildByTag(m_BetTga);

	char strc[32]="";
	sprintf(strc,"%lld",m_lGold);
	LabelAtlas *mGoldFont = LabelAtlas::create(strc, "Common/gold.png", 23, 32, '+');
    mGoldFont->setAnchorPoint(Vec2(0.5f,0.5f));
	m_layer->addChild(mGoldFont, 0, m_BetTga);
	mGoldFont->setPosition(spos);
}

void ShGoldControl::moveWithParabola(Sprite*mSprite, Vec2 startPoint ,Vec2 endPoint, float time)
{   
	float sx = startPoint.x;  
	float sy = startPoint.y;   
	float ex =endPoint.x+50;  
	float ey =endPoint.y+150;   
	int h = mSprite->getContentSize().height*0.5;  
	ccBezierConfig bezier;
	bezier.controlPoint_1 = Vec2(sx, sy);
	bezier.controlPoint_2 = Vec2(sx+(ex-sx)*0.5, sy+(ey-sy)*0.5+200);
	bezier.endPosition = Vec2(endPoint.x-30, endPoint.y+h);
	BezierTo *actionMove = BezierTo::create(time,bezier);
	mSprite->runAction(actionMove);
}  

void ShGoldControl::SetShow(bool _show)
{
	m_bdraw = _show;
	m_overHide = false;
}
