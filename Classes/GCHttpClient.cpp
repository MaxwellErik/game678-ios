﻿#include "GCHttpClient.h"

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#ifdef _WIN32
#pragma comment(lib, "Ws2_32.lib")
#else
#include <sys/types.h>
#include <sys/stat.h>
#include <netinet/tcp.h> 
#include <netdb.h>
#endif

#ifndef _WIN32

#include <setjmp.h>
#include <time.h>
#include <signal.h>


static sigjmp_buf jmpbuf;
//static void alarm_func(int t)
//{
//	siglongjmp(jmpbuf, 1);
//}

#endif // _DEBUG


GCHttpClient::GCHttpClient()
{
#ifdef _WIN32
	WSAData lpWSAData;
	WSAStartup(0x0202, &lpWSAData);
#endif

	m_sock = 0;

	m_iKeepAliveTime = 15;

	m_bServerConnectionClose = false;
}

GCHttpClient::~GCHttpClient()
{
	
}


void GCHttpClient::SetConnectiongKeepAlive(int iKeepAliveTime)
{
	m_iKeepAliveTime = iKeepAliveTime;
	if(m_iKeepAliveTime == 0 && m_sock > 0)
	{
#ifdef _WIN32
		closesocket(m_sock);
#else	
		close(m_sock);
#endif
		m_sock = 0;
	}
}

int GCHttpClient::GetWebsiteContent(char *szURL,char *szContent,int &iContentLen,char *szReqMsg,int iReqLen)
{
	iContentLen = 0;

	m_pBlockResMsg = szContent;
	m_iBlockResMsgLen = &iContentLen;

	m_szURL = szURL;
	
	ParseURL(m_szURL,m_szHostName,m_szFilePath,m_iPort);

	int rt = 0;

	time_t tmNow = time(NULL);
	bool bUseLastSocket = false;
	if(m_sock > 0)
	{
		if(m_strLastHostName != m_szHostName || tmNow-m_tmLastAlive >= m_iKeepAliveTime)	//服务器不一样或者连接超时15秒，服务器默认设置20秒
		{
#ifdef _WIN32
			closesocket(m_sock);
#else	
			close(m_sock);
#endif
			m_sock = 0;
		}
	}
	if(m_sock == 0)	//新建连接
	{
		rt = ConnectServer((char*)m_szHostName.c_str(),m_iPort);
		if(rt > 0)
			return rt;
	}
	else	//利用老接连
	{
		bUseLastSocket = true;
	}
	m_strLastHostName = m_szHostName;
	m_tmLastAlive = tmNow;

	rt = TryHttpSendAndRec(szReqMsg,iReqLen);

	if(rt != HTTP_ERROR_NONE && bUseLastSocket == true)	//用老接连失败的话，新建连接再试次
	{
		rt = ConnectServer((char*)m_szHostName.c_str(),m_iPort);
		if(rt > 0)
			return rt;

		rt = TryHttpSendAndRec(szReqMsg,iReqLen);
	}

	return rt;
}


int GCHttpClient::TryHttpSendAndRec(char *szReqMsg,int iReqLen)
{
	int rt = 0;
	do
	{
		m_bServerConnectionClose = false;

		rt = SendHttpReq(szReqMsg,iReqLen);
			if(rt > 0)
				break;

		m_iHttpHeadHaveRecLen = 0;
		m_iHttpContentHaveRecLen = 0;
		m_iHttpFileSize = -1;
		memset(m_szHttpHeadRes,0,sizeof(m_szHttpHeadRes));
		m_bRecOK = false;

		time_t tmStartRec = time(NULL);
		time_t tmNow = tmStartRec;
		while(1)
		{
			rt = ReadSocketNodeData();

			if(rt > 0)
				break;

			//读完长度就算成功
			if(m_bRecOK == true || (m_iHttpFileSize > 0 && m_iHttpFileSize == m_iHttpContentHaveRecLen))
			{
				*m_iBlockResMsgLen = (int)m_iHttpContentHaveRecLen;
				if(*m_iBlockResMsgLen == 0)
				{
					rt = HTTP_ERROR_FILESIZE_ZERO;
				}
				else
				{
					rt = HTTP_ERROR_NONE;
				}
				break;
			}


			tmNow = time(NULL);
			if(tmNow - tmStartRec > 30)	//30秒收不到，报错
			{
				rt = HTTP_ERROR_REC_TIMEOUT;
				break;
			}

		}

	} while (0);
	
	
	if(m_sock > 0 && (rt != HTTP_ERROR_NONE || m_iKeepAliveTime == 0 || m_bServerConnectionClose == true))
	{
#ifdef _WIN32
		closesocket(m_sock);
#else	
		close(m_sock);
#endif

		m_sock = 0;
	}
	
	return rt;
}

void GCHttpClient::ParseURL(string strUrl,string &strHostName,string &strFilePath,int &iPort)
{
	int iPos = 0;
	if(strUrl.find("http://") == 0)
	{
		iPos = strlen("http://");
	}
	else if(strUrl.find("https://") == 0)
	{
		iPos = strlen("https://");
	}
	auto iPos2 = strUrl.find('/',iPos+1);
	if(iPos2 != -1)
	{
		strHostName = strUrl.substr(iPos,iPos2-iPos);
		strFilePath = strUrl.substr(iPos2+1);
	}
	else
	{
		strHostName = strUrl.substr(iPos);
	}
	auto iPos3 = strHostName.find(':');
	if(iPos3 != -1)
	{
		string strPort = strHostName.substr(iPos3+1);

		iPort = atoi(strPort.c_str());

		string strHost = strHostName.substr(0,iPos3);
		strHostName = strHost;
	}
	else
	{
		iPort = 80;
	}
}



int GCHttpClient::ConnectServer(char *szServerIP,unsigned short iServerPort)
{
	struct sockaddr_in 	addrServer;
	int sock = -1;
	int flag;

	memset(&addrServer, 0, sizeof(addrServer));
	addrServer.sin_family = AF_INET;
	addrServer.sin_port =htons(iServerPort);

	unsigned long uIP = inet_addr(szServerIP);
	if(uIP == 0xffffffff)	//域名
	{
//#ifdef _WIN32
		struct hostent *host = gethostbyname(szServerIP);
//#else
//		struct hostent *host = gngethostbyname(szServerIP , 3);
//#endif // _DEBUG
		
		if(host == NULL)
		{
			return HTTP_ERROR_CREATESOCKET;
		}
		char *szIP = (char *)inet_ntoa(*(struct in_addr *)(host->h_addr));
		uIP = inet_addr(szIP);
	}
	addrServer.sin_addr.s_addr = (unsigned int)uIP;

	if((sock = socket(AF_INET,SOCK_STREAM,0)) < 0)
	{
		return HTTP_ERROR_CREATESOCKET;
	}
	flag = 1;
	if(setsockopt(sock, SOL_SOCKET, SO_REUSEADDR,(char *)&flag, sizeof flag) < 0)
	{
		return HTTP_ERROR_CREATESOCKET;
	}
	if(setsockopt(sock, IPPROTO_TCP,TCP_NODELAY,(char *)&flag, sizeof flag) < 0)
	{
		return HTTP_ERROR_CREATESOCKET;
	}

	if(connect(sock,(struct sockaddr *)&addrServer, sizeof(addrServer)) < 0)
	{
		return HTTP_ERROR_CONNECTHOST;
	}

#ifdef _WIN32
	unsigned long on = 1;
	if(ioctlsocket(sock, FIONBIO, &on) < 0)
	{
		return HTTP_ERROR_CREATESOCKET;
	}
#else
	flag = fcntl(sock, F_GETFL, 0);
	if(fcntl(sock, F_SETFL, flag | O_NONBLOCK) < 0)
	{
		return HTTP_ERROR_CREATESOCKET;
	}
#endif

	m_sock = sock;

	return HTTP_ERROR_NONE;
}

int GCHttpClient::SendHttpReq(char *szReqMsg,int iReqLen)
{
	std::string szHttpReqHead = "";
	char szPort[10];
	char szTemp[128];
	sprintf(szPort,"%d",m_iPort);
	
	//模式
	szHttpReqHead += "POST /";
	szHttpReqHead += m_szFilePath;
	szHttpReqHead += " HTTP/1.0";
	szHttpReqHead += "\r\n";

	//主机
	szHttpReqHead += "Host:";
	szHttpReqHead += m_szHostName;
	szHttpReqHead += "\r\n";

	//包体长度
	sprintf(szTemp,"Content-Length:%d",iReqLen);
	szHttpReqHead += szTemp;
	szHttpReqHead += "\r\n";

	//包体类型
	szHttpReqHead += "Content-type:application/x-www-form-urlencoded";
	szHttpReqHead += "\r\n";

	//接收的数据类型
	szHttpReqHead += "Accept:*/*";
	szHttpReqHead += "\r\n";

	//浏览器类型
	szHttpReqHead += "User-Agent:Mozilla/5.0 (compatible; MSIE 7.0; Windows NT 5.1)";
	szHttpReqHead += "\r\n";

	//连接设置,保持
	szHttpReqHead += "Connection:Keep-Alive";
	szHttpReqHead += "\r\n";

	//不用cache
	szHttpReqHead += "Cache-Control:no-cache";
	szHttpReqHead += "\r\n";

	//最后一行:空行
	szHttpReqHead += "\r\n";

	unsigned long iHeadlen = szHttpReqHead.length();
	unsigned long iNewLen = iHeadlen+iReqLen;
	iNewLen = iNewLen+(4-iNewLen%4);
	char *pSendData = new char[iNewLen];
	memset(pSendData, 0x00, iNewLen);
	strcpy(pSendData,szHttpReqHead.c_str());
	if(iReqLen > 0)
	{
		memcpy(pSendData+iHeadlen,szReqMsg,iReqLen);
	}
	iHeadlen += iReqLen;
	int iHaveSend = 0;
	while(1)
	{
		long iSendLen = send(m_sock,pSendData+iHaveSend,iHeadlen-iHaveSend,0);
		if(iSendLen <= 0)
		{
			delete []pSendData;
			return HTTP_ERROR_SENDREQ;
		}
		iHaveSend += iSendLen;
		if(iHaveSend == iHeadlen)
			break;
	}
	delete []pSendData;
	return HTTP_ERROR_NONE;
}

int GCHttpClient::ReadSocketNodeData()
{
	FD_ZERO(&m_rfds);
	FD_ZERO(&m_wfds);

	m_tv.tv_sec = 0;
	m_tv.tv_usec = 100;

	FD_SET(m_sock,&m_rfds);
	
	int rt = select(m_sock+1, &m_rfds, &m_wfds, 0, &m_tv);
	if (rt < 0)
	{
		if(m_iHttpFileSize == -1)
		{
			return HTTP_ERROR_RECVHTTPRESHEAD;
		}
		else
		{
			return HTTP_ERROR_RECVHTTPFILE;
		}
	}

	if(FD_ISSET(m_sock, &m_rfds))
	{
		long iReadLen = recv(m_sock, m_szRecTmp,MAX_REC_TMP_LEN,0);
		if(iReadLen <= 0)
		{
			if(iReadLen == 0 && m_bServerConnectionClose == true && m_iHttpContentHaveRecLen > 0 && m_iHttpFileSize == 0)	//指定需要特殊判断接受长度为0情况
			{
				m_bRecOK = true;
				return HTTP_ERROR_NONE;
			}
			if(m_iHttpFileSize == -1)
			{
				return HTTP_ERROR_RECVHTTPRESHEAD;
			}
			else
			{
				return HTTP_ERROR_RECVHTTPFILE;
			}
		}
		else
		{
			if(m_iHttpFileSize == -1)//还没接收完包头，找包头的结束符
			{
				for(int i = 0;i<iReadLen-3;i++)
				{
					if(m_szRecTmp[i] == '\r' && m_szRecTmp[i+1] == '\n'
						&& m_szRecTmp[i+2] == '\r' && m_szRecTmp[i+3] == '\n')
					{
						memcpy(m_szHttpHeadRes+m_iHttpHeadHaveRecLen,m_szRecTmp,i+4);

						m_iHttpHeadHaveRecLen += i+4;

						if(iReadLen-i-4 > 0)	//有的多的数据就是包体了
						{
							memcpy(m_pBlockResMsg,m_szRecTmp+i+4,iReadLen-i-4);
							m_iHttpContentHaveRecLen = iReadLen-i-4;
						}

						int iRtSize = GetHttpFileSize();	//检查包头中的长度字段
						if(iRtSize > 0)
						{
							return iRtSize;
						}
						if(m_iHttpFileSize == 0)	//获取不到包大小的话，还是得继续读取
						{
							if(iReadLen < MAX_REC_TMP_LEN && m_bServerConnectionClose == false)	//且如果本身就没读取满BUFFER，则算读取结束
							{
								m_bRecOK = true;
								return HTTP_ERROR_NONE;
							}
							else	//否则继续读取
							{
								return HTTP_ERROR_NONE;
							}
						}
						return HTTP_ERROR_NONE;
					}
				}
				//没有找到包头结束符，继续读
				memcpy(m_szHttpHeadRes+m_iHttpHeadHaveRecLen,m_szRecTmp,iReadLen);
				m_iHttpHeadHaveRecLen += iReadLen;
				if(m_iHttpHeadHaveRecLen > 512)	//包头不太可能超过256还没有结束了
				{
					return HTTP_ERROR_RECVHTTPRESHEAD;
				}
				return HTTP_ERROR_NONE;

			}
			else	//读取过包头，剩下的肯定就是包体了
			{
				memcpy(m_pBlockResMsg+m_iHttpContentHaveRecLen,m_szRecTmp,iReadLen);
				m_iHttpContentHaveRecLen += iReadLen;

				if(iReadLen < MAX_REC_TMP_LEN && m_iHttpFileSize == 0 && m_bServerConnectionClose == false)	//如果本身就没读取满BUFFER，则算读取结束
				{
					m_bRecOK = true;
					return HTTP_ERROR_NONE;
				}
			}
			
		}
	}
	return HTTP_ERROR_NONE;
}



int GCHttpClient::GetHttpFileSize()
{
	m_iHttpFileSize = 0;

	char szValue[30];
	//int iPos = -1;

	if(strstr(m_szHttpHeadRes,"200 OK") == NULL && strstr(m_szHttpHeadRes,"206 Partial Content") == NULL)
	{
		if(strstr(m_szHttpHeadRes,"302 Found"))	//302缓存错误特殊处理
		{
			return HTTP_ERROR_302FOUND;
		}
		else
		{
			return HTTP_ERROR_GETHTTPFILESIZE;
		}
	}

	char *pBegin = strstr(m_szHttpHeadRes,"Content-Length");
	if(pBegin)
	{
		pBegin += strlen("Content-Length");
		pBegin += 2;
		char *pEnd = strstr(pBegin,"\r\n");
		if(pEnd)
		{
			strncpy(szValue,pBegin,pEnd-pBegin);
			szValue[pEnd-pBegin] = '\0';
			m_iHttpFileSize = atoi(szValue);
		}
	}

	if(strstr(m_szHttpHeadRes,"Connection: close"))	//服务器要求关闭的话，则标记关闭
	{
		m_bServerConnectionClose = true;
	}
	
	return HTTP_ERROR_NONE;
}
