#ifndef _GameLayerMove_H_
#define _GameLayerMove_H_
#include "LobbyLayer.h"
#include "GameKind.h"
#include "GameTable.h"
#include "gameShowHand/ShGameViewLayer.h"
#include "gameOx2/Ox2GameViewLayer.h"
#include "GameSetingLayer.h"
#include "GameUserInfo.h"
#include "LoginLayer.h"

class GameLayerMove
{
public:
	GameLayerMove(void);
	~GameLayerMove(void);
	static GameLayerMove* sharedGameLayerMoveSink();
	
	void SetLobbyLayer(LobbyLayer* lobbylayer);
	void SetGameKind(GameKind* gamekind);
	void SetGameTable(GameTable* gametable);
    void SetLoginLayer(LoginLayer* loginlayer);
    
    void loginGameServerSuccess();
	void GoTableToGame(bool _move = true);
	
    void QuitGame();
    void GoGameToTable();
    void GoGameToKind();
	void GoKindToTable();
    void GoKindToGame();
	void GoTableToKind();
    void CloseGame();
	int  GetLayerIndex();
    void EnterRoomList();
    
	void InitTable();
	void ReloadTable(CMD_GR_TableStatus* TableStatus);
	void InitTableStatus(CMD_GR_TableInfo* TableStatus);

	void SetGameID(int gameid);
	void StartGame(bool _move = true);

	void AddEnterRoomTime();
	void KillEnterRoomTime(bool removeLoad = true);

	void AddEnterGameTime();
	void KillEnterGameTime();

	void CallCloseRoom(char* text);

	void SetGameName(const char* gamename, const char* roomname);

	void OpenSeting();
	void ClearSeting();
	void CloseSeting();

	void OpenUserInfo();
	void ClearUserInfo();
	void CloseUserInfo();

	void UpdataLobbyScore();
    void OnIosRechargeSuccess(int payType);
    
	void OpenGetScore();
	void CloseGetScore();
	void ClearGetScore();
	void FlashGetScore();

    void IosWxSucCallBack(const char* unionid, int code);
    void IosAuditStateCallBack();
    void AddLoading();
    void ClearLoading();
    void PushWinodow(const char* reson);
    
private:
	static GameLayerMove* m_GameLayerMove;

public:
	LobbyLayer*				 m_LobbyLayer;
	GameKind*				 m_GameKind;
	GameTable*				 m_GameTable;
	GameSetingLayer*		 m_GameSetingLayer;
	GameLayer*				 m_CurGameLayer;
	GameUserInfo*			 m_GameUserInfo;
	GameGetScore*			 m_GetScoreLayer;
    LoginLayer*              m_LoginLayer;
    
	int						 m_LayerIndex;
    bool                     m_BackBtnEnable;
	int						 m_CurGameKindId;
	char					 m_CurGameName[256];
	char					 m_CurRoomName[128];
};

#endif
