#ifndef _GameBANKSUC_H_
#define _GameBANKSUC_H_

#include "GameScene.h"
#include "cocos-ext.h"

//银行
class GameBankSuc : public GameLayer
{
public:
	virtual bool init();  
	static GameBankSuc *create(GameScene *pGameScene);
	virtual ~GameBankSuc();
	GameBankSuc(GameScene *pGameScene);
	GameBankSuc(const GameBankSuc&);
	GameBankSuc& operator = (const GameBankSuc&);
	enum Tag
	{
		_BtnCancel,
		_BtnOk,
	};

public:
    ui::Scale9Sprite *	m_BackSpr;
	

public:
	virtual void onEnter();
	virtual void onExit();
	virtual bool onTouchBegan(Touch *pTouch, Event *pEvent){return true;}
	virtual void onTouchEnded(Touch *pTouch, Event *pEvent){}
	virtual void onTouchMoved(Touch *pTouch, Event *pEvent){}

	virtual void callbackBt(Ref *pSender);

	Menu* CreateButton( std::string szBtName ,const Vec2 &p , int tag );
	void close();
	void onRemove();
	void SetData(int srcID, const char* scrName,int desID, const char* desName,LONGLONG score, LONGLONG lRevenue, const char * time);
	void SwitchScoreString(LONGLONG lScore, char* pszBuffer, WORD wBufferSize);
	string change(LONGLONG num);
	int count(int input);
	string all_lj(LONGLONG num);
};
#endif
