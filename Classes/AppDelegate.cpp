#include "AppDelegate.h"
#include "GameScene.h"
#include "SimpleAudioEngine.h"
#include "LoginLayer.h"
#include "LobbyLayer.h"
#include "ClientSocketSink.h"

#include "CCLuaEngine.h"
#include "lua_module_register.h"
#include "CCLuaStack.h"
// extra lua module
#include "cocos2dx_extra.h"
#include "quick-src/lua_extensions/lua_extensions_more.h"
#include "luabinding/lua_cocos2dx_extension_filter_auto.hpp"
#include "luabinding/lua_cocos2dx_extension_nanovg_auto.hpp"
#include "luabinding/lua_cocos2dx_extension_nanovg_manual.hpp"
#include "luabinding/cocos2dx_extra_luabinding.h"
#include "luabinding/HelperFunc_luabinding.h"
#include "lua_custom_net.hpp"
#include "lua_fishgame.hpp"
#include "fishgame/lua_fishgame_manual.h"

#if (CC_TARGET_PLATFORM != CC_PLATFORM_WP8)
#include "CodeIDESupport.h"
#include "Runtime.h"
#endif
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
#include "luabinding/cocos2dx_extra_ios_iap_luabinding.h"
#endif
#if ANYSDK_DEFINE > 0
#include "anysdkbindings.h"
#include "anysdk_manual_bindings.h"
#endif
#include "Encrypt.h"
#include "LobbySocketSink.h"
#include "lua_cocos2dx_quick_manual.hpp"
#include "Convert.h"


static void quick_module_register(lua_State *L)
{
    luaopen_lua_extensions_more(L);
    
    lua_getglobal(L, "_G");
    if (lua_istable(L, -1))//stack:...,_G,
    {
        register_all_quick_manual(L);
        // extra
        luaopen_cocos2dx_extra_luabinding(L);
        register_all_cocos2dx_extension_filter(L);
        register_all_cocos2dx_extension_nanovg(L);
        register_all_cocos2dx_extension_nanovg_manual(L);
        register_all_custom_net(L);
        register_all_fishgame(L);
        register_all_fishgame_manual(L);
        
        luaopen_HelperFunc_luabinding(L);
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
        luaopen_cocos2dx_extra_ios_iap_luabinding(L);
#endif
    }
    lua_pop(L, 1);
}

int getDeviceNoXS(lua_State *L)
{
      char szMachineID[33];
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    char* p = const_cast<char*>(Application::getInstance()->GetUUID());
    if(p == NULL)  sprintf(szMachineID,"%s","test");
    else  CMD5Encrypt::EncryptData(p, szMachineID);
#else
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    char* p = JniSink::share()->GetDeviceID();
    if(p == NULL)  sprintf(szMachineID,"%s","test");
    else  CMD5Encrypt::EncryptData(p, szMachineID);
#endif
#endif
    std::string deviceNo = szMachineID;
    lua_pushstring(L, deviceNo.c_str());
    
    return true;
}


int utf8_gbk_lua(lua_State *L)
{
    const char *utf8Str = luaL_checkstring(L, 1);
    std::string gbkStr = utf8_gbk(utf8Str).c_str();
    const char *cgbkStr = gbkStr.c_str();
    lua_pushstring(L, cgbkStr);
    lua_pushinteger(L, gbkStr.length());
    return 2;
}

int gbk_utf8_lua(lua_State *L)
{
    if (lua_istable(L, -1) == false) return 0;
    size_t len = lua_objlen(L, -1);
    char *data = new char[len + 1];
    
    for (int i = 1; i <= len; ++i)
    {
        lua_rawgeti(L, -1, i);
        data[i - 1] = lua_tointeger(L, -1);
        lua_pop(L, 1);
    }
    
    data[len] = 0;
    
    std::string uf8Str = gbk_utf8(data).c_str();
    const char *cuf8Str = uf8Str.c_str();
    lua_pushstring(L, cuf8Str);
    return 1;
}

USING_NS_CC;

AppDelegate::AppDelegate():
_launchMode(0)
{
    
}

AppDelegate::~AppDelegate()
{
#if CC_TARGET_PLATFORM == CC_PLATFORM_WIN32
    endRuntime();
#elif (COCOS2D_DEBUG > 0 && CC_CODE_IDE_DEBUG_SUPPORT > 0)
    // NOTE:Please don't remove this call if you want to debug with Cocos Code IDE
    if (_launchMode)
    {
        endRuntime();
    }
#endif
}

//if you want a different context,just modify the value of glContextAttrs
//it will takes effect on all platforms
void AppDelegate::initGLContextAttrs()
{
    //set OpenGL context attributions,now can only set six attributions:
    //red,green,blue,alpha,depth,stencil
    GLContextAttrs glContextAttrs = {8, 8, 8, 8, 24, 8};

    GLView::setGLContextAttrs(glContextAttrs);
}

class LogoutHandler : public Ref
{
public:
    static LogoutHandler* getInstance()
    {
        static LogoutHandler* instance_ = new LogoutHandler();
        return instance_;
    }
    
    void LogoutJavaResult_update(float ft)
    {
        auto engine = LuaEngine::getInstance();
        ScriptEngineManager::getInstance()->setScriptEngine(engine);
        engine->getLuaStack()->executeString("uiManager:runScene(\"MainScene\")");
    }
    
    void LogoutJavaResult(void)
    {
        Director::getInstance()->getScheduler()->schedule(schedule_selector(LogoutHandler::LogoutJavaResult_update), this, 1, 1, 1, false);
    }
};


int LogoutJavaResult(lua_State* L)
{
    LogoutHandler::getInstance()->LogoutJavaResult();
    return 0;
}

class LuaExecuteWrapper : public Ref
{
public:
    static LuaExecuteWrapper* getInstance()
    {
        static LuaExecuteWrapper* instance_ = new LuaExecuteWrapper();
        return instance_;
    }
    void update(float)
    {
        std::lock_guard<std::mutex> l(lock_);
        auto engine = LuaEngine::getInstance();
        ScriptEngineManager::getInstance()->setScriptEngine(engine);
        while (!scripts_.empty())
        {
            auto script = scripts_.front();
            engine->getLuaStack()->executeString(script.c_str());
            scripts_.pop_front();
        }
    }
    void RunScriptInUIThread(const std::string& lua_script)
    {
        std::lock_guard<std::mutex> l(lock_);
        scripts_.push_back(lua_script);
        Director::getInstance()->getScheduler()->schedule(schedule_selector(LuaExecuteWrapper::update), this, 1, 1, 1, false);
    }
private:
    std::mutex lock_;
    std::deque<std::string> scripts_;
};

int RunScriptInUIThread(lua_State* L)
{
    std::string script = lua_tostring(L, 1);
    LuaExecuteWrapper::getInstance()->RunScriptInUIThread(script);
    return 0;
}

int backToMainScene(lua_State* L)
{
    auto director = Director::getInstance();
    auto glview = director->getOpenGLView();
    glview->setDesignResolutionSize(designResolutionSize.width, designResolutionSize.height, ResolutionPolicy::EXACT_FIT);
    SoundUtil::sharedEngine()->playBackMusic("dating", true);
    SoundUtil::sharedEngine()->setBackSoundVolume(g_GlobalUnits.m_fBackMusicValue);
    LobbyLayer* temp = LobbyLayer::create(GameScene::create());
    Director::getInstance()->replaceScene(temp->m_pGameScene);
    temp->showUserInfo();
    temp->SendHeart();
    temp->SendBoardCastReq();
    return 0;
}

// If you want to use packages manager to install more packages,
// don't modify or remove this function
static int register_all_packages()
{
    return 0; //flag for packages manager
}

bool AppDelegate::applicationDidFinishLaunching() {
    // initialize director
    auto director = Director::getInstance();
    auto glview = director->getOpenGLView();
    if(!glview) {
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32) || (CC_TARGET_PLATFORM == CC_PLATFORM_MAC) || (CC_TARGET_PLATFORM == CC_PLATFORM_LINUX)
        glview = GLViewImpl::createWithRect("998game", Rect(0, 0, designResolutionSize.width, designResolutionSize.height));
#else
        glview = GLViewImpl::create("998game");
#endif
        director->setOpenGLView(glview);
    }
    
    vector<string> searchPath;
    
    searchPath.push_back(mediumResource.directory);
    searchPath.push_back("../resources");
	searchPath.push_back("../resources/res");
    searchPath.push_back("../assets");
    director->setContentScaleFactor(MIN(_STANDARD_SCREEN_SIZE_.height/designResolutionSize.height, _STANDARD_SCREEN_SIZE_.width/designResolutionSize.width));
    
    FileUtils::getInstance()->setSearchPaths(searchPath); 
    
    // turn on display FPS
    director->setDisplayStats(false);

    // set FPS. the default value is 1.0/60 if you don't call this
    director->setAnimationInterval(1.0 / 50);

    // Set the design resolution
    glview->setDesignResolutionSize(designResolutionSize.width, designResolutionSize.height, ResolutionPolicy::EXACT_FIT);
    Size frameSize = glview->getFrameSize();
    // if the frame's height is larger than the height of medium size
    register_all_packages();

    // create a scene. it's an autorelease object
   	LobbyLayer* temp = LobbyLayer::create(GameScene::create());

    // run
    director->runWithScene(temp->m_pGameScene);

    
#if (COCOS2D_DEBUG > 0 && CC_CODE_IDE_DEBUG_SUPPORT > 0)
    // NOTE:Please don't remove this call if you want to debug with Cocos Code IDE
    if (_launchMode)
    {
        initRuntime();
    }
#endif
    
    
    auto engine = LuaEngine::getInstance();
    ScriptEngineManager::getInstance()->setScriptEngine(engine);
    lua_State* L = engine->getLuaStack()->getLuaState();
    lua_module_register(L);
    
    // use Quick-Cocos2d-X
    quick_module_register(L);
    
    LuaStack* stack = engine->getLuaStack();
    stack->executeString("require 'src/loadLuaSrc'");
    
    tolua_function(stack->getLuaState(), "LogoutJavaResult", LogoutJavaResult);
    tolua_function(stack->getLuaState(), "RunScriptInUIThread", RunScriptInUIThread);
    tolua_function(stack->getLuaState(), "backToModeScene", backToMainScene);
    tolua_function(stack->getLuaState(), "getDeviceNoXS", getDeviceNoXS);
    tolua_function(stack->getLuaState(), "utf8_gbk", utf8_gbk_lua);
    tolua_function(stack->getLuaState(), "gbk_utf8", gbk_utf8_lua);
    
#if (COCOS2D_DEBUG > 0 && CC_CODE_IDE_DEBUG_SUPPORT > 0)
    // NOTE:Please don't remove this call if you want to debug with Cocos Code IDE
    _launchMode = 0;
    if (_launchMode)
    {
        startRuntime();
    }
#else
    stack->setXXTEAKeyAndSign("vincent", strlen("vincent"), "vincent", strlen("vincent"));
    stack->loadChunksFromZIP("res/framework.zip");
#endif
    return true;
}

// This function will be called when the app is inactive. When comes a phone call,it's be invoked too
void AppDelegate::applicationDidEnterBackground()
{
    Director::getInstance()->stopAnimation();
    // if you use SimpleAudioEngine, it must be pause
    CocosDenshion::SimpleAudioEngine::getInstance()->setEffectsVolume(0);
    CocosDenshion::SimpleAudioEngine::getInstance()->setBackgroundMusicVolume(0);
    Director::getInstance()->getEventDispatcher()->dispatchCustomEvent("APP_ENTER_BACKGROUND_EVENT");
    CocosDenshion::SimpleAudioEngine::getInstance()->pauseAllEffects();
}

// this function will be called when the app is active again
void AppDelegate::applicationWillEnterForeground()
{
    Director::getInstance()->startAnimation();
    // if you use SimpleAudioEngine, it must resume here
    CocosDenshion::SimpleAudioEngine::getInstance()->setEffectsVolume(g_GlobalUnits.m_fBackMusicValue);
    CocosDenshion::SimpleAudioEngine::getInstance()->setBackgroundMusicVolume(g_GlobalUnits.m_fSoundValue);
    CocosDenshion::SimpleAudioEngine::getInstance()->resumeAllEffects();
    if (ClientSocketSink::sharedSocketSink()->m_bClientReady == false)
    {
        CocosDenshion::SimpleAudioEngine::getInstance()->stopBackgroundMusic();
    }
    Director::getInstance()->getEventDispatcher()->dispatchCustomEvent("APP_ENTER_FOREGROUND_EVENT");
}

void AppDelegate::setLaunchMode(int mode)
{
	_launchMode = mode;
}
