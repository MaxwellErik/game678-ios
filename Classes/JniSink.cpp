﻿#include "JniSink.h"
#include "CCJniHelper.h"
#include "jsoncpp.h"
#include "FrameGameView.h"
#include "GlobalUnits.h"
#include "LocalDataUtil.h"
#include "AlertMessageLayer.h"
#include "GameLayerMove.h"


#if(CC_TARGET_PLATFORM != CC_PLATFORM_ANDROID)
#include "NSObjectCHelper.h"
#include "systemInfo.h"
#endif


JniSink *JniSink::m_pJniSink = NULL;

JniSink::JniSink(void)
{
	m_pIGameView = NULL;
	m_dwUserID = 0;
	m_wBuyID = 0;
	m_cbType = 0;
	m_lScore = 0;
	m_lMoney = 0;
	m_pJniCallback = NULL;
	sprintf(m_DeviceID,"test");
}


JniSink::~JniSink(void)
{

}

JniSink *JniSink::share(void)
{
	if (m_pJniSink == NULL)
	{
		m_pJniSink = new JniSink;
	}
	return m_pJniSink;
}

void JniSink::shareWeixin( const char *szUrl , const char *path , const char *title , const char *desc )
{
#ifdef _WIN32
	ShareCallback("1" , 1);
#else
	char szBuffer[2560]={0};
	sprintf(szBuffer , "%s,%s,%s,%s" , szUrl , path, title, desc);
	callJavaCommand(CMD_CTJ_WX , szBuffer);
#endif
}

void JniSink::shareWeixin( const char *path )
{
#ifdef _WIN32
	ShareCallback("1" , 1);
#else
	callJavaCommand(CMD_CTJ_WX , path);
#endif
}

void JniSink::shareWeibo( const char *szUrl , const char *path , const char *title , const char *desc )
{
#ifdef _WIN32
	ShareCallback("1" , 2);
#else
	char szBuffer[2560]={0};
	sprintf(szBuffer , "%s,%s,%s,%s" , szUrl , path, title, desc);
	callJavaCommand(CMD_CTJ_WB , szBuffer);
#endif
}

void JniSink::ShareNewWX(int userid, int ShareType, int ShareFriend, char* Title)
{
	char szBuffer[2560]={0};
	sprintf(szBuffer , "%d,%d,%d,%s" , userid , ShareType, ShareFriend, Title);
	callJavaCommand(CMD_CTJ_ShareWX , szBuffer);
}

void JniSink::shareWeibo( const char *path )
{
#ifdef _WIN32
	ShareCallback("1" , 2);
#else
	callJavaCommand(CMD_CTJ_WB , path);
#endif
}

//分享回调
void JniSink::ShareCallback(const char *szParam , int type)
{

}

void JniSink::qqLoginAuth()
{
	{
		Json::Reader reader;
		Json::Value root;
		if  (!reader.parse("{\"ret\":0,\"ordernum\":\"IOS01161733243110\"}", root, false )) {
			CCAssert(false, "Json::Reader Parse error!");
		}
		bool b = root["ret"].isNull();
		std::string str1 = root["ret"].asString();
		std::string str = root["msg"].asString();
	}

	return;
	Json::Reader reader;
	Json::Value root;
	if  (!reader.parse("{\"ret\":0,\"ordernum\":\"IOS01021402137778\"}", root, false )) 
	{
		CCAssert(false, "Json::Reader Parse error!");
	}
	int iRet = root["ret"].asInt();
	std::string str1 = root["ordernum"].asString();

	std::string str;
	unsigned int id;
	unsigned int outtime;
	for (int i=0; i<(int)root["msg"].size(); i++)
	{
		str = root["msg"][i]["content"].asString();
		id = root["msg"][i]["id"].asUInt();
		outtime = root["msg"][i]["outtime"].asUInt();
	}

	callJavaCommand(CMD_CTJ_QQAUTHLOGIN , "");
}

void JniSink::qqUserInfo()
{
	callJavaCommand(CMD_CTJ_QQUSERINFO , "");
}

void JniSink::qqLoginAuthCallback(const char *szParam)
{

}

void JniSink::qqUserInfoCallback(const char *szParam)
{

}

void JniSink::CTJ_WXLogin()
{
	char szParams[128]={0};
	callJavaCommand(CMD_CTJ_WXLogin, szParams);
}

void JniSink::OpenWX()
{
	char szParams[128]={0};
	callJavaCommand(CMD_CTJ_OpenWX, szParams);
}

void JniSink::UpdataGame()
{
	char szParams[128]={0};
	callJavaCommand(CMD_CTJ_UpdataGame, szParams);
}

void JniSink::SendLockPhone(int userId)
{
	char szParams[128]={0};
	sprintf(szParams, "%d", userId);
	callJavaCommand(CMD_CTJ_LockPhone, szParams);
}

void JniSink::sendPatchOrder( const char *szOrderNum, int score, int money)
{
	m_lScore = score;
	m_lMoney = money;

	callJavaCommand(CMD_CTJ_PAY_PATCH, szOrderNum);
}

void JniSink::SetChannelID(const char *szParam)
{
    if (0 == strcmp(szParam, "bdtb"))
        g_GlobalUnits.m_ChannelID = 101;
    else if (0 == strcmp(szParam, "aqy"))
        g_GlobalUnits.m_ChannelID = 102;
    else if (0 == strcmp(szParam, "bdjj"))
        g_GlobalUnits.m_ChannelID = 103;
    else if (0 == strcmp(szParam, "zht"))
        g_GlobalUnits.m_ChannelID = 104;
    else if (0 == strcmp(szParam, "jrtt"))
        g_GlobalUnits.m_ChannelID = 105;
    else if (0 == strcmp(szParam, "mm"))
        g_GlobalUnits.m_ChannelID = 106;
    else if (0 == strcmp(szParam, "uc"))
        g_GlobalUnits.m_ChannelID = 107;
    else if (0 == strcmp(szParam, "wx"))
        g_GlobalUnits.m_ChannelID = 109;
}

void JniSink::SetMobileInfo(const char *szParam)
{
    char temp[256];
    memset(temp, 0, sizeof(temp));
    sprintf(temp, "%s", szParam);
    
    char delims[] = ",";

    m_MobileKind = strtok(temp, delims);
    m_MobileBrand = strtok(NULL, delims);
    m_SystemVersion = strtok(NULL, delims);
}

std::string JniSink::GetMobileBrand()
{
    return m_MobileBrand;
}

std::string JniSink::GetMobileKind()
{
    return m_MobileKind;
}

std::string JniSink::GetMobileSystemVersion()
{
    return m_SystemVersion;
}

void JniSink::SetDeviceID(const char *szParam)
{
    ZeroMemory(m_DeviceID,128);
    strcpy(m_DeviceID, szParam);
    if (strcmp(m_DeviceID,"") == 0)
    {
        sprintf(m_DeviceID,"test");
    }
}

char* JniSink::GetDeviceID()
{
	return m_DeviceID;
}

void JniSink::javaCallback( int cmd , const char * szParam )
{
	switch (cmd)
	{
    case CMD_JTC_ChannelId:
        {
            JniSink::SetChannelID(szParam);
            break;
        }
	case CMD_JTC_WXLogin:	//微信登陆
		{
			if(m_pJniCallback != NULL) m_pJniCallback->WXLoginSuc(szParam);
			break;
		}
	case CMD_JTC_GetDeviceID:
		{
			JniSink::SetDeviceID(szParam);
			break;
		}
    case CMD_JTC_GetMobileInfo:
        {
            JniSink::SetMobileInfo(szParam);
            break;
        }
        case CMD_JTC_QQAUTHLOGIN:	//QQ认证回调
		JniSink::qqLoginAuthCallback(szParam);
		break;
	case CMD_JTC_QQUSERINFO:	//QQ回调
		JniSink::qqUserInfoCallback(szParam);
		break;
	case CMD_JTC_PAY:			//支付成功回调
		JniSink::androidPaySuccess(szParam);
		break;
	case CMD_JTC_PAY_ERROR:		//支付失败回调
		JniSink::androidPayError(szParam);
		break;
	case CMD_JTC_MILOGIN:		//小米登录回调
#if (GAME_PLATFORM == PLATFORM_XM)
		JniSink::XiaoMiLoginAuthCallback(szParam);
#endif
		break;
	case CMD_JTC_READCONTACTS:	//读取联系人回调
		break;
	case CMD_JTC_SENDMSG:		//发送短信回调
		break;
	case CMD_JTC_PAY_GETORDERNUM:	//返回订单编号
		GetOrderNumcallback(szParam);
		break;
	case CMD_JTC_WX:
		//JniSink::ShareCallback(szParam , 1);
		break;
	case CMD_JTC_WX_PAY:	//微信支付回调
		JniSink::WXPayCallBack(szParam);
		break;
    case CMD_JTC_GetVersion:
        {
            if(m_pJniCallback != NULL) m_pJniCallback->GetAndroidVersionSuc(szParam);
            break;
        }
	}
}

void JniSink::WXPayCallBack(const char * szParam)
{
    if (m_pJniCallback != NULL)
    {
        m_pJniCallback->paycallback(szParam);
    }
}


void JniSink::SetXMLcallback( std::string str )
{
	m_strXML = str;
}

void JniSink::GetOrderNumcallback(const char *szParam)
{
	g_GlobalUnits.AddBuyRecord(m_dwUserID, m_wBuyID , m_cbType, (TCHAR *)szParam, m_lScore, m_lMoney);
	if (m_pJniCallback != NULL)
	{		
		m_pJniCallback->ordernumcallback(szParam);
	}

#if(CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
	androidPaySuccess(szParam);
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
	
	char szBuf[1024];
	sprintf(szBuf, "%s,%lu,%ld,%ld,%d.%d.%d.%d,%d,%d", szParam, m_dwUserID, m_lMoney, m_lScore, 
		VER_PLAZA_HIGH, VER_PLAZA_HIGH, VER_PLAZA_LOW, VER_PLAZA_LOW,m_iPayType, m_iPlatform);
	std::string str = szBuf;
	if (m_iPayType == PAY_TYPE_BANKONLINE)//银行卡支付需要加上XML报文
	{
		str += ",";
		str += m_strXML;
	}
	NSObjectCHelper::sharedNSObjectCHelper()->callObjectCCommand(CMD_CTJ_PAY, str.c_str());
#endif
}

//充值成功
void JniSink::androidPaySuccess( const char *szParam )
{
	if (m_pJniCallback != NULL)
	{
		m_pJniCallback->paycallback(szParam);
	}
}

void JniSink::androidPayError( const char *szParam )
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
	LoadLayer::removeLoadLayer(Director::getInstance()->getRunningScene());
#endif
}

void JniSink::sendWXPayOrder(int userId, int gameId, int money)
{
#if(CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
	char szParams[128] = { 0 };
	sprintf(szParams, "%d,%d,%d", userId, gameId, money); //以后传url，由java那边进行调用
	callJavaCommand(CMD_CTJ_WX_PAY, szParams);
#endif
}

void JniSink::SendGooglePay(int userid, int money, int score, char* password)
{
#if(CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
	char szParams[128] = { 0 };
	sprintf(szParams, "%d,%d,%d,%s", userid, money, score,password); //以后传url，由java那边进行调用
	callJavaCommand(CMD_CTJ_GooglePay, szParams);
#endif
}

//参数payType（1微信 2支付宝 3短信 4小米充值）
void JniSink::sendPayOrder(int userId , int gameId, int money , int score , const char* szVersion ,int payType)
{
	char szParams[128]={0};
	sprintf(szParams, "%d,%d,%d,%d,%d.%d.%d.%d,%d", userId, gameId, money, score,
		 VER_PLAZA_HIGH, VER_PLAZA_HIGH, VER_PLAZA_LOW, VER_PLAZA_LOW,payType);
	callJavaCommand(CMD_CTJ_PAY, szParams);
}

void JniSink::SendToDeviceID()
{
	callJavaCommand(CMD_CTJ_GetDeviceID,"android");
}

void JniSink::GetAndroidVersion()
{
    callJavaCommand(CMD_CTJ_GetVersion,"android");
}

void JniSink::SendLoginSuccess(const char *userd)
{
    callJavaCommand(CMD_CTJ_LOGIN, userd);
}

void JniSink::SendRegisterSuccess(const char *userd)
{
    callJavaCommand(CMD_CTJ_REGISTER, userd);
}

void JniSink::CopyID(int _ID)
{
	char szParams[128]={0};
	sprintf(szParams, "%d", _ID);
	callJavaCommand(CMD_COPY_ID, szParams);
}

void JniSink::CloseGameWin()
{
	char szParams[128]={0};
	callJavaCommand(CMD_CloseGame, szParams);
}

void JniSink::sendPayOrder(int userId , int money , int score , const char* szVersion ,int payType, int platform , const char * szCardNum , const char * szCardPwd)
{
	if (userId != 0)
	{
		m_cbType = 1;
	}
	else
	{
		m_cbType = 2;
	}
	m_lScore = score;
	m_lMoney = money;
	m_dwUserID = userId;
	m_iPayType = payType;
	m_iPlatform = platform;
	char szParams[256]={0};
	sprintf(szParams, "%d,%d,%d,%d.%d.%d.%d,%d,%d,%s,%s", userId, money, score, VER_PLAZA_HIGH, VER_PLAZA_HIGH, VER_PLAZA_LOW, VER_PLAZA_LOW, payType, platform , szCardNum , szCardPwd);
	callJavaCommand(CMD_CTJ_PAY, szParams);
}


void JniSink::sendPayOrder( int userId, WORD id, int money , int score , const char* szVersion ,int payType, int platform )
{
	if (userId != 0)
	{
		m_cbType = 1;
	}
	else
	{
		m_cbType = 2;
	}
	if (payType == 2)//支付宝多送5%
	{
		score = score + score*5/100;
	}
	m_wBuyID = id;
	m_lScore = score;
	m_lMoney = money;
	m_dwUserID = userId;
	m_iPayType = payType;
	m_iPlatform = platform;
//	sendPayOrder(userId, money, score, szVersion, payType, platform);
}

void JniSink::vibrate( int iTime )
{
	char szParams[128]={0};
	sprintf(szParams , "%d" , iTime);
#if(CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
	callJavaCommand(CMD_CTJ_VIBRATE , szParams);
#elif(CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
	NSObjectCHelper::sharedNSObjectCHelper()->callObjectCCommand(CMD_CTJ_VIBRATE , szParams);
	
#endif
}

void JniSink::OpenWebView( char *szURL )
{
#if(CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    callJavaCommand(CMD_CTJ_BROWSE, szURL);
#elif(CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    SystemInfo::openURL(szURL);
#endif
}



void JniSink::OpenWebView( char *szURL, int x, int y, int iWidth, int iHeight )
{
	char szParams[128]={0};
	sprintf(szParams, "%s,%d,%d,%d,%d", szURL, x, y, iWidth, iHeight);
	callJavaCommand(CMD_CTJ_BROWSE, szParams);
#if(CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
	callJavaCommand(CMD_CTJ_BROWSE, szParams);
#elif(CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
	NSObjectCHelper::sharedNSObjectCHelper()->callObjectCCommand(CMD_CTJ_BROWSE , szParams);
#endif
}

void JniSink::CloseWebView()
{
	callJavaCommand(CMD_CTJ_BROWSE_CLOSE, "");
}

void JniSink::SendShortMessage( std::string strNum, std::string strContent )
{
	
}

void JniSink::OpenSystemWeb(const char *szURL )
{
#if(CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
	callJavaCommand(CMD_CTJ_SYSTEM_BROWSE, szURL);
#elif(CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    SystemInfo::openURL(szURL);
#endif
	
}

#if (GAME_PLATFORM == PLATFORM_XM)
void JniSink::XiaoMiLoginAuth()
{
	callJavaCommand(CMD_CTJ_MILOGIN, "");
#ifdef _WIN32
	XiaoMiLoginAuthCallback("f3fd2,22");
#endif
}

void JniSink::XiaoMiLoginAuthCallback( const char *szParam )
{
    char szNickName[NAME_LEN];
    char szOpenID[OPEN_LEN];
    std::string str(szParam);
    int index = str.find(',');
    ZeroMemory(szOpenID,sizeof(szOpenID));
    CopyMemory(szOpenID, str.c_str(), index);
    ZeroMemory(szNickName,sizeof(szNickName));
    CopyMemory(szNickName, str.c_str()+index+1, str.length()-index-1);
    if (strlen(szNickName) == 0)
    {
        CopyMemory(szNickName, szOpenID, NAME_LEN-1);
    }

    strcpy(g_GlobalUnits.GetGolbalUserData().szOpenID, szOpenID);
    strcpy(g_GlobalUnits.GetGolbalUserData().szNickName, szNickName);
}
#endif
