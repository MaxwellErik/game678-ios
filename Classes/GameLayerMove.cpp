#include "GameLayerMove.h"
#include "ClientSocketSink.h"
#include "gameShowHand/ShGameViewLayer.h"
#include "gameOx4/Ox4GameViewLayer.h"
#include "gameOx6/Ox6GameViewLayer.h"
#include "gameBRNN/BRNNGameViewLayer.h"
#include "gameBR30S/BR30SGameViewLayer.h"
#include "gamesk/SKGameViewLayer.h"
#include "cocos2d.h"
#include "GameSetingLayer.h"
#include "gameSDB/SDBGameViewLayer.h"
#include "GameGetScore.h"
#include "gameHHSW/HHSWGameViewLayer.h"
#include "AlertMessageLayer.h"
#include "gameWZMJ/WZMJGameViewLayer.h"
#include "gameWZLZ/WZLZGameViewLayer.h"
#include "gameDDZ/DDZGameViewLayer.h"

GameLayerMove*	GameLayerMove::m_GameLayerMove=NULL;

GameLayerMove::GameLayerMove(void)
{
	m_LobbyLayer = NULL;
	m_GameKind = NULL;
	m_GameTable = NULL;
	m_LayerIndex = 0;
	m_CurGameKindId = 0;
	m_CurGameLayer = NULL;
	m_GameSetingLayer = NULL;
	m_GameUserInfo = NULL;
	m_GetScoreLayer = NULL;
    m_LoginLayer = NULL;
    m_BackBtnEnable = true;
}


GameLayerMove::~GameLayerMove(void)
{
}

void GameLayerMove::FlashGetScore()
{
	if (m_GetScoreLayer != NULL)
	{
		m_GetScoreLayer->FlashScore();
	}
}

void GameLayerMove::CloseGetScore()
{
	if (m_GetScoreLayer != NULL)
	{
		m_GetScoreLayer->onRemove();
		m_GetScoreLayer = NULL;
	}
}

void GameLayerMove::ClearGetScore()
{
	m_GetScoreLayer = NULL;
}

void GameLayerMove::OpenGetScore()
{
	if (m_GetScoreLayer == NULL)
	{
		m_GetScoreLayer = GameGetScore::create(m_LobbyLayer->m_pGameScene);
	}
	else
	{
		m_GetScoreLayer->FlashScore();
	}
}

void GameLayerMove::OpenSeting()
{
	if (m_GameSetingLayer == NULL)
	{
		m_GameSetingLayer = GameSetingLayer::create(m_LobbyLayer->m_pGameScene);
	}
}

void GameLayerMove::CloseSeting()
{
	if (m_GameSetingLayer != NULL)
	{
		m_GameSetingLayer->actionMin();
		m_GameSetingLayer = NULL;
	}
}

void GameLayerMove::ClearSeting()
{
	m_GameSetingLayer = NULL;
}

void GameLayerMove::OpenUserInfo()
{
    auto gameUserInfo = GameUserInfo::create(m_LobbyLayer->m_pGameScene);
    gameUserInfo->SetUserInfo(g_GlobalUnits.GetGolbalUserData().szNickName, g_GlobalUnits.GetGolbalUserData().dwGameID, g_GlobalUnits.GetGolbalUserData().wFaceID);
}

void GameLayerMove::ClearUserInfo()
{
	m_GameUserInfo = NULL;
}

void GameLayerMove::CloseUserInfo()
{
	if (m_GameUserInfo != NULL)
	{
		m_GameUserInfo->onRemove();
		m_GameUserInfo = NULL;
	}
}

void GameLayerMove::UpdataLobbyScore()
{
	if(m_LobbyLayer != NULL) m_LobbyLayer->UpdataLobbyScore();
}

void GameLayerMove::AddEnterGameTime()
{
	if(m_LobbyLayer != NULL) m_LobbyLayer->AddEnterGameTime();
}
void GameLayerMove::KillEnterGameTime()
{
	if(m_LobbyLayer != NULL) m_LobbyLayer->KillEnterGameTime();
}

void GameLayerMove::AddEnterRoomTime()
{
	if(m_LobbyLayer != NULL) m_LobbyLayer->AddEnterRoomTime();
}
void GameLayerMove::KillEnterRoomTime(bool removeLoad)
{
	if(m_LobbyLayer != NULL) m_LobbyLayer->KillEnterRoomTime(removeLoad);
}

GameLayerMove* GameLayerMove::sharedGameLayerMoveSink()
{
	if (m_GameLayerMove == NULL)
	{
		m_GameLayerMove = new GameLayerMove();
	}
	return m_GameLayerMove;
}

void GameLayerMove::StartGame(bool _move)
{
	if (m_CurGameKindId == Dating_GameKind_SH) //梭哈
	{
        if (m_CurGameLayer != NULL)
        {
            m_LobbyLayer->m_pGameScene->removeChild(m_CurGameLayer);
        }
        ShGameViewLayer *pLayer = ShGameViewLayer::create(m_LobbyLayer->m_pGameScene);
        ClientSocketSink::sharedSocketSink()->setFrameGameView(pLayer);
        m_CurGameLayer = pLayer;
        GoTableToGame(_move);

	}
	else if (m_CurGameKindId == Dating_GameKind_Ox2) //2rnn
	{
		if (m_CurGameLayer != NULL)
		{
			m_LobbyLayer->m_pGameScene->removeChild(m_CurGameLayer);
		}
		Ox2GameViewLayer *pLayer = Ox2GameViewLayer::create(m_LobbyLayer->m_pGameScene);
		ClientSocketSink::sharedSocketSink()->setFrameGameView(pLayer);
		m_CurGameLayer = pLayer;
		GoTableToGame(_move);
	}
	else if (m_CurGameKindId == Dating_GameKind_Ox4) //4rnn
	{
        if (m_CurGameLayer != NULL)
        {
            m_LobbyLayer->m_pGameScene->removeChild(m_CurGameLayer);
        }
        Ox4GameViewLayer *pLayer = Ox4GameViewLayer::create(m_LobbyLayer->m_pGameScene);
		ClientSocketSink::sharedSocketSink()->setFrameGameView(pLayer);
		m_CurGameLayer = pLayer;
		GoTableToGame();
	}
	else if (m_CurGameKindId == Dating_GameKind_Ox6) //tbnn
	{
		Ox6GameViewLayer *pLayer = Ox6GameViewLayer::create(m_LobbyLayer->m_pGameScene);
		ClientSocketSink::sharedSocketSink()->setFrameGameView(pLayer);
		m_CurGameLayer = pLayer;
		GoTableToGame();
	}
	else if (m_CurGameKindId == Dating_GameKind_BRNN) //百人牛牛
	{
		BRNNGameViewLayer *pLayer = BRNNGameViewLayer::create(m_LobbyLayer->m_pGameScene);
		ClientSocketSink::sharedSocketSink()->setFrameGameView(pLayer);
		m_CurGameLayer = pLayer;
		GoTableToGame();
	}
	else if (m_CurGameKindId == Dating_GameKind_30S) //百人30秒
	{
		if (m_CurGameLayer != NULL)
		{
			m_LobbyLayer->m_pGameScene->removeChild(m_CurGameLayer);
		}
		BR30SGameViewLayer *pLayer = BR30SGameViewLayer::create(m_LobbyLayer->m_pGameScene);
		ClientSocketSink::sharedSocketSink()->setFrameGameView(pLayer);
		m_CurGameLayer = pLayer;
		GoTableToGame();
	}
	else if (m_CurGameKindId == Dating_GameKind_SDB) //十点半
	{
		if (m_CurGameLayer != NULL)
		{
			m_LobbyLayer->m_pGameScene->removeChild(m_CurGameLayer);
		}
		SDBGameViewLayer *pLayer = SDBGameViewLayer::create(m_LobbyLayer->m_pGameScene);
		ClientSocketSink::sharedSocketSink()->setFrameGameView(pLayer);
		m_CurGameLayer = pLayer;
		GoTableToGame();
	}
	else if (m_CurGameKindId == Dating_GameKind_HHSW) //虎虎生威
	{
		if (m_CurGameLayer != NULL)
		{
			m_LobbyLayer->m_pGameScene->removeChild(m_CurGameLayer);
		}
		HHSWGameViewLayer *pLayer = HHSWGameViewLayer::create(m_LobbyLayer->m_pGameScene);
		ClientSocketSink::sharedSocketSink()->setFrameGameView(pLayer);
		m_CurGameLayer = pLayer;
		GoTableToGame();
	}
    else if (m_CurGameKindId == Dating_GameKind_QBSQ) //千变双扣
    {
        if (m_CurGameLayer != NULL)
        {
            m_LobbyLayer->m_pGameScene->removeChild(m_CurGameLayer);
        }
		SKGameViewLayer *pLayer = SKGameViewLayer::create(m_LobbyLayer->m_pGameScene);
        ClientSocketSink::sharedSocketSink()->setFrameGameView(pLayer);
        m_CurGameLayer = pLayer;
        GoTableToGame();
    }
    else if (m_CurGameKindId == Dating_GameKind_WZMJ) //温州麻将
    {
        if (m_CurGameLayer != NULL)
        {
            m_LobbyLayer->m_pGameScene->removeChild(m_CurGameLayer);
        }
        WZMJGameViewLayer *pLayer = WZMJGameViewLayer::create(m_LobbyLayer->m_pGameScene);
        ClientSocketSink::sharedSocketSink()->setFrameGameView(pLayer);
        m_CurGameLayer = pLayer;
        GoTableToGame();
    }
    else if (m_CurGameKindId == Dating_GameKind_WZLZ) //温州两张
    {
        if (m_CurGameLayer != NULL)
        {
            m_LobbyLayer->m_pGameScene->removeChild(m_CurGameLayer);
        }
        WZLZGameViewLayer *pLayer = WZLZGameViewLayer::create(m_LobbyLayer->m_pGameScene);
        ClientSocketSink::sharedSocketSink()->setFrameGameView(pLayer);
        m_CurGameLayer = pLayer;
        GoTableToGame();
    }
    else if (m_CurGameKindId == Dating_GameKind_DDZ) //斗地主
    {
        if (m_CurGameLayer != NULL)
        {
            m_LobbyLayer->m_pGameScene->removeChild(m_CurGameLayer);
        }
        DDZGameViewLayer *pLayer = DDZGameViewLayer::create(m_LobbyLayer->m_pGameScene);
        ClientSocketSink::sharedSocketSink()->setFrameGameView(pLayer);
        m_CurGameLayer = pLayer;
        GoTableToGame();
    }
    
    if(m_LobbyLayer != NULL) m_LobbyLayer->setVisible(false);
}

void GameLayerMove::SetGameID(int gameid)
{
	m_CurGameKindId = gameid;
}

void GameLayerMove::SetLobbyLayer(LobbyLayer* lobbylayer)
{
	m_LobbyLayer = lobbylayer;
}

void GameLayerMove::SetGameKind(GameKind* gamekind)
{
	m_GameKind = gamekind;
}

void GameLayerMove::SetGameTable(GameTable* gametable)
{
	m_GameTable = gametable;
}

void GameLayerMove::SetLoginLayer(LoginLayer* loginlayer)
{
    m_LoginLayer = loginlayer;
}

void GameLayerMove::IosAuditStateCallBack()
{
    if (m_LoginLayer != NULL)
    {
        m_LoginLayer->IosGetAuditStateResponse();
    }
}

void GameLayerMove::IosWxSucCallBack(const char* unionid, int code)
{
    if (m_LoginLayer != NULL)
    {
        m_LoginLayer->IosWxGoginSuc(unionid, code);
    }
}

void GameLayerMove::OnIosRechargeSuccess(int payType)
{
    m_LobbyLayer->OnIosRechargeSuccess(payType);
}

void GameLayerMove::AddLoading()
{
    if (nullptr != m_LoginLayer)
        LoadLayer::create(m_LoginLayer->m_pGameScene);
}

void GameLayerMove::ClearLoading()
{
    LoadLayer::removeLoadLayer(Director::getInstance()->getRunningScene());
}

void GameLayerMove::PushWinodow(const char* reson)
{
    if (m_LobbyLayer != NULL) {
        AlertMessageLayer::createConfirm(m_LobbyLayer,reson);
    }
}

void GameLayerMove::CloseGame()
{
    ActionInstant *func = nullptr;
    switch (m_CurGameKindId)
    {
        case Dating_GameKind_DDZ:
        {
            func = CallFunc::create(CC_CALLBACK_0(DDZGameViewLayer::OnQuit, (DDZGameViewLayer*)m_CurGameLayer));
            break;
        }
        case Dating_GameKind_SH:
        {
            func = CallFunc::create(CC_CALLBACK_0(ShGameViewLayer::OnQuit, (ShGameViewLayer*)m_CurGameLayer));
            break;
        }
        case Dating_GameKind_Ox2:
        {
            func = CallFunc::create(CC_CALLBACK_0(Ox2GameViewLayer::OnQuit, (Ox2GameViewLayer*)m_CurGameLayer));
            break;
        }
        case Dating_GameKind_Ox4:
        {
            func = CallFunc::create(CC_CALLBACK_0(Ox4GameViewLayer::OnQuit, (Ox4GameViewLayer*)m_CurGameLayer));
            break;
        }
        case Dating_GameKind_Ox6:
        {
            func = CallFunc::create(CC_CALLBACK_0(Ox6GameViewLayer::OnQuit, (Ox6GameViewLayer*)m_CurGameLayer));
            break;
        }
        case Dating_GameKind_BRNN:
        {
            func = CallFunc::create(CC_CALLBACK_0(BRNNGameViewLayer::OnQuit, (BRNNGameViewLayer*)m_CurGameLayer));
            break;
        }
        case Dating_GameKind_30S:
        {
            func = CallFunc::create(CC_CALLBACK_0(::BR30SGameViewLayer::OnQuit, (BR30SGameViewLayer*)m_CurGameLayer));
            break;
        }
        case Dating_GameKind_SDB:
        {
            func = CallFunc::create(CC_CALLBACK_0(::SDBGameViewLayer::OnQuit, (SDBGameViewLayer*)m_CurGameLayer));
            break;
        }
        case Dating_GameKind_HHSW:
        {
            func = CallFunc::create(CC_CALLBACK_0(::HHSWGameViewLayer::OnQuit, (HHSWGameViewLayer*)m_CurGameLayer));
            break;
        }
        case Dating_GameKind_QBSQ:
        {
            func = CallFunc::create(CC_CALLBACK_0(::SKGameViewLayer::OnQuit, (SKGameViewLayer*)m_CurGameLayer));
            break;
        }
        case Dating_GameKind_WZMJ:
        {
            func = CallFunc::create(CC_CALLBACK_0(::WZMJGameViewLayer::OnQuit, (WZMJGameViewLayer*)m_CurGameLayer));
            break;
        }
        case Dating_GameKind_WZLZ:
        {
            func = CallFunc::create(CC_CALLBACK_0(::WZLZGameViewLayer::OnQuit, (WZLZGameViewLayer*)m_CurGameLayer));
            break;
        }
        default:
        {
            log("on quit game faild");
            break;
        }
    }
    g_GlobalUnits.m_bIsMePlaying = false;
    m_CurGameLayer->runAction(Sequence::create(func,NULL));
}

void GameLayerMove::GoGameToKind()
{
    CloseGame();
    ClientSocketSink::sharedSocketSink()->closeSocket();
    ClientSocketSink::sharedSocketSink()->CleaAllUser();
    if(m_LobbyLayer != NULL)
    {
        m_LobbyLayer->setVisible(true);
        m_LobbyLayer->StopGameHeart();
    }
    
    SoundUtil::sharedEngine()->playBackMusic("dating", true);
    SoundUtil::sharedEngine()->setBackSoundVolume(g_GlobalUnits.m_fBackMusicValue);
}

void GameLayerMove::GoGameToTable()
{
	m_GameTable->setVisible(true);
    CloseGame();
	if(m_LobbyLayer != NULL) m_LobbyLayer->setVisible(true);
    SoundUtil::sharedEngine()->playBackMusic("dating", true);
    SoundUtil::sharedEngine()->setBackSoundVolume(g_GlobalUnits.m_fBackMusicValue);
}

void GameLayerMove::QuitGame()
{
    m_LobbyLayer->UpdataLobbyScore();
    if (m_CurGameKindId == Dating_GameKind_BRNN ||
        m_CurGameKindId == Dating_GameKind_30S ||
        m_CurGameKindId == Dating_GameKind_SDB ||
        m_CurGameKindId == Dating_GameKind_HHSW||
        m_CurGameKindId == Dating_GameKind_WZLZ||
        m_CurGameKindId == Dating_GameKind_DDZ)
    {
        GoGameToKind();
    }
    else
    {
        GoGameToTable();
    }
    CloseGetScore();
}

void GameLayerMove::GoTableToGame(bool _move)
{
	m_CurGameLayer->setVisible(true);
	m_GameTable->setVisible(false);
}

int GameLayerMove::GetLayerIndex()
{
	return m_LayerIndex;
}

void GameLayerMove::InitTable()
{
	m_GameTable->initTable();
}

void GameLayerMove::ReloadTable(CMD_GR_TableStatus* TableStatus)
{
	m_GameTable->UpdateTableStatus(TableStatus);
}

void GameLayerMove::InitTableStatus(CMD_GR_TableInfo* TableStatus)
{
	m_GameTable->InitTableStatus(TableStatus);
}

void GameLayerMove::SetGameName(const char* gamename, const char* roomname)
{
	ZeroMemory(m_CurGameName,256);
	sprintf(m_CurGameName,"%s",gamename);
	ZeroMemory(m_CurRoomName, 128);
	sprintf(m_CurRoomName,"%s",roomname);
}

void GameLayerMove::EnterRoomList()
{
    if (m_GameKind)
    {
        m_GameKind->enterRoomList();
    }
}

void GameLayerMove::loginGameServerSuccess()
{
    m_LobbyLayer->StartGameHeart();
    if (m_CurGameKindId == Dating_GameKind_BRNN ||
        m_CurGameKindId == Dating_GameKind_30S ||
        m_CurGameKindId == Dating_GameKind_SDB ||
        m_CurGameKindId == Dating_GameKind_HHSW||
        m_CurGameKindId == Dating_GameKind_WZLZ||
        m_CurGameKindId == Dating_GameKind_DDZ)
    {
        GoKindToGame();
    }
    else
    {
        InitTable();
        GoKindToTable();
    }
}

void GameLayerMove::GoKindToGame()
{
    CloseUserInfo();
    LoadLayer::create(m_GameKind->m_pGameScene);
    AddEnterGameTime();
//    WORD table = 0, chair = 0;
    ClientSocketSink::sharedSocketSink()->SitdownReq();
}

void GameLayerMove::GoKindToTable()
{
    m_LayerIndex = 1;
    MoveTo *leftMoveTo = MoveTo::create(0.5f, Vec2(0,0));
	m_GameTable->runAction(Sequence::create(leftMoveTo,NULL));
	m_GameTable->setVisible(true);
	m_GameTable->SetTableName(m_CurGameName,m_CurRoomName);

	MoveTo *tempmove = MoveTo::create(0.5f, Vec2(-_STANDARD_SCREEN_SIZE_.width,0));
	ActionInstant* func = CallFunc::create(CC_CALLBACK_0(GameKind::OnCallMove, m_GameKind));
	m_GameKind->runAction(Sequence::create(tempmove,func,NULL));
}

void GameLayerMove::GoTableToKind()
{
    m_LayerIndex = 0;
    m_LobbyLayer->StopGameHeart();
    MoveTo *leftMoveTo = MoveTo::create(0.2f, Vec2(0,0));
	m_GameKind->setVisible(true);
	m_GameKind->runAction(Sequence::create(leftMoveTo,NULL));
	
	MoveTo *tempmove = MoveTo::create(0.2f, Vec2(_STANDARD_SCREEN_SIZE_.width,0));
    ActionInstant* func = CallFunc::create(CC_CALLBACK_0(GameTable::OnCallMove, m_GameTable));
	m_GameTable->runAction(Sequence::create(tempmove,func,NULL));
}

void GameLayerMove::CallCloseRoom(char* text)
{
	if(m_GameTable != NULL) m_GameTable->CloseTable(text);
}
