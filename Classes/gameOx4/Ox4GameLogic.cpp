﻿#include "Ox4GameLogic.h"

//////////////////////////////////////////////////////////////////////////

//扑克数据
BYTE Ox4GameLogic::m_cbCardListData[54]=
{
	0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08,0x09,0x0A,0x0B,0x0C,0x0D,	//方块 A - K
	0x11,0x12,0x13,0x14,0x15,0x16,0x17,0x18,0x19,0x1A,0x1B,0x1C,0x1D,	//梅花 A - K
	0x21,0x22,0x23,0x24,0x25,0x26,0x27,0x28,0x29,0x2A,0x2B,0x2C,0x2D,	//红桃 A - K
	0x31,0x32,0x33,0x34,0x35,0x36,0x37,0x38,0x39,0x3A,0x3B,0x3C,0x3D,	//黑桃 A - K
	0x41,0x42
};

BYTE Ox4GameLogic::m_cbCardListData1[27]=
{
	0x01,0x02,0x23,0x04,0x15,0x16,0x17,0x38,0x29,0x1A,0x2B,0x2C,0x2D,
	0x31,0x32,0x03,0x34,0x25,0x26,0x27,0x28,0x39,0x3A,0x3B,0x1C,0x3D,0x4E
};

BYTE Ox4GameLogic::m_cbCardListData2[27]=
{
	0x11,0x12,0x13,0x14,0x05,0x06,0x07,0x08,0x09,0x0A,0x0B,0x0C,0x0D,
	0x21,0x22,0x33,0x24,0x35,0x36,0x37,0x18,0x39,0x2A,0x1B,0x3C,0x1D,0x4F
};

//////////////////////////////////////////////////////////////////////////

//构造函数
Ox4GameLogic::Ox4GameLogic()
{
}

//析构函数
Ox4GameLogic::~Ox4GameLogic()
{
}
//分析扑克
bool Ox4GameLogic::AnalysebCardData(const BYTE cbCardData[], BYTE cbCardCount, Ox4_tagAnalyseResult & AnalyseResult)
{
	//设置结果
	ZeroMemory(&AnalyseResult,sizeof(AnalyseResult));

	//扑克分析
	for (BYTE i=0;i<cbCardCount;i++)
	{
		//变量定义
		BYTE cbSameCount=1;
		BYTE cbLogicValue=GetCardValue(cbCardData[i]);
		if(cbLogicValue<=0) return false;

		//搜索同牌
		for (BYTE j=i+1;j<cbCardCount;j++)
		{
			//获取扑克
			if (GetCardValue(cbCardData[j])!=cbLogicValue) break;

			//设置变量
			cbSameCount++;
		}

		//设置结果
		switch (cbSameCount)
		{
		case 1:		//单张
			{
				BYTE cbIndex=AnalyseResult.cbSignedCount++;
				AnalyseResult.cbSignedCardData[cbIndex*cbSameCount]=cbCardData[i];
				break;
			}
		case 2:		//两张
			{
				BYTE cbIndex=AnalyseResult.cbDoubleCount++;
				AnalyseResult.cbDoubleCardData[cbIndex*cbSameCount]=cbCardData[i];
				AnalyseResult.cbDoubleCardData[cbIndex*cbSameCount+1]=cbCardData[i+1];
				break;
			}
		case 3:		//三张
			{
				BYTE cbIndex=AnalyseResult.cbThreeCount++;
				AnalyseResult.cbThreeCardData[cbIndex*cbSameCount]=cbCardData[i];
				AnalyseResult.cbThreeCardData[cbIndex*cbSameCount+1]=cbCardData[i+1];
				AnalyseResult.cbThreeCardData[cbIndex*cbSameCount+2]=cbCardData[i+2];
				break;
			}
		case 4:		//四张
			{
				BYTE cbIndex=AnalyseResult.cbFourCount++;
				AnalyseResult.cbFourCardData[cbIndex*cbSameCount]=cbCardData[i];
				AnalyseResult.cbFourCardData[cbIndex*cbSameCount+1]=cbCardData[i+1];
				AnalyseResult.cbFourCardData[cbIndex*cbSameCount+2]=cbCardData[i+2];
				AnalyseResult.cbFourCardData[cbIndex*cbSameCount+3]=cbCardData[i+3];
				break;
			}
		}

		//设置索引
		i+=cbSameCount-1;
	}

	return true;
}
//获取类型
BYTE Ox4GameLogic::GetCardType(BYTE cbCardData[], BYTE cbCardCount)
{
	ASSERT(cbCardCount==Ox4_MAX_COUNT);

	SortCardList(cbCardData,cbCardCount);
    
	BYTE bTemp[Ox4_MAX_COUNT];
	BYTE bSum=0;
	for (BYTE i=0;i<cbCardCount;i++)
	{
        BYTE temp = GetCardLogicValue(cbCardData[i]);
        bTemp[i]= temp > 10 ? 10 : temp;
		bSum+=bTemp[i];
	}

	for (BYTE i=0;i<cbCardCount-1;i++)
	{
		for (BYTE j=i+1;j<cbCardCount;j++)
		{
			if((bSum-bTemp[i]-bTemp[j])%10==0)
			{
				return ((bTemp[i]+bTemp[j])>10)?(bTemp[i]+bTemp[j]-10):(bTemp[i]+bTemp[j]);
			}
		}
	}

	return OX_VALUE0;
}

//获取倍数
BYTE Ox4GameLogic::GetTimes(BYTE cbCardData[], BYTE cbCardCount)
{
	if(cbCardCount!=Ox4_MAX_COUNT)return 0;

	BYTE bTimes=GetCardType(cbCardData,Ox4_MAX_COUNT);
	if(bTimes<7)
		return 1;
	else if(bTimes==7)
		return 2;
	else if(bTimes==8)
		return 3;
	else if(bTimes==9)
		return 4;
	else if(bTimes==10)
		return 5;
	else if(bTimes==OX_FIVEKING)
		return 5;
	else if(bTimes==OX_HULU)
		return 5;
	else if(bTimes==OX_BOMB)
		return 5;

	return 0;
}

//获取牛牛
bool Ox4GameLogic::GetOxCard(BYTE cbCardData[], BYTE cbCardCount)
{
	ASSERT(cbCardCount==Ox4_MAX_COUNT);

	//设置变量
	BYTE bTemp[Ox4_MAX_COUNT], bTempData[Ox4_MAX_COUNT];
	CopyMemory(bTempData,cbCardData,sizeof(bTempData));
	BYTE bSum=0;
	for (BYTE i=0;i<cbCardCount;i++)
	{
		bTemp[i] = GetCardLogicValue(cbCardData[i]);
        bTemp[i] = bTemp[i] > 10 ? 10 : bTemp[i];
		bSum += bTemp[i];
	}

	//查找牛牛
	for (BYTE i=0;i<cbCardCount-1;i++)
	{
		for (BYTE j=i+1;j<cbCardCount;j++)
		{
			if((bSum-bTemp[i]-bTemp[j])%10==0)
			{
				BYTE bCount=0;
				for (BYTE k=0; k<cbCardCount; k++)
				{
					if(k!=i && k!=j)
					{
						cbCardData[bCount++] = bTempData[k];
					}
				}ASSERT(bCount==3);

				cbCardData[bCount++] = bTempData[i];
				cbCardData[bCount++] = bTempData[j];

				return true;
			}
		}
	}

	if(GetCardType(cbCardData,cbCardCount))return true;
	return false;
}

//获取整数
bool Ox4GameLogic::IsIntValue(BYTE cbCardData[], BYTE cbCardCount)
{
	BYTE sum=0;
	for(BYTE i=0;i<cbCardCount;i++)
	{
		sum+=GetCardLogicValue(cbCardData[i]);
	}
	ASSERT(sum>0);
	return (sum%10==0);
}

//排列扑克
void Ox4GameLogic::SortCardList(BYTE cbCardData[], BYTE cbCardCount)
{
	//转换数值
	BYTE cbLogicValue[Ox4_MAX_COUNT];
	for (BYTE i=0; i<cbCardCount; i++)
		cbLogicValue[i]=GetCardLogicValue(cbCardData[i]);

	//排序操作
	bool bSorted=true;
	BYTE cbTempData,bLast=cbCardCount-1;
	do
	{
		bSorted=true;
		for (BYTE i=0;i<bLast;i++)
		{
			if ((cbLogicValue[i]<cbLogicValue[i+1])||
				((cbLogicValue[i]==cbLogicValue[i+1])&&(cbCardData[i]<cbCardData[i+1])))
			{
				//交换位置
				cbTempData=cbCardData[i];
				cbCardData[i]=cbCardData[i+1];
				cbCardData[i+1]=cbTempData;
				cbTempData=cbLogicValue[i];
				cbLogicValue[i]=cbLogicValue[i+1];
				cbLogicValue[i+1]=cbTempData;
				bSorted=false;
			}	
		}
		bLast--;
	} while(bSorted==false);

	return;
}

//混乱扑克
void Ox4GameLogic::RandCardList(BYTE cbCardBuffer[], BYTE cbBufferCount,DWORD dwUserID)
{
	//CopyMemory(cbCardBuffer,m_cbCardListData,cbBufferCount);
//	srand((DWORD)(GetTickCount()/11+dwUserID));
	//混乱准备
	BYTE cbCardData[CountArray(m_cbCardListData)];
	CopyMemory(cbCardData,m_cbCardListData,sizeof(m_cbCardListData));

	//混乱扑克
	BYTE bRandCount=0,bPosition=0;
	do
	{
		bPosition=(BYTE)(rand()%(CountArray(m_cbCardListData)-bRandCount));
		cbCardBuffer[bRandCount++]=cbCardData[bPosition];
		cbCardData[bPosition]=cbCardData[CountArray(m_cbCardListData)-bRandCount];
	} while (bRandCount<cbBufferCount);

	return;
}

//混乱扑克
void Ox4GameLogic::RandCardList1(BYTE cbCardBuffer[], BYTE cbBufferCount,DWORD dwUserID)
{
	//CopyMemory(cbCardBuffer,m_cbCardListData,cbBufferCount);
//	srand((DWORD)(GetTickCount()/12+dwUserID));
	//混乱准备
	BYTE cbCardData[CountArray(m_cbCardListData1)];
	CopyMemory(cbCardData,m_cbCardListData1,sizeof(m_cbCardListData1));

	//混乱扑克
	BYTE bRandCount=0,bPosition=0;
	do
	{
		bPosition=(BYTE)(rand()%(CountArray(m_cbCardListData1)-bRandCount));
		cbCardBuffer[bRandCount++]=cbCardData[bPosition];
		cbCardData[bPosition]=cbCardData[CountArray(m_cbCardListData1)-bRandCount];
	} while (bRandCount<cbBufferCount);

	return;
}

//混乱扑克
void Ox4GameLogic::RandCardList2(BYTE cbCardBuffer[], BYTE cbBufferCount,DWORD dwUserID)
{
	//CopyMemory(cbCardBuffer,m_cbCardListData,cbBufferCount);
//	srand((DWORD)(GetTickCount()/13+dwUserID));
	//混乱准备
	BYTE cbCardData[CountArray(m_cbCardListData2)];
	CopyMemory(cbCardData,m_cbCardListData2,sizeof(m_cbCardListData2));

	//混乱扑克
	BYTE bRandCount=0,bPosition=0;
	do
	{
		bPosition=(BYTE)(rand()%(CountArray(m_cbCardListData2)-bRandCount));
		cbCardBuffer[bRandCount++]=cbCardData[bPosition];
		cbCardData[bPosition]=cbCardData[CountArray(m_cbCardListData2)-bRandCount];
	} while (bRandCount<cbBufferCount);

	return;
}

//逻辑数值
BYTE Ox4GameLogic::GetCardLogicValue(BYTE cbCardData)
{
	//扑克属性
	BYTE bCardColor=GetCardColor(cbCardData);
	BYTE bCardValue=GetCardValue(cbCardData);

	//大小王转化为10点
	if( bCardColor == 4)
		return (bCardValue + 13) ;

	//转换数值
	return bCardValue;
}

//对比扑克
bool Ox4GameLogic::CompareCard(BYTE cbFirstData[], BYTE cbNextData[], BYTE cbCardCount,bool FirstOX,bool NextOX)
{
	if(FirstOX!=NextOX)return (FirstOX>NextOX);

	//比较牛大小
	if(FirstOX==TRUE)
	{
		//获取点数
		BYTE cbNextType=GetCardType(cbNextData,cbCardCount);
		BYTE cbFirstType=GetCardType(cbFirstData,cbCardCount);

		//点数判断
		if (cbFirstType!=cbNextType) return (cbFirstType>cbNextType);
	}

	//排序大小
	BYTE bFirstTemp[Ox4_MAX_COUNT],bNextTemp[Ox4_MAX_COUNT];
	CopyMemory(bFirstTemp,cbFirstData,cbCardCount);
	CopyMemory(bNextTemp,cbNextData,cbCardCount);
	SortCardList(bFirstTemp,cbCardCount);
	SortCardList(bNextTemp,cbCardCount);

	//比较数值
	BYTE cbNextMaxValue=GetCardValue(bNextTemp[0]);
	BYTE cbFirstMaxValue=GetCardValue(bFirstTemp[0]);
	if(cbNextMaxValue!=cbFirstMaxValue)return cbFirstMaxValue>cbNextMaxValue;

	//比较颜色
	return GetCardColor(bFirstTemp[0]) > GetCardColor(bNextTemp[0]);

	return false;
}

bool Ox4GameLogic::CompareCard(BYTE cbFirstData[], BYTE cbNextData[], BYTE cbCardCount)
{

	//获取点数
	BYTE cbNextType=GetCardType(cbNextData,cbCardCount);
	BYTE cbFirstType=GetCardType(cbFirstData,cbCardCount);

	//点数判断
	if (cbFirstType!=cbNextType) 
		return (cbFirstType>cbNextType);

	//排序大小
	BYTE bFirstTemp[Ox4_MAX_COUNT],bNextTemp[Ox4_MAX_COUNT];
	CopyMemory(bFirstTemp,cbFirstData,cbCardCount);
	CopyMemory(bNextTemp,cbNextData,cbCardCount);
	SortCardList(bFirstTemp,cbCardCount);
	SortCardList(bNextTemp,cbCardCount);

	//比较数值
	BYTE cbNextMaxValue=GetCardValue(bNextTemp[0]);
	BYTE cbFirstMaxValue=GetCardValue(bFirstTemp[0]);
	if(cbNextMaxValue!=cbFirstMaxValue)return cbFirstMaxValue>cbNextMaxValue;

	//比较颜色
	return GetCardColor(bFirstTemp[0]) > GetCardColor(bNextTemp[0]);

	return false;
}
//////////////////////////////////////////////////////////////////////////
