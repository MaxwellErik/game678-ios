#ifndef CMD_OX_HEAD_FILE
#define CMD_OX_HEAD_FILE

#pragma pack(1)


//////////////////////////////////////////////////////////////////////////
//公共宏定义
#define PRE_COMPILE_PRJ_III

#define Ox4_KIND_ID						165                                 //游戏 I D
#define Ox4_GAME_PLAYER					4                                   //游戏人数
#define Ox4_GAME_NAME					TEXT("四人牛牛")                        //游戏名字

#define GAME_GENRE						(GAME_GENRE_GOLD|GAME_GENRE_MATCH)	//游戏类型
#define Ox4_MAXCOUNT					5                                   //扑克数目

//结束原因
#define Ox4_GER_NO_PLAYER				0x10								//没有玩家
#define Ox4_GER_COMPARECARD				0x20								//比牌结束
#define Ox4_GER_OPENCARD				0x30								//开牌结束

//游戏状态
#define Ox4_GS_TK_FREE					GS_FREE
#define Ox4_GS_TK_BANK					GS_PLAYING				//叫庄状态
#define Ox4_GS_TK_INVEST				GS_PLAYING + 1			//下注状态
#define Ox4_GS_TK_SEND					GS_PLAYING + 2 			//发牌状态
#define Ox4_GS_TK_DIVIDE				GS_PLAYING + 3			//分牌状态
#define Ox4_GS_TK_CHECK_OUT				GS_PLAYING + 4


//用户状态
#define USEX_NULL                   0                                       //用户状态
#define USEX_PLAYING                1                                       //用户状态
#define USEX_DYNAMIC                2                                       //用户状态   

//////////////////////////////////////////////////////////////////////////
//服务器命令结构
#define SUB_S_CALL_BANK_STATUS			1				//进入叫庄状态
#define SUB_S_CALL_BANK					2				//有人叫庄
#define SUB_S_INVEST_STATUS				3				//进入下注状态
#define SUB_S_INVEST					4				//有人下注
#define SUB_S_SEND_STATUS				5				//进入发牌状态
#define SUB_S_DIVIDE_STATUS				6				//进入分牌状态
#define SUB_S_DIVIDE					7				//有人分牌
#define SUB_S_CHECK_OUT					8				//游戏结算
#define SUB_S_CHECK_OUT_LEFT			9				//游戏结算(用户强退导致)

//聊天结构
struct CMD_GF_Ox4_UserChat
{
	WORD								wChatLength;					//信息长度
	COLORREF							crFontColor;					//信息颜色
	DWORD								dwSendUserID;					//发送用户
	DWORD								dwTargetUserID;					//目标用户
	TCHAR								szChatMessage[1024];	//聊天信息
};

//游戏状态
struct CMD_S_Ox4_StatusFree
{
	LONGLONG								lCellScore;							//基础积分
};

//游戏状态
struct CMD_S_Ox4_StatusCall
{
    BYTE mTime;                                 //叫庄倒计时
    BYTE mBankAction[Ox4_GAME_PLAYER];          //叫庄状态 0--默认 1--叫庄 2--不叫庄
    WORD mActionChairID;                        //当前行的ChairID
    bool mIsHalfWay;          //中途加入
};

//游戏状态
struct CMD_S_Ox4_StatusScore
{
	//下注信息
    BYTE mTime;                                 //下注倒计时
    LONGLONG mInvestJetonGold;                  //下注筹码
    WORD mBankChairID;                          //庄家ID
    LONGLONG mInvestGold[Ox4_GAME_PLAYER];      //每个人的下注金币
    bool mIsHalfWay;          //中途加入
};

struct CMD_S_Ox4_SendScene
{
    WORD mBankChairID;                           //庄家ID
    LONGLONG mInvestGold[Ox4_GAME_PLAYER];      //每个人的下注金币
    BYTE mHandCardData[Ox4_MAXCOUNT];           //自己手牌
    bool mIsHalfWay;          //中途加入
};

//分牌状态
struct CMD_S_Ox4_DivideScene
{
    BYTE mTime;									//分牌倒计时
    WORD mBankChairID;							//庄家ID
    LONGLONG mInvestGold[Ox4_GAME_PLAYER];		//每个人的下注金币
    bool mDivideAction[Ox4_GAME_PLAYER];		//每个人是否分牌
    BYTE mHandCardData[Ox4_MAXCOUNT];			//自己手牌
    BYTE mCardType[2];							//自己手牌牌型[0]牌型(0-- 无牛 1--有牛 2--牛牛 3--四炸 4--五金牛)[1]牌型值(牛1-牛9)
    bool mIsHalfWay;          //中途加入
};

//结算状态
struct CMD_S_Ox4_CheckOutScene
{
    WORD mBankChairID;
    LONGLONG mInvestGold[ Ox4_GAME_PLAYER ];
    BYTE mHandCardData[ Ox4_MAXCOUNT ];
    BYTE mCardType[2];
    bool mIsHalfWay;          //中途加入
};

//用户叫庄
struct CMD_S_Ox4_CallBanker
{
    BYTE mTime;
    WORD mActionChairID;		//叫庄的人ID
    bool mBankAction;			//true--叫庄 false--不叫庄
    WORD mNextActionChairID;	//下一个说话ID
};

//游戏开始
struct CMD_S_Ox4_GameStart
{
    BYTE mTime;					//叫庄状态倒计时，超时服务器会自动让未叫庄的人默认不叫并发消息通知( 秒 )
    WORD mNextActionChairID;	//
};

//用户下注
struct CMD_S_Ox4_AddScore
{
    BYTE        mTime;					//下注状态倒计时，超时服务器会自动让未下注的自动下注并发消息通知( 秒 )
    WORD        mBankChairID;			//庄家ID
    LONGLONG    mInvestJettonGold;
};

//进入分牌状态
struct CMD_S_DivideStatus
{
    BYTE mTime;							//分牌倒计时，超时服务器会自动让未分牌的自动分牌并发消息通知( 秒 )
};

//用户下注
struct CMD_S_Ox4_AddScoreRes
{
    WORD        mActionChairID;		//下注的人ID
    LONGLONG    mInvestGold;
};

//游戏结束
struct CMD_S_Ox4_GameEnd
{
    char mNickName[Ox4_GAME_PLAYER ][NAME_LEN];
    LONGLONG mGameGold[Ox4_GAME_PLAYER];		//输赢金币
    BYTE mHandCardData[ Ox4_GAME_PLAYER ][Ox4_MAXCOUNT];
    BYTE mCardType[Ox4_GAME_PLAYER][2];			//牌的类型[0]牌型[1]牌型值(牛1-牛9)
};

//游戏结算(用户强退导致)
struct CMD_S_CheckOutLeft
{
    CHAR mNickName[Ox4_GAME_PLAYER][NAME_LEN];
    LONGLONG mGameGold[Ox4_GAME_PLAYER];
};

//发牌数据包
struct CMD_S_Ox4_SendCard
{
    BYTE mHandCardData[Ox4_MAXCOUNT];	//发给自己的牌
};

struct CMD_S_Ox4_Admin_Result
{
	BYTE                                 cbCardData[Ox4_MAXCOUNT];
};

//用户退出
struct CMD_S_Ox4_PlayerExit
{
	WORD								wPlayerID;							//退出用户
};

//用户摊牌
struct CMD_S_Ox4_Open_Card
{
    WORD mActionChairID;				//分牌的人ID
};
//////////////////////////////////////////////////////////////////////////
//客户端命令结构
#define SUB_C_Ox4_CALL_BANKER				1									//用户叫庄
#define SUB_C_Ox4_ADD_SCORE					2									//用户加注
#define SUB_C_Ox4_OPEN_CARD					3									//用户摊牌
#define SUB_C_Ox4_APPLAY_SUPPER				14									//请求控制
#define SUB_C_Ox4_ADMIN_NIU9                15                                   //牛9
#define SUB_C_Ox4_ADMIN_NIUNIU             16                                   //牛牛

//用户叫庄
struct CMD_C_Ox4_CallBanker
{
	bool								bBanker;							//叫庄用户
};

//用户加注
struct CMD_C_Ox4_AddScore
{
    BYTE mInvestIndex;		//自己下注索引
};

//用户摊牌
struct CMD_C_Ox4_OxCard
{
    BYTE mDivideCardData[Ox4_MAX_COUNT];	//分牌的数据(例如.[J][Q][K][2][5]--牛 七 )
};

//超级账号
struct SupperAccount
{
	IosDword dwUserID;
	int		nPercent;
	bool	bEffect;
};

//输赢控制账号信息
struct SupperApplay
{
	IosDword	dwUserID;
	int		nChairID;		//椅子编号
	BYTE	cbApplayType;	//请求类型，0x01为左键请求,0x02为右键请求
};

#define LBUTTON_APPLAY		0x01
#define RBUTTON_APPLAY		0x02
//////////////////////////////////////////////////////////////////////////
#pragma pack()
#endif
