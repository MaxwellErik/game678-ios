﻿#include "Ox4GameViewLayer.h"
#include "LobbyLayer.h"
#include "ClientSocketSink.h"
#include "HXmlParse.h"
#include "LocalDataUtil.h"
#include "JniSink.h"
#include "VisibleRect.h"
#include "HttpConstant.h"
#include "Screen.h"
#include "LobbySocketSink.h"
#include "LoginLayer.h"
#include "GameLayerMove.h"
#include "Convert.h"

#define		MENU_INIT_POINT			(Vec2Make(-_STANDARD_SCREEN_CENTER_.x+68+30 , _STANDARD_SCREEN_CENTER_.y-68-20))

#define		ADV_SIZE				(CCSizeMake(520,105))
//宝箱坐标
#define		BOX_POINT				(ccpx(1195,70))

//最少筹码数
#define		MIN_PROP_PAY_SCORE			1
#define		ALL_ANIMATION_COUNT			5


#define		MAX_CREDIT					(9999999)
#define		MAX_BET						(9999)
#define		MAX_ADV_COUNT				(10)

#if (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
#define		BIG_AWARD_MULITY			10
#else
#define		BIG_AWARD_MULITY			30
#endif // _DEBUG

#define Btn_Ready						1
#define Btn_BackToLobby					2
#define Btn_OpenCard					3
#define Btn_Seting						4
#define Btn_AutoOk						5
#define Btn_AutoCancle					6
#define Btn_CallBankYesBtn				7
#define Btn_CallBankNoBtn				8
#define Btn_BetBtn						9	//9~12

#define Btn_TG_Max						15
#define Btn_TG_5f4						16
#define Btn_TG_2f1						17
#define Btn_TG_5f1						18
#define Btn_TG_Rand						19
#define Btn_TG_Cancel					20

Ox4GameViewLayer::Ox4GameViewLayer(GameScene *pGameScene):IGameView(pGameScene)
{
	m_GameState=enGameNormal;
	m_BtnReadyPlay = NULL;
	m_UpCardIndex = 0;
	m_LeftCardIndex1 = 0;
	m_LeftCardIndex2 = 0;
	m_RightCardIndex1 = 0;
	m_RightCardIndex2 = 0;
	m_DownCardIndex = 0;
	ZeroMemory(m_HandCardData,sizeof(m_HandCardData));
	m_MyHandIndex = 0;
	m_SendCardIndex = 0;
	ZeroMemory(m_MyHandCardData,sizeof(m_MyHandCardData));

	m_OpenCardSpr[0] = NULL;
	m_OpenCardSpr[1] = NULL;
	m_OpenCardSpr[2] = NULL;
	m_OpenCardSpr[3] = NULL;

	m_StartTime = 15;
	m_bHaveNN = false;
	m_cbDynamicJoin = false;
    m_needCallBanker = false;
	m_bAuto = false;
	m_TG_BackSpr = NULL;
	m_TG_BackShow = false;

	m_TG_BackSpr = NULL;
	m_TG_5f1Btn = NULL;
	m_TG_MaxBtn = NULL;
	m_TG_2f1Btn = NULL;
	m_TG_5f4Btn = NULL;
	m_TG_RandBtn = NULL;
	m_TG_CancelBtn = NULL;
	m_TG_BetType = 0;
	m_TG_bRand = false;
    m_openBySelf = false;
	SetKindId(Ox4_KIND_ID);
}

Ox4GameViewLayer::~Ox4GameViewLayer() 
{
}

Ox4GameViewLayer *Ox4GameViewLayer::create(GameScene *pGameScene)
{
    Ox4GameViewLayer *pLayer = new Ox4GameViewLayer(pGameScene);
    if(pLayer && pLayer->init())
    {
        pLayer->autorelease();
        return pLayer;
    }
    else
    {
        CC_SAFE_DELETE(pLayer);
        return NULL;
    }
}

bool Ox4GameViewLayer::init()
{
	if ( !Layer::init() )
	{
		return false;
	}
	setLocalZOrder(3);
	Tools::addSpriteFrame("Ox4/GameBack.plist");
	Tools::addSpriteFrame("Common/CardSprite2.plist");
	PlayBackMusic("back.mp3", true);
	IGameView::onEnter();
	JniSink::share()->setIGameView(this);
	setGameStatus(GS_FREE);

	InitGame();
	AddButton();
    
    AddPlayerInfo();
    
	setTouchEnabled(true);	
	
	auto listener = EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(true);
	listener->onTouchBegan = CC_CALLBACK_2(Ox4GameViewLayer::onTouchBegan, this);
	listener->onTouchMoved = CC_CALLBACK_2(Ox4GameViewLayer::onTouchMoved, this);
	listener->onTouchEnded = CC_CALLBACK_2(Ox4GameViewLayer::onTouchEnded, this);
	_eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);

	return true;
}

void Ox4GameViewLayer::onEnter()
{	

}

void Ox4GameViewLayer::StartTime(int _time)
{
	m_StartTime = _time;
	schedule(schedule_selector(Ox4GameViewLayer::UpdateTime), 1);
	m_ClockSpr->setVisible(true);
	UpdateTime(m_StartTime);
}

void Ox4GameViewLayer::StopTime()
{
	unschedule(schedule_selector(Ox4GameViewLayer::UpdateTime));
	m_ClockSpr->removeAllChildren();
	m_ClockSpr->setVisible(false);
}

void Ox4GameViewLayer::UpdateTime(float fp)
{
	if (m_StartTime <= 0)
	{
		unschedule(schedule_selector(Ox4GameViewLayer::UpdateTime));
		if (m_GameState == enGameNormal ||
			m_GameState == enGameEnd)
		{
			SoundUtil::sharedEngine()->stopAllEffects();
			SoundUtil::sharedEngine()->stopBackMusic();
			ClientSocketSink::sharedSocketSink()->LeftGameReq();
			GameLayerMove::sharedGameLayerMoveSink()->GoGameToTable();
			ClientSocketSink::sharedSocketSink()->setFrameGameView(NULL);
		}
		return;
	}

	m_ClockSpr->removeAllChildren();
   std::string str = StringUtils::toString(m_StartTime);
   if (m_StartTime < 10)
       str = "0" + str;
   LabelAtlas *time = LabelAtlas::create(str, "Common/time.png", 33, 50, '+');
   time->setPosition(Vec2(22, 15));
   m_ClockSpr->addChild(time);

	m_StartTime--;

}

void Ox4GameViewLayer::onExit()
{
	Tools::removeSpriteFrameCache("Ox4/GameBack.plist");
	Tools::removeSpriteFrameCache("Common/CardSprite2.plist");
	IGameView::onExit();
}

// Touch 触发
bool Ox4GameViewLayer::onTouchBegan(Touch *pTouch, Event *pEvent)
{
	if (m_GameState == enGameBet)
	{
		//图片按钮
		Vec2 touchLocation = pTouch->getLocation();
		Vec2 localPos = convertToNodeSpace(touchLocation);
		for (int i = 0; i < 4; i++)
		{
			Node * temp= getChildByTag(m_GoldBtnTga[i]);
			if (temp == NULL) continue;
			if(temp->isVisible() == false) continue;
			Rect rc = Rect(m_GoldBtnSpr[i]->getPositionX() -
			m_GoldBtnSpr[i]->getContentSize().width * m_GoldBtnSpr[i]->getAnchorPoint().x,
			m_GoldBtnSpr[i]->getPositionY() - m_GoldBtnSpr[i]->getContentSize().height * m_GoldBtnSpr[i]->getAnchorPoint().y,
			m_GoldBtnSpr[i]->getContentSize().width, m_GoldBtnSpr[i]->getContentSize().height);
			bool isTouched = rc.containsPoint(localPos);
			if (isTouched)
			{
				for (int ic = 0; ic < 4; ic++)
				{
					Node * temp= getChildByTag(m_GoldBtnTga[ic]);
					if (temp != NULL)m_GoldBtnSpr[ic]->setVisible(false);
				}
                
				CMD_C_Ox4_AddScore AddScore;
				AddScore.mInvestIndex = i;
				SendData(SUB_C_Ox4_ADD_SCORE,&AddScore,sizeof(AddScore));
				SoundUtil::sharedEngine()->playEffect("ADD_SCORE"); 
				return true;
			}
		}
	}


	return true;
}

Vec2 Ox4GameViewLayer::getGoldPos(int chairID)
{
    Vec2 goldPos[4] = {Vec2(480,820), Vec2(200,680), Vec2(480,260), Vec2(1720,680)};
    return goldPos[chairID];
}

Vec2 Ox4GameViewLayer::getReadyPos(int chairID)
{
    Vec2 readyPos[4] = {Vec2(700,960), Vec2(200,380), Vec2(480,280), Vec2(1720,380)};
    return readyPos[chairID];
}

Vec2 Ox4GameViewLayer::getOpenPos(int chairID)
{
   Vec2 openPos[4] = {Vec2(960,900), Vec2(560,540), Vec2(960,140), Vec2(1360,540)};
    return openPos[chairID];
}

Vec2 Ox4GameViewLayer::getCardTypePos(int chairID)
{
    Vec2 cardPos[4] = {Vec2(960,870), Vec2(560,510), Vec2(960,100), Vec2(1360,510)};
    return cardPos[chairID];
}

Vec2 Ox4GameViewLayer::getCardTypeNonePos(int chairID)
{
    Vec2 readyPos[4] = {Vec2(960,870), Vec2(560,510), Vec2(960,110), Vec2(1360,510)};
    return readyPos[chairID];
}

Vec2 Ox4GameViewLayer::getBankTitlePos(int chairID)
{
    Vec2 readyPos[4] = {Vec2(590,1020), Vec2(310,610), Vec2(590,190), Vec2(1790,610)};
    return readyPos[chairID];
}

void Ox4GameViewLayer::onTouchMoved(Touch *pTouch, Event *pEvent)
{
}

void Ox4GameViewLayer::onTouchEnded(Touch*pTouch, Event*pEvent)
{
}

// TextField 触发
bool Ox4GameViewLayer::onTextFieldAttachWithIME(TextFieldTTF *pSender)
{
	return false;
}

bool Ox4GameViewLayer::onTextFieldDetachWithIME(TextFieldTTF *pSender)
{
	return false;
}

bool Ox4GameViewLayer::onTextFieldInsertText(TextFieldTTF *pSender, const char *text, int nLen)
{
	return true;
}

bool Ox4GameViewLayer::onTextFieldDeleteBackward(TextFieldTTF *pSender, const char *delText, int nLen)
{
	return true;
}

// IME 触发
void Ox4GameViewLayer::keyboardWillShow(IMEKeyboardNotificationInfo& info)
{
}

void Ox4GameViewLayer::keyboardDidShow(IMEKeyboardNotificationInfo& info)
{
}

void Ox4GameViewLayer::keyboardWillHide(IMEKeyboardNotificationInfo& info)
{
	MoveTo *pMovAction = MoveTo::create(0.15f, Vec2(0,0));
	Action *pActSeq = Sequence::create(pMovAction, NULL);
	runAction(pActSeq);
}

void Ox4GameViewLayer::keyboardDidHide(IMEKeyboardNotificationInfo& info)
{
}

// 按钮事件管理
// Alert Message 确认消息处理
void Ox4GameViewLayer::DialogConfirm(Ref *pSender)
{
	this->RemoveAlertMessageLayer();

	// 设定 触发
	this->SetAllTouchEnabled(true);
}

// Alert Message 取消消息处理
void Ox4GameViewLayer::DialogCancel(Ref *pSender)
{
	// 清除 Alert Message Layer
	this->RemoveAlertMessageLayer();

	// 设定 触发
	this->SetAllTouchEnabled(true);
}

void Ox4GameViewLayer::DrawUserScore()
{
}

void Ox4GameViewLayer::OnEventUserScore( tagUserData * pUserData, WORD wChairID, bool bLookonUser )
{
	IGameView::OnEventUserScore(pUserData, wChairID, bLookonUser);
    AddPlayerInfo();
}

void Ox4GameViewLayer::OnEventUserStatus(tagUserData * pUserData, WORD wChairID, bool bLookonUser)
{
	IGameView::OnEventUserStatus(pUserData, wChairID, bLookonUser);
	AddPlayerInfo();
}

void Ox4GameViewLayer::OnEventUserEnter( tagUserData * pUserData, WORD wChairID, bool bLookonUser )
{
	AddPlayerInfo();
}

void Ox4GameViewLayer::OnEventUserLeave( tagUserData * pUserData, WORD wChairID, bool bLookonUser )
{
	AddPlayerInfo();
}

//游戏消息
bool Ox4GameViewLayer::OnGameMessage(WORD wSubCmdID, const void * pBuffer, WORD wDataSize)
{
	CCLOG("RECEIVE MESSAGE CMD[%d]" , wSubCmdID);
	switch (wSubCmdID)
	{
    case SUB_S_CALL_BANK_STATUS:	//游戏开始
        return OnSubGameStart(pBuffer, wDataSize);

	case SUB_S_CALL_BANK:	//用户叫庄
		return OnSubCallBanker(pBuffer,wDataSize);

	case SUB_S_INVEST_STATUS:	//用户下注
		return OnSubAddScore(pBuffer,wDataSize);
            
    case SUB_S_INVEST:	//用户下注结果
        return OnSubAddScoreRes(pBuffer,wDataSize);

	case SUB_S_SEND_STATUS:	//发牌消息
		return OnSubSendCard(pBuffer, wDataSize);
    
    case SUB_S_DIVIDE_STATUS://进入摊牌状态
        return OnSubEnterOpenCard(pBuffer, wDataSize);
	
    case SUB_S_DIVIDE:	//用户摊牌
		return OnSubOpenCard(pBuffer, wDataSize);

	case SUB_S_CHECK_OUT:	//游戏结束
		return OnSubGameEnd(pBuffer, wDataSize);
    
    case SUB_S_CHECK_OUT_LEFT:
        return OnSubGameEndPlayerLeave(pBuffer, wDataSize);
	}
	return true;
}

//场景消息
bool Ox4GameViewLayer::OnGameSceneMessage(BYTE cbGameStatus, const void * pBuffer, WORD wDataSize)
{
	setGameStatus(cbGameStatus);
	
	switch (getGameStatus())
	{
	case GS_FREE:
		{
			if (m_BtnReadyPlay != NULL)	m_BtnReadyPlay->setVisible(true);
            if (m_CallBankNoBtn != NULL)	m_CallBankNoBtn->setVisible(false);
            if (m_CallBankYesBtn != NULL)	m_CallBankYesBtn->setVisible(false);
			return true;
		}
	case Ox4_GS_TK_BANK:	// 叫庄状态
		{
			if (wDataSize!=sizeof(CMD_S_Ox4_StatusCall)) return false;
			CMD_S_Ox4_StatusCall * pStatusCall=(CMD_S_Ox4_StatusCall *)pBuffer;
			//游戏信息
            StartTime(pStatusCall->mTime);
            m_GameState = enGameCallBank;
            
            if (pStatusCall->mActionChairID == GetMeChairID())
            {
                if (m_CallBankNoBtn != NULL)	m_CallBankNoBtn->setVisible(true);
                if (m_CallBankYesBtn != NULL)	m_CallBankYesBtn->setVisible(true);
            }
            else
            {
                AddTitle("\u7b49\u5f85\u73a9\u5bb6\u53eb\u5e84");
            }
			
            return true;
		}
	case Ox4_GS_TK_INVEST:	//下注状态
		{
			if (wDataSize!=sizeof(CMD_S_Ox4_StatusScore)) return false;
			CMD_S_Ox4_StatusScore * pStatusScore=(CMD_S_Ox4_StatusScore *)pBuffer;

			//设置变量
            StartTime(pStatusScore->mTime);
            m_GameState = enGameBet;
            
			m_lTurnMaxScore=pStatusScore->mInvestJetonGold;
			m_wBankerUser=pStatusScore->mBankChairID;	
			LONGLONG lTableScore[Ox4_GAME_PLAYER];
			CopyMemory(lTableScore,pStatusScore->mInvestGold,sizeof(lTableScore));

			for (WORD i=0;i<Ox4_GAME_PLAYER;i++)
			{
				//视图位置
				WORD chair = g_GlobalUnits.SwitchViewChairID(i);
                
                removeAllChildByTag(m_BetTga[chair]);
				//桌面筹码
				if(lTableScore[i]>0L)
				{
					Sprite* temp = Sprite::createWithSpriteFrameName("Jetton.png");
					temp->setPosition(getGoldPos(chair));
					temp->setTag(m_BetTga[chair]);
					addChild(temp);
					char strcc[32]="";
					LONGLONG lMon = lTableScore[chair];
					memset(strcc , 0 , sizeof(strcc));
					Tools::AddComma(lMon , strcc);
                    LabelAtlas* font = LabelAtlas::create(strcc, "Common/time_2.png", 39, 51, '+');
                    font->setAnchorPoint(Vec2(0.5f, 0.5f));
                    font->setPosition(Vec2(150, 37));
                    temp->addChild(font);
				}
			}
			return true;
		}
	case Ox4_GS_TK_SEND:
		{
			//效验数据
			if (wDataSize!=sizeof(CMD_S_Ox4_SendScene)) return false;
            CMD_S_Ox4_SendScene * pStatusSend=(CMD_S_Ox4_SendScene *)pBuffer;
            SetPlayerCardData(0,0,0);
            for (int i = 0; i < GAME_PLAYER; i++)
            {
                removeAllChildByTag(m_PlayerCardTypeTga[i]);
                removeAllChildByTag(m_SendCardTga[i]);
                removeAllChildByTag(m_DisCardTga[i]);
                removeAllChildByTag(m_CallBankTga[i]);
            }
            CopyMemory(m_HandCardData, pStatusSend->mHandCardData, sizeof(m_HandCardData));
            
            m_GameState = enGameSendCard;
            schedule(schedule_selector(Ox4GameViewLayer::SendCard), 0.1f);
            
            return true;
		}
        case Ox4_GS_TK_DIVIDE:
        {
            //效验数据
            if (wDataSize!=sizeof(CMD_S_Ox4_DivideScene)) return false;
            CMD_S_Ox4_DivideScene * pStatusDivide=(CMD_S_Ox4_DivideScene *)pBuffer;
            StartTime(pStatusDivide->mTime);
            m_GameState = enGameOpenCard;
            m_wBankerUser = pStatusDivide->mBankChairID;
            
            for (int i = 0; i < GAME_PLAYER; i++)
            {
                removeAllChildByTag(m_PlayerCardTypeTga[i]);
                removeAllChildByTag(m_DisCardTga[i]);
                removeAllChildByTag(m_CallBankTga[i]);
            }
            
            for (int i = 0; i < GAME_PLAYER; i++)
            {
                WORD wViewChairID = g_GlobalUnits.SwitchViewChairID(i);
                   
                if (wViewChairID == 2)
                {
                    if (pStatusDivide->mDivideAction[i])
                    {
                        m_BtnOpenCard->setVisible(false);
                        //扑克数据
                        BYTE bCardData[Ox4_MAXCOUNT];
                        for (int n = 0; n < 5; n++)
                        {
                            bCardData[n] = pStatusDivide->mHandCardData[n];
                        }
                        BYTE bCardType=m_GameLogic.GetCardType(bCardData,5);
                       
                        if(bCardType > 0)
                        {
                            //排序
                            m_GameLogic.GetOxCard(bCardData,5);
                            
                            SetMyCardData(bCardData,5);
                            
                            __String* ns=__String::createWithFormat("niu_%d.png",bCardType);
                            Sprite* temp;
                            if (bCardType >= 10)
                            {
                                temp = Sprite::createWithSpriteFrameName("niuniu.png");
                            }
                            else
                            {
                                temp = Sprite::createWithSpriteFrameName(ns->getCString());
                            }
                            temp->setPosition(getCardTypePos(2));
                            temp->setTag(m_PlayerCardTypeTga[2]);
                            addChild(temp);
                        }
                        else
                        {
                            Sprite* temp = Sprite::createWithSpriteFrameName("wuniu.png");
                            temp->setPosition(getCardTypePos(2));
                            temp->setTag(m_PlayerCardTypeTga[2]);
                            addChild(temp);
                        }
                    }
                    else
                    {
                        m_BtnOpenCard->setVisible(true);
                    }
                }
                else
                {
                    if (pStatusDivide->mDivideAction[i])
                    {
                        Sprite* temp = Sprite::createWithSpriteFrameName("opencardok.png");
                        temp->setPosition(getOpenPos(wViewChairID));
                        temp->setLocalZOrder(100);
                        temp->setTag(m_OpenCardTga[wViewChairID]);
                        addChild(temp);
                    }
                }
            }
            return true;
        }
        case Ox4_GS_TK_CHECK_OUT:
        {
            //效验数据
            if (wDataSize!=sizeof(CMD_S_Ox4_CheckOutScene)) return false;
            return true;
        }
    }	
    return true;
}

void Ox4GameViewLayer::SetMyCardData(BYTE cbCardData[], BYTE cbCardCount)
{
	removeAllChildByTag(m_MyMoveOverCardTga);
	if (cbCardCount == 0) return;
	for (int i = 0; i < cbCardCount; i++)
	{
		string _name = GetCardStringName(cbCardData[i]);
        if (_name == "")
            continue;
		Sprite* temp = Sprite::createWithSpriteFrameName(_name.c_str());
        if (i > 2)
            temp->setPosition(Vec2(_STANDARD_SCREEN_CENTER_.x-80+i*60,140));
        else
            temp->setPosition(Vec2(_STANDARD_SCREEN_CENTER_.x-120+i*60,140));
        temp->setTag(m_DisCardTga[2]);
		addChild(temp);
	}
}

void Ox4GameViewLayer::SetPlayerCardDataNone(WORD wchair, BYTE cbCardData[], BYTE cbCardCount)
{
	switch(wchair)
	{
	case 0:
		{
			removeAllChildByTag(m_SendCardTga[0]);
			if (cbCardCount == 0) return;
			for (int i = 0; i < 5; i++)
			{
				string _name = GetCardStringName(cbCardData[i]);
                if (_name == "")
                    continue;
				Sprite* temp = Sprite::createWithSpriteFrameName(_name.c_str());
                temp->setScale(0.7f);
				temp->setPosition(Vec2(_STANDARD_SCREEN_CENTER_.x-80+i*40, 900));
				temp->setTag(m_DisCardTga[0]);
				addChild(temp);
			}
			break;
		}
	case 1:
		{
			removeAllChildByTag(m_SendCardTga[1]);
			if (cbCardCount == 0) return;
			
			for (int i = 0; i < 5; i++)
			{
				string _name = GetCardStringName(cbCardData[i]);
                if (_name == "")
                    continue;
				Sprite* temp = Sprite::createWithSpriteFrameName(_name.c_str());
                temp->setScale(0.7f);
				temp->setPosition(Vec2(480+i*40, 540));
				temp->setTag(m_DisCardTga[1]);
				addChild(temp);
			}
			break;
		}
	case 3:
		{
			removeAllChildByTag(m_SendCardTga[3]);
			if (cbCardCount == 0) return;
			for (int i = 0; i < 5; i++)
			{
				string _name = GetCardStringName(cbCardData[i]);
                if (_name == "")
                    continue;
				Sprite* temp = Sprite::createWithSpriteFrameName(_name.c_str());
                temp->setScale(0.7f);
                temp->setPosition(Vec2(1300+i*40, 540));
				temp->setTag(m_DisCardTga[3]);
				addChild(temp);
			}
			break;
		}
	}
}

void Ox4GameViewLayer::SetPlayerCardData(WORD wchair, BYTE cbCardData[], BYTE cbCardCount)
{
	switch(wchair)
	{
	case 0:
		{
			removeAllChildByTag(m_SendCardTga[0]);
			if (cbCardCount == 0) return;
			//上面两张
			for (int i = 3; i < 5; i++)
			{
				string _name = GetCardStringName(cbCardData[i]);
                if (_name == "")
                    continue;
				Sprite* temp = Sprite::createWithSpriteFrameName(_name.c_str());
                temp->setScale(0.7f);
				temp->setPosition(Vec2(_STANDARD_SCREEN_CENTER_.x-210+i*60, 960));
				temp->setTag(m_DisCardTga[0]);
				addChild(temp);
			}
			//下面3张
			for (int i = 0; i < 3; i++)
			{
				string _name = GetCardStringName(cbCardData[i]);
                if (_name == "")
                    continue;
				Sprite* temp = Sprite::createWithSpriteFrameName(_name.c_str());
                temp->setScale(0.7f);
				temp->setPosition(Vec2(_STANDARD_SCREEN_CENTER_.x-60+i*60, 900));
				temp->setTag(m_DisCardTga[0]);
				addChild(temp);
			}
			break;
		}
	case 1:
		{
			removeAllChildByTag(m_SendCardTga[1]);
			if (cbCardCount == 0) return;
			//上面两张
			for (int i = 3; i < 5; i++)
			{
				string _name = GetCardStringName(cbCardData[i]);
                if (_name == "")
                    continue;
				Sprite* temp = Sprite::createWithSpriteFrameName(_name.c_str());
                temp->setScale(0.7f);
				temp->setPosition(Vec2(350+i*60, 600));
				temp->setTag(m_DisCardTga[1]);
				addChild(temp);
			}
			//下面3张
			for (int i = 0; i < 3; i++)
			{
				string _name = GetCardStringName(cbCardData[i]);
                if (_name == "")
                    continue;
				Sprite* temp = Sprite::createWithSpriteFrameName(_name.c_str());
                temp->setScale(0.7f);
				temp->setPosition(Vec2(500+i*60, 540));
				temp->setTag(m_DisCardTga[1]);
				addChild(temp);
			}
			break;
		}
       case 2:
       {
           SetMyCardData(cbCardData, cbCardCount);
           break;
       }
	case 3:
		{
			removeAllChildByTag(m_SendCardTga[3]);
			if (cbCardCount == 0) return;
			//上面两张
			for (int i = 3; i < 5; i++)
			{
				string _name = GetCardStringName(cbCardData[i]);
                if (_name == "")
                    continue;
				Sprite* temp = Sprite::createWithSpriteFrameName(_name.c_str());
                temp->setScale(0.7f);
				temp->setPosition(Vec2(1150+i*60, 600));
				temp->setTag(m_DisCardTga[3]);
				addChild(temp);
			}
			//下面3张
			for (int i = 0; i < 3; i++)
			{
				string _name = GetCardStringName(cbCardData[i]);
                if (_name == "")
                    continue;
				Sprite* temp = Sprite::createWithSpriteFrameName(_name.c_str());
                temp->setScale(0.7f);
				temp->setPosition(Vec2(1300+i*60, 540));
				temp->setTag(m_DisCardTga[3]);
				addChild(temp);
			}
			break;
		}
	}
}

void Ox4GameViewLayer::SendGameStart()
{
   m_needCallBanker = false;
   removeAllChildByTag(m_MyMoveOverCardTga);
   for (int i = 0; i < Ox4_GAME_PLAYER; i++)
   {
       removeChildByTag(m_ReadyTga[i]);
       removeAllChildByTag(m_BetTga[i]);
       removeAllChildByTag(m_CallBankTga[i]);
       removeAllChildByTag(m_GoldBtnTga[i]);
   }
   
   if (m_BtnReadyPlay != NULL)
   {
       m_BtnReadyPlay->setVisible(false);
   }
   SetMyCardData(0,0);
   
   m_UpCardIndex = 0;
   m_LeftCardIndex1 = 0;
   m_LeftCardIndex2 = 0;
   m_RightCardIndex1 = 0;
   m_RightCardIndex2 = 0;
   m_DownCardIndex = 0;
   ZeroMemory(m_HandCardData,sizeof(m_HandCardData));
   m_MyHandIndex = 0;
   m_SendCardIndex = 0;
   ZeroMemory(m_MyHandCardData,sizeof(m_MyHandCardData));
   m_bHaveNN = false;
   
   ClientSocketSink::sharedSocketSink()->SendData(MDM_GF_FRAME, SUB_GF_USER_READY);
	if (m_BtnReadyPlay != NULL)
	{
		m_BtnReadyPlay->setVisible(false);
	}
	StopTime();


}

void Ox4GameViewLayer::SendOpenCard()
{

	m_BtnOpenCard->setVisible(false);
   m_openBySelf = true;
   
    //扑克数据
    BYTE bCardData[Ox4_MAXCOUNT];
	for (int n = 0; n < 5; n++)
	{
		bCardData[n] = m_MyHandCardData[n];
	}
	BYTE bCardType=m_GameLogic.GetCardType(bCardData,5);

	if(bCardType > 0)
	{
        //排序
        m_GameLogic.GetOxCard(bCardData,5);
        
		SetMyCardData(bCardData,5);

		__String* ns=__String::createWithFormat("niu_%d.png",bCardType);
		Sprite* temp;
		if (bCardType >= 10)
		{
			temp = Sprite::createWithSpriteFrameName("niuniu.png");
		}
		else
		{
			temp = Sprite::createWithSpriteFrameName(ns->getCString());
		}
		temp->setPosition(getCardTypePos(2));
		temp->setTag(m_PlayerCardTypeTga[2]);
		addChild(temp);
	}
	else
	{
		Sprite* temp = Sprite::createWithSpriteFrameName("wuniu.png");
		temp->setPosition(getCardTypePos(2));
		temp->setTag(m_PlayerCardTypeTga[2]);
		addChild(temp);
	}
	
	//发送消息
	CMD_C_Ox4_OxCard OxCard;
    ZeroMemory(OxCard.mDivideCardData, sizeof(OxCard.mDivideCardData));
    CopyMemory(OxCard.mDivideCardData, m_HandCardData, sizeof(m_HandCardData));

	SendData(SUB_C_Ox4_OPEN_CARD, &OxCard, sizeof(OxCard));
}

bool Ox4GameViewLayer::OnBet( const void * pBuffer, WORD wDataSize )
{
	return true;
}

bool Ox4GameViewLayer::OnBetFail()
{
	//下注失败，您再试试吧
	AlertMessageLayer::createConfirm(this , "\u4e0b\u6ce8\u5931\u8d25\uff0c\u60a8\u518d\u8bd5\u8bd5\u5427");
	return true;
}

void Ox4GameViewLayer::AddTitle(const char* _str)
{
	removeAllChildByTag(m_TsTga);
	if (_str != NULL)
	{
        ui::Scale9Sprite*temp = ui::Scale9Sprite::createWithSpriteFrameName("Goldback_normal.png");
        temp->setContentSize(Size(500,80));
        temp->setPosition(Vec2(960,600));
        temp->setTag(m_TsTga);
        addChild(temp);
        
		Label *font=Label::createWithSystemFont(_str,_GAME_FONT_NAME_1_,50);
		font->setAnchorPoint(Vec2(0.5f,0.5f));
        font->setColor(Color3B::WHITE);
		font->setPosition(Vec2(250,40));
		temp->addChild(font);
	}
}

//用户叫庄
bool Ox4GameViewLayer::OnSubCallBanker(const void * pBuffer, WORD wDataSize)
{
	//效验数据
	if (wDataSize!=sizeof(CMD_S_Ox4_CallBanker)) return false;
	CMD_S_Ox4_CallBanker * pCallBanker=(CMD_S_Ox4_CallBanker *)pBuffer;
    
    //实际定时器
    StartTime(pCallBanker->mTime);
    m_GameState = enGameCallBank;
    
    removeAllChildByTag(m_TsTga);
    
	//始叫用户
    m_wBankerUser = pCallBanker->mNextActionChairID;
	if(!pCallBanker->mBankAction)
   {
       if (m_wBankerUser == GetMeChairID())
       {
           //控件显示
           m_CallBankYesBtn->setVisible(true);
           m_CallBankNoBtn->setVisible(true);
       }
       else
       {
           m_CallBankYesBtn->setVisible(false);
           m_CallBankNoBtn->setVisible(false);
           AddTitle("\u7b49\u5f85\u73a9\u5bb6\u53eb\u5e84");
       }
   }
   
	SoundUtil::sharedEngine()->playEffect("follow");

	return true;
}

//等待叫庄
void Ox4GameViewLayer::SetWaitCall(BYTE bCallUser)
{
    Sprite*temp = Sprite::createWithSpriteFrameName("CallBankNo.png");
	temp->setPosition(getReadyPos(bCallUser));
	temp->setTag(m_CallBankTga[bCallUser]);
	addChild(temp);

	return ;
}

void Ox4GameViewLayer::CleanLastGameInfo()
{
    SetPlayerCardData(0,0,0);
    for (WORD i = 0; i < Ox4_GAME_PLAYER; i++)
    {
        removeAllChildByTag(m_PlayerCardTypeTga[i]);
        removeAllChildByTag(m_SendCardTga[i]);
        removeAllChildByTag(m_DisCardTga[i]);
        removeAllChildByTag(m_CallBankTga[i]);
       
        const tagUserData* pUserData = GetUserData(i);
        m_cbPlayStatus[i] = false;
        if (pUserData == NULL)
            continue;
        //游戏信息
        m_cbPlayStatus[i]=true;
    }
}

bool Ox4GameViewLayer::OnSubGameStart( const void * pBuffer, WORD wDataSize )
{
	//效验数据
	if (wDataSize!=sizeof(CMD_S_Ox4_GameStart)) return false;
	CMD_S_Ox4_GameStart * pGameStart=(CMD_S_Ox4_GameStart *)pBuffer;
	SoundUtil::sharedEngine()->playEffect("GAME_START");

    m_wBankerUser = pGameStart->mNextActionChairID;
	removeAllChildByTag(m_BankTga);
   m_GameState = enGameCallBank;
   m_needCallBanker = true;
   StartTime(pGameStart->mTime);
   
   if (m_wBankerUser == GetMeChairID())
   {
       removeAllChildByTag(m_TsTga);
       //控件显示
       m_CallBankYesBtn->setVisible(true);
       m_CallBankNoBtn->setVisible(true);
   }
   else
   {
       AddTitle("\u7b49\u5f85\u73a9\u5bb6\u53eb\u5e84");
   }
   
   //用户信息
   CleanLastGameInfo();
    return true;
}

//进入用户下注状态
bool Ox4GameViewLayer::OnSubAddScore(const void * pBuffer, WORD wDataSize)
{
	//效验数据
	if (wDataSize!=sizeof(CMD_S_Ox4_AddScore)) return false;
	CMD_S_Ox4_AddScore * pAddScore=(CMD_S_Ox4_AddScore *)pBuffer;
	
    //用户信息
   if (!m_needCallBanker)
    CleanLastGameInfo();
    
    m_lTurnMaxScore = pAddScore->mInvestJettonGold;
    m_wBankerUser = pAddScore->mBankChairID;
    
    removeAllChildByTag(m_TsTga);
    m_GameState = enGameBet;
    StartTime(pAddScore->mTime);
    
    //视图位置
    WORD chair = g_GlobalUnits.SwitchViewChairID(pAddScore->mBankChairID);
    Sprite* temp = Sprite::createWithSpriteFrameName("bank.png");
    temp->setPosition(getBankTitlePos(chair));
    temp->setTag(m_BankTga);
    addChild(temp,100);
    
    if (m_wBankerUser == GetMeChairID())
    {
        AddTitle("\u7b49\u5f85\u73a9\u5bb6\u4e0b\u6ce8");
        return true;
    }
    
    if(m_cbDynamicJoin == true)
        return true;
    
    if (m_bAuto)
    {
        schedule(schedule_selector(Ox2GameViewLayer::AutoBet), 1);
    }
    else
    {
        LONGLONG lUserMaxScore[4];
        ZeroMemory(lUserMaxScore,sizeof(lUserMaxScore));
        
        for (int ik=0;ik<4;ik++)
        {
            LONGLONG lTemp=m_lTurnMaxScore / (ik + 1);
            lUserMaxScore[ik]= lTemp > 1 ? lTemp:1;
        }
        
        for (int i = 0; i< 4; i++)
        {
            m_GoldBtnSpr[i] = Sprite::createWithSpriteFrameName("Jetton.png");
            m_GoldBtnSpr[i]->setPosition(Vec2(480+i*320,390));
            addChild(m_GoldBtnSpr[i]);
            m_GoldBtnSpr[i]->setTag(m_GoldBtnTga[i]);
            char strcc[32]="";
            sprintf(strcc,"%lld", lUserMaxScore[i]);
            LabelAtlas* font = LabelAtlas::create(strcc, "Common/time_2.png", 39, 51, '+');
            font->setAnchorPoint(Vec2(0.5f, 0.5f));
            font->setPosition(Vec2(150, 37));
            m_GoldBtnSpr[i]->addChild(font);
        }
    }
    
    SoundUtil::sharedEngine()->playEffect("ADD_SCORE");
    return true;
}

//下注结果
bool Ox4GameViewLayer::OnSubAddScoreRes(const void * pBuffer, WORD wDataSize)
{
    //效验数据
    if (wDataSize!=sizeof(CMD_S_Ox4_AddScoreRes)) return false;
    CMD_S_Ox4_AddScoreRes * pAddScore=(CMD_S_Ox4_AddScoreRes *)pBuffer;
    
    //视图位置
    WORD chair = g_GlobalUnits.SwitchViewChairID(pAddScore->mActionChairID);
   
    Sprite* temp = Sprite::createWithSpriteFrameName("Goldback_normal.png");
    temp->setPosition(getGoldPos(chair));
    temp->setTag(m_BetTga[chair]);
    addChild(temp);
    char strcc[32]="";
    LONGLONG lMon = pAddScore->mInvestGold;
    memset(strcc , 0 , sizeof(strcc));
    Tools::AddComma(lMon , strcc);
    LabelAtlas* font = LabelAtlas::create(strcc, "Common/gold.png", 23, 32, '+');
    font->setAnchorPoint(Vec2(0.5f, 0.5f));
    font->setPosition(Vec2(150, 30));
    temp->addChild(font);
    SoundUtil::sharedEngine()->playEffect("ADD_SCORE");
    
    if (chair == 2)
    {
        for (int i = 0; i < 4; i++)
        {
            Node * temp= getChildByTag(m_GoldBtnTga[i]);
            if (temp != NULL)
                temp->setVisible(false);
        }
    }
    return true;
}

bool Ox4GameViewLayer::OnSubGameEnd(const void * pBuffer, WORD wDataSize)
{
    //效验参数
    if (wDataSize != sizeof(CMD_S_Ox4_GameEnd)) return false;
    CMD_S_Ox4_GameEnd * pGameEnd=(CMD_S_Ox4_GameEnd *)pBuffer;
    m_GameState = enGameEnd;
    StopTime();
    AddTitle("\u7b49\u5f85\u73a9\u5bb6\u51c6\u5907");
    
    //清理数据
    m_BtnOpenCard->setVisible(false);
    for(WORD i=0;i<Ox4_GAME_PLAYER;i++)
    {
        if(m_OpenCardSpr[i] != NULL) m_OpenCardSpr[i]->setVisible(false);
        removeAllChildByTag(m_OpenCardTga[i]);
        removeAllChildByTag(m_BetTga[i]);
    }
    
    //状态设置
    setGameStatus(GS_FREE);
    
    //显示牌型
    for (WORD i=0;i<Ox4_GAME_PLAYER;i++)
    {
        if (!m_cbPlayStatus[i]) continue;
        
        WORD wViewChairID = g_GlobalUnits.SwitchViewChairID(i);
        if (wViewChairID == 2 && m_openBySelf)	continue;
        
        //扑克数据
        BYTE bCardData[MAX_COUNT];
        for (int n = 0; n < 5; n++)
        {
            bCardData[n] = pGameEnd->mHandCardData[i][n];
        }
        if (bCardData[0] == 0)	continue;
        
        BYTE bCardType=m_GameLogic.GetCardType(bCardData,5);
        //有牛
        if(bCardType > 0)
        {
            //排序
            m_GameLogic.GetOxCard(bCardData,5);
            
            SetPlayerCardData(wViewChairID,bCardData,5);
            __String* ns=__String::createWithFormat("niu_%d.png",bCardType);
            Sprite* temp;
            if (bCardType >= 10)
            {
                temp = Sprite::createWithSpriteFrameName("niuniu.png");
                m_bHaveNN = true;
            }
            else
            {
                temp = Sprite::createWithSpriteFrameName(ns->getCString());
            }
            temp->setPosition(getCardTypePos(wViewChairID));
            temp->setTag(m_PlayerCardTypeTga[wViewChairID]);
            addChild(temp);
        }
        else //没牛
        {
            SetPlayerCardDataNone(wViewChairID,bCardData,5);
            Sprite* temp = Sprite::createWithSpriteFrameName("wuniu.png");
            temp->setPosition(getCardTypeNonePos(wViewChairID));
            temp->setTag(m_PlayerCardTypeTga[wViewChairID]);
            addChild(temp);
        }
    }
    
    if (m_bHaveNN) //
    {
        SoundUtil::sharedEngine()->playEffect("GAME_OXOX");
    }
    else
    {
        //播放声音
        int chair = ClientSocketSink::sharedSocketSink()->GetMeUserData()->wChairID;
        if (pGameEnd->mGameGold[chair]>0L)
        {
            SoundUtil::sharedEngine()->playEffect("GAME_WIN");
        }
        else
        {
            SoundUtil::sharedEngine()->playEffect("GAME_LOST");
        }
    }
    
    //用户信息
    for (WORD i = 0; i < Ox4_GAME_PLAYER; i++)
    {
        if (!m_cbPlayStatus[i]) continue;
        
        Sprite* temp = Sprite::createWithSpriteFrameName("Goldback_normal.png");
        temp->setPosition(getGoldPos(g_GlobalUnits.SwitchViewChairID(i)));
        temp->setTag(m_BetTga[g_GlobalUnits.SwitchViewChairID(i)]);
        addChild(temp);
        char strc[32]="";
        
        if (pGameEnd->mGameGold[i]>0L)
        {
            sprintf(strc,"+%lld", pGameEnd->mGameGold[i]);
        }
        else
        {
            sprintf(strc,"%lld", pGameEnd->mGameGold[i]);
        }
        LabelAtlas* font = LabelAtlas::create(strc, "Common/gold.png", 23, 32, '+');
        font->setAnchorPoint(Vec2(0.5f, 0.5f));
        font->setPosition(Vec2(150, 30));
        temp->addChild(font);
    }	
    
    auto userData = ClientSocketSink::sharedSocketSink()->GetMeUserData();
    auto myScore = userData->lScore;
    if (myScore > 0)
    {
        m_BtnReadyPlay->setVisible(true);
        StartTime(15);
    }
    
    return true;
}

//游戏结束
bool Ox4GameViewLayer::OnSubGameEndPlayerLeave(const void * pBuffer, WORD wDataSize)
{
    //效验参数
	if (wDataSize != sizeof(CMD_S_CheckOutLeft)) return false;
	CMD_S_CheckOutLeft * pGameEnd=(CMD_S_CheckOutLeft *)pBuffer;
	m_GameState = enGameEnd;
	StopTime();
	AddTitle("\u7b49\u5f85\u73a9\u5bb6\u51c6\u5907");
    
	//清理数据
	m_BtnOpenCard->setVisible(false);
	for(WORD i=0;i<Ox4_GAME_PLAYER;i++)
	{
		if(m_OpenCardSpr[i] != NULL)
            m_OpenCardSpr[i]->setVisible(false);
		removeAllChildByTag(m_OpenCardTga[i]);
		removeAllChildByTag(m_BetTga[i]);		
	}
    
	removeAllChildByTag(m_TsTga);
    
    for (int i = 0; i < 4; i++)
        removeAllChildByTag(m_GoldBtnTga[i]);
    
    m_CallBankYesBtn->setVisible(false);
    m_CallBankNoBtn->setVisible(false);
    
	//状态设置
	setGameStatus(GS_FREE);

    //播放声音

    SoundUtil::sharedEngine()->playEffect("GAME_WIN");
		
	//用户信息
    for (WORD i = 0; i < Ox4_GAME_PLAYER; i++)
	{
		if (!m_cbPlayStatus[i]) continue;
        
		Sprite* temp = Sprite::createWithSpriteFrameName("Goldback_normal.png");
		temp->setPosition(getGoldPos(g_GlobalUnits.SwitchViewChairID(i)));
		temp->setTag(m_BetTga[g_GlobalUnits.SwitchViewChairID(i)]);
		addChild(temp);
		char strc[32]="";		
		
		if (pGameEnd->mGameGold[i]>0L)
		{
			sprintf(strc,"+%lld", pGameEnd->mGameGold[i]);
		}
		else
		{
			sprintf(strc,"%lld", pGameEnd->mGameGold[i]);
		}
        LabelAtlas* font = LabelAtlas::create(strc, "Common/gold.png", 23, 32, '+');
        font->setAnchorPoint(Vec2(0.5f, 0.5f));
        font->setPosition(Vec2(150, 30));
		temp->addChild(font);
	}	

    m_BtnReadyPlay->setVisible(true);
    StartTime(15);
	

	return true;
}

void Ox4GameViewLayer::AutoStartGame(float dt)
{
	unschedule(schedule_selector(Ox4GameViewLayer::AutoStartGame));
	SendGameStart();
}

void Ox4GameViewLayer::AutoCallBank(float dt)
{
	unschedule(schedule_selector(Ox4GameViewLayer::AutoCallBank));
	CMD_C_Ox4_CallBanker CallBanker;
	CallBanker.bBanker = 2;
	SendData(SUB_C_Ox4_CALL_BANKER,&CallBanker,sizeof(CallBanker));
}

void Ox4GameViewLayer::AutoBet(float dt)
{
	SoundUtil::sharedEngine()->playEffect("ADD_SCORE"); 
	unschedule(schedule_selector(Ox4GameViewLayer::AutoBet));
	LONGLONG lUserMaxScore[4];
	ZeroMemory(lUserMaxScore,sizeof(lUserMaxScore));
	lUserMaxScore[0]= m_lTurnMaxScore/1?m_lTurnMaxScore/1:1L;
	lUserMaxScore[1]= m_lTurnMaxScore*4/5?m_lTurnMaxScore*4/5:1L;
	lUserMaxScore[2]= m_lTurnMaxScore/2?m_lTurnMaxScore/2:1L; 
	lUserMaxScore[3]= m_lTurnMaxScore/5?m_lTurnMaxScore/5:1L;
	//随机
	if (m_TG_bRand) m_TG_BetType = rand()%4;

	CMD_C_Ox4_AddScore AddScore;
    AddScore.mInvestIndex=0;
	SendData(SUB_C_Ox4_ADD_SCORE,&AddScore,sizeof(AddScore));
	for (int i = 0; i < 4; i++)
	{
		Node * temp= getChildByTag(m_GoldBtnTga[i]);
		if (temp != NULL) temp->setVisible(false);
	}
}

//用户摊牌
bool Ox4GameViewLayer::OnSubEnterOpenCard(const void * pBuffer, WORD wDataSize)
{
    //效验数据
    if (wDataSize!=sizeof(CMD_S_DivideStatus)) return false;
    CMD_S_DivideStatus * pOpenCard=(CMD_S_DivideStatus *)pBuffer;
    m_GameState = enGameOpenCard;
    StartTime(pOpenCard->mTime);
    for (int i = 0; i < 4; i++)
    {
        WORD wViewChairID=g_GlobalUnits.SwitchViewChairID(i);
        
        if (wViewChairID == 2 && m_cbPlayStatus[i] == true)
            m_BtnOpenCard->setVisible(true);
    }
    return true;
}

//用户摊牌
bool Ox4GameViewLayer::OnSubOpenCard(const void * pBuffer, WORD wDataSize)
{
	//效验数据
	if (wDataSize!=sizeof(CMD_S_Ox4_Open_Card)) return false;
	CMD_S_Ox4_Open_Card * pOpenCard=(CMD_S_Ox4_Open_Card *)pBuffer;
	m_GameState = enGameOpenCard;
	SoundUtil::sharedEngine()->playEffect("PASS_CARD"); 
	WORD wID=pOpenCard->mActionChairID;
	
	WORD wViewChairID= g_GlobalUnits.SwitchViewChairID(wID);

	if (m_cbDynamicJoin) return true;

	if (wViewChairID == 2)
   {
       m_BtnOpenCard->setVisible(false);
       return true;
   }
	Sprite* temp = Sprite::createWithSpriteFrameName("opencardok.png");
	temp->setPosition(getOpenPos(wViewChairID));
	temp->setLocalZOrder(100);
	temp->setTag(m_OpenCardTga[wViewChairID]);
	addChild(temp);

	return true;
}

void Ox4GameViewLayer::SendCard(float dt)
{
	//派发扑克
	if(m_SendCardIndex >= 5)
	{
		unschedule(schedule_selector(Ox4GameViewLayer::SendCard));
		StartTime(10);
		return;
	}
	SoundUtil::sharedEngine()->playEffect("SEND_CARD"); 
	for (WORD j=m_wBankerUser; j<m_wBankerUser+Ox4_GAME_PLAYER; j++)
	{
		WORD w=j%Ox4_GAME_PLAYER;
		//该位置有用户游戏中
		if (m_cbPlayStatus[w]==true)
		{
			WORD wViewChairID=g_GlobalUnits.SwitchViewChairID(w);
			//有牌
            
			if(wViewChairID == 2)
			{
				dispatchCards(wViewChairID,m_HandCardData[m_SendCardIndex]);
			}
            else
            {
                dispatchCards(wViewChairID, 0);
            }
		}
	}
	m_SendCardIndex++;
}

//发牌消息
bool Ox4GameViewLayer::OnSubSendCard(const void * pBuffer, WORD wDataSize)
{
	//效验数据
	if (wDataSize!=sizeof(CMD_S_Ox4_SendCard)) return false;
	CMD_S_Ox4_SendCard * pSendCard=(CMD_S_Ox4_SendCard *)pBuffer;
	CopyMemory(m_HandCardData, pSendCard->mHandCardData, sizeof(m_HandCardData));
    
    removeAllChildByTag(m_TsTga);

    m_openBySelf = false;
	m_GameState = enGameSendCard;
	schedule(schedule_selector(Ox4GameViewLayer::SendCard), 0.1f);

	return true;
}

void Ox4GameViewLayer::dispatchCards(WORD chair, BYTE card) 
{ 
	switch (chair) 
	{ 
	case 0:  //正上方
		{ 
			if(m_UpCardIndex >= 5) return;
			MoveTo *leftMoveBy = MoveTo::create(0.2f, Vec2(_STANDARD_SCREEN_CENTER_.x-80 + m_UpCardIndex++*40, 900));
			Sprite* temp = Sprite::createWithSpriteFrameName("Ox4backCard.png");
            temp->setScale(0.7f);
			temp->setTag(m_SendCardTga[0]);
			addChild(temp);
			temp->setPosition(Vec2(_STANDARD_SCREEN_CENTER_.x,_STANDARD_SCREEN_CENTER_.y));
			temp->runAction(leftMoveBy);
			
			break; 
		} 
	case 1: //左上1
		{
			if(m_LeftCardIndex1 >= 5) return;
			MoveTo *leftMoveBy = MoveTo::create(0.2f, Vec2(480+m_LeftCardIndex1++*40, 540));
			Sprite* temp = Sprite::createWithSpriteFrameName("Ox4backCard.png");
            temp->setScale(0.7f);
			temp->setTag(m_SendCardTga[1]);
			addChild(temp);
			temp->setPosition(Vec2(_STANDARD_SCREEN_CENTER_.x,_STANDARD_SCREEN_CENTER_.y));
			temp->runAction(leftMoveBy);
			break; 
		} 
	case 2:
		{
			if(m_DownCardIndex >= 5) return;
			m_MyHandCardData[m_DownCardIndex] = card;
			MoveTo *leftMoveBy = MoveTo::create(0.2f, Vec2(_STANDARD_SCREEN_CENTER_.x - 120 + m_DownCardIndex++*60, 140));
			ActionInstant *func = CallFunc::create(CC_CALLBACK_0(Ox4GameViewLayer::CardMoveCallback, this));
			Sprite* temp = Sprite::createWithSpriteFrameName("Ox4backCard.png");
            temp->setScale(0.7f);
			temp->setTag(m_SendCardTga[2]);
			addChild(temp);
			temp->setPosition(Vec2(_STANDARD_SCREEN_CENTER_.x,_STANDARD_SCREEN_CENTER_.y));
			temp->runAction(Sequence::create(leftMoveBy,func,NULL));
			break;
		}
	case 3: //右上1
		{
			if(m_RightCardIndex1 >= 5) return;
			MoveTo *leftMoveBy = MoveTo::create(0.2f, Vec2(1440-m_RightCardIndex1++*40, 540));
			Sprite* temp = Sprite::createWithSpriteFrameName("Ox4backCard.png");
            temp->setScale(0.7f);
			temp->setTag(m_SendCardTga[3]);
			addChild(temp);
			temp->setPosition(Vec2(_STANDARD_SCREEN_CENTER_.x,_STANDARD_SCREEN_CENTER_.y));
			temp->runAction(leftMoveBy);
			
			break; 
		} 
	default: 
		break; 
	} 
} 

void Ox4GameViewLayer::CardMoveCallback()
{
	AddTitle("\u7b49\u5f85\u73a9\u5bb6\u5f00\u724c");
	removeChildByTag(m_SendCardTga[2]);
	string _name = GetCardStringName(m_MyHandCardData[m_MyHandIndex]);
    if (_name == "")
        return;
	Sprite* temp = Sprite::createWithSpriteFrameName(_name.c_str());
	temp->setPosition(Vec2(_STANDARD_SCREEN_CENTER_.x - 120 + m_MyHandIndex*60,140));
	temp->setTag(m_MyMoveOverCardTga);		
	addChild(temp);
	m_MyHandIndex++;
	if(m_DownCardIndex >= 5)
	{
		if (m_bAuto)
		{
			schedule(schedule_selector(Ox4GameViewLayer::AutoOpenCard), 1);
		}
	}
}

void Ox4GameViewLayer::AutoOpenCard(float dt)
{
	unschedule(schedule_selector(Ox4GameViewLayer::AutoOpenCard));
	SendOpenCard();
}

string Ox4GameViewLayer::GetCardStringName(BYTE card)
{
    if (card == 0) return "Ox4backCard.png";
    char tt[32];
    sprintf(tt,"%0x",card);
    BYTE _value = m_GameLogic.GetCardValue(card);
    BYTE _color = m_GameLogic.GetCardColor(card);
    string temp;
    if (_color == 0) temp = "fangkuai_";
    if (_color == 1) temp = "meihua_";
    if (_color == 2) temp = "hongtao_";
    if (_color == 3) temp = "heitao_";
    char _valstr[32];
    ZeroMemory(_valstr,32);
    if (card == 0x41) //小王
    {
        sprintf(_valstr,"xiaowang.png");
    }
    else if (card == 0x42)
    {
        sprintf(_valstr,"dawang.png");
    }
    else if (_value < 10)
    {
        sprintf(_valstr,"0%d.png",_value);
    }
    else
    {
        sprintf(_valstr,"%d.png",_value);
    }
    
    temp+=_valstr;
    return temp;
}

void Ox4GameViewLayer::InitGame()
{
	//背景图
    m_BackSpr = Sprite::create("Common/common_gameBg.jpg");
    m_BackSpr->setPosition(_STANDARD_SCREEN_CENTER_IN_PIXELS_);
    addChild(m_BackSpr);
    
    Sprite *table = Sprite::createWithSpriteFrameName("game_back.png");
    table->setPosition(_STANDARD_SCREEN_CENTER_);
    m_BackSpr->addChild(table);
    
	m_ClockSpr = Sprite::createWithSpriteFrameName("timeback.png");
	m_ClockSpr->setPosition(Vec2(_STANDARD_SCREEN_CENTER_IN_PIXELS_.x+10,_STANDARD_SCREEN_CENTER_IN_PIXELS_.y-60));
	addChild(m_ClockSpr);
	StartTime(20);

	for (int i = 0; i < Ox4_GAME_PLAYER; i++)
	{
		m_ReadyTga[i] = 100+i;
		m_HeadTga[i]  = 110+i;
		m_GlodTga[i]  = 120+i;
		m_NickNameTga[i] = 130+i;
		m_PlayerCardTypeTga[i] = 140+i;
		m_BetTga[i] = 150+i;
		m_SendCardTga[i] = 160+i;
		m_DisCardTga[i] = 170+i;
		m_OpenCardTga[i] = 180+i;
		m_CallBankTga[i] = 190+i;
		m_GoldBtnTga[i] = 200+i;
	}
	m_MyMoveOverCardTga = 99;
	m_BankTga = 98;
	m_TsTga = 89;
}

void Ox4GameViewLayer::AddButton()
{
	m_BtnReadyPlay = CreateButton("game_Start" ,Vec2(960,360),Btn_Ready);
	addChild(m_BtnReadyPlay , 0 , Btn_Ready);
	m_BtnReadyPlay->setVisible(false);

	m_BtnBackToLobby = CreateButton("Returnback" ,Vec2(90,1000),Btn_BackToLobby);
	addChild(m_BtnBackToLobby , 0 , Btn_BackToLobby);
	m_BtnBackToLobby->setVisible(true);

	m_BtnOpenCard = CreateButton("OpenCard" ,Vec2(960,360),Btn_OpenCard);
	addChild(m_BtnOpenCard , 0 , Btn_OpenCard);
	m_BtnOpenCard->setVisible(false);
	
	m_BtnSeting = CreateButton("seting" ,Vec2(1810,1000),Btn_Seting);
	addChild(m_BtnSeting , 0 , Btn_Seting);
	m_BtnSeting->setVisible(true);

	m_CallBankYesBtn = CreateButton("CallBankOk" ,Vec2(760,360),Btn_CallBankYesBtn);
	addChild(m_CallBankYesBtn , 0 , Btn_CallBankYesBtn);
	m_CallBankYesBtn->setVisible(false);
	
	m_CallBankNoBtn = CreateButton("CallBankCancel" ,Vec2(1160,360),Btn_CallBankNoBtn);
	addChild(m_CallBankNoBtn , 0 , Btn_CallBankNoBtn);
	m_CallBankNoBtn->setVisible(false);

	m_TG_MaxBtn = CreateButton("TG_Max" ,Vec2(395,227),Btn_TG_Max);
	addChild(m_TG_MaxBtn , 1001 , Btn_TG_Max);
	m_TG_MaxBtn->setVisible(false);
	
	m_TG_5f4Btn = CreateButton("TG5f4" ,Vec2(565,227),Btn_TG_5f4);
	addChild(m_TG_5f4Btn , 1001 , Btn_TG_5f4);
	m_TG_5f4Btn->setVisible(false);

	m_TG_2f1Btn = CreateButton("TG2f1" ,Vec2(735,227),Btn_TG_2f1);
	addChild(m_TG_2f1Btn , 1001 , Btn_TG_2f1);
	m_TG_2f1Btn->setVisible(false);

	m_TG_5f1Btn = CreateButton("TG5f1" ,Vec2(905,227),Btn_TG_5f1);
	addChild(m_TG_5f1Btn , 1001 , Btn_TG_5f1);
	m_TG_5f1Btn->setVisible(false);

	m_TG_RandBtn = CreateButton("TG_Rand" ,Vec2(505,130),Btn_TG_Rand);
	addChild(m_TG_RandBtn , 1001 , Btn_TG_Rand);
	m_TG_RandBtn->setVisible(false);

	m_TG_CancelBtn = CreateButton("TG_Cancel" ,Vec2(795,130),Btn_TG_Cancel);
	addChild(m_TG_CancelBtn , 1001 , Btn_TG_Cancel);
	m_TG_CancelBtn->setVisible(false);
}

Menu* Ox4GameViewLayer::CreateButton(std::string szBtName ,const Vec2 &p , int tag )
{
	return Tools::Button(StrToChar(szBtName+"_normal.png") , StrToChar(szBtName+"_click.png") , p, this , menu_selector(Ox4GameViewLayer::callbackBt) , tag);
}

void Ox4GameViewLayer::AddPlayerInfo()
{
    Vec2 headPos[4] = {Vec2(480,960), Vec2(200,540), Vec2(480,120), Vec2(1720,540)};
    
    for (int i = 0; i < Ox4_GAME_PLAYER; i++)
	{
		if (ClientSocketSink::sharedSocketSink()->GetMeUserData() == NULL)	return;
		const tagUserData* pUserData = GetUserData(i);
		int chair = g_GlobalUnits.SwitchViewChairID(i);
		if (pUserData != NULL)
		{
			//头像
			removeAllChildByTag(m_HeadTga[chair]);
            Sprite* playerInfoBg = Sprite::createWithSpriteFrameName("bg_player_info.png");
            playerInfoBg->setPosition(headPos[chair]);
            addChild(playerInfoBg, 0, m_HeadTga[chair]);
            
            Sprite* headBg = Sprite::createWithSpriteFrameName("bg_head_img.png");
            headBg->setPosition(Vec2(70, 73));
            playerInfoBg->addChild(headBg);
            
            string heads = g_GlobalUnits.getFace(pUserData->wGender, pUserData->lScore);
            Sprite* mHead = Sprite::createWithSpriteFrameName(heads);
            mHead->setPosition(Vec2(70, 73));
            mHead->setScale(0.9f);
            playerInfoBg->addChild(mHead);

            //金币
            removeAllChildByTag(m_GlodTga[chair]);
            char strc[32]="";
            LONGLONG lMon = pUserData->lScore;
            memset(strc , 0 , sizeof(strc));
            Tools::AddComma(lMon , strc);
            Label* mGoldFont;
            mGoldFont = Label::createWithSystemFont(strc,_GAME_FONT_NAME_1_,30);
            mGoldFont->setTag(m_GlodTga[chair]);
            playerInfoBg->addChild(mGoldFont);
            mGoldFont->setColor(Color3B::YELLOW);
            mGoldFont->setAnchorPoint(Vec2(0, 0.5f));
            mGoldFont->setPosition(Vec2(140, 90));

			//昵称
            removeAllChildByTag(m_NickNameTga[chair]);
            Label* mNickfont;
            mNickfont = Label::createWithSystemFont(gbk_utf8(pUserData->szNickName), _GAME_FONT_NAME_1_,30);
            mNickfont->setTag(m_NickNameTga[chair]);
            playerInfoBg->addChild(mNickfont);
            mNickfont->setAnchorPoint(Vec2(0,0.5f));
            mNickfont->setPosition(Vec2(140, 40));

			//准备
			removeAllChildByTag(m_ReadyTga[chair]);
			Sprite* mReaderSpr = Sprite::createWithSpriteFrameName("game_ready.png");
			mReaderSpr->setPosition(getReadyPos(chair));
			mReaderSpr->setTag(m_ReadyTga[chair]);
			addChild(mReaderSpr);

			if (pUserData->cbUserStatus == US_READY)
			{
				getChildByTag(m_ReadyTga[chair])->setVisible(true);
			}
			else
                getChildByTag(m_ReadyTga[chair])->setVisible(false);
		}
		else
		{
			if (chair == 2 && !g_GlobalUnits.m_bLeaveGameByServer) //如果是自己退出，就关闭游戏
			{
//                AlertMessageLayer::createConfirm(this , "\u60a8\u7684\u9152\u5427\u8c46\u4e0d\u8db3\uff0c\u4e0d\u80fd\u7ee7\u7eed\u6e38\u620f\uff01", menu_selector(IGameView::backLoginView));
                 backLoginView(nullptr);
				return;
			}

			removeAllChildByTag(m_HeadTga[chair]);
			removeAllChildByTag(m_GlodTga[chair]);
			removeAllChildByTag(m_NickNameTga[chair]);
			removeAllChildByTag(m_ReadyTga[chair]);
			removeAllChildByTag(m_SendCardTga[chair]);
		}
	}
}

string Ox4GameViewLayer::AddCommaToNum(LONG Num)
{
	char _str[256];
	sprintf(_str,"%ld", Num);
	string _string = _str;
	auto step = _string.length()/3;
	for (int i = 1; i <= step; i++)
	{
		_string.insert(_string.length()-(i-1)-(i*3), ",");
	}
	return _string;
}

void Ox4GameViewLayer::SendReqTaskConfig()
{
	CMD_C_GP_GetTask GetTask;
	ZeroMemory(&GetTask , sizeof(GetTask));
	GetTask.dwUserID = g_GlobalUnits.GetGolbalUserData().dwUserID;
	GetTask.wKindID = Ox4_KIND_ID;
	SendMobileData(SUB_C_GP_GETTASK , &GetTask , sizeof(GetTask));
}

void Ox4GameViewLayer::callbackBt(Ref *pSender )
{
	Node *pNode = (Node *)pSender;
	switch (pNode->getTag())
	{
	case Btn_TG_Max:	//托管下最大注
		{
			m_TG_bRand = false;
			m_TG_BetType = 0;
			m_bAuto = true;
//			m_AutoOkBtn->setVisible(false);
//			m_AutoCancleBtn->setVisible(true);
			if (m_TG_BackSpr != NULL) m_TG_BackSpr->setVisible(false);
			if(m_TG_MaxBtn != NULL) m_TG_MaxBtn->setVisible(false);
			if(m_TG_5f4Btn != NULL) m_TG_5f4Btn->setVisible(false);
			if(m_TG_2f1Btn != NULL) m_TG_2f1Btn->setVisible(false);
			if(m_TG_5f1Btn != NULL) m_TG_5f1Btn->setVisible(false);
			if(m_TG_RandBtn != NULL) m_TG_RandBtn->setVisible(false);
			if(m_TG_CancelBtn != NULL) m_TG_CancelBtn->setVisible(false);
			m_TG_BackShow = false;
			break;
		}
	case Btn_TG_5f4:
		{
			m_TG_bRand = false;
			m_TG_BetType = 1;
			m_bAuto = true;
//			m_AutoOkBtn->setVisible(false);
//			m_AutoCancleBtn->setVisible(true);	
			if (m_TG_BackSpr != NULL) m_TG_BackSpr->setVisible(false);
			if(m_TG_MaxBtn != NULL) m_TG_MaxBtn->setVisible(false);
			if(m_TG_5f4Btn != NULL) m_TG_5f4Btn->setVisible(false);
			if(m_TG_2f1Btn != NULL) m_TG_2f1Btn->setVisible(false);
			if(m_TG_5f1Btn != NULL) m_TG_5f1Btn->setVisible(false);
			if(m_TG_RandBtn != NULL) m_TG_RandBtn->setVisible(false);
			if(m_TG_CancelBtn != NULL) m_TG_CancelBtn->setVisible(false);
			m_TG_BackShow = false;
			break;
		}
	case Btn_TG_2f1:
		{
			m_TG_bRand = false;
			m_TG_BetType = 2;
			m_bAuto = true;
//			m_AutoOkBtn->setVisible(false);
//			m_AutoCancleBtn->setVisible(true);
			if (m_TG_BackSpr != NULL) m_TG_BackSpr->setVisible(false);
			if(m_TG_MaxBtn != NULL) m_TG_MaxBtn->setVisible(false);
			if(m_TG_5f4Btn != NULL) m_TG_5f4Btn->setVisible(false);
			if(m_TG_2f1Btn != NULL) m_TG_2f1Btn->setVisible(false);
			if(m_TG_5f1Btn != NULL) m_TG_5f1Btn->setVisible(false);
			if(m_TG_RandBtn != NULL) m_TG_RandBtn->setVisible(false);
			if(m_TG_CancelBtn != NULL) m_TG_CancelBtn->setVisible(false);
			m_TG_BackShow = false;
			break;
		}
	case Btn_TG_5f1:
		{
			m_TG_bRand = false;
			m_TG_BetType = 3;
			m_bAuto = true;
//			m_AutoOkBtn->setVisible(false);
//			m_AutoCancleBtn->setVisible(true);
			if (m_TG_BackSpr != NULL) m_TG_BackSpr->setVisible(false);
			if(m_TG_MaxBtn != NULL) m_TG_MaxBtn->setVisible(false);
			if(m_TG_5f4Btn != NULL) m_TG_5f4Btn->setVisible(false);
			if(m_TG_2f1Btn != NULL) m_TG_2f1Btn->setVisible(false);
			if(m_TG_5f1Btn != NULL) m_TG_5f1Btn->setVisible(false);
			if(m_TG_RandBtn != NULL) m_TG_RandBtn->setVisible(false);
			if(m_TG_CancelBtn != NULL) m_TG_CancelBtn->setVisible(false);
			m_TG_BackShow = false;
			break;
		}
	case Btn_TG_Rand:
		{
			m_TG_bRand = true;
			m_TG_BetType = rand()%4;
			m_bAuto = true;
//			m_AutoOkBtn->setVisible(false);
//			m_AutoCancleBtn->setVisible(true);
			if (m_TG_BackSpr != NULL) m_TG_BackSpr->setVisible(false);
			if(m_TG_MaxBtn != NULL) m_TG_MaxBtn->setVisible(false);
			if(m_TG_5f4Btn != NULL) m_TG_5f4Btn->setVisible(false);
			if(m_TG_2f1Btn != NULL) m_TG_2f1Btn->setVisible(false);
			if(m_TG_5f1Btn != NULL) m_TG_5f1Btn->setVisible(false);
			if(m_TG_RandBtn != NULL) m_TG_RandBtn->setVisible(false);
			if(m_TG_CancelBtn != NULL) m_TG_CancelBtn->setVisible(false);
			m_TG_BackShow = false;
			break;
		}
	case Btn_TG_Cancel:
		{
			if (m_TG_BackSpr != NULL) m_TG_BackSpr->setVisible(false);
			if(m_TG_MaxBtn != NULL) m_TG_MaxBtn->setVisible(false);
			if(m_TG_5f4Btn != NULL) m_TG_5f4Btn->setVisible(false);
			if(m_TG_2f1Btn != NULL) m_TG_2f1Btn->setVisible(false);
			if(m_TG_5f1Btn != NULL) m_TG_5f1Btn->setVisible(false);
			if(m_TG_RandBtn != NULL) m_TG_RandBtn->setVisible(false);
			if(m_TG_CancelBtn != NULL) m_TG_CancelBtn->setVisible(false);
			m_TG_BackShow = false;
			break;
		}
	case Btn_Ready:	//准备按钮
		{
			if(m_TG_BackShow) break;
			SendGameStart();
			break;
		}
	case Btn_BackToLobby: // 返回大厅按钮
		{
			SoundUtil::sharedEngine()->playEffect("buttonMusic");
			BackToLobby();
			break;
		}
	case Btn_OpenCard:	//开牌按钮
		{
			if(m_TG_BackShow) break;
			SendOpenCard();
			break;
		}
	case Btn_Seting: //设置
		{
			SoundUtil::sharedEngine()->playEffect("buttonMusic");
			GameLayerMove::sharedGameLayerMoveSink()->OpenSeting();
			break;
		}
	case Btn_AutoOk:	//托管
		{
			if (m_TG_BackSpr == NULL)
			{
				m_TG_BackSpr = Sprite::createWithSpriteFrameName("TG_Back.png");
				m_TG_BackSpr->setPosition(Vec2(650,200));
				addChild(m_TG_BackSpr,1000);
			}
			else
			{
				m_TG_BackSpr->setVisible(true);
			}
			if(m_TG_MaxBtn != NULL) m_TG_MaxBtn->setVisible(true);
			if(m_TG_5f4Btn != NULL) m_TG_5f4Btn->setVisible(true);
			if(m_TG_2f1Btn != NULL) m_TG_2f1Btn->setVisible(true);
			if(m_TG_5f1Btn != NULL) m_TG_5f1Btn->setVisible(true);
			if(m_TG_RandBtn != NULL) m_TG_RandBtn->setVisible(true);
			if(m_TG_CancelBtn != NULL) m_TG_CancelBtn->setVisible(true);
			m_TG_BackShow = true;

			break;
		}		
	case Btn_AutoCancle:	//取消托管
		{
			if (m_TG_BackSpr != NULL) m_TG_BackSpr->setVisible(false);
			if(m_TG_MaxBtn != NULL) m_TG_MaxBtn->setVisible(false);
			if(m_TG_5f4Btn != NULL) m_TG_5f4Btn->setVisible(false);
			if(m_TG_2f1Btn != NULL) m_TG_2f1Btn->setVisible(false);
			if(m_TG_5f1Btn != NULL) m_TG_5f1Btn->setVisible(false);
			if(m_TG_RandBtn != NULL) m_TG_RandBtn->setVisible(false);
			if(m_TG_CancelBtn != NULL) m_TG_CancelBtn->setVisible(false);
			m_TG_BackShow = false;
			m_TG_bRand = false;
			m_bAuto = false;
//			m_AutoOkBtn->setVisible(true);
//			m_AutoCancleBtn->setVisible(false);
			break;
		}
	case Btn_CallBankYesBtn: //抢庄
		{
			if(m_TG_BackShow) break;
			m_CallBankYesBtn->setVisible(false);
			m_CallBankNoBtn->setVisible(false);
			CMD_C_Ox4_CallBanker CallBanker;
			CallBanker.bBanker = true;
			SendData(SUB_C_Ox4_CALL_BANKER,&CallBanker,sizeof(CallBanker));
			break;
		}
	case Btn_CallBankNoBtn: //不抢庄
		{
			if(m_TG_BackShow) break;
			m_CallBankYesBtn->setVisible(false);
			m_CallBankNoBtn->setVisible(false);
			CMD_C_Ox4_CallBanker CallBanker;
			CallBanker.bBanker = false;
			SendData(SUB_C_Ox4_CALL_BANKER,&CallBanker,sizeof(CallBanker));
			break;
		}
	}
}

void Ox4GameViewLayer::backLoginView(Ref *pSender )
{
	IGameView::backLoginView(pSender);	
}

void Ox4GameViewLayer::GameEnd()
{
}

void Ox4GameViewLayer::UpdateDrawUserScore()
{
}

void Ox4GameViewLayer::OnQuit()
{
	m_pGameScene->removeChild(this);
}
