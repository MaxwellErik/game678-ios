﻿#include "SKGameViewLayer.h"
#include "LobbyLayer.h"
#include "ClientSocketSink.h"
#include "HXmlParse.h"
#include "LocalDataUtil.h"
#include "JniSink.h"
#include "VisibleRect.h"
#include "HttpConstant.h"
#include "Screen.h"
#include "LobbySocketSink.h"
#include "LoginLayer.h"
#include "GameLayerMove.h"
#include "GameGetScore.h"
#include "SKGameLogicBase.h"
#include "Convert.h"

#define		MENU_INIT_POINT			(Vec2Make(-_STANDARD_SCREEN_CENTER_.x+68+30 , _STANDARD_SCREEN_CENTER_.y-68-20))

#define		ADV_SIZE				(CCSizeMake(520,105))
//宝箱坐标
#define		BOX_POINT				(ccpx(1195,70))

//最少筹码数
#define		MIN_PROP_PAY_SCORE			1
#define		ALL_ANIMATION_COUNT			5


#define		MAX_CREDIT					(9999999)
#define		MAX_BET						(9999)
#define		MAX_ADV_COUNT				(10)

#if (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
#define		BIG_AWARD_MULITY			10
#else
#define		BIG_AWARD_MULITY			30
#endif // _DEBUG
#define MYSELF_VIEW_ID				1									//视图位置
#define JETTON_COUNT				7

#define Btn_Ready						1
#define Btn_BackToLobby					2
#define Btn_Seting						3
#define Btn_Pop							4
#define Btn_Pass						5
#define Btn_OutCard						6
#define Btn_GetScoreBtn					7

typedef vector<BYTE> skvector_carddata;

SKGameViewLayer::SKGameViewLayer(GameScene *pGameScene)
	:IGameView(pGameScene)
{
	m_cbTimeOutCard = 0;	
	m_cbTimeCallScore = 0;	
	m_cbTimeStartGame = 0;	
	m_cbTimeHeadOutCard = 0;
	m_GameState=enGameNormal;
	m_cbTurnCardCount = 0;
	m_BoomCount = 0;
	m_bCanOutCard = false;
	m_lCellScore = 0;
	m_nTimes[0] = 0;
	m_nTimes[1] = 0;
	m_lGongxian = 0;
	m_wMeChairID = 0;
	SetKindId(SK_KIND_ID);
}

SKGameViewLayer::~SKGameViewLayer() 
{

}

SKGameViewLayer *SKGameViewLayer::create(GameScene *pGameScene)
{
	SKGameViewLayer *pLayer = new SKGameViewLayer(pGameScene);
	if(pLayer && pLayer->init())
	{
		pLayer->autorelease();
		return pLayer;
	}
	else
	{
		CC_SAFE_DELETE(pLayer);
		return NULL;
	}
}


bool SKGameViewLayer::init()
{
	if ( !Layer::init() )
	{
		return false;
	}
	setLocalZOrder(3);

	Tools::addSpriteFrame("sk/skgameback.plist" );
	Tools::addSpriteFrame("Common/CardSprite2.plist");
	SoundUtil::sharedEngine()->playBackMusic("sk/skback",true);
    SoundUtil::sharedEngine()->setBackSoundVolume(g_GlobalUnits.m_fBackMusicValue);
    SoundUtil::sharedEngine()->setSoundVolume(g_GlobalUnits.m_fSoundValue);
	IGameView::onEnter();
	JniSink::share()->setIGameView(this);
	setGameStatus(GS_FREE);

	InitGame();
	AddButton();

	auto listener = EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(true);
	listener->onTouchBegan = CC_CALLBACK_2(SKGameViewLayer::onTouchBegan, this);
	listener->onTouchMoved = CC_CALLBACK_2(SKGameViewLayer::onTouchMoved, this);
	listener->onTouchEnded = CC_CALLBACK_2(SKGameViewLayer::onTouchEnded, this);
	_eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);

	AddPlayerInfo();

	setTouchEnabled(true);
	return true;
}


void SKGameViewLayer::InitGame()
{
	//背景图
	m_BackSpr = Sprite::create("sk/skgame_back.jpg");
	m_BackSpr->setPosition(_STANDARD_SCREEN_CENTER_);
	addChild(m_BackSpr);

    m_ScoreLabel = Sprite::createWithSpriteFrameName("lable_1.png");
    m_ScoreLabel->setPosition(Vec2(_STANDARD_SCREEN_CENTER_.x, 1020));
    addChild(m_ScoreLabel);
    
	m_ClockSpr = Sprite::createWithSpriteFrameName("sktimeback.png");
	m_ClockSpr->setPosition(Vec2(_STANDARD_SCREEN_CENTER_.x-10, 580));
	m_ClockSpr->setVisible(false);
	addChild(m_ClockSpr);

	//tga
	m_HeadTga[0]         = 100;
	m_HeadTga[1]         = 101;
	m_NickName_Tga[0]    = 103;
	m_NickName_Tga[1]    = 104;
	m_Glod_Tga[0]	     = 106;
	m_Glod_Tga[1]	     = 107;
	m_ReadyTga[0]		 = 109;
	m_ReadyTga[1]		 = 110;
	m_TopCardTga		 = 112;
	m_TimeSTga			 = 115;
	m_CallTga			 = 116;
	m_EndBackTga		 = 117;
	m_PassTga[0]		 = 118;
	m_PassTga[1]		 = 119;
	m_LowScoreTga		 = 121;
	m_AnimTga			 = 122;
    m_FirstCardIndex     = 123;
    for (int i = 0; i < SK_MAX_COUNT; i++)
    {
        m_otherLeaveCardTga[i] = 125+i;
    }
    
	//pos
	m_HeadPos[0]          = Vec2(100,800);
	m_HeadPos[1]          = Vec2(100,200);

	m_ReadyPos[0]		 = Vec2(400,800);
	m_ReadyPos[1]		 = Vec2(400,200);


	m_HandCardControl[0].SetLayer(this,300,0);
	m_HandCardControl[1].SetLayer(this,500,1);
	m_UserCardControl[0].SetLayer(this,700,0);
	m_UserCardControl[1].SetLayer(this,900,1);

	SetBankerScore();
}

void SKGameViewLayer::AddButton()
{
	m_BtnBackToLobby = CreateButton("Returnback" ,Vec2(90,1000),Btn_BackToLobby);
	addChild(m_BtnBackToLobby , 0 , Btn_BackToLobby);

	m_BtnSeting = CreateButton("seting" ,Vec2(1810,1000),Btn_Seting);
	addChild(m_BtnSeting , 0 , Btn_Seting);	

	m_BtnReadyPlay = CreateButton("sk_startbtn" ,Vec2(_STANDARD_SCREEN_CENTER_.x,300),Btn_Ready);
	addChild(m_BtnReadyPlay , 200 , Btn_Ready);
	m_BtnReadyPlay->setVisible(false);

	m_btOutPrompt = CreateButton("sk_pop" ,Vec2(_STANDARD_SCREEN_CENTER_.x-320,350),Btn_Pop);
	addChild(m_btOutPrompt , 100 , Btn_Pop);
	m_btOutPrompt->setVisible(false);

	m_btPassCard = CreateButton("sk_pass" ,Vec2(_STANDARD_SCREEN_CENTER_.x,350),Btn_Pass);
	addChild(m_btPassCard , 100 , Btn_Pass);
	m_btPassCard->setVisible(false);

	m_btOutCard = CreateButton("sk_sendcard" ,Vec2(_STANDARD_SCREEN_CENTER_.x+320,350),Btn_OutCard);
	addChild(m_btOutCard , 100 , Btn_OutCard);
	m_btOutCard->setVisible(false);
}

void SKGameViewLayer::onEnter ()
{	

}

void SKGameViewLayer::onExit()
{
	Tools::removeSpriteFrameCache("sk/skgameback.plist");
	Tools::removeSpriteFrameCache("Common/CardSprite2.plist");
	IGameView::onExit();
}


//理顺子
bool SKGameViewLayer::GetShunziBySelect(const BYTE bySelectCardData[], const UINT nSelectCount,
	const BYTE byAllCardData[], const UINT nAllCount,
	BYTE byOutCardData[], UINT& nOutCount)
{
	if ((nSelectCount > nAllCount) || (nSelectCount < 2)) return FALSE;

	UINT i = 0;
	skvector_carddata vtSelectCardData;
	vtSelectCardData.reserve(nSelectCount);
	for (i=0; i<nSelectCount; i++)
	{
		vtSelectCardData.push_back(bySelectCardData[i]);
	}
	m_GameLogic.SortCardData(CGameLogicBaseSK::eAscLogicValue, vtSelectCardData);

	UINT nMinCount = 1;
	INT nMinLogicValue = m_GameLogic.GetLogicValue(vtSelectCardData.at(0));
	UINT nMaxCount = 1;
	BYTE nMaxLogicValue = m_GameLogic.GetLogicValue(vtSelectCardData.at(nSelectCount-1));
	if ((nMinLogicValue == nMaxLogicValue) || (nMaxLogicValue > 14))
		return FALSE;

	for (i=1; i<vtSelectCardData.size(); i++)
	{
		if (m_GameLogic.GetLogicValue(vtSelectCardData.at(i)) != nMinLogicValue) break;
		nMinCount++;
	}
	for (i=(UINT)(vtSelectCardData.size()-2); i>0; i--)
	{
		if (m_GameLogic.GetLogicValue(vtSelectCardData.at(i)) != nMaxLogicValue) break;
		nMaxCount++;
	}

	if (nMinCount+nMaxCount != nSelectCount) return FALSE;
	if ((nMinCount != nMaxCount) && (min(nMinCount, nMaxCount) < 4))
		return FALSE;

	if ((nMinCount == 1) && (nMaxLogicValue-nMinLogicValue < 4))
		return FALSE;
	if ((nMinCount != 1) && (nMaxLogicValue-nMinLogicValue < 2))
		return FALSE;

	typedef list<BYTE> list_carddata;
	map<INT, list_carddata> mapCardData;
	map<INT, list_carddata>::iterator itmap;
	map<INT, list_carddata>::iterator itmap2;
	list_carddata lsCardData;
	list_carddata::iterator itlist;
	INT nLogicValue = 0;

	//合并相同逻辑值的牌
	for (i=0; i<nAllCount; i++)
	{
		lsCardData.clear();
		if (byAllCardData[i] == 0x00)
			continue;
		nLogicValue = m_GameLogic.GetLogicValue(byAllCardData[i]);
		if (nLogicValue > 14)
		{
			continue;
		}
		itmap = mapCardData.find(nLogicValue);
		if (itmap == mapCardData.end())
		{
			lsCardData.push_back(byAllCardData[i]);
			mapCardData[nLogicValue] = lsCardData;
			continue;
		}
		itmap->second.push_back(byAllCardData[i]);
	}

	//提取牌型
	for (nLogicValue=nMinLogicValue+1; nLogicValue<nMaxLogicValue; nLogicValue++)
	{
		itmap = mapCardData.find(nLogicValue);
		if ((itmap == mapCardData.end()) || (itmap->second.size() < nMinCount))
			return FALSE;
	}
	nOutCount = 0;
	//for (i=0; i<nMinCount; i++)
	//{
	//	byOutCardData[nOutCount++] = vtSelectCardData.at(i);
	//}
	for (nLogicValue=nMinLogicValue+1; nLogicValue<nMaxLogicValue; nLogicValue++)
	{
		itmap = mapCardData.find(nLogicValue);
		if (itmap != mapCardData.end())
		{
			i = 0;
			itlist = itmap->second.begin();
			while (itlist != itmap->second.end())
			{
				byOutCardData[nOutCount++] = *itlist;
				itlist++;
				i++;
				if ((nMinCount < 4) && (i == nMinCount))
					break;
			}
		}
	}
	//for (i=nMinCount; i<nSelectCount; i++)
	//{
	//	byOutCardData[nOutCount++] = vtSelectCardData.at(i);
	//}

	return TRUE;
}


//更新按钮状态
void SKGameViewLayer::UpdateControl()
{
	if (!m_bCanOutCard) return;

	if (GetMeChairID() == m_wCurrentUser)
	{
		m_btOutCard->setEnabled(false);
        m_btOutCard->setColor(Color3B(100,100,100));
		m_btPassCard->setEnabled(false);
        m_btPassCard->setColor(Color3B(100,100,100));
	}
	else
	{
		m_btOutCard->setVisible(false);
		m_btPassCard->setVisible(false);
		m_btOutPrompt->setVisible(false);
		m_btOutCard->setEnabled(false);
        m_btOutCard->setColor(Color3B(100,100,100));
		m_btPassCard->setEnabled(false);
        m_btPassCard->setColor(Color3B(100,100,100));
	}

	BYTE byFirstCardData[SK_MAX_COUNT] = {0};
	UINT nFirstCardCount = _countof(byFirstCardData);
	BYTE bySecondCardData[SK_MAX_COUNT] = {0};
	UINT nSecondCardCount = _countof(bySecondCardData);
	BYTE byAllCardData[SK_MAX_COUNT] = {0};
	UINT nAllCardCount = _countof(byAllCardData);
	BYTE byOutCardData[SK_MAX_COUNT] = {0};
	UINT nOutCardCount = _countof(byOutCardData);

	int cc = m_wMeViewID%SK_GAME_PLAYER;
	m_HandCardControl[m_wMeViewID%SK_GAME_PLAYER].GetShootCard(byFirstCardData, nFirstCardCount);
	m_HandCardControl[m_wMeViewID%SK_GAME_PLAYER].GetCardData(byAllCardData, nAllCardCount);

	if (GetShunziBySelect(byFirstCardData, nFirstCardCount,
		byAllCardData, nAllCardCount,
		byOutCardData, nOutCardCount))
	{
		m_HandCardControl[m_wMeViewID%SK_GAME_PLAYER].SetShootCard(byOutCardData, nOutCardCount);
		m_HandCardControl[m_wMeViewID%SK_GAME_PLAYER].GetShootCard(byFirstCardData, nFirstCardCount);
	}

	if (getGameStatus() != GS_FREE)
	{
		m_BtnReadyPlay->setVisible(false);
	}
	if (GetMeChairID() != m_wCurrentUser)
	{
		m_btOutCard->setEnabled(false);
        m_btOutCard->setColor(Color3B(100,100,100));
		return;
	}

	cc = (m_wMeViewID+1)%SK_GAME_PLAYER;
	m_UserCardControl[(m_wMeViewID+1)%SK_GAME_PLAYER].GetCardData(bySecondCardData, nSecondCardCount);

	if (nSecondCardCount>0)
	{
		m_btPassCard->setEnabled(true); m_btPassCard->setColor(Color3B(255,255,255));
	}
	else
	{
		m_btPassCard->setEnabled(false); m_btPassCard->setColor(Color3B(100,100,100));
	}

	if (nFirstCardCount == 0)
	{
		m_btOutCard->setEnabled(false); m_btOutCard->setColor(Color3B(100,100,100));
		return;
	}
	INT nResult = 0;
	if (nFirstCardCount > 0 && nSecondCardCount == 0)
		nResult = m_GameLogic.GetCardType(byFirstCardData, nFirstCardCount);
	else
		nResult = m_GameLogic.CompareCardData(byFirstCardData, nFirstCardCount, bySecondCardData, nSecondCardCount);
	
    if (nResult > 0)
	{
		m_btOutCard->setEnabled(true);
        m_btOutCard->setColor(Color3B(255,255,255));
	}
	else
	{
		m_btOutCard->setEnabled(false);
        m_btOutCard->setColor(Color3B(100,100,100));
	}
}

// Touch 触发
bool SKGameViewLayer::onTouchBegan(Touch *pTouch, Event *pEvent)
{
	Vec2 touchLocation = pTouch->getLocation(); // 返回GL坐标
	Vec2 localPos = convertToNodeSpace(touchLocation);

	m_HandCardControl[1].ccTouchBegan(localPos);

	return true;
}

void SKGameViewLayer::onTouchMoved(Touch *pTouch, Event *pEvent)
{
	Vec2 touchLocation = pTouch->getLocation(); // 返回GL坐标
	Vec2 localPos = convertToNodeSpace(touchLocation);
	m_HandCardControl[1].ccTouchMoved(localPos);
}

void SKGameViewLayer::onTouchEnded(Touch*pTouch, Event*pEvent)
{
	Vec2 touchLocation = pTouch->getLocation(); // 返回GL坐标
	Vec2 localPos = convertToNodeSpace(touchLocation);
	m_HandCardControl[1].ccTouchEnded(localPos);
	UpdateControl();
}

// Alert Message 确认消息处理
void SKGameViewLayer::DialogConfirm(Ref *pSender)
{
	this->RemoveAlertMessageLayer();
	this->SetAllTouchEnabled(true);
}

// Alert Message 取消消息处理
void SKGameViewLayer::DialogCancel(Ref *pSender)
{
	this->RemoveAlertMessageLayer();
	this->SetAllTouchEnabled(true);
}

void SKGameViewLayer::OnEventUserScore( tagUserData * pUserData, WORD wChairID, bool bLookonUser )
{
	IGameView::OnEventUserScore(pUserData, wChairID, bLookonUser);
    AddPlayerInfo();
}

void SKGameViewLayer::OnEventUserStatus(tagUserData * pUserData, WORD wChairID, bool bLookonUser)
{
	m_IsLook = bLookonUser;
	IGameView::OnEventUserStatus(pUserData, wChairID, bLookonUser);
	AddPlayerInfo();
}

void SKGameViewLayer::OnEventUserEnter( tagUserData * pUserData, WORD wChairID, bool bLookonUser )
{
	AddPlayerInfo();
}

void SKGameViewLayer::OnEventUserLeave( tagUserData * pUserData, WORD wChairID, bool bLookonUser )
{
	AddPlayerInfo();
}


void SKGameViewLayer::AddPlayerInfo()
{
	for (int i = 0; i < SK_GAME_PLAYER; i++)
	{
		if (ClientSocketSink::sharedSocketSink()->GetMeUserData() == NULL) return;
		const tagUserData* pUserData = GetUserData(i);
		int chair = g_GlobalUnits.SwitchViewChairID(i);
		if (pUserData != NULL)
		{
			//头像
			removeAllChildByTag(m_HeadTga[chair]);
			
            Sprite* mHeadback = Sprite::createWithSpriteFrameName("headback.png");
            mHeadback->setTag(m_HeadTga[chair]);
            mHeadback->setPosition(m_HeadPos[chair]);
            addChild(mHeadback,10000);

            string heads = g_GlobalUnits.getFace(pUserData->wGender, pUserData->lScore);
            Sprite* mHead = Sprite::createWithSpriteFrameName(heads);
			mHead->setScale(0.9f);
			mHead->setPosition(Vec2(89, 134));
			mHeadback->addChild(mHead,10000);

			//金币
			removeAllChildByTag(m_Glod_Tga[chair]);
			char strc[32]="";
			LONGLONG lMon = pUserData->lScore;
			memset(strc , 0 , sizeof(strc));
			Tools::AddComma(lMon , strc);
			Label* mGoldFont = Label::createWithSystemFont(strc,_GAME_FONT_NAME_1_,30);
			mGoldFont->setTag(m_Glod_Tga[chair]);
			mHeadback->addChild(mGoldFont);
			mGoldFont->setColor(_GAME_FONT_COLOR_4_);
			mGoldFont->setAnchorPoint(Vec2(0,0.5f));
            mGoldFont->setPosition(Vec2(5, 30));

			//昵称
			removeAllChildByTag(m_NickName_Tga[chair]);
			Label* mNickfont = Label::createWithSystemFont(gbk_utf8(pUserData->szNickName).c_str(),_GAME_FONT_NAME_1_,30);
			mNickfont->setTag(m_NickName_Tga[chair]);
			mHeadback->addChild(mNickfont);
			mNickfont->setAnchorPoint(Vec2(0,0.5f));
            mNickfont->setPosition(Vec2(5, 238));

			//准备
			removeAllChildByTag(m_ReadyTga[chair]);
			if (pUserData->cbUserStatus == US_READY)
			{
				Sprite* mReaderSpr = Sprite::createWithSpriteFrameName("zhunbei.png");
				mReaderSpr->setPosition(m_ReadyPos[chair]);
				mReaderSpr->setTag(m_ReadyTga[chair]);
				addChild(mReaderSpr);

				if (chair == 1)
				{
					removeAllChildByTag(m_EndBackTga);
					m_HandCardControl[0].ClearCardData();
					m_HandCardControl[1].ClearCardData();
					m_UserCardControl[0].ClearShowOutCard();
					m_UserCardControl[1].ClearShowOutCard();
				}
			}
		}
		else
		{
            if (chair == 1 && !g_GlobalUnits.m_bLeaveGameByServer)
            {
//                AlertMessageLayer::createConfirm(this , "\u60a8\u7684\u9152\u5427\u8c46\u4e0d\u8db3\uff0c\u4e0d\u80fd\u7ee7\u7eed\u6e38\u620f\uff01", menu_selector(IGameView::backLoginView));
                 backLoginView(nullptr);
                return;
            }
			removeAllChildByTag(m_ReadyTga[chair]);
			removeAllChildByTag(m_HeadTga[chair]);
			removeAllChildByTag(m_Glod_Tga[chair]);
			removeAllChildByTag(m_NickName_Tga[chair]);
		}
	}
}

Menu* SKGameViewLayer::CreateButton(std::string szBtName ,const Vec2 &p , int tag )
{
	return Tools::Button(StrToChar(szBtName+"_normal.png") , StrToChar(szBtName+"_click.png") , p, this , menu_selector(SKGameViewLayer::callbackBt) , tag);
}


void SKGameViewLayer::StartTime(int _time, int chair)
{
	m_StartTime = _time;
	schedule(schedule_selector(SKGameViewLayer::UpdateTime), 1);
	m_ClockSpr->setVisible(true);
	if(g_GlobalUnits.SwitchViewChairID(chair) == 1)
        m_ClockSpr->setPosition(Vec2(300,300));
	else
        m_ClockSpr->setPosition(Vec2(300,900));

	UpdateTime(m_StartTime);
}

void SKGameViewLayer::StopTime()
{
	unschedule(schedule_selector(SKGameViewLayer::UpdateTime));
	m_ClockSpr->removeAllChildren();
	m_ClockSpr->setVisible(false);
	m_ClockSpr->removeAllChildren();
}

void SKGameViewLayer::UpdateTime(float fp)
{
	if (m_StartTime <= 0)
	{        
        if (m_btOutPrompt->isVisible() && m_btOutCard->isVisible() && m_btPassCard->isVisible())
        {
            if (m_btPassCard->isEnabled())
            {
                OnBnPassCard();
            }
            else
            {
                m_HandCardControl[m_wMeViewID%SK_GAME_PLAYER].SetShootFirstCard();
                OutCard();
            }
          
            return;
        }
        
		if (getGameStatus() == GS_FREE)
		{
			GameLayerMove::sharedGameLayerMoveSink()->CloseSeting();
			RemoveAlertMessageLayer();
			SoundUtil::sharedEngine()->stopAllEffects();
			SoundUtil::sharedEngine()->stopBackMusic();
			ClientSocketSink::sharedSocketSink()->LeftGameReq();
			GameLayerMove::sharedGameLayerMoveSink()->CloseGetScore();
			ClientSocketSink::sharedSocketSink()->setFrameGameView(NULL);
			GameLayerMove::sharedGameLayerMoveSink()->GoGameToTable();
		}
		return;
	}
	m_ClockSpr->removeAllChildren();
    std::string str = StringUtils::toString(m_StartTime);
    if (m_StartTime < 10)
        str = "0" + str;
    LabelAtlas *time = LabelAtlas::create(str, "Common/time_1.png", 23, 35, '+');
    time->setPosition(Vec2(20, 26));
    m_ClockSpr->addChild(time);
	time->setTag(m_TimeSTga);
	m_StartTime--;
}


//游戏开始
bool SKGameViewLayer::OnSubGameStart(const void * pData, WORD wDataSize)
{
	//效验参数
	ASSERT(wDataSize==sizeof(CMD_S_SK_GameStart));
	if (wDataSize!=sizeof(CMD_S_SK_GameStart)) return false;

	//变量定义
	CMD_S_SK_GameStart * pGameStart=(CMD_S_SK_GameStart *)pData;
	setGameStatus(SK_GS_T_PLAYING);
    
	m_bCanOutCard = false;
	m_lGongxian = 0;
	m_lLastGongxian = 0;
    for (int i = 0; i < SK_MAX_COUNT; i++)
    {
        removeAllChildByTag(m_otherLeaveCardTga[i]);
    }
	for (int i=0; i<GAME_PLAYER; i++)
	{
		m_nTimes[i] = 0;
	}
	m_nOutCardCount = 0;
	memset(m_byOutCardData, 0, sizeof(m_byOutCardData));

	//清理数据
	removeAllChildByTag(m_EndBackTga);
	m_HandCardControl[0].ClearCardData();
	m_HandCardControl[1].ClearCardData();
	m_UserCardControl[0].ClearShowOutCard();
	m_UserCardControl[1].ClearShowOutCard();

	SetBankerScore();
	m_BoomCount = 0;
	SetUserPassState(0,false);
	SetUserPassState(1,false);

	m_lCellScore = pGameStart->lCellScore;

	//设置扑克
	BYTE CardData[SK_MAX_COUNT];

	for (WORD i=0;i<SK_GAME_PLAYER;i++)
        m_cbHandCardCount[i]=pGameStart->byCardCount;
	
	CopyMemory(CardData,pGameStart->byCardData,sizeof(BYTE)*pGameStart->byCardCount);
	
	CGameLogicBaseSK::vector_carddata tempcard;
	for (int i = 0; i < 27; i++)
	{
		tempcard.push_back(CardData[i]);
	}
	m_GameLogic.SortCardData(CGameLogicBaseSK::eDesLogicValue,tempcard);

	for (int i = 0; i < tempcard.size(); i++)
	{
		m_cbHandCardData[i] = tempcard[i];
	}

	//游戏变量
	m_wBankerUser= pGameStart->wBankerUser;
	m_wCurrentUser = pGameStart->wCurrentUser;
	m_FirstCard = pGameStart->byFirstCard;

	SetBankerScore();

	SoundUtil::sharedEngine()->playEffect("GAME_START");

	//开始发牌
	m_SendCardCount = 0;
    SendTopCard();
    
    string _name = GetCardStringName(m_FirstCard);
    Sprite* temp = Sprite::createWithSpriteFrameName(_name.c_str());
    temp->setPosition(Vec2(1460, 540));
    addChild(temp, 100, m_FirstCardIndex);
    
	return true;
}

void SKGameViewLayer::SendTopCard()
{
	MoveTo *leftMoveBy2 = MoveTo::create(0.05f, Vec2(100,_STANDARD_SCREEN_CENTER_.y));
    ActionInstant *func = CallFuncN::create(CC_CALLBACK_1(SKGameViewLayer::CardMoveCallbackTop, this));
	Sprite* temp = Sprite::createWithSpriteFrameName("backCard.png");
	temp->setScale(0.9f);
	temp->setTag(m_TopCardTga);
	addChild(temp);
	temp->setPosition(Vec2(_STANDARD_SCREEN_CENTER_.x,_STANDARD_SCREEN_CENTER_.y));
	temp->runAction(Sequence::create(leftMoveBy2,func,NULL));
}

void SKGameViewLayer::CardMoveCallbackTop(Node *pSender)
{
	removeChild(pSender);
	
	schedule(schedule_selector(SKGameViewLayer::SendCard), 0.05f);
}

void SKGameViewLayer::SendCard(float dt)
{
	if (m_SendCardCount >= 26)
	{
		unschedule(schedule_selector(SKGameViewLayer::SendCard));

		StartTime(m_cbTimeCallScore,m_wCurrentUser);
		return;
	}
	for (int i = 0; i < 2; i++)
	{
		if (i == 0)
		{
			MoveTo *leftMoveBy = MoveTo::create(0.05f, Vec2(10, _STANDARD_SCREEN_CENTER_.y+25));
            ActionInstant *func = CallFuncN::create(CC_CALLBACK_1(SKGameViewLayer::CardMoveCallback0, this));
			Sprite* temp = Sprite::createWithSpriteFrameName("backCard.png");
            temp->setScale(0.9f);
			addChild(temp);
			temp->setPosition(Vec2(_STANDARD_SCREEN_CENTER_.x,_STANDARD_SCREEN_CENTER_.y+25));
			temp->runAction(Sequence::create(leftMoveBy,func,NULL));
		}
		else if (i == 1)
		{
			MoveTo *leftMoveBy = MoveTo::create(0.05f, Vec2(_STANDARD_SCREEN_CENTER_.x, 140));
            ActionInstant *func = CallFuncN::create(CC_CALLBACK_1(SKGameViewLayer::CardMoveCallback1, this));
			Sprite* temp = Sprite::createWithSpriteFrameName("backCard.png");
            temp->setScale(0.9f);
			addChild(temp);
			temp->setPosition(Vec2(_STANDARD_SCREEN_CENTER_.x,_STANDARD_SCREEN_CENTER_.y+25));
			temp->runAction(Sequence::create(leftMoveBy,func,NULL));
		}
	}
	SoundUtil::sharedEngine()->playEffect("SEND_CARD");
	m_SendCardCount++;
}

void SKGameViewLayer::CardMoveCallback0(Node *pSender)
{
	removeChild(pSender);
	m_HandCardControl[0].SetCardData(0,m_SendCardCount+1,false);
}
void SKGameViewLayer::CardMoveCallback1(Node *pSender) //两个参数
{
	removeChild(pSender);
	m_HandCardControl[1].SetCardData(m_cbHandCardData,m_SendCardCount+1);
}

void SKGameViewLayer::SetUserCallScore(int chair, int cbUserCallScore)
{
	Sprite* temp;
	if(cbUserCallScore == 1) temp = Sprite::createWithSpriteFrameName("callone.png");
	else if(cbUserCallScore == 2) temp = Sprite::createWithSpriteFrameName("calltwo.png");
	else if(cbUserCallScore == 3) temp = Sprite::createWithSpriteFrameName("callthree.png");
	else temp = Sprite::createWithSpriteFrameName("callno.png");
	if(chair == 1) temp->setPosition(Vec2(_STANDARD_SCREEN_CENTER_.x,250));
	else temp->setPosition(m_ReadyPos[chair]);
	temp->setTag(m_CallTga);
	addChild(temp);
}

//用户出牌
bool SKGameViewLayer::OnSubOutCard(const void * pData, WORD wDataSize)
{
	CMD_S_SK_OutCard *pOutCard = (CMD_S_SK_OutCard *)pData;
	WORD wHeadSize=sizeof(CMD_S_SK_OutCard)-sizeof(pOutCard->byCardData);

	setGameStatus(GS_PLAYING);
    m_GameState = enGameOpenCard;
	
    //效验数据
	ASSERT(wDataSize>=wHeadSize);
	if (wDataSize < wHeadSize) return false;
	ASSERT(wDataSize==(wHeadSize+pOutCard->nCardCount*sizeof(pOutCard->byCardData[0])));
	if (wDataSize!=(wHeadSize+pOutCard->nCardCount*sizeof(pOutCard->byCardData[0]))) return false;

	WORD wViewID = g_GlobalUnits.SwitchViewChairID(pOutCard->wOutCardUser);

    m_HandCardControl[wViewID].RemoveCardData(pOutCard->byCardData, pOutCard->nCardCount);
    m_UserCardControl[wViewID].ShowOutCard(pOutCard->byCardData, pOutCard->nCardCount);
	
	if (!pOutCard->bIsGameEnd)
	{
		wViewID = g_GlobalUnits.SwitchViewChairID(pOutCard->wCurrentUser);
		m_UserCardControl[wViewID].ClearShowOutCard();
	}

	int nCardType = m_GameLogic.GetCardType(pOutCard->byCardData, pOutCard->nCardCount);
	if (pOutCard->wOutCardUser != GetMeChairID())
	{
		m_lLastGongxian = -pOutCard->lGongxian - m_lGongxian;
		m_lGongxian = -pOutCard->lGongxian;
	}
	else
	{
		m_lLastGongxian = pOutCard->lGongxian - m_lGongxian;
		m_lGongxian = pOutCard->lGongxian;
	}
	for (int i=0; i<GAME_PLAYER; i++)
	{
		m_nTimes[i] = pOutCard->nTimes[i];
	}

	m_wCurrentUser = pOutCard->wCurrentUser;

	SetBankerScore();

	//播放声音
	if (nCardType >= (INT)(CGameLogicSK::SK_CT_4))
	{
		SoundUtil::sharedEngine()->playEffect("sk/maxbom");
	}
	else
	{
		SoundUtil::sharedEngine()->playEffect("sk/outcard");
	}
	

	//播放动画
	OnAnimate(nCardType);

	if (!pOutCard->bIsGameEnd)
	{
		if (m_bOvertime)
		{
			StartTime(5, pOutCard->wCurrentUser);
		}
		else
		{
			StartTime(30,pOutCard->wCurrentUser);
		}

		if (pOutCard->wCurrentUser == GetMeChairID())
		{
			OnOutCard();
		}
	}
	return true;
}

void SKGameViewLayer::OnAnimate(int nCardType)
{
	if (nCardType < (INT)(CGameLogicSK::SK_CT_4))
        return;
	
    switch ((CGameLogicSK::SKEnumCardType)nCardType)
	{
	case CGameLogicSK::SK_CT_4:  LoadAnim("sk4"); break;
	case CGameLogicSK::SK_CT_5:  LoadAnim("sk5"); break;
	case CGameLogicSK::SK_CT_3K: LoadAnim("sk6"); break;
	case CGameLogicSK::SK_CT_6:  LoadAnim("sk6"); break;
	case CGameLogicSK::SK_CT_7L: LoadAnim("sk7"); break;
	case CGameLogicSK::SK_CT_7:  LoadAnim("sk7"); break;
	case CGameLogicSK::SK_CT_4K: LoadAnim("sk7"); break;
	case CGameLogicSK::SK_CT_8L: LoadAnim("sk8"); break;
	case CGameLogicSK::SK_CT_8:  LoadAnim("sk8"); break;
	case CGameLogicSK::SK_CT_9L: LoadAnim("sk9"); break;
	case CGameLogicSK::SK_CT_9:  LoadAnim("sk9"); break;
	case CGameLogicSK::SK_CT_10L: LoadAnim("sk10"); break;
	case CGameLogicSK::SK_CT_10:  LoadAnim("sk10"); break;
	case CGameLogicSK::SK_CT_11L: LoadAnim("sk11"); break;
	case CGameLogicSK::SK_CT_11:  LoadAnim("sk11"); break;
	case CGameLogicSK::SK_CT_12L: LoadAnim("sk12"); break;
	case CGameLogicSK::SK_CT_12:  LoadAnim("sk12"); break;
	default: break;
	}
}

void SKGameViewLayer::LoadAnim(const char* name)
{
	removeAllChildByTag(m_AnimTga);
	Vector<AnimationFrame*> arrayOfAnimationFrameNames;
	for(int i=1; i<7; ++i)
	{
		char buffer[50]= {0};
		sprintf(buffer, "%s_%d.png",name, i);

		SpriteFrame *pSpriteFrame = SpriteFrameCache::getInstance()->getSpriteFrameByName(buffer);
		ValueMap userInfo;
		AnimationFrame *pAnimationFrame = AnimationFrame::create(pSpriteFrame, 0.5f, userInfo);
		arrayOfAnimationFrameNames.pushBack(pAnimationFrame);
	}
	//生成动画数据对象
	Animation *pAnimation = Animation::create(arrayOfAnimationFrameNames, 0.3f);
	//生成动画动作对象
	Animate* animate = Animate::create(pAnimation);

	Sprite* pSprite2 = Sprite::create();
	this->addChild(pSprite2,4,m_AnimTga);
	pSprite2->setPosition(Vec2(_STANDARD_SCREEN_CENTER_.x, _STANDARD_SCREEN_CENTER_.y+50));
    ActionInstant *func = CallFuncN::create(CC_CALLBACK_1(SKGameViewLayer::AnimEnd, this));
	pSprite2->runAction(Sequence::create(animate,func,NULL));
}

void SKGameViewLayer::AnimEnd(Node *pSender)
{
	removeChild(pSender);
}

//玩家出牌
bool SKGameViewLayer::OnOutCard()
{
	m_btOutCard->setVisible(true);
	m_btPassCard->setVisible(true);
	m_btOutPrompt->setVisible(true);
	m_btOutCard->setEnabled(false);
    m_btOutCard->setColor(Color3B(100,100,100));
	m_btPassCard->setEnabled(false);
    m_btPassCard->setColor(Color3B(100,100,100));


	m_nOutCardCount = 0;
	memset(m_byOutCardData, 0, sizeof(m_byOutCardData));

	UpdateControl();

	return true;
}

//用户放弃
bool SKGameViewLayer::OnSubPassCard(const void * pData, WORD wDataSize)
{
	ASSERT(wDataSize == sizeof(CMD_S_SK_PassCard));
	if (wDataSize != sizeof(CMD_S_SK_PassCard)) return false;
	CMD_S_SK_PassCard *pPassCard = (CMD_S_SK_PassCard *)pData;

	StartTime(30, pPassCard->wCurrentUser);
	m_wCurrentUser = pPassCard->wCurrentUser;
    m_GameState = enGameOpenCard;
	
    if ((GetMeChairID() == pPassCard->wCurrentUser))
	{
		WORD wViewID = g_GlobalUnits.SwitchViewChairID(GetMeChairID());
		m_UserCardControl[(wViewID)%SK_GAME_PLAYER].RemoveAllCardData();
		OnOutCard();
	}

	return true;
}

void SKGameViewLayer::SetUserPassState(int wchair, bool _show)
{
	if (_show == false) //清理
	{
		if (wchair == 0) removeAllChildByTag(m_PassTga[0]);
		else if (wchair == 1) removeAllChildByTag(m_PassTga[1]);
		return;
	}
	if (wchair == 0)
	{
		Sprite* temp = Sprite::createWithSpriteFrameName("fjbuzt_f.png");
		temp->setTag(m_PassTga[0]);
		temp->setPosition(Vec2(250,470));
		addChild(temp);
	}
	else if (wchair == 1)
	{
		Sprite* temp = Sprite::createWithSpriteFrameName("fjbuzt_f.png");
		temp->setTag(m_PassTga[1]);
		temp->setPosition(Vec2(_STANDARD_SCREEN_CENTER_.x,240));
		addChild(temp);
	}
}

//发牌完成
bool SKGameViewLayer::OnSubSendCard(const void* pBuffer, WORD wDataSize)
{
	ASSERT(wDataSize == sizeof(CMD_S_SK_SendCard));
	if (wDataSize != sizeof(CMD_S_SK_SendCard)) return false;

	CMD_S_SK_SendCard *pSendCard = (CMD_S_SK_SendCard *)pBuffer;

    auto child = getChildByTag(m_FirstCardIndex);
    if (child)
        child->removeFromParent();
    
	removeAllChildByTag(m_TopCardTga);
	StartTime(30, pSendCard->wCurrentUser);
	m_wCurrentUser = pSendCard->wCurrentUser;
    m_bCanOutCard = true;
	if (GetMeChairID() == m_wCurrentUser)
	{
        m_GameState = enGameOpenCard;
		m_btOutCard->setVisible(true);
		m_btPassCard->setVisible(true);
		m_btOutPrompt->setVisible(true);
		m_btOutCard->setEnabled(false);
        m_btOutCard->setColor(Color3B(100,100,100));
		m_btPassCard->setEnabled(false);
        m_btPassCard->setColor(Color3B(100,100,100));
        UpdateControl();
	}
    
	return true;
}

//游戏结束
bool SKGameViewLayer::OnSubGameEnd(const void * pData, WORD wDataSize)
{
	ASSERT(wDataSize == sizeof(CMD_S_SK_GameEnd));
	if (wDataSize != sizeof(CMD_S_SK_GameEnd)) return false;
	CMD_S_SK_GameEnd *pGameEnd = (CMD_S_SK_GameEnd *)pData;

	//设置定时器
    m_GameState = enGameNormal;
	setGameStatus(GS_FREE);
	StartTime(30, GetMeChairID());

	//设置输赢玩家
	m_wWinner = pGameEnd->wWinner;
	if(pGameEnd->nCardCount < SK_COUNT_DK)//平扣
	{
		m_eCardMode=ePingKou;	
	}else if(pGameEnd->nCardCount < SK_COUNT_PK)//单扣
	{
		m_eCardMode=eDanKou;
	}else if(pGameEnd->nCardCount < SK_COUNT_SK)//双扣
	{
		m_eCardMode=eShuangKou;
	}

	//设置积分
	m_lGongxian = pGameEnd->lGongxian[GetMeChairID()];

	for (WORD i=0; i<GAME_PLAYER; i++)
	{
		m_nTimes[i] = pGameEnd->nTimes[i];
		sprintf(m_szName[i], "%s", gbk_utf8(pGameEnd->szName[i]).c_str());
		m_lScore[i] = pGameEnd->lGameScore[i];
	}
    
    if (pGameEnd->wWinner == GetMeChairID())
    {
        showOtherCard(pGameEnd->byCardData, pGameEnd->nCardCount);
    }
	OnGameEnd();

	return true;
}

void SKGameViewLayer::showOtherCard(BYTE *cbCardData, int count)
{
    m_UserCardControl[0].SetTopCardData(0, 0);
    m_HandCardControl[0].SetTopCardData(0, 0);
    CGameLogicBaseSK::vector_carddata tempcard;
    for (int j = 0; j < count; j++)
    {
        tempcard.push_back(cbCardData[j]);
    }
    m_GameLogic.SortCardData(CGameLogicBaseSK::eDesLogicValue,tempcard);
    
    for (int i = 0; i < tempcard.size(); i++)
    {
        cbCardData[i] = tempcard[i];
    }
    for (int i = 0; i < count; i++)
    {
        string _name = GetCardStringName(cbCardData[i]);
        Sprite* temp = Sprite::createWithSpriteFrameName(_name.c_str());
        temp->setScale(0.55f);
        temp->setPosition(50 + (i % 10) * 35, 560 - (i / 10)* 80);
        addChild(temp, 100, m_otherLeaveCardTga[i]);
    }
}

//游戏结束
bool SKGameViewLayer::OnGameEnd()
{
	//显示结算框
	m_wCurrentUser = INVALID_CHAIR;
	m_lCellScore = m_lCellScore;
	m_lGongXian[m_wMeChairID] = m_lGongxian;
	m_lGongXian[(m_wMeChairID+1)%SK_GAME_PLAYER] = -m_lGongxian;
    
    CallFunc *func = CallFunc::create(CC_CALLBACK_0(SKGameViewLayer::ShowResultDialog, this));
    DelayTime *delay = DelayTime::create(2.0f);
    Sequence * s = Sequence::create(delay, func, NULL);
    this->runAction(s);

	m_btOutCard->setVisible(false);
	m_btPassCard->setVisible(false);
	m_btOutPrompt->setVisible(false);
	return true;
}

//显示结算框
void SKGameViewLayer::ShowResultDialog()
{
    m_BtnReadyPlay->setVisible(true);
    
	Sprite* endback = Sprite::createWithSpriteFrameName("tips_back.png");
	endback->setPosition(_STANDARD_SCREEN_CENTER_);
	endback->setTag(m_EndBackTga);
	addChild(endback);
    
    Sprite *title =Sprite::createWithSpriteFrameName("text_title.png");
    title->setPosition(Vec2(468,550));
    endback->addChild(title);

    Sprite *label =Sprite::createWithSpriteFrameName("lable.png");
    label->setPosition(Vec2(468,420));
    endback->addChild(label);

    
	char strc[128];

	//翻倍
	if (m_wWinner != INVALID_CHAIR)
	{
		sprintf(strc, "翻倍: %d", m_nTimes[m_wWinner]);
		Label* mGoldFont = Label::createWithSystemFont(strc,_GAME_FONT_NAME_1_,46);
		mGoldFont->setPosition(Vec2(240,420));
		mGoldFont->setAnchorPoint(Vec2(0,0.5f));
		endback->addChild(mGoldFont);
	}

	//底分
	sprintf(strc, "底分: %lld", m_lCellScore);
	Label* cell = Label::createWithSystemFont(strc,_GAME_FONT_NAME_1_,46);
	cell->setPosition(Vec2(480,420));
	cell->setAnchorPoint(Vec2(0,0.5f));
	endback->addChild(cell);
	
	for (int i = 0; i < SK_GAME_PLAYER; i++)
	{
        Sprite *nameSpr =Sprite::createWithSpriteFrameName("text_name.png");
        nameSpr->setPosition(Vec2(200,330));
        endback->addChild(nameSpr);
        
		//用户名
		Label* mGoldFont = Label::createWithSystemFont(m_szName[i],_GAME_FONT_NAME_1_,46);
		mGoldFont->setPosition(Vec2(200,250-i*60));
		endback->addChild(mGoldFont);

        Sprite *conSpr =Sprite::createWithSpriteFrameName("text_attr.png");
        conSpr->setPosition(Vec2(468,330));
        endback->addChild(conSpr);
        
		//贡献
		sprintf(strc, "%lld", m_lGongXian[i]);
		mGoldFont = Label::createWithSystemFont(strc, _GAME_FONT_NAME_1_, 46);
		mGoldFont->setPosition(Vec2(468,250-i*60));
		endback->addChild(mGoldFont);

        Sprite *scoreSpr =Sprite::createWithSpriteFrameName("text_score.png");
        scoreSpr->setPosition(Vec2(758,330));
        endback->addChild(scoreSpr);
        
		//成绩
		sprintf(strc, "%lld", m_lScore[i]);
		mGoldFont = Label::createWithSystemFont(strc,_GAME_FONT_NAME_1_,46);
		mGoldFont->setPosition(Vec2(758,250-i*60));
		endback->addChild(mGoldFont);
        
//        std::string modeStr;
//        if(m_lScore[i]>0)
//        {
//            switch(m_eCardMode)
//            {
//                case ePingKou:
//                {
//                    modeStr = "平扣";
//                    break;
//                }
//                case eDanKou:
//                {
//                    modeStr = "单扣";
//                    break;
//                }
//                case eShuangKou:
//                {
//                    modeStr = "双扣";
//                    break;
//                }
//            }
//            Label * modeLabel = Label::createWithSystemFont(modeStr, _GAME_FONT_NAME_1_, 46);
//            modeLabel->setPosition(Vec2(628,250-i*60));
//            endback->addChild(modeLabel);
//        }
	}
}


//游戏消息
bool SKGameViewLayer::OnGameMessage(WORD wSubCmdID, const void * pBuffer, WORD wDataSize)
{
	CCLOG("RECEIVE MESSAGE CMD[%d]" , wSubCmdID);
	switch (wSubCmdID)
	{
	case SUB_S_SK_GAME_START:		//游戏开始
		{
			return OnSubGameStart(pBuffer,wDataSize);
		}
	case SUB_S_SK_SEND_CARD://发牌完成
		{
			return OnSubSendCard(pBuffer, wDataSize);
		}
	case SUB_S_SK_OUT_CARD://玩家出牌
		{
			return OnSubOutCard(pBuffer, wDataSize);
		}
	case SUB_S_SK_PASS_CARD://玩家过牌
		{
			return OnSubPassCard(pBuffer, wDataSize);
		}
	case SUB_S_SK_GAME_END://游戏结束
		{
			return OnSubGameEnd(pBuffer, wDataSize);
		}
	}
	return true;
}



//场景消息
bool SKGameViewLayer::OnGameSceneMessage(BYTE cbGameStatus, const void * pBuffer, WORD wDataSize)
{
    switch (cbGameStatus)
	{
	case GS_FREE:			//空闲状态
		{		
			//效验数据
			ASSERT(wDataSize == sizeof(CMD_S_SK_StatusFree));
			if (wDataSize != sizeof(CMD_S_SK_StatusFree)) return false;
//			CMD_S_SK_StatusFree *pStatusFree = (CMD_S_SK_StatusFree *)pBuffer;
			setGameStatus(GS_FREE);

			WORD wMeChairID = GetMeChairID();
			m_wMeChairID = wMeChairID;
			m_wMeViewID = g_GlobalUnits.SwitchViewChairID(wMeChairID);

			StartTime(30, m_wMeChairID);

			//显示开始按钮
			m_BtnReadyPlay->setVisible(true);

			return true;
		}
	case GS_PLAYING:
		{
			//效验数据
			ASSERT(wDataSize == sizeof(CMD_S_SK_StatusPlay));
			if (wDataSize != sizeof(CMD_S_SK_StatusPlay)) return false;

			CMD_S_SK_StatusPlay *pStatusPlay = (CMD_S_SK_StatusPlay *)pBuffer;
			WORD i = 0;

			setGameStatus(GS_PLAYING);

			WORD wMeChairID = GetMeChairID();
			m_wMeChairID = wMeChairID;
			m_wMeViewID = g_GlobalUnits.SwitchViewChairID(wMeChairID);
			m_bCanOutCard = TRUE;

			m_lCellScore = pStatusPlay->lCellScore;
			m_wCurrentUser = pStatusPlay->wCurrentUser;

			for (i=0; i<SK_GAME_PLAYER; i++)
			{
				m_nTimes[i] = pStatusPlay->nTimes[i];
			}
			m_lGongxian = pStatusPlay->lGongXian;
			WORD wViewID = g_GlobalUnits.SwitchViewChairID(pStatusPlay->wOutCardUser);
			if(wViewID>=0&&wViewID<2)
			{
				m_UserCardControl[wViewID].SetCardData(pStatusPlay->byOutCardData, pStatusPlay->nOutCardCount);
			}
			for (i=0; i<SK_GAME_PLAYER; i++)
			{
				if ((i != wMeChairID))
				{
					BYTE byCardData[SK_MAX_COUNT] = {0};
					m_HandCardControl[g_GlobalUnits.SwitchViewChairID(i)].SetCardData(byCardData, pStatusPlay->nCardCount[i]);
				}
				else
				{
					BYTE CardData[SK_MAX_COUNT];
					CopyMemory(CardData,pStatusPlay->byCardData,sizeof(BYTE)*pStatusPlay->nCardCount[i]);
					CGameLogicBaseSK::vector_carddata tempcard;
					for (int j = 0; j < pStatusPlay->nCardCount[i]; j++)
					{
						tempcard.push_back(CardData[j]);
					}
					m_GameLogic.SortCardData(CGameLogicBaseSK::eDesLogicValue,tempcard);

					for (int i = 0; i < tempcard.size(); i++)
					{
						m_cbHandCardData[i] = tempcard[i];
					}
					m_HandCardControl[g_GlobalUnits.SwitchViewChairID(i)].SetCardData(m_cbHandCardData, tempcard.size());
				}
			}

			if (pStatusPlay->wCurrentUser == GetMeChairID())
			{
				OnOutCard();
			}
			else
			{
				m_btOutCard->setVisible(false);
				m_btPassCard->setVisible(false);
			}
			StartTime(30,pStatusPlay->wCurrentUser);
			return true;
		}
	}
	return true;
}

string SKGameViewLayer::GetCardStringName(BYTE card)
{
	if (card == 0) return "backCard.png";
	char tt[32];
	sprintf(tt,"%0x",card);
	BYTE _value = m_GameLogic.GetCardValue(card);
	BYTE _color = m_GameLogic.GetCardColor(card);
	string temp;
	if (_color == 0) temp = "fangkuai_";
	if (_color == 1) temp = "meihua_";
	if (_color == 2) temp = "hongtao_";
	if (_color == 3) temp = "heitao_";
	char _valstr[32];
	ZeroMemory(_valstr,32);
	if (card == 0x41) //小王
	{
		sprintf(_valstr,"xiaowang.png");
	}
	else if (card == 0x42)
	{
		sprintf(_valstr,"dawang.png");
	}
	else if (_value < 10)
	{
		sprintf(_valstr,"0%d.png",_value);
	}
	else 
	{
		sprintf(_valstr,"%d.png",_value);
	}

	temp+=_valstr;
	return temp;
}

string SKGameViewLayer::AddCommaToNum(LONG Num)
{
	char _str[256];
	sprintf(_str,"%ld", Num);
	string _string = _str;
	auto step = _string.length()/3;
	for (int i = 1; i <= step; i++)
	{
		_string.insert(_string.length()-(i-1)-(i*3), ",");
	}
	return _string;
}

void SKGameViewLayer::callbackBt( Ref *pSender )
{
	Node *pNode = (Node *)pSender;
	switch (pNode->getTag())
	{
	case Btn_GetScoreBtn:
		{
			GetScoreForBank();
			break;
		}
	case Btn_BackToLobby: // 返回大厅按钮
		{
			SoundUtil::sharedEngine()->playEffect("buttonMusic");
			BackToLobby();
			break;
		}
	case Btn_Seting: //设置
		{
			SoundUtil::sharedEngine()->playEffect("buttonMusic");
			GameLayerMove::sharedGameLayerMoveSink()->OpenSeting();
			break;
		}
	case Btn_Ready:
		{
			m_HandCardControl[0].RemoveAllCardData();
			m_HandCardControl[1].RemoveAllCardData();
			ClientSocketSink::sharedSocketSink()->SendData(MDM_GF_FRAME, SUB_GF_USER_READY);
			m_BtnReadyPlay->setVisible(false);
			break;
		}
	case Btn_OutCard:
		{
			OutCard();
			break;
		}
	case Btn_Pass:
		{
			OnBnPassCard();
			break;
		}
	case Btn_Pop:
		{
			OnBnPrompt();
			break;
		}
	}
}

//提示按钮
void SKGameViewLayer::OnBnPrompt()
{
	if (m_wMeChairID != m_wCurrentUser) return;

	if (m_nOutCardCount == 0)
	{
		m_nOutCardCount = SK_MAX_COUNT;
		m_nOutCardCount = m_UserCardControl[(m_wMeViewID+1)%SK_GAME_PLAYER].GetCardData(m_byOutCardData, m_nOutCardCount);
	}

	if (m_nOutCardCount > 0)
	{
		BYTE bySrcCardData[SK_MAX_COUNT] = {0};
		UINT nSrcCardCount = SK_MAX_COUNT;
		nSrcCardCount = m_HandCardControl[m_wMeViewID%SK_GAME_PLAYER].GetCardData(bySrcCardData, nSrcCardCount);
		
        if (m_GameLogic.GetPromptCardData(m_byOutCardData, m_nOutCardCount, bySrcCardData, nSrcCardCount, m_byOutCardData, m_nOutCardCount))
		{
			m_HandCardControl[m_wMeViewID%SK_GAME_PLAYER].SetShootCard(m_byOutCardData, m_nOutCardCount);
			UpdateControl();
		}
		else
		{
			m_nOutCardCount = 0;
			memset(m_byOutCardData, 0, sizeof(m_byOutCardData));
		}
	}
}


//过牌按钮
void SKGameViewLayer::OnBnPassCard()
{
	if (m_wMeChairID != m_wCurrentUser)
		return;

	if (m_UserCardControl[(m_wMeViewID+1)%SK_GAME_PLAYER].GetCardCount() <= 0)
		return;

	m_UserCardControl[(m_wMeViewID+1)%GAME_PLAYER].RemoveAllCardData();

	m_btOutCard->setVisible(false);
	m_btPassCard->setVisible(false);
	m_btOutPrompt->setVisible(false);
	m_btOutCard->setEnabled(false); m_btOutCard->setColor(Color3B(100,100,100));

	SendData(SUB_C_SK_PASS_CARD);
	m_bOvertime = FALSE;
}

//出牌消息
void SKGameViewLayer::OutCard()
{
	if (GetMeChairID() != m_wCurrentUser) return;
	m_btOutCard->setVisible(false);
	m_btPassCard->setVisible(false);
	m_btOutPrompt->setVisible(false);
	m_btOutCard->setEnabled(false); m_btOutCard->setColor(Color3B(100,100,100));

	CMD_C_SK_OutCard C_OutCard;
	memset(&C_OutCard, 0, sizeof(C_OutCard));

	C_OutCard.nCardCount = _countof(C_OutCard.byCardData);
	WORD wMeViewID = g_GlobalUnits.SwitchViewChairID(GetMeChairID());
	m_HandCardControl[wMeViewID].GetShootCard(C_OutCard.byCardData, C_OutCard.nCardCount);

	WORD wDataSize = sizeof(C_OutCard)-sizeof(C_OutCard.byCardData)+C_OutCard.nCardCount*sizeof(C_OutCard.byCardData[0]);
	SendData(SUB_C_SK_OUT_CARD, &C_OutCard, wDataSize);

	m_bOvertime = false;
}

void SKGameViewLayer::backLoginView( Ref *pSender )
{
	IGameView::backLoginView(pSender);	
}

void SKGameViewLayer::OnQuit()
{
	m_pGameScene->removeChild(this);
}


//提示消息
void SKGameViewLayer::OutPrompt()
{
	//放弃出牌
	//设置变量
	m_wCurrentUser=INVALID_CHAIR;
	m_cbSearchResultIndex = 0;

	//设置界面
	m_btOutCard->setVisible(false);
	m_btPassCard->setVisible(false);
	m_btOutPrompt->setVisible(false);
	m_btOutCard->setEnabled(false);
	m_btOutCard->setColor(Color3B(100,100,100));

	//设置扑克
	m_HandCardControl[MYSELF_VIEW_ID].SetShootCard(NULL,0);
}

//播放声音
void SKGameViewLayer::PlayOutCardSound(WORD wChairID, BYTE cbCardData[], BYTE cbCardCount, bool follow)
{
	//效验参数
	ASSERT((wChairID<SK_GAME_PLAYER)&&(cbCardCount>0));
	if ((wChairID>=SK_GAME_PLAYER)||(cbCardCount==0)) return;

	return;
}

//设置倍数
void SKGameViewLayer::SetBankerScore()
{
	removeAllChildByTag(m_LowScoreTga);
	char strc[32];

	//底分
	sprintf(strc,"%lld", m_lCellScore);
	Label* mGoldFont = Label::createWithSystemFont(strc,_GAME_FONT_NAME_1_,30);
    mGoldFont->setColor(_GAME_FONT_COLOR_4_);
	mGoldFont->setAnchorPoint(Vec2(0,0.5f));
	mGoldFont->setTag(m_LowScoreTga);
	mGoldFont->setPosition(Vec2(580,1020));
	addChild(mGoldFont);
	
	//输倍数
	sprintf(strc,"%d", m_nTimes[(m_wMeChairID+1)%SK_GAME_PLAYER]);
	mGoldFont = Label::createWithSystemFont(strc,_GAME_FONT_NAME_1_,30);
    mGoldFont->setColor(_GAME_FONT_COLOR_4_);
	mGoldFont->setAnchorPoint(Vec2(0,0.5f));
	mGoldFont->setTag(m_LowScoreTga);
	mGoldFont->setPosition(Vec2(780,1020));
	addChild(mGoldFont);

	//赢倍数
	sprintf(strc,"%d", m_nTimes[m_wMeChairID]);
	mGoldFont = Label::createWithSystemFont(strc,_GAME_FONT_NAME_1_,30);
    mGoldFont->setColor(_GAME_FONT_COLOR_4_);
	mGoldFont->setAnchorPoint(Vec2(0,0.5f));
	mGoldFont->setTag(m_LowScoreTga);
	mGoldFont->setPosition(Vec2(980,1020));
	addChild(mGoldFont);

	//贡献
	sprintf(strc,"%lld", m_lGongxian);
	mGoldFont = Label::createWithSystemFont(strc,_GAME_FONT_NAME_1_,30);
    mGoldFont->setColor(_GAME_FONT_COLOR_4_);
	mGoldFont->setAnchorPoint(Vec2(0,0.5f));
	mGoldFont->setTag(m_LowScoreTga);
	mGoldFont->setPosition(Vec2(1200,1020));
	addChild(mGoldFont);
}
