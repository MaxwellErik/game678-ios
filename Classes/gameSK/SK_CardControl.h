#ifndef _SKCARDCONTROL_H_
#define _SKCARDCONTROL_H_
#include <vector>

using namespace std;
#include "GameScene.h"
#include "SKGameLogic.h"

#define CARD_UP     170//牌弹起纵坐标
#define CARD_DOWN   130//牌没弹起纵坐标

//∆ÀøÀΩ·ππ
struct SK_tagCardItem
{
public:
	SK_tagCardItem()
	{
		cardspr = NULL;
		cbCardData = 0;
	}
	Sprite*						cardspr;
	BYTE						cbCardData;
};

class SK_CardControl
{
public:
	SK_CardControl();
	~SK_CardControl();
	void SetLayer(GameLayer* _layer, int tga, int chair);

public:
	GameLayer*		m_layer;
	CGameLogicSK	m_GameLogic;
	vector<SK_tagCardItem>	m_CardData;
	int				m_CardTga[27];
	int				m_LeftCardBackSprTga;
    int				m_LeftCardCountSprTga;
	int				m_RightCardBackSprTga;
	DWORD			m_CardCount;
	int				m_Chair;
	int				m_OutCardTga;
	bool			m_IsLand;		//µÿ÷˜
	bool			m_TouchDown;
	int				m_BeginCardIndex;
	int				m_EndCardIndex;

public:
	void SetLand(bool island){m_IsLand = island;}
	void SetCardData(BYTE bCardData[], DWORD dwCardCount,bool setdata = true);
	void SetTopCardData(BYTE bCardData[], DWORD dwCardCount, bool setdata = true);
	
	void SetMyCardData(BYTE bCardData[], DWORD dwCardCount,bool setdata = true);
	void ShowOutCard(BYTE bCardData[], DWORD dwCardCount);

	BYTE GetShootCard(BYTE cbCardData[], UINT& cbBufferCount);

	void ClearShowOutCard();
	WORD GetCardData(BYTE cbCardData[], UINT& wBufferCount);
	void ClearCardData();
	int GetCardCount();
	string GetCardStringName(BYTE card);

	void ccTouchBegan(Vec2 localPos);
	void ccTouchMoved(Vec2 localPos);
	void ccTouchEnded(Vec2 localPos);

	bool SetShootCard(BYTE cbCardData[], BYTE cbCardCount);
    bool SetShootFirstCard();
	bool RemoveShootItem();

	void RemoveCardData(BYTE byCardData[], int nCardCount);
	void RemoveAllCardData();
};
#endif
