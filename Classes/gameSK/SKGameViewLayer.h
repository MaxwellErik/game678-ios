﻿#ifndef _SKCCGAME_VIEW_LAYER_H_
#define _SKCCGAME_VIEW_LAYER_H_

#include "GameScene.h"
#include "FrameGameView.h"
#include "SKGameLogic.h"
#include "CMD_SK.h"
#include "SK_CardControl.h"

enum SKEnumCardMode
{
	ePingKou=0,
	eDanKou,
	eShuangKou
};

class SKGameViewLayer : public IGameView
{
public:
	static SKGameViewLayer *create(GameScene *pGameScene);
	virtual ~SKGameViewLayer();
	SKGameViewLayer(GameScene *pGameScene);
	static void reset();
	SKGameViewLayer(const SKGameViewLayer&);
	SKGameViewLayer& operator = (const SKGameViewLayer&);

	virtual bool init(); 
	virtual void onEnter();
	virtual void onExit();

	virtual bool onTouchBegan(Touch *pTouch, Event *pEvent);
	virtual void onTouchMoved(Touch *pTouch, Event *pEvent);
	virtual void onTouchEnded(Touch *pTouch, Event *pEvent);

	virtual bool onTextFieldAttachWithIME(TextFieldTTF*pSender){return false;}
	virtual bool onTextFieldDetachWithIME(TextFieldTTF*pSender){return false;}
	virtual bool onTextFieldInsertText(TextFieldTTF *pSender, const char *text, int nLen){return true;}
	virtual bool onTextFieldDeleteBackward(TextFieldTTF *pSender, const char *delText, int nLen){return true;}

	virtual void keyboardWillShow(IMEKeyboardNotificationInfo &info){}
	virtual void keyboardDidShow(IMEKeyboardNotificationInfo &info){}
	virtual void keyboardWillHide(IMEKeyboardNotificationInfo &info){}
	virtual void keyboardDidHide(IMEKeyboardNotificationInfo &info){}
	virtual void DrawUserScore(){}	//绘制玩家分数
	virtual void GameEnd(){};
	virtual void ShowAddScoreBtn(bool bShow=true){};
	virtual void UpdateDrawUserScore(){};		//更新显示的游戏币数量
	void callbackBt( Ref *pSender );
	virtual void backLoginView(Ref *pSender);
	string  AddCommaToNum(LONG Num);

	//初始化
	void InitGame();
	void AddPlayerInfo();
	void AddButton();
	Menu* CreateButton(std::string szBtName ,const Vec2 &p , int tag);

	// 按钮事件管理
	void DialogConfirm(Ref *pSender);
	void DialogCancel(Ref *pSender);
	string GetCardStringName(BYTE card);
	//网络接口
public:
	void OnEventUserLeave( tagUserData * pUserData, WORD wChairID, bool bLookonUser );
	//用户进入
	virtual void OnEventUserEnter(tagUserData * pUserData, WORD wChairID, bool bLookonUser);
	//用户积分
	virtual void OnEventUserScore(tagUserData * pUserData, WORD wChairID, bool bLookonUser);
	//用户状态
	virtual void OnEventUserStatus(tagUserData * pUserData, WORD wChairID, bool bLookonUser);
    //用户换桌
    virtual void OnEventUserChangeTable(tagUserData * pUserData, WORD wChairID, bool bLookonUser){};
	//游戏消息
	virtual bool OnGameMessage(WORD wSubCmdID, const void * pBuffer, WORD wDataSize);
	//场景消息
	virtual bool OnGameSceneMessage(BYTE cbGameStatus, const void * pBuffer, WORD wDataSize);

	virtual void OnReconnectAction(){};

	void OnQuit();

protected:	
	CGameLogicSK		m_GameLogic;

public:
	bool OnSubGameStart(const void * pData, WORD wDataSize);
	bool OnSubSendCard(const void* pBuffer, WORD wDataSize);
	bool OnSubCallScore(const void * pData, WORD wDataSize);
	bool OnSubBankerInfo(const void * pData, WORD wDataSize);
	bool OnSubOutCard(const void * pData, WORD wDataSize);
	bool OnSubPassCard(const void * pData, WORD wDataSize);
	bool OnSubGameEnd(const void * pData, WORD wDataSize);

public:
	//图片
	Sprite*			m_BackSpr;
	Sprite*         m_ClockSpr;
    Sprite*         m_ScoreLabel;
	Sprite*			m_BankListBackSpr;
	Sprite*			m_LzBack;

	//按钮
	Menu*				m_BtnBackToLobby;
	Menu*				m_BtnSeting;
	Menu*				m_BtnReadyPlay;
	Menu*				m_btOutCard;						//出牌按钮
	Menu*				m_btPassCard;						//PASS按钮
	Menu*				m_btOutPrompt;						//提示按钮
	Menu*				m_BtnGetScoreBtn;

	//索引
	int					m_HeadTga[2];			//自己的信息
	int					m_NickName_Tga[2];
	int					m_Glod_Tga[2];
	int					m_ReadyTga[2];
	int					m_TopCardTga;
	int					m_TimeSTga;
	int					m_CallTga;
	int					m_EndBackTga;
	int					m_PassTga[2];
	int					m_LowScoreTga;
	int					m_AnimTga;
    int                 m_otherLeaveCardTga[SK_MAX_COUNT];
    
	//位置
	Vec2				m_HeadPos[2];          //自己的信息
	Vec2				m_ReadyPos[2];


	//变量
	BYTE							m_FirstCard;
    int                             m_FirstCardIndex;
	int								m_StartTime;
	WORD							m_wBankerUser;
	WORD							m_wCurrentUser;
	WORD							m_wMostCardUser;					//最大玩家
	BYTE							m_cbBankerScore;					//庄家叫分
	BYTE							m_cbSearchResultIndex;				//搜索结果索引

	BYTE							m_cbTurnCardCount;					//出牌数目
	BYTE							m_cbTurnCardData[SK_MAX_COUNT];		//出牌列表

public:
	BYTE							m_cbHandCardData[SK_MAX_COUNT];		//手上扑克
	BYTE							m_cbHandCardCount[SK_GAME_PLAYER];		//扑克数目
	SK_CardControl					m_HandCardControl[SK_GAME_PLAYER];
	SK_CardControl					m_UserCardControl[SK_GAME_PLAYER];		//扑克视图
	int								m_SendCardCount;

	BYTE							m_cbTimeOutCard;					//出牌时间
	BYTE							m_cbTimeCallScore;					//叫分时间
	BYTE							m_cbTimeStartGame;					//开始时间
	BYTE							m_cbTimeHeadOutCard;				//首出时间
	int								m_BoomCount;

	LONGLONG						m_lGongxian;
	LONGLONG						m_lLastGongxian;
	int								m_nTimes[SK_GAME_PLAYER];
	bool							m_bOvertime;
	BYTE							m_byOutCardData[SK_MAX_COUNT];
	UINT							m_nOutCardCount;
	int								m_wMeChairID;
	int								m_wMeViewID;
	bool							m_bCanOutCard;

	WORD							m_wWinner;
	SKEnumCardMode					m_eCardMode;
	char							m_szName[SK_GAME_PLAYER][LEN_NICKNAME*2];
	LONGLONG						m_lScore[SK_GAME_PLAYER];
	LONGLONG						m_lGongXian[SK_GAME_PLAYER];
	bool							m_IsLook;
public:
	void UpdateTime(float fp);
	void StopTime();
	void StartTime(int _time, int chair);
	void SendCard(float dt);
	void SendTopCard();
	void CardMoveCallback0(Node *pSender);
	void CardMoveCallback1(Node *pSender);
	void CardMoveCallbackTop(Node *pSender);
	void GetMostCard();
	void SetUserCallScore(int chair, int cbUserCallScore);
	void OutCard();
	void OutPrompt();
	void PlayOutCardSound(WORD wChairID, BYTE cbCardData[], BYTE cbCardCount, bool follow = false);
	void SetUserPassState(int wchair, bool _show);
	
	//设置底分
	void SetBankerScore();

	bool OnOutCard();
	void UpdateControl();
	bool GetShunziBySelect(const BYTE bySelectCardData[], const UINT nSelectCount,
		const BYTE byAllCardData[], const UINT nAllCount,
		BYTE byOutCardData[], UINT& nOutCount);

	void OnBnPassCard();
	void OnBnPrompt();
	bool OnGameEnd();
    void showOtherCard(BYTE cbCardData[], int count);
	void ShowResultDialog();

	void OnAnimate(int nCardType);
	void LoadAnim(const char* name);
	void AnimEnd(Node *pSender);
};

#endif
