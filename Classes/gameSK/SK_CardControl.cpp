#include "SK_CardControl.h"
#include "SKGameLogic.h"

//构造函数
SK_CardControl::SK_CardControl()
{
	m_TouchDown = false;
	m_BeginCardIndex = 0;
	m_EndCardIndex = 100;
}

void SK_CardControl::SetLayer(GameLayer* _layer,int tga, int chair)
{
	m_layer = _layer;
	for (int i = 0; i < 27; i++)
	{
		m_CardTga[i] = tga+i;
	}
	m_LeftCardBackSprTga = tga+50;
	m_RightCardBackSprTga = tga+51;
	m_OutCardTga = tga+52;
    m_LeftCardCountSprTga = tga+53;
	m_Chair = chair;
	m_IsLand = false;
}

//析构函数
SK_CardControl::~SK_CardControl()
{
}

void SK_CardControl::ClearCardData()
{
	for (int i = 0; i < m_CardData.size(); i++)
	{
		m_layer->removeChild(m_CardData[i].cardspr);
	}
	m_CardData.clear();
}

int SK_CardControl::GetCardCount()
{
	int r = 0;
	for (int i = 0; i < m_CardData.size(); i++)
	{
		if(m_CardData[i].cbCardData != 0) r++;
	}
	return r;
}

void SK_CardControl::SetTopCardData(BYTE bCardData[], DWORD dwCardCount,bool setdata)
{
	if (setdata == true)
	{
		m_CardData.clear();
		for (int i = 0; i < dwCardCount; i++)
		{
			SK_tagCardItem temp;
			temp.cbCardData = bCardData[i];
			m_CardData.push_back(temp);
		}
	}
	m_CardCount = dwCardCount;

	m_layer->removeAllChildByTag(m_LeftCardBackSprTga);
    m_layer->removeAllChildByTag(m_LeftCardCountSprTga);
    if (dwCardCount == 0)
        return;
    
	Sprite* temp = Sprite::createWithSpriteFrameName("backCard.png");
	temp->setPosition(Vec2(100,_STANDARD_SCREEN_CENTER_.y+25));
    temp->setScale(0.5f);
	temp->setTag(m_LeftCardBackSprTga);		
	m_layer->addChild(temp);

    std::string str = StringUtils::toString(m_CardCount);
    if (m_CardCount < 10)
        str = "0" + str;
    LabelAtlas *count = LabelAtlas::create(str, "Common/time.png", 33, 50, '+');
    count->setPosition(Vec2(65,_STANDARD_SCREEN_CENTER_.y));
    count->setTag(m_LeftCardCountSprTga);
    m_layer->addChild(count);
}

void SK_CardControl::RemoveAllCardData()
{
	if (m_Chair == 0)
	{
		ClearCardData();
		ClearShowOutCard();
	}
	else
	{
		ClearCardData();
		ClearShowOutCard();
	}
}

void SK_CardControl::RemoveCardData(BYTE byCardData[], int nCardCount)
{
	if (m_Chair == 0)
	{
		m_CardCount -= nCardCount;
		SetTopCardData(0,m_CardCount,false);
	}
	else
	{
		for (int i = 0; i < 27; i++)
		{
			m_layer->removeAllChildByTag(m_CardTga[i]);
		}

		for (int i = 0; i < nCardCount; i++)
		{
			for (int j = 0; j < m_CardData.size(); j++)
			{
				if (byCardData[i] == m_CardData[j].cbCardData)
				{
					m_CardData.erase(m_CardData.begin()+j);
					break;
				}
			}
		}

		int xc = (m_CardData.size()/2.0f)*55-120;
		for (int i = 0; i < m_CardData.size(); i++)
		{
			string _name = GetCardStringName(m_CardData[i].cbCardData);
			Sprite* temp = Sprite::createWithSpriteFrameName(_name.c_str());
			temp->setPosition(Vec2(_STANDARD_SCREEN_CENTER_.x-xc+i*55,130));
			temp->setScale(0.92f);
			temp->setTag(m_CardTga[i]);		
			m_layer->addChild(temp);
			m_CardData[i].cardspr = temp;
		}

	}
}

void SK_CardControl::SetMyCardData(BYTE bCardData[], DWORD dwCardCount,bool setdata)
{
	m_CardCount = dwCardCount;
	if (setdata == false)
	{
		for (int i = 0; i < 27; i++)
		{
			m_layer->removeAllChildByTag(m_CardTga[i]);
		}
		m_CardData.clear();
		return;
	}

	for (int i = 0; i < 27; i++)
	{
		m_layer->removeAllChildByTag(m_CardTga[i]);
	}
	m_CardData.clear();

	for (int i = 0; i < dwCardCount; i++)
	{
		SK_tagCardItem temp;
		temp.cbCardData = bCardData[i];
		m_CardData.push_back(temp);
	}

	int xc = (m_CardData.size()/2.0f)*60 - 120;
	for (int i = 0; i < m_CardData.size(); i++)
	{
		string _name = GetCardStringName(m_CardData[i].cbCardData);
		Sprite* temp = Sprite::createWithSpriteFrameName(_name.c_str());
        temp->setScale(0.9f);
		temp->setPosition(Vec2(_STANDARD_SCREEN_CENTER_.x-xc+i*60,130));
		temp->setTag(m_CardTga[i]);		
		m_layer->addChild(temp);
		m_CardData[i].cardspr = temp;
	}
}

void SK_CardControl::ClearShowOutCard()
{
	m_CardData.clear();
	m_layer->removeAllChildByTag(m_OutCardTga);
}

void SK_CardControl::ShowOutCard(BYTE bCardData[], DWORD dwCardCount)
{
	m_layer->removeAllChildByTag(m_OutCardTga);
	if (m_Chair == 0)
	{
		float cc = 0.1f;
		int xc = (dwCardCount/2.0f)*55-40;
		m_CardData.clear();
		for (int i = 0; i < dwCardCount; i++)
		{
			string _name = GetCardStringName(bCardData[i]);
			Sprite* temp = Sprite::createWithSpriteFrameName(_name.c_str());
			temp->setPosition(Vec2(_STANDARD_SCREEN_CENTER_.x-xc+i*55,600));
			temp->setScale(0.0f);
			temp->setTag(m_OutCardTga);		
			m_layer->addChild(temp);

			MoveTo *move = MoveTo::create(cc, Vec2(_STANDARD_SCREEN_CENTER_.x-xc+i*55,600));
			ScaleTo *scale = ScaleTo::create(cc, 0.9f);
			FiniteTimeAction * spawn =Spawn::create(move, scale, NULL);
			temp->runAction(spawn);
			cc+=0.05f;

			SK_tagCardItem tempcard;
			tempcard.cbCardData = bCardData[i];
			m_CardData.push_back(tempcard);
		}
	}
	else
	{
		float cc = 0.1f;
		int xc = (dwCardCount/2.0f)*55-40;
		for (int i = 0; i < dwCardCount; i++)
		{
			string _name = GetCardStringName(bCardData[i]);
			Sprite* temp = Sprite::createWithSpriteFrameName(_name.c_str());
			temp->setPosition(Vec2(_STANDARD_SCREEN_CENTER_.x-xc+i*55,600));
			temp->setScale(0.0f);
			temp->setTag(m_OutCardTga);		
			m_layer->addChild(temp);

			MoveTo *move = MoveTo::create(cc, Vec2(_STANDARD_SCREEN_CENTER_.x-xc+i*55,600));
			ScaleTo *scale = ScaleTo::create(cc,0.9f);
			FiniteTimeAction * spawn =Spawn::create(move, scale, NULL);
			temp->runAction(spawn);
			cc+=0.05f;
		}
	}

}

void SK_CardControl::SetCardData(BYTE bCardData[], DWORD dwCardCount, bool setdata)
{
	if (m_Chair == 0) //左边
	{
		SetTopCardData(bCardData, dwCardCount, setdata);
	}
	else
	{
		SetMyCardData(bCardData, dwCardCount, setdata);
	}
}


//获取扑克
BYTE SK_CardControl::GetShootCard(BYTE cbCardData[], UINT& cbBufferCount)
{
	//变量定义
	BYTE cbShootCount=0;

	//拷贝扑克
	for (BYTE i=0;i<m_CardData.size();i++) 
	{
		//效验参数
		ASSERT(cbBufferCount>cbShootCount);
		if (cbBufferCount<=cbShootCount) break;

		//拷贝扑克
		if (m_CardData[i].cardspr != nullptr && m_CardData[i].cardspr->getPositionY() == CARD_UP)
            cbCardData[cbShootCount++]=m_CardData[i].cbCardData;
	}
	cbBufferCount = cbShootCount;

	return cbShootCount;
}

void SK_CardControl::ccTouchBegan(Vec2 localPos)
{
    if ( m_CardData.size() <= 0)
        return;
    
	for (ssize_t i = m_CardData.size()-1; i >= 0; i--)
	{
		m_CardData[i].cardspr->setColor(Color3B(255,255,255));
		Rect rc = m_CardData[i].cardspr->getBoundingBox();
		bool isTouched = rc.containsPoint(localPos);
		if (isTouched)
		{
			m_CardData[i].cardspr->setColor(Color3B(100,100,100));
			m_BeginCardIndex = (int)i;
			m_TouchDown = true;
			return;
		}
	}
}
void SK_CardControl::ccTouchMoved(Vec2 localPos)
{
	if (m_TouchDown) 
	{
		for (ssize_t i = m_CardData.size()-1; i >= 0; i--)
		{
			Rect rc = m_CardData[i].cardspr->getBoundingBox();
			bool isTouched = rc.containsPoint(localPos);
			if (isTouched)
			{
				m_EndCardIndex = (int)i;
				break;;
			}
		}
		for (ssize_t i = m_CardData.size()-1; i >= 0; i--)
		{
			if(i >= m_CardData.size())break;
			if(m_CardData[i].cardspr != NULL) m_CardData[i].cardspr->setColor(Color3B(255,255,255));
		}

		if (m_BeginCardIndex > m_EndCardIndex) //往左
		{
			for (int i = m_EndCardIndex; i <= m_BeginCardIndex; i++)
			{
				if(i >= m_CardData.size())break;
				if(m_CardData[i].cardspr != NULL) m_CardData[i].cardspr->setColor(Color3B(100,100,100));
			}
		}
		else //往右
		{
			for (int i = m_BeginCardIndex; i <= m_EndCardIndex; i++)
			{
				if(i >= m_CardData.size())break;
				if(m_CardData[i].cardspr != NULL) m_CardData[i].cardspr->setColor(Color3B(100,100,100));
			}
		}
	}
}
void SK_CardControl::ccTouchEnded(Vec2 localPos)
{
	m_TouchDown = false;

	if (m_EndCardIndex == 100)
	{
		for (int i = m_CardData.size()-1; i >= 0; i--)
		{
			Rect rc = m_CardData[i].cardspr->getBoundingBox();
			bool isTouched = rc.containsPoint(localPos);
			if (isTouched)
			{
				if (m_CardData[i].cardspr->getPositionY() == CARD_DOWN)
				{
					m_CardData[i].cardspr->setColor(Color3B(255,255,255));
					m_CardData[i].cardspr->setPositionY(CARD_UP);
				}
				else
				{
					m_CardData[i].cardspr->setColor(Color3B(255,255,255));
					m_CardData[i].cardspr->setPositionY(CARD_DOWN);
				}
				break;;
			}
		}
		return;
	}

	if (m_BeginCardIndex > m_EndCardIndex) //往左
	{
		for (int i = m_EndCardIndex; i <= m_BeginCardIndex; i++)
		{
			if(i >= m_CardData.size())break;
			if(m_CardData[i].cardspr != NULL)
			{
                if (m_CardData[i].cardspr->getPositionY() == CARD_DOWN)
                {
                    m_CardData[i].cardspr->setColor(Color3B(255,255,255));
                    m_CardData[i].cardspr->setPositionY(CARD_UP);
                }
                else
                {
                    m_CardData[i].cardspr->setColor(Color3B(255,255,255));
                    m_CardData[i].cardspr->setPositionY(CARD_DOWN);
                }
            }
        }
	}
	else //往右
	{
		for (int i = m_BeginCardIndex; i <= m_EndCardIndex; i++)
		{
			if(i >= m_CardData.size())break;
			if(m_CardData[i].cardspr != NULL)
			{
                if (m_CardData[i].cardspr->getPositionY() == CARD_DOWN)
                {
                    m_CardData[i].cardspr->setColor(Color3B(255,255,255));
                    m_CardData[i].cardspr->setPositionY(CARD_UP);
                }
                else
                {
                    m_CardData[i].cardspr->setColor(Color3B(255,255,255));
                    m_CardData[i].cardspr->setPositionY(CARD_DOWN);
                }
            }
        }
	}

	m_BeginCardIndex = 0;
	m_EndCardIndex = 100;
}

bool SK_CardControl::RemoveShootItem()
{
	vector<SK_tagCardItem>	TempCardData;
	for (int i = 0; i < m_CardData.size(); i++)
	{
		 if (m_CardData[i].cardspr->getPositionY() == CARD_DOWN)
		{
			SK_tagCardItem temp;
			temp.cbCardData = m_CardData[i].cbCardData;
			TempCardData.push_back(temp);
		}
		m_layer->removeChild(m_CardData[i].cardspr);
	}
	m_CardData.clear();

	int xc = (TempCardData.size()/2.0f)*50-80;
	for (int i = 0; i < TempCardData.size(); i++)
	{
		SK_tagCardItem temp;
		temp.cbCardData = TempCardData[i].cbCardData;
		string _name = GetCardStringName(TempCardData[i].cbCardData);
		Sprite* tempspr = Sprite::createWithSpriteFrameName(_name.c_str());
		tempspr->setPosition(Vec2(_STANDARD_SCREEN_CENTER_.x-xc+i*60,CARD_DOWN));
		tempspr->setTag(m_CardTga[i]);		
		m_layer->addChild(tempspr);
		temp.cardspr = tempspr;
		m_CardData.push_back(temp);
	}

	m_CardCount = m_CardData.size();

	return true;
}

bool SK_CardControl::SetShootFirstCard()
{
    for (int i = 0; i < m_CardData.size(); i++)
    {
        m_CardData[i].cardspr->setPositionY(CARD_DOWN);
    }
    m_CardData[ m_CardData.size()-1].cardspr->setPositionY(CARD_UP);
    return true;
}

//设置扑克
bool SK_CardControl::SetShootCard(BYTE cbCardData[], BYTE cbCardCount)
{
	//设置牌不弹起
	if (cbCardCount == 0)
	{
		for (int i = 0; i < m_CardData.size(); i++)
		{
			m_CardData[i].cardspr->setPositionY(CARD_DOWN);
		}
	}
	else //弹起扑克
	{
		for (int i = 0; i < m_CardData.size(); i++)
		{
			m_CardData[i].cardspr->setPositionY(CARD_DOWN);
		}

		for (BYTE i=0;i<cbCardCount;i++)
		{
			for (BYTE j=0;j<m_CardData.size();j++)
			{
				if ((m_CardData[j].cardspr->getPositionY() == CARD_DOWN) && (m_CardData[j].cbCardData == cbCardData[i]))
				{
					m_CardData[j].cardspr->setPositionY(CARD_UP);
					break;
				}
			}
		}
	}

	return true;
}

WORD SK_CardControl::GetCardData(BYTE cbCardData[], UINT& wBufferCount)
{
	if (wBufferCount == 0)	return m_CardData.size();

	WORD wCardCount = wBufferCount < m_CardData.size()? wBufferCount:m_CardData.size();

	//拷贝扑克
	for( WORD i = 0; i < wCardCount; i++ )
        cbCardData[i] = m_CardData[i].cbCardData;

	wBufferCount = (UINT)(m_CardData.size());

	return wCardCount;
}

string SK_CardControl::GetCardStringName(BYTE card)
{
	if (card == 0)
	{
		return "backCard.png";
	}
	char tt[32];
	sprintf(tt,"%0x",card);
	BYTE _value = m_GameLogic.GetCardValue(card);
	BYTE _color = m_GameLogic.GetCardColor(card);
	string temp;
	if (_color == 0) temp = "fangkuai_";
	if (_color == 1) temp = "meihua_";
	if (_color == 2) temp = "hongtao_";
	if (_color == 3) temp = "heitao_";
	char _valstr[32];
	ZeroMemory(_valstr,32);
	if (card == 0x41) //小王
	{
		sprintf(_valstr,"xiaowang.png");
	}
	else if (card == 0x42)
	{
		sprintf(_valstr,"dawang.png");
	}
	else if (_value < 10)
	{
		sprintf(_valstr,"0%d.png",_value);
	}
	else 
	{
		sprintf(_valstr,"%d.png",_value);
	}

	temp+=_valstr;
	return temp;
}
