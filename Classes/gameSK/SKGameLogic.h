﻿#ifndef _SKCCGAME_LOGIC_LAYER_H_
#define _SKCCGAME_LOGIC_LAYER_H_
#include "SKGameLogicBase.h"

/////////////////////////////
#include "GameLayer.h"
// 千变双扣游戏逻辑

class CGameLogicSK : public CGameLogicBaseSK
{
public:
	//扑克类型
	enum SKEnumCardType
	{
		SK_CT_ERROR = 0,								//错误类型
		SK_CT_1 = 1,									//单牌类型
		SK_CT_2 = 2,									//对牌类型
		SK_CT_3 = 3,									//三条类型
		SK_CT_1X = 4,									//单连类型
		SK_CT_2X = 5,									//对连类型
		SK_CT_3X = 6,									//三连类型
		SK_CT_4 = 7,									//炸弹类型(4张)
		SK_CT_5 = 8,									//炸弹类型(5张)
		SK_CT_3K = 9,									//三王炸弹类型(6相最小)
		SK_CT_6 = 10,									//6星硬炸
		SK_CT_7L = 11,									//7星连炸
		SK_CT_7 = 12,									//7星硬炸
		SK_CT_4K = 13,									//天王炸弹
		SK_CT_8L = 15,									//8星连炸
		SK_CT_8 = 16,									//8星硬炸
		SK_CT_9L = 17,									//9星连炸
		SK_CT_9 = 18,									//9星硬炸
		SK_CT_10L = 19,								//10星连炸
		SK_CT_10 = 20,									//10星硬炸
		SK_CT_11L = 21,								//11星连炸
		SK_CT_11 = 22,									//11星硬炸
		SK_CT_12L = 23,								//12星连炸
		SK_CT_12 = 24									//12星硬炸
	};
	enum enumCardCount
	{
		eMaxCardCount = 27,//牌数量
		eMaxMagicCardCount = 4,//财神数量
	};
	struct tagLogicValue
	{
		int nLogicValue;
		int bIsEnd2;
	};
	struct tagCardInfo
	{
		int nLogicValue;
		UINT nCardCount;
	};
	typedef map<int, tagLogicValue> map_cardtype;

public:
	CGameLogicSK(void);
	virtual ~CGameLogicSK(void);

	//继承特有函数 多态性
public:
	//复位牌数据
	virtual void ResetCardData();
	//获取最大牌数量
	virtual UINT GetMaxCardCount(){ return (UINT)eMaxCardCount; }
	//获取最大财神牌数量
	virtual UINT GetMaxMagicCardCount(){ return (UINT)eMaxMagicCardCount; }
	//财神变换
	virtual int MagicCardData(const BYTE bySrcCardData[], const UINT nCardCount, BYTE byDestCardData[]);
	//获取牌型
	virtual int GetCardType(const BYTE byCardData[], const UINT nCardCount, void* pResult=NULL);
	//比较大小
	virtual int CompareCardData(const BYTE byFirstCardData[], const UINT nFirstCardCount,
		const BYTE bySecondCardData[], const UINT nSecondCardCount);//return (first>second);
	//获取类型描述
	virtual int GetTypeDescribe(int nCardType, string& strTypeDescribe, void* pResult=NULL);

public:
	//获取普通牌型
	int GetNormalCardType(const BYTE byCardData[], const UINT nCardCount, void* pResult=NULL);
	//获取变换牌型
	int GetMagicCardType(const BYTE byCardData[], const UINT nCardCount, void* pResult=NULL);
	//提示出牌
	int GetPromptCardData(BYTE byPromptCardData[], UINT& nPromptCardCount, const BYTE bySrcCardData[], const UINT nSrcCardCount,
		const BYTE byOutCardData[], const UINT nOutCardCount);
	//提示出牌
	int GetAndroidOutCardData(BYTE byPromptCardData[], UINT& nPromptCardCount, const BYTE bySrcCardData[], const UINT nSrcCardCount,
		const BYTE byOutCardData[], const UINT nOutCardCount);

protected:
	map_cardtype m_mapCardType;//牌型信息
};

#endif