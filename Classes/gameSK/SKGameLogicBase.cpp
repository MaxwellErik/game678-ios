#include "SKGameLogicBase.h"

CGameLogicBaseSK* CGameLogicBaseSK::m_pGameLogic = NULL;

CGameLogicBaseSK::CGameLogicBaseSK(void)
{
//	srand(GetTickCount());
	m_byCardData.clear();
}

CGameLogicBaseSK::~CGameLogicBaseSK(void)
{
}

//洗牌
void CGameLogicBaseSK::RandomCardData()
{
	random_shuffle(m_byCardData.begin(), m_byCardData.end());
}

//获取牌值
BYTE CGameLogicBaseSK::GetCardValue(const BYTE& byCardData)
{
	return (byCardData & 0x0F);
}

//获取牌花色
BYTE CGameLogicBaseSK::GetCardColor(const BYTE& byCardData)
{
	return ( (byCardData & 0xF0) >> 4 );
}

//获取牌花色
int CGameLogicBaseSK::GetCardLogic(const BYTE& byCardData)
{
	BYTE byCardValue = GetCardValue(byCardData);

	//大小王
	if ((byCardData == 0x41) || (byCardData == 0x42))
		return (byCardValue+15);

	return byCardValue;
}

//获取逻辑值
int CGameLogicBaseSK::GetLogicValue(const BYTE& byCardData)
{
	BYTE byCardValue = GetCardValue(byCardData);

	//大小王
	if ((byCardData == 0x41) || (byCardData == 0x42))
		return (byCardValue+15);
	//A、2
	if((byCardValue == 0x01) || (byCardValue == 0x02))
		return (byCardValue+13);

	return byCardValue;
}

//逻辑值相同牌数量
UINT CGameLogicBaseSK::GetSameLogicCardCount(BYTE byCardData)
{
	UINT nCount = 0;

	if (byCardData == 0x41)//小王
	{
		nCount = 254;
	}
	else if (byCardData == 0x42)//大王
	{
		nCount = 255;
	}
	else//其他牌
	{
		for (int i=0; i<(int)(m_vtSortData.size()); i++)
		{
			if (GetLogicValue(m_vtSortData.at(i)) == GetLogicValue(byCardData))
			{
				nCount++;
			}
		}
		if (nCount < 4) nCount = 0;
	}

	return nCount;
}
//排序函数
bool CGameLogicBaseSK::fncmp(const BYTE& lhs, const BYTE& rhs)
{
	if (m_pGameLogic == NULL)
		return false;

	switch (m_pGameLogic->m_eSortMode)
	{
	case eAscending:		{return (lhs<rhs); break;}
	case eAscendingColor:	{return (m_pGameLogic->GetCardColor(lhs)<m_pGameLogic->GetCardColor(rhs)); break;}
	case eAscendingValue:	{return (m_pGameLogic->GetCardValue(lhs)<m_pGameLogic->GetCardValue(rhs)); break;}
	case eAscendingLogic:	{return (m_pGameLogic->GetCardLogic(lhs)<m_pGameLogic->GetCardLogic(rhs)); break;}
	case eAscLogicValue:	{return (m_pGameLogic->GetLogicValue(lhs)<m_pGameLogic->GetLogicValue(rhs)); break;}
	case eDescending:		{return (lhs>rhs); break;}
	case eDescendingColor:	{return (m_pGameLogic->GetCardColor(lhs)>m_pGameLogic->GetCardColor(rhs)); break;}
	case eDescendingValue:	{return (m_pGameLogic->GetCardValue(lhs)>m_pGameLogic->GetCardValue(rhs)); break;}
	case eDescendingLogic:	{return (m_pGameLogic->GetCardLogic(lhs)>m_pGameLogic->GetCardLogic(rhs)); break;}
	case eDesLogicValue:	{return (m_pGameLogic->GetLogicValue(lhs)>m_pGameLogic->GetLogicValue(rhs)); break;}
	case eDescendingLogicCount:	{return (m_pGameLogic->GetSameLogicCardCount(lhs)>m_pGameLogic->GetSameLogicCardCount(rhs)); break;}
	default: break;	
	}

	return false;
}

//排序牌
int CGameLogicBaseSK::SortCardData(enumSortMode eSortMode, const int nStart, const int nEnd)
{
	return SortCardData(eSortMode, m_byCardData, nStart, nEnd);
}

//排序牌
int CGameLogicBaseSK::SortCardData(enumSortMode eSortMode, vector_carddata& byCardData, const int nStart, const int nEnd)
{
	m_pGameLogic = this;
	m_vtSortData.clear();
	if ((nStart == -1) && (nEnd == -1))
	{
		m_eSortMode = eSortMode;
		if (m_eSortMode == eDescendingLogicCount)
		{
			m_vtSortData.assign(byCardData.begin(), byCardData.end());
		}
		sort(byCardData.begin(), byCardData.end(), fncmp);
		m_vtSortData.clear();
		return TRUE;
	}
	else
	{
		if (nStart >= nEnd) return FALSE;
		if (nStart < 0) return FALSE;
		if (nEnd > (int)(byCardData.size())) return FALSE;
	}

	m_eSortMode = eSortMode;
	if (m_eSortMode == eDescendingLogicCount)
	{
		m_vtSortData.assign(byCardData.begin(), byCardData.end());
	}
	sort(byCardData.begin()+nStart, byCardData.begin()+nEnd, fncmp);
	m_vtSortData.clear();

	return TRUE;
}

//获取牌数据
int CGameLogicBaseSK::GetCardData(vector_carddata& byCardData, const int nStart, const int nEnd)
{
	return GetCardData(byCardData, m_byCardData, nStart, nEnd);
}

//获取牌数据
int CGameLogicBaseSK::GetCardData(vector_carddata& vtCardDataDest, const vector_carddata& vtCardDataSrc, const int nStart, const int nEnd)
{
	vtCardDataDest.clear();
	if ((nStart == -1) && (nEnd == -1))
	{
		vtCardDataDest.assign(vtCardDataSrc.begin(), vtCardDataSrc.end());
		return TRUE;
	}
	else
	{
		if (nStart >= nEnd) return FALSE;
		if (nStart < 0) return FALSE;
		if (nEnd > (int)(vtCardDataSrc.size())) return FALSE;
	}

	vtCardDataDest.assign(vtCardDataSrc.begin()+nStart, vtCardDataSrc.begin()+nEnd);

	return TRUE;
}

//设置牌数据
void CGameLogicBaseSK::SetCardData(vector_carddata& byCardData, const BYTE byAddCardData[], const UINT nAddCardCount)
{
	byCardData.clear();
	AddCardData(byCardData, byAddCardData, nAddCardCount);
}

//设置牌数据
void CGameLogicBaseSK::SetCardData(const BYTE byAddCardData[], const UINT nAddCardCount)
{
	SetCardData(m_byCardData, byAddCardData, nAddCardCount);
}

//添加牌数据
void CGameLogicBaseSK::AddCardData(const BYTE byAddCardData[], const UINT nAddCardCount)
{
	AddCardData(m_byCardData, byAddCardData, nAddCardCount);
}

//添加牌数据
void CGameLogicBaseSK::AddCardData(vector_carddata& byCardData, const BYTE byAddCardData[], const UINT nAddCardCount)
{
	if (nAddCardCount <= 0)
		return;

	byCardData.insert(byCardData.end(), byAddCardData, byAddCardData+nAddCardCount);
}

//删除牌数据
int CGameLogicBaseSK::RemoveCardData(const BYTE byRemoveCardData[], const UINT nRemoveCardCount)
{
	return RemoveCardData(m_byCardData, byRemoveCardData, nRemoveCardCount);
}

//删除牌数据
int CGameLogicBaseSK::RemoveCardData(vector_carddata& byCardData, const BYTE byRemoveCardData[], const UINT nRemoveCardCount)
{
	UINT i = 0;
	vector_carddata vtTemp;
	vector_carddata::iterator itvector;
	int bIsValid = TRUE;
	vtTemp.clear();
	for (i=0; i<nRemoveCardCount; i++)
	{
		itvector = find(byCardData.begin(), byCardData.end(), byRemoveCardData[i]);
		if (itvector == byCardData.end())
		{
			bIsValid = FALSE;
			if (vtTemp.size() > 0)
			{
				AddCardData(byCardData, &vtTemp[0], (UINT)(vtTemp.size()));
			}
			break;
		}
		else
		{
			vtTemp.push_back(byRemoveCardData[i]);
			byCardData.erase(itvector);
		}
	}

	return bIsValid;
}