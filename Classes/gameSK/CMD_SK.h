#ifndef CMD_SKPROTOCOL_HEAD_FILE
#define CMD_SKPROTOCOL_HEAD_FILE
#pragma pack(1)
//////////////////////////////////////////////////////////////////////////
//公共宏定义

#define SK_KIND_ID						120									//游戏 I D
#define GAME_PLAYER_MAX					100									//游戏最大人数
#define GAME_NAME						TEXT("千变双扣")                     //游戏名字
#define GAME_GENRE						(GAME_GENRE_GOLD|GAME_GENRE_MATCH|GAME_GENRE_SCORE)	//游戏类型
#define CARD_COUNT						108									//扑克总数
#define SK_GAME_PLAYER					2									//游戏人数
#define SK_MAX_COUNT					27									//扑克数目
#define VERSION_SERVER			    	PROCESS_VERSION(6,0,3)				//程序版本
#define VERSION_CLIENT				    PROCESS_VERSION(6,0,3)				//程序版本

#define MAX_TOP_COUNT                  10
//结束原因
#define SK_GER_NO_PLAYER				0x10								//没有玩家

//游戏状态
#define SK_GS_T_FREE					GS_FREE								//等待开始
#define SK_GS_T_PLAYING				    GS_PLAYING							//游戏进行

//////////////////////////////////////////////////////////////////////////
//服务器命令结构

#define SUB_S_SK_GAME_START				121									//游戏开始
#define SUB_S_SK_SEND_CARD				122									//发牌消息
#define SUB_S_SK_OUT_CARD				123									//出牌结果
#define SUB_S_SK_PASS_CARD				124									//过牌消息
#define SUB_S_SK_GAME_END				105									//游戏结束
#define SUB_S_SK_OUT_CARD_ERROR			106									//出牌失败
#define SUB_S_SK_YUYIN					107									//语音消息
#define SUB_S_SK_SHOW_CARD				108									//调试显牌
#define SUB_S_SK_TRUSTEE				109									//托管消息
#define SUB_S_SK_CHANGE_CARD			110									//调试换牌
#define SUB_S_SK_ANDROID_SITDOWN		111									//机器人坐下
#define SUB_S_SK_ANDROID_STANDUP		112									//机器人坐下
#define SUB_S_SK_USER_RANK				113									//发送名次

//游戏定时器
#define SK_IDT_GAME_FREE					1									//开始定时器
#define SK_TIME_GAME_FREE					30									//开始定时器(秒)
#define SK_IDT_DISPATCH_CARD				2									//发牌定时器
#define SK_TIME_DISPATCH_CARD				6									//发牌定时器(秒)
#define SK_IDT_OUT_CARD                     3									//操作定时器
#define SK_TIME_OUT_CARD					30									//操作定时器(秒)
#define SK_TIME_OUT_CARD2					5									//超时出牌

#define SK_IDT_SEND_RANK					4									//发送名次
#define SK_TIME_SEND_RANK					10									//发送名次定时器(秒)

#define SK_COUNT_DK						7//平扣
#define SK_COUNT_PK						13//单扣
#define SK_COUNT_SK						28//双扣

struct SK_tagClientSerial
{
    BYTE Serial[33];
};
//调试显示
struct CMD_S_SK_ShowCard
{
    WORD wChairID;
    UINT nCardCount;
    BYTE byCardData[SK_MAX_COUNT];
};

//调试换牌
struct CMD_S_SK_ChangeCard
{
    WORD wChairID;
    UINT nCardCount;
    BYTE byCardData[SK_MAX_COUNT];
};

//比赛前十
struct tagMatchTopTen
{
    WORD wCount;
    UINT32 dwUserID[MAX_TOP_COUNT];
    LONGLONG lScore[MAX_TOP_COUNT];
    CHAR szNickname[MAX_TOP_COUNT][NAME_LEN];
};

//游戏状态
struct CMD_S_SK_StatusFree
{
    WORD								wBankerUser;						//庄家用户
    WORD				 				wCurrentUser;						//当前玩家
    UINT32								lCellScore;							//基础积分
    tagMatchTopTen						TopTen;                             //比赛前10
    
};

//游戏状态
struct CMD_S_SK_StatusPlay
{
    //加注信息
    LONGLONG								lCellScore;						//单元下注
    int										nTimes[SK_GAME_PLAYER];			//当前倍数
    LONGLONG								lGongXian;						//用户分数上限
    
    //状态信息
    WORD				 					wCurrentUser;					//当前玩家
    BYTE									cbPlayStatus[SK_GAME_PLAYER];		//游戏状态
    
    //扑克信息
    BYTE									byCardData[SK_MAX_COUNT];			//扑克数据
    UINT									nCardCount[SK_GAME_PLAYER];		//牌数量
    //出牌信息
    WORD									wOutCardUser;					//出牌玩家
    UINT									nOutCardCount;					//出牌数目
    BYTE									byOutCardData[SK_MAX_COUNT];		//出牌数据
    tagMatchTopTen							TopTen;							//前十名
};

//游戏开始
struct CMD_S_SK_GameStart
{
    //下注信息
    UINT32								lMaxScore;							//最大下注
    LONGLONG							lCellScore;							//单元下注
    
    //用户信息
    WORD								wBankerUser;						//庄家用户
    WORD				 				wCurrentUser;						//当前玩家
    
    BYTE								byFirstCard;						//随机牌
    BYTE								byCardCount;						//牌数量
    BYTE								byCardData[SK_MAX_COUNT];			//牌数据
};

//发牌完成消息
struct CMD_S_SK_SendCard
{
    WORD wCurrentUser;//首先出牌玩家
    BYTE byCardData;//首张牌
};

//用户出牌
struct CMD_S_SK_OutCard
{
    UINT32								bIsGameEnd;							//游戏是否结束
    WORD								wCurrentUser;						//当前用户
    WORD								wOutCardUser;						//出牌用户
    int									nTimes[SK_GAME_PLAYER];				//当前倍数
    LONGLONG							lGongxian;							//贡献分
    UINT								nCardCount;							//出牌数目
    BYTE								byCardData[SK_MAX_COUNT];				//牌数据
};

//玩家过牌
struct CMD_S_SK_PassCard
{
    WORD wCurrentUser;
    WORD wPassCardUser;
};

//游戏结束
struct CMD_S_SK_GameEnd
{
    WORD								wWinner;							//赢家
    LONGLONG							lGongxian[SK_GAME_PLAYER];			//贡献
    int									nTimes[SK_GAME_PLAYER];//倍数
    LONGLONG							lGameTax;							//游戏税收
    LONGLONG							lGameScore[SK_GAME_PLAYER];			//游戏得分
    UINT								nCardCount;							//牌数目
    char								szName[SK_GAME_PLAYER][NAME_LEN];		//用户名字
    BYTE								byCardData[SK_MAX_COUNT];				//用户扑克
};

//游戏结束
//struct CMD_S_SK_GameEnd_Mobile
//{
//    WORD								wWinner;							//赢家
//    LONGLONG							lGongxian[SK_GAME_PLAYER];			//贡献
//    int									nTimes[SK_GAME_PLAYER];//倍数
//    LONGLONG							lGameTax;							//游戏税收
//    LONGLONG							lGameScore[SK_GAME_PLAYER];			//游戏得分
//    UINT								nCardCount;							//牌数目
//    char								szName[SK_GAME_PLAYER][LEN_NICKNAME];		//用户名字
//    BYTE								byCardData[SK_MAX_COUNT];				//用户扑克
//};

//玩家名次
struct CMD_S_UserRank
{
    tagMatchTopTen TopTen;//前十名
};

//////////////////////////////////////////////////////////////////////////

//客户端命令结构
#define SUB_C_SK_OUT_CARD					11									//玩家出牌
#define SUB_C_SK_PASS_CARD					12									//玩家过牌
#define SUB_C_SK_YUYIN						13									//语音消息
#define SUB_C_SK_TRUSTEE					14									//托管消息
#define SUB_C_SK_SHOW_CARD					15									//显示消息
#define SUB_C_SK_CHANGE_CARD				19									//换牌消息

//用户加注
struct CMD_C_SK_OutCard
{
    WORD								wCurrentUser;								//出牌玩家
    UINT								nCardCount;									//牌数量
    BYTE								byCardData[SK_MAX_COUNT];						//牌数据
};

//比牌数据包
struct CMD_C_SK_PassCard
{
    WORD								wCompareUser;						//比牌用户
};

//语音消息
struct CMD_C_SK_YuYin
{
    int					nIndex;
    WORD				wChairID;
};

//托管
struct CMD_C_SK_Trustee
{
    WORD wChairID;
    bool bTrustee;
};

//用户下注
struct CMD_C_SK_ManageControl
{
    bool						bManageControl;							//是否控制
    BYTE						cbControlArea;							//区域控制
    SK_tagClientSerial				ClientSerial;							//机器序列号
};
#pragma pack()
//////////////////////////////////////////////////////////////////////////

#endif
