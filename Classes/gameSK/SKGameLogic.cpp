#include "SKGameLogic.h"

CGameLogicSK::CGameLogicSK(void)
{
    ResetCardData();
}

CGameLogicSK::~CGameLogicSK(void)
{
}

//复位牌数据
void CGameLogicSK::ResetCardData()
{
    //扑克数据
    const BYTE byCardData[]=
    {
        0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08,0x09,0x0A,0x0B,0x0C,0x0D,	//方块 A - K
        0x11,0x12,0x13,0x14,0x15,0x16,0x17,0x18,0x19,0x1A,0x1B,0x1C,0x1D,	//梅花 A - K
        0x21,0x22,0x23,0x24,0x25,0x26,0x27,0x28,0x29,0x2A,0x2B,0x2C,0x2D,	//红桃 A - K
        0x31,0x32,0x33,0x34,0x35,0x36,0x37,0x38,0x39,0x3A,0x3B,0x3C,0x3D,	//黑桃 A - K
        0x41,0x42,															//小、大王
        0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08,0x09,0x0A,0x0B,0x0C,0x0D,	//方块 A - K
        0x11,0x12,0x13,0x14,0x15,0x16,0x17,0x18,0x19,0x1A,0x1B,0x1C,0x1D,	//梅花 A - K
        0x21,0x22,0x23,0x24,0x25,0x26,0x27,0x28,0x29,0x2A,0x2B,0x2C,0x2D,	//红桃 A - K
        0x31,0x32,0x33,0x34,0x35,0x36,0x37,0x38,0x39,0x3A,0x3B,0x3C,0x3D,	//黑桃 A - K
        0x41,0x42															//小、大王
    };
    m_byCardData.assign(byCardData, byCardData+_countof(byCardData));
}

//财神变换
int CGameLogicSK::MagicCardData(const BYTE bySrcCardData[], const UINT nCardCount, BYTE byDestCardData[])
{
    return TRUE;
}

//获取牌型
int CGameLogicSK::GetCardType(const BYTE byCardData[], const UINT nCardCount, void* pResult)
{
    int nCardType = 0;
    m_mapCardType.clear();
    
    if (nCardCount > 0)
    {
        if (pResult == NULL)
        {
            pResult = &m_mapCardType;
        }
        
        nCardType = GetMagicCardType(byCardData, nCardCount, pResult);
    }
    
    return nCardType;
}

//获取普通牌型
int CGameLogicSK::GetNormalCardType(const BYTE byCardData[], const UINT nCardCount, void* pResult)
{
    int nCardType = 0;
    int bIsEnd2 = FALSE;
    map<int, int> mapCardData;
    map<int, int>::iterator itmap;
    map<int, int>::reverse_iterator ritmap;
    UINT i = 0;
    UINT nKingCount = 0;
    int nTemp = 0;
    ULONGLONG dwType1 = 0x0000000000000000;
    BYTE byType[12] = {0};
    int nLogicValue = 0;
    
    try
    {
        //合并相同逻辑值的牌
        for (i=0; i<nCardCount; i++)
        {
            if (byCardData[i] == 0x00)
                throw SK_CT_ERROR;
            nTemp = GetLogicValue(byCardData[i]);
            if (nTemp > 15)
                nKingCount++;
            itmap = mapCardData.find(nTemp);
            if (itmap == mapCardData.end())
            {
                mapCardData[nTemp] = 1;
                continue;
            }
            itmap->second++;
        }
        
        //三王炸判断
        if ((nKingCount == nCardCount) && (nCardCount == 3))
        {
            nLogicValue = itmap->first;
            throw SK_CT_3K;
        }
        //天王炸
        if ((nKingCount == nCardCount) && (nCardCount == 4))
        {
            nLogicValue = itmap->first;
            throw SK_CT_4K;
        }
        
        //统计牌
        itmap = mapCardData.begin();
        while (itmap != mapCardData.end())
        {
            switch (itmap->second)
            {
                case 1:  { dwType1 |= 0x0000000000000001; byType[0] += 1; } break;
                case 2:  { dwType1 |= 0x0000000000000010; byType[1] += 1; } break;
                case 3:  { dwType1 |= 0x0000000000000100; byType[2] += 1; } break;
                case 4:  { dwType1 |= 0x0000000000001000; byType[3] += 1; } break;
                case 5:  { dwType1 |= 0x0000000000010000; byType[4] += 1; } break;
                case 6:  { dwType1 |= 0x0000000000100000; byType[5] += 1; } break;
                case 7:  { dwType1 |= 0x0000000001000000; byType[6] += 1; } break;
                case 8:  { dwType1 |= 0x0000000010000000; byType[7] += 1; } break;
                case 9:  { dwType1 |= 0x0000000100000000; byType[8] += 1; } break;
                case 10: { dwType1 |= 0x0000001000000000; byType[9] += 1; } break;
                case 11: { dwType1 |= 0x0000010000000000; byType[10] += 1; } break;
                case 12: { dwType1 |= 0x0000100000000000; byType[11] += 1; } break;
                default: throw SK_CT_ERROR; break;
            }
            itmap++;
        }
        
        switch (nCardCount)
        {
            case 1://单牌
                nLogicValue = mapCardData.begin()->first;
                throw SK_CT_1;
                break;
            case 2://对子（不能是二张王）
                if ((dwType1 == 0x0000000000000010) && (byType[1] == 1))
                {
                    nLogicValue = mapCardData.begin()->first;
                    throw SK_CT_2;
                }
                break;
            case 3://三张、三王炸弹
                if ((dwType1 == 0x0000000000000100) && (byType[2] == 1))
                {
                    nLogicValue = mapCardData.begin()->first;
                    throw SK_CT_3;
                }
                break;
            case 4://炸弹、天王炸弹
                if ((dwType1 == 0x0000000000001000) && (byType[3] == 1))
                {
                    nLogicValue = mapCardData.begin()->first;
                    throw SK_CT_4;
                }
                break;
            case 5://五相、单顺
                if ((dwType1 == 0x0000000000010000) && (byType[4] == 1))
                {
                    nLogicValue = mapCardData.begin()->first;
                    throw SK_CT_5;
                }
                break;
            case 6://六相、双顺、单顺
                if ((dwType1 == 0x0000000000100000) && (byType[5] == 1))
                {
                    nLogicValue = mapCardData.begin()->first;
                    throw SK_CT_6;
                }
                break;
            case 7://七相、单顺
                if ((dwType1 == 0x0000000001000000) && (byType[6] == 1))
                {
                    nLogicValue = mapCardData.begin()->first;
                    throw SK_CT_7;
                }
                break;
            case 8://八相、双顺、单顺
                if ((dwType1 == 0x0000000010000000) && (byType[7] == 1))
                {
                    nLogicValue = mapCardData.begin()->first;
                    throw SK_CT_8;
                }
                break;
            case 9://九相、三顺、单顺
                if ((dwType1 == 0x0000000100000000) && (byType[8] == 1))
                {
                    nLogicValue = mapCardData.begin()->first;
                    throw SK_CT_9;
                }
                break;
            case 10://十相、双顺、单顺
                if ((dwType1 == 0x0000001000000000) && (byType[9] == 1))
                {
                    nLogicValue = mapCardData.begin()->first;
                    throw SK_CT_10;
                }
                break;
            case 11://十一相、单顺
                if ((dwType1 == 0x0000010000000000) && (byType[10] == 1))
                {
                    nLogicValue = mapCardData.begin()->first;
                    throw SK_CT_11;
                }
                break;
            case 12://十二相、三连炸、三顺、双顺、单顺
                if ((dwType1 == 0x0000100000000000) && (byType[11] == 1))
                {
                    nLogicValue = mapCardData.begin()->first;
                    throw SK_CT_12;
                }
                break;
            case 13://三连炸
                break;
            case 14://三连炸、双顺
                break;
            case 15://三连炸、三顺
                break;
            case 16://四连炸、三连炸、双顺
                break;
            case 17://四连炸、三连炸
                break;
            case 18://四连炸、三连炸、三顺、双顺
                break;
            case 19://四连炸、三连炸
                break;
            case 20://五连炸、四连炸、三连炸、双顺
                break;
            case 21://五连炸、四连炸、三连炸、三顺
                break;
            case 22://五连炸、四连炸、三连炸、双顺
                break;
            case 23://五连炸、四连炸、三连炸
                break;
            case 24://六连炸、五连炸、四连炸、三连炸、三顺、双顺
                break;
            case 25://六连炸、五连炸、四连炸、三连炸
                break;
            case 26://六连炸、五连炸、四连炸、三连炸
                break;
            case 27://六连炸、五连炸、四连炸、三连炸、三顺
                break;
        }
        
        //3-4-5-6-7-8-9-10-11-12-13-14-15
        //3-4-5-6-7-8-9-10-J -Q -K -A -2
        
        if (((mapCardData.size() == 6) && (nCardCount >= 24) && (nCardCount <= 27))//六连炸
            || ((mapCardData.size() == 5) && (nCardCount >= 20) && (nCardCount <= 27))//五连炸
            || ((mapCardData.size() == 4)&& (nCardCount >= 16)&& (nCardCount <= 27))//四连炸
            || ((mapCardData.size() == 3) && (nCardCount >= 12) && (nCardCount <= 27)))//三连炸
        {
            int bGreaterThanFour = TRUE;
            UINT nCount1 = 0;
            UINT nCount2 = 0;
            int nMinCount = 0;
            
            ritmap = mapCardData.rbegin();
            while (ritmap != mapCardData.rend())
            {
                if (ritmap->second < 4)
                {
                    bGreaterThanFour = FALSE;
                    break;
                }
                if (ritmap == mapCardData.rbegin())
                {
                    nCount2 = 1;
                    nTemp = ritmap->first;
                    nMinCount = ritmap->second;
                }
                else
                {
                    nMinCount = min(nMinCount, ritmap->second);
                    if (ritmap->first == nTemp-1)
                    {
                        nCount2++;
                        nTemp = ritmap->first;
                    }
                }
                ritmap++;
            }
            if (bGreaterThanFour)
            {
                SKEnumCardType eCardType = SK_CT_ERROR;
                switch (nMinCount+mapCardData.size())
                {
                    case 12: eCardType = SK_CT_12L; break;
                    case 11: eCardType = SK_CT_11L; break;
                    case 10: eCardType = SK_CT_10L; break;
                    case 9: eCardType = SK_CT_9L; break;
                    case 8: eCardType = SK_CT_8L; break;
                    case 7: eCardType = SK_CT_7L; break;
                }
                if (nCount2 == mapCardData.size())
                {
                    nLogicValue = mapCardData.rbegin()->first;
                    throw eCardType;
                }
                itmap = mapCardData.begin();
                while (itmap != mapCardData.end())
                {
                    if (itmap == mapCardData.begin())
                    {
                        nCount1 = 1;
                        nTemp = itmap->first;
                    }
                    else
                    {
                        if (itmap->first == nTemp+1)
                        {
                            nCount1++;
                            nTemp = itmap->first;
                        }
                    }
                    itmap++;
                }
                if (((nCount1+nCount2) == mapCardData.size())
                    && ((mapCardData.rbegin()->first-mapCardData.begin()->first) == 12))
                {
                    nLogicValue = nTemp;
                    throw eCardType;
                }
                
            }
        }
        
        if ((dwType1 == 0x0000000000000001)
            && (byType[0] == nCardCount)
            && (nCardCount >= 5)
            && (nCardCount <= 12))
        {
            if ((mapCardData.rbegin())->first <= 14)
            {
                if (((mapCardData.rbegin())->first - (mapCardData.begin())->first) == (int)(nCardCount-1))
                {
                    nLogicValue = mapCardData.begin()->first;
                    throw SK_CT_1X;
                }
            }
        }
        //双顺判断
        if ((dwType1 == 0x0000000000000010)
            && (byType[1] == nCardCount/2)
            && ((nCardCount%2) == 0)
            && (nCardCount >= 6)
            && (nCardCount <= 24))
        {
            if ((mapCardData.rbegin())->first < 15)
            {
                if (((mapCardData.rbegin())->first - (mapCardData.begin())->first) == (int)(nCardCount/2-1))
                {
                    nLogicValue = mapCardData.begin()->first;
                    throw SK_CT_2X;
                }
            }
        }
        //三顺判断
        if ((dwType1 == 0x0000000000000100)
            && (byType[2] == nCardCount/3)
            && ((nCardCount%3) == 0)
            && (nCardCount >= 9)
            && (nCardCount <= 27))
        {
            if ((mapCardData.rbegin())->first < 15)
            {
                if (((mapCardData.rbegin())->first - (mapCardData.begin())->first) == (int)(nCardCount/3-1))
                {
                    nLogicValue = mapCardData.begin()->first;
                    throw SK_CT_3X;
                }
            }
        }
    }
    catch (SKEnumCardType eCardType)
    {
        nCardType = (int)eCardType;
    }
    catch (...)
    {
        nCardType = (int)SK_CT_ERROR;
    }
    if (pResult != NULL)
    {
        memset(pResult, 0, sizeof(tagLogicValue));
        if (nCardType != 0)
        {
            ((tagLogicValue *)pResult)->nLogicValue = nLogicValue;
            ((tagLogicValue *)pResult)->bIsEnd2 = bIsEnd2;
        }
    }
    
    return nCardType;
}

//获取变换牌型
int CGameLogicSK::GetMagicCardType(const BYTE byCardData[], const UINT nCardCount, void* pResult)
{
    //变量定义
    int nMagicCardCount = 0;//财神牌数量
    int nNormalCardCount = 0;//非财神牌数量
    vector_carddata vtTempCardData(nCardCount);//临时数组
    map_cardtype *pCardTypeMap = NULL;
    map_cardtype::iterator itmap;
    UINT i = 0;
    BYTE byMax = 0;
    BYTE byMin = 13;
    BYTE byCard1 = 0;
    BYTE byCard2 = 0;
    BYTE byCard3 = 0;
    BYTE byCard4 = 0;
    int nTempType = 0;
    int nTempLogicValue = 0;
    tagLogicValue LogicValue = {0};
    map<int, int> mapCardData;
    map<int, int>::iterator itmap2;
    
    //牌型变量
    if ((pResult == NULL))
        return nTempType;
    pCardTypeMap = (map_cardtype *)pResult;
    
    //计算财神牌数
    for (i=0; i<nCardCount; i++)
    {
        nTempLogicValue = GetLogicValue(byCardData[i]);
        itmap2 = mapCardData.find(nTempLogicValue);
        if (itmap2 == mapCardData.end())
            mapCardData[nTempLogicValue] = 1;
        else
            itmap2->second++;
        if (nTempLogicValue > 15)
        {
            nMagicCardCount++;
            continue;
        }
        if ((byCardData[i] != 0x41) && (byCardData[i] != 0x42))
        {
            byMax = max(byMax, GetCardValue(byCardData[i]));
            byMin = min(byMin, GetCardValue(byCardData[i]));
        }
        vtTempCardData[nNormalCardCount++] = byCardData[i];
    }
    
    //没有财神牌
    if ((nMagicCardCount == 0) || (nMagicCardCount == nCardCount))
    {
        //获取原始牌型
        nTempType = GetNormalCardType(byCardData, nCardCount, &LogicValue);
        if (nTempType > 0)
        {
            (*pCardTypeMap)[nTempType] = LogicValue;
        }
        return nTempType;
    }
    //起始点
    //byMin = max(0x51, 0x51+byMin-nMagicCardCount);
    //byMax = min(0x5D, 0x51+byMax+nMagicCardCount-1);
    byMin = 0x51;
    byMax = 0x5D;
    //只有1张癞子牌
    if (nMagicCardCount == 1)
    {
        //获取原始牌型
        nTempType = GetNormalCardType(byCardData, nCardCount, &LogicValue);
        if (nTempType > 0)
        {
            (*pCardTypeMap)[nTempType] = LogicValue;
        }
        //变换
        for (byCard1=byMin; byCard1<=byMax; byCard1++)
        {
            vtTempCardData[nNormalCardCount++] = byCard1;
            i = (UINT)(vtTempCardData.size());
            i = vtTempCardData.back();
            nTempType = GetNormalCardType(&vtTempCardData[0], nCardCount, &LogicValue);
            if (nTempType > 0)
            {
                itmap = (*pCardTypeMap).find(nTempType);
                if (itmap != (*pCardTypeMap).end())
                {
                    if (LogicValue.bIsEnd2 == itmap->second.bIsEnd2)
                    {
                        itmap->second.nLogicValue = max(itmap->second.nLogicValue, LogicValue.nLogicValue);
                    }
                    else
                    {
                        if (!LogicValue.bIsEnd2)
                            itmap->second = LogicValue;
                    }
                }
                else
                {
                    (*pCardTypeMap)[nTempType] = LogicValue;
                }
            }
            vtTempCardData[--nNormalCardCount] = 0x00;
        }
    }
    //有2张癞子牌
    if (nMagicCardCount == 2)
    {
        //获取原始牌型
        nTempType = GetNormalCardType(byCardData, nCardCount, &LogicValue);
        if (nTempType > 0)
        {
            (*pCardTypeMap)[nTempType] = LogicValue;
        }
        for (byCard1=byMin; byCard1<=byMax; byCard1++)
        {
            vtTempCardData[nNormalCardCount++] = byCard1;
            for (byCard2=byCard1; byCard2<=byMax; byCard2++)
            {
                vtTempCardData[nNormalCardCount++] = byCard2;
                nTempType = GetNormalCardType(&vtTempCardData[0], nCardCount, &LogicValue);
                if (nTempType > 0)
                {
                    itmap = (*pCardTypeMap).find(nTempType);
                    if (itmap != (*pCardTypeMap).end())
                    {
                        if (LogicValue.bIsEnd2 == itmap->second.bIsEnd2)
                        {
                            itmap->second.nLogicValue = max(itmap->second.nLogicValue, LogicValue.nLogicValue);
                        }
                        else
                        {
                            if (!LogicValue.bIsEnd2)
                                itmap->second = LogicValue;
                        }
                    }
                    else
                    {
                        (*pCardTypeMap)[nTempType] = LogicValue;
                    }
                }
                vtTempCardData[--nNormalCardCount] = 0x00;
            }
            vtTempCardData[--nNormalCardCount] = 0x00;
        }
    }
    //有3张癞子牌
    if (nMagicCardCount == 3)
    {
        //获取原始牌型
        nTempType = GetNormalCardType(byCardData, nCardCount, &LogicValue);
        if (nTempType > 0)
        {
            (*pCardTypeMap)[nTempType] = LogicValue;
        }
        for (byCard1=byMin; byCard1<=byMax; byCard1++)
        {
            vtTempCardData[nNormalCardCount++] = byCard1;
            for (byCard2=byCard1; byCard2<=byMax; byCard2++)
            {
                vtTempCardData[nNormalCardCount++] = byCard2;
                for (byCard3=byCard2; byCard3<=byMax; byCard3++)
                {
                    vtTempCardData[nNormalCardCount++] = byCard3;
                    nTempType = GetNormalCardType(&vtTempCardData[0], nCardCount, &LogicValue);
                    if (nTempType > 0)
                    {
                        itmap = (*pCardTypeMap).find(nTempType);
                        if (itmap != (*pCardTypeMap).end())
                        {
                            if (LogicValue.bIsEnd2 == itmap->second.bIsEnd2)
                            {
                                itmap->second.nLogicValue = max(itmap->second.nLogicValue, LogicValue.nLogicValue);
                            }
                            else
                            {
                                if (!LogicValue.bIsEnd2)
                                    itmap->second = LogicValue;
                            }
                        }
                        else
                        {
                            (*pCardTypeMap)[nTempType] = LogicValue;
                        }
                    }
                    vtTempCardData[--nNormalCardCount] = 0x00;
                }
                vtTempCardData[--nNormalCardCount] = 0x00;
            }
            vtTempCardData[--nNormalCardCount] = 0x00;
        }
    }
    //有4张癞子牌
    if (nMagicCardCount == 4)
    {
        //获取原始牌型
        nTempType = GetNormalCardType(byCardData, nCardCount, &LogicValue);
        if (nTempType > 0)
        {
            (*pCardTypeMap)[nTempType] = LogicValue;
        }
        for (byCard1=byMin; byCard1<=byMax; byCard1++)
        {
            vtTempCardData[nNormalCardCount++] = byCard1;
            for (byCard2=byCard1; byCard2<=byMax; byCard2++)
            {
                vtTempCardData[nNormalCardCount++] = byCard2;
                for (byCard3=byCard2; byCard3<=byMax; byCard3++)
                {
                    vtTempCardData[nNormalCardCount++] = byCard3;
                    for (byCard4=byCard3; byCard4<=byMax; byCard4++)
                    {
                        vtTempCardData[nNormalCardCount++] = byCard4;
                        nTempType = GetNormalCardType(&vtTempCardData[0], nCardCount, &LogicValue);
                        if (nTempType > 0)
                        {
                            itmap = (*pCardTypeMap).find(nTempType);
                            if (itmap != (*pCardTypeMap).end())
                            {
                                if (LogicValue.bIsEnd2 == itmap->second.bIsEnd2)
                                {
                                    itmap->second.nLogicValue = max(itmap->second.nLogicValue, LogicValue.nLogicValue);
                                }
                                else
                                {
                                    if (!LogicValue.bIsEnd2)
                                        itmap->second = LogicValue;
                                }
                            }
                            else
                            {
                                (*pCardTypeMap)[nTempType] = LogicValue;
                            }
                        }
                        vtTempCardData[--nNormalCardCount] = 0x00;
                    }
                    vtTempCardData[--nNormalCardCount] = 0x00;
                }
                vtTempCardData[--nNormalCardCount] = 0x00;
            }
            vtTempCardData[--nNormalCardCount] = 0x00;
        }
    }
    
    if (pCardTypeMap->size() > 0)
    {
        nTempType = pCardTypeMap->rbegin()->first;
    }
    return nTempType;
}

//比较大小
int CGameLogicSK::CompareCardData(const BYTE byFirstCardData[], const UINT nFirstCardCount,
                                  const BYTE bySecondCardData[], const UINT nSecondCardCount)//return (first>second);
{
    int nResult = 0;
    int nFirstType = 0;
    int nSecondType = 0;
    tagLogicValue FirstLogicValue = {0};
    tagLogicValue SecondLogicValue = {0};
    map_cardtype mapTypeFirst;
    map_cardtype mapTypeSecond;
    map_cardtype::iterator itmap1;
    map_cardtype::iterator itmap2;
    
    //获取牌型
    nFirstType = GetCardType(byFirstCardData, nFirstCardCount, &mapTypeFirst);
    nSecondType = GetCardType(bySecondCardData, nSecondCardCount, &mapTypeSecond);
    
    //if ((mapTypeFirst.size() > 0) && ((mapTypeFirst.rbegin())->first >= CT_4))
    //{
    //	nFirstType = (mapTypeFirst.rbegin())->first;
    //	FirstLogicValue = (mapTypeFirst.rbegin())->second;
    //}
    //if ((mapTypeSecond.size() > 0) && ((mapTypeSecond.rbegin())->first >= CT_4))
    //{
    //	nSecondType = (mapTypeSecond.rbegin())->first;
    //	SecondLogicValue = (mapTypeSecond.rbegin())->second;
    //}
    //if (((mapTypeFirst.rbegin())->first != (mapTypeSecond.rbegin())->first)
    //	|| ((((mapTypeFirst.rbegin())->first==(mapTypeSecond.rbegin())->first) && ((mapTypeFirst.rbegin())->first != 0))))
    //{
    //	nResult = (mapTypeFirst.rbegin())->first - (mapTypeSecond.rbegin())->first;
    //	if ((nResult == 0) && (nFirstCardCount == nSecondCardCount))
    //	{
    //		FirstLogicValue = (mapTypeFirst.rbegin())->second;
    //		SecondLogicValue = (mapTypeSecond.rbegin())->second;
    //		nResult = SecondLogicValue.bIsEnd2 - FirstLogicValue.bIsEnd2;
    //		if (nResult == 0)
    //		{
    //			nResult = FirstLogicValue.nLogicValue - SecondLogicValue.nLogicValue;
    //		}
    //	}
    //}
    //else
    //{
    nResult = nFirstType - nSecondType;
    if (nFirstType <= 0)
        return nResult;
    
    if ((mapTypeFirst.size() > 0) && (mapTypeSecond.size() > 0))
    {
        if (nResult == 0)//牌型相同
        {
            nResult = mapTypeSecond.rbegin()->second.bIsEnd2 - mapTypeFirst.rbegin()->second.bIsEnd2;
            if (nResult == 0)
            {
                if ((mapTypeFirst.rbegin()->first >= SK_CT_4) || (nFirstCardCount == nSecondCardCount))
                {
                    nResult = mapTypeFirst.rbegin()->second.nLogicValue - mapTypeSecond.rbegin()->second.nLogicValue;
                }
            }
            return nResult;
        }
        if ((nResult > 0))
        {
            if (mapTypeFirst.rbegin()->first >= SK_CT_4)
                return nResult;
            
            itmap1 = mapTypeFirst.find(mapTypeSecond.rbegin()->first);
            if (itmap1 != mapTypeFirst.end())
            {
                nResult = itmap1->second.nLogicValue - mapTypeSecond.rbegin()->second.nLogicValue;
            }
            else
            {
                nResult = 0;
            }
        }
    }
    //}
    
    
    return nResult;
}

//获取类型描述
int CGameLogicSK::GetTypeDescribe(int nCardType, string& strTypeDescribe, void* pResult)
{
    strTypeDescribe = "";
    map_cardtype::iterator itmap;
    itmap = m_mapCardType.begin();
    while (itmap != m_mapCardType.end())
    {
        nCardType = itmap->first;
        switch (nCardType)
        {
            case SK_CT_1:									//单牌类型
                strTypeDescribe += "单牌类型";
                break;
            case SK_CT_2:									//对牌类型
                strTypeDescribe += "对牌类型";
                break;
            case SK_CT_3:									//三条类型
                strTypeDescribe += "三条类型";
                break;
            case SK_CT_1X:									//单连类型
                strTypeDescribe += "单连类型";
                break;
            case SK_CT_2X:									//对连类型
                strTypeDescribe += "对连类型";
                break;
            case SK_CT_3X:									//三连类型
                strTypeDescribe += "三连类型";
                break;
            case SK_CT_4:									//炸弹类型(4张)
                strTypeDescribe += "炸弹类型(4张)";
                break;
            case SK_CT_5:									//炸弹类型(5张)
                strTypeDescribe += "炸弹类型(5张)";
                break;
            case SK_CT_3K:									//三王炸弹
                strTypeDescribe += "三王炸弹(最小6星)";
                break;
            case SK_CT_6:									//6星硬炸
                strTypeDescribe += "6星硬炸";
                break;
            case SK_CT_7L:									//7星连炸
                strTypeDescribe += "7星连炸";
                break;
            case SK_CT_7:									//7星硬炸
                strTypeDescribe += "7星硬炸";
                break;
            case SK_CT_4K:									//天王炸弹
                strTypeDescribe += "天王炸弹";
                break;
            case SK_CT_8L:									//8星连炸
                strTypeDescribe += "8星连炸";
                break;
            case SK_CT_8:									//8星硬炸
                strTypeDescribe += "8星硬炸";
                break;
            case SK_CT_9L:									//9星连炸
                strTypeDescribe += "9星连炸";
                break;
            case SK_CT_9:									//9星硬炸
                strTypeDescribe += "9星硬炸";
                break;
            case SK_CT_10L:								//10星连炸
                strTypeDescribe += "10星连炸";
                break;
            case SK_CT_10:									//10星硬炸
                strTypeDescribe += "10星硬炸";
                break;
            case SK_CT_11L:								//11星连炸
                strTypeDescribe += "11星连炸";
                break;
            case SK_CT_11:									//11星硬炸
                strTypeDescribe += "11星硬炸";
                break;
            case SK_CT_12L:								//12星连炸
                strTypeDescribe += "12星连炸";
                break;
            case SK_CT_12:									//12星硬炸
                strTypeDescribe += "12星硬炸";
                break;
        }
        itmap++;
    }
    
    return TRUE;
}

//提示出牌
int CGameLogicSK::GetPromptCardData(BYTE byPromptCardData[], UINT& nPromptCardCount, const BYTE bySrcCardData[], const UINT nSrcCardCount,
                                    const BYTE byOutCardData[], const UINT nOutCardCount)
{
    map_cardtype mapCardType;         //出牌牌行
    int nOutCardType = 0;
    UINT i = 0;
    int nLogicValue = 0;
    typedef list<BYTE> list_carddata;
    map<int, list_carddata> mapCardData;
    map<int, list_carddata>::iterator itmap;
    map<int, list_carddata>::iterator itmap2;
    list_carddata lsCardData;
    list_carddata::iterator itlist;
    UINT nKingCount = 0;
    nPromptCardCount = 0;
    list<tagCardInfo> lsCardInfo;
    list<tagCardInfo>::iterator itci;
    
    if (nSrcCardCount == 0) return FALSE;
    
    //获取牌型
    nOutCardType = GetCardType(byOutCardData, nOutCardCount, &mapCardType);
    
    //合并相同逻辑值的牌
    for (i=0; i<nSrcCardCount; i++)
    {
        lsCardData.clear();
        if (bySrcCardData[i] == 0x00) continue;
        nLogicValue = GetLogicValue(bySrcCardData[i]);
        if (nLogicValue > 15) nKingCount++;
        itmap = mapCardData.find(nLogicValue);
        if (itmap == mapCardData.end())
        {
            lsCardData.push_back(bySrcCardData[i]);
            mapCardData[nLogicValue] = lsCardData;
            continue;
        }
        itmap->second.push_back(bySrcCardData[i]);
    }
    
    //按牌数量排序
    tagCardInfo ci;
    itmap = mapCardData.begin();
    while (itmap != mapCardData.end())
    {
        itci = lsCardInfo.begin();
        while (itci != lsCardInfo.end())
        {
            if (itci->nCardCount > itmap->second.size())
            {
                ci.nLogicValue = itmap->first;
                ci.nCardCount = (UINT)(itmap->second.size());
                lsCardInfo.insert(itci, ci);
                break;
            }
            itci++;
        }
        if (itci == lsCardInfo.end())
        {
            ci.nLogicValue = itmap->first;
            ci.nCardCount = (UINT)(itmap->second.size());
            lsCardInfo.push_back(ci);
        }
        itmap++;
    }
    
    //try
    //{
    switch ((SKEnumCardType)nOutCardType)
    {
        case SK_CT_1://单张
        case SK_CT_2://对子
        case SK_CT_3://三张
        {
            if ((nSrcCardCount < nOutCardCount) || (mapCardData.rbegin()->first <= mapCardType.rbegin()->second.nLogicValue))
            {
                i = 4;
                break;
            }
            itci = lsCardInfo.begin();
            while (itci != lsCardInfo.end())
            {
                if ((itci->nLogicValue > mapCardType.rbegin()->second.nLogicValue)
                    && (itci->nCardCount >= nOutCardCount))
                {
                    itmap = mapCardData.find(itci->nLogicValue);
                    if (itmap != mapCardData.end())
                    {
                        itlist = itmap->second.begin();
                        nPromptCardCount = 0;
                        while ((itlist != itmap->second.end()) && (nPromptCardCount < nOutCardCount))
                        {
                            byPromptCardData[nPromptCardCount++] = *itlist;
                            itlist++;
                        }
                        return TRUE;
                    }
                }
                itci++;
            }
            
            i = 4;
            break;
        }
            break;
        case SK_CT_1X:									//单连类型
        case SK_CT_2X:									//对连类型
        case SK_CT_3X:									//三连类型
        {
            if ((nSrcCardCount < nOutCardCount) || (mapCardData.rbegin()->first <= mapCardType.rbegin()->second.nLogicValue))
            {
                i = 4;
                break;
            }
            
            UINT nSingleCount = (nOutCardType-SK_CT_3);
            UINT nLineCount = nOutCardCount / nSingleCount;
            UINT nSearchCount = 0;
            itmap = mapCardData.begin();
            while ((itmap != mapCardData.end()) && (itmap->first <= 14))
            {
                nSearchCount = 0;
                if (itmap->first > mapCardType.rbegin()->second.nLogicValue)
                {
                    itmap2 = itmap;
                    while ((itmap2 != mapCardData.end()) && (itmap2->first < (int)(itmap->first+nLineCount)))
                    {
                        if (itmap2->second.size() > nSingleCount)
                            nSearchCount += nSingleCount;
                        else
                            nSearchCount += (UINT)(itmap2->second.size());
                        itmap2++;
                    }
                    if ((nSearchCount+nKingCount) >= nOutCardCount)
                    {
                        itmap2 = itmap;
                        nPromptCardCount = 0;
                        while ((itmap2 != mapCardData.end()) && (itmap2->first < (int)(itmap->first+nLineCount)))
                        {
                            i = 0;
                            itlist = itmap2->second.begin();
                            while ((itlist != itmap2->second.end()) && (i < nSingleCount))
                            {
                                byPromptCardData[nPromptCardCount++] = *itlist;
                                itlist++;
                                i++;
                            }
                            itmap2++;
                        }
                        if (nPromptCardCount < nOutCardCount)
                        {
                            while ((itmap2 != mapCardData.end()) && (nPromptCardCount < nOutCardCount))
                            {
                                if (itmap2->first > 15)
                                {
                                    itlist = itmap2->second.begin();
                                    while (itlist != itmap2->second.end())
                                    {
                                        byPromptCardData[nPromptCardCount++] = *itlist;
                                        itlist++;
                                    }
                                }
                                itmap2++;
                            }
                        }
                        return TRUE;
                    }
                }
                itmap++;
            }
            i = 4;
            break;
        }
            break;
        case SK_CT_4://炸弹
        case SK_CT_5:									//炸弹类型(5张)
        case SK_CT_6:									//6星硬炸
        case SK_CT_7:									//7星硬炸
        case SK_CT_8:									//8星硬炸
        case SK_CT_9:									//9星硬炸
        case SK_CT_10:									//10星硬炸
        {
            //寻找炸弹
            nPromptCardCount = 0;
            itci = lsCardInfo.begin();
            while (itci != lsCardInfo.end())
            {
                if ((itci->nLogicValue > mapCardType.rbegin()->second.nLogicValue)
                    && ((itci->nCardCount+nKingCount) >= nOutCardCount))
                {
                    itmap = mapCardData.find(itci->nLogicValue);
                    if (itmap != mapCardData.end())
                    {
                        itlist = itmap->second.begin();
                        nPromptCardCount = 0;
                        while ((itlist != itmap->second.end()) && (nPromptCardCount < nOutCardCount))
                        {
                            byPromptCardData[nPromptCardCount++] = *itlist;
                            itlist++;
                        }
                        if (nPromptCardCount < nOutCardCount)
                        {
                            itmap2 = itmap;
                            while ((itmap2 != mapCardData.end()) && (nPromptCardCount < nOutCardCount))
                            {
                                if (itmap2->first > 15)
                                {
                                    itlist = itmap2->second.begin();
                                    while (itlist != itmap2->second.end())
                                    {
                                        byPromptCardData[nPromptCardCount++] = *itlist;
                                        itlist++;
                                    }
                                }
                                itmap2++;
                            }
                        }
                        return TRUE;
                    }
                }
                itci++;
            }
            
            if ((nOutCardCount == 7) && (nKingCount == 4))
            {
                nPromptCardCount = 0;
                byPromptCardData[nPromptCardCount++] = 0x41;
                byPromptCardData[nPromptCardCount++] = 0x41;
                byPromptCardData[nPromptCardCount++] = 0x42;
                byPromptCardData[nPromptCardCount++] = 0x42;
                return TRUE;
            }
            i = (nOutCardCount+1);
            break;
        }
            break;
        case SK_CT_3K:
        {i = 6; break;	}				//三王炸弹类型
        case SK_CT_7L:
        {i = 7;break;	}				//7星连炸
        case SK_CT_4K:
        {i = 8;  break;	}				//天王炸弹
        case SK_CT_8L:
        {i = 8; break; }					//8星连炸
        case SK_CT_9L:
        {i = 9; break; }					//9星连炸
        case SK_CT_10L:
        {i = 10;break; }				//10星连炸
        case SK_CT_11L:
        {i = 11;break; }				//11星连炸
        case SK_CT_11:									//11星硬炸
        case SK_CT_12L:								//12星连炸
        case SK_CT_12:									//12星硬炸
        default: break;
    }
    
    
    UINT nCount = i;
    if (i == 0)
    {
        nPromptCardCount = 0;
        return FALSE;
    }
    if (nCount < 4) return FALSE;
    
    //寻找炸弹
    nPromptCardCount = 0;
    itci = lsCardInfo.begin();
    while (itci != lsCardInfo.end())
    {
        if ((itci->nCardCount+nKingCount) >= nCount)
        {
            itmap = mapCardData.find(itci->nLogicValue);
            if (itmap != mapCardData.end())
            {
                itlist = itmap->second.begin();
                nPromptCardCount = 0;
                while (itlist != itmap->second.end())
                {
                    byPromptCardData[nPromptCardCount++] = *itlist;
                    itlist++;
                }
                if (nPromptCardCount < nCount)
                {
                    itmap2 = itmap;
                    while ((itmap2 != mapCardData.end()) && (nPromptCardCount < nCount))
                    {
                        if (itmap2->first > 15)
                        {
                            itlist = itmap2->second.begin();
                            while (itlist != itmap2->second.end())
                            {
                                byPromptCardData[nPromptCardCount++] = *itlist;
                                itlist++;
                            }
                        }
                        itmap2++;
                    }
                }
                return TRUE;
            }
        }
        itci++;
    }
    
    if ((nOutCardCount <= 7) && (nKingCount == 4))
    {
        nPromptCardCount = 0;
        byPromptCardData[nPromptCardCount++] = 0x41;
        byPromptCardData[nPromptCardCount++] = 0x41;
        byPromptCardData[nPromptCardCount++] = 0x42;
        byPromptCardData[nPromptCardCount++] = 0x42;
        return TRUE;
    }
    return FALSE;
}

//提示出牌
int CGameLogicSK::GetAndroidOutCardData(BYTE byPromptCardData[], UINT& nPromptCardCount, const BYTE bySrcCardData[], const UINT nSrcCardCount,
                                        const BYTE byOutCardData[], const UINT nOutCardCount)
{
    if (nSrcCardCount == 0) return FALSE;
    
    map_cardtype mapCardType;
    int nOutCardType = 0;
    UINT i = 0;
    int nLogicValue = 0;
    typedef list<BYTE> list_carddata;
    map<int, list_carddata> mapCardData;
    map<int, list_carddata>::iterator itmap;
    map<int, list_carddata>::iterator itmap2;
    list_carddata lsCardData;
    list_carddata::iterator itlist;
    UINT nKingCount = 0;
    nPromptCardCount = 0;
    list<tagCardInfo> lsCardInfo;
    list<tagCardInfo>::iterator itci;
    
    //合并相同逻辑值的牌
    mapCardData.clear();
    for (i=0; i<nSrcCardCount; i++)
    {
        lsCardData.clear();
        if (bySrcCardData[i] == 0x00)
            continue;
        nLogicValue = GetLogicValue(bySrcCardData[i]);
        if (nLogicValue > 15)
        {
            nKingCount++;
        }
        itmap = mapCardData.find(nLogicValue);
        if (itmap == mapCardData.end())
        {
            lsCardData.push_back(bySrcCardData[i]);
            mapCardData[nLogicValue] = lsCardData;
            continue;
        }
        itmap->second.push_back(bySrcCardData[i]);
    }
    
    //按牌数量排序
    lsCardInfo.clear();
    tagCardInfo ci;
    if (mapCardData.size() >= 1)
    {
        itmap = mapCardData.begin();
        while (itmap != mapCardData.end())
        {
            if (itmap->first > 15)
            {
                itmap++;
                continue;
            }
            itci = lsCardInfo.begin();
            while (itci != lsCardInfo.end())
            {
                if (itci->nCardCount > itmap->second.size())
                {
                    ci.nLogicValue = itmap->first;
                    ci.nCardCount = (UINT)(itmap->second.size());
                    lsCardInfo.insert(itci, ci);
                    break;
                }
                itci++;
            }
            if (itci == lsCardInfo.end())
            {
                ci.nLogicValue = itmap->first;
                ci.nCardCount = (UINT)(itmap->second.size());
                lsCardInfo.push_back(ci);
            }
            itmap++;
        }
    }
    
    //机器人出牌
    if (nOutCardCount == 0)
    {
        nPromptCardCount = 0;
        if (lsCardInfo.size() >= 1)
        {
            itmap = mapCardData.find(lsCardInfo.begin()->nLogicValue);
            if (itmap != mapCardData.end())
            {
                itlist = itmap->second.begin();
                nPromptCardCount = 0;
                while (itlist != itmap->second.end())
                {
                    byPromptCardData[nPromptCardCount++] = *itlist;
                    itlist++;
                }
            }
        }
        if(nPromptCardCount==1 && lsCardInfo.size() >= 1)
        {
            if (mapCardData.size() >= 1)
            {
                itmap = mapCardData.begin();
                while (itmap != mapCardData.end())
                {
                    if(itmap->second.size() == 2 || itmap->second.size() == 3)
                    {
                        itlist = itmap->second.begin();
                        
                        BYTE cbTemp = * itlist;
                        
                        if((GetLogicValue(byPromptCardData[0]) > 8 && GetLogicValue(*itlist) < 8) || (GetLogicValue(byPromptCardData[0]) < 8 && GetLogicValue(*itlist) < 8 && rand()%100<50) || (GetLogicValue(byPromptCardData[0]) >13 &&  (GetLogicValue(*itlist) < 13)))
                        {
                            nPromptCardCount = 0;
                            ZeroMemory(byPromptCardData,sizeof(byPromptCardData));
                            while (itlist != itmap->second.end())
                            {
                                byPromptCardData[nPromptCardCount++] = *itlist;
                                itlist++;
                            }
                            return TRUE;
                        }
                    }
                    
                    itmap++;
                }
            }
        }
        if (lsCardInfo.size() > 1) return TRUE;
        if (lsCardInfo.size() == 1)
        {
            if ((nKingCount >= 3) && ((nPromptCardCount+nKingCount)<(7+nKingCount-3)))
                return TRUE;
        }
        if ((nKingCount > 0))
        {
            int nSallKingCount = 0;
            itmap2 = mapCardData.find(16);
            if (itmap2 != mapCardData.end())
            {
                nSallKingCount = (int)(itmap2->second.size());
                itlist = itmap2->second.begin();
                while (itlist != itmap2->second.end())
                {
                    byPromptCardData[nPromptCardCount++] = *itlist;
                    itlist++;
                }
            }
            int bAddBigKing = TRUE;
            if (nPromptCardCount == nSallKingCount)
            {
                if (nKingCount < 3)
                    bAddBigKing = FALSE;
            }
            if (bAddBigKing)
            {
                itmap2 = mapCardData.find(17);
                if (itmap2 != mapCardData.end())
                {
                    itlist = itmap2->second.begin();
                    while (itlist != itmap2->second.end())
                    {
                        byPromptCardData[nPromptCardCount++] = *itlist;
                        itlist++;
                    }
                }
            }
            return TRUE;
        }
        if (nPromptCardCount > 0)
            return TRUE;
        return FALSE;
    }
    
    //机器人压牌
    try
    {
        nOutCardType = GetCardType(byOutCardData, nOutCardCount, &mapCardType);
        switch ((SKEnumCardType)nOutCardType)
        {
            case SK_CT_1://单张
            case SK_CT_2://对子
            {
                if ((nSrcCardCount < nOutCardCount) || (mapCardData.rbegin()->first <= mapCardType.rbegin()->second.nLogicValue))
                {
                    i = 4;
                    throw i;
                }
                itci = lsCardInfo.begin();
                while (itci != lsCardInfo.end())
                {
                    if ((itci->nLogicValue > mapCardType.rbegin()->second.nLogicValue)
                        && (itci->nCardCount == nOutCardCount))
                    {
                        itmap = mapCardData.find(itci->nLogicValue);
                        if (itmap != mapCardData.end())
                        {
                            itlist = itmap->second.begin();
                            nPromptCardCount = 0;
                            while ((itlist != itmap->second.end()) && (nPromptCardCount < nOutCardCount))
                            {
                                byPromptCardData[nPromptCardCount++] = *itlist;
                                itlist++;
                            }
                            return TRUE;
                        }
                    }
                    itci++;
                }
                
                itmap = mapCardData.find(15);
                BYTE cbCard = byOutCardData[0];
                BYTE cbLogicValue = GetLogicValue(byOutCardData[0] );
                if (itmap != mapCardData.end() && cbLogicValue < 15 && itmap->second.size() > nOutCardCount)
                {
                    itlist = itmap->second.begin();
                    nPromptCardCount = 0;
                    while ((itlist != itmap->second.end()) && (nPromptCardCount < nOutCardCount))
                    {
                        byPromptCardData[nPromptCardCount++] = *itlist;
                        itlist++;
                    }
                    return TRUE;
                }
                
                
                itci = lsCardInfo.begin();
                while (itci != lsCardInfo.end())
                {
                    if ((itci->nLogicValue > mapCardType.rbegin()->second.nLogicValue)
                        && (itci->nCardCount > nOutCardCount) && itci->nCardCount < 4)
                    {
                        itmap = mapCardData.find(itci->nLogicValue);
                        if (itmap != mapCardData.end())
                        {
                            itlist = itmap->second.begin();
                            nPromptCardCount = 0;
                            while ((itlist != itmap->second.end()) && (nPromptCardCount < nOutCardCount))
                            {
                                byPromptCardData[nPromptCardCount++] = *itlist;
                                itlist++;
                            }
                            return TRUE;
                        }
                    }
                    itci++;
                }
                
                if(nSrcCardCount > 15 && rand()%100 < 50)
                {
                    return FALSE;
                }
                
                i = 4;
                throw i;
                
                break;
            }
            case SK_CT_3://三张
            {
                if ((nSrcCardCount < nOutCardCount) || (mapCardData.rbegin()->first <= mapCardType.rbegin()->second.nLogicValue))
                {
                    i = 4;
                    throw i;
                }
                itci = lsCardInfo.begin();
                while (itci != lsCardInfo.end())
                {
                    if ((itci->nLogicValue > mapCardType.rbegin()->second.nLogicValue)
                        && (itci->nCardCount >= nOutCardCount) && (itci->nCardCount < 4))
                    {
                        itmap = mapCardData.find(itci->nLogicValue);
                        if (itmap != mapCardData.end())
                        {
                            itlist = itmap->second.begin();
                            nPromptCardCount = 0;
                            while ((itlist != itmap->second.end()) && (nPromptCardCount < nOutCardCount))
                            {
                                byPromptCardData[nPromptCardCount++] = *itlist;
                                itlist++;
                            }
                            return TRUE;
                        }
                    }
                    itci++;
                }
                
                i = 4;
                throw i;
            }
                break;
            case SK_CT_1X:									//单连类型
            case SK_CT_2X:									//对连类型
            case SK_CT_3X:									//三连类型
            {
                if ((nSrcCardCount < nOutCardCount) || (mapCardData.rbegin()->first <= mapCardType.rbegin()->second.nLogicValue))
                {
                    i = 4;
                    throw i;
                }
                
                UINT nSingleCount = (nOutCardType-SK_CT_3);
                UINT nLineCount = nOutCardCount / nSingleCount;
                UINT nSearchCount = 0;
                itmap = mapCardData.begin();
                while ((itmap != mapCardData.end()) && (itmap->first <= 14))
                {
                    nSearchCount = 0;
                    if (itmap->first > mapCardType.rbegin()->second.nLogicValue)
                    {
                        itmap2 = itmap;
                        while ((itmap2 != mapCardData.end()) && (itmap2->first < (int)(itmap->first+nLineCount)))
                        {
                            if (itmap2->second.size() > nSingleCount)
                                nSearchCount += nSingleCount;
                            else
                                nSearchCount += (UINT)(itmap2->second.size());
                            itmap2++;
                        }
                        if ((nSearchCount+nKingCount) >= nOutCardCount)
                        {
                            itmap2 = itmap;
                            nPromptCardCount = 0;
                            while ((itmap2 != mapCardData.end()) && (itmap2->first < (int)(itmap->first+nLineCount)))
                            {
                                i = 0;
                                itlist = itmap2->second.begin();
                                while ((itlist != itmap2->second.end()) && (i < nSingleCount))
                                {
                                    byPromptCardData[nPromptCardCount++] = *itlist;
                                    itlist++;
                                    i++;
                                }
                                itmap2++;
                            }
                            if (nPromptCardCount < nOutCardCount)
                            {
                                itmap2 = itmap;
                                while ((itmap2 != mapCardData.end()) && (nPromptCardCount < nOutCardCount))
                                {
                                    if (itmap2->first > 15)
                                    {
                                        itlist = itmap2->second.begin();
                                        while (itlist != itmap2->second.end() && (nPromptCardCount < nOutCardCount))
                                        {
                                            byPromptCardData[nPromptCardCount++] = *itlist;
                                            itlist++;
                                        }
                                    }
                                    itmap2++;
                                }
                            }
                            return TRUE;
                        }
                    }
                    itmap++;
                }
                i = 4;
                throw i;
            }
                break;
            case SK_CT_4://炸弹
            case SK_CT_5:									//炸弹类型(5张)
            case SK_CT_6:									//6星硬炸
            case SK_CT_7:									//7星硬炸
            case SK_CT_8:									//8星硬炸
            case SK_CT_9:									//9星硬炸
            case SK_CT_10:									//10星硬炸
            {
                //寻找炸弹
                nPromptCardCount = 0;
                itci = lsCardInfo.begin();
                while (itci != lsCardInfo.end())
                {
                    if ((itci->nLogicValue > mapCardType.rbegin()->second.nLogicValue)
                        && ((itci->nCardCount) >= nOutCardCount))
                    {
                        itmap = mapCardData.find(itci->nLogicValue);
                        if (itmap != mapCardData.end())
                        {
                            itlist = itmap->second.begin();
                            nPromptCardCount = 0;
                            while (itlist != itmap->second.end())
                            {
                                byPromptCardData[nPromptCardCount++] = *itlist;
                                itlist++;
                            }
                            //是否为最后一项
                            if ((itci->nLogicValue == lsCardInfo.back().nLogicValue) && (nKingCount > 0) && (nKingCount < 3))
                            {
                                itmap2 = mapCardData.find(16);
                                if (itmap2 != mapCardData.end())
                                {
                                    itlist = itmap2->second.begin();
                                    while (itlist != itmap2->second.end())
                                    {
                                        byPromptCardData[nPromptCardCount++] = *itlist;
                                        itlist++;
                                    }
                                }
                                itmap2 = mapCardData.find(17);
                                if (itmap2 != mapCardData.end())
                                {
                                    itlist = itmap2->second.begin();
                                    while (itlist != itmap2->second.end())
                                    {
                                        byPromptCardData[nPromptCardCount++] = *itlist;
                                        itlist++;
                                    }
                                }
                            }
                            return TRUE;
                        }
                    }
                    itci++;
                }
                
                if ((nOutCardCount <= 7) && (nKingCount == 4))
                {
                    nPromptCardCount = 0;
                    byPromptCardData[nPromptCardCount++] = 0x41;
                    byPromptCardData[nPromptCardCount++] = 0x41;
                    byPromptCardData[nPromptCardCount++] = 0x42;
                    byPromptCardData[nPromptCardCount++] = 0x42;
                    return TRUE;
                }
                if ((nOutCardCount < 6) && (nKingCount == 3))
                {
                    nPromptCardCount = 0;
                    itmap2 = mapCardData.find(16);
                    if (itmap2 != mapCardData.end())
                    {
                        itlist = itmap2->second.begin();
                        while (itlist != itmap2->second.end())
                        {
                            byPromptCardData[nPromptCardCount++] = *itlist;
                            itlist++;
                        }
                    }
                    itmap2 = mapCardData.find(17);
                    if (itmap2 != mapCardData.end())
                    {
                        itlist = itmap2->second.begin();
                        while (itlist != itmap2->second.end())
                        {
                            byPromptCardData[nPromptCardCount++] = *itlist;
                            itlist++;
                        }
                    }
                    return TRUE;
                }
                
                //最后一项
                if (((lsCardInfo.back().nCardCount+nKingCount) > nOutCardCount)
                    || ((lsCardInfo.back().nLogicValue > mapCardType.rbegin()->second.nLogicValue)&&((lsCardInfo.back().nCardCount+nKingCount) >= nOutCardCount)))
                {
                    nPromptCardCount = 0;
                    itmap = mapCardData.find(lsCardInfo.back().nLogicValue);
                    if (itmap != mapCardData.end())
                    {
                        itlist = itmap->second.begin();
                        while (itlist != itmap->second.end())
                        {
                            byPromptCardData[nPromptCardCount++] = *itlist;
                            itlist++;
                        }
                    }
                    itmap = mapCardData.find(16);
                    if (itmap != mapCardData.end())
                    {
                        itlist = itmap->second.begin();
                        while (itlist != itmap->second.end())
                        {
                            byPromptCardData[nPromptCardCount++] = *itlist;
                            itlist++;
                        }
                    }
                    itmap = mapCardData.find(17);
                    if (itmap != mapCardData.end())
                    {
                        itlist = itmap->second.begin();
                        while (itlist != itmap->second.end())
                        {
                            byPromptCardData[nPromptCardCount++] = *itlist;
                            itlist++;
                        }
                    }
                    return TRUE;
                }
                
                i = (nOutCardCount+1);
                throw i;
            }
                break;
            case SK_CT_3K:	i = 6; throw i; break;					//三王炸弹类型
            case SK_CT_7L:	i = 7; throw i; break;					//7星连炸
            case SK_CT_4K: i = 8; throw i; break;					//天王炸弹
            case SK_CT_8L:	i = 8; throw i; break;					//8星连炸
            case SK_CT_9L:	i = 9; throw i; break;					//9星连炸
            case SK_CT_10L: i = 10; throw i; break;				//10星连炸
            case SK_CT_11L: i = 11; throw i; break;				//11星连炸
            case SK_CT_11:									//11星硬炸
            case SK_CT_12L:								//12星连炸
            case SK_CT_12:									//12星硬炸
            default: break;
        }
    }
    catch (UINT nCount)
    {
        if (nCount < 4) return FALSE;
        
        //寻找炸弹
        nPromptCardCount = 0;
        itci = lsCardInfo.begin();
        while (itci != lsCardInfo.end())
        {
            if ((itci->nCardCount) >= nCount)//+nKingCount
            {
                itmap = mapCardData.find(itci->nLogicValue);
                if (itmap != mapCardData.end())
                {
                    itlist = itmap->second.begin();
                    nPromptCardCount = 0;
                    while (itlist != itmap->second.end())
                    {
                        byPromptCardData[nPromptCardCount++] = *itlist;
                        itlist++;
                    }
                    //是否为最后一项
                    if ((itci->nLogicValue == lsCardInfo.back().nLogicValue) && (nKingCount > 0) && (nKingCount < 3))
                    {
                        itmap2 = mapCardData.find(16);
                        if (itmap2 != mapCardData.end())
                        {
                            itlist = itmap2->second.begin();
                            while (itlist != itmap2->second.end())
                            {
                                byPromptCardData[nPromptCardCount++] = *itlist;
                                itlist++;
                            }
                        }
                        itmap2 = mapCardData.find(17);
                        if (itmap2 != mapCardData.end())
                        {
                            itlist = itmap2->second.begin();
                            while (itlist != itmap2->second.end())
                            {
                                byPromptCardData[nPromptCardCount++] = *itlist;
                                itlist++;
                            }
                        }
                    }
                    return TRUE;
                }
            }
            itci++;
        }
        
        if ((nCount <= 7) && (nKingCount == 4))
        {
            nPromptCardCount = 0;
            byPromptCardData[nPromptCardCount++] = 0x41;
            byPromptCardData[nPromptCardCount++] = 0x41;
            byPromptCardData[nPromptCardCount++] = 0x42;
            byPromptCardData[nPromptCardCount++] = 0x42;
            return TRUE;
        }
        if ((nCount < 6) && (nKingCount == 3))
        {
            nPromptCardCount = 0;
            itmap2 = mapCardData.find(16);
            if (itmap2 != mapCardData.end())
            {
                itlist = itmap2->second.begin();
                while (itlist != itmap2->second.end())
                {
                    byPromptCardData[nPromptCardCount++] = *itlist;
                    itlist++;
                }
            }
            itmap2 = mapCardData.find(17);
            if (itmap2 != mapCardData.end())
            {
                itlist = itmap2->second.begin();
                while (itlist != itmap2->second.end())
                {
                    byPromptCardData[nPromptCardCount++] = *itlist;
                    itlist++;
                }
            }
            return TRUE;
        }
        //最后一项
        if (((lsCardInfo.back().nCardCount+nKingCount) >= nCount))
        {
            nPromptCardCount = 0;
            itmap = mapCardData.find(lsCardInfo.back().nLogicValue);
            if (itmap != mapCardData.end())
            {
                itlist = itmap->second.begin();
                while (itlist != itmap->second.end())
                {
                    byPromptCardData[nPromptCardCount++] = *itlist;
                    itlist++;
                }
            }
            itmap = mapCardData.find(16);
            if (itmap != mapCardData.end())
            {
                itlist = itmap->second.begin();
                while (itlist != itmap->second.end())
                {
                    byPromptCardData[nPromptCardCount++] = *itlist;
                    itlist++;
                }
            }
            itmap = mapCardData.find(17);
            if (itmap != mapCardData.end())
            {
                itlist = itmap->second.begin();
                while (itlist != itmap->second.end())
                {
                    byPromptCardData[nPromptCardCount++] = *itlist;
                    itlist++;
                }
            }
            return TRUE;
        }
        
        return FALSE;
    }
    catch (...)
    {
        nPromptCardCount = 0;
        return FALSE;
    }
    
    return FALSE;
}
