#ifndef _SKCCGAME_LOGICBASE_LAYER_H_
#define _SKCCGAME_LOGICBASE_LAYER_H_

#include <vector>
#include <map>
#include <list>
#include <algorithm>
using namespace std;

#ifndef _countof
#define _countof(array) (sizeof(array)/sizeof(array[0]))
#endif
#include "GameLayer.h"
class CGameLogicBaseSK
{
	typedef BYTE (*CallBackCustom)(const BYTE byCardData);
public:
	//排序方式
	enum enumSortMode
	{
		eNormal = 0,		//不排序
		eAscending,			//升序
		eAscendingColor,	//按花色升序
		eAscendingValue,	//按牌值升序
		eAscendingLogic,	//按逻辑升序
		eAscLogicValue,		//按逻辑值升序
		eDescending,		//降序
		eDescendingColor,	//按花色降序
		eDescendingValue,	//按牌值降序
		eDescendingLogic,	//按逻辑降序
		eDesLogicValue,		//按逻辑值降序
		eDescendingLogicCount,//逻辑值相同牌数量降序
		eSortModeCount
	};
	typedef vector<BYTE> vector_carddata;

private:
	//排序函数
	static bool fncmp(const BYTE& lhs, const BYTE& rhs);
	CallBackCustom m_pfnCustom;

public:
	CGameLogicBaseSK(void);
	virtual ~CGameLogicBaseSK(void);

	//通用函数
public:
	//洗牌
	void RandomCardData();
	//获取牌值
	virtual BYTE GetCardValue(const BYTE& byCardData);
	//获取牌花色
	virtual BYTE GetCardColor(const BYTE& byCardData);
	//获取牌花色
	virtual int GetCardLogic(const BYTE& byCardData);
	//获取逻辑值
	virtual int	GetLogicValue(const BYTE& byCardData);
	//逻辑值相同牌数量
	virtual UINT GetSameLogicCardCount(BYTE byCardData);
	//排序牌数据
	int SortCardData(enumSortMode eSortMode, const int nStart=-1, const int nEnd=-1);
	int SortCardData(enumSortMode eSortMode, vector_carddata& byCardData, const int nStart=-1, const int nEnd=-1);
	//获取牌数据
	int GetCardData(vector_carddata& byCardData, const int nStart=-1, const int nEnd=-1);
	int GetCardData(vector_carddata& vtCardDataDest, const vector_carddata& vtCardDataSrc, const int nStart=-1, const int nEnd=-1);
	//设置牌数据
	void SetCardData(const BYTE byAddCardData[], const UINT nAddCardCount);
	void SetCardData(vector_carddata& byCardData, const BYTE byAddCardData[], const UINT nAddCardCount);
	//添加牌数据
	void AddCardData(const BYTE byAddCardData[], const UINT nAddCardCount);
	void AddCardData(vector_carddata& byCardData, const BYTE byAddCardData[], const UINT nAddCardCount);
	//删除牌数据
	int RemoveCardData(const BYTE byRemoveCardData[], const UINT nRemoveCardCount);
	int RemoveCardData(vector_carddata& byCardData, const BYTE byRemoveCardData[], const UINT nRemoveCardCount);

	//特有函数
public:
	//复位牌数据
	virtual void ResetCardData() = 0;
	//获取最大牌数量
	virtual UINT GetMaxCardCount() = 0;
	//获取最大财神牌数量
	virtual UINT GetMaxMagicCardCount() = 0;
	//财神变换
	virtual int MagicCardData(const BYTE bySrcCardData[], const UINT nCardCount, BYTE byDestCardData[]) = 0;
	//获取牌型
	virtual int GetCardType(const BYTE byCardData[], const UINT nCardCount, void* pResult=NULL) = 0;
	//比较大小
	virtual int CompareCardData(const BYTE byFirstCardData[], const UINT nFirstCardCount,
		const BYTE bySecondCardData[], const UINT nSecondCardCount) = 0;//return (first>second);
	//获取类型描述
	virtual int GetTypeDescribe(int nCardType, string& strTypeDescribe, void* pResult=NULL) = 0;

protected:
	static CGameLogicBaseSK*	m_pGameLogic;//游戏逻辑类
	enumSortMode		m_eSortMode;			//牌排序方式
	vector_carddata			m_byCardData;			//牌数组
	vector_carddata			m_vtSortData;			//排序数组
};
#endif
