#ifndef _GameBANK_H_
#define _GameBANK_H_

#include "GameScene.h"
#include "ui/UIScrollview.h"
#include "cocos-ext.h"

//银行
USING_NS_CC_EXT;

class GameBank : public GameLayer,public TableViewDataSource, public TableViewDelegate
{
public:
    virtual bool init();
    static GameBank *create(GameScene *pGameScene);
    virtual ~GameBank();
    GameBank(GameScene *pGameScene);
    GameBank(const GameBank&);
    GameBank& operator = (const GameBank&);
    
    enum Tag
    {
        w100 = 0,
        w500,
        w1000,
        wAllGet,
        
        w5000,
        y1,
        clean,
        wAllSave,
        Tag_Back,
        Tag_KeepBtn,
        Tag_Save_Score,
        Tag_Save_GameName,
        Tag_Save_BankScore,
        Tag_Save_EdScore,
        Tag_Save_BtnSave,
        Tag_Get_BtnGet,
        Tag_Send_EdUserID,
        Tag_Send_NickName,
        Tag_Send_BankGold,
        Tag_Send_EdScore,
        Tag_Send_Ok,
        Tag_Send_NickBtn,
        Tag_Record_TitleLine,
        Tag_Modify_OldPsw,
        Tag_Modify_NewPsw,
        Tag_Modify_NewPsw1,
        Tag_Modify_Ok,
        Tag_RecordTable,
        Tag_Save_MoneyOp,
        Tag_WeChatok,
        Tag_WeCharCancel,
    };
public:
    TableView*	m_tableView;
    int			m_CurIndex;		//当前索引
    int			m_TagTitle;
    int			m_TagBankBack;
    int			m_TagBtn[4];	//底部按钮标签
    Vec2		m_BtnPos[4];	//底部按钮位置
    string		m_BtnName[4];	//底部按钮文件名
    string		m_BtnName1[4];	//底部按钮文件名
    LONGLONG	m_BankScore;
    LONGLONG	m_Score;
    
    //存取钱
    ui::EditBox*        m_Ed_Save_Trade;	//金币输入框
    
    //赠送
    ui::EditBox*        m_Ed_Send_UserID;	//用户输入框
    ui::EditBox*        m_Ed_Send_Trade;	//金币输入框
    
    //密码修改
    ui::EditBox*        m_Ed_OldPsw;	//旧密码
    ui::EditBox*        m_Ed_NewPsw;	//新密码
    ui::EditBox*        m_Ed_NewPsw1;	//重新输入新密码
    
    char				m_GameKindName[128];
    int					m_GameKindID;
    int					m_BtnClickIndex;
    int                 m_UserType;
    bool				m_bStartFlash;
private:
    string				m_NickNameText;
    UINT32              m_RecvUserID;
    LONGLONG            m_SendScore;
   
    Menu*               m_passBgmenu;
    Sprite*             m_passBg;
    ui::EditBox*        m_pass;
    string              m_passStr;
    ui::ScrollView*     m_gameList;
    Layer               *m_saveLayer;
    Layer               *m_sendLayer;
    Layer               *m_recordLayer;
    
public:
    void                initSaveLayer();
    void                initSendLayer();
    void                initRecordLayer();
    void OpenSave();
    void OpenSend();
    void OpenRecord();
    void ClearState(int _index);
    
    void OnSendSave();
    void OnSendGet();
    void OnSendSend(Ref *pSender);
    void OnClickSend();
    void OnSendNickName();
    void OnSendRecord();
    void OnSendModifyPassWord();
    void FlashSave(bool suc,int _index);
    void SetNickName(const char* nickname, Color3B color, int nType);
    void SetGameKindName(char* kindname, int kindid);
    bool AddGameKind(int _kind, Vec2 pos);
    bool AddGameKindTo(int _kind, Vec2 pos);
    void IsClick(int BtnTga);
    int GetGameKindBtnTga(int kindid);
    void SendSuc(LONGLONG lSwapScore, LONGLONG lRevenue, const char* time);
    void showPassLayer();
public:
    virtual void onEnter();
    virtual void onExit();
    virtual void DialogConfirm(Ref *pSender);
    virtual bool onTouchBegan(Touch *pTouch, Event *pEvent);
    virtual void onTouchEnded(Touch *pTouch, Event *pEvent);
    virtual void onTouchMoved(Touch *pTouch, Event *pEvent);
    
    virtual void callbackBt(Ref *pSender);
    virtual std::string getButtionNameByID(int id) { return UI_SEND_BT_NAME[id]; }
    virtual void initCell(TableViewCell *cell, ssize_t iCellIndex);
    
    virtual void scrollViewDidScroll(cocos2d::extension::ScrollView* view){}
    virtual void scrollViewDidZoom(cocos2d::extension::ScrollView* view){}
    virtual void tableCellTouched(TableView* table, TableViewCell* cell){}
    virtual ssize_t numberOfCellsInTableView(TableView *table);
    virtual cocos2d::Size tableCellSizeForIndex(TableView *table, ssize_t idx );
    virtual TableViewCell* tableCellAtIndex(TableView *table, ssize_t idx);
    
    Menu* CreateButton( std::string szBtName ,const Vec2 &p , int tag );
    Label * createLabel(const char *szText ,const Vec2 &p, int fontSize ,Color3B color, bool bChinese=false);
    Label * createLabel(const std::string szText ,const Vec2 &p, int fontSize ,Color3B color, bool bChinese=false);
    void close();
    void onRemove();
    
    void reloadData();
};
#endif
