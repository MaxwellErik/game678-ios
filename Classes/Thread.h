﻿#pragma once

#include "pthread.h"

//线程类
class CThread
{
public:
	CThread(void);
	~CThread(void);
	//启动线程
	int Start();
	//注意 结束调用它的线程
	void Stop();
	virtual void Run();
private:
	pthread_t m_thread; 
	static void* thread_funcation(void *arg);
};

