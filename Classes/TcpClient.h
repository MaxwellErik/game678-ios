#ifndef __TcpClient_h__
#define __TcpClient_h__

#include "../utils/GameBase.h"
#include "Packet.h"
#include "ODSocket.h"
#include "NetBase.h"
#include <iostream>
#include <thread>
#include <mutex>


enum SocketState
{
	eSocketConnecting,
	eSocketConnected,
	eSocketClosed
};

const INT MAX_RECV_BUF_SIZE = 61440;  //Ω” ’–≠“Èª∫≥Â«¯60k
const INT MAX_SEND_BUF_SIZE = 61440; //–≠“È∑¢ÀÕª∫≥Â«¯60k

class TcpClient
{
public:
	TcpClient(UINT clientId);
	virtual ~TcpClient();

	void connect(std::string ip, UINT port);
	void setGameServer(ISvrNetEvent *pNetEvent);
	void sendMessage(TCP_Buffer *ptr);		//∑¢ÀÕ–≠“È

	void update();
	void setHeartDelayTime(UINT delaytime);
	void close(bool bActive = false);  // «∑Ò÷˜∂Ø∂œø
    void onRecvData(const char* pdata, int dataLen);
    void onConnect(bool bSuccess);
    void onNetClose();
private:
	void connectRun(UINT clientId);	  
	void parseMessage();   //Ω‚Œˆ–≠“È£¨¥¶¿ÌΩ” ’ª∫≥Â«¯
	void recvMessage();   //Ω” ’Ω‚Œˆ
private:
	UINT _clientId;   //√øÃıtcp¡¨Ω”µƒŒ®“ªid
	UINT _port;       //∂Àø⁄
	std::string _ip;          //ip

	SocketState _socketState;   //Õ¯¬Á◊¥Ã¨
	ODSocket *_pOdsocket;
	ISvrNetEvent *_pNetEvent;
	std::thread *_pthread;
	std::mutex _mutex;

	char _msgRecvBuf[MAX_RECV_BUF_SIZE];

	UINT _recvBufLen;
	bool _bRecvMsg;

	TcpBufferList _sendBufList;
};


#endif