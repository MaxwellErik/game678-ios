﻿#include "LocalDataUtil.h"
#include "Tools.h"
#include "HXmlParse.h"
#include "GlobalUnits.h"

LocalDataUtil::LocalDataUtil(void)
{
}

LocalDataUtil::~LocalDataUtil(void)
{
}

void LocalDataUtil::SaveUserName( const char * szUserName, const char *szUserNameType )
{
	std::string szTempUserName = szUserName;
	Tools::SaveDataByKey(szUserNameType , szTempUserName);
}

const char * LocalDataUtil::GetUsername(char * szUserName, const char *szUserNameType)
{
	std::string szTempUserName;
	Tools::LoadDataByKey(szUserNameType , szTempUserName);
	strcpy(szUserName , szTempUserName.c_str());
	return szUserName;
}

void LocalDataUtil::SavePassword( const char * szPassword,  const char *szPasswordType )
{

	std::string szTempPassword = szPassword;
	for (int i=0; i<szTempPassword.length(); i++)
	{
		szTempPassword[i] ^= '@';
	}
	Tools::SaveDataByKey(szPasswordType , szTempPassword);
}

void LocalDataUtil::SavePasswordIsOk(bool IsSave, char *szIsSavePsdType)
{
	std::string szTempUserName;
	IsSave ?  szTempUserName="1":szTempUserName="0";
	Tools::SaveDataByKey(szIsSavePsdType , szTempUserName);
}


bool LocalDataUtil::GetPasswordIsOk(char *szIsSavePsdType)
{
	std::string szTempUserName;
	Tools::LoadDataByKey(szIsSavePsdType , szTempUserName);
	if (szTempUserName == "1")	return true;
	return false;
}


void LocalDataUtil::GetPassword(char * szPassword, const char *szPasswordType)
{
	std::string szTempPassword("");
	Tools::LoadDataByKey(szPasswordType , szTempPassword);
	for (int i=0; i<szTempPassword.length(); i++)
	{
		szTempPassword[i] ^= '@';
	}
	strcpy(szPassword , szTempPassword.c_str());
}

void LocalDataUtil::SaveSoundValue( float fd )
{
	std::string key = DATA_KEY_SETUP"SOUND_VALUE";
	UserDefault::getInstance()->setFloatForKey(key.c_str(), fd);
	UserDefault::getInstance()->flush();
}

float LocalDataUtil::GetSoundValue()
{
	std::string key = DATA_KEY_SETUP"SOUND_VALUE";
	return UserDefault::getInstance()->getFloatForKey(key.c_str(), 5.f);
}

void LocalDataUtil::SaveBackMusicValue( float fd )
{
	std::string key = DATA_KEY_SETUP"BACKMUSIC_VALUE";
	UserDefault::getInstance()->setFloatForKey(key.c_str(), fd);
	UserDefault::getInstance()->flush();
}

float LocalDataUtil::GetBackMusicValue()
{
	std::string key = DATA_KEY_SETUP"BACKMUSIC_VALUE";
	return UserDefault::getInstance()->getFloatForKey(key.c_str(), 5.f);
}

void LocalDataUtil::setObject( char *szKey ,const void *pData , unsigned short wDataSize )
{
	char *szTemp = new char[wDataSize];
	memset(szTemp , 0 , wDataSize);
	memcpy(szTemp , pData , wDataSize);
	UserDefault::getInstance()->setStringForKey(szKey, szTemp);
	UserDefault::getInstance()->flush();
}

void LocalDataUtil::getObject( char *szKey , void *pData )
{
	std::string szTemp="";
	UserDefault::getInstance()->getStringForKey(szKey , szTemp);
	memcpy(pData , szTemp.c_str() , szTemp.size());
}

void LocalDataUtil::readFileBuyRecord(DWORD dwUserID, std::vector<BuyRecordInfo> &vcRecord)
{
	char szBuf[100];
	sprintf(szBuf, "%lu_%s", dwUserID, DATA_KEY_BUYRECORD);
	std::string _path = FileUtils::getInstance()->getWritablePath() + szBuf;
	ssize_t size=0;
	unsigned char *buffer = FileUtils::getInstance()->getFileDataFromZip(_path.c_str(), "r", &size);
	if (size > 0 && buffer != NULL)
	{
		int iReadSize = 0;
		while (iReadSize < size)
		{
			BuyRecordInfo info;
			ZeroMemory(&info , sizeof(info));
			CopyMemory(&info , buffer+iReadSize, sizeof(BuyRecordInfo));
			iReadSize += sizeof(BuyRecordInfo);
			vcRecord.push_back(info);
		}
	}
	bool bLimit = false;//超过限制
	while(vcRecord.size() > 50)
	{
		vcRecord.erase(vcRecord.begin());
		bLimit = true;
	}
	if (bLimit)
	{
		for (int i=0; i<vcRecord.size(); i++)
		{
			writeBuyRecord(dwUserID, &vcRecord[i], i==0);
		}
	}
}

bool LocalDataUtil::readFile( char *szFileName , void *pData , ssize_t &size )
{
	std::string _path = FileUtils::getInstance()->getWritablePath() + szFileName;
	unsigned char *buffer = FileUtils::getInstance()->getFileDataFromZip(_path.c_str(), "rb", &size);
	if (size > 0 && buffer != NULL)
	{
		CopyMemory(pData, buffer, size);
		return true;
	}
	else
	{
		return false;
	}
}

void LocalDataUtil::writeFile(char *szFileName , void *pData , WORD wDataSize, bool bNew)
{
	
	std::string _path = FileUtils::getInstance()->getWritablePath() + szFileName;
	FILE *pFile;// = fopen(_path.c_str(), "a");
	if (bNew)
	{
		pFile = fopen(_path.c_str(), "wb");
	}
	else
	{
		pFile = fopen(_path.c_str(), "ab+");
	}
	if (pFile != NULL)
	{
		if (wDataSize > 0)
		{
			fwrite(pData,wDataSize,1,pFile);
		}		
		fclose(pFile);
	}
}

void LocalDataUtil::writeBuyRecord(DWORD dwUserID, BuyRecordInfo *info, bool bNew)
{
	char szBuf[100];
	sprintf(szBuf, "%lu_%s", dwUserID, DATA_KEY_BUYRECORD);
	writeFile(szBuf , info , sizeof(BuyRecordInfo), bNew);
}

void LocalDataUtil::SaveUserId( DWORD dwUserID )
{
	char szUserID[32]={0};
	sprintf(szUserID , "%ld" , dwUserID);
	std::string szTempUserID = szUserID;
	Tools::SaveDataByKey(DATA_KEY_GUEST_USERID , szTempUserID);
}

DWORD LocalDataUtil::GetUserID()
{
	std::string szTempUserID("");
	Tools::LoadDataByKey(DATA_KEY_GUEST_USERID , szTempUserID);
	return atol(szTempUserID.c_str());
}

void LocalDataUtil::SaveSetup( int iIndex , bool bCheck )
{
	std::string key = DATA_KEY_SETUP;
	UserDefault::getInstance()->setBoolForKey(key.c_str(), bCheck);
	UserDefault::getInstance()->flush();
}

bool LocalDataUtil::GetSetup( int iIndex )
{
	std::string key = DATA_KEY_SETUP;
	if (iIndex == 0)
	{
		return UserDefault::getInstance()->getBoolForKey(key.c_str(), false);
	}
	else
	{
		return UserDefault::getInstance()->getBoolForKey(key.c_str(), true);
	}

}