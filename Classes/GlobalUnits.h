#ifndef GLOBAL_UNITS_HEAD_FILE
#define GLOBAL_UNITS_HEAD_FILE


//////////////////////////////////////////////////////////////////////////
#include "cocos2d.h"

#include "def.h"
#include "CMD_Mobile.h"
#include "CMD_Plaza.h"
#include "ServerListManager.h"

#define             AREA1                   "fsl."
#define             AREA2                   "fzl."
#define             AREA3                   "hbl."
#define             AREA4                   "hnl."
#define             AREA5                   "xgl."

#define				LOGIN_IP1               "678game.cc"
#define				LOGIN_IP2               "678game.co"
#define				LOGIN_IP3               "678game.me"

#define				IOS_PORT_START          (8301)
#define				IOS_PORT_END            (8309)

#define				ANDROID_PORT_STRTR		(8301)
#define				ANDROID_PORT_END		(8309)

#define             PLATFORM_ANDROID        2
#define             PLATFORM_APPLE          1
#define             PLATFORM_PC             0
#define             PWDHEAD                 "Ga^mE6#7@8.C%s"
struct UserData
{
public:
	UserData()
	{
		ZeroMemory(szAccounts,NAME_LEN);
		ZeroMemory(pszPassword,PASS_LEN);
		ZeroMemory(WxPassword,PASS_LEN);
		isWxUser = false;
	}
	char	szAccounts[NAME_LEN];	
	char	pszPassword[PASS_LEN];
	char    WxPassword[PASS_LEN];
	bool	isWxUser;
    char	pszMD5Password[PASS_LEN];   //YUNG
    bool    isTourist;
};

//全局用户资料
struct tagGlobalUserData
{
	WORD								wFaceID;						//头像索引
	WORD								wGender;						//用户性别
	BYTE								cbMember;						//会员等级
	UINT32                              dwUserID;						//用户 I D
	UINT32                              dwGameID;						//游戏 I D
	UINT32                              dwExperience;					//用户经验
	UINT32                              dwLoveliness;					//魅力值
	CHAR								szAccounts[NAME_LEN];			//登录帐号
	CHAR								szNickName[NAME_LEN];			//用户昵称
	CHAR								szPassWord[PASS_LEN];			//登录密码
	CHAR								szGroupName[GROUP_LEN];			//社团信息
	CHAR								szUnderWrite[UNDER_WRITE_LEN];	//个性签名
	BYTE								cbOpenPlat;						//开放平台
	char								szOpenID[OPEN_LEN];				//openID
    double                              fLastLogonTime;                 //登录时间
    LONGLONG							lWebToken;                      //网页验证
	
    //扩展信息
	UINT32                              dwCustomFaceVer;				//头像版本
	BYTE								cbMoorMachine;					//锁定机器
	CHAR								szUserToken[PASS_LEN];			//用户TOKEN

	LONGLONG							lScore;
	LONGLONG							lInsureScore;
	LONGLONG							lSilverScore;
	LONGLONG							lInsureSilverScore;
	LONGLONG							lIngot;
	LONGLONG							lLoveliness;					//魅力值
	INT									iGiftTicket;					//礼券
    WORD                                wLogonType;                     //用户类型 0、普通用户 1、游客
    BYTE                                bBindWeChat;                    //绑定微信
    BYTE                                cbBanker;                       //是否商人
};


#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
struct BuyRecordInfo
{
	WORD		wBuyID;			//充值编号
	BYTE		cbType;			//类型
	BYTE		cbStatus;		//充值状态(0可补单 1为已完成 2为未付款)
	int         lScore;			//充值分数
	int         lMoney;			//人民币
	TCHAR		szTime[32];		//时间
	TCHAR		szNum[40];		//订单号
	TCHAR		szJson[4096];	//苹果返回的Json数据
};
#else
struct BuyRecordInfo
{
	WORD		wBuyID;			//充值编号
	BYTE		cbType;			//类型
	BYTE		cbStatus;		//充值状态(0可补单 1为已完成)
	int         lScore;			//充值分数
	int         lMoney;			//人民币
	TCHAR		szTime[32];		//时间
	TCHAR		szNum[40];		//订单号
};
#endif

struct SysMsg
{
public:
	SysMsg()
	{
		ZeroMemory(szText,1024);
	}
	char szText[1024];
};

typedef std::vector<TransPropRecord> VectorTransPropRecord;

typedef std::vector<BuyRecordInfo> VectorBuyRecordInfo;

typedef std::vector<tagMBLogTransfer> VectorSendRecord;

class CGlobalUnits
{
public:
	CGlobalUnits(void);

public:
	tagGlobalUserData					m_GlobalUserData;
	VectorTransPropRecord				m_vcTransPropRecord;
	float								m_fSoundValue;			//声音大小
	float								m_fBackMusicValue;
	bool								m_bContralCheck[3];		//第一个是震动 第二个是压线提示
	VectorBuyRecordInfo					m_vcGoldRecord;
	VectorSendRecord					m_SendRecordVec;
public:
	CServerListManager					m_ServerListManager;			//列表管理
	vector<UserData>					m_UserDataVec;
    vector<CMD_GP_ForwardServerResult>  m_ForwardServerList;
	bool								m_bSingleGame;					//是否单机游戏
	bool								m_bIsMePlaying;                 //自己是否在游戏中
	bool								m_bNoviceTeaching;
	WORD								m_wStep;
	bool								m_bShowNoMoney;					//是否显示过筹码不足框
	int									m_iFreeReceiveCount;			//单机游戏每日赠送次数
    int                                 m_HeartCount;                    //心跳计数
	bool								m_bLink;						//是否连接
	bool								m_bPraise;						//是否好评
	tagUserData*						m_pUserItem[MAX_CHAIR];			//用户信息
	WORD								m_wChairCount;					//椅子数量
	WORD								m_wTableCount;					//桌子数量
	WORD								m_wMeChair;
	vector<SysMsg>						m_SysMsg;
	bool								m_bCheckPhone;					//是否验证过手机
	UserData							m_UserLoginInfo;				//记录登录前的账号信息
	bool								m_IsLogin;
	char								m_BankPassWord[33];
	bool								m_LogonBank_IsOk;               //此次登录游戏，是否进过保险箱，进过无需输入密码
    bool								m_IsBankOpen;                   //银行是否已打款
	char								m_SignName[128];
	std::string							m_IP;							 //自动选择IP
	int                                 m_SwitchYZ;                      //苹果验证开关
    UINT32                              m_YZPASS;                        //苹果验证密码
    int                                 m_IPIndex;
	unsigned short						m_ServerPort;
    bool                                m_bLeaveGameByServer;            //服务器踢出玩家
    UINT32                              m_boradcastTime;                  //上次广播时间
    bool                                m_isLoadLayerShow;                //loading界面是否显示
    int                                 m_ChannelID;                      //渠道号
    BYTE                                m_FreeAccount;                      //1：帐号有资格领取
    BYTE                                m_FreeWeixin;                       //1：微信有资格领取
    
	//获取用户信息
	tagGlobalUserData & GetGolbalUserData() { return m_GlobalUserData; }
	IosDword GetUserID() { return GetGolbalUserData().dwUserID; }
	LONGLONG GetScore() { return GetGolbalUserData().lScore; }

public:
	void InsertUserData(UserData data);
	void SaveUserData();

	void AddBuyRecord(IosDword dwUserID, WORD wID , BYTE cbType, TCHAR *szNum, int lScore = 0, int lMoney = 0);	//添加购买记录
	bool OrderSuccess(const char *szOrderNum , bool bCheck);					//购买记录
	void SaveBuyRecord();														//保存购买记录
	void AddBuyRecordJson(const char *szOrderNum, const char *szJson);			//添加购买记录的Json

	void loadBuyRecord();
	void cleanAllBuyRecord();

	WORD SwitchViewChairID(WORD wChairID);
	bool SetUserInfo(WORD wChairID, tagUserData * pUserItem);
	const tagUserData* GetUserInfo(WORD wChairID);
	int GetFaceID(LONGLONG lscore);
    string getFace(BYTE gender, LONGLONG lscore);
};

//////////////////////////////////////////////////////////////////////////

//全局信息
extern	CGlobalUnits					g_GlobalUnits;					//信息组件

//////////////////////////////////////////////////////////////////////////

#endif
