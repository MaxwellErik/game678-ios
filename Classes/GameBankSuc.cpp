#include "GameBankSuc.h"
#include "LobbySocketSink.h"
#include "Encrypt.h"
#include "SoundUtil.h"
GameBankSuc::GameBankSuc( GameScene *pGameScene ):GameLayer(pGameScene)
{
	m_BackSpr = NULL;
}
GameBankSuc::~GameBankSuc()
{

}

GameBankSuc* GameBankSuc::create(GameScene *pGameScene)
{
	GameBankSuc* temp = new GameBankSuc(pGameScene);
	if(temp && temp->init())
	{
		temp->autorelease();
		return temp;
	}
	else
	{
		CC_SAFE_DELETE(temp);
		return NULL;
	}
}

bool GameBankSuc::init()
{
	if ( !Layer::init() )	return false;
	setLocalZOrder(10);

    m_BackSpr = ui::Scale9Sprite::createWithSpriteFrameName("bg_tongyongkuang2.png");
    m_BackSpr->setContentSize(Size(1050,800));
	m_BackSpr->setPosition(_STANDARD_SCREEN_CENTER_IN_PIXELS_);
	addChild(m_BackSpr);

    Sprite* title = Sprite::createWithSpriteFrameName("zengsongxiangqing.png");
    title->setPosition(Vec2(525, 740));
    m_BackSpr->addChild(title);
    
	Menu* pNode = CreateButton("btn_queding", Vec2(_STANDARD_SCREEN_CENTER_IN_PIXELS_.x,230), _BtnOk);
	addChild(pNode ,0 , _BtnOk);

	setTouchEnabled(true);
    auto listener = EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(true);
    listener->onTouchBegan = CC_CALLBACK_2(GameBankSuc::onTouchBegan, this);
    listener->onTouchMoved = CC_CALLBACK_2(GameBankSuc::onTouchMoved, this);
    listener->onTouchEnded = CC_CALLBACK_2(GameBankSuc::onTouchEnded, this);
    Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(listener, this);
	return true;
}

int GameBankSuc::count(int input)   //计算数字的长度 
{
int output=0;
if(input==0)
return 1;
 while(input > 0)
{
input/=10;
 output++;
}
return output;
}


string GameBankSuc::change(LONGLONG num)//将每四个数字组转化为仟佰拾
{
    string str="";
    
    int count = 0;
    int level = 0;
    while (num > 0) {
        int curNum = num % 10;
        num /= 10;
        count++;
        if (count == 5)
        {
            count = 1;
            level++;
        }
        
        switch(curNum)
        {
         case 0:str="零"+str;break;
         case 1:str="壹"+str;break;
         case 2:str="贰"+str;break;
         case 3:str="叁"+str;break;
         case 4:str="肆"+str;break;
         case 5:str="伍"+str;break;
         case 6:str="陆"+str;break;
         case 7:str="柒"+str;break;
         case 8:str="捌"+str;break;
         case 9:str="玖"+str;break;
        }
        
        if(count == 2)
        {
            str="拾"+str;
        }
        else if(count == 3)
        {
            str="佰"+str;
        }
        else if(count == 4)
        {
            str="仟"+str;
        }
        
        if (level == 1)
        {
             str="万"+str;
        }
        else if (level == 2)
        {
             str="亿"+str;
        }
        else if (level == 3)
        {
             str="兆"+str;
        }
    }
    return str;
}


string GameBankSuc::all_lj(LONGLONG num)//再将每个 数字组合并为万亿兆的形式 
{
 string str="";
 for(int i=0;num!=0;i++)
 {
  int temp=num%10000;
  num/=10000;
  str=change(temp)+str;
  if(i==0&&num!=0)
  str="万"+str;
  else if(i==1&&num!=0) 
  str="亿"+str;
  else if(i==2&&num!=0)
  str="兆"+str;
 }
 return str;
}

void GameBankSuc::SetData(int srcID, const char* scrName,int desID, const char* desName,LONGLONG score, LONGLONG lRevenue, const char * time)
{
	if(m_BackSpr == NULL) return;

    Sprite* zhang = Sprite::createWithSpriteFrameName("yinzhang.png");
    zhang->setScale(2.5f);
    zhang->setAnchorPoint(Vec2(1, 0));
    zhang->setPosition(Vec2(1000, 50));
    m_BackSpr->addChild(zhang);
    
    Label *sendName = Label::createWithSystemFont("赠送者昵称: ", _GAME_FONT_NAME_1_, 46);
    sendName->setAnchorPoint(Vec2(1, 0.5f));
    sendName->setPosition(Vec2(525, 640));
    m_BackSpr->addChild(sendName);
	//赠送人id
	char TempSrc[128];
	sprintf(TempSrc,"%s", scrName);
	Label *font=Label::createWithSystemFont(TempSrc,_GAME_FONT_NAME_1_,46);
	font->setAnchorPoint(Vec2(0,0.5f));
	font->setPosition(Vec2(525,640));
	m_BackSpr->addChild(font);

    Label *sendID = Label::createWithSystemFont("赠送者游戏ID: ", _GAME_FONT_NAME_1_, 46);
    sendID->setAnchorPoint(Vec2(1, 0.5f));
    sendID->setPosition(Vec2(525, 580));
    m_BackSpr->addChild(sendID);
    
	sprintf(TempSrc,"%d", srcID);
	font=Label::createWithSystemFont(TempSrc,_GAME_FONT_NAME_1_,46);
	font->setAnchorPoint(Vec2(0,0.5f));
	font->setPosition(Vec2(525,580));
	m_BackSpr->addChild(font);

    Label *recvName = Label::createWithSystemFont("接收者昵称: ", _GAME_FONT_NAME_1_, 46);
    recvName->setAnchorPoint(Vec2(1, 0.5f));
    recvName->setPosition(Vec2(525, 520));
    m_BackSpr->addChild(recvName);
    
	sprintf(TempSrc,"%s", desName);
	font=Label::createWithSystemFont(TempSrc,_GAME_FONT_NAME_1_,46);
	font->setAnchorPoint(Vec2(0,0.5f));
	font->setPosition(Vec2(525,520));
	m_BackSpr->addChild(font);

    Label *recvID = Label::createWithSystemFont("接收者游戏ID: ", _GAME_FONT_NAME_1_, 46);
    recvID->setAnchorPoint(Vec2(1, 0.5f));
    recvID->setPosition(Vec2(525, 460));
    m_BackSpr->addChild(recvID);
    
	sprintf(TempSrc,"%d", desID);
	font=Label::createWithSystemFont(TempSrc,_GAME_FONT_NAME_1_,46);
	font->setAnchorPoint(Vec2(0,0.5f));
	font->setPosition(Vec2(525,460));
	m_BackSpr->addChild(font);
    
    Label *scoreLabel = Label::createWithSystemFont("赠送酒吧豆: ", _GAME_FONT_NAME_1_, 46);
    scoreLabel->setAnchorPoint(Vec2(1, 0.5f));
    scoreLabel->setPosition(Vec2(525, 400));
    m_BackSpr->addChild(scoreLabel);
    
	Tools::AddComma(score,TempSrc);
	font=Label::createWithSystemFont(TempSrc,_GAME_FONT_NAME_1_,46);
	font->setAnchorPoint(Vec2(0,0.5f));
	font->setPosition(Vec2(525,400));
	m_BackSpr->addChild(font);
    
    Label *bigLabel = Label::createWithSystemFont("赠送税费: ", _GAME_FONT_NAME_1_, 46);
    bigLabel->setAnchorPoint(Vec2(1, 0.5f));
    bigLabel->setPosition(Vec2(525, 340));
    m_BackSpr->addChild(bigLabel);
    
	char NumStr[128];
	sprintf(NumStr,"%lld",lRevenue);
	font=Label::createWithSystemFont(NumStr,_GAME_FONT_NAME_1_,46);
	font->setAnchorPoint(Vec2(0,0.5f));
	font->setPosition(Vec2(525,340));
	m_BackSpr->addChild(font);
    
    Label *tscoreLabel = Label::createWithSystemFont("实收酒吧豆: ", _GAME_FONT_NAME_1_, 46);
    tscoreLabel->setAnchorPoint(Vec2(1, 0.5f));
    tscoreLabel->setPosition(Vec2(525, 280));
    m_BackSpr->addChild(tscoreLabel);
    
    sprintf(TempSrc,"%lld", (score - lRevenue));
    font=Label::createWithSystemFont(TempSrc,_GAME_FONT_NAME_1_,46);
    font->setAnchorPoint(Vec2(0,0.5f));
    font->setPosition(Vec2(525,280));
    m_BackSpr->addChild(font);
    
    Label *timeL = Label::createWithSystemFont("赠送时间: ", _GAME_FONT_NAME_1_, 46);
    timeL->setAnchorPoint(Vec2(1, 0.5f));
    timeL->setPosition(Vec2(525, 220));
    m_BackSpr->addChild(timeL);
    
    font=Label::createWithSystemFont(time, _GAME_FONT_NAME_1_,46);
    font->setAnchorPoint(Vec2(0,0.5f));
    font->setPosition(Vec2(525,220));
    m_BackSpr->addChild(font);
}

void GameBankSuc::close()
{
	m_pGameScene->removeChild(this);
}

void GameBankSuc::onRemove()
{
	m_pGameScene->removeChild(this);
}

void GameBankSuc::onEnter()
{
	GameLayer::onEnter();
}

void GameBankSuc::onExit()
{
	GameLayer::onExit();
}

void GameBankSuc::callbackBt( Ref *pSender )
{
	Node *pNode = (Node *)pSender;
	switch (pNode->getTag())
	{
	case _BtnOk:	//确定按钮
		{
			SoundUtil::sharedEngine()->playEffect("buttonMusic");
			onRemove();
			break;
		}
	case _BtnCancel:	//取消按钮
		{
			SoundUtil::sharedEngine()->playEffect("buttonMusic");
			onRemove();
			break;
		}
	}
}

Menu* GameBankSuc::CreateButton( std::string szBtName ,const Vec2 &p , int tag )
{
	Menu *pBT = Tools::Button(StrToChar(szBtName+"_normal.png") , StrToChar(szBtName+"_click.png") , p, this , menu_selector(GameBankSuc::callbackBt) , tag);
	return pBT;
}
