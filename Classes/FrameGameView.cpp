﻿#include "FrameGameView.h"
#include "ClientSocketSink.h"
#include "gameShowhand/ShGameViewLayer.h"
#include "LobbySocketSink.h"
#include "LoginLayer.h"
#include "GameLayerMove.h"
#include "Convert.h"

int FrameGameView::m_iReconnectCount = 0;
BYTE FrameGameView::m_cbGameStatus = 0;

#define				FLAG_SIZE							(CCSizeMake(1829, 878))

FrameGameView::FrameGameView(GameScene *pGameScene):GameLayer(pGameScene)
{
	m_pTopLayer = NULL;
	m_KindId = 0;
}


FrameGameView::~FrameGameView(void)
{
}

int FrameGameView::ShowMessageBox(const char * pszMessage, UINT nType )
{
	return 0;
}

const tagUserData * FrameGameView::GetUserData(int chair)
{
	tagUserData * p = ClientSocketSink::sharedSocketSink()->GetMeUserData();
	if(p == NULL)return NULL;
	return ClientSocketSink::sharedSocketSink()->m_UserManager.GetUserData(p->wTableID, chair);
}

bool FrameGameView::SendData(WORD wSubCmdID )
{
	if (!g_GlobalUnits.m_bSingleGame) displayConnect();
	return ClientSocketSink::sharedSocketSink()->SendData(MDM_GF_GAME , wSubCmdID);
}

bool FrameGameView::SendData(WORD wSubCmdID, void * pData, WORD wDataSize )
{
	if (!g_GlobalUnits.m_bSingleGame) displayConnect();
	return ClientSocketSink::sharedSocketSink()->SendData(MDM_GF_GAME , wSubCmdID , pData , wDataSize);
}

void FrameGameView::OnEventUserEnter( tagUserData * pUserData, WORD wChairID, bool bLookonUser )
{

}

void FrameGameView::OnEventUserLeave( tagUserData * pUserData, WORD wChairID, bool bLookonUser )
{

}

void FrameGameView::OnEventUserScore( tagUserData * pUserData, WORD wChairID, bool bLookonUser )
{

}

void FrameGameView::OnEventUserStatus( tagUserData * pUserData, WORD wChairID, bool bLookonUser )
{

}

void FrameGameView::OnEventUserChangeTable( tagUserData * pUserData, WORD wChairID, bool bLookonUser )
{
    
}

bool FrameGameView::OnGameMessage( WORD wSubCmdID, const void * pBuffer, WORD wDataSize )
{
	return true;
}


bool FrameGameView::OnFrameMessage( WORD wSubCmdID, const void * pBuffer, WORD wDataSize )
{
	switch (wSubCmdID)
	{
	case SUB_CM_SYSTEM_MESSAGE:
		{
			CMD_CM_SystemMessage_MB* temp = (CMD_CM_SystemMessage_MB*)pBuffer;
			if (temp->wType&SMT_CLOSE_ROOM)
			{
                AlertMessageLayer::createConfirm(this,gbk_utf8(temp->szString).c_str(),
                                                 menu_selector(IGameView::backLoginView));
			}
			break;
		}
	case SUB_GF_SYSTEM_MESSAGE: //系统消息
		{
			CMD_CM_SystemMessage_MB* temp = (CMD_CM_SystemMessage_MB*)pBuffer;
			if (temp->wType&SMT_CLOSE_GAME)
			{
				AlertMessageLayer::createConfirm(this,gbk_utf8(temp->szString).c_str(),
					menu_selector(IGameView::backLoginView));
			}
			else if (temp->wType&SMT_EJECT)
			{
				AlertMessageLayer::createConfirm(this,temp->szString);
			}
			break;
		}
	case SUB_GF_MESSAGE:
		{
           
			CMD_GF_Message *RoomOption = (CMD_GF_Message *)pBuffer;
            if (RoomOption->wMessageType & SMT_CLOSE_GAME)
			{
                g_GlobalUnits.m_bLeaveGameByServer = true;
				AlertMessageLayer::createConfirm(this, gbk_utf8(RoomOption->szContent).c_str(),
					menu_selector(IGameView::backLoginView));
			}
            else if (RoomOption->wMessageType & SMT_EJECT)
            {
                AlertMessageLayer::createConfirm(this, gbk_utf8(RoomOption->szContent).c_str());
            }
            
			break;
		}
	case SUB_GR_FIRST_INGOT:
		break;
	case SUB_GR_ROOM_OPTION:
		{
			CMD_CF_RoomOption *RoomOption = (CMD_CF_RoomOption *)pBuffer;
			m_lCellScore = RoomOption->lCellScore;
			m_wServerType = RoomOption->wServerType;
			return true;
		}
	case SUB_GF_OPTION:
		{
			//效验参数
			CC_ASSERT(wDataSize==sizeof(CMD_GF_Option));
			if (wDataSize!=sizeof(CMD_GF_Option)) return false;

			//消息处理
			CMD_GF_Option * pOption=(CMD_GF_Option *)pBuffer;
			m_cbGameStatus=pOption->bGameStatus;
			return true;
		}
	case SUB_GF_SCENE:
		{
			//removeConnect();
			//removeconnect();
			m_iReconnectCount = 0;
			return OnGameSceneMessage(m_cbGameStatus, pBuffer, wDataSize);
		}
	case SUB_GF_NOTICE:
		{
			//效验参数
			CMD_GF_UserChat * pUserChat=(CMD_GF_UserChat *)pBuffer;
			ASSERT(wDataSize>=(sizeof(CMD_GF_UserChat)-sizeof(pUserChat->szChatMessage)));
			ASSERT(wDataSize==(sizeof(CMD_GF_UserChat)-sizeof(pUserChat->szChatMessage)+pUserChat->wChatLength));
			if (wDataSize<(sizeof(CMD_GF_UserChat)-sizeof(pUserChat->szChatMessage))) return false;
			if (wDataSize!=(sizeof(CMD_GF_UserChat)-sizeof(pUserChat->szChatMessage)+pUserChat->wChatLength)) return false;

			AddTopString(pUserChat->szChatMessage);
			
			return true;
		}
	}
	return true;
}

bool FrameGameView::OnGameSceneMessage( BYTE cbGameStatus, const void * pBuffer, WORD wDataSize )
{
	return true;
}

const tagUserData * FrameGameView::GetMeUserData()
{
	return ClientSocketSink::sharedSocketSink()->GetMeUserData();
}

WORD FrameGameView::GetMeChairID()
{
	return ClientSocketSink::sharedSocketSink()->GetMeUserData()->wChairID;
}

void FrameGameView::SendGameScene( BYTE cbScene )
{
	//CMD_C_NextScene NextScene;
	//ZeroMemory(&NextScene , sizeof(NextScene));
	//NextScene.cbScene = cbScene;
	//SendData(SUB_C_NEXTSCENE , &NextScene , sizeof(NextScene));
}

bool FrameGameView::SendMobileData( WORD wSubCmdID )
{
	if (!g_GlobalUnits.m_bSingleGame) displayConnect();
	switch (wSubCmdID)
	{
	case SUB_C_GP_RECEIVETASKPRIZE:
	case SUB_C_GP_SENDPROP:
		{
			return ClientSocketSink::sharedSocketSink()->SendData(MDM_GP_MOBILE , wSubCmdID);
		}
	default:
		{
			LobbySocketSink::sharedSocketSink()->setRcvLayer(this);
			return LobbySocketSink::sharedSocketSink()->SendMobileData(wSubCmdID);
		}
	}
}

bool FrameGameView::SendMobileData( WORD wSubCmdID, void * pData, WORD wDataSize )
{
	if (!g_GlobalUnits.m_bSingleGame) displayConnect();
	switch (wSubCmdID)
	{
	case SUB_C_GP_RECEIVETASKPRIZE:
	case SUB_C_GP_SENDPROP:
		{
			return ClientSocketSink::sharedSocketSink()->SendData(MDM_GP_MOBILE , wSubCmdID , pData , wDataSize);
		}
	default:
		{
			LobbySocketSink::sharedSocketSink()->setRcvLayer(this);
			return LobbySocketSink::sharedSocketSink()->SendMobileData(wSubCmdID , pData , wDataSize);
		}
	}
}

void FrameGameView::delayConnect( float fd )
{
	removeConnect();
	LoadLayer::create(m_pGameScene);
	scheduleOnce(schedule_selector(FrameGameView::connectWaitlong) , REMOVE_CONNECT_TIMEOUT);
	if (!ClientSocketSink::sharedClientSocket()->IsConnect())
	{
		reconnect();
	}
}

void FrameGameView::displayConnect()
{
	removeConnect();
	scheduleOnce(schedule_selector(FrameGameView::delayConnect) , CONNECT_TIMEOUT);
}

void FrameGameView::removeConnect()
{
	unschedule(schedule_selector(FrameGameView::delayConnect));
	unschedule(schedule_selector(FrameGameView::connectWaitlong));
	LoadLayer::removeLoadLayer(m_pGameScene);
}

void FrameGameView::connectWaitlong(float fd)
{
	unschedule(schedule_selector(FrameGameView::connectWaitlong));
	removeConnect();
	AlertMessageLayer::createConfirm(_UNICODE_CONNECT_FAILT);	//网络连接失败
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    ClientSocketSink::sharedClientSocket()->CloseSocket();
	beamreconnect();
#endif

}
void FrameGameView::SetKindId(WORD KindId)
{
	m_KindId = KindId;
}
void FrameGameView::reconnect()
{
	if (!ClientSocketSink::sharedClientSocket()->IsConnect())
	{
		ClientSocketSink::sharedSocketSink()->LoginServer(m_KindId);
	}
	else
	{
		removeConnect();
		removeconnect();
		m_iReconnectCount = 0;
		OnReconnectAction();
		//CC_ASSERT(FALSE);
	}
}

void FrameGameView::OnReconnectAction()
{

}

void FrameGameView::setGameStatus( BYTE cbGameStatus )
{
	m_cbGameStatus = cbGameStatus;
}

void FrameGameView::beamreconnect(Ref *pSender)
{
	m_iReconnectCount++;
	if (m_iReconnectCount >= 2)
	{
		m_iReconnectCount = 0;
		//您的网络不太给力，请尝试用单机版进行游戏！
		AlertMessageLayer::createConfirm(this,"\u60a8\u7684\u7f51\u7edc\u4e0d\u592a\u7ed9\u529b\uff0c\u8bf7\u9000\u51fa\u623f\u95f4\u91cd\u65b0\u8fde\u63a5\uff01",
			menu_selector(FrameGameView::backLoginView));
		return;
	}
	removeAllChildByTag(TAG_CONNECT_BT);
	std::string szTempTitle = _UNICODE_CONNECTING;

#ifdef _WIN32
		Tools::GBKToUTF8(szTempTitle , "gb2312" , "utf-8");
#endif // _WIN32
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    ClientSocketSink::sharedClientSocket()->CloseSocket();
#endif

	Label *pLable = Label::createWithSystemFont(szTempTitle.c_str(), _GAME_FONT_NAME_1_, 20);
	pLable->setHorizontalAlignment(TextHAlignment::CENTER);
	pLable->setColor(RED_COLOR);
	pLable->setPosition(ccpx(_STANDARD_SCREEN_CENTER_.x , _STANDARD_SCREEN_CENTER_.y+230));
	addChild(pLable ,TAG_CONNECT_BT ,  TAG_CONNECT_BT);
	reconnect();
	schedule(schedule_selector(FrameGameView::connectFait) , 5.f);
}

void FrameGameView::removeconnect()
{
	removeAllChildByTag(TAG_CONNECT_BT);
	unschedule(schedule_selector(FrameGameView::connectFait));
}

void FrameGameView::connectFait(float fd)
{
	removeconnect();
	std::string szTempTitle = _UNICODE_CONNECT_AGAIN;
#ifdef _WIN32
	Tools::GBKToUTF8(szTempTitle , "gb2312" , "utf-8");
#endif // _WIN32
	Label *pLable = Label::createWithSystemFont(szTempTitle.c_str(), _GAME_FONT_NAME_1_, 20);
	pLable->setHorizontalAlignment(TextHAlignment::CENTER);
	pLable->setColor(RED_COLOR);
	pLable->setPosition(ccpx(_STANDARD_SCREEN_CENTER_.x-80 , _STANDARD_SCREEN_CENTER_.y+230));
	addChild(pLable ,TAG_CONNECT_BT ,  TAG_CONNECT_BT);

	Menu *pAgain = Tools::Button("bt_connect_again_normal.png" , "bt_connect_again_click.png" , ccpx(pLable->getPositionX()+pLable->getContentSize().width/2+100 , _STANDARD_SCREEN_CENTER_.y+230) ,this , menu_selector(FrameGameView::beamreconnect) ,  TAG_CONNECT_BT);
	addChild(pAgain ,TAG_CONNECT_BT ,  TAG_CONNECT_BT);
}

bool FrameGameView::isConnecting()
{
	if (getChildByTag(TAG_CONNECT_BT))
	{
		return true;
	}
	return false;
}

void FrameGameView::AddTopString( const char* pstr )
{

}



////////////////////////////////////////////////////////////////////////////////////////////////////////////

bool IGameView::updateUserScore()
{
	if (g_GlobalUnits.m_bSingleGame)
	{
		UpdateDrawUserScore();
		return true;
	}
	else
	{
		m_iUpdateIndex = 0;
		schedule(schedule_selector(IGameView::onTimeUpdateUserScore) , 1.0f);
		return ClientSocketSink::sharedSocketSink()->SendData(MDM_GF_FRAME, SUB_GF_UPDATE_SCORE);
	}

}

IGameView::IGameView( GameScene *pGameScene ):FrameGameView(pGameScene)
{
	m_pGameFunChoose=NULL;
	m_cbCurrentGame=GAME_TYPE_MAIN;
	m_cbNextScene=GAME_TYPE_MAIN;
}

SoundUtil * IGameView::shareSoundEngine()
{
	return SoundUtil::sharedEngine();
}

IGameView::~IGameView( void )
{
	
}

void IGameView::backLoginView(Ref *pSender)
{
	m_IsExit = true;
	SoundUtil::sharedEngine()->stopAllEffects();
	SoundUtil::sharedEngine()->stopBackMusic();
	ClientSocketSink::sharedSocketSink()->LeftGameReq();
    GameLayerMove::sharedGameLayerMoveSink()->QuitGame();
	ClientSocketSink::sharedSocketSink()->setFrameGameView(NULL);
	this->RemoveAlertMessageLayer();
	this->SetAllTouchEnabled(true);
}

void IGameView::BackToLobby()
{
	std::string str = std::string("\u60a8\u786e\u5b9a\u8981\u9000\u51fa\u6e38\u620f\u5417\uff1f");

	AlertMessageLayer::createConfirmAndCancel(this, str.c_str(), menu_selector(IGameView::backLoginView));
}

void IGameView::onExit()
{
	stopAllActions();
	FrameGameView::onExit();
}

void IGameView::onEnter()
{
	FrameGameView::onEnter();
}

void IGameView::OnEventUserScore( tagUserData * pUserData, WORD wChairID, bool bLookonUser )
{
	unschedule(schedule_selector(IGameView::onTimeUpdateUserScore));
	FrameGameView::OnEventUserScore(pUserData, wChairID, bLookonUser);
}

void IGameView::onTimeUpdateUserScore( float delta )
{
	unschedule(schedule_selector(IGameView::onTimeUpdateUserScore));
	m_iUpdateIndex++;
	float fDalayTime[5] = {
		1.0f, 2.0f, 4.0f, 8.0f, 16.0f
	};
	ClientSocketSink::sharedSocketSink()->SendData(MDM_GF_FRAME, SUB_GF_UPDATE_SCORE);
	if (m_iUpdateIndex < 5)
	{
		schedule(schedule_selector(IGameView::onTimeUpdateUserScore) , fDalayTime[m_iUpdateIndex]);
	}
}

int IGameView::getRand(int start,int end)  
{  
    //产生一个从start到end间的随机数
    srand((unsigned)time(0));
    int randNum = std::rand();
    int swpan = end - start + 1;
    int result = randNum % swpan + start;
    return result;
}  

bool IGameView::GetScoreForBank()
{
	if (!g_GlobalUnits.m_bSingleGame) displayConnect();

	CMD_GR_C_QueryInsureInfoRequest temp;
	temp.cbActivityGame = 1;
	return  ClientSocketSink::sharedSocketSink()->SendData(MDM_GR_INSURE, SUB_GR_QUERY_INSURE_INFO , &temp , sizeof(CMD_GR_C_QueryInsureInfoRequest));
}
