#ifndef __PlatUtil_h__
#define __PlatUtil_h__

#include "utils/GameBase.h"
#include "utils/CodeChange.h"
#include "net/NetMessage.h"
#include <iostream>
using namespace std;
using namespace custom;

class PlatUtil
{
public:
	//lua  ---->   c++
	static void connectServer(UINT clientId, string ip, UINT port, UINT heartDelayTime);    //连接服务器
	static void sendMessage(UINT clientId, NetMessage *msg);   //发送协议
	static void Utf8ToUnicode(CodeChange *str);      //字符串编码转换
	static string UnicodeToUtf8(CodeChange *str);		//字符换编码转换
	static string getMD5String(string str);
	static void closeServer(UINT clientId);  //断开服务器
	static void setMessageParsePerFrame(UINT data);  //每一帧处理几个协议
	static bool uncompressZip(string zipFilePath, string dirPath);
	static bool createDirectory(const char* folderPath);

	//c++  ---->   lua
	static void onMessage(UINT mainCmdId, UINT subCmdId, NetMessage msg);   //协议下行
	static void onNetConnected(UINT clientId, bool bConnect);   //服务器连接结果
	static void onNetClosed(UINT clientId);
};

#endif
