﻿#ifndef __SERVER_LIST_MANAGER_H__
#define __SERVER_LIST_MANAGER_H__
#include "cocos2d.h"
#include <vector>

#include "GlobalDef.h"


using namespace std;

typedef std::vector<tagGameType > VectorType;
typedef std::vector<tagGameKind > VectorKind;
typedef std::vector<tagGameServer > VectorServer;


class CServerListManager
{

public:
	VectorType			m_vcType;				//类型指针
	VectorKind			m_vcKind;				//游戏指针
	VectorServer		m_vcServer;				//房间指针

	//函数定义
public:
	//构造函数
	CServerListManager();
	//析构函数
	virtual ~CServerListManager();

	//插入接口
public:
	//插入子项
	bool InsertTypeItem(tagGameType GameType[], WORD wCount);
	//插入子项
	bool InsertKindItem(tagGameKind GameKind[], WORD wCount);
	//插入子项
	bool InsertServerItem(tagGameServer GameServer[], WORD wCount);


	//查找接口
public:
	//查找子项
	tagGameType * SearchTypeItem(WORD wTypeID);
	//查找子项
	tagGameKind * SearchKindItem(WORD wKindID);
	//查找子项
	tagGameServer * SearchServerItem(WORD wKindID, WORD wServerID);

	//功能接口
public:
	//枚举房间类型项
	tagGameType * EnumTypeItem(ULONGLONG nIndex);
	//枚举游戏项
	tagGameKind * EnumKindItem(ULONGLONG nIndex);
	//枚举房间项
	tagGameServer * EnumServerItem(ULONGLONG nIndex);

	void SortServerItem();
	void ClearAllData();
};



#endif
