#include "GameBuyLayer.h"
#include "LobbySocketSink.h"
#include "ClientSocketSink.h"
#include "Encrypt.h"
#include "jsoncpp.h"
#include "LocalDataUtil.h"
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
#include "GoodsByWeChatPay.h"
#endif
#include "GameLayerMove.h"

#define WEB_ORDER_ID    1
#define WEB_PAY_ID      2

#define SERVER_URL			"https://pay.game678.com.cn/apple_pay"

#define ORDER_URL			std::string(std::string(SERVER_URL) + std::string("/get_pay_order.php?")).c_str()

#define PAY_URL				std::string(std::string(SERVER_URL) + std::string("/acquire.php?")).c_str()

#define Btn_close		1
#define Btn_zfb			2
#define Btn_wx			3

//pay
#define pay0		8
#define pay1		9
#define pay2		10
#define pay3		11
#define pay4		12

GamePay::GamePay(GameScene *pGameScene):GameLayer(pGameScene)
{
    m_cbType = 0;
    m_GoodIndex = 0;
    m_loadres = false;
    m_strPayCallBack = "";
}

GamePay::~GamePay()
{
}

Menu* GamePay::CreateButton(std::string szBtName ,const Vec2 &p , int tag )
{
    Menu *pBT = Tools::Button(StrToChar(szBtName+"_normal.png") , StrToChar(szBtName+"_click.png") , p, this , menu_selector(GamePay::callbackBt) , tag);
    return pBT;
}

void GamePay::UpdateButtonWay(float deltaTime)
{
    for (int i = 0; i < 3; i++)
    {
        Menu* temp = (Menu*)getChildByTag(Btn_zfb+i);
        if(temp != NULL){ temp->setEnabled(true); temp->setColor(Color3B(255,255,255));}
    }
}

void GamePay::callbackBt(Ref *pSender)
{
    Node *pNode = (Node *)pSender;
    int iTag = pNode->getTag();
    switch (iTag)
    {
        case Btn_close:
        {
            SoundUtil::sharedEngine()->playEffect("buttonMusic");
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
            if (g_GlobalUnits.m_SwitchYZ == 0)
            {
                onRemove();
            }
            else if (m_GoodSLayer->isVisible())
            {
                onRemove();
            }
            else
            {
                m_GoodSLayer->setVisible(true);
                m_WayLayer->setVisible(false);
            }
#else
            if (m_GoodSLayer->isVisible())
            {
                onRemove();
            }
            else
            {
                m_GoodSLayer->setVisible(true);
                m_WayLayer->setVisible(false);
            }
#endif
            break;
        }
        case pay0:
        case pay1:
        case pay2:
        case pay3:
        case pay4:
        {
            SoundUtil::sharedEngine()->playEffect("buttonMusic");
            m_GoodIndex = iTag - pay0;
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
            if (g_GlobalUnits.m_SwitchYZ == 0)
            {
                LoadLayer::create(m_pGameScene);
                GoodsByWeChatPay_cpp::shared()->IosPay(m_GoodIndex);
            }
            else
            {
                m_GoodSLayer->setVisible(false);
                m_WayLayer->setVisible(true);
            }
#else
            m_GoodSLayer->setVisible(false);
            m_WayLayer->setVisible(true);
#endif
            break;
        }
        case Btn_wx:
        {
            SoundUtil::sharedEngine()->playEffect("buttonMusic");
            for (int i = 0; i < 5; i++)
            {
                Menu* temp = (Menu*)getChildByTag(Btn_zfb+i);
                if(temp != NULL){ temp->setEnabled(false); temp->setColor(Color3B(100,100,100));}
            }
            scheduleOnce(schedule_selector(GamePay::UpdateButtonWay),2.0f);
            
            m_cbType = 1;
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
            LoadLayer::create(m_pGameScene);
            GoodsByWeChatPay_cpp::shared()->payWeChat(g_GlobalUnits.GetUserID(), g_GlobalUnits.GetGolbalUserData().dwGameID, m_GoodIndex);
#endif
            
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
            JniSink::share()->setJniCallback(this);
            JniSink::share()->sendWXPayOrder(g_GlobalUnits.GetUserID(), g_GlobalUnits.GetGolbalUserData().dwGameID, BUY_GOLD_SETUP[m_GoodIndex][1]);
#endif
            break;
        }
        case Btn_zfb:
        {
            SoundUtil::sharedEngine()->playEffect("buttonMusic");
            for (int i = 0; i < 5; i++)
            {
                Menu* temp = (Menu*)getChildByTag(Btn_zfb+i);
                if(temp != NULL){ temp->setEnabled(false); temp->setColor(Color3B(100,100,100));}
            }
            scheduleOnce(schedule_selector(GamePay::UpdateButtonWay),2.0f);

            m_cbType = 2;
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)            
            GoodsByWeChatPay_cpp::shared()->IosAliPay(g_GlobalUnits.GetUserID(), g_GlobalUnits.GetGolbalUserData().dwGameID, m_GoodIndex);
#endif
            
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
            JniSink::share()->setJniCallback(this);
            JniSink::share()->sendPayOrder(g_GlobalUnits.GetUserID(), g_GlobalUnits.GetGolbalUserData().dwGameID, BUY_GOLD_SETUP[m_GoodIndex][1], BUY_GOLD_SETUP[m_GoodIndex][0], "", m_cbType);
#endif
            break;
        }
    }
}

void GamePay::update(float delta)
{
    if (m_strPayCallBack != "")
    {
        AlertMessageLayer::createConfirm(this, m_strPayCallBack.c_str());
        m_strPayCallBack = "";
    }
}

void GamePay::paycallback( const char *szOrderNum)
{
    if (strcmp(szOrderNum, "0") != 0)
    {
        m_strPayCallBack = "取消充值！";
        return;
    }

    g_GlobalUnits.GetGolbalUserData().lInsureScore += BUY_GOLD_SETUP[m_GoodIndex][0];
    GameLayerMove::sharedGameLayerMoveSink()->UpdataLobbyScore();

    m_strPayCallBack = "您已充值成功！";
}

bool GamePay::init()
{
    if ( !Layer::init() )	return false;
    setLocalZOrder(1000);
    setTouchEnabled(true);
    Tools::addSpriteFrame("newPay.plist");
    
    auto listener = EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(true);
    listener->onTouchBegan = CC_CALLBACK_2(GamePay::onTouchBegan, this);
    listener->onTouchMoved = CC_CALLBACK_2(GamePay::onTouchMoved, this);
    listener->onTouchEnded = CC_CALLBACK_2(GamePay::onTouchEnded, this);
    Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(listener, this);
    
    ui::Scale9Sprite* colorBg = ui::Scale9Sprite::create("Common/bg_gray.png");
    colorBg->setContentSize(Size(1920, 1080));
    colorBg->setPosition(_STANDARD_SCREEN_CENTER_);
    addChild(colorBg);
    
    m_payback = Sprite::createWithSpriteFrameName("bg_tongyongkuang1.png");
    m_payback->setPosition(_STANDARD_SCREEN_CENTER_);
    addChild(m_payback);
    
    Menu* paybtn = CreateButton("btn_close" ,Vec2(1040,635),Btn_close);
    m_payback->addChild(paybtn);
    
    
    Sprite *pSprite = Sprite::createWithSpriteFrameName("recharge_title.png");
    pSprite->setPosition(Vec2(535,630));
    m_payback->addChild(pSprite);
    
    initGoodsLayer();
    initWayLayer();
    m_WayLayer->setVisible(false);
    this->scheduleUpdate();
    return true;
}

void GamePay::initGoodsLayer()
{
    m_GoodSLayer = Layer::create();
    addChild(m_GoodSLayer);
    
    char szOut[1024];
    for (int i = 0; i < 5; i++)
    {
        Sprite* pSprite = Sprite::createWithSpriteFrameName(__String::createWithFormat("new_goldicon%d.png", i)->getCString());
        pSprite->setPosition(Vec2(550 , 705-i*110));
        m_GoodSLayer->addChild(pSprite);

#if CC_TARGET_PLATFORM == CC_PLATFORM_IOS
        if (g_GlobalUnits.m_SwitchYZ == 0)
            sprintf(szOut, "%.01f\u4e07\u9152\u5427\u8c46", BUY_GOLD_IOS[i][0]/10000.0);
        else
            sprintf(szOut, "%.01f\u4e07\u9152\u5427\u8c46", BUY_GOLD_SETUP[i][0]/10000.0);
#else
        sprintf(szOut, "%.01f\u4e07\u9152\u5427\u8c46", BUY_GOLD_SETUP[i][0]/10000.0);
#endif
        Label *gameMoney = Label::createWithSystemFont(szOut, _GAME_FONT_NAME_1_, 46);
        gameMoney->setPosition(Vec2(780 , 700-i*110));
        m_GoodSLayer->addChild(gameMoney);
        
#if CC_TARGET_PLATFORM == CC_PLATFORM_IOS
        if (g_GlobalUnits.m_SwitchYZ == 0)
            sprintf(szOut, "\uffe5%ld\u5143", BUY_GOLD_IOS[i][2]);
        else
            sprintf(szOut, "\uffe5%ld\u5143", BUY_GOLD_SETUP[i][1]);
#else
        sprintf(szOut, "\uffe5%ld\u5143", BUY_GOLD_SETUP[i][1]);
#endif
        Label* money = Label::createWithSystemFont(szOut, _GAME_FONT_NAME_1_, 46);
        money->setPosition(Vec2(1050 , 700-i*110));
        m_GoodSLayer->addChild(money);
        
        Menu *pMenu =  CreateButton("newbuybtn" , Vec2(1330,700-i*110), pay0+i);
        m_GoodSLayer->addChild(pMenu);
    }

}

void GamePay::initWayLayer()
{
    m_WayLayer = Layer::create();
    addChild(m_WayLayer);
    
    Menu* paybtn = CreateButton("new_zfbpay" ,Vec2(760,540),Btn_zfb);
    m_WayLayer->addChild(paybtn);
    
    paybtn = CreateButton("new_wxpay" ,Vec2(1160,540),Btn_wx);
    m_WayLayer->addChild(paybtn);
}

GamePay* GamePay::create(GameScene *pGameScene)
{
    GamePay* temp = new GamePay(pGameScene);
    if(temp && temp->init())
    {
        temp->autorelease();
        return temp;
    }
    else
    {
        CC_SAFE_DELETE(temp);
        return NULL;
    }
}

void GamePay::onEnter()
{
    GameLayer::onEnter();
}

void GamePay::onExit()
{
    GameLayer::onExit();
    unscheduleUpdate();
    if (m_loadres)
    {
        m_loadres = false;
        Tools::removeSpriteFrameCache("newPay.plist");
    }
}

void GamePay::onRemove()
{
    JniSink::share()->setJniCallback(this);
    m_pGameScene->removeChild(this);
}
