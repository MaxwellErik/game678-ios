#include "Buff.h"

namespace fishgame{

	Buff* Buff::create(int buffType, float buffParam, float buffTime){
		auto* ret = new (std::nothrow) Buff();
		if (ret)
		{
			ret->m_BTP = buffType;
			ret->m_param = buffParam;
			ret->m_fLife = buffTime;
		}

		return ret;

	}

	Buff::Buff()
		: m_BTP(EBT_NONE)
		, m_fLife(0.0f)
		, m_pOwner(nullptr)
		, m_param(1.0f)
	{}

	Buff::~Buff(){}

	void Buff::Clear(){  }

	bool Buff::OnUpdate(float fdt){ 
		if (m_fLife > 0.0f)
			m_fLife -= fdt;

		return m_fLife == -1.0f || m_fLife > 0.0f;
	}
}