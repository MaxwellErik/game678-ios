#pragma once

#ifndef _FISH_GAME_COMMON_
#define  _FISH_GAME_COMMON_


#include "cocos2d.h"
namespace fishgame{

	

	struct ImageInfo
	{
		std::string		imageName;
		std::string		aniName;
		int				aniType;
		float			offsetX;
		float			offsetY;
		float			direction;
		float			scale;
	};

	struct VisualNode
	{
		cocos2d::Node*	target;
		float			scale;
		float			direction;
		float			offsetX;
		float			offsetY;

		VisualNode()
			: target(nullptr)
		{

		}
	};

}

#endif
