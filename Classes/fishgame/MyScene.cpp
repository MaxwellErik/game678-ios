#include "MyScene.h"
#include "cocostudio/CCArmature.h"
#include "cocostudio/CCArmatureDataManager.h"
#include "MyObject.h"
#include "PathManager.h"
#include "spine/spine.h"
#include "spine/SkeletonAnimation.h"

namespace fishgame{


	MyScene::MyScene() :cocos2d::Scene()
	{
		m_pLayerBullet = nullptr;
		m_pLayerBulletDie = nullptr;
		m_pLayerFish = nullptr;
	}

	MyScene::~MyScene()
	{
	}

	bool MyScene::AddFish(Fish* pFish, std::list<VisualNode>* result){
		if (m_pLayerFish == nullptr){
			m_pLayerFish = cocos2d::Layer::create();
			addChild(m_pLayerFish, OBJECT_FISH);
		}

		VisualData* pVisualData = nullptr;
		bool bDead;
		int visualId = pFish->GetVisualId();
		switch (pFish->GetState())
		{

		case EOS_LIVE:
		case EOS_HIT:
			bDead = false;
			pVisualData = PathManager::GetInstance()->GetVisualData(visualId);
			break;
		case EOS_DEAD:
			bDead = true;
			pVisualData = PathManager::GetInstance()->GetVisualData(visualId);
			break;
		default:
			break;
		}
		if (pVisualData != nullptr){
			auto& images = bDead ? pVisualData->ImageInfoDie : pVisualData->ImageInfoLive;
			for (auto& image: images){


				switch (image.AniType){
				case VAT_FRAME:{
					VisualNode visualNode;					
					cocostudio::ArmatureDataManager::getInstance()->addArmatureFileInfo("fishgame2d/animation/" + image.Image + "/" + image.Image + ".ExportJson");
					cocostudio::Armature* pArmature = cocostudio::Armature::create(image.Image);
					if (pArmature != nullptr){
						if (pArmature->getAnimation() != nullptr){
							pArmature->getAnimation()->play(image.Name, -1, bDead ? 0 : -1);
							if (bDead){
								pArmature->getAnimation()->setMovementEventCallFunc([=](cocostudio::Armature* sender, cocostudio::MovementEventType type, const std::string& id)
								{
									if (type == cocostudio::MovementEventType::COMPLETE)
									{
										sender->getAnimation()->stop(); 
										sender->setVisible(false);
									}
								});
							}							
						}
												
						pArmature->setScale(image.Scale);
						m_pLayerFish->addChild(pArmature, visualId);

						visualNode.direction = image.Direction;
						visualNode.offsetX = image.OffestX;
						visualNode.offsetY = image.OffestY;
						visualNode.scale = image.Scale;
						visualNode.target = pArmature;

						result->push_back(visualNode);
					}										
					break;
				}
				case VAT_SKELETON:{
					VisualNode visualNode;
					auto* pSkeleton = spine::SkeletonAnimation::createWithFile(
						"fishgame2d/animation/" + image.Image + "/" + image.Image + ".json"
						, "fishgame2d/animation/" + image.Image + "/" + image.Image + ".atlas"
						, 1);
					if (pSkeleton != nullptr){
						pSkeleton->setAnimation(0, image.Name, true);
						pSkeleton->setScale(image.Scale);
						m_pLayerFish->addChild(pSkeleton, visualId);

						visualNode.direction = image.Direction;
						visualNode.offsetX = image.OffestX;
						visualNode.offsetY = image.OffestY;
						visualNode.scale = image.Scale;
						visualNode.target = pSkeleton;

						result->push_back(visualNode);
					}					
					break;
				}}
			}
		}
		// 测试包围盒;
		//auto boundingBox = cocos2d::DrawNode::create();
		//boundingBox->drawDot(cocos2d::Vec2(0,0), 10, cocos2d::Color4F(1, 0, 0, 0.3));
		//boundingBox->setScale(1280.0f / 1440.0f, 720.0f / 900.0f);

		//auto boxId = pFish->GetBoundingBox();
		//auto* pathData = PathManager::GetInstance()->GetBoundingBoxData(boxId);
		//if (pathData != nullptr){
		//	for (auto v : pathData->value){
		//		boundingBox->drawDot(cocos2d::Vec2(v.offsetX, v.offsetY), v.rad, cocos2d::Color4F(1, 1, 1, 0.3));
		//	}
		//}

		//addChild(boundingBox, OBJECT_FISH_TEST);

		//VisualNode visualNode;
		//
		//visualNode.direction = 0;
		//visualNode.offsetX = 0;
		//visualNode.offsetY = 0;
		//visualNode.scale = 1;
		//visualNode.target = boundingBox;
		//
		//result->push_back(visualNode);

		return result;
	}

	bool MyScene::AddBullet(Bullet* pBullet, std::list<VisualNode>* result){
		if (m_pLayerBullet == nullptr){
			m_pLayerBullet = cocos2d::Layer::create();
			addChild(m_pLayerBullet, OBJECT_BULLET);
		}
		if (m_pLayerBulletDie == nullptr){
			m_pLayerBulletDie = cocos2d::Layer::create();
			addChild(m_pLayerBulletDie, OBJECT_BULLET_DIE);
		}


		int cannonSetType = pBullet->GetCannonSetType();
		int cannonType = pBullet->GetCannonType();

		bool bDead;
		Cannon* cannon = nullptr;
		switch (pBullet->GetState())
		{

		case EOS_LIVE:
		case EOS_HIT:
			bDead = false;
			cannon = PathManager::GetInstance()->GetCannonData(cannonSetType, cannonType);
			break;
		case EOS_DEAD:
			bDead = true;
			cannon = PathManager::GetInstance()->GetCannonData(cannonSetType, cannonType);
			break;
		default:
			break;
		}
		if (cannon != nullptr){
			if (bDead){
				VisualNode visualNode;

				cocostudio::ArmatureDataManager::getInstance()->addArmatureFileInfo("fishgame2d/animation/" + cannon->net.resName + "/" + cannon->net.resName + ".ExportJson");
				cocostudio::Armature* hehe = cocostudio::Armature::create(cannon->net.resName);
				hehe->getAnimation()->play(cannon->net.Name,-1,0);
				hehe->getAnimation()->setMovementEventCallFunc([=](cocostudio::Armature* sender, cocostudio::MovementEventType type, const std::string& id)
				{
					if (type == cocostudio::MovementEventType::COMPLETE)
					{
						sender->getAnimation()->stop();  
						sender->setVisible(false);
					}
				});
				visualNode.direction = 0;
				visualNode.offsetX = cannon->net.PosX;
				visualNode.offsetY = cannon->net.PosY;
				visualNode.scale = 1;
				visualNode.target = hehe;

				m_pLayerBulletDie->addChild(hehe, cannon->type);
				result->push_back(visualNode);
			}
			else{
				VisualNode visualNode;

				cocostudio::ArmatureDataManager::getInstance()->addArmatureFileInfo("fishgame2d/animation/" + cannon->bullet.resName + "/" + cannon->bullet.resName + ".ExportJson");
				cocostudio::Armature* hehe = cocostudio::Armature::create(cannon->bullet.resName);
				hehe->getAnimation()->play(cannon->bullet.Name,-1,-1);
				visualNode.direction = 0;
				visualNode.offsetX = 0;
				visualNode.offsetY = 0;
				visualNode.scale = 1;
				visualNode.target = hehe;

				m_pLayerBullet->addChild(hehe, cannon->type);
				result->push_back(visualNode);
			}
		}

		return result;
	}

	bool MyScene::AddMyObject(MyObject* pObj, std::list<VisualNode>* result){
		if (pObj == NULL) return false;

		int objType = pObj->GetType();
		switch (objType)
		{
		case EOT_FISH:{
			return AddFish((Fish*)pObj, result);
		}			
		case EOT_BULLET:{
			return AddBullet((Bullet*)pObj, result);
		}
		default:
			break;
		}

		return true;
	}
}