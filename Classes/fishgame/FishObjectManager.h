#pragma once
#ifndef __FishObjectManager_h__
#define __FishObjectManager_h__

#define SERVER_WIDTH 1440.0f
#define SERVER_HEIGHT 900.0f
#define CLIENT_WIDTH 1280.0f
#define CLIENT_HEIGHT 720.0f

#include "cocos2d.h"
#include "cocos-ext.h"

namespace fishgame{
	class Bullet;
	class Fish;

	class FishObjectManager : public cocos2d::Ref
	{
	private:
		FishObjectManager();

		static FishObjectManager* m_Instance;
	public:
		~FishObjectManager();

		static FishObjectManager* GetInstance(){
			if (m_Instance == NULL){
				m_Instance = new (std::nothrow) FishObjectManager();
				if (m_Instance){
					m_Instance->autorelease();
					m_Instance->retain();
				}
				else{
					CC_SAFE_DELETE(m_Instance);
				}
			}
			return m_Instance;
		}

		static void DestoryInstace(){
			if (m_Instance != NULL){
				m_Instance->release();
				m_Instance = NULL;
			}
		}

		void Init(int,int,int,int);
		void Clear();
		bool AddBullet(Bullet* pBullet);
		Bullet* FindBullet(unsigned long  id);
		bool RemoveAllBullets(bool);
		bool AddFish(Fish* pFish);
		Fish* FindFish(unsigned long);

		cocos2d::Vector<Fish* > GetAllFishes();

		bool RemoveAllFishes(bool);
		bool OnUpdate(float dt);

		void RegisterBulletHitFishHandler(int);


		void AddFishBuff(int buffType, float buffParam, float buffTime);

		
		bool MirrowShow();
		void SetMirrowShow(bool);

		void ConvertMirrorCoord(float*, float*);
		void ConvertCoortToCleientPosition(float*, float*);
		void ConvertCoortToScreenSize(float*, float*);

		void ConvertCoord(float*, float*);
		float ConvertDirection(float);

		void SetGameLoaded(bool b){ m_bLoaded = b; }
		bool IsGameLoaded(){ return m_bLoaded; }


		int GetServerWidth(){ return m_nServerWidth; }
		int GetServerHeight(){ return m_nServerHeight; }

		void	SetSwitchingScene(bool b){ m_bSwitchingScene = b; }
		bool	IsSwitchingScene(){ return m_bSwitchingScene; }
	private:
		bool BBulletHitFish(Bullet* pBullet,Fish* pFish);
		void onActionBulletHitFish(Bullet* pBullet, Fish* pFish);

	public:

	private:
		int m_nServerWidth;
		int m_nServerHeight;
		int m_nClientWidth;
		int m_nClientHeight; 

		int		m_nHandlerBulletHitFish; 

		cocos2d::Map< unsigned long, Bullet*>	m_MapBullet;
		cocos2d::Map< unsigned long, Fish*>		m_MapFish;


		std::mutex m_lock;
		//std::mutex m_lock;

		bool m_bMorrow;
		bool m_bLoaded;
		bool m_bSwitchingScene;
	};
}

#endif // __FishObjectManager_h__