#pragma once

#ifndef _FISH_GAME_SCENE_
#define _FISH_GAME_SCENE_


#include "./common.h"

namespace fishgame{
	enum LayerZOrder
	{
		START = -2,
		STATIC_BACKGROUND,			//背景;
		OBJECT_EFFECT_DOWN,			//效果;
		OBJECT_FISH,				//鱼;
		OBJECT_FISH_TEST,			//鱼;
		STATIC_WATER,				//水纹;
		STATIC_SWITCH_BACKGROUND,	//切换场景背景;
		STATIC_SWITCH_WATER,		//切换场景水纹;
		STATIC_SWITCH_WAVE,			//切换场景波浪;
		OBJECT_BULLET,				//子弹;
		OBJECT_BULLET_DIE,			//子弹死亡效果,单独一层视觉效果好点;
		OBJECT_EFFECT_UP,			//效果;
		TOP_UI,						//UI;
		BOUNDING_BOX,				//绑定盒;
		LAYER_LOADING,				//资源加载层;
		LAYER_TEST,					//测试;
		END
	};


	class MyObject;
	class Fish;
	class Bullet;
	class MyScene : public cocos2d::Scene
	{
	private:
		MyScene();


		bool AddFish(Fish*, std::list<VisualNode>*);
		bool AddBullet(Bullet*, std::list<VisualNode>*);

		cocos2d::Layer*		m_pLayerBullet;
		cocos2d::Layer*		m_pLayerBulletDie;
		cocos2d::Layer*		m_pLayerFish;
	public:		
		~MyScene();
		CREATE_FUNC(MyScene);
		
		bool AddMyObject(MyObject*, std::list<VisualNode>*);
	};
}
#endif // !_FISH_GAME_SCENE_