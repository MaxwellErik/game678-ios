#include "lua_fishgame.hpp"
#include "FishObjectManager.h"
#include "FishUtils.h"
#include "ExtentAction.h"
#include "tolua_fix.h"
#include "LuaBasicConversions.h"

int tolua_fishgame_FishObjectManager_RegisterBulletHitFishHandler(lua_State* tolua_S)
{
	int argc = 0;
	fishgame::FishObjectManager* cobj = nullptr;
	bool ok = true;

#if COCOS2D_DEBUG >= 1
	tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
	if (!tolua_isusertype(tolua_S, 1, "fishgame.FishObjectManager", 0, &tolua_err)) goto tolua_lerror;
#endif

	cobj = (fishgame::FishObjectManager*)tolua_tousertype(tolua_S, 1, 0);

#if COCOS2D_DEBUG >= 1
	if (!cobj)
	{
		tolua_error(tolua_S, "invalid 'cobj' in function 'tolua_fishgame_FishObjectManager_RegisterBulletHitFishHandler'", nullptr);
		return 0;
	}
#endif

	argc = lua_gettop(tolua_S) - 1;
	if (argc == 1)
	{
		int handler = toluafix_ref_function(tolua_S, 2, 0);
		cobj->RegisterBulletHitFishHandler(handler);
		return 0;
	}
	luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "fishgame.FishObjectManager:RegisterBulletHitFishHandler", argc, 1);
	return 0;

#if COCOS2D_DEBUG >= 1
tolua_lerror:
	tolua_error(tolua_S, "#ferror in function 'tolua_fishgame_FishObjectManager_RegisterBulletHitFishHandler'.", &tolua_err);
#endif

	return 0;
}

int lua_custom_net_FuncAction_create(lua_State* tolua_S)
{
	int argc = 0;
	bool ok = true;

#if COCOS2D_DEBUG >= 1
	tolua_Error tolua_err;
#endif

#if COCOS2D_DEBUG >= 1
	if (!tolua_isusertable(tolua_S, 1, "custom.FuncAction", 0, &tolua_err)) goto tolua_lerror;
#endif

	argc = lua_gettop(tolua_S) - 1;

	if (argc == 2)
	{
		double arg0;
		int arg1;
		ok &= luaval_to_number(tolua_S, 2, &arg0, "custom.FuncAction:create");
		// ok &= luaval_to_int32(tolua_S, 3, (int *)&arg1, "custom.FuncAction:create");
		int handler = toluafix_ref_function(tolua_S, 3, 0);
		if (!ok)
		{
			tolua_error(tolua_S, "invalid arguments in function 'lua_custom_net_FuncAction_create'", nullptr);
			return 0;
		}
		custom::FuncAction* ret = custom::FuncAction::create(arg0, handler);
		object_to_luaval<custom::FuncAction>(tolua_S, "custom.FuncAction", (custom::FuncAction*)ret);
		return 1;
	}
	luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d\n ", "custom.FuncAction:create", argc, 2);
	return 0;
#if COCOS2D_DEBUG >= 1
tolua_lerror:
	tolua_error(tolua_S, "#ferror in function 'lua_custom_net_FuncAction_create'.", &tolua_err);
#endif
	return 0;
}


static int extendFishObjectManager(lua_State* tolua_S)
{
	lua_pushstring(tolua_S, "fishgame.FishObjectManager");
	lua_rawget(tolua_S, LUA_REGISTRYINDEX);
	if (lua_istable(tolua_S, -1))
	{
		lua_pushstring(tolua_S, "RegisterBulletHitFishHandler");
		lua_pushcfunction(tolua_S, tolua_fishgame_FishObjectManager_RegisterBulletHitFishHandler);
		lua_rawset(tolua_S, -3);
	}
	lua_pop(tolua_S, 1);

	lua_pushstring(tolua_S, "custom.FuncAction");
	lua_rawget(tolua_S, LUA_REGISTRYINDEX);
	if (lua_istable(tolua_S, -1))
	{
		lua_pushstring(tolua_S, "create");
		lua_pushcfunction(tolua_S, lua_custom_net_FuncAction_create);
		lua_rawset(tolua_S, -3);
	}
	lua_pop(tolua_S, 1);

	return 0;
}


int register_all_fishgame_manual(lua_State* tolua_S){
	if (NULL == tolua_S)
		return 0;

	extendFishObjectManager(tolua_S);
	return 1;
}
