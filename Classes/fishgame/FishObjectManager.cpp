#include "FishObjectManager.h"
#include "PathManager.h"
#include "MyObject.h"
#include "CCLuaEngine.h"
#include "CCLuaStack.h"
namespace fishgame{
	FishObjectManager* FishObjectManager::m_Instance = NULL;

	FishObjectManager::FishObjectManager()
		: m_nHandlerBulletHitFish(0)
		, m_nServerWidth(0)
		, m_nServerHeight(0)
		, m_nClientWidth(0)
		, m_nClientHeight(0)
		, m_bMorrow(false)
		, m_bLoaded(false)
		, m_bSwitchingScene(false)
	{

	}


	FishObjectManager::~FishObjectManager()
	{
	}

	void FishObjectManager::Init(int a,int b,int c, int d){
		m_nServerWidth = a;
		m_nServerHeight = b;
		m_nClientWidth = c;
		m_nClientHeight = d;

		PathManager::GetInstance()->LoadData();
	}

	void FishObjectManager::Clear(){}

	bool FishObjectManager::AddBullet(Bullet* pBullet){
		if (pBullet == nullptr)
		{
			return false;
		}

		auto itr = m_MapBullet.find(pBullet->GetId());
		if (itr != m_MapBullet.end()){
			return false;
		}

		pBullet->SetManager(this);

		m_MapBullet.insert(pBullet->GetId(), pBullet);

		return true;
	}

	Bullet* FishObjectManager::FindBullet(unsigned long  id){
		auto itr = m_MapBullet.find(id);
		if (itr == m_MapBullet.end()){
			return nullptr;
		}
		return itr->second;
	}

	bool FishObjectManager::RemoveAllBullets(bool noCleanNode){
		for (auto& v : m_MapBullet){
			v.second->Clear(true, noCleanNode);
		}
		m_MapBullet.clear();
		return true;
	}

	bool FishObjectManager::AddFish(Fish* pFish){
		if (pFish == nullptr)
		{
			return false;
		}
		auto itr = m_MapFish.find(pFish->GetId());
		if (itr != m_MapFish.end()){
			return false;
		}

		pFish->SetManager(this);

		m_MapFish.insert(pFish->GetId(),pFish);
	}

	Fish* FishObjectManager::FindFish(unsigned long id){
		auto itr = m_MapFish.find(id);
		if (itr == m_MapFish.end()){
			return nullptr;
		}
		return itr->second;
	}
	
	cocos2d::Vector< Fish*> FishObjectManager::GetAllFishes(){
		cocos2d::Vector< Fish*> ret;
		for (auto a : m_MapFish){
			ret.pushBack( a.second);
		}
		return ret;
	}

	bool FishObjectManager::RemoveAllFishes(bool noCleanNode){
		for (auto v : m_MapFish)
		 {
			 v.second->Clear(true, noCleanNode);
		 }
		m_MapFish.clear();
		return true;
	}

	bool FishObjectManager::OnUpdate(float dt){
		int X_COUNT = 10;// m_nServerWidth / INETVAL + 1;
		int Y_COUNT = 4;// m_nServerHeight / INETVAL + 1;
		int X_INTERVAL = m_nServerWidth / X_COUNT;
		int Y_INTERVAL = m_nServerHeight / Y_COUNT;

		std::vector<Fish* > temp[4][10];

		int state;
		
		auto i = m_MapFish.begin();
		while (i != m_MapFish.end()){
			i->second->OnUpdate(dt);
			state = i->second->GetState();

			// 如果不是死亡的更新状态;
			if (state < EOS_DEAD){
				// 加入到数组缓存;
				cocos2d::Point pos = i->second->GetPosition();
				
				float maxRadio = i->second->GetMaxRadio();
				for (int __x = (pos.x - maxRadio) / X_INTERVAL; __x <= (pos.x + maxRadio) / X_INTERVAL; __x++){
					for (int __y = (pos.y - maxRadio) / Y_INTERVAL; __y <= (pos.y + maxRadio) / Y_INTERVAL; __y++){
						if (__y >= 0 && __y < Y_COUNT && __x >= 0 && __x < X_COUNT){
							temp[__y][__x].push_back(i->second);
						}
					}
				}
			}
			// 剔除死亡;
			if (state == EOS_DEAD){
				i->second->Clear(false);
				i = m_MapFish.erase(i);
			}
			else if (state > EOS_DEAD){
				i->second->Clear(true);
				i = m_MapFish.erase(i);
			}
			else{
				i++;
			}
		}

		// 子弹的剔除;
		auto itrBullet = m_MapBullet.begin();
		while (itrBullet != m_MapBullet.end()){
			itrBullet->second->OnUpdate(dt);
			state = itrBullet->second->GetState();

			// 如果不是死亡的更新状态;
			if (!m_bSwitchingScene && state < EOS_DEAD){
				if (itrBullet->second->GetTarget() == 0){
					auto pos = itrBullet->second->GetPosition();
					//int yInterval = pos.y / Y_INTERVAL;
					//int xInterval = pos.x / X_INTERVAL;

					/*if (yInterval >= 0 && yInterval < Y_COUNT && xInterval >= 0 && xInterval < X_COUNT){
						for (auto fish : temp[yInterval][xInterval]){
						if (BBulletHitFish(itrBullet->second, fish)){
						onActionBulletHitFish(itrBullet->second, fish);
						break;
						}
						}
						}*/

					bool catched = false;
					float maxRadio = itrBullet->second->GetCatchRadio();
					for (int __x = (pos.x - maxRadio) / X_INTERVAL; __x <= (pos.x + maxRadio) / X_INTERVAL; __x++){
						for (int __y = (pos.y - maxRadio) / Y_INTERVAL; __y <= (pos.y + maxRadio) / Y_INTERVAL; __y++){
							if (__y >= 0 && __y < Y_COUNT && __x >= 0 && __x < X_COUNT){
								for (auto fish : temp[__y][__x]){
									if (BBulletHitFish(itrBullet->second, fish)){
										onActionBulletHitFish(itrBullet->second, fish);
										catched = true;
										break;
									}
								}
							}
							if (catched) break;
						}
						if (catched) break;
					}


					/*for (auto fish : m_MapFish){
						if (BBulletHitFish(itrBullet->second, fish.second)){
							onActionBulletHitFish(itrBullet->second, fish.second);
							break;
						}
					}*/
				}
				else{
					auto itr = m_MapFish.find(itrBullet->second->GetTarget());
					if (m_MapFish.end() == itr){
						itrBullet->second->SetTarget(0);
					}
					else{

						if (BBulletHitFish(itrBullet->second, itr->second)){
							onActionBulletHitFish(itrBullet->second, itr->second);
						}
					}	
				}

			}

			// 剔除死亡;
			if (state == EOS_DEAD){
				itrBullet->second->Clear(false);
				itrBullet = m_MapBullet.erase(itrBullet);
			}
			else if (state > EOS_DEAD){
				itrBullet->second->Clear(true);
				itrBullet = m_MapBullet.erase(itrBullet);
			}
			else{
				itrBullet++;
			}
		}
		return true;
	}
	
	void FishObjectManager::RegisterBulletHitFishHandler(int handler){
#if CC_ENABLE_SCRIPT_BINDING
		m_nHandlerBulletHitFish = handler;
#endif
	}

	void FishObjectManager::AddFishBuff(int buffType, float buffParam, float buffTime){
		for (auto& fish : m_MapFish){
			fish.second->AddBuff(buffType, buffParam, buffTime);
		}
	}

	bool FishObjectManager::MirrowShow(){
		return m_bMorrow;
	}
	void FishObjectManager::SetMirrowShow(bool b){
		m_bMorrow = b;
	}

	void FishObjectManager::ConvertMirrorCoord(float* x, float* y){
		*x = m_nServerWidth - *x;
		*y = m_nServerHeight - *y;
	}
	void FishObjectManager::ConvertCoortToCleientPosition(float* x, float* y){
		*y = m_nServerHeight - *y;
	}
	void FishObjectManager::ConvertCoortToScreenSize(float* x, float* y){
		*x *= 0.889f;
		*y *= 0.8f;
	}

	void FishObjectManager::ConvertCoord(float* x, float * y){
		if (MirrowShow()){
			ConvertMirrorCoord(x, y);
		}
		ConvertCoortToCleientPosition(x, y);
		ConvertCoortToScreenSize(x, y);
	}

	float FishObjectManager::ConvertDirection(float dir){
		if (MirrowShow()){
			return dir + M_PI;
		}
		else{
			return dir;
		}
		
	}

	bool FishObjectManager::BBulletHitFish(Bullet* pBullet, Fish* pFish){
		int bulletRad = pBullet->GetCatchRadio();
		int maxFishRadio = pFish->GetMaxRadio();
		cocos2d::Point posBullet = pBullet->GetPosition();
		cocos2d::Point posFish = pFish->GetPosition();

		if ((posBullet.x + bulletRad < posFish.x - maxFishRadio && posBullet.y + bulletRad < posFish.y - maxFishRadio)
			|| (posBullet.x - bulletRad > posFish.x + maxFishRadio && posBullet.y - bulletRad > posFish.y + maxFishRadio)){
			return false;
		}
		float dirFish = pFish->GetDirection();
		float sinDir = sinf(dirFish);
		float cosDir = cosf(dirFish);

		auto boxId = pFish->GetBoundingBox();
		auto* pathData = PathManager::GetInstance()->GetBoundingBoxData(boxId);
		if (pathData != nullptr){
			for (auto& v : pathData->value){
				// 转换坐标;
				float x = v.offsetX * cosDir - v.offsetY * sinDir + posFish.x;
				float y = v.offsetX * sinDir + v.offsetY * cosDir + posFish.y;

				float rad = v.rad;

				float _x = x - posBullet.x;
				float _y = y - posBullet.y;
				float _dis = (rad + bulletRad);
				if (_x * _x + _y * _y <= _dis * _dis){
					return true;
				}				
			}
		}

		return false;
	}

	void FishObjectManager::onActionBulletHitFish(Bullet* pBullet, Fish* pFish){
		pBullet->SetState(EOS_DEAD);
		pFish->OnHit();

		//TODO 添加通知到LUA层;
#if CC_ENABLE_SCRIPT_BINDING
		cocos2d::LuaStack *_stack = cocos2d::LuaEngine::getInstance()->getLuaStack();
		_stack->pushObject(pBullet, "fishgame.Bullet");
		_stack->pushObject(pFish, "fishgame.Fish");

		_stack->executeFunctionByHandler(m_nHandlerBulletHitFish, 2);
		_stack->clean();
#endif
	}
}
