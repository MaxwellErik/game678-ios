#include "MyObject.h"
#include "MoveCompent.h"
#include "MyScene.h"
#include "PathManager.h"
#include "Buff.h"

namespace fishgame{
	MyObject::MyObject() 
		: m_nType(EOT_NONE)
		, m_nId(0)
		, m_fDirection(0.0f)
		, m_bInScreen(false)
		, m_bDirtyPos(false)
		, m_bDirtyDir(false)
		, m_bDirtyInScreen(false)
		, m_nState(EOS_INIT)
		, m_bDirtyState(false)
		, m_pOwner(nullptr)
		, m_pManager(nullptr)
		, m_pMoveCompent(nullptr)
		, m_nTargetId(0)
	{}
	MyObject::~MyObject(){
		if (m_pMoveCompent != nullptr){
			m_pMoveCompent->release();
		}
	}

	void MyObject::Clear(bool bForce, bool noCleanNode){
		if (bForce){
			for (auto v : m_pVisualNodeList){
				if (v.target)
					v.target->removeFromParentAndCleanup(true);
				v.target = nullptr;
			}
			m_pVisualNodeList.clear();
		}
		else{
			for (auto v : m_pVisualNodeList){
				if (v.target) v.target->runAction(cocos2d::Sequence::createWithTwoActions(
					cocos2d::DelayTime::create(m_nType == EOT_FISH?3:1), cocos2d::CallFuncN::create([](cocos2d::Node* sender){
					sender->removeFromParentAndCleanup(true);
				})));

				v.target = nullptr;
			}
			m_pVisualNodeList.clear();
		}

		OnClear(bForce);
	}

	void MyObject::OnClear(bool){}

	void MyObject::OnUpdate(float dt){
		if (m_pMoveCompent != nullptr){
			m_pMoveCompent->OnUpdate(dt);
		}
		auto it = m_pBuffList.begin();
		while (it != m_pBuffList.end())
		{
			if (!(*it)->OnUpdate(dt))
			{
				it = m_pBuffList.erase(it);
				continue;
			}
			++it;
		}


		// 更新状态和显示;
		if (m_pManager != nullptr 
			&& m_bInScreen 
			&& m_bDirtyState 
			&& FishObjectManager::GetInstance()->IsGameLoaded()){
			m_bDirtyState = false;

			for (auto v : m_pVisualNodeList){
				if (v.target) v.target->removeFromParentAndCleanup(true);
				v.target = nullptr;
			}
			m_pVisualNodeList.clear();

			MyScene* scene = (MyScene*)cocos2d::Director::getInstance()->getRunningScene();
			if (scene != NULL){
				scene->AddMyObject(this, &m_pVisualNodeList);
			}

			m_bDirtyPos = true;
			m_bDirtyDir = true;
			m_bDirtyInScreen = true;
		}

		// 更新位置;
		if (m_bDirtyPos || m_bDirtyDir || m_bDirtyInScreen){
			float x, y, dir, sinDir, cosDir;

			if (m_bDirtyPos){
				x = m_pPosition.x;
				y = m_pPosition.y;
				FishObjectManager::GetInstance()->ConvertCoord(&x, &y);

				sinDir = sinf(-m_fDirection);
				cosDir = cosf(-m_fDirection);
			}
			if (m_bDirtyDir){
				dir = m_fDirection;
				dir = FishObjectManager::GetInstance()->ConvertDirection(dir);
			}


			for (auto& i : m_pVisualNodeList)
			{
				if (m_bDirtyPos){

					if (FishObjectManager::GetInstance()->MirrowShow()){
						if (i.target) i.target->setPosition(-i.offsetX * cosDir + i.offsetY * sinDir + x,
							-i.offsetX * sinDir - i.offsetY * cosDir + y);
					}
					else{
						if (i.target) i.target->setPosition(i.offsetX * cosDir - i.offsetY * sinDir + x,
							i.offsetX * sinDir + i.offsetY * cosDir + y);
					}
				}
				if (m_bDirtyDir){
					if (i.target) i.target->setRotation(CC_RADIANS_TO_DEGREES(dir + i.direction));
				}
				if (m_bDirtyInScreen){
					if (i.target) i.target->setVisible(m_bInScreen);
				}
			}
			m_bDirtyPos = false;
			m_bDirtyDir = false;
			m_bDirtyInScreen = false;
		}
	}

	void MyObject::OnMoveEnd(){
		SetState(EOS_DESTORY);
	}

	void MyObject::SetPosition(float x, float y){
		if (m_pPosition.x == x && m_pPosition.y == y) {
			return;
		}
		m_pPosition.x = x;
		m_pPosition.y = y;
		m_bDirtyPos = true;

		// 检测是否在屏幕内;
		if (x < -100 || y < -100 || x > 1540 || y > 1000) {
			if (m_bInScreen){
				OnMoveEnd();
			}
			m_bInScreen = false;
		}
		else{
			m_bInScreen = true;
		}
	}

	void  MyObject::SetDirection(float dir){
		if (m_fDirection == dir){
			return;
		}
		m_fDirection = dir;
		m_bDirtyDir = true;
	}

	void MyObject::SetState(int st){
		if (st == m_nState) {
			return;
		}

		if ((st == EOS_LIVE || st == EOS_HIT)
			&& (m_nState == EOS_LIVE || m_nState == EOS_HIT)){
			return;
		}

		m_nState = st;
		m_bDirtyState = true;

	}

	void MyObject::AddBuff(int buffType, float buffParam, float buffTime){
		auto* pBuff = Buff::create(buffType, buffParam, buffTime);
		if (pBuff != nullptr)
		{
			m_pBuffList.push_back(pBuff);
		}
	}

    void MyObject::SetMoveCompent(MoveCompent* p){
		if (p == NULL){ return; }
		if (m_pMoveCompent != nullptr){
			m_pMoveCompent->release();
			m_pMoveCompent->OnDetach();
		}

        m_pMoveCompent = p;
		m_pMoveCompent->retain();
		m_pMoveCompent->SetOwner(this);
		m_pMoveCompent->OnAttach();
    }

	void MyObject::SetTarget(int i){ m_nTargetId = i; }
	int MyObject::GetTarget(){
		return m_nTargetId;
	}

	Fish::Fish() 
		: MyObject()
		, m_nRedTime(0)
		, m_fMaxRadio(0.0f)
	{
		m_nType = EOT_FISH;
	}

	Fish::~Fish(){}

	void Fish::SetBoundingBox(int id){
		m_nBoundingBoxId = id;

		auto boxId = GetBoundingBox();
		auto* pathData = PathManager::GetInstance()->GetBoundingBoxData(boxId);
		if (pathData != nullptr){
			for (auto v : pathData->value){
				m_fMaxRadio = fmax(m_fMaxRadio, fabs(v.offsetX) + v.rad);
				m_fMaxRadio = fmax(m_fMaxRadio, fabs(v.offsetY) + v.rad);
			}
		}
	}

	int Fish::GetBoundingBox(){
		return m_nBoundingBoxId;
	}

	void Fish::OnUpdate(float fdt){
		MyObject::OnUpdate(fdt);

		// 更新被攻击效果;
		if (m_nRedTime > 0){
			m_nRedTime--;
			if (m_nRedTime == 0){
				for (auto& v : m_pVisualNodeList){
					if (v.target) v.target->setColor(cocos2d::Color3B(0xFF, 0xFF, 0xFF));
				}
			}
		}
	}

	void Fish::OnHit(){
		for (auto& v: m_pVisualNodeList){
			if (v.target) v.target->setColor(cocos2d::Color3B(0xFF, 0x11, 0));
		}

		m_nRedTime = 5;
	}

	Bullet::Bullet() 
		: MyObject()
		, m_nCatchRadio(30)
		, m_hitTime(0.0f)
	{
		m_nType = EOT_BULLET;
	}
	
	Bullet::~Bullet(){}

	void Bullet::SetCannonSetType(int n){
		m_nCannonSetType = n;
	}

	int	Bullet::GetCannonSetType(){ return m_nCannonSetType; }

	void Bullet::SetCannonType(int n){
		m_nCannonType = n;
	}

	int	Bullet::GetCannonType(){ return m_nCannonType; }

	void Bullet::SetCatchRadio(int n){ m_nCatchRadio = n; }

	int	Bullet::GetCatchRadio(){ return m_nCatchRadio; }

	void Bullet::SetState(int st){
		MyObject::SetState(st);

		if (st == EOS_HIT){
			m_hitTime = 0.5f;
		}
	}

	void Bullet::OnUpdate(float fdt){
		// 更新被攻击效果;
		if (m_hitTime > 0){
			m_hitTime -= fdt;
			if (m_hitTime <= 0){
				SetState(EOS_DEAD);
			}
		}

		MyObject::OnUpdate(fdt);
	}
}
