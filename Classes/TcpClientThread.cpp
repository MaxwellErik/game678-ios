﻿#include "TcpClientThread.h"

#if(CC_TARGET_PLATFORM != CC_PLATFORM_WIN32)
#include <sys/time.h>
inline unsigned int GetTickCount()
{
	struct timeval tv;
	if (gettimeofday(&tv, 0)) {
		return 0;
	}
	return (tv.tv_sec * 1000 + tv.tv_usec / 1000);
}
#endif



void CTcpClientThread::Run()
{
	while (true)
	{
		RecvPack();
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
		Sleep(1);
#else
		usleep(1000);
#endif
	}
}

bool CTcpClientThread::RecvPack()
{
	if(!this->IsConnect())
	{
		return false;
	}

	int tempRecvLength = recv(this->m_Socket, (char *)this->m_cbRecvBuf + this->m_wRecvSize, sizeof(this->m_cbRecvBuf) - this->m_wRecvSize, 0);

#if (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)

	// 错误 处理
	if(tempRecvLength < 0) 
	{
		int tempError = WSAGetLastError();  

		if(tempError == WSAEWOULDBLOCK)
		{
			return true;
		}

		CCLOG("LastError[%d]" , tempError);

		// 关闭 socket
		this->CloseSocket();		

		return false;
	}
	else if(tempRecvLength == 0)
	{
		// 关闭 socket
		this->CloseSocket();		

		return false;
	}

#else

	// 错误 处理
	if(tempRecvLength < 0)
	{
		// 由于是非阻塞的模式,所以当errno为EAGAIN时,表示当前缓冲区已无数据可读
		// 在这里就当作是该次事件已处理处.
		//即当buflen < 0 且 errno == EAGAIN时，表示没有数据了。(读/写都是这样)
		if(errno == EAGAIN || errno == EINTR)  
		{
			return true;
		}

		// 关闭 socket
		this->CloseSocket();		

		return false;	
	}
	else if(tempRecvLength == 0)
	{
		// 关闭 socket
		this->CloseSocket();		

		return false;	
	}

#endif

	this->m_RecvBufferUsedCounter += tempRecvLength;

	m_wRecvSize += tempRecvLength;
	m_dwRecvTickCount = GetTickCount() / 1000L;

	//变量定义
	WORD wPacketSize = 0;
	BYTE cbDataBuffer[SOCKET_BUFFER+sizeof(CMD_Head)];
	CMD_Head * pHead = (CMD_Head *)m_cbRecvBuf;

	while (m_wRecvSize >= sizeof(CMD_Head))
	{
		//效验参数
		wPacketSize = pHead->CmdInfo.wPacketSize;

		if (pHead->CmdInfo.cbVersion != SOCKET_VER)
		{
			this->CloseSocket();		

			return false;
		}
		if (wPacketSize > (SOCKET_BUFFER + sizeof(CMD_Head)))
		{
			this->CloseSocket();		

			return false;
		}
		if (m_wRecvSize < wPacketSize) return true;

		//拷贝数据
		m_dwRecvPacketCount++;
		memcpy(cbDataBuffer, m_cbRecvBuf, wPacketSize);
		m_wRecvSize -= wPacketSize;
		memmove(m_cbRecvBuf, m_cbRecvBuf + wPacketSize, m_wRecvSize);

		//解密数据
		WORD wRealySize = CrevasseBuffer(cbDataBuffer, wPacketSize);

		//解释数据
		WORD wDataSize = wRealySize - sizeof(CMD_Head);
		void * pDataBuffer = cbDataBuffer + sizeof(CMD_Head);
		CMD_Command Command = ((CMD_Head *)cbDataBuffer)->CommandInfo;

		//内核命令
		if (Command.wMainCmdID == MDM_KN_COMMAND)
		{
			switch (Command.wSubCmdID)
			{
			case SUB_KN_DETECT_SOCKET:	//网络检测
				{
					//发送数据
					SendData(MDM_KN_COMMAND, SUB_KN_DETECT_SOCKET, pDataBuffer, wDataSize);
					break;
				}
			}
			continue;
		}

		MsgInfo msg;
		msg.Command = Command;
		msg.wDataSize = wDataSize;
		CopyMemory(msg.szBuffer, pDataBuffer, wDataSize);

		pthread_mutex_lock(&m_MsgLock);
		m_vecMsgInfo.push_back(msg);
		pthread_mutex_unlock(&m_MsgLock);
	};
	return true;
}

CTcpClientThread::CTcpClientThread( void )
{
	m_bClose = false;
	Start();
	pthread_mutex_init(&m_MsgLock,NULL);
}

CTcpClientThread::~CTcpClientThread(void)
{
}

void CTcpClientThread::StartReceive()
{
	Director::getInstance()->getScheduler()->schedule(schedule_selector(CTcpClientThread::update), this, 0, false);
}

void CTcpClientThread::StopReceive()
{
	Director::getInstance()->getScheduler()->unschedule(schedule_selector(CTcpClientThread::update), this);
}

void CTcpClientThread::update( float delta )
{
	if (m_pTcpCallback == NULL)
	{
		return;
	}
	MsgInfo msg;
	bool bFind = false;
	pthread_mutex_lock(&m_MsgLock);
	if (m_vecMsgInfo.size() > 0)
	{
		msg = m_vecMsgInfo[0];
		m_vecMsgInfo.erase(m_vecMsgInfo.begin());
		bFind = true;
	}
	pthread_mutex_unlock(&m_MsgLock);
	
	if (bFind)
	{
		//m_pTcpCallback->OnEventTCPSocketRead(msg.Command, msg.szBuffer, msg.wDataSize);
		//处理数据
		bool bSuccess = this->m_pTcpCallback->OnEventTCPSocketRead(msg.Command, msg.szBuffer, msg.wDataSize);
		if (bSuccess == false)
		{
#ifdef _DEBUG
			cocos2d::MessageBox("数据包不匹配!", "警告");
#endif
			this->CloseSocket();		
		}
	}

	if (m_bClose)
	{
		if (m_pTcpCallback != NULL)
		{
			m_pTcpCallback->OnEventTCPSocketShut();
		}
		m_bClose = false;
	}
	
}

void CTcpClientThread::CloseSocket()
{
	//结束前把容器中的数据包给清除
	m_vecMsgInfo.clear();

	// 接收 缓存索引 归零
	this->m_RecvBufferUsedCounter = 0;
	this->m_dwSendPacketCount = 0;

	if(this->m_Socket < 0)
	{
		return;
	}	

#if (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)

	shutdown(this->m_Socket, SD_BOTH);
	closesocket(this->m_Socket);
	this->m_Socket = INVALID_SOCKET;

#else	

	shutdown(this->m_Socket, 2);
	close(this->m_Socket);
	this->m_Socket = -1;

#endif		
	m_bClose = true;
	
}
