#include "LockPassWord.h"
#include "LobbySocketSink.h"
#include "ClientSocketSink.h"


LockPassWord::LockPassWord(GameScene *pGameScene):GameLayer(pGameScene)
{
	m_TableLockShow = false;
	m_IsHavePsw = false;
}

LockPassWord::~LockPassWord()
{

}

Menu* LockPassWord::CreateButton(std::string szBtName ,const Vec2 &p , int tag )
{
    auto sp1 = Sprite::createWithSpriteFrameName(StrToChar(szBtName+"_normal.png"));
    auto sp2 = Sprite::createWithSpriteFrameName(StrToChar(szBtName+"_normal.png"));
    sp2->setColor(Color3B(150,150,150));
    sp2->setScale(0.96f);
    auto pMenuItem1 = MenuItemSprite::create(sp1, sp2,CC_CALLBACK_1(LockPassWord::callbackBt, this));
    pMenuItem1->setTag(tag);
    // 创建菜单对象，是存放菜单项的容器
    Menu* menu = Menu::create(pMenuItem1, NULL);
    // 注意:默认情况下，Menu是忽略锚点的
    //    this->ignoreAnchorPointForPosition(false);
    menu->setPosition(p);
    return menu;
}

void LockPassWord::SetTableArr(int tableId, int chairid)
{
    m_chairid = chairid;
    m_tableid = tableId;
}

void LockPassWord::callbackBt(Ref *pSender)
{
    Node *pNode = (Node *)pSender;
    int tga = pNode->getTag();
    if (tga == PassWordOk)
    {
        LoadLayer::create(m_pGameScene);
		ClientSocketSink::sharedSocketSink()->SitdownReq(m_pUserPassWord->getText(), m_tableid, m_chairid);
        ShowTableLock(false);
	}
	else if (tga == PassWordCancel)
	{
		ShowTableLock(false);
	}
}

bool LockPassWord::onTouchBegan(Touch *pTouch, Event *pEvent)
{
    log("LockPassWord::onTouchBegan");
    
    auto touchPosition = Director::getInstance()->convertToGL(pTouch->getLocation());
    
    auto rect = getChildByTag(1995)->getBoundingBox();
    if (rect.containsPoint(touchPosition)) return false;
    
    return true;
}

void LockPassWord::onTouchMoved(Touch *pTouch, Event *pEvent)
{
    
}

void LockPassWord::onTouchEnded(Touch *pTouch, Event *pEvent)
{
}

void LockPassWord::ShowTableLock(bool _show)
{
     _eventDispatcher->removeEventListener(listener);
    this->removeFromParent();
}

bool LockPassWord::init()
{
	if (!Layer::init())
	{
		return false;
	}
    

	Sprite *LockBack = Sprite::createWithSpriteFrameName("bg_tongyongkuang2.png");
	LockBack->setPosition(_STANDARD_SCREEN_CENTER_IN_PIXELS_);
	addChild(LockBack);
    LockBack->setTag(1995);
   
    ui::Scale9Sprite* passBg = ui::Scale9Sprite::createWithSpriteFrameName("bg_shuru.png");
    passBg->setContentSize(Size(550, 100));
    passBg->setPosition(Vec2(400, 270));
    LockBack->addChild(passBg);
    
    std::string str = "请输入密码：";
    Tools::StringTransform(str);
    const Size inputSize = Size(450,100);
    m_pUserPassWord = cocos2d::ui::EditBox::create(inputSize, INPUT_BACKGROUND);
    m_pUserPassWord->setFontName(_GAME_FONT_NAME_1_);
    m_pUserPassWord->setFontSize(46);
    m_pUserPassWord->setFontColor(_GAME_FONT_COLOR_2_);
    m_pUserPassWord->setPlaceholderFontName(_GAME_FONT_NAME_1_);
    m_pUserPassWord->setPlaceHolder(str.c_str());
    m_pUserPassWord->setPlaceholderFontSize(46);
    m_pUserPassWord->setPlaceholderFontColor(_GAME_FONT_COLOR_2_);
    m_pUserPassWord->setInputFlag(cocos2d::ui::EditBox::InputFlag::PASSWORD);
    m_pUserPassWord->setPosition(Vec2(400, 270));
    m_pUserPassWord->setMaxLength(20);
    LockBack->addChild(m_pUserPassWord);
    
    m_LockOk = CreateButton("btn_queding" ,Vec2(600,90),PassWordOk);
	LockBack->addChild(m_LockOk);

	m_LockCanCel = CreateButton("btn_quxiao" ,Vec2(230,90),PassWordCancel);
	LockBack->addChild(m_LockCanCel);
    
    listener = EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(true);
    listener->onTouchBegan = CC_CALLBACK_2(LockPassWord::onTouchBegan, this);
    listener->onTouchMoved = CC_CALLBACK_2(LockPassWord::onTouchMoved, this);
    listener->onTouchEnded = CC_CALLBACK_2(LockPassWord::onTouchEnded, this);
    _eventDispatcher->addEventListenerWithFixedPriority(listener, -5);
    return true;
}

LockPassWord* LockPassWord::create(GameScene *pGameScene)
{
	LockPassWord* temp = new LockPassWord(pGameScene);
	if(temp && temp->init())
	{
		temp->autorelease();
		return temp;
	}
	else
	{
		CC_SAFE_DELETE(temp);
		return NULL;
	}
}

void LockPassWord::onEnter()
{
	GameLayer::onEnter();
}

void LockPassWord::onExit()
{
	GameLayer::onExit();
}
