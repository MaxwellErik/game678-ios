﻿#ifndef  _FRAMEGAMEVIEW_H_
#define  _FRAMEGAMEVIEW_H_

#include "GameLayer.h"
#include "SoundUtil.h"
#include "GameGetScore.h"

#define TAG_CONNECT_BT									1000000

class TopLayer;
class FrameGameView : public GameLayer
{
friend class ClientSocketSink;

public:
	~FrameGameView(void);

	//发送接口
public:
	//显示消息
	virtual int ShowMessageBox(const char * pszMessage, UINT nType);
	//发送函数
	virtual bool SendData(WORD wSubCmdID);
	//发送函数
	virtual bool SendData(WORD wSubCmdID, void * pData, WORD wDataSize);
	//发送函数
	virtual bool SendMobileData(WORD wSubCmdID);
	//发送函数
	virtual bool SendMobileData(WORD wSubCmdID, void * pData, WORD wDataSize);

	//用户事件
public:
	//用户进入
	virtual void OnEventUserEnter(tagUserData * pUserData, WORD wChairID, bool bLookonUser);
	//用户离开
	virtual void OnEventUserLeave(tagUserData * pUserData, WORD wChairID, bool bLookonUser);
	//用户积分
	virtual void OnEventUserScore(tagUserData * pUserData, WORD wChairID, bool bLookonUser);
	//用户状态
	virtual void OnEventUserStatus(tagUserData * pUserData, WORD wChairID, bool bLookonUser);
    //用户换桌
    virtual void OnEventUserChangeTable(tagUserData * pUserData, WORD wChairID, bool bLookonUser);
	//网络消息
	//游戏消息
	virtual bool OnGameMessage(WORD wSubCmdID, const void * pBuffer, WORD wDataSize);
	//框架消息
	virtual bool OnFrameMessage(WORD wSubCmdID, const void * pBuffer, WORD wDataSize);
	//场景消息
	virtual bool OnGameSceneMessage(BYTE cbGameStatus, const void * pBuffer, WORD wDataSize);

	virtual void OnReconnectAction();

	virtual void setGameStatus(BYTE cbGameStatus);

	virtual BYTE getGameStatus() { return m_cbGameStatus; }

	virtual void AddPlayerInfo(){};

	const tagUserData * GetUserData(int chair);
	

public:
	const tagUserData * GetMeUserData();

	void delayConnect(float fd);
	void displayConnect();
	void removeConnect();
	void connectWaitlong(float fd);
	void reconnect();
	void beamreconnect(Ref *pSender=NULL);	//重连按钮
	void removeconnect();
	void connectFait(float fd=0);
	bool isConnecting();
	virtual void backLoginView(Ref *pSender){}
	void AddTopString(const char *pstr);
public:
	void SendGameScene(BYTE cbScene);
	WORD GetMeChairID();
	void SetKindId(WORD KindId);
	
	

protected:
	static BYTE						m_cbGameStatus;						//游戏状态
	LONGLONG						m_lCellScore;
	WORD							m_wServerType;
	bool							m_bDealerSpeak;
	TopLayer						*m_pTopLayer;						//顶上的跑马灯
	static int						m_iReconnectCount;					//重连次数
	WORD							m_KindId;
	
protected:
	FrameGameView(GameScene *pGameScene);

};

class GameLayer;
class GameFunChooseUILayer;
class GameMenuUILayer;
class BtnMenuLayer;

enum enGameState
{
	enGameNormal=0,
	enGameCallBank,
	enGameBet,
	enGameSendCard,
	enGameOpenCard,
	enGameEnd,
};

class IGameView : public FrameGameView
{
	friend class ClientSocketSink;
public:
	BYTE			m_cbNextScene;
	BYTE			m_cbCurrentGame;
	enGameState		m_GameState;
public:
	// 响应手机快捷键
	virtual void onEnter();
	virtual void onExit();

	void onTimeUpdateUserScore( float delta );
	virtual bool updateUserScore();//更新用户游戏币
	virtual void UpdateDrawUserScore(){};		//更新显示的游戏币数量
	virtual void OnEventUserScore( tagUserData * pUserData, WORD wChairID, bool bLookonUser );
	virtual void backLoginView(Ref *pSender);
	void BackToLobby();
	int getRand(int start,int end);

	bool GetScoreForBank();

protected:
	IGameView(GameScene *pGameScene);
	~IGameView(void);
	static SoundUtil * shareSoundEngine();


protected:
	GameFunChooseUILayer	*m_pGameFunChoose;
	int						m_iUpdateIndex;
	bool					m_IsExit;
};

#endif
