
--------------------------------
-- @module PlatUtil
-- @parent_module custom

--------------------------------
-- 
-- @function [parent=#PlatUtil] connectServer 
-- @param self
-- @param #unsigned int clientId
-- @param #string ip
-- @param #unsigned int port
-- @param #unsigned int heartDelayTime
        
--------------------------------
-- 
-- @function [parent=#PlatUtil] closeServer 
-- @param self
-- @param #unsigned int clientId
        
--------------------------------
-- 
-- @function [parent=#PlatUtil] uncompressZip 
-- @param self
-- @param #string zipFilePath
-- @param #string dirPath
-- @return bool#bool ret (return value: bool)
        
--------------------------------
-- 
-- @function [parent=#PlatUtil] setMessageParsePerFrame 
-- @param self
-- @param #unsigned int data
        
--------------------------------
-- 
-- @function [parent=#PlatUtil] UnicodeToUtf8 
-- @param self
-- @param #CodeChange str
-- @return string#string ret (return value: string)
        
--------------------------------
-- 
-- @function [parent=#PlatUtil] getMD5String 
-- @param self
-- @param #string str
-- @return string#string ret (return value: string)
        
--------------------------------
-- 
-- @function [parent=#PlatUtil] Utf8ToUnicode 
-- @param self
-- @param #CodeChange str
        
--------------------------------
-- 
-- @function [parent=#PlatUtil] sendMessage 
-- @param self
-- @param #unsigned int clientId
-- @param #custom.NetMessage msg
        
--------------------------------
-- 
-- @function [parent=#PlatUtil] createDirectory 
-- @param self
-- @param #char folderPath
-- @return bool#bool ret (return value: bool)
        
return nil
