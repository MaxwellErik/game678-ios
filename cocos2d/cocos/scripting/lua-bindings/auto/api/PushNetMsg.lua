
--------------------------------
-- @module PushNetMsg
-- @parent_module 

--------------------------------
-- 
-- @function [parent=#PushNetMsg] clear 
-- @param self
        
--------------------------------
-- 
-- @function [parent=#PushNetMsg] setMsgLen 
-- @param self
-- @param #unsigned int msgLen
        
--------------------------------
-- 
-- @function [parent=#PushNetMsg] getDataAtIndex 
-- @param self
-- @param #unsigned int index
-- @return unsigned char#unsigned char ret (return value: unsigned char)
        
--------------------------------
-- 
-- @function [parent=#PushNetMsg] getMsgLen 
-- @param self
-- @return unsigned int#unsigned int ret (return value: unsigned int)
        
--------------------------------
-- 
-- @function [parent=#PushNetMsg] setDataAtIndex 
-- @param self
-- @param #unsigned int index
-- @param #unsigned char chData
        
--------------------------------
-- @overload self, unsigned int, char         
-- @overload self         
-- @function [parent=#PushNetMsg] PushNetMsg
-- @param self
-- @param #unsigned int msgLen
-- @param #char pData

return nil
