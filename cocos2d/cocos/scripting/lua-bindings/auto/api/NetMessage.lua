
--------------------------------
-- @module NetMessage
-- @parent_module custom

--------------------------------
-- 
-- @function [parent=#NetMessage] setMsgLen 
-- @param self
-- @param #unsigned int msgLen
        
--------------------------------
-- 
-- @function [parent=#NetMessage] clear 
-- @param self
        
--------------------------------
-- 
-- @function [parent=#NetMessage] getMainID 
-- @param self
-- @return unsigned int#unsigned int ret (return value: unsigned int)
        
--------------------------------
-- 
-- @function [parent=#NetMessage] getMsgLen 
-- @param self
-- @return unsigned int#unsigned int ret (return value: unsigned int)
        
--------------------------------
-- 
-- @function [parent=#NetMessage] setClientId 
-- @param self
-- @param #unsigned int clientId
        
--------------------------------
-- 
-- @function [parent=#NetMessage] setMainID 
-- @param self
-- @param #unsigned int mainId
        
--------------------------------
-- 
-- @function [parent=#NetMessage] getDataAtIndex 
-- @param self
-- @param #unsigned int index
-- @return unsigned char#unsigned char ret (return value: unsigned char)
        
--------------------------------
-- 
-- @function [parent=#NetMessage] getClientId 
-- @param self
-- @return unsigned int#unsigned int ret (return value: unsigned int)
        
--------------------------------
-- 
-- @function [parent=#NetMessage] getSubId 
-- @param self
-- @return unsigned int#unsigned int ret (return value: unsigned int)
        
--------------------------------
-- 
-- @function [parent=#NetMessage] setSubId 
-- @param self
-- @param #unsigned int subId
        
--------------------------------
-- 
-- @function [parent=#NetMessage] setDataAtIndex 
-- @param self
-- @param #unsigned int index
-- @param #unsigned char chData
        
--------------------------------
-- @overload self, unsigned int, unsigned int, unsigned int, char, unsigned int         
-- @overload self         
-- @function [parent=#NetMessage] NetMessage
-- @param self
-- @param #unsigned int mainId
-- @param #unsigned int subId
-- @param #unsigned int msgLen
-- @param #char pData
-- @param #unsigned int clientId

return nil
