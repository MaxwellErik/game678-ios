--------------------------------
-- @module custom

--------------------------------------------------------
-- the custom CodeChange
-- @field [parent=#custom] CodeChange#CodeChange CodeChange preloaded module


--------------------------------------------------------
-- the custom FuncAction
-- @field [parent=#custom] FuncAction#FuncAction FuncAction preloaded module


--------------------------------------------------------
-- the custom NetMessage
-- @field [parent=#custom] NetMessage#NetMessage NetMessage preloaded module


--------------------------------------------------------
-- the custom PlatUtil
-- @field [parent=#custom] PlatUtil#PlatUtil PlatUtil preloaded module


--------------------------------------------------------
-- the custom LLongCode
-- @field [parent=#custom] LLongCode#LLongCode LLongCode preloaded module


--------------------------------------------------------
-- the custom CSVHelper
-- @field [parent=#custom] CSVHelper#CSVHelper CSVHelper preloaded module


--------------------------------------------------------
-- the custom XMLParser
-- @field [parent=#custom] XMLParser#XMLParser XMLParser preloaded module


return nil
