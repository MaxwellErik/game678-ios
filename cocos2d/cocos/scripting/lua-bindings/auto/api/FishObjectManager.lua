
--------------------------------
-- @module FishObjectManager
-- @extend Ref
-- @parent_module fishgame

--------------------------------
-- 
-- @function [parent=#FishObjectManager] FindFish 
-- @param self
-- @param #unsigned long 
-- @return Fish#Fish ret (return value: fishgame.Fish)
        
--------------------------------
-- 
-- @function [parent=#FishObjectManager] ConvertCoortToScreenSize 
-- @param self
-- @param #float 
-- @param #float 
        
--------------------------------
-- 
-- @function [parent=#FishObjectManager] Init 
-- @param self
-- @param #int 
-- @param #int 
-- @param #int 
-- @param #int 
        
--------------------------------
-- 
-- @function [parent=#FishObjectManager] IsSwitchingScene 
-- @param self
-- @return bool#bool ret (return value: bool)
        
--------------------------------
-- 
-- @function [parent=#FishObjectManager] GetServerWidth 
-- @param self
-- @return int#int ret (return value: int)
        
--------------------------------
-- 
-- @function [parent=#FishObjectManager] MirrowShow 
-- @param self
-- @return bool#bool ret (return value: bool)
        
--------------------------------
-- 
-- @function [parent=#FishObjectManager] FindBullet 
-- @param self
-- @param #unsigned long id
-- @return Bullet#Bullet ret (return value: fishgame.Bullet)
        
--------------------------------
-- 
-- @function [parent=#FishObjectManager] SetGameLoaded 
-- @param self
-- @param #bool b
        
--------------------------------
-- 
-- @function [parent=#FishObjectManager] AddFishBuff 
-- @param self
-- @param #int buffType
-- @param #float buffParam
-- @param #float buffTime
        
--------------------------------
-- 
-- @function [parent=#FishObjectManager] RemoveAllBullets 
-- @param self
-- @param #bool 
-- @return bool#bool ret (return value: bool)
        
--------------------------------
-- 
-- @function [parent=#FishObjectManager] SetSwitchingScene 
-- @param self
-- @param #bool b
        
--------------------------------
-- 
-- @function [parent=#FishObjectManager] Clear 
-- @param self
        
--------------------------------
-- 
-- @function [parent=#FishObjectManager] IsGameLoaded 
-- @param self
-- @return bool#bool ret (return value: bool)
        
--------------------------------
-- 
-- @function [parent=#FishObjectManager] RemoveAllFishes 
-- @param self
-- @param #bool 
-- @return bool#bool ret (return value: bool)
        
--------------------------------
-- 
-- @function [parent=#FishObjectManager] AddFish 
-- @param self
-- @param #fishgame.Fish pFish
-- @return bool#bool ret (return value: bool)
        
--------------------------------
-- 
-- @function [parent=#FishObjectManager] ConvertCoord 
-- @param self
-- @param #float 
-- @param #float 
        
--------------------------------
-- 
-- @function [parent=#FishObjectManager] GetAllFishes 
-- @param self
-- @return array_table#array_table ret (return value: array_table)
        
--------------------------------
-- 
-- @function [parent=#FishObjectManager] AddBullet 
-- @param self
-- @param #fishgame.Bullet pBullet
-- @return bool#bool ret (return value: bool)
        
--------------------------------
-- 
-- @function [parent=#FishObjectManager] GetServerHeight 
-- @param self
-- @return int#int ret (return value: int)
        
--------------------------------
-- 
-- @function [parent=#FishObjectManager] OnUpdate 
-- @param self
-- @param #float dt
-- @return bool#bool ret (return value: bool)
        
--------------------------------
-- 
-- @function [parent=#FishObjectManager] ConvertCoortToCleientPosition 
-- @param self
-- @param #float 
-- @param #float 
        
--------------------------------
-- 
-- @function [parent=#FishObjectManager] ConvertDirection 
-- @param self
-- @param #float 
-- @return float#float ret (return value: float)
        
--------------------------------
-- 
-- @function [parent=#FishObjectManager] SetMirrowShow 
-- @param self
-- @param #bool 
        
--------------------------------
-- 
-- @function [parent=#FishObjectManager] ConvertMirrorCoord 
-- @param self
-- @param #float 
-- @param #float 
        
--------------------------------
-- 
-- @function [parent=#FishObjectManager] DestoryInstace 
-- @param self
        
--------------------------------
-- 
-- @function [parent=#FishObjectManager] GetInstance 
-- @param self
-- @return FishObjectManager#FishObjectManager ret (return value: fishgame.FishObjectManager)
        
return nil
