
--------------------------------
-- @module MyObject
-- @extend Ref
-- @parent_module fishgame

--------------------------------
-- 
-- @function [parent=#MyObject] SetMoveCompent 
-- @param self
-- @param #fishgame.MoveCompent 
        
--------------------------------
-- 
-- @function [parent=#MyObject] GetOwner 
-- @param self
-- @return MyObject#MyObject ret (return value: fishgame.MyObject)
        
--------------------------------
-- 
-- @function [parent=#MyObject] SetOwner 
-- @param self
-- @param #fishgame.MyObject p
        
--------------------------------
-- 
-- @function [parent=#MyObject] OnMoveEnd 
-- @param self
        
--------------------------------
-- 
-- @function [parent=#MyObject] GetMoveCompent 
-- @param self
-- @return MoveCompent#MoveCompent ret (return value: fishgame.MoveCompent)
        
--------------------------------
-- 
-- @function [parent=#MyObject] SetId 
-- @param self
-- @param #unsigned long newId
        
--------------------------------
-- 
-- @function [parent=#MyObject] OnClear 
-- @param self
-- @param #bool 
        
--------------------------------
-- 
-- @function [parent=#MyObject] SetManager 
-- @param self
-- @param #fishgame.FishObjectManager manager
        
--------------------------------
-- 
-- @function [parent=#MyObject] GetTarget 
-- @param self
-- @return int#int ret (return value: int)
        
--------------------------------
-- 
-- @function [parent=#MyObject] GetDirection 
-- @param self
-- @return float#float ret (return value: float)
        
--------------------------------
-- 
-- @function [parent=#MyObject] SetState 
-- @param self
-- @param #int 
        
--------------------------------
-- 
-- @function [parent=#MyObject] SetDirection 
-- @param self
-- @param #float dir
        
--------------------------------
-- 
-- @function [parent=#MyObject] Clear 
-- @param self
-- @param #bool 
-- @param #bool noCleanNode
        
--------------------------------
-- 
-- @function [parent=#MyObject] GetId 
-- @param self
-- @return unsigned long#unsigned long ret (return value: unsigned long)
        
--------------------------------
-- 
-- @function [parent=#MyObject] GetManager 
-- @param self
-- @return FishObjectManager#FishObjectManager ret (return value: fishgame.FishObjectManager)
        
--------------------------------
-- 
-- @function [parent=#MyObject] AddBuff 
-- @param self
-- @param #int buffType
-- @param #float buffParam
-- @param #float buffTime
        
--------------------------------
-- 
-- @function [parent=#MyObject] GetState 
-- @param self
-- @return int#int ret (return value: int)
        
--------------------------------
-- 
-- @function [parent=#MyObject] SetTarget 
-- @param self
-- @param #int i
        
--------------------------------
-- 
-- @function [parent=#MyObject] GetType 
-- @param self
-- @return int#int ret (return value: int)
        
--------------------------------
-- 
-- @function [parent=#MyObject] OnUpdate 
-- @param self
-- @param #float fdt
        
--------------------------------
-- 
-- @function [parent=#MyObject] SetPosition 
-- @param self
-- @param #float x
-- @param #float y
        
--------------------------------
-- 
-- @function [parent=#MyObject] InSideScreen 
-- @param self
-- @return bool#bool ret (return value: bool)
        
--------------------------------
-- 
-- @function [parent=#MyObject] GetPosition 
-- @param self
-- @return vec2_table#vec2_table ret (return value: vec2_table)
        
return nil
