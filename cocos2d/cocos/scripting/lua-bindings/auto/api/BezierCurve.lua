
--------------------------------
-- @module BezierCurve
-- @extend Ref
-- @parent_module custom

--------------------------------
-- 
-- @function [parent=#BezierCurve] Bezier2D 
-- @param self
-- @param #array_table initX
-- @param #array_table initY
-- @param #int posCount
-- @param #int initCount
-- @param #float fDistance
-- @return array_table#array_table ret (return value: array_table)
        
--------------------------------
-- 
-- @function [parent=#BezierCurve] create 
-- @param self
-- @return BezierCurve#BezierCurve ret (return value: custom.BezierCurve)
        
--------------------------------
-- 
-- @function [parent=#BezierCurve] BezierCurve 
-- @param self
        
return nil
