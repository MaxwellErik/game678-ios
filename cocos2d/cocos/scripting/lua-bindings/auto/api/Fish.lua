
--------------------------------
-- @module Fish
-- @extend MyObject
-- @parent_module fishgame

--------------------------------
-- 
-- @function [parent=#Fish] OnUpdate 
-- @param self
-- @param #float fdt
        
--------------------------------
-- 
-- @function [parent=#Fish] GetVisualId 
-- @param self
-- @return int#int ret (return value: int)
        
--------------------------------
-- 
-- @function [parent=#Fish] GetMaxRadio 
-- @param self
-- @return int#int ret (return value: int)
        
--------------------------------
-- 
-- @function [parent=#Fish] OnHit 
-- @param self
        
--------------------------------
-- 
-- @function [parent=#Fish] SetVisualId 
-- @param self
-- @param #int id
        
--------------------------------
-- 
-- @function [parent=#Fish] GetBoundingBox 
-- @param self
-- @return int#int ret (return value: int)
        
--------------------------------
-- 
-- @function [parent=#Fish] SetBoundingBox 
-- @param self
-- @param #int 
        
--------------------------------
-- 
-- @function [parent=#Fish] Create 
-- @param self
-- @return Fish#Fish ret (return value: fishgame.Fish)
        
return nil
