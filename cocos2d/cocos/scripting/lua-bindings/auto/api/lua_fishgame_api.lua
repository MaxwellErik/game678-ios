--------------------------------
-- @module fishgame

--------------------------------------------------------
-- the fishgame MyScene
-- @field [parent=#fishgame] MyScene#MyScene MyScene preloaded module


--------------------------------------------------------
-- the fishgame FishObjectManager
-- @field [parent=#fishgame] FishObjectManager#FishObjectManager FishObjectManager preloaded module


--------------------------------------------------------
-- the fishgame FishUtils
-- @field [parent=#fishgame] FishUtils#FishUtils FishUtils preloaded module


--------------------------------------------------------
-- the fishgame MyObject
-- @field [parent=#fishgame] MyObject#MyObject MyObject preloaded module


--------------------------------------------------------
-- the fishgame Fish
-- @field [parent=#fishgame] Fish#Fish Fish preloaded module


--------------------------------------------------------
-- the fishgame Bullet
-- @field [parent=#fishgame] Bullet#Bullet Bullet preloaded module


--------------------------------------------------------
-- the fishgame MoveCompent
-- @field [parent=#fishgame] MoveCompent#MoveCompent MoveCompent preloaded module


--------------------------------------------------------
-- the fishgame MoveByPath
-- @field [parent=#fishgame] MoveByPath#MoveByPath MoveByPath preloaded module


--------------------------------------------------------
-- the fishgame MoveByDirection
-- @field [parent=#fishgame] MoveByDirection#MoveByDirection MoveByDirection preloaded module


return nil
