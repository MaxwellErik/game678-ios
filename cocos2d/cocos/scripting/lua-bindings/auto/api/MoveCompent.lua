
--------------------------------
-- @module MoveCompent
-- @extend Ref
-- @parent_module fishgame

--------------------------------
-- 
-- @function [parent=#MoveCompent] GetOffest 
-- @param self
-- @return vec2_table#vec2_table ret (return value: vec2_table)
        
--------------------------------
-- 
-- @function [parent=#MoveCompent] GetDelay 
-- @param self
-- @return float#float ret (return value: float)
        
--------------------------------
-- 
-- @function [parent=#MoveCompent] bTroop 
-- @param self
-- @return bool#bool ret (return value: bool)
        
--------------------------------
-- 
-- @function [parent=#MoveCompent] GetSpeed 
-- @param self
-- @return float#float ret (return value: float)
        
--------------------------------
-- 
-- @function [parent=#MoveCompent] GetOwner 
-- @param self
-- @return MyObject#MyObject ret (return value: fishgame.MyObject)
        
--------------------------------
-- 
-- @function [parent=#MoveCompent] SetOwner 
-- @param self
-- @param #fishgame.MyObject owner
        
--------------------------------
-- 
-- @function [parent=#MoveCompent] IsPaused 
-- @param self
-- @return bool#bool ret (return value: bool)
        
--------------------------------
-- 
-- @function [parent=#MoveCompent] SetPathID 
-- @param self
-- @param #int pid
-- @param #bool bt
        
--------------------------------
-- 
-- @function [parent=#MoveCompent] HasBeginMove 
-- @param self
-- @return bool#bool ret (return value: bool)
        
--------------------------------
-- 
-- @function [parent=#MoveCompent] OnUpdate 
-- @param self
-- @param #float dt
        
--------------------------------
-- 
-- @function [parent=#MoveCompent] IsEndPath 
-- @param self
-- @return bool#bool ret (return value: bool)
        
--------------------------------
-- 
-- @function [parent=#MoveCompent] Rebound 
-- @param self
-- @return bool#bool ret (return value: bool)
        
--------------------------------
-- 
-- @function [parent=#MoveCompent] SetDirection 
-- @param self
-- @param #float dir
        
--------------------------------
-- 
-- @function [parent=#MoveCompent] SetPause 
-- @param self
        
--------------------------------
-- 
-- @function [parent=#MoveCompent] OnDetach 
-- @param self
        
--------------------------------
-- 
-- @function [parent=#MoveCompent] GetPathID 
-- @param self
-- @return int#int ret (return value: int)
        
--------------------------------
-- 
-- @function [parent=#MoveCompent] SetDelay 
-- @param self
-- @param #float f
        
--------------------------------
-- 
-- @function [parent=#MoveCompent] SetRebound 
-- @param self
-- @param #bool b
        
--------------------------------
-- 
-- @function [parent=#MoveCompent] SetSpeed 
-- @param self
-- @param #float sp
        
--------------------------------
-- 
-- @function [parent=#MoveCompent] InitMove 
-- @param self
        
--------------------------------
-- 
-- @function [parent=#MoveCompent] OnAttach 
-- @param self
        
--------------------------------
-- 
-- @function [parent=#MoveCompent] SetOffest 
-- @param self
-- @param #vec2_table pt
        
--------------------------------
-- 
-- @function [parent=#MoveCompent] SetEndPath 
-- @param self
-- @param #bool be
        
--------------------------------
-- 
-- @function [parent=#MoveCompent] SetPosition 
-- @param self
-- @param #float x
-- @param #float y
        
return nil
