LOCAL_PATH := $(call my-dir)
CC_USE_CURL := 1
CC_USE_CCSTUDIO := 1
CC_USE_SPINE := 1
CC_USE_3D := 1
CC_USE_CCBUILDER := 1
#NDK_DEBUG := 1
-stdlib=libc++
include $(CLEAR_VARS)

$(call import-add-path,$(LOCAL_PATH)/../../cocos2d)
$(call import-add-path,$(LOCAL_PATH)/../../cocos2d/external)
$(call import-add-path,$(LOCAL_PATH)/../../cocos2d/cocos)
$(call import-add-path,$(LOCAL_PATH)/../../Classes)

LOCAL_MODULE := cocos2dcpp_shared

LOCAL_MODULE_FILENAME := libcocos2dcpp

FILE_LIST := hellocpp/main.cpp  
FILE_LIST += hellocpp/Runtime_android.cpp
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/*.cpp)
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/fishgame/*.cpp)  
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/gameDDZ/*.cpp) 
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/gameBR30S/*.cpp)  
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/gameBRNN/*.cpp)  
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/gameHHSW/*.cpp)  
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/gameOx2/*.cpp) 
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/gameOx4/*.cpp) 
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/gameOx6/*.cpp)
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/gameSDB/*.cpp) 
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/gameShowHand/*.cpp) 
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/gamesk/*.cpp) 
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/gameThreeKing/*.cpp) 
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/gameWZMJ/*.cpp)
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/gameWZLZ/*.cpp)
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/net/*.cpp) 
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/runtime/*.cpp) 
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/runtime/*.c) 
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/runtime/*.cc) 
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/utils/*.cpp) 
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/View/*.cpp)  
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/quick-src/extra/apptools/*.cpp)  
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/quick-src/extra/crypto/*.cpp)  
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/quick-src/extra/crypto/base64/*.c)  
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/quick-src/extra/crypto/md5/*.c)
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/quick-src/extra/filters/filters/*.cpp)
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/quick-src/extra/filters/nodes/*.cpp)
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/quick-src/extra/filters/shaders/*.cpp)
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/quick-src/extra/luabinding/*.cpp)
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/quick-src/extra/nanovg/nanonode/*.cpp)
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/quick-src/extra/nanovg/nanovg/*.cpp)
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/quick-src/extra/nanovg/nanovg/*.c)
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/quick-src/extra/network/*.cpp)
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/quick-src/extra/native/*.cpp)
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/quick-src/extra/platform/android/*.cpp)
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/quick-src/extra/store/*.cpp) 
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/quick-src/lua_extensions/*.c) 
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/quick-src/lua_extensions/cjson/*.c)
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/quick-src/lua_extensions/debugger/*.c)
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/quick-src/lua_extensions/filesystem/*.c)
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/quick-src/lua_extensions/lpack/*.c)
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/quick-src/lua_extensions/lsqlite3/*.c)
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/quick-src/lua_extensions/zlib/*.c)
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/quick-src/ProjectConfig/*.cpp)

LOCAL_SRC_FILES := $(FILE_LIST:$(LOCAL_PATH)/%=%)  
LOCAL_C_INCLUDES := $(LOCAL_PATH)/../../Classes
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../Classes/utils
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../Classes/runtime
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../Classes/fishgame
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../cocos2d/cocos
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../cocos2d/external/protobuf-lite/src
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../cocos2d/cocos/scripting/lua-bindings/manual
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../cocos2d/cocos/scripting/lua-bindings/manual/cocos2d
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../cocos2d/external/lua/lua
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../cocos2d/external/lua/tolua
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../cocos2d/cocos/2d
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../cocos2d/cocos/platform/third_party/android/prebuilt/libcurl/include
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../cocos2d/external/protobuf-lite/src
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../Classes/quick-src/extra
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../Classes/quick-src/lua_extensions
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../Classes/quick-src/ProjectConfig

LOCAL_STATIC_LIBRARIES := cocos2dx_static

LOCAL_WHOLE_STATIC_LIBRARIES := libiconv_static
LOCAL_WHOLE_STATIC_LIBRARIES += cocos_protobuf_lite_static    
LOCAL_WHOLE_STATIC_LIBRARIES += cocos_network_static
LOCAL_WHOLE_STATIC_LIBRARIES += cocosbuilder_static 
LOCAL_WHOLE_STATIC_LIBRARIES += cocostudio_static 
LOCAL_WHOLE_STATIC_LIBRARIES += spine_static 
LOCAL_WHOLE_STATIC_LIBRARIES += cocos2d_lua_android_static  
LOCAL_WHOLE_STATIC_LIBRARIES += cocos2d_lua_static 
#LOCAL_WHOLE_STATIC_LIBRARIES += extra_static 
#LOCAL_WHOLE_STATIC_LIBRARIES += lua_extensions_static 

LOCAL_SHyORT_COMMANDS := true  
include $(BUILD_SHARED_LIBRARY)

$(call import-module,.)
$(call import-module,libiconv)
$(call import-module,protobuf-lite)  
$(call import-module,network) 
$(call import-module,editor-support/cocosbuilder) 
$(call import-module,editor-support/cocostudio) 
$(call import-module,editor-support/spine) 
#$(call import-module,quick-src/extra) 
#$(call import-module,quick-src/lua_extensions)
$(call import-module,scripting/lua-bindings/proj.android)
