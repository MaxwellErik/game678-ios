package com.merchant.android;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.text.DecimalFormat;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;

/**
 * 工具类
 * 
 */
public class BaseHelper {

	/***
	 * 启动新的activity
	 * 
	 * @param fromActivity
	 * @param targetClass
	 */
	public static void startActivity(Activity fromActivity, Class targetClass,
			String name, Serializable objectValue, boolean finishFlag) {
		Intent intent = new Intent(fromActivity, targetClass);
		intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
		if (objectValue != null)
			intent.putExtra(name, objectValue);
		fromActivity.startActivity(intent);
		fromActivity.overridePendingTransition(android.R.anim.fade_in,
				android.R.anim.fade_out);
		if (finishFlag) {
			fromActivity.finish();
		}
	}

	/**
	 * 流转字符串方法
	 * 
	 * @param is
	 * @return
	 */
	public static String convertStreamToString(InputStream is) {
		BufferedReader reader = new BufferedReader(new InputStreamReader(is));
		StringBuilder sb = new StringBuilder();
		String line = null;
		try {
			while ((line = reader.readLine()) != null) {
				sb.append(line);
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				is.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return sb.toString();
	}

	/**
	 * 获取权限
	 * 
	 * @param permission
	 *            权限
	 * @param path
	 *            路径
	 */
	public static void chmod(String permission, String path) {
		try {
			String command = "chmod " + permission + " " + path;
			Runtime runtime = Runtime.getRuntime();
			runtime.exec(command);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static ProgressDialog getProgressDialog(Context context) {
		ProgressDialog localProgressDialog = new ProgressDialog(context);
		localProgressDialog.setTitle("");
		localProgressDialog.setMessage("正在发送请求...");
		localProgressDialog.setIndeterminate(true);
		localProgressDialog.setCancelable(false);
		return localProgressDialog;
	}

	/**
	 * 显示进度条
	 * 
	 * @param context
	 *            环境
	 * @param title
	 *            标题
	 * @param message
	 *            信息
	 * @param indeterminate
	 *            确定性
	 * @param cancelable
	 *            可撤销
	 * @return
	 */
	public static ProgressDialog showProgress(Context context,
			CharSequence title, CharSequence message, boolean indeterminate,
			boolean cancelable) {
		ProgressDialog dialog = new ProgressDialog(context);
		dialog.setTitle(title);
		dialog.setMessage(message);
		dialog.setIndeterminate(indeterminate);
		dialog.setCancelable(false);
		dialog.show();
		return dialog;
	}

	public static ProgressDialog createProgressDialog(Context context,
			String message) {
		ProgressDialog localProgressDialog = new ProgressDialog(context);
		localProgressDialog.setTitle("");
		localProgressDialog.setMessage(message);
		localProgressDialog.setIndeterminate(true);
		localProgressDialog.setCancelable(false);
		return localProgressDialog;
	}

	public static String formatTimeStr(String timeStr) {
		StringBuilder builder = new StringBuilder();
		builder.append(timeStr.substring(0, 4));// �?
		builder.append("-").append(timeStr.substring(4, 6));// �?
		builder.append("-").append(timeStr.substring(6, 8));// �?
		builder.append(" ").append(timeStr.substring(8, 10));// �?
		builder.append(":").append(timeStr.substring(10, 12));// �?
		builder.append(":").append(timeStr.substring(12, 14));// �?
		return builder.toString();
	}

	public static String fomatAmount(String amountStr) {
		if (!TextUtils.isEmpty(amountStr)) {
			double amount = Double.parseDouble(amountStr);
			DecimalFormat myformat = new DecimalFormat();
			myformat.applyPattern("000000000.00");
			return myformat.format(amount);
		} else {
			return null;
		}

	}
}