package com.merchant.android;

import android.text.Editable;
import android.text.TextWatcher;
/***
 * 
 * @author huangaixiu
 *
 */
public class EditTextGroupWatch implements TextWatcher {
	private String string = "";
	private int groupPosition;

	public EditTextGroupWatch(int groupPosition) {
		this.groupPosition = groupPosition;
	}

	public void afterTextChanged(Editable editable) {
		String editString = editable.toString();
		editString = editString.replace(" ", "");
		int number = editString.length() / groupPosition;

		if (editString.length() % groupPosition == 0
				&& editString.length() >= groupPosition) {
			number--;
		}
		for (int i = 0; i < number; i++) {
			String big = editString.substring(0, groupPosition * (i + 1) + i);
			String end = editString.substring(groupPosition * (i + 1) + i);
			editString = big + " " + end;

		}
		if (!string.equals(editable.toString())) {
			editable.delete(0, editable.length());
			string = editString;
			editable.append(editString);
		}

	}

	@Override
	public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
			int arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {
		// TODO Auto-generated method stub
		
	}
}
