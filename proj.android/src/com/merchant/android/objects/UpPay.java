package com.merchant.android.objects;

public class UpPay extends MerchantObject {
	/** 应用名称 */
	private String application;
	/***/
	private String version;
	/** 商户名称 */
	private String merchantName;
	/** 商户代码 */
	private String merchantId;
	/** 商户订单号 */
	private String merchantOrderId;
	/** 商户订单时间 */
	private String merchantOrderTime;
	/** 商户订单金额 */
	private String merchantOrderAmt;
	/** 商户订单描述 */
	private String merchantOrderDesc;
	/** 交易超时时间 */
	private String transTimeout;
	/** 商户通知URL */
	private String backEndUrl;
	
	private String connectType;
	/** 商户广播Action */
	private String backAction;
	/** 签名 */
	private String sign;
	/** 商户公钥证书 */
	private String merchantPublicCert;
	/** 清算日期 */
	private String settleDate;
	/** 清算金额 */
	private String setlAmt;
	/** 清算币种 */
	private String setlCurrency;
	/** 清算汇率 */
	private String converRate;
	/** Cups交易流水号 */
	private String cupsQid;
	/** Cups系统跟踪号 */
	private String cupsTraceNum;
	/** Cups系统跟踪时间 */
	private String cupsTraceTime;
	/** Cups响应码 */
	private String cupsRespCode;
	/** 附加信息 */
	private String msgExt;
	/** 自定义保留域 */
	private String misc;
	/** 应答码 */
	private String respCode;
	/** 应答码描述 */
	private String respDesc;

	/** 银行卡后四位 */
	private String pan;
	/** 手机号码 */
	private String mobileNumber;

	public UpPay() {
		super();
	}

	public UpPay(String application, String merchantName, String merchantId,
			String merchantOrderId, String merchantOrderTime,
			String merchantOrderAmt, String merchantOrderDesc,
			String transTimeout, String backEndUrl, String backAction,
			String sign, String merchantPublicCert, String settleDate,
			String setlAmt, String setlCurrency, String converRate,
			String cupsQid, String cupsTraceNum, String cupsTraceTime,
			String cupsRespCode, String msgExt, String misc, String respCode,
			String respDesc, String pan, String mobileNumber) {
		super();
		this.application = application;
		this.merchantName = merchantName;
		this.merchantId = merchantId;
		this.merchantOrderId = merchantOrderId;
		this.merchantOrderTime = merchantOrderTime;
		this.merchantOrderAmt = merchantOrderAmt;
		this.merchantOrderDesc = merchantOrderDesc;
		this.transTimeout = transTimeout;
		this.backEndUrl = backEndUrl;
		this.backAction = backAction;
		this.sign = sign;
		this.merchantPublicCert = merchantPublicCert;
		this.settleDate = settleDate;
		this.setlAmt = setlAmt;
		this.setlCurrency = setlCurrency;
		this.converRate = converRate;
		this.cupsQid = cupsQid;
		this.cupsTraceNum = cupsTraceNum;
		this.cupsTraceTime = cupsTraceTime;
		this.cupsRespCode = cupsRespCode;
		this.msgExt = msgExt;
		this.misc = misc;
		this.respCode = respCode;
		this.respDesc = respDesc;
		this.pan = pan;
		this.mobileNumber = mobileNumber;
	}

	public String getApplication() {
		return application;
	}

	public void setApplication(String application) {
		this.application = application;
	}

	public String getMerchantName() {
		return merchantName;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public void setMerchantName(String merchantName) {
		this.merchantName = merchantName;
	}

	public String getMerchantId() {
		return merchantId;
	}

	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}

	public String getMerchantOrderId() {
		return merchantOrderId;
	}

	public void setMerchantOrderId(String merchantOrderId) {
		this.merchantOrderId = merchantOrderId;
	}

	public String getMerchantOrderTime() {
		return merchantOrderTime;
	}

	public void setMerchantOrderTime(String merchantOrderTime) {
		this.merchantOrderTime = merchantOrderTime;
	}

	public String getMerchantOrderAmt() {
		return merchantOrderAmt;
	}

	public void setMerchantOrderAmt(String merchantOrderAmt) {
		this.merchantOrderAmt = merchantOrderAmt;
	}

	public String getMerchantOrderDesc() {
		return merchantOrderDesc;
	}

	public void setMerchantOrderDesc(String merchantOrderDesc) {
		this.merchantOrderDesc = merchantOrderDesc;
	}

	public String getTransTimeout() {
		return transTimeout;
	}

	public void setTransTimeout(String transTimeout) {
		this.transTimeout = transTimeout;
	}

	public String getBackEndUrl() {
		return backEndUrl;
	}

	public void setBackEndUrl(String backEndUrl) {
		this.backEndUrl = backEndUrl;
	}

	public String getBackAction() {
		return backAction;
	}

	public void setBackAction(String backAction) {
		this.backAction = backAction;
	}

	public String getSign() {
		return sign;
	}

	public void setSign(String sign) {
		this.sign = sign;
	}

	public String getMerchantPublicCert() {
		return merchantPublicCert;
	}

	public void setMerchantPublicCert(String merchantPublicCert) {
		this.merchantPublicCert = merchantPublicCert;
	}

	public String getSettleDate() {
		return settleDate;
	}

	public void setSettleDate(String settleDate) {
		this.settleDate = settleDate;
	}

	public String getSetlAmt() {
		return setlAmt;
	}

	public void setSetlAmt(String setlAmt) {
		this.setlAmt = setlAmt;
	}

	public String getSetlCurrency() {
		return setlCurrency;
	}

	public void setSetlCurrency(String setlCurrency) {
		this.setlCurrency = setlCurrency;
	}

	public String getConverRate() {
		return converRate;
	}

	public void setConverRate(String converRate) {
		this.converRate = converRate;
	}

	public String getCupsQid() {
		return cupsQid;
	}

	public void setCupsQid(String cupsQid) {
		this.cupsQid = cupsQid;
	}

	public String getCupsTraceNum() {
		return cupsTraceNum;
	}

	public void setCupsTraceNum(String cupsTraceNum) {
		this.cupsTraceNum = cupsTraceNum;
	}

	public String getCupsTraceTime() {
		return cupsTraceTime;
	}

	public void setCupsTraceTime(String cupsTraceTime) {
		this.cupsTraceTime = cupsTraceTime;
	}

	public String getCupsRespCode() {
		return cupsRespCode;
	}

	public void setCupsRespCode(String cupsRespCode) {
		this.cupsRespCode = cupsRespCode;
	}

	public String getMsgExt() {
		return msgExt;
	}

	public void setMsgExt(String msgExt) {
		this.msgExt = msgExt;
	}

	public String getMisc() {
		return misc;
	}

	public void setMisc(String misc) {
		this.misc = misc;
	}

	public String getRespCode() {
		return respCode;
	}

	public void setRespCode(String respCode) {
		this.respCode = respCode;
	}

	public String getRespDesc() {
		return respDesc;
	}

	public void setRespDesc(String respDesc) {
		this.respDesc = respDesc;
	}

	public String getPan() {
		return pan;
	}

	public void setPan(String pan) {
		this.pan = pan;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((application == null) ? 0 : application.hashCode());
		result = prime * result
				+ ((backAction == null) ? 0 : backAction.hashCode());
		result = prime * result
				+ ((backEndUrl == null) ? 0 : backEndUrl.hashCode());
		result = prime * result
				+ ((converRate == null) ? 0 : converRate.hashCode());
		result = prime * result + ((cupsQid == null) ? 0 : cupsQid.hashCode());
		result = prime * result
				+ ((cupsRespCode == null) ? 0 : cupsRespCode.hashCode());
		result = prime * result
				+ ((cupsTraceNum == null) ? 0 : cupsTraceNum.hashCode());
		result = prime * result
				+ ((cupsTraceTime == null) ? 0 : cupsTraceTime.hashCode());
		result = prime * result
				+ ((merchantId == null) ? 0 : merchantId.hashCode());
		result = prime * result
				+ ((merchantName == null) ? 0 : merchantName.hashCode());
		result = prime
				* result
				+ ((merchantOrderAmt == null) ? 0 : merchantOrderAmt.hashCode());
		result = prime
				* result
				+ ((merchantOrderDesc == null) ? 0 : merchantOrderDesc
						.hashCode());
		result = prime * result
				+ ((merchantOrderId == null) ? 0 : merchantOrderId.hashCode());
		result = prime
				* result
				+ ((merchantOrderTime == null) ? 0 : merchantOrderTime
						.hashCode());
		result = prime
				* result
				+ ((merchantPublicCert == null) ? 0 : merchantPublicCert
						.hashCode());
		result = prime * result + ((misc == null) ? 0 : misc.hashCode());
		result = prime * result
				+ ((mobileNumber == null) ? 0 : mobileNumber.hashCode());
		result = prime * result + ((msgExt == null) ? 0 : msgExt.hashCode());
		result = prime * result + ((pan == null) ? 0 : pan.hashCode());
		result = prime * result
				+ ((respCode == null) ? 0 : respCode.hashCode());
		result = prime * result
				+ ((respDesc == null) ? 0 : respDesc.hashCode());
		result = prime * result + ((setlAmt == null) ? 0 : setlAmt.hashCode());
		result = prime * result
				+ ((setlCurrency == null) ? 0 : setlCurrency.hashCode());
		result = prime * result
				+ ((settleDate == null) ? 0 : settleDate.hashCode());
		result = prime * result + ((sign == null) ? 0 : sign.hashCode());
		result = prime * result
				+ ((transTimeout == null) ? 0 : transTimeout.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UpPay other = (UpPay) obj;
		if (application == null) {
			if (other.application != null)
				return false;
		} else if (!application.equals(other.application))
			return false;
		if (backAction == null) {
			if (other.backAction != null)
				return false;
		} else if (!backAction.equals(other.backAction))
			return false;
		if (backEndUrl == null) {
			if (other.backEndUrl != null)
				return false;
		} else if (!backEndUrl.equals(other.backEndUrl))
			return false;
		if (converRate == null) {
			if (other.converRate != null)
				return false;
		} else if (!converRate.equals(other.converRate))
			return false;
		if (cupsQid == null) {
			if (other.cupsQid != null)
				return false;
		} else if (!cupsQid.equals(other.cupsQid))
			return false;
		if (cupsRespCode == null) {
			if (other.cupsRespCode != null)
				return false;
		} else if (!cupsRespCode.equals(other.cupsRespCode))
			return false;
		if (cupsTraceNum == null) {
			if (other.cupsTraceNum != null)
				return false;
		} else if (!cupsTraceNum.equals(other.cupsTraceNum))
			return false;
		if (cupsTraceTime == null) {
			if (other.cupsTraceTime != null)
				return false;
		} else if (!cupsTraceTime.equals(other.cupsTraceTime))
			return false;
		if (merchantId == null) {
			if (other.merchantId != null)
				return false;
		} else if (!merchantId.equals(other.merchantId))
			return false;
		if (merchantName == null) {
			if (other.merchantName != null)
				return false;
		} else if (!merchantName.equals(other.merchantName))
			return false;
		if (merchantOrderAmt == null) {
			if (other.merchantOrderAmt != null)
				return false;
		} else if (!merchantOrderAmt.equals(other.merchantOrderAmt))
			return false;
		if (merchantOrderDesc == null) {
			if (other.merchantOrderDesc != null)
				return false;
		} else if (!merchantOrderDesc.equals(other.merchantOrderDesc))
			return false;
		if (merchantOrderId == null) {
			if (other.merchantOrderId != null)
				return false;
		} else if (!merchantOrderId.equals(other.merchantOrderId))
			return false;
		if (merchantOrderTime == null) {
			if (other.merchantOrderTime != null)
				return false;
		} else if (!merchantOrderTime.equals(other.merchantOrderTime))
			return false;
		if (merchantPublicCert == null) {
			if (other.merchantPublicCert != null)
				return false;
		} else if (!merchantPublicCert.equals(other.merchantPublicCert))
			return false;
		if (misc == null) {
			if (other.misc != null)
				return false;
		} else if (!misc.equals(other.misc))
			return false;
		if (mobileNumber == null) {
			if (other.mobileNumber != null)
				return false;
		} else if (!mobileNumber.equals(other.mobileNumber))
			return false;
		if (msgExt == null) {
			if (other.msgExt != null)
				return false;
		} else if (!msgExt.equals(other.msgExt))
			return false;
		if (pan == null) {
			if (other.pan != null)
				return false;
		} else if (!pan.equals(other.pan))
			return false;
		if (respCode == null) {
			if (other.respCode != null)
				return false;
		} else if (!respCode.equals(other.respCode))
			return false;
		if (respDesc == null) {
			if (other.respDesc != null)
				return false;
		} else if (!respDesc.equals(other.respDesc))
			return false;
		if (setlAmt == null) {
			if (other.setlAmt != null)
				return false;
		} else if (!setlAmt.equals(other.setlAmt))
			return false;
		if (setlCurrency == null) {
			if (other.setlCurrency != null)
				return false;
		} else if (!setlCurrency.equals(other.setlCurrency))
			return false;
		if (settleDate == null) {
			if (other.settleDate != null)
				return false;
		} else if (!settleDate.equals(other.settleDate))
			return false;
		if (sign == null) {
			if (other.sign != null)
				return false;
		} else if (!sign.equals(other.sign))
			return false;
		if (transTimeout == null) {
			if (other.transTimeout != null)
				return false;
		} else if (!transTimeout.equals(other.transTimeout))
			return false;
		return true;
	}

	public String getConnectType() {
		return connectType;
	}

	public void setConnectType(String connectType) {
		this.connectType = connectType;
	}

}
