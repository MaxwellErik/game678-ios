package com.merchant.android.objects;

public class SubmitOrder extends MerchantObject {
	/** 应用名称 */
	private String application;
	/** 版本号 */
	private String version;
	/** 商户名称 */
	private String merchantName;
	/** 商户代码 */
	private String merchantId;
	/** 商户订单号 */
	private String merchantOrderId;
	/** 商户订单时间 */
	private String merchantOrderTime;
	/** 商户订单金额 */
	private String merchantOrderAmt;
	/** 银行卡号，彩票行业 */
	private String pan;
	/** 商户订单描述 */
	private String merchantOrderDesc;
	/** 交易超时时间 */
	private String transTimeout;
	/** 商户通知URL */
	private String backEndUrl;
	/** 签名 */
	private String sign;
	/** 商户公钥证书 */
	private String merchantPublicCert;
	/** 附加信息 */
	private String msgExt;
	/** 自定义保留域 */
	private String misc;
	/** 应答码 */
	private String respCode;
	/** 应答码描述 */
	private String respDesc;

	public SubmitOrder() {
		super();
	}

	public SubmitOrder(String application, String version, String merchantName,
			String merchantId, String merchantOrderId,
			String merchantOrderTime, String merchantOrderAmt, String pan,
			String merchantOrderDesc, String transTimeout, String backEndUrl,
			String sign, String merchantPublicCert, String msgExt, String misc,
			String respCode, String respDesc) {
		super();
		this.application = application;
		this.version = version;
		this.merchantName = merchantName;
		this.merchantId = merchantId;
		this.merchantOrderId = merchantOrderId;
		this.merchantOrderTime = merchantOrderTime;
		this.merchantOrderAmt = merchantOrderAmt;
		this.pan = pan;
		this.merchantOrderDesc = merchantOrderDesc;
		this.transTimeout = transTimeout;
		this.backEndUrl = backEndUrl;
		this.sign = sign;
		this.merchantPublicCert = merchantPublicCert;
		this.msgExt = msgExt;
		this.misc = misc;
		this.respCode = respCode;
		this.respDesc = respDesc;
	}

	public String getApplication() {
		return application;
	}

	public void setApplication(String application) {
		this.application = application;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getMerchantName() {
		return merchantName;
	}

	public void setMerchantName(String merchantName) {
		this.merchantName = merchantName;
	}

	public String getMerchantId() {
		return merchantId;
	}

	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}

	public String getMerchantOrderId() {
		return merchantOrderId;
	}

	public void setMerchantOrderId(String merchantOrderId) {
		this.merchantOrderId = merchantOrderId;
	}

	public String getMerchantOrderTime() {
		return merchantOrderTime;
	}

	public void setMerchantOrderTime(String merchantOrderTime) {
		this.merchantOrderTime = merchantOrderTime;
	}

	public String getMerchantOrderAmt() {
		return merchantOrderAmt;
	}

	public void setMerchantOrderAmt(String merchantOrderAmt) {
		this.merchantOrderAmt = merchantOrderAmt;
	}

	public String getPan() {
		return pan;
	}

	public void setPan(String pan) {
		this.pan = pan;
	}

	public String getMerchantOrderDesc() {
		return merchantOrderDesc;
	}

	public void setMerchantOrderDesc(String merchantOrderDesc) {
		this.merchantOrderDesc = merchantOrderDesc;
	}

	public String getTransTimeout() {
		return transTimeout;
	}

	public void setTransTimeout(String transTimeout) {
		this.transTimeout = transTimeout;
	}

	public String getBackEndUrl() {
		return backEndUrl;
	}

	public void setBackEndUrl(String backEndUrl) {
		this.backEndUrl = backEndUrl;
	}

	public String getSign() {
		return sign;
	}

	public void setSign(String sign) {
		this.sign = sign;
	}

	public String getMerchantPublicCert() {
		return merchantPublicCert;
	}

	public void setMerchantPublicCert(String merchantPublicCert) {
		this.merchantPublicCert = merchantPublicCert;
	}

	public String getMsgExt() {
		return msgExt;
	}

	public void setMsgExt(String msgExt) {
		this.msgExt = msgExt;
	}

	public String getMisc() {
		return misc;
	}

	public void setMisc(String misc) {
		this.misc = misc;
	}

	public String getRespCode() {
		return respCode;
	}

	public void setRespCode(String respCode) {
		this.respCode = respCode;
	}

	public String getRespDesc() {
		return respDesc;
	}

	public void setRespDesc(String respDesc) {
		this.respDesc = respDesc;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((application == null) ? 0 : application.hashCode());
		result = prime * result
				+ ((backEndUrl == null) ? 0 : backEndUrl.hashCode());
		result = prime * result
				+ ((merchantId == null) ? 0 : merchantId.hashCode());
		result = prime * result
				+ ((merchantName == null) ? 0 : merchantName.hashCode());
		result = prime
				* result
				+ ((merchantOrderAmt == null) ? 0 : merchantOrderAmt.hashCode());
		result = prime
				* result
				+ ((merchantOrderDesc == null) ? 0 : merchantOrderDesc
						.hashCode());
		result = prime * result
				+ ((merchantOrderId == null) ? 0 : merchantOrderId.hashCode());
		result = prime
				* result
				+ ((merchantOrderTime == null) ? 0 : merchantOrderTime
						.hashCode());
		result = prime
				* result
				+ ((merchantPublicCert == null) ? 0 : merchantPublicCert
						.hashCode());
		result = prime * result + ((misc == null) ? 0 : misc.hashCode());
		result = prime * result + ((msgExt == null) ? 0 : msgExt.hashCode());
		result = prime * result + ((pan == null) ? 0 : pan.hashCode());
		result = prime * result
				+ ((respCode == null) ? 0 : respCode.hashCode());
		result = prime * result
				+ ((respDesc == null) ? 0 : respDesc.hashCode());
		result = prime * result + ((sign == null) ? 0 : sign.hashCode());
		result = prime * result
				+ ((transTimeout == null) ? 0 : transTimeout.hashCode());
		result = prime * result + ((version == null) ? 0 : version.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SubmitOrder other = (SubmitOrder) obj;
		if (application == null) {
			if (other.application != null)
				return false;
		} else if (!application.equals(other.application))
			return false;
		if (backEndUrl == null) {
			if (other.backEndUrl != null)
				return false;
		} else if (!backEndUrl.equals(other.backEndUrl))
			return false;
		if (merchantId == null) {
			if (other.merchantId != null)
				return false;
		} else if (!merchantId.equals(other.merchantId))
			return false;
		if (merchantName == null) {
			if (other.merchantName != null)
				return false;
		} else if (!merchantName.equals(other.merchantName))
			return false;
		if (merchantOrderAmt == null) {
			if (other.merchantOrderAmt != null)
				return false;
		} else if (!merchantOrderAmt.equals(other.merchantOrderAmt))
			return false;
		if (merchantOrderDesc == null) {
			if (other.merchantOrderDesc != null)
				return false;
		} else if (!merchantOrderDesc.equals(other.merchantOrderDesc))
			return false;
		if (merchantOrderId == null) {
			if (other.merchantOrderId != null)
				return false;
		} else if (!merchantOrderId.equals(other.merchantOrderId))
			return false;
		if (merchantOrderTime == null) {
			if (other.merchantOrderTime != null)
				return false;
		} else if (!merchantOrderTime.equals(other.merchantOrderTime))
			return false;
		if (merchantPublicCert == null) {
			if (other.merchantPublicCert != null)
				return false;
		} else if (!merchantPublicCert.equals(other.merchantPublicCert))
			return false;
		if (misc == null) {
			if (other.misc != null)
				return false;
		} else if (!misc.equals(other.misc))
			return false;
		if (msgExt == null) {
			if (other.msgExt != null)
				return false;
		} else if (!msgExt.equals(other.msgExt))
			return false;
		if (pan == null) {
			if (other.pan != null)
				return false;
		} else if (!pan.equals(other.pan))
			return false;
		if (respCode == null) {
			if (other.respCode != null)
				return false;
		} else if (!respCode.equals(other.respCode))
			return false;
		if (respDesc == null) {
			if (other.respDesc != null)
				return false;
		} else if (!respDesc.equals(other.respDesc))
			return false;
		if (sign == null) {
			if (other.sign != null)
				return false;
		} else if (!sign.equals(other.sign))
			return false;
		if (transTimeout == null) {
			if (other.transTimeout != null)
				return false;
		} else if (!transTimeout.equals(other.transTimeout))
			return false;
		if (version == null) {
			if (other.version != null)
				return false;
		} else if (!version.equals(other.version))
			return false;
		return true;
	}

}
