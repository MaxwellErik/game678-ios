package com.merchant.android.parse;

import java.io.InputStream;

public interface IXmlParseService {

	public Object getByParseXml(InputStream is, Class classObject, int classType)
			throws Exception;

	public Object getByParseXml(InputStream is, Class classObject)
			throws Exception;

}
