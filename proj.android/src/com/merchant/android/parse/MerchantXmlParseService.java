package com.merchant.android.parse;

import java.io.InputStream;
import java.lang.reflect.Field;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.merchant.android.objects.MerchantObject;

import android.util.Log;

public class MerchantXmlParseService extends XmlParseService {
	public static final int MERCHANT_OBJECT = 1;

	public Object getByParseXml(InputStream is, Class classObject, int classType)
			throws Exception {
		SAXParserFactory factory = SAXParserFactory.newInstance();
		SAXParser parser = factory.newSAXParser();
		SaxHandler handler = new SaxHandler(classObject, classType);
		parser.parse(is, handler);
		is.close();
		return handler.getData();
	}

	private static class SaxHandler extends DefaultHandler {
		private static final String TAG = "PandaPaySaxHandler";
		private static final String ROOT_TAG = "pomp";
		private int classType;
		private Class classObject;

		private MerchantObject merchantResultObj;
		private MerchantObject merchantTempObj;
		private String currentTag;
		private StringBuffer currentValue = new StringBuffer();

		public SaxHandler(Class classObject, int classType) {
			this.classObject = classObject;
			this.classType = classType;
		}

		private Object getData() {
			if (classType == MERCHANT_OBJECT) {
				return merchantResultObj;
			} else {
				return null;
			}

		}

		@Override
		public void startDocument() throws SAXException {
		}

		@Override
		public void endDocument() throws SAXException {
			super.endDocument();
		}

		@Override
		public void startElement(String uri, String localName, String qName,
				Attributes attributes) throws SAXException {
			currentValue.delete(0, currentValue.length());
			if (ROOT_TAG.equals(qName)) {
				try {
					if (classType == MERCHANT_OBJECT) {
						merchantTempObj = (MerchantObject) classObject
								.newInstance();
						String application = attributes.getValue("application");
						if (application != null) {
							Field field = classObject
									.getDeclaredField("application");
							field.setAccessible(true);
							field.set(merchantTempObj, application);
						}
					}

				} catch (Exception e) {
					Log.i(TAG, "xml analysis fail");
					e.printStackTrace();
				}
			}
			currentTag = qName;
		}

		@Override
		public void characters(char[] ch, int start, int length)
				throws SAXException {
			if (currentTag != null) {
				String s = new String(ch, start, length);
				currentValue.append(s);
			}
		}

		@Override
		public void endElement(String uri, String localName, String qName) {
			String value = currentValue.toString().trim();
			Log.i("===currentValue======", value + "  " + currentTag);
			if (value != null && !value.equals("\n\t\t\t")) {
				Field[] fields = classObject.getDeclaredFields();
				for (Field field : fields) {
					field.setAccessible(true);
					if (field.getName().equals(currentTag)) {
						try {
							if (classType == MERCHANT_OBJECT) {
								field.set(merchantTempObj, value);
							}
							currentTag = null;
						} catch (Exception e) {
							Log.i(TAG, "xml analysis fail");
							e.printStackTrace();
						}
						break;
					}
				}
			}
			if (ROOT_TAG.equals(qName)) {
				if (classType == MERCHANT_OBJECT) {
					merchantResultObj = merchantTempObj;
					merchantTempObj = null;
				}
			}
			currentTag = null;
		}

	}

}