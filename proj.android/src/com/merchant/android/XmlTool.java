package com.merchant.android;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.StringWriter;
import java.lang.reflect.Field;
import org.xmlpull.v1.XmlPullParserFactory;
import org.xmlpull.v1.XmlSerializer;

import android.util.Log;

import com.merchant.android.objects.MerchantObject;
import com.merchant.android.parse.MerchantXmlParseService;

public class XmlTool {

	public static String objectToXml(MerchantObject basicObj) {
		StringWriter writer = new StringWriter();
		try {
			XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
			XmlSerializer serializer = factory.newSerializer();

			serializer.setOutput(writer);
			serializer.startDocument("UTF-8", false);
			Class dataClass = basicObj.getClass();
			Field[] fields = dataClass.getDeclaredFields();
			serializer.startTag("", "pomp");

			Field application = dataClass.getDeclaredField("application");
			application.setAccessible(true);
			serializer.attribute(null, "application",
					String.valueOf(application.get(basicObj)));
			try {
				Field version = dataClass.getDeclaredField("version");
				version.setAccessible(true);
				serializer.attribute(null, "version",
						String.valueOf(version.get(basicObj)));
			} catch (NoSuchFieldException e) {
				Log.i("XmlTool", "no such version field");
			}

			for (Field field : fields) {
				field.setAccessible(true);
				String fieldName = field.getName();
				if ((!fieldName.equals("application"))
						&& (!fieldName.equals("version"))) {
					Object fieldValue = field.get(basicObj);
					if (fieldValue != null) {
						String value = String.valueOf(fieldValue);
						serializer.startTag("", fieldName);
						serializer.text(value);
						serializer.endTag("", fieldName);
					}
				}
			}
			serializer.endTag("", "pomp");

			// 关闭文档
			serializer.endDocument();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return writer.toString();
	}

	public static Object xmlToObject(String xmlString, Class objectClass,
			int classType) {
		Object obj = null;
		if (xmlString != null) {
			try {
				byte[] bytes = xmlString.getBytes();
				InputStream is = new ByteArrayInputStream(bytes);
				MerchantXmlParseService saxParse = new MerchantXmlParseService();
				obj = saxParse.getByParseXml(is, objectClass, classType);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return obj;
	}
}
