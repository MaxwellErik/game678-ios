package com.merchant.android;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;

public class VersionManageHelper {
	private static final String SERVER_NEW_APK_URL = "";
	private static final String NEW_APK_SAVE_NAME = "new_plugin";
	private static ProgressDialog pBar;

	public static void updateApk(Activity context, boolean isShowNote) {
		int serverVersion = getServerVersionCode();
		int currentVersion = getVersionCode(context);
		serverVersion = 2;
		if (serverVersion > currentVersion) {
			updateNewVersion(context); // 更新版本
		} else if (isShowNote) {
			showNotify(context); // 提示当前为最新版本
		}

	}

	private static void showNotify(final Activity context) {
		StringBuffer builder = new StringBuffer();
		builder.append("当前版本:");
		builder.append(getVersionName(context));
		builder.append(" Code:");
		builder.append(getVersionCode(context));
		builder.append(",已是最新版,无需更新!");
		Dialog dialog = new AlertDialog.Builder(context).setTitle("软件更新")
				.setMessage(builder.toString())// 设置内容
				.setPositiveButton("确定",// 设置确定按钮
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								dialog.dismiss();
							}
						}).create();// 创建
		// 显示对话框
		dialog.show();
	}

	private static void updateNewVersion(final Activity context) {
		StringBuffer sb = new StringBuffer();
		sb.append("当前版本:");
		sb.append(getVersionName(context));
		sb.append(" Code:");
		sb.append(getVersionCode(context));
		sb.append(", 发现新版本:");
		sb.append(getServerVersionCode());
		sb.append(", 是否更新?");
		Dialog dialog = new AlertDialog.Builder(context)
				.setTitle("软件更新")
				.setMessage(sb.toString())
				// 设置内容
				.setPositiveButton("更新",// 设置确定按钮
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								pBar = new ProgressDialog(context);
								pBar.setTitle("正在下载");
								pBar.setMessage("请稍候...");
								pBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
								downNewApkFromServer(context,
										SERVER_NEW_APK_URL);
							}
						})
				.setNegativeButton("暂不更新",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int whichButton) {
								// 点击"取消"按钮之后退出程序
								dialog.dismiss();
							}
						}).create();// 创建
		// 显示对话框
		dialog.show();
	}

	private static int getServerVersionCode() {
		int versionCode = -1;
		try {

		} catch (Exception e) {
			Log.e("VersionManageHelper", e.getMessage());
		}
		return versionCode;
	}

	private static int getVersionCode(Context context) {
		int verCode = -1;
		try {
			verCode = context.getPackageManager().getPackageInfo(
					"com.payeco.pandapay.android", 0).versionCode;
		} catch (NameNotFoundException e) {
			Log.e("VersionManageHelper", e.getMessage());
		}
		return verCode;
	}

	private static String getVersionName(Context context) {
		String verName = "";
		try {
			verName = context.getPackageManager().getPackageInfo(
					"com.payeco.pandapay.android", 0).versionName;
		} catch (NameNotFoundException e) {
			Log.e("VersionManageHelper", e.getMessage());
		}
		return verName;
	}

	private static void downNewApkFromServer(final Context context,
			final String url) {
		pBar.show();
		new Thread() {
			public void run() {
				HttpClient client = new DefaultHttpClient();
				HttpGet get = new HttpGet(url);
				HttpResponse response;
				try {
					response = client.execute(get);
					HttpEntity entity = response.getEntity();
					long length = entity.getContentLength();
					InputStream is = entity.getContent();
					FileOutputStream fileOutputStream = null;
					if (is != null) {
						File file = new File(
								Environment.getExternalStorageDirectory(),
								NEW_APK_SAVE_NAME);
						fileOutputStream = new FileOutputStream(file);
						byte[] buf = new byte[1024];
						int ch = -1;
						int count = 0;
						while ((ch = is.read(buf)) != -1) {
							fileOutputStream.write(buf, 0, ch);
							count += ch;
							if (length > 0) {
							}
						}
					}
					if(fileOutputStream != null)fileOutputStream.flush();
					if (fileOutputStream != null) {
						fileOutputStream.close();
					}
					installNewApk(context);
				} catch (ClientProtocolException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}.start();
	}

	private static void installNewApk(final Context context) {
		new Handler().post(new Runnable() {
			public void run() {
				pBar.cancel();
				install(context);
			}
		});
	}

	private static void install(Context context) {
		Intent intent = new Intent(Intent.ACTION_VIEW);
		intent.setDataAndType(Uri.fromFile(new File(Environment
				.getExternalStorageDirectory(), NEW_APK_SAVE_NAME)),
				"application/vnd.android.package-archive");
		context.startActivity(intent);
	}
}
