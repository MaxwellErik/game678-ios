package com.aspire;

import android.os.Handler;
import android.os.Message;

import com.game.constant.IDialogConstant;
import com.game.jni.JniHelper;
import com.game.util.PayHelper;

public class IAPHandler extends Handler {

	public static final int INIT_FINISH = 10000;
	public static final int BILL_FINISH = 10001;
	public static final int QUERY_FINISH = 10002;
	public static final int UNSUB_FINISH = 10003;
	public static final int QUERY_START = 10004;

	@Override
	public void handleMessage(Message msg) {
		super.handleMessage(msg);
		int what = msg.what;
		switch (what) {
		case INIT_FINISH:
			JniHelper.showMessage((String) msg.obj);
			break;
		case QUERY_START:
			JniHelper.instance().showDialog(IDialogConstant.WAIT_ACCOUNT);
			try {
				String orderNum = PayHelper.mmQuery((String) msg.obj);
				PayHelper.payCallback(orderNum);
			} catch (Exception e) {
				JniHelper.showMessage(e.getMessage());
			} finally {
				JniHelper.instance().dismissDialog(IDialogConstant.WAIT_ACCOUNT);
			}
			break;
		default:
			break;
		}
	}

	public void showDialog(String title, String msg) {
		JniHelper.showMessage((msg == null) ? "Undefined error" : msg);
	}

}
