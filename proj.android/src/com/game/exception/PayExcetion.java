package com.game.exception;

public class PayExcetion extends RuntimeException{

	protected String code;
	
	public PayExcetion() {
		super();
		// TODO Auto-generated constructor stub
	}

	public PayExcetion(String detailMessage, Throwable throwable) {
		super(detailMessage, throwable);
		// TODO Auto-generated constructor stub
	}

	public PayExcetion(String detailMessage) {
		super(detailMessage);
		// TODO Auto-generated constructor stub
	}

	public PayExcetion(Throwable throwable) {
		super(throwable);
		// TODO Auto-generated constructor stub
	}
	
	public PayExcetion(String code , String detailMessage) {
		super(detailMessage);
		setCode(code);
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

}
