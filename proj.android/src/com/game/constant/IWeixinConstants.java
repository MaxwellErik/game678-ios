package com.game.constant;

public class IWeixinConstants {
	// APP_ID
    public static final String APP_ID = "wx7017d8ee0425f676";

    public static class ShowMsgActivity {
		public static final String STitle = "showmsg_title";
		public static final String SMessage = "showmsg_message";
		public static final String BAThumbData = "showmsg_thumb_data";
	}
}
