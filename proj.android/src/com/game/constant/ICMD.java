package com.game.constant;

public interface ICMD {

	//c++调用java
	/////////////////////////////////////////////////////////////////////////
	public static final int CMD_CTJ_QQAUTHLOGIN	= 1;				//QQ登录
	public static final int CMD_CTJ_QQUSERINFO = 2;					//QQ用户信息
	
	public static final int CMD_CTJ_MILOGIN = 10;					//小米登录
	
	public static final int CMD_CTJ_PAY = 100;						//支付
	public static final int CMD_CTJ_PAY_PATCH = 101;				//补单
	
	public static final int CMD_CTJ_BROWSE = 200;					//浏览器
	public static final int CMD_CTJ_BROWSE_CLOSE = 201;				//关闭浏览器
	public static final int CMD_CTJ_OPENSYSBROWSE = 202;			//打开系统浏览器
	
	public static final int CMD_CTJ_VIBRATE = 210;					//震动
	public static final int CMD_CTJ_READCONTACTS = 220;				//读取联系人
	public static final int CMD_CTJ_SENDMSG = 230;					//发送短信
	public static final int CMD_CTJ_WIFI = 240;						//判断是否有WIFI
	public static final int CMD_CTJ_SENDHTTPREQUEST = 250;			//发送HTTP请求
	public static final int CMD_CTJ_DOWNLOADAPK = 260;				//下载APK
	public static final int CMD_CTJ_UUID = 270;						//获取唯一标识
	public static final int CMD_CTJ_WX = 280;					//分享微信
	public static final int CMD_CTJ_WX_PAY = 281;					//微信支付	public static final int CMD_CTJ_WB = 290;					//分享微博
	public static final int CMD_CTJ_WB = 290;				//微博
	public static final int CMD_COPY_ID = 300;				//复制ID
	public static final int CMD_CloseGame = 301;				//退出游戏
	public static final int CMD_CTJ_GetDeviceID = 302;				//复制ID
	public static final int CMD_CTJ_LockPhone = 303;				//手机锁
	public static final int CMD_CTJ_NeedUpdate = 304;
	public static final int CMD_CTJ_ShareWX = 305;
	public static final int CMD_CTJ_OpenWX = 307;		//打开微信
	public static final int CMD_CTJ_WXLogin = 308;				//微信登陆
	
	//Java调用c++	(回调)
	////////////////////////////////////////////////////////////////////////
	public static final int CMD_JTC_QQAUTHLOGIN = 1;				//QQ登录
	public static final int CMD_JTC_QQUSERINFO = 2;					//QQ用户信息
	
	public static final int CMD_JTC_MILOGIN = 10;					//小米登录回调
	
	public static final int CMD_JTC_PAY = 100;						//支付回调
	public static final int CMD_JTC_PAY_GET_ORDER = 101;			//返回订单编号
	public static final int CMD_JTC_WX_PAY = 103;					//微信支付回调
	
	public static final int CMD_JTC_READCONTACTS = 220;				//读取联系人
	public static final int CMD_JTC_SENDMSG = 230;					//发送短信
	public static final int CMD_JTC_WIFI = 240;						//判断是否有WIFI
	public static final int CMD_JTC_SENDHTTPREQUEST = 250;			//发送HTTP请求回调
	public static final int CMD_JTC_DOWNLOADAPKCANCEL = 260;		//下载APK被取消
	public static final int CMD_JTC_UUID = 270;						//返回唯一标识
	public static final int CMD_JTC_WX = 280;					//分享微信
	public static final int CMD_JTC_WB = 290;					//分享微博
	public static final int CMD_JTC_ChannelId = 300;			//渠道号
	public static final int CMD_JTC_GetDeviceID = 302;
	public static final int CMD_JTC_WXLogin	= 303;				//微信登陆成功	

}
