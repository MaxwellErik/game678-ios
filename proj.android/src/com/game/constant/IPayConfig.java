package com.game.constant;

public interface IPayConfig extends IDialogConstant{
	
	
	//手机锁
	public static final String PhoneLockURL = "http://app.game621.com/S2S/CreateLock.aspx?";
//	public static final String SERVER = "http://116.236.169.10:8080/appstore/";
//	public static final String SERVER = "http://116.236.169.10:8877/";
	
	//充值
	public static final String SERVER = "https://pay.game678.com.cn/";
	
	public static final String PAY_URL = SERVER + "AliAppPayCreateOrder.ashx?";
	public static final String CALLBACK_URL = SERVER + "order/bankcallback.do";
	public static final String NOTIFY_URL = SERVER + "notify_url.aspx?";
	public static final String PATCH_URL = SERVER + "Bill/Query.aspx?";
	public static final String CARD_URL = SERVER + "Bill/Create.aspx?";
	
	public static final String MMQUERY_URL = SERVER + "order/mmshzandroidquery.do?";
	
	public static final String APP_NAME = "678游戏中心";
	public static final int KIND_ID = 1000;
	public static final String GAME_KEY = "DSDNdsfnDF89F0FNg";
	//网银
	///////////////////////////////////////////////////////////////////////////////////////////
	public static final String PUBLIC_KEY = "312000100816359-Signature.cer";

	public static final String KEY_PASS = "11175751";

	public static final String PRIVATE_KEY = "312000100816359-Signature.pfx";
	
	public static final String MERCHANT_NAME = "模拟商户";

	public static final String ORDER_DESC = "测试商品";

	public static final String CONNECTTYPE = "00";
	
	// test
	public static final String[] values = { "202020000225|123456|05023141",
			"102020000028|123456|02064455", "302020000013|123456|02023625" };

	
//	 public static final String[] values = {
//	 "702020000001|F9C1C7C0C13A4C2B|06014635",
//	 "802020000001|0D09C53173D64656|06014432",
//	 "602020000001|4C2C1ABD920F4FFB|05032700" };

	//短信
	///////////////////////////////////////////////////////////////////////////////////////////
	// 计费信息
	public static final String APPID = "300008661062";
	public static final String APPKEY = "CBE6B60C8EBB4D8AD6A185365FCE5029";
	// 计费点信息
	public static final String CODE_5 = "30000866106201";
	public static final String CODE_5_SINGLE = "30000866106202";
	
	//支付宝
	///////////////////////////////////////////////////////////////////////////////////////////

	public static final String SUBJECT = "游戏币";

	public static final String BODY =  APP_NAME+"虚拟游戏币";

}
