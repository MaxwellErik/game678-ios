package com.game.constant;

public interface IDialogConstant {
	
	public static final int SEND_ORDER_REQUEST = 0;
	public static final int RQF_PAY = 1;
	public static final int SHOW_MESSAGE = 2;	
	public static final int DOWNLOAD_CMD = 3;
	public static final int JNI_CALL_BACK = 4;
	public static final int CopyID_REQUEST = 10;
	public static final int CreateDeviceID_REQUEST = 11;
	public static final int LockPhone = 12;
	public static final int CloseGame = 13;
	public static final int UpdateGame = 14;
	public static final int OpenWX = 15;
	public static final int WXLogin = 16;
	
	public static final int SUBMIT_ORDER_DIALOG = 0;
	public static final int LOAD_WEBBROWSE_DIALOG = 1;
	public static final int WAIT_ACCOUNT = 2;

	
}
