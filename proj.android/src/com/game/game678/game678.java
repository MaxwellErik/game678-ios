/****************************************************************************
Copyright (c) 2010-2012 cocos2d-x.org

http://www.cocos2d-x.org

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
****************************************************************************/
package com.game.game678;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.cocos2dx.lib.Cocos2dxActivity;
import org.cocos2dx.lib.Cocos2dxEditText;
import org.cocos2dx.lib.Cocos2dxGLSurfaceView;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.KeyEvent;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.Toast;
import android.content.BroadcastReceiver;
import android.content.ClipData;
import android.content.ClipboardManager; 
import android.content.Context; 
import android.content.IntentFilter;

import com.alipay.android.msp.Result;
import com.game.game678.GetDeviceId;
import com.game.constant.ICMD;
import com.game.constant.IPayConfig;
import com.game.jni.JniHelper;
import com.game.util.AndroidHelper;
import com.game.util.PayHelper;
import com.game.util.UpdateManager;
import com.game.util.WebBrowseProxy;
import com.game.util.WeiboShareProxy;
import com.sina.weibo.sdk.api.share.BaseResponse;
import com.sina.weibo.sdk.api.share.IWeiboHandler;
import com.sina.weibo.sdk.api.share.IWeiboShareAPI;
import com.sina.weibo.sdk.constant.WBConstants;

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class game678 extends Cocos2dxActivity implements IPayConfig,IWeiboHandler.Response{

	public static float powerPercent;
	 BatteryBroadcastReceiver receiver = new BatteryBroadcastReceiver();
	 IntentFilter filter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
	 
	 class BatteryBroadcastReceiver extends BroadcastReceiver{

        @Override
        public void onReceive(Context context, Intent intent) {
            //判断它是否是为电量变化的Broadcast Action
            if(Intent.ACTION_BATTERY_CHANGED.equals(intent.getAction())){
                //获取当前电量
                int level = intent.getIntExtra("level", 0);

                powerPercent =  level / 100.0f;
            }
        }
    }
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		AndroidHelper.init();
	}
        
    public static game678 instance(){
    	return (game678)getContext();
    }

	public Handler mUIHandler = new UIHandler();
    
	public void onCreateLayout(Cocos2dxGLSurfaceView surfaceView,Cocos2dxEditText	editText,
			FrameLayout mFrameLayout) {
		//很明显这里没走的  framelayout这个值就是mContentLayout null的  你看看这里为啥没走  这网太卡了  我不好看的 
		WebBrowseProxy.instance().onCreateLayout(surfaceView,editText, mFrameLayout);
	}

	public Cocos2dxGLSurfaceView onCreateView() {
		Cocos2dxGLSurfaceView glSurfaceView = new Cocos2dxGLSurfaceView(this);
		// TestCpp should create stencil buffer
		glSurfaceView.setEGLConfigChooser(5, 6, 5, 0, 16, 8);

		return glSurfaceView;
	}

	public boolean onKeyDown(int keyCoder, KeyEvent event){
		return WebBrowseProxy.instance().onKeyDown(keyCoder, event);
	}

	class UIHandler extends Handler {
		public UIHandler() {
			super();
		}

		@SuppressLint("NewApi")
		@Override
		public void handleMessage(Message message) {
			switch (message.what) {
			case WXLogin:	//微信登陆成功
			{
				String params = (String) message.obj;
				JniHelper.sendJniCallback(JniHelper.CMD_JTC_WXLogin, params);
				break;
			}
			case UpdateGame:
			{
				Context cc = getContext();
				UpdateManager _manager = new UpdateManager(cc,"http://www.789y.com/down/Game789.apk");
				_manager.checkUpdateInfo1();
				break;
			}
			case LockPhone:
			{
				break;
			}
			case CloseGame:
			{
				System.exit(0);
				break;
			}
			case CreateDeviceID_REQUEST:
			{
				GetDeviceId getDeviceId=new GetDeviceId((game678)getContext());
				String CombinedID=getDeviceId.getCombinedId();
				Log.i("CombinedID","cccc|"+CombinedID);
				JniHelper.sendJniCallback(JniHelper.CMD_JTC_GetDeviceID, CombinedID);
				break;
			}
			case OpenWX: //打开微信
			{
				Intent intent = getPackageManager().getLaunchIntentForPackage("com.tencent.mm");  
				startActivity(intent);    
				break;
			}
			case SEND_ORDER_REQUEST: 
			{
				String params = (String) message.obj;
				JniHelper.handlerPayRequest(Integer.parseInt(params.split(",")[5]), params);
				break;
			}
			case CopyID_REQUEST:
			{
				//TestPing();
				String params = (String) message.obj;
				ClipboardManager clipboardManager = (ClipboardManager)getSystemService(Context.CLIPBOARD_SERVICE);  
				clipboardManager.setPrimaryClip(ClipData.newPlainText(null, params)); 
				break;
			}
			case RQF_PAY: {
				Result.sResult = (String) message.obj;
				if(Result.checkSuccess()){
					PayHelper.payCallback("0");
				}else{
					Toast.makeText(instance(), Result.getResult(),
							Toast.LENGTH_SHORT).show();
				}
				break;
			}
			case SHOW_MESSAGE:{
				Toast.makeText(instance(), (String)message.obj,
						Toast.LENGTH_SHORT).show();
				break;
			}
			case DOWNLOAD_CMD:
			{
				new UpdateManager(instance(), (String) message.obj).showDownloadDialog();
				break;
			}
			case JNI_CALL_BACK:
			{
				final Object[] param = (Object[])message.obj;
				runOnGLThread(new Runnable() {
					
					@Override
					public void run() {
						JniHelper.callCCommand((Integer)param[0], (String)param[1]);
					}
				});
				
				break;
			}
			default:

			}
		}
	}
	
	
	private static final void TestPing() 
	{
		 String lost = new String();  
		 String delay = new String();  
		 String ip = "g1.game678.com";
		 
		 try {
		 Process p = Runtime.getRuntime().exec("ping -c 1 -w 0.5 " + ip);//ping3次  
		 BufferedReader buf = new BufferedReader(new InputStreamReader(p.getInputStream()));  
		 String str = new String();  
		 while((str=buf.readLine())!=null)
		 {  
		                 if(str.contains("packet loss"))
		                 {  
		                     int i= str.indexOf("received");  
		                     int j= str.indexOf("%");  
		                     System.out.println("丢包率:"+str.substring(i+10, j+1));  
		                     lost = str.substring(i+10, j+1);  
		                 }  
		                 if(str.contains("avg"))
		                 {  
		                     int i=str.indexOf("/", 20);  
		                     int j=str.indexOf(".", i);  
		                     System.out.println("延迟:"+str.substring(i+1, j));  
		                     delay =str.substring(i+1, j);  
		                     delay = delay+"ms";  
		                 }  
		        Log.i("aaaaaaa", str);
		   }  

		 }
		 catch (IOException e)
		 {
			return;
		 } 
	}

	public static void sendMessage(Message msg){
		instance().mUIHandler.sendMessage(msg);
	}

	private ProgressDialog createProgressDialog(String message) {
		ProgressDialog localProgressDialog = new ProgressDialog(instance());
		localProgressDialog.setTitle("");
		localProgressDialog.setMessage(message);
		localProgressDialog.setIndeterminate(true);
		localProgressDialog.setCancelable(false);
		return localProgressDialog;
	}

	@Override
	protected Dialog onCreateDialog(int id) {
		Dialog dialog = null;
		switch (id) {
		case SUBMIT_ORDER_DIALOG:
			dialog = createProgressDialog("正在提交订单信息...");
			break;
		case LOAD_WEBBROWSE_DIALOG:
			dialog = createProgressDialog("正在加载网页...");
			break;
		case WAIT_ACCOUNT:
			dialog = createProgressDialog("等待游戏到账...");
			break;
		default:
			dialog = null;
		}
		return dialog;

	}
	
	/**
	 * 接收微客户端博请求的数据。 当微博客户端唤起当前应用并进行分享时，该方法被调用。
	 * 
	 * @param baseRequest
	 *            微博请求数据对象
	 * @see {@link IWeiboShareAPI#handleWeiboRequest}
	 */
	@Override
	public void onResponse(BaseResponse baseResp) {
		switch (baseResp.errCode) {
		case WBConstants.ErrorCode.ERR_OK:
			JniHelper.sendJniCallback(ICMD.CMD_JTC_WB, "1");
			break;
		case WBConstants.ErrorCode.ERR_CANCEL:
			JniHelper.sendJniCallback(ICMD.CMD_JTC_UUID , "0");
			break;
		default:
			JniHelper.sendJniCallback(ICMD.CMD_JTC_WB, "0");
			break;
		}
	}

	@Override
	protected void onNewIntent(Intent intent) {
		super.onNewIntent(intent);
		setIntent(intent);
		
		WeiboShareProxy.instance().api.handleWeiboResponse(intent, this);
	}
}
