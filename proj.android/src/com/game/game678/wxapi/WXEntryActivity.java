package com.game.game678.wxapi;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.json.JSONObject;
import org.json.JSONArray;

import com.game.constant.IWeixinConstants;
import com.game.jni.JniHelper;
import com.tencent.mm.sdk.constants.ConstantsAPI;
import com.tencent.mm.sdk.modelbase.BaseReq;
import com.tencent.mm.sdk.modelbase.BaseResp;
import com.tencent.mm.sdk.openapi.IWXAPI;
import com.tencent.mm.sdk.openapi.IWXAPIEventHandler;
import com.tencent.mm.sdk.openapi.WXAPIFactory;
import com.tencent.mm.sdk.modelmsg.SendAuth;
import com.tencent.mm.sdk.modelmsg.SendMessageToWX;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

public class WXEntryActivity extends Activity implements IWXAPIEventHandler{
	
    private IWXAPI api;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.pay_result);
    	api = WXAPIFactory.createWXAPI(this, IWeixinConstants.APP_ID);
        api.handleIntent(getIntent(), this);
    }

	@Override
	protected void onNewIntent(Intent intent) {
		super.onNewIntent(intent);
		setIntent(intent);
        api.handleIntent(intent, this);
	}

	@Override
	public void onReq(BaseReq req) {
	}

	@Override
	public void onResp(BaseResp resp) {
		if(resp.getType() == ConstantsAPI.COMMAND_SENDAUTH) {
			SendAuth.Resp authResp = (SendAuth.Resp)resp;
			switch(authResp.errCode) {
			case BaseResp.ErrCode.ERR_OK:
				//发送给服务器
				String code = authResp.code; 
				Log.e("微信登录", "回调成功");
				
				StringBuilder urlStr = new StringBuilder("https://weixin.game678.net.cn/wl/WeixinLogin.ashx?");
				urlStr.append("code=").append(code);
			try{
					byte[] buf = com.game.util.Util.httpGet(urlStr.toString());
					if (buf != null && buf.length > 0) 
					{
						String content = new String(buf);
						JSONObject json = new JSONObject(content);
						String _access_token = json.getString("tokenpass");
						String _openid = json.getString("openid");
					
						StringBuilder _call = new StringBuilder(_openid);
						_call.append(",").append(_access_token);
						JniHelper.sendJniCallback(JniHelper.CMD_JTC_WXLogin, _call.toString()); //登陆成功
						finish();
						return;
					}
					else
					{
						Log.e("微信登陆", "异常:" + String.valueOf(authResp.errCode));
					}
	        	}
			catch(Exception e)
	        	{
	        		Log.e("微信登陆", "异常："+e.getMessage());
	        	}
				break;
			default:
				Log.e("微信分享", "错误:" + String.valueOf(authResp.errCode));
				break;
			}
			finish();
		}
		else if(resp.getType() == ConstantsAPI.COMMAND_SENDMESSAGE_TO_WX) {
			SendMessageToWX.Resp msgResp = ((SendMessageToWX.Resp) resp);
			switch(msgResp.errCode) {
			case BaseResp.ErrCode.ERR_OK:
				Log.e("微信分享", "回调成功");
				break;
			default:
				Log.e("微信分享", "错误:" + String.valueOf(msgResp.errCode));
				break;
			}
			finish();
		}
	}
}