package com.game.util;

import java.net.URLEncoder;

import org.cocos2dx.lib.Cocos2dxActivity;

import android.app.Dialog;
import android.os.AsyncTask;
import android.util.Log;

import com.alipay.android.app.IAlixPay;
import com.alipay.android.msp.Keys;
import com.alipay.android.msp.Result;
import com.alipay.android.msp.Rsa;
import com.alipay.sdk.app.PayTask;
import com.alipay.sdk.auth.AlipaySDK;
import com.game.constant.IPayConfig;
import com.game.jni.JniHelper;

public class AlipayHelper implements Keys , IPayConfig {
	
	public static final String TAG = "AlipayHelper";

	static Cocos2dxActivity mActivity;
	
	private static AlipayHelper helper;
	
	protected AlipayHelper(Cocos2dxActivity mActivity){
		AlipayHelper.mActivity = mActivity;
	}
	
	public static AlipayHelper instance(){
		if(helper == null) helper = new AlipayHelper(JniHelper.instance());
		return helper;
	}

	public void sendAlipay(final String params) {
		
		new AsyncTask<Void, Void, Void>() {

			@Override
			protected Void doInBackground(Void... aa) {
				try {
					final String orderNum = PayHelper.sendOrderPay(params);
//					PayHelper.getOrderCallback(orderNum);
							
					// start the pay.
					Result.sResult = null;
					Log.i(TAG, "info = " + orderNum);
					final String orderInfo = orderNum;// 订单信息

					PayTask alipay = new PayTask(mActivity);
					String result = alipay.pay(orderInfo, true);
						                
					StringBuffer buffer = new StringBuffer();
					buffer.append(result);
					
					JniHelper.sendMessage(RQF_PAY , buffer.toString());

				} catch (Exception ex) {
					ex.printStackTrace();
					Log.i("qqqqqqqqqqqq", "qqqqqqqqqqqqqqqqqq");
					JniHelper.showMessage("调用支付宝插件失败："+ex.getMessage());
					mActivity.dismissDialog(SUBMIT_ORDER_DIALOG);
				}
				return null;
			}

			@Override
			protected void onPostExecute(Void result) {
				// TODO Auto-generated method stub
				super.onPostExecute(result);
				mActivity.removeDialog(SUBMIT_ORDER_DIALOG);
			}

			@Override
			protected void onPreExecute() {
				// TODO Auto-generated method stub
				super.onPreExecute();		
				mActivity.showDialog(SUBMIT_ORDER_DIALOG);
			}
			
			
		}.execute();
		
		
	}

	private String getNewOrderInfo(String orderNum, int price) {
		StringBuilder sb = new StringBuilder();
		sb.append("partner=\"");
		sb.append(Keys.DEFAULT_PARTNER);
		sb.append("\"&out_trade_no=\"");
		sb.append(orderNum);
		sb.append("\"&subject=\"");
		sb.append(SUBJECT);
		sb.append("\"&body=\"");
		sb.append(BODY);
		sb.append("\"&total_fee=\"");
		sb.append(price).append(".00");
		sb.append("\"&notify_url=\"");

		// 网址需要做URL编码
		sb.append(URLEncoder.encode(NOTIFY_URL));
		sb.append("\"&service=\"mobile.securitypay.pay");
		sb.append("\"&_input_charset=\"UTF-8");
		sb.append("\"&return_url=\"");
		sb.append(URLEncoder.encode("http://m.alipay.com"));
		sb.append("\"&payment_type=\"1");
		sb.append("\"&seller_id=\"");
		sb.append(Keys.DEFAULT_SELLER);

		// 如果show_url值为空，可不传
		// sb.append("\"&show_url=\"");
		sb.append("\"&it_b_pay=\"30m");
		sb.append("\"");

		return new String(sb);
	}

	private String getSignType() {
		return "sign_type=\"RSA\"";
	}	
	
}
