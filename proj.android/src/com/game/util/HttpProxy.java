package com.game.util;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.game.constant.ICMD;
import com.game.jni.JniHelper;

public class HttpProxy {

	private static HttpProxy proxy = null;
	protected ExecutorService server = null;
	
	protected Set<String> urlSet = new HashSet<String>();
	
	protected HttpProxy(){
		server = Executors.newCachedThreadPool();
	}
	
	public static HttpProxy instance(){
		if(proxy == null){
			proxy = new HttpProxy();
		}
		return proxy;
	}
	
	public void execute(String params){
		if(urlSet.add(params)){
			final String url = params.split(",")[0];
			final int key = Integer.parseInt(params.split(",")[1]);
			server.submit(new Runnable() {			
				@Override
				public void run() {
					try {
						String res = RemoteHttpUtil.URLPost(url , "" , 3);
						callback(url , key , 0 , res);
					} catch (Exception e) {
						callback(url , key , 100 , "");
					}
				}
			});
			server.submit(new Runnable() {			
				@Override
				public void run() {
					try {
						Thread.sleep(3000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					callback(url , key , 100 , "");
				}
			});
		}		
	}
	
	public void callback(String url , int key , int code , String content){
		if(urlSet.remove(url.concat(",").concat(String.valueOf(key)))){
			StringBuffer buffer = new StringBuffer();
			buffer.append(url).append(",");
			buffer.append(key).append(",");
			buffer.append(code).append(",");
			buffer.append(content);
			JniHelper.sendJniCallback(ICMD.CMD_JTC_SENDHTTPREQUEST, buffer.toString());	
		}
	}
	
}
