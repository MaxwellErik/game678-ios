package com.game.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.util.Calendar;

import org.json.JSONObject;

import android.text.TextUtils;
import android.util.Log;

import com.merchant.android.objects.SubmitOrder;
import com.game.constant.IPayConfig;
import com.game.exception.PayExcetion;
import com.game.jni.JniHelper;

public class PayHelper implements IPayConfig{

	private final static String[] strDigitss = { "0", "1", "2", "3", "4", "5",
	    "6", "7", "8", "9", "a", "b", "c", "d", "e", "f" };
	
	public static String sendOrderPay(String params){
		return sendOrderPay(params , "data")[0];
	}
	
	public static String SendLockPhone(String params)
	{
		return "aaaaa";
		//return sendLockPhoneTo(params , "ErrorCode")[0];
	}
	
	public static String[] sendLockPhoneTo(String params , String... response){
		String Game_Sign;
		StringBuffer buffer = new StringBuffer();
		buffer.append("UserID=").append(params);
    	buffer.append("&IP=").append("127.0.01");
    	buffer.append("&LockType=").append("0");
    	buffer.append("&LockRemark=").append("登陆");
    	
    	StringBuffer TempBuffer = new StringBuffer();
    	TempBuffer.append(params);
    	TempBuffer.append("&127.0.01");
    	TempBuffer.append("&0");
    	TempBuffer.append("&登陆");
    	TempBuffer.append("&netfog88");
    	Log.i("LockPhoneParams = ","TempBuffer="+TempBuffer);
    	Game_Sign = GetMD5CodeX(TempBuffer.toString());
    	buffer.append("&Sign=").append(Game_Sign);
    	Log.i("LockPhoneParams = ","buffer="+buffer);
    	
		try {
			String json = RemoteHttpUtil.URLPost(PhoneLockURL , buffer.toString());
			JSONObject o = new JSONObject(json);
			String temp = o.getString("ErrorCode");
			Log.i("temp_LockPhoneParams = ","buffer="+temp);
			if(o.get("ErrorCode") != null && "0".equals(o.get("ret").toString())){
				String[] res = new String[response.length];
				for(int i=0; i<res.length ; i++){
					res[i] = o.getString(response[i]);
				}
				return res;
			}else{
				throw new PayExcetion(o.getString("msg"));
			}
		} catch (Exception e) {
			throw new PayExcetion("程序错误："+e.getMessage());
		}
	}
	
	public static String[] sendOrderPay(String params , String... response){
		try {
			String json = RemoteHttpUtil.URLPost(PAY_URL, createURI(params));
			Log.i("bbbbbbbbbbbbbb", json);
			JSONObject o = new JSONObject(json);
			if(o.get("result") != "SUCCEE"){
				String[] res = new String[response.length];
				for(int i=0; i<res.length ; i++){
					res[i] = o.getString(response[i]);
					Log.i("bbbbbbbbbbbbbb", res[i]);
				}
				
				return res;
			}else{
				throw new PayExcetion(o.getString("msg"));
			}
		} catch (Exception e) {
			throw new PayExcetion("程序错误："+e.getMessage());
		}
	}
	
	public static String mmQuery(String params){
		try {
			String json = RemoteHttpUtil.URLPost(MMQUERY_URL , createURI(params));
			JSONObject o = new JSONObject(json);
			if(o.get("ret") != null && "0".equals(o.get("ret").toString())){
				return params.split(",")[6];
			}else{
				throw new PayExcetion(o.getString("msg"));
			}
		} catch (Exception e) {
			throw new PayExcetion("程序错误："+e.getMessage());
		}
	}
	
	   // 返回形式为数字跟字符串
	public  static String byteToArrayString(byte bByte) {
int iRet = bByte;
// System.out.println("iRet="+iRet);
if (iRet < 0) {
   iRet += 256;
}
int iD1 = iRet / 16;
int iD2 = iRet % 16;
return strDigitss[iD1] + strDigitss[iD2];
}
	
//转换字节数组为16进制字串
private static String byteToString(byte[] bByte) {
StringBuffer sBuffer = new StringBuffer();
for (int i = 0; i < bByte.length; i++) {
   sBuffer.append(byteToArrayString(bByte[i]));
}
return sBuffer.toString();
}

public static String GetMD5CodeX(String strObj) {
String resultString = null;
try {
	          resultString = new String(strObj);
	          MessageDigest md = MessageDigest.getInstance("MD5");
	           // md.digest() 该函数返回值为存放哈希值结果的byte数组
	          resultString = byteToString(md.digest(strObj.getBytes()));
	   } catch (NoSuchAlgorithmException ex) {
	            ex.printStackTrace();
	        }
	        return resultString;
}
	
	public static String sendPatchOrder(String orderNum){
		StringBuffer buffer = new StringBuffer(PATCH_URL);
		buffer.append("billcode=").append(orderNum);
		buffer.append("&kindId=").append(0);
		
    	StringBuffer TempBuffer = new StringBuffer();
    	TempBuffer.append("billcode=").append(orderNum);
    	TempBuffer.append("&kindId=").append(0);
    	TempBuffer.append("&key=").append(GAME_KEY);
    	String Game_SignC = GetMD5CodeX(TempBuffer.toString());
    	buffer.append("&sign=").append(Game_SignC);
		try {
			Log.i("SEND URL", buffer.toString());
			String json = RemoteHttpUtil.URLPost(buffer.toString() , "");
			JSONObject o = new JSONObject(json);
			if(o.get("ret") != null && "0".equals(o.get("ret").toString())){
				return orderNum;
			}else{
				throw new PayExcetion(o.get("ret").toString() , o.getString("msg"));
			}
		} catch (PayExcetion e) {
			throw e;
		} catch (Exception e) {
			throw new PayExcetion("程序错误："+e.getMessage());
		}
	}
	
	public static String sendCardOrder(String params , int kindId){
		try {
			String json = RemoteHttpUtil.URLPost(CARD_URL , createURI(params));
			JSONObject o = new JSONObject(json);
			if(o.get("ret") != null && "0".equals(o.get("ret").toString())){
				return o.getString("ordernum");
			}else{
				throw new PayExcetion(o.getString("msg"));
			}
		} catch (Exception e) {
			throw new PayExcetion("程序错误："+e.getMessage());
		}
	}
	
	public static SubmitOrder createSubOrder(String params , String ordernum){
		String[] szParams = params.split(",");
		SubmitOrder order = new SubmitOrder();
		order.setApplication("SubmitOrder.Req");
		order.setVersion("1.1.0");
		order.setMerchantName(MERCHANT_NAME);

		order.setMerchantOrderAmt(fomatAmount(szParams[1]));
		String orderTime = createOrderTime();
		order.setMerchantOrderTime(orderTime);
		order.setMerchantOrderId(ordernum);
		order.setMerchantOrderDesc(ORDER_DESC);
		order.setTransTimeout(orderTime);
		order.setBackEndUrl(CALLBACK_URL);
		order.setMerchantId(values[2]);
		return order;
	}
	

	public static String fomatAmount(String amountStr) {
		if (!TextUtils.isEmpty(amountStr)) {
			double amount = Double.parseDouble(amountStr);
			DecimalFormat myformat = new DecimalFormat();
			myformat.applyPattern("0.00");
			return myformat.format(amount);
		} else {
			return null;
		}

	}
	
	
	public static String createOrderTime() {
		Calendar CD = Calendar.getInstance();
		String YY = String.valueOf(CD.get(Calendar.YEAR));
		String MM = String.valueOf(CD.get(Calendar.MONTH) + 1);
		if (MM.length() < 2) {
			MM = "0" + MM;
		}
		String DD = String.valueOf(CD.get(Calendar.DAY_OF_MONTH));
		if (DD.length() < 2) {
			DD = "0" + DD;
		}
		String HH = String.valueOf(CD.get(Calendar.HOUR_OF_DAY));
		if (HH.length() < 2) {
			HH = "0" + HH;
		}

		String NN = String.valueOf(CD.get(Calendar.MINUTE));
		if (NN.length() < 2) {
			NN = "0" + NN;
		}
		String SS = String.valueOf(CD.get(Calendar.SECOND));
		if (SS.length() < 2) {
			SS = "0" + SS;
		}
		StringBuilder builder = new StringBuilder();
		builder.append(YY);
		builder.append(MM);
		builder.append(DD);
		builder.append(HH);
		builder.append(NN);
		builder.append(SS);
		return builder.toString();
	}
	
	public static void patchOrder(String orderNum){
		try {
			sendPatchOrder(orderNum);
			payCallback(orderNum);
		} catch (PayExcetion e) {
			JniHelper.showMessage(e.getMessage());
		}
	}
	
	public static void payCallback(String ordernum){
		JniHelper.sendJniCallback(JniHelper.CMD_JTC_PAY, ordernum);
	}
	
	public static void getOrderCallback(String ordernum){
		Log.i("ORDER NUM CALLBACK", ordernum);
		JniHelper.sendJniCallback(JniHelper.CMD_JTC_PAY_GET_ORDER, ordernum);
	}
	
	public static String createURI(String params){
		PayParams pay = new PayParams(params);
		pay.setKindId(KIND_ID);
		pay.setGameKey(GAME_KEY);
		pay.setChannelId(AndroidHelper.getMMIAP());
		pay.setMobileId(AndroidHelper.getDeviceUuid().toString());
		pay.setMobileType(AndroidHelper.mobileType());
		return pay.createURI();
	}
	public static String CreateLockPhoneURI(String params)
	{
		PayParams pay = new PayParams(params);
		return pay.createURI();
	}
}
