package com.game.util;

import com.game.game678.R;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import org.cocos2dx.lib.Cocos2dxActivity;
import org.cocos2dx.lib.Cocos2dxEditText;
import org.cocos2dx.lib.Cocos2dxGLSurfaceView;

import android.annotation.SuppressLint;
import android.app.ActionBar.LayoutParams;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Color;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings.LayoutAlgorithm;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.game.constant.IPayConfig;
import com.game.jni.JniHelper;

import android.widget.ImageView;

import com.game.game678.*;

public class WebBrowseProxy implements IPayConfig {
	static Cocos2dxActivity mActivity = null;
	FrameLayout mContentLayout;
	Cocos2dxGLSurfaceView mGlSurfaceView;
	Cocos2dxEditText mEditText;
	RelativeLayout mWebLayout;
	ImageView m_imageView;// ImageView控件
	Button m_backButton;// 关闭按钮
//	Button btn_bc;
	WebView mWebView = null;
	boolean bPart = false;
	static WebBrowseProxy mProxy;

	public static final int CLOSE_WEBVIEW = 1;

	public Handler handler = null;

	public static synchronized WebBrowseProxy instance() {
		if (mProxy == null)
			mProxy = new WebBrowseProxy();
		return mProxy;
	}

	protected WebBrowseProxy() {
		mActivity = JniHelper.instance();
		handler = new Handler(mActivity.getMainLooper()) {
			public void handleMessage(Message msg) {

				switch (msg.what) {
				case CLOSE_WEBVIEW: {
					removeWebView();
				}
					break;
				default:
					break;
				}
			};
		};
	}

	public void onCreateLayout(Cocos2dxGLSurfaceView surfaceView,
			Cocos2dxEditText editText, FrameLayout mFrameLayout) {
		mGlSurfaceView = surfaceView;
		mEditText = editText;
		mContentLayout = mFrameLayout;

		mWebLayout = new RelativeLayout (mActivity);
		@SuppressWarnings("deprecation")
		RelativeLayout.LayoutParams lel = new RelativeLayout.LayoutParams(
				RelativeLayout.LayoutParams.MATCH_PARENT,
				RelativeLayout.LayoutParams.MATCH_PARENT);
		mWebLayout.setLayoutParams(lel);
		mWebLayout.setBackgroundColor(0);
	}

	public void openWebBrowse(final String url) {
		Intent intent = new Intent();
		intent.setAction("android.intent.action.VIEW");
		Uri content_url = Uri.parse(url);
		intent.setData(content_url);
		mActivity.startActivity(intent);
	}

	public void openWebView(final String params) {
		bPart = (params != null && params.split(",").length >= 5);
		mActivity.runOnUiThread(new Runnable() {
					@SuppressWarnings("deprecation")
					public void run() {
						String[] arr = params.split(",");
						mWebView = new WebView(mActivity);

						// mWebView.setInitialScale(5);
						mWebView.getSettings().setUseWideViewPort(true);
						mWebView.getSettings().setLoadWithOverviewMode(true);
						mWebView.getSettings().setJavaScriptEnabled(true); 
						// 设置可以支持缩放 
//						mWebView.getSettings().setSupportZoom(true); 
//						// 设置出现缩放工具 
//						mWebView.getSettings().setBuiltInZoomControls(true);
//						//扩大比例的缩放
//						mWebView.getSettings().setUseWideViewPort(true);
						//自适应屏幕
						mWebView.getSettings().setLayoutAlgorithm(LayoutAlgorithm.SINGLE_COLUMN);
						mWebView.getSettings().setLoadWithOverviewMode(true);
						// mWebView.getSettings().setBuiltInZoomControls(false);
						mWebView.getSettings().setJavaScriptEnabled(true);
						// mWebView.getSettings().setGeolocationEnabled(false);
						mWebView.setHorizontalScrollBarEnabled(false);// 水平不显示
						mWebView.setVerticalScrollBarEnabled(false); // 垂直不显示

						RelativeLayout.LayoutParams web = new RelativeLayout.LayoutParams(
								RelativeLayout.LayoutParams.WRAP_CONTENT,
								RelativeLayout.LayoutParams.WRAP_CONTENT);
						web.addRule(RelativeLayout.CENTER_HORIZONTAL);
						web.addRule(RelativeLayout.ALIGN_PARENT_TOP);
						web.topMargin = 335;
						web.width =420;
						web.height =420;
						mWebView.setLayoutParams(web);
						
						Log.i("adfafdfsdfdf", params);
						if (params != null) {
							if (arr[0].startsWith("https://"))
								mWebView.loadUrl(arr[0]);
							else
								mWebView.loadUrl("" + arr[0]);
						}

						mWebView.setWebViewClient(new WebViewClient() {
							public boolean shouldOverrideUrlLoading(
									WebView view, String url) {
								if (url.indexOf("tel:") < 0) {
									view.loadUrl(url);
								}
								return true;
							}
						});

						m_imageView = new ImageView(mActivity);
						m_imageView.setImageResource(R.drawable.kuan);

						m_backButton = new Button(mActivity);
						m_backButton.setBackgroundResource(R.drawable.kuanclose);
						RelativeLayout.LayoutParams lypt = new RelativeLayout.LayoutParams(
								RelativeLayout.LayoutParams.WRAP_CONTENT,
								RelativeLayout.LayoutParams.WRAP_CONTENT);
						lypt.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
						lypt.rightMargin = 180;
						lypt.topMargin = 80;
						lypt.width = 200;
						lypt.height = 200;
						m_backButton.setLayoutParams(lypt);

//						btn_bc = new Button(mActivity);
//						btn_bc.setBackgroundResource(R.drawable.btn_bc);
//						RelativeLayout.LayoutParams bypt = new RelativeLayout.LayoutParams(
//								RelativeLayout.LayoutParams.WRAP_CONTENT,
//								RelativeLayout.LayoutParams.WRAP_CONTENT);
//						bypt.addRule(RelativeLayout.CENTER_VERTICAL);
//						bypt.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
//						bypt.rightMargin = 80;
//						bypt.width = 350;
//						bypt.height = 100;
//						btn_bc.setLayoutParams(bypt);

						setCoco2dVisiable(false);
						
						mContentLayout.addView(m_imageView);
						mWebLayout.addView(m_backButton);
						mWebLayout.addView(mWebView);
//						mWebLayout.addView(btn_bc);
						mContentLayout.addView(mWebLayout);

						m_backButton.setOnClickListener(new OnClickListener() {
							public void onClick(View v) {
								Bitmap newb = Bitmap.createBitmap(
										mWebView.getWidth(),
										mWebView.getHeight(), Config.ARGB_8888);
								Canvas canvas = new Canvas(newb);
								mWebView.draw(canvas);
								saveImageToGallery(newb);
								removeWebView();
								Toast.makeText(mActivity, "保存成功", Toast.LENGTH_LONG).show();
							}

						});

//						m_backButton.setOnClickListener(new OnClickListener() {
//							public void onClick(View v) {
//
//								removeWebView();
//							}
//						});

						mWebView.setWebChromeClient(new WebChromeClient() {
							@Override
							public void onProgressChanged(WebView view,
									int newProgress) {
								Log.i("load progress",
										String.valueOf(newProgress));
								if (bPart) {
									if (newProgress == 100)
										mWebLayout.setVisibility(View.VISIBLE);
								} else {
									if (newProgress == 100) { // 这里是设置activity的标题，
																// 也可以根据自己的需求做一些其他的操作
										mActivity.setTitle("加载完成");

										mActivity
												.dismissDialog(LOAD_WEBBROWSE_DIALOG);
									} else {
										mActivity.setTitle("加载中.......");
										mActivity
												.showDialog(LOAD_WEBBROWSE_DIALOG);
										// mActivity.onCreateDialog(LOAD_WEBBROWSE_DIALOG);
									}
								}
							}
						});

						if (bPart) {
							mWebLayout.setVisibility(View.GONE);
						}
					}

				});
	}

	public void removeWebView() {
		if (mWebView != null) {
			setCoco2dVisiable(true);
			mContentLayout.removeView(m_imageView);
			m_imageView.destroyDrawingCache();
			mContentLayout.removeView(mWebLayout);
			mWebLayout.destroyDrawingCache();

			mWebLayout.removeView(mWebView);

			mWebLayout.removeView(m_backButton);
			m_backButton.destroyDrawingCache();
//			mWebLayout.removeView(btn_bc);
//			btn_bc.destroyDrawingCache();
			mWebView.destroy();
			mWebView = null;

			// JniHelper.sendJniCallback(JniHelper.CMD_JTC_GetScore,
			// "GetScore");
		}
	}

	public void saveImageToGallery(Bitmap bmp) {
		// 首先保存图片
		File appDir = new File(Environment.getExternalStorageDirectory(),
				"Boohee");
		if (!appDir.exists()) {
			appDir.mkdir();
		}
		String fileName = System.currentTimeMillis() + ".jpg";
		File file = new File(appDir, fileName);
		try {
			FileOutputStream fos = new FileOutputStream(file);
			bmp.compress(CompressFormat.JPEG, 100, fos);
			fos.flush();
			fos.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		// 其次把文件插入到系统图库
		try {
			MediaStore.Images.Media.insertImage(mActivity.getContentResolver(),
					file.getAbsolutePath(), fileName, null);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		// 最后通知图库更新
		mActivity.sendBroadcast(new Intent(
				Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.parse("file://"
						+ fileName)));
	}

	public void sendCloseMessage() {
		handler.sendEmptyMessage(CLOSE_WEBVIEW);
	}

	public void setCoco2dVisiable(boolean visiable) {
		 if(!bPart){
		 if(visiable){
		 mGlSurfaceView.setVisibility(View.VISIBLE);
		 mEditText.setVisibility(View.VISIBLE);
		 }else{
		 mGlSurfaceView.setVisibility(View.GONE);
		 mEditText.setVisibility(View.GONE);
		 }
		 }
	}

	public boolean onKeyDown(int keyCoder, KeyEvent event) {
		if (bPart)
			return false;
		if (mWebView != null) {
			if (mWebView.canGoBack() && keyCoder == KeyEvent.KEYCODE_BACK) {// �����ҳ�ܻ�������ˣ�����ܺ����Ƴ�WebView
				mWebView.goBack();
			} else {
				removeWebView();
			}
		}
		return false;
	}

}
