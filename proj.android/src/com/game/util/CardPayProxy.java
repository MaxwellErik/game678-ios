package com.game.util;

import com.game.constant.IPayConfig;
import com.game.exception.PayExcetion;
import com.game.jni.JniHelper;

public class CardPayProxy implements IPayConfig{

	protected static CardPayProxy proxy = null;
	
	public static CardPayProxy instance(){
		if(proxy == null){
			proxy = new CardPayProxy();
		}
		return proxy;
	}
	
	@SuppressWarnings("deprecation")
	public void sendCard(final String params){
		JniHelper.instance().showDialog(SUBMIT_ORDER_DIALOG);
		new Thread() {
			public void run() {
				try {
					String orderNum = PayHelper.sendCardOrder(params , KIND_ID);
					PayHelper.getOrderCallback(orderNum);
					int i=0;
					while(true){
						Thread.sleep((++i)*(i>5?2000:1000));
						if(i > 10){
							throw new PayExcetion("订单未到账");
						}
						try {
							
							PayHelper.sendPatchOrder(orderNum);
							PayHelper.payCallback(orderNum);
							break;
						} catch (PayExcetion e) {
							
							if(!"100".equals(e.getCode())){
								PayParams p = new PayParams(params);
								if(p.getUserId() != 0)
								{
									if("4".equals(e.getCode())){
										PayHelper.payCallback(orderNum);
										break;
									}
								}
								throw new PayExcetion(e.getMessage());
							}
						}						
					}
				} catch (PayExcetion e){
					JniHelper.showMessage(e.getMessage());
				} catch (Exception e) {
					JniHelper.showMessage("程序异常"+e.getMessage());
				} finally{
					JniHelper.instance().dismissDialog(SUBMIT_ORDER_DIALOG);
				}
			}
		}.start();		
		
	}
	
}
