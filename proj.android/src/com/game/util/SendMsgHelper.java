package com.game.util;

import java.util.List;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.telephony.SmsManager;

import com.game.jni.JniHelper;

public class SendMsgHelper {
	
	private static String BROADCASTACTION = "com.moling.sms.send";
	
	private static SendMsgHelper helper;
	
	private BroadcastReceiver send = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();
			if (BROADCASTACTION.equals(action)) {
				JniHelper.showMessage("短信发送成功");
			}
		}
	};
	
	protected SendMsgHelper(){
		IntentFilter filter = new IntentFilter();
		filter.addAction(BROADCASTACTION);
		JniHelper.instance().registerReceiver(send, filter);
	}
	
	public static SendMsgHelper instance(){
		if(helper == null){
			helper = new SendMsgHelper();
		}
		return helper;
	}
	

	public void send(String mobile, String content) {

		SmsManager smsManager = SmsManager.getDefault();
		Intent sent = new Intent();
		sent.setAction(BROADCASTACTION);
		PendingIntent sentIntent = PendingIntent.getBroadcast(JniHelper.instance(), 0, sent, 0);

		if (content.length() >= 70) {
			// 短信字数大于70，自动分条
			List<String> ms = smsManager.divideMessage(content);

			for (String str : ms) {
				// 短信发送
				smsManager.sendTextMessage(mobile, null, str, sentIntent, null);
			}
		} else {
			smsManager.sendTextMessage(mobile, null, content, sentIntent, null);
		}
	}

	
}
