package com.game.util;

import java.io.File;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.game.constant.IWeiboConstants;
import com.game.jni.JniHelper;
import com.game.game678.*;
import com.sina.weibo.sdk.api.ImageObject;
import com.sina.weibo.sdk.api.TextObject;
import com.sina.weibo.sdk.api.WebpageObject;
import com.sina.weibo.sdk.api.WeiboMessage;
import com.sina.weibo.sdk.api.WeiboMultiMessage;
import com.sina.weibo.sdk.api.share.IWeiboShareAPI;
import com.sina.weibo.sdk.api.share.SendMessageToWeiboRequest;
import com.sina.weibo.sdk.api.share.SendMultiMessageToWeiboRequest;
import com.sina.weibo.sdk.api.share.WeiboShareSDK;
import com.sina.weibo.sdk.utils.Utility;

public class WeiboShareProxy {

	private static final int THUMB_SIZE = 150;

	/** 微博微博分享接口实例 */
	public IWeiboShareAPI api = null;

	private static WeiboShareProxy proxy = null;

	protected WeiboShareProxy() {
		api = WeiboShareSDK.createWeiboAPI(JniHelper.instance(),
				IWeiboConstants.APP_KEY);
	}

	public static WeiboShareProxy instance() {
		if (proxy == null) {
			proxy = new WeiboShareProxy();
		}
		return proxy;
	}

	public void share(String params) {
		String[] arr = params.split(",");
		
		if (!api.isWeiboAppInstalled()) {
			JniHelper.showMessage("您还没有安装新浪微博，无法分享");
		} else if (api.checkEnvironment(true)) {
			api.registerApp();
			int supportApi = api.getWeiboAppSupportAPI();
			if(arr.length < 4){
				String path = arr[0];
				if (supportApi >= 10351 /* ApiUtils.BUILD_INT_VER_2_2 */) {
					sendMultiMessage(path);
				} else {
					sendSingleMessage(path);
				}
			}else{
				String url = arr[0];
				String path = arr[1];
				String title = arr[2];
				String desc = arr[3];
				if (supportApi >= 10351 /* ApiUtils.BUILD_INT_VER_2_2 */) {
					sendMultiMessage(url, path, title, desc);
				} else {
					sendSingleMessage(url, path, title, desc);
				}
			}
			
		} else {
			JniHelper.showMessage("您的新浪微博环境异常，无法分享");
		}
	}

	private void sendSingleMessage(String url, String path, String title,
			String desc) {

		// 1. 初始化微博的分享消息
		// 用户可以分享文本、图片、网页、音乐、视频中的一种
		WeiboMessage weiboMessage = new WeiboMessage();
		weiboMessage.mediaObject = getWebpageObj(url, path, title, desc);
		/*
		 * if (hasVoice) { weiboMessage.mediaObject = getVoiceObj(); }
		 */

		// 2. 初始化从第三方到微博的消息请求
		SendMessageToWeiboRequest request = new SendMessageToWeiboRequest();
		// 用transaction唯一标识一个请求
		request.transaction = String.valueOf(System.currentTimeMillis());
		request.message = weiboMessage;

		// 3. 发送请求消息到微博，唤起微博分享界面
		api.sendRequest(request);
	}

	private void sendSingleMessage(String path) {

		// 1. 初始化微博的分享消息
		// 用户可以分享文本、图片、网页、音乐、视频中的一种
		WeiboMessage weiboMessage = new WeiboMessage();
		weiboMessage.mediaObject = getImageObj(path);
		/*
		 * if (hasVoice) { weiboMessage.mediaObject = getVoiceObj(); }
		 */

		// 2. 初始化从第三方到微博的消息请求
		SendMessageToWeiboRequest request = new SendMessageToWeiboRequest();
		// 用transaction唯一标识一个请求
		request.transaction = String.valueOf(System.currentTimeMillis());
		request.message = weiboMessage;

		// 3. 发送请求消息到微博，唤起微博分享界面
		api.sendRequest(request);
	}

	private void sendMultiMessage(String url, String path, String title,
			String desc) {

		// 1. 初始化微博的分享消息
		WeiboMultiMessage weiboMessage = new WeiboMultiMessage();
		weiboMessage.textObject = getTextObj(desc);
		weiboMessage.imageObject = getImageObj(path);
		weiboMessage.mediaObject = getWebpageObj(url, path, title, desc);

		// 2. 初始化从第三方到微博的消息请求
		SendMultiMessageToWeiboRequest request = new SendMultiMessageToWeiboRequest();
		// 用transaction唯一标识一个请求
		request.transaction = String.valueOf(System.currentTimeMillis());
		request.multiMessage = weiboMessage;

		// 3. 发送请求消息到微博，唤起微博分享界面
		api.sendRequest(request);
	}

	private void sendMultiMessage(String path) {
		// 1. 初始化微博的分享消息
		WeiboMultiMessage weiboMessage = new WeiboMultiMessage();
		weiboMessage.imageObject = getImageObj(path);

		// 2. 初始化从第三方到微博的消息请求
		SendMultiMessageToWeiboRequest request = new SendMultiMessageToWeiboRequest();
		// 用transaction唯一标识一个请求
		request.transaction = String.valueOf(System.currentTimeMillis());
		request.multiMessage = weiboMessage;

		// 3. 发送请求消息到微博，唤起微博分享界面
		api.sendRequest(request);
	}

	/**
	 * 创建文本消息对象。
	 * 
	 * @return 文本消息对象。
	 */
	private TextObject getTextObj(String text) {
		TextObject textObject = new TextObject();
		textObject.text = text;
		return textObject;
	}

	/**
	 * 创建图片消息对象。
	 * 
	 * @return 图片消息对象。
	 */
	private ImageObject getImageObj(String path) {
		ImageObject imageObject = new ImageObject();
		// 设置 Bitmap 类型的图片到视频对象里
		if (path == null || path.length() == 0) {
			Bitmap thumb = BitmapFactory.decodeResource(JniHelper.instance()
					.getResources(), com.game.game678.R.drawable.ic_launcher);
			// mediaObject.thumbData = Util.bmpToByteArray(thumb, true);
			imageObject.setImageObject(thumb);
		} else {
			File file = new File(path);
			if (!file.exists()) {
				JniHelper.showMessage(path + " 不存在");
				return null;
			}
			Bitmap bmp = BitmapFactory.decodeFile(path);
			// Bitmap thumbBmp = Bitmap.createScaledBitmap(bmp, THUMB_SIZE,
			// THUMB_SIZE, true);
			// bmp.recycle();
			imageObject.setImageObject(bmp);
		}
		return imageObject;
	}

	private WebpageObject getWebpageObj(String url, String path, String title,
			String desc) {
		WebpageObject mediaObject = new WebpageObject();
		mediaObject.identify = Utility.generateGUID();
		mediaObject.title = title;
		mediaObject.description = desc;

		// 设置 Bitmap 类型的图片到视频对象里
		if (path == null || path.length() == 0) {
			Bitmap thumb = BitmapFactory.decodeResource(JniHelper.instance()
					.getResources(), com.game.game678.R.drawable.ic_launcher);
			// mediaObject.thumbData = Util.bmpToByteArray(thumb, true);
			mediaObject.setThumbImage(thumb);
			
//			Bitmap thumb = BitmapFactory.decodeResource(JniHelper.instance().getResources(), R.drawable.ic_launcher);
//			Bitmap thumbBmp = Bitmap.createScaledBitmap(thumb, THUMB_SIZE, 120, true);
//			thumb.recycle();
//			mediaObject.setThumbImage(thumbBmp);
		} else {
			File file = new File(path);
			if (!file.exists()) {
				JniHelper.showMessage(path + " 不存在");
				return null;
			}
			Bitmap bmp = BitmapFactory.decodeFile(path);
			// Bitmap thumbBmp = Bitmap.createScaledBitmap(bmp, THUMB_SIZE,
			// THUMB_SIZE, true);
			// bmp.recycle();
			mediaObject.setThumbImage(bmp);
		}
		mediaObject.actionUrl = url;
		mediaObject.defaultText = desc;
		return mediaObject;
	}

}
