package com.game.util;

import android.os.AsyncTask;

import com.game.constant.IPayConfig;
import com.game.exception.PayExcetion;
import com.game.jni.JniHelper;

public abstract class BasePayProxy implements IPayConfig{

	protected static BasePayProxy proxy = null;
	
	protected BasePayProxy(){
	}
	
	protected String orderNum;
	
	protected String params;
	
	protected PayParams payParams;
	
	public void pay(String params){
		this.params = params;
		payParams = new PayParams(params);
		requestOrderNum(params);
	}
	
	public void requestOrderNum(String params) {
		new AsyncTask<String, Void, Boolean>() {

			@Override
			protected Boolean doInBackground(String... aa) {
				try {
					BasePayProxy.this.orderNum = sendOrderRequest(aa[0]);
					PayHelper.getOrderCallback(BasePayProxy.this.orderNum);
				} catch (PayExcetion e) {
					JniHelper.showMessage(e.getMessage());
					return false;
				} catch (Exception e) {
					e.printStackTrace();
					JniHelper.showMessage("程序异常：" + e.getMessage());
					return false;
				}
				return true;
			}

			@Override
			protected void onPostExecute(Boolean result) {
				super.onPostExecute(result);
				JniHelper.instance().removeDialog(SUBMIT_ORDER_DIALOG);
				try {
					if(result){
						callbackSDK(BasePayProxy.this.orderNum);
					}
				} catch (Exception e) {
					e.printStackTrace();
					JniHelper.showMessage("程序异常：" + e.getMessage());
				}
			}

			@Override
			protected void onPreExecute() {
				super.onPreExecute();
				JniHelper.instance().showDialog(SUBMIT_ORDER_DIALOG);
			}
		}.execute(params);
	}
	
	public abstract void callbackSDK(String orderNum);
	
	public abstract boolean checkSupportType(int type);
	
	public void payFinish(String orderNum , boolean delay){
		if(!delay){
			PayHelper.payCallback(orderNum);
		}
	}
	
	public String sendOrderRequest(String params){
		return PayHelper.sendOrderPay(params);
	}
	
	public void waitAccount(String orderNum , int waitTime){
		new AsyncTask<Object, Void, Void>() {

			@Override
			protected Void doInBackground(Object... aa) {
				try {
					int i=0;
					int time = Math.max(10 , (Integer)aa[1]);
					while(true){
						i++;
						int sleep = 2*(i/3+1);
						Thread.sleep(sleep*1000);
						try {
							long t1 = System.currentTimeMillis()/1000;
							if(validOrder(BasePayProxy.this.params, (String)aa[0])){
								payFinish((String)aa[0], false);
								break;
							}
							t1 = System.currentTimeMillis()/1000 - t1;
							time -= t1;
						} catch (PayExcetion e) {
							//已回调获取，抛出回调错误信息
							if(!"100".equals(e.getCode())){
								throw e;
							}
						}
						time -= sleep;
						if(time <= 0){
							throw new PayExcetion("DELAY" , "订单延迟到账，请联系客服");
						}
					}
				} catch (PayExcetion e){
					if("DELAY".equals(e.getCode())){
						payFinish((String)aa[0], true);
					}
					JniHelper.showMessage(e.getMessage());
				} catch (Exception e) {
					JniHelper.showMessage(e.getMessage());
				}
				return null;
			}

			@Override
			protected void onPostExecute(Void result) {
				super.onPostExecute(result);
				JniHelper.instance().removeDialog(WAIT_ACCOUNT);				
			}

			@Override
			protected void onPreExecute() {
				super.onPreExecute();
				JniHelper.instance().showDialog(WAIT_ACCOUNT);
			}
		}.execute(orderNum , waitTime);
	}
	
	public abstract boolean validOrder(String params , String orderNum);
	
	public void reset(){
		orderNum = null;
		params = null;
		payParams = null;
	}
	
	
}
