package com.game.util;

import android.util.Log;

import com.game.jni.JniHelper;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import com.game.jni.JniHelper;

public class PayParams {

	private String params;
	
	private int userId;
	private int gameId;
	private int money;
	private int score;
	private String version;
	private int infullType;
	private String orderNum;
	private int kindId;
	private String mobileId;
	private String channelId;
	private String mobileType;
	private String cardNo;
	private String cardPass;
	private String Game_Key;
	private String Game_Sign;
	private final static String[] strDigits = { "0", "1", "2", "3", "4", "5",
         "6", "7", "8", "9", "a", "b", "c", "d", "e", "f" };
	 
	public PayParams(String params){
		this.params = params;
		String[] szParams = params.split(",");
		userId = Integer.parseInt(szParams[0]);
		gameId = Integer.parseInt(szParams[1]);
		money = Integer.parseInt(szParams[2]);
		score = Integer.parseInt(szParams[3]);
		version = szParams[4];
		infullType = Integer.parseInt(szParams[5]);
		switch (infullType) {
		case JniHelper.PAY_TYPE_MOBILE:
		case JniHelper.PAY_TYPE_TELE:
		case JniHelper.PAY_TYPE_UNICOM:
		case JniHelper.PAY_TYPE_SD:
		case JniHelper.PAY_TYPE_WY:
		case JniHelper.PAY_TYPE_JW:
		{
			if(szParams.length >= 7){
				cardNo = szParams[6];
			}
			if(szParams.length >= 8){
				cardPass = szParams[7];
			}
			if(szParams.length >= 9){
				mobileId = szParams[8];
			}
			if(szParams.length >= 10){
				channelId = szParams[9];
			}
			if(szParams.length >= 11){
				mobileType = szParams[10];
			}
		}
			break;

		default:
		{
			if(szParams.length >= 7){
				orderNum = szParams[6];
			}
			if(szParams.length >= 8){
				kindId = Integer.parseInt(szParams[7]);
			}
			if(szParams.length >= 9){
				mobileId = szParams[8];
			}
			if(szParams.length >= 10){
				channelId = szParams[9];
			}
			if(szParams.length >= 11){
				mobileType = szParams[10];
			}
		}
			break;
		}		
	}

	public String getParams() {
		return params;
	}

	public int getUserId() {
		return userId;
	}

	public int getGameId() {
		return gameId;
	}
	
	public int getMoney() {
		return money;
	}

	public int getScore() {
		return score;
	}

	public String getVersion() {
		return version;
	}

	public int getInfullType() {
		return infullType;
	}

	public String getOrderNum() {
		return orderNum;
	}

	public int getKindId() {
		return kindId;
	}

	public String getMobileId() {
		return mobileId;
	}

	public void setMobileId(String mobileId) {
		this.mobileId = mobileId;
	}

	public String getChannelId() {
		return channelId;
	}

	public void setChannelId(String channelId) {
		this.channelId = channelId;
	}

	public String getMobileType() {
		return mobileType;
	}

	public void setMobileType(String mobileType) {
		this.mobileType = mobileType;
	}

	public void setParams(String params) {
		this.params = params;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public void setGameId(int gameId) {
		this.gameId = gameId;
	}
	
	public void setMoney(int money) {
		this.money = money;
	}

	public void setScore(int score) {
		this.score = score;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public void setInfullType(int infullType) {
		this.infullType = infullType;
	}

	public void setOrderNum(String orderNum) {
		this.orderNum = orderNum;
	}

	public void setKindId(int kindId) {
		this.kindId = kindId;
	}
	
	public void setGameKey(String gamekey){
		this.Game_Key = gamekey;
	}

	public String getCardNo() {
		return cardNo;
	}

	public void setCardNo(String cardNo) {
		this.cardNo = cardNo;
	}

	public String getCardPass() {
		return cardPass;
	}

	public void setCardPass(String cardPass) {
		this.cardPass = cardPass;
	}

	   // 返回形式为数字跟字符串
	public  String byteToArrayString(byte bByte) {
        int iRet = bByte;
        // System.out.println("iRet="+iRet);
        if (iRet < 0) {
            iRet += 256;
        }
        int iD1 = iRet / 16;
        int iD2 = iRet % 16;
        return strDigits[iD1] + strDigits[iD2];
    }
	
    // 转换字节数组为16进制字串
    private String byteToString(byte[] bByte) {
        StringBuffer sBuffer = new StringBuffer();
        for (int i = 0; i < bByte.length; i++) {
            sBuffer.append(byteToArrayString(bByte[i]));
        }
        return sBuffer.toString();
    }
    
	public String GetMD5Code(String strObj) {
	   String resultString = null;
	   try {
	          resultString = new String(strObj);
	          MessageDigest md = MessageDigest.getInstance("MD5");
	           // md.digest() 该函数返回值为存放哈希值结果的byte数组
	          resultString = byteToString(md.digest(strObj.getBytes()));
	        } catch (NoSuchAlgorithmException ex) {
	            ex.printStackTrace();
	        }
	        return resultString;
	    }
	
	public String createURI(){
		StringBuffer buffer = new StringBuffer();
		buffer.append("userid=").append(userId);
		buffer.append("&gameid=").append(gameId);
    	buffer.append("&total_amount=").append(money);
    	buffer.append("&subject=").append(score);
    	appendStr(buffer, "version" , version);
    	buffer.append("&infullType=").append(infullType);
    	appendStr(buffer , "orderNum" , orderNum);
    	buffer.append("&kindId=").append(kindId);
    	appendStr(buffer , "cardno" , cardNo);
    	appendStr(buffer , "cardpwd" , cardPass);
    	appendStr(buffer , "mobileId" , mobileId);
    	appendStr(buffer , "channelId" , channelId);
    	appendStr(buffer , "mobileType" , mobileType);
    	
    	StringBuffer TempBuffer = new StringBuffer();
    	TempBuffer.append("userId=").append(userId);
    	TempBuffer.append("&gameId=").append(gameId);
    	TempBuffer.append("&total_amount=").append(money);
    	TempBuffer.append("&subject=").append(score);
    	TempBuffer.append("&infullType=").append(infullType);
    	TempBuffer.append("&kindId=").append(kindId);
    	TempBuffer.append("&key=").append(Game_Key);
    	Game_Sign = GetMD5Code(TempBuffer.toString());
    	buffer.append("&sign=").append(Game_Sign);
    	Log.i("PayParams","buffer="+buffer);
    	
    	return buffer.toString();
	}

	public void appendStr(StringBuffer buffer ,String name , String param){
		if(param!=null && param.length()>0){
			buffer.append("&").append(name).append("=").append(param);
		}		
	}
	
}
