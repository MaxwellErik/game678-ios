package com.game.util;

import org.cocos2dx.lib.Cocos2dxActivity;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;

import com.merchant.android.XmlTool;
import com.merchant.android.objects.MerchantObject;
import com.merchant.android.objects.UpPay;
import com.merchant.android.objects.Xml;
import com.merchant.android.parse.MerchantXmlParseService;
import com.game.constant.IPayConfig;
import com.game.jni.JniHelper;
import com.payeco.android.plugin.PayecoPluginLoadingActivity;
/**
 * 2.0插件
 * @author 彭方良
 *
 */
public class PayProxy2 implements IPayConfig{

	private static final String PAYECO_PLUGIN_PAYEND_ACTION = "com.payeco.plugin.payend.broadcast";
	private final String TAG = "PayProxy";
	private Cocos2dxActivity mActivity;
	private String jniParams;
	private boolean isSubmitOrder = false;
	private Handler mUIHandler;

	private static final int UI_SUBMITORDER_SUCCESS = 0;
	private static final int UI_SUBMITORDER_FAIL = 1;
	
	private final int PAY_DIALOG = 1;
	private PayecoBroadcastReceiver mPayecoPayEndReceiver;
	
	private static PayProxy2 o;
	
	private String orderNum;


	protected PayProxy2(){
		this.mActivity = JniHelper.instance();
		mUIHandler = new UIHandler(this.mActivity.getMainLooper());
		mPayecoPayEndReceiver = new PayecoBroadcastReceiver();
		IntentFilter filter = new IntentFilter();
		filter.addCategory(Intent.CATEGORY_DEFAULT);
		filter.addAction(PAYECO_PLUGIN_PAYEND_ACTION);
		mActivity.registerReceiver(mPayecoPayEndReceiver, filter);
	}
	
	public synchronized static PayProxy2 instance(){
		if(o == null){
			o = new PayProxy2();
		}
		return o;
	}
	
	public void execute(String params){
		this.jniParams = params;
		if (!isSubmitOrder)
			new OrderTask().execute();
	}
	
	class UIHandler extends Handler {
		public UIHandler(Looper looper) {
			super(looper);
		}

		@Override
		public void handleMessage(Message message) {
			switch (message.what) {
			case UI_SUBMITORDER_SUCCESS:
				submitOrderSuccess((String)message.obj);
				break;
			case UI_SUBMITORDER_FAIL:
				submitOrderFail((String)message.obj);
				break;
			default:

			}
		}
	}
	


	private void submitOrderSuccess(String xmlStr) {
		isSubmitOrder = true;
		Intent intent = new Intent(mActivity,
				PayecoPluginLoadingActivity.class);
		Log.i(TAG, "调用插件：" + xmlStr);
		intent.putExtra("upPay.Req", xmlStr);
		intent.putExtra("Broadcast", PAYECO_PLUGIN_PAYEND_ACTION); 
		intent.putExtra("Environment", "01");
		mActivity.startActivity(intent);
		isSubmitOrder = false;
	}

	private void submitOrderFail(String msg) {
		isSubmitOrder = false;
		Toast.makeText(mActivity, "订单提交失败，请重新提交订单："+msg, Toast.LENGTH_LONG).show();
		
	}

	private class OrderTask extends AsyncTask<Void, Void, MerchantObject> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			Log.i(TAG, "onPreExecute");
			mActivity.showDialog(SUBMIT_ORDER_DIALOG);
		}

		@Override
		protected MerchantObject doInBackground(Void... params) {
			Log.i(TAG, "doInBackground");
			try {
	//			response = getOrder(XmlTool.objectToXml(order));
				String[] response = PayHelper.sendOrderPay(jniParams , "ordernum" , "responseXML");
				orderNum = response[0];
				PayHelper.getOrderCallback(orderNum);
				Message msg = new Message();
				msg.what = UI_SUBMITORDER_SUCCESS;
				msg.obj = response[1];
				mUIHandler.sendMessage(msg);
			} catch (Exception e) {
				JniHelper.showMessage(e.getMessage());
			}
			return null;
		}

		@Override
		protected void onPostExecute(MerchantObject result) {
			super.onPostExecute(result);
			Log.i(TAG, "onPostExecute");
			mActivity.dismissDialog(SUBMIT_ORDER_DIALOG);
		}
	}
	
	public class PayecoBroadcastReceiver extends BroadcastReceiver {

		@Override
		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();
			if (PAYECO_PLUGIN_PAYEND_ACTION.equals(action)) {
				String payResp = intent.getExtras().getString("upPay.Rsp");
				
				try {
					JSONObject reqJsonParams = new JSONObject(payResp);
					if("02".equals(reqJsonParams.getString("Status"))){
						PayHelper.payCallback(orderNum);
					}
					Log.i("bank uppay", reqJsonParams.getString("Status")+":"+reqJsonParams.getString("MerchOrderId"));
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} else {
				Toast.makeText(mActivity, "返回结果出错", Toast.LENGTH_LONG).show();
			}

		}
	}
	
	
	
}
