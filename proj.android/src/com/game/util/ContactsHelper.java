package com.game.util;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.provider.ContactsContract.Contacts.Photo;
import android.text.TextUtils;
import android.util.Log;

import com.game.jni.JniHelper;

public class ContactsHelper {

	/** 获取库Phon表字段 **/
	private static final String[] PHONES_PROJECTION = new String[] {
			Phone.DISPLAY_NAME, Phone.NUMBER, Photo.PHOTO_ID, Phone.CONTACT_ID };
	/** 联系人显示名称 **/
	private static final int PHONES_DISPLAY_NAME_INDEX = 0;
	/** 电话号码 **/
	private static final int PHONES_NUMBER_INDEX = 1;
	/** 头像ID **/
	private static final int PHONES_PHOTO_ID_INDEX = 2;
	/** 联系人的ID **/
	private static final int PHONES_CONTACT_ID_INDEX = 3;
	
	public List<String[]> getAllContacts(){
		List<String[]> contactList = new ArrayList<String[]>();
		getPhoneContacts(JniHelper.instance(), contactList);
		getSIMContacts(JniHelper.instance(), contactList);
		return contactList;
	}
	
	public String createToString(List<String[]> list){
		StringBuffer buffer = new StringBuffer();
		for (String[] ss : list) {
			for (String s : ss) {
				buffer.append(s).append(",");
			}
		}
		if(buffer.length() > 0){
			buffer.deleteCharAt(buffer.length()-1);
		}
		return buffer.toString();
	}

	/** 得到手机通讯录联系人信息 **/
	public void getPhoneContacts(Context mContext , List<String[]> contactList) {
		try {
			ContentResolver resolver = mContext.getContentResolver(); // 获取手机联系人
			Cursor phoneCursor = resolver.query(Phone.CONTENT_URI,
					PHONES_PROJECTION, null, null, null);
			if (phoneCursor != null) {
				while (phoneCursor.moveToNext()) { // 得到手机号码
					String phoneNumber = phoneCursor.getString(PHONES_NUMBER_INDEX); // 当手机号码为空的或者为空字段
																						// 跳过当前循环
					if (TextUtils.isEmpty(phoneNumber))
						continue; // 得到联系人名称
					String contactName = phoneCursor
							.getString(PHONES_DISPLAY_NAME_INDEX); // 得到联系人ID
					Long contactid = phoneCursor.getLong(PHONES_CONTACT_ID_INDEX); // 得到联系人头像ID
					Long photoid = phoneCursor.getLong(PHONES_PHOTO_ID_INDEX); // 得到联系人头像Bitamp
					if (photoid > 0) {
						Uri uri = ContentUris.withAppendedId(
								ContactsContract.Contacts.CONTENT_URI, contactid);
					}
					Log.i("Contacts", contactName+"="+phoneNumber);
					String[] contact = new String[]{contactName , phoneNumber};
					if(checkAdd(contact, contactList)){
						contactList.add(contact);
					}				
				}
				phoneCursor.close();
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		
	}

	/** 得到手机SIM卡联系人人信息 **/
	public void getSIMContacts(Context mContext, List<String[]> contactList) {
		try {
			ContentResolver resolver = mContext.getContentResolver(); // 获取Sims卡联系人
			Uri uri = Uri.parse("content://icc/adn");
			Cursor phoneCursor = resolver.query(uri, PHONES_PROJECTION, null, null,
					null);
			if (phoneCursor != null) {
				while (phoneCursor.moveToNext()) { // 得到手机号码
					String phoneNumber = phoneCursor.getString(PHONES_NUMBER_INDEX); // 当手机号码为空的或者为空字段
																						// 跳过当前循环
					if (TextUtils.isEmpty(phoneNumber))
						continue; // 得到联系人名称
					String contactName = phoneCursor
							.getString(PHONES_DISPLAY_NAME_INDEX); // Sim卡中没有联系人头像
					Log.i("Contacts SIM", contactName+"="+phoneNumber);
					String[] contact = new String[]{contactName , phoneNumber};
					if(checkAdd(contact, contactList)){
						contactList.add(contact);
					}
				}
				phoneCursor.close();
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		
	}
	
	public boolean checkAdd(String[] c , List<String[]> cs){
		if(c == null || c.length != 2 || isBank(c[0]) || isBank(c[1])){
			return false;
		}
		for (String[] cc : cs) {
			if(cc.length != c.length) return false;
			if(cc[0].equals(c[0]) && cc[1].equals(c[1])){
				return false;
			}
		}
		return true;
	}

	public boolean isBank(String s){
		if(s == null || s.trim().length() == 0) return true;
		return false;
	}
	
}
