package com.game.util;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Iterator;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.text.format.Time;
import android.util.Log;

import com.tencent.mm.sdk.modelpay.PayReq;
import com.tencent.mm.sdk.openapi.IWXAPI;
import com.tencent.mm.sdk.openapi.WXAPIFactory;
import com.tencent.mm.sdk.modelmsg.SendAuth;
import com.tencent.mm.sdk.modelmsg.SendMessageToWX;
import com.tencent.mm.sdk.modelmsg.WXImageObject;
import com.tencent.mm.sdk.modelmsg.WXMediaMessage;
import com.tencent.mm.sdk.modelmsg.WXTextObject;
import com.tencent.mm.sdk.modelmsg.WXWebpageObject;
import com.game.game678.*;
import com.game.constant.IWeixinConstants;
import com.game.jni.JniHelper;

public class WeixinShareProxy {
	
	private static final int THUMB_SIZE = 100;
	
	private IWXAPI api;
	
	private static WeixinShareProxy proxy = null;

	protected WeixinShareProxy(){
		api = WXAPIFactory.createWXAPI(JniHelper.instance(), IWeixinConstants.APP_ID);
		api.registerApp(IWeixinConstants.APP_ID);
	}
	
	public static WeixinShareProxy instance(){
		if(proxy == null){
			proxy = new WeixinShareProxy();
		}
		return proxy;
	}
	
	public void openWX(String params) {
		api.openWXApp();
	}
	
	//微信支付
		public void pay(String params) {
			//参数检验
			
			String[] arrParam = params.split(",");
			
			//生成订单信息
			StringBuilder urlStr = new StringBuilder("https://pay.game678.com.cn/WeiXinAppPayCreateOrder.ashx?");
			urlStr.append("userid=").append(arrParam[0]);
			urlStr.append("&gameid=").append(arrParam[1]);
			urlStr.append("&total_amount=").append(arrParam[2]).append("00");
			Log.i("微信支付",urlStr.toString());
			
			Log.i("微信支付", "获取订单中...");
	        try{
				byte[] buf = com.game.util.Util.httpGet(urlStr.toString());
				if (buf != null && buf.length > 0) {
					String content = new String(buf);
					Log.i("微信支付",content);
		        	JSONObject json = new JSONObject(content); 
					if(null != json){
							PayReq req = new PayReq();
							req.appId			= json.getString("appid");
							req.partnerId		= json.getString("partnerid");
							req.prepayId		= json.getString("prepayid");
							req.nonceStr		= json.getString("noncestr");
							req.timeStamp		= json.getString("timestamp");
							req.packageValue	= json.getString("package");
							req.sign			= json.getString("sign");
							
							Log.i("微信支付appId",req.appId);
							Log.i("微信支付partnerId",req.partnerId);
							Log.i("微信支付prepayId",req.prepayId);
							Log.i("微信支付nonceStr",req.nonceStr);
							Log.i("微信支付timeStamp",req.timeStamp);
							Log.i("微信支付packageValue",req.packageValue);
							Log.i("微信支付sign",req.sign);

							api.sendReq(req);
							Log.i("微信支付","正常调起支付");
					}else{
						Log.e("微信支付", "ErrorCode"+json.getString("error"));
			        	Log.e("微信支付", "返回错误"+json.getString("retmsg"));
					}
				}else{
		        	Log.e("微信支付", "服务器请求错误");
		        }
	        }catch(Exception e){
	        	Log.e("微信支付", "异常："+e.getMessage());
	        }
		}

	//微信分享
	public void share(String params){
		
		String[] arrParam = params.split(",");
		//分享网页或图片
		boolean isShareWebpage = true;
		//分享给朋友圈或好友
		boolean isShareToTimeline = true;
		int tepyc = Integer.parseInt(arrParam[2]);
		Log.e("qqqqqqqqqqqqqqqqq", params);
		if(tepyc == 1)
		{
			isShareToTimeline = true;
		}
		else
		{
			isShareToTimeline = false;
			Log.e("aaaaaaaaaaaa","eeeeeeeeeeeeeeeee");
		}
		
		try {
			//1、分享网页
			if(isShareWebpage) {
				// 创建链接分享
				StringBuilder urlStr = new StringBuilder("http://wap.789y.com/wx/index.aspx?");
				urlStr.append("code=").append(arrParam[0]);
				
				WXWebpageObject webpage = new WXWebpageObject();
				webpage.webpageUrl = urlStr.toString();
				Log.e("urlStr = ",urlStr.toString());
				WXMediaMessage msg = new WXMediaMessage(webpage);
				if(tepyc == 1) //朋友圈
				{
					msg.title = arrParam[3];
				}
				else
				{
					msg.title = "星星游戏中心";
					msg.description = arrParam[3];
				}
				
				
				
				Bitmap bmp;
				bmp = BitmapFactory.decodeStream(new URL("http://www.789y.com/temp/New/images/wknh.png").openStream());
				Bitmap thumbBmp = Bitmap.createScaledBitmap(bmp, THUMB_SIZE, THUMB_SIZE, true);
				bmp.recycle();
				msg.thumbData = Util.bmpToByteArray(thumbBmp, true);
				
				//发送分享请求
				SendMessageToWX.Req req = new SendMessageToWX.Req();
				req.transaction = buildTransaction("webpage"); // transaction字段用于唯一标识一个请求
				req.message = msg;
				req.scene = isShareToTimeline ? SendMessageToWX.Req.WXSceneTimeline : SendMessageToWX.Req.WXSceneSession;

				// 调用api接口发送数据到微信
				api.sendReq(req);
				Log.e("微信分享", "发送网页分享请求");
			}
			//2、分享图片
			else {
				//创建图片分享
				WXImageObject imgObj = new WXImageObject();
				Bitmap bmp;
				bmp = BitmapFactory.decodeStream(new URL("http://www.789y.com/images/activity/wxs.jpg").openStream());
				imgObj.imageData = Util.bmpToByteArray(bmp, true);
				WXMediaMessage msg = new WXMediaMessage(imgObj);
				msg.title = "星星游戏中心";
				msg.description = "星星游戏中心";
				
				//发送分享请求
				SendMessageToWX.Req req = new SendMessageToWX.Req();
				req.transaction = buildTransaction("img"); // transaction字段用于唯一标识一个请求
				req.message = msg;
				req.scene = isShareToTimeline ? SendMessageToWX.Req.WXSceneTimeline : SendMessageToWX.Req.WXSceneSession;
				// 调用api接口发送数据到微信
				api.sendReq(req);
				Log.e("微信分享", "发送图片请求");
			}
		} catch (MalformedURLException e) {
			Log.e("微信分享", "异常："+e.getMessage());
		} catch (IOException e) {
			Log.e("微信分享", "异常："+e.getMessage());
		}
	}
	
	//微信登录
	public void login(String params) {
		try{
			// send oauth request 
		    final SendAuth.Req req = new SendAuth.Req();
		    req.scope = "snsapi_userinfo";
		    req.state = "game678";  //star_game
		    boolean b = api.sendReq(req);
		    if(b)
		    {
				Log.e("微信登录", "调用成功");		    	
		    }
		    else 
		    {
		    	Log.e("微信登录", "调用失败");	
		    }
		    
		} catch(Exception e){
        	Log.e("微信登录", "异常："+e.getMessage());
        }
	}
	
	//是否安装了微信
	public boolean isWXAppInstalled()
	{
		return api.isWXAppInstalled();
	}
	
	private String buildTransaction(final String type) {
		return (type == null) ? String.valueOf(System.currentTimeMillis()) : type + System.currentTimeMillis();
	}
	
	public String md5(String string) {
	    byte[] hash;
	    try {
	        hash = MessageDigest.getInstance("MD5").digest(string.getBytes("UTF-8"));
	    } catch (NoSuchAlgorithmException e) {
	        throw new RuntimeException("Huh, MD5 should be supported?", e);
	    } catch (UnsupportedEncodingException e) {
	        throw new RuntimeException("Huh, UTF-8 should be supported?", e);
	    }

	    StringBuilder hex = new StringBuilder(hash.length * 2);
	    for (byte b : hash) {
	        if ((b & 0xFF) < 0x10) hex.append("0");
	        hex.append(Integer.toHexString(b & 0xFF));
	    }
	    return hex.toString();
	}
	
	public String getRandomString(int length) { //length表示生成字符串的长度  
	    String base = "abcdefghijklmnopqrstuvwxyz0123456789";     
	    Random random = new Random();     
	    StringBuffer sb = new StringBuffer();     
	    for (int i = 0; i < length; i++) {     
	        int number = random.nextInt(base.length());     
	        sb.append(base.charAt(number));     
	    }     
	    return sb.toString();     
	 } 
}
