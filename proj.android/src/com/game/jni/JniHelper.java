package com.game.jni;

import android.util.Log;

import com.game.constant.ICMD;
import com.game.constant.IPayConfig;
import com.game.exception.PayExcetion;
import com.game.game678.game678;
import com.game.util.AlipayHelper;
import com.game.util.AndroidHelper;
import com.game.util.CardPayProxy;
import com.game.util.ContactsHelper;
import com.game.util.HttpProxy;
//import com.game.util.MMSMSProxy;
import com.game.util.*;
//import com.moling.util.SendMsgHelper;//读取用户信息发短信的。
import com.game.util.TipHelper;
import com.game.util.WebBrowseProxy;
import com.game.util.WeiboShareProxy;
import com.game.util.WeixinShareProxy;

import android.app.DownloadManager;
import android.app.DownloadManager.Request;
import android.net.Uri;
import android.provider.Settings.Secure;
import android.telephony.TelephonyManager;


public class JniHelper implements IPayConfig , ICMD{

	public static final String TAG = "JNI";
	
	public static game678 mActivity;
	
 
	public static native void callCCommand(int command , String params);
	
	 
	public static void callJavaCommand(int command , String params){
		Log.i(TAG, "command="+command+",params="+params);
		switch (command)
		{
		case CMD_CTJ_WXLogin:	//微信登陆
		{
			if(WeixinShareProxy.instance().isWXAppInstalled())
			{
				WeixinShareProxy.instance().login(params);
			}
			else 
			{
				if(mActivity != null) 
				{
					showMessage("请先安装微信！");
				}
			}
			break;
		}
		case CMD_CTJ_OpenWX: //打开微信
		{
			Log.i("aaaaaaaaaaaaaaaaaaa", "bbbbbbbbbbbbbbbbbb");
			if(WeixinShareProxy.instance().isWXAppInstalled()) 
			{
				sendMessage(OpenWX , params);
			}
			else
			{
				if(mActivity != null) 
				{
					showMessage("请先安装微信！");
				}
			}
			break;
		}
		case CMD_CTJ_ShareWX: //分享
		{
			if(WeixinShareProxy.instance().isWXAppInstalled()) {
				WeixinShareProxy.instance().share(params);
			}
			else {
				if(mActivity != null) {
					showMessage("请先安装微信！");
				}
			}
			break;
		}
		case CMD_CTJ_LockPhone:
		{ 
			//WebBrowseProxy.instance().openWebBrowse("http://app.game621.com/GameLocker.apk");
			//sendMessage(LockPhone , params);
			break;
		}
		case CMD_CTJ_GetDeviceID:
		{
			sendMessage(CreateDeviceID_REQUEST , params);
			break;
		}
		case CMD_CTJ_NeedUpdate:
		{
			sendMessage(UpdateGame , params);
			break;
		}
		case CMD_COPY_ID:
		{
			sendMessage(CopyID_REQUEST , params);
			break;
		}
		case CMD_CloseGame:
		{
			sendMessage(CloseGame , params);
			break;
		}
		case CMD_CTJ_PAY:
		{
			sendMessage(SEND_ORDER_REQUEST , params);
			break;
		}
		case CMD_CTJ_PAY_PATCH:
		{
			PayHelper.patchOrder(params);
			break;
		}
		case CMD_CTJ_BROWSE:
			WebBrowseProxy.instance().openWebView(params);
			break;
		case CMD_CTJ_BROWSE_CLOSE:
			WebBrowseProxy.instance().sendCloseMessage();
			break;
		case CMD_CTJ_VIBRATE:
		{
			if(params.split(",").length > 1){
				String[] ll = params.split(",");
				long[] pattern = new long[ll.length];
				for (int i = 0; i < ll.length; i++) {
					pattern[i] = Long.parseLong(ll[i]);
				}
				TipHelper.Vibrate(JniHelper.instance(), pattern , false);
			}else{
				TipHelper.Vibrate(JniHelper.instance(), Long.parseLong(params));
			}			
			break;
		}
		case CMD_CTJ_READCONTACTS:
			ContactsHelper contact = new ContactsHelper();
			String str = contact.createToString(contact.getAllContacts());
			callCCommand(CMD_JTC_READCONTACTS , str);
			break;
		/*case CMD_CTJ_SENDMSG:
			String[] pp = params.split(",");
			SendMsgHelper.instance().send(pp[0], pp[1]);
			callCCommand(CMD_JTC_SENDMSG , pp[0]);
			break;*/
		case CMD_CTJ_OPENSYSBROWSE:
			WebBrowseProxy.instance().openWebBrowse(params);
			break;
		case CMD_CTJ_WIFI:
		{
			boolean b = TipHelper.isWiFiActive(JniHelper.instance());
			callCCommand(CMD_JTC_WIFI , b?"1":"0");
		}
			break;
		case CMD_CTJ_SENDHTTPREQUEST:
			HttpProxy.instance().execute(params);
			break;
		case CMD_CTJ_DOWNLOADAPK:
			sendMessage(DOWNLOAD_CMD, params);
			break;
		case CMD_CTJ_WX:
		{
			WeixinShareProxy.instance().share(params);
			break;
		}
		case CMD_CTJ_WB:
		{
			WeiboShareProxy.instance().share(params);
			break;
		}		
		case CMD_CTJ_UUID:
		{
			new AndroidHelper(JniHelper.instance());
			callCCommand(CMD_JTC_UUID, AndroidHelper.getDeviceUuid().toString());
			break;
		}
		case CMD_CTJ_WX_PAY://微信支付
		{
			if(WeixinShareProxy.instance().isWXAppInstalled())
			{
				WeixinShareProxy.instance().pay(params);
			}
			else 
			{
				if(mActivity != null) 
				{
					showMessage("请先安装微信！");
				}
			}
			break;
		}
		default:
			break;
		}
		

	}
		
	public static final int PAY_TYPE_ALIPAY = 2;
	
	public static final int PAY_TYPE_SHORTMESSAGE = 3;
	
	public static final int PAY_TYPE_MI = 4;
	
	public static final int PAY_TYPE_IOS = 5;
	
	public static final int PAY_TYPE_MOBILE = 6;
	
	public static final int PAY_TYPE_TELE = 7;
	
	public static final int PAY_TYPE_UNICOM = 8;
	
	public static final int PAY_TYPE_SD = 9;
	
	public static final int PAY_TYPE_WY = 10;
	
	public static final int PAY_TYPE_JW = 11;
	
	public static final int PAY_TYPE_UPAY = 12;//优贝充值

	
	public static void handlerPayRequest(int infullType , String params){
		switch (infullType) {
		case PAY_TYPE_ALIPAY:
			AlipayHelper.instance().sendAlipay(params);
			break;
		case PAY_TYPE_SHORTMESSAGE:
			break;
		case PAY_TYPE_MI:
			break;
		case PAY_TYPE_MOBILE:
		case PAY_TYPE_TELE:
		case PAY_TYPE_UNICOM:
		case PAY_TYPE_SD:
		case PAY_TYPE_WY:
		case PAY_TYPE_JW:
			CardPayProxy.instance().sendCard(params);
			break;
		default:
			break;
		}
	}
	 
	public static synchronized game678 instance(){
		if(mActivity == null) mActivity = game678.instance();
		return mActivity;
	}
	
	public static void sendMessage(int what , String m){
		instance().mUIHandler.obtainMessage(what, m).sendToTarget();
	}
	
	public static void showMessage(String m){
		sendMessage(SHOW_MESSAGE, m);
	}
	
	public static void sendJniCallback(int cmd , String params){
		instance().mUIHandler.obtainMessage(JNI_CALL_BACK, new Object[]{cmd , params}).sendToTarget();
	}
	
}
