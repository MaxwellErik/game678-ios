
local BaseScene = class("BaseScene", function(sceneName)
    return display:newScene(sceneName)
end)

function BaseScene:ctor()
    if VERSION_DEBYG == true then
        print("BaseScnce")
    end
end

function BaseScene:onEnter()
    if VERSION_DEBYG == true then
        print("BaseScene:onEnter")
    end
    app.eventDispather:addListenerEvent(eventHallNetConnected, self, function(data)
        return self:onHallNetConnected(data)
    end)
    
    app.eventDispather:addListenerEvent(eventHallNetClosed, self, function(data)
        self:onHallNetClose(data)
    end)    
    
    app.eventDispather:addListenerEvent(eventLoginSrvLoginSuccessed, self, function(data)
        self:onLoginSrvLoginSuccess(data)
    end)
    
    app.eventDispather:addListenerEvent(eventLoginSrvLoginFailed, self, function(data)
        self:onLoginSrvLoginFailed(data)
    end)
    
    app.eventDispather:addListenerEvent(eventGameLoginFailed, self, function(data)
        self:onGameLoginFailed(data)
    end)
    
    app.eventDispather:addListenerEvent(eventGameReqFailed, self, function(data)
    	self:onGameReqFailed(data)
    end)
    
    app.eventDispather:addListenerEvent(eventRemoveLoading, self, function(data)
        self._loadingLayer = nil
        self:onLoadingCancle()
    end)   
end

function BaseScene:onEnterTransitionFinish()
    if VERSION_DEBYG == true then
        print( "BaseScene:onEnterTransitionFinish" )
    end
end

function BaseScene:onExit()
    app.eventDispather:delListenerEvent(self)
end

function BaseScene:onLoadingCancle()
end

--添加loading
function BaseScene:addLoading()
    if VERSION_DEBYG == true then
        print("BaseScene:addLoading")
    end
    if self._loadingLayer then
        return
    end
    
    self._loadingLayer = Loading:new()
    self:addChild(self._loadingLayer, 1000)
end

--移除loading
function BaseScene:delLoading()
    if self._loadingLayer  then
        self:removeChild(self._loadingLayer)
        self._loadingLayer = nil
    end
end

function BaseScene:showTip(showBtn, tips, okCallBack, cancleCallBack)
	local tipBox = BombBox.new(showBtn, tips, okCallBack, cancleCallBack)
	self:addChild(tipBox, 1000)
end

function BaseScene:onHallNetConnected(bConnected)
end

function BaseScene:onHallNetClose(data)
end

function BaseScene:onLoginSrvLoginSuccess(data)
    checkPayFailed()
end

function BaseScene:onLoginSrvLoginFailed(data)
	self:delLoading()
    MyToast.new(self,data.szDescribeString)
    custom.PlatUtil:closeServer(CLIENT_GAME)
end

function BaseScene:onGameLoginFailed(data)
    self:delLoading()
    MyToast.new(self,data.szDescribeString)
    custom.PlatUtil:closeServer(CLIENT_GAME)
end

function BaseScene:onGameReqFailed(data)
    self:delLoading()
    MyToast.new(self,data.szDescribeString)
    custom.PlatUtil:closeServer(CLIENT_GAME)
end

return BaseScene