--
-- Author: vincent
-- Date: 2016-03-22 18:32:38
--
--用户兑换记录条目
local ExchangeType =
{
    ExchangeType_Shopping = 1, -- 购买
    ExchangeType_Present = 2, -- 赠送
    ExchangeType_Receive = 3, -- 收到
    ExchangeType_Exchange = 4, -- 兑换
};



require("sdk.manager.NewStoreConfig")
local DataManager = require("app.uiManager.DataManager")

local NewStore = class("NewStore", import(".BaseScene"))

function NewStore:ctor(m_type)
	self._giveUserList = nil   --赠送的用户列表
	self._userAdress = nil --用户实物兑换信息
	self._shiwumsg = nil --拥有的实物碎片信息
	self.TargetPlatform = cc.Application:getInstance():getTargetPlatform() --设备系统
	self._isChinaIP = cc.UserDefault:getInstance():getIntegerForKey("IsChinaIP")

	self._shopLayer = ccs.GUIReader:getInstance():widgetFromJsonFile("HallUI/NewStore.json")
	self._bg = self._shopLayer:getChildByName("storebg")
	self:addChild(self._shopLayer)

	self._userInformation = self._bg:getChildByName("userInformationBtn")
	self._userInformation:setVisible(false)

	self._exitBtn = self._bg:getChildByName("exitBtn")
	self._exitBtn:addTouchEventListener(function(sender, type)
        if type == ccui.TouchEventType.ended then
            app.musicSound:playSound(SOUND_HALL_TOUCH)
            self:onDismiss()
        end
    end)
    self._leixingtip = self._bg:getChildByName("leixingtip") --购买类型Image
    self._playercoinNum = self._bg:getChildByName("coinkuang"):getChildByName("coinAtlasLabel") --玩家金币
    self:updateLoginInfo()

    self._onlinenum = self._bg:getChildByName("online_lable") --玩家在线人数
    self._onlinenum:setString("在线人数："..app.hallLogic._zongRenShu)

    self._ScrollView = self._bg:getChildByName("ScrollView") --商城ScrollView

	self._btnTab = {}
    self._buyCoinBtn = self._bg:getChildByName("buyCoinBtn") --购买金币Btn
    table.insert(self._btnTab, self._buyCoinBtn)
    self._buyCoinBtn:setTag(1)
    self._buyCoinBtn:addTouchEventListener(function(sender, type)
        if type == ccui.TouchEventType.ended then
            app.musicSound:playSound(SOUND_HALL_TOUCH)
            self:btnShow(sender:getTag())
        end
    end)

    self._buydaojuBtn = self._bg:getChildByName("buydaojuBtn") --购买道具Btn
    table.insert(self._btnTab, self._buydaojuBtn)
    self._buydaojuBtn:setTag(2)
    self._buydaojuBtn:addTouchEventListener(function(sender, type)
        if type == ccui.TouchEventType.ended then
            app.musicSound:playSound(SOUND_HALL_TOUCH)
            self:btnShow(sender:getTag())
        end
    end)

    self._buyshiwuBtn = self._bg:getChildByName("buyshiwuBtn") --购买实物Btn
    -- if self.TargetPlatform == cc.PLATFORM_OS_IPHONE or self.TargetPlatform == cc.PLATFORM_OS_IPAD then
    --     self._buyshiwuBtn:setVisible(false)
    --     self._buyshiwuBtn:setTouchEnabled(false)
    -- end
    if not self._isChinaIP or self._isChinaIP == 0 then
    	if not app.buttonConfig or app.buttonConfig["Entity"] ~= "1" then
	        self._buyshiwuBtn:setVisible(false)
	        self._buyshiwuBtn:setTouchEnabled(false)
	    end
    end
    table.insert(self._btnTab, self._buyshiwuBtn)
    self._buyshiwuBtn:setTag(3)
    self._buyshiwuBtn:addTouchEventListener(function(sender, type)
        if type == ccui.TouchEventType.ended then
            app.musicSound:playSound(SOUND_HALL_TOUCH)
            self:btnShow(sender:getTag())
        end
    end)

    self._buyjiluBtn = self._bg:getChildByName("buyjiluBtn") --购买记录Btn
    table.insert(self._btnTab, self._buyjiluBtn)
    self._buyjiluBtn:setTag(4)
    self._buyjiluBtn:addTouchEventListener(function(sender, type)
        if type == ccui.TouchEventType.ended then
            app.musicSound:playSound(SOUND_HALL_TOUCH)
            self:btnShow(sender:getTag())
        end
    end)

    if m_type then
    	self:btnShow(m_type)
    else
    	self:btnShow(1)
    end

    DataManager:getInstance():setNewStoreLayer(self)
    self:show()
    self:updateShiWuMessage()
end

--购买金币界面
function NewStore:buyCoinLayer()
	local coinconfig = NewStoreConfig.coinconfig
	self._ScrollView:removeAllChildren()
	self._ScrollView:setTouchEnabled(true)
	self._ScrollView:setContentSize(cc.size(1100, 440))
	local innerWidth = self._ScrollView:getContentSize().width
    local innerHeight = self._ScrollView:getContentSize().height
    self._ScrollView:setInnerContainerSize(cc.size(innerWidth, innerHeight/2*math.ceil(#coinconfig/4)))

    for k, v in pairs( coinconfig ) do
    	local button = ccui.Button:create()
	    button:setTouchEnabled(true)
	    local string = "HallUI/NewStore/"..v.image
	    button:loadTextures(string, string, "")
	    local innerconHeight = self._ScrollView:getInnerContainerSize().height
	    local buttonsize = button:getContentSize()
	    button:setTag(v.goodsIndex)
	    button:addTouchEventListener(function(sender, type)
	        if type == ccui.TouchEventType.ended then
	            app.musicSound:playSound(SOUND_HALL_TOUCH)
	            pay(sender:getTag())
	        end
	    end)
	    
	    local heightnum = math.ceil(k/4)
	    if (k-1)%4+1 == 1 then
	    	button:setPosition(cc.p(innerWidth/2-buttonsize.width*1.5-90, innerconHeight-buttonsize.height*(heightnum-0.5)))
	    elseif (k-1)%4+1 == 2 then
	    	button:setPosition(cc.p(innerWidth/2-buttonsize.width*0.5-30, innerconHeight-buttonsize.height*(heightnum-0.5)))
	    elseif (k-1)%4+1 == 3 then
	    	button:setPosition(cc.p(innerWidth/2+buttonsize.width*0.5+30, innerconHeight-buttonsize.height*(heightnum-0.5)))
	    elseif (k-1)%4+1 == 4 then
	    	button:setPosition(cc.p(innerWidth/2+buttonsize.width*1.5+90, innerconHeight-buttonsize.height*(heightnum-0.5)))
	    end
	    self._ScrollView:addChild(button)
    end
end

--购买道具界面
function NewStore:buyDaojuLayer()
	app.hallLogic:queryGiveList() --请求用户赠送列表
	
	local daojuconfig = NewStoreConfig.daojuconfig
	self._ScrollView:removeAllChildren()
	self._ScrollView:setTouchEnabled(true)
	self._ScrollView:setContentSize(cc.size(1100, 440))
	local innerWidth = self._ScrollView:getContentSize().width
    local innerHeight = self._ScrollView:getContentSize().height
    self._ScrollView:setInnerContainerSize(cc.size(innerWidth, innerHeight/2*math.ceil(#daojuconfig/3)))
    for k, v in pairs( daojuconfig ) do
    	local node = ccs.GUIReader:getInstance():widgetFromJsonFile("HallUI/StoredaojuNode.json")
    	node:setAnchorPoint(0.5, 0.5)
    	local string = "HallUI/NewStore/"..v.image
	    node:getChildByName("image"):loadTextures(string, string, "")
		local innerconHeight = self._ScrollView:getInnerContainerSize().height
    	local buttonsize = node:getContentSize()
    	local heightnum = math.ceil(k/3)
	    if (k-1)%3+1 == 1 then
	    	node:setPosition(cc.p(innerWidth/2-buttonsize.width-50, innerconHeight-(buttonsize.height+20)*(heightnum-0.5)))
	    elseif (k-1)%3+1 == 2 then
	    	node:setPosition(cc.p(innerWidth/2, innerconHeight-(buttonsize.height+20)*(heightnum-0.5)))
	    elseif (k-1)%3+1 == 3 then
	    	node:setPosition(cc.p(innerWidth/2+buttonsize.width+50, innerconHeight-(buttonsize.height+20)*(heightnum-0.5)))
	    end

	    local goodsBtn = node:getChildByName("image")
	    goodsBtn:setTag(k)
	    goodsBtn:addTouchEventListener(function(sender, type)
	        if type == ccui.TouchEventType.ended then
	            app.musicSound:playSound(SOUND_HALL_TOUCH)
	            self:goodsDetail(sender:getTag())
	        end
	    end)

	    local giveBtn = node:getChildByName("image"):getChildByName("zengsongBtn")
	    if not self._isChinaIP or self._isChinaIP == 0 then
	    	if not app.buttonConfig or app.buttonConfig["Give"] ~= "1" then
	        	giveBtn:setVisible(false)
	        end
	    end
	    giveBtn:setTag(k)
	    giveBtn:addTouchEventListener(function(sender, type)
	        if type == ccui.TouchEventType.ended then
	            app.musicSound:playSound(SOUND_HALL_TOUCH)
	            self:giveDetailLayer(sender:getTag())
	        end
	    end)
	    if v.name == "小喇叭" then
	    	giveBtn:setVisible(false)
	    end

	    local buyBtn = node:getChildByName("image"):getChildByName("goumaiBtn")
	    buyBtn:setTag(k)
	    buyBtn:addTouchEventListener(function(sender, type)
	        if type == ccui.TouchEventType.ended then
	            app.musicSound:playSound(SOUND_HALL_TOUCH)
	            self:buyDetailLayer(sender:getTag())
	        end
	    end)

    	self._ScrollView:addChild(node)
    end
end

--商品详细介绍
function NewStore:goodsDetail(num)
	local daojuconfig = NewStoreConfig.daojuconfig
	local goodsdetaillayer = ccs.GUIReader:getInstance():widgetFromJsonFile("HallUI/GoodsDetail.json")	
	self._shopLayer:addChild(goodsdetaillayer, 99)

	local detail = goodsdetaillayer:getChildByName("goodsdetailbg")
	detail:getChildByName("goodspic"):loadTexture("HallUI/NewStore/"..daojuconfig[num].imagename)
	detail:getChildByName("goodsname"):setString(daojuconfig[num].name)
	detail:getChildByName("detailLable"):setString(daojuconfig[num].detail)

	local closeBtn = detail:getChildByName("closeBtn")
    closeBtn:addTouchEventListener(function(sender, type)
        if type == ccui.TouchEventType.ended then
            app.musicSound:playSound(SOUND_HALL_TOUCH)
            if goodsdetaillayer then goodsdetaillayer:removeFromParent() end
        end
    end)
end

--购买实物界面
function NewStore:buyShiwuLayer(m_tab)
	local m_tab = app.hallLogic._shiwuNumTab
	local shiwuconfig = NewStoreConfig.shiwuconfig
	self._ScrollView:removeAllChildren()
	self._ScrollView:setTouchEnabled(true)
	self._ScrollView:setContentSize(cc.size(1100, 440))
	local innerWidth = self._ScrollView:getContentSize().width
    local innerHeight = self._ScrollView:getContentSize().height
    self._ScrollView:setInnerContainerSize(cc.size(innerWidth, innerHeight/2*math.ceil(#shiwuconfig/4)))

    self._userInformation:setVisible(true)
    self._userInformation:addTouchEventListener(function(sender, type)
        if type == ccui.TouchEventType.ended then
            app.musicSound:playSound(SOUND_HALL_TOUCH)
            self:userMessageLayer()
        end
    end)

    for k, v in pairs( shiwuconfig ) do
    	local picbg = display.newSprite("HallUI/NewStore/shiwubg.png")
	    local picsize = picbg:getContentSize()

    	local button = ccui.Button:create()
	    button:setTouchEnabled(true)
	    local string = "HallUI/NewStore/"..v.image
	    button:loadTextures(string, string, "")
	    local innerconHeight = self._ScrollView:getInnerContainerSize().height
	    button:setTag(m_tab.stItem[k].wPropsID)
	    button:addTouchEventListener(function(sender, type)
	        if type == ccui.TouchEventType.ended then
	            app.musicSound:playSound(SOUND_HALL_TOUCH)
	            local n = sender:getTag()
	            if m_tab.stItem[n].dwPieceAmount >= m_tab.stItem[n].dwPieceExchangeAmount then
	            	if self._userAdress then
	            		self:buyDetailLayer(n, true)
		            else
		            	self:tipRemindLayer()
		            end
	            else
	            	MyToast.new(self._shopLayer, "兑换失败，碎片数量不足")
	           	end 
	        end
	    end)
	    button:setPosition(cc.p(picsize.width/2, picsize.height/2+15))
	    picbg:addChild(button)

	    --个数
	    local lable = cc.Label:create()
	    lable:setSystemFontName("微软雅黑")
	    lable:setSystemFontSize(25)
	    lable:setTextColor(cc.c3b(129,120,127))
	    lable:setString("(拥有"..m_tab.stItem[k].dwPieceAmount.."个)") 
	    lable:setPosition(cc.p(picsize.width/2, -20))
	    picbg:addChild(lable)

	    --名字
	    local lable1 = cc.Label:create()
	    lable1:setSystemFontName("微软雅黑")
	    lable1:setSystemFontSize(20)
	    lable1:setTextColor(cc.c3b(255,255,255))
	    lable1:setString(v.name) 
	    lable1:setPosition(cc.p(picsize.width/2, picsize.height-12))
	    picbg:addChild(lable1)

	    --兑换要求
	    local lable2 = cc.Label:create()
	    lable2:setSystemFontName("微软雅黑")
	    lable2:setSystemFontSize(23)
	    lable2:setTextColor(cc.c3b(55,26,0))
	    lable2:setString("花费"..m_tab.stItem[k].dwPieceExchangeAmount.."个碎片兑换") 
	    lable2:setPosition(cc.p(picsize.width/2, 25))
	    picbg:addChild(lable2)
	    
	    local heightnum = math.ceil(k/4)
	    if (k-1)%4+1 == 1 then
	    	picbg:setPosition(cc.p(innerWidth/2-picsize.width*1.5-30, innerconHeight-picsize.height*(heightnum-0.5)))
	    elseif (k-1)%4+1 == 2 then
	    	picbg:setPosition(cc.p(innerWidth/2-picsize.width*0.5-10, innerconHeight-picsize.height*(heightnum-0.5)))
	    elseif (k-1)%4+1 == 3 then
	    	picbg:setPosition(cc.p(innerWidth/2+picsize.width*0.5+10, innerconHeight-picsize.height*(heightnum-0.5)))
	    elseif (k-1)%4+1 == 4 then
	    	picbg:setPosition(cc.p(innerWidth/2+picsize.width*1.5+30, innerconHeight-picsize.height*(heightnum-0.5)))
	    end
	    self._ScrollView:addChild(picbg)
    end
end

--提示界面
function NewStore:tipRemindLayer()
	local tempTab = NewStoreConfig.tipconfig

	local node = ccs.GUIReader:getInstance():widgetFromJsonFile("HallUI/PublicTipUi.json")	
	self._shopLayer:addChild(node, 100)
	local layer = node:getChildByName("bg")

	local lable = layer:getChildByName("contentLable")
	lable:setString(tempTab.string)

	local sureBtn = layer:getChildByName("sureBtn")
	sureBtn:loadTextures(tempTab.sureBtn1name, tempTab.sureBtn2name, "")
	sureBtn:addTouchEventListener(function(sender, type)
	        if type == ccui.TouchEventType.ended then
	            app.musicSound:playSound(SOUND_HALL_TOUCH)  
	            if node then node:removeFromParent() end
	            self:userMessageLayer()
	        end
		end)

	local cancelBtn = layer:getChildByName("cancelBtn")
	cancelBtn:loadTextures(tempTab.cancelBtn1name, tempTab.cancelBtn2name, "")
	cancelBtn:addTouchEventListener(function(sender, type)
	        if type == ccui.TouchEventType.ended then
	            app.musicSound:playSound(SOUND_HALL_TOUCH)  
	            if node then node:removeFromParent() end
	        end
		end)
end

--用户信息填写界面
function NewStore:userMessageLayer()
	local node = ccs.GUIReader:getInstance():widgetFromJsonFile("HallUI/ChangeShiWuMessage.json")	
	self._shopLayer:addChild(node, 100)
	local layer = node:getChildByName("bg")

	local nametext = layer:getChildByName("namebg"):getChildByName("nameTextField")
	local adresstext = layer:getChildByName("adressbg"):getChildByName("adressTextField")
	local phonetext = layer:getChildByName("phonebg"):getChildByName("phoneTextField")
	if self._userAdress then
		nametext:setString(self._userAdress.szContact)
		adresstext:setString(self._userAdress.szAddress)
		phonetext:setString(self._userAdress.szMobilePhone)
	end

	local sureBtn = layer:getChildByName("sureBtn")
	sureBtn:addTouchEventListener(function(sender, type)
	        if type == ccui.TouchEventType.ended then
	            app.musicSound:playSound(SOUND_HALL_TOUCH)  
	            local name = nametext:getString()
	            local adress = adresstext:getString()
	            local phone = phonetext:getString()
	            if not phone or not adress or not name then
	            	MyToast.new(self._shopLayer, "请将信息填写完整")
	            elseif string.len(phone) ~= 11 then
	            	MyToast.new(self._shopLayer, "电话号码填写有误")
	            else
	            	app.hallLogic:sendRessMessage(adress, name, phone)
	            	if node then node:removeFromParent() end
	            end  
	        end
		end)

	local closeBtn = layer:getChildByName("closeBtn")
	closeBtn:addTouchEventListener(function(sender, type)
	        if type == ccui.TouchEventType.ended then
	            app.musicSound:playSound(SOUND_HALL_TOUCH)  
	            if node then node:removeFromParent() end
	        end
		end)
end

--购买界面
--@m_bool是否是兑换实物
function NewStore:buyDetailLayer(num, m_bool)
	local daojuconfig = NewStoreConfig.daojuconfig
	if m_bool then
		daojuconfig = NewStoreConfig.shiwuconfig
	end
	local StoreBuylayer = ccs.GUIReader:getInstance():widgetFromJsonFile("HallUI/StoreBuy.json")	
	self._shopLayer:addChild(StoreBuylayer, 99)

	local layer = StoreBuylayer:getChildByName("bg")
	local buyNamePic = layer:getChildByName("buyNamePic") --显示图片
	local string = "HallUI/NewStore/"..daojuconfig[num].imagename
	buyNamePic:loadTextures(string, string, "")
	local buyNameLable = layer:getChildByName("buyNameLable") --名字
	buyNameLable:setString(daojuconfig[num].name)

	if not m_bool then
		local buyCount = layer:getChildByName("buykuang"):getChildByName("buyCount") --购买数量
		buyCount:setString(1)
		local costAtlasLabel = layer:getChildByName("coinpricebg"):getChildByName("costAtlasLabel") --购买总价格
		costAtlasLabel:setString(daojuconfig[num].price)

		layer:getChildByName("coinpricebg"):getChildByName("Label"):setVisible(false)

		local addBtn = layer:getChildByName("buykuang"):getChildByName("addBtn")
		addBtn:addTouchEventListener(function(sender, type)
		        if type == ccui.TouchEventType.ended then
		            app.musicSound:playSound(SOUND_HALL_TOUCH)
		            local count = tonumber(buyCount:getString()) 
		            if daojuconfig[num].price*(count+1) <= app.hallLogic._loginSuccessInfo.lUserScore then
		            	buyCount:setString(count+1) 
		            	costAtlasLabel:setString(daojuconfig[num].price*(count+1))
		            else
		            	MyToast.new(StoreBuylayer, "金币不足")
		            end
		        end
		    end)

		local subBtn = layer:getChildByName("buykuang"):getChildByName("subBtn")
		subBtn:addTouchEventListener(function(sender, type)
		        if type == ccui.TouchEventType.ended then
		            app.musicSound:playSound(SOUND_HALL_TOUCH)
		            local count = tonumber(buyCount:getString())
		            if count - 1 > 0 then
		            	buyCount:setString(count-1) 
		            	costAtlasLabel:setString(daojuconfig[num].price*(count-1))
		            end
		            
		        end
		    end)

		local sureBtn = layer:getChildByName("sureBtn")
		sureBtn:addTouchEventListener(function(sender, type)
		        if type == ccui.TouchEventType.ended then
		            app.musicSound:playSound(SOUND_HALL_TOUCH)
		            if tonumber(costAtlasLabel:getString()) <= app.hallLogic._loginSuccessInfo.lUserScore then
		            	local buynum = tonumber(buyCount:getString())
		            	if num == 4 then --喇叭
		            		app.hallLogic:buyTrumpet(buynum)
		            	else --num对应服务器配置的第几个面值宝箱
		            		local typenum = daojuconfig[num].typenum
		            		app.hallLogic:buyRedEnvelope(app.hallLogic._redEnvelopeInfo.items[typenum].dwRMBValue, buynum)
		            		-- app.hallLogic:buyRedEnvelope(daojuconfig[num].price, buynum)
		            	end
		            else
		            	MyToast.new(StoreBuylayer, "购买失败，金币不足")
		            end
		        end
		    end)
	else
		buyNamePic:setScale(0.7)
		local tempTab = self._shiwumsg.stItem[num]
		local buyCount = layer:getChildByName("buykuang"):getChildByName("buyCount") --购买数量
		buyCount:setString(1)
		local costAtlasLabel = layer:getChildByName("coinpricebg"):getChildByName("costAtlasLabel") --购买总价格
		costAtlasLabel:setString(tempTab.dwPieceExchangeAmount)

		layer:getChildByName("coinpricebg"):getChildByName("cointip"):setVisible(false)

		local addBtn = layer:getChildByName("buykuang"):getChildByName("addBtn")
		addBtn:addTouchEventListener(function(sender, type)
		        if type == ccui.TouchEventType.ended then
		            app.musicSound:playSound(SOUND_HALL_TOUCH)
		            local count = tonumber(buyCount:getString()) 
		            if tonumber(tempTab.dwPieceExchangeAmount*(count+1)) <= tempTab.dwPieceAmount then
		            	buyCount:setString(count+1) 
		            	costAtlasLabel:setString(tempTab.dwPieceExchangeAmount*(count+1))
		            else
		            	MyToast.new(StoreBuylayer, "碎片不足")
		            end
		        end
		    end)

		local subBtn = layer:getChildByName("buykuang"):getChildByName("subBtn")
		subBtn:addTouchEventListener(function(sender, type)
		        if type == ccui.TouchEventType.ended then
		            app.musicSound:playSound(SOUND_HALL_TOUCH)
		            local count = tonumber(buyCount:getString())
		            if count - 1 > 0 then
		            	buyCount:setString(count-1) 
		            	costAtlasLabel:setString(tempTab.dwPieceExchangeAmount*(count-1))
		            end 
		        end
		    end)

		local sureBtn = layer:getChildByName("sureBtn")
		sureBtn:addTouchEventListener(function(sender, type)
		        if type == ccui.TouchEventType.ended then
		            app.musicSound:playSound(SOUND_HALL_TOUCH)
		            -- if tonumber(tempTab.dwPieceExchangeAmount*(count+1)) <= tempTab.dwPieceAmount then
		            	local buynum = tonumber(buyCount:getString())
		            	app.hallLogic:changeRealProps(tempTab.wPropsID, buynum)
		            -- else
		            -- 	MyToast.new(StoreBuylayer, "兑换失败，碎片不足")
		            -- end
		        end
		    end)
	end

	local closeBtn = layer:getChildByName("closeBtn")
	closeBtn:addTouchEventListener(function(sender, type)
	        if type == ccui.TouchEventType.ended then
	            app.musicSound:playSound(SOUND_HALL_TOUCH)
	            if StoreBuylayer then StoreBuylayer:removeFromParent() end
	        end
	    end)

	local cancelBtn = layer:getChildByName("cancelBtn")
	cancelBtn:addTouchEventListener(function(sender, type)
	        if type == ccui.TouchEventType.ended then
	            app.musicSound:playSound(SOUND_HALL_TOUCH)
	            if StoreBuylayer then StoreBuylayer:removeFromParent() end
	        end
	    end)
end

--赠送界面
function NewStore:giveDetailLayer(num)
	local daojuconfig = NewStoreConfig.daojuconfig
	local giveLayer = ccs.GUIReader:getInstance():widgetFromJsonFile("HallUI/StoreGive.json")
	self._shopLayer:addChild(giveLayer, 99)

	local layer = giveLayer:getChildByName("bg")
	local buyNamePic = layer:getChildByName("buyNamePic") --显示图片
	local string = "HallUI/NewStore/"..daojuconfig[num].imagename
	buyNamePic:loadTextures(string, string, "")
	local buyNameLable = layer:getChildByName("buyNameLable") --名字
	buyNameLable:setString(daojuconfig[num].name)
	local buyCount = layer:getChildByName("buykuang"):getChildByName("buyCount") --购买数量
	buyCount:setString(1)
	local costAtlasLabel = layer:getChildByName("coinpricebg"):getChildByName("costAtlasLabel") --购买总价格
	costAtlasLabel:setString(daojuconfig[num].price)
	local inputNikeName = layer:getChildByName("giveNikeNamebg"):getChildByName("inputNikeName")

	local givenameTab = self._giveUserList
	if givenameTab and next(givenameTab) ~= nil then
		local scrollview = giveLayer:getChildByName("givelistbg"):getChildByName("giveScrollView")
		scrollview:setTouchEnabled(true)
		scrollview:setContentSize(cc.size(300, 390))
		local innerWidth = scrollview:getContentSize().width
	    local innerHeight = scrollview:getContentSize().height
	    local pic = display.newSprite("HallUI/NewStore/givenamebg1.png")
	    local height = (pic:getContentSize().height+10)*#givenameTab
	    if height > innerHeight then
			innerHeight = height
		end
		scrollview:setInnerContainerSize(cc.size(innerWidth, innerHeight))

	    for k, v in pairs( givenameTab ) do
	    	local button = ccui.Button:create()
		    button:setTouchEnabled(true)
		    local string1 = "HallUI/NewStore/givenamebg1.png"
		    local string2 = "HallUI/NewStore/givenamebg2.png"
		    button:loadTextures(string1, string2, "")
		    button:setPosition(cc.p(button:getContentSize().width / 2,
                                scrollview:getInnerContainerSize().height-(button:getContentSize().height+10)*(k-0.5)))
		    height = height + button:getContentSize().height
		    button:setTag(k)

		    button:setTitleText(v.szAccount)
		    button:setTitleFontSize(25)
		    button:setTitleColor(cc.c3b(255,255,255))

		    button:addTouchEventListener(function(sender, type)
		        if type == ccui.TouchEventType.ended then
		            inputNikeName:setString(button:getTitleText())
		        end
		    end)
		    scrollview:addChild(button)
		end
		
	else
		layer:setPosition(cc.p(640, 360))
		giveLayer:getChildByName("givelistbg"):setVisible(false)
	end

	local addBtn = layer:getChildByName("buykuang"):getChildByName("addBtn")
	addBtn:addTouchEventListener(function(sender, type)
	        if type == ccui.TouchEventType.ended then
	            app.musicSound:playSound(SOUND_HALL_TOUCH)
	            local count = tonumber(buyCount:getString()) 
	            if daojuconfig[num].price*(count+1) <= app.hallLogic._loginSuccessInfo.lUserScore then
	            	buyCount:setString(count+1) 
	            	costAtlasLabel:setString(daojuconfig[num].price*(count+1))
	            else
	            	MyToast.new(giveLayer, "金币不足")
	            end
	        end
	    end)

	local subBtn = layer:getChildByName("buykuang"):getChildByName("subBtn")
	subBtn:addTouchEventListener(function(sender, type)
	        if type == ccui.TouchEventType.ended then
	            app.musicSound:playSound(SOUND_HALL_TOUCH)
	            local count = tonumber(buyCount:getString())
	            if count - 1 > 0 then
	            	buyCount:setString(count-1) 
	            	costAtlasLabel:setString(daojuconfig[num].price*(count-1))
	            end
	            
	        end
	    end)

	local closeBtn = layer:getChildByName("closeBtn")
	closeBtn:addTouchEventListener(function(sender, type)
	        if type == ccui.TouchEventType.ended then
	            app.musicSound:playSound(SOUND_HALL_TOUCH)
	            if giveLayer then giveLayer:removeFromParent() end
	        end
	    end)

	local cancelBtn = layer:getChildByName("cancelBtn")
	cancelBtn:addTouchEventListener(function(sender, type)
	        if type == ccui.TouchEventType.ended then
	            app.musicSound:playSound(SOUND_HALL_TOUCH)
	            if giveLayer then giveLayer:removeFromParent() end
	        end
	    end)

	local sureBtn = layer:getChildByName("sureBtn")
	sureBtn:addTouchEventListener(function(sender, type)
	        if type == ccui.TouchEventType.ended then
	            app.musicSound:playSound(SOUND_HALL_TOUCH)
	            if tonumber(costAtlasLabel:getString()) <= app.hallLogic._loginSuccessInfo.lUserScore then
	            	local buynum = tonumber(buyCount:getString())
	            	local account = tostring(inputNikeName:getString())
	            	if account == nil or string.len(account) == 0 then
		            	MyToast.new(giveLayer, STR_ACCOUNT_NULL)
		            	return
		            end
		            
		            local typenum = daojuconfig[num].typenum
		            app.hallLogic:storeGiveBox(account, app.hallLogic._redEnvelopeInfo.items[typenum].dwRMBValue, buynum)
		            -- app.hallLogic:storeGiveBox(account, daojuconfig[num].price, buynum)
	            else
	            	MyToast.new(giveLayer, "赠送失败，金币不足")
	            end
	        end
	    end)

end

--更新用户金币乐卷信息
function NewStore:updateLoginInfo()
    local userInfo = app.hallLogic._loginSuccessInfo
    self._playercoinNum:setString(userInfo.lUserScore)
    -- self.playerLeJuan:setString(app.hallLogic._leJuan)    
end

--按钮的显示
function NewStore:btnShow(num)
	for k, v in pairs( self._btnTab ) do
		if k == num then
			self._leixingtip:loadTexture("HallUI/NewStore/"..NewStoreConfig.leixingTab[k])
			v:setTouchEnabled(false)
			v:setBright(false)
			self._userInformation:setVisible(false)
			if num == 1 then
				self:buyCoinLayer()
			elseif num == 2 then
				self:buyDaojuLayer()
			elseif num == 3 then
				app.hallLogic:queryRessMessage() --查询用户收货地址
				--app.hallLogic:shiwuMessage()
				self._ScrollView:removeAllChildren()	
				self:buyShiwuLayer(nil)
			elseif num == 4 then
				app.hallLogic:qureyBuyReward()
			end
		else
			v:setTouchEnabled(true)
			v:setBright(true)
		end
	end
end

function NewStore:updateShiWuMessage()
    --更新实物信息
    local shiwuNumTab = app.hallLogic._shiwuNumTab
    --dump(shiwuNumTab)
    for i = 1, 4 do
        local numlabel = self._bg:getChildByName("shiwubg"..i):getChildByName("shiwunum"..i)
        numlabel:setString("x"..shiwuNumTab.stItem[shiwuNumTab.stItem[i].wPropsID].dwPieceAmount)
    end
end

--实物道具
function NewStore:shiwuCountMessage(data)
	if data then
		self._shiwumsg = data
		self:buyShiwuLayer(data)
	else
		MyToast.new(self._shopLayer, "获取实物道具信息失败")
	end
end

--返回商城的页面
function NewStore:getLayer()
	self:show()
	return self._shopLayer
end

--展示动画
function NewStore:show()
	local x, y = self._bg:getPosition()
	self._bg:setPosition(cc.p(x, y*3))
	local moveac = cc.MoveTo:create(0.5, cc.p(x, y))
	self._bg:runAction(moveac)
end

--移除
function NewStore:onDismiss()
	local x, y = self._bg:getPosition()
	local moveac = cc.MoveTo:create(0.5, cc.p(x, y*3))
	local seq = transition.sequence({
		moveac,
		cc.CallFunc:create(function()
			self:removeFromParent()
		end)
		})
	self._bg:runAction(seq)
end

--记录
function NewStore:setPayRecordData(m_tab) 
    if m_tab.stItem and next(m_tab.stItem) ~= nil then
    	self._ScrollView:removeAllChildren()
		self._ScrollView:setTouchEnabled(true)
		self._ScrollView:setContentSize(cc.size(1100, 435))
		local innerWidth = self._ScrollView:getContentSize().width
	    local innerHeight = self._ScrollView:getContentSize().height

	    local height = 33*#m_tab.stItem
	    if height > innerHeight then
			innerHeight = height
		end
		self._ScrollView:setInnerContainerSize(cc.size(innerWidth, innerHeight))

	    for k, v in pairs( m_tab.stItem ) do
	    	local lable = cc.Label:create()
		    lable:setWidth(1060)
		    lable:setHeight(33)
		    lable:setSystemFontName("微软雅黑")
		    lable:setSystemFontSize(21)

		    if v.cbType == ExchangeType.ExchangeType_Present then -- 赠送
		    	lable:setTextColor(cc.c3b(130, 252, 0))
		    elseif v.cbType == ExchangeType.ExchangeType_Receive then -- 收到
		    	lable:setTextColor(cc.c3b(255, 235, 12))
		    elseif v.cbType == ExchangeType.ExchangeType_Exchange then -- 兑换
		    	lable:setTextColor(cc.c3b(255, 100, 97))
		   	end
		    lable:setString(v.szDate.."   "..v.szDetails)
		    lable:setPosition(cc.p(innerWidth/2,
                                self._ScrollView:getInnerContainerSize().height-(lable:getContentSize().height)*(k-0.5)))
		    self._ScrollView:addChild(lable)
		end
	else
		self._ScrollView:removeAllChildren()
	end
end

--支付提示框
function Store:sethintBox(num)
    if num==1 then
        MyToast.new(self._shopLayer, STR_STORE_HINT_MSG)
	elseif num==2 then
	    MyToast.new(self._shopLayer, STR_STORE_HINT_MSG1)
	elseif num==3 then
        MyToast.new(self._shopLayer, STR_STORE_HINT_MSG2)
    end
end

--赠送消息回调
function NewStore:giveMessageCallBack(data)
	if data.cbResult == 0 then
		MyToast.new(self._shopLayer, "赠送宝箱成功")
	else
		MyToast.new(self._shopLayer, "赠送失败，"..data.szMessage)
	end
end

--收货地址结果
function NewStore:adressCallBack(data)
	self._userAdress = data
end

--赠送记录
function NewStore:getGiveUserList(data)
	self._giveUserList = data.stItem
end

--用户提交信息结果
function NewStore:AdressMessageResult(data)
	if data.lResult == 0 then
		MyToast.new(self._shopLayer, "提交信息成功")
		app.hallLogic:queryRessMessage() --查询用户收货地址
	else
		MyToast.new(self._shopLayer, "提交信息失败")
	end
end

--兑换实物道具结果
function NewStore:ChangeThingResult(data)
	if data.lResult == 0 then
		-- MyToast.new(self._shopLayer, data.szMessage)
		self:ChangeResultTips(true, data)
		app.hallLogic:queryRessMessage() --查询用户收货地址
		app.hallLogic:shiwuMessage()
	else
		--MyToast.new(self._shopLayer, data.szMessage)
		self:ChangeResultTips(false, data)
	end
end

--兑换实物后的提示
function NewStore:ChangeResultTips(m_bool, data)
	local node = ccs.GUIReader:getInstance():widgetFromJsonFile("HallUI/shiwuResultTip.json")
	self._shopLayer:addChild(node, 99)
	local layer = node:getChildByName("bg")

	local showlable = layer:getChildByName("showlable")
	showlable:setString(data.szMessage)

	local sureBtn = layer:getChildByName("sureBtn")
	sureBtn:addTouchEventListener(function(sender, type)
	        if type == ccui.TouchEventType.ended then
	            app.musicSound:playSound(SOUND_HALL_TOUCH)
	            if node then node:removeFromParent() end
	        end
	    end)

	if m_bool then
		layer:getChildByName("tippic"):loadTexture("HallUI/NewStore/exChangeSuccess.png")
	else
		layer:getChildByName("tippic"):loadTexture("HallUI/NewStore/exChangeDefault.png")
		layer:getChildByName("tiplable"):setVisible(false)
	end
end

function NewStore:PaySuccessCallBack(orderid)
	self.payResLayer = ccs.GUIReader:getInstance():widgetFromJsonFile("HallUI/PayEndUiLayer.json")	

	local timelabel = self.payResLayer:getChildByName("Image_1"):getChildByName("Label_4")
	timelabel:setString("10秒")
	local timepic = self.payResLayer:getChildByName("Image_1"):getChildByName("Image_3")

	local rotate = cc.RotateBy:create(0.8, 90)
	timepic:runAction(cc.RepeatForever:create(rotate))

	local time = 10
	local ispaysuccess = 0 --0默认，1成功，2没有充值
	local paymoney = 0

	local function PayCheckCallBack()
		local xhr = cc.XMLHttpRequest:new() -- 新建一个XMLHttpRequest对象  
	    xhr.responseType = cc.XMLHTTPREQUEST_RESPONSE_JSON -- json数据类型
	    xhr:open("POST", app.GETPAYCONFIG_UEL)-- POST方式  
	    orderid = tostring(orderid)
	    local md5 = crypto.md5("QueryOrder"..orderid.."tjyjerge!@#sddf$^$h234^^DsSEG")
	    local param = "action=QueryOrder&order_id="..orderid.."&sign="..md5
	    xhr:send(param)

	    local function myPayCallBack()  
	    	print( app.GETPAYCONFIG_UEL.."?"..param )
	        local response = xhr.response -- 获得响应数据  
	        local output = json.decode(response, 1) -- 解析json数据
	        if output.State == "true" then 
	        	ispaysuccess = 1
	        	paymoney = output.Gold
	        elseif output.State == "false" then --没有充值
	        end
	    end
	    -- 注册脚本方法回调 
	    xhr:registerScriptHandler(myPayCallBack)
	end
	PayCheckCallBack()

	local seq = transition.sequence({
		cc.DelayTime:create(1),
		cc.CallFunc:create(function()
			time = time - 1
			if time <= 0 then
				timepic:stopAllActions()
				timelabel:stopAllActions()
				if self.payResLayer then
					self.payResLayer:removeFromParent()
					self:PayResult(1, orderid, 0)
				end
			else
				timelabel:setString(time.."秒")
				if tonumber(time)%3 == 0 and tonumber(time) > 0 then
		        	PayCheckCallBack()
		        end
				if ispaysuccess ~= 0 then
					timepic:stopAllActions()
					timelabel:stopAllActions()
					if self.payResLayer then
						self.payResLayer:removeFromParent()
					end
					if ispaysuccess == 1 then
						self:PayResult(3, orderid, paymoney)
					end
				end
			end
		end)
		})
	timelabel:runAction(cc.RepeatForever:create(seq))

	self:addChild(self.payResLayer, 129)
end

function NewStore:PayResult(m_type, orderid, m_money)
	self.payResultLayer = ccs.GUIReader:getInstance():widgetFromJsonFile("HallUI/PayResultLayer.json")
	local chaoshi = self.payResultLayer:getChildByName("tipslayer1") --订单超时
	chaoshi:setVisible(false)
	local yichang = self.payResultLayer:getChildByName("tipslayer2") --订单异常
	yichang:setVisible(false)
	local chenggong = self.payResultLayer:getChildByName("tipslayer3") --充值成功
	chenggong:setVisible(false)
	self:addChild(self.payResultLayer, 120)

	local sureBtn = self.payResultLayer:getChildByName("sureBtn")
	sureBtn:addTouchEventListener(function(sender, type)
	        if type == ccui.TouchEventType.ended then
	            app.musicSound:playSound(SOUND_HALL_TOUCH)
	            if self.payResultLayer then 
	            	self.payResultLayer:removeFromParent()
	            end
	        end
	    end)

	if m_type == 1 then
		chaoshi:setVisible(true)
		--订单超时
		local kefuBtn1 = chaoshi:getChildByName("Button_7")
		kefuBtn1:addTouchEventListener(function(sender, type)
		        if type == ccui.TouchEventType.ended then
		            app.musicSound:playSound(SOUND_HALL_TOUCH)
		            if self.payResultLayer then 
		            	self.payResultLayer:removeFromParent()
		            end
		            showUrl('http://chat32.live800.com/live800/chatClient/chatbox.jsp?companyID=645036&configID=82709&jid=6560548107')
		        end
		    end)
		local tijiaoBtn = chaoshi:getChildByName("Button_8")
		tijiaoBtn:addTouchEventListener(function(sender, type)
		        if type == ccui.TouchEventType.ended then
		            app.musicSound:playSound(SOUND_HALL_TOUCH)
		            if self.payResultLayer then 
		            	self.payResultLayer:removeFromParent() 
		            	self:PaySuccessCallBack(orderid)
		            end
		        end
		    end)
	elseif m_type == 2 then
		yichang:setVisible(true)
		--订单异常
		local kefuBtn2 = yichang:getChildByName("Button_11")
		kefuBtn2:addTouchEventListener(function(sender, type)
		        if type == ccui.TouchEventType.ended then
		            app.musicSound:playSound(SOUND_HALL_TOUCH)
		            if self.payResultLayer then 
		            	self.payResultLayer:removeFromParent()
		            end
		            showUrl('http://chat32.live800.com/live800/chatClient/chatbox.jsp?companyID=645036&configID=82709&jid=6560548107')
		        end
		    end)
	elseif m_type == 3 then
		chenggong:setVisible(true)
		app.hallLogic:sendUpdateUserCoin() --刷新金币请求

		app.musicSound:playSound("win")
		--测试金币
	    for i = 1, 100 do
	        local t_coin = WinCoin.new(self, false, true)
	        self:addChild(t_coin, 123)
	    end
	    --大金币
	    local t_big = math.random(1, 3)
	    for i = 1, t_big do
	        local t_coin = WinCoin.new(self, true, true)
	        self:addChild(t_coin, 123)
	    end   
		--充值成功
		local closeBtn = chenggong:getChildByName("Button_13")
		closeBtn:addTouchEventListener(function(sender, type)
		        if type == ccui.TouchEventType.ended then
		            app.musicSound:playSound(SOUND_HALL_TOUCH)
		            if self.payResultLayer then 
		            	self.payResultLayer:removeFromParent()
		            end
		        end
		    end)
		local moneylabel = chenggong:getChildByName("Label_14")
		moneylabel:setString("恭喜您充值成功获得"..m_money.."金币")
		local viplabel = chenggong:getChildByName("Label_18")
		local vipnum = app.hallLogic._loginSuccessInfo.obMemberInfo.cbMemberOrder
		viplabel:setString("您现在享有VIP"..vipnum.."特权")
	end
end

function NewStore:onEnter()
	Store.super.onEnter(self)
	--更新商城，兑换成功
    app.eventDispather:addListenerEvent(eventLoginStoreCoin, self, function(data)
        self:updateLoginInfo()
        self:sethintBox(1)
    end)
    
    --兑换失败
    app.eventDispather:addListenerEvent(eventLoginStoreErro, self, function(data)
        self:sethintBox(2)
    end)
    
    app.eventDispather:addListenerEvent(eventTrumpetCount, self, function(data)
        -- self.trumpetCount:setString(app.hallLogic._trumpetCount)
    end)
    
    app.eventDispather:addListenerEvent(eventLoginUserCoinUpdate, self, function(data)
        self:updateLoginInfo()
    end)

    --记录回调
    app.eventDispather:addListenerEvent(eventExchangeData, self, function(data)
        self:setPayRecordData(data)
    end)
    --赠送回调
    app.eventDispather:addListenerEvent(eventGiveRedBoxData, self, function(data)
        self:giveMessageCallBack(data)
    end)
    --历史赠送记录回调
    app.eventDispather:addListenerEvent(eventGiveListData, self, function(data)
        self:getGiveUserList(data)
    end)
    --查询用户详细信息
    app.eventDispather:addListenerEvent(eventAdressMessageData, self, function(data)
        self:adressCallBack(data)
    end)
    --用户提交信息结果
    app.eventDispather:addListenerEvent(eventAdressMessageResult, self, function(data)
        self:AdressMessageResult(data)
    end)
    --兑换实物道具结果
    app.eventDispather:addListenerEvent(eventChangeThingResult, self, function(data)
        self:ChangeThingResult(data)
    end)

    --充值成功后的等待回调
    app.eventDispather:addListenerEvent(eventPaySuccessCallBack, self, function(data)
        self:PaySuccessCallBack(data)
    end)
end

function NewStore:onExit()
    Store.super.onExit(self)
    app.eventDispather:delListenerEvent(self)
    DataManager:getInstance():setNewStoreLayer(nil)
end







return NewStore
