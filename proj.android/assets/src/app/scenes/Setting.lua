
Setting = class("Setting", function()
    return display.newScene()
end)


Setting.__index = Setting
Setting._uiLayer= nil

Setting._sounds= nil            --音乐
Setting._soundEffects= nil      --音效  
Setting._headIcon = nil

-- global var
GameData={}

function Setting:ctor()
    self._uiLayer = cc.Layer:create()
    self:addChild(self._uiLayer)
    
    local layerInventory = ccs.GUIReader:getInstance():widgetFromJsonFile("HallUI/Setting.json")
    self._uiLayer:addChild(layerInventory)
    layerInventory:addTouchEventListener(function(sender, type) 
        if type == ccui.TouchEventType.ended then
        	self:dismiss()
        end  
    end)
    
    self._BG = ccui.Helper:seekWidgetByName(layerInventory,"ImageBG")   
    self._bgPosX = self._BG:getPositionX()
    self._bgPosY = self._BG:getPositionY()
    
    self._BG:setPositionY(1500)
    
    self._sounds=ccui.Helper:seekWidgetByName(layerInventory,"bgmBtn")             --音乐0,关闭1，开启
    self._soundEffects=ccui.Helper:seekWidgetByName(layerInventory,"effectBtn")    --音效  0,关闭1，开启

    self._tips = ccui.Helper:seekWidgetByName(layerInventory,"Button_8")
    self._tips:setVisible(false)
    
    if cc.UserDefault:getInstance():getFloatForKey("sound")~=0 then
        self._sounds:setPositionX(368)
    else
        self._sounds:setPositionX(418)
    end
    
    if cc.UserDefault:getInstance():getFloatForKey("soundEffect")~=0 then
        self._soundEffects:setPositionX(658)
    else
        self._soundEffects:setPositionX(709)
    end  

    --音乐事件
    local function soundsEvent(sender, eventType)
        if eventType == ccui.TouchEventType.ended then      
            if self._sounds:getPositionX()== 418 then
                self._sounds:setPositionX(368)
            	cc.UserDefault:getInstance():setFloatForKey("sound",1)
                app.musicSound:playMusic(DATINGBG, true)
            else
                self._sounds:setPositionX(418)
                app.musicSound:stopMusic()
                cc.UserDefault:getInstance():setFloatForKey("sound",0)
            end
        end
    end
    self._sounds:addTouchEventListener(soundsEvent)
    
    --音效事件
    local function soundsEffectsEvent(sender, eventType)
        if eventType == ccui.TouchEventType.ended then
            --            --音乐
            if self._soundEffects:getPositionX()== 709 then
                self._soundEffects:setPositionX(658)
                cc.UserDefault:getInstance():setFloatForKey("soundEffect",1)
                app.musicSound:playSound(SOUND_HALL_TOUCH)
            else
                self._soundEffects:setPositionX(709)
                cc.UserDefault:getInstance():setFloatForKey("soundEffect",0)
            end
        end
    end
    self._soundEffects:addTouchEventListener(soundsEffectsEvent)

    
    --返回按钮点击事件
    local function returnEvent(sender, eventType)
        if eventType == ccui.TouchEventType.ended then
            self:dismiss()
        end
    end

    --返回按钮
    local returnBtn = ccui.Helper:seekWidgetByName(layerInventory, "returnBtn")
    returnBtn:addTouchEventListener(returnEvent)
    

    --切换账号事件
    local function changePlayerEvent(sender, eventType)
        if eventType == ccui.TouchEventType.ended then
            if app.hallLogic.isplayGame then
                app.musicSound:playSound(SOUND_HALL_TOUCH)
                uiManager:runScene("MainScene")
            else
                self:changeCallBack()
            end
        end
    end

    --玩家名字
    local changePlayerBtn = ccui.Helper:seekWidgetByName(layerInventory, "changePlayerBtn")
    changePlayerBtn:addTouchEventListener(changePlayerEvent)
    
    local playerName = ccui.Helper:seekWidgetByName(layerInventory, "playerName")
    playerName:setString(app.hallLogic._loginSuccessInfo["szNickName"])
    
    self._headIcon = ccui.Helper:seekWidgetByName(layerInventory, "headIcon")
    
    self:init()

    self._bShow = false
end

-- 切换账号回调
function Setting:changeCallBack()
    self._tips:setVisible(true)

    -- 退出按钮
    local function closeBtnCallBack(sender, eventType)
        if eventType == ccui.TouchEventType.ended then
            app.musicSound:playSound(SOUND_HALL_TOUCH)
            uiManager:runScene("MainScene")
        end
    end
    local closeBtn = self._tips:getChildByName("exitBtn")
    closeBtn:addTouchEventListener(closeBtnCallBack)

    -- 进入游戏按钮
    local function sureCallBack(sender, eventType)
        if eventType == ccui.TouchEventType.ended then
            self._uiLayer:removeFromParent()
        end
    end
    local sureBtn = self._tips:getChildByName("sureBtn")
    sureBtn:addTouchEventListener(sureCallBack)
end

function Setting:show()
    self._BG:stopAllActions()
    self._BG:runAction(cc.Sequence:create(cc.MoveTo:create(0.6, {['x']=self._bgPosX, ['y']=self._bgPosY}),
        cc.MoveBy:create(0.05, {['x']=0, ['y']=20}), cc.MoveBy:create(0.04, {['x']=0, ['y']=-20}), cc.CallFunc:create(function() 
            self._bShow = true
        end)))
end

function Setting:dismiss()
    if self._bShow == false then
        return
    end

    self._BG:stopAllActions()
    self._BG:runAction(cc.Sequence:create(cc.MoveBy:create(0.6,{['x']=0, ['y']=2000}), cc.CallFunc:create(function()
        self:removeFromParent()
    end)))
end

function Setting:init()
    --玩家头像headIcon
    local headIconNum=cc.UserDefault:getInstance():getIntegerForKey("headIcon")    
    local t_png = ""
    if headIconNum <= 15 then
        t_png = "headIcon/y"..headIconNum..".png"
    else
        t_png = "headIcon/yy"..(headIconNum - 15)..".png"
    end
    self._headIcon:loadTexture(t_png)
end

return Setting