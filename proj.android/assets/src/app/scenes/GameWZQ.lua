
local GameWZQ = class("GameWZQ", function()
    return display.newScene("GameWZQ")
end)

GameWZQ.ALIVE=0 
GameWZQ.DIED=1 
GameWZQ.HALFALIVE=2

--得分
GameWZQ.FIVE = 100;--成5
GameWZQ.DOUBLE_ALIVE_FOUR = 99--双活4(分析对手用)
GameWZQ.ALIVE_FOUR_AND_DEAD_FOUR = 98--活4死4(对手分析用)
GameWZQ.ALIVE_FOUR_AND_ALIVE_THREE = 96--活4活3(分析对手用)
GameWZQ.ALIVE_FOUR_AND_DEAD_THREE = 95--活4死3(分析对手用)
GameWZQ.ALIVE_FOUR_AND_ALIVE_TWO = 94--活4活2
GameWZQ.ALIVE_FOUR = 93--活4
GameWZQ.DOUBLE_DEAD_FOUR = 92--双死4
GameWZQ.DEAD_FOUR_AND_ALIVE_THREE = 91--死4活3
GameWZQ.DEAD_FOUR_AND_ALIVE_TWO = 90--死4活2
GameWZQ.DOUBLE_ALIVE_THREE = 80--双活3
GameWZQ.ALIVE_THREE_AND_DEAD_THREE = 70--活死3
GameWZQ.HALF_ALIVE_FOUR = 65--半活4(类似○○ ○形),优先级小于活4
GameWZQ.ALIVE_THREE = 60--活3
GameWZQ.DEAD_FOUR = 50--死4
GameWZQ.DOUBLE_ALIVE_TWO = 40--双活2
GameWZQ.DEAD_THREE = 30--死3
GameWZQ.ALIVE_TWO = 20--活2
GameWZQ.DEAD_TWO = 10--死2
GameWZQ.SINGLE = 0--单个

function GameWZQ:getUILayer()
    return self._uiLayer
end

function GameWZQ:ctor()
    self._qiZhuo = {}
    self._chessStatus = {}  --AI横竖斜斜状态的判断
    self._uiLayer= nil

    self._layerInventory=nil--当前场景

    self._table=nil
    self._playerHB=true --true黑子false白子
    self._isfangda=false --是否放大
    self._jiesu=nil --结束界面
    self._zudang=nil --阻挡玩家点击
    self._isPlayerWin=false --玩家是否胜利
    
    
    self._uiLayer = cc.Layer:create()
    self:addChild(self._uiLayer)
    
    local layerInventory = ccs.GUIReader:getInstance():widgetFromJsonFile("WZQ/WZQ.json")
    self._uiLayer:addChild(layerInventory)
    self._layerInventory=layerInventory
    self._table=layerInventory:getChildByName("tableScorll"):getChildByName("table")
    self._tableBg=self._table:getChildByName("bg")
    self._suoxiaoBtn=layerInventory:getChildByName("suoxiaoBtn")    --缩小按钮
    self._jiesu=layerInventory:getChildByName("jiesu")              --结束
    self._zudang=layerInventory:getChildByName("zudangPanel")       --阻挡玩家点击
    self._zudang:setVisible(false)
    
    --------------------------------
    app.musicSound:addMusic("WUZIQI_MUSIC", "HcpySound/wuziqibg.mp3")
    
    --点击区域放大事件
    local function fangdaBtnCallBack(sender, eventType)
        if eventType == ccui.TouchEventType.ended then
            --app.musicSound:playSound(SOUND_HALL_TOUCH)
            local szX=sender:getTouchEndPosition().x
            local szY=sender:getTouchEndPosition().y
            if szX<=523 then
                szX=523
            end
            
            if szY<=265 then
                szY=265
            end
            
            if szX>=761 then
                szX=761
            end
            
            if szY >= 483 then
                szY=483
            end
            
            self._table:setAnchorPoint(cc.p(szX/1280,szY/720))  
            self._table:setScale(1.5)      
            self._table:setPosition(cc.p(self._table:getPositionX()+600,self._table:getPositionY()+400))   
            self._fangdaBtn:setVisible(false)       
            self._isfangda=true     
            self._suoxiaoBtn:setVisible(true)
        end
    end
    --点击区域放大按钮
    self._fangdaBtn=self._table:getChildByName("fangdaBtn")
    self._fangdaBtn:addTouchEventListener(fangdaBtnCallBack)
    
    
    --AI四个状态的判断
    for i=1, 4 do   --横15
        self._chessStatus[i] =self.DIED
    end  
       
    --棋桌初始化  左上为11右下为1515
    for i=0, 14 do   --横15
        for j=0, 14 do   --竖15
            local t_value= {}
            t_value["x"] = i
            t_value["y"] = j
            t_value["type"] = 0         --type0默认 1玩家 2电脑
            t_value["cPoint"] = 0       --电脑棋子在这个点的得分
            t_value["pPoint"] = 0       --玩家棋子在这个点的得分         
            table.insert(self._qiZhuo,t_value)
        end  
    end   
    
    
    --按钮点击事件
    self._btnPanel=self._table:getChildByName("btnPanel")
    for i=0, 14 do   --横15
        for j=0, 14 do   --竖15
            local btn = ccui.Helper:seekWidgetByName(self._btnPanel, (i+1).."btn"..(j+1))  
            btn:setBright(true)
            btn:setTouchEnabled(true)                 
            btn:addTouchEventListener(function(sender, eventType)
                if eventType == ccui.TouchEventType.ended then
                    app.musicSound:playSound(SOUND_HALL_TOUCH)
                    --大小还原
                    if self._isfangda==true    then
                        self:jiemianHuanYuan()
                    end
                    
                    self._zudang:setVisible(true) --玩家不能点击
 
                    --点击过后不能点击
                    btn:setBright(false)
                    btn:setTouchEnabled(false)
                    
                    self:creatQiZi(self._playerHB,i+1,j+1)--创建棋子图片                
                    self:setQiZi(i,j,self._playerHB)   --逻辑添加棋子                 
                    app.musicSound:playSound("wuziqixiazi")    
                                          
                    self:shuYin()--判断输赢 --初始化在这里          
                    if self._isPlayerWin==false then --玩家胜利过后电脑不下子
                        self:AIsetP()--电脑下子
                    else
                        self._isPlayerWin=false --是否胜利
                    end
                    
                end
            end)     
        end  
    end  
    
    
    --缩小事件
    local function suoxiaoBtnCallBack(sender, eventType)
        if eventType == ccui.TouchEventType.ended then
            app.musicSound:playSound(SOUND_HALL_TOUCH)
            self:jiemianHuanYuan()
        end
    end
    self._suoxiaoBtn:addTouchEventListener(suoxiaoBtnCallBack)
    
    

    --返回事件
    local function returnBtnCallBack(sender, eventType)
        if eventType == ccui.TouchEventType.ended then
            app.musicSound:playSound(SOUND_HALL_TOUCH)
            uiManager:runScene("GameChoice")
        end
    end
    --返回按钮
    self._returnBtn=self._table:getChildByName("returnBtn")
    self._returnBtn:addTouchEventListener(returnBtnCallBack)
    
    --结束面板点击事件
    local function jiesuBtnCallBack(sender, eventType)
        if eventType == ccui.TouchEventType.ended then
            app.musicSound:playSound(SOUND_HALL_TOUCH)
            self:initTable()
        end
    end
    --结束面板按钮
    self._jiesuBtn=self._jiesu:getChildByName("jiesuBtn")
    self._jiesuBtn:addTouchEventListener(jiesuBtnCallBack)
    
    math.randomseed(os.time()) 
    --设置玩家头像
    local headIconNum=cc.UserDefault:getInstance():getIntegerForKey("headIcon")
    self._table:getChildByName("player"):getChildByName("icon"):loadTexture("headIcon/"..headIconNum..".png")
    --名字
    local userInfo = app.hallLogic._loginSuccessInfo
    self._table:getChildByName("player"):getChildByName("name"):setString(userInfo.szNickName)
    --电脑头像
    local randIcon=math.random(1,30)
    self._table:getChildByName("computer"):getChildByName("icon"):loadTexture("headIcon/"..randIcon..".png")
    
    --电脑最新的下子提示
    self._xiaZi=self._table:getChildByName("kuangkuang")
    
    
    --self:AIsetP()

end

--AI电脑下子
function GameWZQ:AIsetP()
    local p=self:getBestPoint()
    
    print("电脑下子")
    --dump(p)
    
    self:creatQiZi(false,p.x+1,p.y+1)--创建棋子图片                
    self:setQiZi(p.x,p.y,false)   --逻辑添加棋子                                               
    self:shuYin()--判断输赢 --初始化在这里               
    self._zudang:setVisible(false)	
    
    local btn = ccui.Helper:seekWidgetByName(self._btnPanel, (p.x+1).."btn"..(p.y+1))  
    --点击过后不能点击
    btn:setBright(false)
    btn:setTouchEnabled(false)
    
end

--AI初始得分和四个状态
function GameWZQ:initValue()
    for k,v in pairs(self._qiZhuo) do
        v["cPoint"] = 0 --电脑棋子在这个点的得分
        v["pPoint"] = 0 --玩家棋子在这个点的得分       
    end

    for i=1, 4 do   
        self._chessStatus[i] =self.DIED --AI四个状态的判断
    end

end

--AI拿到最好的点
function GameWZQ:getBestPoint()
    self:initValue()
	local p={}
	p.x=0
	p.y=0
	
    for k,v in pairs(self._qiZhuo) do
        if v["type"]==0 then        --当这个点没有棋的时候
            v["cPoint"] = self:getValue(v["x"], v["y"],2);
            v["pPoint"] = self:getValue(v["x"], v["y"],1);
        end

    end
    local pcMax = 0
    local playerMax = 0;
    local pcPoint={}
    local playerPoint={}
    
    --// 分别选出pc估分和玩家估分的最大值
    for k,v in pairs(self._qiZhuo) do
        --选出电脑估分的最大值
        if pcMax == v["cPoint"] then          
           if math.random(1,10)<=5 then --1/2的概率
                pcMax = v["cPoint"]
                pcPoint.x = v["x"]
                pcPoint.y = v["y"]
           end
        elseif pcMax < v["cPoint"] then
           pcMax = v["cPoint"]
           pcPoint.x = v["x"]
           pcPoint.y = v["y"]
        end
        
        --选出玩家估分的最大值
        if playerMax == v["pPoint"] then          
            if math.random(1,10)<=5 then   --1/2的概率
                playerMax = v["pPoint"]
                playerPoint.x = v["x"]
                playerPoint.y = v["y"]
           end
        elseif playerMax < v["pPoint"] then
           playerMax = v["pPoint"]
           playerPoint.x = v["x"]
           playerPoint.y = v["y"]
        end
    end

    --两者都在90~100分之间，优先选择PC     
    if pcMax>=90 and pcMax<100 and playerMax >=90 and playerMax <100 then
        p.x=pcPoint.x
        p.y=pcPoint.y
    else
        if playerMax > pcMax then
            p.x=playerPoint.x
            p.y=playerPoint.y
        else
            p.x=pcPoint.x
            p.y=pcPoint.y
        end 
    end
    
	
	return p
end


--AI得到该位置的分数 r=v["x"] c=v["y"] chessType=v["type"]
function GameWZQ:getValue(r,c,chessType)   
    local dir={}--dir主要是拿到的得分
	dir[1] = self:getHorCount(r, c, chessType)
	dir[2] = self:getVerCount(r, c, chessType)
	dir[3] = self:getSloRCount(r, c, chessType)
	dir[4] = self:getSloLCount(r, c, chessType)
    local lenth=4
    local t1 = 0
    local t2 = 0    
    --成五      
    for i=1, lenth do   
       if dir[i] >= 5 then
          return self.FIVE
       end
    end
    
    -- 双活四
    local temp = 0
    for i=1, lenth do   
       if dir[i]==4 and self._chessStatus[i]~=self.DIED then
          temp=temp+1
       end
       if temp == 2 then
          return self.DOUBLE_ALIVE_FOUR
       end       
    end
    
    --活四死四
    t1 = 0
    t2 = 0   
    for i=1, lenth do   
       if dir[i]==4 and self._chessStatus[i]==self.ALIVE then
          t1 = 1
       end
       if dir[i]==4 and self._chessStatus[i]~=self.DIED then
          t2 = 1
       end
       if t1 == 1 and t2 == 1 then
          return self.ALIVE_FOUR_AND_DEAD_FOUR
       end       
    end
    
    --活四死三1
    t1 = 0
    t2 = 0   
    for i=1, lenth do   
        if dir[i]==4 and self._chessStatus[i]~=self.DIED then
            t1 = 1
        end
        if dir[i]==3 and self._chessStatus[i]==self.ALIVE then
            t2 = 1
        end
        if t1 == 1 and t2 == 1 then
            return self.ALIVE_FOUR_AND_ALIVE_THREE
        end       
    end
    
    --活四死三2
    t1 = 0
    t2 = 0   
    for i=1, lenth do   
        if dir[i]==4 and self._chessStatus[i]~=self.DIED then
            t1 = 1
        end
        if dir[i]==3 and self._chessStatus[i]~=self.DIED then
            t2 = 1
        end
        if t1 == 1 and t2 == 1 then
            return self.ALIVE_FOUR_AND_DEAD_THREE
        end       
    end 
       
    --活四活二
    t1 = 0
    t2 = 0  
    for i=1, lenth do   
        if dir[i]==4 and self._chessStatus[i]~=self.DIED then
            t1 = 1
        end
        if dir[i]==2 and self._chessStatus[i]~=self.ALIVE then
            t2 = 1
        end
        if t1 == 1 and t2 == 1 then
            return self.ALIVE_FOUR_AND_ALIVE_TWO
        end       
    end 
    
    --活四
    for i=1, lenth do   
       if dir[i] == 4 and self._chessStatus[i]==self.ALIVE then
          return self.ALIVE_FOUR
       end
    end
    
    --双死四
    temp = 0
    for i=1, lenth do   
        if dir[i]==4 and self._chessStatus[i]==self.DIED then
            temp=temp+1
        end
        if temp == 2 then
            return self.DOUBLE_DEAD_FOUR
        end       
    end 
    
    --死四活3
    t1 = 0;
    t2 = 0;
    for i=1, lenth do   
        if dir[i]==4 and self._chessStatus[i]==self.DIED then
            t1 = 1
        end
        if dir[i]==3 and self._chessStatus[i]~=self.DIED then
            t2 = 1
        end
        if t1 == 1 and t2 == 1 then
            return self.DEAD_FOUR_AND_ALIVE_THREE
        end       
    end      
       
    --死四活2
    t1 = 0
    t2 = 0
    for i=1, lenth do   
        if dir[i]==4 and self._chessStatus[i]==self.DIED then
            t1 = 1
        end
        if dir[i]==2 and self._chessStatus[i]~=self.DIED then
            t2 = 1
        end
        if t1 == 1 and t2 == 1 then
            return self.DEAD_FOUR_AND_ALIVE_TWO
        end       
    end    
        
    --双活三
    temp = 0 
    for i=1, lenth do   
        if dir[i]==3 and self._chessStatus[i]~=self.DIED then
            temp=temp+1
        end
        if temp == 2 then
            return self.DOUBLE_ALIVE_THREE
        end       
    end    
        
    --活死三
    t1 = 0
    t2 = 0
    for i=1, lenth do   
        if dir[i]==3 and self._chessStatus[i]==self.ALIVE then
            t1 = 1
        end
        if dir[i]==3 and self._chessStatus[i]==self.DIED then
            t2 = 1
        end
        if t1 == 1 and t2 == 1 then
            return self.ALIVE_THREE_AND_DEAD_THREE
        end       
    end     
              
    --活三    
    for i=1, lenth do   
        if dir[i]==3 and self._chessStatus[i]==self.ALIVE then
           return self.ALIVE_THREE
        end    
    end    
        
    --死四 
    for i=1, lenth do   
        if dir[i]==4 and self._chessStatus[i]==self.DIED then
           return self.DEAD_FOUR
        end    
    end     
        
    --半活死3
        t1 = 0;
        t2 = 0;  
    for i=1, lenth do   
        if dir[i]==3 and self._chessStatus[i]==self.DIED then
            t1 = 1
        end
        if dir[i]==3 and self._chessStatus[i]==self.HALFALIVE then
            t2 = 1
        end
        if t1 == 1 and t2 == 1 then
            return self.ALIVE_THREE_AND_DEAD_THREE
        end       
    end     
        
    --双活2
    temp = 0
    for i=1, lenth do   
        if dir[i]==2 and self._chessStatus[i]==self.ALIVE then
            temp=temp+1
        end
        if temp == 2 then
            return self.DOUBLE_ALIVE_TWO
        end       
    end    
              
    --死3
    for i=1, lenth do   
        if dir[i]==3 and self._chessStatus[i]==self.DIED then
           return self.DEAD_THREE
        end    
    end       
        
    --活2   
    for i=1, lenth do   
        if dir[i]==2 and self._chessStatus[i]==self.ALIVE then
           return self.ALIVE_TWO
        end    
    end     
    
    --死2    
    for i=1, lenth do   
        if dir[i]==2 and self._chessStatus[i]==self.DIED then
           return self.DEAD_TWO
        end    
    end 
	
	return 0
end
 
--AI横向搜索  ChessType.NONE=0  ChessType.WHITE=1 ChessType.BLACK=2
function GameWZQ:getHorCount(r,c,chessType)
    local count = 1
    local t1 = c + 1
    local t2 = c - 1

    for j=c + 1,c + 5-1 do   
        if j>=15 then               --1
           self._chessStatus[1] = self.DIED
           break          
        end   
        
        if self:chaZhao(r,j)["type"]==chessType then  --2
           count=count+1
           if count>=5 then
                return count
           end
        else
            if self:chaZhao(r,j)["type"]== 0 then 
            self._chessStatus[1]=self.ALIVE
            else
            self._chessStatus[1]=self.DIED
            end
            
            t1 = j;
            break;       
        end 
    end 

    for j=c - 1,c - 5+1 ,-1 do 
        --print(j.." "..c)  
        if j<0 then     --1
            if self._chessStatus[1] == self.DIED and count < 5 then --1.1
                return 0
            end
        self._chessStatus[1]=self.DIED
            break
        end
        if self:chaZhao(r,j)["type"]==chessType then  --2
            count=count+1
            if count>=5 then
                return count
            end
        else --2
            if self._chessStatus[1] == self.DIED then --2.1
                if count<5 and self:chaZhao(r,j)["type"]~=0 then        --2.1.1
                    return 0
                end
            else --2.2
               if self:chaZhao(r,j)["type"]==0 then
                    self._chessStatus[1]=self.ALIVE
               else
                    self._chessStatus[1]=self.DIED
               end
                t2 = j --记录遇到的空格
                -- 当两端都活的时候，看是否可以延伸
                if self._chessStatus[1]==self.ALIVE then --2.2.1
            local tempCount1=count
            local tempCount2=count
            local isAlive1=false
            local isAlive2=false   
                    for i=t1 + 1,t1+5-1 do   --f1
                        if i>=15 then    --f1i1
                            break
                        end
                        if self:chaZhao(r,i)["type"]==chessType then    --f1i1.1
                            tempCount1=tempCount1+1
                        else        --f1i1.2
                            if self:chaZhao(r,i)["type"]==0 then
                                isAlive1=true
                            else
                                isAlive1=false
                            end
                        
                            break
                        end                  
                    end  
                    ------      
                    for i=t2 - 1,t2-5+1,-1 do   --f2
                       if i<0 then --f2i1
                            break
                       end
                       if self:chaZhao(r,i)["type"]==chessType then --f2i1.1
                            tempCount2=tempCount2+1
                       else     --f2i1.2
                            if self:chaZhao(r,i)["type"]==0 then
                                isAlive2=true
                            else
                                isAlive2=false
                            end
                            break
                       end
                    end
                    --如果两头都是空，直接跳出
                    if tempCount1 == count and tempCount2 == count then    --i3                  
                        break
                    end
                    if tempCount1==tempCount2 then --i4
                        count = tempCount1
                        if isAlive1 and isAlive2 then
                            self._chessStatus[1]=self.HALFALIVE
                        else
                            self._chessStatus[1]=self.DIED
                        end                        
                    else  --i4.e
                        if tempCount1 > tempCount2 then
                            count=tempCount1
                        else
                            count=tempCount2
                        end
                        if count >= 5 then --i4.e.i1
                           return 0
                        end
                        if tempCount1 > tempCount2 then --i4.e.i2
                            if isAlive1 then
                                self._chessStatus[1]=self.HALFALIVE
                            else
                                self._chessStatus[1]=self.DIED
                            end
                        else        --i4.e.e1
                            if isAlive2 then
                                self._chessStatus[1]=self.HALFALIVE
                            else
                                self._chessStatus[1]=self.DIED
                            end         
                        end              
                    end              
                end                          
            end
            break;
        end
    end 
    return count
end


--AI竖向搜索
function GameWZQ:getVerCount(r,c,chessType)
    local count = 1
    local t1 = r + 1
    local t2 = r - 1
    for i=r + 1,r + 5-1 do     --f1
        if i>=15 then             --f1i1
           self._chessStatus[2] = self.DIED
           break          
        end   
        
        if self:chaZhao(i,c)["type"]==chessType then  --f1i2
           count=count+1
           if count>=5 then
                return count
           end
        else
            if self:chaZhao(i,c)["type"]== 0 then 
            self._chessStatus[2]=self.ALIVE
            else
            self._chessStatus[2]=self.DIED
            end
            
            t1 = i
            break;       
        end 
    end 
    for i=r - 1,r - 5+1 ,-1 do      --f2
        if i<0 then     --f2i1
            if self._chessStatus[2] == self.DIED and count < 5 then --f2i2
                return 0
            end
        self._chessStatus[2]=self.DIED
            break
        end
        if self:chaZhao(i,c)["type"]==chessType then  --f2i3
            count=count+1
            if count>=5 then        --f2i4
                return count
            end
        else --2
            if self._chessStatus[2] == self.DIED then --f2i5
                if count<5 and self:chaZhao(i,c)["type"]~=0 then        --f2i6
                    return 0
                end
            else --2.2
               if self:chaZhao(i,c)["type"]==0 then
                    self._chessStatus[2]=self.ALIVE
               else
                    self._chessStatus[2]=self.DIED
               end
                t2 = i --记录遇到的空格
                -- 当两端都活的时候，看是否可以延伸
                if self._chessStatus[2]==self.ALIVE then --f2i7
            local tempCount1=count
            local tempCount2=count
            local isAlive1=false
            local isAlive2=false   
                    for j=t1 + 1,t1+5-1 do   --f3
                        if j>=15 then    --f3i1
                            break
                        end
                        if self:chaZhao(j,c)["type"]==chessType then    --f3i2
                            tempCount1=tempCount1+1
                        else        --f1i1.2
                            if self:chaZhao(j,c)["type"]==0 then
                                isAlive1=true
                            else
                                isAlive1=false
                            end
                        
                            break
                        end                  
                    end        
-------------
                    for j=t2 - 1,t2-5+1,-1 do   --f4
                       if j<0 then --f4i1
                            break
                       end
                       if self:chaZhao(j,c)["type"]==chessType then --f4i2
                            tempCount2=tempCount2+1
                       else     --f2i1.2
                            if self:chaZhao(j,c)["type"]==0 then
                                isAlive2=true
                            else
                                isAlive2=false
                            end
                            break
                       end
                    end
                    --如果两头都是空，直接跳出
                    if tempCount1 == count and tempCount2 == count then    --f4i3                  
                        break
                    end
                    if tempCount1==tempCount2 then --f4i4
                        count = tempCount1
                        if isAlive1 and isAlive2 then
                            self._chessStatus[2]=self.HALFALIVE
                        else
                            self._chessStatus[2]=self.DIED
                        end                        
                    else 
                        if tempCount1 > tempCount2 then
                            count=tempCount1
                        else
                            count=tempCount2
                        end
                        if count >= 5 then --f4i
                           return 0
                        end
                        if tempCount1 > tempCount2 then --f4i5
                            if isAlive1 then
                                self._chessStatus[2]=self.HALFALIVE
                            else
                                self._chessStatus[2]=self.DIED
                            end
                        else        --i4.e.e1
                            if isAlive2 then
                                self._chessStatus[2]=self.HALFALIVE
                            else
                                self._chessStatus[2]=self.DIED
                            end         
                        end              
                    end              
                end                          
            end
            break;
        end
    end 
    return count

end


--AI斜向1搜索
function GameWZQ:getSloRCount(r,c,chessType)
    local count = 1
    local tr1 = r + 1
    local tc1 = c + 1
    local tr2 = r - 1
    local tc2 = c - 1
    local j =c 
    for i=r + 1,r + 5-1 do     --f1
        j=j+1
        if i>=15 or j>=15 then             --f1i1
           self._chessStatus[3] = self.DIED
           break          
        end   
        
        if self:chaZhao(i,j)["type"]==chessType then  --f1i2
           count=count+1
           if count>=5 then
                return count
           end
        else
            if self:chaZhao(i,j)["type"]== 0 then 
            self._chessStatus[3]=self.ALIVE
            else
            self._chessStatus[3]=self.DIED
            end
            
            tr1 = i;
            tc1 = j;
            break;       
        end 
    end 
    j=c
    for i=r - 1,r - 5+1 ,-1 do      --f2
        j=j-1
        if i<0 or j<0 then     --f2i1
            if self._chessStatus[3] == self.DIED and count < 5 then --f2i2
                return 0
            end
        self._chessStatus[3]=self.DIED
            break
        end
        if self:chaZhao(i,j)["type"]==chessType then  --f2i3
            count=count+1
            if count>=5 then        --f2i4
                return count
            end
        else --2
            if self._chessStatus[3] == self.DIED then --f2i5
                if count<5 and self:chaZhao(i,j)["type"]~=0 then        --f2i6
                    return 0
                end
            else --2.2
               if self:chaZhao(i,j)["type"]==0 then
                    self._chessStatus[3]=self.ALIVE
               else
                    self._chessStatus[3]=self.DIED
               end
                tr2 = i --记录遇到的空格
                tc2 = j
                -- 当两端都活的时候，看是否可以延伸
                if self._chessStatus[3]==self.ALIVE then --f2i7
            local tempCount1=count
            local tempCount2=count
            local isAlive1=false
            local isAlive2=false   
                local q=tc1
                    for p=tr1 + 1,tr1+5-1 do   --f3
                        q=q+1
                        if p>=15 or q>=15 then    --f3i1
                            break
                        end
                        if self:chaZhao(p,q)["type"]==chessType then    --f3i2
                            tempCount1=tempCount1+1
                        else        --f1i1.2
                            if self:chaZhao(p,q)["type"]==0 then
                                isAlive1=true
                            else
                                isAlive1=false
                            end
                        
                            break
                        end                  
                    end        
-------------
                    q=tc2
                    for p=tr2 - 1,tr2-5+1,-1 do   --f4
                       q=q-1
                       if p<0 or q<0 then --f4i1
                            break
                       end
                       if self:chaZhao(p,q)["type"]==chessType then --f4i2
                            tempCount2=tempCount2+1
                       else     --f2i1.2
                            if self:chaZhao(p,q)["type"]==0 then
                                isAlive2=true
                            else
                                isAlive2=false
                            end
                            break
                       end
                    end
                    --如果两头都是空，直接跳出
                    if tempCount1 == count and tempCount2 == count then    --f4i3                  
                        break
                    end
                    if tempCount1==tempCount2 then --f4i4
                        count = tempCount1
                        if isAlive1 and isAlive2 then
                            self._chessStatus[3]=self.HALFALIVE
                        else
                            self._chessStatus[3]=self.DIED
                        end                        
                    else 
                        if tempCount1 > tempCount2 then
                            count=tempCount1
                        else
                            count=tempCount2
                        end
                        if count >= 5 then --f4i
                           return 0
                        end
                        if tempCount1 > tempCount2 then --f4i5
                            if isAlive1 then
                                self._chessStatus[3]=self.HALFALIVE
                            else
                                self._chessStatus[3]=self.DIED
                            end
                        else        --i4.e.e1
                            if isAlive2 then
                                self._chessStatus[3]=self.HALFALIVE
                            else
                                self._chessStatus[3]=self.DIED
                            end         
                        end              
                    end              
                end                          
            end
            break;
        end
    end 
    return count

end


--AI斜向2搜索
function GameWZQ:getSloLCount(r,c,chessType)
    local count = 1
    local tr1 = r + 1
    local tc1 = c + 1
    local tr2 = r - 1
    local tc2 = c - 1
    local j =c 
    for i=r + 1,r + 5-1 do     --f1
        j=j-1
        if i>=15 or j<0 then             --f1i1
            self._chessStatus[4] = self.DIED
            break          
        end   

        if self:chaZhao(i,j)["type"]==chessType then  --f1i2
            count=count+1
            if count>=5 then
                return count
            end
        else
            if self:chaZhao(i,j)["type"]== 0 then 
                self._chessStatus[4]=self.ALIVE
            else
                self._chessStatus[4]=self.DIED
            end

            tr1 = i;
            tc1 = j;
            break;       
        end 
    end 
    j=c
    for i=r - 1,r - 5+1 ,-1 do      --f2
        j=j+1
        if i<0 or j>=15 then     --f2i1
            if self._chessStatus[4] == self.DIED and count < 5 then --f2i2
                return 0
        end
        self._chessStatus[4]=self.DIED
        break
        end
        if self:chaZhao(i,j)["type"]==chessType then  --f2i3
            count=count+1
            if count>=5 then        --f2i4
                return count
            end
        else --2
            if self._chessStatus[4] == self.DIED then --f2i5
                if count<5 and self:chaZhao(i,j)["type"]~=0 then        --f2i6
                    return 0
            end
        else --2.2
            if self:chaZhao(i,j)["type"]==0 then
                self._chessStatus[4]=self.ALIVE              
            else
                self._chessStatus[4]=self.DIED
            end
        tr2 = i --记录遇到的空格
        tc2 = j
        -- 当两端都活的时候，看是否可以延伸
        if self._chessStatus[4]==self.ALIVE then --f2i7
            local tempCount1=count
            local tempCount2=count
            local isAlive1=false
            local isAlive2=false   
            local q=tc1
            for p=tr1 + 1,tr1+5-1 do   --f3
                q=q-1
                if p>=15 or q<0 then    --f3i1
                    break
                end
                if self:chaZhao(p,q)["type"]==chessType then    --f3i2
                    tempCount1=tempCount1+1
                else        --f1i1.2
                    if self:chaZhao(p,q)["type"]==0 then
                        isAlive1=true
                    else
                        isAlive1=false
                    end
                break
                end                  
            end        
            -------------
            q=tc1
            for p=tr2 - 1,tr2-5+1,-1 do   --f4
                q=q+1
                if p<0 or q>=15 then --f4i1
                    break
                end
                if self:chaZhao(p,q)["type"]==chessType then --f4i2
                    tempCount2=tempCount2+1
                else     --f2i1.2
                    if self:chaZhao(p,q)["type"]==0 then
                        isAlive2=true
                    else
                        isAlive2=false
                    end
                break
                end
            end
            --如果两头都是空，直接跳出
            if tempCount1 == count and tempCount2 == count then    --f4i3                  
                break
            end
            if tempCount1==tempCount2 then --f4i4
                count = tempCount1
                if isAlive1 and isAlive2 then
                    self._chessStatus[4]=self.HALFALIVE
                else
                    self._chessStatus[4]=self.DIED
                end                        
            else 
                if tempCount1 > tempCount2 then
                    count=tempCount1
                else
                    count=tempCount2
                end
                if count >= 5 then --f4i
                    return 0
                end
                if tempCount1 > tempCount2 then --f4i5
                    if isAlive1 then
                        self._chessStatus[4]=self.HALFALIVE
                    else
                        self._chessStatus[4]=self.DIED
                    end
                else        --i4.e.e1
                    if isAlive2 then
                        self._chessStatus[4]=self.HALFALIVE
                    else
                        self._chessStatus[4]=self.DIED
                    end         
                end              
            end              
        end                          
        end
        break;
        end
    end 
    return count
end

--界面还原
function GameWZQ:jiemianHuanYuan()
    self._table:setAnchorPoint(cc.p(0,0))  
    self._table:setScale(1)         
    self._fangdaBtn:setVisible(true) 
    self._table:setPosition(cc.p(0,0))   
    self._isfangda=false
    self._suoxiaoBtn:setVisible(false)
end

--创建棋子 1黑白，2横坐标，3纵坐标
function GameWZQ:creatQiZi(isHB,idxI,IdxJ)
    local img = cc.Sprite:create()
    local btn = ccui.Helper:seekWidgetByName(self._btnPanel, idxI.."btn"..IdxJ) 
    img:setPosition(btn:getPosition())
    self._xiaZi:setPosition(btn:getPosition())
    self._tableBg:addChild(img)  
    if isHB==true then
        img:setTexture("WZQ/heizi.png")
    else
        img:setTexture("WZQ/baizi.png")
    end	   

end


--判断输赢
function GameWZQ:shuYin()
    local xiaoZhi=2
    local bigZhi=12
    for k,v in pairs(self._qiZhuo) do
        ------------------横向------------------------
        if v["x"]>=xiaoZhi and v["x"]<=bigZhi then
            for i=1, 5 do   
                local v2=self:chaZhao(v["x"]-i+3,v["y"])--查找左右各两个的对象是否跟自身点一个类型
                if v2["type"]~=v["type"] or v["type"]==0 then
                    break 
                else
                    if i==5 then
                        if v2["type"]==1 then
                            self:win(1)--玩家赢了
                        else
                            self:win(2)--电脑赢了
                        end
                    end
                end                
            end             
        end
        
        ------------------竖向------------------------
        if v["y"]>=xiaoZhi and v["y"]<=bigZhi then
            for i=1, 5 do   
                local v2=self:chaZhao(v["x"],v["y"]-i+3)--查找上下各两个的对象是否跟自身点一个类型
                if v2["type"]~=v["type"] or v["type"]==0 then
                    break 
                else
                    if i==5 then
                        if v2["type"]==1 then
                            self:win(1)--玩家赢了
                        else
                            self:win(2)--电脑赢了
                        end
                    end
                end                
            end             
        end
        
        ------------------左上右下------------------------
        if v["x"]>=xiaoZhi and v["x"]<=bigZhi and v["y"]>=xiaoZhi and v["y"]<=bigZhi then
            for i=1, 5 do   
                local v2=self:chaZhao(v["x"]-i+3,v["y"]-i+3)--查找左右上下各两个的对象是否跟自身点一个类型
                if v2["type"]~=v["type"] or v["type"]==0 then
                    break 
                else
                    if i==5 then
                        if v2["type"]==1 then
                            self:win(1)--玩家赢了
                        else
                            self:win(2)--电脑赢了
                        end
                    end
                end                
            end             
        end
        
        ------------------左下右上------------------------
        if v["x"]>=xiaoZhi and v["x"]<=bigZhi and v["y"]>=xiaoZhi and v["y"]<=bigZhi then
            for i=1, 5 do   
                local v2=self:chaZhao(v["x"]+i-3,v["y"]-i+3)--查找左右上下各两个的对象是否跟自身点一个类型
                if v2["type"]~=v["type"] or v["type"]==0 then
                    break 
                else
                    if i==5 then
                        if v2["type"]==1 then
                            self:win(1)--玩家赢了
                            else
                            self:win(2)--电脑赢了
                        end
                        
                    end
                end                
            end             
        end
      
    end
    ---------和----------------
    local ishe=0   
    for k,v in pairs(self._qiZhuo) do
        if v["type"]~=0 then
        	ishe=ishe+1
            --print("和++++"..ishe)
            if ishe==225 then
                self:win(0)--和
        	end
        end
    end
		
end

--查找第几个对象
function GameWZQ:chaZhao(x,y)
    return self._qiZhuo[x*15+y+1]
end

--改变对象参数
function GameWZQ:setQiZi(x,y,ishb)
    local v = self._qiZhuo[x*15+y+1]
    if ishb==true then
        v["type"]=1
    else
        v["type"]=2
    end
end


--胜利 who0和1玩家赢2电脑赢
function GameWZQ:win(who)
    print("我赢了"..who)   
    self._isPlayerWin=true --是否胜利
    if who==0 then
        self._jiesu:loadTexture("WZQ/he.png")
    elseif who==1 then
        self._jiesu:loadTexture("WZQ/sheng.png")
    elseif who==2 then
        self._jiesu:loadTexture("WZQ/bai.png")
        self._isPlayerWin=false --是否胜利
    end   
    self._jiesu:setPosition(cc.p(634,394))
    self._zudang:setVisible(false)
end

--桌面初始化
function GameWZQ:initTable()
    --棋桌初始化  左上为11右下为1515
    for k,v in pairs(self._qiZhuo) do
        v["type"]=0
    end 

    --桌面棋子清空
    self._tableBg:removeAllChildren()

    --可以点击桌面
    for i=0, 14 do   --横15
        for j=0, 14 do   --竖15
            local btn = ccui.Helper:seekWidgetByName(self._btnPanel, (i+1).."btn"..(j+1))  
            btn:setBright(true)
            btn:setTouchEnabled(true) 
        end  
    end 
    
    self._xiaZi:setPosition(cc.p(-500,-500))
    
    self._jiesu:setPosition(cc.p(634,-500))--移除结束面板
end


--退出游戏
function GameWZQ:gotoGameChoice()
    uiManager:runScene("GameChoice")
end

function GameWZQ:onEnter()
    print( "GameWZQ:onEnter" )
    app.musicSound:playMusic("WUZIQI_MUSIC", true)

end

function GameWZQ:onExit()
   app.musicSound:stopMusic() 
end

return GameWZQ