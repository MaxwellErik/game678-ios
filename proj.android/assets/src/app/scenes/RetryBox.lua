
RetryBox = class("RetryBox", function ()
	return cc.Layer:create()
end)


RetryBox.__index = RetryBox
RetryBox._uiLayer= nil
RetryBox._confirmBtn=nil             --确定按钮
RetryBox._cancelBtn=nil              --取消按钮
RetryBox._textLable=nil              --显示文字
RetryBox._confirmCallBack=nil        --确认函数
RetryBox._time=4                     --显示时间

--参数二，显示文字。
function RetryBox:ctor(bool, text, retry)  
    self._uiLayer = cc.Layer:create()
    self:addChild(self._uiLayer)

    local layerInventory = ccs.GUIReader:getInstance():widgetFromJsonFile("HallUI/ConfigBox.json")
    self._uiLayer:addChild(layerInventory)
    
    self._textLable = ccui.Helper:seekWidgetByName(layerInventory,"textLable")
    self._confirmCallBack = retry

    --------------点击事件
    --取消领取
    
    -- local function confirmBtnCallBack(sender, eventType)
    --     if bool then
    --         printf("有按钮")
    --     else
    --         self:setVisible(false)
    --     end
    -- end
    -- local imgBg=ccui.Helper:seekWidgetByName(layerInventory,"Panel_8")
    -- imgBg:addTouchEventListener(confirmBtnCallBack)
   

    -----------------------

    --确定事件
    local function confirmBtnCallBack(sender, eventType)
        if eventType == ccui.TouchEventType.ended then
            if self._confirmCallBack==nil then
                printf("onfirm is nil")
            else
            	self._confirmCallBack()
            end
        end
    end

    --确定按钮
    self._confirmBtn = ccui.Helper:seekWidgetByName(layerInventory, "retryBtn")
    self._confirmBtn:addTouchEventListener(confirmBtnCallBack)
    
    
    -- --取消领取
    -- local function cancelBtnCallBack(sender, eventType)
    --     if eventType == ccui.TouchEventType.ended then
    --         if self._cancelBtnCallBack==nil then
    --         	printf("cancel is nil")
    --         	else
    --         	self._cancelBtnCallBack()
    --         end 
    --     end
    -- end

    -- --取消按钮
    -- self._cancelBtn = ccui.Helper:seekWidgetByName(layerInventory,"cancelBtn")
    -- self._cancelBtn:addTouchEventListener(cancelBtnCallBack)
    
    
    self:isBtn(bool)                     --是否显示按钮
    self._textLable:setString(text)      --设置文字
   
    if bool then   --三秒后关闭界面
        local scheduler = cc.Director:getInstance():getScheduler()
        local schedulerEntry1 = nil
        local function step1(dt)
            if self._time == 3 then
                self:setVisible(false)     
                self._confirmCallBack()  	
            end
            self._time = self._time+1
        end
        
	    local function onNodeEvent(event)
            if event == "enter" then
                schedulerEntry1 = scheduler:scheduleScriptFunc(step1, 1.00, false)  --参数2，设置关闭界面时间
            elseif event == "exit" then
                scheduler:unscheduleScriptEntry(schedulerEntry1)
                if cc.Director:getInstance():isPaused() then
                    cc.Director:getInstance():resume()
                end
            end
        end

        self._uiLayer:registerScriptHandler(onNodeEvent)
    end

end

function RetryBox:showText()
	self:setVisible(true)
	self._time = 0
end

function RetryBox:isBtn(bool)
    self._confirmBtn:setVisible(bool)
    -- self._cancelBtn:setVisible(bool)
    
    if bool==false then
        self._textLable:setPosition(629,359)
    end
    
end

function RetryBox:setText(text)
    self._textLable:setString(text)
end

return RetryBox