PersonalInformation = class("PersonalInformation", function()
    return display:newScene()
end)

PersonalInformation.__index = PersonalInformation
PersonalInformation._uiLayer= nil

PersonalInformation.ID=nil          --ID
PersonalInformation.name=nil        --昵称
PersonalInformation.coin=nil        --金币
PersonalInformation.lejuan=nil      --乐卷
PersonalInformation.headIcon=nil    --头像
PersonalInformation.vipImg=nil      --vip等级图片
PersonalInformation.vipNum=nil      --vip等级数字
PersonalInformation.levelNum=nil    --玩家等级数字
PersonalInformation.LvPrBar=nil     --玩家等级进度条
PersonalInformation.copyBtn = nil   --复制按钮
PersonalInformation.bindBtn = nil   --绑定按钮

PersonalInformation.layerInventory =nil
PersonalInformation.inLayer =nil    --哪个界面打开的1gamechoice 2arenachoice
PersonalInformation.nameTextField=nil   --修改昵称editbox


function PersonalInformation:ctor()
    self._uiLayer = cc.Layer:create()
    self:addChild(self._uiLayer)

    self.TargetPlatform = cc.Application:getInstance():getTargetPlatform() --设备系统
    self._isChinaIP = cc.UserDefault:getInstance():getIntegerForKey("IsChinaIP")

    local layerInventory = ccs.GUIReader:getInstance():widgetFromJsonFile("HallUI/PersonalInformation.json")
    self._uiLayer:addChild(layerInventory)
    
    self._BG = layerInventory:getChildByName("ImageBG")
    layerInventory:addTouchEventListener(function(sender, type)  
        if type == ccui.TouchEventType.ended then
        	self:dismiss()
        end
    end)
    
    self._BG:setTouchEnabled(true)
    self._bgPosX = self._BG:getPositionX()
    self._bgPosY = self._BG:getPositionY()
    self._BG:setPositionY(1500)

    self.ID=self._BG:getChildByName("PersonalInfoPanel"):getChildByName("idLabel")                --ID
    self.name=self._BG:getChildByName("PersonalInfoPanel"):getChildByName("nameLabel")            --昵称
    self.coin=self._BG:getChildByName("PersonalInfoPanel"):getChildByName("coinLabel")            --金币
    self.lejuan=self._BG:getChildByName("PersonalInfoPanel"):getChildByName("leJuanLabel")          --乐卷
    self.lejuan:setVisible(false)

    self.headIcon=self._BG:getChildByName("PersonalInfoPanel"):getChildByName("headIcon")         --头像
    self.vipImg=self._BG:getChildByName("PersonalInfoPanel"):getChildByName("vipImg")            --vip等级图片
    self.vipNum=self._BG:getChildByName("PersonalInfoPanel"):getChildByName("vipNum")            --vip等级数字
    self.levelNum=self._BG:getChildByName("PersonalInfoPanel"):getChildByName("levelNum")          --玩家等级数字
    self.LvPrBar=self._BG:getChildByName("PersonalInfoPanel"):getChildByName("LvPrBar")         --玩家等级进度条
    self.copyBtn = ccui.Helper:seekWidgetByName( layerInventory, "copyBtn");
    self.bindBtn = self._BG:getChildByName("PersonalInfoPanel"):getChildByName("bindBnt")
	
	local function copyStrCakkback( pSender, ty)
		if ty ~= 2 then
			return;
		end
		
		local name = self.name:getString();
		local ID = self.ID:getString();
		
		copyStrToShearPlate(self, name.." "..ID);
	end
	self.copyBtn:addTouchEventListener(copyStrCakkback);

    local function bindEvent(sender, eventType)
        if eventType == ccui.TouchEventType.ended then
            app.musicSound:playSound(SOUND_HALL_TOUCH)
            local t_bind = BindInfo.new()
            self:addChild(t_bind)
        end
    end

    self.bindBtn:addTouchEventListener(bindEvent)    
    
    self.nickChangeBox = BombBox.new(false, STR_HCPY_BANKER_MSG)
    self:addChild(self.nickChangeBox, 1000)
    self.nickChangeBox:setVisible(false)
    
    self.tipBox = BombBox.new(false, STR_ACCOUNTS_PWD_LESS1)
    self:addChild(self.tipBox, 1000)
    self.tipBox:setVisible(false)

    --返回按钮点击事件
    local function returnEvent(sender, eventType)
        if eventType == ccui.TouchEventType.ended then
            self:dismiss()
        end
    end

    --返回按钮
    local returnBtn = self._BG:getChildByName("returnBtn")
    returnBtn:addTouchEventListener(returnEvent)
    
    local password=custom.PlatUtil:getMD5String(cc.UserDefault:getInstance():getStringForKey("password"))
    --local nameLabel = self._BG:getChildByName("PersonalInfoPanel"):getChildByName("nameLabel")        --修改过后名字
    --local nameTextField = self._BG:getChildByName("nameTextField")--可修改名字 local editBoxSize = cc.size(300, 40)
    self.nameTextField=nil
    
    --保存事件
    local function savaEvent(sender, eventType)
        if eventType == ccui.TouchEventType.ended then
            printf("sava")
            local username=self.nameTextField:getText()
            --判断输入的账号密码长度对不对
            if string.len(username) > LEN_LESS_MAX then
                self.tipBox:showText()
                return;
            end
            self._nickname = self.nameTextField:getText()
            app.hallLogic:sendChangeNickname(self.nameTextField:getText(),password)
        end
    end

    --保存按钮
    local savaBtn = self._BG:getChildByName("PersonalInfoPanel"):getChildByName("savaBtn")
    savaBtn:addTouchEventListener(savaEvent)
    
    
    local isNickName=true
    
    if app.hallLogic._isNick==0 then
    	isNickName=false
    else
        isNickName=true
    end
    
    if isNickName then
        savaBtn:setVisible(false)
        self.name:setVisible(true)
        self._bNickEdit = false
    else
        self._bNickEdit = true
        savaBtn:setVisible(true)        
        self.name:setVisible(false)
    end
    
    app.eventDispather:addListenerEvent(eventLoginSrvNickName, self, function(data)
        self:setChangeBox(1)
        self:getNickName()
        if self.inLayer==1 then
            app.eventDispather:dispatherEvent(eventLoginGameChoice)
        end
    end)

    app.eventDispather:addListenerEvent(eventLoginSrvNickName1, self, function(data)
        self:setChangeBox(0)
    end)

    self:updateUserInfo()
    
    self._bShow = false
    self._nickname = ""

    self:initBtns()
    self:initXiuGaiMiMa()
    self:initAnQuan()

    if app.hallLogic:isBinded() then
        self.bindBtn:setVisible(false)
        self._BG:getChildByName("PersonalInfoPanel"):getChildByName("tishi"):setVisible(false)

        self:changeBtn(1)        
    else
        self:changeBtn(0)
    end
    self:changePage(1)
    self:updateShiWuMessage()
end

function PersonalInformation:updateShiWuMessage()
    --更新实物信息
    local tempnode = self._BG:getChildByName("PersonalInfoPanel")
    local shiwuNumTab = app.hallLogic._shiwuNumTab
    --dump(shiwuNumTab)
    for i = 1, 4 do
        if not self._isChinaIP or self._isChinaIP == 0 then
            if not app.buttonConfig or app.buttonConfig["Entity"] ~= "1" then
                tempnode:getChildByName("shiwupic"..i):setVisible(false)
            end
        end
        local numlabel = tempnode:getChildByName("shiwupic"..i):getChildByName("shiwunum"..i)
        numlabel:setString("x"..shiwuNumTab.stItem[shiwuNumTab.stItem[i].wPropsID].dwPieceAmount)
    end
end

function PersonalInformation:initBtns()
    local t_infoBtn = self._BG:getChildByName("infoBtn")
    local t_xiugaimimaBtn = self._BG:getChildByName("xiugaimimaBtn")
    local t_zhanghaoanquanBtn = self._BG:getChildByName("zhanghaoanquanBtn")

    local function infoEvent(sender, eventType)
        if eventType == ccui.TouchEventType.ended then
            self:changeBtn(1)
            self:changePage(1)
        end
    end

    local function mimaEvent(sender, eventType)
        if eventType == ccui.TouchEventType.ended then
            self:changeBtn(2)
            self:changePage(2)
        end
    end

    local function anquanEvent(sender, eventType)
        if eventType == ccui.TouchEventType.ended then
            self:changeBtn(3)
            self:changePage(3)
        end
    end

    t_infoBtn:addTouchEventListener(infoEvent)
    t_xiugaimimaBtn:addTouchEventListener(mimaEvent)
    t_zhanghaoanquanBtn:addTouchEventListener(anquanEvent)
end

function PersonalInformation:initXiuGaiMiMa()
    local editBoxSize = cc.size(400, 70)
    local EditOldPassword = ccui.EditBox:create(editBoxSize, "HallUI/loginUI/textbg.png")
    EditOldPassword:setPosition(cc.p(600,420))
    EditOldPassword:setFontSize(10)
    EditOldPassword:setFontColor(cc.c3b(200,200,200))
    -- EditOldPassword:setPlaceHolder("请输入旧密码")
    EditOldPassword:setPlaceholderFontColor(cc.c3b(255,255,255))
    EditOldPassword:setMaxLength(20)
    EditOldPassword:setReturnType(cc.KEYBOARD_RETURNTYPE_DONE )
    EditOldPassword:setInputFlag(cc.EDITBOX_INPUT_FLAG_PASSWORD)
    self._BG:getChildByName("xiugaimimaPanel"):addChild(EditOldPassword,100)

    local EditNewPassword = ccui.EditBox:create(editBoxSize, "HallUI/loginUI/textbg.png")
    EditNewPassword:setPosition(cc.p(600,340))
    EditNewPassword:setFontSize(10)
    EditNewPassword:setFontColor(cc.c3b(200,200,200))
    -- EditNewPassword:setPlaceHolder("请输入新密码")
    EditNewPassword:setPlaceholderFontColor(cc.c3b(255,255,255))
    EditNewPassword:setMaxLength(20)
    EditNewPassword:setReturnType(cc.KEYBOARD_RETURNTYPE_DONE )
    EditNewPassword:setInputFlag(cc.EDITBOX_INPUT_FLAG_PASSWORD)
    self._BG:getChildByName("xiugaimimaPanel"):addChild(EditNewPassword,100)

    local EditNewPassword2 = ccui.EditBox:create(editBoxSize, "HallUI/loginUI/textbg.png")
    EditNewPassword2:setPosition(cc.p(600,260))
    EditNewPassword2:setFontSize(10)
    EditNewPassword2:setFontColor(cc.c3b(200,200,200))
    -- EditNewPassword2:setPlaceHolder("请输入新密码")
    EditNewPassword2:setPlaceholderFontColor(cc.c3b(255,255,255))
    EditNewPassword2:setMaxLength(20)
    EditNewPassword2:setReturnType(cc.KEYBOARD_RETURNTYPE_DONE )
    EditNewPassword2:setInputFlag(cc.EDITBOX_INPUT_FLAG_PASSWORD)
    self._BG:getChildByName("xiugaimimaPanel"):addChild(EditNewPassword2,100)

    local t_confirmBtn = self._BG:getChildByName("xiugaimimaPanel"):getChildByName("confirmBtn")

    local function confirmEvent(sender, eventType)
        if eventType == ccui.TouchEventType.ended then
            local t_old = EditOldPassword:getText()
            local pwd1 = EditNewPassword:getText()
            local pwd2 = EditNewPassword2:getText()

            --判断账号密码是否为空
            if  t_old=="" or  pwd1=="" or pwd2=="" then
                MyToast.new(self,STR_LOGIN_REGISTER_NULL)
                --self:showTip(false, STR_LOGIN_REGISTER_NULL)
                return
            end
            
            --判断账号是否有非法字符
            if strLegalRegist(t_old) ~= true then
                MyToast.new(self,STR_LOGIN_ID_UNLEGAL)
                --self:showTip(false, STR_LOGIN_ID_UNLEGAL)
                return
            end

            if strLegalRegist(pwd1) ~= true then
                MyToast.new(self,STR_LOGIN_PWD_UNLEGAL)
                --self:showTip(false, STR_LOGIN_PWD_UNLEGAL)
                return
            end

            --判断两次密码是否相等
            if pwd1 ~= pwd2 then
                MyToast.new(self,STR_LOGIN_PASSWORD_NULL)
                --self:showTip(false, STR_LOGIN_PASSWORD_NULL)
                return
            end

            --判断输入的账号密码长度对不对
            if string.len(t_old) < LEN_LESS_PASSWORD or string.len(pwd1) < LEN_LESS_PASSWORD then
                MyToast.new(self, string.format(STR_ACCOUNTS_PWD_LESS, LEN_LESS_ACCOUNTS, LEN_LESS_PASSWORD))
                --self:showTip(false, string.format(STR_ACCOUNTS_PWD_LESS, LEN_LESS_ACCOUNTS, LEN_LESS_PASSWORD))
                return;
            end            

            app.hallLogic:revisePassword(t_old, pwd1)
        end
    end

    t_confirmBtn:addTouchEventListener(confirmEvent)

    local t_zhaohuiBtn = self._BG:getChildByName("xiugaimimaPanel"):getChildByName("zhaohuimimaBtn")

    --找回密码 
    local function zhaoHuiEvent(sender, eventType)
        if eventType == ccui.TouchEventType.ended then
            local t_url = ACTION_URL
            local param = self:getInfoString("FindPassword")
            showUrl(t_url.."?"..param)
            self:dismiss()
        end
    end

    t_zhaohuiBtn:addTouchEventListener(zhaoHuiEvent)
end

function PersonalInformation:initAnQuan()
    self:getYouXiangIsBind()
    self:getMiBaoIsBind()

    local t_youXiangBind = false        

    local t_mibaoSprite = self._BG:getChildByName("zhanghaianquanPanel"):getChildByName("miBaoSprite")
    local t_miBaoBtn = self._BG:getChildByName("zhanghaianquanPanel"):getChildByName("mibaoBtn")
    local t_mibaoXiuGaiBtn = self._BG:getChildByName("zhanghaianquanPanel"):getChildByName("mibaoXiuGaiBtn")

    local t_youXiangSprite = self._BG:getChildByName("zhanghaianquanPanel"):getChildByName("youXiangSprite")
    local t_youxiangBtn = self._BG:getChildByName("zhanghaianquanPanel"):getChildByName("youxiangBtn")    
    local t_youxiangXiuGaiBtn = self._BG:getChildByName("zhanghaianquanPanel"):getChildByName("youxiangXiuGaiBtn")

    

    --密保绑定
    local function miBaoEvent(sender, eventType)
        if eventType == ccui.TouchEventType.ended then
            local t_url = ACTION_URL
            local param = self:getInfoString("ApplyQuestionBind")
            showUrl(t_url.."?"..param)
            self:dismiss()
        end        
    end
    t_miBaoBtn:addTouchEventListener(miBaoEvent)

    --密保修改
    local function miBaoXiuGaiEvent(sender, eventType)
        if eventType == ccui.TouchEventType.ended then
            local t_url = ACTION_URL
            local param = self:getInfoString("UpdateQuestionBind")
            showUrl(t_url.."?"..param)
            self:dismiss()
        end        
    end
    t_mibaoXiuGaiBtn:addTouchEventListener(miBaoXiuGaiEvent)

    --邮箱绑定
    local function youXiangEvent(sender, eventType)
        if eventType == ccui.TouchEventType.ended then
            local t_url = ACTION_URL
            local param = self:getInfoString("ApplyEmailBind")
            showUrl(t_url.."?"..param)
            self:dismiss()
        end
    end
    t_youxiangBtn:addTouchEventListener(youXiangEvent)    

    --邮箱修改
    local function youXiangXiuGaiEvent(sender, eventType)
        if eventType == ccui.TouchEventType.ended then
            local t_url = ACTION_URL
            local param = self:getInfoString("UpdateEmailBind")
            showUrl(t_url.."?"..param)
            self:dismiss()
        end
    end
    t_youxiangXiuGaiBtn:addTouchEventListener(youXiangXiuGaiEvent)

end

function PersonalInformation:initMiBao( a_bind )
    local t_mibaoSprite = self._BG:getChildByName("zhanghaianquanPanel"):getChildByName("miBaoSprite")
    local t_miBaoBtn = self._BG:getChildByName("zhanghaianquanPanel"):getChildByName("mibaoBtn")
    local t_mibaoXiuGaiBtn = self._BG:getChildByName("zhanghaianquanPanel"):getChildByName("mibaoXiuGaiBtn")

    local t_miBaoBind = a_bind
    if t_miBaoBind == "1" then
        t_mibaoSprite:loadTexture("HallUI/PersonalInfo/mibaowenti2.png")
        t_miBaoBtn:setVisible(false)
        t_mibaoXiuGaiBtn:setVisible(true)
    elseif t_miBaoBind == "0" then
        t_mibaoSprite:loadTexture("HallUI/PersonalInfo/mibaowenti.png")
        t_miBaoBtn:setVisible(true)
        t_mibaoXiuGaiBtn:setVisible(false)
    end
end

function PersonalInformation:initYouXiang(a_bind)
    local t_youXiangSprite = self._BG:getChildByName("zhanghaianquanPanel"):getChildByName("youXiangSprite")
    local t_youxiangBtn = self._BG:getChildByName("zhanghaianquanPanel"):getChildByName("youxiangBtn")    
    local t_youxiangXiuGaiBtn = self._BG:getChildByName("zhanghaianquanPanel"):getChildByName("youxiangXiuGaiBtn")

    local t_youXiangBind = a_bind
    if t_youXiangBind == "1" then 
        t_youXiangSprite:loadTexture("HallUI/PersonalInfo/youxiangwenti2.png")
        t_youxiangBtn:setVisible(false)
        t_youxiangXiuGaiBtn:setVisible(true)        
    elseif t_youXiangBind == "0" then
        t_youXiangSprite:loadTexture("HallUI/PersonalInfo/youxiangwenti.png")
        t_youxiangBtn:setVisible(true)
        t_youxiangXiuGaiBtn:setVisible(false)       
    end
end

function PersonalInformation:show()
    self._BG:stopAllActions()
    self._BG:runAction(cc.Sequence:create(cc.MoveTo:create(0.6, {['x']=self._bgPosX, ['y']=self._bgPosY}),
        cc.MoveBy:create(0.05, {['x']=0, ['y']=20}), cc.MoveBy:create(0.04, {['x']=0, ['y']=-20}), cc.CallFunc:create(function() 
            self._bShow = true
            
            if self._bNickEdit == true then
                local editBoxSize = cc.size(200, 40)
                local EditName = ccui.EditBox:create(editBoxSize, "HallUI/loginUI/textbg.png")
                EditName:setPosition(cc.p(self.name:getPosition()))
                EditName:setFontSize(15)
                EditName:setAnchorPoint(0.5, 0.5)
                EditName:setFontColor(cc.c3b(200,200,200))
                EditName:setPlaceholderFontColor(cc.c3b(255,255,255))
                EditName:setMaxLength(8)
                EditName:setReturnType(cc.KEYBOARD_RETURNTYPE_DONE )
                self._BG:getChildByName("PersonalInfoPanel"):addChild(EditName,100)

                self.nameTextField = EditName
                self.nameTextField:setText(app.hallLogic._loginSuccessInfo.szNickName)              
            end            
        end)))
end

function PersonalInformation:dismiss()
    if self._bShow == false then
        return
    end

    self._BG:stopAllActions()
    if self.nameTextField then
        self.nameTextField:removeFromParent()
        self.nameTextField = nil
    end
    self._BG:runAction(cc.Sequence:create(cc.MoveBy:create(0.6,{['x']=0, ['y']=2000}), cc.CallFunc:create(function()
        self:removeFromParent()
    end)))
end

function PersonalInformation:init()
    --设置头像
    local headIconNum=cc.UserDefault:getInstance():getIntegerForKey("headIcon")
    self.headIcon:loadTexture("headIcon/"..headIconNum..".png")
end

--nickChange提示框
function PersonalInformation:setChangeBox(b)
    if b==0 then
        self.nickChangeBox:setText(STR_HCPY_CHANGE_MSG0)
    else
        self.nickChangeBox:setText(STR_HCPY_CHANGE_MSG1)
    end
    
    self.nickChangeBox:showText()
end

--设置玩家nickname
function PersonalInformation:getNickName()
    --local nameLabel = self._BG:getChildByName("nameLabel")        --修改过后名字
    --local nameTextField = self._BG:getChildByName("nameTextField")--可修改名字
    local savaBtn = self._BG:getChildByName("savaBtn")
    savaBtn:setVisible(false)
    if self.nameTextField then
    	self.nameTextField:removeFromParent()
    	self.nameTextField = nil
    end
  
    app.hallLogic._loginSuccessInfo.szNickName = self._nickname
    app.hallLogic._isNick = 1
    self.name:setVisible(true)
    self.name:setString(app.hallLogic._loginSuccessInfo.szNickName)
end

function PersonalInformation:updateUserInfo()
    local userInfo = app.hallLogic._loginSuccessInfo   
    self.ID:setString(string.format("%d", userInfo.dwUserID))  --ID
    self.name:setString(userInfo.szNickName)            --昵称
    self.coin:setString(userInfo.lUserScore)            --金币 
    self.lejuan:setString(app.hallLogic._leJuan)          --乐卷
    self.vipNum:setString(userInfo.obMemberInfo.cbMemberOrder)          --vip等级数字
    if userInfo.obMemberInfo.cbMemberOrder == 0 then
        self.vipImg:loadTexture("HallUI/loginUI/vipImg1.png")            --vip等级图片
    else
        self.vipImg:loadTexture("HallUI/loginUI/vipImg2.png")
    end   
    self.levelNum:setString(app.hallLogic:getPlayerLevel())                      --玩家等级数字       
    self.LvPrBar:setPercent( app.hallLogic:getPlayerExpPer())         --玩家等级进度条
end

function PersonalInformation:onEnter()
    app.eventDispather:addListenerEvent(eventOperateSucceed, self, function(data)
        self:onOperateSuccess(data)
    end)

    app.eventDispather:addListenerEvent(eventOperateFailed, self, function(data)
        self:onOperateFailed(data)
    end)
end

function PersonalInformation:onExit()
    app.eventDispather:delListenerEvent(self)
end

function PersonalInformation:onOperateSuccess(data)
    self:changeBtn(1)
    self:changePage(1)

    MyToast.new(self, data.szDescribeString)
end

function PersonalInformation:onOperateFailed(data)
    MyToast.new(self, data.szDescribeString)
end

function PersonalInformation:changePage(page)
    local t_personal = self._BG:getChildByName("PersonalInfoPanel")
    local t_xiugaimima = self._BG:getChildByName("xiugaimimaPanel")
    local t_zhanghaianquan = self._BG:getChildByName("zhanghaianquanPanel")
    if page == 1 then
        t_personal:setVisible(true)
        t_xiugaimima:setVisible(false)
        t_zhanghaianquan:setVisible(false)
    elseif page == 2 then 
        t_personal:setVisible(false)
        t_xiugaimima:setVisible(true)
        t_zhanghaianquan:setVisible(false)
    elseif page == 3 then
        t_personal:setVisible(false)
        t_xiugaimima:setVisible(false)
        t_zhanghaianquan:setVisible(true)
    end
end

function PersonalInformation:changeBtn(btn)
    local t_infoBtn = self._BG:getChildByName("infoBtn")
    local t_xiugaimimaBtn = self._BG:getChildByName("xiugaimimaBtn")
    local t_zhanghaoanquanBtn = self._BG:getChildByName("zhanghaoanquanBtn")

    t_infoBtn:setVisible(true)
    t_xiugaimimaBtn:setVisible(true)
    t_zhanghaoanquanBtn:setVisible(true)

    if btn == 0 then
        t_infoBtn:setVisible(false)
        t_xiugaimimaBtn:setVisible(false)
        t_zhanghaoanquanBtn:setVisible(false)
    elseif btn == 1 then
        t_infoBtn:setTouchEnabled(false)
        t_infoBtn:setBright(false)

        t_xiugaimimaBtn:setTouchEnabled(true)
        t_xiugaimimaBtn:setBright(true)

        t_zhanghaoanquanBtn:setTouchEnabled(true)
        t_zhanghaoanquanBtn:setBright(true)
    elseif btn == 2 then 
        t_infoBtn:setTouchEnabled(true)
        t_infoBtn:setBright(true)

        t_xiugaimimaBtn:setTouchEnabled(false)
        t_xiugaimimaBtn:setBright(false)

        t_zhanghaoanquanBtn:setTouchEnabled(true)
        t_zhanghaoanquanBtn:setBright(true)
    elseif btn == 3 then
        t_infoBtn:setTouchEnabled(true)
        t_infoBtn:setBright(true)

        t_xiugaimimaBtn:setTouchEnabled(true)
        t_xiugaimimaBtn:setBright(true)

        t_zhanghaoanquanBtn:setTouchEnabled(false)
        t_zhanghaoanquanBtn:setBright(false)
    end
end

--获取字符串
function PersonalInformation:getInfoString(a_action)
    local t_action = a_action
    local t_uid = app.hallLogic._loginSuccessInfo["dwUserID"]    
    local t_pwd = custom.PlatUtil:getMD5String(custom.PlatUtil:getMD5String(cc.UserDefault:getInstance():getStringForKey("password")))
    local t_time = tostring(os.date("%Y-%m-%d %H:%M:%S"))
    local t_sign = crypto.md5(t_uid..t_pwd..t_time.."jiujiu_888$99")
    local param = "action="..t_action.."&sign="..t_sign.."&time="..t_time.."&uid="..t_uid.."&pwd="..t_pwd

    return param
end

--获取邮箱绑定
function PersonalInformation:getYouXiangIsBind()
    self.xhrYouXiang = cc.XMLHttpRequest:new() -- 新建一个XMLHttpRequest对象  
    self.xhrYouXiang.responseType = cc.XMLHTTPREQUEST_RESPONSE_JSON -- json数据类型
    self.xhrYouXiang:open("POST", ACTION_URL)-- POST方式

    local param = self:getInfoString("IsBindedEmail")

    self.xhrYouXiang:send(param)

    local function onYouXiangResponse()  
        print(" http請求回調 ")
        local t_result  = self.xhrYouXiang.response -- 获得响应数据  
        self:initYouXiang(t_result)
    end

    -- 注册脚本方法回调 
    self.xhrYouXiang:registerScriptHandler(onYouXiangResponse)
end

--获取密保绑定
function PersonalInformation:getMiBaoIsBind()
    self.xhrMiBao = cc.XMLHttpRequest:new() -- 新建一个XMLHttpRequest对象  
    self.xhrMiBao.responseType = cc.XMLHTTPREQUEST_RESPONSE_JSON -- json数据类型
    self.xhrMiBao:open("POST", ACTION_URL)-- POST方式

    local param = self:getInfoString("IsBindedQuestion")

    self.xhrMiBao:send(param)

    local function onMiBaoResponse()  
        print(" http請求回調 ")
        local t_result  = self.xhrMiBao.response -- 获得响应数据  
        self:initMiBao(t_result)
    end

    -- 注册脚本方法回调 
    self.xhrMiBao:registerScriptHandler(onMiBaoResponse)
end

return PersonalInformation