

local ArenaChoice = class("ArenaChoice", import(".BaseScene"))


ArenaChoice.__index = ArenaChoice
ArenaChoice._uiLayer= nil

ArenaChoice._coin = nil --金币
ArenaChoice._icon = nil --头像
ArenaChoice._name = nil --名字
ArenaChoice._reward = nil   --签到页面
ArenaChoice._isOpenStore = false --是否打开过商城
ArenaChoice.zoumaLabel=nil     --走马灯文字
ArenaChoice._zouma = {}        --走马灯文字列表         
ArenaChoice._dianjuan = nil --点卷label

ArenaChoice._gameServerInfo = nil

function ArenaChoice:ctor(gameId)
    ArenaChoice.super.ctor(self)
    self._reward = nil

    if gameId then
    	app.hallLogic:setLastSelGame(gameId)
    end
    
    self._gameId = app.hallLogic:getLastSelGame()
    
    self._uiLayer = cc.Layer:create()
    self:addChild(self._uiLayer)

    local layerInventory = ccs.GUIReader:getInstance():widgetFromJsonFile("HallUI/CarChoice.json")
    self._uiLayer:addChild(layerInventory)
    
    self._coin=layerInventory:getChildByName("topImg"):getChildByName("goldBg"):getChildByName("goldLabel")
    self._icon=layerInventory:getChildByName("topImg"):getChildByName("icon")
    self._name=layerInventory:getChildByName("topImg"):getChildByName("name")
    self._dianjuan = layerInventory:getChildByName("topImg"):getChildByName("dianjuanBg"):getChildByName("goldLabel")
    
    ------------------------------------box事件
    --创建并隐藏提示框
    self._tipBox = BombBox.new(true,STR_OPME_STRO,function (event)
        self._store:setVisible(true)
        self._tipBox:setVisible(false)
    end
    ,function (even)
        self._tipBox:setVisible(false)
    end)
    self:addChild(self._tipBox, 1000)
    self._tipBox:setVisible(false)
    --------
    self.baoXiangTiShi = BombBox.new(false, STR_HCPY_BAOXIANG_MSG)
    self:addChild(self.baoXiangTiShi, 1000)
    self.baoXiangTiShi:setVisible(false)
    -------------------------------------------
    
    --宝箱
    self._baoXiangBox=BaoXiangBox.new()
    self:addChild(self._baoXiangBox, 1000)
    self._baoXiangBox:setVisible(false)    
    
    self._icon:setTouchEnabled(true)
    self._icon:addTouchEventListener(function(sender, eventType)
        if eventType == ccui.TouchEventType.ended then
            app.musicSound:playSound(SOUND_HALL_TOUCH)
            local personInfoLayer = PersonalInformation.new()
            personInfoLayer.inLayer=2
            self:addChild(personInfoLayer, 500)
        end
    end)
    
    --游戏scrollview
    local gameScrollview = layerInventory:getChildByName("gameScrollView")
    
    --商城
    self._store=Store.new()
    self._uiLayer:addChild(self._store)
    self._store:setVisible(false)
    
    --商城点击事件
    local function storeCallBack(sender, eventType)
        if eventType == ccui.TouchEventType.ended then
            app.musicSound:playSound(SOUND_HALL_TOUCH)
            self._store:setVisible(true)
        end
    end
    --商城按钮
    local storeBtn = layerInventory:getChildByName("storeBtn")
    storeBtn:addTouchEventListener(storeCallBack)
    
    
    --快速开始点击事件
    local function quickCallBack(sender, eventType)
        if eventType == ccui.TouchEventType.ended then
            app.musicSound:playSound(SOUND_HALL_TOUCH)
        end
    end
    --快速开始按钮
    local quickBtn = layerInventory:getChildByName("quickStartBtn")
    quickBtn:addTouchEventListener(quickCallBack)
    
    
    --公告点击事件
    local function noticeCallBack(sender, eventType)
        if eventType == ccui.TouchEventType.ended then
            app.musicSound:playSound(SOUND_HALL_TOUCH)
        end
    end
    --公告按钮
    local noticeBtn = layerInventory:getChildByName("topImg"):getChildByName("noticeBtn")
    noticeBtn:addTouchEventListener(noticeCallBack)
    
    --活动点击事件
    local function eventCallBack(sender, eventType)
        if eventType == ccui.TouchEventType.ended then
            app.musicSound:playSound(SOUND_HALL_TOUCH)
        end
    end
    --活动按钮
    local eventBtn = layerInventory:getChildByName("eventBtn")
    eventBtn:addTouchEventListener(eventCallBack)
    
    --排名点击事件
    local function rankingCallBack(sender, eventType)
        if eventType == ccui.TouchEventType.ended then
            app.musicSound:playSound(SOUND_HALL_TOUCH)
        end
    end
    --排名按钮
    local rankingBtn = layerInventory:getChildByName("rankingBtn")
    rankingBtn:addTouchEventListener(rankingCallBack)
    
    local rewards = LoginRewards.new( app.hallLogic.SigninDay, app.hallLogic.SigninCoin, 0 )
    self._reward = rewards
    self._uiLayer:addChild(rewards)
    --每日签到点击事件
    local function loginRewardCallBack(sender, eventType)
        if eventType == ccui.TouchEventType.ended then
            app.musicSound:playSound(SOUND_HALL_TOUCH)
            app.hallLogic:registCurrentScene(rewards)
            rewards:init( app.hallLogic.SigninDay, app.hallLogic.SigninCoin )
            rewards:setVisible(true)            
        end
    end
    --每日签到
    local loginRewardBtn = layerInventory:getChildByName("qianDaoBtn")
    loginRewardBtn:addTouchEventListener(loginRewardCallBack)  
    --如果没签到打开签到界面
    if app.hallLogic.IsLingQu==false then
        rewards:init( app.hallLogic.SigninDay, app.hallLogic.SigninCoin )
        rewards:setVisible(true)
    end
    --监听
    app.eventDispather:addListenerEvent(eventLoginNoLingQu1, self, function(data)
        app.hallLogic:registCurrentScene(rewards)
        rewards:init( app.hallLogic.SigninDay, app.hallLogic.SigninCoin )
        rewards:setVisible(true)
    end)
    app.hallLogic._jiemian=1
    
    
    
    --设置
    self._settings=Setting.new()
    self._uiLayer:addChild(self._settings)
    self._settings:setVisible(false)
    --设置点击事件
    local function settingCallBack(sender, eventType)
       if eventType == ccui.TouchEventType.ended then
            app.musicSound:playSound(SOUND_HALL_TOUCH)
            self._settings:setVisible(true)
        end
    end
    --设置按钮
    local setBtn = layerInventory:getChildByName("topImg"):getChildByName("settingBtn")
    setBtn:addTouchEventListener(settingCallBack)
 
    --返回按钮事件
    local function returnCallBack(sender, eventType)
        if eventType == ccui.TouchEventType.ended then
            app.musicSound:playSound(SOUND_HALL_TOUCH)
            if self._store:isVisible() then
                self._store:setVisible(false)
                return
            end

            if self._settings:isVisible() then
                self._settings:setVisible(false)
                return  
            end

            uiManager:runScene("GameChoice")
        end
    end
    
    --返回按钮
    local retrunBtn = layerInventory:getChildByName("returnBtn")
    retrunBtn:addTouchEventListener(returnCallBack)
    
    --返回键和home键
    self._uiLayer:setKeypadEnabled(true)  
    self._uiLayer:addNodeEventListener(cc.KEYPAD_EVENT, function (event)  
        if event.key == "back" then  --返回键
            if self._store:isVisible() then
                self._store:setVisible(false)
                return
            end

        if self._settings:isVisible() then
            self._settings:setVisible(false)
            return  
        end

        uiManager:runScene("GameChoice")
        end      
    end)
        
    local gameServers = app.hallLogic:getGameServersByKindId(self._gameId)
    local addIndex = 0
    local scrollSize=400
    for index, gameServer in ipairs(gameServers) do
        local filePath = string.format("GameRes/%d_Room_%d.png", self._gameId, gameServer.wServerID)
        if cc.FileUtils:getInstance():isFileExist(filePath) == true then
            local gameLevel = GameDifficulty.new(gameServer)
            gameScrollview:addChild(gameLevel)
            if addIndex%2==0 then
                gameLevel:setPosition(100,scrollSize-(addIndex+1)*100-70)
            else
                gameLevel:setPosition(650, scrollSize-(addIndex)*100-70)
            end	
            
            addIndex = addIndex + 1
        end     
    end
    
    if addIndex%2==0 then
        scrollSize=(addIndex/2+1)*200
    else
        scrollSize=((addIndex-1)/2+1)*200
           
    end
    
    if (addIndex-1)*200<400 then
        scrollSize=400
    end
    
    gameScrollview:setInnerContainerSize(cc.size(1200,scrollSize))    
    
    
    self._isOpenStore=cc.UserDefault:getInstance():getBoolForKey("firstStore")
    self:openStore()--弹出商城
    
    --走马灯
    --**********************************************--
    self.zoumaLabel=layerInventory:getChildByName("zoumaPanel"):getChildByName("label")
    self.zoumaLabel:setString("")

    local moveS=layerInventory:getChildByName("zoumaPanel"):getContentSize().width 
    local zouMaSpeed=500
    local zouMaL=self.zoumaLabel:getContentSize().width+moveS

    local firstBool=false
    --下一个文字，重置位置
    local function unReversal1()
        zouMaSpeed=100
        self.zoumaLabel:setPosition(cc.p(moveS,11))
        local isDelete=false

        for k,v in pairs( self._zouma ) do          
            if k == 1 then
                self.zoumaLabel:setString(v["label"])
                zouMaL=self.zoumaLabel:getContentSize().width+moveS
            end

            if firstBool then--第一次进入不删除
                if k == 2 then
                    isDelete=true
            end
            else
                firstBool=true
                isDelete=false 
            end

        end

        if isDelete then
            local zouma1=self._zouma[1]
            table.remove(self._zouma,1)  
            table.insert(self._zouma,zouma1)
        end

        local callfunc1 = cc.CallFunc:create(unReversal1)  
        self.zoumaLabel:runAction(cc.Sequence:create(cc.MoveBy:create(zouMaL/zouMaSpeed,cc.p(-zouMaL,0)),callfunc1))

    end 
    local callfunc1 = cc.CallFunc:create(unReversal1)  
    self.zoumaLabel:runAction(cc.Sequence:create(cc.MoveBy:create(zouMaL/zouMaSpeed,cc.p(-zouMaL,0)),callfunc1))
    --**********************************************--
    self:requestFromServer(10)--走马灯数据获得
    self:updateUserInfo()
    
    ------宝箱

    --宝箱按钮事件
    local function baoxiangCallBack(sender, eventType)
        if eventType == ccui.TouchEventType.ended then
            app.musicSound:playSound(SOUND_HALL_TOUCH)
            local changci=(app.hallLogic._baoxiangGe+1)*app.hallLogic._baoxiangkaiqi-app.hallLogic._baoxiangJuShu
            if changci<=0 and app.hallLogic._baoxiangGeShu>0 and app.hallLogic._baoxiangGeShu>app.hallLogic._baoxiangGe then
                self:showBaoXiang(1,"12345")
            else
                print("未达到开启宝箱条件")
                self:noBaoXiang()
            end
        end
    end
    --宝箱按钮
    self._baoxiang=layerInventory:getChildByName("baoxiangBtn")
    self._baoxiang:addTouchEventListener(baoxiangCallBack)
    self:setBaoXiangText()--设置宝箱文字
    
    
end

--不能开启宝箱
function ArenaChoice:noBaoXiang()
    self.baoXiangTiShi:showText()
end


--设置宝箱文字
function ArenaChoice:setBaoXiangText()
    local baoxiangLab=self._baoxiang:getChildByName("lab")
    local changci=(app.hallLogic._baoxiangGe+1)*app.hallLogic._baoxiangkaiqi-app.hallLogic._baoxiangJuShu
    print("宝箱个数"..app.hallLogic._baoxiangGe.."开启场次"..app.hallLogic._baoxiangkaiqi.."宝箱局数"..app.hallLogic._baoxiangJuShu)

    local proCount = app.hallLogic._baoxiangJuShu
    if proCount > app.hallLogic._baoxiangkaiqi then
        proCount = app.hallLogic._baoxiangkaiqi
    end
    local strBaoXiang = string.format('开启宝箱：%d/%d场', proCount, app.hallLogic._baoxiangkaiqi)
    baoxiangLab:setString(strBaoXiang)
end

--宝箱num 1，获得乐卷，2 获得金币 text金币数量
function ArenaChoice:showBaoXiang(num,text)
    self._baoXiangBox:setVisible(true)
    self._baoXiangBox:showBox(num,text)
end

--联网拿走马灯
function ArenaChoice:requestFromServer(waittime)
    local t_channel = getChannelName()
    local t_version = getVersionName() 
    local t_sign = crypto.md5("GameConfig"..t_channel..t_version.."tjyjerge!@#sddf$^$h234^^DsSEG")
    local param = "action=GameConfig&channel="..t_channel.."&version="..t_version.."&sign="..t_sign

    local url = GETCONFIG_UEL.."?"..param
    self.request = network.createHTTPRequest(function(event)
        self:onResponse(event)
    end, url, "POST")
    if self.request then
        self.request:setTimeout(waittime or 10)
        self.request:start()
    end
end
--返回走马灯信息
function ArenaChoice:onResponse(event, dumpResponse)
    local request = event.request
    if event.name == "completed" then
        self.request = nil
        if request:getResponseStatusCode() ~= 200 then
            self:endProcess()
        else
            self.dataRecv = request:getResponseData()
            --公告走马灯
            local script = self.dataRecv
            local tb = assert(loadstring(script))()
            
            for k,v in pairs( tb.hgallMarquees ) do          
                self:setZoumaLabel(v)
            end
        end
    elseif event.name == "failed" then
        self:endProcess()
        self.request = nil
    end
end

--传入走马灯文字
function ArenaChoice:setZoumaLabel(label)
    local t_value= {}
    t_value["label"] = label
    table.insert(self._zouma,t_value)
end

function ArenaChoice:onLoginSrvLoginSuccess(data)
    self:updateUserInfo()
end


function ArenaChoice:openStore()
    local userInfo = app.hallLogic._loginSuccessInfo
    print("当前得分"..userInfo.lUserScore)
    self._coin:setString(userInfo.lUserScore)
    if userInfo.lUserScore==nil then
    	return
    end
    
    local number=tonumber(userInfo.lUserScore)  	
    
    if number<2001 then
        if self._isOpenStore==false then
            self._tipBox:showText()
            self._isOpenStore=true
            cc.UserDefault:getInstance():setBoolForKey("firstStore",true)
        end
    end
    
end


function ArenaChoice:updateUserInfo()
    local userInfo = app.hallLogic._loginSuccessInfo
    self._name:setString(userInfo.szNickName)
    self._coin:setString(userInfo.lUserScore)
    self._dianjuan:setString(app.hallLogic._leJuan)
    
    local number=tonumber(userInfo.lUserScore)      
 
    if number<=2000 then
        if self._isOpenStore==false then
            self._store:setVisible(true)
            self._isOpenStore=true
            cc.UserDefault:getInstance():setBoolForKey("firstStore",true)
        end
       
    end
end

function ArenaChoice:onEnter()
    print("ArenaChoice:onEnter")
    ArenaChoice.super.onEnter(self)
	app.eventDispather:addListenerEvent("GameLevelStartGame", self, function(data)
        print( "ArenaChoice   GameLevelStartGame" )
		if data then
            --连接服务器
            self:addLoading()
            self._gameServerInfo = data;            
            app.hallLogic:connectGameServer(data)
		end		
	end)
	
    app.eventDispather:addListenerEvent(eventGameNetConnected, self, function(data)
        print( "連接成功  準備登陸" )
        if data == false then
            self:delLoading()
            self:showTip(false, STR_GAMES_NET_CONN_FAILED)
        	return
        end
        local password = cc.UserDefault:getInstance():getStringForKey("password")
        app.table:loginByID(password, self._gameId)
    end)
    
    app.eventDispather:addListenerEvent(eventGameLoginSuccess, self, function(data)
        --判断启动哪个游戏
        print( "eventGameLoginSuccess  登陆成功" )
        for index, gameConf in pairs(allGameConfs) do
            if gameConf.wKind == app.table._serverInfo.wKindID then
                app.musicSound:playSound(SOUND_HALL_TOUCH)
                import(gameConf.entrance)
                uiManager:runGameScene(gameConf.module)
                break
            end
        end
    end)    
    
    app.eventDispather:addListenerEvent(eventGameNetClosed, self, function(data)
        self:delLoading()
        self:showTip(false, STR_GAMES_NET_CONN_FAILED)
    end)
    
    app.eventDispather:addListenerEvent(eventLoginArenaChoice, self, function(data)
        self:updateUserInfo()
        self:setBaoXiangText()
    end)
end

function ArenaChoice:onEnterTransitionFinish()
--    if IsChongLian then
--        print( "开始重连" )
--        self:showTip(false, STR_GAMES_NET_CONNECT)
--
--        for index, gameConf in pairs(allGameConfs) do
--            if gameConf.wKind == app.table._serverInfo.wKindID then    
--                self:runAction(cc.Sequence:create(cc.DelayTime:create(1.0), cc.CallFunc:create(self.reconnectGameServer)))    
--                break
--                --app.hallLogic:connectGameServer(app.table._serverInfo)
--            end
--        end
--    end
end


function ArenaChoice:reconnectGameServer()
    self:addLoading()
	app.hallLogic:connectGameServer(app.table._serverInfo)
end

function ArenaChoice:onExit()
    ArenaChoice.super.onExit(self)
	app.eventDispather:delListenerEvent(self)
	if self.request then
        self.request:cancel()
        self.request = nil
	end
end

function ArenaChoice:onHallNetConnected(data)
    
end

function ArenaChoice:onLoginSrvLoginSuccess(data)
    ArenaChoice.super.onLoginSrvLoginSuccess(self, data)
    self:updateUserInfo()
end

return ArenaChoice