require("app.scenes.BaseScene")
require("app.object.GameDifficulty")
require("app.scenes.Store")
require("app.scenes.LoginRewards")
require("app.scenes.Setting")
require("app.object.Goods")
require("app.scenes.Loading")
require("app.uiManager.uiManager")
require("app.scenes.BombBox")
require("app.scenes.TipInfos")
require("app.scenes.PersonalInformation")
require("app.object.Poker")
require("app.object.Bead")
require("app.object.payRecordItem")
require("app.object.BaoXiangBox")
require("app.scenes.Ranking")
require("app.object.RankingObject")
require("app.scenes.Vip")
require("app.object.CoinListObject")
require("app.object.WinCoin")
require("app.object.MyToast")
require("app.object.Clock")
require("app.object.HeGuan")
require("app.object.NiuNiuFaPai")
require("app.object.BaoXiangLayer")
require("app.object.TalkLayer")
require("app.object.ShzObject")
require("app.object.BackLayer")
require("app.scenes.RetryBox")
require("app.scenes.PayBox")
require("app.scenes.BindInfo")

require("app.object.ShzBangZu")
require("app.scenes.Helper")
require("app.scenes.Notice")
require("app.object.NoCoinLayer")
require("app.object.GameDownLoad")
require("app.object.GameUpdate")
require("app.object.GameXml")
require("app.object.LHJGuiZe")
require("app.object.DDZPoker")



hallGames = {{["gameId"]=108, ["gameName"]="豪车漂移"},
             {["gameId"]=122, ["gameName"]="新百家乐"},
             {["gameId"]=104, ["gameName"]="百人牛牛"},
             {["gameId"]=219, ["gameName"]="连环夺宝"},
             {["gameId"]=150, ["gameName"]="水浒传"},
             {["gameId"]=233, ["gameName"]="老虎机"},
             {["gameId"]=114, ["gameName"]="百人跑马"}}
--hallGames = {{["gameId"]=108, ["gameName"]="豪车漂移"}}