require("app.uiManager.uiManager")
MainScene = class("MainScene", import(".BaseScene"))

MainScene.TYPE_LOGIN = "TOUCH_ID_PWD"
MainScene.TYPE_REGIST = "TOUCH_REGIST"
MainScene.TYPE_QUICK = "TOUCH_QUICK"

local ALL_RETRY_TIME = 4

MainSceneBtnID = {
    DENG_LU = 1,        --账号登陆
    --ZHU_CE  = 2,        --快速注册    
    YOU_XI  = 2         --快速游戏
}

NOTICE_TAG      =   4681354153
HELP_TAG        =   6121349684

isFirstLogin = true

MainScene.__index = MainScene
MainScene._uiLayer= nil
MainScene._time = 0         --动画时间
MainScene._delTime = 0      --每次按钮动画间隔时间
MainScene._loginType = nil

function MainScene:ctor()
    MainScene.super.ctor(self)

    self._zouma = {}
    self.zoumaLabel = nil

    self._bRequestLoginByDns = false    --用域名读取还是IP读取
    self._getConfig = false             --读取配置成功还是失败
    self._retryTime = 0                 --重试的次数
    self._getConfigState = 0            --获取哪个配置
    
    math.randomseed(os.time())  --相当于srand()的过程
    
    self._uiLayer = cc.Layer:create()
    self:addChild(self._uiLayer)
    
    self._username = nil
    self._password = nil

    local loginBgLayer = ccs.GUIReader:getInstance():widgetFromJsonFile("HallUI/LoginBg.json")          --背景layer
    local loginChoiceLayer = ccs.GUIReader:getInstance():widgetFromJsonFile("HallUI/LoginChoice.json")  --选择登陆layer
    local loginAccountLayer = ccs.GUIReader:getInstance():widgetFromJsonFile("HallUI/LoginAccount.json")--账号注册layer
    self._uiLayer:addChild(loginBgLayer)
    self._uiLayer:addChild(loginChoiceLayer)
    self._uiLayer:addChild(loginAccountLayer)
    local root = self._uiLayer:getChildByTag(7)
    loginAccountLayer:setVisible(false)

    ------------------------------------box事件
    --创建并隐藏提示框
    self._tipBox = RetryBox.new(true, STR_CANNOT_GET_CONFIG, function(event)
        if self._getConfigState == 1 then
            self:requestWebServerInfo()
            self._tipBox:setVisible(false)
        elseif self._getConfigState == 2 then
            -- self:requestFromServer()
            self:requestWebFromServer()
            self._tipBox:setVisible(false)
        end
    end)
    self:addChild(self._tipBox, 1000)
    self._tipBox:setVisible(false)

    self._tipNetBox = BombBox.new(true,STR_NET_BAD,function (event)
        if self._getConfigState == 1 then
            self:requestWebServerInfo()
            self._tipNetBox:setVisible(false)
        elseif self._getConfigState == 2 then
            -- self:requestFromServer()
            self:requestWebFromServer()
            self._tipNetBox:setVisible(false)
        end
    end
    ,function (even)
        -- self._tipNetBox:setVisible(false)
        exitGame()
    end)
    self:addChild(self._tipNetBox, 1000)
    self._tipNetBox:setVisible(false)
    ---------------------------------------
    
    self._usernameField = ccui.Helper:seekWidgetByName(loginAccountLayer,"accountTf")   --账号
    --self._passwordField = ccui.Helper:seekWidgetByName(loginAccountLayer,"passwordTf")  --密码
    local editBoxSize = cc.size(344, 77)
    local EditPassword = ccui.EditBox:create(editBoxSize, "HallUI/loginUI/textbg.png")
    EditPassword:setPosition(cc.p(650,352))
    EditPassword:setFontSize(12)
    EditPassword:setFontColor(cc.c3b(200,200,200))
    EditPassword:setPlaceHolder("输入密码")
    EditPassword:setPlaceholderFontColor(cc.c3b(255,255,255))
    EditPassword:setMaxLength(8)
    EditPassword:setInputFlag(cc.EDITBOX_INPUT_FLAG_PASSWORD)
    EditPassword:setReturnType(cc.KEYBOARD_RETURNTYPE_DONE )
    loginAccountLayer:addChild(EditPassword,100)
    self._passwordField=EditPassword  
    
    self._usernameField:setString(cc.UserDefault:getInstance():getStringForKey("account"))
    self._passwordField:setText(cc.UserDefault:getInstance():getStringForKey("password"))
        
    self._helpLayer = ccui.Helper:seekWidgetByName(loginChoiceLayer, "Panel_Kefu")
    self._helpLayer:setVisible(false)

    self._helpBG = ccui.Helper:seekWidgetByName(self._helpLayer, "bgImg")
    local helpOkBtn = ccui.Helper:seekWidgetByName(self._helpBG, "OkBtn")
    helpOkBtn:addTouchEventListener(function (sender, type)
        if type == ccui.TouchEventType.ended then
            self._helpBG:runAction(cc.Sequence:create(cc.ScaleTo:create(0.06, 1.1), cc.ScaleTo:create(0.2, 0.0), cc.CallFunc:create(function ()
                self._helpLayer:setVisible(false)
                callQQChat("800112257")  --打开qq对话框进行聊天
            end)))
        end
    end)

    local helpCancleBtn = ccui.Helper:seekWidgetByName(self._helpBG, "cancelBtn")
    helpCancleBtn:addTouchEventListener(function (sender, type)
        if type == ccui.TouchEventType.ended then
            self._helpBG:runAction(cc.Sequence:create(cc.ScaleTo:create(0.06, 1.1), cc.ScaleTo:create(0.2, 0.0), cc.CallFunc:create(function ()
                self._helpLayer:setVisible(false)
            end)))
        end
    end)

    local helpBtn = ccui.Helper:seekWidgetByName(loginChoiceLayer, "kefuBtn")
    helpBtn:addTouchEventListener(function(sender, type)
        if type == ccui.TouchEventType.ended then
            -- self._helpBG:setScale(0)
            -- self._helpLayer:setVisible(true)
            -- self._helpBG:runAction(cc.Sequence:create(cc.ScaleTo:create(0.2, 1.1), cc.ScaleTo:create(0.06, 1.0), cc.CallFunc:create(function ()
            showUrl('http://chat32.live800.com/live800/chatClient/chatbox.jsp?companyID=645036&configID=82709&jid=6560548107')
            -- end)))
        end
    end)

    --快速登录
    local function quickEvent(sender, eventType)
        if eventType == ccui.TouchEventType.began then
            sender:getChildByTag(80):setVisible(false)
            sender:setOpacity(255)
        elseif eventType == ccui.TouchEventType.ended then
            sender:getChildByTag(80):setVisible(true)
            app.musicSound:playSound(SOUND_HALL_TOUCH)
            if cc.UserDefault:getInstance():getStringForKey("account")~=nil and cc.UserDefault:getInstance():getStringForKey("password")~=nil then            
                self:doQuickByIDPWD()
            else
                self:doQuickGame()
            end
        elseif eventType == ccui.TouchEventType.canceled then
            sender:setOpacity(0)
            sender:getChildByTag(80):setVisible(true)
        end
    end
    --快速登录按钮
    self._quickBtn = ccui.Helper:seekWidgetByName(loginChoiceLayer,"quickBtn")
    self._quickBtn:addTouchEventListener(quickEvent)
    --快速登录粒子效果
    ccs.ArmatureDataManager:getInstance():addArmatureFileInfo("HallUI/loginUI/anniulizi/anniulizi0.png", 
        "HallUI/loginUI/anniulizi/anniulizi0.plist", 
        "HallUI/loginUI/anniulizi/anniulizi.ExportJson" )            
    local t_dengluAnimation = ccs.Armature:create( "anniulizi" )   
    self._quickBtn:addChild(t_dengluAnimation, 100)                    
    t_dengluAnimation:setPosition(cc.p(self._quickBtn:getContentSize().width / 2, self._quickBtn:getContentSize().height / 2)) 
    t_dengluAnimation:getAnimation():play("anniulizidonghua")

    self._quickBtn:setTouchEnabled(false)
    self._quickBtn:setBright(false)
    
    --切换到账号注册点击事件
    local function openAccount(sender, eventType)
        if eventType == ccui.TouchEventType.began then
            sender:getChildByTag(80):setVisible(false)
            sender:setOpacity(255)
        elseif eventType == ccui.TouchEventType.ended then
            sender:getChildByTag(80):setVisible(true)
            app.musicSound:playSound(SOUND_HALL_TOUCH)
            loginAccountLayer:setVisible(true)
            loginChoiceLayer:setVisible(false)
        elseif eventType == ccui.TouchEventType.canceled then
            sender:setOpacity(0)
            sender:getChildByTag(80):setVisible(true)
        end
    end
    
    --账号登陆按钮
    self._accountBtn = root:getChildByName("loginBg"):getChildByName("accountBtn")
    self._accountBtn:addTouchEventListener(openAccount)

    self._accountBtn:setTouchEnabled(false)
    -- self._accountBtn:setBright(false)
    
    --返回按钮点击事件
    local function returnAccount(sender, eventType)
        if eventType == ccui.TouchEventType.ended then
            app.musicSound:playSound(SOUND_HALL_TOUCH)
            loginAccountLayer:setVisible(false)
            loginChoiceLayer:setVisible(true)
        end
    end
    
    --返回按钮
    local returnBtn = loginAccountLayer:getChildByName("returnBtn")
    returnBtn:addTouchEventListener(returnAccount)

    --找回密码按钮点击事件
    local function findPasswordAccount(sender, eventType)
        if eventType == ccui.TouchEventType.ended then
            local t_url = ACTION_URL
            --[[local param = self:getInfoString("FindPassword")
            showUrl(t_url.."?"..param)--]]
			showUrl(t_url.."?action=FindPassword")
            print( t_url.."?action=FindPassword" )
        end
    end

    --找回密码按钮
    local findPasswordBtn = loginAccountLayer:getChildByName("zhaohuimimaBtn")
    findPasswordBtn:addTouchEventListener(findPasswordAccount)
    
    --快速注册点击事件
    local function openRegister(sender, eventType)
        if eventType == ccui.TouchEventType.began then
            sender:getChildByTag(80):setVisible(false)
            sender:setOpacity(255)
        elseif eventType == ccui.TouchEventType.ended then            
            sender:setOpacity(0)
            sender:getChildByTag(80):setVisible(true)
            app.musicSound:playSound(SOUND_HALL_TOUCH)
            uiManager:runScene("RegisterScene","fade",0.5,cc.c3b(15, 115, 200))
        elseif eventType == ccui.TouchEventType.canceled then
            sender:setOpacity(0)
            sender:getChildByTag(80):setVisible(true)
        end
    end

    --快速注册按钮
    self._registerBtn = root:getChildByName("loginBg"):getChildByName("registerBtn")
    self._registerBtn:addTouchEventListener(openRegister)

    self._registerBtn:setTouchEnabled(false)

    
--    --登陆点击事件
    local function openLogin(sender, eventType)
        if eventType == ccui.TouchEventType.ended then
            app.musicSound:playSound(SOUND_HALL_TOUCH)
            
            self._username = self._usernameField:getString()
            self._password = self._passwordField:getText()
            self:doLoginByIDPWD()
        end
    end
    --登陆按钮
    local accountBtn = loginAccountLayer:getChildByName("loginBg"):getChildByName("loginBtn")
    accountBtn:addTouchEventListener(openLogin)

    --注册点击事件
    local function zhuceLogin(sender, eventType)
        if eventType == ccui.TouchEventType.ended then
            app.musicSound:playSound(SOUND_HALL_TOUCH)
            uiManager:runScene("RegisterScene","fade",0.5,cc.c3b(15, 115, 200))
        end
    end 
    --注册按钮
    local zhuceBtn = loginAccountLayer:getChildByName("loginBg"):getChildByName("zhuceBtn")
    zhuceBtn:addTouchEventListener(zhuceLogin)
    
    
    --返回键和home键
    self:setKeypadEnabled(true)  
    self:addNodeEventListener(cc.KEYPAD_EVENT, function (event)  
        if event.key == "back" then  --返回键
            exitGame()
        end
    end)

    -- 隐藏客服QQ
    loginChoiceLayer:getChildByName("Label_8"):setVisible(false)

    -- 版本号
    local Version = loginChoiceLayer:getChildByName("Version")
    Version:setString(getVersionName())

    --走马灯
    --**********************************************--
    self.zoumaLabel = loginChoiceLayer:getChildByName("zoumaPanel"):getChildByName("lable")
    self.zoumaLabel:setString("")
    loginChoiceLayer:getChildByName("zoumaPanel"):setPositionY(650)

    local moveS = loginChoiceLayer:getChildByName("zoumaPanel"):getContentSize().width
    local zouMaSpeed = 500
    local zouMaL = self.zoumaLabel:getContentSize().width + moveS

    local firstBool=false
    --下一个文字，重置位置
    local shu=1
    local function unReversal1()
        zouMaSpeed=100
        self.zoumaLabel:setPosition(cc.p(moveS,11))
        local isDelete=false

        for k, v in pairs( self._zouma ) do
            if k == 1 then
                self.zoumaLabel:setString(v["label"])
                zouMaL=self.zoumaLabel:getContentSize().width+moveS
            end

            if firstBool then--第一次进入不删除
                if k == 2 then
                    isDelete=true
            end
            else
                firstBool=true
                isDelete=false
            end

        end

        if isDelete then
            local zouma1=self._zouma[1]
            table.remove(self._zouma,1)
            table.insert(self._zouma,zouma1)
        end

        local callfunc1 = cc.CallFunc:create(unReversal1)
        self.zoumaLabel:runAction(cc.Sequence:create(cc.MoveBy:create(zouMaL/zouMaSpeed,cc.p(-zouMaL,0)),callfunc1))

    end
    local callfunc1 = cc.CallFunc:create(unReversal1)
    self.zoumaLabel:runAction(cc.Sequence:create(cc.MoveBy:create(zouMaL/zouMaSpeed,cc.p(-zouMaL,0)),callfunc1))
    --**********************************************--
    --    self._dianjuan:setString(app.hallLogic._leJuan)--设置乐卷
    


    -- -- 公告
    -- local function noticeCallBack(sender, eventType)
    --     if eventType == ccui.TouchEventType.ended then
    --         app.musicSound:playSound(SOUND_HALL_TOUCH)

    --         local noticeLayer = Notice.new()
    --         noticeLayer:setTag(NOTICE_TAG)
    --         noticeLayer:show()
    --         self._uiLayer:addChild(noticeLayer, 100)  
    --     end
    -- end
    -- --公告按钮
    -- local noticeBtn = root:getChildByName("loginBg"):getChildByName("gonggaoBtn")
    -- noticeBtn:addTouchEventListener(noticeCallBack)

    -- -- 客服
    -- local function kefuCallBack(sender, eventType)
    --     if eventType == ccui.TouchEventType.ended then
    --         app.musicSound:playSound(SOUND_HALL_TOUCH)
    --         local helperLayer = Helper.new()
    --         helperLayer:setTag(HELP_TAG)
    --         helperLayer:show()
    --         self._uiLayer:addChild(helperLayer, 100)
    --     end
    -- end
    -- -- 客服按钮
    -- local kefuBtn = root:getChildByName("loginBg"):getChildByName("kefuBtn")
    -- kefuBtn:addTouchEventListener(kefuCallBack)
    -- -- 客服返回监听
    -- app.eventDispather:addListenerEvent(eventLoginFanKui, self, function(data)
    --     MyToast.new(self,app.hallLogic.fankuiMsg)
    -- end)

    
    --是否打开商城
    cc.UserDefault:getInstance():setBoolForKey("isStore",false)
    
    cc.UserDefault:getInstance():setBoolForKey("firstStore",false)
    
    --首页动画
    ccs.ArmatureDataManager:getInstance():addArmatureFileInfo( "HallUI/loginUI/shouyedonghua/shouye0.png", 
                                                                "HallUI/loginUI/shouyedonghua/shouye0.plist" , 
                                                                "HallUI/loginUI/shouyedonghua/shouye.ExportJson" )            
    local t_animation = ccs.Armature:create( "shouye" )                        
    t_animation:setPosition(cc.p(640, 360))
    loginBgLayer:addChild(t_animation)
    t_animation:getAnimation():play("shouye")

    --注册动画
    ccs.ArmatureDataManager:getInstance():addArmatureFileInfo( "HallUI/loginUI/zhecedonghua/zhuce0.png", 
                                                                "HallUI/loginUI/zhecedonghua/zhuce0.plist" , 
                                                                "HallUI/loginUI/zhecedonghua/zhuce.ExportJson" ) 

    local t_zhuCeAnimation = ccs.Armature:create( "zhuce" )   
    self._registerBtn:addChild(t_zhuCeAnimation)                    
    t_zhuCeAnimation:setPosition(cc.p(self._registerBtn:getContentSize().width / 2, self._registerBtn:getContentSize().height / 2 - 5))
    t_zhuCeAnimation:setTag(80)
    t_zhuCeAnimation:getAnimation():play("zhuceanniudonghua")
    self._registerBtn:setOpacity(0)

    
    --按钮动画
    ccs.ArmatureDataManager:getInstance():addArmatureFileInfo( "HallUI/loginUI/anniudonghua/anniu0.png", 
        "HallUI/loginUI/anniudonghua/anniu0.plist" , 
        "HallUI/loginUI/anniudonghua/anniu.ExportJson" )            
    local t_dengluAnimation = ccs.Armature:create( "anniu" )   
    self._accountBtn:addChild(t_dengluAnimation)                    
    t_dengluAnimation:setPosition(cc.p(self._accountBtn:getContentSize().width / 2, self._accountBtn:getContentSize().height / 2))
    t_dengluAnimation:setTag(80)    
    t_dengluAnimation:getAnimation():play("denglu")
    t_dengluAnimation:getAnimation():stop()
        
    
    local t_quickAnimation = ccs.Armature:create( "anniu" )   
    self._quickBtn:addChild(t_quickAnimation)                    
    t_quickAnimation:setPosition(cc.p(self._quickBtn:getContentSize().width / 2, self._quickBtn:getContentSize().height / 2))
    t_quickAnimation:setTag(80)
    t_quickAnimation:getAnimation():play("youke")
    t_quickAnimation:getAnimation():stop()
    
    self._time = 0
    self._delTime = math.random(3, 5)
    self:playBtnAnimation(1)
    self:playBtnAnimation(2)
    
    self:myUpdate()
end


--获取IP地址是不是国外的
function MainScene:getIPChina()
    local t_channel = getChannelName()
    local t_version = getVersionName()
    local t_sign = crypto.md5("IsChinaIP"..t_channel..t_version.."tjyjerge!@#sddf$^$h234^^DsSEG")
    local param = "action=IsChinaIP&channel="..t_channel.."&version="..t_version..
                "&sign="..t_sign
    local url = GETCONFIG_UEL.."?"..param

    local request = network.createHTTPRequest(function(event)
            if self then
                self:IPChinaCallBack(event)
            end        
        end, url, "POST")
    if request then
        request:setTimeout(10)
        request:start()
    end
end
function MainScene:IPChinaCallBack(event)
    local request = event.request
    if event.name == "completed" then
        if request:getResponseStatusCode() == 200 then
            local script = request:getResponseData()
            print( "IsChinaIP == ", script )
            cc.UserDefault:getInstance():setIntegerForKey("IsChinaIP", tonumber(script))
        end
    end
end

--获取开启哪些按钮
function MainScene:getButtonConfig()
    local t_channel = getChannelName()
    local t_version = getVersionName()
    local t_sign = crypto.md5("ButtonConfig"..t_channel..t_version.."tjyjerge!@#sddf$^$h234^^DsSEG")
    local param = "action=ButtonConfig&channel="..t_channel.."&version="..t_version..
                "&sign="..t_sign
    local url = GETCONFIG_UEL.."?"..param

    local request = network.createHTTPRequest(function(event)
            if self then
                self:ButtonConfigCallBack(event)
            end        
        end, url, "POST")
    if request then
        request:setTimeout(10)
        request:start()
    end
end
function MainScene:ButtonConfigCallBack(event)
    local request = event.request
    if event.name == "completed" then
        if request:getResponseStatusCode() == 200 then
            local script = request:getResponseData()
            local output = json.decode(script, 1) -- 解析json数据
            if output then
                dump(output)
                app.buttonConfig = output
            end
        end
    end
end

--获取充值相关的域名
function MainScene:getPayUrl()
    local t_channel = getChannelName()
    local t_version = getVersionName()
    local t_sign = crypto.md5("GetPayURL"..t_channel..t_version.."tjyjerge!@#sddf$^$h234^^DsSEG")
    local param = "action=GetPayURL&channel="..t_channel.."&version="..t_version..
                "&sign="..t_sign
    local url = GETCONFIG_UEL.."?"..param

    local request = network.createHTTPRequest(function(event)
            if self then
                self:getPayUrlCallBack(event)
            end        
        end, url, "POST")
    if request then
        request:setTimeout(10)
        request:start()
    end
end
function MainScene:getPayUrlCallBack(event)
    local request = event.request
    if event.name == "completed" then
        if request:getResponseStatusCode() == 200 then
            local script = request:getResponseData()
            if script then
                app.GETPAYCONFIG_UEL = script
            end
        end
    end
end

--获取充值相关的域名
function MainScene:getGameShunxu()
    local t_channel = getChannelName()
    local t_version = getVersionName()
    local t_sign = crypto.md5("GetSortGame"..t_channel..t_version.."tjyjerge!@#sddf$^$h234^^DsSEG")
    local param = "action=GetSortGame&channel="..t_channel.."&version="..t_version..
                "&sign="..t_sign
    local url = GETCONFIG_UEL.."?"..param
    print( "getGameShunxu === ", url )
    
    local request = network.createHTTPRequest(function(event)
            if self then
                self:getGameShunxuCallBack(event)
            end        
        end, url, "POST")
    if request then
        request:setTimeout(10)
        request:start()
    end
end
function MainScene:getGameShunxuCallBack(event)
    local request = event.request
    if event.name == "completed" then
        if request:getResponseStatusCode() == 200 then
            local script = request:getResponseData()
            local gametab = json.decode(script, 1)
            if gametab then
                app.GETGAMESHUNXU = gametab
            end
        end
    end
end

function MainScene:myUpdate()  
    local scheduler = cc.Director:getInstance():getScheduler()
    local schedulerEntry2 = nil

    local function step2(dt)
        self._time = self._time + dt
        if self._time >= self._delTime then
            self._time = 0
            self._delTime = math.random(3, 5)
            self:playBtnAnimation(1)
            self:playBtnAnimation(2)
        end
    end

    local function onNodeEvent(event)
        if event == "enter" then
            schedulerEntry2 = scheduler:scheduleScriptFunc(step2, 0.5, false)  --时间记录
        elseif event == "exit" then
            scheduler:unscheduleScriptEntry(schedulerEntry2)
            if cc.Director:getInstance():isPaused() then
                cc.Director:getInstance():resume()
            end        
        end
    end
    self._uiLayer:registerScriptHandler(onNodeEvent)
end

--传入走马灯文字
function MainScene:setZoumaLabel(label)
    local t_value= {}
    t_value["label"] = label
    table.insert(self._zouma, t_value)
end

--Web联网拿走马灯
local web_type = 1
function MainScene:requestWebFromServer(waittime)
    local t_channel = getChannelName()
    local t_version = getVersionName()
    local t_sign = crypto.md5("GameConfig"..t_channel..t_version.."tjyjerge!@#sddf$^$h234^^DsSEG")
    local param = "action=GameConfig&channel="..t_channel.."&version="..t_version.."&sign="..t_sign
    local url = GETCONFIG_UEL.."?"..param
    if web_type == 2 then
        url = GETCONFIG_UEL_BAK.."?"..param
        GETCONFIG_UEL = GETCONFIG_UEL_BAK
    end
    print( "requestWebFromServer == ",url )

    self.request = network.createHTTPRequest(function(event)
        if self and self.request then
            self:onWebResponse(event)
        end        
    end, url, "POST")
    if self.request then
        self.request:setTimeout(waittime or 10)
        self.request:start()
    end
end

--返回走马灯信息
function MainScene:onWebResponse(event, dumpResponse)
    local request = event.request
    if event.name == "completed" then
        self._accountBtn:setTouchEnabled(true)
        self._accountBtn:setBright(true)

        self._quickBtn:setTouchEnabled(true)
        self._quickBtn:setBright(true)

        self._registerBtn:setTouchEnabled(true)

        if request:getResponseStatusCode() ~= 200 then
            if web_type == 1 then
                web_type = 2
                self:requestWebFromServer()
            elseif web_type == 2 then
                MyToast.new(self, "网络连接失败，请稍后重试")
                --self:requestFromServer()
            end
        else
            self.dataRecv = request:getResponseData()
            --公告走马灯
            local script = self.dataRecv
            local tb = assert(loadstring(script))()
            self._VIPConf = tb.vipMoneyConfs
            app.hallLogic.webGameConf = tb

            local t_close = tb["hallModuleClose"]
            if t_close ~= nil then
                local channle = getChannelName()
                local version = getVersionName()

                local t_channles = t_close[channle]
                if t_channles ~= nil then
                    for k, v in pairs(t_channles) do
                        if v.version == version then
                            app.hallLogic._BtnsConfig = v.details
                            break
                        end
                    end
                end
            end
            self:setZoumaLabel(tb.hallMarquee)
        end 
        self.request = nil
        --判断是否登录过，如果是，则自动登录
        if isFirstLogin == true and cc.UserDefault:getInstance():getStringForKey("account")~=nil and cc.UserDefault:getInstance():getStringForKey("password")~=nil then            
            self:doQuickByIDPWD()
            isFirstLogin = false
        end
    elseif event.name == "failed" then
        if web_type == 1 then
            web_type = 2
            self:requestWebFromServer()
        elseif web_type == 2 then
            MyToast.new(self, "网络连接失败，请稍后重试")
            --self:requestFromServer()
        end
    end
end

--联网拿走马灯
function MainScene:requestFromServer()
    local url = GAMECONF_URL
    self.request = network.createHTTPRequest(function(event)
        if self and self.request then
            self:onResponse(event)
        end        
    end, url, "GET")
    if self.request then
        self.request:setTimeout(10)
        self.request:start()
    end
end

--返回走马灯信息
function MainScene:onResponse(event, dumpResponse)
    local request = event.request
    if event.name == "completed" then
        self._retryTime = 0

        self._accountBtn:setTouchEnabled(true)
        self._accountBtn:setBright(true)

        self._quickBtn:setTouchEnabled(true)
        self._quickBtn:setBright(true)

        self._registerBtn:setTouchEnabled(true)

        if request:getResponseStatusCode() == 200 then
            self.dataRecv = request:getResponseData()
            --公告走马灯
            local script = self.dataRecv
            local tb = assert(loadstring(script))()
            print( " GameChoice:onResponse  " )
            self._VIPConf = tb.vipMoneyConfs
            app.hallLogic.webGameConf = tb

            local t_close = tb["hallModuleClose"]
            if t_close ~= nil then
                local channle = getChannelName()
                local version = getVersionName()

                local t_channles = t_close[channle]
                if t_channles ~= nil then
                    for k, v in pairs(t_channles) do
                        if v.version == version then
                            app.hallLogic._BtnsConfig = v.details
                            break
                        end
                    end
                end
            end
            self:setZoumaLabel(tb.hallMarquee)
        end
        self.request = nil
        --判断是否登录过，如果是，则自动登录
        if isFirstLogin == true and cc.UserDefault:getInstance():getStringForKey("account")~=nil and cc.UserDefault:getInstance():getStringForKey("password")~=nil then            
            self:doQuickByIDPWD()
            isFirstLogin = false
        end
    elseif event.name == "failed" then
        self._retryTime = self._retryTime + 1
        self:getGameConfigFailed()
    end
end

function MainScene:playBtnAnimation( a_id )
--    print( "a_id   "..a_id )
    if a_id == MainSceneBtnID.DENG_LU then
        self._accountBtn:getChildByTag(80):getAnimation():play("denglu")
    -- elseif a_id == MainSceneBtnID.ZHU_CE then
    --     self._registerBtn:getChildByTag(80):getAnimation():play("zhuce")
    elseif a_id == MainSceneBtnID.YOU_XI then
        self._quickBtn:getChildByTag(80):getAnimation():play("youke")
    end
end

function MainScene:doQuickGame()
    self._loginType = self.TYPE_QUICK
    self:startLoginSchedule()
end

function MainScene:doLoginByIDPWD()
    self._username = self._usernameField:getString()
    self._password = self._passwordField:getText()
        
    if not self._username or #self._username==0 or not self._password or #self._password==0 then
        print("username or password is nil");
        self:delLoading()
        --显示tips
        MyToast.new(self,STR_LOGIN_IDPWD_NULL)
        return
    end
    self._loginType = self.TYPE_LOGIN
    self:startLoginSchedule()
end

function MainScene:onLoadingCancle()
	app.hallLogic:closeNet()
end
--快速进入
function MainScene:doQuickByIDPWD()
    self._username = cc.UserDefault:getInstance():getStringForKey("account")
    self._password = cc.UserDefault:getInstance():getStringForKey("password")
    
    if not self._username or #self._username==0 or not self._password or #self._password==0 then
        print("username or password is nil");
        --快速注册
        self:doQuickGame()
        return
    end
    
    self._loginType = self.TYPE_LOGIN
    self:startLoginSchedule()
end

function MainScene:onEnter()
    MainScene.super.onEnter(self)   
    app.musicSound:addMusic(DATINGBG, "HallSound/DATINGBG.mp3") 
    app.musicSound:addSound(SOUND_HALL_TOUCH, "HallSound/btn.mp3") 
    app.musicSound:addSound(SOUND_UI_OUTIN, "HallSound/UIOutIn.mp3") 
    app.hallLogic:closeHeartScheduler()

    self:requestWebServerInfo()  --获取配置文件 
    
    --第一次进入游戏的玩家默认播放音乐   
    local firstLogin=cc.UserDefault:getInstance():getBoolForKey("firstLogin")
    if firstLogin~=true then
        print("第一次进入了")
        cc.UserDefault:getInstance():setBoolForKey("firstLogin",true)
        cc.UserDefault:getInstance():setFloatForKey("soundEffect",1)
        cc.UserDefault:getInstance():setFloatForKey("sound",1)
    end
    
    print("播放背景音乐")
    app.musicSound:playMusic(DATINGBG, true)
    cc.UserDefault:getInstance():setBoolForKey("bgm",false)
    
    
    app.eventDispather:addListenerEvent(eventLoginSrvQuickRegist, self, function(data)
        if data.cbResult == 0 then  --快速注册成功
            self._username = data.szAccount
            self._password = data.szPassword
        else
            self:delLoading()
            MyToast.new(self,data.szFailedString)
            --self:showTip(false, data.szFailedString)
        end
    end)
end

function MainScene:onExit()
    MainScene.super.onExit(self)
    if self.request then
        self.request:cancel()
        self.request = nil
    end
end

function MainScene:onHallNetConnected(bConnected)
    if bConnected then
    
    else
        self:delLoading();
        --添加提示语句，提示网络连接失败
        MyToast.new(self,STR_LOGIN_NET_CONN_FAILED)
	end
end

function MainScene:onHallNetClose(data)
    self:delLoading();
    --添加提示语句，提示网络连接失败
    MyToast.new(self,STR_LOGIN_NET_CONN_FAILED)
end

function MainScene:onLoginSrvLoginSuccess(data)
    MainScene.super.onLoginSrvLoginSuccess(data)
    --登录成功
    self:delLoading()
    --切换到游戏选择页面
    cc.UserDefault:getInstance():setStringForKey("account", self._username)
    cc.UserDefault:getInstance():setStringForKey("password", self._password)
	--(sceneName, args, transitionType, time, more)
    uiManager:runScene("GameChoiceFish","","fade",0.5,cc.c3b(15, 115, 200))
end

function MainScene:startLoginSchedule()
    if isPlatLogin()==true then
        platLogin()     
        return
    end

    local function onLogin()
        if self._loginType == self.TYPE_LOGIN then
            app.hallLogic:loginByUsername(self._username,self._password)
        elseif self._loginType == self.TYPE_REGIST then
        
        elseif self._loginType == self.TYPE_QUICK then
            app.hallLogic:quickRegist()
    	end
    end
    
    self:addLoading()
	self:runAction(cc.Sequence:create(cc.DelayTime:create(0.0001) ,cc.CallFunc:create(onLogin)))
end

--Web流程
local websever_type = 1
function MainScene:requestWebServerInfo()
    local t_channel = getChannelName()
    local t_version = getVersionName()
    local t_sign = crypto.md5("LoginConfig"..t_channel..t_version.."tjyjerge!@#sddf$^$h234^^DsSEG")
    local param = "action=LoginConfig&channel="..t_channel.."&version="..t_version..
                "&sign="..t_sign

    local requestUrl = GETCONFIG_UEL.."?"..param
    if websever_type == 2 then
        requestUrl = GETCONFIG_UEL_BAK.."?"..param
    end
    print( "message === ",websever_type )
    print( "requestWebServerInfo : ", requestUrl )

    local request = network.createHTTPRequest(function(event)
        self:onLoginWebServerResponse(event)
    end, requestUrl, "POST")

    if request then
        request:setTimeout(6)
        request:start()
    end
end

function MainScene:onLoginWebServerResponse(event, dumpResponse)
    local request = event.request
    local function failedCallBack()
        if websever_type == 1 then
            websever_type = 2 
            self:requestWebServerInfo()    
        elseif websever_type == 2 then
            websever_type = 1
            self:getLoginServerConfFailed()
        end
    end
    if event.name == "completed" then
        if request:getResponseStatusCode() ~= 200 then
            failedCallBack()
        else
            self._bRequestLoginByDns = false
            self.dataRecv = request:getResponseData()
            local script = self.dataRecv
            local tb = json.decode(script, 1)
            if tb then
                if websever_type == 2 then
                    GETCONFIG_UEL = GETCONFIG_UEL_BAK
                end

                self:getIPChina()  --得到IP归属地
                self:getButtonConfig() --得到开启哪些按钮
                self:getPayUrl() --得到充值相关的域名
                self:getGameShunxu() --得到开放游戏的顺序

                if hallServerConf == nil then
                    hallServerConf = tb
                end
                self:requestWebFromServer()
            else
                failedCallBack()
            end
        end
    elseif event.name == "failed" then
        failedCallBack()
    end
end

function MainScene:getLoginServerConfFailed()
    self._getConfigState = 1
    if self._retryTime >= ALL_RETRY_TIME then
        self._tipNetBox:showText()
    else
        self._tipBox:setText(STR_CANNOT_GET_CONFIG)
        self._tipBox:showText()
    end
end

function MainScene:getGameConfigFailed()
    self._getConfigState = 2
    if self._retryTime >= ALL_RETRY_TIME then        
        self._tipNetBox:showText()
    else
        self._tipBox:setText(STR_CANNOT_GET_GAME_CONFIG)
        self._tipBox:showText()
    end
end

--获取字符串
function MainScene:getInfoString(a_action)
    local t_action = a_action
    local t_uid = app.hallLogic._loginSuccessInfo["dwUserID"]    
    local t_pwd = custom.PlatUtil:getMD5String(custom.PlatUtil:getMD5String(cc.UserDefault:getInstance():getStringForKey("password")))
    local t_time = tostring(os.date("%Y-%m-%d %H:%M:%S"))
    local t_sign = crypto.md5(t_uid..t_pwd..t_time.."jiujiu_888$99")
    local param = "action="..t_action.."&sign="..t_sign.."&time="..t_time.."&uid="..t_uid.."&pwd="..t_pwd

    return param
end

return MainScene
