require "lfs"
require("sdk.manager.NewStoreConfig")
local DataManager = require("app.uiManager.DataManager")

local GameChoice = class("GameChoice", import(".BaseScene"))

-- local BtnsKey = {"firstrecBtn", "BaoXiang2", "settingBtn", 
--                 "eventBtn", "noticeBtn", "rankingBtn", 
--                 "fankuiBtn", "storeBtn", "vipBtn", 
--                 "benefitsBtn", "addCoin"}

GameChoice.mainBtnName = { "bairen", "xiuxian", "qipai", "qita"};

GameChoiceIDs = {}
GameChoiceIDs[104] = "bairenniuniu"
GameChoiceIDs[108] = "benchibaoma"
GameChoiceIDs[122] = "baijiale"
GameChoiceIDs[219] = "lianhuanduobao"
GameChoiceIDs[114] = "xmkp"
GameChoiceIDs[150] = "shz"
GameChoiceIDs[998] = "wuziqi"
GameChoiceIDs[233] = "laohuji"
GameChoiceIDs[10815] = "feiqinzoushou"
GameChoiceIDs[614] = "senlinwuhui"
GameChoiceIDs[130] = "fishgame2d"

GameChoiceBtnPos = {}
GameChoiceBtnPos[104] = { x = 0, y = 30 }
GameChoiceBtnPos[108] = { x = 0, y = 28 }
GameChoiceBtnPos[122] = { x = 10, y = 46 }
GameChoiceBtnPos[219] = { x = 0, y = 110 }
GameChoiceBtnPos[114] = { x = 0, y = 80 }
GameChoiceBtnPos[150] = { x = 0, y = 65 }
GameChoiceBtnPos[998] = { x = 0, y = 35 }
GameChoiceBtnPos[233] = { x = 0, y = 156 }
GameChoiceBtnPos[10815] = { x = 0, y = 30 }
GameChoiceBtnPos[614] = { x = 0, y = 30 }
GameChoiceBtnPos[130] = {x = 0,y = 0}

GameChoiceDengPos = {}
GameChoiceDengPos[104] = { x = 0, y = 0 }
GameChoiceDengPos[108] = { x = 4, y = 0 }
GameChoiceDengPos[122] = { x = -10, y = 0 }
GameChoiceDengPos[219] = { x = -5, y = 0 }
GameChoiceDengPos[114] = { x = -5, y = 0 }
GameChoiceDengPos[150] = { x = -5, y = 0 }
GameChoiceDengPos[998] = { x = -5, y = 0 }
GameChoiceDengPos[233] = { x = -5, y = 0 }
GameChoiceDengPos[10815] = { x = -5, y = 0 }
GameChoiceDengPos[614] = { x = -5, y = 0 }
GameChoiceDengPos[130] = { x = 0, y = 0 }

GameChoiceShouZhiPos = {}
GameChoiceShouZhiPos[104] = { x = 80, y = -150 }
GameChoiceShouZhiPos[108] = { x = 80, y = -150 }
GameChoiceShouZhiPos[122] = { x = 80, y = -150 }
GameChoiceShouZhiPos[219] = { x = 80, y = -150 }
GameChoiceShouZhiPos[114] = { x = 80, y = -150 }
GameChoiceShouZhiPos[150] = { x = 80, y = -150 }
GameChoiceShouZhiPos[998] = { x = 80, y = -150 }
GameChoiceShouZhiPos[233] = { x = 80, y = -150 }
GameChoiceShouZhiPos[10815] = { x = 80, y = -150 }
GameChoiceShouZhiPos[614] = { x = 80, y = -150 }
GameChoiceShouZhiPos[130] = { x = 80, y = -150 }

GameShunXu = {}
GameShunXu[1] = 233
GameShunXu[2] = 10815
GameShunXu[3] = 150
GameShunXu[4] = 614
GameShunXu[5] = 104
GameShunXu[6] = 108
GameShunXu[7] = 114
GameShunXu[8] = 122
GameShunXu[9] = 998
GameShunXu[10] = 219
GameShunXu[11] = 130

local houseMusic = {}
houseMusic[150] = "ShzSound/BGM.mp3"


NOTICE_TAG      =   4681354153
SHOP_TAG        =   3684153616
RANK_TAG        =   4400541355
SET_TAG         =   8671453612
HELP_TAG        =   6121349684
VIP_TAG         =   4646845345
USERINFO_TAG    =   8649814595
BAOXIANG_TAG    =   8643861652

ACTION1_TAG     =   10000000
ACTION2_TAG     =   10000001
ACTION3_TAG     =   10000002
GAME_DOWNLOAD_TAG   =   34964164613
HOUSEUI_TAG         =   34964161234

GameChoice.__index = GameChoice
GameChoice._uiLayer= nil
GameChoice._layerInventory=nil

GameChoice._coin = nil --金币label
GameChoice._icon = nil --头像
GameChoice._name = nil --名字
GameChoice._gameListView = nil
GameChoice.zoumaLabel=nil     --走马灯文字
GameChoice._zouma = {}        --走马灯文字列表
GameChoice._dianjuan = nil --点卷label
GameChoice._baoxiang = nil  --宝箱
GameChoice._gameBtnList = nil --选择游戏的按钮list
GameChoice._xiaoBaoxiang = nil --小宝箱
GameChoice._nextPageBtn = nil   --下一页按钮
GameChoice._lastPageBtn = nil   --上一页按钮
GameChoice._nowPage = 0         --当前第几页
GameChoice._allPage = 0         --共多少页
GameChoice._updateRoom = false  --是否配置过房间
GameChoice._pinbiShouChong = false --是否屏蔽掉首冲界面的显示

GameChoice._gameId = 108

function GameChoice:ctor()
    self.TargetPlatform = cc.Application:getInstance():getTargetPlatform() --设备系统
    self._isChinaIP = cc.UserDefault:getInstance():getIntegerForKey("IsChinaIP")

    GameChoice.super.ctor(self)
    self._uiLayer = cc.Layer:create()
    self:addChild(self._uiLayer)

    self._gameBtnList = {}
    self._gameIdTab = {}

    self._isshenhe = false 

    self._updateRoom = false
    self._gameOpenConf = nil
    self._loading = true
    self._pinbiShouChong = false
    self._gonggaourl = nil --公告地址

    --主界面
    local layerInventory = ccs.GUIReader:getInstance():widgetFromJsonFile("GameSelect/GameSelect.json")
    self._uiLayer:addChild(layerInventory)

    self._layerInventory=layerInventory

    -- 救济金
    local benefitsBtn = layerInventory:getChildByName("Image_Top"):getChildByName("benefitsBtn")
    local finger = nil
    local function benefitsCallBack( sender, eventType )
        if eventType == ccui.TouchEventType.ended then
            app.musicSound:playSound(SOUND_HALL_TOUCH)
            if benefitsBtn:getParent():getParent():getChildByTag(11111) then
                benefitsBtn:getParent():getParent():getChildByTag(11111):removeFromParent()
            end
            local temptab = {cbResult = 2}
            -- if tonumber(app.hallLogic._loginSuccessInfo.lUserScore) >= 2000 then
            --     self:addBenefits(temptab)
            -- else
                local getCount = cc.UserDefault:getInstance():getIntegerForKey("getCount")
                if getCount then
                    if getCount <= 0 then
                        temptab.cbResult = 3
                        self:addBenefits(temptab)
                    else
                        app.hallLogic:onBenefits()
                    end
                else
                    app.hallLogic:onBenefits()
                end   
            --end
        end
    end
    benefitsBtn:addTouchEventListener(benefitsCallBack)

    ccs.ArmatureDataManager:getInstance():addArmatureFileInfo( "GameSelect/jiujijinxiaoguo/jiujijinxiaoguo0.png", 
        "GameSelect/jiujijinxiaoguo/jiujijinxiaoguo0.plist" , 
        "GameSelect/jiujijinxiaoguo/jiujijinxiaoguo.ExportJson" )
    local t_animation = ccs.Armature:create( "jiujijinxiaoguo")
    t_animation:getAnimation():play("jiujijinxiaoguodonghua")
    t_animation:setPosition(benefitsBtn:getContentSize().width / 2, benefitsBtn:getContentSize().height / 2)
    benefitsBtn:addChild(t_animation)

    --指引
    local is_finger = cc.UserDefault:getInstance():getIntegerForKey("beneguide")
    if is_finger == 3 then
        ccs.ArmatureDataManager:getInstance():addArmatureFileInfo( "GameRes/shouzhidonghua/shouzhi0.png", "GameRes/shouzhidonghua/shouzhi0.plist" , "GameRes/shouzhidonghua/shouzhi.ExportJson" )
        local animation = ccs.Armature:create( "shouzhi" )
        animation:setPosition(cc.p(display.cx, display.cy))
        benefitsBtn:getParent():getParent():addChild(animation, 126)
        animation:setTag(11111)
        animation:setVisible(false)
        animation:getAnimation():play("shouzhi")

        local size = benefitsBtn:getContentSize()
        local x, y = benefitsBtn:getPosition()
        local moveto = cc.MoveTo:create(0.5, cc.p(display.width-size.width/2+10, display.height-size.height/2-20))
        local seq_1 = cc.Sequence:create({
            cc.DelayTime:create(1),
            cc.CallFunc:create(function()
                animation:setVisible(true)
            end),
            moveto,
            })
        animation:runAction(seq_1)
        cc.UserDefault:getInstance():setIntegerForKey("beneguide", 0)
    end

    -- 首充值
    local function firstRecCallBack(sender, eventType)
        if eventType == ccui.TouchEventType.ended then
            app.musicSound:playSound(SOUND_HALL_TOUCH)
            
            local recUi = self:addFirstRecUi()
            self._uiLayer:addChild(recUi, 95)
        end
    end
    self.firstrecBtn = layerInventory:getChildByName("Image_Buttom"):getChildByName("firstrecBtn")
    self.firstrecBtn:addTouchEventListener(firstRecCallBack) 
    self.firstrecBtn:setVisible(false)
    
    ccs.ArmatureDataManager:getInstance():addArmatureFileInfo( "GameSelect/shouchonglibao/shouchonglibao0.png", 
        "GameSelect/shouchonglibao/shouchonglibao0.plist" , 
        "GameSelect/shouchonglibao/shouchonglibao.ExportJson" )
    local t_animation = ccs.Armature:create( "shouchonglibao")
    local f_size = self.firstrecBtn:getContentSize()
    t_animation:setPosition(f_size.width/2, f_size.height/2+10)
    self.firstrecBtn:addChild(t_animation)
    t_animation:getAnimation():play("shouchonglibaodonghua")

    -- local btn = cc.SpriteBatchNode:create("GameSelect/add2.png")
    -- for i = 1, 400 do
    --     local pic = cc.Sprite:createWithTexture(btn:getTexture())
    --     pic:setPosition(cc.p(2*i, 2*i))
    --     self:addChild(pic, 1000)
    -- end

    --------------------------------------------------------------------------------------
    --添加声音
    --
    app.musicSound:addSound(GameChoiceIDs[108], "HallSound/Game_108.mp3")
    app.musicSound:addSound(GameChoiceIDs[122], "HallSound/Game_122.mp3")
    app.musicSound:addSound(GameChoiceIDs[104], "HallSound/Game_104.mp3")
    app.musicSound:addSound(GameChoiceIDs[219], "HallSound/Game_219.mp3")
    app.musicSound:addSound(GameChoiceIDs[114], "HallSound/Game_114.mp3")
    app.musicSound:addSound(GameChoiceIDs[150], "HallSound/Game_150.mp3")
    app.musicSound:addSound(GameChoiceIDs[998], "HallSound/Game_998.mp3")
    app.musicSound:addSound(GameChoiceIDs[233], "HallSound/Game_233.mp3")
    app.musicSound:addSound(GameChoiceIDs[10815], "HallSound/Game_10815.mp3")
    app.musicSound:addSound("naoZhong", "HallSound/NaoZhong.mp3")
    app.musicSound:addSound(BAOXIANG_USE, "HallSound/BaoXiangUse.mp3")

    app.musicSound:addMusic(houseMusic[150], houseMusic[150])
    --
    --------------------------------------------------------------------------------------

    -------------box事件
    self.baoXiangTiShi = BombBox.new(false, STR_HCPY_BAOXIANG_MSG)
    self:addChild(self.baoXiangTiShi, 1000)
    self.baoXiangTiShi:setVisible(false)
    
    self.fanKuiTiShi = BombBox.new(false, STR_HCPY_FANKUI_MSG)
    self:addChild(self.fanKuiTiShi, 1000)
    self.fanKuiTiShi:setVisible(false)

    --宝箱
    self._baoXiangBox = BaoXiangBox.new()
    self:addChild(self._baoXiangBox, 1000)
    self._baoXiangBox:setVisible(false)

    --播放背景音乐
    local isBgm = cc.UserDefault:getInstance():getBoolForKey("bgm")
    if isBgm == true then
        local function unReversal1()
            print("播放背景音乐2")
            app.musicSound:playMusic(DATINGBG, true)
        end

        local callfunc1 = cc.CallFunc:create(unReversal1)
        self._layerInventory:runAction(cc.Sequence:create(cc.MoveBy:create(1, cc.p(0, 0)), callfunc1))
    end
    cc.UserDefault:getInstance():setBoolForKey("bgm", true)

    local topLayer = ccui.Helper:seekWidgetByName(layerInventory, "Image_Top")
    local buttomLayer = ccui.Helper:seekWidgetByName(layerInventory, "Image_Buttom")
    self._topLayer = topLayer
    self._buttomLayer = buttomLayer

    local topPosY = topLayer:getPositionY()
    local buttomPosY = buttomLayer:getPositionY()

    topLayer:setPositionY(1200)
    buttomLayer:setPositionY(-500)

    topLayer:runAction(cc.MoveBy:create(0.9, { ['x'] = 0, ['y'] = -(1200 - topPosY) }))
    buttomLayer:runAction(cc.MoveBy:create(0.9, { ['x'] = 0, ['y'] = 500 + buttomPosY }))

    --头像框的帧动画
    local t_touXiangKuang = ccui.Helper:seekWidgetByName(layerInventory, "touXiangKuang")
    ccs.ArmatureDataManager:getInstance():addArmatureFileInfo("GameSelect/touxiangkuang0.png", "GameSelect/touxiangkuang0.plist", "GameSelect/touxiangkuang.ExportJson")
    local t_touXiangAnimation = ccs.Armature:create("touxiangkuang")
    t_touXiangAnimation:getAnimation():play("touxiangkuang1")
    t_touXiangAnimation:setPosition(cc.p(83, 84.5))
    t_touXiangKuang:addChild(t_touXiangAnimation)

    --公告点击事件
    local function noticeCallBack(sender, eventType)
        if eventType == ccui.TouchEventType.ended then
            app.musicSound:playSound(SOUND_HALL_TOUCH)
            --MyToast.new(self._uiLayer, STR_NOTICE_NULL)

            local noticeLayer = Notice.new()
            noticeLayer:setTag(NOTICE_TAG)
            noticeLayer:show()
            self._uiLayer:addChild(noticeLayer, 100)
        end
    end

    --公告按钮
    local noticeBtn = ccui.Helper:seekWidgetByName(buttomLayer, "noticeBtn")
    noticeBtn:addTouchEventListener(noticeCallBack)
    -- if self.TargetPlatform == cc.PLATFORM_OS_IPHONE or self.TargetPlatform == cc.PLATFORM_OS_IPAD then
    --     noticeBtn:setVisible(false)
    --     noticeBtn:setTouchEnabled(false)
    -- end
    if not self._isChinaIP or self._isChinaIP == 0 then
        if not app.buttonConfig or app.buttonConfig["Notice"] ~= "1" then
            noticeBtn:setVisible(false)
            noticeBtn:setTouchEnabled(false)
        end
    end
    
    self.trumpetCount = ccui.Helper:seekWidgetByName(topLayer,"trumpetCount")    --小喇叭

    --活动点击事件
    local function eventCallBack(sender, eventType)
        if eventType == ccui.TouchEventType.ended then
            app.musicSound:playSound(SOUND_HALL_TOUCH)
            if self._gonggaourl and self._gonggaourl.URL ~= false then
                showUrl(self._gonggaourl.URL)
            else
                MyToast.new(self._uiLayer, STR_ACTIVE_NULL)
            end
            -- showUrl("www.baidu.com")
        end
    end
    --活动按钮
    local eventBtn = ccui.Helper:seekWidgetByName(buttomLayer, "eventBtn")
    eventBtn:addTouchEventListener(eventCallBack)
    -- if self.TargetPlatform == cc.PLATFORM_OS_IPHONE or self.TargetPlatform == cc.PLATFORM_OS_IPAD then
    --     eventBtn:setVisible(false)
    --     eventBtn:setTouchEnabled(false)
    -- end
    if not self._isChinaIP or self._isChinaIP == 0 then
        if not app.buttonConfig or app.buttonConfig["Activity"] ~= "1" then
            eventBtn:setVisible(false)
            eventBtn:setTouchEnabled(false)
        end
    end


    local t_rewardVis = true
    if app.hallLogic._BtnsConfig ~= nil then                 
        for k,v in pairs(app.hallLogic._BtnsConfig) do            
            if v == "loginRewards" then 
                t_rewardVis = false
                break
            end
        end                
    end
    if t_rewardVis then
        local rewards = LoginRewards.new( app.hallLogic.SigninDay, app.hallLogic.SigninCoin, 0 )
        self._uiLayer:addChild(rewards, 100)
        if app.hallLogic.IsLingQu == false then
           rewards:setVisible(true)
        end

        --监听
        app.eventDispather:addListenerEvent(eventLoginNoLingQu, self, function(data)
            app.hallLogic:registCurrentScene(rewards)
            rewards:init( app.hallLogic.SigninDay, app.hallLogic.SigninCoin )
            rewards:setVisible(true)
            end)
        app.hallLogic._jiemian=0
    end

    -- 客服QQ隐藏
    ccui.Helper:seekWidgetByName(buttomLayer, "Label_58"):setVisible(false)

    --商店按钮点击事件
    local function storeCallBack(sender, eventType)
        if eventType == ccui.TouchEventType.ended then
            app.musicSound:playSound(SOUND_HALL_TOUCH)
            self:showStore()
        end
    end
    --商店按钮
    local storeBtn = ccui.Helper:seekWidgetByName(buttomLayer, "storeBtn")
    storeBtn:addTouchEventListener(storeCallBack)
    
    --商店按钮动画
    ccs.ArmatureDataManager:getInstance():addArmatureFileInfo( "GameSelect/shangchenganniu0.png", "GameSelect/shangchenganniu0.plist" , "GameSelect/shangchenganniu.ExportJson")
    self._shangchenganniu = ccs.Armature:create( "shangchenganniu")
    self._shangchenganniu:getAnimation():play("shangchenganiu")
    self._shangchenganniu:setPosition(cc.p(80,20))
    storeBtn:addChild(self._shangchenganniu)
 
    --金币+按钮点击事件
    local function addCoinCallBack(sender, eventType)
        if eventType == ccui.TouchEventType.ended then
            app.musicSound:playSound(SOUND_HALL_TOUCH)
            self:showStore()
        end
    end

    --金币+按钮
    local addCoinBtn = ccui.Helper:seekWidgetByName(topLayer, "addCoin")
    addCoinBtn:addTouchEventListener(addCoinCallBack)
    
    --加金币按钮的动画 和 金币的动画
    ccs.ArmatureDataManager:getInstance():addArmatureFileInfo( "GameSelect/zengjiajinbianniudonghua/button_recharge0.png", 
        "GameSelect/zengjiajinbianniudonghua/button_recharge0.plist" , 
        "GameSelect/zengjiajinbianniudonghua/button_recharge.ExportJson" )
    local t_animation = ccs.Armature:create( "button_recharge")
    t_animation:getAnimation():play("button_recharge_ani")
    t_animation:setPosition(addCoinBtn:getContentSize().width / 2, addCoinBtn:getContentSize().height / 2)
    addCoinBtn:addChild(t_animation)


    --排行按钮点击事件
    local function rankingBtnAccount(sender, eventType)
        if eventType == ccui.TouchEventType.ended then
            app.musicSound:playSound(SOUND_HALL_TOUCH)   
            local rankLayer = self._uiLayer:getChildByTag(RANK_TAG)

            if rankLayer then
                return
            end
            rankLayer=Ranking.new()
            rankLayer:setTag(RANK_TAG)
            rankLayer:show()
            self._uiLayer:addChild(rankLayer, 100)
                      
        end
    end
    
    --排行按钮
    local rankingBtn = ccui.Helper:seekWidgetByName(buttomLayer, "rankingBtn")
    rankingBtn:addTouchEventListener(rankingBtnAccount) 
    --rankingBtn:setVisible(false)
    -- if self.TargetPlatform == cc.PLATFORM_OS_IPHONE or self.TargetPlatform == cc.PLATFORM_OS_IPAD then
    --     rankingBtn:setVisible(false)
    --     rankingBtn:setTouchEnabled(false)
    -- end
    if not self._isChinaIP or self._isChinaIP == 0 then
        if not app.buttonConfig or app.buttonConfig["Ranking"] ~= "1" then
            rankingBtn:setVisible(false)
            rankingBtn:setTouchEnabled(false)
        end
    end
    
    --设置按钮点击事件
    local function settingAccount(sender, eventType)
        if eventType == ccui.TouchEventType.ended then
            app.musicSound:playSound(SOUND_HALL_TOUCH)
            local setting = Setting.new()
            setting:setTag(SET_TAG)
            setting:init()
            setting:show()
            self._uiLayer:addChild(setting, 100)       
        end
    end
    --设置按钮
    local settingBtn = ccui.Helper:seekWidgetByName(buttomLayer, "settingBtn")
    settingBtn:addTouchEventListener(settingAccount)
    if not self._isChinaIP or self._isChinaIP == 0 then
        if not app.buttonConfig or app.buttonConfig["Set"] ~= "1" then
            settingBtn:setVisible(false)
            settingBtn:setTouchEnabled(false)
        end
    end

    --VIP按钮点击事件
    local function vipBtnAccount(sender, eventType)
        if eventType == ccui.TouchEventType.ended then
            app.musicSound:playSound(SOUND_HALL_TOUCH)
            local vipLayer = Vip.new(self)
            vipLayer:setParam(app.hallLogic._vipConf)
            vipLayer:show()
            vipLayer:setTag(VIP_TAG)
            self._uiLayer:addChild(vipLayer, 100)
        end
    end
    --VIP按钮
    local vipBtn = ccui.Helper:seekWidgetByName(topLayer, "vipBtn")
    vipBtn:addTouchEventListener(vipBtnAccount)
    --Vip特权动画
    ccs.ArmatureDataManager:getInstance():addArmatureFileInfo( "GameSelect/viptqanimation/viptqanniu0.png", 
        "GameSelect/viptqanimation/viptqanniu0.plist" , 
        "GameSelect/viptqanimation/viptqanniu.ExportJson" )
    local t_animation = ccs.Armature:create( "viptqanniu")
    t_animation:getAnimation():play("viptqanniudonghua")
    t_animation:setPosition(vipBtn:getContentSize().width / 2, vipBtn:getContentSize().height / 2)
    vipBtn:addChild(t_animation)

    --金币label
    self._coin = ccui.Helper:seekWidgetByName(topLayer, "goldLable")
    DataManager:getInstance():setCoinLable(self._coin)
    self._icon = ccui.Helper:seekWidgetByName(topLayer, "touXiangKuang")
    self._name = ccui.Helper:seekWidgetByName(topLayer, "name")
    self._dianjuan = ccui.Helper:seekWidgetByName(topLayer, "dianjuanLable")
    -- if self.TargetPlatform == cc.PLATFORM_OS_IPHONE or self.TargetPlatform == cc.PLATFORM_OS_IPAD then
    --     self._dianjuan:setVisible(false)
    -- end
    self._dianjuan:setVisible(false)

    self.lejuanbg = ccui.Helper:seekWidgetByName(topLayer, "lejuan")
    -- if self.TargetPlatform == cc.PLATFORM_OS_IPHONE or self.TargetPlatform == cc.PLATFORM_OS_IPAD then
    --     self.lejuanbg:setVisible(false)
    -- end
    self.lejuanbg:setVisible(false)
    DataManager:getInstance():setTicketLable(self._dianjuan)
    
    --头像点击事件
    self._icon:setTouchEnabled(true)
    self._icon:addTouchEventListener(function(sender, eventType)
        if eventType == ccui.TouchEventType.ended then
            app.musicSound:playSound(SOUND_HALL_TOUCH)
            local userInfoLayer = PersonalInformation.new(self)
            userInfoLayer:setTag(USERINFO_TAG)
            userInfoLayer:init()
            userInfoLayer:show()
            self._uiLayer:addChild(userInfoLayer, 100)
                     
        end
    end)

    --返回键和home键
    self._uiLayer:setKeypadEnabled(true)
    self._uiLayer:addNodeEventListener(cc.KEYPAD_EVENT, function (event)
        if event.key ~= "back" then  --返回键
            return
        end
        
        local houseUI= self._uiLayer:getChildByTag(HOUSEUI_TAG)
        if houseUI ~=nil then
            houseUI:removeFromParent()
            app.musicSound:playMusic(DATINGBG, true)
            return
        end

        if self._store and self._store._bShow == true then
            self._store:dismiss()
            return
        end
                
        local setLayer = self._uiLayer:getChildByTag(SET_TAG)
        if setLayer then
            setLayer:dismiss()
            return
        end

        local rankLayer = self._uiLayer:getChildByTag(RANK_TAG)
        if rankLayer then
            rankLayer:dismiss() 
            return       
        end 
                
        local vipLayer = self._uiLayer:getChildByTag(VIP_TAG)
        if vipLayer then
            vipLayer:dismiss()
            return
        end
        
        local noticeLayer = self._uiLayer:getChildByTag(NOTICE_TAG)
        if noticeLayer then
            noticeLayer:dismiss()
            return
        end
        
        local helpLayer = self._uiLayer:getChildByTag(HELP_TAG)
        if helpLayer then
            helpLayer:dismiss()
            return
        end
        
        local baoxiangLayer = self._uiLayer:getChildByTag(BAOXIANG_TAG)
        if baoxiangLayer then
            baoxiangLayer:dismiss()
            return
        end
        
        local userinfoLayer = self._uiLayer:getChildByTag(USERINFO_TAG)
        if userinfoLayer then
            userinfoLayer:dismiss()
            return
        end

        if self._loading == true then
            return
        end
        
        uiManager:runScene("MainScene")
    end)


    --_gameListView
    self._gameListView = layerInventory:getChildByName("ListView_Game")
    self._gameListView:setVisible(false)
    self._gameListByListView = layerInventory:getChildByName("ListView_GameList")
    
    local listPosX = self._gameListByListView:getPositionX()
    local listPosY = self._gameListByListView:getPositionY()
    self._gameListByListView:setPositionX(2000)
    self._gameListByListView:runAction(cc.Sequence:create(cc.MoveTo:create(0.4,cc.p(listPosX, listPosY)), 
        cc.MoveBy:create(0.05,cc.p(30, 0)), 
        cc.MoveBy:create(0.03,cc.p(-30, 0))))
    
    self._gameListView:setCustomScrollThreshold(300)                        --设置拖动多少像素就翻页

    local function gameViewCallback(sender, eventType)
        self._nowPage = sender:getCurPageIndex()
        self:setPageBtn()
    end
    self._gameListView:addEventListener(gameViewCallback)

    -- 添加强制指引
    self._guide = nil
    local isadd = cc.UserDefault:getInstance():getBoolForKey("addGuide")
    local guideBtn = layerInventory:getChildByName("guideBtn")
    if not isadd then
        print( "------- addGuide ---------" )
        cc.UserDefault:getInstance():setBoolForKey("addGuide", true)
        guideBtn:setVisible(true)

        ccs.ArmatureDataManager:getInstance():addArmatureFileInfo( "GameRes/shouzhidonghua/shouzhi0.png", "GameRes/shouzhidonghua/shouzhi0.plist" , "GameRes/shouzhidonghua/shouzhi.ExportJson" )
        local t_shouzhiAnimation = ccs.Armature:create( "shouzhi" )
        t_shouzhiAnimation:setPosition(cc.p(display.cx+300, display.cy-170))
        guideBtn:addChild(t_shouzhiAnimation)
        t_shouzhiAnimation:getAnimation():play("shouzhi")

        local function guideBtnCallBack(sender, eventType)
            if eventType == ccui.TouchEventType.moved then
               guideBtn:setVisible(false)
               guideBtn:removeFromParent()
            end
        end
        guideBtn:addTouchEventListener(guideBtnCallBack)
    else
        guideBtn:setVisible(false)
        guideBtn:removeFromParent()
    end

    --走马灯
    --**********************************************--
    local zoumaPosX=layerInventory:getChildByName("zoumaPanel"):getContentSize().width/2
    self.zoumaLabel=layerInventory:getChildByName("zoumaPanel"):getChildByName("label")
    self.zoumaLabel:setString("")
    self.zoumaLabel:setAnchorPoint(cc.p(0.5, 1.0))
    self.zoumaLabel:setPositionX(zoumaPosX)
    
    local zouMaSpeed = 500
    local firstBool = false
    local moveL = 600
    local zouMaL = 0
    local function unReversal1()
        zouMaSpeed = 100
        local isDelete = false

        for k, v in pairs(self._zouma) do
            if k == 1 then
                local ChatUI = require("sdk.manager.ChatUI")
                self.zoumaLabel:removeAllChildren()
                local text, lenth = ChatUI:getRichText(v["label"], 540) --走马长度600
                self.zoumaLabel:addChild(text)
                zouMaL = lenth+moveL
                self.zoumaLabel:setPosition(cc.p(zouMaL, 11))
            end

            if firstBool then--第一次进入不删除
                if k == 2 then
                    isDelete = true
                end
            else
                firstBool = true
                isDelete = false
            end
        end

        if isDelete then
            table.remove(self._zouma, 1)  
        end

        local callfunc1 = cc.CallFunc:create(unReversal1)
        self.zoumaLabel:runAction(cc.Sequence:create(cc.MoveBy:create(zouMaL / zouMaSpeed + 3, cc.p(-zouMaL * 1.5, 0)),
            callfunc1))
    end
    local callfunc1 = cc.CallFunc:create(unReversal1)
    self.zoumaLabel:runAction(cc.Sequence:create(callfunc1))
    
    -- local firstBool=false
    -- local zoumaIndex = 0
    -- --下一个文字，重置位置
    -- local function unReversal1()
    --     self.zoumaLabel:setPositionY(-100)
        
    --     zoumaIndex = zoumaIndex + 1
    --     local lines = 1
    --     if self._zouma and zoumaIndex > #self._zouma then
    --         zoumaIndex = 1
    --     end

    --     if self._zouma[zoumaIndex] then
    --         local msg = "";
    --         msg, lines = insertSubPerLen(self._zouma[zoumaIndex]["label"], 84, "\n")
    --         self.zoumaLabel:setString(msg)
    --     end

    --     local repeatMove = cc.Repeat:create(cc.Sequence:create(cc.MoveBy:create(0.4,cc.p(0, 28)), cc.DelayTime:create(2.5)), lines)

    --     local callfunc1 = cc.CallFunc:create(unReversal1)
    --     self.zoumaLabel:setPositionY(-100)
    --     self.zoumaLabel:setPositionX(zoumaPosX)
    --     self.zoumaLabel:runAction(cc.Sequence:create(cc.MoveBy:create(0.5,cc.p(0, 100)), cc.DelayTime:create(2.5),repeatMove, cc.MoveBy:create(0.5,cc.p(0, 100)), callfunc1))

    -- end
    -- local callfunc1 = cc.CallFunc:create(unReversal1)
    -- self.zoumaLabel:setPositionY(-100)
    -- self.zoumaLabel:setPositionX(zoumaPosX)
    -- self.zoumaLabel:runAction(cc.Sequence:create(cc.MoveBy:create(0.5,cc.p(0, 100)), cc.DelayTime:create(2.5), cc.MoveBy:create(0.5,cc.p(0, 100)), callfunc1))

    --**********************************************--
    --    self._dianjuan:setString(app.hallLogic._leJuan)--设置乐卷
    --self:requestFromServer(10)--走马灯数据获得
    self:setRenShu()--设置总人数
    ------宝箱

    --宝箱按钮事件
    local function baoxiangCallBack(sender, eventType)
        if eventType == ccui.TouchEventType.ended then
            app.musicSound:playSound(SOUND_HALL_TOUCH)           
            if app.hallLogic._baoxiangGeShu>0 and app.hallLogic._baoxiangGeShu>app.hallLogic._baoxiangGe
             and app.hallLogic._baoxiangJuShu>=app.hallLogic._baoxiangkaiqi and app.hallLogic._isKaiQi==true then
                self:showBaoXiang(1,"12345")
                app.hallLogic._isKaiQi=false
            else
                print("未达到开启宝箱条件")
                self:noBaoXiang()
            end

        end
    end
    --宝箱按钮
    self._baoxiang=layerInventory:getChildByName("baoxiangBtn")
    self._baoxiang:addTouchEventListener(baoxiangCallBack)
    self:setBaoXiangText()--设置宝箱文字
    --小宝箱
    ccs.ArmatureDataManager:getInstance():addArmatureFileInfo( "BaoXiang/xiaobaoxiang0.png", "BaoXiang/xiaobaoxiang0.plist" , "BaoXiang/xiaobaoxiang.ExportJson" )
    self._xiaoBaoxiang = ccs.Armature:create( "xiaobaoxiang")
    --self._xiaoBaoxiang:getAnimation():play("xiaobaoxiang")
    self._xiaoBaoxiang:setPosition(cc.p(self._baoxiang:getPositionX()-3,self._baoxiang:getPositionY()+6))
    layerInventory:addChild(self._xiaoBaoxiang)
    self:setBaoXiangText()

    --下一页按钮点击事件
    local function nextPageBtnCallback(sender, eventType)
        if eventType == ccui.TouchEventType.ended then
            app.musicSound:playSound(SOUND_HALL_TOUCH)
            print( "nextPageBtnCallback self._nowPage "..self._nowPage )
            self._nowPage = self._nowPage + 1
            self._gameListView:scrollToPage(self._nowPage)
        end
    end

    --下一页按钮
    self._nextPageBtn = layerInventory:getChildByName("NextPageBtn")
    self._nextPageBtn:addTouchEventListener(nextPageBtnCallback)

    --箭头动画
    ccs.ArmatureDataManager:getInstance():addArmatureFileInfo( "GameSelect/jiantoudonghua/jiantou0.png",
        "GameSelect/jiantoudonghua/jiantou0.plist" ,
        "GameSelect/jiantoudonghua/jiantou.ExportJson" )
    local t_nextAnimation = ccs.Armature:create( "jiantou" )
    self._nextPageBtn:addChild(t_nextAnimation)
    t_nextAnimation:getAnimation():play("jiantou1")
    t_nextAnimation:setPosition(self._nextPageBtn:getContentSize().width / 2, self._nextPageBtn:getContentSize().height / 2)
    --self._nextPageBtn:setVisible(false)

    --上一页按钮点击事件
    local function lastPageBtnCallback(sender, eventType)
        if eventType == ccui.TouchEventType.ended then
            app.musicSound:playSound(SOUND_HALL_TOUCH)
            self._nowPage = self._nowPage - 1
            print( "lastPageBtnCallback self._nowPage "..self._nowPage )
            self._gameListView:scrollToPage(self._nowPage)
        end
    end

    --上一页按钮
    self._lastPageBtn = layerInventory:getChildByName("LastPageBtn")
    self._lastPageBtn:addTouchEventListener(lastPageBtnCallback)

    local t_lastAnimation = ccs.Armature:create( "jiantou" )
    self._lastPageBtn:addChild(t_lastAnimation)
    t_lastAnimation:getAnimation():play("jiantou1")
    t_lastAnimation:setPosition(self._lastPageBtn:getContentSize().width / 2, self._lastPageBtn:getContentSize().height / 2)
    --self._lastPageBtn:setVisible(false)

    --反馈按钮点击事件
    local function fankuiBtnAccount(sender, eventType)
        if eventType == ccui.TouchEventType.ended then
            app.musicSound:playSound(SOUND_HALL_TOUCH)
            -- local helperLayer = Helper.new()
            -- helperLayer:setTag(HELP_TAG)
            -- helperLayer:show()
            -- self._uiLayer:addChild(helperLayer, 100)
            showUrl('http://chat32.live800.com/live800/chatClient/chatbox.jsp?companyID=645036&configID=82709&jid=6560548107')
        end
    end
    --反馈按钮
    self.fankuiBtn = ccui.Helper:seekWidgetByName(buttomLayer, "fankuiBtn")
    self.fankuiBtn:addTouchEventListener(fankuiBtnAccount)
    if not self._isChinaIP or self._isChinaIP == 0 then
        if not app.buttonConfig or app.buttonConfig["Service"] ~= "1" then
            self.fankuiBtn:setVisible(false)
            self.fankuiBtn:setTouchEnabled(false)
        end
    end
    --反馈返回监听
    app.eventDispather:addListenerEvent(eventLoginFanKui, self, function(data)
        MyToast.new(self, app.hallLogic.fankuiMsg)
    end)
    

    app.hallLogic._firstRank=false
    
    --设置头像
    local headIconNum=cc.UserDefault:getInstance():getIntegerForKey("headIcon")
    if headIconNum==0 then
        math.randomseed(os.time()) 
        print("随机头像生成")
        headIconNum=math.random(1,30)
        headIconNum=math.random(1,30)
        cc.UserDefault:getInstance():setIntegerForKey("headIcon",headIconNum)
    end
    
    if headIconNum<=15 then
        t_touXiangKuang:getChildByName("touXiang"):loadTexture("headIcon/y"..headIconNum..".png")
    else
        t_touXiangKuang:getChildByName("touXiang"):loadTexture("headIcon/yy"..(headIconNum-15)..".png")
    end
    
    --宝箱2事件
    self.BaoXiang2Btn = ccui.Helper:seekWidgetByName(buttomLayer,"BaoXiang2")
    if not self._isChinaIP or self._isChinaIP == 0 then
        if not app.buttonConfig or app.buttonConfig["TreasureChests"] ~= "1" then
            self.BaoXiang2Btn:setVisible(false)
        end
    end
    local function BaoXiang2BtnAccount(sender, eventType)
        if eventType == ccui.TouchEventType.ended then
            self.BaoXiang2Btn:stopAllActions()
            self.BaoXiang2Btn:setScale(1)

            app.musicSound:playSound(SOUND_HALL_TOUCH)
            local baoxiangLayer = BaoXiangLayer.new()
            DataManager:getInstance():setBoxLayer(baoxiangLayer)
            baoxiangLayer:setXiangziInfo(app.hallLogic._userRedEnvelopeInfo)
            baoxiangLayer:setTag(BAOXIANG_TAG)
            baoxiangLayer:show()
            self._uiLayer:addChild(baoxiangLayer, 100)
        end
    end
    --宝箱2按钮 
    self.BaoXiang2Btn:addTouchEventListener(BaoXiang2BtnAccount)

    self:isShowFirstRec()

    DataManager:getInstance():setChoiceLayer(self)    
    self:parseWebConf()

    local isstoreTip = cc.UserDefault:getInstance():getIntegerForKey("storeTip")
    if not isstoreTip or isstoreTip == 0 then
        self:tipRemindLayer()
    end
	
	--主界面的四个按钮
	--[[function mainBtnCallback( pSender, ty)
		if ty ~= 2 then
			return;
		end
		
		local name = pSender:getName();
		local index = 0;
		for k,v in ipairs( GameChoice.mainBtnName) do
			if v == name then
				index = k;
			end
		end
		
		--self:initSecond( index);
		copyStrToShearPlate( "zuogeceshi "..name);
	end
	for k,v in ipairs( GameChoice.mainBtnName) do
		local btn = ccui.Helper:seekWidgetByName( layerInventory, v);
		btn:addTouchEventListener( mainBtnCallback);
	end--]]
    app.hallLogic:shiwuMessage()
end

function GameChoice:updateShiWuMessage()
    --更新实物信息
    local tempnode = self._layerInventory:getChildByName("Image_Top")
    local shiwuNumTab = app.hallLogic._shiwuNumTab
    --dump(shiwuNumTab)
    for i = 1, 4 do
        if not self._isChinaIP or self._isChinaIP == 0 then
            if not app.buttonConfig or app.buttonConfig["Entity"] ~= "1" then
                tempnode:getChildByName("shiwubg"..i):setVisible(false)
            end
        end
        local numlabel = tempnode:getChildByName("shiwubg"..i):getChildByName("shiwunum"..i)
        numlabel:setString("x"..shiwuNumTab.stItem[shiwuNumTab.stItem[i].wPropsID].dwPieceAmount)
    end
end

function GameChoice:initSecond( index)
	if index == 0 then
		return;
	end
	
	local mainLayer = ccui.Helper:seekWidgetByName( self._layerInventory, "mainLayer");
	mainLayer:setVisible( false);
	local secondLayer = ccui.Helper:seekWidgetByName( self._layerInventory, "secondLayer");
	secondLayer:setVisible( true);
	local list = ccui.Helper:seekWidgetByName( self._layerInventory, "secondList");
	
	--返回
	local function returnMain( pSender, ty)
		if ty ~= 2 then
			return;
		end
		mainLayer:setVisible( true);
		secondLayer:setVisible( false);
	end
	local returnBtn = ccui.Helper:seekWidgetByName( self._layerInventory, "returnMain");
	returnBtn:addTouchEventListener( returnMain);
	
	if index == 1 then
		--百人
	elseif index == 2 then
		--休闲
	elseif index == 3 then
		--棋牌
	elseif index == 4 then
		--其他
	end
end

--收到红包提示
function GameChoice:getRedPackageTip(tab)
    self.BaoXiang2Btn:stopAllActions()
    self.BaoXiang2Btn:setScale(1)
    local ac1 = cc.ScaleTo:create(0.4, 0.8)
    local ac2 = cc.ScaleTo:create(0.4, 1.1)
    local seq = transition.sequence({
        ac1,
        ac2
        })
    self.BaoXiang2Btn:runAction(cc.RepeatForever:create(seq))
    MyToast.new(self, tab.szFromNick.."赠送你"..tab.dwCount.."个"..tab.dwRMBValue.."元宝箱")
end

function GameChoice:setBtnsVisibleWithConfig(tb)
    -- if app.hallLogic._BtnsConfig == nil then
    --     local t_close = tb["hallModuleClose"]
    --     if t_close ~= nil then
    --         local channle = getChannelName()
    --         local version = getVersionName()

    --         local t_channles = t_close[channle]
    --         if t_channles ~= nil then
    --             for k,v in pairs(t_channles) do
    --                 if v.version == version then
    --                     app.hallLogic._BtnsConfig = v.details
    --                     break
    --                 end
    --             end
    --         end
    --     end
    -- end

    -- local btns = {}

    -- for k,v in pairs(BtnsConfig) do
    --     btns[v] = ccui.Helper:seekWidgetByName(self._layerInventory, v)
    --     -- btns[v]:setVisible(true)
    -- end

    if app.hallLogic._BtnsConfig ~= nil then
        for k,v in pairs(app.hallLogic._BtnsConfig) do
            local t_btn = ccui.Helper:seekWidgetByName(self._layerInventory, v)
            if t_btn ~= nil then
                t_btn:setVisible(false)
            end

            if v == "firstrecBtn" then
                self._pinbiShouChong = true
            end
        end
    end


    --self:everyShowFirstRec()

end

-- 是否显示首充礼包
function GameChoice:isShowFirstRec()
	-- self.firstrecBtn:setVisible(false)
	-- self.firstrecBtn:setTouchEnabled(false)
	-- self.fankuiBtn:setVisible(true)
	-- self.fankuiBtn:setTouchEnabled(true)
	return;    
    --[[if self._pinbiShouChong then
        return
    end

    local ispay = cc.UserDefault:getInstance():getIntegerForKey("isPay")
    print("----------ispay = ",ispay)
    if ispay and ispay ~= 0 then
        self.firstrecBtn:setVisible(false)
        self.firstrecBtn:setTouchEnabled(false)
        self.fankuiBtn:setVisible(true)
        self.fankuiBtn:setTouchEnabled(true)
    else
        self.firstrecBtn:setVisible(true)
        self.firstrecBtn:setTouchEnabled(true)
        self.fankuiBtn:setVisible(false)
        self.fankuiBtn:setTouchEnabled(false)
    end--]]
    -- local x, y = self.firstrecBtn:getPosition()
    -- self.firstrecBtn:setPosition(cc.p(x-100,y))
    -- local ac = cc.MoveTo:create(1, cc.p(x, y))
    -- self.firstrecBtn:runAction(ac)
end

--每次进主界面显示首充界面
function GameChoice:everyShowFirstRec()
    print("self._pinbiShouChong "..tostring(self._pinbiShouChong))
    if self._pinbiShouChong then
        return
    end

    local ispay = cc.UserDefault:getInstance():getIntegerForKey("isPay")
    if (not isplay or ispay == 0) and app.hallLogic._loginSuccessInfo.obMemberInfo.cbMemberOrder == 0 then
        local recUi = self:addFirstRecUi()
        self._uiLayer:addChild(recUi, 95)
    end
end

--抽奖界面显示
function GameChoice:isShowRotaryUiLayer()
    local isshow = cc.UserDefault:getInstance():getIntegerForKey("isRotrayTab")
    if (not isshow or isshow > 0) and app.hallLogic._loginSuccessInfo.obMemberInfo.cbMemberOrder > 0 then
        -- 抽奖界面测试
        local RotaryTabLayer = require("app.uiManager.RotaryTabLayer")
        local rotarylogic = RotaryTabLayer.new()
        DataManager:getInstance():setRotary(rotarylogic)
        self._uiLayer:addChild(rotarylogic, 129)
    end
end

-- 加载救济金页面
--id=0成功, =1参数不正确, =2钱多不能领, =3次数不够
function GameChoice:addBenefits(tab)
    local id = tab.cbResult
    if id == 0 then
        local string1 = "领取"..tab.llDonateScore.."金币,还可以领取"..(tab.dwReliefCount-tab.dwCurrentCount).."次"
        -- 存储领取的次数
        cc.UserDefault:getInstance():setIntegerForKey("getCount", tab.dwReliefCount-tab.dwCurrentCount)
        if tab.dwReliefCount-tab.dwCurrentCount == 0 then
            string1 = "领取"..tab.llDonateScore.."金币,今天次数已领完"
        end
        local string3 = "（每天最多可以领取"..tab.dwReliefCount.."次救济金）"

        local beneui = self:jumpBenefitsUI(string1, 10, string3, true)
        self._uiLayer:addChild(beneui, 101)
    elseif id == 2 then
        local string1 = "当前游戏币过多不能领取，是否去充值"
        local string3 = "（每天最多可以领取"..tab.dwReliefCount.."次救济金）"
        local beneui = self:jumpBenefitsUI(string1, 10, string3)
        self._uiLayer:addChild(beneui, 101)
    elseif id == 3 then
        local string1 = "领取次数不足，是否充值"
        local string3 = "（每天最多可以领取"..tab.dwReliefCount.."次救济金）"
        local beneui = self:jumpBenefitsUI(string1, 10, string3)
        self._uiLayer:addChild(beneui, 101)
    end
end

-- 救济金页面
-- string1提示语 string2倒计时 string3领取次数
function GameChoice:jumpBenefitsUI(string1, dt, string3, isexit)
    local bene_ui = ccs.GUIReader:getInstance():widgetFromJsonFile("HallUI/BenefitsUi.json")
    local function sureCallBack(sender, eventType)
        if eventType == ccui.TouchEventType.ended then
            app.musicSound:playSound(SOUND_HALL_TOUCH)
            bene_ui:removeFromParent()
            if not isexit then
                self:showStore()
            end
        end
    end
    local sureBtn = bene_ui:getChildByName("benefitsbg"):getChildByName("sure_Btn")
    sureBtn:addTouchEventListener(sureCallBack)

    local function exitCallBack(sender, eventType)
        if eventType == ccui.TouchEventType.ended then
            app.musicSound:playSound(SOUND_HALL_TOUCH)
            bene_ui:removeFromParent()
        end
    end
    local exitBtn = bene_ui:getChildByName("benefitsbg"):getChildByName("exit_Btn")
    exitBtn:addTouchEventListener(exitCallBack)

    local lab_1 = bene_ui:getChildByName("benefitsbg"):getChildByName("Label_remind")
    local lab_2 = bene_ui:getChildByName("benefitsbg"):getChildByName("Label_time")
    local lab_3 = bene_ui:getChildByName("benefitsbg"):getChildByName("Label_Tips")
    lab_1:setString(string1)
    lab_3:setString(string3)

    local schedulerEntry = nil
    local str_time = dt
    lab_2:setString(tostring("("..str_time.."S)"))
    local function step()
        str_time = str_time-1    
        if str_time <= 0 then
            bene_ui:removeFromParent()
        else
            lab_2:setString(tostring("("..str_time.."S)"))
        end
    end 
    local seq = transition.sequence({
        cc.DelayTime:create(1),
        cc.CallFunc:create(step)
        })
    bene_ui:runAction(cc.RepeatForever:create(seq))

    return bene_ui
end

--首充页面
function GameChoice:addFirstRecUi()
    local rec_ui = ccs.GUIReader:getInstance():widgetFromJsonFile("HallUI/firstRecUi.json")

    local function exitCallBack(sender, eventType)
        if eventType == ccui.TouchEventType.ended then
            app.musicSound:playSound(SOUND_HALL_TOUCH)
            rec_ui:removeFromParent()
        end
    end
    local exitBtn = rec_ui:getChildByName("bg"):getChildByName("exitBtn")
    exitBtn:addTouchEventListener(exitCallBack)

    local function RecCallBack(sender, eventType)
        if eventType == ccui.TouchEventType.ended then
            app.musicSound:playSound(SOUND_HALL_TOUCH)
            -- 去首充
            pay(FIRST_PAY_INDEX)
        end
    end
    local recBtn = rec_ui:getChildByName("bg"):getChildByName("recBtn")
    recBtn:addTouchEventListener(RecCallBack)
    rec_ui:getChildByName("bg"):setScale(0)
    local sca_Ac = cc.ScaleTo:create(0.5, 1)
    rec_ui:getChildByName("bg"):runAction(sca_Ac)

    return rec_ui
end

--排行 1，信息2，listview、
function GameChoice:setRanking(msg,listview)
    for k,v in pairs(msg["UserInfo"]) do        
        local list={}
        --1百家乐122，2百人牛牛104，3豪车漂移108，4连环夺宝219
        if v["wGameCount"]~=0 then
            for k2,v2 in pairs(v["wGameKind"]) do
                if v2==122 then
                    list[1]=true
                end
                if v2==104 then
                    list[2]=true
                end
                if v2==108 then
                    list[3]=true
                end
                if v2==219 then
                    list[4]=true
                end
            end
        end
        local t_score=string.format("%f", v["lScore"])
        local tmps = string.split(t_score, '.')
        local t_test0 = RankingObject.new(k,tmps[1],v["wFaceID"],v["szNickname"],list)
        listview:insertCustomItem(t_test0,k-1)
    end
end

--不能开启宝箱
function GameChoice:noBaoXiang()
    --self.baoXiangTiShi:showText()
    MyToast.new(self,STR_HCPY_BAOXIANG_MSG)
end

--设置宝箱文字
function GameChoice:setBaoXiangText()
    local baoxiangLab=self._baoxiang:getChildByName("lab")
    if VERSION_DEBYG == true then      
        print("有多少个宝箱"..app.hallLogic._baoxiangGeShu.."已开启多少个宝箱"..app.hallLogic._baoxiangGe.."开启场次"..app.hallLogic._baoxiangkaiqi.."宝箱局数"..app.hallLogic._baoxiangJuShu)
    end

    if app.hallLogic._baoxiangGeShu==0 or app.hallLogic._baoxiangGe==app.hallLogic._baoxiangGeShu then
        print("没有宝箱了")
        baoxiangLab:setString("-/-")
        return
    end
    local proCount = app.hallLogic._baoxiangJuShu
    if proCount > app.hallLogic._baoxiangkaiqi then
        proCount = app.hallLogic._baoxiangkaiqi
    end
    local strBaoXiang = string.format('%d/%d', proCount, app.hallLogic._baoxiangkaiqi)
    baoxiangLab:setString(strBaoXiang)
    --播放小宝箱动画
    if proCount==app.hallLogic._baoxiangkaiqi then
        if self._xiaoBaoxiang~=nil then
            self._xiaoBaoxiang:getAnimation():play("xiaobaoxiang")
        end
    else
        if self._xiaoBaoxiang~=nil then
            self._xiaoBaoxiang:getAnimation():play("xiaobaoxiang")
            self._xiaoBaoxiang:getAnimation():stop()
        end
    end

end

--宝箱num 1，获得乐卷，2 获得金币 text金币数量
function GameChoice:showBaoXiang(num,text)
    --self._baoXiangBox:setVisible(true)
    self._baoXiangBox:kaiQi()
end

function GameChoice:setRenShu()
    -- print("在线人数-"..app.hallLogic._zongRenShu)
    ccui.Helper:seekWidgetByName(self._layerInventory,"zaixianNum"):setString(app.hallLogic._zongRenShu)--设置在线玩家人数
end

--联网拿走马灯
local url_type = 1
function GameChoice:requestFromServer(waittime)
    local t_channel = getChannelName()
    local t_version = getVersionName()

    local t_sign = crypto.md5("GameConfig"..t_channel..t_version.."tjyjerge!@#sddf$^$h234^^DsSEG")
    local param = "action=GameConfig&channel="..t_channel.."&version="..t_version.."&sign="..t_sign
    local url = GETCONFIG_UEL.."?"..param
    if url_type == 2 then
        url = GETCONFIG_UEL_BAK.."?"..param
    end
    self.request = network.createHTTPRequest(function(event)
        if self then
        	self:onResponse(event)
        end        
    end, url, "POST")
    if self.request then
        self.request:setTimeout(waittime or 10)
        self.request:start()
    end
end

--返回走马灯信息
function GameChoice:onResponse(event, dumpResponse)
    local request = event.request
    if event.name == "completed" then
        self.request = nil
        if request:getResponseStatusCode() ~= 200 then
            if url_type == 1 then
                url_type = 2
                self:requestFromServer()
            end
        else
            self.dataRecv = request:getResponseData()
            --公告走马灯
            local  script = self.dataRecv
            local tb = assert(loadstring(script))()                    
            app.hallLogic._vipConf = tb.vipMoneyConfs
            self:setBtnsVisibleWithConfig(tb)
            app.eventDispather:dispatherEvent(eventUpdateVipConf, app.hallLogic._vipConf)
            for k,v in pairs( tb.hgallMarquees ) do
                self:setZoumaLabel(v)
            end
            self._gameOpenInfo = tb.gameOpen
            if tb.gameInOrder then
                self._gameIdTab = tb.gameInOrder --游戏ID排序
            end
            if self._gameOpenInfo and #self._gameOpenInfo > 0 then
                local function updateGames()                                                       
                    local channle = getChannelName()
                    local version = getVersionName()
                    local games = nil
                    local defaultGames = nil
                    if channle and version then
                        for index, item in ipairs(self._gameOpenInfo) do
                            if item.channle == "default" then
                            	defaultGames = item.games
                            end
                            
                            if item.channle == channle and item.version == version then
                                games = item.games
                                break
                            end
                        end
                    end
                    
                    if games == nil then
                    	games = defaultGames
                    end 
                    if games then
                        self._gameOpenConf = games
                    	self._updateRoom = false
                    	self:updateGames()
                    end                    
                end

                self:runAction(cc.Sequence:create(cc.DelayTime:create(1.0), cc.CallFunc:create(updateGames)))
            end
        end
    elseif event.name == "failed" then
        if url_type == 1 then
            url_type = 2
            self:requestFromServer()
        elseif url_type == 2 then
            url_type = 1
            self:endProcess()
            self.request = nil
        end
    end
end



--传入走马灯文字
function GameChoice:setZoumaLabel(label)
    local t_value= {}
    t_value["label"] = label
    table.insert(self._zouma,t_value)
end

function GameChoice:getGameInfo(gameId)
    for index, gameinfo in ipairs(hallGames) do
        if gameinfo.gameId == gameId then
            return gameinfo;
        end
    end
end

function GameChoice:setDownProgress(gameWgt, percent)
    local downWgt = gameWgt:getChildByTag(GAME_DOWNLOAD_TAG)
    if downWgt then
    	downWgt.progressBG:setVisible(true)
    	downWgt.progressBG.progress:setPercentage(percent)
        downWgt.progressBG.progressLab:setString(percent.."%")	
    end
end

function GameChoice:setGameWgtUpdate(gameWgt, bDown)
	if gameWgt._bDownload ~= true then	
        gameWgt:removeAllChildren()
        local size = gameWgt:getContentSize()
        local wgt = self:getGameDownloadWgt(gameWgt._gameInfo)
        wgt:setPosition(size.width/2, size.height/2)
        wgt:setTag(GAME_DOWNLOAD_TAG)
        gameWgt:addChild(wgt, 100)                
        gameWgt._bDownload = true
	end
	
    local downWgt = gameWgt:getChildByTag(GAME_DOWNLOAD_TAG)
    if downWgt and downWgt.updateIcon then
        downWgt.progressBG:setVisible(false)
        downWgt.updateIcon:loadTexture("GameRes/GameUpdate.png")      
    end
end

function GameChoice:getGameDownloadWgt(game)
    local gameUnable = ccui.ImageView:create()
    local unableFile = string.format("GameRes/GameUnable_%d.png", game.gameId)
    gameUnable:loadTexture(unableFile)
    
    local size = gameUnable:getContentSize()

    local updateIcon = ccui.ImageView:create()
    updateIcon:loadTexture("GameRes/GameUpdate.png")
    updateIcon:setPosition(size.width/2, size.height*0.3)
    gameUnable:addChild(updateIcon)
    gameUnable.updateIcon = updateIcon

    local spriteBG = cc.Sprite:create("GameRes/GameDownProBG.png")
    spriteBG:setPosition(size.width/2, size.height*0.6)
    gameUnable:addChild(spriteBG)
    
    local bgSize = spriteBG:getContentSize()
    local label = cc.LabelTTF:create("0%", "", 25)
    label:setString("0%")
    label:setPosition(bgSize.width/2, bgSize.height*0.5)
    spriteBG:addChild(label, 10)
    
    local progreSp = cc.Sprite:create("GameRes/GameDownPro.png")
    local progress = cc.ProgressTimer:create(progreSp)
    progress:setAnchorPoint(0, 0)
    spriteBG:addChild(progress)
    progress:setType(cc.PROGRESS_TIMER_TYPE_RADIAL)
    spriteBG.progress = progress
    spriteBG:setVisible(false)
    
    gameUnable.progressBG = spriteBG
    spriteBG.progressLab = label
    
    return gameUnable
end

function GameChoice:setGameActionWgt(game, parentWgt)
    parentWgt:removeAllChildren()
    parentWgt._gameInfo = game
    --按钮动画
    local t_name = GameChoiceIDs[game.gameId]
    ccs.ArmatureDataManager:getInstance():addArmatureFileInfo( "GameRes/"..t_name.."donghua/"..t_name.."0.png", "GameRes/"..t_name.."donghua/"..t_name.."0.plist" , "GameRes/"..t_name.."donghua/"..t_name..".ExportJson" )
    local t_anNiuAnimation = ccs.Armature:create( t_name )
    print( "t_anNiuAnimation  "..tostring(t_anNiuAnimation) )
    t_anNiuAnimation:setPosition(cc.p(parentWgt:getContentSize().width/2 - GameChoiceBtnPos[game.gameId].x, parentWgt:getContentSize().height/2 - GameChoiceBtnPos[game.gameId].y))
    parentWgt:addChild(t_anNiuAnimation)
    t_anNiuAnimation:setTag(ACTION3_TAG)

    --添加灯光 动画
    ccs.ArmatureDataManager:getInstance():addArmatureFileInfo( "GameRes/dingguangdonghua/dingguang0.png", "GameRes/dingguangdonghua/dingguang0.plist" , "GameRes/dingguangdonghua/dingguang.ExportJson" )
    local t_dengAnimation = ccs.Armature:create( "dingguang" )
    t_dengAnimation:setPosition(cc.p(parentWgt:getContentSize().width/2 + GameChoiceDengPos[game.gameId].x, parentWgt:getContentSize().height/2))
    parentWgt:addChild(t_dengAnimation)
    t_dengAnimation:setVisible(false)
    t_dengAnimation:setTag(ACTION2_TAG)

    --手指动画
    ccs.ArmatureDataManager:getInstance():addArmatureFileInfo( "GameRes/shouzhidonghua/shouzhi0.png", "GameRes/shouzhidonghua/shouzhi0.plist" , "GameRes/shouzhidonghua/shouzhi.ExportJson" )
    local t_shouzhiAnimation = ccs.Armature:create( "shouzhi" )
    t_shouzhiAnimation:setPosition(cc.p(parentWgt:getContentSize().width/2 + GameChoiceShouZhiPos[game.gameId].x, parentWgt:getContentSize().height/2 + GameChoiceShouZhiPos[game.gameId].y))
    parentWgt:addChild(t_shouzhiAnimation)
    t_shouzhiAnimation:setVisible(false)
    t_shouzhiAnimation:setTag(ACTION1_TAG)

    --美术要求缩小 百人牛牛和奔驰宝马的大小
    if game.gameId == 108 or game.gameId == 104 then
        t_anNiuAnimation:setScale(0.9)
    end

    --美术要求缩小 跑马
    if game.gameId == 114 then
        t_anNiuAnimation:setScale(0.9, 0.9)
    end
    
    --美术要求缩小 老虎机
    if game.gameId == 233 then
        t_anNiuAnimation:setScale(0.95, 0.95)
    end

    if index == 1 then
        parentWgt._touchTime = 1
        self:setGameBtnAnimation(game.gameId)
    end
end

function GameChoice:onGameDetailTouch(gameWgt)
    if GAME_SUBGAME == true and gameWgt._bDownload == true then
        if gameWgt.isLoading ~= true then
        
            --判断是否还有游戏在下载更新中
            for index, gameWgt in pairs(self._gameBtnList) do
                if gameWgt.isLoading == true then
                    MyToast.new(self._uiLayer, STR_SUBGAME_DOWNLOADIING)
                    return
                end
            end
            
            gameWgt.isLoading = true
            self:setDownProgress(gameWgt, 0)
            gameWgt.gameUpdate:startDownLoad()
        end
        return
    end

    if gameWgt._touchTime == 0 then
        app.musicSound:stopAllSound()
        app.musicSound:playSound(GameChoiceIDs[gameWgt._gameID])
        gameWgt._touchTime = 1
        self:setGameBtnAnimation(gameWgt._gameID)
    elseif gameWgt._touchTime == 1 then
        app.musicSound:stopAllSound()
        --第二次点击开始进入游戏 
        if gameWgt._gameID ~= 998 then
            --网游
            local gameServerl=nil
            local gameServers = app.hallLogic:getGameServersByKindId(gameWgt._gameID)
            self._gameId = gameWgt._gameID

            gameServerl = gameServers[1]
            --判断是否还有游戏在下载更新中
            for index, gameWgt in pairs(self._gameBtnList) do
                if gameWgt.isLoading == true then
                    MyToast.new(self._uiLayer, STR_GAME_DOWNLOADING)
            		return
            	end
            end
            if gameWgt._gameID == 150 then
                self:choiceHouse(gameWgt._gameID, gameServers) 
            elseif gameWgt._gameID == 233 then
                self:choiceHouse(gameWgt._gameID, gameServers) 
            elseif gameWgt._gameID == 614 then
                self:slwhHouse(gameWgt._gameID, gameServers)
            elseif gameWgt._gameID == 130 then
                self:choiceHouseOfFishGame(gameWgt._gameID, gameServers)
            else
                self:begainGame(gameWgt._gameID, gameServerl)
            end
        else
            --单机
            uiManager:runScene("GameWZQ")
            --uiManager:runGameScene("slwh.GameSlwhScene")-- 森林舞会
            -- uiManager:runScene("GameDDZ")
            --self:slwhHouse(gameWgt._gameID, gameServers) 
        end
    end
end

function GameChoice:isSubStrExist(srcStr, subStr)
    local beginIndex, endIndex = string.find(srcStr, subStr)
    if beginIndex and beginIndex >= 1 then
        return true
    end
    return false
end

-- 捕鱼达人开始房间UI
function GameChoice:choiceHouseOfFishGame(gameID, gameServerls)
    if GAME_SUBGAME == true then
        for index, gameConf in pairs(allGameConfs) do
            if gameConf.wKind == gameID then
                cc.LuaLoadChunksFromZIP(gameConf.gameModule)
                break
            end
        end
    end


    local houseUI = ccs.GUIReader:getInstance():widgetFromJsonFile("fishgame2d/ui/FishGameChoice.json")
    self._uiLayer:addChild(houseUI, 128)

    local function exitCallBack(sender, eventType)
        if eventType == ccui.TouchEventType.ended then
            -- app.musicSound:playSound(SOUND_HALL_TOUCH)
            if houseUI then houseUI:removeFromParent() end
        end
    end

    local btnQuit = ccui.Helper:seekWidgetByName(houseUI, "btnQuit")
    btnQuit:addTouchEventListener(exitCallBack)

    self.curent = nil
    self.animations = {}
    self.m_fishDengguang = {}
dump(gameServerls)
    ccs.ArmatureDataManager:getInstance():addArmatureFileInfo("fishgame2d/ui/fish_room_button/fish_room_button.ExportJson")
    ccs.ArmatureDataManager:getInstance():addArmatureFileInfo("GameRes/dingguangdonghua/dingguang0.png", "GameRes/dingguangdonghua/dingguang0.plist", "GameRes/dingguangdonghua/dingguang.ExportJson")
    local rooms = { [1] = {}, [2] = {}, [3] = {} }
    for _, v in ipairs(gameServerls) do
        if string.match(v.szServerName, "悟空闹海百炮房") then
            table.insert(rooms[1], v)
        elseif string.match(v.szServerName, "悟空闹海千炮房") then
            table.insert(rooms[2], v)
        elseif string.match(v.szServerName, "悟空闹海万炮房") then
            table.insert(rooms[3], v)
        else
            table.insert(rooms[1], v)

        end
    end

    local buttonIdex = {}
    local _buttonIdex = {}
    local index = 1
    for i = 1, 3 do
        local button = ccui.Helper:seekWidgetByName(houseUI, "Button_" .. i)
        button:addTouchEventListener(function(sender, eventType)
            if eventType == ccui.TouchEventType.ended then
                if _buttonIdex[i] == nil then return end

                if self.curent == i then
                    self:begainGame(gameID, rooms[_buttonIdex[i]][math.random(#rooms[_buttonIdex[i]])])
                else
                    for k, v in ipairs(self.animations) do
                        if buttonIdex[k] == i then
                            v:getAnimation():play("press_0" .. k)
                            self.m_fishDengguang[k]:setVisible(true)
                        else
                            v:getAnimation():play("stay_0" .. k)
                            self.m_fishDengguang[k]:setVisible(false)
                        end
                    end
                    self.curent = i
                end
            end
        end)

        local animation = ccs.Armature:create("fish_room_button");
        animation:getAnimation():play("stay_0" .. i)
        animation:setTouchEnabled(true)
        animation:pos(1280 / 4 * index + 80 * (index - 2), 360)
        local t_dengAnimation1 = ccs.Armature:create("dingguang")
        t_dengAnimation1:pos(1280 / 4 * index + 80 * (index - 2), 360)
        t_dengAnimation1:setVisible(false)
        t_dengAnimation1:getAnimation():play("dingguang1")

        houseUI:addChild(animation)
        houseUI:addChild(t_dengAnimation1)
        self.animations[i] = animation
        self.m_fishDengguang[i] = t_dengAnimation1

        if #rooms[i] > 0 then
            button:setVisible(true)
            animation:setVisible(true)
            t_dengAnimation1:setVisible(false)

            buttonIdex[i] = index
            _buttonIdex[index] = i
            index = index + 1
        else
            button:setVisible(true)
            animation:setVisible(false)
            t_dengAnimation1:setVisible(false)
        end
    end
end

-- 森林舞会开始房间UI
function GameChoice:slwhHouse(gameID, gameServerls)
    if GAME_SUBGAME == true then
        for index, gameConf in pairs(allGameConfs) do
            if gameConf.wKind == gameID then
                cc.LuaLoadChunksFromZIP(gameConf.gameModule)
                break
            end
        end
    end

    local houseUI = ccs.GUIReader:getInstance():widgetFromJsonFile("GameSlwh/2DUI/slwhBeganUi.json")
    self._uiLayer:addChild(houseUI, 128)
    houseUI:setTag(HOUSEUI_TAG) 

    local bg = houseUI:getChildByName("bgBtn")
    local house1 = bg:getChildByName("tutingBtn")
    local house2 = bg:getChildByName("maotingBtn")
    local house3 = bg:getChildByName("shitingBtn")

    local commonServerInfo = nil
    local vipServerInfo = nil
    local tuhaoServerInfo = nil
    for index,info in ipairs(gameServerls) do
        if self:isSubStrExist(info.szServerName, "新手房")==true then
            commonServerInfo = info
        elseif self:isSubStrExist(info.szServerName, "VIP房")==true then
            vipServerInfo = info
        elseif self:isSubStrExist(info.szServerName, "土豪房")==true then
            tuhaoServerInfo = info
        end
    end

    -- 点击空白背景移除
    local function exitCallBack(sender, eventType)
        if eventType == ccui.TouchEventType.ended then
            -- app.musicSound:playSound(SOUND_HALL_TOUCH)
            if houseUI then houseUI:removeFromParent() end
        end
    end
    bg:addTouchEventListener(exitCallBack)

    --兔厅动画
    ccs.ArmatureDataManager:getInstance():addArmatureFileInfo( "GameSlwh/2DUI/beganUI/tuting0.png", 
        "GameSlwh/2DUI/beganUI/tuting0.plist" , 
        "GameSlwh/2DUI/beganUI/tuting.ExportJson" )
    local t_animation = ccs.Armature:create( "tuting")
    t_animation:getAnimation():play("tuting1")
    t_animation:setPosition(house1:getContentSize().width/2, house1:getContentSize().height/2)
    house1:addChild(t_animation)
    --猫厅动画
    ccs.ArmatureDataManager:getInstance():addArmatureFileInfo( "GameSlwh/2DUI/beganUI/maoti0.png", 
        "GameSlwh/2DUI/beganUI/maoti0.plist" , 
        "GameSlwh/2DUI/beganUI/maoti.ExportJson" )
    ccs.ArmatureDataManager:getInstance():addArmatureFileInfo( "GameSlwh/2DUI/beganUI/maoti1.png", 
        "GameSlwh/2DUI/beganUI/maoti1.plist" , 
        "GameSlwh/2DUI/beganUI/maoti.ExportJson" )
    local t_animation = ccs.Armature:create( "maoti")
    t_animation:getAnimation():play("maoti1")
    t_animation:setPosition(house2:getContentSize().width/2, house2:getContentSize().height/2)
    house2:addChild(t_animation)
    --狮厅动画
    ccs.ArmatureDataManager:getInstance():addArmatureFileInfo( "GameSlwh/2DUI/beganUI/shiti0.png", 
        "GameSlwh/2DUI/beganUI/shiti0.plist" , 
        "GameSlwh/2DUI/beganUI/shiti.ExportJson" )
    ccs.ArmatureDataManager:getInstance():addArmatureFileInfo( "GameSlwh/2DUI/beganUI/shiti1.png", 
        "GameSlwh/2DUI/beganUI/shiti1.plist" , 
        "GameSlwh/2DUI/beganUI/shiti.ExportJson" )
    local t_animation = ccs.Armature:create( "shiti")
    t_animation:getAnimation():play("shiti1")
    t_animation:setPosition(house3:getContentSize().width/2, house3:getContentSize().height/2)
    house3:addChild(t_animation)

    local isnum = 0
    local function house1CallBack(sender, eventType)
        if eventType == ccui.TouchEventType.ended and isnum ~= 1 then
            isnum = 1
            t_dengAnimation1:setVisible(true)
            t_dengAnimation2:setVisible(false)
            t_dengAnimation3:setVisible(false)
            t_dengAnimation1:getAnimation():play("dingguang1")
            return
        end
        if eventType == ccui.TouchEventType.ended and isnum == 1 then
            -- app.musicSound:playSound(SOUND_HALL_TOUCH)
            self:begainGame(gameID, sender.gameserverInfo)
            --uiManager:runGameScene("slwh.SlwhGameScene")-- 森林舞会
            houseUI:removeFromParent()
        end
        cc.UserDefault:getInstance():setFloatForKey("fangjian", 1)
    end
    house1:setVisible(commonServerInfo~=nil)
    house1.gameserverInfo = commonServerInfo
    house1:addTouchEventListener(house1CallBack)

    local function house2CallBack(sender, eventType)
        if eventType == ccui.TouchEventType.ended and isnum ~= 2 then
            isnum = 2
            t_dengAnimation1:setVisible(false)
            t_dengAnimation2:setVisible(true)
            t_dengAnimation3:setVisible(false)
            t_dengAnimation2:getAnimation():play("dingguang1")
            return
        end
        if eventType == ccui.TouchEventType.ended and isnum == 2 then
            -- app.musicSound:playSound(SOUND_HALL_TOUCH)
            self:begainGame(gameID, sender.gameserverInfo)
            --uiManager:runGameScene("slwh.SlwhGameScene")-- 森林舞会
            houseUI:removeFromParent()
        end
        cc.UserDefault:getInstance():setFloatForKey("fangjian", 2)
    end
    house2:setVisible(vipServerInfo~=nil)
    house2.gameserverInfo = vipServerInfo
    house2:addTouchEventListener(house2CallBack)

    local function house3CallBack(sender, eventType)
        if eventType == ccui.TouchEventType.ended and isnum ~= 3 then
            isnum = 3
            t_dengAnimation1:setVisible(false)
            t_dengAnimation2:setVisible(false)
            t_dengAnimation3:setVisible(true)
            t_dengAnimation3:getAnimation():play("dingguang1")
            return
        end
        if eventType == ccui.TouchEventType.ended and isnum == 3 then
            -- app.musicSound:playSound(SOUND_HALL_TOUCH)
            self:begainGame(gameID, sender.gameserverInfo)
            --uiManager:runGameScene("slwh.SlwhGameScene")-- 森林舞会
            houseUI:removeFromParent()
        end
        cc.UserDefault:getInstance():setFloatForKey("fangjian", 3)
    end
    house3:setVisible(tuhaoServerInfo~=nil)
    house3.gameserverInfo = tuhaoServerInfo
    house3:addTouchEventListener(house3CallBack)

    ccs.ArmatureDataManager:getInstance():addArmatureFileInfo( "GameRes/dingguangdonghua/dingguang0.png", "GameRes/dingguangdonghua/dingguang0.plist" , "GameRes/dingguangdonghua/dingguang.ExportJson" )
    t_dengAnimation1 = ccs.Armature:create("dingguang")
    t_dengAnimation1:setVisible(false)
    house3:getParent():addChild(t_dengAnimation1, 100)
    t_dengAnimation1:setPosition(cc.p(house1:getPositionX(), house1:getPositionY()+30))

    t_dengAnimation2 = ccs.Armature:create("dingguang")
    t_dengAnimation2:setVisible(false)
    house3:getParent():addChild(t_dengAnimation2, 100)
    t_dengAnimation2:setPosition(cc.p(house2:getPositionX(), house2:getPositionY()+30))

    t_dengAnimation3 = ccs.Armature:create("dingguang")
    t_dengAnimation3:setVisible(false)
    house3:getParent():addChild(t_dengAnimation3, 100)
    t_dengAnimation3:setPosition(cc.p(house3:getPositionX(), house3:getPositionY()+30))

end

-- 选择房间UI
function GameChoice:choiceHouse(gameID, gameServerls)
	if GAME_SUBGAME == true then
        for index, gameConf in pairs(allGameConfs) do
            if gameConf.wKind == gameID then
                cc.LuaLoadChunksFromZIP(gameConf.gameModule)
                break
            end
        end
    end
    local PublicUI = require("app.object.PublicUI")
    app.musicSound:playMusic(houseMusic[gameID], true)

    local houseUI = PublicUI:getVipHouseUI()
    self._uiLayer:addChild(houseUI, 128)
    houseUI:setTag(HOUSEUI_TAG)    

    local commonServerInfo = nil
    local vipServerInfo = nil
    local tuhaoServerInfo = nil

    for index,info in ipairs(gameServerls) do
        if self:isSubStrExist(info.szServerName, "新手房")==true then
            commonServerInfo = info
        elseif self:isSubStrExist(info.szServerName, "VIP房")==true then
            vipServerInfo = info
        elseif self:isSubStrExist(info.szServerName, "土豪房")==true then
            tuhaoServerInfo = info
        end
    end

    local isnum = 0
    local t_dengAnimation1 = nil
    local t_dengAnimation2 = nil
    local t_dengAnimation3 = nil
    local house1 = houseUI:getChildByName("bg"):getChildByName("1_house") --普通房间
    if gameID==233 then
        house1:loadTextureNormal("HallUI/viphouse/normal"..gameID..".png")
        house1:loadTexturePressed("HallUI/viphouse/normal"..gameID..".png")
        house1:getChildByName("img"):setVisible(false)
    end
    local function house1CallBack(sender, eventType)
        if eventType == ccui.TouchEventType.ended and isnum ~= 1 then
            isnum = 1
            t_dengAnimation1:setVisible(true)
            t_dengAnimation2:setVisible(false)
            t_dengAnimation3:setVisible(false)
            t_dengAnimation1:getAnimation():play("dingguang1")
            return
        end
        if eventType == ccui.TouchEventType.ended and isnum == 1 then
            app.musicSound:playSound(SOUND_HALL_TOUCH)
            self:begainGame(gameID, sender.gameserverInfo)
            houseUI:removeFromParent()
        end
        cc.UserDefault:getInstance():setFloatForKey("fangjian",1)
    end  
    house1:setVisible(commonServerInfo~=nil)
    house1.gameserverInfo = commonServerInfo
    house1:addTouchEventListener(house1CallBack)

    local house2 = houseUI:getChildByName("bg"):getChildByName("2_house") --VIP房间
    local function house2CallBack(sender, eventType)
        if eventType == ccui.TouchEventType.ended and isnum ~= 2 then
            isnum = 2
            t_dengAnimation1:setVisible(false)
            t_dengAnimation2:setVisible(true)
            t_dengAnimation3:setVisible(false)
            t_dengAnimation2:getAnimation():play("dingguang1")
            return
        end
        if eventType == ccui.TouchEventType.ended and isnum == 2 then
            app.musicSound:playSound(SOUND_HALL_TOUCH)
            self:begainGame(gameID, sender.gameserverInfo)
            houseUI:removeFromParent()
        end
        cc.UserDefault:getInstance():setFloatForKey("fangjian",2)
    end 
    if gameID==233 then 
        house2:loadTextureNormal("HallUI/viphouse/viphouse"..gameID..".png")
        house2:loadTexturePressed("HallUI/viphouse/viphouse"..gameID..".png")
        house2:getChildByName("img"):setVisible(false)
    end
    house2:setVisible(vipServerInfo~=nil)
    house2.gameserverInfo = vipServerInfo
    house2:addTouchEventListener(house2CallBack)

    local house3 = houseUI:getChildByName("bg"):getChildByName("3_house") --土豪房间
    local function house3CallBack(sender, eventType)
        if eventType == ccui.TouchEventType.ended and isnum ~= 3 then
            isnum = 3
            t_dengAnimation1:setVisible(false)
            t_dengAnimation2:setVisible(false)
            t_dengAnimation3:setVisible(true)
            t_dengAnimation3:getAnimation():play("dingguang1")
            return
        end
        if eventType == ccui.TouchEventType.ended and isnum == 3 then
            app.musicSound:playSound(SOUND_HALL_TOUCH)
            self:begainGame(gameID, sender.gameserverInfo)
            houseUI:removeFromParent()
        end
        cc.UserDefault:getInstance():setFloatForKey("fangjian",3)
    end
    if gameID==233 then
        house3:loadTextureNormal("HallUI/viphouse/tuhaohouse"..gameID..".png")
        house3:loadTexturePressed("HallUI/viphouse/tuhaohouse"..gameID..".png")
        house3:getChildByName("img"):setVisible(false)
    end
    house3:setVisible(tuhaoServerInfo~=nil)
    house3.gameserverInfo = tuhaoServerInfo
    house3:addTouchEventListener(house3CallBack)

    -- 关闭
    local function closeCallBack(sender, eventType)
        if eventType == ccui.TouchEventType.ended then
            app.musicSound:playSound(SOUND_HALL_TOUCH)
            app.musicSound:playMusic(DATINGBG, true)
            houseUI:removeFromParent()
        end
    end
    local closeBtn = houseUI:getChildByName("bg")
    closeBtn:addTouchEventListener(closeCallBack)

    ccs.ArmatureDataManager:getInstance():addArmatureFileInfo( "GameRes/dingguangdonghua/dingguang0.png", "GameRes/dingguangdonghua/dingguang0.plist" , "GameRes/dingguangdonghua/dingguang.ExportJson" )
    t_dengAnimation1 = ccs.Armature:create("dingguang")
    t_dengAnimation1:setVisible(false)
    house3:getParent():addChild(t_dengAnimation1, 100)
    t_dengAnimation1:setPosition(cc.p(house1:getPositionX(), house1:getPositionY()+30))

    t_dengAnimation2 = ccs.Armature:create("dingguang")
    t_dengAnimation2:setVisible(false)
    house3:getParent():addChild(t_dengAnimation2, 100)
    t_dengAnimation2:setPosition(cc.p(house2:getPositionX(), house2:getPositionY()+30))

    t_dengAnimation3 = ccs.Armature:create("dingguang")
    t_dengAnimation3:setVisible(false)
    house3:getParent():addChild(t_dengAnimation3, 100)
    t_dengAnimation3:setPosition(cc.p(house3:getPositionX(), house3:getPositionY()+30))
end

-- 进入游戏
function GameChoice:begainGame(gameID, gameServerl)
    for index, gameConf in pairs(allGameConfs) do
        if gameConf.wKind == gameID then
            print( "eventGameLoginSuccess  quick:"..gameID)
            if GAME_SUBGAME == true then
                cc.LuaLoadChunksFromZIP(gameConf.gameModule)
            end
            
            import(gameConf.entrance)
            app.table:setClientVersion(gameConf.dwClientVersion)
            app.eventDispather:dispatherEvent("GameLevelStartGame", gameServerl)

            cc.UserDefault:getInstance():setIntegerForKey("lastGameId", gameID)
            app.hallLogic.isplayGame = true
            break
        end
    end
end

function GameChoice:getGameWidget(game)
    local widgtWidth = 320
    local widgtHeight = 500
    local gameId = game.gameId
    local gameType = ccui.Widget:create()
    gameType:setTouchEnabled(true)
    gameType:setContentSize(cc.size(widgtWidth, widgtHeight))
    gameType._gameInfo = game
    gameType._gameID = gameId
    gameType._touchTime = 0
    gameType._bDownload = false
    
    self._gameBtnList[gameId] = gameType  
    
    for ix, gameConf in pairs(allGameConfs) do
        if gameConf.wKind == gameId then
            gameType._gameConf = gameConf
            if GAME_SUBGAME == true and cc.FileUtils:getInstance():isFileExist(gameConf.gameModule) == false then
                local wgt = self:getGameDownloadWgt(game)
                wgt:setPosition(widgtWidth/2, widgtHeight/2)
                wgt:setTag(GAME_DOWNLOAD_TAG)
                gameType:addChild(wgt, 100)                
                gameType._bDownload = true
            end
            break
        end
    end
    
    if gameType._bDownload == false then
        self:setGameActionWgt(game, gameType)
    end
    
    if gameType._gameConf and GAME_SUBGAME == true then
        gameType.gameUpdate = GameUpdate.new()

        local t_channel = getChannelName()
        local t_version = getVersionName()
        local t_sign = crypto.md5("HotUpdateConfig"..t_channel..t_version.."tjyjerge!@#sddf$^$h234^^DsSEG")
        local param = "action=HotUpdateConfig&channel="..t_channel.."&version="..t_version.."&sign="..t_sign
        local url = GETCONFIG_UEL.."?"..param
        if url_type == 2 then
            url = GETCONFIG_UEL_BAK.."?"..param
        end

        self.request = network.createHTTPRequest(function(event)
            if self and self.request then
                local request = event.request
                if event.name == "completed" and request:getResponseStatusCode() == 200 then
                    self.dataRecv = request:getResponseData()
                    --公告走马灯
                    local update_URL = self.dataRecv
                    print( update_URL )
                    gameType.gameUpdate:setUpdateData(gameType._gameConf.gameKey, update_URL, self)
                elseif event.name == "failed" then
                    self.request = nil
                end
            end        
        end, url, "POST")
        if self.request then
            self.request:setTimeout(waittime or 10)
            self.request:start()
        end
    end
    
    local updatePath = device.writablePath.."upd/"..gameType._gameConf.gameKey.."/"
    local oldPaths = cc.FileUtils:getInstance():getSearchPaths()

    local newPaths = {}
    table.insert(newPaths, updatePath)
    for index, path in ipairs(oldPaths) do
        table.insert(newPaths, path)
    end

    cc.FileUtils:getInstance():setSearchPaths(newPaths);
    
    gameType:addTouchEventListener(function(sender, type)
        if type == ccui.TouchEventType.ended then
            self:onGameDetailTouch(sender)
        end
    end)

    gameType:setScale(0.9)
    
    return gameType
end

function GameChoice:parseWebConf()
    if app.hallLogic.webGameConf == nil then
        return
    end

    local webConf = app.hallLogic.webGameConf

    app.hallLogic._vipConf = webConf.vipMoneyConfs
    self:setBtnsVisibleWithConfig(webConf)
    
    app.eventDispather:dispatherEvent(eventUpdateVipConf, app.hallLogic._vipConf)

    for k,v in pairs( webConf.hgallMarquees ) do
        self:setZoumaLabel(v)
    end

    self._gameOpenInfo = webConf.gameOpen
    self._gonggaourl = webConf.GongGao
    if self._gonggaourl and self._gonggaourl.URL ~= false then
        showUrl(self._gonggaourl.URL)
    end

    if webConf.gameInOrder then
        self._gameIdTab = webConf.gameInOrder --游戏ID排序
    end
    
    if self._gameOpenInfo and #self._gameOpenInfo > 0 then
        local channle = getChannelName()
        local version = getVersionName()
        local games = nil
        local defaultGames = nil
        if channle and version then
            for index, item in ipairs(self._gameOpenInfo) do
                if item.channle == "default" then
                    defaultGames = item.games
                end
                
                if item.channle == channle then --and item.version == version 
                    games = item.games
                    break
                end
            end
        end
        
        if games == nil then
            games = defaultGames
        end
        
        if games then
            self._gameOpenConf = games
            
            if VERSION_DEBYG == true then
                 -- dump(games)
            end                 
        end             
    end
end

function GameChoice:updateGamesByListView()
    if not self._updateRoom then
        if app.GETGAMESHUNXU then
            GameShunXu = app.GETGAMESHUNXU
        end
        self._updateRoom = true
        self._gameListByListView:removeAllChildren(true)
        self._gameBtnList = {}
        local games = app.hallLogic:getGames() --服务器开启的游戏
        local t_games = {} --需要显示的游戏
        local gameIdString = ""
        for k, v in pairs(GameShunXu) do --和本地配置进行对比
            for index, game in pairs(games) do 
                if v == game.gameId then
                    if not (self._gameOpenConf and self._gameOpenConf[game.gameId] == false) then
                        table.insert(t_games, game)
                    end                   
                    break
                end
            end
        end

        local t_wuziqi = {["gameId"] = 998, ["gameName"] = "五子棋"}
        if not (self._gameOpenConf and self._gameOpenConf[t_wuziqi.gameId] == false) then
            table.insert(t_games, t_wuziqi)
        end

        -- 把字符串换成ID
        local IdString = cc.UserDefault:getInstance():getStringForKey("IdString")
        local stringTab = {}
        if IdString and IdString ~= "" then
            gameIdString = IdString
            stringTab = lua_string_split(gameIdString, ",")
            if next(self._gameIdTab) ~= nil then 
               stringTab = self._gameIdTab
            end
            -- 替换游戏顺序
            local tempGameTab = {}
            for k, v in pairs(stringTab) do
                for k1, v1 in pairs(t_games) do
                    if tonumber(v) == v1.gameId then
                        table.insert(tempGameTab, v1)
                        break
                    end
                end
            end
            --把没有放进tempGameTab的游戏插入后面
            for k, v in pairs(t_games) do
                for k1, v1 in pairs(tempGameTab) do
                    if v1.gameId == v.gameId then
                        break
                    end
                    if k1 == #tempGameTab then
                        table.insert(tempGameTab, v)
                    end
                end
            end
            if tempGameTab and #tempGameTab > 0 then
                t_games = tempGameTab
            end                
        end

        -- 把上次玩的游戏排到第一个
        local lastGameTd = cc.UserDefault:getInstance():getIntegerForKey("lastGameId")
        if lastGameTd then
            local tempTab = {}
            for k, v in pairs( t_games ) do
                if v.gameId == lastGameTd and lastGameTd ~= 998 then
                    table.remove(t_games, k)
                    table.insert(t_games, 1, v)
                end
                if v.gameId == 998 then
                    table.remove(t_games, k)
                    table.insert(t_games, #t_games+1, v)
                end
            end
        end

        --审核使用
        if self._isshenhe then
            t_games = {{["gameId"] = 998, ["gameName"] = "五子棋"}}
        end

        -- 转换游戏ID成字符串存起来
        gameIdString = ""
        for k, v in pairs(t_games) do
            gameIdString = tostring(gameIdString..v.gameId..",")
        end
        cc.UserDefault:getInstance():setStringForKey("IdString", gameIdString)
        for index, game in pairs(t_games) do 
            local filePath = string.format("GameRes/Game_%d.png", game.gameId)
            local bExist = true--cc.FileUtils:getInstance():isFileExist(filePath)
            if bExist then
                if VERSION_DEBYG == true then
                    printf("新建游戏gameId:%d", game.gameId)
                end
                local gameWgt = self:getGameWidget(game)
                self._gameListByListView:insertCustomItem(gameWgt, index-1)               
            end 
        end
        self:setPageBtn()
    end
end

function GameChoice:updateGames()
    self:updateGamesByListView()
    --[[
    if not self._updateRoom then
        self._updateRoom = true

        self._gameListView:removeAllChildren(true)
        
        self._gameBtnList = {}
        local games = app.hallLogic:getGames()
        local addIndex = 0
        --dump(games)
        local t_games = {}
        for k,v in pairs(GameShunXu) do
            for index, game in pairs(games) do
                if v == game.gameId then
                    table.insert(t_games,game)
                    break
                end
            end
        end
        
        local t_wuziqi = {}
        t_wuziqi["gameId"] = 998
        t_wuziqi["gameName"] = "五子棋"
        table.insert(t_games, t_wuziqi)
        dump(t_games)
        local t_gameNumber = 0
        for index, game in pairs(t_games) do  --table.insert(games, {["gameId"]=gameServer.wKindID, ["gameName"] = gameServer.szServerName})
            local filePath = string.format("GameRes/Game_%d.png", game.gameId)
            local bExist = true--cc.FileUtils:getInstance():isFileExist(filePath)
            if bExist then
                t_gameNumber = t_gameNumber + 1

                self._nowPage = math.floor(t_gameNumber / 4)
                local moduleWgt = ccui.Widget:create()

                --            local gameType = ccui.Button:create(string.format("GameRes/Game_%d.png", game.gameId),
                --                                                string.format("GameRes/Game_%d.png", game.gameId),
                --                                                string.format("GameRes/Game_%d.png", game.gameId))
                local gameType = ccui.Widget:create()
                gameType:setTouchEnabled(true)
                gameType:setContentSize(cc.size(424, 561))
                gameType._gameInfo = game
                gameType._gameID = game.gameId
                gameType._touchTime = 0
                self._gameBtnList[game.gameId] = gameType

                --按钮动画
                local t_name = GameChoiceIDs[game.gameId]
                ccs.ArmatureDataManager:getInstance():addArmatureFileInfo( "GameRes/"..t_name.."donghua/"..t_name.."0.png", "GameRes/"..t_name.."donghua/"..t_name.."0.plist" , "GameRes/"..t_name.."donghua/"..t_name..".ExportJson" )
                local t_anNiuAnimation = ccs.Armature:create( t_name )
                print( "t_anNiuAnimation  "..tostring(t_anNiuAnimation) )
                t_anNiuAnimation:setPosition(cc.p(gameType:getContentSize().width/2 - GameChoiceBtnPos[game.gameId].x, gameType:getContentSize().height/2 - GameChoiceBtnPos[game.gameId].y))
                gameType:addChild(t_anNiuAnimation)
                t_anNiuAnimation:setTag(10000002)

                --添加灯光 动画
                ccs.ArmatureDataManager:getInstance():addArmatureFileInfo( "GameRes/dingguangdonghua/dingguang0.png", "GameRes/dingguangdonghua/dingguang0.plist" , "GameRes/dingguangdonghua/dingguang.ExportJson" )
                local t_dengAnimation = ccs.Armature:create( "dingguang" )
                t_dengAnimation:setPosition(cc.p(gameType:getContentSize().width/2 + GameChoiceDengPos[game.gameId].x, gameType:getContentSize().height/2))
                gameType:addChild(t_dengAnimation)
                t_dengAnimation:setVisible(false)
                t_dengAnimation:setTag(10000001)

                --手指动画
                ccs.ArmatureDataManager:getInstance():addArmatureFileInfo( "GameRes/shouzhidonghua/shouzhi0.png", "GameRes/shouzhidonghua/shouzhi0.plist" , "GameRes/shouzhidonghua/shouzhi.ExportJson" )
                local t_shouzhiAnimation = ccs.Armature:create( "shouzhi" )
                t_shouzhiAnimation:setPosition(cc.p(gameType:getContentSize().width/2 + GameChoiceShouZhiPos[game.gameId].x, gameType:getContentSize().height/2 + GameChoiceShouZhiPos[game.gameId].y))
                gameType:addChild(t_shouzhiAnimation)
                t_shouzhiAnimation:setVisible(false)
                t_shouzhiAnimation:setTag(10000000)

                --美术要求缩小 百人牛牛和奔驰宝马的大小
                if game.gameId == 108 or game.gameId == 104 then
                    t_anNiuAnimation:setScale(0.9)
                end
                
                --美术要求缩小 跑马
                if game.gameId == 114 then
                    t_anNiuAnimation:setScale(0.9, 0.85)
                end

                --            moduleWgt:setContentSize(cc.size(426, 481))
                --            gameType:setAnchorPoint(0.5, 0.5)
                --            moduleWgt:addChild(gameType)
                --            gameType:setPosition( cc.p(moduleWgt:getContentSize().width/2, moduleWgt:getContentSize().height/2) )

                if index == 1 then
                    gameType._touchTime = 1
                    self:setGameBtnAnimation(game.gameId)
                end
                print( "self._nowPage  "..self._nowPage.." game.gameId "..game.gameId )
                self._gameListView:addWidgetToPage(gameType, self._nowPage, true)
                gameType:setPosition(212 + 424 * ( t_gameNumber - 1 - 3 * self._nowPage ), self._gameListView:getContentSize().height / 2)
                gameType:addTouchEventListener(function(sender, type)
                    if type == ccui.TouchEventType.ended then                       
                        if sender._touchTime == 0 then
                            app.musicSound:stopAllSound()
                            app.musicSound:playSound(GameChoiceIDs[sender._gameID])
                            sender._touchTime = 1
                            self:setGameBtnAnimation(sender._gameID)
                        elseif sender._touchTime == 1 then
                            sender._touchTime = 0
                            app.musicSound:stopAllSound()
                            --第二次点击开始进入游戏 
                            if sender._gameID ~= 998 then
                                --网游
                                local gameServerl=nil
                                local gameServers = app.hallLogic:getGameServersByKindId(sender._gameID)
                                self._gameId = sender._gameID
    
                                gameServerl = gameServers[1]
    
    
                                --判断启动哪个游戏
                                for index, gameConf in pairs(allGameConfs) do
                                    if gameConf.wKind == sender._gameID then
                                        print( "eventGameLoginSuccess  quick:"..sender._gameID)
                                        import(gameConf.entrance)
                                        app.table:setClientVersion(gameConf.dwClientVersion)
                                        app.eventDispather:dispatherEvent("GameLevelStartGame", gameServerl)
    
                                        break
                                    end
                                end
                            else
                                --单机
                                uiManager:runScene("GameWZQ")
                            end
                        end
                    end
                end)

                addIndex = addIndex + 1
            end
        end

        self._allPage = math.floor(t_gameNumber / 4)
        self._nowPage = 0
        self._gameListView:scrollToPage(self._nowPage)

        self:setPageBtn()
    end
    ]]
end

function GameChoice:setPageBtn()
    --[[
    if self._nowPage < self._allPage then
        self._nextPageBtn:setVisible(true)
    else
        self._nextPageBtn:setVisible(false)
    end

    if self._nowPage == 0 then
        self._lastPageBtn:setVisible(false)
    else
        self._lastPageBtn:setVisible(true)
    end
    ]]
end

--根据游戏ID 设置按钮动画
function GameChoice:setGameBtnAnimation( gameId )
    print( "GameChoice:setGameBtnAnimation:"..gameId )
    for k,v in pairs(self._gameBtnList) do
        if v._bDownload ~= true then
            if v._gameInfo and v._gameInfo.gameId == gameId then
--				Log(GameChoiceIDs[v._gameID]);
                v:getChildByTag(ACTION3_TAG):getAnimation():play(GameChoiceIDs[v._gameID].."1")
                v:getChildByTag(ACTION2_TAG):setVisible(true)
                v:getChildByTag(ACTION2_TAG):getAnimation():play("dingguang1")
    
                v:getChildByTag(ACTION1_TAG):setVisible(true)
                v:getChildByTag(ACTION1_TAG):getAnimation():play("shouzhi")
            else
                v._touchTime = 0
                v:getChildByTag(ACTION3_TAG):getAnimation():gotoAndPause(1)
    
                v:getChildByTag(ACTION2_TAG):setVisible(false)
                v:getChildByTag(ACTION1_TAG):setVisible(false)
            end
        end
    end
end

function GameChoice:updateUserInfo()    
    if app.hallLogic:isBinded() then
        ccui.Helper:seekWidgetByName(self._layerInventory, "tishi"):setVisible(false)
    end
    local userInfo = app.hallLogic._loginSuccessInfo
    self.myvipNum = app.hallLogic._loginSuccessInfo.obMemberInfo.cbMemberOrder
    --    print( "userInfo.szNickName "..userInfo.szNickName )
    self._name:setString(userInfo.szNickName)
    self._coin:setString(userInfo.lUserScore)
    self._dianjuan:setString(app.hallLogic._leJuan)

    --设置VIP等级
    local vipLevel = ccui.Helper:seekWidgetByName(self._topLayer, "vipBtn"):getChildByName("vipLevel")
    vipLevel:setString(self.myvipNum)

    --设置玩家等级
    local t_levelBg = ccui.Helper:seekWidgetByName(self._topLayer, "LevelBg")
    local t_levelLabel = t_levelBg:getChildByName("Level")
    local t_jingDuTiao = t_levelBg:getChildByName("levelJingDuTiao")
    t_levelLabel:setString(app.hallLogic:getPlayerLevel())
    t_jingDuTiao:setPercent( app.hallLogic:getPlayerExpPer() )

end

function GameChoice:onEnter()
    self._loading = false
    GameChoice.super.onEnter(self)
    self:updateGames()
    self:updateUserInfo()
    self:setRenShu()
    app.eventDispather:addListenerEvent(eventLoginSrvListFinish, self, function(data)
        self:updateGames()
    end)
    
    --设置总人数
    app.eventDispather:addListenerEvent(eventLoginRenShu, self, function(data)
        self:setRenShu()
    end)

    app.eventDispather:addListenerEvent(eventLoginGameChoice, self, function(data)
        self:updateUserInfo()
        self:setBaoXiangText()
    end)
    
    app.eventDispather:addListenerEvent(eventBaoXiangLingQu, self, function(data)
        self:updateUserInfo()
        self:setBaoXiangText()
    end)

    -- 领取救济金结果
    app.eventDispather:addListenerEvent(eventJiuJiJinLingQu, self, function(data)
        self:updateUserInfo()
        self:addBenefits(data)
    end)

    --更新实物信息
    app.eventDispather:addListenerEvent(eventShiWuMessage, self, function(data)
        self:updateShiWuMessage()
    end)

    -- 是否显示首冲
    app.eventDispather:addListenerEvent(eventFirstRec, self, function(data)
        local seq = transition.sequence({
            cc.DelayTime:create(0.4),
            cc.CallFunc:create(function ()
                self:isShowFirstRec()
                self:isShowRotaryUiLayer()
            end)
            })
        self:runAction(seq)
    end)

    app.eventDispather:addListenerEvent("GameLevelStartGame", self, function(data)
        print( "GameLevelStartGame" )
        if VERSION_DEBYG == true then
            print( "ArenaChoice   GameLevelStartGame" )
            -- dump(data)
        end
        if data then
            --连接服务器
            self:addLoading()
            app.table:setServerInfo(data)
            --app.hallLogic:connectGameServer(data)
            --app.hallLogic:connectGameGateWay()
            app.hallLogic:sendQueryGameGameWay()
        end
    end)

    app.eventDispather:addListenerEvent(eventLoginGameGateWay, self, function(data)
        if data and data.cbActiveServer==1 then
            app.hallLogic:connectGameGateWay()
        else
            self:delLoading()
            MyToast.new(self._uiLayer, STR_NOGAMEGATEINFO)
        end
    end)
    
    app.eventDispather:addListenerEvent(eventLoginGatewayNull, self, function(data)
        self:delLoading()
        MyToast.new(self._uiLayer, STR_NOGAMEGATEINFO)
    end)

    app.eventDispather:addListenerEvent(eventGameNetConnected, self, function(data)
        print( "eventGameNetConnected" )
        if VERSION_DEBYG == true then
            print( "連接成功  準備登陸quick" )
        end
        if data == false then
            self:delLoading()
            self:showTip(false, STR_GAMES_NET_CONN_FAILED)
            return
        end
        local password = cc.UserDefault:getInstance():getStringForKey("password")
        app.table:loginByID(password, self._gameId)
    end)
    
    app.eventDispather:addListenerEvent(eventGameNetClosed, self, function(data)
        print("---------gameCHoice network close")
        self:delLoading()
        self:showTip(false, STR_GAMES_NET_CONN_FAILED)
    end)

    app.eventDispather:addListenerEvent(eventGameLoginSuccess, self, function(data)
        --判断启动哪个游戏
        print( "eventGameLoginSuccess  登陆成功quick" )
        if VERSION_DEBYG == true then
            --print( "eventGameLoginSuccess  登陆成功quick" )
        end
        for index, gameConf in pairs(allGameConfs) do
            if gameConf.wKind == app.table._serverInfo.wKindID then
                app.hallLogic:setLastSelGame(self._gameId)       --设置最后的场次
                if GAME_SUBGAME == true then
                    cc.LuaLoadChunksFromZIP(gameConf.gameModule)
                end
                
                import(gameConf.entrance)
                uiManager:runGameScene(gameConf.module)
                break
            end
        end
    end)
    
    app.eventDispather:addListenerEvent(eventBuyTrumpetRet, self, function(data)
        if data.cbResult == 0 then
            MyToast.new(self._uiLayer, STR_TRUMPET_BUY_OK)
        else
            MyToast.new(self._uiLayer, STR_TRUMPET_BUY_FAILED)
        end
    end)
    
    app.eventDispather:addListenerEvent(eventTrumpetCount, self, function(data)
        self.trumpetCount:setString(app.hallLogic._trumpetCount)
    end)
    
    app.eventDispather:addListenerEvent(eventUserRedEvevelopeInfo, self, function(data)
        local baoxiangLayer = self._uiLayer:getChildByTag(BAOXIANG_TAG)
        if baoxiangLayer ~= nil then
            baoxiangLayer:setXiangziInfo(data)
        end
    end)
    
    app.eventDispather:addListenerEvent(eventBuyRedEnvelope, self, function(data)
        if data and data.cbResult == 0 then
        	MyToast.new(self._uiLayer, STR_REDENVELOPE_BUYOK)
        else
            MyToast.new(self._uiLayer, data.szMessage)
        end
    end)
    
    app.eventDispather:addListenerEvent(eventGiveRedEnvelope, self, function(data)
        if data and data.cbResult == 0 then
            MyToast.new(self._uiLayer, STR_REDENVELOPE_GIVEOK)
            app.musicSound:playSound(BAOXIANG_USE)
        else
            MyToast.new(self._uiLayer, data.szMessage)
        end
    end)
    
    app.eventDispather:addListenerEvent(eventUseRedEnvelope, self, function(data)
        if data and data.cbResult == 0 then
            MyToast.new(self._uiLayer, STR_REDENVELOPE_USEOK)
            app.musicSound:playSound(BAOXIANG_USE)
        else
            MyToast.new(self._uiLayer, data.szMessage)
        end
    end)
    
    app.eventDispather:addListenerEvent(eventLoginUserCoinUpdate, self, function(data)
        self:updateUserInfo()
    end)
    
    app.eventDispather:addListenerEvent(eventUiShowShop, self, function(data)
        self:showStore()
    end)
    
    self.trumpetCount:setString(app.hallLogic._trumpetCount)
    app.hallLogic:sendUpdateUserCoin()
    
    if cc.UserDefault:getInstance():getBoolForKey("isStore")==true then
        cc.UserDefault:getInstance():setBoolForKey("isStore", false)
        self:showStore()
    end
end

function GameChoice:messageNotice()
    checkOrderID()
end

function GameChoice:showStore()
    if app.hallLogic._BtnsConfig ~= nil then
        for k,v in pairs(app.hallLogic._BtnsConfig) do            
            if v == "closeStore" then 
                return
            end
        end 
    end
    if self._store then
        self._store:show()
    else
        -- --旧商城
        -- self._store = Store.new()
        -- self._uiLayer:addChild(self._store, 100)
        -- self._store:show()
        --新商城
        local NewStore = require("app.scenes.NewStore")
        local store = NewStore.new(1)
        self._uiLayer:addChild(store, 100)
    end     
end

--提示界面
function GameChoice:tipRemindLayer()
    local tempTab = NewStoreConfig.tipconfig
    local limitTab = NewStoreConfig.limitTab
    if 100000000 < limitTab.limit then
        return
    end
    
    cc.UserDefault:getInstance():setIntegerForKey("storeTip", 1)
    local node = ccs.GUIReader:getInstance():widgetFromJsonFile("HallUI/PublicTipUi.json")  
    self._uiLayer:addChild(node)
    local layer = node:getChildByName("bg")

    local lable = layer:getChildByName("contentLable")
    lable:setString(limitTab.label)
    lable:setColor(cc.c3b(255,237,134))

    local sureBtn = layer:getChildByName("sureBtn")
    sureBtn:loadTextures(tempTab.sureBtn1name, tempTab.sureBtn2name, "")
    sureBtn:addTouchEventListener(function(sender, type)
            if type == ccui.TouchEventType.ended then
                app.musicSound:playSound(SOUND_HALL_TOUCH)  
                if node then node:removeFromParent() end
                local NewStore = require("app.scenes.NewStore")
                local store = NewStore.new(2)
                self._uiLayer:addChild(store, 100)
            end
        end)

    local cancelBtn = layer:getChildByName("cancelBtn")
    cancelBtn:loadTextures(tempTab.cancelBtn1name, tempTab.cancelBtn2name, "")
    cancelBtn:addTouchEventListener(function(sender, type)
            if type == ccui.TouchEventType.ended then
                app.musicSound:playSound(SOUND_HALL_TOUCH)  
                if node then node:removeFromParent() end
            end
        end)
end

function GameChoice:onLoadingCancle()
    app.table:closeNet()
end

function GameChoice:onExit()
    GameChoice.super.onExit(self)
    app.eventDispather:delListenerEvent(self)
    DataManager:getInstance():setChoiceLayer(nil)
    if self.request then
    	-- self.request:cancel()
        self.request = nil
    end
end

function GameChoice:onHallNetClose(data)
    self:delLoading()
    MyToast.new(self._uiLayer, STR_LOGIN_CONNECT_FAILED)
end

function GameChoice:onHallNetConnected(data)
end

function GameChoice:onLoginSrvLoginSuccess(data)
    GameChoice.super.onLoginSrvLoginSuccess(self, data)
    self:updateUserInfo()
end

function GameChoice:getGameWgtByGameKey(gameKey)
    for index, item in pairs(self._gameBtnList) do
        if item._gameConf and item._gameConf.gameKey == gameKey then
			return item
		end
	end
end

function GameChoice:onConfFailed(gameKey)
    -- printf("获取配置信息失败 gameKey：%s", gameKey)
	local gameWgt = self:getGameWgtByGameKey(gameKey)
    gameWgt.isLoading = false
    
end

function GameChoice:getConfOK(gameKey, downlist, removelist)
    -- printf("获取配置信息成功 gameKey：%s", gameKey)
    local gameWgt = self:getGameWgtByGameKey(gameKey)
    
    if #downlist > 0 then
        self:setGameWgtUpdate(gameWgt)  --设置为可更新状态
    else
        gameWgt._bDownload = false
        self:setGameActionWgt(gameWgt._gameInfo, gameWgt)  --设置为游戏可用状态
    end
end

--更新游戏下载进度
function GameChoice:downProgress(gameKey, progress)
    --printf("下载进度更新 gameKey：%s", gameKey)
    local gameWgt = self:getGameWgtByGameKey(gameKey)
    self:setDownProgress(gameWgt, progress)  
end

function GameChoice:downError(gameKey)
    printf("下载错误 gameKey：%s", gameKey)
    local gameWgt = self:getGameWgtByGameKey(gameKey)
    gameWgt.isLoading = false
    
    MyToast.new(self._uiLayer, string.format(STR_GAME_DOWNLOAD_ERROR, gameWgt._gameConf.gameName))
        
    local downWgt = gameWgt:getChildByTag(GAME_DOWNLOAD_TAG)
    if downWgt then
        downWgt.progressBG:setVisible(false)
    end
end

function GameChoice:downFinish(gameKey)
    printf("下载完成 gameKey：%s", gameKey)
    local gameWgt = self:getGameWgtByGameKey(gameKey)
    gameWgt.isLoading = false
    gameWgt._bDownload = false
    
    MyToast.new(self._uiLayer, string.format(STR_GAME_DOWNLOAD_FINISH, gameWgt._gameConf.gameName))
    
    gameWgt:removeAllChildren()
    self:setGameActionWgt(gameWgt._gameInfo, gameWgt)  --设置为游戏可用状态
end

return GameChoice