
local FirstPay = class("FirstPay",function()
    return display.newLayer()
end)


FirstPay.__index = FirstPay
FirstPay._uiLayer= nil
FirstPay._widget = nil
FirstPay._sceneTitle = nil


function FirstPay:ctor()
    FirstPay.super.ctor(self)
    self._uiLayer = cc.Layer:create()
    self:addChild(self._uiLayer)

    local layerInventory = ccs.GUIReader:getInstance():widgetFromJsonFile("HallUI/FirstPay.json")
    self._uiLayer:addChild(layerInventory)

    --返回按钮点击事件
    local function returnCallBack(sender, eventType)
        if eventType == ccui.TouchEventType.ended then
            self:setVisible(false)
        end
    end

    --返回按钮
    local returnBtn = layerInventory:getChildByName("closeBtn")
    returnBtn:addTouchEventListener(returnCallBack)
    
    
    
    
    --立即领取
    local function quickGetCallBack(sender, eventType)
        if eventType == ccui.TouchEventType.ended then
            --TODO
        end
    end

    --立即领取按钮
    local quickGetBtn = layerInventory:getChildByName("quickGetBtn")
    quickGetBtn:addTouchEventListener(quickGetCallBack)
    

end

return FirstPay