
Loading = class("Loading", function()
    return display.newScene("Loading")
end)


Loading.__index = Loading
Loading._uiLayer= nil


function Loading:ctor()

    self._uiLayer = cc.Layer:create()
    self:addChild(self._uiLayer)

    local layerInventory = ccs.GUIReader:getInstance():widgetFromJsonFile("fishgame2d/ui/fishGame2D_5.json")
    self._uiLayer:addChild(layerInventory)
    
    local bg = display.newSprite("fishgame2d/ui/room_bg.png")
    bg:pos(640, 360)
    self._uiLayer:addChild(bg)

    local loadbg = display.newSprite("fishgame2d/ui/progress_02.png")
    loadbg:pos(640, 86)
    self._uiLayer:addChild(loadbg)

    --animation --
    local path = "fish_loading_enter"
    ccs.ArmatureDataManager:getInstance():addArmatureFileInfo("fishgame2d/ui/" .. path .. "/" .. path .. ".ExportJson")
    local layerBackground = display.newLayer():addTo(self)
    layerBackground:setCascadeOpacityEnabled(true)
    self.ani01 = ccs.Armature:create(path)
    self.ani01:align(display.CENTER, 640,360)
    self.ani01:addTo(layerBackground)
    self.ani01:getAnimation():play("ani_01")
    self.ani03 = ccs.Armature:create(path)
    self.ani03:align(display.CENTER, 640,360)
    self.ani03:addTo(layerBackground)
    self.ani03:getAnimation():play("ani_03")


    local x, y = loadbg:getPosition()
    self.startX = x - loadbg:getContentSize().width / 2
    self.ani03:setPosition(self.startX, y + 50)

    --取消按钮点击事件
    local function cancleCallback( sender, eventType )
        if eventType == ccui.TouchEventType.ended then
            custom.PlatUtil:closeServer(CLIENT_GAME)
            app.musicSound:playSound(SOUND_HALL_TOUCH)
            app.eventDispather:dispatherEvent(eventRemoveLoading)
            self:removeFromParent()
        end
    end
    
    --取消按钮
    self.cancleBtn = ccui.Button:create("fishgame2d/ui/btn_quit.png","fishgame2d/ui/btn_quit_pressed.png")
    self.cancleBtn:setPosition(72,653)
    self._uiLayer:addChild(self.cancleBtn,9999)
    self.cancleBtn:addTouchEventListener(cancleCallback)
    self.cancleBtn:setVisible(false)

    self.label = cc.LabelTTF:create("网络不畅，请退出重试！", "", 30)
    self.label:setPosition(640,120)
    self._uiLayer:addChild(self.label, 9999)
    self.label:setVisible(false)
    
    local showCancleBtn = function()
        self.cancleBtn:setVisible(true)
        self.label:setVisible(true)
        self:closeScheduler()
    end

    local scheduler = cc.Director:getInstance():getScheduler()
    self._sendHeartScheduler = scheduler:scheduleScriptFunc(showCancleBtn, 5.0, false)  --时间记录
end

function Loading:onExit()
    self:closeScheduler()
end

function Loading:closeScheduler()
    if self._sendHeartScheduler then
        local scheduler = cc.Director:getInstance():getScheduler()
        scheduler:unscheduleScriptEntry(self._sendHeartScheduler)
        self._sendHeartScheduler = nil
    end
end

return Loading