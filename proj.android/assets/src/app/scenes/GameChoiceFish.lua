require "lfs"
require("sdk.manager.NewStoreConfig")
require("ServerData")

local FishGameEventRank = require("app.fishgame.FishGameEventRank")
local FishGameEventResult = require("app.fishgame.FishGameEventResult")
local FishGameBoardCast = require("app.fishgame.FishGameBoardCast")

local DataManager = require("app.uiManager.DataManager")

local GameChoiceFish = class("GameChoiceFish", import(".BaseScene"))

function GameChoiceFish:ctor()
	self:initServerInfo() -- 初始化服务器列表 模拟
    self.TargetPlatform = cc.Application:getInstance():getTargetPlatform() --设备系统
    self._isChinaIP = cc.UserDefault:getInstance():getIntegerForKey("IsChinaIP")

    GameChoiceFish.super.ctor(self)
    self._uiLayer = cc.Layer:create()
    self:addChild(self._uiLayer)
end

function GameChoiceFish:initServerInfo()
	self.gameServerls = {}
    self.gameServerls = getServerData()
end

function GameChoiceFish:onEnter()
    GameChoiceFish.super.onEnter(self)
    self:createEventButton()

	-- 监听按钮点击事件
	app.eventDispather:addListenerEvent("GameLevelStartGame", self, function(data)
        if VERSION_DEBYG == true then
            print( "GameLevelStartGame" )
        end
        if data then
            print("开始链接.......")

            self:addLoading()
            app.table:setServerInfo(data)
            app.hallLogic:connectGameServer(data)
        end
    end)

	 --监听服务器连接成功的事件
	 app.eventDispather:addListenerEvent(eventGameNetConnected, self, function(data)
        if VERSION_DEBYG == true then
            print( "連接成功  準備登陸quick" )
        end
        if data == false then
            self:delLoading()
            self:showTip(false, STR_GAMES_NET_CONN_FAILED)
            return
        end

        app.table:loginByID(610)
    end)

    app.eventDispather:addListenerEvent(eventGameLoginSuccess, self, function(data)
        --判断启动哪个游戏
        if VERSION_DEBYG == true then
            print( "eventGameLoginSuccess  登陆成功" )
        end

        for index, gameConf in pairs(allGameConfs) do
            if gameConf.wKind == 130 then
                app.hallLogic:setLastSelGame(self._gameId)       --设置最后的场次
                if GAME_SUBGAME == true then
                    cc.LuaLoadChunksFromZIP(gameConf.gameModule)
                end
                
                import(gameConf.entrance)
                uiManager:runGameScene(gameConf.module)
                break
            end
        end
    end)

	self._gameId = 130
	local gameServers = app.hallLogic:getGameServersByKindId(self._gameId)
    self:choiceHouseOfFishGame(self._gameId, self.gameServerls)
    
end

function GameChoiceFish:createEventButton()
     -- 活动公告
    function boardcastCallBack(sender, eventType)
        if eventType == ccui.TouchEventType.began then
            sender:setScale(1.05)
        end

        if eventType == ccui.TouchEventType.moved or eventType == ccui.TouchEventType.canceled then
            sender:setScale(1)
        end
        if eventType == ccui.TouchEventType.ended then
            sender:setScale(1)
            local boardcastLayer = FishGameBoardCast.new():addTo(self,999)
        end 
    end  

    local boardcast = ccui.Button:create("fishgame2d/ui/btn_eventboardcast.png","fishgame2d/ui/btn_eventboardcast.png")
    if boardcast then
        boardcast:addTouchEventListener(boardcastCallBack)
        boardcast:setPosition(1050,650)
        self:addChild(boardcast)
    end
end

-- 捕鱼达人开始房间UI
function GameChoiceFish:choiceHouseOfFishGame(gameID, gameServerls)
    if GAME_SUBGAME == true then
        for index, gameConf in pairs(allGameConfs) do
            if gameConf.wKind == gameID then
                cc.LuaLoadChunksFromZIP(gameConf.gameModule)
                break
            end
        end
    end

    local houseUI = ccs.GUIReader:getInstance():widgetFromJsonFile("fishgame2d/ui/FishGameChoice.json")
    self._uiLayer:addChild(houseUI, 128)

    local function exitCallBack(sender, eventType)
        if eventType == ccui.TouchEventType.ended then
            backToModeScene()
        end
    end

    local btnQuit = ccui.Helper:seekWidgetByName(houseUI, "btnQuit")
    btnQuit:addTouchEventListener(exitCallBack)

    ccs.ArmatureDataManager:getInstance():addArmatureFileInfo("fishgame2d/ui/fish_room_button/fish_room_button.ExportJson")
    local rooms = {}

    for k, v in pairs(gameServerls) do
        if VERSION_DEBYG == true then
            print("---------"..v.szServerName)
        end
        table.insert(rooms, v)
    end

    local scrollView = ccui.Helper:seekWidgetByName(houseUI, "ScrollView_RoomList")
    local buttonDemo = ccui.Helper:seekWidgetByName(scrollView, "Button_1")
    local roomId = 0
    local roomCnt = {0,0,0,0}
    local names = {"roomNameB", "roomNameQ", "roomNameW", "roomNameWW"}
    if #rooms > 3 then
        scrollView:setInnerContainerSize(cc.size(#rooms * 380, 360))
    end

    
    for i = 1, #rooms do
        local posx = i * 380 - 190
        local posy = 360

        if string.match(rooms[i].szServerName, "五万炮房") then
            roomId = 4
        elseif string.match(rooms[i].szServerName, "万炮房") then
            roomId = 3
        elseif string.match(rooms[i].szServerName, "千炮房") then
            roomId = 2
        elseif string.match(rooms[i].szServerName, "百炮房") then
            roomId = 1
        end
        roomCnt[roomId] = roomCnt[roomId]+1

        local button = buttonDemo:clone()
        button.index = i
        button.roomId = roomId
        button:pos(posx, posy)
        scrollView:addChild(button)

        button:addTouchEventListener(function(sender, eventType)
            if eventType == ccui.TouchEventType.ended then
                self:begainGame(gameID, rooms[sender.index])
                app.FishRoomID = sender.roomId - 1
            end
        end)

        local animation = ccs.Armature:create("fish_room_button");
        animation:getAnimation():play("stay_0" .. roomId)
        animation:setTouchEnabled(true)
        animation:pos(posx, posy)
        scrollView:addChild(animation)

        local nameStr = "fishgame2d/ui/"..names[roomId]..tostring(roomCnt[roomId])..".png"
        local roomName = display.newSprite(nameStr)
        if roomName then
            roomName:pos(posx, 150)
            scrollView:addChild(roomName)
        end
    end
end

-- 进入游戏
function GameChoiceFish:begainGame(gameID, gameServerl)
    dump(gameServerl)
    for index, gameConf in pairs(allGameConfs) do
        if gameConf.wKind == gameID then
            if GAME_SUBGAME == true then
                cc.LuaLoadChunksFromZIP(gameConf.gameModule)
            end
            
            import(gameConf.entrance)
            app.table:setClientVersion(gameConf.dwClientVersion)
            app.eventDispather:dispatherEvent("GameLevelStartGame", gameServerl)

            cc.UserDefault:getInstance():setIntegerForKey("lastGameId", gameID)
            app.hallLogic.isplayGame = true
            break
        end
    end
end


return GameChoiceFish
