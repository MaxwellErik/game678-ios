CoinListObject = class("CoinListObject", function ()
    return ccui.Widget:create()
end)

CoinListObject._layerInventory = nil
CoinListObject._mingciImg = nil   --排名等级
CoinListObject._mingciNum = nil   --排名等级
CoinListObject._coin = nil       --金币数量
CoinListObject._headIcon = nil   --头像
CoinListObject._name = nil       --昵称
CoinListObject._btn = nil       --点击按钮



--等级，金币数量，头像，名字,点击事件，玩家显示位置
function CoinListObject:ctor(rankIcon, coin,headIcon,name,confirm,playerInfo)
    print("进来了")
    local layerInventory = ccs.GUIReader:getInstance():widgetFromJsonFile("HallUI/coinListObject.json")
    self:addChild(layerInventory)
    self._layerInventory = layerInventory
    local img = cc.Sprite:create()

    img:setTexture("HallUI/gameLayer/coinlistBg.png")
    img:setAnchorPoint(cc.p(0,0))
    img:setPosition(0,0)
    self:addChild(img)  
    self:setContentSize(cc.size(137,134))
    
    self._coin = layerInventory:getChildByName("coin")
    self._coin:setString(coin)
    self._name = layerInventory:getChildByName("name")
    self._name:setString(name)
    
    self._headIcon = layerInventory:getChildByName("icon")   
    local randIcon=math.random(1,30)
    self._headIcon:loadTexture("headIcon/"..randIcon..".png")
    

    self._mingciImg = layerInventory:getChildByName("mingciImg")   
    self._mingciNum = layerInventory:getChildByName("mingciNum")   
    if rankIcon==1 then
        self._mingciImg:loadTexture("HallUI/Ranking/jiangbei1.png")
        self._mingciImg:setVisible(true)
        self._mingciNum:setVisible(false)
    elseif rankIcon==2 then
        self._mingciImg:loadTexture("HallUI/Ranking/jiangbei2.png")
        self._mingciImg:setVisible(true)
        self._mingciNum:setVisible(false)
    elseif rankIcon==3 then
        self._mingciImg:loadTexture("HallUI/Ranking/jiangbei3.png") 
        self._mingciImg:setVisible(true)    
        self._mingciNum:setVisible(false)  
    else
        self._mingciImg:setVisible(false)
        self._mingciNum:setVisible(true)
        self._mingciNum:setString(rankIcon)
    end
    
    self._confirmCallBack=confirm
    
     --点击打开玩家列表事件
    local function confirmBtnCallBack(sender, eventType)
        if eventType == ccui.TouchEventType.ended then
            --self:setVisible(false)
            if self._confirmCallBack==nil then
                printf("onfirm is nil")
                else
                self._confirmCallBack()
                local pp=self:convertToWorldSpace(cc.p(0,0))
               
                playerInfo:setPositionY(pp.y+60)
            end

        end
    end  
    
    self._btn = layerInventory:getChildByName("btn")        --点击按钮
    self._btn:addTouchEventListener(confirmBtnCallBack)


end

return CoinListObject