--
-- Author: YL
-- Date: 2016-01-07 10:39:19
--
local PublicUI = class("PublicUI")

function PublicUI:ctor()
end

--点击后弹领取
function PublicUI:isJumpRemind()
    local getCount = cc.UserDefault:getInstance():getIntegerForKey("getCount")
    if getCount > 0 then
        self:getBeneLayer()
    else
        self:isGotoPay()
    end
end

--收到赠送红包后的提示
function PublicUI:getRedPackageTip(tab)
    local runLayer = display.getRunningScene():getUILayer()
    MyToast.new(runLayer, tab.szFromNick.."赠送你"..tab.dwCount.."个"..tab.dwRMBValue.."元宝箱")
end

--提示领取救济金页面
function PublicUI:getBeneLayer()
	local layer = cc.Layer:create()
    local runLayer = display.getRunningScene():getUILayer()
    if not runLayer:getChildByTag(122) then
    	local bene_ui = ccs.GUIReader:getInstance():widgetFromJsonFile("HallUI/BenefitsUi.json")
        local function sureCallBack(sender, eventType)
            if eventType == ccui.TouchEventType.ended then
                local temp = cc.UserDefault:getInstance():getIntegerForKey("beneguide")
                if temp or temp ~= 0 then
                    cc.UserDefault:getInstance():setIntegerForKey("beneguide", 3)
                end
                app.musicSound:playSound(SOUND_HALL_TOUCH)
                layer:removeFromParent()
                local seq = transition.sequence({
                    cc.DelayTime:create(0.1),
                    cc.CallFunc:create(function()
                        app.table:sendStandup()
                        uiManager:runScene("GameChoice")
                        custom.PlatUtil:closeServer(CLIENT_GAME)
                    end)
                    })
                runLayer:runAction(seq)
            end
        end
        local sureBtn = bene_ui:getChildByName("benefitsbg"):getChildByName("sure_Btn")
        sureBtn:addTouchEventListener(sureCallBack)

        local function exitCallBack(sender, eventType)
            if eventType == ccui.TouchEventType.ended then
                app.musicSound:playSound(SOUND_HALL_TOUCH)
                layer:removeFromParent()
            end
        end
        local exitBtn = bene_ui:getChildByName("benefitsbg"):getChildByName("exit_Btn")
        exitBtn:addTouchEventListener(exitCallBack)

        local lab_1 = bene_ui:getChildByName("benefitsbg"):getChildByName("Label_remind")
        local lab_2 = bene_ui:getChildByName("benefitsbg"):getChildByName("Label_time")
        local lab_3 = bene_ui:getChildByName("benefitsbg"):getChildByName("Label_Tips")
        lab_1:setString("金币不足，是否去主界面领取")
        lab_3:setString("（每天最多可以领取3次救济金）")

        local schedulerEntry = nil
        local str_time = 10
        lab_2:setString(tostring("("..str_time.."S)"))
        local function step()
            str_time = str_time-1    
            if str_time <= 0 then
                bene_ui:removeFromParent()
            else
                lab_2:setString(tostring("("..str_time.."S)"))
            end
        end 
        local seq = transition.sequence({
            cc.DelayTime:create(1),
            cc.CallFunc:create(step)
            })
        bene_ui:runAction(cc.RepeatForever:create(seq))
        layer:addChild(bene_ui, 1)

        runLayer:addChild(layer, 999)
    end
end

function PublicUI:getVipHouseUI()
	local viphouse_ui = ccs.GUIReader:getInstance():widgetFromJsonFile("HallUI/VipHouse.json")
    local house1 = viphouse_ui:getChildByName("bg"):getChildByName("1_house") --普通房间
    local house2 = viphouse_ui:getChildByName("bg"):getChildByName("2_house") --VIP房间
    local house3 = viphouse_ui:getChildByName("bg"):getChildByName("3_house") --土豪房间

    local parTab = {}
    for i = 1, 12 do
	ccs.ArmatureDataManager:getInstance():addArmatureFileInfo( "GameSelect/shanxingxing/shanxingxing0.png", 
	    "GameSelect/shanxingxing/shanxingxing0.plist" , 
	    "GameSelect/shanxingxing/shanxingxing.ExportJson" )
	local t_animation = ccs.Armature:create( "shanxingxing")
	t_animation:getAnimation():play("shangxingxingdonghua")
	table.insert(parTab, t_animation)
    end

    local poaTab = {cc.p(80, 410), cc.p(80, 190), cc.p(195, 20), cc.p(280, 120)}
    local num = 1
    for k, v in pairs( poaTab ) do
    	parTab[num]:setPosition(v)
    	house1:addChild(parTab[num])
    	num = num+1

    	parTab[num]:setPosition(v)
    	house2:addChild(parTab[num])
    	num = num+1

    	parTab[num]:setPosition(v)
    	house3:addChild(parTab[num])
    	num = num+1
    end

	return viphouse_ui
end

-- 提示金币不足是否去充值
function PublicUI:isGotoPay()
    local layer = cc.Layer:create()
    local runLayer = display.getRunningScene():getUILayer()
    if not runLayer:getChildByTag(122) then
        local uiLayer = ccs.GUIReader:getInstance():widgetFromJsonFile("HallUI/NoCoin.json") 
        layer:addChild(uiLayer)     
        local okBtn = ccui.Helper:seekWidgetByName(uiLayer, "okBtn")
        local cancleBtn = ccui.Helper:seekWidgetByName(uiLayer, "quXiaoBtn")
        
        cancleBtn:addTouchEventListener(function(sender, t_type)
            if t_type == ccui.TouchEventType.ended then
                app.musicSound:playSound(SOUND_HALL_TOUCH)
                layer:removeFromParent()
            end
        end)
        
        okBtn:addTouchEventListener(function(sender, t_type)
            if t_type == ccui.TouchEventType.ended then
                app.musicSound:playSound(SOUND_HALL_TOUCH)
                layer:removeFromParent()
                cc.UserDefault:getInstance():setBoolForKey("isStore", true)
                app.table:sendStandup()
                uiManager:runScene("GameChoice")
                custom.PlatUtil:closeServer(CLIENT_GAME)
            end
        end)
    
        runLayer:addChild(layer, 122)
    end
end










return PublicUI