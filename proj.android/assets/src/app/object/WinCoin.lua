WinCoin = class("WinCoin", function()
    return ccui.Widget:new()
end)

WinCoin._uiFrame = nil      --图片


function WinCoin:ctor( owner, isBig, isWin )             
    if isWin then        
        --金币动画
        ccs.ArmatureDataManager:getInstance():addArmatureFileInfo( "CoinAnimation/WinAnimation/jinbi0.png", 
            "CoinAnimation/WinAnimation/jinbi0.plist" , 
            "CoinAnimation/WinAnimation/jinbi.ExportJson" )            
        local t_coinAnimation = ccs.Armature:create( "jinbi" )                            
        self:addChild(t_coinAnimation)
        
        local t_num = math.random(1,5)
        if isBig and t_num == 4 then
            t_num = 3
        end
        t_coinAnimation:getAnimation():play("jb"..t_num)    
        
        --设置初始位置
        local t_starX = math.random(0, 1280)
        self:setPosition(t_starX,750)
        
        local t_time = math.random(0,20)    --延迟时间
        
        --设置结束位置
        local t_endX = math.random(0, 1280)
        local t_endY = 0
        if isBig then
            t_endX = math.random( 400, 800 )
            t_endY = 300
        end
        
        --设置下落动画
        local function moveEnd()
--            owner:removeChild(self)
            self:removeFromParent()
        end
        local callfunc = cc.CallFunc:create(moveEnd)
        self:runAction(cc.Sequence:create(cc.MoveBy:create(t_time / 10, cc.p(0, 0)), cc.MoveTo:create(1.0, cc.p(t_endX, t_endY)), callfunc))
        
        local t_scale = 0.8
        if isBig then
            t_scale = 3
        end
        --放大动画
        self:setScale(0.1)
        self:runAction(cc.Sequence:create(cc.MoveBy:create(t_time / 10, cc.p(0, 0)), cc.ScaleTo:create(1.0, t_scale)))
    else        
        --拳头动画
        ccs.ArmatureDataManager:getInstance():addArmatureFileInfo( "CoinAnimation/shudonghua/shu0.png", 
            "CoinAnimation/shudonghua/shu0.plist" , 
            "CoinAnimation/shudonghua/shu.ExportJson" )            
        local t_coinAnimation = ccs.Armature:create( "shu" )                            
        self:addChild(t_coinAnimation)
        t_coinAnimation:getAnimation():play("shu")
        t_coinAnimation:setPosition(640, 360)
        t_coinAnimation:setVisible(false)
        
        local function moveEnd()
--            owner:removeChild(self)
            self:removeFromParent()
        end
        local callfunc = cc.CallFunc:create(moveEnd)
        self:runAction(cc.Sequence:create(cc.MoveBy:create(1.5, cc.p(0, 0)),  callfunc))
    end
end

return WinCoin