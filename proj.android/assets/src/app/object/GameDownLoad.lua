GameDownLoad = class("GameDownLoad")

local function hex(s)
    s=string.gsub(s,"(.)",function (x) return string.format("%02X",string.byte(x)) end)
    return s
end

local function readFile(path)
    local file = io.open(path, "rb")
    if file then
        local content = file:read("*all")
        io.close(file)
        return content
    end
    return nil
end

local function removeFile(path)
    os.remove(path)
end

local function checkFile(fileName, cryptoCode)
    --print("checkFile:", fileName)
    --print("cryptoCode:", cryptoCode)

    if not io.exists(fileName) then
        --print("checkFile:"..fileName.." not exists")
        return false
    end

    local data=readFile(fileName)
    if data==nil then
        --print("checkFile:"..fileName.." filedata is nil")
        return false
    end

    if cryptoCode==nil then
        return true
    end

    local ms = crypto.md5(hex(data))
    --print("file cryptoCode:", ms)
    if ms==cryptoCode then
        return true
    end

    return false
end

local function checkDirOK( path )
    if path == nil then
        return false
    end

    local ret = lfs.chdir(path)
    if ret==nil then
        return false
    else
        return true
    end
end

local function findLastStr(srcStr, subStr)
    if srcStr ~= nil and srcStr ~= "" then
        local startIndex, endIndex = string.find(srcStr,subStr)
        if startIndex == nil then
            return 0
        else
            return endIndex + findLastStr(string.sub(srcStr, endIndex+1, -1), subStr)
        end
    else
        return 0
    end
end

local function createDir(path)
    path = string.sub(path, 1, -2)
    local lastIndex = findLastStr(path, "/")
    local subPath = string.sub(path, 1, lastIndex)
    local dirName = string.sub(path, lastIndex, string.len(path))

    local pathinfo = io.pathinfo(subPath)
    if checkDirOK(pathinfo.dirname)==false then
        createDir(subPath)
    end

    lfs.mkdir(path)
end

function os.rmdir(path)
    --print("os.rmdir:", path)
    --print(io.exists(path))
    if checkDirOK(path)==true then
        local function _rmdir(path)
            local iter, dir_obj = lfs.dir(path)
            while true do
                local dir = iter(dir_obj)
                if dir == nil then
                    break
                end

                if dir ~= "." and dir ~= ".." then
                    local curDir = path..dir
                    local mode = lfs.attributes(curDir, "mode") 
                    if mode == "directory" then
                        _rmdir(curDir.."/")
                    elseif mode == "file" then
                        --print('delete file:'..curDir)
                        os.remove(curDir)
                    end
                end
            end
            lfs.rmdir(path)
            return succ
        end
        _rmdir(path)
    end
    return true
end

function io.writefileCheckDir(path, data)
    local pathinfo = io.pathinfo(path)
    if checkDirOK(pathinfo.dirname) then
        io.writefile(path, data)
        return true
    else
        if device.platform == "windows" then
            local newStr = string.gsub(pathinfo.dirname, "/", "\\")
            os.execute("mkdir "..newStr)
        else
            createDir(pathinfo.dirname)
        end

        if checkDirOK(pathinfo.dirname) then
            io.writefile(path, data)
            return true
        else
            return false
        end
    end
end

function GameDownLoad:ctor()
    self._gameKey = nil
    self._callDownPro = nil
    self._callDownError = nil
    self._callDownFinish = nil
    self._downloadList = nil
    self._downloadIndex = 0
    self._dwonloadFileCount = 0
end

function GameDownLoad:setDownData(gameKey, downloadList, serverUrl, callDownPro, callDownError, callDownFinish)
    self._callDownPro = callDownPro
    self._callDownError = callDownError
    self._callDownFinish = callDownFinish
    self._downloadList = downloadList
    self._bEndProcess = false
    self._downloadIndex = 0
    self._dwonloadFileCount = #self._downloadList
    self._serverUrl = serverUrl
    self.requestCount = 0
    self.path = device.writablePath.."upd/"..gameKey.."/"
    
    self._downFileTotalByte = 0
    self._downFileCurByte = 0
    
    self:startDownLoad()
end

function GameDownLoad:startDownLoad()
    --printf("开始下载时间：%d", os.time())
    self._startTime = os.time()
    self._updateOK = true
    self:reqNextFile()
    self:downLoop()
end

function GameDownLoad:reqNextFile()
    self._downFileTotalByte = 0
    self._downFileCurByte = 0
    self.dataRecv = nil
    self._downloadIndex = self._downloadIndex+1
    self._downFileTotalByte = 0
    self._downFileCurByte = 0
    
    self.curDownloadFile = self._downloadList[self._downloadIndex]
    if self.curDownloadFile and self.curDownloadFile.name then       
        self:requestFromServer(self.curDownloadFile.name)
        return
    end
    
    if self._downloadIndex <= #self._downloadList then
        --print("self._downloadIndex <= #self._downloadList")
        self._updateOK = false
    end
    
    self:endProcess()
end

function GameDownLoad:updateProgressPercent()
    local percent = 0
    local filePercent = 0
    
    if self._downFileTotalByte and self._downFileCurByte and self._downFileTotalByte > 0 and self._downFileCurByte <= self._downFileTotalByte  then
        filePercent = self._downFileCurByte *100 / self._downFileTotalByte
    end
    
    if self._dwonloadFileCount > 0 then
        percent = (self._downloadIndex-1)*100/self._dwonloadFileCount + filePercent/self._dwonloadFileCount
    end
        
    percent = math.ceil(percent)
    
    if self._callDownPro then
        self._callDownPro(self._gameKey, percent)
    end
end
  
function GameDownLoad:onEnterFrame(dt)
    self:updateProgressPercent()
    if self.dataRecv then
        local fn = self.path..self._downloadList[self._downloadIndex].name
        io.writefileCheckDir(fn, self.dataRecv)
        self:reqNextFile()
    end
    
    if self._bEndProcess == true then
    	self:endProcess()
    end
end

function GameDownLoad:downLoop()
    local scheduler = cc.Director:getInstance():getScheduler()
    self.schedulerEntry = scheduler:scheduleScriptFunc(function() self:onEnterFrame(0) end, 0, false)
end

function GameDownLoad:endProcess()
    if self.schedulerEntry then
        local scheduler = cc.Director:getInstance():getScheduler()
        scheduler:unscheduleScriptEntry(self.schedulerEntry) 
    end    
    
    if self._updateOK == true then
        --print("下载完成")
        --printf("完成下载时间：%d", os.time())
        self._endTime = os.time()
        --printf("下载消耗时间:%d", self._endTime - self._startTime)
        self._callDownFinish(self._gameKey)
    else
        --print("下载失败")
        self._callDownError(self._gameKey)
    end
end

function GameDownLoad:requestFromServer(filename, waittime)
    local url = self._serverUrl..filename
    --printf('下载文件:%s', url)
    self.requestCount = self.requestCount + 1
    local index = self.requestCount
    local request = network.createHTTPRequest(function(event)
        self:onResponse(event, index)
    end, url, "GET")
    if request then
        request:setTimeout(9999)
        request:start()
    else
        self._updateOK = false
        --printf('下载文件失败:%s', url)
        self:endProcess()
    end
end

function GameDownLoad:onResponse(event, index, dumpResponse)
    local request = event.request
    
    self._downFileTotalByte = event.total
    self._downFileCurByte = event.dltotal
    
    if event.name == "completed" then
        self.dataRecv = request:getResponseData()
    elseif event.name == "failed" then
        self._updateOK = false
        self._bEndProcess = true
        --printf('下载文件失败onResponse:%s', self._serverUrl..self._downloadList[self._downloadIndex].name)
    end
end

return GameDownLoad