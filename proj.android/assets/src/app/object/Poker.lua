
Poker = class("Poker", function()
    return ccui.Widget:new()
end)

Poker._uiFrame=nil      --图片
Poker._isFront=nil      --正反(true正面，false反面)
Poker._color=nil        --花色("h"黑,"x"红,"m"樱,"f"方,"w"王)
Poker._point=nil        --点数(1-13花色，14小王，15大王)


--参数一，正反，参数二，花色，参数三，点数
function Poker:ctor(isFront,flowerColor,num)
    self._uiFrame=cc.Sprite:create()
    self:addChild(self._uiFrame)
    self:setContent(isFront,flowerColor,num)
end

function Poker:setFront(isFront)
    self._isFront=isFront
    if isFront then
        if self._point>=14 then
            self._uiFrame:setTexture("poker/".."w"..self._point..".png")
        else
            self._uiFrame:setTexture("poker/"..self._color..self._point..".png")
        end

    else
        self._uiFrame:setTexture("poker/bei.png")
    end
end

--参数一，正反，参数二，花色，参数三，点数
function Poker:setContent(isFront,flowerColor,num) 
    self._isFront=isFront
    self._color=flowerColor
    self._point=num
    if isFront then
        if self._point>=14 then
            self._uiFrame:setTexture("poker/".."w"..self._point..".png")
        else
            self._uiFrame:setTexture("poker/"..self._color..self._point..".png")
        end

    else
        self._uiFrame:setTexture("poker/bei.png")
    end

end

--将扑克翻转 time翻转用的时间
function Poker:reversal(time,flowerColor,num)
    if flowerColor~=nil then
        self._color=flowerColor
    end
    if num~=nil then
        self._point=num
    end

    local function unReversal()
        if self._isFront then
            self._uiFrame:setTexture("poker/bei.png")
            self._isFront=false
        else
            self._uiFrame:setTexture("poker/"..self._color..self._point..".png")
            self._uiFrame:setFlippedX(true)
            self._isFront=true
        end
    end

    local callfunc = cc.CallFunc:create(unReversal)
    self._uiFrame:runAction(cc.Sequence:create(cc.RotateTo:create(time/2,0,90),callfunc,cc.RotateTo:create(time/2,0,180)))
end

return Poker
