Bead = class("Bead", function()
    return ccui.Widget:new()
end)

Bead._uiFrame = nil      --图片
Bead._animationNumber = 0      --动画图片的张数
Bead._Id = nil            --宝石ID
Bead._cache = nil
Bead._finish = true       --动画播放完毕
Bead._action = nil

function Bead:ctor( a_id, a_num )       
--    print( "id :   "..a_id.."/gem"..a_id.."_1.png" )
    if a_id == "ball" then
        --小球动画
        ccs.ArmatureDataManager:getInstance():addArmatureFileInfo( "lhdb/xiaoqiudonghua/xiaoqiu0.png", 
            "lhdb/xiaoqiudonghua/xiaoqiu0.plist" , 
            "lhdb/xiaoqiudonghua/xiaoqiu.ExportJson" )            
        local t_animation = ccs.Armature:create( "xiaoqiu" )                        
        t_animation:setPosition(cc.p(0, 0))
        self:addChild(t_animation)
        t_animation:getAnimation():play("Animation1")
    else
        local t_cache = cc.SpriteFrameCache:getInstance()
        
        t_cache:addSpriteFrames( "lhdb/duobao0.plist", "lhdb/duobao0.png" )        
        
        self._cache = t_cache
    
        --print( "id :   "..a_id.."/gem"..a_id.."_1.png" )
        if a_id == "zuantou" then
            self._uiFrame = cc.Sprite:createWithSpriteFrame(t_cache:getSpriteFrame(a_id.."/"..a_id.."_1.png"))
        elseif a_id == "ball" then
            self._uiFrame = cc.Sprite:createWithSpriteFrame(t_cache:getSpriteFrame(a_id.."/"..a_id.."_1.png"))
        else
            self._uiFrame = cc.Sprite:createWithSpriteFrame(t_cache:getSpriteFrame(a_id.."/gem"..a_id.."_1.png"))
        end
            
        self._Id = a_id
        --print( "self._Id "..self._Id )
                  
        
        local t_time = math.random(0,10)    --延迟时间
        --print( "t_time   "..t_time )
        --self._uiFrame:runAction(cc.RepeatForever:create(cc.Animate:create(animation)))
        
        local function yanci()
            --print("yanci~~")
            --创建帧动画图片队列
            local animFrames = {}
            for i=2, a_num do
                local str = ""
                local frame = nil
                if a_id == "zuantou" then
                    str = "zuantou/zuantou_"..i..".png"
                elseif a_id == "ball" then
                    str = "ball/ball_"..i..".png"                                
                else        
                    str = a_id.."/gem"..a_id.."_"..i..".png"            
                end  
                frame = t_cache:getSpriteFrame(str)          
                animFrames[i] = frame
            end
    
    
            --创建动画，设置播放间隔
            local animation = cc.Animation:createWithSpriteFrames(animFrames,0.05,1)
            self._uiFrame:runAction(cc.RepeatForever:create(cc.Animate:create(animation)))
        end
        
        local callfunc = cc.CallFunc:create(yanci)
        self._uiFrame:runAction(cc.Sequence:create(cc.MoveBy:create(t_time/10, cc.p(0, 0)), callfunc))   
    
        self:addChild( self._uiFrame )
    end
end

--下落动画
function Bead:downAnimation( a_stop )
    self._finish = false

    local function animationEnd()
        --print("下落完成")
        self._finish = true
    end
    
    local callfunc = cc.CallFunc:create(animationEnd)
    
    local t_y = self:getPositionY()
    local t_v = math.random(700,900)
    local t_time = (t_y - a_stop.y) / t_v
    
    self._action = self:runAction(cc.Sequence:create(cc.MoveTo:create(t_time,cc.p(a_stop.x,a_stop.y)),callfunc))        
end

--消除动画
function Bead:clearAnimation( a_pos, parent )
    self._finish = false
    local function jumpEnd()
        self._finish = true
        parent:removeChild(self, true)
    end

    local callfunc = cc.CallFunc:create(jumpEnd)
    
    local t_x = math.random(-400, 400)
    self:runAction(cc.Sequence:create(cc.JumpBy:create(1, cc.p( t_x, -200 ), 500, 1)
                                     ,callfunc))
    
    --self._action = self:runAction(cc.Sequence:create(cc.MoveTo:create(0.75,cc.p(a_pos.x,a_pos.y)),callfunc))
end

function Bead:getContentSize()
    return self._uiFrame:getContentSize()
end

--连线动画
function Bead:lineAnimation()

    self._uiFrame:stopAllActions()

    self._finish = false
    local function lineEnd()
        --print("爆炸动画结束")
        self._finish = true
        self._uiFrame:setVisible(false)
    end

    local callfunc = cc.CallFunc:create(lineEnd)    

    local animFrames = {}
    for i=1, 6 do 
        local str = "xiaoguo/xiaoguo"..i..".png"
        local frame = self._cache:getSpriteFrame(str)
        animFrames[i] = frame
    end


    --创建动画，设置播放间隔    
    local animation = cc.Animation:createWithSpriteFrames(animFrames,0.1,1)
    --self._action = self._uiFrame:runAction(cc.RepeatForever:create(cc.Animate:create(animation)))    
    self._uiFrame:runAction(cc.Sequence:create(cc.Repeat:create(cc.Animate:create(animation),1),callfunc))    
    --print("播放爆炸动画")
end

function Bead:isAnimationEnd()
    return self._finish
--    if self._action then
--        return self._action:isDone()
--    end
--    return true
end

return Bead