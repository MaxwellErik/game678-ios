
BaoXiangBox = class("BaoXiangBox",function()
    return display:newScene(sceneName)
end)

BaoXiangBox._lingquBg=nil               --领取
BaoXiangBox._baoxiangBg=nil             --宝箱按钮
BaoXiangBox._lejuan=nil                 --乐卷        
BaoXiangBox._jinbi=nil                  --金币
BaoXiangBox._baoXiangNum=nil            --道具种类 1乐卷，2金币
BaoXiangBox._baoXiangText=nil           --道具的数量
BaoXiangBox._armaturet=nil              --宝箱动画


function BaoXiangBox:ctor()  
    self._uiLayer = cc.Layer:create()
    self:addChild(self._uiLayer)

    local layerInventory = ccs.GUIReader:getInstance():widgetFromJsonFile("HallUI/BaoXiangBox.json")
    self._uiLayer:addChild(layerInventory)

    self._lingquBg=layerInventory:getChildByName("lingquBg")
    self._baoxiangBg=layerInventory:getChildByName("baoxiangBg")
    self._lejuan=self._lingquBg:getChildByName("lejuan")
    self._jinbi=self._lingquBg:getChildByName("jinbi")
    
    ccs.ArmatureDataManager:getInstance():addArmatureFileInfo( "BaoXiang/baoxiang0.png", "BaoXiang/baoxiang0.plist" , "BaoXiang/baoxiang.ExportJson" )
    self._armaturet = ccs.Armature:create( "baoxiang")
    self._armaturet:getAnimation():play("dongzuo1")
    self._armaturet:setPosition(cc.p(420,280))
    self._baoxiangBg:addChild(self._armaturet)
    
    
    self._lingquBg:setPosition(cc.p(-1500,-1500))
    self._baoxiangBg:setPosition(cc.p(-1500,-1500))
    
    --确定事件
    local function unReversal1()
        print("领取")
        self:lingQu()
    end
    local function kaiqiBtnCallBack(sender, eventType)
        if eventType == ccui.TouchEventType.ended then
            app.musicSound:playSound(SOUND_HALL_TOUCH)
            --self._kaiqiBtn:setTouchEnabled(false)                    
            self:runAction(cc.CallFunc:create(unReversal1))
        end
    end
    --确定按钮
    self._kaiqiBtn = self._baoxiangBg:getChildByName("kaiqiBtn")
    self._kaiqiBtn:addTouchEventListener(kaiqiBtnCallBack)
    self._kaiqiBtn:setPosition(cc.p(-1000,-1000))
    self._kaiqiBtn:setVisible(false)


    --关闭事件
    local function closeBtnCallBack(sender, eventType)
        if eventType == ccui.TouchEventType.ended then
            app.musicSound:playSound(SOUND_HALL_TOUCH)
            self._kaiqiBtn:setTouchEnabled(true)
            self._armaturet:getAnimation():play("dongzuo1")
            self:setVisible(false)          
            self._lingquBg:setPosition(cc.p(-1500,-1500))
            self._baoxiangBg:setPosition(cc.p(-1500,-1500))
        end
    end
    --关闭按钮
    self._closeBtn = self._lingquBg:getChildByName("closeBtn")
    self._closeBtn:addTouchEventListener(closeBtnCallBack)
    

    --宝箱领取成功回调事件
    app.eventDispather:addListenerEvent(eventLoginBaoXiangFinish, self, function(data)
        if app.hallLogic._baoXianMsg["lDonateScore"]~=0 then
            print("宝箱领取成功回调事件"..app.hallLogic._baoXianMsg["lDonateScore"])
            self:showBox(2,app.hallLogic._baoXianMsg["lDonateScore"].."")
        elseif app.hallLogic._baoXianMsg["lDonateLeQuan"]~=0 then
            self:showBox(1,"0")
        end    
        app.hallLogic._isKaiQi=true
        app.eventDispather:dispatherEvent(eventBaoXiangLingQu)      
        self._armaturet:getAnimation():play("dongzuo2") 
        
        local function unReversal()--显示拿到的道具
            print("领取")
            self:lingQu()
        end

        local callfunc = cc.CallFunc:create(unReversal)
        self:runAction(cc.Sequence:create(cc.MoveBy:create(1.5,cc.p(0,0)),callfunc))  --等待开宝箱的时间
    end)
    
    --宝箱领取失败回调事件
    app.eventDispather:addListenerEvent(eventLoginBaoXiangError, self, function(data)    
        print("宝箱领取失败回调事件")     
        self._kaiqiBtn:setTouchEnabled(true)
        self._armaturet:getAnimation():play("dongzuo1")
        self:setVisible(false)       
        app.hallLogic._isKaiQi=true   
        self._lingquBg:setPosition(cc.p(-1500,-1500))
        self._baoxiangBg:setPosition(cc.p(-1500,-1500))
       
    end)

end

--显示宝箱num 1，获得乐卷，2 获得金币 text金币数量
function BaoXiangBox:showBox(num,text)
	self:setVisible(true)
	self._baoxiangBg:setPosition(cc.p(640,360))  
    self._baoXiangNum=num          
    self._baoXiangText=text         
	if num==1 then
        self._lejuan:setPosition(cc.p(420,360)) 
        self._jinbi:setPosition(cc.p(-1000,-1000)) 
	elseif num==2 then
        self._lejuan:setPosition(cc.p(-1000,-1000)) 
        self._jinbi:setPosition(cc.p(420,360)) 
        self._jinbi:getChildByName("lab"):setString(text)
	end

end

--开启宝箱
function BaoXiangBox:kaiQi()
    app.hallLogic:OpenBaoxiangBox(app.hallLogic._loginSuccessInfo["dwUserID"])--开宝箱
end

--领取
function BaoXiangBox:lingQu()
    self._baoxiangBg:setPosition(cc.p(-1640,-1360))  
    self._lingquBg:setPosition(cc.p(640,360))
end

function BaoXiangBox:onExit()
    app.eventDispather:delListenerEvent(self)
end

return BaoXiangBox
