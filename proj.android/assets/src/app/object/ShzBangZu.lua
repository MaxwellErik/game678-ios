ShzBangZu = class("ShzBangZu", function ()
    return display.newLayer()
end)

ShzBangZu._uiLayer=nil
ShzBangZu._bangzhuPage=nil  --翻页容器
ShzBangZu._dian=nil  --点
ShzBangZu._yeMian=0     --当前页面


--1，要添加的对象，2文字，3持续时间
function ShzBangZu:ctor()
    self._uiLayer = cc.Layer:create()
    self:addChild(self._uiLayer)
    local layerInventory = ccs.GUIReader:getInstance():widgetFromJsonFile("GameShz/ShzBangZu.json")
    self._uiLayer:addChild(layerInventory)
    self._bangzhuPage=layerInventory:getChildByName("bangzhuPage")
    self._dian=layerInventory:getChildByName("dian")

    --返回按钮事件
    local function returnBtnAccount(sender, eventType)
        if eventType == ccui.TouchEventType.ended then
            app.musicSound:playSound(SOUND_HALL_TOUCH)                   
            local function unReversal1()
                self:removeFromParent()
            end
            local callfunc1 = cc.CallFunc:create(unReversal1)
            self:runAction(cc.Sequence:create(cc.MoveBy:create(0.3,cc.p(0,720)),callfunc1))
            
        end
    end
    --返回按钮
    local returnBtn = layerInventory:getChildByName("closeBtn")
    returnBtn:addTouchEventListener(returnBtnAccount)

    --翻页画点
    local function gameViewCallback(sender, eventType)
        print("帮助页面"..sender:getCurPageIndex())
        app.musicSound:playSound("bangzhu_fanye")
        
--        if sender:getCurPageIndex()==self._yeMian and self._yeMian==0 then  --翻页1到4 4到1
--            self._bangzhuPage:scrollToPage(4)
--        elseif sender:getCurPageIndex()==self._yeMian and self._yeMian==4 then
--            self._bangzhuPage:scrollToPage(0)
--        else
--            self._yeMian=sender:getCurPageIndex()
--        end
               
        
        for i=1, 5 do       --点判断
            local orCheak=self._dian:getChildByName("page"..i)
            if sender:getCurPageIndex()==(i-1) then
                orCheak:setSelected(true)
            else
                orCheak:setSelected(false)
            end
        end

        
    end
    self._bangzhuPage:addEventListener(gameViewCallback)


    --点点的点击判断
    for i=1, 5 do
        local oneCheck=self._dian:getChildByName("page"..i) --当前点点
        local function dianSelectedEvent(sender,eventType)
            if eventType == ccui.CheckBoxEventType.selected then
                oneCheck:setSelected(true)
                for j=1, 5 do       --其他点点
                    if j~=i then
                        local orCheak=self._dian:getChildByName("page"..j)
                        orCheak:setSelected(false)
                end
                end
                self._bangzhuPage:scrollToPage(i-1)
            elseif eventType == ccui.CheckBoxEventType.unselected then
                oneCheck:setSelected(true)
                for j=1, 5 do       --其他点点
                    if j~=i then
                        local orCheak=self._dian:getChildByName("page"..j)
                        orCheak:setSelected(false)
                end
                end
            end
            self._bangzhuPage:scrollToPage(i-1)
        end
        oneCheck:addEventListener(dianSelectedEvent)
    end
    self._dian:getChildByName("page1"):setSelected(true)


    --左翻按钮
    local function zuofanBtnAccount(sender, eventType)
        if eventType == ccui.TouchEventType.ended then
            app.musicSound:playSound(SOUND_HALL_TOUCH)
            local yeshu=self._bangzhuPage:getCurPageIndex()          
            if yeshu~=0 then
            	yeshu=yeshu-1
                self._bangzhuPage:scrollToPage(yeshu)
--            else
--                yeshu=4
--                self._bangzhuPage:scrollToPage(yeshu)
            end
            
            --self._yeMian=yeshu
            
--            for j=1, 5 do       --其他点点
--                local orCheak=self._dian:getChildByName("page"..j)
--                if j~=yeshu then                  
--                    orCheak:setSelected(false)
--                    else
--                    orCheak:setSelected(true)
--                end
--            end

        end
    end
    layerInventory:getChildByName("zuofanBtn"):addTouchEventListener(zuofanBtnAccount)
    
    
    --右翻按钮
    local function youfanBtnAccount(sender, eventType)
        if eventType == ccui.TouchEventType.ended then
            app.musicSound:playSound(SOUND_HALL_TOUCH)
            local yeshu=self._bangzhuPage:getCurPageIndex()          
            if yeshu~=4 then
                yeshu=yeshu+1
                self._bangzhuPage:scrollToPage(yeshu)
--            else
--                yeshu=0
--                self._bangzhuPage:scrollToPage(yeshu)
            end
            
            --self._yeMian=yeshu
            
--            for j=1, 5 do       --其他点点
--                    local orCheak=self._dian:getChildByName("page"..j)
--                if j~=yeshu then
--                    orCheak:setSelected(false)
--                else
--                    orCheak:setSelected(true)
--                end
--            end

        end
    end
    layerInventory:getChildByName("youfanBtn"):addTouchEventListener(youfanBtnAccount)





end

return ShzBangZu