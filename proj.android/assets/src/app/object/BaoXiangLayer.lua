BaoXiangLayer = class("BaoXiangLayer", function ()
    return display.newLayer()
end)

BaoXiangLayer._baoxiangLayer=nil    --宝箱layer
BaoXiangLayer._zhanghaoLayer=nil    --账号layer

--1，要添加的对象，2文字，3持续时间
function BaoXiangLayer:ctor()
    self._uiLayer = cc.Layer:create()
    self:addChild(self._uiLayer)

    self._giving = 0 --是否是赠送记录
    self._nowpageNum = 1 --当前页数

    local layerInventory = ccs.GUIReader:getInstance():widgetFromJsonFile("GameSelect/BaoXiangLayer.json")
    self._uiLayer:addChild(layerInventory)
    self._baoxiangLayer=layerInventory:getChildByName("baoxiangLayer")
    self._zhanghaoLayer=layerInventory:getChildByName("zhanghaoLayer")
    
    self._baoxiangLayer:setPosition(640, 1500)
    self._zhanghaoLayer:setPosition(640, 1000)
    
    self._account = ccui.Helper:seekWidgetByName(self._zhanghaoLayer, "wanjiaZhangHao")
    self._number = ccui.Helper:seekWidgetByName(self._zhanghaoLayer, "num")
    
    self._selectIndex = 0
    self._geziMaxCount = 6
    
    layerInventory:setTouchEnabled(true)
    layerInventory:addTouchEventListener(function(sender, type) 
        if self._bShowGive == true then
            self:dismissGiveLayer()
        elseif self._bShow == true then
            self:dismiss()
        end
    end)
    
    --返回按钮事件
    local function returnBtnAccount(sender, eventType)
        if eventType == ccui.TouchEventType.ended then
            app.musicSound:playSound(SOUND_HALL_TOUCH)
            self:dismiss()
        end
    end
    --返回按钮
    local returnBtn = self._baoxiangLayer:getChildByName("returnBtn")
    returnBtn:addTouchEventListener(returnBtnAccount)
    
    --使用按钮事件
    local function shiyongBtnAccount(sender, eventType)
        if eventType == ccui.TouchEventType.ended then
            app.musicSound:playSound(SOUND_HALL_TOUCH)
            if self._selectIndex > 0 then
                local buyInfo = self._xiangziMap[self._selectIndex].info         
                
                PayBox.new(2, buyInfo.dwCount, buyInfo.dwRMBValue, buyInfo.dwRMBValue, self)   
                -- app.hallLogic:openRedEnvelope(buyInfo.dwRMBValue, 1)
            end            
        end
    end
    --使用按钮
    local shiyongBtn = self._baoxiangLayer:getChildByName("shiyongBtn")
    shiyongBtn:addTouchEventListener(shiyongBtnAccount)
    
    --赠送按钮事件
    local function zengsongBtnAccount(sender, eventType)
        if eventType == ccui.TouchEventType.ended then
            app.musicSound:playSound(SOUND_HALL_TOUCH)
            dump(self._xiangziMap[self._selectIndex].info)
            self._selRedInfo = self._xiangziMap[self._selectIndex].info
            if self._bShowGiveIng ~= true and self._bShowGive ~= true then
                self:showGiveLayer()
            end            
        end
    end
    --赠送按钮
    local zengsongBtn = self._baoxiangLayer:getChildByName("zengsongBtn")
    zengsongBtn:addTouchEventListener(zengsongBtnAccount)  
    
    ---------------宝箱     
    self._xiangziMap = {}
    
    for i=1, self._geziMaxCount do
        local geziBG = ccui.Helper:seekWidgetByName(self._baoxiangLayer,"ImgGezi"..i)
        geziBG.numberLab = geziBG:getChildByName("number")
        geziBG.numberLab:setVisible(false)
        geziBG:setTouchEnabled(true)
        geziBG:addTouchEventListener(function(sender, type)
            if type == ccui.TouchEventType.ended then
                app.musicSound:playSound(SOUND_HALL_TOUCH)
                
                if sender.info ~= nil then
                    self:updateXiangZiState(sender)                    
                end
            end
        end)
        self._xiangziMap[i] = geziBG
    end
    
    ----------------------------------
    --赠送账号
    
    --赠送确认按钮事件
    local function querenBtnAccount(sender, eventType)
        if eventType == ccui.TouchEventType.ended then
            app.musicSound:playSound(SOUND_HALL_TOUCH)
            
            local account = string.rtrim(string.ltrim(self._account:getString())) --账号
            local numberTxt = string.rtrim(string.ltrim(self._number:getString())) --个数
            local number = tonumber(numberTxt)
            
            if account == nil or string.len(account)==0 then
            	MyToast.new(self._uiLayer, STR_ACCOUNT_NULL)
            	return
            end
            
            if number == nil or number <= 0 then
                MyToast.new(self._uiLayer, STR_NUMBER_ZERO)
                return
            end
            
            if number > self._selRedInfo.dwCount then
                MyToast.new(self._uiLayer, string.format(STR_NUMBER_TOOMUCH, self._selRedInfo.dwCount, self._selRedInfo.dwRMBValue))
                return
            end
           
            print('account:'..account)
            print("number:"..number)
            
            app.hallLogic:giveRedEnvelope(account, self._xiangziMap[self._selectIndex].info.dwRMBValue, number)
        end
    end
    --赠送确认按钮
    local querenBtn = self._zhanghaoLayer:getChildByName("querenBtn")
    querenBtn:addTouchEventListener(querenBtnAccount) 
    
    --赠送取消按钮事件
    local function quxiaoBtnAccount(sender, eventType)
        if eventType == ccui.TouchEventType.ended then
            app.musicSound:playSound(SOUND_HALL_TOUCH)
            self:dismissGiveLayer()
        end
    end
    
    --赠送取消按钮
    local quxiaoBtn = self._zhanghaoLayer:getChildByName("quxiaoBtn")
    quxiaoBtn:addTouchEventListener(quxiaoBtnAccount) 
    
    self._bShow = false
    self._bShowGive = false

    --宝箱按钮
    self._boxBtn = self._baoxiangLayer:getChildByName("leftBg"):getChildByName("boxBtn")
    local function boxCallBack(sender, eventType)
        if eventType == ccui.TouchEventType.ended then
            app.musicSound:playSound(SOUND_HALL_TOUCH)
            self._Record:setVisible(false)
        end
    end
    self._boxBtn:addTouchEventListener(boxCallBack) 
    --赠送记录
    self._givingBtn = self._baoxiangLayer:getChildByName("leftBg"):getChildByName("givingRecordBtn")
    local function givingCallBack(sender, eventType)
        if eventType == ccui.TouchEventType.ended then
            app.musicSound:playSound(SOUND_HALL_TOUCH)
            self._pageView:removeAllChildren()
            --self:recordCallBack()
            self._giving = 1
            self._nowpageNum = 1
            app.hallLogic:sendGivingRecord(self._nowpageNum)
        end
    end
    self._givingBtn:addTouchEventListener(givingCallBack) 
    --获赠记录
    self._getRecordBtn = self._baoxiangLayer:getChildByName("leftBg"):getChildByName("getRecordBtn")
    local function getRecordCallBack(sender, eventType)
        if eventType == ccui.TouchEventType.ended then
            app.musicSound:playSound(SOUND_HALL_TOUCH)
            self._pageView:removeAllChildren()
            --self:recordCallBack()
            self._giving = 2
            self._nowpageNum = 1
            app.hallLogic:sendGetRecord(self._nowpageNum)
        end
    end
    self._getRecordBtn:addTouchEventListener(getRecordCallBack) 

    --记录Layer
    self._Record = self._baoxiangLayer:getChildByName("Record")
    self._Record:setVisible(false)
    --pageview
    self._pageView = self._Record:getChildByName("pageViewList")
    --左箭头按钮
    self._leftarrowBtn = self._Record:getChildByName("leftArrowBtn")
    local function leftCallBack(sender, eventType)
        if eventType == ccui.TouchEventType.ended then
            app.musicSound:playSound(SOUND_HALL_TOUCH)
            self._pageView:removeAllChildren()
            self._nowpageNum = self._nowpageNum - 1
            self:arrowCallBack()
        end
    end
    self._leftarrowBtn:addTouchEventListener(leftCallBack) 
    --右箭头按钮
    self._rightarrowBtn = self._Record:getChildByName("rightArrowBtn")
    local function rightCallBack(sender, eventType)
        if eventType == ccui.TouchEventType.ended then
            app.musicSound:playSound(SOUND_HALL_TOUCH)
            self._pageView:removeAllChildren()
            self._nowpageNum = self._nowpageNum + 1
            self:arrowCallBack()
        end
    end
    self._rightarrowBtn:addTouchEventListener(rightCallBack) 
    --页数
    self._pageNum = self._Record:getChildByName("pagelable")
end

--箭头回调
function BaoXiangLayer:arrowCallBack()
    if self._giving == 1 and self._nowpageNum >= 1 then --赠送
        app.hallLogic:sendGivingRecord(self._nowpageNum)
    elseif self._giving == 2 and self._nowpageNum >= 1 then --获赠
        app.hallLogic:sendGetRecord(self._nowpageNum)
    end
end

--得到记录的回调
function BaoXiangLayer:recordCallBack(tab)
    if self._nowpageNum <= 1 then
        self._leftarrowBtn:setVisible(false)
    else
        self._leftarrowBtn:setVisible(true)
    end
    if self._nowpageNum == tab.dwTotal then
        self._rightarrowBtn:setVisible(false)
    else
        self._rightarrowBtn:setVisible(true)
    end
    self._pageNum:setString(self._nowpageNum.."/"..tab.dwTotal)

    self._Record:setVisible(true)
    self._pageView:setInertiaScrollEnabled(true)
    self._pageView:setDirection(1)
    self._pageView:setGravity(2)
    self._pageView:removeAllChildren()
    -- --測試代碼
    -- for i=1, 30 do
    --     local t_test1 = payRecordItem.new( "2015/10/15 10:05:22", ""..i.."元", ""..i..i..i.."萬", 1)
    --     local t_width = t_test1:getContentSize().width
    --     local t_height = t_test1:getContentSize().height
    --     local moduleWgt = ccui.Widget:create()
    --     moduleWgt:setContentSize(cc.size(700, 50))
    --     moduleWgt:addChild(t_test1)
    --     t_test1:setPosition(cc.p(0, 0))

    --     self._pageView:insertCustomItem( moduleWgt, self._pageView:getChildrenCount() )
    -- end 
    dump(tab.redEnvelopeRecord)
    local string = ""
    for k, v in pairs(tab.redEnvelopeRecord) do
        string = v.dwRMBValue.."元宝箱"
        if v.dwCount > 1 then
            string = v.dwCount.."*"..v.dwRMBValue.."元宝箱"
        end
        local t_test1 = payRecordItem.new(self:setTimeForTab(v.date), v.szNickname, string, 1)
        local t_width = t_test1:getContentSize().width
        local t_height = t_test1:getContentSize().height
        local moduleWgt = ccui.Widget:create()
        moduleWgt:setContentSize(cc.size(700, 50))
        moduleWgt:addChild(t_test1)
        t_test1:setPosition(cc.p(0, 0))

        self._pageView:insertCustomItem( moduleWgt, self._pageView:getChildrenCount() )
    end     
end

function BaoXiangLayer:setTimeForTab(tab)
    local wYear = tab.wYear
    local wMonth = tab.wMonth
    local wDay = tab.wDay
    local wHour = string.format("%02d", tab.wHour)
    local wMinute = string.format("%02d", tab.wMinute)
    local wSecond = string.format("%02d", tab.wSecond)
    local timestr = wYear.."/"..wMonth.."/"..wDay.." "..wHour..":"..wMinute..":"..wSecond
    return timestr
end

function BaoXiangLayer:showGiveLayer()
    self._bShowGiveIng = true
    self._zhanghaoLayer:runAction(cc.Sequence:create(cc.MoveTo:create(0.6, cc.p(640,425)),
        cc.MoveBy:create(0.05, cc.p(0, 20)), cc.MoveBy:create(0.04, cc.p(0, -20)), cc.CallFunc:create(function() 
            self._bShowGive = true
            self._bShowGiveIng = false
        end)))
end

function BaoXiangLayer:dismissGiveLayer()
    if self._bShowGive == false then
    	return
    end
    
    self._zhanghaoLayer:stopAllActions()
    self._zhanghaoLayer:runAction(cc.Sequence:create(cc.MoveBy:create(0.6,{['x']=0, ['y']=1000}), cc.CallFunc:create(function()
        self._bShowGive = false
    end)))
end

function BaoXiangLayer:show()
    self._baoxiangLayer:stopAllActions()
    self._baoxiangLayer:runAction(cc.Sequence:create(cc.MoveTo:create(0.6, cc.p(640, 360)),
        cc.MoveBy:create(0.05, {['x']=0, ['y']=20}), cc.MoveBy:create(0.04, {['x']=0, ['y']=-20}), cc.CallFunc:create(function() 
            self._bShow = true
        end)))
end

function BaoXiangLayer:dismiss()
    if self._bShowGive == true then
        self:dismissGiveLayer()
    	return
    end

	if self._bShow == false then
		return
	end
	
    self._baoxiangLayer:stopAllActions()
    self._baoxiangLayer:runAction(cc.Sequence:create(cc.MoveBy:create(0.6,{['x']=0, ['y']=2000}), cc.CallFunc:create(function()
        self:removeFromParent()
    end)))
end

function BaoXiangLayer:updateXiangZiState(xiangzi)
    for i=1, self._geziMaxCount do
        local item = self._xiangziMap[i] 
        if item == xiangzi then
            self._selectIndex = i
            if item.imgBright then
                item.imgBright:setVisible(true)
            end            
        else
            if item.imgBright then
                item.imgBright:setVisible(false)
            end
    	end
    end
end

function BaoXiangLayer:resetXiangzi()
    for i=1, self._geziMaxCount do
        local gezi = self._xiangziMap[i]
        gezi.numberLab:setVisible(false)
        gezi.info = nil
        gezi.imgNormal = nil
        gezi.imgBright = nil
        
        gezi:removeChildByTag(100000)
        gezi:removeChildByTag(100001)
    end
end

function BaoXiangLayer:setXiangziInfo(info)
	self:resetXiangzi()
	if info==nil then
	    self._selectIndex = 0
		return
	end
	
	local isState = false
	local index = 1
    if self._selRedInfo then
        self._selRedInfo.dwCount = 0
    end
    
	for i=1, info.dwCount do
        if i <= self._geziMaxCount then
			local infoDetail = info.items[i]
            local geziDetail = self._xiangziMap[index]

            if self._selRedInfo and self._selRedInfo.dwRMBValue == infoDetail.dwRMBValue then
                self._selRedInfo.dwCount = infoDetail.dwCount
            end
            			
			if infoDetail.dwCount > 0 then
                local pos = geziDetail:getContentSize()
			    geziDetail.numberLab:setVisible(true)
			    geziDetail.numberLab:setString(infoDetail.dwCount)
				geziDetail.info = infoDetail
				geziDetail.imgNormal = ccui.ImageView:create()
				geziDetail.imgNormal:loadTexture("GameSelect/baoxiangLayer/"..infoDetail.dwRMBValue.."baoxiang.png")
                geziDetail.imgNormal:setPosition(pos.width/2, pos.height/2)
                geziDetail.imgNormal:setTag(100000)
				
				geziDetail.imgBright = ccui.ImageView:create()
                geziDetail.imgBright:loadTexture("GameSelect/baoxiangLayer/geziguang.png")
                geziDetail.imgBright:setPosition(pos.width/2, pos.height/2)
                geziDetail.imgBright:setTag(100001)
                geziDetail.imgBright:setVisible(false)
                

                geziDetail:addChild(geziDetail.imgNormal, 20)
                geziDetail:addChild(geziDetail.imgBright, 10)
                
                if index == self._selectIndex then
                	isState = true
                end
                
                index = index + 1
			end
		end
	end	
	
    if info.dwCount>0 then
        if self._selectIndex == 0 then
        	self._selectIndex = 1
        end
        self:updateXiangZiState(self._xiangziMap[self._selectIndex])
	end
end

return BaoXiangLayer