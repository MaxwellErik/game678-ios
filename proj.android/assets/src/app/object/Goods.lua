
Goods = class("Goods", function()
    return display.newLayer()
end)

Goods._uiFrame=nil  --购买按钮
Goods.ID=0          --id
Goods.isMove=true   --是否移动
Goods.isHot=true    --是否热卖
Goods.hot=nil       --热卖图片
Goods.icon=nil      --商品图片
Goods.describe=nil  --文字描述
Goods.coinLable=nil --金币数量


function Goods:ctor()

    --购买点击事件
    local function buyCallBack(sender, eventType)

        if eventType == ccui.TouchEventType.moved then
            self.isMove=false
        end

        if eventType == ccui.TouchEventType.ended then
            printf(self.ID)
            if self.isMove then
                printf("jin lai le") 
                --TODO
            else
                self.isMove=true
            end

        end
    end

    --按钮
    self._uiFrame = ccui.Button:create("res/HallUI/gameStore/buy_1.png","res/HallUI/gameStore/buy_2.png")
    self._uiFrame:setAnchorPoint(1,0)
    self._uiFrame:setPosition(1250,0)
    self:addChild(self._uiFrame)   
    self._uiFrame:setSwallowTouches(false)
    self._uiFrame:addTouchEventListener(buyCallBack)
    
    --热卖图片
    self.hot=cc.Sprite:create("HallUI/gameStore/hot.png")
    self.hot:setAnchorPoint(0,0)
    self.hot:setPosition(30,8)
    self:addChild(self.hot)
    
    --商品图片
    self.icon=cc.Sprite:create("HallUI/gameStore/ico_2.png")
    self.icon:setAnchorPoint(0,0)
    self.icon:setPosition(140,0)
    self:addChild(self.icon)
    
    --文字描述图片
    self.describe=cc.Sprite:create("HallUI/gameStore/goods-1.png")
    self.describe:setAnchorPoint(0,0)
    self.describe:setPosition(370,30)
    self:addChild(self.describe)
    
    --¥的标志
    local moneyIcon=cc.Sprite:create("HallUI/gameStore/ico_1.png")
    moneyIcon:setAnchorPoint(0,0)
    moneyIcon:setPosition(700,30)
    self:addChild(moneyIcon)
    
    --金币数量
    self.coinLable = cc.LabelAtlas:_create("02564", "HallUI/gameStore/num.png", 22, 27,  string.byte("0"))
    self:addChild(self.coinLable)
    self.coinLable:setPosition(730, 30)
    

end

----参数1，是否热卖；参数2，商品图片；参数3，文字描述；参数4，价格；参数56，坐标  
function Goods:setContent(isHot,icon,describe,coinLable,x,y)
	self.hot:setVisible(isHot)
    self.icon:setTexture(icon)
	self.describe:setTexture(describe)
    self.coinLable:setString(coinLable)
    self:setPosition(x,y)
end


return Goods

