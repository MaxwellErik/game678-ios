MyToast = class("MyToast", function ()
    return display.newLayer()
end)

MyToast._bg=nil

--1，要添加的对象，2文字，3持续时间
function MyToast:ctor(parentLayer,text,num)
    self._uiLayer = cc.Layer:create()
    self:addChild(self._uiLayer)
    local layerInventory = ccs.GUIReader:getInstance():widgetFromJsonFile("res/HallUI/MyToast.json")
    self._uiLayer:addChild(layerInventory)
    
    self._bg=layerInventory:getChildByName("bg")
    local t_text= self._bg:getChildByName("lab")
    t_text:setString(text)

    --self._bg:setTextureRect(t_text:getTextureRect())--getContentSize()

    parentLayer:addChild(self,1000)


    local function unReversal1()
        self:removeFromParent()
    end


    local callfunc1 = cc.CallFunc:create(unReversal1)
    if num==nil or num==0 then
        self._bg:runAction(cc.Sequence:create(cc.MoveBy:create(1.2,cc.p(0,500)),cc.FadeOut:create(0.7),callfunc1))
    else
        self._bg:runAction(cc.Sequence:create(cc.MoveBy:create(1.2,cc.p(0,500)),cc.FadeOut:create(num),callfunc1))
    end
    

end

return MyToast

