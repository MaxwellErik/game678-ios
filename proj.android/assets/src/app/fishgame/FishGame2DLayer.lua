
function FishLayer()
	local fl = {};
	
	function fl.clear()	
		if fl.layer then
			fl.layer:removeFromParent(true);
			fl.layer:release();
		end
		fl.layer = nil;
	end
	
	--增加一只鱼
	function fl.addFish(id,key)
		local fh = Fish();
		fh.init(id);
		local animation = fh.getAnimation();
		local layer = fl.layer;
		layer:addChild(animation);
		return fh;
	end
	
	--增加一发子弹
	function fl.addBullet(bl)
		local node = bl.getNode();
		if node then
			fl.layer:addChild(node);
		end
	end
	
	--向场景中增加一个动画
	function fl.addAnimation(ccsNode)
		local node = ccsNode.getArmature();
		if node then
			fl.layer:addChild(node);
		end
	end
	
	--初始化Fish层
	function fl.create()
		fl.clear();
		local layer = cc.Layer:create();
		layer:retain();
		fl.layer = layer;
	end
	
	function fl.getLayer()
		return fl.layer;
	end
	
	fl.create();
	return fl;
end