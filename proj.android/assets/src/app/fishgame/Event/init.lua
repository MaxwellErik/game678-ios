MyEvent = import(".MyEvent")
EventManager = import(".EventManager")

function RaiseEvent(i_strEventName, i_pParam, i_pSource, i_pTarget)
    local ie = MyEvent.new(i_strEventName, i_pParam, i_pSource, i_pTarget)
    EventManager.GetInstance():ProcessEvent(ie)
end

function PostEvent(i_strEventName, i_pParam, i_pSource, i_pTarget)
    local ie = MyEvent.new(i_strEventName, i_pParam, i_pSource, i_pTarget)
    EventManager.GetInstance():ProcessEvent(ie)
end

function Bind_Event_Handler(_evnet,_handler)
    EventManager.GetInstance():RegisterEvent(_evnet,_handler)
end


EME_STATE_CHANGED = 0 --状态变化
EME_QUERY_SPEED_MUL = 1 --查询速度倍率
EME_QUERY_ADDMUL = 2    --查询额外增加的倍率