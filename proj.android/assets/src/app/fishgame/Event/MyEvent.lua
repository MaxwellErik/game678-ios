local MyEvent = class("MyEvent")

function MyEvent:ctor(name, param, source, target)
    self.strName = name
    self.pParam = param
    self.pSource = source
    self.pTarget = target
end

function MyEvent:SetName(name) self.strName = name end

function MyEvent:GetName() return self.strName end

function MyEvent:SetParam(param) self.pParam = param end

function MyEvent:GetParam() return self.pParam end

function MyEvent:SetSource(s) self.pSource = s end

function MyEvent:GetSource() return self.pSource end

function MyEvent:SetTarget(t) self.pTarget = t end

function MyEvent:GetTarget() return self.pTarget end

return MyEvent