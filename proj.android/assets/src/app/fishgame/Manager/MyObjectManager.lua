local MyObjectManager = class("MyObjectManager")

function MyObjectManager:ctor()
    self.m_fishes = {}
    self.m_bullets = {}

    self.m_logic = nil
end

function MyObjectManager:Init(logic)
    self.m_logic = logic

    fishgame.FishObjectManager:GetInstance():Init(1440, 900, 1280, 720)
end

function MyObjectManager:AddBullet(id, bullet)
    --    if not id or not bullet then return end
    --    if bullet.__cname ~= "Bullet" then return end
    --
    --    if self.m_bullets[id] then
    --        self.m_bullets[id]:Clear(true)
    --        return
    --    end
    --
    --    self.m_bullets[id] = bullet

    --        self.m_logic._fishGame2DScene:AddBullet(bullet)

    fishgame.FishObjectManager:GetInstance():AddBullet(bullet)
end

function MyObjectManager:RemoveAllBullets(noCleanNode)
    --    for k, v in pairs(self.m_bullets) do
    --        v:Clear()
    --    end
    --    self.m_bullets = {}
    fishgame.FishObjectManager:GetInstance():RemoveAllBullets(noCleanNode)
end

function MyObjectManager:FindBullet(id)

    return fishgame.FishObjectManager:GetInstance():FindBullet(id)
    --    return self.m_bullets[id]
end


function MyObjectManager:AddFish(id, fish)
    fish:SetState(1)
    if fishgame.FishObjectManager:GetInstance():AddFish(fish) then
        --        self.m_fishes[id] = fish
    end
end

function MyObjectManager:RemoveAllFishes(noCleanNode)
    --    for _, v in pairs(self.m_fishes) do
    --        v:Clear(true)
    --    end
    self.m_fishes = {}
    fishgame.FishObjectManager:GetInstance():RemoveAllFishes(noCleanNode)
end

function MyObjectManager:FindFish(id)
    --    return self.m_fishes[id]

    return fishgame.FishObjectManager:GetInstance():FindFish(id)
end

function MyObjectManager:AddFishBUff(buffType, buffParam, buffTime)
    --    for k, v in pairs(self.m_fishes) do
    --        local pBM = v:GetComponent(MyComponentType.ECF_BUFFERMGR)
    --        if pBM then
    --            pBM:Add(buffType, buffParam, buffTime)
    --        end
    --    end

    fishgame.FishObjectManager:GetInstance():AddFishBuff(buffType, buffParam, buffTime)
end

function MyObjectManager:Clear()
    self:RemoveAllBullets(true)
    self:RemoveAllFishes(true)

    self.m_logic = nil
end


function MyObjectManager:OnUpdate(dt)
    for k, v in pairs(self.m_fishes) do
        local status = v:GetState()
        -- 如果不是死亡的更新状态 --
        if status < EOS_DEAD then
            v:OnUpdate(dt)
            status = v:GetState()
        end
        -- 剔除死亡 --
        if status == EOS_DEAD then
            v:Clear()
            self.m_fishes[k] = nil
        elseif status > EOS_DEAD then
            v:Clear(true)
            self.m_fishes[k] = nil
        end
    end

    local bSwitching = CGameConfig.GetInstance():IsSwitchingScene()
    for k, v in pairs(self.m_bullets) do
        local status = v:GetState()
        -- 先检查碰撞 --
        if status < EOS_DEAD then
            local hasLockFish = true

            if not bSwitching then
                for kk, vv in pairs(self.m_fishes) do
                    if v:GetTarget() == 0 then
                        if self:BBulletHitFish(v, vv) then
                            self.m_logic:onActionBulletHitFish(v, vv)
                            break
                        end
                    else
                        if vv:GetId() == v:GetTarget() then
                            hasLockFish = false

                            if self:BBulletHitFish(v, vv) then
                                self.m_logic:onActionBulletHitFish(v, vv)
                                break
                            end
                        end
                    end
                end
            end
            if hasLockFish then
                v:SetTarget(0)
            end

            v:OnUpdate(dt)
            status = v:GetState()
        end
        -- 刷新状态 --
        if status == EOS_DEAD then
            v:Clear()
            self.m_bullets[k] = nil
        elseif status > EOS_DEAD then
            v:Clear(true)
            self.m_bullets[k] = nil
        end
    end
end

function MyObjectManager:OnUpdate(dt)
    fishgame.FishObjectManager:GetInstance():OnUpdate(dt)
end

function MyObjectManager:BBulletHitFish(bullet, fish)
    --    local bulletRad = bullet:GetCatchRadio()
    --    local maxFishRadio = fish:GetMaxRadio()
    --    local xBullet,yBullet = bullet:GetPosition()
    --    local xFish,yFish = fish:GetPosition()
    --
    --
    --
    --    if (xBullet + bulletRad < xFish - maxFishRadio
    --            and yBullet+ bulletRad < yFish - maxFishRadio)
    --            or (xBullet - bulletRad > xFish + maxFishRadio
    --            and yBullet - bulletRad > yFish+ maxFishRadio)
    --    then
    --        return false
    --    end
    --
    --    local dirFish = fish:GetDirection()
    --
    --    local sinDir = math.sin(dirFish)
    --    local cosDir = math.cos(dirFish)
    --    --    x1=x0cosn-y0sinn
    --    --    y1=x0sinn+y0cosn
    --    local boxs = fish:GetBoundingBoxs()
    --    for _, v in pairs(boxs) do
    --        -- 转换坐标 --
    --        local x = v.offsetX * cosDir - v.offsetY * sinDir + xFish
    --        local y = v.offsetX * sinDir + v.offsetY * cosDir + yFish
    --        local rad = v.rad
    --
    --        local dis = math.sqrt((x - xBullet) ^ 2 + (y - yBullet) ^ 2)
    --        if dis < rad + bulletRad then
    --            return true
    --        end
    --    end
    --
    --    return false
end

return singleton(MyObjectManager)