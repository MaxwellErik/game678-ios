SpecialFishType = {
    ESFT_NORMAL = 0,
    ESFT_KING = 1,
    ESFT_KINGANDQUAN = 2,
    ESFT_SANYUAN = 3,
    ESFT_SIXI = 4,
    ESFT_MAX = 5,
}

RefershType = {
    ERT_NORMAL = 0,
    ERT_GROUP = 1,
    ERT_LINE = 2,
    ERT_SNAK = 3,
}

ResourceType = {
    ERST_Sprite = 0,
    ERST_Animation = 1,
    ERST_Particle = 2,
};

RenderState = {
    ERSS_Normal = 1,
    ERSS_FIRE = 2,
    ERSS_Mul = 4,
    ERSS_Score = 8,
};
PartType = {
    EPT_BASE = 0,
    EPT_CANNON = 1,
    EPT_EFFECT = 2,
    EPT_CANNUM = 3,
    EPT_SCORE = 4,
    EPT_TAG = 5,
};


local CGameConfig = class("CGameConfig")

function CGameConfig:ctor()
    self.nDefaultWidth = 0
    self.nDefaultHeight = 0
    self.nWidth = 0
    self.nHeight = 0
    self.nChangeRatioUserScore = 0
    self.nChangeRatioFishScore = 0
    self.nExchangeOnce = 0
    self.nFireInterval = 0
    self.nMaxInterval = 0
    self.nMinInterval = 0
    self.nMinNotice = 0
    self.fAndroidProbMul = 0
    self.nPlayerCount = 0
    self.nSpecialProb = {}
    self.nSpecialProb[SpecialFishType.ESFT_MAX] = 0

    self.VisualMap = {} -- map<int, Visual>
    self.FishMap = {} -- map<int, Fish>
    self.BulletVector = {} -- vector<Bullet>
    self.BBXMap = {} -- map<int, BBX>

    self.nAddMulBegin = 0
    self.nAddMulCur = 0

    self.m_MaxCannon = 0

    self.bImitationRealPlayer = false

    self.FirstFireList = {} --  vector < FirstFire >

    self.fHScale = 0
    self.fVScale = 0

    self.SceneSets = {} -- map<int, SceneSet>
    self.FishSound = {} -- map<int, SoundSet>
    self.KingFishMap = {} -- map<int, SpecialSet>
    self.SanYuanFishMap = {} -- map<int, SpecialSet>
    self.SiXiFishMap = {} -- map<int, SpecialSet>

    self.CannonPos = {} -- vector < CMovePoint >
    self.CannonSetArray = {} -- vector < CannonSetS >

    self.szCannonEffect = ""
    self.EffectPos = MyPoint.new(0, 0)
    self.nJettonCount = 0
    self.JettonPos = MyPoint.new(0, 0)
    self.LockInfo = {}


    self.ShowDebugInfo = false
    self.nShowGoldMinMul = 0
    self.ShowShadow = false

    self.nIonMultiply = 0
    self.nIonProbability = 0
    self.fDoubleTime = 0

    self.nMaxBullet = 0
    self.nMaxSpecailCount = 0

    self.fGiveRealPlayTime = 0
    self.fGiveTime = 0


    self.vGiveFish = {} -- vector < int >
    self.vGiveProb = {} -- vector < int >

    self.nSnakeHeadType = 0
    self.nSnakeTailType = 0

    self.m_bMirror = false

    self:LoadFish(FISHGAME2D_XML_FISH)
    self:LoadSpecialFish(FISHGAME2D_XML_SPECIAL_FISH)
    self:LoadBulletSet(FISHGAME2D_XML_BULLETSET)
    self:LoadFishSound(FISHGAME2D_XML_FISH_SOUND)
    self:LoadBoundBox(FISHGAME2D_XML_BOUNDING_BOX)
    self:LoadScenes(FISHGAME2D_XML_SCENE)
    self:LoadCannonSet(FISHGAME2D_XML_CANNONSET)
    self:LoadVisual(FISHGAME2D_XML_VISUAL)
    self:LoadSystemConfig(FISHGAME2D_XML_SYSTEM)
end

function CGameConfig:Clear()
    SpecialFishType = nil
    RefershType = nil
    ResourceType = nil
    RenderState = nil
    PartType = nil
end

function CGameConfig:LoadSystemConfig(xmlFile)
    local xmlData = GetXmlTable(xmlFile)
    local SystemSet = xmlData.SystemSet


    self.ShowDebugInfo = SystemSet.ShowDebugInfo == "true"
    self.ShowShadow = SystemSet.Shadow == "true"
    self.bImitationRealPlayer = SystemSet.ImitationRealPlayer == "true"


    local sets = SystemSet.DefaultScreenSet

    self.nDefaultWidth = tonumber(sets.width)
    self.nDefaultHeight = tonumber(sets.height)

    local ExchangeScore = SystemSet.ExchangeScore
    local rat = ExchangeScore.Ratio
    local ratio = string.split(rat, ":")
    self.nChangeRatioUserScore = tonumber(ratio[1])
    if self.nChangeRatioUserScore <= 0 then self.nChangeRatioUserScore = 1 end
    self.nChangeRatioFishScore = tonumber(ratio[2])
    if self.nChangeRatioFishScore <= 0 then self.nChangeRatioFishScore = 1 end
    self.nExchangeOnce = tonumber(ExchangeScore.Once)
    if self.nExchangeOnce <= 0 then self.nExchangeOnce = 10000 end
    self.nShowGoldMinMul = tonumber(ExchangeScore.ShowGoldMinMul)

    local Fire = SystemSet.Fire
    self.nFireInterval = (tonumber(Fire.Interval) or 0) / 1000
    self.nMaxInterval = (tonumber(Fire.MaxInterval) or 0)
    self.nMinInterval = (tonumber(Fire.MinInterval) or 0)
    self.nMaxBullet = tonumber(Fire.MaxBullet)

    local IonSet = SystemSet.IonSet
    self.nIonMultiply = tonumber(SystemSet.Multiple)
    self.nIonProbability = tonumber(SystemSet.Probability)
    self.fDoubleTime = tonumber(SystemSet.time)

    local Catch = SystemSet.Catch
    self.nMinNotice = tonumber(Catch.NoticeLevel) or 200
    self.fAndroidProbMul = tonumber(Catch.AndroidProbMul) or 1.2

    local Special = SystemSet.Special
    self.nMaxSpecailCount = tonumber(Special.Special)
    self.nSpecialProb[SpecialFishType.ESFT_KING] = tonumber(Special.King) or 0
    self.nSpecialProb[SpecialFishType.ESFT_KINGANDQUAN] = tonumber(Special.KingQuan) or 0
    self.nSpecialProb[SpecialFishType.ESFT_SANYUAN] = tonumber(Special.SanYuan) or 0
    self.nSpecialProb[SpecialFishType.ESFT_SIXI] = tonumber(Special.SiXi) or 0

    local AddMul = SystemSet.AddMul or {}
    self.nAddMulBegin = tonumber(AddMul.Begin) or 40

    local SNAKE = SystemSet.SNAKE or {}
    self.nSnakeHeadType = tonumber(SNAKE.Head) or 901
    self.nSnakeTailType = tonumber(SNAKE.Tail) or 902

    self.FirstFireList = {}
    local FirstFire = SystemSet.FirstFire or {}
    for _, v in ipairs(FirstFire) do
        local ff = {}

        ff.nLevel = tonumbe(v.level) or 0
        ff.nCount = tonumbe(v.Count) or 0
        ff.nPriceCount = tonumbe(v.PirceCount) or 0
        ff.TypeCount = tonumbe(v.TypeCount) or 0

        local ts = string.split(v.TypeList, ",")
        local tw = string.split(v.WeightList, ",")
        ff.FishTypeVector = {}
        ff.WeightVector = {}
        for i = 1, ff.TypeCount do
            ff.FishTypeVector[i] = ts[i]
            ff.WeightVector[i] = tw[i]
        end
        table.inset(self.FirstFireList, ff)
    end

    local GIVE = SystemSet.GIVE or {}
    self.fGiveRealPlayTime = tonumber(GIVE.interval) or 1800
    self.fGiveTime = tonumber(GIVE.time) or 180

    local tc = tonumber(GIVE.TypeCount) or 0
    local ts = GIVE.TypeList
    local tw = GIVE.Probability

    for i = 1, tc do
        self.vGiveFish[i] = ts[i]
        self.vGiveProb[i] = tw[i]
    end
end

function CGameConfig:LoadFish(xmlFile)
    local xmlData = GetXmlTable(xmlFile);
    local FishSet = xmlData.FishSet
    local fish = FishSet.Fish

    self.FishMap = {}

    for _, v in ipairs(fish) do
        local ff = {}

        ff.nTypeID = tonumber(v.TypeID)
        ff.szName = v.Name

        ff.bBroadCast = v.BroadCast == "true"
        ff.fProbability = tonumber(v.Probability)
        ff.nSpeed = tonumber(v.Speed)

        ff.nVisualID = tonumber(v.VisualID)
        ff.nBoundBox = tonumber(v.BoundingBox)

        ff.bShowBingo = v.ShowBingo == "true"
        ff.szParticle = v.Particle
        ff.bShakeScree = v.ShowBingo == "true"
        ff.nLockLevel = tonumber(v.LockLevel)

        ff.EffectSet = {}

        local effct = v.Effect or {}
        if #effct == 0 and table.nums(effct) > 0 then
            local ecf = {}
            ecf.nTypeID = tonumber(effct.TypeID)
            ecf.nParam = {}

            for i = 1, 10 do
                local par = effct["Param" .. i]

                if not par then break end

                table.insert(ecf.nParam, tonumber(par))
            end

            table.insert(ff.EffectSet, ecf)
        else
            for __, vv in ipairs(effct) do
                local ecf = {}
                ecf.nTypeID = tonumber(vv.TypeID)
                ecf.nParam = {}

                for i = 1, 10 do
                    local par = vv["Param" .. i]

                    if not par then break end

                    table.insert(ecf.nParam, tonumber(par))
                end

                table.insert(ff.EffectSet, ecf)
            end
        end

        ff.BufferSet = {}

        local buf = v.Buffer or {}
        if #buf == 0 and table.nums(buf) > 0 then
            local but = {}
            but.nTypeID = tonumber(buf.TypeID)
            but.fParam = tonumber(buf.Param)
            but.fLife = tonumber(buf.Life)

            table.insert(ff.BufferSet, but)
        else
            for __, vv in ipairs(buf) do
                local but = {}
                but.nTypeID = tonumber(vv.TypeID)
                but.fParam = tonumber(vv.Param)
                but.fLife = tonumber(vv.Life)

                table.insert(ff.BufferSet, but)
            end
        end


        self.FishMap[ff.nTypeID] = ff;
    end
end

function CGameConfig:LoadVisual(xmlFile)
    local xmlData = GetXmlTable(xmlFile)
    local VisualSet = xmlData.VisualSet
    local Visual = VisualSet.Visual
    self.VisualMap = {}
    for _, v in ipairs(Visual) do
        local vs = {}

        vs.nID = tonumber(v.Id)
        vs.nTypeID = tonumber(v.TypeID)
        vs.ImageInfoLive = {}
        vs.ImageInfoDie = {}

        local image = v.Live or {}
        if #image == 0 and table.nums(image) > 0 then
            local imi = {}
            imi.Image = image.Image
            imi.Name = image.Name
            imi.Scale = tonumber(image.Scale)
            imi.OffestX = tonumber(image.OffestX)
            imi.OffestY = tonumber(image.OffestY)
            imi.Direction = math.deg(tonumber(image.Direction))
            imi.AniType = tonumber(image.AniType)
            table.insert(vs.ImageInfoLive, imi)
        else
            for __, vv in ipairs(image) do
                local imi = {}
                imi.Image = vv.Image
                imi.Name = vv.Name
                imi.Scale = tonumber(vv.Scale)
                imi.OffestX = tonumber(vv.OffestX)
                imi.OffestY = tonumber(vv.OffestY)
                imi.Direction = math.deg(tonumber(vv.Direction))
                imi.AniType = tonumber(vv.AniType)

                table.insert(vs.ImageInfoLive, imi)
            end
        end


        local image = v.Die or {}
        if #image == 0 and table.nums(image) > 0 then
            local imi = {}
            imi.Image = image.Image
            imi.Name = image.Name
            imi.Scale = tonumber(image.Scale)
            imi.OffestX = tonumber(image.OffestX)
            imi.OffestY = tonumber(image.OffestY)
            imi.Direction = math.deg(tonumber(image.Direction))
            imi.AniType = tonumber(image.AniType)

            table.insert(vs.ImageInfoDie, imi)
        else
            for __, vv in ipairs(image) do
                local imi = {}
                imi.Image = vv.Image
                imi.Name = vv.Name
                imi.Scale = tonumber(vv.Scale)
                imi.OffestX = tonumber(vv.OffestX)
                imi.OffestY = tonumber(vv.OffestY)
                imi.Direction = math.deg(tonumber(vv.Direction))
                imi.AniType = tonumber(vv.AniType)

                table.insert(vs.ImageInfoDie, imi)
            end
        end
        self.VisualMap[vs.nID] = vs
    end
end

function CGameConfig:LoadCannonSet(xmlFile)
    local xmlData = GetXmlTable(xmlFile)
    local CCPOS = xmlData.CannonPos
    local Cannon = CCPOS.Cannon

    self.CannonPos = {}

    for _, v in ipairs(Cannon) do
        local id = tonumber(v.id)

        local x_ = tonumber(v.PosX)
        local y_ = tonumber(v.PosY)
        local dir = tonumber(v.Direction)
       
        self.CannonPos[id] = CMovePoint.new(MyPoint.new(x_, y_), dir)
    end

    local cef = CCPOS.CannonEffect

    self.szCannonEffect = cef.name
    self.EffectPos.x_ = tonumber(cef.PosX)
    self.EffectPos.y_ = tonumber(cef.PosY)

    local tnode = CCPOS.LOCK

    self.LockInfo.szLockIcon = tnode.name
    self.LockInfo.szLockLine = tnode.line
    self.LockInfo.szLockFlag = tnode.flag
    self.LockInfo.Pos = MyPoint.new(tonumber(tnode.PosX), tonumber(tnode.PosY))


    local jetton = CCPOS.Jetton
    self.nJettonCount = tonumber(jetton.Max)
    self.JettonPos.x_ = tonumber(tnode.PosX)
    self.JettonPos.y_ = tonumber(tnode.PosY)

    self.CannonSetArray = {}
    local CannonSet = xmlData.CannonSet
    for _, v in ipairs(CannonSet) do
        local canset = {}

        canset.bRebound = v.Rebound == "true"
        canset.nID = tonumber(v.id)
        canset.nNormalID = tonumber(v.normal)
        canset.nIonID = tonumber(v.ion)
        canset.nDoubleID = tonumber(v.double)

        canset.Sets = {}

        local CannonType = v.CannonType or {}

        for __, vv in ipairs(CannonType) do
            local ccs = {}
            ccs.nTypeID = tonumber(vv.type)

            local Part = vv.Cannon or {}
            local cpt = {}
            cpt.szResourceName = Part.ResName
            cpt.Name = Part.Name
            cpt.nResType = tonumber(Part.ResType)
            cpt.Pos = MyPoint.new(tonumber(Part.PosX), tonumber(Part.PosY))
            cpt.PosX = tonumber(Part.PosX)
            cpt.PosY = tonumber(Part.PosY)

            cpt.FireOfffest = tonumber(Part.FireOffest)
            cpt.nType = tonumber(Part.type)
            cpt.RoateSpeed = tonumber(Part.RoateSpeed)
            ccs.Cannon = cpt

            ccs.BulletSet = {}
            local Bullet = vv.Bullet or {}
            local cb = {}
            cb.szResourceName = Bullet.ResName
            cb.Name = Bullet.Name
            cb.nResType = tonumber(Bullet.ResType)
            cb.fScale = tonumber(Bullet.Scale)
            cb.Pos = MyPoint.new(tonumber(Bullet.PosX), tonumber(Bullet.PosY))
            cb.PosX = tonumber(Part.PosX)
            cb.PosY = tonumber(Part.PosY)
            ccs.BulletSet = cb


            local Net = vv.Net
            local ns = {}
            ns.szResourceName = Net.ResName
            ns.Name = Net.Name
            ns.nResType = tonumber(Net.ResType)
            ns.fScale = tonumber(Net.Scale)
            ns.Pos = MyPoint.new(tonumber(Net.PosX), tonumber(Net.PosY))
            ns.PosX = tonumber(Part.PosX)
            ns.PosY = tonumber(Part.PosY)
            ccs.NetSet = ns

            canset.Sets[ccs.nTypeID] = ccs
        end

        self.CannonSetArray[canset.nID] = canset
    end
    -- dump(self.CannonSetArray)
end

function CGameConfig:LoadBulletSet(xmlFile)
    local xmlData = GetXmlTable(xmlFile)
    local BulletSet = xmlData.BulletSet
    for _, v in ipairs(BulletSet) do
        local bt = {}
        bt.nMulriple = tonumber(v.Mulriple)
        bt.nSpeed = tonumber(v.Speed)
        bt.nMaxCatch = tonumber(v.MaxCatch)
        bt.nCatchRadio = tonumber(v.CatchRadio)
        bt.nCannonType = tonumber(v.CannonType)
        bt.nBulletSize = tonumber(v.BRidio)
        
        if self.m_MaxCannon < bt.nMulriple then
            self.m_MaxCannon = bt.nMulriple
        end

        bt.ProbabilitySet = {}

        local cat = v.Catch or {}
        for __, vv in ipairs(cat) do
            bt.ProbabilitySettonumber[vv.FishID] = tonumber(vv.Probability)
        end

        table.insert(self.BulletVector, bt)
    end
end

function CGameConfig:LoadBoundBox(xmlFile)
    local xmlData = GetXmlTable(xmlFile)

    local nbbx = xmlData.BoundingBox
    for _, v in ipairs(nbbx) do
        local boubx = {}
        boubx.nID = tonumber(v.id)
        boubx.BBList = {}

        local nbb = v.BB or {}

        if #nbb == 0 and table.nums(nbb) > 0 then
            local b = {}
            b.fRadio = tonumber(nbb.Radio)
            b.nOffestX = tonumber(nbb.OffestX)
            b.nOffestY = tonumber(nbb.OffestY)

            table.insert(boubx.BBList, b)
        else
            for __, vv in ipairs(nbb) do
                local b = {}
                b.fRadio = tonumber(vv.Radio)
                b.nOffestX = tonumber(vv.OffestX)
                b.nOffestY = tonumber(vv.OffestY)

                table.insert(boubx.BBList, b)
            end
        end

        self.BBXMap[boubx.nID] = boubx
    end
end

function CGameConfig:LoadScenes(xmlFile)
    local xmlData = GetXmlTable(xmlFile);

    self.SceneSets = {}
    local sst = xmlData.scene
    for _, v in ipairs(sst) do
        local SceSet = {}
        SceSet.nID = tonumber(v.id)
        SceSet.fSceneTime = tonumber(v.time)
        SceSet.szMap = v.map
        SceSet.nNextID = tonumber(v.next)

        SceSet.TroopList = {}

        local tps = v.TroopSet or {}
        for __, vv in ipairs(tps) do
            local ts = {}

            ts.fBeginTime = tonumber(vv.BeginTime)
            ts.fEndTime = tonumber(vv.EndTime)
            ts.nTroopID = tonumber(vv.id)

            table.insert(SceSet.TroopList, ts)
        end

        SceSet.DistrubList = {}
        local sets = v.DistrubFish or {}
        for __, vv in ipairs(sets) do
            local dis = {}
            dis.ftime = tonumber(vv.Time) or 10
            dis.nMinCount = tonumber(vv.MinCount) or 1
            dis.nMaxCount = math.max(dis.nMinCount, tonumber(vv.MaxCount) or 1)
            dis.nRefershType = tonumber(vv.RefershType) or RefershType.ERT_NORMAL

            dis.TypeCount = tonumber(vv.TypeCount) or 1
            dis.FishID = {}
            dis.Weight = {}

            local tf = string.split(vv.TypeList, ",")
            local tw = string.split(vv.WeightList, ",")

            for i = 1, dis.TypeCount do
                table.insert(dis.FishID, tonumber(tf[i]))
                table.insert(dis.Weight, tonumber(tw[i]))
            end

            dis.OffestX = tonumber(vv.OffestX)
            dis.OffestY = tonumber(vv.OffestY)
            dis.OffestTime = tonumber(vv.OffestTime)

            table.insert(SceSet.DistrubList, dis)
        end

        self.SceneSets[SceSet.nID] = SceSet
    end
end

function CGameConfig:LoadFishSound(xmlFile)
    local xmlData = GetXmlTable(xmlFile);

    local nbbx = xmlData.FishSound
    local fs = nbbx.Fish

    for _, v in ipairs(fs) do
        local id = tonumber(v.id)

        local ss = {}
        ss.szFoundName = v.Sound
        ss.m_nProbility = tonumber(v.Probility)

        self.FishSound[id] = ss
    end
end

function CGameConfig:LoadSpecialFish(xmlFile)
    local xmlData = GetXmlTable(xmlFile)

    self.KingFishMap = {}
    local kf = xmlData.FishKing
    for _, v in ipairs(kf) do
        local ks = {}

        ks.nTypeID = tonumber(v.TypeID)
        ks.fProbability = tonumber(v.Probability)
        ks.nMaxScore = tonumber(v.MaxScore)
        ks.fCatchProbability = tonumber(v.CatchProbability)
        ks.fVisualScale = tonumber(v.VisualScale)
        ks.nVisualID = tonumber(v.VisualAttach)
        ks.nBoundingBox = tonumber(v.BoundingBox)
        ks.nLockLevel = tonumber(v.LockLevel)

        self.KingFishMap[ks.nTypeID] = ks
    end

    self.SanYuanFishMap = {}
    local kf = xmlData.FishSanYuan
    for _, v in ipairs(kf) do
        local ks = {}

        ks.nTypeID = tonumber(v.TypeID)
        ks.fProbability = tonumber(v.Probability)
        --        ks.nMaxScore = tonumber(v.MaxScore)
        --        ks.fCatchProbability = tonumber(v.CatchProbability)
        ks.fVisualScale = tonumber(v.VisualScale)
        ks.nVisualID = tonumber(v.VisualAttach)
        ks.nBoundingBox = tonumber(v.BoundingBox)
        ks.nLockLevel = tonumber(v.LockLevel)

        self.SanYuanFishMap[ks.nTypeID] = ks
    end

    self.SiXiFishMap = {}
    local kf = xmlData.FishSiXi
    for _, v in ipairs(kf) do
        local ks = {}

        ks.nTypeID = tonumber(v.TypeID)
        ks.fProbability = tonumber(v.Probability)
        --        ks.nMaxScore = tonumber(v.MaxScore)
        --        ks.fCatchProbability = tonumber(v.CatchProbability)
        ks.fVisualScale = tonumber(v.VisualScale)
        ks.nVisualID = tonumber(v.VisualAttach)
        ks.nBoundingBox = tonumber(v.BoundingBox)
        ks.nLockLevel = tonumber(v.LockLevel)

        self.SanYuanFishMap[ks.nTypeID] = ks
    end
end

function CGameConfig:AddBulletSet(bt)
    table.insert(self.BulletVector, bt)
end

function CGameConfig:ClearBulletSet()
    self.BulletVector = {}
end

function CGameConfig:GetBoundBoxConfigById(id)
    return self.BBXMap[id]
end

function CGameConfig:GetVisualConfigById(id)
    return self.VisualMap[id]
end

function CGameConfig:GetFishDataById(id)
    return self.FishMap[id]
end

function CGameConfig:ConvertCoortToCleientPosition(x, y)
    return x, self.nDefaultHeight - y
end

function CGameConfig:ConvertCoortToScreenSize(x, y)
    --    return test_width / self.nDefaultWidth * x, test_height / self.nDefaultHeight * y
    return 0.889 * x, 0.8 * y
end

function CGameConfig:SetMirror(b)
    self.m_bMirror = b
    fishgame.FishObjectManager:GetInstance():SetMirrowShow(b)
end

function CGameConfig:MirrorShow() return self.m_bMirror end

--self.m_bMirror end

function CGameConfig:ConvertMirrorCoord(x, y)
    return self.nDefaultWidth - x, self.nDefaultHeight - y
end

function CGameConfig:GetCannonPosition(chairId)
    local v = self.CannonPos[chairId]
    return v.m_Position.x_ * self.nDefaultWidth, self.nDefaultHeight * v.m_Position.y_
end

function CGameConfig:GetShowBingonPosition(chairId)
    local _chairId = chairId

    if self:MirrorShow() then
        if chairId == 0 then
            _chairId = 2
        elseif chairId == 1 then
            _chairId = 3
        elseif chairId == 2 then
            _chairId = 0
        elseif chairId == 3 then
            _chairId = 1
        end
    end

    local offset = 100
    if chairId == 2 or chairId == 3 then
        offset = -150
    end

    local v = self.CannonPos[_chairId]
    return v.m_Position.x_ * test_width, test_height * (1 - v.m_Position.y_) + offset
end

function CGameConfig:GetBulletInfoByMulriple(mul)
    return self.BulletVector[mul + 1]
end

function CGameConfig:GetCannonSetById(id)
    return self.CannonSetArray[id]
end

function CGameConfig:ConvertDirection(dir)
    if self:MirrorShow() then
        return dir + math.pi
    else
        return dir
    end
end

function CGameConfig:ConvertCoord(x, y)
    if self:MirrorShow() then
        x, y = self:ConvertMirrorCoord(x, y)
    end
    x, y = self:ConvertCoortToCleientPosition(x, y)
    x, y = self:ConvertCoortToScreenSize(x, y)

    return x, y
end

function CGameConfig:SetSwitchingScene(b) self.m_bSwitchingScene = b end

function CGameConfig:IsSwitchingScene() return self.m_bSwitchingScene end

return singleton(CGameConfig)