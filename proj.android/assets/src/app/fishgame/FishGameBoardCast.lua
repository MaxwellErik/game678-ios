local FishGameBoardCast = class("FishGameBoardCast", function()
    return display.newLayer()
end)

function FishGameBoardCast:ctor()
    GetControl()
    self:init()
end

function FishGameBoardCast:init()
    self.houseUI = ccs.GUIReader:getInstance():widgetFromJsonFile("fishgame2d/ui/eventBoardcast.json")
    self:addChild(self.houseUI, 128)

    function exitCallBack(sender, eventType)
        if eventType == ccui.TouchEventType.ended then
            PutControl()
            self:removeFromParent()
        end
    end

    local exit = ccui.Helper:seekWidgetByName(self.houseUI, "Button_Close")
    if exit then
        exit:addTouchEventListener(exitCallBack)
    end
end

return FishGameBoardCast 