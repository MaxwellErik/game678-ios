local TimeService = class("TimeService", {
    MAX_DIFFRENCE = 2000,
    SYNC_TIME = 3.0,
})

function TimeService:ctor()
    self:Clear()
end

function TimeService:Init()
    self:Clear()
end

function TimeService:Clear()
    self.m_startTime = socket.gettime()

    self.m_dwServerTick = self:GetClientTick()
    self.m_lDiffrenceTick = 0
    self.m_dwTrade = 0
    self.m_bReady = false
    self.m_nSyncTime = 0
end

function TimeService:TimerSync(client_tick, server_tick)
    server_tick = server_tick
    local now_tick = self:GetClientTick()

    self.m_dwTrade = (now_tick - client_tick) / 2
    self.m_dwServerTick = server_tick + self.m_dwTrade
    self.m_lDiffrenceTick = self.m_dwServerTick - now_tick
    self.m_bReady = true
end

function TimeService:GetDelayTick(packet_tick)
    if not self.m_bReady then return 0 end
    local delay = self:GetServerTick() - packet_tick;
    return delay;
end

-- 通过这个来获取时间，好与服务器对应时间格式，服务器用的是毫秒 --
function TimeService:GetClientTick()
    return math.floor((socket.gettime() - self.m_startTime) * 1000)
end

function TimeService:GetServerTick()
    return self:GetClientTick() + self.m_lDiffrenceTick;
end

function TimeService:SetSyncTime(t) self.m_nSyncTime = t + TimeService.SYNC_TIME end

function TimeService:GetSyncTime(t) return self.m_nSyncTime end

function TimeService:GetDiffrence() return self.m_lDiffrenceTick end

function TimeService:GetDelay() return self.m_dwTrade end

function TimeService:BeReady() return self.m_bReady end

return singleton(TimeService)