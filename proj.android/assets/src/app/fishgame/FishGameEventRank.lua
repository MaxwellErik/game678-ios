require("RankData")
require("ServerData")

local FishGameEventRank = class("FishGameEventRank", function()
    return display.newLayer()
end)

function FishGameEventRank:ctor()
    self.myUserId = getuserInfo().dwUserID
    GetControl()
    self:init()
end

function FishGameEventRank:init()
    self.houseUI = ccs.GUIReader:getInstance():widgetFromJsonFile("fishgame2d/ui/eventRank.json")
    self.cell = ccui.Helper:seekWidgetByName(self.houseUI, "Panel_20")
    self:addChild(self.houseUI, 128)

    function exitCallBack(sender, eventType)
        if eventType == ccui.TouchEventType.began then
            sender:setScale(1.2)
        else
            sender:setScale(1)
        end
       if eventType == ccui.TouchEventType.ended then
            PutControl()
            self:removeFromParent()
        end
    end

    local exit = ccui.Helper:seekWidgetByName(self.houseUI, "Button_Close")
    if exit then
        exit:addTouchEventListener(exitCallBack)
    end

    self.listView = ccui.Helper:seekWidgetByName(self.houseUI, "ListView_rank")
    self:refreshTab()
end

function FishGameEventRank:refreshTab()
    local isShow = {[1] = false, [2] = false, [3] = false}
    local hasShow = false
    local rankData = getRankData()
    for i = 1, 3 do
        local curRank = rankData[i]
        for k, v in pairs(curRank) do
            -- print("------>>>>k = "..k.."       " ..v.dwUserId.."      "..self.myUserId)
            if v.dwUserId == self.myUserId then
                -- print("##############  i = "..i)
                isShow[i] = true
            end
        end
    end

    function rankCallBack(sender, eventType)
        if eventType == ccui.TouchEventType.began then
            sender:setScale(1.2)
        else
            sender:setScale(1)
        end
        self:refreshList(sender.type)
    end

    local rankW = ccui.Helper:seekWidgetByName(self.houseUI, "Button_RankW")
    if rankW then
        if not isShow[3] then
            rankW:setVisible(false)
        else
            self:refreshList(3)
            hasShow = true
        end
        rankW.type = 3
        rankW:addTouchEventListener(rankCallBack)
    end
    local rankQ = ccui.Helper:seekWidgetByName(self.houseUI, "Button_RankQ")
    if rankQ then
        if not isShow[2] then
            rankQ:setVisible(false)
        elseif not hasShow then
            self:refreshList(2)
            hasShow = true
        end
        rankQ.type = 2
        rankQ:addTouchEventListener(rankCallBack)
    end
    local rankB = ccui.Helper:seekWidgetByName(self.houseUI, "Button_RankB")
    if rankB then
        if not isShow[1] then
            rankB:setVisible(false)
        elseif not hasShow then
            self:refreshList(1)
            hasShow = true
        end
        rankB.type = 1
        rankB:addTouchEventListener(rankCallBack)
    end

    if not hasShow then
        local noteLabel = cc.LabelTTF:create("您没有参加上一场比赛！", "", 40)
        noteLabel:setPosition(640, 360)

        self.houseUI:addChild(noteLabel)
        local myPlace = ccui.Helper:seekWidgetByName(self.houseUI, "Label_MyPlace")
        myPlace:setVisible(false)

        local myScore = ccui.Helper:seekWidgetByName(self.houseUI, "Label_MyScore")
        myScore:setVisible(false)
    end
end

function FishGameEventRank:refreshList(type)
    self.listView:removeAllChildren()

    local rankData = getRankData()
    local curRank = rankData[type]
    local name = {"百炮房第","千炮房第","万炮房第"}
  
    for i, rank in pairs(curRank) do
        if curRank[i].dwUserId == self.myUserId or i > 20 then
            local myPlace = ccui.Helper:seekWidgetByName(self.houseUI, "Label_MyPlace")
            myPlace:setString(tostring(curRank[i].dwRanking))

            local myScore = ccui.Helper:seekWidgetByName(self.houseUI, "Label_MyScore")
            myScore:setString(tostring(curRank[i].dwCatchScore))

            self.matchCount = cc.LabelTTF:create(name[type]..tostring(curRank[i].dwMatchCount).."场", "", 40)
            self.matchCount:setPosition(640, 80)
            self.houseUI:addChild(self.matchCount)
            if  i > 20 then
                break
            end
        end
        
        local cell_ = self.cell:clone()
        local place = ccui.Helper:seekWidgetByName(cell_, "Label_place")
        place:setString(tostring(i))

        local name = ccui.Helper:seekWidgetByName(cell_, "Label_nickName")
        name:setString(curRank[i].szNickName)

        local score = ccui.Helper:seekWidgetByName(cell_, "Label_score")
        score:setString(tostring(curRank[i].dwCatchScore))

        self.listView:pushBackCustomItem(cell_)
    end
end

return FishGameEventRank 