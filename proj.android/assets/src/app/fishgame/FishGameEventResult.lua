require("SelfRankData")

local FishGameEventResult = class("FishGameEventResult", function()
    return display.newLayer()
end)

rankList = {}

function FishGameEventResult:ctor()
    GetControl()
    self:init()
end

function FishGameEventResult:init()
    self.houseUI = ccs.GUIReader:getInstance():widgetFromJsonFile("fishgame2d/ui/eventResult.json")
    self.cell = ccui.Helper:seekWidgetByName(self.houseUI, "Panel_cell")
    self:addChild(self.houseUI, 128)

    function exitCallBack(sender, eventType)
        if eventType == ccui.TouchEventType.began then
            sender:setScale(1.2)
        else
            sender:setScale(1)
        end
        if eventType == ccui.TouchEventType.ended then
            PutControl()
            self:removeFromParent()
        end
    end

    local exit = ccui.Helper:seekWidgetByName(self.houseUI, "Button_Close")
    if exit then
        exit:addTouchEventListener(exitCallBack)
    end

    self.listView = ccui.Helper:seekWidgetByName(self.houseUI, "ListView_Rank")
    self:initButton()
    -- self:refreshTab()
end

function FishGameEventResult:initButton()
    local list_Game = ccui.Helper:seekWidgetByName(self.houseUI, "Panel_GameList")
    local label_CurGame = ccui.Helper:seekWidgetByName(self.houseUI, "Label_CurrentChose")

    local function gameChoseCallBack(sender, eventType)
        if eventType == ccui.TouchEventType.began then
            sender:setScale(1.1)
        else
            sender:setScale(1)
        end
        if eventType == ccui.TouchEventType.ended then
            list_Game:setVisible(false)
            local name = sender:getTitleText()
            label_CurGame:setString(name or "")
            self.CurType = sender.type
        end
    end

    for i = 1, 3 do
        local button_game = ccui.Helper:seekWidgetByName(list_Game,"Button_Game"..i) 
        if button_game then
            button_game.type = 4 - i
            button_game:addTouchEventListener(gameChoseCallBack)
        end
    end

    local downBtn = ccui.Helper:seekWidgetByName(self.houseUI, "Button_Down")
    if downBtn then
        downBtn:addTouchEventListener(function(sender, eventType)
            if eventType == ccui.TouchEventType.began then
                sender:setScale(1.2)
            else
                sender:setScale(1)
            end
            
            if eventType == ccui.TouchEventType.ended then
                local visible = not list_Game:isVisible()
                list_Game:setVisible(visible)
            end
        end)
    end

    local checkBtn = ccui.Helper:seekWidgetByName(self.houseUI, "Button_Check")
    if checkBtn then
         checkBtn:addTouchEventListener(function(sender, eventType)
            if eventType == ccui.TouchEventType.began then
                sender:setScale(1.2)
            else
                sender:setScale(1)
            end
           
            if eventType == ccui.TouchEventType.ended then

                if self.CurType then
                    self:refreshList()
                end
            end
        end)
    end
end



function FishGameEventResult:refreshList()
    self.listView:removeAllChildren()
    local rankData = getSelfRankData()
    local curRank = rankData[self.CurType]

    -- print("------>>>>"..#curRank)
    for i = 1, #curRank do
        local cell_ = self.cell:clone()
        local place = ccui.Helper:seekWidgetByName(cell_, "Label_place")
        place:setString(tostring(curRank[i].dwRanking))

        local name = ccui.Helper:seekWidgetByName(cell_, "Label_nickName")
        name:setString(app.userInfo.szNickName)

        local score = ccui.Helper:seekWidgetByName(cell_, "Label_score")
        score:setString(tostring(curRank[i].dwCatchScore))

        local time = ccui.Helper:seekWidgetByName(cell_, "Label_time")
        time:setString(tostring(curRank[i].szEndTime))

        self.listView:pushBackCustomItem(cell_)
    end
end
return FishGameEventResult 