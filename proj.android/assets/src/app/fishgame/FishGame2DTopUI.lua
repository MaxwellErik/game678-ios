local FishGame2DTopUI = CreateClass(UIObject);
--初始化自己，或者清理自己
function FishGame2DTopUI:clearOrInit()
	
	if self.threadKey then
		StopThread(self.threadKey);
	end
	self.threadKey = nil;
	self.fLogic = nil;
	self.checkLongTouch = false;--是否开始检测长按
	self.nCheckTime = 0;--检测的时间
	self.endlPos = nil;--最后一次点击的地点，在move的时候设置
end

--设置逻辑处理
function FishGame2DTopUI:setLogic(fLogic)
	self.fLogic = fLogic;
end

--设置加农炮的图片和等级
function FishGame2DTopUI:setCannonImage(cn)
	local p = cn.getPosition();
	local player = self:getWidgetByName("player_"..p);
	local can = GetWidgetByName(player,"cannon");
	local ccsNode = cn.createCannonAnimation();
	local size = can:getContentSize();
	ccsNode.setPosition(size.width/2,size.height/2);
	can:addChild(ccsNode.getArmature());
	
	local level = cn.getLevel();
	local cannonType = GetWidgetByName(player,"cannonType");
	cannonType:setString(tostring(level));
end

--更新指定玩家的金币和宝石数量
function FishGame2DTopUI:updatePlayerCoin(pl,coin,gem)
	local p = pl.getPosition();
	local player = self:getWidgetByName("player_"..p);
	local coinLabel = GetWidgetByName(player,"coinLabel");
	coinLabel:setString(tonumber(coin));
	local zuanshiLabel = GetWidgetByName(player,"zuanshiLabel");
	zuanshiLabel:setString(tostring(gem));
end


--请重写此方法，用于将这个UI设置为不可见，当有新的全屏UI加载进入的时候，引擎会主动调用此方法
function FishGame2DTopUI:backGround()
	
end

--请重写此方法，用于，当这个界面重新出现在最顶层的时候需要处理的事情，例如恢复显示，播放音乐，等
function FishGame2DTopUI:frontGround()
	
end

function FishGame2DTopUI:initNewBulletPosition(bl,position)
	
	local player = self:getWidgetByName("player_"..position);--@TODO:需要修改
	local tm = GetWidgetByName(player,"tm");--获得炮口的那个空白图片
	local size = tm:getContentSize();
	local cp = cc.p(size.width/2,size.height/2);
	cp = tm:convertToWorldSpace(cp);--获取到这个点转换到屏幕上的坐标
	bl.setPosition(cp.x,cp.y);
end

--更新炮台的旋转角度，指定位置
function FishGame2DTopUI:updateCannonRotationToPosition(position,endPos)
	local player = self:getWidgetByName("player_"..position);--@TODO:需要修改
	local cannon = GetWidgetByName(player,"cannon");
	local size = cannon:getContentSize();
	local cp = cc.p(size.width/2,0);--坐标点在中下
	cp = cannon:convertToWorldSpace(cp);
	--计算两点之间的夹角
	local dx = endPos.x - cp.x;
	local dy = endPos.y - cp.y;
	local dis = math.sqrt(math.pow(dx,2)+math.pow(dy,2));--斜边长度
	local tan0 = dy/dx;
	
	local cos0 = dx/dis;
	local rad = math.acos(cos0);
	local angle = math.deg(rad);
	if position == Cannon_Position_Left then
		rad = math.atan(tan0);
		angle = math.deg(rad);
		angle = -angle;
	elseif position == Cannon_Position_Right then
		rad = math.atan(tan0);
		angle = math.deg(rad);
	else
		angle = 90-angle;--炮口朝上
	end
	
	cannon:setRotation(angle);
	
	self.fLogic.updatePlayerCannonAngle(position,angle);--更新这个玩家的加农炮的旋转坐标
end

--系统主动回调的方法，每帧调用的定时器
function FishGame2DTopUI:run(dt)
	if self.checkLongTouch then
		self.nCheckTime = self.nCheckTime + dt;
		if self.nCheckTime >= LONGTOUCH_CKECK_DURING then
			self.nCheckTime = 0;
			local endPos = self.endlPos;
			self:onFire(MyPosition,endPos);
		end
	end
end


function FishGame2DTopUI:onFire(mineP,endPos)
	
	
	self:updateCannonRotationToPosition(mineP,endPos);
	local fLogic = self.fLogic;
	local bl = fLogic.playerFireTo(mineP,endPos);
	if bl then
		self:initNewBulletPosition(bl,mineP);
	end
	
end

--请重写此方法，在这个方法中，请初始化自己的界面，动画，对象，数据等
function FishGame2DTopUI:initSelf(scene)
	self:clearOrInit();
	local panel = self:getWidgetByName("Panel_1");
	--点击事件的回调函数
	local function PanelTouchBegan(pSener, ty)
		local mineP = MyPosition;--设置我的位置。随便乱来的
		local endPos = cc.p(0,0);
		if ty == 2 then
			endPos = pSener:getTouchEndPosition();--获取到最后点击的位置，像这个位置发送一颗子弹
			self.checkLongTouch = false;
			self.endlPos = nil;
		end
		
		if ty == 0 then
			--玩家点下
			endPos = pSener:getTouchBeganPosition();--获取到最后点击的位置，像这个位置发送一颗子弹
			self.checkLongTouch = true;
			self.nCheckTime = 0;
			self.endlPos = endPos;
		end
		
		if ty == 1 then
			--玩家拖动手指
			endPos = pSener:getTouchMovePosition();--获取到最后点击的位置，像这个位置发送一颗子弹
			self.endlPos = endPos;
		end
		self:onFire(mineP,endPos);
	end
	panel:addTouchEventListener(PanelTouchBegan);
end




--设置指定位置的炮台是否可见(同时可以更新金币和gem数量)
function FishGame2DTopUI:setCannonVisible(p,v,c,gem)
	local player = self:getWidgetByName("player_"..p);
	local waiting = GetWidgetByName(player,"waiting");
	local cannon = GetWidgetByName(player,"cannonT");
	if v then
		waiting:setVisible(false);
		cannon:setVisible(true);
	else
		waiting:setVisible(true);
		cannon:setVisible(false);
	end
	
	if c then
		local coin = GetWidgetByName(player,"coinLabel");
		coin:setString(tostring(c));
	end
	
	if gem then
		local zuanshiLabel = GetWidgetByName(player,"zuanshiLabel");
		zuanshiLabel:setString(tostring(gem));
	end
end


--协程
function FishGame2DTopUI.update(self)
	while true do
		
		WaitFrame();
	end
	
end

--重写此方法，用于退出该界面的时候清理自己定义的变量，内存等
function FishGame2DTopUI:clearSelf()
	
end

return FishGame2DTopUI;