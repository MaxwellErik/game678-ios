local MyComponent = class("MyComponent",{})

MyComponent.m_pOwner = nil
MyComponent.m_id = -1

function MyComponent:ctor() end

function MyComponent:SetID(id) self.m_id = id end
function MyComponent:GetID() return self.m_id end

function MyComponent:GetFamilyID()
    return math.floor(self.m_id / 2 ^ 8)
end

function MyComponent:OnAttach() end

-- 附加到对象后被调用
function MyComponent:OnDetach() end

-- 从对象移出前被调用
function MyComponent:OnUpdate() end

-- 响应时间流逝
function MyComponent:OnCCEvent() end

-- 响应组件消息
function MyComponent:SetOwner(owner) self.m_pOwner = owner end

function MyComponent:GetOwner() return self.m_pOwner end

function MyComponent:RaiseEvent() end

function MyComponent:PostEvent() end

return MyComponent