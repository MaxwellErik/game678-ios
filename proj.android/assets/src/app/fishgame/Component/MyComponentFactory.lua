local MyComponentFactory = class("MyComponentFactory",{})

function MyComponentFactory:ctor()
    self.mFactory = {}
end

function MyComponentFactory:CreateComponent(soc_id)
    local _class = self.mFactory[soc_id]
    if _class then
        local instance = _class.new()
        instance:SetID(soc_id)
        return instance
    end
end

function MyComponentFactory:register(soc_id, _class)
    self.mFactory[soc_id] = _class
end

return singleton(MyComponentFactory)