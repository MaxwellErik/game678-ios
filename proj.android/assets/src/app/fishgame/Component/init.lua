import(".Effect.init")
import(".Buff.init")
CComEvent = import(".CComEvent")


MyComponentType = {
    ECF_NONE = 0,
    ECF_MOVE = 1, --移动组件
    ECF_VISUAL = 2, --可视化组件
    ECF_EFFECTMGR = 3, --死亡效果管理器
    ECF_BUFFERMGR = 4, -- BUFF 管理器
}

MyComponentMovementType = {
    EMCT_PATH = 2 ^ 8, -- 按路径移动
    EMCT_DIRECTION = 2 ^ 8 + 1, -- 按指定方向移动
    EMCT_TARGET = 2 ^ 8 + 2, -- 向目标方向移动
}

EEffectMgrComType = {
    EECT_MGR = MyComponentType.ECF_EFFECTMGR * 2 ^ 8
}

BufferMgrComType = {
    EBCT_BUFFERMGR = MyComponentType.ECF_BUFFERMGR * 2 ^ 8
}

local MyComponentFactory = import(".MyComponentFactory")
local MyCommonentBuff = import(".MyCommonentBuff")
local MyComponentEffect = import(".MyComponentEffect")

MyComponentFactory.GetInstance():register(BufferMgrComType.EBCT_BUFFERMGR, MyCommonentBuff)
MyComponentFactory.GetInstance():register(EEffectMgrComType.EECT_MGR, MyComponentEffect)

