local MyComponent = import(".MyComponent")
local MyCommonentBuff = class("MyCommonentBuff", MyComponent)

function MyCommonentBuff:ctor()
    MyCommonentBuff.super.ctor(self)
    self.m_Buffers = {} --std::list< CBuffer* >
end

function MyCommonentBuff:GetFamilyID() return MyComponentType.ECF_BUFFERMGR end

function MyCommonentBuff:Add(bty, par, tim)
    local pBuff = BuffFactory.GetInstance():CreateBuff(bty)
    if pBuff then
        pBuff:SetParam(par)
        pBuff:SetLife(tim)
        pBuff:SetOwner(self:GetOwner())
        pBuff:Init()

        table.insert(self.m_Buffers, pBuff)
    end
end

function MyCommonentBuff:HasBuffer(byt)
    for _, v in ipairs(self.m_Buffers) do
        if v.GetType() == byt then
            return v
        end
    end
end

function MyCommonentBuff:OnUpdate(dt)
    local i = 1
    while i <= #self.m_Buffers do
        local v = self.m_Buffers[i]
        if not v:OnUpdate(dt) then
            v:Clear()
            table.remove(self.m_Buffers, i)
        else
            i = i + 1
        end
    end
end

function MyCommonentBuff:OnCCEvent(pEvent)
    for _,v in ipairs(self.m_Buffers) do
        v:OnCCEvent(pEvent)
    end
end

function MyCommonentBuff:OnDetach(byt) end

return MyCommonentBuff