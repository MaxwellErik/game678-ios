local CComEvent = class("CComEvent")

function CComEvent:ctor(_id, _sender, _param1, _param2,_param3)
    self.id_ = _id
    self.sender_ = _sender
    self.param1_ = _param1
    self.param2_ = _param2
    self.param3_ = _param3
end

function CComEvent:SetID(n) self.id_ = n end

function CComEvent:GetID() return self.id_ end

function CComEvent:SetParam1(n) self.param1_ = n end

function CComEvent:GetParam1() return self.param1_ end

function CComEvent:SetParam2(n) self.param2_ = n end

function CComEvent:GetParam2() return self.param2_ end
function CComEvent:SetParam3(n) self.param3_ = n end
function CComEvent:GetParam3() return self.param3_ end


return CComEvent