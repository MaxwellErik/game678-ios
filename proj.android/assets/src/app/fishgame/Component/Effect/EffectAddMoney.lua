local Effect = import(".Effect")

-- 增加金币
-- 参数１为０时表示增加固定的金币数，参数２表示钱数
-- 参数１为１时表示增加一定倍数的钱数，参数２表示倍数
local EffectAddMoney = class("EffectAddMoney", Effect)

function EffectAddMoney:ctor()
    EffectAddMoney.super.ctor(self)

    self:SetEffectType(EffectType.ETP_ADDMONEY)

    self.m_nParamCount = 3

    self.lSco = 0
end

function EffectAddMoney:Execute(pSelf, pTarget, list, bPretreating)
    if (pSelf == nil) then return 0, list end

    local lScore = 0;
    local mul = 1;

    if (self.lSco == 0) then
        self.lSco = self:GetParam(2) > self:GetParam(1) and math.random(self:GetParam(1), self:GetParam(2)) or self:GetParam(1);
    end

    if (self:GetParam(0) == 0) then
        mul = 1;
    elseif (pTarget) then
        mul = pTarget:GetScore();
    end

    local n = -1;
--    pSelf:ProcessCCEvent(EME_QUERY_ADDMUL, nil, 0, n, function(_n)
--        n = _n
--    end);

    if (n ~= -1) then
        self.lSco = CGameConfig.GetInstance().nAddMulBegin;
    end

    if (n + self.lSco > self:GetParam(2)) then
        n = self:GetParam(2) - self.lSco;
    end

    if (not bPretreating) then
        CGameConfig.GetInstance().nAddMulCur = 0;
    else
        n = 0;
    end

    lScore = (self.lSco + n) * mul;

    if pTarget.__cname == "Bullet" and pTarget:bDouble() then
        lScore = lScore * 2;
    end

    return lScore, list;
end

return EffectAddMoney