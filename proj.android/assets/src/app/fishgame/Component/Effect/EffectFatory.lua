local EffectFatory = class("EffectFatory", {})

function EffectFatory:ctor()
    self.m_effectFactory = {}
end

function EffectFatory:CreateEffect(_effectType)
    local _class = self.m_effectFactory[_effectType]
    if _class then
        return _class.new()
    end
end

function EffectFatory:Register(_effectType, _class)
    self.m_effectFactory[_effectType] = _class
end

function EffectFatory:Clear()
    self.m_effectFactory = {}
end

return singleton(EffectFatory)