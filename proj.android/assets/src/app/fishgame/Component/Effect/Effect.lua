local Effect = class("Effect", {})

function Effect:ctor()
    self.m_pParam = {}
    self.m_nParamCount = 2
    self.m_nType = EffectType.ETF_NONE
end

function Effect:GetEffectType() return self.m_nType end

function Effect:SetEffectType(_t) self.m_nType = _t end

function Effect:GetParam(_pos) return self.m_pParam[_pos] or 0 end

function Effect:SetParam(_pos, _val) self.m_pParam[_pos] = _val end

function Effect:GetParamSize()
    return self.m_nParamCount
end

function Effect:Execute()
    print("Effect:Execute 必须被继承并重写")
end

return Effect