EffectType = {
    ETF_NONE = -1,
    ETP_ADDMONEY = 0, --增加金币
    ETP_KILL = 1, --杀死其它鱼
    ETP_ADDBUFFER = 2, --增加BUFFER
    ETP_PRODUCE = 3, --生成其它鱼
    ETP_BLACKWATER = 4, --乌贼喷墨汁效果
    ETP_AWARD = 5, --抽奖
}


local EffectFatory = import(".EffectFatory")
local EffectAddMoney = import(".EffectAddMoney")
local EffectKill = import(".EffectKill")
local EffectAddBuffer = import(".EffectAddBuffer")
local EffectProduce = import(".EffectProduce")
local EffectBlackWater = import(".EffectBlackWater")
local EffectAward = import(".EffectAward")

EffectFatory.GetInstance():Register(EffectType.ETP_ADDMONEY, EffectAddMoney)
EffectFatory.GetInstance():Register(EffectType.ETP_KILL, EffectKill)
EffectFatory.GetInstance():Register(EffectType.ETP_ADDBUFFER, EffectAddBuffer)
EffectFatory.GetInstance():Register(EffectType.ETP_PRODUCE, EffectProduce)
EffectFatory.GetInstance():Register(EffectType.ETP_BLACKWATER, EffectBlackWater)
EffectFatory.GetInstance():Register(EffectType.ETP_AWARD, EffectAward)


import(".Jetton")
