local Effect = import(".Effect")

-- 杀死杀死其它鱼
-- 参数１为０时表示杀死全部的鱼
-- 参数１为１时表示杀死指定范围内的鱼，参数２表示半径
-- 参数１为２时表示杀死指定类型的鱼，参数２表示指定类型
-- 参数１为３时表示杀死同一批次刷出来的鱼。
local EffectKill = class("EffectKill", Effect)

function EffectKill:ctor()
    EffectKill.super.ctor(self)

    self:SetEffectType(EffectType.ETP_KILL)

    self.m_nParamCount = 3
end

function EffectKill:Execute(pSelf, pTarget, list, bPretreating)
    if pSelf == nil then return 0, list end

    local score = 0
    if not bPretreating then
        RaiseEvent("AddChain", self, pSelf);
    end

    local fishes = fishgame.FishObjectManager:GetInstance():GetAllFishes()
    for k, v in ipairs(fishes) do
        if v:GetId() ~= pSelf:GetId()
                and v:GetState() < EOS_DEAD
        then
            local param_0 = self:GetParam(0)
            if param_0 == 0 and v:InSideScreen() and v:GetMoveCompent():HasBeginMove() then
                -- 参数１为０时表示杀死全部的鱼
                local _score, _list = v:ExecuteEffects(pTarget, list, bPretreating);
                score = score + _score
                list = _list

                app.eventDispather:dispatherEvent(FISHGAME2D_EVENT.ADD_CHAIN, {
                    start_x = pSelf:GetPosition().x,
                    start_y = pSelf:GetPosition().y,
                    end_x = v:GetPosition().x,
                    end_y = v:GetPosition().y,
                    type = E_Red
                })
            elseif param_0 == 1 and v:InSideScreen() and v:GetMoveCompent():HasBeginMove() then
                -- 参数１为１时表示杀死指定范围内的鱼，参数２表示半径
                if CMathAide.CalcDistance(pSelf:GetPosition().x,
                    pSelf:GetPosition().y,
                    v:GetPosition().x,
                    v:GetPosition().y) < self:GetParam(1)
                then
                    local _score, _list = v:ExecuteEffects(pTarget, list, bPretreating);
                    score = score + _score
                    list = _list

                    app.eventDispather:dispatherEvent(FISHGAME2D_EVENT.ADD_CHAIN, {
                        start_x = pSelf:GetPosition().x,
                        start_y = pSelf:GetPosition().y,
                        end_x = v:GetPosition().x,
                        end_y = v:GetPosition().y,
                        type = E_Red
                    })
                end
            elseif param_0 == 2 and v:InSideScreen() and v:GetMoveCompent():HasBeginMove() then
                -- 参数１为２时表示杀死指定类型的鱼，参数２表示指定类型
                if self:GetParam(1) == v:GetTypeID()
                        and v:GetFishType() == SpecialFishType.ESFT_NORMAL
                then
                    local _score, _list = v:ExecuteEffects(pTarget, list, bPretreating);
                    score = score + _score
                    list = _list

                    app.eventDispather:dispatherEvent(FISHGAME2D_EVENT.ADD_CHAIN, {
                        start_x = pSelf:GetPosition().x,
                        start_y = pSelf:GetPosition().y,
                        end_x = v:GetPosition().x,
                        end_y = v:GetPosition().y,
                        type = E_Blue
                    })
                end
            elseif param_0 == 3 and v:InSideScreen() and v:GetMoveCompent():HasBeginMove() then
                -- 参数１为３时表示杀死同一批次刷出来的鱼。
                if v:GetRefershID() == pSelf:GetRefershID() then
                    local _score, _list = v:ExecuteEffects(pTarget, list, bPretreating);
                    score = score + _score
                    list = _list

                    app.eventDispather:dispatherEvent(FISHGAME2D_EVENT.ADD_CHAIN, {
                        start_x = pSelf:GetPosition().x,
                        start_y = pSelf:GetPosition().y,
                        end_x = v:GetPosition().x,
                        end_y = v:GetPosition().y,
                        type = E_Light
                    })
                end
            end
            --            end
        end
    end

    if (score / pTarget:GetScore() > self:GetParam(2)) then
        score = pTarget:GetScore() * self:GetParam(2);
    end

    return score, list
end

return EffectKill