local Buff = import(".Buff")

local BuffAddMulByHit = class("BuffAddMulByHit", Buff)

function BuffAddMulByHit:ctor()
    BuffAddMulByHit.super.ctor(self)

    self:SetType(BuffType.EBT_ADDMUL_BYHIT)

    self.m_param = 0
end

function BuffAddMulByHit:Init()
    self.nCurMul = CGameConfig.GetInstance().nAddMulCur;
end

function BuffAddMulByHit:Clear()
    if not tolua.isnull(self.m_labelScore) then
        self.m_labelScore:removeSelf()
    end
    self.nCurMul = CGameConfig.GetInstance().nAddMulCur;
end

function BuffAddMulByHit:OnUpdate(...)
    if tolua.isnull(self.m_labelScore) then
        local owner = self:GetOwner()
        if owner then
            local labelScore = cc.LabelAtlas:_create("0123456789", "fishgame2d/ui/gold_hit_numbers.png", 39, 50, 48)
            labelScore:align(display.CENTER, 0, 0)

            local width, height = self:GetOwner():GetContentSize()

            table.insert(owner:GetCNodeList(), {
                scale = 1,
                offsetX = 0,
                offsetY = height / 2,
                direction = 0,
                ccsNode = labelScore,
            })

            FishGame2DLogic.GetInstance()._fishGame2DScene:AddEffectUp(labelScore, "gold_hit_numbers.png")
            self.m_labelScore = labelScore
        end
    end
    self.m_labelScore:setString(self:GetOwner():GetScore())

    return BuffAddMulByHit.super.OnUpdate(self, ...)
end

function BuffAddMulByHit:OnCCEvent(pEvent)
    if not pEvent then return end
    if pEvent:GetID() == EME_STATE_CHANGED and pEvent:GetParam1() == EOS_HIT then
        local pBullet = pEvent:GetParam2();
        if pBullet ~= nil and pBullet:GetScore() == CGameConfig.GetInstance().m_MaxCannon then
            self.nCurMul = self.nCurMul + 1
            CGameConfig.GetInstance().nAddMulCur = CGameConfig.GetInstance().nAddMulCur + 1

            RaiseEvent("FishMulChange", self.m_pOwner);
        end
    elseif pEvent:GetID() == EME_QUERY_ADDMUL then
        self.nCurMul = math.max(self.nCurMul, CGameConfig.GetInstance().nAddMulCur);
        local pMul = pEvent:GetParam2();
        local callback = pEvent:GetParam3();
        if callback then
            callback(self.nCurMul)
        end
    end
end

return BuffAddMulByHit