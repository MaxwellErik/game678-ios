local Buff = import(".Buff")

local BuffCIonCannon = class("BuffCIonCannon",Buff)

function BuffCIonCannon:ctor()
    BuffCIonCannon.super.ctor(self)

    self:SetType(BuffType.EBT_ION_CANNON)
end


function BuffCIonCannon:Clear()
    local pOwner = self.m_pOwner;
    if (pOwner) then

        local n = pOwner:GetCannonSetType();
        if (n < #CGameConfig.GetInstance().CannonSetArray) then
            n = CGameConfig.GetInstance().CannonSetArray[n].nNormalID;
        end
        pOwner:SetCannonSetType(n);
        --RaiseEvent("CannonSetChanaged", pOwner);

    end
end

function BuffCIonCannon:SetOwner(pOwner)
    BuffCIonCannon.super.SetOwner(self,pOwner)

    if (pOwner) then
        local n = pOwner:GetCannonSetType();
        if (n < #CGameConfig.GetInstance().CannonSetArray) then
            n = CGameConfig.GetInstance().CannonSetArray[n].nIonID;
        end
        pOwner:SetCannonSetType(n);
        --        RaiseEvent("CannonSetChanaged", pOwner);
    end
end

return BuffCIonCannon