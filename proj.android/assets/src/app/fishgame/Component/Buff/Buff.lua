local Buffer = class("Buffer", {})

function Buffer:ctor()
    self.m_BTP = BuffType.EBT_NONE
    self.m_fLife = 0
    self.m_param = 0
    self.m_pOwner = nil
end

function Buffer:GetOwner() return self.m_pOwner end
function Buffer:SetOwner(n) self.m_pOwner  = n end

function Buffer:GetType() return self.m_BTP end

function Buffer:SetType(b) self.m_BTP = b end

function Buffer:GetLife() return self.m_fLife end

function Buffer:SetLife(f) self.m_fLife = f end

function Buffer:OnUpdate(dt)
    if self.m_fLife > 0.0 then
        self.m_fLife = self.m_fLife - dt
    end
    return self.m_fLife == -1.0 or self.m_fLife > 0.0
end

function Buffer:SetParam(p) self.m_param = p end

function Buffer:Clear() end
function Buffer:Init() end

return Buffer
