local Buff = import(".Buff")

local BuffDoubleCannon = class("BuffDoubleCannon", Buff)

function BuffDoubleCannon:ctor()
    BuffDoubleCannon.super.ctor(self)

    self:SetType(BuffType.EBT_DOUBLE_CANNON)
end

function BuffDoubleCannon:Clear()
    local pOwner = self.m_pOwner;
    if (pOwner) then

        local n = pOwner:GetCannonSetType();
        if (n < #CGameConfig.GetInstance().CannonSetArray) then
            n = CGameConfig.GetInstance().CannonSetArray[n].nNormalID;
        end
        pOwner:SetCannonSetType(n);
        RaiseEvent("CannonSetChanaged", pOwner);

    end
end

function BuffDoubleCannon:SetOwner(pOwner)
    BuffDoubleCannon.super.SetOwner(self,pOwner)

    if (pOwner) then
        local n = pOwner:GetCannonSetType();

        if (n < #CGameConfig.GetInstance().CannonSetArray) then
            n = CGameConfig.GetInstance().CannonSetArray[n].nDoubleID;
        end
        pOwner:SetCannonSetType(n);
        RaiseEvent("CannonSetChanaged", pOwner);
    end
end

return BuffDoubleCannon