--玩家类的封装
function Player()
	local pl = {};
	
	function pl.clear()
		pl.key = nil;
		pl.position = -1;
		
		pl.coinNum = 0;--玩家的金币数量
		pl.gemNum = 0;--玩家的宝石数量
		pl.isMianPlayer = false;--是不是玩家自己
		pl.lockedFishID = -1;--锁定的鱼的ID（当玩家使用锁定技能的时候设置这个参数）
		if pl.cn then
			pl.cn.clear();
		end
		pl.cn = nil;--玩家的加农炮
		
	end
	
	--初始化玩家的key和位置
	function pl.init(key,position)
		pl.clear();
		pl.key = key;
		pl.position = position;
	end
	
	function pl.update(dt)
		local cn = pl.cn;
		if cn then
			cn.update(dt);
		end
	end
	
	--初始化玩家的加农炮
	function pl.initConnon(ty)
		local angle = 0;
		if pl.cn then
			local cn = pl.cn;
			angle = cn.getCannonAngle();
			pl.cn.clear();
		end
		pl.cn = nil;
		local cn = Cannon();
		cn.init(ty,pl.position);
		cn.setCannonAngle(angle);
		pl.cn = cn;
		return cn;
	end
	
	--获取玩家的加农炮
	function pl.getCannon()
		return pl.cn;
	end
	--获取玩家的位置
	function pl.getPosition()
		return pl.position;
	end
	--玩家像某个地方开火
	function pl.fireToPosition(p)
		local cn = pl.cn;
		if cn then
			return cn.fireToPosition(p);
		end
		return nil;
	end
	
	--像某条鱼开火
	function pl.fireToTargetFish(fh)
		local cn = pl.cn;
		if cn then
			return cn.fireToTargetFish(fh);
		end
		return nil;
	end
	
	--更新这个玩家 加农炮的旋转角度
	function pl.updateCannonAngle(angle)
		local cn = pl.cn;
		if cn then
			cn.setCannonAngle(angle);
		end
	end
	
	return pl;
end

--加农炮设定
function Cannon()
	local cn = {};
	
	function cn.clear()
		cn.cost = 0;--每一发炮弹需要的金币数量
		cn.bulletType = -1;--当前加农炮发射的子弹类型
		cn.ty = -1;--当前加农炮的类型
		cn.usingImage = nil;--当前加农炮使用的图片
		cn.userPk = -1;--使用者的pk
		cn.position = -1;--加农炮的位置
		cn.level = -1;--加农炮的等级
		cn.angle = 0;--当前加农炮的旋转角度
		cn.fireStepMax = 0.15;
		cn.nFireStepMax = 0;
		if cn.ccsNode then
			cn.ccsNode.clear();
		end
		cn.ccsNode = nil;
	end
	
	
	function cn.update(dt)
		if cn.nFireStepMax > 0 then
			cn.nFireStepMax = cn.nFireStepMax - dt;
			if cn.nFireStepMax <= 0 then
				cn.nFireStepMax = 0;
			end
		end
	end
	
	--设置加农炮的角度
	function cn.setCannonAngle(ang)
		cn.angle = ang;
	end
	
	--获取加农炮的旋转角度
	function cn.getCannonAngle()
		return cn.angle;
	end
	
	--生成Cannon
	function cn.init(ty,position)
		cn.clear();
		cn.bulletType = 1;
		cn.cost = 50;
		cn.ty = ty;
		cn.usingImage = "fish_paotai";
		cn.position = position;
		cn.level = 2;
		
	end
	
	function cn.getCost()
		return cn.cost;
	end
	
	function cn.getBulletType()
		return cn.bulletType;
	end
	
	function cn.getUsingImage()
		return cn.usingImage;
	end
	
	function cn.createCannonAnimation()
		if cn.ccsNode then
			return cn.ccsNode;
		end
		local path = cn.getUsingImage();
		local ccsFactory = GetCCSFactory();
		local ccsNode = ccsFactory.createCCSAniamtion(path);
		cn.ccsNode = ccsNode;
		return ccsNode;
	end
	
	--获取加农炮的位置
	function cn.getPosition()
		return cn.position;
	end
	
	--获取加农炮的等级
	function cn.getLevel()
		return cn.level;
	end
	
	--初始化一个子弹
	function cn.createBullet(p,fh)
		if cn.nFireStepMax > 0 then
			return nil;
		end
		cn.nFireStepMax = cn.fireStepMax;
		if cn.ccsNode then
			cn.ccsNode.playWithName("move");
		end
		local bl = Bullet();
		if p then
			bl.initWithTargetPosition(cn.bulletType,p);
			if cn.position == Cannnon_Position_Down_1 or cn.position == Cannnon_Position_Down_2 then
				bl.setRotation(cn.angle);
			elseif cn.position == Cannnon_Position_Up_1 or cn.position == Cannnon_Position_Up_2 then
				--上方的炮台的位置需要转换
				bl.setRotation(180-cn.angle);
			elseif cn.position == Cannon_Position_Left then
				bl.setRotation(90+cn.angle);
			else
				bl.setRotation(-cn.angle-90);
			end
		end
		if fh then
			bl.initWithTargetFish(cn.bulletType,fh);
		end
		return bl;
	end
	
	--像目标开火,返回一颗子弹
	function cn.fireToPosition(p)
		return cn.createBullet(p);
	end
	
	--像指定的某条鱼开火
	function cn.fireToTargetFish(fh)
		return cn.createBullet(nil,fh);
	end
	
	return cn;
end

--子弹
function Bullet()
	local bl = {};
	
	--清理
	function bl.clear()
		bl.ty = -1;
		bl.isAnimation = true;
		bl.resPath = "bullet1";
		if bl.fbx then--包围盒
			bl.fbx.clear();
		end
		if bl.node then
			if bl.isAnimation then
				local ccsNode = bl.node;
				ccsNode.clear();
			else
				bl.node:release();
				bl.node:removeFromParent(true);
			end
		end
		bl.node = nil;
		bl.targetPos = nil;--指向某个地点（第一次计算出速度之后，就不在改变这个方向，一直飞出去）
		bl.fh = nil;--锁定的鱼
		bl.speed = 20;--子弹的移动速度
		bl.angle = 0;--子弹发射角度（只有没有锁定目标的时候才生效，用于撞墙反弹等）
		
		bl.fbx = nil;
		bl.size = nil;
		bl.rad = 12;
		
		bl.deadAni = "fish_wang";--死亡的时候播放特效
		bl.isDead = false;--是否已经死亡
	end
	
	function bl.initBulletNode()
		if bl.isAnimation then
			local ccsFactory = GetCCSFactory();
			local ccsNode = ccsFactory.createCCSAniamtion(bl.resPath);
			ccsNode.playWithName("move");
			bl.node = ccsNode;
		else
			local sp = cc.Sprite:create(bl.resPath);
			sp:retain();
			bl.node = sp;
		end
		
		local fbx = FishGame2DBox();
		local rad = bl.rad;
		fbx.init(0,0,rad);
		bl.fbx = fbx;
		
		local size = nil;
		if bl.isAnimation then
			local ccsNode = bl.node;
			size = ccsNode.getContentSize();
		else
			size =  bl.node:getContentSize();
		end
		bl.size = size;
		
		if FISHGAME_TEST_DRAW then
			
			local node = fbx.getNode();
			node:setPosition(0,size.height/2-rad);
			if bl.isAnimation then
				local ccsNode = bl.node;
				ccsNode.addChild(node,1);
			else
				bl.node:addChild(node,1);
			end
		end
	end
	
	--返回一个可以直接加载到场景中的对象
	function bl.getNode()
		if bl.isAnimation then
			local ccsNode = bl.node;
			local ar = ccsNode.getArmature();
			return ar;
		end
		return bl.node;
	end
	
	--设置坐标
	function bl.setPosition(x,y)
		if bl.isAnimation then
			local ccsNode = bl.node;
			ccsNode.setPosition(x,y);
		else
			bl.node:setPosition(x,y);
		end
	end
	
	function bl.getPosition()
		local px,py;--当前坐标
		
		if bl.isAnimation then
			local ccsNode = bl.node;
			px = ccsNode.getPosition("x");
			py = ccsNode.getPosition("y");
		else
			local node = bl.node;
			px = node:getPositionX();
			py = node:getPositionY();
		end
		local cp = cc.p(px,py);
		return cp;
	end
	
	--设置旋转角度
	function bl.setRotation(angle)
		bl.angle = angle;
		if bl.isAnimation then
			local ccsNode = bl.node;
			ccsNode.setRotation(angle);
		else
			bl.node:setRotation(angle);
		end
	end
	
	--初始化，向某点发射
	function bl.initWithTargetPosition(ty,p)
		bl.clear();
		bl.ty = ty;
		bl.initBulletNode();
		bl.targetPos = p;
		--计算子弹朝向
	end
	
	--初始化，向某条鱼发射
	function bl.initWithTargetFish(ty,fh)
		bl.clear();
		bl.ty = ty;
		bl.initBulletNode();
		bl.fh = fh;
	end
	
	--没有锁定鱼的子弹，每帧更新
	function bl.updateNoneTagetFish()
		local angle = bl.angle;--获取当前旋转的角度
		local speed = bl.speed;
		
		local cos0 = math.cos(math.rad(angle));
		local ySpeed = cos0 * speed;
		local sin0 = math.sin(math.rad(angle));
		local xSpeed = sin0 * speed;
		if angle == 0 or angle == 180 or angle == 360 then
			xSpeed = 0;
		end
		
		if angle == 90 or angle == 270 then
			ySpeed = 0;
		end
		
--		Log("angle = "..angle..",xSpeed = "..xSpeed..",ySpeed = "..ySpeed);
		local px,py;--当前坐标
		
		local cp = bl.getPosition();
		px = cp.x;
		py = cp.y;
		
		px = px + xSpeed;
		py = py + ySpeed;
		
		bl.setPosition(px,py);
		bl.contactToWall(xSpeed,ySpeed);
	end
	
	--检查是否撞墙
	function bl.contactToWall(xSpeed,ySpeed)
		local angle = bl.angle;
		local cp = bl.getPosition();--获取当前子弹的位置
		local w = CONFIG_SCREEN_WIDTH;
		local h = CONFIG_SCREEN_HEIGHT;
		
		if(xSpeed + cp.x >= w) then--撞到右边的墙
			if ySpeed > 0 then
				--向上跑
--				angle = angle - 90;
				angle = 360 - angle;
			else
--				angle = angle + 90;
				angle = 360 - angle;
			end
		end
		
		if(xSpeed + cp.x <= 0) then--撞到左边的墙
			if ySpeed > 0 then
--				angle = angle + 90;
				angle = 360 - angle;
			else
--				angle = angle - 90;
				angle = 360 - angle;
			end
		end
		
		--撞到上面的墙
		if (ySpeed + cp.y >= h) then
			if xSpeed > 0 then
--				angle = angle + 90;
				angle = angle + (90-angle) * 2;
			else
				if xSpeed == 0 then
					angle = 179;
				else
					local seta = 360 - angle;
					angle = -(90 + (90-seta));
				end
			end
		end
		
		if (ySpeed + cp.y <= 0) then
			if xSpeed > 0 then
				
				local seta = 180 - angle;
				angle = seta;
--				angle = 360 - angle;
			else
				if xSpeed == 0 then
					angle = 1;
				else
					local seta = angle-180;
					angle = -seta;
				end
				
			end
		end
		if angle > 360 then
			angle = angle - 360;
		end
		if angle < -360 then
			angle = 360 + angle;
		end
		bl.setRotation(angle);
	end

	--更新
	function bl.updateWithTargetFish()
		
	end
	
	--判断是否击中某条鱼
	function bl.hitFish(fishs)
		if not fishs or #fishs == 0 then
			return nil;
		end
		
		local size = bl.size;
		local rad = bl.rad;
		local fbx = bl.fbx;
		local node = fbx.getNode();
		local blCp = cc.p(0,size.height/2-rad);
		blCp = node:getParent():convertToWorldSpace(blCp);--转换到世界坐标系
		
		local fhed = nil;
		
		local n = #fishs;

		for i = 1,n do
			local fh = fishs[i];
			local b = fh.contactToCircle(blCp,rad);
			if b then
				fhed = fh;
				break;
			end
		end
		if fhed then
--			Log("我打中了一条鱼！");
			local cp = bl.getPosition();
			local ccsFactory = GetCCSFactory();
			local ccsNode = ccsFactory.createCCSAniamtion(bl.deadAni);
			ccsNode.playWithName("move");
			ccsNode.setPosition(cp.x,cp.y);
			
			local f2d = GetFishData2D();
			local index = f2d.initBulletIndex();
			ccsNode.setKey(tostring(index));
			fhed.shadered(FISH_SHADER_RED);
			bl.isDead = true;
			return ccsNode;
		end
		return nil;
	end
	
	--每帧更新
	function bl.update(dt)
		if not bl.fh then
			return bl.updateNoneTagetFish();
		end
		bl.updateWithTargetFish();
	end
	
	return bl;
end