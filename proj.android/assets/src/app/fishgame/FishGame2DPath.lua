PATH_TYPE_LINE = 0;--直线
PATH_TYPE_UNLINE = 1;--曲线
--2d捕鱼游戏的一个路径对象的封装
function FishGame2DPath()
	local fp = {};
	--清理数据
	function fp.clear()
		fp.ty = -1;
		fp.next = -1;
		fp.positions = {};
		fp.delay = 0;
		fp.line8 = {};--每个路径点，会有8条不同的分支
	end
	
	function fp.init(data)
		fp.clear();
		local ty = tonumber(data["Type"]);
		fp.ty = ty;
		local nxt = tonumber(data["Next"]);
		fp.next = nxt;
		local delay = tonumber(data["Delay"]);
		fp.delay = delay;
		fp.positions = data["Position"];
		
		local stos = fp.positions[1];
		local var = fp.initAllPath8(stos);
		fp.line8[1] = var;
		
		local ctos1 = fp.positions[2];
		var = fp.initAllPath8(ctos1);
		fp.line8[2] = var;
		
		local endpos = fp.positions[3];
		var = fp.initAllPath8(endpos);
		fp.line8[3] = var;
		
		local ctos2 = fp.positions[3];
		var = fp.initAllPath8(ctos2);
		fp.line8[4] = var;
		
	end
	
	function fp.getType()
		return fp.ty;
	end
	
	function fp.getDelay()
		return fp.delay;
	end
	
	function fp.getNext()
		return fp.next;
	end
	--[1] = {x="1",y="0.5"},[2]={x="-1",y="10"}..etc
	function fp.getPsitions()
		return fp.positions;
	end
	
	--初始化所有的8条路线
	function fp.initAllPath8(pos)
		local var = {};
		if not pos then
			return var;
		end
		local x = tonumber(pos.x);
		local y = tonumber(pos.y);
		
		var[1] = cc.p(x,1-y);		
		var[2] = cc.p(1-x,1-y);
		
		var[3] = cc.p(y,x);
		var[4] = cc.p(y,1-x);
		
		var[5] = cc.p(1-x,y);
		var[6] = cc.p(x,y);
		
		var[7] = cc.p(1-y,1-x);
		var[8] = cc.p(1-y,x);
		
		return var;
	end
	
	--获取屏幕的坐标点
	function fp.getScreenPos(pos)
		local x = pos.x;
		local y = pos.y;
		local w = CONFIG_SCREEN_WIDTH;
		local h = CONFIG_SCREEN_HEIGHT;
		
		x = x*w;
		y = y*h;
		return cc.p(x,y);
	end
	
	--初始化游动的动作(每一组数据总共有8种不同的游动方式)
	function fp.createSwingAction(index)
		local ty = fp.getType();
		local ac = nil;
		local pos = fp.line8;
		local po = {0,0};
		local eop = {0,0};
		--设计的时候为左上原点（0，0）
		if ty == PATH_TYPE_LINE then
			
		end
		if ty == PATH_TYPE_UNLINE then
			local stos = pos[1];
			local cots = pos[2];
			local endos = pos[3];
			
			po = stos[index];
			eop = endos[index];
			local cot = cots[index];
			Log(po);
			po = fp.getScreenPos(po);
			eop = fp.getScreenPos(eop);
			cot = fp.getScreenPos(cot);
			Log(po);
			
			ac = BezierTo(15,{cot,eop,eop});
		end
		return po,eop,ac;
	end
	
	return fp;
end

--解析路径的数据，转换成一个一个的路径的对象
function AnalysisFishGame2DPaths(tabaleData)
	local result = {};
	local FishPath = tabaleData["FishPath"];
	local Path = FishPath["Path"];
	local n = #Path;
	for i = 1,n do
		local data = Path[i];
		local fp = FishGame2DPath();
		fp.init(data);
		result[i] = fp;
	end
	return result;
end