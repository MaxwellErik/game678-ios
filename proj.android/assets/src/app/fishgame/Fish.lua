FISH_RED_TIME = 5;--被击中之后，红色五帧

function Fish()
	local fh = {};
	
	--clear or init argument in this function
	function fh.clear()
		fh.id = -1;--Fish's id
		--Fish's Animation
		if fh.ccsNode then
			local ccsNode = fh.ccsNode;
			ccsNode.removeFromParent();
			ccsNode.clear();
		end
		fh.ccsNode = nil;
		
		fh.x = 0;
		fh.y = 0;
		fh.rotation = 0;
		fh.fx = 1;--正常朝向右边
		fh.fy = 1;
		fh.coin = 0;--死亡之后给与的金币数量
		fh.animationPath = "";--使用的动画文件的名称
		fh.name = "";
		fh.key = "";--鱼的唯一标识
		fh.fp = nil;--鱼游泳的路劲
		fh.eop = cc.p{0,0};--鱼的目标地址
		
		fh.nRedTime = 0;--当前已经红了多久
		
		if fh.boxs then
			for i = 1,#fh.boxs do
				local fbx = fh.boxs[i];
				fbx.clear();
			end
		end
		
		fh.boxs = {};--鱼的所有包围盒
	end
	
	--set the position of this fish
	function fh.setPosition(x,y)
		fh.x = x;
		fh.y = y;
		local ccsNode = fh.ccsNode;
		ccsNode.setPosition(x,y);
	end
	
	--get the position of this fish_
	--return x,y
	function fh.getPosition()
		return fh.x,fh.y;
	end
	
	function fh.getName()
		return fh.name;
	end
	
	function fh.getCoin()
		return fh.coin;
	end
	
	--增加一个节点
	function fh.addChild(node,zorder,tag)
		local ccsNode = fh.ccsNode;
		ccsNode.addChild(node,zorder,tag);
	end
	
	--init a fish with id
	function fh.init(_id,key)
		fh.clear();
		fh.id = _id;
		local data = Global["fishData"][tostring(_id)];
		if not data then
			Log("没有找到id = ".._id.."的鱼的数据");
		end
		
		local ccsFactory = GetCCSFactory();
		local ccsNode = ccsFactory.createCCSAniamtion("fish_21");
		ccsNode.playWithName("move");
		
		local size = ccsNode.getContentSize();

		ccsNode.setUserData(_id);
		fh.ccsNode = ccsNode;
		--这个地方需要初始化包围盒
		local f2d = GetFishData2D();
		local boxData = f2d.getBoxData(21);
		Log(boxData);
		local bb = boxData["BB"];
		for i = 1,#bb do
			local ob = bb[i];
			local x = tonumber(ob["OffestX"]);
			local y = tonumber(ob["OffestY"]);
			local rad = tonumber(ob["Radio"]);
			local fbx = FishGame2DBox();
			fbx.init(x,y,rad);
			fh.boxs[i] = fbx;
			if FISHGAME_TEST_DRAW then
				local node = fbx.getNode();
				node:setPosition(x,y);
				fh.addChild(node,1,i);
			end
		end		
		
	end
	
	--鱼游动到终点之后的回调函数(不可以在外部调用)
	function fh.swimmingEndl()
		local fp = fh.fp;
		local nxt = fp.getNext();
		if nxt > 0 then
			--如果还有下一个路径
			local f2d = GetFishData2D();
			local fxp = f2d.getPath(nxt);
			if fxp then
				fh.setPath(fxp);
				return;
			end 
		end
		
	end
	
	--设置鱼的游动路径
	function fh.setPath(fp)
		fh.fp = fp;
		Log("设置鱼的游动路径：");
		local po;
		local act;
		local eop;--结束的坐标点
		po,eop,act = fp.createSwingAction(8);
		
		fh.setPosition(400,320);
		
		--[[fh.setPosition(po.x,po.y);
		local call = CallFunction(fh.swimmingEndl);
		local obo = ActionOneByOne({act,call});
		fh.runAction(obo);
		fh.eop = eop;--]]
	end
	
	--设置鱼的反转，镜像
	function fh.setScaleX(fx)
		fh.fx = fx;
		local ccsNode = fh.ccsNode;
		ccsNode.setScaleX(fx);
	end
	
	function fh.setScaleY(fy)
		fh.fy = fy;
		local ccsNode = fh.ccsNode;
		ccsNode.setScaleY(fy);
	end
	
	--play a animation with target name
	function fh.play(name)
		fh.ccsNode.playWithName(name);
	end
	
	--get this Fish's Animation
	function fh.getAnimation()
		return fh.ccsNode.getArmature();
	end
	
	--set the rotation of this fish
	function fh.setRotation(angle)
		fh.rotation = angle;
		fh.ccsNode.setRotation(angle);
	end
	
	--get the rotation of this fish
	function fh.getRotation()
		return fh.rotation;
	end
	
	
	function fh.updateShaderTime()
		if fh.nRedTime <= 0 then
			return;
		end
		fh.nRedTime = fh.nRedTime - 1;
		if fh.nRedTime == 0 then
			ColorThisArmature(fh.ccsNode.getArmature());
		end	
	end
	
	--update the fish per millisecond
	function fh.update(dt)
		fh.updateShaderTime();
	end
	
	function fh.shadered(ty)
		local ccsNode = fh.ccsNode;
		if ty == FISH_SHADER_RED then
			if fh.nRedTime == 0 then
				RedThisArmature(ccsNode.getArmature());
			end
			fh.nRedTime = 5;
		end
		if ty == FISH_SHADER_COLOR then
			ColorThisArmature(ccsNode.getArmature());
		end
		
	end
	
	--执行一个动作
	function fh.runAction(ac)
		local ccsNode = fh.ccsNode;
		ccsNode.runAction(ac);
	end
	
	--当前的鱼是否有对应的包围盒和参数碰撞
	function fh.contactToCircle(p,rad)
		local fbxs = fh.boxs;
		if not fbxs or #fbxs == 0 then
			return;
		end
		
		for i = 1,#fbxs do
			local fbx = fbxs[i];
			local b = fbx.contactToOther(p,rad);
			if b then
				return true;
			end
		end
		return false;
	end
	
	return fh;
end