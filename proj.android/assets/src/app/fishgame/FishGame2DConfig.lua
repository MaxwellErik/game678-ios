function singleton(class)
    local _instance = nil
    local _class = {}
    function _class.Clear()
        if _instance then
            _instance:Clear()
        end
        _instance = nil
        _class = nil
    end

    function _class:GetInstance()
        if _instance == nil then
            _instance = class.new()
        end

        return _instance
    end

    _G[class.__cname] = _class

    return _class
end


local c = cc

local Node = c.Node

local _temp = Node.addChild

function Node:addChild(child, ...)
    if tolua.isnull(child) then
        print(traceback())
        return false
    end
    _temp(self, child, ...)
end

test_width = 1280
test_height = 720
test_cx = test_width / 2
test_cy = test_height / 2

function createFrameAnimation(path)
    if (path == "fishgame2d") then
        print(debug.traceback())
    end

    ccs.ArmatureDataManager:getInstance():addArmatureFileInfo(path .. "/" .. path .. ".ExportJson")
    return ccs.Armature:create(path);
end

function createSkeletonAnimation(path)
    return sp.SkeletonAnimation:create(path .. "/" .. path .. ".json", path .. "/" .. path .. ".atlas", 1)
end


FISHGAME2D_XML_BULLETSET = "fishgame2d/BulletSet.xml"
FISHGAME2D_XML_FISH = "fishgame2d/Fish.xml"
FISHGAME2D_XML_SPECIAL_FISH = "fishgame2d/Special.xml"
FISHGAME2D_XML_FISH_SOUND = "fishgame2d/FishSound.xml"
FISHGAME2D_XML_BOUNDING_BOX = "fishgame2d/BoundingBox.xml"
FISHGAME2D_XML_SCENE = "fishgame2d/Scene.xml"
FISHGAME2D_XML_CANNONSET = "fishgame2d/CannonSet.xml"
FISHGAME2D_XML_VISUAL = "fishgame2d/Visual.xml"
FISHGAME2D_XML_SYSTEM = "fishgame2d/System.xml"
FISHGAME2D_XML_PATH = "fishgame2d/path.xml"
FISHGAME2D_XML_TROOPSET = "fishgame2d/TroopSet.xml"

FISHGAME_TEST_DRAW = false; --是否开启包围盒绘制


FISH_SHADER_RED = "red";
FISH_SHADER_COLOR = "color"

FISH_GAME_PLAYER_NUM = 4

LONGTOUCH_CKECK_DURING = 0.15; --每隔0.25秒检测一次长按

FISH_GAME_MAX_BULLET_AMOUNT = 20
MAX_PROBABILITY = 1000.0

FORWARD = 100

SpecialFishType = {
    ESFT_NORMAL = 0,
    ESFT_KING = 1,
    ESFT_KINGANDQUAN = 2,
    ESFT_SANYUAN = 3,
    ESFT_SIXI = 4,
    ESFT_MAX = 5,
}

PTCOUNT = 4
NormalPathType = {
    NPT_LINE = 0,
    NPT_BEZIER = 1,
    NPT_CIRCLE = 2,
}

ObjectType = {
    EOT_NONE = 0,
    EOT_PLAYER = 1,
    EOT_BULLET = 2,
    EOT_FISH = 3,
}

--VisualType = {
--    EVCT_NORMAL = MyComponentType.ECF_VISUAL * 2 ^ 8
--}

MyPoint = class("MyPoint")
MyPoint.x_ = 0
MyPoint.y_ = 0
function MyPoint:ctor(x, y)
    self.x_ = x or 0
    self.y_ = y or 0
end

CMovePoint = class("CMovePoint", {})

function CMovePoint:ctor(pos, dir)
    self.m_Position = pos or MyPoint.new(0, 0)
    self.m_Direction = dir or 0
end

CHAIR_ID = {
    START = 0,
    END = 3
}

E_Red = 1
E_Blue = 2
E_Light = 3

FISHGAME2D_EVENT = {
    ADD_CHAIN = "add_chain",
    ADD_BINGO = "add_bingo",
    ADD_FISH_GOLD = "ADD_FISH_GOLD",
    ADD_JETTON = "ADD_JETTON",
    ADD_EFFECT_SHAKE_SCREEN = "ADD_EFFECT_SHAKE_SCREEN",
    ADD_EFFECT_PARTICAL = "ADD_EFFECT_PARTICAL",
    EVENT_LOTTERY_BEGAN = "event_lottery_began",
    EVENT_LOTTERY_END = "event_lottery_end",
    EVENT_LOTTERY_OPEN_STATE = "event_lottery_open_state",
    EVENT_BEFORE_TIME = "event_before_time",
    EVENT_SYSTEM_INFO = "EVENT_SYSTEM_INFO",
    EVENT_PLAYER_INFO = "EVENT_PLAYER_INFO",
    UPDATE_PLAYER_DATA = "upadate_player_data",
    EVENT_REWARD_INFO = "event_reward_info",
    EVENT_JINJI_READY = "event_jinji_ready",
    EVENT_JINJI_START = "event_jinji_start",
    EVENT_JINJI_APPLY_MATH = "event_jinji_apply_match",
    EVENT_JINJI_APPLY_COUNT = "event_jinji_apply_count",
    EVENT_JINJI_APPLY_FULL = "event_jinji_apply_full",
    EVENT_JINJI_SCORE_INFO = "event_jinji_score_info",
    EVENT_JINJI_UPGRADE_INFO = "event_jinji_upgrade_info",
    EVENT_JINJI_OVER = "event_jinji_over",
    EVENT_JINJI_SYSTEM_INFO = "event_jinji_system_info",
    EVENT_JINJI_USER_INFO = "event_jinji_user_info",
    EVENT_JINJI_LIST = "event_jinji_list",
    EVENT_INVATE_TEAM = "event_invate_team",
    EVENT_AGREE_INVITE = "event_agree_invite",
    EVENT_PLAYER_LEAVE_TEAM = "event_player_leave_team",
    EVENT_INVITE_INFO = "event_invite_info",
    EVENT_DELETE_TEAMMATE = "event_delete_teammate",
    EVENT_INSERT_TEAMMATE = "event_insert_teammate",
    EVENT_REFRESH_TEAM_LIST = "event_refresh_team_list",
}

--cc.FileUtils:getInstance():addSearchPath("res/fishgame2d"); --资源
--cc.FileUtils:getInstance():addSearchPath("res/fishgame2d/ui"); --资源
--cc.FileUtils:getInstance():addSearchPath("res/fishgame2d/animation"); --资源
--cc.FileUtils:getInstance():addSearchPath("res/fishgame2d/test"); --资源
--cc.FileUtils:getInstance():addSearchPath("res/fishgame2d/scene"); --资源
--cc.FileUtils:getInstance():addSearchPath("res/fishgame2d/LockFish"); --资源
--
--cc.FileUtils:getInstance():addSearchPath("res/fishgame2d/sound/bgm")
--cc.FileUtils:getInstance():addSearchPath("res/fishgame2d/sound/effect")
--cc.FileUtils:getInstance():addSearchPath("res/fishgame2d/sound/add_effect")

require("Engine.init") --陈建波，增加一些通用方法
require("socket")
import(".FishGame2DMsgId")
import(".TimeService").GetInstance()
import(".Aide.init")
import(".Component.init")

require("app.fishgame.Manager.CGameConfig").GetInstance()
require("app.fishgame.Manager.CSoundManager")
require("app.fishgame.Manager.MyObjectManager")
import(".Model.init")
import(".Event.init")

