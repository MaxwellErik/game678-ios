--
-- Author: YL
-- Date: 2016-01-19 11:54:23
--
local DataManager = require("app.uiManager.DataManager")
local RotaryTabLayer = class("RotaryTabLayer", function()
	return display.newLayer()
end)


--抽奖界面
function RotaryTabLayer:ctor()
	--是否可以点击
	self._istouch = true
	self._getcoinNum = 0

	self.rotaryLayer = ccs.GUIReader:getInstance():widgetFromJsonFile("HallUI/RotaryTab.json")
	self:addChild(self.rotaryLayer)

	--屏蔽蒙版
	self._bg = self.rotaryLayer:getChildByName("mengban")
	local function exitCallBack(sender, eventType)
        if eventType == ccui.TouchEventType.ended then
        	app.musicSound:playSound(SOUND_HALL_TOUCH)
        	self:removeFromParent()
        end
    end
    self._bg:getChildByName("closeBtn"):addTouchEventListener(exitCallBack)

	--女郎眨眼
	self._nvlang1 = self._bg:getChildByName("nvlang1")
	local seq = transition.sequence({
		cc.DelayTime:create(3),
		cc.CallFunc:create(function()
			self._nvlang1:setVisible(false)
		end),
		cc.DelayTime:create(0.3),
		cc.CallFunc:create(function()
			self._nvlang1:setVisible(true)
		end),
		})
	self._nvlang1:runAction(cc.RepeatForever:create(seq))

	--Vip图标
	self._vip = self._bg:getChildByName("vipNum")
	--抽奖次数
	self._count = self._bg:getChildByName("countNum")
	--发光
	self._beiguang1 = self._bg:getChildByName("beiguang1")
	self._beiguang1:setVisible(false)
	self._beiguang2 = self._bg:getChildByName("beiguang2")
	self._beiguang2:setVisible(false)
	self._beiguang3 = self._bg:getChildByName("beiguang3")
	self._beiguang3:setVisible(false)
	--发光
	self._xingguang1 = self._bg:getChildByName("xingguang1")
	self._xingguang1:setVisible(false)
	self._xingguang2 = self._bg:getChildByName("xingguang2")
	self._xingguang2:setVisible(false)
	self._xingguang3 = self._bg:getChildByName("xingguang3")
	self._xingguang3:setVisible(false)
	--次数底板
	self._message1 = self._bg:getChildByName("message1")
	self._message2 = self._bg:getChildByName("message2")
	--转盘地板
	self._diban1 = self._bg:getChildByName("diban1")
	self._diban2 = self._bg:getChildByName("diban2")
	--转盘
	self._zhuanpan = self._bg:getChildByName("zhuanpan")
	--转盘上的金币
	self._level1 = self._zhuanpan:getChildByName("level1")
	--开始按钮
	self._startBtn = self._bg:getChildByName("startBtn")
	local function startCallBack(sender, eventType)
        self:startCallBack(sender, eventType)
    end
	self._startBtn:addTouchEventListener(startCallBack)

	self:setLevelShow()

end

--开始回调
function RotaryTabLayer:startCallBack(sender, eventType)
	if eventType == ccui.TouchEventType.ended and self._istouch then
		local isshow = cc.UserDefault:getInstance():getIntegerForKey("isRotrayTab")
	    if not isshow or isshow ~= 0 then
			self._istouch = false
	    	app.musicSound:playSound(SOUND_HALL_TOUCH)
	    	self._count:loadTexture("HallUI/RotaryTable/0.png")
	    	app.hallLogic:onRotaryTab()
	    end
    end
end

--转盘转动逻辑
function RotaryTabLayer:rotaryLogic(resultNum)
	cc.UserDefault:getInstance():setIntegerForKey("isRotrayTab", 0)
	--闪灯
	local tip_num = false
	local seq = transition.sequence({
		cc.DelayTime:create(0.15),
		cc.CallFunc:create(function()
			if tip_num then
				tip_num = false
				self._message1:setVisible(false)
				self._message2:setVisible(true)
				self._diban1:setVisible(false)
				self._diban2:setVisible(true)
			else
				tip_num = true
				self._message2:setVisible(false)
				self._message1:setVisible(true)
				self._diban2:setVisible(false)
				self._diban1:setVisible(true)
			end
		end),
		})
	self._startBtn:runAction(cc.RepeatForever:create(seq))

	--转盘转
	self._zhuanpan:setRotation(0)
	local angle = self:getAngleByCoin(resultNum)
	local rotationto2 = cc.RotateBy:create(1, 180)
	local rotationto3 = cc.RotateBy:create(3, 1800)
	local rotationto4 = cc.RotateBy:create(4, 180+angle)
	local seq = transition.sequence({
		rotationto2,
		rotationto3,
		rotationto4,
		cc.CallFunc:create(function()
			MyToast.new(self, self:getResult(resultNum))
			self._startBtn:stopAllActions()
		end),
		cc.DelayTime:create(3),
		cc.CallFunc:create(function()
			if angle then
				self._istouch = true
				self._zhuanpan:stopAllActions()
			end
		end),
		})
	self._zhuanpan:runAction(cc.RepeatForever:create(seq))
end

--判断得到的东西
function RotaryTabLayer:getResult(num)
	local strTab = {
		"恭喜抽中10000金币",
		"恭喜抽中9000金币",
		"恭喜抽中8000金币",
		"恭喜抽中7000金币",
		"恭喜抽中6000金币",
		"恭喜抽中5000金币",
		"恭喜抽中4000金币",
		"恭喜抽中3000金币",
		"恭喜抽中2000金币",
		"恭喜抽中1000金币",
		"恭喜抽中10元宝箱",
		"恭喜抽中1元宝箱",
		"恭喜抽中1张乐卷",
	}
	local coinTab = {10000,9000,8000,7000,6000,5000,4000,3000,2000,1000}
	if num <= #coinTab then --金币
		app.hallLogic._loginSuccessInfo.lUserScore = app.hallLogic._loginSuccessInfo.lUserScore + coinTab[num]
		DataManager:getInstance():getCoinLable():setString(app.hallLogic._loginSuccessInfo.lUserScore)
	elseif num == 13 then --乐券
		app.hallLogic._leJuan = app.hallLogic._leJuan + 1
		DataManager:getInstance():getTicketLable():setString(app.hallLogic._leJuan)
	elseif num == 11 or num == 12 then
		app.hallLogic:queryUserRedEnvelope()
	end


	return strTab[num]
end

--根据VIP改变金币范围
function RotaryTabLayer:setLevelShow()
	local vipNum = app.hallLogic._loginSuccessInfo.obMemberInfo.cbMemberOrder
	self._vip:loadTexture("HallUI/RotaryTable/VIP"..vipNum..".png")
	if vipNum >= 1 and vipNum <= 3 then
		self._beiguang1:setVisible(true)
		self._xingguang1:setVisible(true)
	elseif vipNum >= 4 and vipNum <= 6 then
		self._beiguang2:setVisible(true)
		self._xingguang2:setVisible(true)
	elseif vipNum >= 7 and vipNum <= 9 then
		self._beiguang3:setVisible(true)
		self._xingguang3:setVisible(true)
	end
end

--根据金币计算角度
function RotaryTabLayer:getAngleByCoin(num)
	local angle
	--分别代表10000,9000,8000,7000,6000,5000,4000,3000,2000,1000,10元宝箱,1元宝箱,1张乐卷 对应的角度
	local angleTab = {346, 13, 41, 69, 96, 124, 152, 180, 207, 235, 263, 290, 318}
	angle = angleTab[num]

	--随机10°波动
	local rand = math.random(1, 2)
	if rand == 1 then
		angle = angle - math.random(0, 10)
	else
		angle = angle + math.random(0, 10)
	end

	return angle
end







return RotaryTabLayer