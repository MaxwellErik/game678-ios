
uiManager = class("uiManager", cc.mvc.AppBase)

--进入scene，参数二 进入场景模式，参数三 进入场景消耗时间，参数四 场景转换时颜色
function uiManager:runScene(id,args,type,time,color)
    if not color then
    	color = display.COLOR_WHITE
    end
    
    if not time then
    	time = 0.2
    end
    --(sceneName, args, transitionType, time, more)
    app:enterScene(id, args, type, time, color)
end

--进入Gamescene
function uiManager:runGameScene(id, args, type, time, color)
    if not color then
        color = display.COLOR_WHITE
    end

    if not time then
        time = 0.2
    end
    
    app:enterGameScene(id, args, type, time, color)
end


return uisManager