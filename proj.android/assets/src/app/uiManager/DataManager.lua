--
-- Author: YL
-- Date: 2016-01-20 15:53:28
--
local DataManager = class("DataManager")

local instance = nil

function DataManager:ctor()
	self._rotary = nil   --大转盘层
	self._coinlable = nil   --金币的lable
	self._ticketlable = nil   --乐券的lable
	self._gamechoice = nil --大厅界面
	self._boxLayer = nil --宝箱layer
	self._newstore = nil
end

function DataManager:getInstance()
	if instance == nil then
		instance = DataManager.new()
	end
	return instance
end

--转盘回调
function DataManager:setRotary(callfunc)
	self._rotary = callfunc
end 
function DataManager:getRotary()
	return self._rotary
end

--设置金币的lable
function DataManager:setCoinLable(lable)
	self._coinlable = lable
end 
function DataManager:getCoinLable()
	return self._coinlable
end

--设置乐券的lable
function DataManager:setTicketLable(lable)
	self._ticketlable = lable
end 
function DataManager:getTicketLable()
	return self._ticketlable
end

--设置选择游戏Layer
function DataManager:setChoiceLayer(layer)
	self._gamechoice = layer
end 
function DataManager:getChoiceLayer()
	return self._gamechoice
end

--设置宝箱Layer
function DataManager:setBoxLayer(layer)
	self._boxLayer = layer
end 
function DataManager:getBoxLayer()
	return self._boxLayer
end

--设置新商城Layer
function DataManager:setNewStoreLayer(layer)
	self._newstore = layer
end 
function DataManager:getNewStoreLayer()
	return self._newstore
end















--释放单例
function DataManager:delDataManager()
	instance = nil
end

return DataManager