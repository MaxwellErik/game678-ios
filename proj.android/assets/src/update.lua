require("config")
require("framework.init")
require "lfs"

local UpdateScene = class("UpdateScene", function()
    return display.newScene("UpdateScene")
end)

--jiujiu appstore
-- local server = "http://dwc.jiujiu.com/gameUpdate/jiujiu_test/V100_AppStore/"--正式
--local server = "http://dwc.jiujiu.com/gameUpdate/jiujiu_test/V100_AppStore/" --测试

local server = "http://dwc.jiujiu.com/gameUpdate/jiujiu/V100_Android/"--正式

local list_filename = "GPUpdateConf.txt"
local downList = {}

local function readFile(path)
    local file = io.open(path, "rb")
    if file then
        local content = file:read("*all")
        io.close(file)
        return content
    end
    return nil
end

local function removeFile(path)
    os.remove(path)
end

local function checkDirOK( path )
    if path == nil then
        return false
    end

    local ret = lfs.chdir(path)
    if ret==nil then
        return false
    else
        return true
    end
end

local function findLastStr(srcStr, subStr)
    if srcStr ~= nil and srcStr ~= "" then
        local startIndex, endIndex = string.find(srcStr,subStr)
        if startIndex == nil then
            return 0
        else
            return endIndex + findLastStr(string.sub(srcStr, endIndex+1, -1), subStr)
        end
    else
        return 0
    end
end

local function createDir(path)
    path = string.sub(path, 1, -2)
    local lastIndex = findLastStr(path, "/")
    local subPath = string.sub(path, 1, lastIndex)
    local dirName = string.sub(path, lastIndex, string.len(path))

    local pathinfo = io.pathinfo(subPath)
    if checkDirOK(pathinfo.dirname)==false then
        createDir(subPath)
    end

    lfs.mkdir(path)
end

function os.rmdir(path)
    print("os.rmdir:", path)
    print(io.exists(path))
    if checkDirOK(path)==true then
        local function _rmdir(path)
            local iter, dir_obj = lfs.dir(path)
            while true do
                local dir = iter(dir_obj)
                if dir == nil then
                    break
                end
                
                if dir ~= "." and dir ~= ".." then
                    local curDir = path..dir
                    local mode = lfs.attributes(curDir, "mode") 
                    if mode == "directory" then
                        _rmdir(curDir.."/")
                    elseif mode == "file" then
                        print('delete file:'..curDir)
                        os.remove(curDir)
                    end
                end
            end
            lfs.rmdir(path)
            return succ
        end
        _rmdir(path)
    end
    return true
end

function io.writefileCheckDir(path, data)
    local pathinfo = io.pathinfo(path)
    if checkDirOK(pathinfo.dirname) then
        io.writefile(path, data)
        return true
    else
        if device.platform == "windows" then
            local newStr = string.gsub(pathinfo.dirname, "/", "\\")
            os.execute("mkdir "..newStr)
        else
            createDir(pathinfo.dirname)
        end

        if checkDirOK(pathinfo.dirname) then
            io.writefile(path, data)
            return true
        else
            return false
        end
    end
end

function UpdateScene:ctor()
    self.path = device.writablePath.."upd/"

    print( " self.path === " ,self.path )
    cc.FileUtils:getInstance():addSearchPath(self.path,true)
    self._updateNetOkPercent = 20
    self._downFileIndex = 0
    self._downFileCount = 0
    self._updateOK = true
    self._downFileTotalByte = 0
    self._downFileCurByte = 0
    self._downFileCount = 0
    self._downloadIndex = 0
    self._updateOK = true
    
    local bg = cc.Sprite:create("res/GameUpdate/gameUpdate_bg.png")
    bg:setAnchorPoint(0,0)
    self:addChild(bg)
    
    self._checkPro = ccui.LoadingBar:create("res/GameUpdate/gameUpdate_pro.png", 0)
    self._checkPro:setAnchorPoint(0, 0)
    self._checkPro:setPosition(233, 138)
    self:addChild(self._checkPro)
    
    self._checkPercent = cc.LabelTTF:create()
    self._checkPercent:setFontSize(40)
    self._checkPercent:setString("0%")
    self._checkPercent:setPosition(640, 150)
    self:addChild(self._checkPercent)
end

function UpdateScene:reqNextFile()
    self._downloadIndex = self._downloadIndex+1
    self.curStageFile = self._downFileList[self._downloadIndex]
    if self.curStageFile and self.curStageFile.name then
        
        self:requestFromServer(self.curStageFile.name)
        return
    end
    self:endProcess()
end

function UpdateScene:updateProgressPercent()
    local percent = self._updateNetOkPercent
    local filePercent = 0
    if self._downFileTotalByte and self._downFileCurByte and self._downFileTotalByte > 0 and self._downFileCurByte <= self._downFileTotalByte  then
        filePercent = self._downFileCurByte *80 / self._downFileTotalByte
    end

    if self._downFileCount > 0 then
        percent = percent + (self._downloadIndex-1)*80/self._downFileCount + filePercent/self._downFileCount
    end

    percent = math.ceil(percent)
    self._checkPro:setPercent(percent)
    self._checkPercent:setString(percent.."%")
end

--确定所有下载文件,删除文件
function UpdateScene:parseUpdateConf()
    for index, item in ipairs(self.fileListNew.detail.code) do
    	table.insert(self._downFileList, item)
    end
    
    for index, item in ipairs(self.fileListNew.detail.res) do
    	if self:checkUpdateByVersion(self.fileList, item.ver)== true then
            for fileIndex, fileItem in ipairs(item.stage) do
                table.insert(self._downFileList, fileItem)
    		end
    		
            for fileIndex, fileItem in ipairs(item.remove) do
                table.insert(self._removeFileList, fileItem)
    		end
    	end
    end
    
    self._downFileCount = #self._downFileList
end

function UpdateScene:checkUpdateByVersion(fileList, confVersion)
    local curVers = string.split(fileList.ver, '.')
    local confVers = string.split(confVersion, '.')
        
    if #curVers ~= #confVers or #curVers ~= 3 then
    	return false
    end
    
    local curVer1 = tonumber(curVers[1])
    local curVer2 = tonumber(curVers[2])
    local curVer3 = tonumber(curVers[3])
    
    local confVer1 = tonumber(confVers[1])
    local confVer2 = tonumber(confVers[2])
    local confVer3 = tonumber(confVers[3])
    
    if  curVer1 < confVer1 then
    	return true
    end
    
    if curVer1 == confVer1 and curVer2 < confVer2 then
    	return true
    end
    
    if curVer1 == confVer1 and curVer2 == confVer2 and curVer3 < confVer3 then
        return true
    end
    return false
end
  
function UpdateScene:onEnterFrame(dt)
    self:updateProgressPercent()
    print( "UpdateScene:onEnterFrame" )
    if self.dataRecv then
        if self.requesting == list_filename then
            print('检查更新文件')
            io.writefileCheckDir(self.newListFile, self.dataRecv)
            self.dataRecv = nil
            self.fileListNew = dofile(self.newListFile)
            if self.fileListNew==nil then
                print(self.newListFile..": Open Error!")
                self._updateOK = false
                self:endProcess()
                return
            end
            
            print(self.fileListNew.ver)
            self._downFileList = {}
            self._removeFileList = {}
            self._downFileTotalByte = 0
            self._downFileCurByte = 0
            
            if self:checkUpdateByVersion(self.fileList, self.fileListNew.ver)== true then               
                self:parseUpdateConf()
                
                self._downloadIndex = 0
                self.requesting = "files"
                self:reqNextFile()
                return            
            else
                self:endProcess()
                return
            end
        end

        if self.requesting == "files" then
            self._downFileIndex = self._downFileIndex + 1
            self:updateProgressPercent()
            local newfilename = self.path..self.curStageFile.name
            io.writefileCheckDir(newfilename, self.dataRecv) 
            self:reqNextFile()
            
            self.dataRecv = nil
            return
        end
    end
end

function UpdateScene:checkUpdateByVersion(fileList, confVersion)
    local curVers = string.split(fileList.ver, '.')
    local confVers = string.split(confVersion, '.')

    if #curVers ~= #confVers or #curVers ~= 3 then
        return false
    end

    local curVer1 = tonumber(curVers[1])
    local curVer2 = tonumber(curVers[2])
    local curVer3 = tonumber(curVers[3])

    local confVer1 = tonumber(confVers[1])
    local confVer2 = tonumber(confVers[2])
    local confVer3 = tonumber(confVers[3])

    if  curVer1 < confVer1 then
        return true
    end

    if curVer1 == confVer1 and curVer2 < confVer2 then
        return true
    end

    if curVer1 == confVer1 and curVer2 == confVer2 and curVer3 < confVer3 then
        return true
    end
    return false
end

function UpdateScene:onEnter()
    self.curListFile =  self.path..list_filename

    print( "self.curListFile == ",self.curListFile )
    self.fileList = nil
    if io.exists(self.curListFile) then
        self.fileList = dofile(self.curListFile)
    end
    self.baseFileList = {
            ver = "2.1.0",
            detail = {
                code = {},
                res = {},
            },
        }
    
    if self.fileList==nil then
        self.fileList = self.baseFileList
    end
    
    local fileVers = string.split(self.fileList.ver, '.')
    local baseVers = string.split(self.baseFileList.ver, '.')
    
    local clearCache = false
    if #fileVers ~= #baseVers or #baseVers ~= 3 then
        clearCache = true
    else
        local fileVer1 = tonumber(fileVers[1])
        local fileVer2 = tonumber(fileVers[2])
        local fileVer3 = tonumber(fileVers[3])

        local baseVer1 = tonumber(baseVers[1])
        local baseVer2 = tonumber(baseVers[2])
        local baseVer3 = tonumber(baseVers[3])

        if  (fileVer1 < baseVer1) or (fileVer1 == baseVer1 and fileVer2 < baseVer2) or (fileVer1 == baseVer1 and fileVer2 == baseVer2 and fileVer3 < baseVer3) then
            clearCache = true
        end
    end
    
    if clearCache == true then  --删除之前热更新的所有文件
        os.rmdir(self.path)
    end

    self.requestCount = 0
    self.requesting = list_filename
    self.newListFile = self.curListFile..".upd"
    self.dataRecv = nil
    self:requestFromServer(self.requesting)

    self:downLoop()
end

function UpdateScene:downLoop()
    local scheduler = cc.Director:getInstance():getScheduler()
    self.schedulerEntry = scheduler:scheduleScriptFunc(function() self:onEnterFrame(0) end, 0, false)
end

function UpdateScene:onExit()
end

function UpdateScene:endProcess()
    local scheduler = cc.Director:getInstance():getScheduler()
    scheduler:unscheduleScriptEntry(self.schedulerEntry) 
    
    local loadNewCode = false
    if self._updateOK == true then
        local data = readFile(self.newListFile)
        if data then
            io.writefileCheckDir(self.curListFile, data)
            self.fileList = dofile(self.curListFile)
            removeFile(self.newListFile)
        end
        
        --解压zip文件
        for index, item in ipairs(self._downFileList) do
            if item.act == "unzip" then
                local unZipFile = self.path..item.name
                printf("解压文件：%s", unZipFile)
                custom.PlatUtil:uncompressZip(unZipFile, self.path)
                os.remove(unZipFile)
            end             
        end
                        
        if self.fileListNew and self.fileListNew.detail and self.fileListNew.detail.code then
            for i,v in ipairs(self.fileListNew.detail.code) do
                loadNewCode = true
                local ret = cc.LuaLoadChunksFromZIP(self.path..v.name)
                if ret ~= true then
                    loadNewCode = false
                end
            end
        end
        
        if self._removeFileList and #self._removeFileList > 0 then
            for i,v in ipairs(self._removeFileList) do
                removeFile(self.path..v)
            end
        end
    end
    if loadNewCode==false then
        print("加在游戏原始代码")
        removeFile(self.curListFile)
        cc.LuaLoadChunksFromZIP("res/game.zip")
    end
    
    require("appentry")
end

function UpdateScene:requestFromServer(filename, waittime)
    local url = server..filename
    print( "111111111111" )
    print(url)
   
    local request = network.createHTTPRequest(function(event)
        self:onResponse(event, 0)
    end, url, "GET")
    if request then
        request:setTimeout(9999)
        request:start()
    else
        self._updateOK = false
        self:endProcess()
    end
end

function UpdateScene:onResponse(event, index, dumpResponse)
    self._downFileTotalByte = event.total
    self._downFileCurByte = event.dltotal
    
    local request = event.request
    if event.name == "completed" then
        if request:getResponseStatusCode() ~= 200 then
            self._updateOK = false
            self:endProcess()
        else
            self.dataRecv = request:getResponseData()
        end
    elseif event.name == "failed" then
        self._updateOK = false
        self:endProcess()
    end
end

local upd = UpdateScene.new()
display.replaceScene(upd)