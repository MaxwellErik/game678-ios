
--大厅里面的协议码

MDM_GF_GAME = 100           --游戏命令
MDM_GF_FRAME = 101      --框架命令

MDM_GP_LOGON = 1        --广场登录
SUB_GP_GROWLEVEL_CONFIG = 107   --等级配置

MDM_GP_USER_SERVICE = 3         --用户服务
SUB_GP_USER_HEARTBEAT = 10      --在线状态
SUB_GP_S_USER_HEARTBEAT = 11    --心跳下行
SUB_GP_MODIFY_MACHINE = 100     --修改机器
SUB_GP_MODIFY_LOGON_PASS = 101  --修改密码
SUB_GP_MODIFY_INSURE_PASS = 102 --修改密码
SUB_GP_MODIFY_UNDER_WRITE = 103 --修改签名
SUB_GP_C_QUERY_RENAMED = 104 --查询用户是否更改过昵称
SUB_GP_S_QUERY_RENAMED = 105 --返回值：零，未更改过昵称；非零，更改过昵称。
SUB_GP_C_EXIST_NICKNAME = 106 --检查昵称是否存在
SUB_GP_S_EXIST_NICKNAME = 107 --返回值：零，该昵称不存在；非零，该昵称已存在。
SUB_GP_C_MODIFY_NICKNAME = 108 --修改昵称
SUB_GP_S_MODIFY_NICKNAME = 109 --修返回值：零，修改失败；非零，修改成功。
SUB_GP_C_QUERY_SIGNIN_REWARD = 110 --查询签到奖励
SUB_GP_S_SIGNIN_REWARD_INFO = 111 --签到奖励信息
SUB_GP_C_QUERY_USER_SIGNIN = 112 --查询用户是否签到
SUB_GP_S_USER_SIGNIN_INFO = 113 --用户签到信息
SUB_GP_C_USER_SIGNIN = 114 --用户签到
SUB_GP_S_USER_SIGNIN_RESULT = 115 --签到结果
SUB_GP_USER_FACE_INFO = 200     --头像信息
SUB_GP_SYSTEM_FACE_INFO = 201   --系统头像
SUB_GP_CUSTOM_FACE_INFO = 202   --自定头像
SUB_GP_USER_INDIVIDUAL = 301    --个人资料
SUB_GP_QUERY_INDIVIDUAL = 302   --查询信息
SUB_GP_MODIFY_INDIVIDUAL = 303  --修改资料
SUB_GP_C_QUERY_USER_SCORE = 304   --查询用户金币信息
SUB_GP_S_USER_SCORE_INFO = 305    --用户金币信息
SUB_GP_USER_SAVE_SCORE = 400    --存款操作
SUB_GP_USER_TAKE_SCORE = 401    --取款操作
SUB_GP_USER_TRANSFER_SCORE = 402    --转账操作
SUB_GP_USER_INSURE_INFO = 403   --银行资料
SUB_GP_QUERY_INSURE_INFO = 404  --查询银行
SUB_GP_USER_INSURE_SUCCESS = 405    --银行成功
SUB_GP_USER_INSURE_FAILURE = 406    --银行失败
SUB_GP_QUERY_USER_INFO_REQUEST = 407    --查询用户
SUB_GP_QUERY_USER_INFO_RESULT = 408 --用户信息
SUB_GP_QUERY_INSURE_OPEN = 409  --查询银行是否开通
SUB_GP_QUERY_INSURE_OPEN_RESULT = 500   --查询银行是否开通结果
SUB_GP_OPERATE_SUCCESS = 900    --操作成功
SUB_GP_OPERATE_FAILURE = 901    --操作失败

MDM_GP_REMOTE_SERVICE = 4   --远程服务
SUB_GP_C_SEARCH_DATABASE = 100  --数据查找
SUB_GP_C_SEARCH_CORRESPOND = 101    --协调查找
SUB_GP_S_SEARCH_DATABASE = 200  --数据查找
SUB_GP_S_SEARCH_CORRESPOND = 201    --协调查找

MDM_MB_LOGON = 100          --广场登录
SUB_MB_LOGON_GAMEID = 1     --I D 登录
SUB_MB_LOGON_ACCOUNTS = 2   --帐号登录
SUB_MB_REGISTER_ACCOUNTS = 3    --注册帐号
SUB_MB_QUICK_REGISTER = 4       --快速注册
SUB_MB_QUICK_REGISTER_RESULT = 5   --快速注册结果
SUB_GP_C_BIND_ACCOUNT = 6		--绑定账号
SUB_GP_S_BIND_ACCOUNT_RESULT = 7	--绑定账号结果
SUB_MB_LOGON_SUCCESS = 100      --登录成功
SUB_MB_LOGON_FAILURE = 101      --登录失败
SUB_MB_UPDATE_NOTIFY = 200      --升级提示

MDM_MB_SERVER_LIST = 101        --列表信息
SUB_MB_LIST_KIND = 100          --种类列表
SUB_MB_LIST_SERVER = 101        --房间列表
SUB_MB_LIST_FINISH = 200        --列表完成

-- 乐券业务
MDM_GP_LEQUAN_SERVICE  = 7


SUB_GP_C_QUERY_LEQUAN_EXCHANGE_INFO = 1         -- 查询乐券兑换信息
SUB_GP_C_QUERY_SCORE_EXCHANGE_INFO = 2          -- 查询积分兑换信息
SUB_GP_C_QUERY_REFILLCARD_EXCHANGE_INFO = 3     -- 查询充值卡兑换信息
SUB_GP_C_QUERY_USER_LEQUAN_INFO = 4             -- 查询用户乐券信息
SUB_GP_C_QUERY_TREASUREBOX_ITEM = 5             -- 查询宝箱奖品条目
SUB_GP_C_QUERY_TREASUREBOX_CONDITION = 6        -- 查询打开宝箱条件
SUB_GP_C_QUERY_USER_GAME_INNINGS = 7            -- 查询玩家当天玩的游戏局数
SUB_GP_C_QUERY_USER_OPEN_TREASUREBOX_INFO = 8   -- 查询用户当天开宝箱信息
SUB_GP_C_EXCHANGE_LEQUAN = 9                    -- 兑换乐券
SUB_GP_C_EXCHANGE_SCORE = 10                    -- 兑换积分
SUB_GP_C_EXCHANGE_REFILLCARD = 11               -- 兑换充值卡
SUB_GP_C_OPEN_TREASUREBOX = 12                  -- 打开宝箱

SUB_GP_S_LEQUAN_EXCHANGE_INFO = 100             -- 乐券兑换信息
SUB_GP_S_SCORE_EXCHANGE_INFO = 101              -- 积分兑换信息
SUB_GP_S_REFILLCARD_EXCHANGE_INFO = 102         -- 充值卡兑换信息
SUB_GP_S_USER_LEQUAN_INFO = 103                 -- 用户乐券信息
SUB_GP_S_TREASUREBOX_ITEM = 104                 -- 宝箱奖品条目
SUB_GP_S_TREASUREBOX_CONDITION = 105            -- 打开宝箱条件
SUB_GP_S_USER_GAME_INNINGS = 106                -- 玩家当天玩的游戏局数
SUB_GP_S_USER_OPEN_TREASUREBOX_INFO = 107       -- 用户当天开宝箱信息
SUB_GP_S_EXCHANGE_LEQUAN_RESULT = 108           -- 兑换乐券结果
SUB_GP_S_EXCHANGE_SCORE_RESULT = 109            -- 兑换积分结果
SUB_GP_S_EXCHANGE_REFILLCARD_RESULT = 110       -- 兑换充值卡结果
SUB_GP_S_OPEN_TREASUREBOX_RESULT = 111          -- 打开宝箱结果
SUB_GP_C_EXCHANGE_REFILLCARD_RECORD = 13 		-- 兑换充值卡记录
SUB_GP_C_EXCHANGE_SCORE_RECORD = 14 			-- 兑换积分记录
SUB_GP_S_EXCHANGE_REFILLCARD_RECORD = 113		-- 兑换充值卡记录
SUB_GP_S_EXCHANGE_SCORE_RECORD = 114 			-- 兑换积分记录


--游戏服务器命令Id
MDM_GR_LOGON            =   1    --登录信息
SUB_GR_LOGON_USERID     =   3    --ID登录
SUB_GR_LOGON_MOBILE     =   2    --账号登录
SUB_GR_LOGON_USERID_FISH     =   14    --ID登录
SUB_GR_LOGON_ACCOUNTS   =   3    --帐户登录
SUB_GR_LOGON_SUCCESS    =   100    --登录成功
SUB_GR_LOGON_FAILURE    =   101    --登录失败
SUB_GR_LOGON_FINISH     =   102    --登录完成
SUB_GR_LOGON_GATEWAY_FINISH = 103  --登录网关完成
SUB_GR_LOGON_GATEWAY_C_HEART = 104  --网关心跳  
SUB_GR_LOGON_GATEWAY_S_HEART = 105  --网关心跳  

SUB_GR_UPDATE_NOTIFY    =   200    --升级提示

MDM_GR_CONFIG           =   3    --配置信息
SUB_GR_CONFIG_COLUMN    =   100    --列表配置
SUB_GR_CONFIG_SERVER    =   101    --房间配置
SUB_GR_CONFIG_PROPERTY  =   102    --道具配置
SUB_GR_CONFIG_FINISH    =   103    --配置完成
SUB_GR_CONFIG_USER_RIGHT    =    104    --玩家权限

MDM_GR_USER           	  	=    2    --用户信息
SUB_GR_USER_SITDOWN    		=    1    --坐下请求
SUB_GR_USER_LOOKON     		=    2    --旁观请求
SUB_GR_USER_STANDUP    		=    3    --起立请求
SUB_GR_USER_COMPULSION_STANDUP     =    4    --强制离开座位
SUB_GR_USER_INVITE      	=    5    --用户邀请
SUB_GR_USER_INVITE_REQ      =    6    --邀请请求
SUB_GR_USER_REPULSE_SIT     =    7    --拒绝玩家坐下
SUB_GR_USER_KICK_USER       =    8    --踢出用户
SUB_GR_USER_INFO_REQ        =    9    --请求用户信息
SUB_GR_USER_CHAIR_REQ       =    10    --请求更换位置
SUB_GR_USER_CHAIR_INFO_REQ   =    11    --请求椅子用户信息
SUB_GR_USER_WAIT_DISTRIBUTE  =    12    --等待分配
SUB_GR_USER_ENTER       =    100    --用户进入
SUB_GR_USER_STATUS      =    101    --用户状态
SUB_GR_USER_SCORE       =    102    --用户分数
SUB_GR_REQUEST_FAILURE  =    103    --请求失败
SUB_GR_USER_CHAT        =    201    --聊天消息
SUB_GR_USER_EXPRESSION      =    202    --表情消息
SUB_GR_WISPER_CHAT          =    203    --私聊消息
SUB_GR_WISPER_EXPRESSION    =    204    --私聊表情
SUB_GR_COLLOQUY_CHAT        =    205    --会话消息
SUB_GR_COLLOQUY_EXPRESSION  =    206    --会话表情
SUB_GR_PROPERTY_BUY         =    300    --购买道具
SUB_GR_PROPERTY_SUCCESS     =    301    --道具成功
SUB_GR_PROPERTY_FAILURE     =    302    --道具失败
SUB_GR_PROPERTY_MESSAGE     =    303    --道具消息
SUB_GR_PROPERTY_EFFECT      =    304    --道具效应
SUB_GR_PROPERTY_TRUMPET     =    305    --喇叭消息

MDM_GR_STATUS               =    4    --状态信息
SUB_GR_TABLE_INFO           =    100    --桌子信息
SUB_GR_TABLE_STATUS         =    101    --桌子状态
MDM_GR_INSURE               =    5    --银行信息
SUB_GR_QUERY_INSURE_INFO    =    1    --查询银行
SUB_GR_SAVE_SCORE_REQUEST   =    2    --存款操作
SUB_GR_TAKE_SCORE_REQUEST   =    3    --取款操作
SUB_GR_TRANSFER_SCORE_REQUEST       =    4    --取款操作
SUB_GR_QUERY_USER_INFO_REQUEST      =    5    --查询用户
SUB_GR_USER_INSURE_INFO     =    100    --银行资料
SUB_GR_USER_INSURE_SUCCESS  =    101    --银行成功
SUB_GR_USER_INSURE_FAILURE  =    102    --银行失败
SUB_GR_USER_TRANSFER_USER_INFO      =    103    --用户资料
MDM_GR_MANAGE           =    6    --管理命令
SUB_GR_SEND_WARNING     =    1    --发送警告
SUB_GR_SEND_MESSAGE     =    2    --发送消息
SUB_GR_LOOK_USER_IP     =    3    --查看地址
SUB_GR_KILL_USER        =    4    --踢出用户
SUB_GR_LIMIT_ACCOUNS    =    5    --禁用帐户
SUB_GR_SET_USER_RIGHT   =    6    --权限设置
SUB_GR_QUERY_OPTION     =    7    --查询设置
SUB_GR_OPTION_SERVER    =    8    --房间设置
SUB_GR_OPTION_CURRENT   =    9    --当前设置
SUB_GR_LIMIT_USER_CHAT  =    10    --限制聊天
SUB_GR_KICK_ALL_USER    =    11    --踢出用户
SUB_GR_DISMISSGAME      =    12    --解散游戏

MDM_GR_MATCH        =    7    --比赛命令
SUB_GR_MATCH_FEE    =    400    --报名费用
SUB_GR_MATCH_NUM    =    401    --等待人数
SUB_GR_LEAVE_MATCH  =    402    --退出比赛
SUB_GR_MATCH_INFO   =    403    --比赛信息
SUB_GR_MATCH_WAIT_TIP   =    404    --等待提示
SUB_GR_MATCH_RESULT     =    405    --比赛结果
SUB_GR_MATCH_STATUS     =    406    --比赛状态
SUB_GR_MATCH_DESC       =    408    --比赛描述
SUB_GR_MATCH_INFO_ER_SPARROWS    =    410    --比赛信息(2人麻将)
--框架消息
SUB_GF_GAME_OPTION       = 1        --游戏配置     
SUB_GF_USER_READY        = 2        --用户准备     
SUB_GF_LOOKON_CONFIG     = 3        --旁观配置     
SUB_GF_USER_CHAT         = 10       --用户聊天     
SUB_GF_USER_EXPRESSION   = 11       --用户表情     
SUB_GF_GAME_STATUS       = 100      --游戏状态     
SUB_GF_GAME_SCENE        = 101      --游戏场景     
SUB_GF_LOOKON_STATUS     = 102      --旁观状态     
SUB_GF_SYSTEM_MESSAGE    = 200      --系统消息     
SUB_GF_ACTION_MESSAGE    = 201      --动作消息     
SUB_GF_GAME_SCENE_PLAY   = 111      --游戏场景(Play)

MDM_CM_SYSTEM            = 1000     --系统命令
SUB_CM_SYSTEM_MESSAGE    = 1        --系统消息
SUB_CM_ACTION_MESSAGE    = 2        --动作消息
SUB_CM_DOWN_LOAD_MODULE  = 3        --动作消息
MDM_GF_RELIEF = 7                   --救济金
SUB_GF_S_GRANT_RELIEF = 1           --赠送救济金

MDM_GP_GAME_GATEWAY = 6     --游戏网关
SUB_GP_C_QUERY_GAME_GATEWAY = 1     --获取网关
SUB_GP_S_GAME_GATEWAY_ADDRESS = 2   --网关地址

MDM_GP_RANKING = 8     --排行
SUB_GP_C_QUERY_TREASURE_RANKING = 1     --查询财富排行
SUB_GP_C_QUERY_MONTH_GAIN_RANKING = 2   --查询月盈利排行
SUB_GP_C_QUERY_WEEK_GAIN_RANKING = 3     --查询周盈利排行
SUB_GP_C_QUERY_DAY_GAIN_RANKING = 4   --查询天盈利排行
SUB_GP_S_TREASURE_RANKING = 100     --财富排行
SUB_GP_S_MONTH_GAIN_RANKING = 101   --月盈利排行
SUB_GP_S_WEEK_GAIN_RANKING = 102     --周盈利排行
SUB_GP_S_DAY_GAIN_RANKING = 103   --天盈利排行

MDM_GP_FEEDBACK = 9     --反馈
SUB_GP_C_USER_FEEDBACK = 1     --用户反馈
SUB_GP_S_FEEDBACK_RESULT = 100   --反馈结果

MDM_GP_TRUMPET = 10    --小喇叭
SUB_GP_C_QUERY_TRUMPET_INFO = 1    --查询喇叭信息
SUB_GP_C_QUERY_USER_TRUMPET = 2    --查询用户喇叭
SUB_GP_C_BUY_TRUMPET = 3    --购买喇叭
SUB_GP_S_TRUMPET_INFO = 100    --喇叭信息
SUB_GP_S_USER_TRUMPET = 101    --用户喇叭
SUB_GP_S_BUY_TRUMPET_RESULT = 102    --购买喇叭结果
        
--因为游戏抽奖中已经有宝箱相关的协议了，为了以示区分，这里的定义以红包作为区分
MDM_GP_REDENVELOPE = 11    --红包
SUB_GP_C_QUERY_REDENVELOPE_INFO = 1    --查询红包信息
SUB_GP_C_QUERY_USER_REDENVELOPE = 2    --查询用户红包
SUB_GP_C_EXCHANGE_REDENVELOPE = 3    --兑换红包
SUB_GP_C_GIVE_REDENVELOPE = 4    --赠送红包
SUB_GP_C_OPEN_REDENVELOPE = 5    --打开红包
SUB_GP_S_REDENVELOPE_INFO = 100    --红包信息
SUB_GP_S_USER_REDENVELOPE = 101    --用户红包
SUB_GP_S_EXCHANGE_REDENVELOPE = 102    --兑换红包
SUB_GP_S_GIVE_REDENVELOPE = 103    --赠送红包
SUB_GP_S_OPEN_REDENVELOPE = 104    --打开红包

MDM_GF_TRUMPET = 10 --小喇叭
SUB_GF_C_USE_TRUMPET = 1    --使用喇叭
SUB_GF_S_TRUMPET_MSG = 100  --使用喇叭

--MDM_GF_FRAME = 100  --框架命宁
SUB_GF_HEART = 9    --心跳


SUB_GP_C_RECV_RELIEF_DONATE = 120  --领取救济金
SUB_GP_S_RECV_RELIEF_DONATE_RESULT = 121 --领取救济金结果

SUB_GP_C_QUERY_USER_RELIEF = 118  --查询玩家当天救济信息
SUB_GP_S_USER_RELIEF_INFO = 119   --玩家当天的救济信息

SUB_GP_C_GIVE_REDENVELOPE_RECORD = 6 -- 赠送红包记录
SUB_GP_C_RECV_REDENVELOPE_RECORD = 7 -- 获赠红包记录


SUB_GP_S_GET_REDENVELOPE = 105 -- 被赠送红包 add lee 2016.01.19
SUB_GP_S_GIVE_REDENVELOPE_RECORD = 106 -- 赠送红包记录
SUB_GP_S_RECV_REDENVELOPE_RECORD = 107 -- 获赠红包记录

SUB_GP_C_LUCKY_ROLLER = 122            --幸运转盘
SUB_GP_S_LUCKY_ROLLER_RESULT = 123     --幸运转盘结果


MDM_GP_REALPROPS_SERVICE = 12 -- 实物道具

SUB_GP_C_QUERY_USER_REALPROPS_INFO = 1 -- 查询用户实物道具信息
SUB_GP_S_USER_REALPROPS_INFO = 2 -- 用户实物道具信息
SUB_GP_C_SUBMIT_DELIVERY_ADDRESS = 3 -- 提交收货地址
SUB_GP_S_SUBMIT_DELIVERY_ADDRESS_RESULT = 4 -- 提交收货地址结果
SUB_GP_C_QUERY_DELIVERY_ADDRESS = 5 -- 查询收货地址
SUB_GP_S_DELIVERY_ADDRESS = 6 -- 收货地址
SUB_GP_C_EXCHANGE_REALPROPS = 7 -- 兑换实物道具
SUB_GP_S_EXCHANGE_REALPROPS_RESULT = 8 -- 兑换实物道具结果

MDM_GP_EXCHANGE_SERVICE = 13 -- 兑换记录服务

SUB_GP_C_QUERY_USER_EXCHANGE_RECORDS = 1 -- 查询用户兑换记录
SUB_GP_S_USER_EXCHANGE_RECORDS = 2 -- 用户兑换记录
SUB_GP_C_QUERY_HISTORY_TRANSACTION_USER = 3 -- 查询历史交易用户
SUB_GP_S_HISTORY_TRANSACTION_USER = 4 -- 历史交易用户

MDM_GP_REDENVELOPE = 11 -- 红包

SUB_GP_C_EXCHANGE_AND_GIVE_REDENVELOPE = 8 --兑换并赠送红包
SUB_GP_S_EXCHANGE_AND_GIVE_REDENVELOPE = 108 --兑换并赠送红包