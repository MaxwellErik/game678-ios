--
-- Author: vincent
-- Date: 2016-03-23 11:17:50
--
NewStoreConfig = {
	limitTab = {limit = 100000, label = "恭喜！您在游戏中拥有了许多财富！您可以通过购买宝箱的办法将自己的金币存起来哦！宝箱可以自己使用，也可送给好友哦！”"},
	leixingTab = {"buycointips.png", "buydaojutips.png", "buyshiwutips.png", "buyjilutips.png"},
	coinconfig = {{price = 6, image = "coinka_6.png", goodsIndex = 2},
				{price = 12, image = "coinka_12.png", goodsIndex = 3},
				{price = 30, image = "coinka_30.png", goodsIndex = 4},
				{price = 50, image = "coinka_50.png", goodsIndex = 5},
				{price = 128, image = "coinka_128.png", goodsIndex = 6},
				{price = 328, image = "coinka_328.png", goodsIndex = 7},
				{price = 618, image = "coinka_618.png", goodsIndex = 8}},

	daojuconfig = {{name = "小宝箱", image = "xiaobaoxiang.png", price = 100000, imagename = "surexiaobox.png",
				typenum = 2,
				detail = "打开获得10万金币。您可以通过购买宝箱的方式存储您的金币，您也可以赠送给您的朋友。"},
				{name = "宝箱", image = "baoxiang.png", price = 500000, imagename = "surebox.png",
				typenum = 4,
				detail = "打开获得50万金币。您可以通过购买宝箱的方式存储您的金币，您也可以赠送给您的朋友。"},
				{name = "大宝箱", image = "dabaoxiang.png", price = 1000000, imagename = "surebigbox.png",
				typenum = 5,
				detail = "打开获得100万金币。您可以通过购买宝箱的方式存储您的金币，您也可以赠送给您的朋友。"},
				{name = "小喇叭", image = "xiaolaba.png", price = 100000, imagename = "xiaolabapic.png",
				detail = "通过消耗小喇叭发送的喇叭消息，所有在线的玩家都可以看到。您可以在游戏中使用它。"}},

	shiwuconfig = {{name = "10元话费充值卡", image = "10yuanhuafei.png", imagename = "10yuanhuafei.png"},
				{name = "100元话费充值卡", image = "100yuanhuafei.png", imagename = "100yuanhuafei.png"},
				{name = "100元京东购物券", image = "jdgouwujuan.png", imagename = "jdgouwujuan.png"},
				{name = "iphone6s手机", image = "ipone6s.png", imagename = "ipone6s.png"}},

	tipconfig = {string = "在兑换实物道具前，必须先完善收货人信息！",
				sureBtn1name = "HallUI/NewStore/sureBtn1.png",
				sureBtn2name = "HallUI/NewStore/sureBtn2.png",
				cancelBtn1name = "HallUI/NewStore/cancelBtn1.png",
				cancelBtn2name = "HallUI/NewStore/cancelBtn2.png"}
}