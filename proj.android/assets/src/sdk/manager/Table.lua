
import("..ITable")
Table = class("Table", ITable)

ChongLianTime = 0       --断线重连次数
IsChongLian = false     --是否有是断线重连
require("ServerData")
--[[
Table：维护玩家所在桌子的信息，目前采用系统自动匹配模式，所以不维护非玩家所在桌子的信息
]]
function Table:ctor()
    self._iGame = nil
    self._serverInfo = nil
	self:reset()
	self._sendHeartScheduler = nil
end

function Table:reset()
    self._gameServerConnected = false
    self._loginSuccessInfo = nil
    self._playerList = {}
    self._tableId = -1
    self._bSelfEnter = false
    self._bGameConf = false
    self._nHeartNumber = 0
    self:closeHeartScheduler()
end

function Table:setClientVersion(dwClientVersion)
	self._dwClientVersion = dwClientVersion
end

function Table:registGameLogic(gameLogic)
	self._iGame = gameLogic
    -- dump(self._iGame)
	if gameLogic then
	   self._gameServer:beginCountDown()
    else
        self._gameServer:endCountDown()
    end
end

function Table:pauseCountDown()
    self._gameServer:pauseCountDown()
end

function Table:continueCountDown()
    self._gameServer:continueCountDown()
end

--游戏具体协议，由每个游戏自己解析处理
function Table:onGameDetailMessage(mainId, subId, msg)
    if mainId == MDM_GR_LOGON and subId == SUB_GR_LOGON_GATEWAY_FINISH then
        self._gameServer:sendMessage(MDM_GR_LOGON, SUB_GR_LOGON_USERID_FISH, self._loginInfo)
        return
    elseif mainId == MDM_GR_LOGON and subId == SUB_GR_LOGON_FINISH then
        if VERSION_DEBYG == true then
            print( "登录完成，请求坐下" )
        end
        self:sendSit(INVALID_TABLE,INVALID_CHAIR)
    end
    if self._iGame then
    	self._iGame:onDetailMessage(mainId, subId, msg)
    end
end

function Table:onGameMessage(mainId, subId, msgDetail)
    if VERSION_DEBYG == true then
        print("Table:onGameMessage ",mainId,subId,msgDetail)
    end
    --dump(msgDetail);
    if mainId == MDM_GR_LOGON then    --登录信息
        self:onLoginMessage(subId, msgDetail)
    elseif mainId == MDM_GR_CONFIG then  --配置信息
        self:onConfigMessage(subId, msgDetail)
    elseif mainId == MDM_GR_USER then  --用户信息
        if VERSION_DEBYG == true then
            print("用户信息")
        end
        self:onUserMessage(subId, msgDetail)
    elseif mainId == MDM_GR_STATUS then  --状态信息
        self:onStatusMessage(subId, msgDetail)
    elseif mainId == MDM_GR_INSURE then  --银行信息
        self:onBankMessage(subId, msgDetail)
    elseif mainId == MDM_CM_SYSTEM then  --系统消息
        self:onSystemMessage(subId,msgDetail)
    elseif mainId == MDM_GF_RELIEF then  --救济金消息
        self:onReliefMessage(subId,msgDetail)
    elseif mainId == MDM_GF_TRUMPET then --小喇叭
         self:onTrumpetMessage(subId, msgDetail)
    elseif mainId == MDM_GF_FRAME then
        self:onFrameMessage(subId, msgDetail)
    end
end


---------------------------
--@param
--@return
function Table:onFrameMessage(subId, msgDetail)
    if subId == SUB_GF_HEART then
		self:onGameHeart()
	end
end

function Table:onTrumpetMessage(subId, msgDetail)
    if VERSION_DEBYG == true then
        print('有人发喇叭消息')
    end

    local errorId = msgDetail.lErrorCode
    local remain = {}
    print("-------errorId"..errorId)
    for i = 1, 13 do
        errorId, remain = math.modf(errorId/2)
        print("------"..errorId.."      "..remain)
    end 
    if remain == 0.5 then
        app.eventDispather:dispatherEvent(eventGameReqFailed, msgDetail)
        return
    end
    --dump(msgDetail)
 --    if subId == SUB_GF_S_TRUMPET_MSG then
 --        if self._iGame then
 --            self._iGame:onTrumpetMessage(msgDetail.szSenderNickname, msgDetail.szContent)
 --        end
	-- end
end

--救济金消息
function Table:onReliefMessage(subId,msg)
	if subId == SUB_CM_SYSTEM_MESSAGE then
        --把修改过后的player的金币传过去 
        app.hallLogic._loginSuccessInfo.lUserScore = msg.llScore;
        for index, player in ipairs(self._playerList) do
            if player.dwUserID == app.hallLogic._loginSuccessInfo["dwUserID"] then
                    player.lScore = msg["llScore"] 
                    table.remove(self._playerList,index);    
                    table.insert(self._playerList,player);
                break           
            end
        end  
        
        if self._iGame then
            self._iGame:onReliefMessage(msg)
        end
    end
    
end

function Table:onSystemMessage(subId, msg)
    if subId == SUB_CM_SYSTEM_MESSAGE then --系统消息
        -- if self._iGame then
        --     self._iGame:onSysNotice(msg)
        -- end
    elseif subId == SUB_CM_ACTION_MESSAGE then  --动作消息

    elseif subId == SUB_CM_DOWN_LOAD_MODULE then  --下载消息

    end
end

function Table:onLoginMessage(subId, msg)
    if subId == SUB_GR_LOGON_SUCCESS then --登录成功
        self._loginSuccessInfo = msg
        app.eventDispather:dispatherEvent(eventGameLoginSuccess, msg)
        
        self:closeHeartScheduler()
        local onHeartSchedule = function()
            --发送心跳
            self:sendGameHeart()
        end
        
        local scheduler = cc.Director:getInstance():getScheduler()
        self._sendHeartScheduler = scheduler:scheduleScriptFunc(onHeartSchedule, 10.0, false)  --时间记录
               
    elseif subId == SUB_GR_LOGON_FAILURE then  --登录失败
        app.eventDispather:dispatherEvent(eventGameLoginFailed, msg)
        self:closeHeartScheduler()
    elseif subId == SUB_GR_LOGON_FINISH then  --登录完成
        app.eventDispather:dispatherEvent(eventGameLoginFinish, msg)
    elseif subId == SUB_GR_UPDATE_NOTIFY then  --升级提示
        app.eventDispather:dispatherEvent(eventGameUodate, msg)
    elseif subId == SUB_GR_LOGON_GATEWAY_FINISH then  --登录网关完成
        self._gameServer:sendMessage(MDM_GR_LOGON, SUB_GR_LOGON_USERID_FISH, self._loginInfo)
    end
end

function Table:sendDetailMessage(mainId, subId, msgDetail)
    self._gameServer:sendDetailMessage(mainId, subId, msgDetail)
end

function Table:onGameHeart()
	self._nHeartNumber = 0
end

function Table:closeHeartScheduler()
    self._nHeartNumber = 0
    local scheduler = cc.Director:getInstance():getScheduler()
    if self._sendHeartScheduler then
        scheduler:unscheduleScriptEntry(self._sendHeartScheduler)
        self._sendHeartScheduler = nil
    end
end

function Table:onConfigMessage(subId, msg)
    if subId == SUB_GR_CONFIG_COLUMN then  --列表配置
        
    elseif subId == SUB_GR_CONFIG_SERVER then  --房间配置
    
    elseif subId == SUB_GR_CONFIG_PROPERTY then --道具配置
    
    elseif subId == SUB_GR_CONFIG_FINISH then --配置完成
    
    elseif subId == SUB_GR_CONFIG_USER_RIGHT then  --玩家权限
    
    end
end

function Table:onUserMessage(subId, msg)
    if subId == SUB_GR_USER_ENTER then  --用户进入
        print("用户进入")
        self:onPlayerEnter(msg)
    elseif subId == SUB_GR_USER_SCORE then  --用户分数
        self:onPlayerScore(msg)
    elseif subId == SUB_GR_USER_STATUS then  --用户状态
        if VERSION_DEBYG == true then
            print("用户状态")
        end
        self:onPlayerStatus(msg)        
    elseif subId == SUB_GR_REQUEST_FAILURE then  --请求失败
        if VERSION_DEBYG==true then
            printf("Game Request Failed errorId:%d   msg:%s", msg.lErrorCode, msg.szDescribeString)
        end
        app.eventDispather:dispatherEvent(eventGameReqFailed, msg)
    elseif subId == SUB_GR_USER_CHAT then  --聊天消息
        if VERSION_DEBYG==true then
            printf("收到聊天信息")
        end
        app.eventDispather:dispatherEvent(eventGameChat, msg)
    elseif subId == SUB_GR_USER_EXPRESSION then  --表情消息
        app.eventDispather:dispatherEvent(eventGameChatExpression, msg)
    elseif subId == SUB_GR_WISPER_CHAT then  --私聊消息
        
    elseif subId == SUB_GR_WISPER_EXPRESSION then  --私聊表情
    
    elseif subId == SUB_GR_COLLOQUY_CHAT then  --会话消息
    
    elseif subId == SUB_GR_COLLOQUY_EXPRESSION then  --会话表情
    
    elseif subId == SUB_GR_PROPERTY_SUCCESS then  --道具成功
    
    elseif subId == SUB_GR_PROPERTY_FAILURE then  --道具失败
    
    elseif subId == SUB_GR_PROPERTY_MESSAGE then  --道具消息
    
    elseif subId == SUB_GR_PROPERTY_EFFECT then  --道具效应
    
    elseif subId == SUB_GR_PROPERTY_TRUMPET then  --喇叭消息
        if self._iGame then
            self._iGame:onTrumpetMessage(msg.szSendNickName, msg.szTrumpetContent)
        end
    end
end

function Table:onPlayerEnterExpand(msg)
    if msg.expandStr then
        local byteArray = GameByteArray:new()
        for index, byte in ipairs(msg.expandStr) do
            byteArray:writeChar(byte)
        end
        
        byteArray:setPos(1)
        while true do
            if byteArray:getPos() >= byteArray:getLen() then
                break
            end
            
            local dataLen = byteArray:readUShort()
            local dataType = byteArray:readUShort()
           
            if dataType == DTP_NULL or dataLen <= 2 or dataLen-2 > byteArray:getLen()-byteArray:getPos() then
                break
            end
            
            local bytes = {}
            for i = 1, dataLen do
                table.insert(bytes,byteArray:readByte())
                if byteArray:getPos() > byteArray:getLen() then
                    break
                end
            end
            local utf8Str = gbk_utf8(bytes)
            
            if dataType == 3 then  --昵称
                if VERSION_DEBYG == true then
                    print("昵称:", utf8Str)
                end
                msg.nickName = utf8Str
            elseif dataType == DTP_GR_GROUP_NAME then --社团名字
                if VERSION_DEBYG == true then
                    print("社团名字:", utf8Str)
                end
                msg.groupName = utf8Str
            elseif dataType == DTP_GR_UNDER_WRITE then  --个性签名
                if VERSION_DEBYG == true then
                    print("个性签名:", utf8Str)
                end
                msg.underWrite = utf8Str
            end 
        end
    end
end

function Table:onPlayerEnter(msg)
    --处理扩展字段
    self:onPlayerEnterExpand(msg)
    -- dump(msg)

    if msg.dwUserID == app.userInfo.dwUserID then
        -- if self._bSelfEnter then
        --     return      	
        -- end
        --自己进入游戏，判断自己是站立的还是已经在游戏中，断线重连从这里开始
        if VERSION_DEBYG==true then
            printf("玩家进入 userId:%d  tableID:%d  chairId:%d  userState:%d",msg.dwUserID, msg.wTableID, msg.wChairID, msg.cbUserStatus)
        end
        self._bSelfEnter = true
        if msg.cbUserStatus == US_SIT then  --自己已经坐下了，发送准备
            -- print("发送准备消息 --------------------------------- ")
            self:sendReady()
        end
        self._tableId = msg.wTableID  
    end
    
    table.insert(self._playerList, msg)
    
    if self._tableId ~= msg.wTableID then
    	return
    end
   
    if self._iGame then
    	self._iGame:onPlayerEnter(msg)
    end
end

function Table:onPlayerScore(msg)
    for index, player in ipairs(self._playerList) do

        if player.dwUserID == msg.dwUserID then
            player.lScore = msg.UserScore.lGrade
            player.lGrade = msg.UserScore.lGrade
            player.lInsure = msg.UserScore.lInsure
            player.dwWinCount = msg.UserScore.dwWinCount
            player.dwLostCount = msg.UserScore.dwLostCount
            player.dwDrawCount = msg.UserScore.dwDrawCount
            player.dwFleeCount = msg.UserScore.dwFleeCount
            player.dwUserMedal = msg.UserScore.lReturnValue
            player.dwExperience = msg.UserScore.dwExperience
            player.lLoveLiness = msg.UserScore.dwExperience
            
            
            if self._iGame then
                self._iGame:onPlayerUpdate(player)
            end            
            break    		
    	end
    end  
end

function Table:onPlayerStatus(msg)
    --判断是否是自己状态改变
    if msg.dwUserID == app.userInfo.dwUserID then
        if VERSION_DEBYG==true then
            printf("玩家信息更新 userId:%d  tableID:%d  chairId:%d  userState:%d",msg.dwUserID, msg.wTableID, msg.wChairID, msg.cbUserStatus)
       end

        --获取游戏配置信息
        if self._bGameConf == false then
            if msg.wTableID ~= INVALID_TABLE  then
                self._bGameConf = true
                if msg.cbUserStatus == US_SIT then
                    self:sendGameConfig(0)
                else
                    self:sendGameConfig(1)
                end
            end
        end
        print("用户状态改变 ..................................................... ")

        if msg.cbUserStatus == US_SIT then  --自己已经坐下了，发送准备
            self:sendReady()
        elseif msg.cbUserStatus == US_FREE then --自己站起        
            self:delPlayerById(msg.dwUserID)
            if self._iGame then
                self._iGame:onPlayerExit(msg.dwUserID)
            end       
        end
        self._tableId = msg.wTableID
        print(self._tableId .. "..................................................... ")
    end
    if msg.cbUserStatus == US_NULL then --玩家离开
        if VERSION_DEBYG == true then
            print("玩家离开")
        end
        self:delPlayerById(msg.dwUserID)
        if self._iGame then
        	self._iGame:onPlayerExit(msg.dwUserID)
        end
    else
        if VERSION_DEBYG == true then
            print("玩家update")
        end
        for index, player in ipairs(self._playerList) do
            if player.dwUserID == msg.dwUserID then 
                player.wTableID = msg.wTableID
                player.wChairID = msg.wChairID
                player.cbUserStatus = msg.cbUserStatus
                if self._iGame then
                    self._iGame:onPlayerUpdate(player)
                end
            end
        end
    end
end

function Table:delPlayerById(userId)
	for index, player in ipairs(self._playerList) do
        if player.dwUserID == userId then
            table.remove(self._playerList, index)
        end
    end
end

function Table:onStatusMessage(subId, msg)
    if subId == SUB_GR_TABLE_INFO then  --桌子信息
        
    elseif subId == SUB_GR_TABLE_STATUS then   --桌子状态
        if VERSION_DEBYG==true then
            printf("Game Table Status update tableId:%d tablelock:%d tableStatus:%d", msg.wTableID, msg.bTableLock, msg.bPlayStatus)
        end
        if self._tableId ~= msg.wTableID then
            return
        end 
        
        local deskLock = false
        local deskPlaying = false
        if msg.bTableLock == 1 then  --桌子锁定
            deskLock = true
        end
        if msg.bPlayStatus == 1 then  --游戏开始
            deskPlaying = true
        end
        
        if self._iGame then
        	self._iGame:onTableStatus(deskLock, deskPlaying)
        end
    end
end

function Table:onBankMessage(subId, msg)
    if subId == SUB_GR_USER_INSURE_INFO then  --银行资料
    
    elseif subId == SUB_GR_USER_INSURE_SUCCESS then  --银行成功
    
    elseif subId == SUB_GR_USER_INSURE_FAILURE then  --银行失败
    
    elseif subId == SUB_GR_USER_TRANSFER_USER_INFO then  --用户资料
    
    end  
end

function Table:loginByID(wKindId)
    self._loginInfo = {}
    self._loginInfo.dwPlazaVersion = VERSION_PLAZA
    --self._loginInfo.dwFrameVersion = VERSION_FRAME
    self._loginInfo.dwProcessVersion = self._dwClientVersion
    self._loginInfo.dwUserID = app.userInfo.dwUserID
    self._loginInfo.szPassword = app.userInfo.szPassword
    self._loginInfo.dbLastLogonTime = app.userInfo.loginTime
    self._loginInfo.szVersion = ""
    self._loginInfo.wDataSize = 33
    self._loginInfo.wDataDescribe = 1000
    self._loginInfo.szMachineID = getDeviceNoXS()
    -- self._loginInfo.wKindID = wKindId
    -- self._loginInfo.wServerID = 1213
    -- self._loginInfo.nChargeValue = 1213

    -- local targetPlatform = cc.Application:getInstance():getTargetPlatform() --设备系统
    -- if targetPlatform == cc.PLATFORM_OS_ANDROID then
    --     self._loginInfo.cbDeviceID = 0x10
    -- elseif targetPlatform == cc.PLATFORM_OS_IPHONE or targetPlatform == cc.PLATFORM_OS_IPAD then
    --     self._loginInfo.cbDeviceID = 0x40
    -- else
    --     self._loginInfo.cbDeviceID = 0x00
    -- end
    dump(self._loginInfo)
    
    self._gameServer:sendMessage(MDM_GR_LOGON, SUB_GR_LOGON_USERID, self._loginInfo)
end

function Table:onGameServerConnected(bConnected)
	self._gameServerConnected = bConnected
    app.eventDispather:dispatherEvent(eventGameNetConnected, bConnected)    
end

function Table:closeNet()
	self:reset()
	custom.PlatUtil:closeServer(CLIENT_GAME)
end

function Table:onGameServerClosed()
	self._gameServerConnected = false
    app.eventDispather:dispatherEvent(eventGameNetClosed)    
end

function Table:registGameServer(gameServer)
	self._gameServer = gameServer
end

function Table:getGameServer()
	return self._gameServer
end

function Table:setServerInfo(serverInfo)
	self._serverInfo = serverInfo
end

function Table:sendSit(tableId, chairId)
    print("发送做下椅子消息 ---------------------------------------------------")
    local msg = {}
    msg.dwProcessVersion = 0
    msg.wTableID = tableId
    msg.wChairID = chairId
    msg.szVersion = "!@#$%^123"
    msg.cbPassLen = 33
    msg.szTablePass = ""
    dump(msg)
    self._gameServer:sendMessage(MDM_GR_USER, SUB_GR_USER_SITDOWN, msg)
end

function Table:sendStandup()
    if VERSION_DEBYG == true then
        print( "Table:sendStandup" )
    end
    local player = self:queryPlayerByID(app.userInfo.dwUserID)
    if player then
        self._gameServer:sendMessage(MDM_GR_USER, SUB_GR_USER_COMPULSION_STANDUP)
    end
end

function Table:queryPlayerByID(userID) 
    for key, player in pairs(self._playerList) do  
        if player.dwUserID==userID then
           return player
        end 
    end 
    
end

function Table:sendReady()
    self._gameServer:sendMessage(MDM_GF_FRAME, SUB_GF_USER_READY)
end

function Table:sendGameConfig(bLookOn)
    local msg = {}
    msg.cbAllowLookon = bLookOn
    msg.szVersion = "!@#$%^123"

    self._gameServer:sendMessage(MDM_GF_FRAME, SUB_GF_GAME_OPTION, msg)
end

function Table:sendUserTrumpet(content)
    local msg = {}
    msg.cbAllRoom = 1
    msg.szSenderNickname = app.hallLogic._loginSuccessInfo.szNickName
    msg.szContent = content

    self._gameServer:sendMessage(MDM_GF_TRUMPET, SUB_GF_C_USE_TRUMPET, msg)
    if VERSION_DEBYG == true then
        print('发送喇叭')
    end
end

function Table:sendChatMessage(content)
    local msg = {}
    msg.szChatString = content

    self._gameServer:sendMessage(MDM_GR_USER, SUB_GR_USER_CHAT, msg)
    if VERSION_DEBYG == true then
        print('发送聊天信息')
    end
end

function Table:sendGameHeart()
    -- if VERSION_DEBYG == true then
    --     print("发送游戏心跳")
    -- end
    -- local msg = {}
    -- msg.wHeart = 0xFE03
    -- self._gameServer:sendMessage(MDM_GF_FRAME, SUB_GF_HEART, msg)
    
    -- self._nHeartNumber = self._nHeartNumber + 1
    -- if self._nHeartNumber >=3 then  --心跳超时
    --     if VERSION_DEBYG == true then
    --         print('游戏心跳超时')
    --     end
    --     custom.PlatUtil:closeServer(CLIENT_GAME)
    --     self:onGameServerClosed()
    --     self:closeHeartScheduler()
    -- end
end

return Table