--
-- Author: YL
-- Date: 2016-01-14 14:24:58
--
local ChatUI = class("ChatUI")

function ChatUI:ctor()
end

function ChatUI:getChatText(str, vipnum, namestr, lenth)
	local node = cc.Node:create()
	--是否是表情
	local i, j = string.gsub(str, "%$%$", "")
	
	if tonumber(i) and j == 2 and tonumber(i) <= 23 and tonumber(i) >= 0 and string.len(str) <= 6 then
		-- 名字
		local lable = cc.Label:create()
		lable:setSystemFontSize(26)
		lable:setString(namestr..": ")
		if vipnum > 0 then
			lable:setString("         "..namestr..": ")
			lable:setColor(cc.c3b(213, 164, 15))
		end
		local size1 = lable:getContentSize()
		lable:setPosition(cc.p(size1.width/2, size1.height/2))
		node:addChild(lable)

 		if tonumber(i) < 10 then
 			i = tostring("0"..i)
 		end
 		local b_pic = display.newSprite("HallUI/Talk/"..i..".png")
		local size2 = b_pic:getContentSize()
		b_pic:setPosition(cc.p(size1.width+size2.width/2, size2.height/2))
		node:addChild(b_pic)
		node:setContentSize(cc.size(lenth, size2.height))

		if vipnum > 0 then
			-- Vip标记
			local pic = display.newSprite("Vip/VIP"..vipnum..".png")
			local size = pic:getContentSize()
			pic:setPosition(cc.p(size.width/2, size1.height-size.height/2+10))
			node:addChild(pic)
			node:setContentSize(cc.size(lenth, size.height))
		end
	else
		-- 聊天文字
		local lable_1 = cc.Label:create()
		local strMsg = namestr..": "..str
		if vipnum > 0 then
			strMsg = "         "..namestr..": "..str
			lable_1:setColor(cc.c3b(213, 164, 15))
		end
		
		--lable_1:setColor(cc.c3b(213, 164, 15));
		strMsg = insertSubPerLen(strMsg, 30, "\n")
		lable_1:setSystemFontSize(26)
		lable_1:setString(strMsg)
		local size1 = lable_1:getContentSize()
		lable_1:setPosition(cc.p(size1.width/2, size1.height/2))
		node:addChild(lable_1)
		node:setContentSize(cc.size(lenth, size1.height))

		if vipnum > 0 then
			-- Vip标记
			local pic = display.newSprite("Vip/VIP"..vipnum..".png")
			local size = pic:getContentSize()
			pic:setPosition(cc.p(size.width/2, size1.height-size.height/2+10))
			node:addChild(pic)
			node:setContentSize(cc.size(lenth, size1.height))
		end	
    end
	return node
end

-- 富文本
function ChatUI:getRichText(str, lenth)
	local richText = ccui.RichText:create()
	--richText:ignoreContentAdaptWithSize(false)
	local my_lenth = 0
    if str then
		local colorTab = {}
		local stringTab = {}
    	local _,_, sf = str:find("([^#]+)")
    	local re1 = ccui.RichElementText:create(4, cc.c3b(255, 255, 255), 255, sf, "微软雅黑", 30)
    	local lable1 = cc.Label:createWithSystemFont(sf, "微软雅黑", 30)
    	my_lenth = lable1:getContentSize().width + my_lenth
		richText:pushBackElement(re1)

		for k, v in str:gmatch("##([^##]+)##([^##]+)") do
		    table.insert(colorTab, k)
		    table.insert(stringTab, v)
		end
		for k, v in pairs(colorTab) do
			local re1 = ccui.RichElementText:create(k+5, cc.c3b(self:ConvertHexToRGB(v)), 255, stringTab[k], "微软雅黑", 30)
			local lable1 = cc.Label:createWithSystemFont(stringTab[k], "微软雅黑", 30)
			my_lenth = lable1:getContentSize().width + my_lenth
			richText:pushBackElement(re1)
		end
    end

 --    if my_lenth >= lenth then
	-- 	richText:setContentSize(cc.size(lenth, 40))
	-- else
	-- 	richText:setContentSize(cc.size(my_lenth, 40))
	-- end

    richText:formatText()
    richText:setAnchorPoint(cc.p(0.5, 0.5))

    return richText, my_lenth
end

function ChatUI:ConvertHexToRGB(hex)
	local red = string.sub(hex, 1, 2)  
	local green = string.sub(hex, 3, 4)  
	local blue = string.sub(hex, 5, 6)  
	 
	red = tonumber(red, 16)  
	green = tonumber(green, 16)
	blue = tonumber(blue, 16) 
	 
	return red, green, blue  
end

function ChatUI:getRGB(r, g, b)
	local colorinfo = {}
    colorinfo.r = r
    colorinfo.g = g
    colorinfo.b = b
    return colorinfo
end


function ChatUI:getQueueStrs(str)
	if str then
		local colorTab = {}
		local stringTab = {}
		local retStrs = {}
    	local _,_, sf = str:find("([^#]+)")

    	local charNull = " "   --android用2个空格，win32用1个空格
    	
    	if sf and sf ~= "" then
    		table.insert(colorTab, self:getRGB(255,255,255))
    		table.insert(stringTab, sf)
    	end

		for k, v in str:gmatch("##([^##]+)##([^##]+)") do
		    table.insert(colorTab, self:getRGB(self:ConvertHexToRGB(k)))
		    table.insert(stringTab, v)
		end

		for index1, item1 in ipairs(stringTab) do
			local strTmp = ""
			for index2, item2 in ipairs(stringTab) do
				if index1 ~= index2 then
					local len = string.len(item2)
					local index = 1
					local i = 1
				    while i<=len do
				        local curByte = string.byte(item2, i)
				        local byteCount = 0;
				        if curByte>0 and curByte<=127 then
				            byteCount = 1
				        elseif curByte>=192 and curByte<=223 then
				            byteCount = 2
				        elseif curByte>=224 and curByte<=239 then
				            byteCount = 3
				        elseif curByte>=240 and curByte<=247 then
				            byteCount = 4
				        else
				        	byteCount = 1
				        end
			        
				        if byteCount == 1 then
				        	strTmp = strTmp .."  " --2个英文空格(WIN上一个空格)
				        elseif byteCount>0 then
				        	index = index + 1
				        	strTmp = strTmp .."　" --1个中文全角空格
				        end		

				        i = i + byteCount		       
					end
				else
					strTmp = strTmp .. item2
				end

			end

			table.insert(retStrs, strTmp)
		end

		return retStrs, colorTab
	end
end


return ChatUI