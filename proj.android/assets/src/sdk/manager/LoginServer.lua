
LoginServer = class("LoginServer")

function LoginServer:ctor()
	self.messageDeal = MessageDeal.new()
    self.messageDeal:parseConf("gameMsg/loginserver.json")
    
    self._hallCallBack = nil
    self._bNetConnected = false
end

function LoginServer:closeNet()
    self._bNetConnected = false       --暂时屏蔽
    custom.PlatUtil:closeServer(CLIENT_HALL)
end

--协议下行
function LoginServer:onGameMessage(msg)
    local mainId = msg:getMainID()
    local subId = msg:getSubId()
    local bytelen = msg:getMsgLen()
       
   
    if VERSION_DEBYG==true then
        -- printf("LoginServer::onMessage maindid:%d,  %d,  %d", mainId, subId, bytelen)
    end
    
    if msg then
        local msgDetail = self.messageDeal:decodeMessage(mainId, subId, msg)
        
        if self._hallCallBack then
            self._hallCallBack:onGameMessage(mainId, subId, msgDetail)
        end
    end
end

function LoginServer:sendMessage(mainId, subId, msg)
    --判断服务器是否连接成功
    if self._bNetConnected == false then
    	return
    end

    local msgStr = nil
    local clientId = CLIENT_HALL
    
    msgStr = self.messageDeal:encodeMessage(mainId, subId, msg)
    
    --调用c++协议发送接口   
    local netMsg = custom.NetMessage:new()
    netMsg:setMainID(mainId)
    print( subId )
    netMsg:setSubId(subId)
    if msgStr then
        netMsg:setMsgLen(msgStr:getLen())
    else
        netMsg:setMsgLen(0)
    end
    
    if VERSION_DEBYG==true then
        -- printf("LoginServer:sendMessage mainId:%d   subId:%d   len:%d", mainId, subId, netMsg:getMsgLen())
    end
    
    if msgStr then
        netMsg:setMsgLen(msgStr:getLen())
        msgStr:setPos(1)
        for i=1, msgStr:getLen() do
            netMsg:setDataAtIndex(i-1, msgStr:readUByte())
        end
    end
    
    custom.PlatUtil:sendMessage(clientId,netMsg)
end

--注册IHall接口,具体接口函数查看IHall.lua
function LoginServer:registIHall(callBack)
    self._hallCallBack = callBack
end

function LoginServer:resetIHall(parameters)
    self._hallCallBack = nil
end

function LoginServer:connect(clientId, ip, port, heartDelaytime)
    if VERSION_DEBYG == true then
        print('连接登陆服务器')
    end
    custom.PlatUtil:connectServer(clientId, ip, port, heartDelaytime)
end

function LoginServer:onNetConnected(bConnect)
    self._bNetConnected = bConnect
	self._hallCallBack:onLoginNetConnected(bConnect)
end

function LoginServer:onNetClosed()
    self._bNetConnected = false;
    self._hallCallBack:onLoginNetClosed()
end

function LoginServer:isServerConnected()
    return self._bNetConnected
end

return LoginServer