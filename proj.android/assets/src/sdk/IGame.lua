
IGame = class("IGame")

function IGame:ctor()
end

function IGame:onPlayerEnter(player)
end

function IGame:onPlayerExit(userId)
end

function IGame:onPlayerUpdate(player)
end

function IGame:onDetailMessage(mainId, subId, msg)
end

function IGame:onTableStatus(bTableLock, bTablePlaying)
end

return IGame