
GameByteArray = class("GameByteArray")

GameByteArray.ENDIAN_LITTLE = "ENDIAN_LITTLE"
GameByteArray.ENDIAN_BIG = "ENDIAN_BIG"
GameByteArray.radix = {[10]="%03u",[8]="%03o",[16]="%02X"}

require("pack")

--- Return a string to display.
-- If self is GameByteArray, read string from self.
-- Else, treat self as byte string.
-- @param __radix radix of display, value is 8, 10 or 16, default is 10.
-- @param __separator default is " ".
-- @return string, number
function GameByteArray.toString(self, __radix, __separator)
    __radix = __radix or 16 
    __radix = GameByteArray.radix[__radix] or "%02X"
    __separator = __separator or " "
    local __fmt = __radix..__separator
    local __format = function(__s)
        return string.format(__fmt, string.byte(__s))
    end
    if type(self) == "string" then
        return string.gsub(self, "(.)", __format)
    end
    local __bytes = {}
    for i=1,#self._buf do
        __bytes[i] = __format(self._buf[i])
    end
    return table.concat(__bytes) ,#__bytes
end

function GameByteArray:ctor(__endian)
    self._endian = __endian
    self._buf = {}
    self._pos = 1
end

function GameByteArray:setByte(byte, index)
    self._buf[index] = byte
end

function GameByteArray:getLen()
    return #self._buf
end

function GameByteArray:getAvailable()
    return #self._buf - self._pos + 1
end

function GameByteArray:getPos()
    return self._pos
end

function GameByteArray:setPos(__pos)
    self._pos = __pos
    return self
end

function GameByteArray:getEndian()
    return self._endian
end

function GameByteArray:setEndian(__endian)
    self._endian = __endian
end

--- Get all byte array as a lua string.
-- Do not update position.
function GameByteArray:getBytes(__offset, __length)
    __offset = __offset or 1
    __length = __length or #self._buf
    --printf("getBytes,offset:%u, length:%u", __offset, __length)
    return table.concat(self._buf, "", __offset, __length)
end

--- Get pack style string by lpack.
-- The result use GameByteArray.getBytes to get is unavailable for lua socket.
-- E.g. the #self:_buf is 18, but #GameByteArray.getBytes is 63.
-- I think the cause is the table.concat treat every item in GameByteArray._buf as a general string, not a char.
-- So, I use lpack repackage the GameByteArray._buf, theretofore, I must convert them to a byte number.
function GameByteArray:getPack(__offset, __length)
    __offset = __offset or 1
    __length = __length or #self._buf
    local __t = {}
    for i=__offset,__length do
        __t[#__t+1] = string.byte(self._buf[i])
    end
    local __fmt = self:_getLC("b"..#__t)
    --print("fmt:", __fmt)
    local __s = string.pack(__fmt, unpack(__t))
    return __s
end

--- rawUnPack perform like lpack.pack, but return the GameByteArray.
function GameByteArray:rawPack(__fmt, ...)
    local __s = string.pack(__fmt, ...)
    self:writeBuf(__s)
    return self
end

--- rawUnPack perform like lpack.unpack, but it is only support FORMAT parameter.
-- Because GameByteArray include a position itself, so we haven't to save another.
function GameByteArray:rawUnPack(__fmt)
    -- read all of bytes.
    local __s = self:getBytes(self._pos)
    local __next, __val = string.unpack(__s, __fmt)
    -- update position of the GameByteArray
    self._pos = self._pos + __next
    -- Alternate value and next
    return __val, __next
end

function GameByteArray:readBool()
    -- When char > 256, the readByte method will show an error.
    -- So, we have to use readChar
    return self:readChar() ~= 0
end

function GameByteArray:writeBool(__bool)
    if __bool then 
        self:writeByte(1)
    else
        self:writeByte(0)
    end
    return self
end

function GameByteArray:readDouble()
    local __, __v = string.unpack(self:readBuf(8), self:_getLC("d"))
    return __v
end

function GameByteArray:writeDouble(__double)
    local __s = string.pack( self:_getLC("d"), __double)
    self:writeBuf(__s)
    return self
end

function GameByteArray:readFloat()
    local __, __v = string.unpack(self:readBuf(4), self:_getLC("f"))
    return __v
end

function GameByteArray:writeFloat(__float)
    local __s = string.pack( self:_getLC("f"),  __float)
    self:writeBuf(__s)
    return self
end

function GameByteArray:writeLongLong(__longlong)
    local llongCode = custom.LLongCode:new()
	llongCode:setLonglongValue(__longlong)
	for i=0, 7 do
        self:writeByte(llongCode:getByteByIndex(i))
	end	
end

function GameByteArray:readLongLong()
	local llongCode = custom.LLongCode:new()
    for i=0, 7 do
        llongCode:setByteByIndex(self:readByte(), i)
    end
    
    return llongCode:getLongLongValue()
end

function GameByteArray:readInt()
    local __, __v = string.unpack(self:readBuf(4), self:_getLC("i"))
    return __v
end

function GameByteArray:writeInt(__int)
    local __s = string.pack( self:_getLC("i"),  __int)
    self:writeBuf(__s)
    return self
end

function GameByteArray:readUInt()
    local __, __v = string.unpack(self:readBuf(4), self:_getLC("I"))
    return __v
end

function GameByteArray:writeUInt(__uint)
    local __s = string.pack(self:_getLC("I"), __uint)
    self:writeBuf(__s)
    return self
end

function GameByteArray:readShort()
    local __, __v = string.unpack(self:readBuf(2), self:_getLC("h"))
    return __v
end

function GameByteArray:writeShort(__short)
    local __s = string.pack( self:_getLC("h"),  __short)
    self:writeBuf(__s)
    return self
end

function GameByteArray:readUShort()
    local __, __v = string.unpack(self:readBuf(2), self:_getLC("H"))
    return __v
end

function GameByteArray:writeUShort(__ushort)
    local __s = string.pack(self:_getLC("H"),  __ushort)
    self:writeBuf(__s)
    return self
end

function GameByteArray:readUByte()
    local __, __val = string.unpack(self:readRawByte(), "b")
    return __val
end

function GameByteArray:writeUByte(__ubyte)
    local __s = string.pack("b", __ubyte)
    self:writeBuf(__s)
    return self
end

function GameByteArray:readLuaNumber(__number)
    local __, __v = string.unpack(self:readBuf(8), self:_getLC("n"))
    return __v
end

function GameByteArray:writeLuaNumber(__number)
    local __s = string.pack(self:_getLC("n"), __number)
    self:writeBuf(__s)
    return self
end

--- The differently about (read/write)StringBytes and (read/write)String
-- are use pack libraty or not.
function GameByteArray:readStringBytes(__len)
    assert(__len, "Need a length of the string!")
    if __len == 0 then return "" end
    self:_checkAvailable()
    local __, __v = string.unpack(self:readBuf(__len), self:_getLC("A"..__len))
    return __v
end

function GameByteArray:writeStringBytes(__string)
    local __s = string.pack(self:_getLC("A"), __string)
    self:writeBuf(__s)
    return self
end

function GameByteArray:readString(__len)
    assert(__len, "Need a length of the string!")
    if __len == 0 then return "" end
    self:_checkAvailable()
    return self:readBuf(__len)
end

function GameByteArray:writeString(__string)
    self:writeBuf(__string)
    return self
end

function GameByteArray:readStringUInt()
    self:_checkAvailable()
    local __len = self:readUInt()
    return self:readStringBytes(__len)
end

function GameByteArray:writeStringUInt(__string)
    self:writeUInt(#__string)
    self:writeStringBytes(__string)
    return self
end

--- The length of size_t in C/C++ is mutable.
-- In 64bit os, it is 8 bytes.
-- In 32bit os, it is 4 bytes.
function GameByteArray:readStringSizeT()
    self:_checkAvailable()
    local __s = self:rawUnPack(self:_getLC("a"))
    return  __s
end

--- Perform rawPack() simply.
function GameByteArray:writeStringSizeT(__string)
    self:rawPack(self:_getLC("a"), __string)
    return self
end

function GameByteArray:readStringUShort()
    self:_checkAvailable()
    local __len = self:readUShort()
    return self:readStringBytes(__len)
end

function GameByteArray:writeStringUShort(__string)
    local __s = string.pack(self:_getLC("P"), __string)
    self:writeBuf(__s)
    return self
end

--- Read some bytes from buf
-- @return a bit string
function GameByteArray:readBytes(__bytes, __offset, __length)
    assert(iskindof(__bytes, "GameByteArray"), "Need a GameByteArray instance!")
    local __selfLen = #self._buf
    local __availableLen = __selfLen - self._pos
    __offset = __offset or 1
    if __offset > __selfLen then __offset = 1 end
    __length = __length or 0
    if __length == 0 or __length > __availableLen then __length = __availableLen end
    __bytes:setPos(__offset)
    for i=__offset,__offset+__length do
        __bytes:writeRawByte(self:readRawByte())
    end
end

--- Write some bytes into buf
function GameByteArray:writeBytes(__bytes, __offset, __length)
    assert(iskindof(__bytes, "GameByteArray"), "Need a GameByteArray instance!")
    local __bytesLen = __bytes:getLen()
    if __bytesLen == 0 then return end
    __offset = __offset or 1
    if __offset > __bytesLen then __offset = 1 end
    local __availableLen = __bytesLen - __offset
    __length = __length or __availableLen
    if __length == 0 or __length > __availableLen then __length = __availableLen end
    local __oldPos = __bytes:getPos()
    __bytes:setPos(__offset)
    for i=__offset,__offset+__length do
        self:writeRawByte(__bytes:readRawByte())
    end
    __bytes:setPos(__oldPos)
    return self
end

--- Actionscript3 readByte == lpack readChar
-- A signed char
function GameByteArray:readChar()
    local __, __val = string.unpack( self:readRawByte(), "c")
    return __val
end

function GameByteArray:writeChar(__char)
    self:writeRawByte(string.pack("c", __char))
    return self
end

--- Use the lua string library to get a byte
-- A unsigned char
function GameByteArray:readByte()
    return string.byte(self:readRawByte())
end

--- Use the lua string library to write a byte.
-- The byte is a number between 0 and 255, otherwise, the lua will get an error.
function GameByteArray:writeByte(__byte)
    self:writeRawByte(string.char(__byte))
    return self
end

function GameByteArray:readRawByte()
    self:_checkAvailable()
    local __byte = self._buf[self._pos]
    self._pos = self._pos + 1
    return __byte
end

function GameByteArray:writeRawByte(__rawByte)
    if self._pos > #self._buf+1 then
        for i=#self._buf+1,self._pos-1 do
            self._buf[i] = string.char(0)
        end
    end
    self._buf[self._pos] = string.sub(__rawByte, 1,1)
    self._pos = self._pos + 1
    return self
end

--- Read a byte array as string from current position, then update the position.
function GameByteArray:readBuf(__len)
    --printf("readBuf,len:%u, pos:%u", __len, self._pos)
    local __ba = self:getBytes(self._pos, self._pos + __len - 1)
    self._pos = self._pos + __len
    return __ba
end

--- Write a encoded char array into buf
function GameByteArray:writeBuf(__s)
    for i=1,#__s do
        self:writeRawByte(string.sub(__s,i,i))
    end
    return self
end

----------------------------------------
-- private
----------------------------------------
function GameByteArray:_checkAvailable()
    assert(#self._buf >= self._pos, string.format("End of file was encountered. pos: %d, len: %d.", self._pos, #self._buf))
end

--- Get Letter Code
function GameByteArray:_getLC(__fmt)
    __fmt = __fmt or ""
    if self._endian == GameByteArray.ENDIAN_LITTLE then
        return "<"..__fmt
    elseif self._endian == GameByteArray.ENDIAN_BIG then
        return ">"..__fmt
    end
    return "="..__fmt
end

return GameByteArray
