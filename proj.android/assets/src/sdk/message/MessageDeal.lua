--协议编解码类，此类为单例类

MessageDeal = class("MessageDeal")

function MessageDeal:ctor()
    self._structs = nil     --结构体map
    self._typedefs = nil   --常用数据类型
    self._msgArray = nil   --协议数组
end

--解析平台的协议配置文件
function MessageDeal:parseConf(confFile)
    print(confFile)
    -- print(" ----------------------------------------- 解析文件")
    local msgConfStr = cc.FileUtils:getInstance():getStringFromFile(confFile);
    if msgConfStr then
        local jsonData = json.decode(msgConfStr)
        self._structs = jsonData.structs     --结构体map
        self._typedefs = jsonData.typedefs   --常用数据类型
        self._msgArray = jsonData.msgArray   --协议数组
    else
        if VERSION_DEBYG == true then
            print("parse plate.json failed")
        end
    end
end

--根据mainId和subId解析协议
function MessageDeal:decodeMessage(mainId, subId, msg)
    local msgStruct = self:queryMessageStruct(mainId, subId, "S2C")   --获取结构体信息

    --dump(msgStruct);
    if msgStruct then
        --解析具体协议
    
        local byteArray = GameByteArray:new()
        byteArray:setEndian(GameByteArray.ENDIAN_LITTLE)
        for i=1, msg:getMsgLen() do
            byteArray:writeChar(msg:getDataAtIndex(i-1))
        end

        byteArray:setPos(1)
        if msgStruct.nodetype and msgStruct.nodetype=="ARRAY" then
            if byteArray:getLen()%self:getMsgStructLen(msgStruct)==0 then
                local structCount = byteArray:getLen()/self:getMsgStructLen(msgStruct)
                local msgRet = {}
                for i=1, structCount do
                    table.insert(msgRet,self:messageDecode(msgStruct, byteArray))
                end
                return msgRet
            end        	
        end
        
        local msgDecode = self:messageDecode(msgStruct, byteArray)
        --查看是否有扩展字段
        if byteArray:getPos() < byteArray:getLen() then
            local detail = {}
            for i=1 , byteArray:getLen()-byteArray:getPos() do 
                table.insert(detail, byteArray:readChar())
            end
            msgDecode.expandStr = detail
        end

        return msgDecode
    end
	
	if VERSION_DEBYG==true then
    	-- printf("get message mainId:%d subId:%d failed", mainId, subId)
	end
	return nil
end

--根据mainId、subId查找协议结构体 msgType:C2S/S2C
function MessageDeal:queryMessageStruct(mainId, subId, msgType)
    for index, msgInfo in pairs(self._msgArray) do
        if msgInfo.mainId == mainId and msgInfo.subId == subId and msgInfo.type == msgType then
            if msgInfo.value then
                return self._structs[msgInfo.value]   --获取结构体信息
            end            
        end
    end
    return nil
end

--协议解码,次函数不能外部调用 lua语言的索引值从1开始计数
--[[
常用数据类型
<typedef name="DWORD" len="4" />
<typedef name="UINT" len="4" />
<typedef name="WORD" len="2" />
<typedef name="TCHAR" len="1" />
<typedef name="BYTE" len="1" />
<typedef name="LLONG" len="8" />
]]
function MessageDeal:messageDecode(msgStruct, msgBytes)
	local index = 1
	local msgDetail = {}
	while true do
        local str = msgStruct[string.format("%d",index)]
        index = index + 1
	    if str then
	        local subStrs = lua_string_split(str, ' ')
            if subStrs[1] == "require" then   --'require ' + 'subtype' + 'type' + 'name'   
                if subStrs[3] == "OBJ" then
                    msgDetail[subStrs[4]] = self:messageDecode(self._structs[subStrs[2]], msgBytes)
                else   --常用数据类型
                    msgDetail[subStrs[4]] = self:getCommonData(subStrs[3], msgBytes)
                end
            else    --'repeated ' + 'subtype' + 'count' + 'name'
                local msgLen = subStrs[3]
                if subStrs[5] then
                    msgLen = msgDetail[subStrs[5]]
                end

                if self._typedefs[subStrs[2]] then  --是基础类型
                    if VERSION_DEBYG == true then
                        if VERSION_DEBYG == true then
                            print("parse subStr[2]"..subStrs[2].."      "..msgLen)
                        end
                    end

                    if subStrs[2] == "TCHAR" then   --字符串数组

                        local bytes = {}

                        for i = 1, subStrs[3] do
                            if msgBytes:getPos() <= msgBytes:getLen() then
                                table.insert(bytes,msgBytes:readByte())
                            end
                        end
                        
                        --进行转码
                        msgDetail[subStrs[4]] = gbk_utf8(bytes)
                        
                        if VERSION_DEBYG==true then
                            print("unicode change end name:%s--:%s",subStrs[4], msgDetail[subStrs[4]])
                        end
                    else --其他常用类型
                        local tmpArray = {}
                        msgDetail[subStrs[4].."Len"] = msgLen --设置长度
                        for i=1, msgLen do
                            table.insert(tmpArray,self:getCommonData(subStrs[2], msgBytes))
                        end

                        msgDetail[subStrs[4]] = tmpArray
                    end
                else  --不是基础类型，为结构体

                    if msgLen == 0 then
                        msgLen = ( msgBytes:getLen() - msgBytes:getPos()) / self:getMsgStructLen(self._structs[subStrs[2]])
                    end

                    local tmpArray = {}
                    for i=1, msgLen do
                        table.insert(tmpArray,self:messageDecode(self._structs[subStrs[2]], msgBytes))
                    end
                    msgDetail[subStrs[4]] = tmpArray
                end
	        end
	    else
	       break
	    end
	end
    -- dump(msgDetail)
    return msgDetail
end

function MessageDeal:getCommonData(type, msgBytes) --获取常用数据类型
    if type == "DWORD" then
        return msgBytes:readUInt()
    elseif type == "UINT" then
        return msgBytes:readUInt()
    elseif type == "INT" then
        return msgBytes:readInt()
    elseif type == "WORD" then
        return msgBytes:readUShort()
    elseif type == "TCHAR" then
        return msgBytes:readUShort()
    elseif type == "BYTE" then
        return msgBytes:readUByte()
    elseif type == "LLONG" then
        return msgBytes:readLongLong()
    elseif type == "FLOAT" then
        return msgBytes:readFloat()
    elseif type == "DOUBLE" then
        return msgBytes:readDouble()
    elseif type == "BOOL" then
        return msgBytes:readBool()
    elseif type == "LONG" then
        return msgBytes:readUInt()
    elseif type == "SCORE" then
        return msgBytes:readLongLong()
    end
    return nil
end

function MessageDeal:getMsgStructLen(msgStruct)
    local structLen = 0
    local index = 1;
    while msgStruct[string.format("%d",index)] do
        local itemInfo = msgStruct[string.format("%d",index)]
        local strs = lua_string_split(itemInfo, ' ')
        
        if strs[1] == "repeated" then
            if self._typedefs[strs[2]] then
                structLen = structLen + self._typedefs[strs[2]] * strs[3]   --常用类型数组
            else
                structLen = structLen + self:getMsgStructLen(self._structs[strs[2]])*strs[3]  --递归获取
        	end
        else
            if strs[2] == "null" then
                structLen = structLen + self._typedefs[strs[3]]   --常用类型数组
            else
                structLen = structLen + self:getMsgStructLen(self._structs[strs[2]])  --递归获取
            end            
        end
        
        index = index + 1
    end
       
    return structLen
end

--[[
协议编码，供外部调用
]]
function MessageDeal:encodeMessage(mainId, subId, msgStruct)
    if msgStruct then
        local tmpStruct = self:queryMessageStruct(mainId, subId, "C2S")   --获取结构体信息
        if tmpStruct then
            local byteArray = GameByteArray:new()
            byteArray:setEndian(GameByteArray.ENDIAN_LITTLE)
            self:messageEncode(msgStruct, tmpStruct, byteArray)
            return byteArray
        end
    end
	
    return nil
end

function MessageDeal:messageEncode(msgStruct, structInfo, byteArray)
    local index = 1

    while true do
        local str = structInfo[string.format("%d",index)]
        index = index + 1
        if str then
            local subStrs = lua_string_split(str, ' ')
            -- dump(subStrs)
            if subStrs[1] == "require" then   --'require ' + 'subtype' + 'type' + 'name'                
                if subStrs[3] == "OBJ" then
                    self:messageEncode(msgStruct[subStrs[4]], self._structs[subStrs[2]], byteArray)
                else   --常用数据类型
                    self:writeCommonData(subStrs[3], msgStruct[subStrs[4]], byteArray)
                end
            else    --'repeated ' + 'subtype' + 'count' + 'name'
                if self._typedefs[subStrs[2]] then  --是基础类型
                    if subStrs[2] == "TCHAR" then   --字符串数组
                        --TCHAR字符串，进行UTF8转码处理
    
                        local gbkStr, lenth = utf8_gbk(msgStruct[subStrs[4]])

                        local needLen = tonumber(subStrs[3])
                        for i=1 ,lenth do
                            if i <= needLen then
                                byteArray:writeUByte(string.byte(gbkStr, i))
                            end                            
                        end
                        
                        if lenth < needLen then
                            for i=1 , needLen-lenth do
                                byteArray:writeUByte('0')
                            end
                        end                                            
                    else --其他常用类型
                        for i=1, subStrs[3] do
                            self:writeCommonData(subStrs[2], msgStruct[subStrs[4]][i], byteArray)
                        end
                    end
                else  --不是基础类型，为结构体
                    for i=1, subStrs[3] do
                        self:messageEncode(msgStruct[subStrs[4]], self._structs[subStrs[2]], byteArray)
                    end
                end            
            end
        else
            break
        end
    end
end

function MessageDeal:writeCommonData(type, info, byteArray)
    if type == "DWORD" then
        byteArray:writeUInt(info)
    elseif type == "INT" then
        byteArray:writeInt(info)
    elseif type == "UINT" then
        byteArray:writeUInt(info)
    elseif type == "WORD" then
        byteArray:writeUShort(info)
    elseif type == "TCHAR" then
        byteArray:writeUShort(info)
    elseif type == "BYTE" then
        byteArray:writeUByte(info)
    elseif type == "LLONG" then
        byteArray:writeLongLong(info)
    elseif type == "FLOAT" then
        byteArray:writeFloat(info)
    elseif type == "DOUBLE" then
        byteArray:writeDouble(info)
    elseif type == "BOOL" then
        byteArray:writeBool(info)
    elseif type == "LONG" then
        byteArray:writeUInt(info)
    elseif type == "SCORE" then
        byteArray:writeLongLong(info)
    end
end


return MessageDeal