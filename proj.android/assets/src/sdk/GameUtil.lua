require "lfs"

appstoreGoodsConfs = {
    {["goodsIndex"]=1, ["goodsName"]="1元礼包", ["money"]=1, ["desc"]="1元购买1w金币", ["appGoodsId"]=9},
    {["goodsIndex"]=2, ["goodsName"]="6元礼包", ["money"]=6, ["desc"]="6元购买9w金币", ["appGoodsId"]=1},
    {["goodsIndex"]=3, ["goodsName"]="12元礼包", ["money"]=12, ["desc"]="12元购买19w金币", ["appGoodsId"]=2},
    {["goodsIndex"]=4, ["goodsName"]="30元礼包", ["money"]=30, ["desc"]="30元购买49w金币", ["appGoodsId"]=4},
    {["goodsIndex"]=5, ["goodsName"]="50元礼包", ["money"]=50, ["desc"]="50元购买83w金币", ["appGoodsId"]=5},
    {["goodsIndex"]=6, ["goodsName"]="128元礼包", ["money"]=128, ["desc"]="128元购买218w金币", ["appGoodsId"]=6},
    {["goodsIndex"]=7, ["goodsName"]="328元礼包", ["money"]=328, ["desc"]="328元购买575w金币", ["appGoodsId"]=11},
    {["goodsIndex"]=8, ["goodsName"]="618元礼包", ["money"]=618, ["desc"]="618元购买1100w金币", ["appGoodsId"]=12},
    {["goodsIndex"]=9, ["goodsName"]="首冲礼包", ["money"]=6, ["desc"]="6元获取18w金币+1张乐券", ["appGoodsId"]=13}
}
FIRST_PAY_INDEX = 9  --首充id


local function hex(s)
    s=string.gsub(s,"(.)",function (x) return string.format("%02X",string.byte(x)) end)
    return s
end

local function readFile(path)
    local file = io.open(path, "rb")
    if file then
        local content = file:read("*all")
        io.close(file)
        return content
    end
    return nil
end

local function removeFile(path)
    os.remove(path)
end

local function checkFile(fileName, cryptoCode)
    if VERSION_DEBYG == true then
        print("checkFile:", fileName)
        print("cryptoCode:", cryptoCode)
    end
    if not io.exists(fileName) then
        if VERSION_DEBYG == true then
            print("checkFile:"..fileName.." not exists")
        end
        return false
    end

    local data=readFile(fileName)
    if data==nil then
        if VERSION_DEBYG == true then
            print("checkFile:"..fileName.." filedata is nil")
        end
        return false
    end

    if cryptoCode==nil then
        return true
    end

    local ms = crypto.md5(hex(data))
    if VERSION_DEBYG == true then
        print("file cryptoCode:", ms)
    end
    if ms==cryptoCode then
        return true
    end

    return false
end

local function checkDirOK( path )
    if path == nil then
        return false
    end

    local ret = lfs.chdir(path)
    if ret==nil then
        return false
    else
        return true
    end
end

local function findLastStr(srcStr, subStr)
    if srcStr ~= nil and srcStr ~= "" then
        local startIndex, endIndex = string.find(srcStr,subStr)
        if startIndex == nil then
            return 0
        else
            return endIndex + findLastStr(string.sub(srcStr, endIndex+1, -1), subStr)
        end
    else
        return 0
    end
end

local function createDir(path)
    path = string.sub(path, 1, -2)
    local lastIndex = findLastStr(path, "/")
    local subPath = string.sub(path, 1, lastIndex)
    local dirName = string.sub(path, lastIndex, string.len(path))

    local pathinfo = io.pathinfo(subPath)
    if checkDirOK(pathinfo.dirname)==false then
        createDir(subPath)
    end

    lfs.mkdir(path)
end

function os.rmdir(path)
    if path == nil then
        print("os.rmdir is error the path is nil")
    	return
    end
    print("os.rmdir:", path)
    print(io.exists(path))
    if checkDirOK(path)==true then
        local function _rmdir(path)
            local iter, dir_obj = lfs.dir(path)
            while true do
                local dir = iter(dir_obj)
                if dir == nil then
                    break
                end

                if dir ~= "." and dir ~= ".." then
                    local curDir = path..dir
                    local mode = lfs.attributes(curDir, "mode") 
                    if mode == "directory" then
                        _rmdir(curDir.."/")
                    elseif mode == "file" then
                        print('delete file:'..curDir)
                        os.remove(curDir)
                    end
                end
            end
            lfs.rmdir(path)
            return succ
        end
        _rmdir(path)
    end
    return true
end

function io.writefileCheckDir(path, data)
    local pathinfo = io.pathinfo(path)
    if checkDirOK(pathinfo.dirname) then
        io.writefile(path, data)
        return true
    else
        if device.platform == "windows" then
            local newStr = string.gsub(pathinfo.dirname, "/", "\\")
            os.execute("mkdir "..newStr)
        else
            createDir(pathinfo.dirname)
        end

        if checkDirOK(pathinfo.dirname) then
            io.writefile(path, data)
            return true
        else
            return false
        end
    end
end


local targetPlatform = cc.Application:getInstance():getTargetPlatform()
local ANDROID_CLASSNAME =   "org.cocos2dx.lua.MobilePlate"
-------------------------------------------------------
-- 参数:待分割的字符串,分割字符
-- 返回:子串表.(含有空串)
function lua_string_split(str, split_char)
    local sub_str_tab = {};

    while (true) do
        local pos = string.find(str, split_char);
        if (not pos) then
            local size_t = table.getn(sub_str_tab)
            table.insert(sub_str_tab,size_t+1,str);
            break;
        end

        local sub_str = string.sub(str, 1, pos - 1);
        local size_t = table.getn(sub_str_tab)
        table.insert(sub_str_tab,size_t+1,sub_str);
        local t = string.len(str);
        str = string.sub(str, pos + 1, t);
    end
    return sub_str_tab;
end

--字符串每隔perlen个字符插入
-- function insertSubPerLen(str, perLen, subStr)
--     local retStr = ""
--     local len = string.len(str)
--     local tmpLen = 0
--     local insertCount = 1
    
--     local i = 1
--     while i<=len do
--         local curByte = string.byte(str, i)
--         local byteCount = 0;
--         if curByte>0 and curByte<=127 then
--             byteCount = 1
--         elseif curByte>=192 and curByte<223 then
--             byteCount = 2
--         elseif curByte>=224 and curByte<239 then
--             byteCount = 3
--         elseif curByte>=240 and curByte<=247 then
--             byteCount = 4
--         end

--         local char = string.sub(str, i, i+byteCount-1)
--         retStr = retStr..char

--         i = i + byteCount

--         if byteCount == 1 then
--             tmpLen = tmpLen + 1
--         else
--             tmpLen = tmpLen + 2
--         end

--         if tmpLen >= perLen and i<=len then
--             tmpLen = 0
--             retStr = retStr..subStr
--             insertCount = insertCount + 1
--         end
--     end
--     return retStr, insertCount
-- end

function insertSubPerLen(str, perLen, subStr)
    local retStr = ""
    local len = string.len(str)
    local tmpLen = 0
    local insertCount = 1
    local char
    local curByte
    local byteCount
    
    -- while i <= len do
    for i = 1, len do
        curByte = string.byte(str, i)
        byteCount = 0;
        if curByte > 0 and curByte <= 127 then -- ASCII
            byteCount = 1
        elseif curByte>=192 and curByte<=223 then
            byteCount = 2
        elseif curByte>=224 and curByte<=239 then
            byteCount = 3
        elseif curByte>=240 and curByte<=247 then
            byteCount = 4
        end

        if byteCount > 0 then
            char = string.sub(str, i, i + byteCount - 1)
            i = i + byteCount - 1
        end
        if byteCount == 1 then
            tmpLen = tmpLen + 1
            retStr = retStr..char
        elseif byteCount > 1 then
            tmpLen = tmpLen + 2
            retStr = retStr..char
        end

        if tmpLen >= perLen and i <= len then
            tmpLen = 0
            retStr = retStr .. subStr
            insertCount = insertCount + 1
        end
    end
    return retStr, insertCount
end

function strLegalRegist(str)
    local index = 1
    
    while index <= #str do
        local ch = string.sub(str,index,index)
    	if not((ch>='0' and ch<='9') or (ch>='a' and ch<='z') or (ch>='A' and ch<='Z') or ch=='_') then
    		return false
    	end
    	index = index + 1
    end
    
    return true
end

function getMobileImei()
    if targetPlatform == cc.PLATFORM_OS_ANDROID then
        local ok,ret = luaj.callStaticMethod(ANDROID_CLASSNAME, "getEmei", nil, "()Ljava/lang/String;")
        return ret
    end

    if targetPlatform == cc.PLATFORM_OS_IPHONE or targetPlatform == cc.PLATFORM_OS_IPAD then
        local ok, ret = luaoc.callStaticMethod("PlatBridge", "getImei")
        return ret;
    end

    return "imei test"
end

function getGuestId()
    if targetPlatform == cc.PLATFORM_OS_ANDROID then
        local ok,ret = luaj.callStaticMethod(ANDROID_CLASSNAME, "getGuestId", nil, "()Ljava/lang/String;")
        return ret
    end

    if targetPlatform == cc.PLATFORM_OS_IPHONE or targetPlatform == cc.PLATFORM_OS_IPAD then
        local ok, ret = luaoc.callStaticMethod("PlatBridge", "getGuestId")
        return ret;
    end

    return "GuestTest001"
end

function getMobileType()
    if targetPlatform == cc.PLATFORM_OS_ANDROID then
        local ok,ret = luaj.callStaticMethod(ANDROID_CLASSNAME, "getMobileType", nil, "()Ljava/lang/String;")
        return ret
    end

    if targetPlatform == cc.PLATFORM_OS_IPHONE or targetPlatform == cc.PLATFORM_OS_IPAD then
        local ok, ret = luaoc.callStaticMethod("PlatBridge", "getMobileType")
        return ret;
    end

    return "Win32 Mobile Test"
end

function getDiskId()
    if targetPlatform == cc.PLATFORM_OS_ANDROID then
        local ok,ret = luaj.callStaticMethod(ANDROID_CLASSNAME, "getDiskId", nil, "()Ljava/lang/String;")
        return ret
    end

    if targetPlatform == cc.PLATFORM_OS_IPHONE or targetPlatform == cc.PLATFORM_OS_IPAD then
        local ok, ret = luaoc.callStaticMethod("PlatBridge", "getDiskId")
        return ret;
    end

    return "getDiskId test"
end

function getMacAddr()
    if targetPlatform == cc.PLATFORM_OS_ANDROID then
        local ok,ret = luaj.callStaticMethod(ANDROID_CLASSNAME, "getMacAddr", nil, "()Ljava/lang/String;")
        return ret
    end

    if targetPlatform == cc.PLATFORM_OS_IPHONE or targetPlatform == cc.PLATFORM_OS_IPAD then
        local ok, ret = luaoc.callStaticMethod("PlatBridge", "getMacAddr")
        return ret;
    end

    return "00-00-00-00"
end

function getScreenRes()
    if targetPlatform == cc.PLATFORM_OS_ANDROID then
        local ok,ret = luaj.callStaticMethod(ANDROID_CLASSNAME, "getScreenRes", nil, "()Ljava/lang/String;")
        return ret
    end

    if targetPlatform == cc.PLATFORM_OS_IPHONE or targetPlatform == cc.PLATFORM_OS_IPAD then
        local ok, ret = luaoc.callStaticMethod("PlatBridge", "getScreenRes")
        return ret;
    end
    return "1280x720"
end

function getOsVersion()
    if targetPlatform == cc.PLATFORM_OS_ANDROID then
        local ok,ret = luaj.callStaticMethod(ANDROID_CLASSNAME, "getOsVersion", nil, "()Ljava/lang/String;")
        return ret
    end
    if targetPlatform == cc.PLATFORM_OS_IPHONE or targetPlatform == cc.PLATFORM_OS_IPAD then
        local ok, ret = luaoc.callStaticMethod("PlatBridge", "getOsVersion")
        return ret;
    end

    return "win7"
end

function getChannelName()
    if targetPlatform == cc.PLATFORM_OS_ANDROID then
        local ok,ret = luaj.callStaticMethod(ANDROID_CLASSNAME, "getChannelName", nil, "()Ljava/lang/String;")
        return ret
    end

    if targetPlatform == cc.PLATFORM_OS_IPHONE or targetPlatform == cc.PLATFORM_OS_IPAD then
        local ok, ret = luaoc.callStaticMethod("PlatBridge", "getChannelName")
        return ret;
    end
    return "LL_android"--"com.win32.channel"
end

function getVersionName()
    if targetPlatform == cc.PLATFORM_OS_ANDROID then
        local ok,ret = luaj.callStaticMethod(ANDROID_CLASSNAME, "getVersionName", nil, "()Ljava/lang/String;")
        return ret
    end

    if targetPlatform == cc.PLATFORM_OS_IPHONE or targetPlatform == cc.PLATFORM_OS_IPAD then
        local ok, ret = luaoc.callStaticMethod("PlatBridge", "getVersionName")
        return ret;
    end
    return "2.0.5"--"com.win32.version"
end

function exitGame()
    if targetPlatform == cc.PLATFORM_OS_ANDROID then
        luaj.callStaticMethod(ANDROID_CLASSNAME, "exitGame", nil, "()V")
    else
        print("退出游戏")
    end
end

function unicode2Utf8(byteArray, unicodeLen)
    local codeChange = custom.CodeChange:new()
    codeChange:setUnicodeLen(unicodeLen)
    for i=1, unicodeLen-1 do
        codeChange:setUnicodeByIndex(i-1, byteArray:readUShort())
    end

    return custom.PlatUtil:UnicodeToUtf8(codeChange)
end

local xhr = nil
local OrderID = nil
function payResult(data)
    local tab = json.decode(data)
    OrderID = data
    -- app.eventDispather:dispatherEvent(eventPaySuccessCallBack)
    -- app.hallLogic:sendUpdateUserCoin()
    RunScriptInUIThread('display.getRunningScene():messageNotice()')
end

function checkOrderID()
    -- body
    app.eventDispather:dispatherEvent(eventPaySuccessCallBack, OrderID)
    OrderID = nil
end

function paytypeInfo()
    if targetPlatform == cc.PLATFORM_OS_IPHONE or targetPlatform == cc.PLATFORM_OS_IPAD then
        local appargs = {backFunc = payResult}
        if appargs then
            luaoc.callStaticMethod("PlatBridge", "payCallBack", appargs)
        end
    end

    if targetPlatform == cc.PLATFORM_OS_ANDROID then
        if targetPlatform == cc.PLATFORM_OS_ANDROID then
            local className = "org/cocos2dx/lua/MobilePlate"
            local args = {payResult}
            local sigs = "(I)V"
            luaj.callStaticMethod(className, "paytypeInfo", args, sigs);
        end
    end
end


function pay(goodsIndex)
    local appargs = nil
    local androidargs = nil
    local function payWeixinOrAli(goodsIndex)
        for index, info in ipairs(appstoreGoodsConfs) do
            if info.goodsIndex == goodsIndex then
                local t_channel = getChannelName()
                local t_version = getVersionName()
                local t_sign = crypto.md5("GetPayConfig"..t_channel..t_version.."tjyjerge!@#sddf$^$h234^^DsSEG")
                local param = "action=GetPayConfig&channel="..t_channel.."&version="..t_version..
                            "&sign="..t_sign.."&uid="..app.hallLogic._loginSuccessInfo.dwUserID
                local requestUrl = GETCONFIG_UEL.."?"..param

                local t_sign1 = crypto.md5("GamePayDetailConfig"..t_channel..t_version.."tjyjerge!@#sddf$^$h234^^DsSEG")
                local param1 = "action=GamePayDetailConfig&channel="..t_channel.."&version="..t_version..
                            "&sign="..t_sign1
                local url = GETCONFIG_UEL.."?"..param1

                appargs = {
                    goodsId = info.appGoodsId,
                    money = info.money,
                    goodsName = info.goodsName,
                    goodsDesc = info.desc,
                    serverId = 0,
                    userId = app.hallLogic._loginSuccessInfo.dwUserID,
                    backFunc = payResult,
                    paySelectUrl = requestUrl,--"http://safe.jiujiu.com:84/Safe/getConfig.aspx?action=GetPayConfig&uid="..app.hallLogic._loginSuccessInfo.dwUserID.."&channel=ios",
                    payDetailUrl = url
                    
                }
                
                androidargs = {
                    goodsId = goodsIndex,
                    money = info.money,
                    goodsName = info.goodsName,
                    goodsDesc = info.desc,
                    serverId = 0,
                    userId = app.hallLogic._loginSuccessInfo.dwUserID
                }
                break
            end
        end
        
        if targetPlatform == cc.PLATFORM_OS_IPHONE or targetPlatform == cc.PLATFORM_OS_IPAD then
            if appargs then
                luaoc.callStaticMethod("PlatBridge", "pay", appargs)
            end
        end
             
        if targetPlatform == cc.PLATFORM_OS_ANDROID then
            if androidargs then
                local payStr = json.encode(androidargs)
                local args = {payStr, payResult}
                luaj.callStaticMethod(ANDROID_CLASSNAME, "pay", args, "(Ljava/lang/String;I)V")
            end
        end
    end

    local function SelectPayTpye(goodsIndex)
        xhr = cc.XMLHttpRequest:new() -- 新建一个XMLHttpRequest对象  
        xhr.responseType = cc.XMLHTTPREQUEST_RESPONSE_JSON -- json数据类型
        -- xhr:open("POST", "http://gameapi.jiujiu.com:82/api.aspx")-- POST方式
        xhr:open("POST", app.GETPAYCONFIG_UEL)-- POST方式

        local t_userId = app.hallLogic._loginSuccessInfo["dwUserID"]
        local t_channel = getChannelName()
        local t_money = nil
        for index, info in ipairs(appstoreGoodsConfs) do
            if info.goodsIndex == goodsIndex then
                t_money = info.money
            end
        end
        local t_serverID = 0
        --获取版本号
        local t_version = getVersionName()
        local t_sign = crypto.md5("SelectPayPlatform"..
            t_channel..t_version..t_userId.."tjyjerge!@#sddf$^$h234^^DsSEG")    
        local param = "action=SelectPayPlatform&".."userID="..t_userId..
            "&version="..t_version.."&channel="..t_channel.."&amount="..
            t_money.."&serverID="..t_serverID.."&sign="..t_sign
        xhr:send(param)

        print( app.GETPAYCONFIG_UEL.."?"..param )
        local function onReadyStateChange()  
            local response = xhr.response -- 获得响应数据  
            local output = json.decode(response, 1) -- 解析json数据 
            dump(output)
            if output then
                if output.PayPlatform == "1" then 
                    payWeixinOrAli(goodsIndex)
                else
                    --掉用java创建窗口，弹出网页支付
                    local sign = crypto.md5(output.OrderID.."tjyjerge!@#sddf$^$h234^^DsSEG")
                    local localurl = output.PayRequestURL.."?OrderID="..output.OrderID.."&Sign="..sign
                    --注册回调函数
                    paytypeInfo()
                    showUrl(localurl)
                end
                xhr = nil
            end
        end
        -- 注册脚本方法回调 
        xhr:registerScriptHandler(onReadyStateChange)
    end

    --调用方式选择
    SelectPayTpye(goodsIndex)

end


function checkPayFailed()
    if targetPlatform == cc.PLATFORM_OS_IPHONE or targetPlatform == cc.PLATFORM_OS_IPAD then
        luaoc.callStaticMethod("PlatBridge", "checkPayFailed")
    end
end

function showUrl(url)
    if targetPlatform == cc.PLATFORM_OS_ANDROID then
        local args = {url}
        luaj.callStaticMethod(ANDROID_CLASSNAME, "showWebView", args, "(Ljava/lang/String;)V")
    end

    if targetPlatform == cc.PLATFORM_OS_IPHONE or targetPlatform == cc.PLATFORM_OS_IPAD then
        appargs = {
            strUrl = url,
        }
        luaoc.callStaticMethod("PlatBridge", "showWebView", appargs)
    end
end

function openBrowser(url)

    if targetPlatform == cc.PLATFORM_OS_ANDROID then
        local args = {url}
        luaj.callStaticMethod(ANDROID_CLASSNAME, "openBrowser", args, "(Ljava/lang/String;)V")
    end

    if targetPlatform == cc.PLATFORM_OS_IPHONE or targetPlatform == cc.PLATFORM_OS_IPAD then
        appargs = {
            strUrl = url,
        }
        luaoc.callStaticMethod("PlatBridge", "openBrowser", appargs)
    end
end

function setGamePayDetailUrl(url)
    local t_channel = getChannelName()
    local t_version = getVersionName()
    local t_sign = crypto.md5("GamePayDetailConfig"..t_channel..t_version.."tjyjerge!@#sddf$^$h234^^DsSEG")
    local param = "action=GamePayDetailConfig&channel="..t_channel.."&version="..t_version..
                "&sign="..t_sign
    local url = GETCONFIG_UEL.."?"..param

    if targetPlatform == cc.PLATFORM_OS_ANDROID then
        local args = {url}
        luaj.callStaticMethod(ANDROID_CLASSNAME, "setGamePayDetailUrl", args, "(Ljava/lang/String;)V")
    end

    if targetPlatform == cc.PLATFORM_OS_IPHONE or targetPlatform == cc.PLATFORM_OS_IPAD then
        appargs = {
            strUrl = url,
        }
        luaoc.callStaticMethod("PlatBridge", "setGamePayDetailUrl", appargs)
    end
end

function setUpdateUrl(url)
    local t_channel = getChannelName()
    local t_version = getVersionName()
    local t_sign = crypto.md5("GameVersionUpdate"..t_channel..t_version.."tjyjerge!@#sddf$^$h234^^DsSEG")
    local param = "action=GameVersionUpdate&channel="..t_channel.."&version="..t_version..
                "&sign="..t_sign
    local url = GETCONFIG_UEL.."?"..param

    if targetPlatform == cc.PLATFORM_OS_ANDROID then
        local args = {url}
        luaj.callStaticMethod(ANDROID_CLASSNAME, "setUpdateUrl", args, "(Ljava/lang/String;)V")
    end

    if targetPlatform == cc.PLATFORM_OS_IPHONE or targetPlatform == cc.PLATFORM_OS_IPAD then
        appargs = {
            strUrl = url,
        }
        luaoc.callStaticMethod("PlatBridge", "setUpdateUrl", appargs)
    end
end

function setGamePaySelectUrl(url)
    -- local t_channel = getChannelName()
    -- local t_version = getVersionName()
    -- local t_sign = crypto.md5("GetPayConfig"..t_channel..t_version.."tjyjerge!@#sddf$^$h234^^DsSEG")
    -- local param = "action=GetPayConfig&channel="..t_channel.."&version="..t_version..
    --             "&sign="..t_sign
    -- local url = GETCONFIG_UEL.."?"..param

    -- if targetPlatform == cc.PLATFORM_OS_ANDROID then
    --     local args = {url}
    --     luaj.callStaticMethod(ANDROID_CLASSNAME, "setGamePaySelectUrl", args, "(Ljava/lang/String;)V")
    -- end

    -- if targetPlatform == cc.PLATFORM_OS_IPHONE or targetPlatform == cc.PLATFORM_OS_IPAD then
    --     appargs = {
    --         strUrl = url,
    --     }
    --     luaoc.callStaticMethod("PlatBridge", "setGamePaySelectUrl", appargs)
    -- end
end

function callQQChat(qqNumber)
    if targetPlatform == cc.PLATFORM_OS_ANDROID then
        local args = {qqNumber}
        luaj.callStaticMethod(ANDROID_CLASSNAME, "callQQChat", args, "(Ljava/lang/String;)V")
    end

    if targetPlatform == cc.PLATFORM_OS_IPHONE or targetPlatform == cc.PLATFORM_OS_IPAD then
        appargs = {
            qqNum = qqNumber,
        }
        luaoc.callStaticMethod("PlatBridge", "callQQChat", appargs)
    end
end

function isPlatLogin()
    local ret = false
    local bPlatLogin = false
    if targetPlatform == cc.PLATFORM_OS_ANDROID then
        ret, bPlatLogin = luaj.callStaticMethod(ANDROID_CLASSNAME, "isPlatLogin", nil, "(Ljava/lang/String;)Z")
    end

    if targetPlatform == cc.PLATFORM_OS_IPHONE or targetPlatform == cc.PLATFORM_OS_IPAD then
        ret, bPlatLogin = luaoc.callStaticMethod("PlatBridge", "isPlatLogin", nil)
    end

    if ret and bPlatLogin == true  then
        return true
    end
    return false
end

function playLoginSuccess(loginInfo)
    
end

function platLogin()
    appargs = {
                backFunc = payResult                
            }
    if targetPlatform == cc.PLATFORM_OS_ANDROID then
        luaj.callStaticMethod(ANDROID_CLASSNAME, "platLogin", nil, "(V)V")
    end

    if targetPlatform == cc.PLATFORM_OS_IPHONE or targetPlatform == cc.PLATFORM_OS_IPAD then
        luaoc.callStaticMethod("PlatBridge", "platLogin", nil)
    end
end

function platLoginout()
    if targetPlatform == cc.PLATFORM_OS_ANDROID then
        luaj.callStaticMethod(ANDROID_CLASSNAME, "platLoginout", nil, "(V)V")
    end

    if targetPlatform == cc.PLATFORM_OS_IPHONE or targetPlatform == cc.PLATFORM_OS_IPAD then
        luaoc.callStaticMethod("PlatBridge", "platLoginout", nil)
    end
end

--复制字符串到剪切板
function copyStrToShearPlate(ui, str)
	if targetPlatform == cc.PLATFORM_OS_ANDROID then
		local args = {str};
        luaj.callStaticMethod(ANDROID_CLASSNAME, "copyStrToShearPlate", args, "(Ljava/lang/String;)V");
		MyToast.new( ui, "复制成功");
	elseif targetPlatform == cc.PLATFORM_OS_IPHONE or targetPlatform == cc.PLATFORM_OS_IPAD then
		appargs = {
            str = str,
        }
        luaoc.callStaticMethod("PlatBridge", "copyStrToShearPlate", appargs);
		MyToast.new( ui, "复制成功");
	else
		MyToast.new( ui, "模拟器不允许复制");
	end
end