
GMusicSound = class("GMusicSound")

function GMusicSound:ctor()
	self:reset()
end

function GMusicSound:reset()
	self._musics = {}
    self._sounds = {}
end

function GMusicSound:addMusic(key, musicFile)
	self._musics[key] = musicFile
end

function GMusicSound:addSound(key, soundFile)
	self._sounds[key] = soundFile
end

function GMusicSound:playMusic(key, bLoop)
	if cc.UserDefault:getInstance():getFloatForKey("sound")==0 then
   	    return
    end
    if not self._musics[key] then
    	return
    end
    self:stopMusic()
    return cc.SimpleAudioEngine:getInstance():playMusic(self._musics[key],bLoop)
end

function GMusicSound:playSound(key)
    if cc.UserDefault:getInstance():getFloatForKey("soundEffect")==0 then
        return
    end
    if not self._sounds[key] then
    	return
    end
    return cc.SimpleAudioEngine:getInstance():playEffect(self._sounds[key],false)
end

function GMusicSound:stopMusic()
	cc.SimpleAudioEngine:getInstance():stopMusic()
end

function GMusicSound:pauseMusic()
	cc.SimpleAudioEngine:getInstance():pauseMusic()
end

function GMusicSound:resumeMusic()
	cc.SimpleAudioEngine:getInstance():resumeMusic()
end

function GMusicSound:setBackgroundMusicVolume(a_value)
    cc.SimpleAudioEngine:getInstance():setMusicVolume(a_value)
end

function GMusicSound:setEffectsVolume(a_value)
    cc.SimpleAudioEngine:getInstance():setEffectsVolume(a_value)
end

function GMusicSound:stopAllSound()
    cc.SimpleAudioEngine:getInstance():stopAllEffects()
end

function GMusicSound:stopSound( a_value )
    cc.SimpleAudioEngine:getInstance():stopEffect(a_value)
end

return GMusicSound