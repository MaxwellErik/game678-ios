
--c++调lua

---------------------------

if DEFINED_CTLUA then return end
DEFINED_CTLUA = true

--@param mainCmdId: subCmdId: msg:
--@return
function onMessage(msgData) --//协议下行
    if VERSION_DEBYG == true then
        print("onMessage")
    end   
    local msg = tolua.cast(msgData, "custom.NetMessage")
    local clientId = msg:getClientId()
    if clientId == CLIENT_HALL then
        app.hallLogic:getLoginServer():onGameMessage(msg)
    elseif clientId == CLIENT_GAME then
        app.table:getGameServer():onGameMessage(msg)
    end
    
end

---------------------------
--@param clientId: 
--@return
function onNetDisConnected(clientId) -- 服务器断开连接
	print("netDisconnected:", clientId)
    if clientId == CLIENT_HALL then
        app.hallLogic:getLoginServer():onNetClosed()
    elseif clientId == CLIENT_GAME then
        app.table:getGameServer():onNetClosed()
    end
end


---------------------------
--@param clientId: 
--@return
function onNetConnectTimeout(clientId)--连接超时
	print("onNetConnectTimeout:", clientId)
	local bConnect = false;
    if clientId == CLIENT_HALL then
        app.hallLogic:getLoginServer():onNetConnected(bConnect)
    elseif clientId == CLIENT_GAME then
        app.table:getGameServer():onNetConnected(bConnect)
    end
end

---------------------------
--@param clientId: bConnect: 
--@return
function onNetConnected(clientId, bConnect)
    print("connect to server -------------------->>>>>>>>>>>>>>>>>>>>>>>>>>>>")
    if VERSION_DEBYG == true then
        print("connected server:", clientId, "result", bConnect)
    end
    if clientId == CLIENT_HALL then
        app.hallLogic:getLoginServer():onNetConnected(bConnect)
    elseif clientId == CLIENT_GAME then
        app.table:getGameServer():onNetConnected(bConnect)
    end
end

---------------------------
--@param clientId: 
--@return
function onNetClosed(clientId)
    print("netClosed:", clientId)
    if clientId == CLIENT_HALL then
        app.hallLogic:getLoginServer():onNetClosed()
    elseif clientId == CLIENT_GAME then
        app.table:getGameServer():onNetClosed()
        --maxwell
        custom.PlatUtil:closeServer(CLIENT_GAME)
    end
end