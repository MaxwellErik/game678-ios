function CocosAnimation()	
	local ccsNode = {};
	
	function ccsNode.clear()
		if ccsNode.armature then
			ccsNode.armature:release();
			ccsNode.armature:removeFromParent(true);
		end
		ccsNode.armature = nil;
		ccsNode.playIndex = 0;
		ccsNode.playName = "";
		ccsNode.name = "";
		ccsNode.key = "";
	end
	
	function ccsNode.setAnchorPoint(ap)
		ccsNode.armature:setAnchorPoint(ap);
	end
	
	
	function ccsNode.setKey(_key)
		ccsNode.key = _key;
		ccsNode.armature.key = _key;
	end
	
	function ccsNode.getKey()
		return ccsNode.key;
	end
	
	--设置用户数据
	function ccsNode.setUserData(da)
		ccsNode.armature.data = da;
	end
	
	function ccsNode.removeFromParent()
		ccsNode.armature:removeFromParent(true);
	end
	
	--get the size of this animation
	function ccsNode.getContentSize()
		return ccsNode.armature:getContentSize();
	end
	
	
	function ccsNode.getPlayIndex()
		return ccsNode.playIndex;
	end
	
	function ccsNode.getPlayName()
		return ccsNode.playName;
	end
	
	function ccsNode.setPosition(x,y)
		if not ccsNode.armature then
			return;
		end
		ccsNode.armature:setPosition(x,y);
	end
	
	function ccsNode.getPosition(ty)
		local armature = ccsNode.armature;
		if ty == 1 or ty == "x" or ty == "X" then
			return armature:getPositionX();
		end
		return armature:getPositionY();
	end
	
	--设置镜像或者缩放
	function ccsNode.setScaleX(fx)
		ccsNode.armature:setScaleX(fx);
	end
	
	--设置镜像或者缩放
	function ccsNode.setScaleY(fy)
		ccsNode.armature:setScaleY(fy);
	end
	
	--执行一个动作
	function ccsNode.runAction(ac)
		ccsNode.armature:runAction(ac);
	end
	
	function ccsNode.playWithIndex(index)
		if not ccsNode.armature then
			return;
		end
		ccsNode.armature:getAnimation():playWithIndex(index);
		ccsNode.playIndex = index;
	end
	
	--根据一个名字播放一个动作
	function ccsNode.playWithName(name)
		if not ccsNode.armature then
			return;
		end
		ccsNode.armature:getAnimation():play(name);
		ccsNode.playName = name;
	end
	
	--给boneName增加一个Sprite的dispaly(请自己生成好)
	function ccsNode.addDisplaySprite(boneName,ccSprite,index)
		local bone = ccsNode.armature:getBone(boneName);
		if not bone then
			Log(ccsNode.name..":在增加图片的时候没有找到:"..boneName.."的bone");
			return;
		end

		bone:addDisplay(ccSprite,index);
		bone:setIgnoreMovementBoneData(true)
	end
	
	function ccsNode.setChildArmature(boneName,armature)
		local bone = ccsNode.armature:getBone(boneName);
		if not bone then
			Log(ccsNode.name..":在设置ChildArmature的时候没有找到:"..boneName.."的bone");
			return;
		end
		bone:setChildArmature(armature);
		bone:setIgnoreMovementBoneData(true)
	end
	
	--切换bone上的display到指定的索引
	function ccsNode.changeDisplayByIndex(boneName,index)
		local bone = ccsNode.armature:getBone(boneName);
		if not bone then
			Log(ccsNode.name..":在换装的时候没有找到:"..boneName.."的bone");
			return;
		end
		bone:changeDisplayWithIndex(index,true);
	end
	--给骨骼上增加一个粒子特效
	function ccsNode.addParticalToBone(boneName,particalPath)
		local bone = ccsNode.armature:getBone(boneName);
		if not bone then
			Log(ccsNode.name..":在换粒子特效的时候没有找到:"..boneName.."的bone");
			return;
		end
		local pBone = ccs.Bone:create(particalPath);
		local p1 = cc.ParticleSystemQuad:create(particalPath);
		pBone:addDisplay(p1,0);
		pBone:changeDisplayWithIndex(0, true);
		pBone:setIgnoreMovementBoneData(true);
		pBone:setLocalZOrder(100);
		ccsNode.armature:addBone(bone,boneName);
	end
	
	--在动画上增加一个节点
	function ccsNode.addChild(node,zorder,tag)
		local armature = ccsNode.armature;
		if zorder then
			armature:addChild(node,zorder);
			return;
		end
		if tag then
			armature:addChild(node,zorder,tag);
			return;
		end
		armature:addChild(node);
	end
	
	--获取Node对象
	function ccsNode.getArmature()
		return ccsNode.armature;
	end
	
	function ccsNode.setScale(scalex,scaley)
		ccsNode.armature:setScaleX(scalex);
		ccsNode.armature:setScaleY(scaley);
	end
	
	function ccsNode.setRotation(angle)
		ccsNode.armature:setRotation(angle);
	end
	
	function ccsNode.init(path)
		ccsNode.clear();
		local armature = ccs.Armature:create(path);
		armature:retain();
		ccsNode.armature = armature;
		ccsNode.name = path;
	end
	
	--增加一个移动事件的监听，例如开始播放，结束播放等
	--armatureBack,movementType,movementID
	function ccsNode.addMovementEventListener(callBack)
		if not ccsNode.armature then
			return;
		end
		ccsNode.armature:getAnimation():setMovementEventCallFunc(callBack);
	end
	
	--增加一个帧事件的回调
	--bone,frameEventName,originFrameIndex,currentFrameIndex
	function ccsNode.addFrameEventListener(callBack)
		if not ccsNode.armature then
			return;
		end
		ccsNode.armature:getAnimation():setFrameEventCallFunc(callBack);
	end
	
	--给这个动画增加一个刚体
	function ccsNode.addPhycisBody(_body)
		local ar = ccsNode.getArmature();
		ar:setPhysicsBody(_body);
	end
	
	return ccsNode;
end