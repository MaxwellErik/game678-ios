function CocosFactory()
	local ccsFactory = {};
	
	function ccsFactory.init()
		ccsFactory.cache = {};
	end
	
	function ccsFactory.clear()
		local manager = ccs.ArmatureDataManager:getInstance();
		for key,path in pairs(ccsFactory.cache) do
			if path then
				manager:removeArmatureFileInfo(path);
			end
		end
		ccsFactory.cache = {};
	end
	--移除一个缓存的动画数据
	function ccsFactory.remove(path)
		local manager = ccs.ArmatureDataManager:getInstance();
		if ccsFactory.cache[path] then
			manager:removeArmatureFileInfo(path);
			ccsFactory.cache[path] = nil;
		end
	end
	
	--加载一个动画的资源
	function ccsFactory.load(path)
		if ccsFactory.cache[path] then
			return;
		end
		local manager = ccs.ArmatureDataManager:getInstance();
		manager:addArmatureFileInfo(path);
	end
	--生成一个特效
	function ccsFactory.createCCSAniamtion(path)
		ccsFactory.load(path.."/"..path..".ExportJson");
		local ccsNode = CocosAnimation();
		ccsNode.init(path);
		return ccsNode;
	end
	
	return ccsFactory;
end

--清理
function ClearCCSFactory()
	local ccsFactory = GetCCSFactory();
	ccsFactory.clear();
	Global["ccsFactory"] = nil;
end


function GetCCSFactory()
	local ccsFactory = Global["ccsFactory"];
	if not ccsFactory then
		ccsFactory = CocosFactory();
		ccsFactory.init();
		Global["ccsFactory"] = ccsFactory;
	end
	return ccsFactory;
end