local SocketTCP = require("Engine.socket.SocketTCP");
--新建一个socket
function Socket(key)	
	local sk = {};
	sk.key = key;
	--清理这个sockect
	function sk.clear()
		sk.ip = "";
		sk.port = 0;
		sk.isRetryConnect = false;
		sk.socket = nil;
	end
	
	--初始化一个socket的连接
	--ip：需要连接的服务器的地址
	--port：需要连接的服务器的端口号
	--isRetryConnect：断线之后是否重试重新连接
	function sk.init(ip,port,isRetryConnect)
		
		sk.clear();
		sk.ip = ip;
		sk.port = port;
		sk.isRetryConnect = isRetryConnect;
		sk.socket = SocketTCP.new(ip,port,isRetryConnect);
	end
	
	--socket状态的返回
	function sk.onStatus(event)
		print("Socket named :"..sk.key..",event named :"..event.name);
	end
	
	--在设置之后开始连接服务器
	function sk.connect()
		if not sk.socket then
			return;
		end
		
		local socket = sk.socket;
		socket:addEventListener(SocketTCP.EVENT_CONNECTED,sk.onStatus);
		socket:addEventListener(SocketTCP.EVENT_DATA,sk.onStatus);
		socket:addEventListener(SocketTCP.EVENT_CLOSE,sk.onStatus);
		socket:addEventListener(SocketTCP.EVENT_CLOSED,sk.onStatus);
		socket:addEventListener(SocketTCP.EVENT_CONNECT_FAILURE,sk.onStatus);
		socket:connect();
	end

	--注册UI和游戏方面的回调函数
	function sk.registUIMessage(ty,ui)
		
		
	end
	
	--注册全局的消息回调函数
	function sk.registCommonMessage(ty,callBack)
		
	end
	
	
	function sk.send(data)
		if not sk.socket then
			return;
		end
		sk.socket:send(data);
	end
	
	--关闭socket连接
	function sk.close()
		if sk.socket then
			sk.socket:close();
		end
	end
	
	--主动断开连接
	function sk.disconnect()
		if sk.socket then
			sk.socket:disconnect();
		end
	end
	
	return sk;
end

--开始连接服务器
function StartConnect(ip,port,retry,key)
	local sk = Global[key];
	if sk then
		sk.disconnect();
		sk.close();
		sk.clear();
		sk = nil;
		Global[key] = nil;
	end
	sk = Socket(key);
	sk.init(ip,port,retry);
	sk.connect();
	Global[key] = sk;
end