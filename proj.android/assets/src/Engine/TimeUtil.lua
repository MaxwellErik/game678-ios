--一些时间的处理函数
--获取今天早上0点的时间
function GetTodayZeroTime()
	local tm = os.date("*t");
	local year = tm.year;
	local month = tm.month;
	local day = tm.day;
	tm.hour = 0;
	tm.min = 0;
	tm.sec = 0;
	local secZero = os.time(tm);
	return secZero;
end
--获取当前时间，秒
function GetTimeNowInSecond()
	return os.time();
end

--获取当前时间，毫秒
function GetCurrentTime()
	return socket.gettime();
end

--获取当前年月日
function getCurDate()
	local timeStr = os.date("%x", os.time());
	local timeTab = Split( timeStr, "/");
	
	return { timeTab[3], timeTab[1], timeTab[2]};
end