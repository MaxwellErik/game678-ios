#import "NSBuy.h"
#import <UIKit/UIKit.h>
#import "cocos2d.h"
#import "StoreKit/StoreKit.h"
#import "IAP.h"
#import "GoodsByWeChatPay.h"

CppBuy *CppBuy::m_pCppBuy = NULL;

IAP *g_pIAP;

void CppBuy::buy(int iType)
{
    if (g_pIAP == nil) {
        g_pIAP = [[IAP alloc] init];
    }
    [g_pIAP buy:iType];
}

CppBuy *CppBuy::sharedCppBuy()
{
    if (m_pCppBuy == NULL) {
        m_pCppBuy = new CppBuy;
    }
    return m_pCppBuy;
}

void CppBuy::OnBuyFinish(bool bSuccess, const char *pJson)
{
    GoodsByWeChatPay_cpp::shared()->onIOSPayFinish(bSuccess, pJson);
}
