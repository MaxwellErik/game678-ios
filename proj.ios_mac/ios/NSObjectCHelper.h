﻿//
//  NSObjectCHelper.h
//  
//
//  Created by user on 13-9-28.
//
//

//#import <Foundation/Foundation.h>



#pragma once


class NSObjectCHelper
{
    //NSBuy   *nsBuy;
public:
    NSObjectCHelper(){}
    
    static NSObjectCHelper *sharedNSObjectCHelper();

    void callObjectCCommand(const int command, const char *szParam);

	void callCppCommand(const int command, const char *szParam);
	

private:
    static NSObjectCHelper *m_pNSObjectCHelper;
};
/*

@end
*/
