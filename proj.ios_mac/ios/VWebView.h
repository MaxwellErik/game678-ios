//
//  PopStarAAppController.h
//  PopStarA
//
//  Created by YongSang Lee on 13. 6. 2..
//  Copyright __MyCompanyName__ 2013년. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseView.h"

@interface VWebView : BaseView<UIWebViewDelegate>

- (void)setUrl:(NSString*)url;

@end
