//
//  NSBuy.h
//  奖金德州
//
//  Created by user on 13-9-28.
//
//

#ifndef _H_NSBUY_H_
#define _H_NSBUY_H_

#include "CCPlatformDefine-ios.h"

#pragma once

enum buyCoinsTag{
    IAP_1_1=10,
    IAP_1_2,
    IAP_1_3,
    IAP_1_4,
    IAP_1_5,
    IAP_1_6,
    IAP_1_7,
    IAP_1_8,
	IAP_2_1,
	IAP_2_2,
	IAP_2_3,
	IAP_2_4,
	IAP_2_5
};

class CppBuy
{
public:
    CppBuy(){}
    void buy(int iType);
    
    static CppBuy *sharedCppBuy();
    //支付完成
    void OnBuyFinish(bool bSuccess, const char *pJson);
    
private:
    int     m_iBuyType;
    static CppBuy *m_pCppBuy;
};

#endif
