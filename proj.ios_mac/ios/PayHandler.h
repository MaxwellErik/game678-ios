//
//  PayHandler.h
//  demo
//
//  Created by lbx on 16/8/6.
//  Copyright © 2016年 nrkt. All rights reserved.
//

#include <iostream>

typedef void(*payResult)(int);


class PayHandler
{
public:
    /*
     identifer                      购买的商品id(从自己服务器获取，必填项，跟appstore上设置的商品id保持一致)
     creatOrderUrl                  去服务器创建订单的地址(从自己服务器获取，是你把数据传给游戏服务器的接口地址，就是通过这个接口把数据传递给服务器)
     mid                            游戏里你的id（从自己服务器获取）
     gameid                         游戏在服务器的id（这个是你的游戏在支付服务器的id，如果未设置，默认99）
     game_name                      游戏的名字（客户端获取）
     goods_name                     购买的商品的名字（从自己服务器获取，商品的名字，如果没有，默认  ”商品“）
     remark                         备注（remark回调的时候原路返回）
     payResult                      支付回调   1 - 成功   -1 - 失败  -2 - 取消  -3 - 验证订单出错
     */
    
    static void payProductWithIdentifier(std::string identifer,
                                         std::string creatOrderUrl,
                                         std::string mid,
                                         std::string gameid,
                                         std::string game_name,
                                         std::string goods_name,
                                         std::string remark,
                                         payResult result = NULL);

};
