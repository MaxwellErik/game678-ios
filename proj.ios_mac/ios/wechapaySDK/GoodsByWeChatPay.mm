//
//  GoodsByWeChatPay.m
//  game550
//
//  Created by acergeiwo on 15/11/7.
//
//

#import "GoodsByWeChatPay.h"
#import <Foundation/Foundation.h>
#import "AppController.h"
#import "payRequsestHandler.h"
#import "WXApi.h"
#import "APPBuyPay.h"
#include "LoginLayer.h"
#include "GameScene.h"
#include "cocos-ext.h"
#include "LobbySocketSink.h"
#include "GameLayerMove.h"
#include "AliPayHandler.h"
#include "NSBuy.h"
#include "IOSAuditStateHandler.h"
#import <PayHandler.h>
#include "sendInfoToUmeng.h"

typedef void(*payResult)(int);

void payS(int result);


@interface  GoodsByWeChatPay: NSObject

@end

@implementation GoodsByWeChatPay

GoodsByWeChatPay_cpp*	GoodsByWeChatPay_cpp::m_GoodsByWechatPay_cpp=NULL;

GoodsByWeChatPay_cpp::GoodsByWeChatPay_cpp(void)
{
    m_payType = 0;
}

GoodsByWeChatPay_cpp::~GoodsByWeChatPay_cpp(void)
{
    
}

GoodsByWeChatPay_cpp* GoodsByWeChatPay_cpp::shared()
{
    if (m_GoodsByWechatPay_cpp == NULL)
    {
        m_GoodsByWechatPay_cpp = new GoodsByWeChatPay_cpp();
    }
    return m_GoodsByWechatPay_cpp;
}

 void GoodsByWeChatPay_cpp::Game_WxLoginSuc(const char* unionid, int code)
{
   GameLayerMove::sharedGameLayerMoveSink()->IosWxSucCallBack(unionid, code);
}

void GoodsByWeChatPay_cpp::GetIOSAuditState(int state, int pass)
{
    g_GlobalUnits.m_SwitchYZ = state;
    g_GlobalUnits.m_YZPASS = pass;
    GameLayerMove::sharedGameLayerMoveSink()->IosAuditStateCallBack();
}

void GoodsByWeChatPay_cpp::IosAliPay(int userId, int gameId, int payTpey)
{
    int moneyString = 0;
    if (payTpey == 0) {
        moneyString = 10;
    }else if (payTpey == 1){
        moneyString = 20;
    }else if (payTpey == 2){
        moneyString = 50;
    }else if (payTpey == 3){
        moneyString = 100;
    }else if (payTpey == 4){
        moneyString = 500;
    }
   
    payRequsestHandler *pay = [payRequsestHandler sharedPayRequestHandler];
    [pay IosAliPay:userId gameId:gameId orderamount:moneyString];
}

void GoodsByWeChatPay_cpp::IosPay(int payType)
{
    m_payType = payType;
    CppBuy::sharedCppBuy()->buy(IAP_1_1+payType);
}

void GoodsByWeChatPay_cpp::onIOSPayFinish(bool bSuccess, const char *pJson)
{
    if (bSuccess)
    {
        GameLayerMove::sharedGameLayerMoveSink()->OnIosRechargeSuccess(m_payType);
    }
    else
    {
        ClearLoading();
        
        UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"购买失败，请稍后重试！" message:nil delegate:nil cancelButtonTitle:@"取消" otherButtonTitles:nil];
        [alert show];
        [alert release];
    }
}

void GoodsByWeChatPay_cpp::postNotification()
{
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"WeChatPayNotitfication" object:nil];
    NSLog(@"受到通知");
}

void GoodsByWeChatPay_cpp::WxBind(char *url)
{
    payRequsestHandler *pay = [payRequsestHandler sharedPayRequestHandler];
    [pay WxBind:url];
}

void GoodsByWeChatPay_cpp::GetIOSAuditState()
{
    IOSAuditStateHandler *state = [IOSAuditStateHandler sharedIOSAuditStateHandler];
    [state sendVersionToServer];
}

 void GoodsByWeChatPay_cpp::Game_WxLogin()
{
    payRequsestHandler *pay = [payRequsestHandler sharedPayRequestHandler];
    [pay weixinLogin];
}

void GoodsByWeChatPay_cpp::payWeChat(int userId, int gameId, int payTpey)
{
    if ([WXApi isWXAppInstalled] == false)
    {
        NSLog(@"请安装微信");
        UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"提示" message:@"请安装微信" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"确定", nil];
        [alertView show];
        [alertView setHidden:YES];
        GoodsByWeChatPay_cpp::shared()->PushWinodow(4);
        return;
    }

    int moneyString = 0;
    if (payTpey == 0) {
        moneyString = 1000;
    }else if (payTpey == 1){
        moneyString = 2000;
    }else if (payTpey == 2){
        moneyString = 5000;
    }else if (payTpey == 3){
        moneyString = 10000;
    }else if (payTpey == 4){
        moneyString = 50000;
    }
    
    payRequsestHandler *pay = [payRequsestHandler sharedPayRequestHandler];
    [pay WeChatPay:userId gameId:gameId orderamount:moneyString];
}

void GoodsByWeChatPay_cpp::UpdateUserScore(int payPlatform)
{
    LONGLONG score = 0;
    int money = 0;
    switch (m_payType) {
        case 0:
        {
            money = 10;
            score = 205000;
            break;
        }
        case 1:
        {
            money = 20;
            score = 410000;
            break;
        }
        case 2:
        {
            money = 50;
            score = 1025000;
            break;
        }
        case 3:
        {
            money = 100;
            score = 2050000;
            break;
        }
        case 4:
        {
            money = 500;
            score = 10250000;
            break;
        }
        default:
            break;
    }
    g_GlobalUnits.GetGolbalUserData().lInsureScore+=score;
    GameLayerMove::sharedGameLayerMoveSink()->UpdataLobbyScore();
    SendInfoToUmeng::sendRechargeInfoToUmeng(payPlatform, money);
}

void GoodsByWeChatPay_cpp::PushWinodow(int reson)
{
    if (reson == 1)  //get order fiale
    {
        GameLayerMove::sharedGameLayerMoveSink()->PushWinodow("\u8ba2\u5355\u83b7\u53d6\u5931\u8d25\uff01");
    }
    else if (reson == 2)    //微信登录失败
    {
        GameLayerMove::sharedGameLayerMoveSink()->PushWinodow("\u5fae\u4fe1\u767b\u5f55\u5931\u8d25");
    }
    else if (reson == 3)  //您已取消微信支付
    {
        GameLayerMove::sharedGameLayerMoveSink()->PushWinodow("\u60a8\u5df2\u53d6\u6d88\u5fae\u4fe1\u652f\u4ed8");
    }
    else if (reson == 4)
    {
        //没有安装微信
    }
    else if(reson == 5)//获取订单失败，请重新尝试
    {
        GameLayerMove::sharedGameLayerMoveSink()->PushWinodow("\u83b7\u53d6\u8ba2\u5355\u5931\u8d25\uff0c\u8bf7\u91cd\u65b0\u5c1d\u8bd5\uff01");
    }
    
    ClearLoading();
}

void GoodsByWeChatPay_cpp::AddLoading()
{
    GameLayerMove::sharedGameLayerMoveSink()->AddLoading();
}

void GoodsByWeChatPay_cpp::ClearLoading()
{
    GameLayerMove::sharedGameLayerMoveSink()->ClearLoading();
}
@end
