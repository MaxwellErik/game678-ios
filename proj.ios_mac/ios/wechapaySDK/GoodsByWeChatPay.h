//
//  GoodsByWeChatPay.h
//  game550
//
//  Created by acergeiwo on 15/11/7.
//
//


#include "LoginLayer.h"

#include <stdio.h>

class GoodsByWeChatPay_cpp
{
public:
    GoodsByWeChatPay_cpp(void);
    ~GoodsByWeChatPay_cpp(void);
    static GoodsByWeChatPay_cpp* shared();
    static GoodsByWeChatPay_cpp* m_GoodsByWechatPay_cpp;
    void postNotification();
    void payWeChat(int userId, int gameId, int payTpey);
    void Game_WxLogin();
    void Game_WxLoginSuc(const char* unionid, int code);
    void GetIOSAuditState(int state, int pass);
    void IosAliPay(int userId, int gameId, int payTpey);
    void IosPay(int payTpey);
    void onIOSPayFinish(bool bSuccess, const char *pJson);
    
    void PushWinodow(int reson);
    void AddLoading();
    void ClearLoading();
    void UpdateUserScore(int payPlatform);
    
    void WxBind(char *);
    void GetIOSAuditState();
    void SaveMachineID(char* machineid);
    char* GetMachineID();
private:
    int m_payType;
};
