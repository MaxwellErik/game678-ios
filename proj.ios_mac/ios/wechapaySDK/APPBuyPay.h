//
//  APPBuyPay.h
//  game550
//
//  Created by acergeiwo on 15/11/22.
//
//

#import <Foundation/Foundation.h>
#import <StoreKit/StoreKit.h>

enum{
    IAP0p99=10,
    IAP1p99,
    IAP4p99,
    IAP9p99,
    IAP24p99,
}buyCoinsTag;



@interface APPBuyPay : NSObject<SKProductsRequestDelegate,SKPaymentTransactionObserver>
{
    int buyType;
    NSString *PrepayId;
    NSString *orderamount;
    NSString *userid;
}

+ (id)sharedBuyPay;

- (id)init;

//获取订单
- (void)getIPAPay:(NSDictionary *)dic;

//支付完成
- (void)payIADFinish;
@end
