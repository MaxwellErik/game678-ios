//
//  WeChatLoginHandler.h
//
//  Created by maxwell on 16/11/10.
//
//

#import <Foundation/Foundation.h>

@interface WeChatLoginHandler : NSObject

/// 获取单例
+ (id)sharedWeChatLoginHandler;

/// 生成预支付订单
- (void)sendCodeToServer:(NSString*)code;

@end
