//
//  WeChatLoginHandler.m
//
//  Created by maxlwell on 16/11/10.
//
//

#import "WeChatLoginHandler.h"
#import "GoodsByWeChatPay.h"

@interface WeChatLoginHandler()
@property (retain, nonatomic)NSString *openid;
@property (nonatomic, assign) int code;

@end


@implementation WeChatLoginHandler

/// 获取单例
+ (id)sharedWeChatLoginHandler
{
    static WeChatLoginHandler *_handler = nil;
    static dispatch_once_t once;
    dispatch_once(&once, ^{
        _handler = [[self alloc] init];
    });
    return _handler;
}

- (void)sendCodeToServer:(NSString*)code
{
    //1.URL
    NSURL* url = [NSURL URLWithString:@"https://weixin.game678.net.cn/wl/WeixinLogin.ashx"];
    // 2. 请求(可以改的请求)
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    //3.默认就是GET请求
    request.HTTPMethod = @"POST";
    
    // 4. 数据体
    NSString *str = [NSString stringWithFormat:@"code=%@", code];
    
    // 5.将字符串转换成数据
    request.HTTPBody = [str dataUsingEncoding:NSUTF8StringEncoding];
    
    // 6. 连接,异步
    [NSURLConnection sendAsynchronousRequest:request queue:[[NSOperationQueue alloc] init] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        
        if (connectionError == nil) {
            
            NSDictionary * dict = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            if ([[dict objectForKey:@"result"] isEqualToString:@"SUCCEE"]) {
                
                _openid = [dict objectForKey:@"openid"];
                _code = [[dict objectForKey:@"tokenpass"]intValue];
                [self onLoginByWeChat];
            }
            else
            {
                GoodsByWeChatPay_cpp::shared()->ClearLoading();
                UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"获取微信授权失败" message:nil delegate:nil cancelButtonTitle:@"取消" otherButtonTitles:nil];
                [alert show];
                [alert release];
            }
        }
    }];
}


- (void)onLoginByWeChat
{
    const char * openid = [_openid cStringUsingEncoding:NSUTF8StringEncoding];
    GoodsByWeChatPay_cpp::shared()->Game_WxLoginSuc(openid, _code);
}
@end
