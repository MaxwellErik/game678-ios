//
//  MyWebView.h
//  game550
//
//  Created by lbx on 16/6/2.
//
//

#import <UIKit/UIKit.h>

@protocol MyWebViewDelegate<NSObject>

-(void)removeFromParent;

@end

@interface MyWebView : UIView


@property(nonatomic,retain)id<MyWebViewDelegate> delegate;

@property(strong,nonatomic)UIImageView *bgView;

@property(strong,nonatomic)UIActivityIndicatorView *indicatorView;

- (instancetype)initWithRequestUrl:(NSString *)urlString;

@end
