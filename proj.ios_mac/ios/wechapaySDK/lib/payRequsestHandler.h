

#import <Foundation/Foundation.h>
#import "WXUtil.h"
#import "ApiXml.h"
#import "WXApi.h"

#import "MyWebView.h"

@interface payRequsestHandler : NSObject<WXApiDelegate,MyWebViewDelegate>
{
	//预支付网关url地址
    NSString *payUrl;

    //lash_errcode;
    long     last_errcode;
	//debug信息
    NSMutableString *debugInfo;
}

@property(strong,nonatomic)MyWebView *mywebView;

/// 获取单例
+ (id)sharedPayRequestHandler;

//微信支付
- (void)WeChatPay:(int)userId gameId:(int)gameId orderamount:(int)orderamount;

- (IBAction)weixinLogin;

- (IBAction)IosAliPay:(int)userId gameId:(int)gameId orderamount:(int)orderamount;

-(void)onResp:(BaseReq *)resp;

-(void)WxBind:(char*)url;

@end
