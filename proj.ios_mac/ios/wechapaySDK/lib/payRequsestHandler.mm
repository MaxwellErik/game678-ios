
#import <Foundation/Foundation.h>
#import "payRequsestHandler.h"
#import "MyWebView.h"

#import "Order.h"
#import "APAuthV2Info.h"
#import <AlipaySDK/AlipaySDK.h>

#include "GoodsByWeChatPay.h"
#include "AliPayHandler.h"
#include "WeChatLoginHandler.h"


@implementation payRequsestHandler

/// 获取单例
+ (id)sharedPayRequestHandler
{
    static payRequsestHandler *_payRequestHandler = nil;
    static dispatch_once_t once;
    dispatch_once(&once, ^{
        _payRequestHandler = [[self alloc] init];
    });
    return _payRequestHandler;
}

- (instancetype)init
{
    self = [super init];
    return self;
}

- (void)WeChatPay:(int)userId gameId:(int)gameId orderamount:(int)orderamount;
{
    // 1.将网址初始化成一个OC字符串对象
    NSString *urlStr = [NSString stringWithFormat:@"https://pay.game678.com.cn/WeiXinAppPayCreateOrder.ashx?userid=%d&gameid=%d&total_amount=%d",userId, gameId, orderamount];
    // 如果网址中存在中文,进行URLEncode
    NSString *newUrlStr = [urlStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    // 2.构建网络URL对象, NSURL
    NSURL *url = [NSURL URLWithString:newUrlStr];
    // 3.创建网络请求
    NSURLRequest *request = [NSURLRequest requestWithURL:url cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:10];
    // 创建同步链接
    NSURLResponse *response = nil;
    NSError *error = nil;
    NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    
    if (data == nil)
    {
        GoodsByWeChatPay_cpp::shared()->PushWinodow(5);
        return;
    }
    
    NSDictionary * dict = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];

    if ([dict objectForKey:@"appid"])
    {
        [self wxpay:dict];
    }
    else{
        GoodsByWeChatPay_cpp::shared()->PushWinodow(5);
    }
}

- (void)wxpay:(NSDictionary *)dict
{
    if ([WXApi isWXAppInstalled] == false)
    {
        NSLog(@"请安装微信");
        UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"提示" message:@"请安装微信" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"确定", nil];
        [alertView show];
        [alertView setHidden:YES];
        GoodsByWeChatPay_cpp::shared()->PushWinodow(4);
        return;
    }

    if(dict == nil){
        //错误提示
        NSLog(@"订单获取失败\n");
    }else{

        NSMutableString *stamp  = [dict objectForKey:@"timestamp"];
        
        //调起微信支付
        PayReq* req             = [[PayReq alloc] init];
        req.openID              = [dict objectForKey:@"appid"];
        req.partnerId           = [dict objectForKey:@"partnerid"];
        req.prepayId            = [dict objectForKey:@"prepayid"];
        req.nonceStr            = [dict objectForKey:@"noncestr"];
        req.timeStamp           = stamp.intValue;
        req.package             = [dict objectForKey:@"package"];
        req.sign                = [dict objectForKey:@"sign"];
        
        [WXApi sendReq:req];
    }
}

- (IBAction)weixinLogin
{
    if ([WXApi isWXAppInstalled] == false)
    {
        UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"提示" message:@"请安装微信" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"确定", nil];
        [alertView show];
        [alertView setHidden:YES];
        GoodsByWeChatPay_cpp::shared()->PushWinodow(4);
        return;
    }
    
    // 正常情况下，走微信登陆
    SendAuthReq* req =[[[SendAuthReq alloc ] init ] autorelease ];
    req.scope = @"snsapi_userinfo" ;
    req.state = @"game678" ;
    [WXApi sendReq:req];
}

- (IBAction)IosAliPay:(int)userId gameId:(int)gameId orderamount:(int)orderamount
{
    int _userid = userId;
    int _gameid = gameId;
    int _orderamount = orderamount;
    
    NSLog(@"userid = %d, gameid = %d, orderamount = %d",_userid, _gameid, orderamount);
    AliPayHandler *aliPayHandler = [AliPayHandler sharedAlipayHandler];
    [aliPayHandler createOrderIdWithUserId:_userid gameId:_gameid price:_orderamount];
}

#pragma mark - weChatPay

-(void)onReq:(BaseReq*)req
{
    if([req isKindOfClass:[GetMessageFromWXReq class]])
    {
        // 微信请求App提供内容， 需要app提供内容后使用sendRsp返回
        NSString *strTitle = [NSString stringWithFormat:@"微信请求App提供内容"];
        NSString *strMsg = @"微信请求App提供内容，App要调用sendResp:GetMessageFromWXResp返回给微信";
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:strTitle message:strMsg delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        alert.tag = 1000;
        [alert show];
    }
    else if([req isKindOfClass:[ShowMessageFromWXReq class]])
    {
        ShowMessageFromWXReq* temp = (ShowMessageFromWXReq*)req;
        WXMediaMessage *msg = temp.message;
        
        //显示微信传过来的内容
        WXAppExtendObject *obj = msg.mediaObject;
        
        NSString *strTitle = [NSString stringWithFormat:@"微信请求App显示内容"];
        NSString *strMsg = [NSString stringWithFormat:@"标题：%@ \n内容：%@ \n附带信息：%@ \n缩略图:%lu bytes\n\n", msg.title, msg.description, obj.extInfo, (unsigned long)msg.thumbData.length];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:strTitle message:strMsg delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        
    }
    else if([req isKindOfClass:[LaunchFromWXReq class]])
    {
        //从微信启动App
        NSString *strTitle = [NSString stringWithFormat:@"从微信启动"];
        NSString *strMsg = @"这是从微信启动的消息";
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:strTitle message:strMsg delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
}

-(void) onResp:(BaseResp*)resp
{
    NSString *strMsg = [NSString stringWithFormat:@"errcode:%d", resp.errCode];
    NSString *strTitle;
    if ([resp isKindOfClass:[SendAuthResp class]]) {
        SendAuthResp *aresp = (SendAuthResp *)resp;
        
        if (aresp.errCode== 0)
        {
             GoodsByWeChatPay_cpp::shared()->AddLoading();
            NSLog(@"%@", aresp.code);
            WeChatLoginHandler *weChatLoginHandler = [WeChatLoginHandler sharedWeChatLoginHandler];
            [weChatLoginHandler sendCodeToServer:aresp.code];
        }
        else
        {
            GoodsByWeChatPay_cpp::shared()->PushWinodow(2);
        }

    } else if([resp isKindOfClass:[SendMessageToWXResp class]])
    {
        strTitle = [NSString stringWithFormat:@"发送媒体消息结果"];
    } else if([resp isKindOfClass:[PayResp class]]){
        //支付返回结果，实际支付结果需要去微信服务器端查询
        strTitle = [NSString stringWithFormat:@"支付结果"];
        
        switch (resp.errCode) {
            case WXSuccess:
                strMsg = @"支付结果：成功！";
                
                // 执行微信支付成功方法
                [[NSNotificationCenter defaultCenter] postNotificationName:@"wechatPaySuccessNotification" object:nil];
                GoodsByWeChatPay_cpp::shared()->PushWinodow(4);
                GoodsByWeChatPay_cpp::shared()-> UpdateUserScore(0);
                break;
                
            default:
                if (resp.errCode == -2) {
                    strMsg = @"您已取消支付，可在我的订单出查看详情";
                    GoodsByWeChatPay_cpp::shared()->PushWinodow(3);
                }
                break;
        }
    }
}

- (void)showErrorString:(NSString *)errMsg
{
    UIAlertView *av = [[UIAlertView alloc] initWithTitle:nil message:errMsg delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
    [av show];
    [av release];
}

-(void)WxBind:(char*)url
{
    _mywebView = [[MyWebView alloc] initWithRequestUrl:[NSString stringWithUTF8String:url]];
    _mywebView.delegate = self;
    [[UIApplication sharedApplication].keyWindow addSubview:_mywebView];
    [_mywebView release];
}

-(void)removeFromParent
{
    [_mywebView removeFromSuperview];
}

//请求失败的回调方法
//- (void)responseFailed:(ASIFormDataRequest *)request
//{
//    GoodsByWeChatPay_cpp::shared()->PushWinodow(1);
//}

/***************************苹果支付**************************************************/
@end
