//
//  MyWebView.m
//  game550
//
//  Created by lbx on 16/6/2.
//
//

#import "MyWebView.h"

@interface MyWebView()<UIWebViewDelegate>

@property (strong, nonatomic)UIWebView *webView;
@property (strong, nonatomic)NSURLRequest *request;

@property(strong,nonatomic)UIImage *savedImage;


@end

@implementation MyWebView

-(void)dealloc
{
    self.delegate = nil;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
    }
    return self;
}

- (instancetype)initWithRequestUrl:(NSString *)urlString
{
    self = [super init];
    if (self) {
        self.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height);
        [self setCenter:CGPointMake([UIScreen mainScreen].bounds.size.width/2, [UIScreen mainScreen].bounds.size.height/2)];
        self.backgroundColor = [UIColor clearColor];
        
        _request = [NSURLRequest requestWithURL:[NSURL URLWithString:urlString]];
        
        [self makeView];
    }
    return self;
}

- (void)makeView
{
     CGSize mainScreenSize = [UIScreen mainScreen].bounds.size;
    _webView = [[UIWebView alloc] initWithFrame:self.bounds];
    _webView.frame = CGRectMake(mainScreenSize.width/2, mainScreenSize.height/2, mainScreenSize.width / 100, mainScreenSize.height / 100);
    _webView.backgroundColor = [UIColor clearColor];
    _webView.delegate = self;
    [self addSubview:_webView];
    [_webView release];
    [_webView loadRequest:_request];
    
//    UIButton *btnReturn = [UIButton buttonWithType:UIButtonTypeSystem];
//    [btnReturn setTitle:@"返回" forState:UIControlStateNormal];
//    btnReturn.frame = CGRectMake(10, 10, 60, 40);
//    [btnReturn addTarget:self action:@selector(closeBtnClick:) forControlEvents:UIControlEventTouchUpInside];
//    [_webView addSubview:btnReturn];
    
    _bgView = [[UIImageView alloc]initWithFrame:self.bounds];
    _bgView.backgroundColor = [UIColor clearColor];
    _bgView.frame = CGRectMake(mainScreenSize.width/2, mainScreenSize.height/2, mainScreenSize.width * 1326 / 1920, mainScreenSize.height * 857 / 1080);
    [_bgView setCenter:CGPointMake(mainScreenSize.width/2, mainScreenSize.height/2)];
    [_bgView setImage:[UIImage imageNamed:@"WechatBind/WeiXinBind"]];
    [self addSubview:_bgView];

    UIButton *closeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [closeBtn setImage:[UIImage imageNamed:@"WechatBind/btn_close_normal"] forState:UIControlStateNormal];
    closeBtn.frame = CGRectMake(mainScreenSize.width - mainScreenSize.width * 410 / 1920,
                                mainScreenSize.height * 120 / 1080,
                                mainScreenSize.width * 136 / 1920,
                                mainScreenSize.height * 136 / 1080);
    [closeBtn addTarget:self action:@selector(closeBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:closeBtn];
}

-(void)closeBtnClick:(UIButton *)sender
{
    if (!(_savedImage == nil))
    {
        UIImageWriteToSavedPhotosAlbum(_savedImage, self, @selector(image:didFinishSavingWithError:contextInfo:), nil);
        [_savedImage release];
    }
    else
    {
        NSData *data = [NSData dataWithContentsOfURL:_request.URL];

        UIImage *image = [[UIImage alloc] initWithData:data];

        UIImageWriteToSavedPhotosAlbum(image, self, @selector(image:didFinishSavingWithError:contextInfo:), nil);

        [image release];
        
    }

    [_delegate removeFromParent];
}

/// 保存结果的回调
- (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo: (void *) contextInfo
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [_delegate removeFromParent];
        
        if (error!=NULL) {
            UIAlertView *av = [[[UIAlertView alloc] initWithTitle:nil message:error.description delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil] autorelease];
            [av show];
            [av setHidden:YES];
            //[av release];
            return;
        }
        UIAlertView *av = [[[UIAlertView alloc] initWithTitle:nil message:@"保存成功" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil] autorelease];
        [av show];
        [av setHidden:YES];
        //[av release];

        
    });
}


#pragma mark - UIWebViewDelegate
- (void)webViewDidStartLoad:(UIWebView *)webView
{
    CGSize mainScreenSize = [UIScreen mainScreen].bounds.size;
    NSLog(@"开始加载");
    _indicatorView = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    _indicatorView.frame = CGRectMake(mainScreenSize.width * 830/1920, mainScreenSize.height*310/1080, 90, 90);
    [_indicatorView setHidesWhenStopped:YES];
    [self addSubview:_indicatorView];
    [_indicatorView startAnimating];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    CGSize mainScreenSize = [UIScreen mainScreen].bounds.size;
    [_indicatorView stopAnimating];
    [_indicatorView removeFromSuperview];
    NSLog(@"加载完成");
    NSData *data = [NSData dataWithContentsOfURL:_request.URL];
    _savedImage = [[UIImage alloc]initWithData:data];
    UIButton *QCRBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    QCRBtn.frame = CGRectMake(mainScreenSize.width * 830/1920,
                              mainScreenSize.height*310/1080,
                              mainScreenSize.width* 255/1920,
                              mainScreenSize.height*255/1080);
    
    if (_savedImage)
    {
        [QCRBtn setImage:_savedImage forState:UIControlStateNormal];
    }
    [self addSubview:QCRBtn];

}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    return YES;
}

@end
