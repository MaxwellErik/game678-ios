//
//  APPBuyPay.m
//  game550
//
//  Created by acergeiwo on 15/11/22.
//
//

#import "APPBuyPay.h"
//#import "ASIFormDataRequest.h"
#define ProductID_IAP0p99 @"com.game550.p1"//$0.99
#define ProductID_IAP1p99 @"com.game550.p2" //$1.99
#define ProductID_IAP4p99 @"com.game550.p3" //$4.99
#define ProductID_IAP9p99 @"com.game550.p4" //$19.99
#define ProductID_IAP24p99 @"com.game550.p5" //$24.99

@implementation APPBuyPay

//+ (id)sharedBuyPay{
//    //
//    static id sharedBuyPay = nil;
//    if(sharedBuyPay == nil){
//        sharedBuyPay = [[self alloc] init];
//    }
//    return sharedBuyPay;
//    //
//}
//
//- (id)init{
//    if (self = [super init]) {
//        
//        [self buy:buyType];
//    }
//    return self;
//}

- (void)buy:(NSInteger)buytype
{
    if ([SKPaymentQueue canMakePayments]) {
        [[SKPaymentQueue defaultQueue] restoreCompletedTransactions];
        
        [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
        
        
        [self RequestProductData:buytype];
    }
    else
    {
        UIAlertView *alerView =  [[UIAlertView alloc] initWithTitle:@"提示"
                                                            message:@"您不能在应用程序内购买"
                                                           delegate:nil cancelButtonTitle:@"好的，我知道了" otherButtonTitles:nil];
        
        [alerView show];
        [alerView release];
        
    }
}

- (void)RequestProductData:(NSInteger)buytype
{
    NSArray *product = nil;
    switch (buytype) {
        case IAP0p99:
            product=[[NSArray alloc] initWithObjects:ProductID_IAP0p99,nil];
            break;
        case IAP1p99:
            product=[[NSArray alloc] initWithObjects:ProductID_IAP1p99,nil];
            break;
        case IAP4p99:
            product=[[NSArray alloc] initWithObjects:ProductID_IAP4p99,nil];
            break;
        case IAP9p99:
            product=[[NSArray alloc] initWithObjects:ProductID_IAP9p99,nil];
            break;
        case IAP24p99:
            product=[[NSArray alloc] initWithObjects:ProductID_IAP24p99,nil];
            break;
            
        default:
            break;
    }
    NSSet *nsset = [NSSet setWithArray:product];
    SKProductsRequest *request=[[SKProductsRequest alloc] initWithProductIdentifiers: nsset];
    request.delegate=self;
    [request start];
    [product release];
}

//<SKProductsRequestDelegate> 请求协议
//收到的产品信息
- (void)productsRequest:(SKProductsRequest *)request didReceiveResponse:(SKProductsResponse *)response{
    
    NSLog(@"-----------收到产品反馈信息--------------");
    NSArray *myProduct = response.products;
    NSLog(@"产品Product ID:%@",response.invalidProductIdentifiers);
    NSLog(@"产品付费数量: %d", [myProduct count]);
    // populate UI
    for(SKProduct *product in myProduct){
        NSLog(@"product info");
        NSLog(@"SKProduct 描述信息%@", [product description]);
        NSLog(@"产品标题 %@" , product.localizedTitle);
        NSLog(@"产品描述信息: %@" , product.localizedDescription);
        NSLog(@"价格: %@" , product.price);
        NSLog(@"Product id: %@" , product.productIdentifier);
    }
    SKPayment *payment = nil;
    switch (buyType) {
        case IAP0p99:
            payment  = [SKPayment paymentWithProductIdentifier:ProductID_IAP0p99];    //支付6
            break;
        case IAP1p99:
            payment  = [SKPayment paymentWithProductIdentifier:ProductID_IAP1p99];    //支付12
            break;
        case IAP4p99:
            payment  = [SKPayment paymentWithProductIdentifier:ProductID_IAP4p99];    //支付18
            break;
        case IAP9p99:
            payment  = [SKPayment paymentWithProductIdentifier:ProductID_IAP9p99];    //支付25
            break;
        case IAP24p99:
            payment  = [SKPayment paymentWithProductIdentifier:ProductID_IAP24p99];    //支付30
            break;
        default:
            break;
    }
    NSLog(@"---------发送购买请求------------");
    [[SKPaymentQueue defaultQueue] addPayment:payment];
    [request autorelease];
    
}
- (void)requestProUpgradeProductData
{
    NSLog(@"------请求升级数据---------");
    NSSet *productIdentifiers = [NSSet setWithObject:@"com.productid"];
    SKProductsRequest* productsRequest = [[SKProductsRequest alloc] initWithProductIdentifiers:productIdentifiers];
    productsRequest.delegate = self;
    [productsRequest start];
    
}
//弹出错误信息
- (void)request:(SKRequest *)request didFailWithError:(NSError *)error{
    NSLog(@"-------弹出错误信息----------");
    UIAlertView *alerView =  [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Alert",NULL) message:[error localizedDescription]
                                                       delegate:nil cancelButtonTitle:NSLocalizedString(@"Close",nil) otherButtonTitles:nil];
    [alerView show];
    [alerView release];
}

-(void) requestDidFinish:(SKRequest *)request
{
    NSLog(@"----------反馈信息结束--------------");
    
}

-(void) PurchasedTransaction: (SKPaymentTransaction *)transaction{
    NSLog(@"-----PurchasedTransaction----");
    NSArray *transactions =[[NSArray alloc] initWithObjects:transaction, nil];
    [self paymentQueue:[SKPaymentQueue defaultQueue] updatedTransactions:transactions];
    [transactions release];
}

//<SKPaymentTransactionObserver> 千万不要忘记绑定，代码如下：
//----监听购买结果
//[[SKPaymentQueue defaultQueue] addTransactionObserver:self];

- (void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray *)transactions//交易结果
{
    NSLog(@"-----paymentQueue--------");
    for (SKPaymentTransaction *transaction in transactions)
    {
        if (transaction.transactionState == SKPaymentTransactionStatePurchased) {
            [self completeTransaction:transaction];
            NSLog(@"-----交易完成 --------");
            NSLog(@"不允许程序内付费购买");
            UIAlertView *alerView =  [[UIAlertView alloc] initWithTitle:@"提示"
                                                                message:@"购买成功"
                                                               delegate:nil cancelButtonTitle:@"好的" otherButtonTitles:nil];
            
            [alerView show];
            
            //支付完成
            [self payIADFinish];
        }else if (transaction.transactionState == SKPaymentTransactionStateFailed){
            [self failedTransaction:transaction];
            NSLog(@"-----交易失败 --------");
            UIAlertView *alerView2 =  [[UIAlertView alloc] initWithTitle:@"提示"
                                                                 message:@"购买失败，请重新尝试购买～"
                                                                delegate:nil cancelButtonTitle:@"好的，我知道了" otherButtonTitles:nil];
            
            [alerView2 show];
        }else if (transaction.transactionState == SKPaymentTransactionStateRestored){
            [self restoreTransaction:transaction];
            NSLog(@"-----已经购买过该商品 --------");
        }else if (transaction.transactionState == SKPaymentTransactionStatePurchasing){
            
        }
    }
}
- (void) completeTransaction: (SKPaymentTransaction *)transaction

{
    NSLog(@"-----completeTransaction--------");
    // Your application should implement these two methods.
    NSString *product = transaction.payment.productIdentifier;
    if ([product length] > 0) {
        
        NSArray *tt = [product componentsSeparatedByString:@"."];
        NSString *bookid = [tt lastObject];
        if ([bookid length] > 0) {
            [self recordTransaction:bookid];
            [self provideContent:bookid];
        }
    }
    
    // Remove the transaction from the payment queue.
    
    [[SKPaymentQueue defaultQueue] finishTransaction: transaction];
    
}

//记录交易
-(void)recordTransaction:(NSString *)product{
    NSLog(@"-----记录交易--------");
}

//处理下载内容
-(void)provideContent:(NSString *)product{
    NSLog(@"-----下载--------");
}

- (void) failedTransaction: (SKPaymentTransaction *)transaction{
    if(transaction.error.code != SKErrorPaymentCancelled) {
        NSLog(@"购买失败");
    } else {
        NSLog(@"用户取消交易");
    }
    [[SKPaymentQueue defaultQueue] finishTransaction: transaction];
}
-(void) paymentQueueRestoreCompletedTransactionsFinished: (SKPaymentTransaction *)transaction{
    
}

- (void) restoreTransaction: (SKPaymentTransaction *)transaction

{
    NSLog(@" 交易恢复处理");
    [[SKPaymentQueue defaultQueue] finishTransaction: transaction]; 
    
}

-(void) paymentQueue:(SKPaymentQueue *) paymentQueue restoreCompletedTransactionsFailedWithError:(NSError *)error{
    NSLog(@"-------paymentQueue----");
}



//获取订单
- (void)getIPAPay:(NSDictionary *)dic
{
    userid = [NSString stringWithFormat:@"%@",[dic objectForKey:@"userid"]];
    
    orderamount = [NSString stringWithFormat:@"%@",[dic objectForKey:@"orderamount"]];
    
    ASIFormDataRequest *request = nil;
    request=[ASIFormDataRequest requestWithURL:[NSURL URLWithString:@"http://pay.game550.com/iapay.aspx?fun=getoid"]];
    [request addRequestHeader:@"Content-Type" value:@"application/json; encoding=utf-8"];
    [request addRequestHeader:@"Accept" value:@"application/json"];
    [request setRequestMethod:@"POST"];
    [request setDelegate:self];
    request.tag = 1;
    //配置代理为本类
    [request setTimeOutSeconds:30];
    //设置超时
    [request setDidFailSelector:@selector(responseFailed:)];
    [request setDidFinishSelector:@selector(responseComplete:)];
    [request startAsynchronous];//异步传输
}


//支付完成
- (void)payIADFinish
{
    NSMutableDictionary *dic1 = [[NSMutableDictionary alloc] init];
    [dic1 setValue:userid forKey:@"userid"];
    [dic1 setValue:orderamount forKey:@"orderamount"];
    [dic1 setValue:PrepayId forKey:@"PrepayId"];
    
    NSError *parseError = nil;
    NSData* jsonData = [NSJSONSerialization dataWithJSONObject:dic1 options:NSJSONWritingPrettyPrinted error:&parseError];
    NSMutableData *requestBody = [[NSMutableData alloc] initWithData:jsonData];
    
    
    
    ASIFormDataRequest *request = nil;
    request=[ASIFormDataRequest requestWithURL:[NSURL URLWithString:@"http://pay.game850.com/iapay.aspx?fun=recharge"]];
    [request addRequestHeader:@"Content-Type" value:@"application/json; encoding=utf-8"];
    [request addRequestHeader:@"Accept" value:@"application/json"];
    [request setRequestMethod:@"POST"];
    [request setPostBody:requestBody];
    [request setDelegate:self];
    request.tag = 2;
    //配置代理为本类
    [request setTimeOutSeconds:30];
    //设置超时
    [request setDidFailSelector:@selector(responseFailed:)];
    [request setDidFinishSelector:@selector(responseComplete:)];
    [request startAsynchronous];//异步传输
}




- (void)responseComplete:(ASIFormDataRequest *)request
{
    NSData *resultString = [request responseData];
    NSError* error;
    NSDictionary* json = [NSJSONSerialization
                          JSONObjectWithData:resultString
                          options:NSJSONReadingAllowFragments
                          error:&error];
    
    NSString *str = [json objectForKey:@"ret"];
    if ([str intValue] == 100) {
        if (request.tag == 1) {
            NSArray *arr = [json objectForKey:@"data"];
            NSDictionary *result_dic = [arr objectAtIndex:0];
            
            PrepayId = [NSString stringWithFormat:@"%@",[result_dic objectForKey:@"PrepayId"]];
//            userid = [NSString stringWithFormat:@"%@",[result_dic objectForKey:@"userid"]];
//            orderamount =[NSString stringWithFormat:@"%@",[result_dic objectForKey:@"orderamount"]];
            
            if ([orderamount intValue] == 6) {
                [self buy:10];
                buyType = 10;
            }else if ([orderamount intValue] == 12){
                [self buy:11];
                buyType = 11;
            }else if ([orderamount intValue] == 18){
                [self buy:12];
                buyType = 12;
            }else if ([orderamount intValue] == 25){
                [self buy:13];
                buyType = 13;
            }else if ([orderamount intValue] == 30){
                [self buy:14];
                buyType = 14;
            }
        }else if (request.tag == 2){
            
        }
        
       
    }else{
        NSLog(@"%@",[json objectForKey:@"desc"]);
    }
    
}

//请求失败的回调方法
- (void)responseFailed:(ASIFormDataRequest *)request
{
    NSLog(@"Get order failed!");
}

@end
