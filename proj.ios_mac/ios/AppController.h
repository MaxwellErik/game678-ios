#import <UIKit/UIKit.h>
#include "WXApi.h"

@class RootViewController;

@interface AppController : NSObject <UIApplicationDelegate, UIAlertViewDelegate, WXApiDelegate> {
    UIWindow *window;
}

@property(nonatomic, readonly) RootViewController* viewController;

@end

