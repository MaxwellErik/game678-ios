//
//  IOSAuditStateHandler
//
//  Created by maxwell on 17/01/04.
//
//

#import <Foundation/Foundation.h>

@interface IOSAuditStateHandler : NSObject

/// 获取单例
+ (id)sharedIOSAuditStateHandler;

- (void)sendVersionToServer;

@end
