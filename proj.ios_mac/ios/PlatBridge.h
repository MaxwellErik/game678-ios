#import <Foundation/Foundation.h>

enum
{
    ePayNull = 0,
    ePayAli,
    ePayWeiXin,
    ePayAppStore,
};

@interface PlatBridge : NSObject

@property (nonatomic, copy) NSString *payProductId;
@property Boolean isPayList;
@property int goodsIndex;

+ (NSString*)getImei;
+ (NSString*)getGuestId;
+ (NSString*)getMobileType;
+ (NSString*)getDiskId;
+ (NSString*)getMacAddr;
+ (NSString*)getScreenRes;
+ (NSString*)getOsVersion;
+ (NSString*)getChannelName;
+ (NSString*)getVersionName;
+ (void)pay:(NSDictionary *)dict;
+ (void)requestPayChannle:(NSDictionary *)dict;
+ (void)requestPayType:(NSDictionary *)dict;
+ (void)showWebView:(NSDictionary *)dict;
+ (void)openBrowser:(NSDictionary *)dict;
+ (void)back;
+ (void)deliverGoods:(NSDictionary *)goodsDict;
+ (void)saveEncodeInfo:(NSDictionary *)goodsDict;
+ (NSString*)getEncodeInfo;

+ (void)paySuccess:(NSDictionary *)goodsDict;
+ (void)checkPayFailed;  //检测是否有支付失败的情况
+ (void)deliverServer:(NSString *)dataStr goodsInfo:(NSDictionary *)goodsDict deliverServerUrl:(NSString*)deliverUrl;
+ (void)payFailed:(NSString*)errorMsg;
+ (void)showLoading;
+ (void)unshowLoading;
+ (NSString *)md5HexDigest:(NSString*)input;
+ (int)getPayType;
+ (void)setPayType:(int)type;
+ (void)setGamePayDetailUrl:(NSDictionary *)dict;
+ (void)setUpdateUrl:(NSDictionary *)dict;
+ (void)setGamePaySelectUrl:(NSDictionary*)dict;
+ (void)copyStrToShearPlate:(NSDictionary*)dict;
+ (void)callQQChat:(NSDictionary*)dict;
+ (NSString *)decryptUseDES:(NSData *)cipherData key:(NSString *)key;
+ (void)queryOrder;

@end


@interface PaySaveInfo : NSObject

@property (nonatomic, copy) NSString *appleStr;
@property (nonatomic, copy) NSDictionary *goodsInfo;

@end
