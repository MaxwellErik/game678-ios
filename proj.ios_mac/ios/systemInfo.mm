//
//  systemInfo.mm
//  game998
//
//  Created by maxwell on 16/9/2.
//
//
#import <Foundation/Foundation.h>
#import <SystemConfiguration/SystemConfiguration.h>

#include "localInfo.h"
#include "SystemInfo.h"

#include "RootViewController.h"
#import <AdSupport/ASIdentifierManager.h>
#import <AudioToolbox/AudioToolbox.h>
#import <CoreTelephony/CTTelephonyNetworkInfo.h>
#import <NetworkExtension/NetworkExtension.h>
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "sys/utsname.h"
#import <AssetsLibrary/AssetsLibrary.h>

#define TEST_URL @"www.baidu.com"

USING_NS_CC;

NetWorkType SystemInfo::getCurNetInfo()
{
    NetWorkType strNetworkType = NetWorkTypeNone;
    
    //创建零地址，0.0.0.0的地址表示查询本机的网络连接状态
    struct sockaddr_storage zeroAddress;
    
    bzero(&zeroAddress, sizeof(zeroAddress));
    zeroAddress.ss_len = sizeof(zeroAddress);
    zeroAddress.ss_family = AF_INET;
    
    // Recover reachability flags
    SCNetworkReachabilityRef defaultRouteReachability = SCNetworkReachabilityCreateWithAddress(NULL, (struct sockaddr *)&zeroAddress);
    SCNetworkReachabilityFlags flags;
    
    //获得连接的标志
    BOOL didRetrieveFlags = SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags);
    CFRelease(defaultRouteReachability);
    
    //如果不能获取连接标志，则不能连接网络，直接返回
    if (!didRetrieveFlags)
    {
        return strNetworkType;
    }
    
    
    if ((flags & kSCNetworkReachabilityFlagsConnectionRequired) == 0)
    {
        // if target host is reachable and no connection is required
        // then we'll assume (for now) that your on Wi-Fi
        strNetworkType = NetWorkTypeWiFI;
    }
    
    if (
        ((flags & kSCNetworkReachabilityFlagsConnectionOnDemand ) != 0) ||
        (flags & kSCNetworkReachabilityFlagsConnectionOnTraffic) != 0
        )
    {
        // ... and the connection is on-demand (or on-traffic) if the
        // calling application is using the CFSocketStream or higher APIs
        if ((flags & kSCNetworkReachabilityFlagsInterventionRequired) == 0)
        {
            // ... and no [user] intervention is needed
            strNetworkType = NetWorkTypeWiFI;
        }
    }
    
    if ((flags & kSCNetworkReachabilityFlagsIsWWAN) == kSCNetworkReachabilityFlagsIsWWAN)
    {
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)
        {
            CTTelephonyNetworkInfo * info = [[CTTelephonyNetworkInfo alloc] init];
            NSString *currentRadioAccessTechnology = info.currentRadioAccessTechnology;
            
            if (currentRadioAccessTechnology)
            {
                if ([currentRadioAccessTechnology isEqualToString:CTRadioAccessTechnologyLTE])
                {
                    strNetworkType =  NetWorkType4G;
                }
                else if ([currentRadioAccessTechnology isEqualToString:CTRadioAccessTechnologyEdge] || [currentRadioAccessTechnology isEqualToString:CTRadioAccessTechnologyGPRS])
                {
                    strNetworkType =  NetWorkType2G;
                }
                else
                {
                    strNetworkType =  NetWorkType3G;
                }
            }
        }
        else
        {
            if((flags & kSCNetworkReachabilityFlagsReachable) == kSCNetworkReachabilityFlagsReachable)
            {
                if ((flags & kSCNetworkReachabilityFlagsTransientConnection) == kSCNetworkReachabilityFlagsTransientConnection)
                {
                    if((flags & kSCNetworkReachabilityFlagsConnectionRequired) == kSCNetworkReachabilityFlagsConnectionRequired)
                    {
                        strNetworkType = NetWorkType2G;
                    }
                    else
                    {
                        strNetworkType = NetWorkType3G;
                    }
                }
            }
        }
    }
    
    return strNetworkType;
}

float SystemInfo::getCurPowerInfo()
{
    [[UIDevice currentDevice] setBatteryMonitoringEnabled:YES];
    return [[UIDevice currentDevice] batteryLevel];
}

void SystemInfo::openURL(const char * pszUrl)
{
    NSString  *urlText =  [NSString stringWithFormat:@"%s",pszUrl];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlText]];
}

std::string SystemInfo::getClientVersion()
{
    NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];

    NSString *app_Version = [infoDictionary objectForKey:@"CFBundleShortVersionString"];
    std::string res=  [app_Version UTF8String];
    return res;
}

std::string SystemInfo::getMobileKind()
{
    struct utsname systemInfo;
    
    uname(&systemInfo);
    
    NSString *platform = [NSString stringWithCString:systemInfo.machine encoding:NSASCIIStringEncoding];
    
    if ([platform isEqualToString:@"iPhone1,1"]) return "iPhone 2G";
    
    if ([platform isEqualToString:@"iPhone1,2"]) return "iPhone 3G";
    
    if ([platform isEqualToString:@"iPhone2,1"]) return "iPhone 3GS";
    
    if ([platform isEqualToString:@"iPhone3,1"]) return "iPhone 4";
    
    if ([platform isEqualToString:@"iPhone3,2"]) return "iPhone 4";
    
    if ([platform isEqualToString:@"iPhone3,3"]) return "iPhone 4";
    
    if ([platform isEqualToString:@"iPhone4,1"]) return "iPhone 4S";
    
    if ([platform isEqualToString:@"iPhone5,1"]) return "iPhone 5";
    
    if ([platform isEqualToString:@"iPhone5,2"]) return "iPhone 5";
    
    if ([platform isEqualToString:@"iPhone5,3"]) return "iPhone 5c";
    
    if ([platform isEqualToString:@"iPhone5,4"]) return "iPhone 5c";
    
    if ([platform isEqualToString:@"iPhone6,1"]) return "iPhone 5s";
    
    if ([platform isEqualToString:@"iPhone6,2"]) return "iPhone 5s";
    
    if ([platform isEqualToString:@"iPhone7,1"]) return "iPhone 6 Plus";
    
    if ([platform isEqualToString:@"iPhone7,2"]) return "iPhone 6";
    
    if ([platform isEqualToString:@"iPhone8,1"]) return "iPhone 6s";
    
    if ([platform isEqualToString:@"iPhone8,2"]) return "iPhone 6s Plus";
    
    if ([platform isEqualToString:@"iPhone8,4"]) return "iPhone SE";
    
    if ([platform isEqualToString:@"iPhone9,1"]) return "iPhone 7";
    
    if ([platform isEqualToString:@"iPhone9,2"]) return "iPhone 7 Plus";
    
    if ([platform isEqualToString:@"iPod1,1"])   return "iPod Touch 1G";
    
    if ([platform isEqualToString:@"iPod2,1"])   return "iPod Touch 2G";
    
    if ([platform isEqualToString:@"iPod3,1"])   return "iPod Touch 3G";
    
    if ([platform isEqualToString:@"iPod4,1"])   return "iPod Touch 4G";
    
    if ([platform isEqualToString:@"iPod5,1"])   return "iPod Touch 5G";
    
    if ([platform isEqualToString:@"iPad1,1"])   return "iPad 1G";
    
    if ([platform isEqualToString:@"iPad2,1"])   return "iPad 2";
    
    if ([platform isEqualToString:@"iPad2,2"])   return "iPad 2";
    
    if ([platform isEqualToString:@"iPad2,3"])   return "iPad 2";
    
    if ([platform isEqualToString:@"iPad2,4"])   return "iPad 2";
    
    if ([platform isEqualToString:@"iPad2,5"])   return "iPad Mini 1G";
    
    if ([platform isEqualToString:@"iPad2,6"])   return "iPad Mini 1G";
    
    if ([platform isEqualToString:@"iPad2,7"])   return "iPad Mini 1G";
    
    if ([platform isEqualToString:@"iPad3,1"])   return "iPad 3";
    
    if ([platform isEqualToString:@"iPad3,2"])   return "iPad 3";
    
    if ([platform isEqualToString:@"iPad3,3"])   return "iPad 3";
    
    if ([platform isEqualToString:@"iPad3,4"])   return "iPad 4";
    
    if ([platform isEqualToString:@"iPad3,5"])   return "iPad 4";
    
    if ([platform isEqualToString:@"iPad3,6"])   return "iPad 4";
    
    if ([platform isEqualToString:@"iPad4,1"])   return "iPad Air";
    
    if ([platform isEqualToString:@"iPad4,2"])   return "iPad Air";
    
    if ([platform isEqualToString:@"iPad4,3"])   return "iPad Air";
    
    if ([platform isEqualToString:@"iPad4,4"])   return "iPad Mini 2G";
    
    if ([platform isEqualToString:@"iPad4,5"])   return "iPad Mini 2G";
    
    if ([platform isEqualToString:@"iPad4,6"])   return "iPad Mini 2G";
    
    if ([platform isEqualToString:@"i386"])      return "iPhone Simulator";
    
    if ([platform isEqualToString:@"x86_64"])    return "iPhone Simulator";
    
    return "";
}

std::string SystemInfo::getMobileBrand()
{
    NSString *deviceName = [[UIDevice currentDevice] model];
    std::string res=  [deviceName UTF8String];
    return res;
}

std::string SystemInfo::getMobileSystemVersion()
{
    NSString *sysVersion = [[UIDevice currentDevice] systemVersion];
    std::string res=  [sysVersion UTF8String];
    return res;
}
