#import "VWebView.h"

@interface VWebView()
{
    BOOL isLoadingFinished;
    UIWebView * _webView;
}

@end


@implementation VWebView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        _webView = [[[UIWebView alloc] init] autorelease];
        _webView.layer.anchorPoint = CGPointMake(0.0, 0.0);
        _webView.delegate = self;
        _webView.frame = frame;
        _webView.center = CGPointMake(0, 0);
        _webView.backgroundColor = [UIColor whiteColor];
        [_webView.layer setMasksToBounds:YES];
        [self addSubview:_webView];
        
        isLoadingFinished = NO;
        [_webView setScalesPageToFit:YES];
        
        UIButton * close = [TQTools buttonWithImage:@"backbtn.png" Target:self Select:@selector(close:) Anchor:CGPointMake(0.0, 0.0) Position:CGPointMake(0, 0)];
        [self addSubview:close];
        
    }
    return self;
}

- (void)setUrl:(NSString*)url
{
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:url] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:10.0];
    [_webView loadRequest:request];
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    return YES;
}

- (void)webViewDidStartLoad:(UIWebView *)webView
{
    NSLog(@"webViewDidStartLoad");
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    NSLog(@"webViewDidFinishLoad");
    //网页宽度自适应
    NSString *meta = [NSString stringWithFormat:@"document.getElementsByName(\"viewport\")[0].content = \"width=%f, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no\"", _webView.frame.size.width];
    [_webView stringByEvaluatingJavaScriptFromString:meta];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    NSLog(@"didFailLoadWithError：%@", error);
}


@end
