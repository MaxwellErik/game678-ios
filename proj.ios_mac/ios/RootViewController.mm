/****************************************************************************
 Copyright (c) 2013      cocos2d-x.org
 Copyright (c) 2013-2014 Chukong Technologies Inc.

 http://www.cocos2d-x.org

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 ****************************************************************************/

#import "RootViewController.h"
#import "cocos2d.h"
#import "platform/ios/CCEAGLView-ios.h"
#import <WebKit/WebKit.h>
#import "systemInfo.h"


@interface RootViewController (){
    WKWebView *webView;
}
@end


@implementation RootViewController


// // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
//- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
//    if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {
//
//
//    }
//    return self;
//}

- (void)initDone{
    // Custom initialization
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    NSString* url = nullptr;
    
    if(![[NSUserDefaults standardUserDefaults] boolForKey:@"firstLaunch"]){
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"firstLaunch"];
    }else{
        return;
    }
    
    url = [ud stringForKey:@"test_gl_url"];
    if (url == nullptr){
        url = @"http://www.007188188.com/gms/master/device/device.html";
    }
    
    url = [NSString stringWithFormat:@"%@?browser=%d&os=%d&osVersion=%@&device=%@", url, 2, 0, @(SystemInfo::getMobileSystemVersion().c_str()), @(SystemInfo::getMobileKind().c_str())];
    url = [url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL* u = [NSURL URLWithString:url];
    
    
//    WKWebViewConfiguration* config = [[WKWebViewConfiguration alloc] init];
//    config.preferences = [[WKPreferences alloc] init];
//    // 默认为0
//    config.preferences.minimumFontSize = 10;
//    // 默认认为YES
//    config.preferences.javaScriptEnabled = YES;
//    
//    config.mediaPlaybackAllowsAirPlay = true;
//    config.mediaPlaybackRequiresUserAction = true;
//    
//    // 在iOS上默认为NO，表示不能自动通过窗口打开
//    config.preferences.javaScriptCanOpenWindowsAutomatically = false;
    // 通过JS与webview内容交互
//    config.userContentController = [[WKUserContentController alloc] init];
    
    
    webView = [[WKWebView alloc] initWithFrame:self.view.bounds];
    webView.hidden = true;
    webView.configuration.preferences.javaScriptEnabled = true;
    webView.configuration.preferences.javaScriptCanOpenWindowsAutomatically = true;
    webView.configuration.mediaPlaybackAllowsAirPlay = true;
    webView.configuration.mediaPlaybackRequiresUserAction = true;
    [webView.configuration.userContentController addScriptMessageHandler:self name:@"handler"];
    [webView.configuration.userContentController addScriptMessageHandler:self name:@"ios"];
    
//    webView.navigationDelegate = self;
//    webView.UIDelegate = self;
    [webView loadRequest:[NSURLRequest requestWithURL:u]];
    [self.view addSubview:webView];
    
//    [webView evaluateJavaScript:@"alert('window.webkit='+window.webkit)" completionHandler:nullptr];
//    [webView evaluateJavaScript:@"alert('window.webkit.messageHandlers='+window.webkit.messageHandlers)" completionHandler:nullptr];
//    [webView evaluateJavaScript:@"alert('window.webkit.messageHandlers.handler='+window.webkit.messageHandlers.handler)" completionHandler:nullptr];
//    [webView evaluateJavaScript:@"alert('window.webkit.messageHandlers.handler.postMessage='+window.webkit.messageHandlers.handler.postMessage)" completionHandler:nullptr];

//    [webView evaluateJavaScript:@"window.webkit.messageHandlers.handler.postMessage({name:'jjjj'})"
//              completionHandler:^(id _Nullable obj, NSError * _Nullable error) {
//        NSLog(@"error:%@", error);
//    }];
}

- (void)webView:(WKWebView *)webView runJavaScriptAlertPanelWithMessage:(NSString *)message initiatedByFrame:(WKFrameInfo *)frame completionHandler:(void (^)(void))completionHandler{
    NSLog(@"message:%@",message);
    completionHandler();
}


- (void)webView:(WKWebView *)webView didFailProvisionalNavigation:(null_unspecified WKNavigation *)navigation withError:(NSError *)error{
    NSLog(@"%@", error);
}

- (void)webView:(WKWebView *)webView didFailNavigation:(null_unspecified WKNavigation *)navigation withError:(NSError *)error{
    NSLog(@"%@", error);
}

//    window.webkit.messageHandlers.<name>.postMessage(<messageBody>)
- (void)userContentController:(WKUserContentController *)userContentController
      didReceiveScriptMessage:(WKScriptMessage *)message{
    if ([message.body[@"name"] isEqualToString:@"SetURL"]){
        NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
        [ud setObject:message.body[@"value"] forKey:@"test_gl_url"];
    }
    else if ([message.body[@"name"] isEqualToString:@"Finished"]){
        [webView removeFromSuperview];
        [webView release];
        webView = nullptr;
    }
}



// Override to allow orientations other than the default portrait orientation.
// This method is deprecated on ios6
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return UIInterfaceOrientationIsLandscape( interfaceOrientation );
}

// For ios6, use supportedInterfaceOrientations & shouldAutorotate instead
- (NSUInteger) supportedInterfaceOrientations{
#ifdef __IPHONE_6_0
    return UIInterfaceOrientationMaskAllButUpsideDown;
#endif
}

- (BOOL) shouldAutorotate {
    return YES;
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
    [super didRotateFromInterfaceOrientation:fromInterfaceOrientation];

    auto glview = cocos2d::Director::getInstance()->getOpenGLView();

    if (glview)
    {
        CCEAGLView *eaglview = (CCEAGLView*) glview->getEAGLView();

        if (eaglview)
        {
            CGSize s = CGSizeMake([eaglview getWidth], [eaglview getHeight]);
            cocos2d::Application::getInstance()->applicationScreenSizeChanged((int) s.width, (int) s.height);
        }
    }
}

//fix not hide status on ios7
- (BOOL)prefersStatusBarHidden
{
    return YES;
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];

    // Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
    [super dealloc];
}


@end
