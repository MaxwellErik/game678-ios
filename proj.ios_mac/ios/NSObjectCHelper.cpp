#include "NSObjectCHelper.h"

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
#include "CCPlatformDefine-ios.h"
#endif

#include "JniSink.h"
//#import <UIKit/UIKit.h>

NSObjectCHelper *NSObjectCHelper::m_pNSObjectCHelper = NULL;

NSObjectCHelper * NSObjectCHelper::sharedNSObjectCHelper()
{
	if (m_pNSObjectCHelper == NULL)
	{
		m_pNSObjectCHelper = new NSObjectCHelper;
	}
	return m_pNSObjectCHelper;
}

void NSObjectCHelper::callObjectCCommand( const int command, const char *szParam )
{
	switch(command)
	{
        case CMD_CTJ_SYSTEM_BROWSE:
        {
//            [[UIApplication sharedApplication]] openURL:[NSURL URLWithString:@"www.apple.com"]];
            break;
        }
	}
}

void NSObjectCHelper::callCppCommand( const int command, const char *szParam )
{
	JniSink::share()->javaCallback(command, szParam);
}
