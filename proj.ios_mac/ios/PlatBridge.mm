#import "PlatBridge.h"
#import <sys/utsname.h>
#import <CommonCrypto/CommonDigest.h>
#import "TQAlertView.h"
//#include "md5.h"
#import "VWebView.h"
#import "Base64.h"
#import <CommonCrypto/CommonCryptor.h>
#include <iostream>
using namespace std;
#include "platform/ios/CCLuaObjcBridge.h"
using namespace cocos2d;
#import "AppController.h"

int s_backFuncID = -1;
int payQueryOrderTimes = 0;
static int s_payType = ePayNull;
static int s_deliverCount = 0;
std::string md5(const std::string &str);
string to_md5(const char* buf,size_t size)
{
    char pwdout[33]={0};
    std::string newMdt = md5(buf);
    memcpy(pwdout,newMdt.c_str(),32);
    return pwdout;
}

@implementation PaySaveInfo

@synthesize appleStr = _appleStr;
@synthesize goodsInfo = _goodsInfo;

@end


@implementation PlatBridge

@synthesize payProductId = _payProductId;


+ (NSString*)getImei
{
    NSString *identifierForVendor = [[UIDevice currentDevice].identifierForVendor UUIDString];
    return identifierForVendor;
}

+ (NSString*)getGuestId
{
    return @"000";
}

+ (NSString*)getMobileType
{
    struct utsname systemInfo;
    uname(&systemInfo);
    NSString *deviceString = [NSString stringWithCString:systemInfo.machine encoding:NSUTF8StringEncoding];
    
    return deviceString;
}

+ (NSString*)getDiskId
{
    NSString *identifierForVendor = [[UIDevice currentDevice].identifierForVendor UUIDString];
    return identifierForVendor;
    
}

+ (NSString*)getMacAddr
{
    NSString *identifierForVendor = [[UIDevice currentDevice].identifierForVendor UUIDString];
    return identifierForVendor;
    
}

+ (NSString*)getScreenRes
{
    NSString *str1 = [NSString stringWithFormat:@"%fX%f", ScreenRect.size.width, ScreenRect.size.height];
    return str1;
}

+ (NSString*)getOsVersion
{
    return [self getMobileType];
}

+ (NSString*)getChannelName
{
    return @"ios_gc";
}

+ (NSString*)getVersionName
{
    return [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
}

+ (void)pay:(NSDictionary *)dict
{
    int goodsId = [[dict objectForKey:@"goodsId"] intValue];
    int money = [[dict objectForKey:@"money"] intValue];
    NSString *goodsName = [dict objectForKey:@"goodsName"];
    int serverId = [[dict objectForKey:@"serverId"] intValue];
    s_backFuncID = [[dict objectForKey:@"backFunc"] intValue];
    
    NSLog(@"道具购买请求 goodsid:%d  goodsName:%@  money:%d  serverId:%d", goodsId, goodsName, money, serverId);
    
    [PlatBridge showLoading];
    NSThread *thread = [[NSThread alloc]initWithTarget:self selector:@selector(requestPayChannle:) object:dict];
    [thread start];
}

+ (void)openBrowser:(NSDictionary *)dict
{
    NSString *url = [dict objectForKey:@"strUrl"];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
}

+(NSString *)decryptUseDES:(NSData *)cipherData key:(NSString *)key
{
//    NSString *cipherText = [[NSString alloc] initWithData:cipherData encoding:NSUTF8StringEncoding];
//    
//    NSString *plaintext = nil;
//    
//    NSData *cipherdata = [Base64 decode:cipherText];
//    int bufferSize = 1024*64;
//    unsigned char *buffer = new unsigned char[bufferSize];
//    memset(buffer, 0, sizeof(char)*bufferSize);
//    size_t numBytesDecrypted = 0;
//    const Byte iv[] = {1,2,3,4,5,6,7,8};
//    
//    CCCryptorStatus cryptStatus = CCCrypt(kCCDecrypt, kCCAlgorithmDES,
//                                          kCCOptionPKCS7Padding,
//                                          [key UTF8String], kCCKeySizeDES,
//                                          iv,
//                                          [cipherdata bytes], [cipherdata length],
//                                          buffer, bufferSize,
//                                          &numBytesDecrypted);
//    if(cryptStatus == kCCSuccess) {
//        NSData *plaindata = [NSData dataWithBytes:buffer length:(NSUInteger)numBytesDecrypted];
//        plaintext = [[NSString alloc]initWithData:plaindata encoding:NSUTF8StringEncoding];
//    }
    return @"";
}

+ (void)requestPayChannle:(NSDictionary *)dict
{
    NSString *payUrl = [dict objectForKey:@"payDetailUrl"];
    if (payUrl)
    {
        NSURL *url = [NSURL URLWithString:payUrl];
        NSURLRequest *request = [NSURLRequest requestWithURL:url cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:10.0];
        
        NSURLResponse * response = nil;
        NSError * error = nil;
        
        // 提交验证请求，并获得官方的验证JSON结果
        NSData *result = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        [PlatBridge unshowLoading];
        if (error != nil || result == nil) {   //网络异常, 直接调用appstore支付
            [PlatBridge payFailed:@"获取配置信息失败，请稍后重试"];
            return;
        }
        
        NSString *decodeStr = [PlatBridge decryptUseDES:result key:@"20151201"];
        if (decodeStr==nil) {
            [PlatBridge payFailed:@"获取配置信息失败，请稍后重试"];
            return;
        }
        
        NSError *err = nil;
        NSDictionary *payDic = [NSJSONSerialization JSONObjectWithData:[decodeStr dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableLeaves error:&err];
        if(err == nil)
        {
            [PlatBridge showLoading];
            [dict setValue:payDic forKey:@"payChannleInfoDic"];
            NSThread *thread = [[NSThread alloc]initWithTarget:self selector:@selector(requestPayType:) object:dict];
            [thread start];
            return;
        }
        else
        {
            [PlatBridge payFailed:@"解析配置信息失败，请稍后重试"];
            return;
        }
    }
    else
    {
        [PlatBridge payFailed:@"获取配置信息失败，请稍后重试"];
    }
}

+(void)requestPayType:(NSDictionary *)dict
{
 }

//苹果验证，然后发货
+ (void)deliverGoods:(NSDictionary *)goodsDict
{
    NSURL *encodeData = [goodsDict objectForKey:@"appleData"];
    NSData *receiptData = [NSData dataWithContentsOfURL:encodeData];
    
    NSString *encodeStr = [receiptData base64EncodedStringWithOptions:NSDataBase64EncodingEndLineWithLineFeed];
    int money = [[goodsDict objectForKey:@"money"] intValue];
    int serverId = [[goodsDict objectForKey:@"serverId"] intValue];
    int userId = [[goodsDict objectForKey:@"userId"] intValue];
    
    NSDictionary *dicPay = [goodsDict objectForKey:@"payChannleInfoDic"];
    NSDictionary *dicApp = [dicPay objectForKey:@"appstore"];
    NSDictionary *dicapplepay = [dicApp objectForKey:@"applepay"];
    NSString *gameKey = [dicapplepay objectForKey:@"gameKey"];
    NSString *deliverUrl = [dicapplepay objectForKey:@"deliverUrl"];
    
    
    NSLog(@"%@", [encodeData absoluteString]);
    
    NSString *channleId = @"ios_gc";
    NSString *payType = @"appstore";
    
    NSString *strTmp = [NSString stringWithFormat:@"pay%d%d%@%@%@", userId, money, channleId, payType, gameKey];
    NSString *strMD5 = [PlatBridge md5HexDigest:strTmp];
    
    NSString *encodedString = (NSString *)
    CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault,
                                            (CFStringRef)encodeStr,
                                            NULL,
                                            (CFStringRef)@"!$&'()*+,-./:;=?@_~%#[]",
                                            kCFStringEncodingUTF8);
    //组装url数据
    NSString *postData = [NSString stringWithFormat:@"action=pay&GamePlatform=jiujiu&userID=%d&amount=%d&channel=%@&payType=%@&serverID=%d&test=%d&sign=%@&appleSign=%@",
                          userId, money, channleId, payType, serverId, 0, strMD5,encodedString];
    
    s_deliverCount = 0;
    [PlatBridge deliverServer:postData goodsInfo:goodsDict deliverServerUrl:deliverUrl];
}

+ (void)deliverServer:(NSString *)dataStr goodsInfo:(NSDictionary *)goodsDict deliverServerUrl:(NSString*)deliverUrl
{
    s_deliverCount++;
    
    NSLog(@"----%@", dataStr);
    
    NSData *payloadData = [dataStr dataUsingEncoding:NSASCIIStringEncoding];
    
    // 发送网络POST请求，对购买凭据进行验证
    NSURL *url = [NSURL URLWithString:deliverUrl];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:20.0f];
    
    request.HTTPMethod = @"POST";
    request.HTTPBody = payloadData;
    
    // 提交验证请求，并获得官方的验证JSON结果
    NSData *result = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
    [PlatBridge unshowLoading];
    
    NSLog(@"----------------------%@", result);
    
    if (result == nil) {   //网络异常,存储信息，下次登录成功重试
        [PlatBridge saveEncodeInfo:goodsDict];
        return;
    }
    
    if ([result isEqual:@"1"])
    {
        NSLog(@"签名错误");
        return;
    }
    
    if ([result isEqual:@"2"])
    {
        NSLog(@"充值错误");
        return;
    }
    
    [[AppController getAppController] setOrderId:[[NSString alloc] initWithData:result encoding:NSUTF8StringEncoding]];
    
    [PlatBridge paySuccess:nil];
}

+ (void)saveEncodeInfo:(NSDictionary *)goodsDict
{
    NSMutableData *dicData = [[NSMutableData alloc] init];
    NSKeyedArchiver *archiver = [[NSKeyedArchiver alloc] initForWritingWithMutableData:dicData];
    [archiver encodeObject:goodsDict forKey:@"PayFailed"];
    [archiver finishEncoding];
    
    NSString *sandboxPath = NSHomeDirectory();
    NSString *documentPath = [sandboxPath stringByAppendingPathComponent:@"Documents"];
    NSString *fileName=[documentPath stringByAppendingPathComponent:@"spayFaild.bat"];
    [dicData writeToFile:fileName atomically:YES];
}

+ (NSString*)getEncodeInfo
{
    NSUserDefaults *userDefaultes = [NSUserDefaults standardUserDefaults];
    //读取NSString类型的数据
    return [userDefaultes objectForKey:@"APPSTOREPAYFAILEDS"];}

+ (void)checkPayFailed
{
    NSString *sandboxPath = NSHomeDirectory();
    NSString *documentPath = [sandboxPath stringByAppendingPathComponent:@"Documents"];
    NSString *fileName=[documentPath stringByAppendingPathComponent:@"spayFaild.bat"];
    
    NSFileManager *fileMgr = [NSFileManager defaultManager];
    BOOL bRet = [fileMgr fileExistsAtPath:fileName];
    if (bRet) {
        NSData *data = [NSData dataWithContentsOfFile:fileName];
        NSKeyedUnarchiver *unarchiver = [[NSKeyedUnarchiver alloc] initForReadingWithData:data];
        NSDictionary *dict = [unarchiver decodeObjectForKey:@"PayFailed"];
        [unarchiver finishDecoding];
        
        NSError *err;
        [fileMgr removeItemAtPath:fileName error:&err];
        
        [PlatBridge deliverGoods:dict];
    }
}

+ (void)queryOrder
{
    payQueryOrderTimes = payQueryOrderTimes + 1;
    [PlatBridge showLoading];
    //验证订单号是否正确
    NSString *gameKey = [[AppController getAppController] getGameKey];
    NSString *deliverUrl = [[AppController getAppController] getQueryOrderUrl];
    NSString *orderId = [[AppController getAppController] getOrderId];
    
    NSString *strTmp = [NSString stringWithFormat:@"QueryOrder%@%@", orderId, gameKey];
    NSString *strMD5 = [PlatBridge md5HexDigest:strTmp];
    //组装url数据
    NSString *postData = [NSString stringWithFormat:@"action=QueryOrder&order_id=%@&sign=%@",
                          orderId, strMD5];
    
    NSLog(@"验证订单号：%@", postData);
    
    NSData *payloadData = [postData dataUsingEncoding:NSUTF8StringEncoding];
    
    // 发送网络POST请求，对购买凭据进行验证
    NSURL *url = [NSURL URLWithString:deliverUrl];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:10.0f];
    
    request.HTTPMethod = @"POST";
    request.HTTPBody = payloadData;
    
    // 提交验证请求，并获得官方的验证JSON结果
    NSData *result = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
    
    if (result == nil) {
        if (payQueryOrderTimes <=3)
        {
            [NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(queryOrder) userInfo:nil repeats:NO];
            return;
        }
        
        [PlatBridge unshowLoading];
        UIAlertView *alter = [[UIAlertView alloc] initWithTitle:@"提示" message:@"充值失败，请联系客服" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alter show];
        [alter release];
        
        return;
    }
    
    NSString *strRet = [[NSString alloc] initWithData:result encoding:NSUTF8StringEncoding];
    NSLog(@"----:%@", strRet);
    if ([strRet isEqualToString:@"true"] != true)
    {
        if (payQueryOrderTimes <=3)
        {
            [NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(queryOrder) userInfo:nil repeats:NO];
            return;
        }
        
        [PlatBridge unshowLoading];
        UIAlertView *alter = [[UIAlertView alloc] initWithTitle:@"提示" message:@"充值失败，请联系客服" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alter show];
        [alter release];
        
        return;
    }
    
    [PlatBridge unshowLoading];

    int backFuncId = s_backFuncID;
    // 1. 将引用 ID 对应的 Lua function 放入 Lua stack
    LuaObjcBridge::pushLuaFunctionById(backFuncId);
    // 2. 将需要传递给 Lua function 的参数放入 Lua stack
    LuaValueDict item;
    item["success"] = LuaValue::booleanValue(TRUE);
    LuaObjcBridge::getStack()->pushLuaValueDict(item);
    // 3. 执行 Lua function
    LuaObjcBridge::getStack()->executeFunction(1);
    // 4. 释放引用 ID
    LuaObjcBridge::releaseLuaFunctionById(backFuncId);
}

+ (void)paySuccess:(NSDictionary *)goodsDict
{
    payQueryOrderTimes = 0;
    [PlatBridge showLoading];
    [NSTimer scheduledTimerWithTimeInterval:2 target:self selector:@selector(queryOrder) userInfo:nil repeats:NO];
}

+ (void)payFailed:(NSString*)errorMsg
{
    [PlatBridge unshowLoading];
    if (errorMsg!= nil)
    {
        UIAlertView *alter = [[UIAlertView alloc] initWithTitle:@"提示" message:errorMsg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alter show];
        [alter release];
    }
}

+ (void) showLoading
{
    UIWindow *window = [[UIApplication sharedApplication]keyWindow];
    UIActivityIndicatorView *indicator = nil;
    indicator = (UIActivityIndicatorView *)[window viewWithTag:103];
    
    if (indicator == nil) {
        
        //初始化:
        UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0, 0, ScreenRect.size.width, ScreenRect.size.height)];
        
        indicator.tag = 103;
        
        //设置显示样式,见UIActivityIndicatorViewStyle的定义
        indicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
        
        
        //设置背景色
        indicator.backgroundColor = [UIColor blackColor];
        
        //设置背景透明
        indicator.alpha = 0.5;
        
        //设置背景为圆角矩形
        indicator.layer.cornerRadius = 6;
        indicator.layer.masksToBounds = YES;
        //设置显示位置
        [indicator setCenter:CGPointMake(ScreenRect.size.width / 2.0, ScreenRect.size.height / 2.0)];
        
        //开始显示Loading动画
        [indicator startAnimating];
        
        [window addSubview:indicator];
        
        [indicator release];
    }
    
    //开始显示Loading动画
    [indicator startAnimating];
}

+ (void)unshowLoading
{
    UIWindow *window = [[UIApplication sharedApplication]keyWindow];
    UIActivityIndicatorView *indicator = nil;
    indicator = (UIActivityIndicatorView *)[window viewWithTag:103];
    if (indicator != nil) {
        [indicator stopAnimating];
        [indicator removeFromSuperview];
    }
}

+ (NSString *)md5HexDigest:(NSString*)input
{
    const char* str = [input UTF8String];
    
    string retStr = to_md5(str, strlen(str));
    NSString *ret = [NSString stringWithCString:retStr.c_str() encoding:NSUTF8StringEncoding];
    return ret;
}

+ (int)getPayType
{
    return s_payType;
}

+ (void)setPayType:(int)type
{
    s_payType = type;
}

+ (void)setGamePayDetailUrl:(NSDictionary *)dict
{
}

+ (void)setUpdateUrl:(NSDictionary *)dict
{
    NSString * url = [dict objectForKey:@"strUrl"];
    if (url)
        [[AppController getAppController] checkUpdate:url];
}

+ (void)setGamePaySelectUrl:(NSDictionary*)dict
{
}

+ (void)copyStrToShearPlate:(NSDictionary*)dict
{
    NSString * str = [dict objectForKey:@"str"];
    if (str)
    {
        UIPasteboard *pboard = [UIPasteboard generalPasteboard]; 
        pboard.string = @"";
    }
}

+ (void)callQQChat:(NSDictionary*)dict
{
    NSString * qqNum = [dict objectForKey:@"qqNum"];
    if (qqNum)
    {
        UIWebView *webView = [[UIWebView alloc] initWithFrame:CGRectZero];
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"mqq://im/chat?chat_type=wpa&uin=%@&version=1&src_type=web", qqNum]];
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        webView.delegate = self;
        [webView loadRequest:request];
        UIWindow *window = [[UIApplication sharedApplication]keyWindow];
        [window addSubview:webView];
    }
}

+ (void)showWebView:(NSDictionary *)dict
{
    NSString *strUrl = [dict objectForKey:@"strUrl"];
    if (strUrl)
    {
        UIWindow *window = [[UIApplication sharedApplication]keyWindow];
        UIWebView *webView = [[UIWebView alloc] initWithFrame:window.bounds];
        NSURL *url = [NSURL URLWithString:strUrl];
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        webView.delegate = self;
        [webView loadRequest:request];
        
        UIView *bview = [[UIView alloc] initWithFrame:window.bounds];
        [bview addSubview:webView];
        UIButton *btnReturn = [UIButton buttonWithType:UIButtonTypeSystem];
        [btnReturn setTitle:@"返回" forState:UIControlStateNormal];
        btnReturn.frame = CGRectMake(10, 10, 60, 40);
        [btnReturn addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
        [bview addSubview:btnReturn];
        
        bview.tag = 10001;
        
        
        [window addSubview:bview];
    }
}

+ (void)back
{
    NSLog(@"HelloWorld");
    UIWindow *window = [[UIApplication sharedApplication]keyWindow];
    
    [[window viewWithTag:10001] removeFromSuperview];
}

@end
