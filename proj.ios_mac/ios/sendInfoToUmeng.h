//
//  sendInfoToUmeng.h
//  game998
//
//  Created by maxwell on 17/1/12.
//
//

#ifndef sendInfoToUmeng_h
#define sendInfoToUmeng_h

class SendInfoToUmeng
{
public:
    static void sendRegisterInfoToUmeng(int userId);
    static void sendLoginInfoToUmeng(int userId);
    static void sendRechargeInfoToUmeng(int platform, int amount);
};

#endif /* sendInfoToUmeng_h */
