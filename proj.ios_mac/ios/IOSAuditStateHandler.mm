//
//  IOSAuditStateHandler.m
//
//  Created by maxlwell on 17/01/04.
//
//

#import "IOSAuditStateHandler.h"
#import "GoodsByWeChatPay.h"

@interface IOSAuditStateHandler()
@end


@implementation IOSAuditStateHandler

/// 获取单例
+ (id)sharedIOSAuditStateHandler
{
    static IOSAuditStateHandler *_handler = nil;
    static dispatch_once_t once;
    dispatch_once(&once, ^{
        _handler = [[self alloc] init];
    });
    return _handler;
}

- (void)sendVersionToServer
{
    // 1.将网址初始化成一个OC字符串对象
    NSString *urlStr = [NSString stringWithFormat:@"https://pay.game678.com.cn/apple_version/apple_version.php"];
    // 如果网址中存在中文,进行URLEncode
    NSString *newUrlStr = [urlStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    // 2.构建网络URL对象, NSURL
    NSURL *url = [NSURL URLWithString:newUrlStr];
    // 3.创建网络请求
    NSURLRequest *request = [NSURLRequest requestWithURL:url cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:10];
    // 创建同步链接
    NSURLResponse *response = nil;
    NSError *error = nil;
    NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];

    //获取当前时间
    NSTimeInterval time=[[NSDate date] timeIntervalSince1970];
    long long int currentTime=(long long int)time;
    long long int endTime = 1485136800;//北京时间2017.01.23 09：00：00

    if (data == nil)
    {
        if (currentTime <= endTime)
        {
            GoodsByWeChatPay_cpp::shared()->GetIOSAuditState(0, 0);
        }
        else
        {
            GoodsByWeChatPay_cpp::shared()->GetIOSAuditState(1, 0);
        }
        return;
    }
    
    NSDictionary * dict = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    if (dict != nil)
    {
        NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];
        NSString *app_Version = [infoDictionary objectForKey:@"CFBundleShortVersionString"];
        
        if ([[dict objectForKey:@"v"] isEqualToString:app_Version]) {
             GoodsByWeChatPay_cpp::shared()->GetIOSAuditState(0, [dict[@"p"]intValue]);
        }
        else
        {
             GoodsByWeChatPay_cpp::shared()->GetIOSAuditState(1, 0);
        }
    }
    else
    {
        
        if (currentTime <= endTime)
        {
            GoodsByWeChatPay_cpp::shared()->GetIOSAuditState(0, 0);
        }
        else
        {
            GoodsByWeChatPay_cpp::shared()->GetIOSAuditState(1, 0);
        }
    }
}
@end
