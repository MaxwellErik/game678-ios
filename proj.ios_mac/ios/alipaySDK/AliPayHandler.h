//
//  AliPayHandler.h
//
//  Created by lbx on 16/6/2.
//
//

#import <Foundation/Foundation.h>

@interface AliPayHandler : NSObject

/// 获取单例 lbxlbx
+ (id)sharedAlipayHandler;

/// 生成预支付订单
- (void)createOrderIdWithUserId:(int)userId gameId:(int)gameId price:(int)price;

@end
