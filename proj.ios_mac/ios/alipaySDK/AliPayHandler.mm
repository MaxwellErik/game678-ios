//
//  AliPayHandler.m
//
//  Created by lbx on 16/6/2.
//
//

#import "AliPayHandler.h"
#import <AlipaySDK/AlipaySDK.h>
#import "Order.h"

@interface AliPayHandler()
@property (retain, nonatomic)NSString *currentOrder;
@property (assign, nonatomic)int payPrice;
@end


@implementation AliPayHandler

/// 获取单例 lbxlbx
+ (id)sharedAlipayHandler
{
    static AliPayHandler *_handler = nil;
    static dispatch_once_t once;
    dispatch_once(&once, ^{
        _handler = [[self alloc] init];
    });
    return _handler;
}

/// 生成预支付订单
- (void)createOrderIdWithUserId:(int)userId gameId:(int)gameId price:(int)price
{
    // 1.将网址初始化成一个OC字符串对象
    NSString *urlStr = [NSString stringWithFormat:@"https://pay.game678.com.cn/AliAppPayCreateOrder.ashx?userid=%d&gameid=%d&total_amount=%d&subject=678游戏%d元充值",userId, gameId, price, price];
    // 如果网址中存在中文,进行URLEncode
    NSString *newUrlStr = [urlStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    // 2.构建网络URL对象, NSURL
    NSURL *url = [NSURL URLWithString:newUrlStr];
    // 3.创建网络请求
    NSURLRequest *request = [NSURLRequest requestWithURL:url cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:10];
    // 创建同步链接
    NSURLResponse *response = nil;
    NSError *error = nil;
    NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    
    if (data == nil)
    {
        UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"支付请求发送失败，请联系客服！" message:nil delegate:nil cancelButtonTitle:@"取消" otherButtonTitles:nil];
        [alert show];
        [alert release];

        return;
    }
    
    NSDictionary * dict = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    if ([dict[@"ret"]intValue] == 0) {
        _currentOrder = dict[@"data"];
        _payPrice = price;
        [self payAlipay];
    }else
    {
        NSLog(@"获取订单失败");
        UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"获取订单失败" message:nil delegate:nil cancelButtonTitle:@"取消" otherButtonTitles:nil];
        [alert show];
        [alert release];
    }
}


- (void)payAlipay
{
    if (_currentOrder.length <= 0 || [_currentOrder isEqualToString:@"(null)"]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示"
                                                        message:@"订单获取失败"
                                                       delegate:self
                                              cancelButtonTitle:@"确定"
                                              otherButtonTitles:nil];
        [alert show];
        [alert release];
        return;
    }

    NSString *orderString = _currentOrder;

    NSString *appScheme = @"game678alipay";
    
    [[AlipaySDK defaultService] payOrder:orderString fromScheme:appScheme callback:^(NSDictionary *resultDic) {
        NSLog(@"reslut = %@",resultDic);
    }];

}
@end
