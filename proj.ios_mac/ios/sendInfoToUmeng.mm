//
//  sendInfoToUmeng.mm
//  game998
//
//  Created by maxwell on 17/1/12.
//
//

#include "GlobalUnits.h"
#include "sendInfoToUmeng.h"
#import "MobClickInOne/MobClick.h"
#import "MobClickInOne/DplusMobClick.h"


void SendInfoToUmeng::sendRegisterInfoToUmeng(int userId)
{
    // 注册，玩家注册的ID。
    
    NSString* ID = [NSString stringWithFormat:@"%d",userId];
    [MobClick event:@"__register" attributes:@{@"userid":ID}];

}

void SendInfoToUmeng::sendLoginInfoToUmeng(int userId)
{
    // 登录，玩家的登录ID。
    
    NSString* ID = [NSString stringWithFormat:@"%d",userId];
    [MobClick event:@"__login" attributes:@{@"userid":ID}];
    
}

void SendInfoToUmeng::sendRechargeInfoToUmeng(int platform, int amount)
{
    NSString* ID = [NSString stringWithFormat:@"%d", g_GlobalUnits.GetGolbalUserData().dwUserID];
    NSString* am =  [NSString stringWithFormat:@"%d", amount];
    if (0 == platform)
        [MobClick event:@"__finish_payment" attributes:@{@"userid":ID,@"orderid":@"0",@"item":@"微信充值",@"amount":am}];
    else
        [MobClick event:@"__finish_payment" attributes:@{@"userid":ID,@"orderid":@"0",@"item":@"支付宝充值",@"amount":am}];
                                                         
}
