//
//  systemInfo.h
//  game998
//
//  Created by maxwell on 16/9/2.
//
//

#ifndef SystemInfo_h
#define SystemInfo_h

#include <stdio.h>
#include "localInfo.h"
class SystemInfo
{
public:
    static NetWorkType getCurNetInfo();
    static float getCurPowerInfo();
    static void openURL(const char * pszUrl);
    static std::string getClientVersion();
    static std::string getMobileBrand();
    static std::string getMobileKind();
    static std::string getMobileSystemVersion();
};

#endif /* systemInfo_h */
