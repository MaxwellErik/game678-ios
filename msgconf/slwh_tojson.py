from xml.dom import minidom
import json

doc = minidom.parse("slwh.xml")
root = doc.documentElement
msgIdNodes = root.getElementsByTagName("MsgIds")[0].getElementsByTagName("MsgId")
msgIds = [(str(node.getAttribute("id")), int(node.getAttribute("value"), 10)) for node in msgIdNodes]
msgNodes = root.getElementsByTagName("Messages")[0].getElementsByTagName("Message")

msgIdMap = {}
for msgType in msgIds:
    msgIdMap[msgType[0]] = msgType[1]

msgArray = []
for msgNode in msgNodes:
	msgType = {}
	msgType['mainId'] = msgIdMap[msgNode.getAttribute('mainId')]
	msgType['subId'] = msgIdMap[msgNode.getAttribute('subId')]
	msgType['value'] = msgNode.getAttribute('value')
	msgType['type'] = msgNode.getAttribute('type')
	msgArray.append(msgType)
	
constants = {}
constantNodes = root.getElementsByTagName("constants")[0].getElementsByTagName('constant')
for constantNode in constantNodes:
	constants[constantNode.getAttribute("type")] = constantNode.getAttribute("value")
	
typedefs = {}
typedefNodes = root.getElementsByTagName("typedefs")[0].getElementsByTagName('typedef')
for typedefNode in typedefNodes:
	typedefs[typedefNode.getAttribute("name")] = typedefNode.getAttribute("len")
	
def parseEntityNode (node):
   attributes = {}
   valueNodes = node.getElementsByTagName("item")
   nodeType = node.getAttribute('type')
   if nodeType:
		attributes['nodetype'] = nodeType
   i = 1
   for valueNode in valueNodes:
       if valueNode.getAttribute('type') == 'ARRAY':
		   if valueNode.getAttribute('truelen'):
				attributes[i] = 'repeated ' + valueNode.getAttribute('subtype') + " "  + constants[valueNode.getAttribute('count')] + " "+ valueNode.getAttribute('name') + " " + valueNode.getAttribute('truelen')
		   else:
				attributes[i] = 'repeated ' + valueNode.getAttribute('subtype') + " "  + constants[valueNode.getAttribute('count')] + " "+ valueNode.getAttribute('name')
       else:
           attributes[i] = 'require ' + valueNode.getAttribute('subtype') + " " + valueNode.getAttribute('type') + " " + valueNode.getAttribute('name')
       i = i + 1
   return node.getAttribute("name"), attributes
   
structs = {}
structNodes = root.getElementsByTagName("structs")[0].getElementsByTagName('struct')
for structNode in structNodes:
    name, body = parseEntityNode(structNode)
    structs[name] = body
	
data = {}
data['structs'] = structs
data['msgArray'] = msgArray
data['msgIds'] = msgIdMap
data['constants'] = constants
data['typedefs'] = typedefs

print json.dumps(data)       


